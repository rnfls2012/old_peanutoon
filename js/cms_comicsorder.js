/* ////////////////////////////////// 컨텐츠 검색 - 순위 //////////////////////////////////////////// */
$( function() {
	// 정렬
	var sortables = $( "#sortable1, #sortable2" ).length;
	if(sortables > 0){
		$( "#sortable1, #sortable2" ).sortable({
		  connectWith: ".connectedSortable", 
		  cancel: ".ui-state-disabled"
		}).disableSelection();
	}

	// 삭제시
	$( ".fa-times-circle" ).click(function() {
		// 활성화
		var id = $(this).parents('li').attr('id');
		$('#'+id).removeClass('ui-state-disabled');
		// 삭제
		$(this).parents('li').remove();
	});

	// 잠금
	$( ".ranking_lock" ).click(function() {
		if($(this).hasClass('fa-lock')){
			$(this).removeClass('fa-lock');
			$(this).addClass('fa-unlock');
			$(this).next().next().val('n');
		}else{
			$(this).removeClass('fa-unlock');
			$(this).addClass('fa-lock');
			$(this).next().next().val('y');
		}
	});
} );


/* ////////////////////////////////// 컨텐츠 검색 //////////////////////////////////////////// */
function cms_search_submit(){
}

/* ////////////////////////////////// 파트너 검색 //////////////////////////////////////////// */
function partner_search_submit(){

}

/* ////////////////////////////////// 장르 추가/삭제 //////////////////////////////////////////// */
function small_add(){
	var tr_last = $('#small_list_form table tr').length;
	var tr_html = Array();
	tr_html.push('<tr class="result_hover">');
	/* 번호 */
	tr_html.push('<td class="cm_small_no text_center">'+tr_last+'');
	tr_html.push('<input class="input_small_no" type="hidden" name="small_no[]" value="'+tr_last+'" />');
	tr_html.push('</td>');
	/* 장르명 */
	tr_html.push('<td class="cm_small_name text_center">');
	tr_html.push('<input class="input_small_val" type="text" name="small_val[]" value="" />');
	tr_html.push('</td>');
	/* 관리 */
	tr_html.push('<td class="cm_small_management text_center">');
	tr_html.push('<a class="del_btn" onclick="small_del(\''+tr_last+'\');">삭제</a>');
	tr_html.push('</td>');
	tr_html.push('</tr>');
	$('#small_list_form table').append(tr_html.join("\n"));
}
function small_del(this_index){
	$('#small_list_form table tr:eq('+this_index+')').remove();
	$('#small_list_form table tr').each(function(idx, val){
		if(idx != 0){ 
			// console.log(idx);
			$(this).children('.cm_small_no').html(idx+'<input class="input_small_no" type="hidden" name="small_no[]"  value="'+idx+'" />');
			if($(this).children('.cm_small_management').text().trim() != '삭제불가'){
				$(this).children('.cm_small_management').html('<a class="del_btn" onclick="small_del(\''+idx+'\');">삭제</a>'); 
			}
		}		
	});
}
/* 업데이트 검사 */
function small_list_submit(){
	var small_name_ckeck = '';
	$('#small_list_form table tr .cm_small_name').each(function(idx, val){	
		if($(this).children().val() == ""){
			alert('장르명을 입력해주시거나 삭제 버튼을 눌러 주세요.');
			$(this).children().focus();
			small_name_ckeck = idx;
			return false;
		}
	});
	if(small_name_ckeck != ''){ return false; }
}
/* ////////////////////////////////// 컨텐츠 분류 - 키워드 //////////////////////////////////////////// */
/* 키워드 검색 */
function keyword_search_submit(){

}
/* 키워드 등록/수정/삭제 */
function keyword_write_submit(){
	/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
	var onlynumber = onlynumber_text = "";
	var onlynumber_check = true;
	$('.onlynumber').each(function() {
		onlynumber = $(this).val();
		onlynumber_text = '숫자만 입력해주세요';
		if(onlynumber != ''){
			var pattern = /(^[0-9]+$)/;
			if (!pattern.test(onlynumber)) {
				alert(onlynumber_text);
				$(this).focus();
				onlynumber_check = false;
				return false; 
			}
		}
	});
}

/* ////////////////////////////////// 컨텐츠 검색 - 순위 //////////////////////////////////////////// */
function ranking_search_submit(){

}

/* ////////////////////////////////// 컨텐츠 검색 - 순위 //////////////////////////////////////////// */
function comics_ranking_submit(){

}