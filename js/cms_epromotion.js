$(function() {

});

function send_push_data (paramId) {

    var boolean = confirm('전송하시겠습니까?');

    if (boolean) {
        // user confirm OK
        $.ajax({
            url: './push_ajax.php',
            method: "POST",
            data: {id: paramId}
        })
            .done(function (data) {
                // success
                
                /**
                 * TODO : process callback data
                 */
                alert(data);
            })
            .fail(function () {
                // fail
                alert('관리자에게 문의 바랍니다.');
            })
            .always(function () {
                // always execute
            })

    } else {
        // user confirm CANCEL
        alert('취소');
    }
}