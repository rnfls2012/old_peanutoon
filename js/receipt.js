function receipt(device, ret_url)
{
	if(device == 'pc'){
		opener.parent.location.href = ret_url;
		window.close();
		self.close();
		window.open("", "_self").close();
	}else{
		document.location.href = ret_url;
	}
}