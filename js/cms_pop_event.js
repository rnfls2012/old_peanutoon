$(function(){
	er_calc_way($("#er_calc_way option:selected").val());
/* ////////////////////// 충전-지급&할인 제어 ////////////////////// */
$("#er_calc_way").change(function() {
	$("#event_write tr").show();
	er_calc_way($(this).val());
});

/* ////////////////////// 쿠폰 제어 ////////////////////// */
$("#ecm_overlap").click(function() {
	$("#ecm_mode").val("overlap");
});

$("#ecm_state").click(function() {
	$("#ecm_mode").val("state");
});


/* ////////////////////// 충전-지급&할인 제어 ////////////////////// */
function er_calc_way(val){
	if(val == 'd'){
		$("#er_cash_point").val(0).parent().parent().hide();
		$("#er_point").val(0).parent().parent().hide();
	}else{
		$("#er_dc_percent").val(0).parent().parent().hide();
	}
}

/* ////////////////////// 무료&코인할인 ////////////////////// */
	// 정렬
	var $comics_list = $("#comics_list ul");
	if($comics_list.length > 0){
		$comics_list.sortable();
		$comics_list.disableSelection();
	}

	$('#comics_search_btn').click(function(e){
		comics_search_result();
	});

	/* 이미지 열기 */
	$('#event_write table td .ep_cover p').click(function(e){
		$(this).next().slideToggle();
	});

	$('#event_write table td .ep_collection_top p').click(function(e){
		$(this).next().slideToggle();
	});

	/* ////////////////////// 투표 ////////////////////// */
	$('#comics_search_vote_btn').click(function(e){
		comics_search_vote_result();
	});

	/* ////////////////////// 랜덤박스 ////////////////////// */
	if($("#er_event_type").val() == "f" && $("#mode").val() == "mod") {
		$("#er_first_count").css("display", "inline-block");
	} // end if

	$("#er_event_type").change(function() {
		if($(this).val() == "f") {
			$("#er_first_count").css("display", "inline-block");
		} else {
			$("#er_first_count").val(0);
			$("#er_first_count").css("display", "none");
		} // end else
	});
});
/* ////////////////////// 충전/지급 등록 ////////////////////// */
/* 등록 */
function er_date_type_chk(this_val){ /* 이벤트 기간 달력 */
	if(this_val == "n"){
		$('.er_date_type_view').hide();
	}else{
		$('.er_date_type_view').show();
	}
}
function event_recharge_write_submit(){
	/* 필수항목 체크 */
	var form = document.event_recharge_write_form;
	
	if(form.er_date_type.value == '0'){
		if(form.er_s_date.value==''){ alert('시작일 체크해주세요'); return false; }
		if(form.er_e_date.value==''){ alert('종료일 체크해주세요'); return false; }
	}

	/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
	var onlynumber = onlynumber_text = "";
	var onlynumber_check = true;
	$('.onlynumber').each(function() {
		onlynumber = $(this).val();
		onlynumber_text = $(this).attr('placeholder');
		onlynumber_text = onlynumber_text.replace(" 입력해주세요","에 숫자만 입력해주세요");
		if(onlynumber != ''){
			var pattern = /(^[0-9]+$)/;
			if (!pattern.test(onlynumber)) {
				alert(onlynumber_text);
				$(this).focus();
				onlynumber_check = false;
				return false; 
			}
		}
	});
	if(onlynumber_check == false){ return false; }
}

/* ////////////////////// 무료&코인할인 ////////////////////// */
/* 등록 */
function ep_date_type_chk(this_val){ /* 이벤트 기간 달력 */
	if(this_val == "n"){
		$('.ep_date_type_view').hide();
	}else{
		$('.ep_date_type_view').show();
	}
}

/* 컨텐츠 검색 엔터제어 */
function comics_search_chk(){
	if(event.keyCode == 13){
		comics_search_result();
		a_click_false();
	}
}

/* 컨텐츠 검색 결과값 가져오기 */
function comics_search_result(){
	var html_arr = new Array();
	var comics_list_except = new Array();
	var comics_search = $('#comics_search').val();

	/* 이벤트 컨텐츠 리스트의 컨텐트는 빼고 가져오기 */
	if($("#comics_list ul li").length > 0){
		$("#comics_list ul li").each(function() {
			comics_list_except.push($(this).attr('data_comics'));
		});
	}
		// console.log(comics_search);
		// console.log(comics_list_except.join(","));
	var search_access = $.ajax({
		url: "./free_dc_ajax.php",
		dataType: "json",
		type: "POST",
		data: { comics_search: comics_search, comics_list_except: comics_list_except.join(",") },
		beforeSend: function( data ) {
			//console.log('로딩');
			$('#link_check_view').removeClass("dis_none");
		}
	})
	search_access.done(function( data ) {
		html_arr.push('<ul>');
		var onclick_val = '';
		$(data).each(function(idx){
			onclick_val= '"'+data[idx]['comics']+'"';
			onclick_val+= ',"'+data[idx]['series']+'"';
			onclick_val+= ',"'+data[idx]['adult']+'"';
			onclick_val+= ',"'+data[idx]['pay']+'"';
			onclick_val+= ',"'+data[idx]['pay_view']+'"';
			onclick_val+= ',"'+data[idx]['pay_unit']+'"';
			onclick_val+= ',"'+data[idx]['episode_total']+'"';
			onclick_val+= ',"'+data[idx]['episode_total_view']+'"';

			html_arr.push('<li id="li_comics'+data[idx]['comics']+'">');
				html_arr.push('<a  href="#'+data[idx]['comics']+'" onclick=\'comics_list_add('+onclick_val+');\'>');
				html_arr.push(data[idx]['series']);
				html_arr.push(' (가격:'+data[idx]['pay_view']+',');
				html_arr.push(' 총화:'+data[idx]['episode_total_view']+')');
				html_arr.push('</a>');
			html_arr.push('</li>');
		});
		html_arr.push('</ul>');
		$('#comics_search_result').html(html_arr.join("\n"));
		//console.log('성공');
	});
	search_access.fail(function( data ) {
		//console.log('실패');
	});
	search_access.always(function( data ) {
		//console.log('로딩삭제');
	});
}

function comics_list_add(comics, project, adult, pay, pay_view, pay_unit, content_total, content_total_view){
	var html_arr = new Array();
	html_arr.push('<li id="li_add_comics'+comics+'" class="result_hover" data_comics="'+comics+'">');
	html_arr.push('<input type="hidden" name="epc_comics[]" value="'+comics+'"/>');
	html_arr.push('<p>');
	html_arr.push(project);
	html_arr.push(' (가격:'+pay_view+',');
	html_arr.push(' 총화:'+content_total_view+', '+adult+')');
	html_arr.push('<a href="#comics_list_del" onclick=\'comics_list_del('+comics+');\'>');
	html_arr.push('<img src="'+nm_img+'cms/free_dc_del.png" alt="삭제" />');
	html_arr.push('</a>');
	html_arr.push('</p>');
	html_arr.push('<label>이벤트할인가격 : </label><input type="text" class="comics_list_add_input onlynumber" placeholder="숫자만" name="epc_sale_pay[]" value="" onchange=\'data_limit_check(this);\' autocomplete="off" maxlength="5" data_limit="'+pay+'" />'+pay_unit);
	html_arr.push(' , ');
	html_arr.push('<label>이벤트할인화수: </label><input type="text" class="comics_list_add_input onlynumber" placeholder="숫자만" name="epc_sale_e[]" value="" onchange=\'data_limit_check(this);\' autocomplete="off" maxlength="5" data_limit="'+content_total+'" />');
	html_arr.push(' , ');
	html_arr.push('<label>이벤트무료화수 : </label><input type="text" class="comics_list_add_input onlynumber" placeholder="숫자만" name="epc_free_e[]" value="" onchange=\'data_limit_check(this);\' autocomplete="off" maxlength="5"  data_limit="'+content_total+'" />');
	html_arr.push('</li>');
	$('#comics_list ul').append(html_arr.join("\n"));
	$('#comics_search_result ul li#li_comics'+comics).remove();

}
function comics_list_del(comics){
	$('#li_add_comics'+comics).remove();
	if($('#comics_search').val() != ''){
		comics_search_result();
	}
}

function data_limit_check(thisval){
	var this_val = Number(thisval.value);
	var this_limit = Number(thisval.getAttribute('data_limit'));
	if(this_val != ''){
		var pattern = /(^[0-9]+$)/;
		if (!pattern.test(this_val)) {
			alert('숫자만 입력해주세요');
			thisval.value = '';
			thisval.focus();
			return false; 
		}
		if(this_val > this_limit){
			alert('입력하신 숫자가 '+this_limit+' 한계값을 넘으셨습니다.');
			thisval.value = '';
			thisval.focus();
			return false; 
		}
	}
}

function event_free_dc_write_submit(){

	/* 필수항목 체크 */
	var form = document.event_free_dc_write_form;

	if(form.mode.value==''){ alert('mode 체크해주세요'); return false; }
	if(form.name.value==''){ alert('name 체크해주세요'); return false; }
	if(form.textt.value==''){ alert('textt 체크해주세요'); return false; }
	if(form.adult.value==''){ alert('adult 체크해주세요'); return false; }

	if(form.date_use.value == '0'){
		if(form.date_start.value==''){ alert('시작일 체크해주세요'); return false; }
		if(form.date_end.value==''){ alert('종료일 체크해주세요'); return false; }
	}
	/* 이미지 체크 */
	if(form.bener.value == ''){ 
		if($('#bener').attr('data-value') == ''){
			alert('bener 체크해주세요'); $('#bener').focus(); return false; 
		}
	}
	
	/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
	var onlynumber = onlynumber_text = "";
	var onlynumber_check = true;
	$('.onlynumber').each(function() {
		onlynumber = $(this).val();
		onlynumber_text = "숫자만 입력해주세요";
		if(onlynumber != ''){
			var pattern = /(^[0-9]+$)/;
			if (!pattern.test(onlynumber)) {
				alert(onlynumber_text);
				$(this).focus();
				onlynumber_check = false;
				return false; 
			}
		}
	});
	if(onlynumber_check == false){ return false; }

	/* 할인가격과 할인화수중 하나 입력시 나머지 빈칸일 경우 */
	var epc_sale_e =  epc_sale_pay = '';
	var epc_sale_check = true;
	$('#comics_list ul li').each(function() {
		epc_sale_pay = $(this).find('input[name="epc_sale_pay[]"]').val();
		epc_sale_e = $(this).find('input[name="epc_sale_e[]"]').val();
		
		if(epc_sale_pay !='' && epc_sale_e ==''){
			epc_sale_check = false;
			alert('이벤트할인화수을 입력해주세요.');
			$(this).find('input[name="epc_sale_e[]"]').focus();
			return false; 
			
		}else if(epc_sale_pay =='' && epc_sale_e !='' ){
			epc_sale_check = false;
			alert('이벤트할인가격을 입력해주세요.');
			$(this).find('input[name="epc_sale_pay[]"]').focus();
			return false; 
		}
	});
	if(epc_sale_check == false){ return false; }

	/* 이벤트 컨텐츠 리스트 갯수가 1개 이상 */
	if($('#comics_list ul li').length < 1){
		alert('이벤트 컨텐츠 리스트를 추가해주세요.');
		return false; 
	}
}


/* ////////////////////// 투표 ////////////////////// */
/* 컨텐츠 검색 엔터제어 */
function comics_search_vote_chk(){
	if(event.keyCode == 13){
		comics_search_vote_result();
		a_click_false();
	}
}

/* 컨텐츠 검색 결과값 가져오기 */
function comics_search_vote_result(){
	var html_arr = new Array();
	var comics_list_except = new Array();
	var comics_search = $('#comics_search').val();

	/* 이벤트 컨텐츠 리스트의 컨텐트는 빼고 가져오기 */
	if($("#comics_list ul li").length > 0){
		$("#comics_list ul li").each(function() {
			comics_list_except.push($(this).attr('data_comics'));
		});
	}
		// console.log(comics_search);
		// console.log(comics_list_except.join(","));
	var search_vote_access = $.ajax({
		url: "./vote_ajax.php",
		dataType: "json",
		type: "POST",
		data: { comics_search: comics_search, comics_list_except: comics_list_except.join(",") },
		beforeSend: function( data ) {
			//console.log('로딩');
			$('#link_check_view').removeClass("dis_none");
		}
	})
	search_vote_access.done(function( data ) {
		html_arr.push('<ul>');
		var onclick_val = '';
		$(data).each(function(idx){
			onclick_val= '"'+data[idx]['comics']+'"';
			onclick_val+= ',"'+data[idx]['series']+'"';

			html_arr.push('<li id="li_comics'+data[idx]['comics']+'">');
				html_arr.push('<a  href="#'+data[idx]['comics']+'" onclick=\'comics_list_vote_select('+onclick_val+');\'>');
				html_arr.push(data[idx]['series']);
				html_arr.push(' (번호:'+data[idx]['comics']+')');
				html_arr.push('</a>');
			html_arr.push('</li>');
		});
		html_arr.push('</ul>');
		$('#comics_search_vote_result').html(html_arr.join("\n"));
		//console.log('성공');
	});
	search_vote_access.fail(function( data ) {
		//console.log('실패');
	});
	search_vote_access.always(function( data ) {
		//console.log('로딩삭제');
	});
}

function comics_list_vote_select(comics, series){
	$('#ev_comics').val(comics);
	$('#comics_vote').html(series);
}

function event_vote_write_submit(){
}

function event_randombox_write_submit() {
	if($('#s_er_date').val() == "" || $('#e_er_date').val() == "") {
		alert("이벤트 기간을 입력해주세요!");
		return false;
	} // end if

	if($('#s_available_date').val() == "" || $('#e_available_date').val() == "") {
		alert("유효기간을 입력해주세요!");
		return false;
	} // end if
}