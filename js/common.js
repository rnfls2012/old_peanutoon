/* 구글 아널리틱스 */
(function(i, s, o, g, r, a, m) {
	i['GoogleAnalyticsObject'] = r;
	i[r] = i[r] ||
	function() {
		(i[r].q = i[r].q || []).push(arguments)
	}, i[r].l = 1 * new Date();
	a = s.createElement(o), m = s.getElementsByTagName(o)[0];
	a.async = 1;
	a.src = g;
	m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-41354734-1', 'm-zzang.com');
ga('send', 'pageview');

function openNewWindow(window, w, h) {
	open(window, "NewWindow", "left=0, top=0, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, width=" + w + ", height=" + h);
}

function link(url) {
	window.document.url = url;
	location.href = window.document.url;
}

/*
// 새로고침했을때 맨 위로 이동
window.addEventListener('load',function(){
	
	setTimeout(scrollTo, 0, 1, 1);
}, false);

if (navigator.userAgent.indexOf('iPhone') != -1) {
	addEventListener("load", function() {
		setTimeout(hideURLbar, 0);
	}, false);
}
// 위에껀 아이폰
// 요 아래껀 다른폰
*/

function hideURLbar() {
	window.addEventListener('load', function() {
		setTimeout(scrollTo, 0, 0, 1);
	}, false);
}

/* a태그 #책받임 기능 막기 */
function a_click_false(){
	event.preventDefault(); //FF
	event.returnValue = false; //IE
}

/* 팝업 */
function popup(url,frame,w,h) {
	var bwidth = 700;
	var bheight =700;
	if(w == 0 || w == "" || typeof w == "undefined"){ w = bwidth; }
	if(h == 0 || h== ""  || typeof h == "undefined"){ h = bheight; }
	if(frame== "" || typeof frame == "undefined"){ frame = "newpopup"; }
	var popup_win = window.open(url, frame, "left=0, top=0, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width="+w+", height="+h);
	popup_win.resizeTo(w, h);
	/* popup_win.moveTo(0, 0); */
	a_click_false();
}

/* 팝업 자동 리사이징 */
/*
 * 팝업 자동 리사이징
 *  - 윈도 환경에 따라 사이즈가 다를 수 있습니다.
 *  - 팝업페이지의 스크립트 최하단에서 실행하십시오.
 *
 * (ex.) window.onload = function(){popupAutoResize();}
*/
function popup_resize(mode) {
	if(!(mode == 'reg' || mode == 'mod')){
	/* 등록 or 수정이 아니라면... */
		var thisX = thisY = '';
		if(navigator.userAgent.indexOf('Chrome') != -1 || navigator.userAgent.indexOf('Safari') != -1){
			//scrolltop = event.clientY+document.body.scrollTop;
			this_W = document.body.scrollWidth;
			//this_H = document.body.scrollHeight;
		}
		else{
			this_W = document.documentElement.scrollWidth;
			//this_H = document.documentElement.scrollHeight;
		}

		this_H = $('body').height();

		var thisX = parseInt(this_W + (window.outerWidth - window.innerWidth) );
		var thisY = parseInt(this_H + (window.outerHeight - window.innerHeight) );
		// 자세한 설명 http://sometimes-n.tistory.com/22

		var maxThisX = screen.width - 50;
		var maxThisY = screen.height - 50;
		// 자세한 설명 http://mohwaproject.tistory.com/entry/%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8-Dimension-Screen-Viewport-Scroll-Element-Mouse-Event

		var marginY = 0;
		// alert(thisX + "===" + thisY);

		if (thisX > maxThisX) {
			window.document.body.scroll = "yes";
			thisX = maxThisX;
		}
		if (thisY > maxThisY - marginY) {
			window.document.body.scroll = "yes";
			thisX += 19;
			thisY = maxThisY - marginY;
		}
		window.resizeTo(thisX+10, thisY+marginY);

		// 센터 정렬
		// var windowX = (screen.width - (thisX+10))/2;
		// var windowY = (screen.height - (thisY+marginY))/2 - 20;
		// window.moveTo(windowX,windowY);

		// 자세한 설명  http://gnujava.com/board/article_view.jsp?article_no=2619&board_no=11&table_cd=EPAR04&table_no=04
	}
}

// 이메일형식 검사
function email_chk(email)
{
	var email_check = true;
    if (!email){ 
		email_check = false;
	}else{
		//var pattern = /(\S+)@(\S+)\.(\S+)/; 이메일주소에 한글 사용시
		var pattern = /([0-9a-zA-Z_-]+)@([0-9a-zA-Z_-]+)\.([0-9a-zA-Z_-]+)/;
		if (!pattern.test(email)) {
			email_check = false;
		}
	}
	return email_check;
}
