$(function() {
	/* 코믹스 검색 */
	$('#comics_search_btn').click(function(e){
		comics_search_result();
	});
	
	/* 분류 선택 */
	$("#ccp_class").change(function(e) {
		if($(this).val() != "") {
			var ccp_class = $(this).val() // 작가 : pf, 제공사 : pv, 출판사 : pb

			/* 작품이 선택되어있을 시 */
			if($("#comics_list ul li").length > 0) {
				$("#"+ccp_class+"_arr").val($("#cm_"+ccp_class).val()).prop("selected", true); // 선택한 ccp_class에 대한 값 자동으로 selected
			} // end if

			$(".display_control").children("div").css('display', 'none');
			$("."+ccp_class).css('display', 'inline-block');
		} else {
			$(".display_control").children("div").css('display', 'none');
		} // end else
	});

	$("#ccp_class_sub").change(function(e) {
		if($(this).val() != "") {
			var ccp_class_sub = $(this).val()+"_sub" // 작가 : pf, 제공사 : pv, 출판사 : pb

			/* 작품이 선택되어있을 시 */
			if($("#comics_list ul li").length > 0) {
				$("#"+ccp_class_sub+"_arr").val($("#cm_"+ccp_class_sub).val()).prop("selected", true); // 선택한 ccp_class에 대한 값 자동으로 selected
			} // end if

			$(".display_control_sub").children("div").css('display', 'none');
			$("."+$(this).val()+"_sub").css('display', 'inline-block');
		} else {
			$(".display_control_sub").children("div").css('display', 'none');
		}
	});
});

/* 컨텐츠 검색 엔터제어 */
function comics_search_chk(){
	if(event.keyCode == 13) {
		comics_search_result();
		a_click_false();
	}
}

/* 컨텐츠 검색 결과값 가져오기 */
function comics_search_result(){
	var html_arr = new Array();
	var comics_list_except = "";
	var comics_search = $('#comics_search').val();

	/* 이벤트 컨텐츠 리스트의 컨텐트는 빼고 가져오기 */
	if($("#comics_list ul li").length > 0){
		$("#comics_list ul li").each(function() {
			comics_list_except = $(this).attr('data_comics');
		});
	}

	var search_access = $.ajax({
		url: "./percentage_ajax.php",
		dataType: "json",
		type: "POST",
		data: { comics_search: comics_search, comics_list_except: comics_list_except }
	})

	search_access.done(function( data ) {
		html_arr.push('<ul>');
		var onclick_val = '';
		$(data).each(function(idx){
			onclick_val= '"'+data[idx]['comics']+'"';
			onclick_val+= ',"'+data[idx]['series']+'"';
			onclick_val+= ',"'+data[idx]['adult']+'"';
			onclick_val+= ',"'+data[idx]['pay']+'"';
			onclick_val+= ',"'+data[idx]['pay_view']+'"';
			onclick_val+= ',"'+data[idx]['pay_unit']+'"';
			onclick_val+= ',"'+data[idx]['episode_total']+'"';
			onclick_val+= ',"'+data[idx]['episode_total_view']+'"';
			onclick_val+= ',"'+data[idx]['pv']+'"';
			onclick_val+= ',"'+data[idx]['pv_sub']+'"';
			onclick_val+= ',"'+data[idx]['pf']+'"';
			onclick_val+= ',"'+data[idx]['pf_sub']+'"';
			onclick_val+= ',"'+data[idx]['pb']+'"';

			html_arr.push('<li id="li_comics'+data[idx]['comics']+'">');
				html_arr.push('<a  href="#'+data[idx]['comics']+'" onclick=\'comics_list_add('+onclick_val+');\'>');
				html_arr.push(data[idx]['series']);
				html_arr.push(' (가격:'+data[idx]['pay_view']+',');
				html_arr.push(' 총화:'+data[idx]['episode_total_view']+')');
				html_arr.push('</a>');
			html_arr.push('</li>');
		});
		html_arr.push('</ul>');
		$('#comics_search_result').html(html_arr.join("\n"));
		
		//console.log('성공');
	});
	search_access.fail(function( data ) {
		//console.log('실패');
	});
	search_access.always(function( data ) {
		//console.log('로딩삭제');
	});
}

function comics_list_add(comics, project, adult, pay, pay_view, pay_unit, content_total, content_total_view, pv, pv_sub, pf, pf_sub, pb){
	/* 한 개 이상 선택 안 되도록 */
	if($("#comics_list ul li").length > 0) {
		alert("한 개 이상의 작품을 선택할 수 없습니다.");
		return false;
	} // end if

	var html_arr = new Array();
	html_arr.push('<li id="li_add_comics'+comics+'" class="result_hover" data_comics="'+comics+'">');
	html_arr.push('<input type="hidden" id="ccp_comics" name="ccp_comics" value="'+comics+'"/>');
	html_arr.push('<p>');
	html_arr.push(project);
	html_arr.push(' (가격:'+pay_view+',');
	html_arr.push(' 총 화:'+content_total_view+', '+adult+')');
	html_arr.push('<a href="#comics_list_del" onclick=\'comics_list_del('+comics+');\'>');
	html_arr.push('<img src="'+nm_img+'cms/free_dc_del.png" alt="삭제" />');
	html_arr.push('</a>');
	html_arr.push('</p>');
	html_arr.push('</li>');
	$('#comics_list ul').append(html_arr.join("\n"));
	$('#comics_search_result ul li#li_comics'+comics).remove();
	
	$("#cm_pv").val(pv);
	$("#cm_pv_sub").val(pv_sub);
	$("#cm_pf").val(pf);
	$("#cm_pf_sub").val(pf_sub);
	$("#cm_pb").val(pb);

	// ccp_class, ccp_class_sub 값이 이미 선택되어 있을 경우, 작품을 선택하면 ccp_class에 맞게 자동 셀렉트
	if($("#ccp_class").val() != "") {
		var ccp_class = $("#ccp_class").val();
		$("#"+ccp_class+"_arr").val($("#cm_"+ccp_class).val()).prop("selected", true);
	} // end if

	if($("#ccp_class_sub").val() != "") {
		var ccp_class_sub = $("#ccp_class").val()+"_sub";
		$("#"+ccp_class_sub+"_arr").val($("#cm_"+ccp_class_sub).val()).prop("selected", true);
	} // end if
}

function comics_list_del(comics){
	$('#li_add_comics'+comics).remove();

	// 셀렉트 값 지우기
	$("#ccp_class").val("").prop("selected", true);
	$("#ccp_class_sub").val("").prop("selected", true);
	$(".display_control").children("div").children($("input:checkbox")).val("").prop("selected", true);
	$(".display_control_sub").children("div").children($("input:checkbox")).val("").prop("selected", true);
	$(".display_control").children("div").css('display', 'none');
	$(".display_control_sub").children("div").css('display', 'none');

	if($('#comics_search').val() != ''){
		comics_search_result();
	}
}

function sales_ccp_write_submit() {
	var ccp_class = $("#ccp_class").val();
	var ccp_class_sub = $("#ccp_class_sub").val();

	if(!$("#ccp_comics").length && $("#mode").val() == "reg") {
		alert("작품을 선택하세요!");
		$(".comics_search").focus();
		return false;
	} // end if

	if(ccp_class == "") {
		alert("분류 1을 선택하세요!");
		$("#ccp_class").focus();
		return false;
	} // end if
	
	if(ccp_class != "" && $("#"+ccp_class+"_arr").val() == "") {
		alert("분류 1에 대한 설정을 해주세요!");
		$("#"+ccp_class+"_arr").focus();
		return false;
	} // end if

	if($("#ccp_class_per").val() == 0) {
		alert("분류 1에 대한 배율을 입력 해주세요!");
		$("#ccp_class_per").focus();
		return false;
	} // end if

	if(ccp_class_sub != "" && $("#"+ccp_class_sub+"_sub_arr").val() == "") {
		alert("분류 2에 대한 설정을 해주세요!");
		$("#"+ccp_class_sub+"_sub_arr").focus();
		return false;
	} // end if

	if(($("#ccp_class_sub_per").val() > 0) && ccp_class_sub == "") {
		alert("분류 2에 대한 설정을 해주세요!");
		$("#ccp_class_sub").focus();
		return false;
	} // end if

	if(ccp_class_sub != "" && $("#ccp_class_sub_per").val() == "") {
		alert("분류 2에 대한 배율을 입력 해주세요!");
		$("#ccp_class_sub_per").focus();
		return false;
	} // end if

	if(parseInt($("#ccp_class_sub_per").val()) + parseInt($("#ccp_class_per").val()) != 100) {
		// alert(parseInt($("#ccp_class_sub_per").val()) + parseInt($("#ccp_class_per").val()));
		// alert(parseInt($("#ccp_class_sub_per").val()));
		// alert($("#ccp_class_per").val());
		alert("분류 두 개에 대한 배율 합은 100이 되어야 합니다.");
		return false;
	} // end if

	if(ccp_class_sub == "" && $("#ccp_class_sub_per").val() != "" && $("#mode").val() == "reg") {
		alert("분류 2를 선택하세요!");
		$("#ccp_class_sub").focus();
		return false;
	} // end if
}