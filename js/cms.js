$(function(){
    var mouse_event = false;
	var winload_wait = false;
    var oldX = oldY = 0;

    $(document).mousemove(function(e) {
        if(oldX == 0) {
            oldX = e.pageX;
            oldY = e.pageY;
        }

        if(oldX != e.pageX || oldY != e.pageY) {
            mouse_event = true;
        }
    });
	setTimeout(winload_wait = winload_wait_fun(winload_wait), 1000);
	
    // 주메뉴
    var $gnbs = $("#gnbs ul li a"); /* 메뉴 */
    var $container = $("#container"); /* 컨텐츠 영역 */
    var $tnbs = $("#tnbs"); /* 상단 */
    $gnbs.mouseover(function() {
        $(".steps2").addClass("steps2_over");
    });
    var $steps1 = $(".steps1 ul li");
    $steps1.mouseover(function() {
        $(".steps2").addClass("steps2_over");
    });

    $container.mouseover(function() {
		if(winload_wait){
			$(".steps2").removeClass("steps2_over");
		}
    });

    $tnbs.mouseover(function() {
		if(winload_wait){
			$(".steps2").removeClass("steps2_over");
		}
    });

    $(document).bind('click',function(){
		$(".steps2").removeClass("steps2_over");
    });

	// 3뎁스 메뉴
    var $steps3_list = $(".steps3_list");
    $steps3_list.mouseover(function() {
        if(mouse_event) {
            $(this).children(".steps3").addClass("steps3_over");
        }
    });
    $steps3_list.mouseleave(function() {
		$(".steps3").removeClass("steps3_over");
    });

});

function winload_wait_fun(winload_wait){
	winload_wait = true;
	return winload_wait;
}
// console.log(mouse_event);



// 검색 관련 소스
$(function(){
	/* 검색버튼 및 최초/최신등록일 */
	var cms_submit_base_h = $('#cms_submit').attr('data-base_h');
	var cms_submit_on_h = $('#cms_submit').attr('data-on_h');
	$('#date_type').change(function(e){
		var date_type_val = $(this).val();
		if(date_type_val == ""){
			$('#cms_search .cs_calendar').slideUp('fast');
			$('#cms_submit').css('height', cms_submit_base_h+"px");
		}else{
			$('#cms_search .cs_calendar').slideDown('fast');
			$('#cms_submit').css('height', cms_submit_on_h+"px");
		}
	});

	/* 필드 고정 - 사용안함 17-11-15 */
	/*
	var cr_thead = $("#cr_thead").length;
	// console.log(cr_thead); // 존재여부 체크
	if(cr_thead > 0){
		cr_thead_top = $("#cr_thead").offset().top;
		//var cr_thead_h = $('#cr_thead').height();
		cr_thead_h = 27;// 위 height에서 제멋대로 값이 바낌 
			
		if(typeof cr_thead_mg_add != 'undefined'){
			var cr_thead_mg = $('#cr_thead_mg_add').css('margin-bottom');
			var cr_thead_mg_add_check = true;
		}
		
		$(window).scroll(function () { 
			ua = window.navigator.userAgent;
			scrolltop = 0;
			if(ua.indexOf('Chrome') != -1 || ua.indexOf('Safari') != -1){
				//scrolltop = event.clientY+document.body.scrollTop;
				scrolltop = document.body.scrollTop;
			}
			else{
				//scrolltop = event.clientY+document.documentElement.scrollTop;
				scrolltop = document.documentElement.scrollTop;
			}
			var cms_submit_h = $('#cms_submit').height();
			var cms_submit_scrolltop = 0;
			if(cms_submit_h == cms_submit_on_h-6){ // 6은 #cms_submit의 padding, border 높이값 
				cms_submit_scrolltop = (cms_submit_on_h - cms_submit_base_h);
			}

			if(cr_thead_top <= scrolltop+(cr_thead_h*2)+77-cms_submit_scrolltop){
			// console.log(cr_thead_top);
				$('#cr_bg').css('padding-top',cr_thead_h);
				$('#cr_thead').css({'position':'fixed','top':'130px','background':'#fff'});
				if(cr_thead_mg_add_check == true){
					$('#cr_thead_mg_add').css('margin-bottom', cr_thead_mg_add);
				}
			}else{
				$('#cr_bg').css('padding-top','0px');
				$('#cr_thead').css({'position':'relative','top':'0px','background':'none'});
				if(cr_thead_mg_add_check == true){
					$('#cr_thead_mg_add').css('margin-bottom', cr_thead_mg);
				}
			}
		});
	}
	*/

	/* 날짜버튼 */
	$('.cs_calendar .cs_calendar_btn a').click(function(e){
		var fr_date = $(this).attr("data-fr_date");
		var to_date = $(this).attr("data-to_date");
		$('#s_date').val(fr_date);
		$('#e_date').val(to_date);
	});
});
