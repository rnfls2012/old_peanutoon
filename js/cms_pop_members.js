
$(function() {
/* ////////////////////////////////// 회원가입 //////////////////////////////////////////// */

	$(".is_btn").click(function() {
		var is_btn = $(this).attr('data-mode');
		var mode = 'is_'+is_btn;
		var chk_val = $('#mb_'+is_btn).val();
		var ajax_url = $(this).attr('data-url');
		is_overlap(is_btn, mode, chk_val, ajax_url);
	});


	
/* ////////////////////////////////// 회원 상세보기 //////////////////////////////////////////// */
	
	
	var a_btn_idx = '';
	/* 회원분류&회원등급 OR 성인인증 */
	$(".field_select a").click(function() {
		$(this).parent().find('.change').toggle();
		$(this).parent().find('.modify').toggle();
		$(this).parent().find('.cancel').toggle();
		$(this).parent().find('select').toggle();
		$(this).parent().find('span').toggle();
		a_btn_idx = $(".change").index( $(this).parent().find('.change') )
		
		switch($("#mb_level").val()) {
			case '13' :
				$("#mb_marketer").show();
				$("#mb_marketer option[value="+$("#cp_no").val()+"]").attr("selected", "selected");
				break;
			case '14' :
				$("#mb_provider").show();
				$("#mb_provider option[value="+$("#cp_no").val()+"]").attr("selected", "selected");
				break;
			case '15' :
				$("#mb_author").show();
				$("#mb_author option[value="+$("#cp_no").val()+"]").attr("selected", "selected");
				break;
			case '16' :
				$("#mb_publisher").show();
				$("#mb_publisher option[value="+$("#cp_no").val()+"]").attr("selected", "selected");
				break;
		} // end switch
		
		// 취소 클릭시
		if($(this).parent().find('.cancel').css('display') == 'none'){
			$(".cp_list").hide();
		}
	});

	// 회원 등급 파트너사 선택시 (회원 등록도 포함)
	$("#mb_level").change(function() {
		$(".cp_list").hide();
		switch($(this).val()) {
			case '13' :
				$("#mb_marketer").show();
				break;
			case '14' :
				$("#mb_provider").show();
				break;
			case '15' :
				$("#mb_author").show();
				break;
			case '16' :
				$("#mb_publisher").show();
				break;
			default :
				$(".cp_list").hide();
				break;
		} // end switch
	});

	/* 회원분류&회원등급 OR 회원 성인인증 -> 적용 버튼 */
	$(".field_select a.modify").click(function() {
		var mb_field_val = $(this).attr('data-field');
		var mb_field_text =$('label[for="'+ mb_field_val +'"]').text();

		$("#mb_field").val(mb_field_val);
		$("#mb_field_text").val(mb_field_text);
		$("#member_view_form").submit();
	});

	/* 비밀번호 임시변경&E-MAIL -> 변경 버튼 */
	$(".field_text a.change").click(function() {
		var mb_field_val =  $(this).attr('data-field');
		var mb_field_text =$('label[for="'+ mb_field_val +'"]').text();

		$("#mb_field").val(mb_field_val);
		$("#mb_field_text").val(mb_field_text);
		$("#member_view_form").submit();
	});

	/* 포인트&캐쉬포인트 */
	$(".field_point a").click(function() {
		$(".cp_list").hide();
		$(this).parent().find('.change').toggle();
		$(this).parent().find('.modify').toggle();
		$(this).parent().find('.cancel').toggle();
		$(this).parent().find('p').toggle();
		a_btn_idx = $(".change").index( $(this).parent().find('.change') )
		a_display(a_btn_idx);
	});

	/* 포인트&캐쉬포인트 -> 적용 버튼 */
	$(".field_point a.modify").click(function() {
		var mb_field_val =  $(this).attr('data-field');
		var mb_field_text =$('label[for="'+ mb_field_val +'"]').text();

		$("#mb_field").val(mb_field_val);
		$("#mb_field_text").val(mb_field_text);
		$("#member_view_form").submit();
	});
	
	/* 미니땅콩&땅콩 내역 구별 */
	$(".mpu_point_class").change(function() {
		$("#member_point_form").submit();
	});
});


// 회원가입
function members_write_submit(){
	
	var form = document.members_write_form;

	if(form.mb_email.value == ''){
		alert("이메일을 체크해주세요.");
		form.mb_email.focus();
		return false;
	}else{
		email_check = email_chk(form.mb_email.value);
		/* 3/30 이메일 중복처리였으나 필요성이 없어서, 주석처리
		if(email_check == false){	
			email_check_msg = "이메일주소 형식이 아닙니다.";
			alert(email_check_msg);

			// 이메일 폼&버튼 처리 
			$('#mb_email').attr("readonly",false);
			$('#mb_email').removeClass("readonly");

			$('#is_email_text').text("이메일 중복체크 해주세요.");
			$('#is_email').val('');
			$('#is_email_btn').show();

			form.mb_email.focus();
			return false;
		}
		*/
	}
	// if(form.is_email.value==''){ alert('이메일 중복체크 해주세요'); form.mb_email.focus(); return false; }	
	if(form.is_id.value==''){ alert('아이디 중복체크 해주세요'); form.mb_id.focus(); return false; }	

	if(form.mb_id.value==''){ alert('아이디 입력해주세요'); form.mb_id.focus(); return false; }	
	if(form.mb_pass.value==''){ alert('비밀번호 입력해주세요'); form.mb_pass.focus(); return false; }	
	if(form.mb_name.value==''){ alert('이름 입력해주세요'); form.mb_name.focus(); return false; }	
	if(form.mb_level.value==''){ alert('회원레벨 입력해주세요'); form.mb_level.focus(); return false; }	
	if(form.mb_class.value==''){ alert('회원분류 입력해주세요'); form.mb_class.focus(); return false; }	
	if(form.mb_adult.value==''){ alert('성인여부 입력해주세요'); form.mb_adult.focus(); return false; }	
	

}

function is_overlap(is_btn, mode, chk_val, ajax_url){ // 중복체크
	var errer_arr = new Array();
	var overlap_access = $.ajax({
		url: ajax_url,
		dataType: "json",
		type: "POST",
		data: { mode: mode, chk_val: chk_val },
		beforeSend: function( data ) {
			//console.log('로딩');
		}
	})
	overlap_access.done(function( data ) {
		if(data['state'] == 0){
			$('#is_'+is_btn).val('ok');
			$('#'+mode+'_btn').hide();
			$('#'+mode+'_text').text('사용 가능합니다.');
			$('#mb_'+is_btn).attr("readonly",true);
			$('#mb_'+is_btn).addClass("readonly");

		}else{
			// errer_arr.push(data['state']);
			errer_arr.push(data['msg']);
			// errer_arr.push(data['error']);
			alert(errer_arr.join("\n"));
		}
		//console.log('성공');
		$('#link_check_view').addClass("dis_none");
		$(data['wr_id']).each(function(idx){
			$('#bo_list .tbl_head01 ul li.wr_id'+String(data['wr_id'][idx])).children('a.link').replaceWith(data['no_link'][idx]);
		});
	});
	overlap_access.fail(function( data ) {
		//console.log('실패');
	});
	overlap_access.always(function( data ) {
		//console.log('로딩삭제');
	});
}

// 상세정보
function a_display(a_btn_idx){
		// console.log(a_btn_idx);
	var display = '';
	$.each( $('.change'), function( idx, value ) {
		display = $(this).css('display');
		// console.log(idx+':'+display);
		if(display == 'none' && a_btn_idx != idx ){
			$(this).parent().find('.change').show();
			$(this).parent().find('.modify').hide();
			$(this).parent().find('.cancel').hide();
			$(this).parent().find('select').hide();
			$(this).parent().find('span').show();
			$(this).parent().find('p').hide();
		}
	});
}



function member_view_submit(){
	
	var form = document.member_view_form;

	/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
	var onlynumber = onlynumber_text = "";
	var onlynumber_check = true;
	$('.onlynumber').each(function() {
		onlynumber = $(this).val();
		onlynumber_text = '숫자만 입력해주세요';
		if(onlynumber != ''){
			// var pattern = /(^[0-9]+$)/;
			var pattern = /^[0-9\-]+$/;
			if (!pattern.test(onlynumber)) {
				alert(onlynumber_text);
				$(this).val('');
				$(this).focus();
				onlynumber_check = false;
				return false; 
			}
		}
	});
	if(onlynumber_check == false){ return false; }

	/* 가감사유 미입력 */
	if($("#cms_cash_point").val() != "" && $("#cms_cash_point_text").val() == "") {
		alert("가감사유를 입력해주세요!");
		$("#cms_cash_point").val("");
		return false;
	} // end if

	if($("#cms_point").val() != "" && $("#cms_point_text").val() == "") {
		alert("가감사유를 입력해주세요!");
		$("#cms_point").val("");
		return false;
	} // end if
}

function member_block() {
	$("#mode").val("block");
	$("#member_view_form").submit();
} // member_block

/* ////////////////////////////////// 구매내용 상세 //////////////////////////////////////////// */
function member_point_submit() {

} // member_point_submit

/* ////////////////////////////////// APK 이용 회원 미니당근 지급 //////////////////////////////////////////// */
function apk_down_write_submit() {
	if($("#mpi_from").val() == "") {
		alert("지급 내용을 입력해주세요!");
		return false;
	} else if($("#mb_point").val() == "" || $("#mb_point").val() <= 0) {
		alert("0보다 큰 수를 입력해주세요!");
		return false;
	} else {
		return true;
	} // end else
} // apk_down_write_submit