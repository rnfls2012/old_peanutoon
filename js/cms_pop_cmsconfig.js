$(function(){
	/* 충전 금액 */
	$('#payway_limit input[type=checkbox]').click(function(e){
		if($(this).val() == ''){
			$('#payway_limit input[type=checkbox]').not(':eq(0)').prop("checked", false);
		}else{
			$('#payway_limit input[type=checkbox]').eq(0).prop("checked", false);
		}
	});
});

function cup_write_submit() {
	var check_num = document.getElementById('cup_won').value;
	if(isNaN(check_num) || check_num <= 0) {
		var msg = "숫자만 입력해 주세요!";

		if(check_num == 0) {
			msg = "0보다 큰 수를 입력해주세요!";
		} // end if

		alert(msg);
		document.getElementById('cup_won').focus();
		return false;
	} else {
		if(confirm(check_num + " 정산 금액 등록하시겠습니까?")) {
			return true;
		} else {
			document.getElementById('cup_won').focus();
			return false;
		} // end else
	} // end else
} // cup_write_submit 숫자 아니면 쳐내기


function config_recharge_price_write_submit(){ 

	/* 필수항목 체크 */
	var form = document.config_recharge_price_write_form;

	if(form.mode.value==''){ alert('mode 체크해주세요'); return false; }
	if(form.crp_won.value==''){ alert('crp_won 체크해주세요'); return false; }
	if(form.crp_cash_point.value==''){ alert('crp_cash_point 체크해주세요'); return false; }
	if(form.crp_point_percent.value==''){ alert('crp_point_percent 체크해주세요'); return false; }
	if(form.crp_point.value==''){ alert('crp_point 체크해주세요'); return false; }

	/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
	var onlynumber = onlynumber_text = "";
	var onlynumber_check = true;
	$('.onlynumber').each(function() {
		onlynumber = $(this).val();
		onlynumber_text = "숫자만 입력해주세요";
		if(onlynumber != ''){
			var pattern = /(^[0-9]+$)/;
			if (!pattern.test(onlynumber)) {
				alert(onlynumber_text);
				$(this).focus();
				onlynumber_check = false;
				return false; 
			}
		}
	});
	if(onlynumber_check == false){ return false; }
}

function crp_point_percent_calc(){
	var crp_cash_point = $('#crp_cash_point').val();
	var point_percent = $('#crp_point_percent').val();

	if(crp_cash_point == ''){ crp_cash_point = 0;}
	if(point_percent == ''){ point_percent = 0;}

	var pattern = /(^[0-9]+$)/;
	if (!pattern.test(point_percent)) {
		alert("숫자를 입력해주세요.");
		this_tag.focus()
		return false; 
	}
	if (!pattern.test(crp_cash_point)) {
		alert("숫자를 입력해주세요.");
		$('#crp_cash_point').focus()
		return false; 
	}

	// 보너스 퍼센트 계산
	var crp_point_val = 0;
	crp_point_val = parseInt(crp_cash_point * point_percent / 10);
	$('#crp_point').val(crp_point_val);
}
/* 충전 금액 등록 */
function crp_date_type_chk(this_val){ /* 이벤트 기간 달력 */
	if(this_val == "n"){
		$('.crp_date_type_view').hide();
	}else{
		$('.crp_date_type_view').show();
	}
}


/* 전체구매시 추가지급 등록 */
function csa_date_type_chk(this_val){ /* 이벤트 기간 달력 */
	if(this_val == "n"){
		$('.csa_date_type_view').hide();
	}else{
		$('.csa_date_type_view').show();
	}
}


/* 풀 페이지 프로모션 기간 적용 */
function emf_date_type_chk(this_val) {
	if(this_val == "n"){
		$('.emf_date_type_view').hide();
	}else{
		$('.emf_date_type_view').show();
	}
} 

function config_sin_allpay_write_submit(){ 

	/* 필수항목 체크 */
	var form = document.config_recharge_price_write_form;

	if(form.mode.value==''){ alert('mode 체크해주세요'); return false; }
	if(form.crp_won.value==''){ alert('crp_won 체크해주세요'); return false; }
	if(form.crp_cash_point.value==''){ alert('crp_cash_point 체크해주세요'); return false; }
	if(form.crp_point_percent.value==''){ alert('crp_point_percent 체크해주세요'); return false; }
	if(form.crp_point.value==''){ alert('crp_point 체크해주세요'); return false; }

	/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
	var onlynumber = onlynumber_text = "";
	var onlynumber_check = true;
	$('.onlynumber').each(function() {
		onlynumber = $(this).val();
		onlynumber_text = "숫자만 입력해주세요";
		if(onlynumber != ''){
			var pattern = /(^[0-9]+$)/;
			if (!pattern.test(onlynumber)) {
				alert(onlynumber_text);
				$(this).focus();
				onlynumber_check = false;
				return false; 
			}
		}
	});
	if(onlynumber_check == false){ return false; }
}


/* ////////////////////// 티켓소켓 등록 ////////////////////// */
/* 등록 */
function tksk_date_type_chk(this_val){ /* 이벤트 기간 달력 */
	if(this_val == "n"){
		$('.er_date_type_view').hide();
	}else{
		$('.er_date_type_view').show();
	}
}
function tksk_write_write_submit(){
	/* 필수항목 체크 */
	var form = document.tksk_write_form;
	
	if(form.er_date_type.value == '0'){
		if(form.er_s_date.value==''){ alert('시작일 체크해주세요'); return false; }
		if(form.er_e_date.value==''){ alert('종료일 체크해주세요'); return false; }
	}

	/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
	var onlynumber = onlynumber_text = "";
	var onlynumber_check = true;
	$('.onlynumber').each(function() {
		onlynumber = $(this).val();
		onlynumber_text = $(this).attr('placeholder');
		onlynumber_text = onlynumber_text.replace(" 입력해주세요","에 숫자만 입력해주세요");
		if(onlynumber != ''){
			var pattern = /(^[0-9]+$)/;
			if (!pattern.test(onlynumber)) {
				alert(onlynumber_text);
				$(this).focus();
				onlynumber_check = false;
				return false; 
			}
		}
	});
	if(onlynumber_check == false){ return false; }
}