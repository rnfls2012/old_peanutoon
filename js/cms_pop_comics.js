$(function(){
	/* ////////////////// 컨텐츠 등록 ////////////////// */
	/* 이벤트 번호 넘기기 */
	$('#event_list li').click(function(e){
		var eventnum_list_class = $('#event_list').attr('data-class');
		if(eventnum_list_class == 'hide'){
			$('#event_list').attr('data-class','');
			$('#event_list li').show();
			$('#event').val('');
		}else{
			$('#event_list').attr('data-class','hide');
			$('#event_list li').hide();
			$('#event_list li').removeClass('on');
			var ep_no = $(this).attr('data-ep_no');
			if(ep_no !=''){
				$(this).addClass('on');
			}
			$(this).show();
			$('#cm_event').val(ep_no);
		}
	});

	/* 이미지 열기 */
	$('#comics_write table td .image p').click(function(e){
		$(this).next().slideToggle();
	});

	/* 줄 셀렉트 박스 */
	$('#cms_page_title #s_limit_select select').change(function(e){
		var s_limit_select_url = location.href;
		if(s_limit_select_url.search("&s_limit=") > 0){
			s_limit_select_url = s_limit_select_url.substr(0,s_limit_select_url.search("&s_limit="));
			
		}
		location.href = s_limit_select_url+"&s_limit="+$(this).val();
		
	});
	/* 키워드 카테고리 3Depth */
	$('#comics_write table td div.depths > .depth2 .depth3 input').click(function(e){
		var depth3_checked = depth3_val = depth2_val = depth1_val = '';
		depth3_checked = $(this).is(":checked");
		depth3_val = $(this).val();
		depth2_val = 'depth2_'+depth3_val.substr(0,4);
		depth1_val = 'depth1_'+depth3_val.substr(0,2);
		if(depth3_checked == true){
			$('#'+depth2_val).prop("checked", depth3_checked);
			$('#'+depth1_val).prop("checked", depth3_checked);
		}
	});
	/* 키워드 카테고리 2Depth */
	$('#comics_write table td div.depths > .depth2 input').click(function(e){
		var depth2_checked = depth2_val = depth1_val = '';
		depth2_checked = $(this).is(":checked");
		depth2_val = $(this).val();
		depth1_val = 'depth1_'+depth2_val.substr(0,2);
		if(depth2_checked == true){
			$('#'+depth2_val).prop("checked", depth2_checked);
			$('#'+depth1_val).prop("checked", depth2_checked);
		}else{
			$('.depth2_'+depth2_val).prop("checked", depth2_checked);
		}
	});
	/* 키워드 카테고리 1Depth */
	$('#comics_write table td div.depths > .depth1 input').click(function(e){
		var depth1_checked = depth1_val = '';
		depth1_checked = $(this).is(":checked");
		depth1_val = $(this).val();
		if(depth1_checked == false){
			$('.depth1_'+depth1_val).prop("checked", depth1_checked);
		}
	});

	/* ////////////////// 화 등록 ////////////////// */
	/* 이미지 열기 */
	$('#episode_write table td .episode_image p.view_btn').click(function(e){
		$(this).next().slideToggle();
		if($(this).hasClass('list_slide')){
			$(this).removeClass('list_slide');
			$('#content_file_list div').slideUp(function(){
				$('#content_file_list div .this_view').hide();				
			});
		}else{
			$(this).addClass('list_slide');
			$('#content_file_list div').slideDown();
		}
	});
	

	$('#episode_write table td div.episode_image .content_file_list img').click(function(e){
		var this_img_src = $(this).attr('src');
		var this_img_alt = $(this).attr('alt');
		$('#episode_write table td .episode_image div .this_view').attr('src',this_img_src);
		$('#episode_write table td .episode_image div .this_view').attr('alt',this_img_alt);
		$('#episode_write table td .episode_image div .this_view').show();
	});
	
	/* 연재 요일 display */
	if($('#cm_public_period_check').val() != 0) {
		$('.cm_public_cycle').css("display", "table-row");
	} // end if

	$('input[name=cm_public_period]').click(function(e) {
		if($(this).val() == 0) {
			$('.cm_public_cycle').css("display", "none");
			$('#cm_public_cycle input[type=checkbox]').prop("checked", false);
		} else {
			$('.cm_public_cycle').css("display", "table-row");
		} // end else
	});
});

function comics_write_submit(){
	/* 필수항목 체크 */
	var form = document.comics_write_form;

	
	if(form.mode.value==''){ alert('mode 체크해주세요'); return false; }
	if(form.cm_big.value==''){ alert('분류 체크해주세요'); return false; }
	if(form.cm_small.value==''){ alert('장르 체크해주세요'); $('#cm_small').focus(); return false; }
	if(form.cm_series.value==''){ alert('제목 체크해주세요'); $('#cm_series').focus(); return false; }
	
	if(form.prof_name.value==''){ alert('작가명 입력해주세요'); $('#prof_name').focus(); return false; }
	if(form.publ_name.value==''){ alert('출판사명 입력해주세요'); $('#publ_name').focus(); return false; }
	if(form.prov_name.value==''){ alert('제공사명 체크해주세요'); $('#prov_name').focus(); return false; }
	if($("#cm_platform input[type=checkbox]:checked").length == 0) {
		alert('이용환경를 하나라도 선택하셔야 합니다.');
		$('#cm_platform input[type=checkbox]:eq(0)').focus();
		return false;
	}

	if($('#cm_public_period input[type=radio]:checked').val() != 0 && $("#cm_public_cycle input[type=checkbox]:checked").length == 0) {
		alert('연재주기를 하나라도 선택하셔야 합니다.');
		$('#cm_public_cycle input[type=checkbox]:eq(1)').focus();
		return false;
	}

	if(form.cm_type.value==''){ alert('컨텐츠 유형 체크해주세요'); $('#cm_type0').focus(); return false; }
	if(form.cm_page_way.value==''){ alert('페이지 넘기는 방향 체크해주세요'); $('#cm_page_way0').focus(); return false; }
	if(form.cm_color.value==''){ alert('흑백/칼라구분 체크해주세요'); $('#cm_colory').focus(); return false; }

	if(form.cm_adult.value==''){ alert('열람등급 체크해주세요'); $('#cm_adulty').focus(); return false; }
	if(form.cm_pay.value==''){ alert('가격 체크해주세요'); $('#cm_pay0').focus(); return false; }
	if(form.cm_end.value==''){ alert('완결 구분 체크해주세요'); $('#cm_endn').focus(); return false; }
	if(form.cm_service.value==''){ alert('서비스승인 체크해주세요'); $('#cm_servicey').focus(); return false; }
	if(form.cm_up_kind.value==''){ alert('화권구별 체크해주세요'); $('#cm_up_kind0').focus(); return false; }
	//if(form.cm_own_type.value==''){ alert('소장/대여구분 체크해주세요'); $('#cm_own_type1').focus(); return false; }
	
	if(form.cm_service.value === 'r' && form.cm_res_open_date.value === '') { alert('예약날짜를 확인해주세요'); $('#cm_res_open_date').focus(); return false;}
	
	// 이미지 체크 
	if(form.cm_cover.value == '' && form.mode.value != 'del'){ 
		if($('#cm_cover').attr('data-value') == ''){
			alert('표지이미지 체크해주세요'); $('#cm_cover').focus(); return false; 
		}
	}
	if(form.cm_cover_sub.value == '' && form.mode.value != 'del'){
		if($('#cm_cover_sub').attr('data-value') == ''){
			alert('작은표지이미지 체크해주세요'); $('#cm_cover_sub').focus(); return false; 
		}
	}

	// 입력 받는 곳 중 숫자만 해야 되는 곳 검사 
	var onlynumber = onlynumber_text = "";
	var onlynumber_check = true;
	$('.onlynumber').each(function() {
		onlynumber = $(this).val();
		onlynumber_text = $(this).attr('placeholder');
		onlynumber_text = onlynumber_text.replace(" 입력해주세요","에 숫자만 입력해주세요");
		if(onlynumber != ''){
			var pattern = /(^[0-9]+$)/;
			if (!pattern.test(onlynumber)) {
				alert(onlynumber_text);
				$(this).focus();
				onlynumber_check = false;
				return false; 
			}
		}
	});
	if(onlynumber_check == false){ return false; }
}

/* 연재주기 */
function cycle_chk(cycle_key){
	/*
	if(cycle_key == 0){
		$('#cm_public_cycle input[type=checkbox]:not(:eq(0))').prop("checked", false);
	}else{
		$('#cm_public_cycle input[type=checkbox]:eq(0)').prop("checked", false);
	}
	*/
}

/* 페이지 넘기는 방향  */
function page_way_chk(val, big){
	if(big!=1){
		alert('단행본은 왼쪽 또는 오른쪽을 선택하실수 없습니다.');
		$('#cm_page_way input[type=radio]:eq(0)').prop("checked", true);
	}else{
		alert('단행본은 스크롤을 선택하실수 없습니다.');
		$('#cm_page_way input[type=radio]:eq(1)').prop("checked", true);
	}
}

/* 작가 추가 텍스트박스넣기  */
function prof_add(this_val){
	var prof_val = this_val.value;
	var prof_name = this_val.options[this_val.selectedIndex].text;
	if(prof_val == ''){ prof_name = ''; }
	$('#prof_name').val(prof_name);
	$('#cm_professional').val(prof_val);
}

function prof_add_sub(this_val){
	var prof_val = this_val.value;
	var prof_name = this_val.options[this_val.selectedIndex].text;
	if(prof_val == ''){ prof_name = ''; }
	$('#prof_name_sub').val(prof_name);
	$('#cm_professional_sub').val(prof_val);
}

function prof_add_thi(this_val){
	var prof_val = this_val.value;
	var prof_name = this_val.options[this_val.selectedIndex].text;
	if(prof_val == ''){ prof_name = ''; }
	$('#prof_name_thi').val(prof_name);
	$('#cm_professional_thi').val(prof_val);
}

/* 출판사 텍스트박스넣기 */
function publ_add(this_val){
	var publ_val = this_val.value;
	var publ_name = this_val.options[this_val.selectedIndex].text;
	if(publ_val == ''){ publ_name = ''; }
	$('#publ_name').val(publ_name);
	$('#cm_publisher').val(publ_val);
}

/* 제공사 텍스트박스넣기  */
function prov_add(this_val){
	var prov_val = this_val.value;
	var prov_name = this_val.options[this_val.selectedIndex].text;
	if(prov_val == ''){ prov_name = ''; }
	$('#prov_name').val(prov_name);
	$('#cm_provider').val(prov_val);
}

/* 제공사 추가 텍스트박스넣기  */
function prov_sub_add(this_val){
	var prov_sub_val = this_val.value;
	var prov_name_sub = this_val.options[this_val.selectedIndex].text;
	if(prov_sub_val == ''){ prov_name_sub = ''; }
	$('#prov_name_sub').val(prov_name_sub);
	$('#cm_provider_sub').val(prov_sub_val);
}

/* ////////////////////////////////// 화등록 //////////////////////////////////////////// */

/* 서비스일 */
function ce_service_state_chk(this_val){
	if(this_val == "r"){
		$('.res_service_date_view').show();
		$('.res_service_date_load').hide();
	}else{
		$('.res_service_date_view').hide();
		$('.res_service_date_load').show();
	}
}
/* 외전에피소드:화번호 */
function ce_ce_outer_chk(this_val){
	if(this_val == "n"){
		$('#ce_outer_chapter').hide();
		$('#ce_notice').attr('readonly', false);
		$('#ce_notice').removeClass('readonly');
	}else{
		$('#ce_outer_chapter').show();
		$('#ce_notice').attr('readonly', true);
		$('#ce_notice').addClass('readonly');
		$('#ce_notice').val('');
	}
}

/* 기다리면 무료일 */
function ce_free_state_chk(this_val){
	if(this_val == "r"){
		$('.res_free_date_view').show();
		$('.res_free_date_load').hide();
	}else{
		$('.res_free_date_view').hide();
		$('.res_free_date_load').show();
	}
}

function episode_write_submit(){
	/* 필수항목 체크 */
	var form = document.episode_write_form;
	
	if(form.mode.value==''){ alert('mode 체크해주세요'); return false; }
	if(form.comics.value==''){ alert('comics 체크해주세요'); return false; }
	/*
	if(form.episode.value==''){ alert('episode 체크해주세요'); return false; }
	*/
	if(form.ce_service_state.value==''){ alert('서비스상태 체크해주세요'); return false; }
	if(form.ce_free_state.value==''){ alert('기다리면 무료일 상태 체크해주세요'); return false; }

	if(form.ce_pay.value==''){ alert('가격 체크해주세요'); return false; }

	/* 이미지 체크 */
	if(form.ce_cover.value == '' && form.mode.value != 'del'){ 
		if($('#ce_cover').attr('data-value') == ''){
			alert('배너 이미지체크해주세요'); $('#ce_cover').focus(); return false; 
		}
	}
	if(form.ce_file.value == '' && form.mode.value != 'del'){
		if($('#ce_file').attr('data-value') == ''){
			alert('화 에피소드 압축파일 체크해주세요'); $('#ce_file').focus(); return false; 
		}
	}
}




/* ////////////////////////////////// 화리스트 //////////////////////////////////////////// */
function episode_list_submit(){

}

/* ////////////////////////////////// 화 수정 및 삭제 //////////////////////////////////////////// */
function back_url(move_url){
	location.href = move_url;
}



/* ////////////////////////////////// 컨텐츠 파트너 등록 //////////////////////////////////////////// */
function cp_write_submit(){
	/* 필수항목 체크 */
	var form = document.cp_write_form;
	
	if(form.mode.value==''){ alert('mode 체크해주세요'); return false; }
	if(form.cp_name.value==''){ alert('제공사명 체크해주세요'); return false; }


	/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
	var onlynumber = onlynumber_text = "";
	var onlynumber_check = true;
	$('.onlynumber').each(function() {
		onlynumber = $(this).val();
		onlynumber_text = $(this).attr('placeholder');
		onlynumber_text = onlynumber_text.replace(" 입력해주세요","에 숫자만 입력해주세요");
		if(onlynumber != ''){
			var pattern = /(^[0-9]+$)/;
			if (!pattern.test(onlynumber)) {
				alert(onlynumber_text);
				$(this).focus();
				onlynumber_check = false;
				return false; 
			}
		}
	});
	if(onlynumber_check == false){ return false; }
}

function publisher_write_submit(){
	/* 필수항목 체크 */
	var form = document.publisher_write_form;
	
	if(form.mode.value==''){ alert('mode 체크해주세요'); return false; }
	if(form.cp_name.value==''){ alert('출판사명 체크해주세요'); return false; }
}

function author_write_submit(){
	/* 필수항목 체크 */
	var form = document.author_write_form;
	
	if(form.mode.value==''){ alert('mode 체크해주세요'); return false; }
	if(form.cp_name.value==''){ alert('작가명 체크해주세요'); return false; }
}

function comics_cp(cp_tb, cp_tb_type, this_val){
	// 추후 autocomplete 기능 넣기
	/*
	var html_arr = new Array();
	var cm_field = "cm_"+cp_tb;
	if(cp_tb_type != ''){ cm_field += "_"+cp_tb_type;}

	// http://pean.nexcube.co.kr/adm/comics/list/list.write_ajax.php?cp_tb=publisher&cp_name=%EB%84%A5

	alert(cm_cp_tb);
	switch(cp_tb){
		case 'publisher' :
		case 'provider' :
		case 'professional' :
		break;
		default: alert(cp_tb+" 는 없는 항목 입니다.");
		break;
	}
	*/
}