$(function(){
	/* ////////////////// 컨텐츠 등록 ////////////////// */
	/* 이벤트 번호 넘기기 */
	$('#event_list li').click(function(e){
		var eventnum_list_class = $('#event_list').attr('data-class');
		if(eventnum_list_class == 'hide'){
			$('#event_list').attr('data-class','');
			$('#event_list li').show();
			$('#event').val('');
		}else{
			$('#event_list').attr('data-class','hide');
			$('#event_list li').hide();
			$('#event_list li').removeClass('on');
			var ep_no = $(this).attr('data-ep_no');
			if(ep_no !=''){
				$(this).addClass('on');
			}
			$(this).show();
			$('#eme_event_no').val(ep_no);
			$('#eme_event_no_text').text(ep_no);
		}
	});

	
	$('#comics_search_btn').click(function(e){
		comics_search_result();
	});

	$('#eme_event_type').change(function(e){
		var eme_et_arr = new Array();
		eme_et_arr['t'] = "editor_html";
		eme_et_arr['c'] = "comics_type_no";
		eme_et_arr['e'] = "event_type_no";
		eme_et_arr['u'] = "event_url";

		var this_val = $(this).val();

		for (var i in eme_et_arr) {
			$('#'+eme_et_arr[i]).hide();
		}
		$('#'+eme_et_arr[this_val]).show();
		
	});
	

	/* 이미지 열기 */
	$('#epromotion_write table td .eme_cover p').click(function(e){
		$(this).next().slideToggle();
	});

	/* 이미지 열기 */
	$('#epromotion_write table td .embc_cover p').click(function(e){
		$(this).next().slideToggle();
	});

 	/* 이미지 열기 */
  $('#epromotion_write table td .push_img p').click(function(e){
      $(this).next().slideToggle();
  });
    
	/* APP PUSH 수정 및 삭제 */
	if($("#push_mode").val() == "mod" || $("#push_mode").val() == "del") {
		$('.res_date').show();
	} // end if
});



/* 컨텐츠 검색 엔터제어 */
function comics_search_chk(){
	if(event.keyCode == 13){
		comics_search_result();
		a_click_false();
	}
}

/* 컨텐츠 검색 결과값 가져오기 */
function comics_search_result(){
	var html_arr = new Array();
	var comics_list_except = new Array();
	var comics_search = $('#comics_search').val();

	/* 이벤트 컨텐츠 리스트의 컨텐트는 빼고 가져오기 */
	if($("#comics_list ul li").length > 0){
		$("#comics_list ul li").each(function() {
			comics_list_except.push($(this).attr('data_comics'));
		});
	}
		// console.log(comics_search);
		// console.log(comics_list_except.join(","));
	var search_access = $.ajax({
		url: "./event_ajax.php",
		dataType: "json",
		type: "POST",
		data: { comics_search: comics_search, comics_list_except: comics_list_except.join(",") },
		beforeSend: function( data ) {
			//console.log('로딩');
			$('#link_check_view').removeClass("dis_none");
		}
	})
	search_access.done(function( data ) {
		html_arr.push('<ul>');
		var onclick_val = '';
		$(data).each(function(idx){
			onclick_val= '"'+data[idx]['comics']+'"';
			onclick_val+= ',"'+data[idx]['series']+'"';
			onclick_val+= ',"'+data[idx]['cover']+'"';

			html_arr.push('<li id="li_comics'+data[idx]['comics']+'">');
				html_arr.push('<a  href="#'+data[idx]['comics']+'" onclick=\'comics_list_add('+onclick_val+');\'>');
				html_arr.push(data[idx]['comics']+'-'+data[idx]['series']);
				html_arr.push('</a>');
			html_arr.push('</li>');
		});
		html_arr.push('</ul>');
		$('#comics_search_result').html(html_arr.join("\n"));
		//console.log('성공');
	});
	search_access.fail(function( data ) {
		//console.log('실패');
	});
	search_access.always(function( data ) {
		//console.log('로딩삭제');
	});
}

function comics_list_add(comics, series, cover){
	var html_arr = new Array();
	html_arr.push('<dl>');
	html_arr.push('<dt>');
	html_arr.push(comics+':'+series);
	html_arr.push('</dt>');
	html_arr.push('<dd>');
	html_arr.push('<img src="'+cover+'" alt="'+series+'" />');
	html_arr.push('</dd>');
	html_arr.push('</dl>');
	$('#comics_list').html(html_arr.join("\n"));
	$('#comics_search_result ul li#li_comics'+comics).remove();
	$('#eme_event_no').val(comics);
	$('#eme_event_no_text').text(comics);

}
function comics_list_del(comics){
	$('#li_add_comics'+comics).remove();
	if($('#comics_search').val() != ''){
		comics_search_result();
	}
}

function eme_date_type_chk(this_val){ /* 이벤트 기간 달력 */
	if(this_val == "n"){
		$('.eme_date_type_view').hide();
	}else{
		$('.eme_date_type_view').show();
	}
}

function emb_date_type_chk(this_val){ /* 배너 기간 달력 */
	if(this_val == "n"){
		$('.emb_date_type_view').hide();
	}else{
		$('.emb_date_type_view').show();
	}
}

function push_write_submit() {
	if($("#push_res_h").val() < 0 || $("#push_res_h").val() > 24) {
		alert("0~23 사이의 시간을 입력해주세요!");
		$("#push_res_h").val("");
		$("#push_res_h").focus();
		return false;
	} // end else if
}

function onlyNumber(obj) {
	$(obj).keyup(function(){
		$(this).val($(this).val().replace(/[^0-9]/,""));
	}); 
}