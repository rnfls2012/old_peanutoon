$(function(){
	/* 출석 이벤트2 개근날짜 자동 계산 및 입력 */
	$(".ea_start_date_select").change(function() {
		var month = $(this).data("month");
		var start_date = $(this).val();
		var end_date = $("#ea_end_date_"+month).val();
		var regu_date = (end_date - start_date) + 1;
		$("#ea_regu_date_"+month).val(regu_date);
	});
	
	$(".ea_end_date_select").change(function() {
		var month = $(this).data("month");
		var start_date = $("#ea_start_date_"+month).val();
		var end_date = ($(this).val());
		var regu_date = (end_date - start_date) + 1;
		$("#ea_regu_date_"+month).val(regu_date);
	});

	/* 출석 이벤트2 개근날짜, 연속출석날짜 비교 */
	$(".ea_cont_date_check").change(function() {
		if($(this).is(":checked")) {
			var cont_date_month = $(this).data("month");
			var regu_date = Number($("#ea_regu_date_"+cont_date_month).val());
			var ea_cont_date_val = "";
			for(var i=1; i<=3; i++) {
				var ea_cont_date_val = Number($("#ea_cont_date_"+i+"_"+cont_date_month).val());
				if(ea_cont_date_val >= regu_date) {
					alert("연속 출석일을 개근일보다 크게 설정할 수 없습니다.");
					$(this).attr("checked", false);
					$("#ea_cont_date_"+i+"_"+cont_date_month).focus();
					break;
				} // end if
			} // end for
		} // end if
	});
});

/* ////////////////////////////////// 출석이벤트 //////////////////////////////////////////// */
function event_att_submit(){

	/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
	var onlynumber = onlynumber_text = "";
	var onlynumber_check = true;
	$('.onlynumber').each(function() {
		onlynumber = $(this).val();
		onlynumber_text = '숫자만 입력해주세요';
		if(onlynumber != ''){
			var pattern = /(^[0-9]+$)/;
			if (!pattern.test(onlynumber)) {
				alert(onlynumber_text);
				$(this).focus();
				onlynumber_check = false;
				return false; 
			}
		}
	});
	if(onlynumber_check == false){ return false; }

	/* 출석 이벤트2 개근날짜 음수값 체크 */
	var regu_date_val = regu_date_month = "";
	var regu_date_check = true;
	$(".ea_regu_date_check").each(function() {
		regu_date_val = $(this).val();
		if(regu_date_val != "") {
			if(regu_date_val < 0) {
				regu_date_month = $(this).data("month");
				alert(regu_date_month+"월의 날짜 입력이 잘못되었습니다.");
				$("#ea_start_date_"+regu_date_month).focus();
				regu_date_check = false;
				return false;
			} // end if
		} // end if
	});
	if(regu_date_check == false){ return false; }

	/* 출석 이벤트2 연속 출석일수 체크 */
	var cont_date_check = true;
	var cont_date_msg = "연속출석 일수에 같은 값을 입력할 수 없습니다.";

	for(var month=1; month<=12; month++) {
		var first_cmp = second_cmp = third_cmp = cont_check = "";
		first_cmp = $("#ea_cont_date_1_"+month);
		second_cmp = $("#ea_cont_date_2_"+month);
		third_cmp = $("#ea_cont_date_3_"+month);
		cont_check = $("#ea_cont_use_"+month);
		
		// 연속출석 사용에 체크했는데, 연속출석 일수가 설정되어 있지 않을때
		if(first_cmp.val() == "" && second_cmp.val() == "" && third_cmp.val() == "" && cont_check.is(":checked")) {
			alert("연속출석을 사용하려면, 연속출석 일수를 설정해주세요.");
			first_cmp.focus();
			cont_date_check = false;
			break;
		} // end if

		// 연속출석 일수에 같은 날짜가 있는지 체크
		var empty_cnt = 0;
		if(first_cmp.val() == "") { empty_cnt++; }
		if(second_cmp.val() == "") { empty_cnt++; }
		if(third_cmp.val() == "") { empty_cnt++; }

		if(empty_cnt < 2) {
			if(first_cmp.val() == second_cmp.val()) {
				alert(cont_date_msg);
				second_cmp.focus();
				cont_date_check = false;
				break;
			} else if(first_cmp.val() == third_cmp.val()) {
				alert(cont_date_msg);
				third_cmp.focus();
				cont_date_check = false;
				break;
			} else if(second_cmp.val() == third_cmp.val()) {
				alert(cont_date_msg);
				third_cmp.focus();
				cont_date_check = false;
				break;
			} // end else if
		} // end if
	} // end for
	if(cont_date_check == false) { return false; }
}
/* ////////////////////////////////// 충전/지급 //////////////////////////////////////////// */

/* /////////////// 검색 ////////////////// */
function event_recharge_search_submit(){
}
/* ////////////////////////////////// 무료&코인할인 //////////////////////////////////////////// */

/* /////////////// 검색 ////////////////// */
function event_free_dc_search_submit(){

}

function setBlind(mb_no) {
	var boolean = confirm("상태를 변경하시겠습니까?");

	if (boolean) {
		$.ajax({
            url: './comments_ajax.php',
            method: "POST",
            data: {mb_no: mb_no}
		})
			.done(function (data) {
            // success

            /**
             * TODO : process callback data
             */
            alert(data);
            location.reload();
        	})
            .fail(function () {
                // fail
                alert('관리자에게 문의 바랍니다.');
            })
            .always(function () {
                // always execute
            })

    }
}