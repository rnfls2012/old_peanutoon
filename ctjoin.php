<?
include_once '_common.php'; // 공통

error_page();

// 리다이렉트 설정
if($_redirect !=''){
	if($is_member == false){ 
		set_redirect_cookie();
	}else{
		goto_redirect($_redirect,1);  // 로그인중이면 리다이렉트 페이지...
	}
}else{
	if($is_member == true){ // 로그인중이면 메인화면으로...
		goto_url(NM_URL);
	}
}
/*
if($is_member == true){ // 로그인중이면 메인화면으로...
	goto_url(NM_URL);
}
*/
// 리다이렉트 설정 end

/* 세션과 쿠키 동시 진행 우선순위 쿠키 > 세션 */

// 로그인 이젠 페이지 세션저장
$config_http_referer = HTTP_REFERER;
if($config_http_referer == ''){
	$config_http_referer = NM_URL;
}else{
	if(strpos($config_http_referer, 'ctlogin.php') || strpos($config_http_referer, 'ctjoin.php')){
		$cookie_http_referer = urldecode(get_session('ss_http_referer'));
	}else{
		set_session('ss_http_referer', urlencode($config_http_referer));
		$cookie_http_referer = $config_http_referer;
	}
}
if($cookie_http_referer == ''){ $cookie_http_referer = NM_URL; }

// cs_login_check() 으로 왔다면...171206
$ss_cs_login_check_http_referer = urldecode(get_session('ss_cs_login_check_http_referer'));
if($ss_cs_login_check_http_referer != ""){
	$cookie_http_referer = $ss_cs_login_check_http_referer;
	del_session('ss_cs_login_check_http_referer');
	$ss_cs_login_check_http_referer = "";
}

// 이전 페이지가 피너툰이 아니라면...
if(intval(strpos($cookie_http_referer, NM_DOMAIN_ONLY)) == 0){
	$cookie_http_referer = NM_URL;		
	set_session('ss_http_referer', urlencode($cookie_http_referer));
}

/* view */
include_once (NM_PATH.'/_head.php'); // 공통
// 181001 아무타스 리타겟팅 태그
gtm_amutus($nm_member); 

include_once($nm_config['nm_path']."/ctjoin.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>