<? if (!defined("_SH_DAY_FIVE_")) exit;


/* 무료화 데이터 안맞음 - 재정리 무료화 표기 속성 추가 */

for($_sh_auto_i=1; $_sh_auto_i<=3; $_sh_auto_i++){
	$_sh_auto_sql_ce = " select *, count(*)as ce_free_cnt
				from comics_episode_".$_sh_auto_i." 
				where ce_chapter>0 and ce_service_state='y' and ce_pay=0 group by ce_comics ";
	
	$_sh_auto_result_ce = sql_query($_sh_auto_sql_ce);
	for($_sh_auto_j=1; $_sh_auto_row_ce = sql_fetch_array($_sh_auto_result_ce); $_sh_auto_j++) {
		$_sh_auto_sql_cm = " select * from comics where cm_service='y' and cm_no='".$_sh_auto_row_ce['ce_comics']."' ";
		$_sh_auto_row_cm = sql_fetch($_sh_auto_sql_cm);
		
		$_sh_auto_save_free_cnt = 0; 
		if(intval($_sh_auto_row_cm['cm_no'])>0){ // 실제 서비스 하는 애들만
			if(intval($_sh_auto_row_cm['cm_episode_total'])>0){ // 에피소드 화 있는 작품만
				if(intval($_sh_auto_row_ce['ce_free_cnt']) > 0 || intval($_sh_auto_row_cm['cm_free']) > 0){

					// 설정 - 에피소드 무료화
					$_sh_auto_save_free_cnt = $_sh_auto_row_ce['ce_free_cnt'];

					// 설정 - 코믹스 무료 1부터 해당 수까지 무료제공
					if($_sh_auto_row_cm['cm_free'] > $_sh_auto_save_free_cnt){
						$_sh_auto_save_free_cnt = $_sh_auto_row_cm['cm_free'];
					}

					// 설정 - 코믹스 최고화 넘을 시 최고화 이하로...
					if($_sh_auto_save_free_cnt > $_sh_auto_row_cm['cm_episode_total']){
						$_sh_auto_save_free_cnt = $_sh_auto_row_cm['cm_episode_total'];
					}

					// echo "<br/>";
					// echo $_sh_auto_sql_cm.";";
					// echo " => ".$_sh_auto_row_ce['ce_free_cnt']." / ".$_sh_auto_row_cm['cm_free']." / ".$_sh_auto_row_cm['cm_series']." / ".$_sh_auto_row_cm['cm_episode_total']." /  =============>>".$_sh_auto_save_free_cnt;
					// echo "<br/><br/>";

					// update
					$_sh_auto_sql_update = " update comics set cm_free_cnt='".$_sh_auto_save_free_cnt."' where cm_service='y' and cm_no='".$_sh_auto_row_ce['ce_comics']."' ";
					sql_query($_sh_auto_sql_update);
				}
			}
		}
	}
}


$_sh_auto_sql_where = "
 WHERE 1 
 AND cm.cm_service = 'y' 
";

// 날짜
$_sh_auto__s_date = NM_TIME_M1;
$_sh_auto__e_date = NM_TIME_M1;
if($_sh_auto__s_date && $_sh_auto__e_date){ 
	$_sh_auto_sql_where.= date_year_month($_sh_auto__s_date, $_sh_auto__e_date, 'sl.sl'); 
}


$_sh_auto_sql_field_free = " 
 if(cm.cm_free_cnt >= 100, 3,
   if(cm.cm_free_cnt >= 10, 4,
    if(cm.cm_free_cnt >= 4, 2,
	 if(cm.cm_free_cnt >= 3, 1,0)
	)
   )
 )as cm_free_order, ";

/*
$_sh_auto_sql_field_free = " 
 if(cm.cm_free_cnt >=3,4,cm.cm_free_cnt) as cm_free_order, ";
*/

// 정렬
$_sh_auto__order_field_add = $_sh_auto__order_add = "";
if($_sh_auto__order_field == null || $_sh_auto__order_field == "" || $_sh_auto__order_field == "sl_won_sum"){ 
	$_sh_auto__order_field = "sl_won_sum"; 
	$_sh_auto__order_field_add = " , sl_comics "; 
}
if($_sh_auto__order == null || $_sh_auto__order == ""){ 
	$_sh_auto__order = "desc"; 
	$_sh_auto__order_add= "desc"; 
}

// 요일 정렬시
if($_sh_auto__order_field == "sl_week"){
	$_sh_auto__order_field_add = " , sl.sl_year_month desc "; 
	$_sh_auto__order_add = " , sl.sl_day desc ";
}

$_sh_auto_sql_order = "order by cm_free_order DESC, ".$_sh_auto__order_field." ".$_sh_auto__order.", sl_open_sum DESC ".$_sh_auto__order_field_add." ".$_sh_auto__order_add;

// 그룹
$_sh_auto_sql_group = " group by sl.sl_comics ";

$_sh_auto_sql_limit = " limit 0, 100 ";

// SQL문
$_sh_auto_sql_field = "		cm.cm_adult, cm.cm_big,
					sl.sl_comics, cm.cm_small, cm.cm_series, 

					{$_sh_auto_sql_field_free}

					sum(if(sl.sl_cash_point>0,sl_cash_point,0))as sl_cash_point_sum, 
					sum(if(sl.sl_cash_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_cash_point_cnt, 
					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won)as sl_cash_point_won_sum, 

					sum(if(sl.sl_point>0,sl_point,0))as sl_point_sum, 
					sum(if(sl.sl_cash_point=0 && sl.sl_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_point_cnt, 
					sum(floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_point_won_sum, 

					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_won_sum, 

					sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
					sum(sl.sl_open)as sl_open_sum 
"; 
$_sh_auto_sql_count	= $_sh_auto_sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$_sh_auto_sql_count	= $_sh_auto_sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$_sh_auto_sql_select = "		select {$_sh_auto_sql_field} ";
$_sh_auto_sql_table = "		FROM sales sl 
					LEFT JOIN comics cm ON sl.sl_comics = cm.cm_no 
					LEFT JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type ";

$_sh_auto_sql_query		= " {$_sh_auto_sql_select} {$_sh_auto_sql_table} {$_sh_auto_sql_where} {$_sh_auto_sql_group}  ";

$_sh_auto_sql_querys		= " {$_sh_auto_sql_query} {$_sh_auto_sql_order} {$_sh_auto_sql_limit} ";

$_sh_auto_sql_where_t	= $_sh_auto_sql_where." AND cm.cm_adult = 'n' "; 
$_sh_auto_sql_query_t	= " {$_sh_auto_sql_select} {$_sh_auto_sql_table} {$_sh_auto_sql_where_t} {$_sh_auto_sql_group}  ";
$_sh_auto_sql_querys_t	= " {$_sh_auto_sql_query_t} {$_sh_auto_sql_order} {$_sh_auto_sql_limit} ";

// echo $_sh_auto_sql_querys;
// echo "<br/><br/><br/><br/>";
// echo $_sh_auto_sql_querys_t;
// echo "<br/><br/><br/><br/>";

// 넣기전에 삭제

$_sh_auto_db_name = " comics_ranking_sales_cmfree_auto ";
$_sh_auto_sql_del = " TRUNCATE ".$_sh_auto_db_name." ";
sql_query($_sh_auto_sql_del);

$_sh_auto_result = sql_query($_sh_auto_sql_querys);
for($_sh_auto_i=1; $_sh_auto_row = sql_fetch_array($_sh_auto_result); $_sh_auto_i++) {
	$_sh_auto_sql_insert = " 
	 INSERT INTO ".$_sh_auto_db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
	 '0', '".$_sh_auto_row['cm_adult']."', '".$_sh_auto_row['sl_comics']."', '".$_sh_auto_row['cm_big']."', '".$_sh_auto_i."', '".NM_TIME_YMDHIS."' );	
	";
	sql_query($_sh_auto_sql_insert);
}

$_sh_auto_result_t = sql_query($_sh_auto_sql_querys_t);
for($_sh_auto_j=1; $_sh_auto_row_t = sql_fetch_array($_sh_auto_result_t); $_sh_auto_j++) {
	$_sh_auto_sql_insert = " 
	 INSERT INTO ".$_sh_auto_db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
	 '0_t', '".$_sh_auto_row_t['cm_adult']."', '".$_sh_auto_row_t['sl_comics']."', '".$_sh_auto_row_t['cm_big']."', '".$_sh_auto_j."', '".NM_TIME_YMDHIS."' );	
	";
	sql_query($_sh_auto_sql_insert);
}



unset($_sh_auto_sql_field, $_sh_auto_sql_count, $_sh_auto_sql_select, $_sh_auto_sql_table, $_sh_auto_sql_query, $_sh_auto_sql_querys, $_sh_auto_sql_where_t, $_sh_auto_sql_query_t, $_sh_auto_sql_querys_t);
unset($_sh_auto_sql_del, $_sh_auto_db_name);
unset($_sh_auto_sql_querys, $_sh_auto_result, $_sh_auto_row, $_sh_auto_i, $_sh_auto_sql_insert);
unset($_sh_auto_sql_querys_t, $_sh_auto_result_t, $_sh_auto_row_t, $_sh_auto_j, $_sh_auto_sql_insert);


?>