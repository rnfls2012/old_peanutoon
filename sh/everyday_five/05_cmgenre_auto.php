<? if (!defined("_SH_DAY_FIVE_")) exit;

$_sh_auto_sql_where = "
 WHERE 1 
 AND cm.cm_service = 'y' 
";

// 날짜
$_sh_auto__s_date = NM_TIME_M1;
$_sh_auto__e_date = NM_TIME_M1;
if($_sh_auto__s_date && $_sh_auto__e_date){ 
	$_sh_auto_sql_where.= date_year_month($_sh_auto__s_date, $_sh_auto__e_date, 'sl.sl'); 
}

/*
$_sh_auto_field_date_year_month = substr(NM_TIME_M1, 0, 7);	
$_sh_auto_field_date_year_day = substr(NM_TIME_M1, 8, 2);	 
$_sh_auto_sql_field_date = " 
 if(sl.sl_year_month='".$_sh_auto_field_date_year_month."', 
 if(sl.sl_day='".$_sh_auto_field_date_year_day."',1,0)
 ,0)as sl_date_order, ";
*/

// 정렬
$_sh_auto__order_field_add = $_sh_auto__order_add = "";
if($_sh_auto__order_field == null || $_sh_auto__order_field == "" || $_sh_auto__order_field == "sl_won_sum"){ 
	$_sh_auto__order_field = "sl_won_sum"; 
	$_sh_auto__order_field_add = " , sl_open_sum "; 
}
if($_sh_auto__order == null || $_sh_auto__order == ""){ 
	$_sh_auto__order = "desc"; 
	$_sh_auto__order_add= "desc"; 
}

// 요일 정렬시
if($_sh_auto__order_field == "sl_week"){
	$_sh_auto__order_field_add = " , sl.sl_year_month desc "; 
	$_sh_auto__order_add = " , sl.sl_day desc ";
}

$_sh_auto_sql_order = "order by ".$_sh_auto__order_field." ".$_sh_auto__order." ".$_sh_auto__order_field_add." ".$_sh_auto__order_add." , sl_comics ASC ";

// 그룹
$_sh_auto_sql_group = " group by cm.cm_no ";

$_sh_auto_sql_limit = "";

// SQL문
$_sh_auto_sql_field = "		cm.cm_adult, cm.cm_big, cm.cm_no,
					sl.sl_comics, cm.cm_small, cm.cm_series, cm.cm_service, 

					sum(if(sl.sl_cash_point>0,sl_cash_point,0))as sl_cash_point_sum, 
					sum(if(sl.sl_cash_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_cash_point_cnt, 
					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won)as sl_cash_point_won_sum, 

					sum(if(sl.sl_point>0,sl_point,0))as sl_point_sum, 
					sum(if(sl.sl_cash_point=0 && sl.sl_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_point_cnt, 
					sum(floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_point_won_sum, 

					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_won_sum, 

					sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
					sum(sl.sl_open)as sl_open_sum 
"; 
$_sh_auto_sql_count	= $_sh_auto_sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$_sh_auto_sql_count	= $_sh_auto_sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$_sh_auto_sql_select = "		select {$_sh_auto_sql_field} ";
$_sh_auto_sql_table = "		FROM comics cm 
					LEFT JOIN sales sl ON sl.sl_comics = cm.cm_no 
					LEFT JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type ";
// 0 전체
$_sh_auto_sql_query[0]	= " {$_sh_auto_sql_select} {$_sh_auto_sql_table} {$_sh_auto_sql_where} {$_sh_auto_sql_group}  ";
$_sh_auto_sql_querys[0]	= " ".$_sh_auto_sql_query[0]." {$_sh_auto_sql_order} {$_sh_auto_sql_limit} ";

// 청소년
$_sh_auto_sql_where_t	= $_sh_auto_sql_where." AND cm.cm_adult = 'n' "; 
$_sh_auto_sql_query_t[0]	= " {$_sh_auto_sql_select} {$_sh_auto_sql_table} {$_sh_auto_sql_where_t} {$_sh_auto_sql_group}  ";
$_sh_auto_sql_querys_t[0]= " ".$_sh_auto_sql_query_t[0]." {$_sh_auto_sql_order} {$_sh_auto_sql_limit} ";

// 장르별
for($_sh_auto_s=1; $_sh_auto_s<=8; $_sh_auto_s++){
// 전체
	$_sh_auto_{'sql_where_small_'.$_sh_auto_s} = $_sh_auto_sql_where." AND cm.cm_small = '".$_sh_auto_s."' "; 
	$_sh_auto_sql_query[$_sh_auto_s]	= " {$_sh_auto_sql_select} {$_sh_auto_sql_table} ".$_sh_auto_{'sql_where_small_'.$_sh_auto_s}." {$_sh_auto_sql_group}  ";
	$_sh_auto_sql_querys[$_sh_auto_s]	= " ".$_sh_auto_sql_query[$_sh_auto_s]." {$_sh_auto_sql_order} {$_sh_auto_sql_limit} ";

	// 청소년만
	$_sh_auto_{'sql_where_small_t_'.$_sh_auto_s} = $_sh_auto_sql_where." AND cm.cm_small = '".$_sh_auto_s."' AND cm.cm_adult = 'n' "; 
	$_sh_auto_sql_query_t[$_sh_auto_s]	= " {$_sh_auto_sql_select} {$_sh_auto_sql_table} ".$_sh_auto_{'sql_where_small_t_'.$_sh_auto_s}." {$_sh_auto_sql_group}  ";
	$_sh_auto_sql_querys_t[$_sh_auto_s]	= " ".$_sh_auto_sql_query_t[$_sh_auto_s]." {$_sh_auto_sql_order} {$_sh_auto_sql_limit} ";

	// if($_sh_auto_s==4){
	
	// echo $_sh_auto_sql_querys[$_sh_auto_s].";<br/><br/>";
	// echo $_sh_auto_sql_querys_t[$_sh_auto_s].";<br/>";
	// echo "<br/><br/><br/><br/>";
	// }
}


	// echo $_sh_auto_sql_querys[0].";<br/><br/>";
	// echo $_sh_auto_sql_querys_t[0].";<br/>";
	// echo "<br/><br/><br/><br/>";

// die;

// 넣기전에 삭제

$_sh_auto_db_name = " comics_ranking_sales_cmgenre_auto ";
$_sh_auto_sql_del = " TRUNCATE ".$_sh_auto_db_name." ";
sql_query($_sh_auto_sql_del);

for($_sh_auto_t=0; $_sh_auto_t<=8; $_sh_auto_t++){ 
	// 확인
	$_sh_auto_cmgenre_arr[$_sh_auto_t] = array(); 
	$_sh_auto_cmgenre_t_arr[$_sh_auto_t] = array(); 
}

for($_sh_auto_s=0; $_sh_auto_s<=8; $_sh_auto_s++){
	$_sh_auto_result = sql_query($_sh_auto_sql_querys[$_sh_auto_s]);
	for($_sh_auto_i=1; $_sh_auto_row = sql_fetch_array($_sh_auto_result); $_sh_auto_i++) {
		$_sh_auto_sql_insert = " 
		 INSERT INTO ".$_sh_auto_db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
		 '".$_sh_auto_s."', '".$_sh_auto_row['cm_adult']."', '".$_sh_auto_row['cm_no']."', '".$_sh_auto_row['cm_big']."', '".$_sh_auto_i."', '".NM_TIME_YMDHIS."' );	
		";
		sql_query($_sh_auto_sql_insert);
		array_push($_sh_auto_cmgenre_arr[$_sh_auto_s],$_sh_auto_row['cm_no']);
	}

	$_sh_auto_result_t = sql_query($_sh_auto_sql_querys_t[$_sh_auto_s]);
	for($_sh_auto_j=1; $_sh_auto_row_t = sql_fetch_array($_sh_auto_result_t); $_sh_auto_j++) {
		$_sh_auto_sql_insert = " 
		 INSERT INTO ".$_sh_auto_db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
		 '".$_sh_auto_s."_t', '".$_sh_auto_row_t['cm_adult']."', '".$_sh_auto_row_t['cm_no']."', '".$_sh_auto_row_t['cm_big']."', '".$_sh_auto_j."', '".NM_TIME_YMDHIS."' );	
		";
		sql_query($_sh_auto_sql_insert);
		array_push($_sh_auto_cmgenre_t_arr[$_sh_auto_s], $_sh_auto_row_t['cm_no']);
	}
}

// 판대 안된 작품 추가
for($_sh_auto_s=0; $_sh_auto_s<=8; $_sh_auto_s++){
	$_sh_auto_sql_small_where = $_sh_auto_sql_small_where_arr = "";
	
	$_sh_auto_sql_small_where = " AND cm_service = 'y' ";
	if($_sh_auto_s > 0){ $_sh_auto_sql_small_where.= " AND cm_small = '".$_sh_auto_s."' "; }
	$_sh_auto_sql_small_where_arr = " AND cm_no not in (".implode(", ",$_sh_auto_cmgenre_arr[$_sh_auto_s]).") ";
	$_sh_auto_sql_small = " select * FROM comics WHERE 1 {$_sh_auto_sql_small_where} {$_sh_auto_sql_small_where_arr} order by cm_no DESC ";
	$_sh_auto_sql_small0 = " select * FROM comics WHERE 1 {$_sh_auto_sql_small_where} order by cm_no DESC ";

	$_sh_auto_sql_small_where_t = $_sh_auto_sql_small_where_arr_t = "";
	$_sh_auto_sql_small_where_t = " AND cm_service = 'y' AND cm_adult='n' ";
	if($_sh_auto_s > 0){ $_sh_auto_sql_small_where_t.= " AND cm_small = '".$_sh_auto_s."' "; }
	$_sh_auto_sql_small_where_arr_t = " AND cm_no not in (".implode(", ",$_sh_auto_cmgenre_t_arr[$_sh_auto_s]).") ";
	$_sh_auto_sql_small_t = " select * FROM comics WHERE 1 {$_sh_auto_sql_small_where_t} {$_sh_auto_sql_small_where_arr} order by cm_no DESC ";
	$_sh_auto_sql_small_t0 = " select * FROM comics WHERE 1 {$_sh_auto_sql_small_where_t} order by cm_no DESC ";

	$_sh_auto_cmgenre_arr_count = count($_sh_auto_cmgenre_arr[$_sh_auto_s]);
	if($_sh_auto_cmgenre_arr_count > 0){		
		$_sh_auto_result = sql_query($_sh_auto_sql_small);
		for($_sh_auto_i=$_sh_auto_cmgenre_arr_count+1; $_sh_auto_row = sql_fetch_array($_sh_auto_result); $_sh_auto_i++) {
			$_sh_auto_sql_insert = " 
			 INSERT INTO ".$_sh_auto_db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
			 '".$_sh_auto_s."', '".$_sh_auto_row['cm_adult']."', '".$_sh_auto_row['cm_no']."', '".$_sh_auto_row['cm_big']."', '".$_sh_auto_i."', '".NM_TIME_YMDHIS."' );	
			";
			sql_query($_sh_auto_sql_insert);
		}
	}else if($_sh_auto_cmgenre_arr_count == 0){
		$_sh_auto_result = sql_query($_sh_auto_sql_small0);
		for($_sh_auto_i=1; $_sh_auto_row = sql_fetch_array($_sh_auto_result); $_sh_auto_i++) {
			$_sh_auto_sql_insert = " 
			 INSERT INTO ".$_sh_auto_db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
			 '".$_sh_auto_s."', '".$_sh_auto_row['cm_adult']."', '".$_sh_auto_row['cm_no']."', '".$_sh_auto_row['cm_big']."', '".$_sh_auto_i."', '".NM_TIME_YMDHIS."' );	
			";
			sql_query($_sh_auto_sql_insert);

		}
	}

	$_sh_auto_cmgenre_t_arr_count = count($_sh_auto_cmgenre_t_arr[$_sh_auto_s]);
	if($_sh_auto_cmgenre_t_arr_count > 0){
		$_sh_auto_result_t = sql_query($_sh_auto_sql_small_t);
		for($_sh_auto_j=$_sh_auto_cmgenre_t_arr_count+1; $_sh_auto_row_t = sql_fetch_array($_sh_auto_result_t); $_sh_auto_j++) {
			$_sh_auto_sql_insert = " 
			 INSERT INTO ".$_sh_auto_db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
			 '".$_sh_auto_s."_t', '".$_sh_auto_row_t['cm_adult']."', '".$_sh_auto_row_t['cm_no']."', '".$_sh_auto_row_t['cm_big']."', '".$_sh_auto_j."', '".NM_TIME_YMDHIS."' );	
			";
			sql_query($_sh_auto_sql_insert);
		}
	}else if($_sh_auto_cmgenre_t_arr_count == 0){
		$_sh_auto_result_t = sql_query($_sh_auto_sql_small_t0);
		for($_sh_auto_j=1; $_sh_auto_row_t = sql_fetch_array($_sh_auto_result_t); $_sh_auto_j++) {
			$_sh_auto_sql_insert = " 
			 INSERT INTO ".$_sh_auto_db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
			 '".$_sh_auto_s."_t', '".$_sh_auto_row_t['cm_adult']."', '".$_sh_auto_row_t['cm_no']."', '".$_sh_auto_row_t['cm_big']."', '".$_sh_auto_j."', '".NM_TIME_YMDHIS."' );	
			";
			sql_query($_sh_auto_sql_insert);

		}
	}	
}



unset($_sh_auto_sql_field, $_sh_auto_sql_count, $_sh_auto_sql_select, $_sh_auto_sql_table, $_sh_auto_sql_query, $_sh_auto_sql_querys, $_sh_auto_sql_where_t, $_sh_auto_sql_query_t, $_sh_auto_sql_querys_t);
unset($_sh_auto_sql_del, $_sh_auto_db_name);
unset($_sh_auto_sql_querys, $_sh_auto_result, $_sh_auto_row, $_sh_auto_i, $_sh_auto_sql_insert);
unset($_sh_auto_sql_querys_t, $_sh_auto_result_t, $_sh_auto_row_t, $_sh_auto_j, $_sh_auto_sql_insert);
unset($_sh_auto_cmgenre_arr, $_sh_auto_s);


?>