<? if (!defined("_SH_DAY_FIVE_")) exit;

/*
SELECT c.*, c_prof.cp_name as prof_name FROM comics c left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
WHERE 1 AND c.cm_service = 'y' AND cm_reg_date >= '2018-04-20 00:00:00' AND cm_reg_date <= '2018-05-11 23:59:59' 
ORDER BY cm_reg_date DESC LIMIT 0, 720;

$_sh_auto_sql_cmnew = " SELECT c.*, 
					".$_sh_auto_sql_week_comicno_field."
					c_prof.cp_name as prof_name 
				FROM comics c 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 $_sh_auto_sql_cm_adult  
				AND c.cm_service = 'y'  
				AND cm_reg_date >= '".NM_TIME_M21." ".NM_TIME_HI_start."'
				AND cm_reg_date <= '".NM_TIME_YMD." ".NM_TIME_HI_end."'
				ORDER BY ".$_sh_auto_sql_week_comicno_order." 
				cm_reg_date DESC 
*/


/*
$_sh_auto_sql_where = "
 WHERE 1 
 AND cm.cm_service = 'y' 
 AND cm.cm_reg_date >= '".NM_TIME_MON_M1." ".NM_TIME_HI_start."'
 AND cm.cm_reg_date <= '".NM_TIME_YMD." ".NM_TIME_HI_end."' 
";
*/
$_sh_auto_sql_where = "
 WHERE 1 
 AND cm.cm_service = 'y' 
";

// 날짜
/*
$_sh_auto__s_date = NM_TIME_M2;
$_sh_auto__e_date = NM_TIME_M1;
if($_sh_auto__s_date && $_sh_auto__e_date){ 
	$_sh_auto_sql_where.= date_year_month($_sh_auto__s_date, $_sh_auto__e_date, 'sl.sl'); 
}
*/


$_sh_auto_field_date_year_month = substr(NM_TIME_M1, 0, 7);	
$_sh_auto_field_date_year_day = substr(NM_TIME_M1, 8, 2);	 
$_sh_auto_sql_field_date = " 
 if(sl.sl_year_month='".$_sh_auto_field_date_year_month."', 
 if(sl.sl_day='".$_sh_auto_field_date_year_day."',1,0)
 ,0)as sl_date_order, ";

$_sh_auto_sql_field_date_reg = " 
 if(cm.cm_reg_date >= '".NM_TIME_M21." ".NM_TIME_HI_start."', 
  if(cm.cm_reg_date <= '".NM_TIME_YMD." ".NM_TIME_HI_end."',1,0)
 ,0)as sl_date_order_reg, ";

// 3 6 9 12개월
$_sh_auto_sql_field_date_reg2 = " 
 if(cm.cm_reg_date < '".NM_TIME_MON_M12_S." ".NM_TIME_HI_start."', 1,
   if(cm.cm_reg_date < '".NM_TIME_MON_M9_S." ".NM_TIME_HI_start."', 2,
    if(cm.cm_reg_date < '".NM_TIME_MON_M6_S." ".NM_TIME_HI_start."', 3,
	 if(cm.cm_reg_date < '".NM_TIME_MON_M3_S." ".NM_TIME_HI_start."', 4,0)
    )
   )
 )as sl_date_order_reg2, ";

/*
$_sh_auto_sql_field_date_concat = "
 concat(sl.sl_year_month,sl.sl_day) as sl_date_concat, ";
*/

// 정렬
$_sh_auto__order_field_add = $_sh_auto__order_add = "";
if($_sh_auto__order_field == null || $_sh_auto__order_field == "" || $_sh_auto__order_field == "sl_won_sum"){ 
	$_sh_auto__order_field = "sl_won_sum"; 
	$_sh_auto__order_field_add = " , sl_comics "; 
}
if($_sh_auto__order == null || $_sh_auto__order == ""){ 
	$_sh_auto__order = "desc"; 
	$_sh_auto__order_add= "desc"; 
}

// 요일 정렬시
if($_sh_auto__order_field == "sl_week"){
	$_sh_auto__order_field_add = " , sl.sl_year_month desc "; 
	$_sh_auto__order_add = " , sl.sl_day desc ";
}

$_sh_auto_sql_order = "order by sl_date_order_reg desc, sl_date_order_reg2 desc, sl_date_order desc, ".$_sh_auto__order_field." ".$_sh_auto__order." ".$_sh_auto__order_field_add." ".$_sh_auto__order_add;

// 그룹
$_sh_auto_sql_group = " group by sl.sl_comics ";

$_sh_auto_sql_limit = " limit 0, 100 ";

// SQL문
$_sh_auto_sql_field = "		cm.cm_adult, cm.cm_big,
					sl.sl_comics, cm.cm_small, cm.cm_series, 

					{$_sh_auto_sql_field_date}
					{$_sh_auto_sql_field_date_reg}
					{$_sh_auto_sql_field_date_reg2}

					sum(if(sl.sl_cash_point>0,sl_cash_point,0))as sl_cash_point_sum, 
					sum(if(sl.sl_cash_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_cash_point_cnt, 
					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won)as sl_cash_point_won_sum, 

					sum(if(sl.sl_point>0,sl_point,0))as sl_point_sum, 
					sum(if(sl.sl_cash_point=0 && sl.sl_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_point_cnt, 
					sum(floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_point_won_sum, 

					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_won_sum, 

					sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
					sum(sl.sl_open)as sl_open_sum 
"; 
$_sh_auto_sql_count	= $_sh_auto_sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$_sh_auto_sql_count	= $_sh_auto_sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$_sh_auto_sql_select = "		select {$_sh_auto_sql_field} ";
$_sh_auto_sql_table = "		FROM sales sl 
					LEFT JOIN comics cm ON sl.sl_comics = cm.cm_no 
					LEFT JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type ";

$_sh_auto_sql_query		= " {$_sh_auto_sql_select} {$_sh_auto_sql_table} {$_sh_auto_sql_where} {$_sh_auto_sql_group}  ";

$_sh_auto_sql_querys		= " {$_sh_auto_sql_query} {$_sh_auto_sql_order} {$_sh_auto_sql_limit} ";

$_sh_auto_sql_where_t	= $_sh_auto_sql_where." AND cm.cm_adult = 'n' "; 
$_sh_auto_sql_query_t	= " {$_sh_auto_sql_select} {$_sh_auto_sql_table} {$_sh_auto_sql_where_t} {$_sh_auto_sql_group}  ";
$_sh_auto_sql_querys_t	= " {$_sh_auto_sql_query_t} {$_sh_auto_sql_order} {$_sh_auto_sql_limit} ";

// echo $_sh_auto_sql_querys;
// echo "<br/><br/><br/><br/>";
// echo $_sh_auto_sql_querys_t;
// echo "<br/><br/><br/><br/>";

// 넣기전에 삭제

$_sh_auto_db_name = " comics_ranking_sales_cmnew_auto ";
$_sh_auto_sql_del = " TRUNCATE ".$_sh_auto_db_name." ";
sql_query($_sh_auto_sql_del);

$_sh_auto_result = sql_query($_sh_auto_sql_querys);
for($_sh_auto_i=1; $_sh_auto_row = sql_fetch_array($_sh_auto_result); $_sh_auto_i++) {
	$_sh_auto_sql_insert = " 
	 INSERT INTO ".$_sh_auto_db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
	 '0', '".$_sh_auto_row['cm_adult']."', '".$_sh_auto_row['sl_comics']."', '".$_sh_auto_row['cm_big']."', '".$_sh_auto_i."', '".NM_TIME_YMDHIS."' );	
	";
	sql_query($_sh_auto_sql_insert);
}

$_sh_auto_result_t = sql_query($_sh_auto_sql_querys_t);
for($_sh_auto_j=1; $_sh_auto_row_t = sql_fetch_array($_sh_auto_result_t); $_sh_auto_j++) {
	$_sh_auto_sql_insert = " 
	 INSERT INTO ".$_sh_auto_db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
	 '0_t', '".$_sh_auto_row_t['cm_adult']."', '".$_sh_auto_row_t['sl_comics']."', '".$_sh_auto_row_t['cm_big']."', '".$_sh_auto_j."', '".NM_TIME_YMDHIS."' );	
	";
	sql_query($_sh_auto_sql_insert);
}





unset($_sh_auto_sql_field, $_sh_auto_sql_count, $_sh_auto_sql_select, $_sh_auto_sql_table, $_sh_auto_sql_query, $_sh_auto_sql_querys, $_sh_auto_sql_where_t, $_sh_auto_sql_query_t, $_sh_auto_sql_querys_t);
unset($_sh_auto_sql_del, $_sh_auto_db_name);
unset($_sh_auto_sql_querys, $_sh_auto_result, $_sh_auto_row, $_sh_auto_i, $_sh_auto_sql_insert);
unset($_sh_auto_sql_querys_t, $_sh_auto_result_t, $_sh_auto_row_t, $_sh_auto_j, $_sh_auto_sql_insert);

?>