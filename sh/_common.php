<?
	// 설정
	$nm_path['path'] = '/var/www/html/peanutoon';
	$path_config 	= $nm_path['path'].'/config';
	$path_lib 		= $nm_path['path'].'/lib';
	
	// config 폴더 파일 연결
	$list_config = scandir($path_config);
	$lib_except = array('kt_connect.php');	// 연결 제외
	foreach($list_config as $config_key => $config_name){
		if($config_name == ".." || $config_name == "."){ continue; }
		if(in_array($config_name, $lib_except)){ continue; }
		
		$path_lib_file = $path_config.'/'.$config_name;
		if(file_exists($path_lib_file)){ 
			include_once($path_lib_file);
		}		
	} 
	
	// DB 설정 가져오기
	$nm_config_sql 		= "SELECT * FROM  `config` c LEFT JOIN `config_img` ci ON c.cf_no = ci.cf_no";
	$nm_config_result = mysql_query($nm_config_sql);
	$nm_config 				= mysql_fetch_assoc($nm_config_result);
	
	// lib 폴더 파일 연결
	$list_lib = scandir($path_lib);
	$lib_first = $lib_except = array();	
	
	// 먼저 연결
	array_push($lib_first, 'common.lib.php', 'mysql.lib.php', 'mb.lib.php', 'default.lib.php');	
	
	// 연결 제외
	array_push($lib_except, 'PHPExcel.php', 'PHPMailer.php');
	array_push($lib_except, 'cloudfiles_exceptions.php', 'cloudfiles_http-kt.php', 'cloudfiles-kt.php');	
	
	// 먼저 연결
	foreach($lib_first as $lib_first_key => $lib_first_name){
		$path_lib_file = $path_lib.'/'.$lib_first_name;
		if(file_exists($path_lib_file)){ 
			include_once($path_lib_file);
		}			
	}
	
	// 나머지 연결
	foreach($list_lib as $lib_key => $lib_name){
		if($lib_name == ".." || $lib_name == "."){ continue; }
		if(in_array($lib_name, $lib_except)){ continue; }
		
		$path_lib_file = $path_lib.'/'.$lib_name;
		if(is_dir($path_lib_file)){ continue; }
		
		if(file_exists($path_lib_file)){ 
			include_once($path_lib_file);
		}		
	} 	
?>