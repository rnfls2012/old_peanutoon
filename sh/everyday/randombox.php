<?php if (!defined("_SH_DAY_")) exit;

/* 랜덤박스 이벤트 내 쿠폰 유효기간 중지 */
$er_av_select_sql = "SELECT * FROM event_randombox WHERE er_available_state='y' AND er_available_date_end < '".NM_TIME_YMD."'";
$er_av_select_result = sql_query($er_av_select_sql);
$er_av_select_size = sql_num_rows($er_av_select_result);

// 하나 이상 나올때 실행
if($er_av_select_size > 0) {
	$er_av_select_array = array();

	while($er_av_select_row = sql_fetch_array($er_av_select_result)) {
		array_push($er_av_select_array, $er_av_select_row['er_no']);
	} // end while
	
	$er_av_no = implode(", ", $er_av_select_array);
	$er_av_update_sql = "UPDATE event_randombox SET er_available_state='n' WHERE er_no IN(".$er_av_no.")";

	if(sql_query($er_av_update_sql)){ // 유효기간 상태 업데이트

	}else{
		// 업데이트 실패시 로그
		cs_error_log($sh_path."/log/".NM_TIME_YMD.".log", $er_av_update_sql, $_SERVER['PHP_SELF']);
	} // end else
} // end if


/* 랜덤박스 할인 쿠폰 중지*/
$er_select_array = array();
$er_select_sql = "SELECT er_no FROM event_randombox WHERE er_available_date_end < '".NM_TIME_YMD."'";
$er_select_result = sql_query($er_select_sql);

while($er_row = sql_fetch_array($er_select_result)) {
	array_push($er_select_array, $er_row['er_no']);
} // end while

if(count($er_select_array) > 0) {
	$er_no = implode(", ", $er_select_array);

	$erc_select_sql = "SELECT erc_no FROM event_randombox_coupon WHERE erc_state='y' AND erc_er_no IN(".$er_no.")";
	$erc_select_result = sql_query($erc_select_sql);
	$erc_select_size = sql_num_rows($erc_select_result);

	// 하나 이상 나올때 실행
	if($erc_select_size > 0) {
		$erc_select_array = array();

		while($erc_select_row = sql_fetch_array($erc_select_result)) {
			array_push($erc_select_array, $erc_select_row['erc_no']);
		} // end while
		
		$erc_no = implode(", ", $erc_select_array);
		$erc_update_sql = "UPDATE event_randombox_coupon SET erc_state='n' WHERE erc_no IN(".$erc_no.")";

		if(sql_query($erc_update_sql)){ // 쿠폰 업데이트

		}else{
			// 업데이트 실패시 로그
			cs_error_log($sh_path."/log/".NM_TIME_YMD.".log", $erc_update_sql, $_SERVER['PHP_SELF']);
		} // end else
	} // end if
} // end if
?>