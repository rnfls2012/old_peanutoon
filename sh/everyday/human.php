<?php if (!defined("_SH_DAY_")) exit;
	$human_date = NM_TIME_YMDHIS; // 오늘 날짜를 저장
	$one_week_ago = date("Y-m-d", strtotime("-7 day")); // 오늘 날짜로부터 1주일 전 날짜 저장
	$stand_by_human_cnt_sql = "SELECT COUNT(*) AS cnt FROM member WHERE mb_state_sub='t' AND mb_human_email_date<='".$one_week_ago." 23:59:59'"; // 휴면 메일 받은지 7주일 된 회원 검색
	$stand_by_human_cnt = sql_count($stand_by_human_cnt_sql, 'cnt');
	
	// 결과가 있을시에 실행
	if($stand_by_human_cnt > 0) {
		$stand_by_human_sql = "SELECT * FROM member WHERE mb_state_sub='t' AND mb_human_email_date<='".$one_week_ago." 23:59:59'";
		$stand_by_human_result = sql_query($stand_by_human_sql);

		while($stand_by_human_row = sql_fetch_array($stand_by_human_result)) {
			$human_update_sql = "UPDATE member SET mb_state_sub='h', mb_human_date='".$human_date."' WHERE mb_no='".$stand_by_human_row['mb_no']."'";
			if(sql_query($human_update_sql)) {
				// 휴면 전환 로그 남기기
				$human_update_log_sql = "INSERT INTO z_human_member(z_hm_member, z_hm_id, z_hm_idx, z_hm_human_email_date, z_hm_human_date) VALUES('".$stand_by_human_row['mb_no']."', '".$stand_by_human_row['mb_id']."', '".$stand_by_human_row['mb_idx']."', '".$stand_by_human_row['mb_human_email_date']."', '".$human_date."')";
				sql_query($human_update_log_sql);
			} // end if
		} // end while
	} // end if
?>