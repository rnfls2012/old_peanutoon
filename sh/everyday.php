<? define('_SH_DAY_', true);

$sh_path = "/var/www/html/".NM_DOMAIN_DIR."/sh";

if($ready_path != ''){ // 강제실행시...
	$sh_path = $ready_path; 
}

// 이벤트-크리스마스
include_once $sh_path.'/everyday/christmas.php';

// 이벤트-충전/지급
include_once $sh_path.'/everyday/recharge.php';

// 이벤트-무료&코인할인 등록 -> 18-01-12 이동 /sh/everyhour.php
// include_once $sh_path.'/everyday/free_dc.php';

// 이벤트-쿠폰 사용 중지
include_once $sh_path.'/everyday/coupon.php';

// 프로모션-배너 -> 18-02-14 이동 /sh/everyhour.php 이동
// include_once $sh_path.'/everyday/banner.php';

// 프로모션-팝업 -> 18-01-22 이동 /sh/everyhour.php
// include_once $sh_path.'/everyday/popup.php';

// 프로모션-이벤트 -> 18-02-14 epromotion_event[주석처리부분] 이동 /sh/everyhour.php
include_once $sh_path.'/everyday/event.php';

// 휴면 대기회원 휴면처리
include_once $sh_path.'/everyday/human.php';

// 이벤트-랜덤박스
include_once $sh_path.'/everyday/randombox.php';

// //////////////////////////////// 실행시간 저장 ////////////////////////////////
cs_auto_time($sh_path, 'cf_sh_everyday_time');
?>