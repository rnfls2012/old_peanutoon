<? define('_SH_HOUR_', true);

$sh_path = "/var/www/html/".NM_DOMAIN_DIR."/sh";

if($ready_path != ''){ // 강제실행시...
	$sh_path = $ready_path; 
}

// 에피소드 서비스 처리, 기다리다무료 처리
include_once $sh_path.'/everyhour/episode.php';
include_once $sh_path.'/everyhour/apppush.php';
include_once $sh_path.'/everyhour/stop_service.php';
include_once $sh_path.'/everyhour/open_service.php';

// 이벤트-무료&코인할인 등록
include_once $sh_path.'/everyhour/free_dc.php';

// 프로모션-팝업
include_once $sh_path.'/everyhour/popup.php';


// 프로모션-배너
include_once $sh_path.'/everyhour/banner.php';

// 프로모션-이벤트
include_once $sh_path.'/everyhour/epromotion_event.php';

// 이벤트-랜덤박스
include_once $sh_path.'/everyhour/randombox.php';

// //////////////////////////////// 실행시간 저장 ////////////////////////////////
cs_auto_time($sh_path, 'cf_sh_everyhour_time');
?>