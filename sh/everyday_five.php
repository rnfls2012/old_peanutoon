<? define('_SH_DAY_FIVE_', true);

$sh_path = "/var/www/html/".NM_DOMAIN_DIR."/sh";
$sh_var = true; // sh용 변수 (다른 곳에서 include 시켰을 시에 함수 자동 실행 방지)

if($ready_path != ''){ // 강제실행시...
	$sh_path = $ready_path; 
}

// 회원-휴면처리
include_once $sh_path.'/everyday_five/02_cmnew_auto.php';			// 신작 코믹스 정렬 순서(매출순)
include_once $sh_path.'/everyday_five/03_cmtop_auto.php';			// 베스트 코믹스 정렬 순서(매출순)
include_once $sh_path.'/everyday_five/04_cmfree_auto.php';			// 무료 코믹스 정렬 순서(매출순)
include_once $sh_path.'/everyday_five/05_cmgenre_auto.php';			// 장르 코믹스 정렬 순서(매출순)
include_once $sh_path.'/everyday_five/06_main_slide_moscroll.php';	// 모바일 메인 업데이트작품 코믹스 정렬 순서(매출순)


// //////////////////////////////// 실행시간 저장 ////////////////////////////////
cs_auto_time($sh_path, 'cf_sh_everyhour_five_time');
?>