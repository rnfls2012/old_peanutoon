<?php if (!defined("_SH_DAY_SIX_")) exit;
/* 날짜 데이터 저장 */
$_1day_ago  = date("Y-m-d", strtotime(NM_TIME_YMDHIS."-1 day")); // 하루 전 날짜 YMD 형태
$_today     = NM_TIME_YMD;										 // 오늘 날짜 YMD 형태
$_1day_next = date("Y-m-d", strtotime(NM_TIME_YMDHIS."+1 day")); // 내일 날짜 YMD 형태

/* 데이터 저장 */
if($sh_var) {
	z_apk_stats($_1day_ago, $_today, "n"); // 어제자 통계(고정값) 저장
	z_apk_stats($_today, $_1day_next, "y"); // 오늘자 통계(변하는 값) 저장
} // end if 다른 곳에서 include 시켰을 시, 함수 자동실행 방지

/*
// 임시 2017-07-31 12:11 에 실행
$temp_date = '2017-07-01';

while($temp_date <= NM_TIME_YMD) {
	$temp_next_date = date("Y-m-d", strtotime($temp_date."+1 day"));
	
	if($temp_date == NM_TIME_YMD) {
		z_apk_stats($temp_date, $temp_next_date, "y");
	} else {
		z_apk_stats($temp_date, $temp_next_date, "n");
	} // end else

	$temp_date = date("Y-m-d", strtotime($temp_date."+1 day"));
} // end while
*/

/* 데이터 저장 함수 날짜 PARAMETER는 YMD 형태로 넣어야 함 */
function z_apk_stats($target_date, $next_date, $real_time) {
	// APK 다운로드 수
	$apk_down_sql = "SELECT COUNT(*) AS apk_down_count FROM stats_log_app WHERE (sla_date >= '".$target_date."' AND sla_date < '".$next_date."') GROUP BY SUBSTR(sla_date, 1, 10)";
	$apk_down_count = sql_count($apk_down_sql, 'apk_down_count');
	
	// APK 사용자 중 당일 결제 회원 수
	$recharge_member_count_sql = "SELECT COUNT(DISTINCT(mpu_member)) AS recharge_member_count FROM member_point_used WHERE (mpu_date >= '".$target_date."' AND mpu_date < '".$next_date."') AND mpu_class = 'r' AND mpu_recharge_won != '0' AND mpu_user_agent LIKE '%appsetapk%' GROUP BY SUBSTR(mpu_date, 1, 10)";
	$recharge_member_count = sql_count($recharge_member_count_sql, 'recharge_member_count');
	
	// APK 사용자 중 당일 결제 건수
	$recharge_count_sql = "SELECT COUNT(*) AS recharge_count FROM member_point_used WHERE (mpu_date >= '".$target_date."' AND mpu_date < '".$next_date."') AND mpu_class = 'r' AND mpu_recharge_won != '0' AND mpu_user_agent LIKE '%appsetapk%' GROUP BY SUBSTR(mpu_date, 1, 10)";
	$recharge_count = sql_count($recharge_count_sql, 'recharge_count');

	// APK 사용자 중 당일 결제 총액
	$recharge_total_sql = "SELECT SUM(mpu_recharge_won) AS recharge_total FROM member_point_used WHERE (mpu_date >= '".$target_date."' AND mpu_date < '".$next_date."') AND mpu_class = 'r' AND mpu_recharge_won != '0' AND mpu_user_agent LIKE '%appsetapk%' GROUP BY SUBSTR(mpu_date, 1, 10)";
	$recharge_total = sql_count($recharge_total_sql, 'recharge_total');

	/* 실시간 검색 여부에 따라 데이터 저장 */
	// QUERY (INSERT, DELETE);
	$insert_sql = "INSERT INTO z_apk_stats(z_as_down_count, z_as_recharge_member_count, z_as_recharge_count, z_as_recharge_total, z_as_real_time_date, z_as_real_time_state, z_as_date) VALUES('".$apk_down_count."', '".$recharge_member_count."', '".$recharge_count."', '".$recharge_total."', '".NM_TIME_YMDHIS."', '".$real_time."', '".$target_date."')";
	
	$delete_sql = "DELETE FROM z_apk_stats WHERE z_as_date = '".$target_date."' AND z_as_real_time_state = 'y'";

	// 날짜로 저장된 데이터가 있는지 먼저 조회
	$real_time_select_sql = "SELECT COUNT(*) AS real_time_cnt FROM z_apk_stats WHERE z_as_date = '".$target_date."' and z_as_real_time_state = 'y'";
	$real_time_count = sql_count($real_time_select_sql, 'real_time_cnt');
	
	if($real_time_count != 0) {
		sql_query($delete_sql); // 있을 경우 삭제
	} // end if

	sql_query($insert_sql);
} // z_member_stats
?>