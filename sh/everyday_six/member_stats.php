<?php if (!defined("_SH_DAY_SIX_")) exit;
/* 날짜 데이터 저장 */
$_1day_ago  = date("Y-m-d", strtotime(NM_TIME_YMDHIS."-1 day")); // 하루 전 날짜 YMD 형태
$_today     = NM_TIME_YMD;										 // 오늘 날짜 YMD 형태
$_1day_next = date("Y-m-d", strtotime(NM_TIME_YMDHIS."+1 day")); // 내일 날짜 YMD 형태

/* 데이터 저장 */
if($sh_var) {
	z_member_stats($_1day_ago, $_today, "n"); // 어제자 통계(고정값) 저장
	z_member_stats($_today, $_1day_next, "y"); // 오늘자 통계(변하는 값) 저장
} // end if 다른 곳에서 include 시켰을 시, 함수 자동실행 방지

/*
// 임시 2017-07-31 12:11 에 실행
$temp_date = '2017-07-01';

while($temp_date <= NM_TIME_YMD) {
	$temp_next_date = date("Y-m-d", strtotime($temp_date."+1 day"));
	
	if($temp_date == NM_TIME_YMD) {
		z_member_stats($temp_date, $temp_next_date, "y");
	} else {
		z_member_stats($temp_date, $temp_next_date, "n");
	} // end else

	$temp_date = date("Y-m-d", strtotime($temp_date."+1 day"));
} // end while
*/

/* 데이터 저장 함수 날짜 PARAMETER는 YMD 형태로 넣어야 함 */
function z_member_stats($target_date, $next_date, $real_time) {
	// 가입자 수
	$join_count_sql = "SELECT COUNT(*) AS join_count FROM member WHERE (mb_join_date >= '".$target_date."' AND mb_join_date < '".$next_date."') GROUP BY SUBSTR(mb_join_date, 1, 10)";
	$join_count = sql_count($join_count_sql, 'join_count');

	// 가입자 중 당일 결제 회원 수
	$recharge_member_count_sql = "SELECT COUNT(DISTINCT(mpu.mpu_member)) AS recharge_member_count FROM member_point_used mpu LEFT JOIN member mb ON mpu.mpu_member = mb.mb_no where (mb.mb_join_date >= '".$target_date."' and mb.mb_join_date < '".$next_date."') AND (mpu.mpu_date >= '".$target_date."' and mpu.mpu_date < '".$next_date."') AND mpu.mpu_class = 'r' AND mpu.mpu_recharge_won != '0' GROUP BY SUBSTR(mb_join_date, 1, 10)";
	$recharge_member_count = sql_count($recharge_member_count_sql, 'recharge_member_count');

	// 가입자 중 당일 결제 건수
	$recharge_count_sql = "SELECT COUNT(*) AS recharge_count FROM member_point_used mpu LEFT JOIN member mb ON mpu.mpu_member = mb.mb_no WHERE (mb.mb_join_date >= '".$target_date."' and mb.mb_join_date < '".$next_date."') AND (mpu.mpu_date >= '".$target_date."' and mpu.mpu_date < '".$next_date."') AND mpu.mpu_class = 'r' AND mpu.mpu_recharge_won != '0' GROUP BY SUBSTR(mb_join_date, 1, 10)";
	$recharge_count = sql_count($recharge_count_sql, 'recharge_count');

	// 가입자 당일 결제 총액
	$recharge_total_sql = "SELECT SUM(mpu.mpu_recharge_won) AS recharge_total FROM member_point_used mpu LEFT JOIN member mb ON mpu.mpu_member = mb.mb_no WHERE (mb.mb_join_date >= '".$target_date."' and mb.mb_join_date < '".$next_date."') AND (mpu.mpu_date >= '".$target_date."' and mpu.mpu_date < '".$next_date."') AND mpu.mpu_class = 'r' AND mpu.mpu_recharge_won != '0' GROUP BY SUBSTR(mb_join_date, 1, 10)";
	$recharge_total = sql_count($recharge_total_sql, 'recharge_total');

	// 실 접속 회원수 (일별로 한 번이라도 로그인 한 회원 수)
	$login_count_sql = "SELECT COUNT(*) AS login_count FROM member WHERE (mb_login_date >= '".$target_date."' and mb_login_date < '".$next_date."')";
	$login_count = sql_count($login_count_sql, 'login_count');
	
	/* 실시간 검색 여부에 따라 데이터 저장 */
	// QUERY (INSERT, DELETE);
	$insert_sql = "INSERT INTO z_member_stats(z_ms_join_count, z_ms_recharge_member_count, z_ms_recharge_count, z_ms_recharge_total, z_ms_login_count, z_ms_real_time_date, z_ms_real_time_state, z_ms_date) VALUES('".$join_count."', '".$recharge_member_count."', '".$recharge_count."', '".$recharge_total."', '".$login_count."', '".NM_TIME_YMDHIS."', '".$real_time."', '".$target_date."')";

	$delete_sql = "DELETE FROM z_member_stats WHERE z_ms_date = '".$target_date."' AND z_ms_real_time_state = 'y'";
	
	// 날짜로 저장된 데이터가 있는지 먼저 조회
	$real_time_select_sql = "SELECT COUNT(*) AS real_time_cnt FROM z_member_stats WHERE z_ms_date = '".$target_date."' and z_ms_real_time_state = 'y'";
	$real_time_count = sql_count($real_time_select_sql, 'real_time_cnt');
	
	if($real_time_count != 0) {
		sql_query($delete_sql); // 있을 경우 삭제
	} // end if

	sql_query($insert_sql);
} // z_member_stats
?>