<?php 
if (!defined("_SH_HOUR_")) exit;

use Classes\Push as PushServer;

// require composer autoload
require_once NM_PATH.'/vendor/autoload.php'; 

$nm_config['cf_big'];
$episode_arr = array();
foreach($nm_config['cf_big'] as $cf_big_key => $cf_big_val){
	if($cf_big_val == ""){ continue; }
	array_push($episode_arr, $cf_big_key);
}

$now_hour_date = NM_TIME_YMD." ".NM_TIME_H; // 현재 날짜+시간
foreach($episode_arr as $episode_val){
	// 에피소드 서비스 처리
	${"sql_episode_".$episode_val} = "select * from comics_episode_".$episode_val." where ce_service_state = 'r' "; 
	${"result_episode_".$episode_val} = sql_query(${"sql_episode_".$episode_val});
	
	${"row_size_episode_".$episode_val} = sql_num_rows(${"result_episode_".$episode_val});
	if(${"row_size_episode_".$episode_val} > 0){
		while (${"row_episode_".$episode_val} = mysql_fetch_array(${"result_episode_".$episode_val})) {
			// 코믹스 정보
			$sql_comics = "select * from comics where cm_no = '".${"row_episode_".$episode_val}['ce_comics']."' ";
			$row_comics = sql_fetch($sql_comics);
			// 서비스 시간
			$service_date = substr(${"row_episode_".$episode_val}['ce_service_date'], 0, 10)." ".$row_comics['cm_res_service_date'];
			
			if($row_comics['cm_res_service_date'] != ''){ // 코믹스 시간
				if($service_date <= $now_hour_date){ // 서비스 시간이 현재시간 이후일 경우 실행
					$sql_episode_update = "update comics_episode_".$episode_val." set ce_service_state = 'y', ce_service_date = '".$service_date.":00:00' where ce_no = '".${"row_episode_".$episode_val}['ce_no']."' ";
					
					if(sql_query($sql_episode_update)){ // 에피소드 업데이트
						$sql_comics_update = "UPDATE comics SET cm_episode_date = '".NM_TIME_YMDHIS."', cm_push_date = '".NM_TIME_YMD."' WHERE cm_no = '".${"row_episode_".$episode_val}['ce_comics']."'";

						if(sql_query($sql_comics_update)) {
							// 성공했을시 시간 비교하여, APP PUSH
							if(NM_TIME_YMD != $row_comics['cm_push_date']) {

							    $msg = $row_comics['cm_series'].'가 업로드 되었습니다.';
                                $url = "dev.nexcube.co.kr/push_server/public/put.php";

                                $newInstance = new PushServer($url);

                                $mb_arr = array();
                                $token_arr = array();

                                /**
                                 * START FETCH
                                 * member mark-list and mb_token
                                 */
                                $sql="
                                  SELECT mcm_member FROM member_comics_mark WHERE mcm_comics = ".$row_comics['cm_no']."
                                ";
                                $query = sql_query($sql);

                                while ( $result = sql_fetch_array($query) ) {
                                    array_push($mb_arr, $result['mcm_member']);
                                }

                                $mb_arr_to_str = implode(',',$mb_arr);
                                $sql="
                                 SELECT mb_token FROM member WHERE mb_no IN ($mb_arr_to_str) AND mb_device = 0 AND mb_push = 1 AND mb_state = 'y'
                                ";
                                $query = sql_query($sql);

                                while ( $result = sql_fetch_array($query) ) {
                                    array_push($token_arr, $result['mb_token']);
                                }
                                /**
                                 * END FETCH
                                 */

                                $push_msg_arr = array(
                                    'KEY_IDN'   => $row_comics['cm_no'],
                                    'TYPE'      => 'NO_POPUP_DIRECTION',
                                    'TITLE'     => '웹툰 업데이트 알림',
                                    'CONTENT'   => $msg,
                                    'EVENT'     => get_comics_url($row_comics['cm_no'])
                                );

                                $push_msg_arr['registration_ids'] = $token_arr;

                                try {
                                    $newInstance->sendPushMsg($push_msg_arr);
                                } catch (\Exception $exception) {
                                    echo $exception->getMessage()." / ".$exception->getCode();
                                }

//								push($row_comics['cm_series'].'가 업로드 되었습니다.',$row_comics['cm_no']);

							} // end if
						} // end if
					}else{
						// 업데이트 실패시 로그
						cs_error_log($sh_path."/log/".NM_TIME_YMD.".log", $sql_episode_update, $_SERVER['PHP_SELF']);
					}
				}
			}
		}
	}
	// 에피소드 기다리다무료 처리
	${"sql_episode_".$episode_val} = "select * from comics_episode_".$episode_val." where ce_free_state = 'r' "; 
	${"result_episode_".$episode_val} = sql_query(${"sql_episode_".$episode_val});
	
	${"row_size_episode_".$episode_val} = sql_num_rows(${"result_episode_".$episode_val});
	if(${"row_size_episode_".$episode_val} > 0){
		while (${"row_episode_".$episode_val} = mysql_fetch_array(${"result_episode_".$episode_val})) {
			// 코믹스 정보
			$sql_comics = "select * from comics where cm_no = '".${"row_episode_".$episode_val}['ce_comics']."' ";
			$row_comics = sql_fetch($sql_comics);
			// 서비스 시간
			$free_date = substr(${"row_episode_".$episode_val}['ce_free_date'], 0, 10)." ".$row_comics['cm_res_free_date'];

			if($row_comics['cm_res_free_date'] != ''){ // 코믹스 시간
				if($free_date <= $now_hour_date){ // 서비스 시간이 현재시간 이후일 경우 실행
					$sql_episode_update = "update comics_episode_".$episode_val." set ce_free_state = 'y', ce_pay = '0', ce_free_date = '".$free_date.":00:00' where ce_no = '".${"row_episode_".$episode_val}['ce_no']."' ";
					if(sql_query($sql_episode_update)){ // 에피소드 업데이트
					}else{
						// 업데이트 실패시 로그
						cs_error_log($sh_path."/log/".NM_TIME_YMD.".log", $sql_episode_update, $_SERVER['PHP_SELF']);
					}
				}
			}
		}
	}
}

?>