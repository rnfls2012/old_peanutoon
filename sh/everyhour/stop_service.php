<?php
/***************************
	AUTO  SERVICE  STOP
***************************/

if (!defined("_SH_HOUR_")) exit;

$full_date = NM_TIME_YMDHIS;// 현재 날짜(연-월-일 시:분:초)
$now_date = NM_TIME_YMD;	// 현재 날짜
$now_hour = NM_TIME_H;		// 현재 시간(시)

$field_s = "cm_no, cm_res_stop_date, cm_res_stop_hour, cm_res_stop_service, cm_service";

$sql_s = "SELECT $field_s FROM comics WHERE cm_service = 'y' AND cm_res_stop_service = 'y' ";
$result_s = sql_query($sql_s);
$row_size_s = sql_num_rows($result_s);

/* START UPDATE */
if ($row_size_s > 0) {
	while ($row_s = mysql_fetch_array($result_s)) {
		$stop_date = $row_s['cm_res_stop_date'];	//서비스 중지 예약 날짜
		$stop_hour = $row_s['cm_res_stop_hour'];	//서비스 중지 예약 시간
		
		if ($row_s['cm_service'] == 'y') {
			if ($row_s['cm_res_stop_service'] == 'y' && (($stop_date == $now_date) && ($stop_hour == $now_hour))) {
				$sql_u = "UPDATE comics SET cm_service = 'n', cm_mod_date = '".$full_date."', cm_service_stop_date = '".$full_date."' WHERE cm_no = ".$row_s['cm_no']."";
				
				sql_query($sql_u);

				if (mysql_affected_rows() > 0) {
				} else {
					// 업데이트 실패시 로그
					cs_error_log($sh_path."/log/".NM_TIME_YMD.".log", $sql_u, $_SERVER['PHP_SELF']);
				}
			}
		} 
	}
}
/* END UPDATE */