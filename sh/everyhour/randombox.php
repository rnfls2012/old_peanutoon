<?php if (!defined("_SH_HOUR_")) exit;

/* 랜덤박스 이벤트 유효기간 중지 */
$randombox_select_sql = "SELECT * FROM event_randombox WHERE er_state='y'";
$randombox_select_result = sql_query($randombox_select_sql);
$randombox_select_size = sql_num_rows($randombox_select_result);

// 진행 중인 이벤트가 있으면
if($randombox_select_size > 0) {
	$randombox_select_array = array();

	while($randombox_select_row = sql_fetch_array($randombox_select_result)) {
		$date_end = $randombox_select_row['er_date_end']." ".$randombox_select_row['er_date_end_hour'].":59:59";

		if($date_end < NM_TIME_YMDHIS) {
			array_push($randombox_select_array, $randombox_select_row['er_no']);
		} // end if
	} // end while

	// 변경해야 할 이벤트가 있으면
	if(count($randombox_select_array) > 0) {
		$randombox_no = implode(", ", $randombox_select_array);
		$randombox_update_sql = "UPDATE event_randombox SET er_state='n' WHERE er_no IN(".$randombox_no.")";

		if(sql_query($randombox_update_sql)){ // 이벤트 상태 업데이트

		}else{
			// 업데이트 실패시 로그
			cs_error_log($sh_path."/log/".NM_TIME_YMD.".log", $randombox_update_sql, $_SERVER['PHP_SELF']);
		} // end else
	} // end if
} // end if



/* 랜덤박스 이벤트 예약 진행 */
$randombox_reserve_sql = "SELECT * FROM event_randombox WHERE er_state='r'";
$randombox_reserve_result = sql_query($randombox_reserve_sql);
$randombox_reserver_size = sql_num_rows($randombox_reserve_result);

if($randombox_reserver_size > 0) {
	$randombox_reserve_array = array();

	while($randombox_reserve_row = sql_fetch_array($randombox_reserve_result)) {
		if($randombox_reserve_row['er_date_start'] == NM_TIME_YMD && $randombox_reserve_row['er_date_start_hour'] == NM_TIME_H) {
			array_push($randombox_reserve_array, $randombox_reserve_row['er_no']);
		} // end if
	} // end while
	
	// 변경해야 할 이벤트가 있으면
	if(count($randombox_reserve_array) > 0) {
		$reserve_no = implode(", ", $randombox_reserve_array);
		$reserve_update_sql = "UPDATE event_randombox SET er_state='y' WHERE er_no IN(".$reserve_no.")";

		if(sql_query($reserve_update_sql)){ // 이벤트 상태 업데이트

		}else{
			// 업데이트 실패시 로그
			cs_error_log($sh_path."/log/".NM_TIME_YMD.".log", $reserve_update_sql, $_SERVER['PHP_SELF']);
		} // end else
	} // end if
} // end if

?>