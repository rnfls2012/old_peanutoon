<?php if (!defined("_SH_DAY_FOUR_TEN_")) exit;

$date_now = NM_SERVER_TIME;
// $date_now = strtotime('-2 days', NM_SERVER_TIME); // //////////////////////////// 임시
$date_now_m1 = strtotime('-1 days', $date_now);

$date_now_y = strval(date("Y", $date_now));
$date_now_m = strval(date("m", $date_now));
$date_now_d = strval(date("d", $date_now));
$date_now_h = strval(date("H", $date_now));

$date_now_m1_y = strval(date("Y", $date_now_m1));
$date_now_m1_m = strval(date("m", $date_now_m1));
$date_now_m1_d = strval(date("d", $date_now_m1));
$date_now_m1_h = strval(date("H", $date_now_m1));

/* 
	실시간으로 데이터를 넣지 않고 6시간 마다 리셋되는 기준의 시간으로 데이터를 넣습니다.
	예를 들어 지금 17년 07월 26일 17시라면....
	리셋되는 기준 시간이 04, 10, 16 22 이며, 17시면 16 기준으로 실행됩니다.
	자세한 내용은 아래과 같은 예시로 실행 됩니다.
	비교기준 : 17년 07월 26일 04시 ~ 17년 07월 26일 10시
	비교대상 : 17년 07월 26일 10시 ~ 17년 07월 26일 16시
*/
switch($date_now_h){ 
	case '04': case '05': case '06': case '07': case '08': case '09': // 04 ~ 09 
		$date_pre_y = $date_now_m1_y;	$date_pre_m = $date_now_m1_m;	$date_pre_d = $date_now_m1_d ;	$time_pre_h = '16'; 
		$date_cen_y = $date_now_m1_y;	$date_cen_m = $date_now_m1_m;	$date_cen_d = $date_now_m1_d ;	$time_cen_h = '22'; 
		$date_now_y = $date_now_y;		$date_now_m = $date_now_m;		$date_now_d = $date_now_d;		$time_now_h = '04';
	break;
	case '10': case '11': case '12': case '13': case '14': case '15': // 10 ~ 15 
		$date_pre_y = $date_now_m1_y;	$date_pre_m = $date_now_m1_m;	$date_pre_d = $date_now_m1_d ;	$time_pre_h = '22'; 
		$date_cen_y = $date_now_y;		$date_cen_m = $date_now_m;		$date_cen_d = $date_now_d;		$time_cen_h = '04'; 
		$date_now_y = $date_now_y;		$date_now_m = $date_now_m;		$date_now_d = $date_now_d;		$time_now_h = '10';
	break;
	case '16': case '17': case '18': case '19': case '20': case '21': // 16 ~ 21 
		$date_pre_y = $date_now_y;		$date_pre_m = $date_now_m;		$date_pre_d = $date_now_d;		$time_pre_h = '04'; 
		$date_cen_y = $date_now_y;		$date_cen_m = $date_now_m;		$date_cen_d = $date_now_d;		$time_cen_h = '10'; 
		$date_now_y = $date_now_y;		$date_now_m = $date_now_m;		$date_now_d = $date_now_d;		$time_now_h = '16';
	break;
	case '22': case '23': case '00': case '01': case '02': case '03': // 22 ~ 03
		$date_pre_y = $date_now_y;		$date_pre_m = $date_now_m;		$date_pre_d = $date_now_d;		$time_pre_h = '10'; 
		$date_cen_y = $date_now_y;		$date_cen_m = $date_now_m;		$date_cen_d = $date_now_d;		$time_cen_h = '16'; 
		$date_now_y = $date_now_y;		$date_now_m = $date_now_m;		$date_now_d = $date_now_d;		$time_now_h = '22';
	break;
}

$time_pre = $time_pre_h.':00:00';	// 비교-시간-시작
$time_cen = $time_cen_h.':00:00';	// 비교-시간-마감	비교-대상-시간-시작
$time_now = $time_now_h.':00:00';	// 비교-대상-시간-마감

$DBtable_cen = 'comics_ranking_small_auto_'.$time_cen_h;	// 이전 테이블(비교기준)
$DBtable_now = 'comics_ranking_small_auto_'.$time_now_h;	// 이전 테이블(비교대상)

// SQL
$date_cen_andor = " AND ";
if($date_pre_y != $date_cen_y || $date_pre_m != $date_cen_m || $date_pre_d != $date_cen_d){ $date_cen_andor = " OR "; }

$date_now_andor = " AND ";
if($date_cen_y != $date_now_y || $date_cen_m != $date_now_m || $date_cen_d != $date_now_d){ $date_now_andor = " OR "; }

$where_sales_cen = " AND 
					( 
						( sl_year_month = '".$date_pre_y ."-".$date_pre_m."' AND sl_day = '".$date_pre_d."' AND sl_hour >= '".$time_pre_h."' ) 
						".$date_cen_andor ." 
						( sl_year_month = '".$date_cen_y ."-".$date_cen_m."' AND sl_day = '".$date_cen_d."' AND sl_hour <  '".$time_cen_h."' ) 
					) 
";

$where_sales_now = " AND 
					( 
						( sl_year_month = '".$date_cen_y ."-".$date_cen_m."' AND sl_day = '".$date_cen_d."' AND sl_hour >= '".$time_cen_h."' ) 
						".$date_now_andor ."  
						( sl_year_month = '".$date_now_y ."-".$date_now_m."' AND sl_day = '".$date_now_d."' AND sl_hour <  '".$time_now_h."' ) 
					) 
";

$field_sales		= " cm_adult, cm_big, cm_no, sl_comics, sum(sl_all_pay+sl_pay)as sl_pay_sum, sum(sl_open)as sl_open_sum ";
$group_sales		= " group by sl_comics ";
$order_sales		= " order by sl_pay_sum desc ";
		
/* ////////////////////// 기존 데이터 삭제 ////////////////////// */
$sql_DBtable_cen_del = "delete from ".$DBtable_cen;
sql_query($sql_DBtable_cen_del);
$sql_DBtable_now_del = "delete from ".$DBtable_now;
sql_query($sql_DBtable_now_del);

/* ////////////////////// AUTO_cen ////////////////////// */
$adult_arr = array('', 'n'); // 성인 체크
foreach($adult_arr as $adult_val){
	
	// 성인SQL
	$where_cm_adult = '';
	if($adult_val == 'n'){ $where_cm_adult = " AND cm_adult='n' "; }

	foreach($nm_config['cf_small']as $cf_small_key  => $cf_small_val){
		// 장르SQL
		$where_cm_small = '';
		$where_cm_small = " AND cm_small='".$cf_small_key."' ";
		if($cf_small_key == 0){ $where_cm_small = ''; } // 전체일 경우
		$crs_class = strval($cf_small_key);
		if($adult_val == 'n'){ $crs_class.= '_t';  }

		/* ////////////////////// 판매순위검색 ////////////////////// */
		$sql_sales_cen = "	select $field_sales 
								FROM sales sl
								JOIN comics c ON sl.sl_comics = c.cm_no 
								where cm_service='y' $where_sales_cen $where_cm_adult $where_cm_small 
								$group_sales $order_sales 
		";
		$result_sales_cen = sql_query($sql_sales_cen);
		$row_size_sales_cen = sql_num_rows($result_sales_cen);
		// echo $sql_sales_cen.';<br/>';
		
		$comics_arr = array();
		$i=1;
		$sql_comics_arr = "";
		if($row_size_sales_cen > 0){
			for($i; $row_sales_cen = sql_fetch_array($result_sales_cen); $i++){
			/* ////////////////////// 기존 데이터 - 판매순위 넣기 ////////////////////// */
				$sql_DBtable_cen_insert = dbtable_insert($crs_class, $row_sales_cen, $i, $DBtable_cen);
				sql_query($sql_DBtable_cen_insert);	
				array_push($comics_arr, $row_sales_cen['cm_no']);
			}
			$sql_comics_arr = " AND cm_no not in (".implode(",",$comics_arr).") ";
		}

		/* ////////////////////// 판매순위검색-제외 ////////////////////// */
		$sql_cm_cen = " select * from comics where cm_service='y' $sql_comics_arr $where_cm_adult $where_cm_small order by cm_episode_date DESC";
		$result_cm_cen = sql_query($sql_cm_cen);
		for($i; $row_cm_cen = sql_fetch_array($result_cm_cen); $i++){
			/* ////////////////////// 기존 데이터 - 판매순위검색-제외 넣기 ////////////////////// */
			$sql_DBtable_cen_insert = dbtable_insert($crs_class, $row_cm_cen, $i, $DBtable_cen);
			// echo $sql_DBtable_cen_insert.';<br/>';
			sql_query($sql_DBtable_cen_insert);	
		}
	}
		// echo '<br/><br/><br/>';
}


/* ////////////////////// AUTO_now ////////////////////// */
$adult_arr = array('', 'n'); // 성인 체크
foreach($adult_arr as $adult_val){
	
	// 성인SQL
	$where_cm_adult = '';
	if($adult_val == 'n'){ $where_cm_adult = " AND cm_adult='n' "; }

	foreach($nm_config['cf_small']as $cf_small_key  => $cf_small_val){
		// 장르SQL
		$where_cm_small = '';
		$where_cm_small = " AND cm_small='".$cf_small_key."' ";
		if($cf_small_key == 0){ $where_cm_small = ''; } // 전체일 경우
		$crs_class = strval($cf_small_key);
		if($adult_val == 'n'){ $crs_class.= '_t';  }

		/* ////////////////////// 판매순위검색 ////////////////////// */
		$sql_sales_now = "	select $field_sales 
								FROM sales sl
								JOIN comics c ON sl.sl_comics = c.cm_no 
								where cm_service='y' $where_sales_now $where_cm_adult $where_cm_small 
								$group_sales $order_sales 
		";
		$result_sales_now = sql_query($sql_sales_now);
		$row_size_sales_now = sql_num_rows($result_sales_now);
		// echo $sql_sales_now.';<br/>';
		
		$comics_arr = array();
		$i=1;
		$sql_comics_arr = "";
		if($row_size_sales_now > 0){
			for($i; $row_sales_now = sql_fetch_array($result_sales_now); $i++){
			/* ////////////////////// 기존 데이터 - 판매순위 넣기 ////////////////////// */
				$sql_DBtable_now_insert = dbtable_insert($crs_class, $row_sales_now, $i, $DBtable_now);
				sql_query($sql_DBtable_now_insert);	
				array_push($comics_arr, $row_sales_now['cm_no']);
			}
			$sql_comics_arr = " AND cm_no not in (".implode(",",$comics_arr).") ";
		}

		/* ////////////////////// 판매순위검색-제외 ////////////////////// */
		$sql_cm_now = " select * from comics where cm_service='y' $sql_comics_arr $where_cm_adult $where_cm_small order by cm_episode_date DESC";
		$result_cm_now = sql_query($sql_cm_now);
		for($i; $row_cm_now = sql_fetch_array($result_cm_now); $i++){
			/* ////////////////////// 기존 데이터 - 판매순위검색-제외 넣기 ////////////////////// */
			$sql_DBtable_now_insert = dbtable_insert($crs_class, $row_cm_now, $i, $DBtable_now);
			// echo $sql_DBtable_now_insert.';<br/>';
			sql_query($sql_DBtable_now_insert);	
		}
	}
		// echo '<br/><br/><br/>';
}

/* ////////////////////// ranking_small ////////////////////// */

/* ////////////////////// 기존 데이터 삭제 ////////////////////// */
$sql_crsa_del = "delete from comics_ranking_small_auto";
sql_query($sql_crsa_del);

foreach($adult_arr as $adult_val){
	foreach($nm_config['cf_small']as $cf_small_key  => $cf_small_val){
		// 장르SQL
		$crs_class = strval($cf_small_key);
		if($adult_val == 'n'){ $crs_class.= '_t';  }

		// 장르 순위에서 고정값 가져오기
		$crs_class_arr = array();
		$sql_crs = " SELECT * FROM `comics_ranking_small` WHERE crs_lock='y' AND crs_class='".$crs_class."' ORDER BY crs_ranking ";
		$result_crs = sql_query($sql_crs);
		$row_size_crs = sql_num_rows($result_crs);
		if($row_size_crs > 0){
			while ($row_crs = sql_fetch_array($result_crs)) {
				array_push($crs_class_arr, $row_crs);
			}
		}

		$crs_class_cen_arr = array();
		/* ////////////////////// 비교기준 ////////////////////// */
		$sql_crs_cen = " SELECT * FROM $DBtable_cen WHERE crs_class='".$crs_class."' ORDER BY crs_ranking ";
		$result_crs_cen = sql_query($sql_crs_cen);
		while ($row_crs_cen = sql_fetch_array($result_crs_cen)) {
			array_push($crs_class_cen_arr, $row_crs_cen);
		}

		/* ////////////////////// 비교대상 ////////////////////// */
		$no = 1;		// 순위 //'n','ff','f','m','p','pp' // n:진입, ff:급상승, f:상승, m:고정, p:강하, pp:급강하 
		$sql_crs_class_arr = "";
		if(count($crs_class_arr) > 0){
			$sql_crs_class_arr = " AND crs_comics not in ( ";
			foreach($crs_class_arr as $crs_class_key  => $crs_class_val){
				$sql_crs_class_arr.= $crs_class_val['crs_comics'].",";
			}
			$sql_crs_class_arr = substr($sql_crs_class_arr,0,strrpos($sql_crs_class_arr, ","));
			$sql_crs_class_arr.= " ) ";
		}
		$sql_crs_now = " SELECT * FROM $DBtable_now WHERE crs_class='".$crs_class."' ".$sql_crs_class_arr." ORDER BY crs_ranking ";
		$no = 0;
		$result_crs_now = sql_query($sql_crs_now);
		while ($row_crs_now = sql_fetch_array($result_crs_now)) {
			$no++;
			// 고정값 확인하기
			if(count($crs_class_arr) > 0){
				foreach($crs_class_arr as $crs_class_key  => $crs_class_val){
					if($no == $crs_class_val['crs_ranking']){
						$crs_ranking = array();
						crs_ranking($crs_class, $crs_class_val, $crs_class_cen_arr, $no, 'y');
						$no++;
						unset($crs_class_arr[$crs_class_key]); // 해당 배열 삭제
					}
				}
			}

			/* ////////////////////// 비교기준와 비교대상 순위차이 ////////////////////// */
			$crs_ranking = array();
			crs_ranking($crs_class, $row_crs_now, $crs_class_cen_arr, $no, 'n');
		}
		
		// 남은 고정값 넣기(맨 끝에 있을때...)
		if(count($crs_class_arr) > 0){
			foreach($crs_class_arr as $crs_class_key  => $crs_class_val){
				$no++;
				crs_ranking($crs_class, $crs_class_val, $crs_class_cen_arr, $no, 'y');
			}
		}

		/* ////////////////////// 순위 CMS 16개 저장 ////////////////////// */
		$sql_crsa_del = "delete from comics_ranking_small WHERE crs_class='".$crs_class."' "; // 순위 CMS 16개 삭제
		sql_query($sql_crsa_del);

		$sql_crsa = " SELECT * FROM comics_ranking_small_auto WHERE crs_class='".$crs_class."' ORDER BY crs_ranking LIMIT 0, 16 ";
		$result_crsa = sql_query($sql_crsa);
		while ($row_crsa = mysql_fetch_array($result_crsa)) {
			// SQL
			$sql_crsa_insert = "INSERT INTO comics_ranking_small( 
			crs_class, crs_adult, crs_lock, crs_comics, crs_big, crs_ranking, crs_ranking_gap, crs_ranking_mark, crs_date
			)VALUES( 
			'".$crs_class."', '".$row_crsa['crs_adult']."', '".$row_crsa['crs_lock']."', '".$row_crsa['crs_comics']."', '".$row_crsa['crs_big']."', '".$row_crsa['crs_ranking']."', '".$row_crsa['crs_ranking_gap']."', '".$row_crsa['crs_ranking_mark']."', '".NM_TIME_YMDHIS."') 
			";
			sql_query($sql_crsa_insert);
		}
	}
	// echo '<br/><br/><br/>';
}


function crs_ranking($crs_class, $row, $crs_class_cen_arr, $no, $crs_lock){
	
	// 순위차이와 순위표기
	$crs_ranking = array();
	$crs_ranking_gap = '';
	$crs_ranking_mark = 'n';
	foreach($crs_class_cen_arr as $crs_class_cen_key => $crs_class_cen_val){
		$rank = $crs_class_cen_key+1;
		if($crs_class_cen_val['crs_comics'] == $row['crs_comics']){
			$crs_ranking_gap = $rank - $no;							// 순위차이
			//'n','ff','f','m','p','pp' // n:진입, ff:급상승, f:상승, m:고정, p:강하, pp:급강하 
			$gap_p = 20;	// 순위 ff
			$gap_m = -20;	// 순위 pp

			// 순위표기
			if($crs_ranking_gap == 0){
				$crs_ranking_mark = 'm';
			}else if($crs_ranking_gap > 0){
				$crs_ranking_mark = 'f';
				if($crs_ranking_gap > $gap_p){ $crs_ranking_mark = 'ff'; }
			}else if($crs_ranking_gap < 0){
				$crs_ranking_mark = 'p';
				if($crs_ranking_gap < $gap_m){ $crs_ranking_mark = 'pp'; }
			}else{
				$crs_ranking_mark = 'n';
			}
		}
	}
	// SQL
	$sql_crsa_insert = "INSERT INTO comics_ranking_small_auto( 
	crs_class, crs_adult, crs_lock, crs_comics, crs_big, crs_ranking, crs_ranking_gap, crs_ranking_mark, crs_date
	)VALUES( 
	'".$crs_class."', '".$row['crs_adult']."', '".$crs_lock."', '".$row['crs_comics']."', '".$row['crs_big']."', '".$no."', '".$crs_ranking_gap."', '".$crs_ranking_mark."', '".NM_TIME_YMDHIS."') 
	";
	sql_query($sql_crsa_insert);
	// echo $no.' : '.$sql_crsa_insert.';<br/>';
}

function dbtable_insert($crs_class, $row, $i, $DBtable){
	$sql_DBtable_insert = "INSERT INTO ".$DBtable."( 
	crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date
	)VALUES( 
	'".$crs_class."', '".$row['cm_adult']."', '".$row['cm_no']."', '".$row['cm_big']."', '".$i."', '".NM_TIME_YMDHIS."') 
	";
	return $sql_DBtable_insert;
}

?>