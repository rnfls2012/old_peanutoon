<? define('_SH_DAY_SIX_', true);

$sh_path = "/var/www/html/".NM_DOMAIN_DIR."/sh";
$sh_var = true; // sh용 변수 (다른 곳에서 include 시켰을 시에 함수 자동 실행 방지)

if($ready_path != ''){ // 강제실행시...
	$sh_path = $ready_path; 
}

// 회원-휴면처리
include_once $sh_path.'/everyday_six/members.php'; // 나중에;;;
include_once $sh_path.'/everyday_six/member_stats.php'; // 회원 일별 가입 및 결제 통계
include_once $sh_path.'/everyday_six/apk_stats.php'; // APK 다운 및 결제 통계


// //////////////////////////////// 실행시간 저장 ////////////////////////////////
cs_auto_time($sh_path, 'cf_sh_everyhour_six_time');
?>