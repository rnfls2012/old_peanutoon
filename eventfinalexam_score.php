<?php
include_once '_common.php'; // 공통

$finalexam_img_url = NM_IMG.'/finalexam/reception'; // 이미지 경로

$efp_no = num_check(tag_get_filter($_REQUEST['efp_no'])); 
$score = num_check(tag_get_filter($_REQUEST['score'])); // 점수
$time = tag_get_filter($_REQUEST['time']); // 시간

$head_thumbnail = $finalexam_img_url.'eventfinalexam.png?'.vs_para();

$efp_arr = array();

if($is_member == false){
	alert('올바른 방법으로 이용하시기 바랍니다.', NM_URL.'/eventfinalexam_reception.php');
	die;
}else{
	// 데이터 가져오기
	$sql_efp = "SELECT * FROM event_finalexam_period WHERE efp_state = 'y' AND efp_no='".$efp_no."' ";
	$row_efp = sql_fetch($sql_efp);
	$efp_arr = $row_efp;
}

include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/eventfinalexam_score.php");

include_once (NM_PATH.'/_tail.php'); // 공통