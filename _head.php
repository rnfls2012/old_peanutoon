<? include_once '_common.php'; // 공통 

/* 19 on/off 체크 */
$cs_url_adult = cs_url_adult($mb_adult_cookie);

/* 19 WEB 용 */
$cs_url_adultn = cs_url_adult($mb_adult_cookie, 'n');
$cs_url_adulty = cs_url_adult($mb_adult_cookie, 'y');

$adulty_on = '';
$adultn_on = 'on';
if($mb_adult_permission == 'y'){
	$adulty_on = 'on';
	$adultn_on = '';
}

$cmserial_lnb = array();
$cmserial_lnb = $nm_config['cf_public_cycle'];
foreach($cmserial_lnb as $cmserial_lnb_key => $cmserial_lnb_val){
	if($cmserial_lnb_val[0] < 1 || $cmserial_lnb_val[0] > 64){
		unset($cmserial_lnb[$cmserial_lnb_key]);
	}
}
array_unshift($cmserial_lnb, array('', '전체'));

/* DB */
/* 상단 메뉴 */
$lnb_arr = array();
$sql_lnb_adult = sql_adult($mb_adult_permission , 'cn_adult');
$sql_order = " order by cn_display_check asc, cn_order asc, cn_id asc ";
$sql_filed = " *,  CASE WHEN cn_display = 'y' THEN 0 ELSE 1 END AS cn_display_check "; // 가져올 필드 정하기
$sql_lnb = "SELECT $sql_filed FROM  comics_menu WHERE 1 AND LENGTH(cn_id)=2 $sql_lnb_adult $sql_order ";

$result_lnb = sql_query($sql_lnb);
// 현재 어느 메뉴인지 체크
$_current_page = $_SERVER['PHP_SELF'];
$_current_page = cs_url_adult_del($_current_page);

$_current_arr = explode("/", $_current_page);
$_current_lnb = trim($_current_arr[count($_current_arr)-1]); // 현재 접속한 파일명
if($_current_lnb == 'index.php'){ 
	$_current_lnb = ""; 
	/* 메인 또는 서브인지 체크 */
	// define('NM_INDEX', true);
	
	/* 메인 또는 서브인지 체크 listing으로 인해 수정 181112*/
	if(trim($_current_arr[1]) == 'index.php'){ // 메인이라면...
		define('NM_INDEX', true);
	}else{
		define('NM_INDEX', false);
	}
}else{
	/* 메인 또는 서브인지 체크 */
	define('NM_INDEX', false);
}

$html_lnb_id = str_replace(".php", "_lnb", $_current_lnb);

$lnb_cn_id = "";
for($i=0;$row_lnb = sql_fetch_array($result_lnb);$i++) {
// while ($row_lnb = sql_fetch_array($result_lnb)) {

	$row_lnb_current = "";
	$row_lnb_cn_link = trim($row_lnb['cn_link']); // 데이터 링크
	$row_lnb['cn_lnb_link'] = NM_URL."/".trim($row_lnb['cn_link']);

	if(strpos($row_lnb['cn_link'],'?')){
		$row_lnb_cn_link = trim(substr($row_lnb['cn_link'], 0, strpos($row_lnb['cn_link'],'?')));
	}
	if($_current_lnb == $row_lnb_cn_link){ $row_lnb_current = "on"; $lnb_cn_id = $row_lnb['cn_id']; }
	$row_lnb['cn_current'] = $row_lnb_current;

	if($nm_config['cf_menu'] > $i){ // 메뉴 출력만큼
		// 보기 여부
		if($row_lnb['cn_display'] == 'y'){
			array_push($lnb_arr,  $row_lnb);
		}else if($row_lnb['cn_display'] == 'n'){ // 숨김
		}else{
			if($nm_config['nm_mode'] == NM_MO){ // 모바일 경우
				if($row_lnb['cn_display'] == 'mob'){
					array_push($lnb_arr,  $row_lnb);
				}
			}else{ // PC일 경우
				if($row_lnb['cn_display'] == 'web'){
					array_push($lnb_arr,  $row_lnb);
				}
			}
		}
	}
}

$lnb_count = count($lnb_arr);
if($lnb_count > 0){
	$lnb_width = intval(100 / $lnb_count);
	$lnb_width_first = $lnb_width + (100 - intval($lnb_count)  * intval(100 / intval($lnb_count)));
}

/* 상단 서브 메뉴 */
$lnb_sub_arr = array();
$sql_lnb_sub_adult = sql_adult($mb_adult_permission , 'cn_adult');
$sql_lnb_sub = "SELECT * FROM  comics_menu WHERE 1 AND left(cn_id, 2) = '$lnb_cn_id' AND LENGTH(cn_id)=4 $sql_lnb_sub_adult ORDER BY  cn_id ASC ";
$result_lnb_sub = sql_query($sql_lnb_sub);
$row_size_lnb_sub = sql_num_rows($result_lnb_sub);

$lnb_sub_width = $lnb_sub_width_first = 100;
if($row_size_lnb_sub > 0){
	// 현재 어느 메뉴인지 체크
	$_current_lnb_sub_para = $_SERVER['QUERY_STRING'];
	$_current_lnb_sub_para = cs_url_adult_del($_current_lnb_sub_para);

	$_current_lnb_sub_para_arr = explode("&", $_current_lnb_sub_para);
	
	// 서브메뉴에서 서브메뉴의 파라마터 아무것도 없고 특정 메뉴 지정 하고 싶을때
	if($_current_lnb_sub_para_arr_define !=''){
		$_current_lnb_sub_para_arr[0] = $_current_lnb_sub_para_arr_define;
	}
	
	$_current_lnb_sub_count = count($_current_lnb_sub_para_arr);
	$row_lnb_sub_no = 0;

	while ($row_lnb_sub = sql_fetch_array($result_lnb_sub)) {
		
		$row_lnb_sub['cn_lnb_link'] = NM_URL."/".trim($row_lnb_sub['cn_link']);

		$row_lnb_sub_no++;
		$row_lnb_sub_current = "";
		$row_lnb_sub_cn_link_para = explode("?", trim($row_lnb_sub['cn_link']));

		if(strpos($row_lnb_sub['cn_link'],'?')){
			$row_lnb_sub_cn_link = trim(substr($row_lnb_sub['cn_link'], strpos($row_lnb_sub['cn_link'],'?')+1, strlen($row_lnb_sub['cn_link'])));
		}
		if($_current_lnb_sub_para_arr[0] == $row_lnb_sub_cn_link){
			$row_lnb_sub_current = "on"; 
		}else if($_current_lnb_sub_para == '' && $row_lnb_sub_no == 1 && $_current_lnb_sub_para_arr_define == ''){
			$row_lnb_sub_current = "on"; 
		}
		$row_lnb_sub['cn_current'] = $row_lnb_sub_current;
		// 보기 여부
		if($row_lnb_sub['cn_display'] == 'y'){
			array_push($lnb_sub_arr,  $row_lnb_sub);
		}else if($row_lnb_sub['cn_display'] == 'n'){ // 숨김
		}else{
			if($nm_config['nm_mode'] == NM_MO){ // 모바일 경우
				if($row_lnb_sub['cn_display'] == 'mob'){
					array_push($lnb_sub_arr,  $row_lnb_sub);
				}
			}else{ // PC일 경우
				if($row_lnb_sub['cn_display'] == 'web'){
					array_push($lnb_sub_arr,  $row_lnb_sub);
				}
			}
		}
	}
}

$lnb_sub_count = count($lnb_sub_arr);
if($lnb_sub_count > 0){
	$lnb_sub_width = intval(100 / $lnb_sub_count);
	$lnb_sub_width_first = $lnb_sub_width + (100 - intval($lnb_sub_count)  * intval(100 / intval($lnb_sub_count)));
}

/* view */
include_once($nm_config['nm_path']."/_head.php");

mkt_rdt_acecounter_login($nm_member);	// AceCounter 로그인-rdt_acecounter_ss_login
mkt_rdt_acecounter_join($nm_member);	// AceCounter 가입-rdt_acecounter_ss_join
mkt_rdt_acecounter_leave($nm_member);	// AceCounter 탈퇴-rdt_acecounter_ss_leave

?>