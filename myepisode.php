<?
include_once '_common.php'; // 공통

error_page();

/* 코믹스 정보 가져오기 */
$comics = get_comics($_comics);
$cm_big = $comics['cm_big'];
$small_txt = $nm_config['cf_small'][$comics['cm_small']]; // 장르
$comics['cm_cover_url'] = img_url_para($comics['cm_cover'], $comics['cm_reg_date'], $comics['cm_mod_date']);

/* 에피소드 정보 가져오기 */
if($comics['cm_package'] == 'n') {
	$episode_arr = cs_get_myepisode_list($comics['cm_no'], $nm_member, 'y', "ASC");
} else {
	$episode_arr = cs_get_episode_list($comics['cm_no'], $nm_member, 'y', "ASC");
}

// 무료화수 포함 화 리스트 0802
$calc_episode_count = count($episode_arr);

// 무료화수 제외한 화 리스트(이벤트 무료 화수는 리스트 포함)
if($comics['cm_package'] == 'y') {
	$calc_episode_count = 0;
	foreach($episode_arr as $ep_key => $ep_val) {
		if(!(($ep_val['ce_pay'] == 0 && $ep_val['ce_event_free'] == "") || ($ep_val['ce_pay'] == 0 && $ep_val['ce_event_sale'] != ""))) {
			$calc_episode_count++;
		} // end if
	} // end foreach
} // end if


/* DB */
// 완전 중지 리스트
$ce_no_x_arr = array();
$sql_ce_no_x = "";
$sql_x = "SELECT ce_no FROM comics_episode_".$cm_big." WHERE 1 AND ce_comics = '$_comics' AND ce_service_state = 'x' ORDER BY ce_order $sql_order ";
$result_x = sql_query($sql_x);
while ($row_x = sql_fetch_array($result_x)) {
	array_push($ce_no_x_arr, $row_x['ce_no']);
}
if(count($ce_no_x_arr) > 0){
	$sql_ce_no_x = " AND mbe_episode not in (".implode(",",$ce_no_x_arr).")";
}

// 구매 목록
$buy_episode_where = "and mbe_own_type in (0,1) and mbe_comics = '".$_comics."' and mbe_member = '".$nm_member['mb_no']."'";
// $buy_episode_where = "and mbe_comics = '".$_comics."' and mbe_member = '".$nm_member['mb_no']."'";
// $buy_episode_where = "and mbe_comics = '".$_comics."' and mbe_member = '".$nm_member['mb_no']."' and mbe_member_idx = '".$nm_member['mb_idx']."'";
$buy_episode_query = "select * from member_buy_episode_".$cm_big." where 1 $buy_episode_where $sql_ce_no_x order by mbe_chapter asc";
$buy_episode_result = sql_query($buy_episode_query);
$buy_episode_count = sql_num_rows($buy_episode_result);

// 퍼센티지
if($calc_episode_count != 0) {
	$buy_percent = ceil(($buy_episode_count/$calc_episode_count)*100);
} else {
	$buy_percent = 0;
} // end else

/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/myepisode.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>