<?
include_once '_common.php'; // 공통

error_page();

cs_login_check(); // 로그인 체크

/* 포인트파크 정보 */
// $pointpark_url = "http://dev.pointpark.com/ASP/Trade/Init.html"; // 테스트
$pointpark_url = "https://asp.pointpark.com/ASP/Trade/Init.html"; // 실시간

/* 포인트파크 관리자 
대행사 아이디 : nexcube
대행사 비밀번호 : nexcube00

테스트 관리자 : http://dev.pointpark.com/Olive/Member/CooperationTop.html
운영 관리자 : http://admin.pointpark.com/Olive/Member/CooperationTop.html
*/

/* 
SQL4Z0R 피너툰
50J7VAG 메가코믹스
*/

// 주문번호생성
$order_no = get_uniqid();

$pointpart_partner = "SQL4Z0R";
$pointpart_userKey = Security::encrypt($nm_member['mb_no'], NM_POINTPARK_KEY);
$pointpart_dealNo = Security::encrypt($order_no, NM_POINTPARK_KEY);
$pointpart_landingUrl= NM_PROC_URL."/pointpark_receipt.php?pointpark_order=".$order_no; // 모바일용 URL
// $pointpart_rtUrl = NM_PROC_URL."/pointpark.php"; /* 포인트파크정책상 안받는다고함 17-09-18 */

// 열기 전에 저장
$sql_pointpark_insert = " INSERT INTO pg_channel_pointpark (
	pointpark_member, pointpark_member_id, pointpark_member_idx, 
	pointpark_order, pointpark_date, pointpark_useragent
	)VALUES( 
	'".$nm_member['mb_no']."', '".$nm_member['mb_id']."', '".$nm_member['mb_idx']."', 
	'".$order_no."', '".NM_TIME_YMDHIS."', '".HTTP_USER_AGENT."'
	)";
if(sql_query($sql_pointpark_insert)){
}else{
	// 실패 로그 남기기
	cs_x_error_log("x_pg_channel_pointpark", "pointpark", NM_PATH, "sql_pointpark_insert", "", $sql_pointpark_insert, 1, "준비실패", $nm_member); // state-1	
}

/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/pointpark.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>