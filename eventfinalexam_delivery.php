<?php
include_once '_common.php'; // 공통

$finalexam_img_url = NM_IMG.'/finalexam/result'; // 이미지 경로


$efm_win = false;
$is_member_false = array('올바른 방법으로 이용하시기 바랍니다.', NM_URL.'/eventfinalexam_result.php');
$efm_win_false = array('당첨자이시면 1:1문의하시기 바랍니다.[error code:efm_win-false]', NM_URL.'/eventfinalexam_result.php');
if($is_member == false){
	if($nm_config['nm_mode'] == NM_PC){ // 팝업창
		pop_close($is_member_false[0], $is_member_false[1]);
		die;
	}else{
		alert($is_member_false[0], $is_member_false[1]);
		die;
	}
}else{
	// 당첨자 정보 가져오기
	$sql_efm = " select * from event_finalexam_member where 1 AND efm_win=1 
	 AND efm_member='".$nm_member['mb_no']."' AND efm_member_idx='".$nm_member['mb_idx']."'
	";
	$row_efm = sql_fetch($sql_efm);
	if($row_efm['efm_member'] !=''){ $efm_win = true; }
}

// 당첨자 아니면...
if($efm_win == false){
	if($nm_config['nm_mode'] == NM_PC){ // 팝업창
		pop_close($efm_win_false[0], $efm_win_false[1]);
		die;
	}else{
		alert($efm_win_false[0], $efm_win_false[1]);
		die;
	}
}

$nm_config['footer'] = false;

include_once($nm_config['nm_path']."/_head.sub.php");

include_once($nm_config['nm_path']."/eventfinalexam_delivery.php");

include_once($nm_config['nm_path']."/_tail.sub.php");