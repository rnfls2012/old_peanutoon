<?
include_once '_common.php'; // 공통

error_page();

/* DB */
/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */

/* view */

switch($_menu) {
	case 1 :
		if($_bn == "") {
			include_once(NM_PATH."/csnotice.php");
			if(substr($nm_config['nm_path'], -1) == "e") {
				include_once(NM_MO_MAIN_PATH.'/siteinfo.php');
			} // end if
		} else {
			include_once(NM_PATH."/csnoticeview.php");
		} // end else
		break; // 공지사항

	case 2 : 
		include_once(NM_PATH."/cshelp.php");
		break; // FAQ

	case 3 :
		cs_login_check(); // 로그인 체크
		if($_mode == "") {
			include_once(NM_PATH."/csask.php");
		} else {
			include_once(NM_PATH."/csaskwrite.php");
		} // end else
		break; // 1:1 문의

	case 4 : 
		cs_login_check(); // 로그인 체크
		include_once(NM_PATH."/cspublish.php");
		break; // 연재 문의

	default :
		include_once(NM_PATH."/csnotice.php");
		if(substr($nm_config['nm_path'], -1) == "e") {
			include_once(NM_MO_MAIN_PATH.'/siteinfo.php');
		} // end if
		break; // 공지사항
} // end switch
?>