<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_bh_no = tag_filter($_REQUEST['bh_no']);

$sql = "select * from board_help where bh_no = '$_bh_no'";

if($sql != ''){
	$row = sql_fetch($sql);
} // end if

/* 대분류로 제목 */
$page_title = "도움말";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<script type="text/javascript">
<!--
	window.onload = function(){popup_resize('<?=$_mode;?>');}
//-->
</script>
<section id="cms_help_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- cms_member_title -->

<section id="board_view">
	<table>
		<tbody>
			<tr>
				<th><label for="bh_title">제목</label></th>
				<td>
					<?=$row['bh_title'];?>
				</td>
			</tr>
			<tr>
				<th><label>분류</label></th>
				<td>
					<?=$d_bh_category[$row['bh_category']];?>
				</td>
			</tr>
			<tr>
				<th><label for="bh_text">내용</label></th>
				<td>
					<?=nl2br($row['bh_text']);?>
				</td>
			</tr>
			<?if($row['bh_link'] !='' && $row['bh_link_article'] != ''){?>
			<tr>
				<th><label for="bh_link">링크</label></th>
				<td>
					<a href="<?=$row['bh_link'];?>"><?=$row['bh_link_article'];?></a>
				</td>
			</tr>
			<?}?>
			<tr>
				<td colspan="2" class="submit_btn">
					<input type="reset" value="닫기" onclick="self.close()">
				</td>
			</tr>
		</tbody>
	</table>
</section><!-- notice_write -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>