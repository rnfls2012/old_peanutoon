<?
include_once '_common.php'; // 공통
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMETER CHECK */
array_push($para_list, 'mode','help','bh_title','bh_text','bh_category','bh_link','bh_link_article','bh_date');

/* 숫자 PARAMETER 체크 */
array_push($para_num_list, 'help', 'bh_category');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'bh_link', 'bh_link_article');

/* 에디터 PARAMITER 체크 */
// array_push($editor_list, 'bh_text'); // 사용 안하기로 함

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

$dbtable = "board_help";
$dbt_primary = "bh_no";
$para_primary = "help";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

/* DB field 아닌 목록 */
$db_field_exception = array('mode', 'help'); 

if($_mode == 'reg'){
	/* 고정값 */
	$_bh_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);

	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = $_bh_title.'의 데이터가 등록되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

/* 수정 */
}else if($_mode == 'mod'){
	$_bh_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$dbt_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$dbt_primary.'값이 없습니다.';
	}else{		
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= ' WHERE '.$dbt_primary.'="'.${'_'.$dbt_primary}.'"';
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $_bh_title.'의 데이터가 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$dbt_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$dbt_primary.'값이 없습니다.';
	}else{
		/* 데이터 삭제 */
		$sql_del = 'delete from '.$dbtable.' where '.$dbt_primary.'="'.${'_'.$dbt_primary}.'"';
		sql_query($sql_del);		
		$db_result['msg'] = $_bh_title.'의 데이터가 삭제되였습니다.';
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
pop_close($db_result['msg'], '', $db_result['error']);
die;

?>