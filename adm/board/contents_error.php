<?
include_once '_common.php'; // 공통

/* PARAMITER 
bh_no // 글 번호
*/


// 기본적으로 몇개 있는 지 체크;
$sql_report_total = "select count(*) as total_report from board_report where 1";
$total_report = sql_count($sql_report_total, 'total_report');

/* 데이터 가져오기 */
$ask_where = "";

// 검색단어+ 검색타입
if($_s_text){
	switch($_s_type){
		case '0': $report_where.= "and br.br_title like '%$_s_text%' ";
		break;
		case '1': $report_where.= "and mb.mb_id like '%$_s_text%'";
		break;
		case '2': $report_where.= "and cm.cm_series like '%$_s_text%'";
		default: $report_where.= "and ( br.br_title like '%$_s_text%' or mb.mb_id like '%$_s_text%' or cm.cm_series like '%$_s_text%')";
		break;
	} // end switch
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "br.br_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$report_order = "order by ".$_order_field." ".$_order;

$sql_report = "";
$field_report = " br.*, cm.cm_series, cm.cm_cover_sub, mb.mb_id "; // 가져올 필드 정하기
$limit_report = "";

$sql_report = "select $field_report FROM board_report br left join comics cm on br.br_comics = cm.cm_no left join member mb on br.br_member = mb.mb_no where 1 $report_where $report_order $limit_report";
$result = sql_query($sql_report);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호', 'br_no', 1));
array_push($fetch_row, array('썸네일', 'cm_cover_sub', 0));
array_push($fetch_row, array('작품명', 'cm_series', 0));
array_push($fetch_row, array('제목', 'br_title', 0));
array_push($fetch_row, array('작성자', 'br_id', 0));
array_push($fetch_row, array('등록일', 'br_date', 1));
array_push($fetch_row, array('관리', 'br_management', 0));

// if($_s_limit == ''){ $_s_limit = 30; }

$page_title = "오류제보";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>총 <?=$page_title?> : <?=number_format($total_report);?> 개</strong>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="report_search_form" id="report_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return report_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_s_type_btn">
				<?	$s_type_list = array('제목','아이디','작품명');
					tag_radios($s_type_list, "s_type", $_s_type, 'y', '전체', ''); ?>
				</div>
				<div class="cs_search_btn">
					<? tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit board_submit">
				<input type="submit" class="br_submit" data-base_h="30" data-on_h="130"  value="검색" id="cms_submit"> <!-- 요기 -->
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="board_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $cp_css.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>

			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?br_no=".$dvalue_val['br_no'];
					$popup_del_url = $popup_url."&mode=del";

					/* 데이터 */
					$cm_cover_sub_url = NM_IMG.$dvalue_val['cm_cover_sub'];

				?>
				<tr class="result_hover">
					<td class="<?=$cp_css.$fetch_row[0][1]?> text_center"><?=$dvalue_val['br_no'];?></td>
					<td class="<?=$cp_css.$fetch_row[1][1]?> text_center">
						<img src="<?=$cm_cover_sub_url;?>" width="75" height="75" alt="<?= $dvalue_val['cm_series'];?>표지"/>
					</td>
					<td class="<?=$cp_css.$fetch_row[2][1]?> text_center"><?=$dvalue_val['cm_series'];?></td>
					<td class="<?=$cp_css.$fetch_row[3][1]?> text_center"><?=$dvalue_val['br_title'];?></td>
					<td class="<?=$cp_css.$fetch_row[4][1]?> text_center"><?=$dvalue_val['mb_id'];?></td>
					<td class="<?=$cp_css.$fetch_row[5][1]?> text_center"><?=$dvalue_val['br_date'];?></td>
					<td class="<?=$cp_css.$fetch_row[6][1]?> text_center">
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','report_del_write', <?=$popup_cms_width?>, 900);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- help_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>