<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_ba_no = tag_filter($_REQUEST['ba_no']);

$sql = "select * from board_ask where ba_no = '$_ba_no'";

/* 모드설정 */
if($_mode==''){$_mode='ans'; /* 답변 */} // end if
switch($_mode){
	case "mod": 
	case "not": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "답변";
	break;
} // end switch

if($sql != ''){
	$row = sql_fetch($sql);
} // end if

$readonly = "";
if($_mode == "mod" || $_mode == "ans") {
	$readonly = "readonly";
} // end if

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "1:1 문의";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_notice_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- cms_member_title -->

<section id="board_write">
	<form name="ask_write_form" id="ask_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return ask_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="ask" name="ask" value="<?=$_ba_no;?>"/><!-- 수정/삭제시 글 번호 -->
		<table>
			<tbody>
				<tr>
					<th><label for="ba_title"><?=$required_arr[0]?>제목</label></th>
					<td>
						<input type="text" placeholder="제목을 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?>  <?=$readonly;?> 
							name="ba_title" id="ba_title" value="<?=$row['ba_title'];?>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="ba_request"><?=$required_arr[0]?>내용</label></th>
					<td>
						<textarea placeholder="내용을 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> <?=$readonly;?> name="ba_request" id="ba_request" ><?=$row['ba_request'];?></textarea>
					</td>
				<? if($_mode != "not") { ?>
				</tr>
					<th class="<?=$_mode?>"><label for="ba_admin_answer"><?=$required_arr[0]?>답변</label></th>
					<td>
						<textarea placeholder="내용을 입력해주세요<?=$required_arr[2]?>"<?($_mode != "not")?$required_arr[1]:""?> name="ba_admin_answer" id="ba_admin_answer"><?=$row['ba_admin_answer'];?></textarea>
					</td>
				<tr>
				<? } // end if ?>
				</tr>
				<tr>
					<td colspan="2" class="submit_btn">
						<input type="submit" name="<?=$_mode?>" id="<?=$_mode?>" value="<?=$mode_text?>">
						<input type="reset" value="취소" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
	 </form>
</section><!-- board_write -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>