<?
include_once '_common.php'; // 공통

/* PARAMITER 
bh_no // 글 번호
*/


// 기본적으로 몇개 있는 지 체크;
$sql_ask_total = "select count(*) as total_ask from board_ask where 1";
$total_ask = sql_count($sql_ask_total, 'total_ask');

/* 데이터 가져오기 */
$ask_where = "";

// 검색단어+ 검색타입
if($_s_text){
	switch($_s_type){
		case '0': $ask_where.= "and ba.ba_title like '%$_s_text%' ";
		break;
		case '1': $ask_where.= "and mb.mb_id like '%$_s_text%'";
		break;
		default: $ask_where.= "and ( ba.ba_title like '%$_s_text%' or mb.mb_id like '%$_s_text%')";
		break;
	} // end switch
}

if($_ba_mode != ''){
	$ask_where.= "and ba.ba_mode = '$_ba_mode' ";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "ba.ba_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$ask_order = "order by ".$_order_field." ".$_order;

$sql_ask = "";
$field_ask = " * "; // 가져올 필드 정하기
$limit_ask = "";

$sql_ask = "select $field_ask FROM board_ask ba left join member mb on ba.ba_member = mb.mb_no where 1 $ask_where $ask_order $limit_ask";
$result = sql_query($sql_ask);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호', 'ba_no', 1));
array_push($fetch_row, array('제목', 'ba_title', 1));
array_push($fetch_row, array('작성자', 'mb_id', 0));
array_push($fetch_row, array('등록일', 'ba_date', 1));
array_push($fetch_row, array('상태', 'ba_mode', 0));
array_push($fetch_row, array('답변일', 'ba_admin_date', 1));
array_push($fetch_row, array('관리', 'ba_management', 0));

// if($_s_limit == ''){ $_s_limit = 30; }

$ask_state = array("0"=>"답변 대기중", "1"=>"<span>답변 완료<span>", "4"=>"관리자");

$page_title = "1:1 문의";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>총 <?=$page_title?> : <?=number_format($total_ask);?> 개</strong>
	</div>
	<!--
	유지보수 기간때 AJAX 리스트로 여러 회원에게 보내게끔 수정해야함
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','ask_write', <?=$popup_cms_width;?>, 650);"><?=$page_title?> 등록</button>
	</div>
	-->
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="ask_search_form" id="ask_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return ask_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<? tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit board_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="board_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $cp_css.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?ba_no=".$dvalue_val['ba_no'];
					$popup_ans_url = $popup_url."&mode=ans";
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_not_url = $popup_url."&mode=not";
					$popup_del_url = $popup_url."&mode=del";

					/* 데이터 저장 */
					$db_ba_no = $dvalue_val['ba_no'];
					$db_ba_title = $dvalue_val['ba_title'];

					$db_ba_date = $dvalue_val['ba_date'];
					$db_ba_mode = $ask_state[$dvalue_val['ba_mode']];
					$db_ba_admin_date = $dvalue_val['ba_admin_date'];
				?>
				<tr class="result_hover">
					<td class="<?=$cp_css.$fetch_row[0][1]?> text_center"><?=$db_ba_no;?></td>
					<td class="<?=$cp_css.$fetch_row[1][1]?> text_center">
						<a href="#" onclick="popup('ask_view.php?ba_no=<?=$db_ba_no;?>','ask_view', <?=$popup_cms_width;?>, 900);"><?=$db_ba_title;?></a></td>
					<td class="<?=$cp_css.$fetch_row[2][1]?> text_center">
						<? if($dvalue_val['ba_mode'] == 4) {
								echo $db_mb_id;
						} else { ?>
								<a href="#" onclick="popup('../members/member_view.php?mb_id=<?=$dvalue_val['mb_id'];?>','member_view', <?=$popup_cms_width?>, 550);"><?=$dvalue_val['mb_id'];?></a> 
						<? } // end else ?>
					</td>
					<td class="<?=$cp_css.$fetch_row[3][1]?> text_center"><?=$db_ba_date;?></td>
					<td class="<?=$cp_css.$fetch_row[4][1]?> text_center mode<?=$dvalue_val['ba_mode']?>" ><?=$db_ba_mode;?></td>
					<td class="<?=$cp_css.$fetch_row[5][1]?> text_center"><?=$db_ba_admin_date;?></td>
					<td class="<?=$cp_css.$fetch_row[6][1]?> text_center">
						<? switch($dvalue_val['ba_mode']) { 
								case 0 : ?>
									<button class="ans_btn" onclick="popup('<?=$popup_ans_url;?>','help_ans_write', <?=$popup_cms_width;?>, 900);">답변</button>
									<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','help_del_write', <?=$popup_cms_width;?>, 900);">삭제</button>
									<? break; // case 0 답변 대기
								case 1 : ?>
									<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','help_mod_write', <?=$popup_cms_width;?>, 900);">수정</button>
									<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','help_del_write', <?=$popup_cms_width;?>, 900);">삭제</button>
									<? break; // case 1 답변 완료
								default : ?>
									<button class="not_btn" onclick="popup('<?=$popup_not_url;?>','help_not_write', <?=$popup_cms_width;?>, 550);">수정</button>
									<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','help_del_write', <?=$popup_cms_width;?>, 900);">삭제</button>
									<? break; // default 공지
						   } // end switch ?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- help_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>