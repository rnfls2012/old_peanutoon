<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_ba_no = tag_filter($_REQUEST['ba_no']);

$sql = "select * from board_ask where ba_no = '$_ba_no'";

if($sql != ''){
	$row = sql_fetch($sql);
} // end if

/* 대분류로 제목 */
$page_title = "1:1 문의";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<script type="text/javascript">
<!--
	window.onload = function(){popup_resize('<?=$_mode;?>');}
//-->
</script>
<section id="cms_notice_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- cms_member_title -->

<section id="board_view">
	<table>
		<tbody>
			<tr>
				<th>제목</th>
					<td>
						<?=$row['ba_title']?>
					</td>
				</tr>
				<tr>
					<th>내용</th>
					<td>
						<?=nl2br($row['ba_request']);?>
					</td>
				</tr>
					<th><label for="ba_admin_answer">답변</label></th>
					<td>
						<?=nl2br($row['ba_admin_answer']);?>
					</td>
				<tr>
				</tr>
				<tr>
					<td colspan="2" class="submit_btn">
						<input type="reset" value="닫기" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
</section><!-- notice_write -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>