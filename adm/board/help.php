<?
include_once '_common.php'; // 공통

/* PARAMITER 
bh_no // 글 번호
*/

// 기본적으로 몇개 있는 지 체크;
$sql_help_total = "select count(*) as total_help from board_help where 1  ";
$total_help = sql_count($sql_help_total, 'total_help');


/* 데이터 가져오기 */
$help_where = "";

// 검색단어+ 검색타입
if($_s_text){
	$help_where.= "and bh_title like '%$_s_text%' ";
}

if($_bh_category){
	$help_where.= "and bh_category = '$_bh_category' ";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "bh_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$help_order = "order by ".$_order_field." ".$_order;

$sql_help = "";
$field_help = " * "; // 가져올 필드 정하기
$limit_help = "";

$sql_help = "select $field_help FROM board_help where 1 $help_where $help_order $limit_help";
$result = sql_query($sql_help);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','bh_no',1));
array_push($fetch_row, array('제목','bh_title',1));
array_push($fetch_row, array('분류','bh_category',0));
array_push($fetch_row, array('등록일','bh_date',0));
array_push($fetch_row, array('관리','bh_management',0));

if($_s_limit == ''){ $_s_limit = 10; }

$bh_category = array(1 => "회원", 2 => "결제", 3 => "이용");

$page_title = "FAQ";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>총 <?=$page_title?> : <?=number_format($total_help);?> 개</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','help_write', <?=$popup_cms_width;?>, 650);"><?=$page_title?> 등록</button>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="help_search_form" id="help_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return help_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<? tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit board_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="board_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $cp_css.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?bh_no=".$dvalue_val['bh_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";
				?>
				<tr class="result_hover">
					<td class="<?=$cp_css.$fetch_row[0][1]?> text_center"><?=$dvalue_val['bh_no'];?></td>
					<td class="<?=$cp_css.$fetch_row[1][1]?> text_center">
						<a href="#" onclick="popup('help_view.php?bh_no=<?=$dvalue_val['bh_no'];?>','help_view', <?=$popup_cms_width;?>, 650);"><?=$dvalue_val['bh_title'];?></a></td>
					<td class="<?=$cp_css.$fetch_row[2][1]?> text_center"><?=$bh_category[$dvalue_val['bh_category']]?></td>
					<td class="<?=$cp_css.$fetch_row[3][1]?> text_center"><?=$dvalue_val['bh_date'];?></td>
					<td class="<?=$cp_css.$fetch_row[4][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','help_mod_write', <?=$popup_cms_width;?>, 650);">수정</button>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','help_del_write', <?=$popup_cms_width;?>, 900);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- board_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>