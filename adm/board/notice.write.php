<?
include_once '_common.php'; // 공통
include_once (NM_EDITOR_LIB);

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_bn_no = tag_filter($_REQUEST['bn_no']);

$sql = "select * from board_notice where bn_no = '$_bn_no'";

/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */} // end if
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
	break;
} // end switch

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "공지사항";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
echo $editor_content_js;
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<script type="text/javascript">
<!--
	window.onload = function(){popup_resize('<?=$_mode;?>');}
//-->
</script>

<section id="cms_notice_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- cms_notice_title -->

<section id="board_write" class="editor_html">
	<form name="notice_write_form" id="notice_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return notice_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/> <!-- 입력모드 -->
		<input type="hidden" id="notice" name="notice" value="<?=$_bn_no;?>"/><!-- 수정/삭제시 글 번호 -->
		<input type="hidden" id="bn_member" name="bn_member" value="<?=$nm_member['mb_no'];?>"/><!-- 작성자 번호 -->
		<input type="hidden" id="bn_member_idx" name="bn_member_idx" value="<?=$nm_member['mb_idx'];?>"/><!-- 작성자 번호 -->

		<table>
			<tbody>
				<tr>
					<th><label for="bn_title"><?=$required_arr[0]?>제목</label></th>
					<td>
						<input type="text" placeholder="제목을 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> 
							name="bn_title" id="bn_title" value="<?=$row['bn_title'];?>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="bn_text"><?=$required_arr[0]?>내용</label></th>
					<td>
						<? if($_mode == "del") { 
							   echo nl2br($row['bn_text']);
						   } else {
							   echo editor_html('bn_text', nl2br(stripslashes($row['bn_text'])));
						   } // end else ?>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="submit_btn">
						<input type="submit" value="<?=$mode_text?>">
						<input type="reset" value="취소" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
	 </form>
</section><!-- board_write -->
<script type="text/javascript">
<!--
	function notice_write_submit(f){
		<?php echo get_editor_js('bn_text'); ?>

			

		/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
		var onlynumber = onlynumber_text = "";
		var onlynumber_check = true;
		$('.onlynumber').each(function() {
			onlynumber = $(this).val();
			onlynumber_text = '숫자만 입력해주세요';
			if(onlynumber != ''){
				var pattern = /(^[0-9]+$)/;
				if (!pattern.test(onlynumber)) {
					alert(onlynumber_text);
					$(this).val('');
					$(this).focus();
					onlynumber_check = false;
					return false; 
				}
			}
		});
		if(onlynumber_check == false){ return false; }
	}
//-->
</script>


<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>