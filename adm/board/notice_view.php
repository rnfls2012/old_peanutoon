<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_bn_no = tag_filter($_REQUEST['bn_no']);

$sql = "select * from board_notice where bn_no = '$_bn_no'";

if($sql != ''){
	$row = sql_fetch($sql);
} // end if

/* 대분류로 제목 */
$page_title = "알림";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<script type="text/javascript">
<!--
	window.onload = function(){popup_resize('<?=$_mode;?>');}
//-->
</script>
<section id="cms_notice_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- cms_member_title -->

<section id="board_view">
	<table>
		<tbody>
			<tr>
				<th><label for="bn_title">제목</label></th>
					<td>
						<?=$row['bn_title']?>
					</td>
				</tr>
				<tr>
					<th><label for="bn_text">내용</label></th>
					<td>
						<?=nl2br($row['bn_text']);?>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="submit_btn">
						<input type="reset" value="닫기" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
</section><!-- board_view -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>