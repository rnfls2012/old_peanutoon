<?
include_once '_common.php'; // 공통

/* PARAMETER
	s_type // 검색 타입 -> 1. 전체, 2. 아이디, 3. 이름
	s_text // 검색 단어
	order_field // 정렬 필드
	date_type // 날짜 타입
	s_date // 날짜-시작
	e_date // 날짜-끝
	s_limit // 보여줄 갯수
*/

// 기본적으로 몇개 있는지 체크
$sql_publish_total = "select count(*) as total_publish from board_publish where 1 $bp_where";
$total_publish = sql_count($sql_publish_total, 'total_publish');

// 검색단어+ 검색타입
if($_s_text){
	switch($_s_type){
		case '0': $bp_where.= "and mb.mb_id like '%$_s_text%' ";
		break;
		case '1': $bp_where.= "and bp.bp_title like '%$_s_text%'";
		break;
		default: $bp_where.= "and ( mb.mb_id like '%$_s_text%' or bp.bp_title like '%$_s_text%' )";
		break;
	} // end switch
} // end if

// 연재 문의 날짜
$start_date = $end_date = "";
if($_date_type) { //all, upload_date, new_date
	if($_s_date && $_e_date) {
		$start_date = $_s_date." ".NM_TIME_HI_start;
		$end_date = $_e_date." ".NM_TIME_HI_end;
	} else if($_s_date) {
		$start_date = $_s_date." ".NM_TIME_HI_start;
		$end_date = NM_TIME_YMD." ".NM_TIME_HI_end;
	}
	
	switch($_date_type){
		case 'bp_date' : $bp_where.= "and bp.bp_date >= '$start_date' and bp.bp_date <= '$end_date'";
		break;

		default: $bp_where.= "";
		break;
	}
} else {
	$_s_date = "";
	$_e_date = "";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "bp.bp_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$publish_order = "order by ".$_order_field." ".$_order;

$publish_sql = "";
$publish_field = " * "; // 가져올 필드 정하기
$publish_limit = ""; // 가져올 갯수 정하기

$publish_sql = "SELECT $publish_field FROM board_publish bp LEFT JOIN member mb ON bp.bp_member = mb.mb_no WHERE 1 $bp_where $publish_order $publish_limit";

$result = sql_query($publish_sql); // 페이징 처리 필수 변수명
$row_size = sql_num_rows($result); // 페이징 처리 필수 변수명

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('문의번호', 'bp_no', 0));
array_push($fetch_row, array('아이디', 'bp_id', 0));
array_push($fetch_row, array('제목', 'bp_title', 0));
array_push($fetch_row, array('문의 날짜', 'bp_date', 1));
array_push($fetch_row, array('관리', 'bp_management', 0));

$cs_calendar_on = $cs_submit_h = '';
if($_date_type){
	$cs_calendar_on = 'cs_calendar_on';
	$cs_submit_h = 'cs_submit_on';
}

$page_title = "연재 문의";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$cms_page_title;?></h1>
	<div>
		<strong><?=$page_title;?> : <?=number_format($total_publish);?>건</strong>
	</div>
</section> <!-- bp_head -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="publish_search_form" id="publish_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return publish_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_s_type_btn">
				<?	$s_type_list = array('아이디','제목');
					tag_radios($s_type_list, "s_type", $_s_type, 'y', '전체', ''); ?>
				</div>
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
				<div class="cs_search_btn">
					<?	$d_bp_date_type = array('bp_date'=>'문의 날짜');
					tag_selects($d_bp_date_type, "date_type", $_date_type, 'y', '날짜 검색안함', ''); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit publish_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="72" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="board_result">
	<h3 id="cr_thead_mg_add">검색 리스트
		<strong>검색 결과 수 : <?=number_format($row_size);?>명</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){ $th_title_giho = $order_giho; }
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){ 
					/* URL */
					$bp_down_url = NM_URL."/download/storage.php?storage_file=".$dvalue_val['bp_file'];
					$bp_del_url = $_cms_update."?mode=del&bp_no=".$dvalue_val['bp_no']."&bp_file=".urlencode($dvalue_val['bp_file']);
					?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['bp_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center">
						<a href="#" onclick="popup('<?=NM_ADM_URL?>/members/member_view.php?mb_id=<?=$dvalue_val['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 550);"><?=$dvalue_val['mb_id'];?></a>
						<!-- 615,765 -->
					</td>
					<td class="<?=$fetch_row[2][1]?> text_center">
						<a href="#" onclick="popup('<?=$_cms_folder?>/publish_view.php?bp_no=<?=$dvalue_val['bp_no'];?>','publish_view', <?=$popup_cms_width;?>, 900);"><?=$dvalue_val['bp_title'];?></a>
					</td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['bp_date'];?>
					<td class="<?=$fetch_row[4][1]?> text_center">
						<button class="down_btn" onclick="location.href='<?=$bp_down_url;?>'">다운로드</button>
						<button class="del_btn" onclick="location.href='<?=$bp_del_url;?>'">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>

<? page_view(); /* 페이지처리 */ ?>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>