<?
include_once '_common.php'; // 공통

$row_member = mb_get($_mb_id);
if(count($row_member) < 2){
	alert($_mb_id."는 없는 ID 입니다.", $_SERVER['HTTP_REFERER']);
	die;
}
$row = $row_member;

$stats_log_member_where = "AND slm_member='".$row_member['mb_no']."' 
					AND slm_member_idx='".$row_member['mb_idx']."'";

$stats_log_member_total_sql = "select count(*) as slm_total from stats_log_member where 1 $stats_log_member_where ";
$stats_log_member_total = sql_count($stats_log_member_total_sql, 'slm_total');


// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "slm_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$stats_log_member_order = "order by ".$_order_field." ".$_order;

$sql_stats_log_member = "select * from stats_log_member 
					where 1 $stats_log_member_where $stats_log_member_order";

$result = sql_query($sql_stats_log_member);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','slm_no',1));
array_push($fetch_row, array('로그날짜','slm_date',1));
array_push($fetch_row, array('로그IP<br/>접속버전','slm_ip',0));
array_push($fetch_row, array('로그환경<br/>접속브라우저','slm_type',0));
array_push($fetch_row, array('자세한환경','slm_user_agent',0));

/* 페이징 */
if($_s_limit == ''){ $_s_limit = 6; }

$mode_text = "로그 내역";

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

$cms_head_title = $row_member['mb_id'];

$head_title = "엠짱-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더


include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
	<div class="clear write left">
		<ul class="cms_btn_list">		
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view.php?mb_id=<?=$row_member['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);">회원 상세 내역</button></li> <!-- 1030,800 -->		
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view_point.php?mb_id=<?=$row_member['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);"><?=$nm_config['cf_point_unit_ko']?>&<?=$nm_config['cf_cash_point_unit_ko']?> 내역</button></li> <!-- 1030,800 -->
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view_comics.php?mb_id=<?=$row_member['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);">코믹스 열람 내역</button></li>
			<li><button onclick="#">로그 내역</button></li> 
		</ul>
	</div>
</section> <!-- cms_page_title -->


<section id="member_view_login">
	<div id="cr_bg">
		<table>
			<thead>
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){

					/* 등록일 */
					$db_slm_date_ymd = get_ymd($dvalue_val['slm_date']);
					$db_slm_date_his = get_his($dvalue_val['slm_date']);
				?>
				<tr>
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['slm_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$db_slm_date_ymd;?><br/><?=$db_slm_date_his;?></td>					
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['slm_ip'];?><br/><?=$dvalue_val['slm_version'];?></td>			
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['slm_type'];?><br/><?=$dvalue_val['slm_kind'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['slm_user_agent'];?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- cms_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>