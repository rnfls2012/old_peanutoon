<?
include_once '_common.php'; // 공통

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

$cms_head_title = "APK 이용 회원 ".$nm_config['cf_point_unit_ko']." 지급";

$head_title = "엠짱-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?></h1>
</section><!-- cms_page_title -->

<section id="member_write">
	<form name="apk_down_write_form" id="apk_down_write_form" enctype="multipart/form-data" method="post" action="<?=NM_ADM_URL;?>/members/apk_down.update.php" onsubmit="return apk_down_write_submit();">
		<table>
			<tbody>
				<tr>
					<th><label for="mpi_from"><?=$required_arr[0]?>지급 내용</label></th>
					<td>
						<input type="text" placeholder="지급 내용을 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="mpi_from" id="mpi_from" autocomplete="off" value="" />
					</td>
				</tr>
				<tr>
					<th><label for="mb_point"><?=$required_arr[0]?>지급 <?=$nm_config['cf_point_unit_ko'];?></label></th>
					<td><input type="text" placeholder="지급 <?=$nm_config['cf_point_unit_ko'].$required_arr[2];?>" <?=$required_arr[1]?> name="mb_point" id="mb_point" autocomplete="off" value="" /></td>
				</tr>
				<tr>
					<td colspan="2" class="submit_btn">
						<input type="submit" value="지급">
						<input type="reset" value="취소" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
	 </form>
</section><!-- mb_write -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>