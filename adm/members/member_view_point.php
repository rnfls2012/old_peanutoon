<?
include_once '_common.php'; // 공통

$row_member = mb_get($_mb_id);
if(count($row_member) < 2){
	alert($_mb_id."는 없는 ID 입니다.", $_SERVER['HTTP_REFERER']);
	die;
}
$row = $row_member;

$point_class = array('', '획득', '사용', '환불중', '환불완료');

$point_class_where = "";
if($_mpu_point_class != "") {
	if($_mpu_point_class == 0) {
		$point_class_where = "";
	}else if($_mpu_point_class == 1) {
		$point_class_where = " AND (mpu_class='r' OR mpu_class='e') AND mpu_refund=0  ";
	}else if($_mpu_point_class == 2) {
		$point_class_where = " AND mpu_class='b' ";
	}else if($_mpu_point_class == 3) {
		$point_class_where = " AND (mpu_class='r' AND mpu_refund=1 AND mpu_recharge_won>0) ";
	} else {
		$point_class_where = " AND (mpu_class='r' AND (mpu_refund=2 OR (mpu_refund=1 AND mpu_recharge_won<0))) ";
	} // end else
} // end if

/* 사용, 충전 내역 DB */
$point_arr = array();
$sql_point_use = "SELECT *, 
						IF(mpu_class='b', '사용', 
							IF(mpu_refund=1 && mpu_recharge_won>0, '환불중',
								IF(mpu_refund=1 && mpu_recharge_won<0, '환불완료', 
									IF(mpu_refund=2, '환불완료', 
										IF(mpu_recharge_cash_point<0 || mpu_recharge_point<0, '사용', '획득'))))) as class, 
						IF(mpu_class='b', mpu_cash_point, mpu_recharge_cash_point) as mpu_cash_point_result, 
						IF(mpu_class='b', mpu_point, mpu_recharge_point) as mpu_point_result, 
						IF(mpu_class='b', 'red', IF(mpu_recharge_cash_point<0 || mpu_recharge_point<0, 'red', 'blue')) as mpu_color 
						FROM member_point_used WHERE mpu_member = '".$row['mb_no']."' and mpu_member_idx = '".$row['mb_idx']."'".$point_class_where." order by mpu_no desc";

$result = sql_query($sql_point_use);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','mpu_no',3));
array_push($fetch_row, array('날짜','mpu_date',3));
array_push($fetch_row, array('내역 상세','mpu_from',3));
array_push($fetch_row, array('구별','class',3));
array_push($fetch_row, array('충전/사용 내역','mpu_result',2));
array_push($fetch_row, array('잔여 내역','mpu_balance',2));

/* 페이징 */
if($_s_limit == ''){ $_s_limit = 10; }

// $_page_mode = 'array'; // 페이지배열

$mode_text = $nm_config['cf_point_unit_ko']."&".$nm_config['cf_cash_point_unit_ko']." 내역";

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

$payment_mb_id = $cms_head_title = $row['mb_id'];

$head_title = "엠짱-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더

include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()

?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<style type="text/css">
	#cms_page_title .payment{
		position: absolute;
		top:22px;
		right:5px;
	}

	#cms_page_title .payment a {
		display: inline-block;
		font-size: 15px;
		font-family: Nanum Gothic,dotum;
		font-weight: bold;
		background: #fb6b6b;
		padding: 5px 10px;
		margin-left: 5px;
		color: #fff;
		border-radius: 50px;
	}
</style>
<script type="text/javascript">
<!--
	function this_pop_close(){
		// window.close();
		// self.close();
		// window.open("", "_self").close();
	}
//-->
</script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
	<div class="clear write left">
		<ul class="cms_btn_list">		
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view.php?mb_id=<?=$row_member['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);">회원 상세 내역</button></li> <!-- 1030,800 -->
			<li><button onclick="#"><?=$nm_config['cf_point_unit_ko']?>&<?=$nm_config['cf_cash_point_unit_ko']?> 내역</button></li>
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view_comics.php?mb_id=<?=$row_member['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);">코믹스 열람 내역</button></li> 
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view_login.php?mb_id=<?=$row_member['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);"><?=$big_val?>로그 내역</button></li> <!-- 1030,800 -->
		</ul>
		<div class="payment"><a href="<?=NM_URL?>/adm/sales/used/payment.php?s_text=<?=$payment_mb_id;?>" target="_blank" onclick="this_pop_close();">결제 상세내역 이동</a></div>
	</div>
</section> <!-- cms_page_title -->

<section id="member_view_point">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<table>
		<tbody>
			<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = $th_rowspan = $th_colspan = "";
					switch($fetch_val[2]){
						case '3': $th_rowspan = 'rowspan="2"';
						break;
						case '2': $th_colspan = 'colspan="2"';
						break;

					}
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th <?=$th_rowspan?> <?=$th_colspan?> class="<?=$th_class?>"><?=$th_title?>
					<? if($fetch_key == 3) { ?>
					<form name="member_point_form" id="member_point_form" method="post" action="<?=$_cms_self;?>" onsubmit="return member_point_submit();">
						<input type="hidden" name="mb_id" id="mb_id" value="<?=$_mb_id?>"/>
						<?tag_selects($point_class, 'mpu_point_class', $_mpu_point_class, 'y', '전체', '');?>
					</form>
					<? } // end if ?>
					</th>
				<?}?>
			</tr>
			<tr>
			<?for($u=0;$u<2;$u++){?>
				<th class="cash_point_unit"><?=$nm_config['cf_cash_point_unit_ko']?></th>
				<th class="point_unit"><?=$nm_config['cf_point_unit_ko'];?></th>
			<?}?>
			</tr>
			<? //foreach($point_arr as $point_key => $point_val) { ?>
			<? foreach($row_data as $dvalue_key => $dvalue_val){ ?>
				<tr>
					<td class="<?=$fetch_row[0][1]?>"><?=$dvalue_val['mpu_no'];?></td>
					<td class="<?=$fetch_row[1][1]?>"><?=substr($dvalue_val['mpu_date'],2);?></td>
					<td class="<?=$fetch_row[2][1]?>"><?=$dvalue_val['mpu_from'];?></td>
					<td class="<?=$fetch_row[3][1]." ".$dvalue_val['mpu_color']?>"><?=$dvalue_val['class'];?></td>
					<td class="<?=$fetch_row[4][1]?>"><?=$dvalue_val['mpu_cash_point_result'].$nm_config['cf_cash_point_unit'];?></td>
					<td class="<?=$fetch_row[4][1]?>"><?=$dvalue_val['mpu_point_result'].$nm_config['cf_point_unit'];?></td>
					<td class="<?=$fetch_row[5][1]?>"><?=$dvalue_val['mpu_cash_point_balance'].$nm_config['cf_cash_point_unit'];?></td>
					<td class="<?=$fetch_row[5][1]?>"><?=$dvalue_val['mpu_point_balance'].$nm_config['cf_point_unit'];?></td>
				</tr>
			<? } // end foreach ?>
		</tbody>
	</table>
</section> <!-- member_view_point -->
<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>