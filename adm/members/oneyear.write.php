<?
include_once '_common.php'; // 공통
include_once (NM_EDITOR_LIB);

/* DB */
$sql = "select * from config_long_text";
$row = sql_fetch($sql);

/* PARAMITER */

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "휴면 안내 이메일";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
echo $editor_content_js;
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> 등록</h1>
</section><!-- cms_page_title -->

<section id="member_write" class="editor_html">
	<form name="human_email_write_form" id="human_email_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return human_email_write_submit();">
		<input type="hidden" id="mode" name="mode" value="mod"/> <!-- 수정모드 -->
		<input type="hidden" id="clt_no" name="clt_no" value="1"/> <!-- clt_no -->
		<table class="email_write">
			<tbody>
				<tr>
					<th><label for="clt_human_email_title"><?=$required_arr[0]?>제목</label></th>
					<td>
						<input type="text" placeholder="제목을 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> 
							name="clt_human_email_title" id="clt_human_email_title" value="<?=$row['clt_human_email_title'];?>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="clt_human_email_text"><?=$required_arr[0]?>내용</label></th>
					<td>
					  <? echo editor_html('clt_human_email_text', nl2br(stripslashes($row['clt_human_email_text']))); ?>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="submit_btn">
						<input type="submit" value="등록">
						<input type="reset" value="취소" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
	 </form>
</section><!-- member_write -->
<script type="text/javascript">
<!--
	function human_email_write_submit(f){
		<?php echo get_editor_js('clt_human_email_text'); ?>
	}
//-->
</script>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>