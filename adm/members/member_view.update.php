<?
include_once '_common.php'; // 공통
/* _array_update.php 처리 불가능 */


/* PARAMETER CHECK */
$para_list = array('mode','mb_field','mb_field_text', 'mb_class', 'mb_level', 'mb_pass', 'mb_email', 'mb_email_change');
array_push($para_list, 'mb_cash_point', 'cms_cash_point_text', 'cms_cash_point');
array_push($para_list, 'mb_point', 'cms_point_text', 'cms_point');
array_push($para_list, 'mb_provider', 'mb_publisher', 'mb_author', 'mb_marketer');
array_push($para_list, 'mb_stop_date', 'mb_adult');

/* 숫자 PARAMITER 체크 */
$para_num_list = array('mb_level');

/* 빈칸 PARAMITER 허용 */
$null_list = array('mb_cash_point', 'mb_point');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
foreach($para_list as $para_key => $para_val){
	if(in_array($para_val,$para_num_list)){
		if(num_check($_POST[$para_val]) == false && $_POST[$para_val] != '' && $_POST[$para_val] != '0'){
			alert($para_val."값에 숫자가 아닌 값이 들어 있습니다.", $_SERVER['HTTP_REFERER']);
			die;
		}
	}
	${'_'.$para_val} = base_filter($_POST[$para_val]); /* 변수담기 */
}

$row_member = mb_get($_mb_id);
if(count($row_member) < 2){
	alert($_mb_id."는 없는 ID 입니다.", $_SERVER['HTTP_REFERER']);
	die;
}
$row = $row_member;

/* 회원정보 */
$_mb_no = $row['mb_no'];
$_mb_id = $row['mb_id'];
$_mb_idx = $row['mb_idx'];

/* 이메일 검사 */
if(email_check($_mb_email_change) == false && $_mb_field == 'mb_email'){
	alert($_mb_email_change."는 이메일 양식에 어긋납니다.", $_SERVER['HTTP_REFERER']);
	die;
}else if(email_check($_mb_email_change) == true && $_mb_field == 'mb_email'){
	// 이메일 중복검사
	if(mb_is_email($_mb_email_change) == true){
		alert($_mb_email."는 중복된 E-mail 입니다.", $_SERVER['HTTP_REFERER']);
		die;
	}
	$_mb_email = $_mb_email_change;
}

/* 패스워드라면 */
if($_mb_pass == '' && $_mb_field == 'mb_pass'){
	alert("패스워드를 입력해주세요.", $_SERVER['HTTP_REFERER']);
	die;
}

/* 캐쉬포인트 / 포인트 계산 */
$_mb_cash_point = intval($_mb_cash_point) + intval($_cms_cash_point);
$_mb_point = intval($_mb_point) + intval($_cms_point);

$dbtable = "member";
$dbt_primary = "mb_id";
$para_primary = "mb_id";
${'_'.$dbt_primary} = ${'_'.$para_primary};

$dbt_primary2 = "mb_idx";
$para_primary2 = "mb_idx";
${'_'.$dbt_primary2} = ${'_'.$para_primary2};

$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

$db_field_exception = array('mode','mb_field','mb_field_text', 'mb_provider', 'mb_publisher', 'mb_author');

if($_mode == 'mod'){
	$_upload_mod_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		
		$sql_mod = ' UPDATE '.$dbtable.' SET ';
		foreach($para_list as $para_key => $para_val){
			if(in_array($para_val, $db_field_exception)){continue;} /* DB field 아닌 목록 제외 */
			if(${'_'.$para_val} == '' && in_array($para_val, $null_list) == false){ continue; } /* DB field에 값 없는거 빼기 */
			if($para_val == $dbt_primary || $para_val == $dbt_primary2){ continue; } /* where문 빼기 */

			/* 선택한것만 업데이트 하기 */			
			if($para_val != $mb_field){continue;} /* mb_field 아닌 목록 제외 */

			/* sql문구 */
			if($_mb_field == 'mb_pass' && $para_val == 'mb_pass'){
				echo "a1";
				$sql_mod.= $para_val." = "."password('".${'_'.$para_val}."'), customer_password='n', ";
			} else if($_mb_field == 'mb_level' && $para_val == 'mb_level') {
				/* 회원 등급에 따라 회원 분류변경 */
				if($_mb_level >= $nm_config['cf_admin_level']) {
					$_mb_class = "a";
				} else if($_mb_level >= $nm_config['cf_partner_level'] && $_mb_level < $nm_config['cf_admin_level']) {
					$_mb_class = "p";
				} else {
					$_mb_class = "c";
				} // end else

				$sql_mod.= $para_val." = '".${'_'.$para_val}."', ";
				$sql_mod.= "mb_class = '".$_mb_class."', ";

				switch($_mb_level) {
					case 13 :
						$sql_mod.= "mb_class_no = '".$_mb_marketer."', ";
						break;
					case 14 :
						$sql_mod.= "mb_class_no = '".$_mb_provider."', ";
						break;
					case 15 : 
						$sql_mod.= "mb_class_no = '".$_mb_author."', ";
						break;
					case 16 : 
						$sql_mod.= "mb_class_no = '".$_mb_publisher."', ";
						break;
					default :
						$sql_mod.= "mb_class_no = '0', ";
						break;
				} // end switch
			} else {
				$sql_mod.= $para_val." = '".${'_'.$para_val}."', ";
			}
		}
		
		$sql_mod = substr($sql_mod,0,strrpos($sql_mod, ","));
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'" ." AND ".$dbt_primary2."='".${'_'.$dbt_primary2}."'";

		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $_mb_id.'님의 '.$_mb_field_text.'의 데이터가 수정되었습니다.';
			// 포인트일때 따로 처리
			if($mb_field == 'mb_cash_point' || $mb_field == 'mb_point'){
				if($mb_field == 'mb_cash_point'){
					$_point_from = $_cms_cash_point_text;
				}else{
					$_point_from = $_cms_point_text;
				}
				
				mb_set_point_income($row_member, $mb_field, $_cms_cash_point, $_cms_point, $_point_from, '4', 'y');
				// mysql_query("INSERT INTO point_income (in_id,in_point,in_cash,in_date,in_from,in_free_date,in_balance,in_cash_balance) VALUES ('$_id','$point','0','$today','$memo','$today2','$point_balance','$cash_balance')");
			}
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

	}

/* 중지 & 해제 */
} else if($_mode == "block") {
	if($mb_stop_date != '') {
		$sql_block = "update member set mb_stop_date = '', mb_state_sub = 'y'";
		$sql_block .= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'" ." AND ".$dbt_primary2."='".${'_'.$dbt_primary2}."'";
		$db_result['msg'] = $_mb_id.'님의 계정이 활성화 되었습니다.';
	} else {
		$block_date = NM_TIME_YMDHIS;
		$sql_block = "update member set mb_stop_date = '".$block_date."', mb_state_sub = 's'";
		$sql_block .= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'" ." AND ".$dbt_primary2."='".${'_'.$dbt_primary2}."'";
		$db_result['msg'] = $_mb_id.'님의 계정이 중지 되었습니다.';
	} // end else

	/* DB 저장 */
	if(!sql_query($sql_block)) {	
		$db_result['state'] = 1;
		$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_block;
	} // end else
} // end else if
alert($db_result['msg'], $_SERVER['HTTP_REFERER'], $db_result['error']);
die;
?>