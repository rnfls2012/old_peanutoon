<?
include_once '_common.php'; // 공통

$row_member = mb_get($_mb_id);
if(count($row_member) < 2){
	alert($_mb_id."는 없는 ID 입니다.", $_SERVER['HTTP_REFERER']);
	die;
}
$row = $row_member;

$member_buy_comics_where = "AND mbc.mbc_member='".$row_member['mb_no']."' 
					AND mbc.mbc_member_idx='".$row_member['mb_idx']."'";
$member_buy_comics_total_sql = "select count(*) as mbc_total from member_buy_comics where 1 $member_buy_comics_where ";
$member_buy_comics_total = sql_count($member_buy_comics_total_sql, 'mbc_total');


// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "mbc_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$member_buy_comics_order = "order by mbc.".$_order_field." ".$_order;

$sql_member_buy = "select * from member_buy_comics mbc 
					left JOIN comics c ON mbc.mbc_comics = c.cm_no
					where 1 $member_buy_comics_where $member_buy_comics_order";
$result = sql_query($sql_member_buy);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','mbc_no',1));
array_push($fetch_row, array('이미지','mbc_img',0));
array_push($fetch_row, array('제목','cm_series',1));
array_push($fetch_row, array('분류','cm_big',1));
array_push($fetch_row, array('소장타입','mbc_own_type',1));
array_push($fetch_row, array('최근 본 화','mbc_recent_chapter',1));
array_push($fetch_row, array('날짜','mbc_date',1));

/* 페이징 */
if($_s_limit == ''){ $_s_limit = 6; }

$mode_text = "코믹스 열람 내역";

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

$cms_head_title = $row_member['mb_id'];

$head_title = "엠짱-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더


include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
	<div class="clear write left">
		<ul class="cms_btn_list">		
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view.php?mb_id=<?=$row_member['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);">회원 상세 내역</button></li> <!-- 1030,800 -->		
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view_point.php?mb_id=<?=$row_member['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);"><?=$nm_config['cf_point_unit_ko']?>&<?=$nm_config['cf_cash_point_unit_ko']?> 내역</button></li> <!-- 1030,800 -->
			<li><button onclick="#">코믹스 열람 내역</button></li>
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view_login.php?mb_id=<?=$row_member['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);"><?=$big_val?>로그 내역</button></li> <!-- 1030,800 -->
		</ul>
	</div>
</section> <!-- cms_page_title -->


<section id="member_view_comics">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead>
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){

					/* 이미지 표지 */
					$cm_cover_sub_url = img_url_para($dvalue_val['cm_cover_sub'], $dvalue_val['mbc_date'], '', 'cm_cover_sub', 'tn75x75');

					/* 등록일 */
					$db_mbc_date_ymd = get_ymd($dvalue_val['mbc_date']);
					$db_mbc_date_his = get_his($dvalue_val['mbc_date']);
					$url_member_view_episode = $_cms_folder."/member_view_episode.php?".$_nm_paras."&comics=".$dvalue_val['cm_no']."&big=".$dvalue_val['cm_big']."&mb_comics_page=".$_page;
				?>
				<tr>
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['cm_no'];?></td>
					<td class="<?=$fetch_row[1][1]?>"><img src="<?=$cm_cover_sub_url;?>" alt="<?= $dvalue_val['cm_series'];?>표지" width="75" height="75" /></td>
					<td class="<?=$fetch_row[2][1]?>"><a href="<?=$url_member_view_episode;?>"><?=$dvalue_val['cm_series'];?></a></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$nm_config['cf_small'][$dvalue_val['cm_small']];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$d_own_type[$dvalue_val['mbc_own_type']];?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$dvalue_val['mbc_recent_chapter'];?><?=$d_cm_up_kind[$dvalue_val['cm_up_kind']]?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$db_mbc_date_ymd;?><br/><?=$db_mbc_date_his;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- cms_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>