<?
include_once '_common.php'; // 공통
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMETER CHECK */
array_push($para_list, 'clt_no', 'mode', 'clt_human_email_title', 'clt_human_email_text');
// $para_list = array('clt_human_email_title', 'clt_human_email_text');

/* 빈칸 PARAMITER 시 NULL 처리 */
array_push($null_list, 'clt_human_email_title', 'clt_human_email_text');

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMETER 숫자 검사하면서 $_PARAMETER로 값 대입 */
para_checked();

$dbtable = "config_long_text";
$dbt_primary = "clt_no";
$para_primary = "clt_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

/* 수정 */
if($_mode == 'mod'){
	
	$_er_mod_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
		/* DB 저장 */
		if(sql_query(stripslashes($sql_mod))){
			$db_result['msg'] = '휴면 이메일 내용이 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}
	}
/* 예외 */
} else {
	echo "mode를 다시 확인해주시기 바랍니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val) {
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/

pop_close($db_result['msg'], NM_ADM_URL.'/members/oneyear.php', $db_result['error']);
die;

?>