<?
include_once '_common.php'; // 공통

/* PARAMITER */
if ( isset($_mb_no) ) {
    $row = mb_get_no($_mb_no);
} elseif ( isset($_mb_id) ) {
    $row = mb_get($_mb_id);
    if ( $row === false ) {
        $row = mb_get($_mb_id, 'member', '*', 'n');
    }
} else {
    alert("유효하지 않은 회원 입니다.");
}

/* 회원 상태 (y,n), 상태 변환 날짜 */
$mb_state = null;
$date = null;

if ( $row['mb_state'] === 'y' ) {
    switch ( $row['mb_state_sub'] ) {
        case 'y' :
            $mb_state = '사용 중';
            break;
        case 's' :
            $mb_state = '정지';
            $date = $row['mb_stop_date'];
            break;
        case 'h' :
            $mb_state = '휴면';
            $date = $row['mb_human_date'];
            break;
        case 't' :
            $mb_state = '휴면대기';
            break;
    }
} else {
    $mb_state = '탈퇴';
    $date = $row['mb_out_date'];
}

/* 연락처 */
$_mb_phone = mb_get_phone($row['mb_phone']);

$mode_text = "상세 정보";

/* 파트너  */
$table_arr = array("comics_provider", "comics_publisher", "comics_professional", "marketer"); // 테이블 명 배열
$cp_arr = $publisher_arr = $author_arr = $marketer_arr = array();
for($i=0; $i<4; $i++) {
	if($i != 3) {
		$order_field = "cp_name";
	} else {
		$order_field = "mkt_title";
	} // end else
	
	$class_sql = "select * from ".$table_arr[$i]." order by ".$order_field." asc";
	$class_result = sql_query($class_sql);

	switch($i) {
		case 0 :
			while($class_row = sql_fetch_array($class_result)) {
				array_push($cp_arr, $class_row); // 제공사
			} // end while
			break;

		case 1 :
			while($class_row = sql_fetch_array($class_result)) {
				array_push($publisher_arr, $class_row); // 출판사
			} // end while
			break;

		case 2 :
			while($class_row = sql_fetch_array($class_result)) {
				array_push($author_arr, $class_row); // 작가
			} // end while
			break;
			
		case 3 :
			while($class_row = sql_fetch_array($class_result)) {
				array_push($marketer_arr, $class_row); // 마케터
			} // end while
			break;
	} // end switch
} // end for

/* 충전 횟수 */
$recharge_sql = "select count(*) as recharge_total from member_point_income where mpi_member='".$row['mb_no']."' and mpi_member_idx='".$row['mb_idx']."' and mpi_state='1'";
$recharge_total = sql_count($recharge_sql, 'recharge_total');

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

$cms_head_title = $row['mb_id'];

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더

/* 마스터&운영자 */
$mb_change_url = "";
if(cs_is_admin() || $nm_member['mb_level'] == '25'){
	$mb_change_url = '[<a href="'.$_cms_folder.'/member_change.php?mb_id='.$row['mb_id'].'">'.$row['mb_id'].'로그인</a>]';
}

?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?> <?=$mb_change_url;?></h1>
	<div class="clear write left">
		<ul class="cms_btn_list">
			<li><button onclick="#">회원 상세 내역</button></li>  
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view_point.php?mb_id=<?=$row['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);"><?=$nm_config['cf_point_unit_ko']?>&<?=$nm_config['cf_cash_point_unit_ko']?> 내역</button></li> <!-- 1030,800 -->
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view_comics.php?mb_id=<?=$row['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);">코믹스 열람 내역</button></li>  
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view_login.php?mb_id=<?=$row['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);"><?=$big_val?>로그 내역</button></li> <!-- 1030,800 -->
		</ul>
	</div>
</section> <!-- cms_page_title -->

<section id="member_view_info">
	<form name="member_view_form" id="member_view_form" enctype="multipart/form-data" method="post" action="<?=$_cms_folder;?>/member_view.update.php" onsubmit="return member_view_submit();">
		<input type="hidden" id="mode" name="mode" value="mod">
		<input type="hidden" id="mb_field" name="mb_field" value="">
		<input type="hidden" id="mb_field_text" name="mb_field_text" value="">
		<input type="hidden" id="cp_no" name="cp_no" value="<?=$row['mb_class_no']?>">
		<input type="hidden" id="mb_stop_date" name="mb_stop_date" value="<?=$row['mb_stop_date']?>">
		<table>
			<tbody>
				<tr>
					<th><label for="mb_no">회원 번호</label></th>
					<td><input type="hidden" id="mb_no" name="mb_no" value="<?=$row['mb_no'];?>"><?=$row['mb_no'];?></td>

					<th><label for="mb_class">회원 분류</label></th>
					<td class="field_submit field_select">
						<input type="hidden" id="mb_class" name="mb_class" value="<?=$row['mb_class'];?>">
						<?=$d_mb_class[$row['mb_class']];?>
						
					</td>
				</tr>
				<tr>
					<th><label for="mb_id">ID</label></th>
					<td><input type="hidden" id="mb_id" name="mb_id" value="<?=$row['mb_id'];?>"><?=$row['mb_id'];?></td>

					<th><label for="mb_level">회원 등급</label></th>
					<td class="field_submit field_select">
						<div>
							<span id="mb_level_view"><?=$nm_config['cf_level_list'][$row['mb_level']];?></span>
							<? tag_selects(mb_only($nm_config['cf_level_list']), "mb_level", $row['mb_level'], 'n'); ?>
							<a class="change" href="#" onclick="a_click_false();">변경</a>
							<a class="modify" href="#" data-field="mb_level" onclick="a_click_false();">적용</a>
							<a class="cancel" href="#" onclick="a_click_false();">취소</a>
						</div>
						<div class="mb_provider_list cp_list"  id="mb_provider">
							<select name="mb_provider">
							<? foreach($cp_arr as $key => $val) { ?>
								<option value="<?=$cp_arr[$key]['cp_no'];?>"><?=$cp_arr[$key]['cp_name'];?></option>
							<? } //end foreach ?>
							</select>
						</div>
						<div class="mb_publisher_list cp_list" id="mb_publisher">
							<select name="mb_publisher">
							<? foreach($publisher_arr as $key => $val) { ?>
								<option value="<?=$publisher_arr[$key]['cp_no'];?>"><?=$publisher_arr[$key]['cp_name'];?></option>
							<? } //end foreach ?>
							</select>
						</div>
						<div class="mb_author_list cp_list" id="mb_author">
							<select name="mb_author">
							<? foreach($author_arr as $key => $val) { ?>
								<option value="<?=$author_arr[$key]['cp_no'];?>"><?=$author_arr[$key]['cp_name'];?></option>
							<? } //end foreach ?>
							</select>
						</div>
						<div class="mb_marketer_list cp_list" id="mb_marketer">
							<select name="mb_marketer">
							<? foreach($marketer_arr as $key => $val) { ?>
								<option value="<?=$marketer_arr[$key]['mkt_no'];?>"><?=$marketer_arr[$key]['mkt_title'];?></option>
							<? } //end foreach ?>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<th>성명</th>
					<td><?=$row['mb_name'];?></td>

					<th><label for="mb_pass">비밀번호 임시변경</label></th>
					<td class="field_submit field_text">
						<input type="password" id="mb_pass" name="mb_pass" placeholder="변경 할 비밀번호 입력하세요" value="" />
						<a class="change" href="#" data-field="mb_pass" onclick="a_click_false();">변경</a>
					</td>
				</tr>
				<tr>
					<th><label for="mb_adult">성별&인증여부</label></th>
					<td class="field_submit field_select">
						<?=$d_mb_sex[$row['mb_sex']];?> & 
						<span id="mb_adult_view">
						<? if($row['mb_adult'] == 'y' && $row['mb_sex'] == 'n') {
								echo "관리자로 인한 성인인증";
							} else {
								echo $d_adult[$row['mb_adult']];	
							} // end else ?>
						</span>
						<? tag_selects($d_adult, "mb_adult", $row['mb_adult'], 'n'); ?>
						<a class="change" href="#" data-field="mb_adult" onclick="a_click_false();">변경</a>
						<a class="modify" href="#" data-field="mb_adult" onclick="a_click_false();">적용</a>
						<a class="cancel" href="#" onclick="a_click_false();">취소</a>
					</td>

					<th><label for="mb_email">E-MAIL</label></th>
					<td class="field_submit field_text">
						<input type="hidden" id="mb_email" name="mb_email" placeholder="변경 할 E-MAIL 입력하세요" value="<?=$row['mb_email'];?>" />
						<input type="text" id="mb_email_change" name="mb_email_change" value="<?=$row['mb_email'];?>">
						<a class="change" href="#" data-field="mb_email" data-url="<?=$_cms_folder;?>/ajax.mb_overlap.php" onclick="a_click_false();">변경</a>
					</td>
				</tr>
				<tr>
					<th><label for="mb_birth">생년월일</label></th>
					<td><input type="hidden" id="mb_birth" name="mb_birth" value="<?=$row['mb_birth'];?>"><?=$row['mb_birth'];?></td>

					<th><label for="mb_cash_point"><?=$nm_config['cf_cash_point_unit_ko']?></th>
					<td class="field_submit field_point">
						<input type="hidden" id="mb_cash_point" name="mb_cash_point" value="<?=$row['mb_cash_point'];?>">
						<?=cash_point_view($row['mb_cash_point'], 'y');?>
						<a class="change" href="#" onclick="a_click_false();"><?=$nm_config['cf_cash_point_unit_ko']?> 관리</a>
						<a class="modify" href="#" data-field="mb_cash_point" onclick="a_click_false();">적용</a>
						<a class="cancel" href="#" onclick="a_click_false();">취소</a>						
						<p>
							<input type="text" placeholder="가감 사유 입력하세요" id="cms_cash_point_text" name="cms_cash_point_text" />
							<input type="text" class="onlynumber" id="cms_cash_point" name="cms_cash_point" placeholder="가감 <?=$nm_config['cf_cash_point_unit_ko']?>" /> 
							<label for="cms_cash_point"><?=$nm_config['cf_cash_point_unit']?></label>
						</p>
					</td>
				</tr>
				<tr>
					<th>휴대폰 번호</th>
					<td><?=$_mb_phone;?></td>

					<th><label for="mb_point"><?=$nm_config['cf_point_unit_ko']?></th>
					<td class="field_submit field_point">
						<input type="hidden" id="mb_point" name="mb_point" value="<?=$row['mb_point'];?>">
						<?=point_view($row['mb_point'], 'y');?>
						<a class="change" href="#" onclick="a_click_false();"><?=$nm_config['cf_point_unit_ko']?> 관리</a>
						<a class="modify" href="#" data-field="mb_point" onclick="a_click_false();">적용</a>
						<a class="cancel" href="#" onclick="a_click_false();">취소</a>						
						<p>
							<input type="text" placeholder="가감 사유 입력하세요" id="cms_point_text" name="cms_point_text" />
							<input type="text" class="onlynumber" id="cms_point" name="cms_point" placeholder="가감 <?=$nm_config['cf_point_unit_ko']?>" /> 
							<label for="cms_point"><?=$nm_config['cf_point_unit']?></label>
						</p>
					</td>
				</tr>
				<tr>
					<th>회원 가입일</th>
					<td><?=$row['mb_join_date'];?></td>

					<th>충전 총액 / 횟수</th>
					<td><?=number_format($row['mb_won']);?> 원 / <?=$recharge_total;?> 회</td>
				</tr>
				<tr>
					<th>최근 접속일</th>
					<td><?=$row['mb_login_date'];?></td>

					<th><label for="mb_post">뉴스레터 신청여부</label></th>
					<td><?=$d_mb_post[$row['mb_post']];?></td>
				</tr>
				<tr>
					<th>회원 상태</th>
					<td><?=$mb_state;?></td>

					<th>상태 변환 날짜</th>
					<td><?=$date?></td>
				</tr>
				<tr>
					<td colspan="4" class="btn">
						<?php
            if ( $row['mb_state'] !== 'n' ) {
            ?>
						<input type="button" onclick="member_block()" value="<?=($row['mb_stop_date'] != '')?'중지 해제':'사용 중지'?>"/>
            <? } ?>
						<input type="reset" value="닫기" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section> <!-- member_view_body -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>