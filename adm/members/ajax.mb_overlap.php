<?
include_once '_common.php'; // 공통


$para_list = array('mode','chk_val');

foreach($para_list as $para_key => $para_val){
	${'_'.$para_val} = base_filter($_REQUEST[$para_val]); /* 변수담기 */
}

$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

if($_chk_val == '') {
	$db_result['state'] = 2;
	$db_result['msg'] = "ID를 입력해주세요.";
	$db_result['error'] = 'ID 빈 값';
} else if($_mode == "is_id" && email_check($_chk_val) == false) {
	$db_result['state'] = 2;
	$db_result['msg'] = $_chk_val."는 잘못된 ID 입니다.";
	$db_result['error'] = 'ID 형식 오류';
} else {
	if($_mode == "is_id" && $_chk_val != ''){
		if(mb_is_email($_chk_val) == true || mb_is($_chk_val) == true) {
			$db_result['state'] = 1;
			$db_result['msg'] = $_chk_val."는 중복된 ID 입니다.";
			$db_result['error'] = 'ID 중복';
		}else{
			$db_result['msg'] = $_chk_val."는 사용가능합니다.";
			$db_result['error'] = '';
		}
	}else{
		$db_result['state'] = 2;
		$db_result['msg'] = '알맞는 변수값을 넣어주세요';
		$db_result['error'] = '변수에러';
	}
}

header( "content-type: application/json; charset=utf-8" );
echo json_encode($db_result);
die;
?>