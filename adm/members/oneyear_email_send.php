<?
include_once '_common.php'; // 공통

/* DB */
$sql = "select * from config_long_text";
$row = sql_fetch($sql);

/* PARAMITER */

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "휴면 안내 이메일";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> 전송</h1>
</section><!-- cms_page_title -->

<section id="member_write" class="editor_html">
	<form name="human_email_send_form" id="human_email_send_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return human_email_send_submit();">
		<input type="hidden" id="clt_human_email_title" name="clt_human_email_title" value="<?=$row['clt_human_email_title'];?>"/> 
		<input type="hidden" id="clt_human_email_text" name="clt_human_email_text" value="<?=$row['clt_human_email_text'];?>"/>
		<table class="email_write">
			<tbody>
				<tr>
					<th><label for="clt_human_email_title"><?=$required_arr[0]?>제목</label></th>
					<td>
						<?=$row['clt_human_email_title'];?>
					</td>
				</tr>
				<tr>
					<th><label for="clt_human_email_text"><?=$required_arr[0]?>내용</label></th>
					<td>
						<?=nl2br($row['clt_human_email_text']);?>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="submit_btn">
						<input type="submit" value="전송">
						<input type="reset" value="취소" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
	 </form>
</section><!-- member_write -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>