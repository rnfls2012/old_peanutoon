<? include_once '_common.php'; // 공통
// /adm/_page.php 에서 사용하는 변수 보기

/* PARAMETER
	member_level // 대분류(회원 레벨)
	s_type // 검색 타입 -> 1. 전체, 2. 아이디, 3. 이름, 4. 이메일
	s_text // 검색 단어
	order_field // 정렬 필드
	date_type // 날짜 타입
	s_date // 날짜-시작
	e_date // 날짜-끝
	s_limit // 보여줄 갯수
*/

// 회원 레벨
$nm_paras_check = array('mb_level');
foreach($nm_paras_check as $nm_paras_key => $nm_paras_val){
	if(${"_".$nm_paras_val} && ${"_".$nm_paras_val} > 0){
		$sql_where.= "and ".$nm_paras_val." = ".${"_".$nm_paras_val}." ";
	}
}

// 회원 등급
if($_mb_class != ""){
	$sql_where.= "and mb_class = '$_mb_class'";
}

// 검색단어+ 검색타입
if($_s_text){
	switch($_s_type){
		case '0': $sql_where.= "and mb_id like '%$_s_text%' ";
		break;
		case '1': $sql_where.= "and mb_name like '%$_s_text%'";
		break;
		case '2': $sql_where.= "and mb_email like '%$_s_text%' ";
		break;
		case '3': $sql_where.= "and mb_no like '%$_s_text%' ";
		break;
		default: $sql_where.= "and ( mb_id like '%$_s_text%' or mb_name like '%$_s_text%' or mb_email like '%$_s_text%' or mb_no like '%$_s_text%' )";
		break;
	} // end switch
} // end if

// 최근 로그인/탈퇴일 날짜
$start_date = $end_date = "";
if($_date_type) { //all, upload_date, new_date
	if($_s_date && $_e_date) {
		$start_date = $_s_date." ".NM_TIME_HI_start;
		$end_date = $_e_date." ".NM_TIME_HI_end;
	} else if($_s_date) {
		$start_date = $_s_date." ".NM_TIME_HI_start;
		$end_date = NM_TIME_YMD." ".NM_TIME_HI_end;
	}
	switch($_date_type){
		case 'mb_login_date' : $sql_where.= "and mb_login_date >= '$start_date' and mb_login_date <= '$end_date'";
		break;
		
		case 'mb_join_date' : $sql_where.= "and mb_join_date >= '$start_date' and mb_join_date <= '$end_date'";
		break;
		
		case 'mb_out_date' : $sql_where.= "and mb_out_date >= '$start_date' and mb_out_date <= '$end_date'";
		break;

		default: $sql_where.= "";
		break;
	}
} else {
	$_s_date = "";
	$_e_date = "";
}

// 그룹
$sql_group = "";

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "mb_login_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$member_login_date_add = '';
if($_order_field == "mb_join_date"){  $member_login_date_add = " , mb_no DESC "; }
$sql_order = "order by ".$_order_field." ".$_order." ".$member_login_date_add ;

// SQL문
$sql_field		= " * ";
$sql_count		= " count(*) as cnt ";

$sql_table		= "select {$sql_field} from member";
$sql_table_cnt	= "select {$sql_count} from member";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_table_cnt} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts	= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값

$total_member = total_member(); // 회원 총수 - function 선언 하단

$th_class = $td_class = "";	// 상단테이블 css-class

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('회원번호', 'mb_no', 1));
array_push($fetch_row, array('아이디', 'mb_id', 1));
array_push($fetch_row, array('이름', 'mb_name', 1));
array_push($fetch_row, array('이메일', 'mb_email', 0));
array_push($fetch_row, array('연락처', 'mb_phone', 0));
array_push($fetch_row, array('가입경로', 'mb_sns_type', 0));
array_push($fetch_row, array('가입일', 'mb_join_date', 1));
array_push($fetch_row, array('최근 접속일', 'mb_login_date', 1));
array_push($fetch_row, array('회원 상태', 'mb_out_date', 1));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'], 'mb_cash_point', 1));
array_push($fetch_row, array($nm_config['cf_point_unit_ko'], 'mb_point', 1));

$cs_calendar_on = $cs_submit_h = '';
if($_date_type){
	$cs_calendar_on = 'cs_calendar_on';
	$cs_submit_h = 'cs_submit_on';
}

$page_title = "전체 회원";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$cms_page_title;?></h1>
	<div>
		<strong><?=$page_title;?> : <?=number_format($total_member);?>명</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_folder_write;?>','members_wirte', 560, 550);">회원 등록</button>
	</div>
</section> <!-- member_head -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="member_search_form" id="member_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return member_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_s_type_btn">
				<?	$s_type_list = array('아이디','이름','이메일','회원번호');
					tag_radios($s_type_list, "s_type", $_s_type, 'y', '전체', ''); ?>
				</div>
				<div class="cs_search_btn">
					<?	tag_selects($d_mb_class, "mb_class", $_mb_class, 'y', '회원분류', ''); ?>
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
				<div class="cs_search_btn">
					<?	mb_selects(mb_only($nm_config['cf_level_list']), "mb_level", $_mb_level, 'y', '회원등급 전체', ''); ?>
					<?	tag_selects($d_mb_date_type, "date_type", $_date_type, 'y', '날짜 검색안함', ''); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit member_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="72" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="member_result">
	<h3 id="cr_thead_mg_add">검색 리스트
		<strong>검색 결과 수 : <?=number_format($rows_cnts);?>명</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){ $th_title_giho = $order_giho; }
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){ 
					
					/* 연락처 */
					$mb_phone = mb_get_phone($dvalue_val['mb_phone']);

					/* 가입일 */
					$db_mb_join_date_ymd = get_ymd($dvalue_val['mb_join_date']);
					$db_mb_join_date_his = get_his($dvalue_val['mb_join_date']);

					/* 최근 접속일 */
					$db_mb_login_date_ymd = get_ymd($dvalue_val['mb_login_date']);
					$db_mb_login_date_his = get_his($dvalue_val['mb_login_date']);

					/* 탈퇴일 */
					$db_mb_out_date_ymd = get_ymd($dvalue_val['mb_out_date']);
					$db_mb_out_date_his = get_his($dvalue_val['mb_out_date']);

					/* 보너스코인 */
					$db_mb_point = point_view($dvalue_val['mb_point'], 'y');

					/* 코인 */
					$db_mb_cash_point = cash_point_view($dvalue_val['mb_cash_point'], 'y');

					/* 회원 상태 (y,n) */
					$mb_state = '';

					if ( $dvalue_val['mb_state'] === 'y' ) {
                        switch ( $dvalue_val['mb_state_sub'] ) {
                            case 'y' :
                                $mb_state = '사용 중';
                                break;
                            case 's' :
                                $mb_state = '정지';
                                break;
                            case 'h' :
                                $mb_state = '휴면';
                                break;
                            case 't' :
                                $mb_state = '휴면대기';
                                break;
                        }
                    } else {
					    $mb_state = '탈퇴';
                    }

					?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['mb_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center">
						<a href="#" onclick="popup('member_view.php?mb_no=<?=$dvalue_val['mb_no'];?>','member_view', <?=$popup_cms_width;?>, 550);"><?=$dvalue_val['mb_id'];?></a>
						<!-- 615,765 -->
					</td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['mb_name'];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['mb_email'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$mb_phone;?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$d_mb_sns_type[$dvalue_val['mb_sns_type']];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$db_mb_join_date_ymd;?><br/><?=$db_mb_join_date_his;?></td>
					<td class="<?=$fetch_row[7][1]?> text_center"><?=$db_mb_login_date_ymd;?><br/><?=$db_mb_login_date_his;?></td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=$mb_state?></td>
					<td class="<?=$fetch_row[9][1]?> text_center"><?=$db_mb_cash_point;?></td>
					<td class="<?=$fetch_row[10][1]?> text_center"><?=$db_mb_point;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>
<? rows_page(); ?>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>

<?	/* 해당 페이지에서 사용하는 function */
// 회원 총수
function total_member()
{
	$sql_member_total = "select count(*) as total_member from member ";
	$total_member = sql_count($sql_member_total, 'total_member');
	return $total_member; 
}


?>