<?
include_once '_common.php'; // 공통

/* 회원등록 */
$_mode='reg'; 
$mode_text = "등록";

/* 파트너  */
$table_arr = array("comics_provider", "comics_publisher", "comics_professional"); // 테이블 명 배열
$cp_arr = $publisher_arr = $author_arr = array();
for($i=0; $i<3; $i++) {
	$class_sql = "select * from ".$table_arr[$i]." order by cp_name";
	$class_result = sql_query($class_sql);

	switch($i) {
		case 0 :
			while($class_row = sql_fetch_array($class_result)) {
				array_push($cp_arr, $class_row); // 제공사
			} // end while
			break;

		case 1 :
			while($class_row = sql_fetch_array($class_result)) {
				array_push($publisher_arr, $class_row); // 출판사
			} // end while
			break;

		case 2 :
			while($class_row = sql_fetch_array($class_result)) {
				array_push($author_arr, $class_row); // 작가
			} // end while
			break;
	} // end switch
} // end for

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

$cms_head_title = "회원";

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- cms_page_title -->

<section id="member_write">
	<form name="members_write_form" id="members_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_folder;?>/members.update.php" onsubmit="return members_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/> <!-- 입력모드 -->
		<table>
			<tbody>
				<tr>
					<th><label for="mb_id"><?=$required_arr[0]?>아이디</label></th>
					<td>
						<input type="text" placeholder="아이디를 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="mb_id" id="mb_id" autocomplete="off" value="" />
						<input type="hidden" id="is_id" name="is_id" value=""/>
						<span class="info">이메일 형식으로 입력해주세요. ex)test@test.com</span>
						<a class="is_btn" id="is_id_btn" href="#is_id" onclick="a_click_false();" data-mode="id" data-url="<?=$_cms_folder;?>/ajax.mb_overlap.php">아이디 중복체크</a>
						<span class="is_text" id="is_id_text">아이디 중복체크를 해야합니다.</span>
					</td>
				</tr>
				<tr>
					<th><label for="mb_pass"><?=$required_arr[0]?>비밀번호</label></th>
					<td><input type="password" placeholder="비밀번호를 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="mb_pass" id="mb_pass" autocomplete="off" value="" /></td>
				</tr>
				<tr>
					<th><label for="mb_name"><?=$required_arr[0]?>이름</label></th>
					<td><input type="text" placeholder="이름을 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="mb_name" id="mb_name" autocomplete="off" value="" /></td>
				</tr>
				<tr>
					<th><label for="mb_level"><?=$required_arr[0]?>회원 등급</label></th>
					<td>
						<?	tag_selects(mb_only($nm_config['cf_level_list']), "mb_level", $_mb_level, 'n'); ?>
						<div class="mb_provider_list cp_list"  id="mb_provider">
							<select name="mb_provider">
							<? foreach($cp_arr as $key => $val) { ?>
								<option value="<?=$cp_arr[$key]['cp_no'];?>"><?=$cp_arr[$key]['cp_name'];?></option>
							<? } //end foreach ?>
							</select>
						</div>
						<div class="mb_publisher_list cp_list" id="mb_publisher">
							<select name="mb_publisher">
							<? foreach($publisher_arr as $key => $val) { ?>
								<option value="<?=$publisher_arr[$key]['cp_no'];?>"><?=$publisher_arr[$key]['cp_name'];?></option>
							<? } //end foreach ?>
							</select>
						</div>
						<div class="mb_author_list cp_list" id="mb_author">
							<select name="mb_author">
							<? foreach($author_arr as $key => $val) { ?>
								<option value="<?=$author_arr[$key]['cp_no'];?>"><?=$author_arr[$key]['cp_name'];?></option>
							<? } //end foreach ?>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="mb_adult"><?=$required_arr[0]?>성인</label></th>
					<td>
					<?	tag_selects($d_adult, "mb_adult", $_mb_adult, 'n'); ?>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="submit_btn">
						<input type="submit" value="<?=$mode_text?>">
						<input type="reset" value="취소" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
	 </form>
</section><!-- mb_write -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>