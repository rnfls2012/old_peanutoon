<?
include_once '_common.php'; // 공통

$row_member = mb_get($_mb_id);
if(count($row_member) < 2){
	alert($_mb_id."는 없는 ID 입니다.", $_SERVER['HTTP_REFERER']);
	die;
}
$row = $row_member;

$member_buy_episode_where = "AND mbe.mbe_member='".$row_member['mb_no']."' 
					AND mbe.mbe_member_idx='".$row_member['mb_idx']."' 
					AND `mbe_comics` = '".$_comics."' ";

$member_buy_episode_total_sql = "select count(*) as mbe_total from member_buy_episode_".$_big." where 1 $member_buy_episode_where ";
$member_buy_episode_total = sql_count($member_buy_episode_total_sql, 'mbe_total');


// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "mbe_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$member_buy_episode_order = "order by mbe.".$_order_field." ".$_order;

$sql_member_buy = "select * from member_buy_episode_".$_big." mbe 
					left JOIN comics c ON mbe.mbe_comics = c.cm_no
					where 1 $member_buy_episode_where $member_buy_episode_order";
$result = sql_query($sql_member_buy);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','mbe_no',1));
array_push($fetch_row, array('이미지','mbe_img',0));
array_push($fetch_row, array('제목','cm_series',0));
array_push($fetch_row, array('화번호','mbe_chapter',1));
array_push($fetch_row, array('소장타입<br/>구매방법','mbe_own_type',0));
array_push($fetch_row, array('보유유효일','mbe_end_date',1));
array_push($fetch_row, array('구매일','mbe_date',1));

/* 페이징 */
if($_s_limit == ''){ $_s_limit = 6; }

$mode_text = "에피소드 열람 내역";

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

$cms_head_title = $row_member['mb_id'];

$head_title = "엠짱-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더


include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
	<div class="clear write left">
		<ul class="cms_btn_list">		
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view.php?mb_id=<?=$row_member['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);">회원 상세 내역</button></li> <!-- 1030,800 -->		
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view_point.php?mb_id=<?=$row_member['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);"><?=$nm_config['cf_point_unit_ko']?>&<?=$nm_config['cf_cash_point_unit_ko']?> 내역</button></li> <!-- 1030,800 -->
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view_comics.php?<?=$_nm_paras;?>&page=<?=$_mb_comics_page;?>','member_view', <?=$popup_cms_width;?>, 900);">코믹스 열람 내역</button></li>
			<li><button onclick="popup('<?=$_cms_folder;?>/member_view_login.php?mb_id=<?=$row_member['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 900);"><?=$big_val?>로그 내역</button></li> <!-- 1030,800 -->
		</ul>
	</div>
</section> <!-- cms_page_title -->


<section id="member_view_episode">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead>
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){

					/* 이미지 표지 */
					$cm_cover_sub_url = NM_IMG.$dvalue_val['cm_cover_sub'];

					/* 등록일 */
					$db_mbe_date_ymd = get_ymd($dvalue_val['mbe_date']);
					$db_mbe_date_his = get_his($dvalue_val['mbe_date']);

					/* 보유유효일 */
					$db_mbe_end_date = $dvalue_val['mbe_end_date'];
					if($dvalue_val['mbe_end_date'] == '3000-01-01'){ $db_mbe_end_date = '무기간'; }
						
				?>
				<tr>
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['mbe_no'];?></td>
					<td class="<?=$fetch_row[1][1]?>"><img src="<?=$cm_cover_sub_url;?>" width="75" height="75" alt="<?= $dvalue_val['cm_series'];?>표지"/></td>
					<td class="<?=$fetch_row[2][1]?>"><?=$dvalue_val['cm_series'];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['mbe_chapter'];?><?=$d_cm_up_kind[$dvalue_val['cm_up_kind']]?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$d_own_type[$dvalue_val['mbe_own_type']];?><br/><?=$dvalue_val['mbe_recent_chapter'];?><?=$d_mbe_way[$dvalue_val['mbe_way']]?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$db_mbe_end_date;?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$db_mbe_date_ymd;?><br/><?=$db_mbe_date_his;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- cms_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>