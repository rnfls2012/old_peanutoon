<?
	
$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// 해당 시트 당 컬럼 크기 변경
$worksheet->set_column(0, 1, 15);
$worksheet->set_column(1, 1, 25);
$worksheet->set_column(2, 5, 20);

// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));
								
$center  = $workbook -> addformat(array(
								'align' => 'center',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));
								
/**************  worksheet  *****************/

//문서 출력일자 출력
$worksheet->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt = 3; //합계 데이터 출력 행 cnt.

/***************  본문 출력   **************/

//worksheet 내에 검색 필터링 항목
$filter_type = $filter_cont = array();
array_push($filter_type, "플랫폼");
array_push($filter_type, "검색어");
array_push($filter_type, "마케팅 업체");
array_push($filter_type, "결제 / 미결제");

//필터링에 들어갈 내용
array_push($filter_cont, $nm_config['cf_title']);
array_push($filter_cont, $s_text);
array_push($filter_cont, $marketers_arr[$mb_mkt]);
array_push($filter_cont, $d_mb_won[$mb_won]);

if ($s_date || $e_date) {
	array_push($filter_type, "검색 날짜");
	array_push($filter_cont, $s_date."~".$e_date);
}

$filter_cnt = count($filter_type);

//필터링 배열 이용
for($i=0; $i < $filter_cnt; $i++){
	$worksheet->write($row_cnt, 0, iconv_cp949($filter_type[$i]), $heading);
	$worksheet->write($row_cnt, 1, iconv_cp949($filter_cont[$i]), $right);
	$row_cnt++;
}

$row_cnt++;

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
}

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
}

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write($row_cnt, $col++, $cell, $heading);
}

$row_cnt++; //필터 지정 후 다음 행 부터 데이터 출력.

/* 데이터 전달 부분 */
for ($i=$row_cnt; $row=mysql_fetch_array($result); $i++) {
	foreach ($data_key as $cell_key => $cell_val) {

		//날짜 및 마케팅이름 표시 (가독성 위해 switch 문)
		switch($cell_val) {
			case "mb_no" :
				$mb_no = $row['mb_no'];
				$worksheet->write($i, $cell_key , iconv_cp949($mb_no), $center);
				break;
				
			case "mb_id" :
				$mb_id = $row['mb_id']; // ID에 번호만 있을 경우 				
				$worksheet->write($i, $cell_key , iconv_cp949($mb_id));				
				break;

			case "mb_join_date" :
				$mb_join_date = $row['mb_join_date'];
				$worksheet->write($i, $cell_key , iconv_cp949($mb_join_date), $center);
				break;

			case "mb_login_date" :
				$mb_login_date = $row['mb_login_date'];
				$worksheet->write($i, $cell_key , iconv_cp949($mb_login_date), $center);
				break;

			case "mb_mkt" :
				$mb_mkt_val = $array_mkt[$row[$cell_val]]['mkt_title']."(".$row[$cell_val].")"; //array_mkt 배열 이용.
				$worksheet->write($i, $cell_key , iconv_cp949($mb_mkt_val), $right);
				break;

			case "mb_won" :
				$mb_won = $row['mb_won'];
				$total_won += $row[$cell_val]; //충전 합
				$worksheet->write($i, $cell_key , iconv_cp949($mb_won), $f_price);
				break;

			case "mb_won_count" : 
				$mb_won_count = $row['mb_won_count'];
				$total_cnt += $row[$cell_val]; //횟수 합
				$worksheet->write($i, $cell_key , iconv_cp949($mb_won_count), $f_price);
				break;
		}
	}
	$row_cnt++;
}

$row_cnt++;

$worksheet->write($row_cnt, 1, iconv_cp949('검색된 회원 수 : '.mysql_num_rows($result)));
$worksheet->write($row_cnt, 5, iconv_cp949('총 누적금액 : '.$total_won));
$worksheet->write($row_cnt, 6, iconv_cp949('총 충전횟수 : '.$total_cnt));

$workbook->close();

header("Content-Disposition: attachment;filename="."member_mkt_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"member_mkt_".$today.".xls");
header("Content-Disposition: inline; filename=\"member_mkt_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;

?>