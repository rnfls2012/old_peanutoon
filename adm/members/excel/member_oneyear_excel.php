<?
$today = date("Ymd");
$send_date = NM_TIME_YMDHIS;
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once('../../lib/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once('../../lib/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
}

// DB 컬럼용 배열
$data_key = array();
array_push($data_key, 'mb_no');
array_push($data_key, 'mb_email');

/* 컬럼 이름 지정 부분 */
$worksheet->write(0, 0, iconv_cp949('회원 번호'));
$worksheet->write(0, 1, iconv_cp949('이메일'));

/* 데이터 전달 부분 */
for ($i=1; $row=mysql_fetch_array($result); $i++) {
	foreach($data_key as $cell_key => $cell_val) {
		$worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]));

		// 회원 상태 업데이트
		if($cell_val == 'mb_no') {
			$stand_by_sql = "UPDATE member SET mb_state_sub = 't', mb_human_email_date = '".$send_date."' WHERE mb_no = '".$row[$cell_val]."'";
			sql_query($stand_by_sql);
		} // end if
	} // end foreach
} // end for

$workbook->close();

header("Content-Disposition: attachment;filename="."member_oneyear_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"member_oneyear_".$today.".xls");
header("Content-Disposition: inline; filename=\"member_oneyear_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;

?>