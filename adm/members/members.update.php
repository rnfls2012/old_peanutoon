<?
include_once '_common.php'; // 공통
/* _array_update.php 처리 불가능 */

/* PARAMETER CHECK */
$para_list = array('mode', 'mb_no', 'mb_id', 'mb_idx', 'mb_pass', 'mb_name', 'mb_class', 'mb_level', 'mb_adult', 'mb_join_date', );
array_push($para_list, 'mb_provider', 'mb_publisher', 'mb_author');

/* 숫자 PARAMETER 체크 */
$para_num_list = array('mb_level');

/* 이메일 PARAMETER 체크 */
$para_email_list = array('mb_id');

/* 빈칸 PARAMETER 허용 */
$null_list = array(); // 사용 안 함

/* PARAMETER 숫자 검사하면서 $_PARAMETER로 값 대입 */
foreach($para_list as $para_key => $para_val) {
	if(in_array($para_val, $para_num_list)) {
		if(num_check($_POST[$para_val]) == false && $_POST[$para_val] != '' && $_POST[$para_val] != '0') {
			alert($para_val."값에 숫자가 아닌 값이 들어 있습니다.", $_SERVER['HTTP_REFERER']);
			die;
		} // end if
	} // end if

	if(in_array($para_val, $para_email_list)) {
		if(email_check($_POST[$para_val]) == false && $_POST[$para_val] != '' && $_POST[$para_val] != '0') {
			// alert("이메일 양식이 옳지 않습니다.", $_SERVER['HTTP_REFERER']);
			die;
		} // end if
	} // end if
	${'_'.$para_val} = base_filter($_POST[$para_val]); /* 변수담기 */
} // end foreach


$dbtable = "member";
$dbt_primary = "mb_no";
$para_primary = "mb_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

/* DB field 아닌 목록 */
$db_field_exception = array('mode', 'mb_provider', 'mb_publisher', 'mb_author');

if($_mode == "reg") {
	//등록전 id/email검사
	/*
	if(mb_is($_mb_id) == true){
		alert($_mb_id."는 중복된 ID 입니다.", $_SERVER['HTTP_REFERER']);
		die;
	}
*/
	/* 고정값 */
	$_mb_join_date = NM_TIME_YMDHIS; // 가입일 저장용
	$_mb_idx = mb_get_idx($_mb_id); // 아이디 인덱스 저장용
	
	/* field명 */
	$sql_reg = 'INSERT INTO '.$dbtable.'(';
	foreach($para_list as $para_key => $para_val) {
		if(in_array($para_val, $db_field_exception)) { continue; } // DB field 아닌 목록 제외
		if(${"_".$para_val} == "" && in_array($para_val, $null_list) == false) { continue; } // DB field에 값 없는거 빼기

		/* sql 문구 */
		if($para_val == 'mb_id') {
			$sql_reg .= $para_val.", ";
			$sql_reg .= "mb_email, ";
		} else if($para_val == 'mb_level') {
			$sql_reg .= $para_val.", ";
			$sql_reg .= "mb_class, mb_class_no, ";
		} else {
			$sql_reg .= $para_val.", ";
		} // end else
	} // end foreach
	$sql_reg = substr($sql_reg,0,strrpos($sql_reg, ","));

	/* VALUE 시작 */
	$sql_reg .= ") VALUES ( ";

	foreach($para_list as $para_key => $para_val) {
		if(in_array($para_val, $db_field_exception)) { continue; } // DB field 아닌 목록 제외
		if(${"_".$para_val} == "" && in_array($para_val, $null_list) == false) { continue; } // DB field에 값 없는거 빼기

		/* sql 문구 */
		if($para_val == 'mb_id') {
			$sql_reg.= "'".${'_'.$para_val}."', '".${'_'.$para_val}."', ";
		} else if($para_val == 'mb_pass') {
			$sql_reg.= "password('".${'_'.$para_val}."'), ";
		} else if($para_val == 'mb_level') {
			$sql_reg.= "'".${'_'.$para_val}."', ";

			if($_mb_level >= $nm_config['cf_admin_level']) {
					$_mb_class = "a";
				} else if($_mb_level >= $nm_config['cf_partner_level'] && $_mb_level < $nm_config['cf_admin_level']) {
					$_mb_class = "p";
				} else {
					$_mb_class = "c";
				} // end else

			$sql_reg.= "'".$_mb_class."', "; // 클래스 단위 먼저 추가 한 뒤,
			
			// 클래스에 따른 파트사 번호 추가
			switch($_mb_level) {
				case 14 :
					$sql_reg.= "'".$_mb_provider."', ";
					break;
				case 15 : 
					$sql_reg.= "'".$_mb_author."', ";
					break;
				case 16 : 
					$sql_reg.= "'".$_mb_publisher."', ";
					break;
				default :
					$sql_reg.= "'0', ";
					break;
			} // end switch
		} else {
			$sql_reg.= "'".${'_'.$para_val}."', ";
		} // end else 비밀번호는 함수로 암호화 저장
	} // end foreach
	$sql_reg = substr($sql_reg, 0, strrpos($sql_reg, ","));

	/* SQL문 마무리 */
	$sql_reg .= ' ) ';
	
	/* DB 저장 */
	if(sql_query($sql_reg)) {
		$db_result['msg'] = $_mb_id.'의 데이터가 등록되었습니다.';
	} else {
		$db_result['state'] = 1;
		$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	} // end else
} else {
	echo "mode를 다시 확인해주시기 바랍니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
} // end else

pop_close($db_result['msg'], NM_ADM_URL.'/members/everyone.php?v=2&page=1&order_field='.$member_join_date.'&order=desc', $db_result['error']);
die;

?>