<?
include_once '_common.php'; // 공통

/* PARAMETER
	big // 대분류
	cm_small // 장르
	cm_publisher // 출판사번호
	cm_professional // 작가번호
	s_type // 검색 타입 -> 1전체, 2작품명, 3작가, 4출판사
	s_text // 검색 단어
	order_field // 컨텐츠정렬필드
	order // 정렬순서
	date_type // 날짜타입
	s_date // 날짜-시작
	e_date // 날짜-끝
	s_limit // 보여줄 갯수
	s_cm_end // 완결여부
	cm_no_list // 서지정보 리스트 용
	cm_no // 코믹스 번호
*/

/* 쿼리문 */
include_once './bibliography_sql.php';

/* 엑셀 URL Parameter */
$excel_para = "s_type=".$_s_type."&s_text=".$_s_text."&cm_small=".$_cm_small."&cm_end=".$_cm_end."&date_type=".$_date_type."&s_date=".$_s_date."&e_date=".$_e_date."&cm_no_list=".$cm_no_list;

/* 출력 필드 리스트 - array('표제목', '정렬 필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','cm_no',0));
array_push($fetch_row, array('썸네일','cm_cover_sub',0));
array_push($fetch_row, array('작품명','cm_series',0));
array_push($fetch_row, array('출판사','cm_publisher',0));
array_push($fetch_row, array('작가','cm_professional',0));
array_push($fetch_row, array('장르'.'<br>'.'(성인 여부)','cm_small',0));
array_push($fetch_row, array('완결 구분','cm_end',0));
array_push($fetch_row, array('가격','cm_pay',0));
array_push($fetch_row, array('줄거리','cm_somaery',0));
array_push($fetch_row, array('총 권(화)수','cm_episode_total',0));
array_push($fetch_row, array('최초 등록일','cm_reg_date',0));
array_push($fetch_row, array('최신 등록일','cm_episode_date',0));

if($_s_limit == ''){ $_s_limit = 10; }

$cs_calendar_on = $cs_submit_h = '';
if($_date_type){
	$cs_calendar_on = 'cs_calendar_on';
	$cs_submit_h = 'cs_submit_on';
} // end if

$cms_head_title = "서지 정보";
// if($cm_big != ''){ $cms_head_title = $nm_config['cf_big'][$cm_big]; }

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;

include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>서비스 중인 컨텐츠 : <?=number_format($bib_service_total);?> 건</strong>
		<strong> + </strong>
		<strong>중지인 컨텐츠 : <?=number_format($bib_stop_total);?> 건</strong>
		<strong> = </strong>
		<strong>총 컨텐츠 : <?=number_format($bib_service_total+$bib_stop_total);?> 건</strong>
	</div>
<!--
	<div class="write">
		<? /* 상단 페이지 라면 버튼 전부 출력 */
		foreach($nm_config['cf_big'] as $big_key => $big_val){ 
			if($big_key == 0 || ($cm_big != '' && $cm_big != $big_key)){ continue; } ?>
			<button onclick="popup('<?=$_cms_folder_write;?>?big=<?=$big_key?>','book_wirte', 900, 900);"><?=$big_val?> 등록</button>
		<?} /* foreach($nm_config['cf_big'] as $big_key => $big_val){  */ /* if($cm_big != ''){ */ ?>
	</div>
-->
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="cms_search_form" id="cms_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return cms_search_submit();">
		<input type="hidden" name="big" id="big" value="<?=$cm_big?>"> <!-- 대분류 -->
		<input type="hidden" name="cm_no_list" id="cm_no_list" value="<?=$cm_no_list?>"> <!-- 대분류 -->
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_s_type_btn">
				<?	$s_type_list = array('작품명','작가','출판사');
					tag_radios($s_type_list, "s_type", $_s_type, 'y', '전체', ''); ?>
				</div>
				<div class="cs_search_btn">
					<?	tag_selects($nm_config['cf_small'], "cm_small", $_cm_small, 'y', '장르 전체', ''); ?>
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
				<div class="cs_date_btn">
					<?	tag_selects($d_cm_end, "cm_end", $_cm_end, 'y', '완결구분 전체', ''); ?>
					<?	tag_selects($d_cm_date_type, "date_type", $_date_type, 'y', '최초/최신등록일 검색안함', ''); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="72" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="bib_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div class="excel_down">
		<input type="button" class="excel_down" value="검색 결과 엑셀 다운로드" onclick="location.href='../excel/bibliography_excel.php?type=1&<?=$excel_para;?>'"/>
		<input type="button" class="excel_small_down" value="엑셀 다운로드 리스트 보기" />
	</div>
	<div class="excel_small_down_list">
		<? foreach($nm_config['cf_small'] as $key => $val) { 
			if($key == 0) { continue; } // end if ?>
			<input type="button" class="excel_small_down_btn" value="<?=$val;?> 부분 다운로드" onclick="location.href='../excel/bibliography_excel.php?type=2&small=<?=$key;?>'"/>
		<? } // end foreach ?>
	</div>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				} // end if
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 서비스여부 */
					$cm_service = "제공";
					if($dvalue_val['cm_service'] == 'n') { $cm_service = "중지"; }

					/* 완결여부 */
					$cm_end = "완결";
					if($dvalue_val['cm_end'] == 'n') { $cm_end = "연속"; }

					/* 성인여부 */
					$cm_adult = "성인";
					if($dvalue_val['cm_adult'] == 'n') { $cm_adult = "전체"; }
					
					/* 이미지 표지 */
					$cm_cover_sub_url = img_url_para($dvalue_val['cm_cover_sub'], $dvalue_val['cm_reg_date'], $dvalue_val['cm_mod_date'], 'cm_cover_sub', 'tn75x75');

					/* 줄거리 생략 */
					$cm_somaery = $dvalue_val['cm_somaery'];
					if(strlen($cm_somaery) > 30) { $cm_somaery = mb_substr($cm_somaery, 0, 40, 'UTF-8')."...(생략)"; } // end if
					
					/* 컨텐츠 수정/삭제 */

					/* 컨텐츠 화 등록/리스트보기 */
					
					/* 대분류 */
					$row_big_text = '';
					if($th_big_text != ''){ $row_big_text = "<p class='big_list'>".$nm_config['cf_big'][$dvalue_val['big']]."</p>"; }

					/* 등록일 */
					$db_cm_reg_date_ymd = get_ymd($dvalue_val['cm_reg_date']);
					$db_cm_reg_date_his = get_his($dvalue_val['cm_reg_date']);
				?>
				<tr class="result_hover" onclick="location.href='<?=$_cms_self;?>?cm_no=<?=$dvalue_val['cm_no'];?>&cm_no_list=<?=$cm_no_list;?>&page=<?=$page;?>'">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['cm_no'];?></td>
					<td class="<?=$fetch_row[1][1]?>"><img src="<?=$cm_cover_sub_url;?>" width="75" height="75" alt="<?= $dvalue_val['cm_series'];?>표지"/></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['cm_series'];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['cpu_name'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['cpf_name'];?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$nm_config['cf_small'][$dvalue_val['cm_small']]."<br>(".$cm_adult.")";?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$cm_end;?></td>
					<td class="<?=$fetch_row[7][1]?> text_center"><?=$dvalue_val['cm_pay'];?></td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=$cm_somaery;?></td>
					<td class="<?=$fetch_row[9][1]?> text_center"><?=$dvalue_val['cm_episode_total'];?></td>
					<td class="<?=$fetch_row[10][1]?> text_center"><?=$dvalue_val['cm_reg_date'];?></td>
					<td class="<?=$fetch_row[11][1]?> text_center"><?=$dvalue_val['cm_episode_date'];?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- cms_result -->

<? page_view(); /* 페이지처리 */ ?>

<br>
<br>
<br>
<!-- 선택 리스트 -->
<? if($cm_no_list != null && $cm_no_list != "") {
		$bib_list_where = " and cm_no in($cm_no_list) ";
		$bib_list_sql = "select $bib_field $bib_join where 1 $bib_list_where $bib_order";
		$list_result = sql_query($bib_list_sql);
		$list_row_size = sql_num_rows($list_result);
?>
	<h3>엑셀 리스트 
		<strong>추가 수: <?=number_format($list_row_size);?>건</strong>
		<input type="button" class="excel_list_down" onclick="location.href='../excel/bibliography_excel.php?cm_no_list=<?=$cm_no_list?>&type=3'" value="엑셀 다운로드" />
	</h3>
	<section id="bib_result">
	<div id="cr_bg">
		<table>
			<tbody>
				<? while($list_row = sql_fetch_array($list_result)) {
					/* 서비스여부 */
					$cm_service = "제공";
					if($list_row['cm_service'] == 'n') { $cm_service = "중지"; }

					/* 완결여부 */
					$cm_end = "완결";
					if($list_row['cm_end'] == 'n') { $cm_end = "연속"; }

					/* 성인여부 */
					$cm_adult = "성인";
					if($list_row['cm_adult'] == 'n') { $cm_adult = "전체"; }
					
					/* 이미지 표지 */
					// $cm_cover_sub_url = NM_IMG.$list_row['cm_cover_sub'];
					$cm_cover_sub_url = img_url_para($list_row['cm_cover_sub'], $list_row['cm_reg_date'], $list_row['cm_mod_date'], 'cm_cover_sub', 'tn75x75');

					/* 줄거리 생략 */
					$cm_somaery = $list_row['cm_somaery'];
					if(strlen($cm_somaery) > 30) { $cm_somaery = mb_substr($cm_somaery, 0, 40, 'UTF-8')."...(생략)"; } // end if
					
					/* 컨텐츠 수정/삭제 */

					/* 컨텐츠 화 등록/리스트보기 */
					
					/* 대분류 */
					$row_big_text = '';
					if($th_big_text != ''){ $row_big_text = "<p class='big_list'>".$nm_config['cf_big'][$list_row['big']]."</p>"; }

					/* 등록일 */
					$db_cm_reg_date_ymd = get_ymd($list_row['cm_reg_date']);
					$db_cm_reg_date_his = get_his($list_row['cm_reg_date']);
				?>
					<tr class="result_hover" onclick="location.href='<?=$_cms_self;?>?cm_del_no=<?=$list_row['cm_no'];?>&cm_no_list=<?=$cm_no_list;?>&page=<?=$page;?>'">
						<td class="<?=$fetch_row[0][1]?> text_center"><?=$list_row['cm_no'];?></td>
						<td class="<?=$fetch_row[1][1]?> text_center"><img src="<?=$cm_cover_sub_url;?>" width="75" height="75" alt="<?= $list_row['cm_series'];?>표지"/></td>
						<td class="<?=$fetch_row[2][1]?> text_center"><?=$list_row['cm_series'];?></td>
						<td class="<?=$fetch_row[3][1]?> text_center"><?=$list_row['cpu_name'];?></td>
						<td class="<?=$fetch_row[4][1]?> text_center"><?=$list_row['cpf_name'];?></td>
						<td class="<?=$fetch_row[5][1]?> text_center"><?=$nm_config['cf_small'][$list_row['cm_small']]."<br>(".$cm_adult.")";?></td>
						<td class="<?=$fetch_row[6][1]?> text_center"><?=$cm_end;?></td>
						<td class="<?=$fetch_row[7][1]?> text_center"><?=$list_row['cm_pay'];?></td>
						<td class="<?=$fetch_row[8][1]?> text_center"><?=$cm_somaery;?></td>
						<td class="<?=$fetch_row[9][1]?> text_center"><?=$list_row['cm_episode_total'];?></td>
						<td class="<?=$fetch_row[10][1]?> text_center"><?=$list_row['cm_reg_date'];?></td>
						<td class="<?=$fetch_row[11][1]?> text_center"><?=$list_row['cm_episode_date'];?></td>
					</tr>
				<?}?>
				</tbody>
			</table>
		</div>
	</section><!-- bib_result -->

<?} // end if 선택 리스트 ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>