<?
	include_once '_common.php'; // 공통

	// 기본 적으로 몇 개 있는지 체크 (서비스 중)
	$bib_where = " and cm_service='y' ";
	$bib_service_total_sql = "select count(*) as bib_total from comics where 1 $bib_where ";
	$bib_service_total = sql_count($bib_service_total_sql, "bib_total");

	// 기본 적으로 몇 개 있는지 체크 (서비스 중단)
	$bib_where = " and cm_service='n' ";
	$bib_service_total_sql = "select count(*) as bib_total from comics where 1 $bib_where ";
	$bib_stop_total = sql_count($bib_service_total_sql, "bib_total");

	/* JOIN */
	$bib_join = " from comics c left JOIN comics_publisher cpu ON cpu.cp_no = c.cm_publisher
								left JOIN comics_professional cpf ON cpf.cp_no = c.cm_professional
								left JOIN comics_provider cpv ON cpv.cp_no = c.cm_provider
								left JOIN comics_provider cpv_sub ON cpv_sub.cp_no = c.cm_provider_sub";

	/* 데이터 가져오기 */
	$bib_where = ""; // $comics_where = $sql_cbig;

	// 장르, 작가번호, 출판사 번호
	$nm_paras_check = array('cm_small', 'cm_professional', 'cm_publisher');
	foreach($nm_paras_check as $nm_paras_key => $nm_paras_val){
		if(${"_".$nm_paras_val} && ${"_".$nm_paras_val} > 0){
			$bib_where.= "and c.".$nm_paras_val." = ".${"_".$nm_paras_val}." ";
		} // end if
	} // end foreach

	// 검색 단어 + 검색 타입
	if($_s_text){
		switch($_s_type){
			case '0': $bib_where.= "and c.cm_series like '%$_s_text%' ";
			break;
			case '1': $bib_where.= "and cpf.cp_name like '%$_s_text%' ";
			break;
			case '2': $bib_where.= "and cpu.cp_name like '%$_s_text%' ";
			break;
			default: $bib_where.= "and ( c.cm_series like '%$_s_text%' or cpf.cp_name like '%$_s_text%' or cpu.cp_name like '%$_s_text%' ) ";
			break;
		} // end switch
	} // end if

	// 완결 구분
	if($_cm_end != ""){
		$bib_where.= "and c.cm_end = '$_cm_end' ";
	} // end if

	// 최초/최신등록일 날짜
	$start_date = $end_date = "";
	if($_date_type) { //all, cm_reg_date, cm_episode_date
		if($_s_date && $_e_date) {
			$start_date = $_s_date." ".NM_TIME_HI_start;
			$end_date = $_e_date." ".NM_TIME_HI_end;
		} else if($_s_date) {
			$start_date = $_s_date." ".NM_TIME_HI_start;
			$end_date = NM_TIME_YMD." ".NM_TIME_HI_end;
		} // end else if

		if($start_date && $end_date) {
			switch($_date_type){
				case 'all' : $bib_where.= "and ( c.cm_episode_date <= '$end_date' and c.cm_episode_date >= '$start_date' or c.cm_reg_date <= '$end_date' and c.cm_reg_date >= '$start_date') ";
				break;

				case 'cm_reg_date' : $bib_where.= "and c.cm_reg_date <= '$end_date' and c.cm_reg_date >= '$start_date' ";
				break;

				case 'cm_episode_date' : $bib_where.= "and c.cm_episode_date <= '$end_date' and c.cm_episode_date >= '$start_date' ";
				break;

				default: $bib_where.= " ";
				break;
			} // end switch
		} // end if
	} else {
		$_s_date = "";
		$_e_date = "";
	} // end else

	// 서지정보 리스트
	if($_REQUEST['cm_del_no']) { // 삭제 후 리스트 재 등록
		$cm_no_list = $_REQUEST['cm_no_list'];
		$cm_del_no = $_REQUEST['cm_del_no'];

		if(strlen($cm_no_list) == 4) {
			$cm_no_list = null;
		} else {
			if(strpos($cm_no_list, $cm_del_no) == 0) {
				$cm_no_list = str_replace($cm_del_no.", ", "", $cm_no_list);
			} else {
				$cm_no_list = str_replace(", ".$cm_del_no, "", $cm_no_list);
			} // end else
		} // end else

	} else if($_REQUEST['cm_no_list']) { // 리스트 있을 시 리스트 등록
		$cm_no_list = $_REQUEST['cm_no_list'];
		$cm_no_list .= ", ".$_REQUEST['cm_no'];

		if(strrpos($cm_no_list, ",") == strlen($cm_no_list)-2) {
			$cm_no_list = substr($cm_no_list, 0, -2);
		} // end if

	} else if($_REQUEST['cm_no']) { // 리스트 없을 시 리스트 등록
		$cm_no_list = $_REQUEST['cm_no'];
	} // end else if

	if($cm_no_list != null) {
		$bib_where .= " and cm_no not in($cm_no_list) ";
	} // end if

	/* 엑셀 다운로드 일때 */
	if($_type) {		
		switch($_type) {
			case 1 :
				break;

			case 2 :
				$bib_where = " and c.cm_small='".$_small."' ";
				break;

			case 3 :
				$cm_no_list = $_REQUEST['cm_no_list']; // 리스트 번호
				$bib_where = " and c.cm_no in($cm_no_list) ";
				break;

			default : 
				break;
		} // end switch
	} // end if

	// 정렬
	if($_order_field == null || $_order_field == "") { $_order_field = "cm_reg_date"; } // end if
	if($_order == null || $_order == "") { $_order = "desc"; } // end if
	$bib_order = "order by c.".$_order_field." ".$_order;

	$bib_sql = "";
	$bib_field = " c.*, cpu.cp_name as cpu_name, cpf.cp_name as cpf_name, cpv.cp_name as cpv_name, cpv_sub.cp_name as cpv_name_sub "; // 가져올 필드 정하기
	$bib_limit = "";

	$bib_sql = "select $bib_field $bib_join where 1 $bib_where $bib_order $bib_limit";
	
	$result = sql_query($bib_sql);
	$row_size = sql_num_rows($result);
?>