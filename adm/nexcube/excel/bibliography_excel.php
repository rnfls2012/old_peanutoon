<?php
include_once '../team1/_common.php'; // 공통

/*
if($mb_level < 6) { 
	alert_close($mb_level."접근권한이 없습니다.");
	die;
}
*/

$today = date("Ymd");
// $today = NM_TIME_YMD;

/* 쿼리문 */
include_once '../team1/bibliography_sql.php';

// 엑셀 Library Import
include_once('../../../lib/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once('../../../lib/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
array_push($data_head, '번호');
array_push($data_head, '썸네일');
array_push($data_head, '작품명');
array_push($data_head, '출판사');
array_push($data_head, '작가');
array_push($data_head, '장르');
array_push($data_head, '성인 여부');
array_push($data_head, '완결 구분');
array_push($data_head, '가격');
array_push($data_head, '줄거리');
array_push($data_head, '총 권(화)수');
array_push($data_head, '최초 등록일');
array_push($data_head, '최신 등록일');

// DB 컬럼용 배열
$data_key = array();
array_push($data_key, 'cm_no');
array_push($data_key, 'cm_cover_sub');
array_push($data_key, 'cm_series');
array_push($data_key, 'cpu_name');
array_push($data_key, 'cpf_name');
array_push($data_key, 'cm_small');
array_push($data_key, 'cm_adult');
array_push($data_key, 'cm_end');
array_push($data_key, 'cm_pay');
array_push($data_key, 'cm_somaery');
array_push($data_key, 'cm_episode_total');
array_push($data_key, 'cm_reg_date');
array_push($data_key, 'cm_episode_date');


/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write(0, $col++, $cell);
} // end foreach

/* 데이터 전달 부분 */
for ($i=1; $row=mysql_fetch_array($result); $i++){
	foreach($data_key as $cell_key => $cell_val) {
		switch($cell_key) {
			case 5:
				$worksheet->write($i, $cell_key , iconv_cp949($nm_config['cf_small'][$row[$cell_val]]));
				break;
			
			case 6:
				$worksheet->write($i, $cell_key , iconv_cp949($d_adult[$row['cm_adult']]));
				break;

			case 7:
				$worksheet->write($i, $cell_key , iconv_cp949($d_cm_end[$row['cm_end']]));
				break;

			default:
				$worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]));
				break;
		} // end switch
	} // end foreach
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."bibliography_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"bibliography_".$today.".xls");
header("Content-Disposition: inline; filename=\"bibliography_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
?>