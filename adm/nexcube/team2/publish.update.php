<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMETER CHECK */
array_push($para_list, 'mode', 'bp_no', 'bp_file');

/* 숫자 PARAMETER 체크 */
array_push($para_num_list, 'bp_no');

/* 에디터 PARAMITER 체크 */

/* 빈칸 PARAMITER 시 NULL 처리 */

$dbtable = "board_publish";
$dbt_primary = "bp_no";
$para_primary = "bp_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

/* DB field 아닌 목록 */
$db_field_exception = array('mode'); 
if($_mode == 'del'){	
	if(${'_'.$dbt_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$dbt_primary.'값이 없습니다.';
	}else{
		/* 데이터 삭제 
		$sql_del = 'delete from '.$dbtable.' where '.$dbt_primary.'="'.${'_'.$dbt_primary}.'"';
		sql_query($sql_del);		
		$db_result['msg'] = '데이터가 삭제되였습니다.';
		*/
		
		/* 완전 삭제에서 상태만 y->n으로 바꿈 170904 */
		$sql_del = "UPDATE ".$dbtable." SET bp_state='n' WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		sql_query($sql_del);
		$db_result['msg'] = '데이터가 삭제되였습니다.';

		/* 삭제 로그 추가 170904 */
		$sql_log = "INSERT INTO x_publish(x_pb_member, x_pb_member_idx, x_pb_publish_no, x_pb_date) VALUES('".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', '".${'_'.$dbt_primary}."', '".NM_TIME_YMDHIS."')";
		sql_query($sql_log);
	} // end else

	/* Storage 실제 파일부터 삭제 170904 주석 처리
	if(kt_storage_delete($_bp_file)) {
	} else {
		$db_result['msg'] = '파일 삭제에 실패하였습니다.';
	} // end else
	 */
/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바랍니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/

alert($db_result['msg'], $_SERVER['HTTP_REFERER'], $db_result['error']);
die;

?>