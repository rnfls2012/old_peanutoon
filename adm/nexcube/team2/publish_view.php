<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_bp_no = tag_filter($_REQUEST['bp_no']);

$sql = "select * from board_publish where bp_no = '$_bp_no'";

if($sql != ''){
	$row = sql_fetch($sql);
} // end if

/* 대분류로 제목 */
$page_title = "연재 문의";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<script type="text/javascript">
	<!--
	window.onload = function(){popup_resize('<?=$_mode;?>');}
	//-->
</script>

<section id="cms_publish_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- cms_member_title -->

<section id="board_view">
	<table>
		<tbody>
			<tr>
				<th><label for="bp_title">제목</label></th>
					<td>
						<?=$row['bp_title']?>
					</td>
				</tr>
				<tr>
					<th><label for="bp_text">내용</label></th>
					<td>
						<?=nl2br($row['bp_text']);?>
					</td>
				</tr>
				<tr>
          <th><label for="bp_return">연락받을 메일주소</label></th>
          <td>
              <?=$row['bp_return']?>
          </td>
	      </tr>
				<tr>
					<td colspan="2" class="submit_btn">
						<input type="reset" value="닫기" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
</section><!-- board_view -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>