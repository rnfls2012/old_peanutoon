<?
include_once '_common.php'; // 공통

/* PARAMITER */

/* 데이터 가져오기 */
$where_z_ms = ""; /* 충전 */

if($_s_date == ""){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ""){ $_e_date = NM_TIME_YMD; }
if($_s_date && $_e_date){
	$where_z_ms .= " AND (z_ms_date >='".$_s_date."' AND z_ms_date <= '".$_e_date."') ";
} // end if

$realtime = $_POST["realtime"];
if($realtime) {
	define('_SH_DAY_SIX_', true);
	include_once NM_PATH.'/sh/everyday_six/member_stats.php';
	$tomorrow = date("Y-m-d", strtotime(NM_TIME_YMD."+1 day"));
	z_member_stats(NM_TIME_YMD, $tomorrow, "y");
} // end if

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "z_ms_date"){ 
	$_order_field = "z_ms_date"; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc";  
}

$order_z_ms = "order by ".$_order_field." ".$_order;

$sql_z_ms = "";
$field_z_ms = " * "; // 가져올 필드 정하기
$limit_z_ms = "";

$sql_z_ms = "SELECT $field_z_ms FROM z_member_stats WHERE 1 $where_z_ms $order_z_ms $limit_z_ms";
$result = sql_query($sql_z_ms);
$row_size = sql_num_rows($result);


/******** 상단 테이블 ********/
// 당일 데이터
$z_recharge_member_sum = 0;
$z_recharge_count_sum = 0;
$z_recharge_total_sum = 0;

while($t_date_row = sql_fetch_array($result)) {
	$z_recharge_member_sum += $t_date_row["z_ms_recharge_member_count"];
	$z_recharge_count_sum += $t_date_row["z_ms_recharge_count"];
	$z_recharge_total_sum += $t_date_row["z_ms_recharge_total"];
} // end while

// 누적 데이터
$_e_next_date = date("Y-m-d", strtotime($_e_date."+1 day"));
/*
// 1. 기간내 가입자 중 누적 결제 회원 수
$recharge_member_count_sql = "SELECT COUNT(DISTINCT(mpu.mpu_member)) AS recharge_member_count FROM member_point_used mpu LEFT JOIN member mb ON mpu.mpu_member = mb.mb_no where (mb.mb_join_date >= '".$_s_date."' and mb.mb_join_date < '".$_e_next_date."') AND mpu.mpu_class = 'r' AND mpu.mpu_recharge_won != '0'";
$recharge_member_count = sql_count($recharge_member_count_sql, 'recharge_member_count');
*/
// 1. 기간내 탈퇴 제외 회원 수
$member_count_sql = "SELECT COUNT(*) AS member_count FROM member WHERE mb_join_date < '".$_e_next_date."' AND mb_state = 'y'";
$member_count = sql_count($member_count_sql, 'member_count');
/*
// 2. 기간내 가입자 중 누적 결제 건수
$recharge_count_sql = "SELECT COUNT(*) AS recharge_count FROM member_point_used mpu LEFT JOIN member mb ON mpu.mpu_member = mb.mb_no WHERE (mb.mb_join_date >= '".$_s_date."' and mb.mb_join_date < '".$_e_next_date."') AND mpu.mpu_class = 'r' AND mpu.mpu_recharge_won != '0'";
$recharge_count = sql_count($recharge_count_sql, 'recharge_count');
*/
// 2. 기간내 총 결제 건수
$recharge_count_sql = "SELECT COUNT(*) AS recharge_count FROM member_point_used WHERE (mpu_date >= '".$_s_date."' and mpu_date < '".$_e_next_date."') AND mpu_class = 'r' AND mpu_recharge_won != '0'";
$recharge_count = sql_count($recharge_count_sql, 'recharge_count');
/*
// 3. 기간내 가입자 누적 결제 총액
$recharge_total_sql = "SELECT SUM(mpu.mpu_recharge_won) AS recharge_total FROM member_point_used mpu LEFT JOIN member mb ON mpu.mpu_member = mb.mb_no WHERE (mb.mb_join_date >= '".$_s_date."' and mb.mb_join_date < '".$_e_next_date."') AND mpu.mpu_class = 'r' AND mpu.mpu_recharge_won != '0'";
$recharge_total = sql_count($recharge_total_sql, 'recharge_total');
*/
// 3. 기간내 결제 총액
// $recharge_total_sql = "SELECT SUM(mpu_recharge_won) AS recharge_total FROM member_point_used WHERE (mpu_date >= '".$_s_date."' and mpu_date < '".$_e_next_date."') AND mpu_class = 'r' AND mpu_recharge_won != '0'";
// db가 잘못됨 member_point_used -> sales_recharge 2018-03-07

$where_sales_recharge = "";
if($_s_date && $_e_date){
	$where_sales_recharge = date_year_month($_s_date, $_e_date, 'sr');
}

$recharge_total_sql = "select SUM(sr_pay) AS recharge_total FROM sales_recharge where 1 $where_sales_recharge";
$recharge_total = sql_count($recharge_total_sql, 'recharge_total');

// 4. 잔여 땅콩, 미니땅콩
$balance_sql = "SELECT SUM(mb_cash_point) AS cash_point, SUM(mb_point) AS point FROM member WHERE mb_state = 'y'";
$balance_total = sql_fetch($balance_sql);

// 5. 포인트파크
/*
$where_pointpark = "";
if($_s_date && $_e_date){	
	$where_pointpark = "AND (pointpark_date > '".$_s_date." ".NM_TIME_HI_start."' AND pointpark_date < '".$_e_date." ".NM_TIME_HI_end."') ";
}

$pointpark_sql = " select SUM(pointpark_amt) AS pointpark_total FROM pg_channel_pointpark where 1 AND code='0'  AND pointpark_amt >0 $where_pointpark ";
$pointpark_total = sql_count($pointpark_sql, 'pointpark_total');
*/
$where_pointpark = "";
if($_s_date && $_e_date){
	$where_pointpark.= date_year_month($_s_date, $_e_date, 'srp');
}

$pointpark_total_sql = "select SUM(srp_pay) AS pointpark_total FROM sales_recharge_pointpark where 1 $where_pointpark";
$pointpark_total = sql_count($pointpark_total_sql, 'pointpark_total');




/******** 상단 테이블 끝 *******/

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array("날짜", "z_ms_date", 1));
array_push($fetch_row, array("신규회원", "z_ms_join_count", 0));
array_push($fetch_row, array("결제 신규회원", "z_ms_recharge_member_count", 0));
array_push($fetch_row, array("신규회원 결제 수", "z_ms_recharge_count", 0));
array_push($fetch_row, array("신규 결제 금액", "z_ms_recharge_total", 0));
array_push($fetch_row, array("로그인 회원", "z_ms_login_count", 0));
array_push($fetch_row, array("최종 저장 시간", "z_ms_real_time_date", 0));

// 날짜 보이게...
$cs_calendar_on = "cs_calendar_on";

$page_title = "회원 가입/결제 통계";
$cms_head_title = $page_title;

$head_title = $nm_config["cf_title"]."-CMS-".$cms_head_title;
include_once NM_ADM_PATH."/_head_menu.sub.php"; // 해더
include_once NM_ADM_PATH."/_page_v1.php"; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<!--
<script type="text/javascript">
	var cr_thead_mg_add = '22px'; /* fixed 테이블이 높이가 커서 그만큼 마진 */
</script>
-->
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
</section><!-- cms_title -->

<section id="cms_explain">
	<ul>
		<li>데이터는 매일 새벽 6시에 갱신됩니다. </li>
		<li>실시간 데이터 검색 시 '실시간 검색' 체크한 뒤, 검색하셔야 합니다. </li>
	</ul>
</section><!-- cms_explain -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="z_ms_search_form" id="z_ms_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return z_ms_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<input type="checkbox" name="realtime" id="realtime" class="realtime" value="realtime"/><label for="realtime">실시간 검색</label>
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- z_ms_search -->

<section id="bib_result">
	<h3 id="cr_thead_mg_add">
		<strong><?=$_s_date." ~ ".$_e_date." 전체회원 결제 정보";?></strong>
		<table>
			<tr>
				<th class="topleft">총 회원</th>
				<th class="">총 결제 건수</th>
				<th class="">결제 총액</th>
				<th class="">포인트파크 결제 총포인트</th>
				<th class="point">잔여 <?=$nm_config['cf_cash_point_unit_ko'];?></th>
				<th class="topright point">잔여 <?=$nm_config['cf_point_unit_ko'];?></th>
			</tr>
			<tr>
				<td><?=number_format($member_count);?> 명</td>
				<td><?=number_format($recharge_count);?> 건</td>
				<td><?=number_format($recharge_total);?> 원</td>
				<td><?=number_format($pointpark_total);?> 포인트</td>
				<td><?=number_format($balance_total['cash_point'])." ".$nm_config['cf_cash_point_unit_ko'];?></td>
				<td><?=number_format($balance_total['point'])." ".$nm_config['cf_point_unit_ko'];?></td>
			</tr>
		</table>

		<br>

		<strong><?=$_s_date." ~ ".$_e_date." 신규회원 결제 정보";?></strong>
		<table>
			<tr>
				<th class="topleft">결제 회원</th>
				<th class="">결제 건수</th>
				<th class="topright">결제 총액</th>
			</tr>
			<tr>
				<td><?=number_format($z_recharge_member_sum);?> 명</td>
				<td><?=number_format($z_recharge_count_sum);?> 건</td>
				<td><?=number_format($z_recharge_total_sum);?> 원</td>
			</tr>
		</table>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){?>
				<tr>
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val["z_ms_date"];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=number_format($dvalue_val["z_ms_join_count"]);?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=number_format($dvalue_val["z_ms_recharge_member_count"]);?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=number_format($dvalue_val["z_ms_recharge_count"]);?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=number_format($dvalue_val["z_ms_recharge_total"])." 원";?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=number_format($dvalue_val["z_ms_login_count"])." 명";?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$dvalue_val["z_ms_real_time_date"];?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>