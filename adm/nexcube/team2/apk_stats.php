<?
include_once '_common.php'; // 공통

/* PARAMITER */

/* 데이터 가져오기 */
$where_z_ms = ""; /* 충전 */

if($_s_date == ""){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ""){ $_e_date = NM_TIME_YMD; }
if($_s_date && $_e_date){
	$where_z_as .= " AND (z_as_date >='".$_s_date."' AND z_as_date <= '".$_e_date."') ";
} // end if

$realtime = $_POST["realtime"];
if($realtime) {
	define('_SH_DAY_SIX_', true);
	include_once NM_PATH.'/sh/everyday_six/apk_stats.php';
	$tomorrow = date("Y-m-d", strtotime(NM_TIME_YMD."+1 day"));
	z_apk_stats(NM_TIME_YMD, $tomorrow, "y");
} // end if

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "z_as_date"){ 
	$_order_field = "z_as_date"; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc";  
}

$order_z_as = "order by ".$_order_field." ".$_order;

$sql_z_as = "";
$field_z_as = " * "; // 가져올 필드 정하기
$limit_z_as = "";

$sql_z_as = "SELECT $field_z_as FROM z_apk_stats WHERE 1 $where_z_as $order_z_as $limit_z_as";
$result = sql_query($sql_z_as);
$row_size = sql_num_rows($result);


/******** 상단 테이블 ********/
// 당일 데이터
$z_down_sum = 0;
$z_recharge_member_sum = 0;
$z_recharge_count_sum = 0;
$z_recharge_total_sum = 0;
while($t_date_row = sql_fetch_array($result)) {
	// $z_down_sum += $t_date_row["z_as_down_count"];
	$z_recharge_member_sum += $t_date_row["z_as_recharge_member_count"];
	$z_recharge_count_sum += $t_date_row["z_as_recharge_count"];
	$z_recharge_total_sum += $t_date_row["z_as_recharge_total"];
} // end while
/******** 상단 테이블 끝 *******/

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array("날짜", "z_as_date", 1));
array_push($fetch_row, array("앱 다운로드 수", "z_as_down_count", 0));
array_push($fetch_row, array("앱 결제 회원 수", "z_as_recharge_member_count", 0));
array_push($fetch_row, array("앱 결제 건수", "z_as_recharge_count", 0));
array_push($fetch_row, array("앱 결제 금액", "z_as_recharge_total", 0));
array_push($fetch_row, array("최종 저장 시간", "z_as_real_time_date", 0));

// 날짜 보이게...
$cs_calendar_on = "cs_calendar_on";

$page_title = "회원 가입/결제 통계";
$cms_head_title = $page_title;

$head_title = $nm_config["cf_title"]."-CMS-".$cms_head_title;
include_once NM_ADM_PATH."/_head_menu.sub.php"; // 해더
include_once NM_ADM_PATH."/_page_v1.php"; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<!--
<script type="text/javascript">
	var cr_thead_mg_add = '22px'; /* fixed 테이블이 높이가 커서 그만큼 마진 */
</script>
-->
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
</section><!-- cms_title -->

<section id="cms_explain">
	<ul>
		<li>데이터는 매일 새벽 6시에 갱신됩니다. </li>
		<li>실시간 데이터 검색 시 '실시간 검색' 체크한 뒤, 검색하셔야 합니다. </li>
	</ul>
</section><!-- cms_explain -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="z_ms_search_form" id="z_ms_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return z_ms_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<input type="checkbox" name="realtime" id="realtime" class="realtime" value="realtime"/><label for="realtime">실시간 검색</label>
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- z_ms_search -->

<section id="bib_result">
	<h3 id="cr_thead_mg_add">
		<strong><?=$_s_date." ~ ".$_e_date." 앱 결제 정보";?></strong>
		<table>
			<tr>
				<!--<th class="topleft">APK 다운로드 수</th>-->
				<th class="topleft">앱 결제 회원</th>
				<th>앱 결제 건 수</th>
				<th class="topright">앱 결제 총액</th>
			</tr>
			<tr>
				<!--<td><?=number_format($z_down_sum);?> 건</td>-->
				<td><?=number_format($z_recharge_member_sum);?> 명</td>
				<td><?=number_format($z_recharge_count_sum);?> 건</td>
				<td><?=number_format($z_recharge_total_sum);?> 원</td>
			</tr>
		</table>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){?>
				<tr>
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val["z_as_date"];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=number_format($dvalue_val["z_as_down_count"]);?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=number_format($dvalue_val["z_as_recharge_member_count"]);?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=number_format($dvalue_val["z_as_recharge_count"]);?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=number_format($dvalue_val["z_as_recharge_total"])." 원";?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$dvalue_val["z_as_real_time_date"];?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>