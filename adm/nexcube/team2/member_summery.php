<?
include_once '_common.php'; // 공통

// 회원 종류별로 카운트
$cmp_kind_arr = array('총 회원', '현재 회원', '관리자 회원', '파트너 회원', '휴면 회원', '이용 중지 회원', '탈퇴 회원', '성인 인증 회원', '오늘 가입한 회원');
$count_mb_row = kind_member($cmp_kind_arr); // 회원 종류 별 - function 선언 하단

$page_title = "전체 회원 요약";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="member_summery">
	<h3 id="cr_thead_mg_add">회원 요약
		<table>
			<tr>
				<? foreach($cmp_kind_arr as $key => $val) { 
					if($key == 0) { $th_class = "topleft"; } 
					else if(end(array_keys($cmp_kind_arr)) == $key) { $th_class = "topright"; }
					else { $th_class = ""; } ?>
					<th class="<?=$th_class;?>"><?=$val;?></th>
				<? } // end foreach ?>
			</tr>
			<tr>
				<? foreach($count_mb_row as $key => $val) { 
					if($key == 'total_member') { $td_class = "btmleft"; }
					else if(end(array_keys($count_mb_row)) == $key) { $td_class = "btmright"; }
					else { $td_class = ""; } ?>
					<td class="<?=$td_class;?>"><?=number_format($val);?> 명</td>
				<? } // end foreach ?>
			</tr>
		</table>
	</h3>
</section>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>

<?	/* 해당 페이지에서 사용하는 function */
// 회원 총수
function total_member()
{
	$sql_member_total = "select count(*) as total_member from member ";
	$total_member = sql_count($sql_member_total, 'total_member');
	return $total_member; 
}

// 회원 종류 별
function kind_member($cmp_kind_arr)
{
	$sql_count_member = "SELECT count(*) as total_member, 
					sum(if(mb_state='y', 1, 0)) as activity_member, 
					sum(if(mb_class='a', 1, 0)) as admin_member, 
					sum(if(mb_class='p', 1, 0)) as partner_member, 
					sum(if(mb_state='y' and mb_state_sub='h', 1, 0)) as human_member, 
					sum(if(mb_state='y' and mb_state_sub='s', 1, 0)) as stop_member, 
					sum(if(mb_state='n', 1, 0)) as out_member,
					sum(if(mb_adult='y', 1, 0)) as adult_member, 
					sum(if(mb_join_date >= '".NM_TIME_YMD."' AND mb_join_date < '".NM_TIME_P1."', 1, 0)) as today_join_member

					FROM member";
	$count_mb_row = sql_fetch($sql_count_member);
	return $count_mb_row;
}


?>