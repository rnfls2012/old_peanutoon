<?
include_once '_common.php';

	// 관리자만
	if(intval($nm_member['mb_level']) < 29){
		header( "content-type: application/json; charset=utf-8" );
		echo json_encode("접근 권한이 없습니다.");
		die;
	}

	// db cdn 정보
	$cdn_config_token = cdn_config_token();

	// cdn 토근 발급여부	
	$get_token = false;
	if($cdn_config_token['cf_cdn_token_exprie']!=''){	
		// 현재시간 구하기	
		$time =  NM_SERVER_TIME;
		$date = date('Y-n-j', $time);
		$hour = intval(date('G', $time));
		$min = intval(date('i', $time));
		$sec = intval(date('s', $time));
		$time_cdn_type = $date.' '.$hour.':'.$min.':'.$sec;

		// 시간 비교
		$str_now = strtotime($time_cdn_type);
		$str_target = strtotime($cdn_config_token['cf_cdn_token_exprie']);
		if($str_now > $str_target) { // CDN에서 24시간안에 1번 부르는 걸로 권고, 또는 90일동안 사용 안하면 계정 정지됨
			$get_token = true;
		}		
	}else{
		$get_token = true;		
	}
	
	// cdn 토큰 재발급
	if($get_token == true){	
		$cdn_get_token = cdn_get_token();
		$sql_update = "UPDATE config SET 
									 cf_cdn_token='".$cdn_get_token['cf_cdn_token']."', 
									 cf_cdn_token_exprie='".$cdn_get_token['cf_cdn_token_exprie']."' 
									 WHERE cf_no=1";
		sql_query($sql_update);
		$cdn_config_token = cdn_config_token();
	}
	
	/*
	// purge 예시
	$data = array();
	array_push($data, 'data/2/1240/ce_cover/chapter2.jpg');
	array_push($data, 'thumbnail/data/2/1240/ce_cover/chapter2_tn173x86.jpg');
	array_push($data, 'thumbnail/data/2/1240/ce_cover/chapter2_tn668x334.jpg');
	array_push($data, 'thumbnail/data/2/1240/ce_cover/chapter2_tn640x320.jpg');

	*/
	$_storagedata = $_REQUEST['storagedata'];
	$purge_list = explode("||", $_storagedata);
	$cdn_purge_api = cdn_purge_api($purge_list, $cdn_config_token['cf_cdn_token'], true);

	header( "content-type: application/json; charset=utf-8" );
	// echo $cdn_purge_api;
	echo json_encode($cdn_purge_api);
	die;

// function /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function cdn_config_token(){
		// db cdn 정보
		$sql = "SELECT cf_cdn_token, cf_cdn_token_exprie FROM config WHERE cf_no=1";
		$row = sql_fetch($sql);

		return $row;
	}

	function cdn_get_token(){

		// CDN 정보
		$FQDN = 'api-sbp.ktcdn.co.kr:8090';
		$purge_user='peanutoonapi'; // Purge 전용 ID
		$purge_password='peanutoonapi1@'; // Purge 전용 ID-PW

		$cdn_token_api_url = 'https://'.$FQDN.'/v1/auth/tokens?pretty=1';

		$ch_data = array();
		$ch_data["user"] = $purge_user;
		$ch_data["password"] = $purge_password;
		$jsonBody = stripslashes(json_encode($ch_data));
		// print_r($jsonBody);

		$ch = curl_init($cdn_token_api_url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($jsonBody))
		);

		$result = curl_exec($ch);
		curl_close($ch);
		$result_token = json_decode(stripslashes($result),true);
		
		// print_r($result_token);
		
		$cdn_result = array();
		$cdn_result['cf_cdn_token'] = $result_token['token']['id'];
		$cdn_result['cf_cdn_token_exprie'] = $result_token['token']['expire'];
		
		return $cdn_result;
	}

	function cdn_purge_api($data, $token, $data_path=false){
	
		// CDN 정보
		$FQDN = 'api-sbp.ktcdn.co.kr:8090';
		$svc_name='peanutoon';	
		$vol_name='img';
		
		$cdn_purge_api_url = 'https://'.$FQDN.'/v1/management/service/'.$svc_name.'/volume/'.$vol_name.'/purge?async=1';
		
		$save_data =  array();
		$data_bool = true;
		$data_giho = '';
		if($data_path == false){
			$data_giho = '/';
		}
		
		if(isset($data) && is_array($data)){
			foreach($data as $data_val){
				if($data_val == ''){ continue; }
				array_push($save_data, $data_giho.$data_val);
			}
		}else{
			$result_mse = '데이터가없음';
			$data_bool = false;
		}

		if($data_bool == true){
			$ch_data = array();
			$ch_data["filelist"] = $save_data;
			$ch_data["comments"] = 'filemod';
			$jsonBody = stripslashes(json_encode($ch_data));
			// print_r($ch_data);

			$ch = curl_init($cdn_purge_api_url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'X-Auth-Token: '.$token.'')
			);

			$result = curl_exec($ch);
			curl_close($ch);
			$cdn_api_result = json_decode(stripslashes($result),true);
			
			// print_r($cdn_api_result);
			
			$result_api = true;
			if(isset($cdn_api_result['errorcode'])){
				$result_api = false;
				if(is_array($cdn_api_result['error'])){
					$result_mse = implode("||",$cdn_api_result['error']);
				}else{
					$result_mse = $cdn_api_result['error'];
					if($result_mse == 'Authentication failure'){ // 토큰 초기화
						$sql_update = "UPDATE config SET 
													 cf_cdn_token='', 
													 cf_cdn_token_exprie='' 
													 WHERE cf_no=1";
						sql_query($sql_update);
						$result_mse.=" (다시 시도해주세요 반복되면 관리자에게 문의 주세요)"; 
					}
				}
			}else{	
				$result_mse = '요청하신 데이터 Purge 작업이 완료 되었습니다.';
			}	
		}
		
		return $result_mse;
	}

?>