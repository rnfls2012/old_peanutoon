<?
include_once '_common.php'; // 공통

/* 공통 처리 */
$cw_class = $cmweek;
$page_adult = '';
if($cmweek_adult == 'n') { 
	$page_adult = '-청소년'; 
	$cw_class.= '_t';
}

$page_title = $week_list[$cw_class];

$get_comics_cycle_fix = $cmweek;
if($cmweek == '0'){ $get_comics_cycle_fix = '7';}
$cmserial_cycle_in_arr = get_comics_cycle_fix($get_comics_cycle_fix);

$sql_where = " AND cm_public_cycle IN(".$cmserial_cycle_in_arr.") ";
$sql_cw_where = " AND cw_class='".$cw_class."' ";

$where_adult = "";
if($cmweek_adult == 'n'){
	$sql_where.= " AND cm_adult='n' ";
	$sql_cw_where.= " AND cw_adult='n' ";
}

/* PARAMITER */

/* 데이터 가져오기 */
$sql_where.= "AND cm_service = 'y' ";

// 검색단어+ 검색타입
if($_s_text){
	$sql_where.= "AND cm_series like '%$_s_text%' ";
} // end if

// 정렬
$order_sales = "";

$sql_sales = "";
$sql_field = " * "; // 가져올 필드 정하기
$sql_group = " ";
$sql_limit = " LIMIT 0, 100 ";




// 순위 통계 비교
$sql = " 
 select $sql_field FROM comics	
 where 1 $sql_where 
 $sql_group 
 $order_sales 
 $sql_limit
";

$result = sql_query($sql);
$row_data  = array();
while ($row = sql_fetch_array($result)) {
	array_push($row_data, $row);
}

$sql_cw = "SELECT * FROM comics_week WHERE 1 $sql_cw_where ORDER BY cw_ranking ASC LIMIT 0, 100;";
$result_cw = sql_query($sql_cw);
$row_size_cw = sql_num_rows($result_cw);
$row_data_cw_comics  = $row_data_cw  = array();
while ($row_cw = mysql_fetch_array($result_cw)) {
	array_push($row_data_cw, $row_cw);
	array_push($row_data_cw_comics, $row_cw['cw_comics']);
}

// 보기 카운터
$show_list = 20;

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
// 페이지 처리 안함

$small_js = str_replace("cms_comics.js", "cms_comics_small.js", $_cms_js);
$small_update = str_replace("ranking_small.update.php", "cmweek.update.php", $_cms_folder_update);
?>
<style type="text/css">
	#lnbs ul li:nth-child(<?=count($week_list)+1?>) {clear: both;}
	#lnbs_end {margin-bottom: 54px;}
</style>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<!-- <script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script> -->
<script type="text/javascript" src="<?=$small_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> 이벤트 : 총 <?=number_format($total_epromotion);?> 건</strong>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="ranking_search_form" id="ranking_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return ranking_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<input type="text" id="s_text" name="s_text" placeholder="코믹스명을 입력해주세요." value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit cmweek_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="30"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="comics_result" class="clear">
	<div class="comics_ranking_list">

		<!-- //////////////////////// 순위 통계 등급 //////////////////////// -->

		<div class="comics_sales">
		<h3>검색 결과</h3>
			<ul id="sortable1" class="connectedSortable">
			<? $comics_ranking_no = 0;
			foreach($row_data as $dvalue_key => $dvalue_val){
				if($dvalue_key == $show_list){ break; }
				$comics_ranking_no++;

				/* 컨텐츠 정보 */
				$get_comics = get_comics($dvalue_val['cm_no']);
				
				/* 컨텐츠 제목 */
				$db_series = $get_comics['cm_series'];

				/* 이미지 표지 */
				$cm_cover_sub_url = img_url_para($get_comics['cm_cover_sub'], $get_comics['cm_reg_date'], $get_comics['cm_mod_date'],75,75);

				/* 컨텐츠 등급 */
				$db_adult = $d_adult[$get_comics['cm_adult']];

				/* 순위차이 */
				$db_ranking_gap = $dvalue_val['cw_ranking_gap'];

				/* 순위표기 */
				$db_ranking_mark = $d_ranking_mark[$dvalue_val['cw_ranking_mark']];

				/* 순위표기 아이콘 */
				$db_ranking_mark_icon = $d_ranking_mark_icon[$dvalue_val['cw_ranking_mark']];

				/* 저장할 정보 */
				$db_cw_adult = $get_comics['cm_adult'];

				/* 컨텐츠 완결 */
				$db_cm_end = $d_cm_end[$get_comics['cm_end']];

				/* 컨텐츠 주기 */
				$db_cm_public_period = $d_cm_public_period[$get_comics['cm_public_period']];

				$db_cw_lock = 'n';
				$db_cw_comics = $get_comics['cm_no'];
				$db_cw_big = $get_comics['cm_big'];

				$db_cw_ranking_gap = $dvalue_val['cw_ranking_gap'];
				if($dvalue_val['cw_ranking_gap'] == 0){ $db_cw_ranking_gap = ''; }
				$db_cw_ranking_mark = $dvalue_val['cw_ranking_mark'];

				/* 전체 순위 있는 건 고정 */
				$ui_state_disabled = "";
				if(in_array($db_cw_comics, $row_data_cw_comics)){
					$ui_state_disabled = "ui-state-disabled";					
				}
				
			?>
				<li id="row_data_<?=$db_cw_comics?>" class="ui-state-default <?=$ui_state_disabled;?>">
					<dl>
						<dt><img src="<?=$cm_cover_sub_url;?>" alt="<?= $db_series;?>표지" width="75" height="75" /></dt>
						<dd>
							<p class="bold">No<?=$comics_ranking_no;?> (등급: <?= $db_adult;?>) 
								<i class="fa fa-<?=$db_ranking_mark_icon?>" aria-hidden="true"></i>
								<span><?=$db_ranking_gap?></span>
							</p>
							<p><?= $db_series;?></p>
							<p>정보 : <?= $db_cm_end;?> / <?= $db_cm_public_period;?></p>
						</dd>
					</dl>
					<i class="fa fa-times-circle" aria-hidden="true"></i>
					<i class="ranking_lock fa fa-unlock" aria-hidden="true"></i>
					<input type="hidden" name="cw_adult[]" class="cw_adult" value="<?=$db_cw_adult?>">
					<input type="hidden" name="cw_lock[]" class="cw_lock" value="<?=$db_cw_lock?>">
					<input type="hidden" name="cw_comics[]" class="cw_comics" value="<?=$get_comics['cm_no']?>">
					<input type="hidden" name="cw_big[]" class="cw_big" value="<?=$db_cw_big?>">

					<input type="hidden" name="cw_ranking_gap[]" class="cw_ranking_gap" value="<?=$db_cw_ranking_gap?>">
					<input type="hidden" name="cw_ranking_mark[]" class="cw_ranking_mark" value="<?=$db_cw_ranking_mark?>">
				</li>
			<?}?>
			<li class="ui-state-default ui-state-disabled">COMICS</li>
			</ul>
		</div>

		<!-- //////////////////////// 전체순위 //////////////////////// -->

		<div class="comics_ranking">
		<!--<form name="comics_ranking_form" id="comics_ranking_form" method="post" action="<?=$_cms_folder_update;?>" onsubmit="return comics_ranking_submit();">-->
			<form name="comics_ranking_form" id="comics_ranking_form" method="post" action="<?=$small_update;?>" onsubmit="return comics_ranking_submit();">
					<input type="hidden" name="cw_class" id="cw_class" value="<?=$cw_class?>">

					<input type="hidden" name="s_text" id="s_text" value="<?=$s_text?>">

				<h3>
					<?=$page_title?>
					<input type="submit" class="" value="적용" id="ranking_submit">
				</h3>
				<ul id="sortable2" class="connectedSortable">
				<? $comics_ranking_no = 0;
				foreach($row_data_cw as $dvalue_key => $dvalue_val){
					if($dvalue_key == $show_list){ break; }
					$comics_ranking_no++;

					/* 컨텐츠 정보 */
					$get_comics = get_comics($dvalue_val['cw_comics']);
					
					/* 컨텐츠 제목 */
					$db_series = $get_comics['cm_series'];

					/* 이미지 표지 */
					$cm_cover_sub_url = img_url_para($get_comics['cm_cover_sub'], $get_comics['cm_reg_date'], $get_comics['cm_mod_date'],'cm_cover_sub','tn75x75');

					/* 컨텐츠 등급 */
					$db_adult = $d_adult[$get_comics['cm_adult']];

					/* 순위차이 */
					$db_ranking_gap = $dvalue_val['cw_ranking_gap'];
					if($dvalue_val['cw_ranking_gap'] == 0){ $db_ranking_gap = ''; }

					/* 순위표기 */
					$db_ranking_mark = $d_ranking_mark[$dvalue_val['cw_ranking_mark']];

					/* 순위표기 아이콘 */
					$db_ranking_mark_icon = $d_ranking_mark_icon[$dvalue_val['cw_ranking_mark']];

					/* 저장할 정보 */
					$db_cw_adult = $get_comics['cm_adult'];

					/* 컨텐츠 완결 */
					$db_cm_end = $d_cm_end[$get_comics['cm_end']];

					/* 컨텐츠 주기 */
					$db_cm_public_period = $d_cm_public_period[$get_comics['cm_public_period']];

					$db_cw_lock = $dvalue_val['cw_lock'];
					$db_cw_lock_icon = 'fa-unlock';
					if($db_cw_lock == 'y'){
						$db_cw_lock_icon = 'fa-lock';
					}
					$db_cw_comics = $dvalue_val['cw_comics'];
					$db_cw_big = $get_comics['cm_big'];

					$db_cw_ranking_gap = $dvalue_val['cw_ranking_gap'];
					$db_cw_ranking_mark = $dvalue_val['cw_ranking_mark'];				
				?>
					<li id="row_data_<?=$db_cw_comics?>" class="ui-state-default">
						<dl>
							<dt><img src="<?=$cm_cover_sub_url;?>" alt="<?= $db_series;?>표지" width="75" height="75" /></dt>
							<dd>
								<p class="bold">No<?=$comics_ranking_no;?> (등급: <?= $db_adult;?>) 
									<i class="fa fa-<?=$db_ranking_mark_icon?>" aria-hidden="true"></i>
									<span><?=$db_ranking_gap?></span>
								</p>
								<p><?= $db_series;?></p>
							<p>정보 : <?= $db_cm_end;?> / <?= $db_cm_public_period;?></p>
							</dd>
						</dl>
						<i class="fa fa-times-circle" aria-hidden="true"></i>
						<i class="ranking_lock fa <?=$db_cw_lock_icon?>" aria-hidden="true"></i>
						<input type="hidden" name="cw_adult[]" class="cw_adult" value="<?=$db_cw_adult?>">
						<input type="hidden" name="cw_lock[]" class="cw_lock" value="<?=$db_cw_lock?>">
						<input type="hidden" name="cw_comics[]" class="cw_comics" value="<?=$db_cw_comics?>">
						<input type="hidden" name="cw_big[]" class="cw_big" value="<?=$db_cw_big?>">

						<input type="hidden" name="cw_ranking_gap[]" class="cw_ranking_gap" value="<?=$db_cw_ranking_gap?>">
						<input type="hidden" name="cw_ranking_mark[]" class="cw_ranking_mark" value="<?=$db_cw_ranking_mark?>">
					</li>
				<?}?>
				<li class="ui-state-default ui-state-disabled">COMICS</li>
				</ul>
			</form>
		</div>
	</div>
</section><!-- comics_result -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>