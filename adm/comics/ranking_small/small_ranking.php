<?
include_once '_common.php'; // 공통

/* 공통 처리 */
$crs_class = $rank_cm_small;
$page_adult = '';
if($rank_cm_adult == 'n') { 
	$page_adult = '-청소년'; 
	$crs_class.= '_t';
}

$page_title = $nm_config['cf_small'][$rank_cm_small].$page_adult;
if($rank_cm_small == '0'){ $page_title = '전체'.$page_adult; }
$page_title.= ' 순위';


$where_small = '';
$where_crs_small = " AND crs_class='".$crs_class."' ";
if($rank_cm_small != '0'){
	$where_cm_small = " AND cm_small='".$rank_cm_small."' ";
}
$where_sales_r .= $where_small;

$where_crs_adult = $where_cm_adult = '';
if($rank_cm_adult == 'n'){
	$where_cm_adult = " AND cm_adult='n' ";
	$where_crs_adult = " AND crs_adult='n' ";
}

/* PARAMITER */

/* 데이터 가져오기 */
$where_sales = ""; /* 충전 */
$where_comics = "AND cm_service = 'y' ";

// 검색단어+ 검색타입
if($_s_text){
	switch($_s_type) {
		case 0 : 
			$where_sales.= "AND c.cm_series like '%$_s_text%' ";
			break;

		case 1 :
			$where_comics .= "AND cm_series like '%$_s_text%' ";
			break;
	} // end switch
} // end if

if($_s_type == "") {
	$_s_type = '0';
} // end if

$where_sales_r = $where_sales;

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }
if($_s_date && $_e_date){
	$where_sales.= date_year_month($_s_date, $_e_date, 'sl', $_s_hour, $_e_hour);
}


if($_sr_date == ''){ $_sr_date = NM_TIME_MON_M1_S; }
if($_er_date == ''){ $_er_date = NM_TIME_MON_M1_E; }
if($_sr_date && $_er_date){
	$where_sales_r.= date_year_month($_sr_date, $_er_date, 'sl', $_sr_hour, $_er_hour);	
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sl_pay_sum"){ 
	$_order_field = "sl_pay_sum"; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
}

$order_sales = "order by ".$_order_field." ".$_order." ";

$sql_sales = "";
$field_sales = "	sl_comics, 
							sum(sl_all_pay+sl_pay)as sl_pay_sum, 
							sum(sl_open)as sl_open_sum 
							"; // 가져올 필드 정하기
$group_sales = " group by sl_comics ";
$limit_sales = " LIMIT 0, 100 ";

$where_sales = '';



// 순위 통계 비교
$sql_r = "	select $field_sales 
						FROM sales sl
						JOIN comics c ON sl.sl_comics = c.cm_no 
						where 1 $where_sales_r $where_cm_adult $where_cm_small  
						$group_sales 
						$order_sales 
						$limit_sales";
$result_r = sql_query($sql_r);
$row_size_r = sql_num_rows($result_r);
$row_data_r  = array();
while ($row_r = mysql_fetch_array($result_r)) {
	array_push($row_data_r, $row_r);
}

if($s_type == '1') {
	$sql_sales = " select *, IF(cm_episode_date='',cm_reg_date, cm_episode_date )as content_date from comics where 1 $where_comics order by content_date DESC LIMIT 0, 100";
} else {
	$sql_sales = "	select $field_sales 
							FROM sales sl
							left JOIN comics c ON sl.sl_comics = c.cm_no 
							where 1 $where_cm_adult $where_cm_small 
							$group_sales 
							$order_sales 
							$limit_sales";
}
$result = sql_query($sql_sales);
$row_size = sql_num_rows($result);

// 순위 통계 비교 및 적용
$row_data  = array();
$no = 0;
$gap_p = 20;
$gap_m = -20;
//'n','ff','f','m','p','pp'
while ($row = sql_fetch_array($result)) {
	$no++;
	// 순위차이와 순위표기
	$crs_ranking_gap = '';
	$crs_ranking_mark = 'n';
	foreach($row_data_r as $row_r_key => $row_r_val){
		$rank = $row_r_key+1;
		if($row_r_val['sl_comics'] == $row['sl_comics']){
			$crs_ranking_gap = $rank - $no;
			if($crs_ranking_gap == 0){
				$crs_ranking_mark = 'm';
			}else if($crs_ranking_gap > 0){
				$crs_ranking_mark = 'f';
				if($crs_ranking_gap > $gap_p){ $crs_ranking_mark = 'ff'; }
			}else if($crs_ranking_gap < 0){
				$crs_ranking_mark = 'p';
				if($crs_ranking_gap < $gap_m){ $crs_ranking_mark = 'pp'; }
			}
		}
	}
	$row['crs_ranking_mark'] = $crs_ranking_mark;
	$row['crs_ranking_gap'] = $crs_ranking_gap;

	// 순위 제외시
	if($_s_type == '1') { $row['sl_comics'] = $row['cm_no']; }
	array_push($row_data, $row);
}

$where_rank = " AND crs_class='$crs_class' ";
if($rank_cm_adult == 'n'){
	$where_rank.= " AND crs_adult='n' ";
}

$sql_crs = "SELECT * FROM `comics_ranking_small` WHERE 1 $where_crs_small $where_crs_adult ORDER BY crs_ranking ASC LIMIT 100;";
$result_crs = sql_query($sql_crs);
$row_size_crs = sql_num_rows($result_crs);
$row_data_crs_comics  = $row_data_crs  = array();
while ($row_crs = mysql_fetch_array($result_crs)) {
	array_push($row_data_crs, $row_crs);
	array_push($row_data_crs_comics, $row_crs['crs_comics']);
}

// 보기 카운터
$show_list = 20;

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
// 페이지 처리 안함

$small_js = str_replace("cms_comics.js", "cms_comics_small.js", $_cms_js);
$small_update = str_replace("ranking_small.update.php", "small_ranking.update.php", $_cms_folder_update);
?>
<style type="text/css">
	#lnbs ul li:nth-child(<?=count($nm_config['cf_small'])+1?>) {clear: both;}
	#lnbs_end {margin-bottom: 54px;}
</style>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<!-- <script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script> -->
<script type="text/javascript" src="<?=$small_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> 이벤트 : 총 <?=number_format($total_epromotion);?> 건</strong>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="ranking_search_form" id="ranking_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return ranking_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	$s_type_list = array('순위 포함', '순위 제외');
					tag_radios($s_type_list, "s_type", $_s_type, 'n'); ?>
					<input type="text" id="s_text" name="s_text" placeholder="코믹스명을 입력해주세요." value="<?=$_s_text?>">
				</div>
				<div class="cs_calendar <?=$cs_calendar_on;?>">
					<h3>순위 통계 비교</h3>
					<div class="cs_calendar_btn">
						<label for="sr_date">시작일</label>
						<input type="text" name="sr_date" value="<?=$_sr_date;?>" id="sr_date" class="d_date readonly" readonly /> 
						<label for="sr_date_view">일</label>&nbsp;
						<?=set_hour('sr_hour', $_sr_hour, '시작시간');?>
					</div>
					<div class="cs_calendar_btn">
						<label for="er_date">종료일</label>
						<input type="text" name="er_date" value="<?=$_er_date;?>" id="er_date" class="d_date readonly" readonly />
						<label for="e_date_view">일</label>&nbsp;
						<?=set_hour('er_hour', $_er_hour, '종료시간');?>
					</div>
					
					<h3>순위 통계 등급</h3>
					<div class="cs_calendar_btn">
						<label for="s_date">시작일</label>
						<input type="text" name="s_date" value="<?=$_s_date;?>" id="s_date" class="d_date readonly" readonly /> 
						<label for="s_date_view">일</label>&nbsp;
						<?=set_hour('s_hour', $_s_hour, '시작시간');?>
					</div>
					<div class="cs_calendar_btn">
						<label for="e_date">종료일</label>
						<input type="text" name="e_date" value="<?=$_e_date;?>" id="e_date" class="d_date readonly" readonly />
						<label for="e_date_view">일</label>&nbsp;
						<?=set_hour('e_hour', $_e_hour, '종료시간');?>
					</div>
				</div>
			</div>
			<div class="cs_submit ranking_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="comics_result" class="clear">
	<div class="comics_ranking_list">


		<!-- //////////////////////// 순위 통계 비교 //////////////////////// -->


		<div class="comics_sales">
		<h3>순위 통계 비교</h3>
			<ul id="sortable0" class="connectedSortable">
			<? $comics_ranking_no = 0;
			foreach($row_data_r as $dvalue_r_key => $dvalue_r_val){
				if($dvalue_r_key == $show_list){ break; }
				$comics_ranking_no++;

				/* 컨텐츠 정보 */
				$get_comics_r = get_comics($dvalue_r_val['sl_comics']);
				
				/* 컨텐츠 제목 */
				$db_series_r = $get_comics_r['cm_series'];

				/* 이미지 표지 */
				$cm_cover_sub_url_r = img_url_para($get_comics_r['cm_cover_sub'], $get_comics_r['cm_reg_date'], $get_comics_r['cm_mod_date'],75,75);

				/* 컨텐츠 등급 */
				$db_adult_r = $d_adult[$get_comics_r['cm_adult']];
			?>
				<li class="ui-state-default">
					<dl>
						<dt><img src="<?=$cm_cover_sub_url_r;?>" alt="<?= $db_series_r;?>표지" width="75" height="75" /></dt>
						<dd>
							<p class="bold"><?=$comics_ranking_no;?>위 (등급: <?= $db_adult_r;?>)</p>
							<p><?= $db_series_r;?></p>
						</dd>
					</dl>
				</li>
			<?}?>
			<li class="ui-state-default ui-state-disabled">COMICS</li>
			</ul>
		</div>

		<!-- //////////////////////// 순위 통계 등급 //////////////////////// -->

		<div class="comics_sales">
		<h3>순위 통계 등급</h3>
			<ul id="sortable1" class="connectedSortable">
			<? $comics_ranking_no = 0;
			foreach($row_data as $dvalue_key => $dvalue_val){
				if($dvalue_key == $show_list){ break; }
				$comics_ranking_no++;

				/* 컨텐츠 정보 */
				$get_comics = get_comics($dvalue_val['sl_comics']);
				
				/* 컨텐츠 제목 */
				$db_series = $get_comics['cm_series'];

				/* 이미지 표지 */
				$cm_cover_sub_url = img_url_para($get_comics['cm_cover_sub'], $get_comics['cm_reg_date'], $get_comics['cm_mod_date'],75,75);

				/* 컨텐츠 등급 */
				$db_adult = $d_adult[$get_comics['cm_adult']];

				/* 순위차이 */
				$db_ranking_gap = $dvalue_val['crs_ranking_gap'];

				/* 순위표기 */
				$db_ranking_mark = $d_ranking_mark[$dvalue_val['crs_ranking_mark']];

				/* 순위표기 아이콘 */
				$db_ranking_mark_icon = $d_ranking_mark_icon[$dvalue_val['crs_ranking_mark']];

				/* 저장할 정보 */
				$db_crs_adult = $get_comics['cm_adult'];
				$db_crs_lock = 'n';
				$db_crs_comics = $dvalue_val['sl_comics'];
				$db_crs_big = $get_comics['cm_big'];

				$db_crs_ranking_gap = $dvalue_val['crs_ranking_gap'];
				if($dvalue_val['crs_ranking_gap'] == 0){ $db_crs_ranking_gap = ''; }
				$db_crs_ranking_mark = $dvalue_val['crs_ranking_mark'];

				/* 전체 순위 있는 건 고정 */
				$ui_state_disabled = "";
				if(in_array($db_crs_comics, $row_data_crs_comics)){
					$ui_state_disabled = "ui-state-disabled";					
				}
				
			?>
				<li id="row_data_<?=$db_crs_comics?>" class="ui-state-default <?=$ui_state_disabled;?>">
					<dl>
						<dt><img src="<?=$cm_cover_sub_url;?>" alt="<?= $db_series;?>표지" width="75" height="75" /></dt>
						<dd>
							<p class="bold"><?=$comics_ranking_no;?>위 (등급: <?= $db_adult;?>) 
								<i class="fa fa-<?=$db_ranking_mark_icon?>" aria-hidden="true"></i>
								<span><?=$db_ranking_gap?></span>
							</p>
							<p><?= $db_series;?></p>
						</dd>
					</dl>
					<i class="fa fa-times-circle" aria-hidden="true"></i>
					<i class="ranking_lock fa fa-unlock" aria-hidden="true"></i>
					<input type="hidden" name="crs_adult[]" class="crs_adult" value="<?=$db_crs_adult?>">
					<input type="hidden" name="crs_lock[]" class="crs_lock" value="<?=$db_crs_lock?>">
					<input type="hidden" name="crs_comics[]" class="crs_comics" value="<?=$db_crs_comics?>">
					<input type="hidden" name="crs_big[]" class="crs_big" value="<?=$db_crs_big?>">

					<input type="hidden" name="crs_ranking_gap[]" class="crs_ranking_gap" value="<?=$db_crs_ranking_gap?>">
					<input type="hidden" name="crs_ranking_mark[]" class="crs_ranking_mark" value="<?=$db_crs_ranking_mark?>">
				</li>
			<?}?>
			<li class="ui-state-default ui-state-disabled">COMICS</li>
			</ul>
		</div>

		<!-- //////////////////////// 전체순위 //////////////////////// -->

		<div class="comics_ranking">
		<!--<form name="comics_ranking_form" id="comics_ranking_form" method="post" action="<?=$_cms_folder_update;?>" onsubmit="return comics_ranking_submit();">-->
			<form name="comics_ranking_form" id="comics_ranking_form" method="post" action="<?=$small_update;?>" onsubmit="return comics_ranking_submit();">
					<input type="hidden" name="crs_class" id="crs_class" value="<?=$crs_class?>">

					<input type="hidden" name="s_text" id="s_text" value="<?=$s_text?>">

					<input type="hidden" name="sr_date" id="sr_date" value="<?=$sr_date;?>">
					<input type="hidden" name="sr_hour" id="sr_hour" value="<?=$sr_hour?>">
					<input type="hidden" name="er_date" id="er_date" value="<?=$er_date?>">
					<input type="hidden" name="er_hour" id="er_hour" value="<?=$er_hour?>">

					<input type="hidden" name="s_date" id="s_date" value="<?=$s_date?>">
					<input type="hidden" name="s_hour" id="s_hour" value="<?=$s_hour?>">
					<input type="hidden" name="e_date" id="e_date" value="<?=$e_date?>">
					<input type="hidden" name="e_hour" id="e_hour" value="<?=$e_hour?>">

				<h3>
					<?=$page_title?>
					<input type="submit" class="" value="적용" id="ranking_submit">
				</h3>
				<ul id="sortable2" class="connectedSortable">
				<? $comics_ranking_no = 0;
				foreach($row_data_crs as $dvalue_key => $dvalue_val){
					if($dvalue_key == $show_list){ break; }
					$comics_ranking_no++;

					/* 컨텐츠 정보 */
					$get_comics = get_comics($dvalue_val['crs_comics']);
					
					/* 컨텐츠 제목 */
					$db_series = $get_comics['cm_series'];

					/* 이미지 표지 */
					$cm_cover_sub_url = img_url_para($get_comics['cm_cover_sub'], $get_comics['cm_reg_date'], $get_comics['cm_mod_date'],'cm_cover_sub','tn75x75');

					/* 컨텐츠 등급 */
					$db_adult = $d_adult[$get_comics['cm_adult']];

					/* 순위차이 */
					$db_ranking_gap = $dvalue_val['crs_ranking_gap'];
					if($dvalue_val['crs_ranking_gap'] == 0){ $db_ranking_gap = ''; }

					/* 순위표기 */
					$db_ranking_mark = $d_ranking_mark[$dvalue_val['crs_ranking_mark']];

					/* 순위표기 아이콘 */
					$db_ranking_mark_icon = $d_ranking_mark_icon[$dvalue_val['crs_ranking_mark']];

					/* 저장할 정보 */
					$db_crs_adult = $get_comics['cm_adult'];

					$db_crs_lock = $dvalue_val['crs_lock'];
					$db_crs_lock_icon = 'fa-unlock';
					if($db_crs_lock == 'y'){
						$db_crs_lock_icon = 'fa-lock';
					}
					$db_crs_comics = $dvalue_val['crs_comics'];
					$db_crs_big = $get_comics['cm_big'];

					$db_crs_ranking_gap = $dvalue_val['crs_ranking_gap'];
					$db_crs_ranking_mark = $dvalue_val['crs_ranking_mark'];				
				?>
					<li id="row_data_<?=$db_crs_comics?>" class="ui-state-default">
						<dl>
							<dt><img src="<?=$cm_cover_sub_url;?>" alt="<?= $db_series;?>표지" width="75" height="75" /></dt>
							<dd>
								<p class="bold"><?=$comics_ranking_no;?>위 (등급: <?= $db_adult;?>) 
									<i class="fa fa-<?=$db_ranking_mark_icon?>" aria-hidden="true"></i>
									<span><?=$db_ranking_gap?></span>
								</p>
								<p><?= $db_series;?></p>
							</dd>
						</dl>
						<i class="fa fa-times-circle" aria-hidden="true"></i>
						<i class="ranking_lock fa <?=$db_crs_lock_icon?>" aria-hidden="true"></i>
						<input type="hidden" name="crs_adult[]" class="crs_adult" value="<?=$db_crs_adult?>">
						<input type="hidden" name="crs_lock[]" class="crs_lock" value="<?=$db_crs_lock?>">
						<input type="hidden" name="crs_comics[]" class="crs_comics" value="<?=$db_crs_comics?>">
						<input type="hidden" name="crs_big[]" class="crs_big" value="<?=$db_crs_big?>">

						<input type="hidden" name="crs_ranking_gap[]" class="crs_ranking_gap" value="<?=$db_crs_ranking_gap?>">
						<input type="hidden" name="crs_ranking_mark[]" class="crs_ranking_mark" value="<?=$db_crs_ranking_mark?>">
					</li>
				<?}?>
				<li class="ui-state-default ui-state-disabled">COMICS</li>
				</ul>
			</form>
		</div>
	</div>
</section><!-- comics_result -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>