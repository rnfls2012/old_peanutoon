<?
include_once '_common.php'; // 공통
/* _array_update.php 처리 불가능 */

/* PARAMITER CHECK */
$para_list = array('ct_class');
array_push($para_list, 'ct_adult', 'ct_lock', 'ct_comics', 'ct_big');
array_push($para_list, 'ct_ranking_gap', 'ct_ranking_mark');
array_push($para_list, 'ct_ranking', 'ct_date');

array_push($para_list, 's_text');


/* 숫자 PARAMITER 체크 */
$para_num_list = array();

/* 체크박스배열 PARAMITER 체크 */
$checkbox_list = array();

/* 빈칸 PARAMITER 허용 */
$null_list = array();


/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
foreach($para_list as $para_key => $para_val){
	if(in_array($para_val,$para_num_list)){
		if(num_check($_POST[$para_val]) == false && $_POST[$para_val] != '' && $_POST[$para_val] != '0'){
			alert($para_val."값에 숫자가 아닌 값이 들어 있습니다.", $_SERVER['HTTP_REFERER']);
			die;
		}
	}
	if(is_array($_POST[$para_val])){
		${'_'.$para_val} = $_POST[$para_val]; /* 변수담기 */
	}else{
		${'_'.$para_val} = base_filter($_POST[$para_val]); /* 변수담기 */
	}
}

/* DB field 아닌 목록 */
$db_field_exception = array(); 
array_push($db_field_exception, 's_text');

$dbtable = 'comics_top';
// 데이터 초기화

if($_ct_class != ''){
	$sql_delete = " DELETE FROM ".$dbtable." WHERE ct_class='$_ct_class' ";
	sql_query($sql_delete);
}

for ($i=0; $i<count($_POST['ct_comics']); $i++) {
	// 랭킹 넘버링
	$_ct_ranking = $i+1;
	$_ct_date = NM_TIME_YMDHIS; /* 저장일 */

	/* field명 */
	$sql_reg = 'INSERT INTO '.$dbtable.'(';
	foreach($para_list as $para_key => $para_val){
		if(in_array($para_val, $db_field_exception)){continue;} /* DB field 아닌 목록 제외 */
		if(${'_'.$para_val} == '' && in_array($para_val, $null_list) == false){ continue; } /* DB field에 값 없는거 빼기 */

		/* sql문구 */
		$sql_reg.= $para_val.', ';
	}
	$sql_reg = substr($sql_reg,0,strrpos($sql_reg, ","));

	/* VALUES 시작 */
	$sql_reg.= ' )VALUES( ';

	/* field값 */
	foreach($para_list as $para_key => $para_val){
		if(in_array($para_val, $db_field_exception)){continue;} /* DB field 아닌 목록 제외 */
		if(${'_'.$para_val} == '' && in_array($para_val, $null_list) == false){ continue; } /* DB field에 값 없는거 빼기 */

		/* sql문구 */

		if($para_val == 'ct_ranking_gap' && ${'_'.$para_val}[$i] == ''){ ${'_'.$para_val}[$i] = 0; } // 순위차이
			
		if(is_array(${'_'.$para_val})){
			$sql_reg.= '"'.${'_'.$para_val}[$i].'", ';
		}else{
			$sql_reg.= '"'.${'_'.$para_val}.'", ';
		}
	}
	$sql_reg = substr($sql_reg,0,strrpos($sql_reg, ","));

	/* SQL문 마무리 */

	$sql_reg.= ' ) ';

	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = $_cm_series[$i].'의 데이터가 등록되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '등록 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}
}

$ct_class_para = $ct_class;
// auto도 수정하기
// define('_SH_DAY_FOUR_TEN_', true);
// include_once NM_PATH.'/sh/everyday_four_ten/ranking.php'; // 공통


goto_url(NM_ADM_URL."/comics/cmtop/cmtop_".$ct_class_para.".php?s_text=$s_text");
?>