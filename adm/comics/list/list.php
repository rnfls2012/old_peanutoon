<?
include_once '_common.php'; // 공통

/* PARAMITER 
big //대분류
cm_small //대분류
cm_publisher // 출판사번호
cm_professional // 작가번호
cm_nos //컨텐츠번호들[체크해서 넣은 값 123,546,579 이런식으로 넘겨버리자]
s_type //검색 타입 -> 1전체, 2작품명, 3작가, 4출판사
s_text //검색 단어
order_field //컨텐츠정렬필드
order //정렬순서
date_type //날짜타입
s_date //날짜-시작
e_date //날짜-끝
s_limit //보여줄 갯수
s_cm_end_end //완료여부
*/

// 대분류 파일제어
$sql_big = $sql_cbig = $th_big_text = "";
if($cm_big !=''){ $sql_big = "and cm_big=$cm_big "; $sql_cbig = " and cm_big=$cm_big "; }
else{$th_big_text = "<span class='big_list'>대분류</span>/";}

// 기본적으로 몇개 있는 지 체크
$comics_where = $sql_big." and cm_service='y' ";
$comics_cm_service_total_sql = "select count(*) as comics_total from comics where 1 $comics_where ";
$comics_cm_service_total = sql_count($comics_cm_service_total_sql, 'comics_total');

// 기본적으로 몇개 있는 지 체크
$comics_where = $sql_big." and cm_service='n' ";
$comics_stop_total_sql = "select count(*) as comics_total from comics where 1 $comics_where ";
$comics_stop_total = sql_count($comics_stop_total_sql, 'comics_total');

/* JOIN */
$comics_join = "	from comics c 
					left JOIN comics_publisher c_publ ON c_publ.cp_no = c.cm_publisher 
					left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
					left JOIN comics_professional c_prof_sub ON c_prof_sub.cp_no = c.cm_professional_sub 
					left JOIN comics_professional c_prof_thi ON c_prof_thi.cp_no = c.cm_professional_thi 
					left JOIN comics_provider c_prov ON c_prov.cp_no = c.cm_provider 
					left JOIN comics_provider c_prov_sub ON c_prov_sub.cp_no = c.cm_provider_sub 
";

/* 데이터 가져오기 */
$comics_where = $sql_cbig;

// 장르, 작가번호, 출판사번호
$nm_paras_check = array('cm_small', 'cm_professional', 'cm_publisher');
foreach($nm_paras_check as $nm_paras_key => $nm_paras_val){
	if(${"_".$nm_paras_val} && ${"_".$nm_paras_val} > 0){
		$comics_where.= "and c.".$nm_paras_val." = ".${"_".$nm_paras_val}." ";
	}
}

// 검색단어+ 검색타입
if($_s_text){
	switch($_s_type){
		case '0': $comics_where.= "and c.cm_series like '%$_s_text%' ";
		break;
		case '1': $comics_where.= "and ( c_prof.cp_name like '%$_s_text%' or 
										 c_prof_sub.cp_name like '%$_s_text%' or 
										 c_prof_thi.cp_name like '%$_s_text%' ) ";
		break;

		case '2': $comics_where.= "and ( c_publ.cp_name like '%$_s_text%' ) ";
		break;

		case '3': $comics_where.= "and ( c_prov.cp_name like '%$_s_text%' or 
										 c_prov_sub.cp_name like '%$_s_text%' ) ";
		break;

		default: $comics_where.= "and ( c.cm_series like '%$_s_text%' or 
										c_prof.cp_name like '%$_s_text%' or 
										c_prof_sub.cp_name like '%$_s_text%' or 
										c_prof_thi.cp_name like '%$_s_text%' or 
										
										c_publ.cp_name like '%$_s_text%' or
										
										c_prov.cp_name like '%$_s_text%' or 
										c_prov_sub.cp_name like '%$_s_text%' ) ";
		break;
	}
}

// 완결구분
if($_cm_end != ""){
	$comics_where.= "and c.cm_end = '$_cm_end'";
}

// 서비스 구분
if($_cm_service != ""){
	$comics_where.= "and c.cm_service = '$_cm_service'";
}

// 성인 구분
if($_cm_adult != ""){
	$comics_where.= "and c.cm_adult = '$_cm_adult'";
}

// 최초/최신등록일 날짜
$start_date = $end_date = "";
if($_date_type){ //all, cm_reg_date, cm_episode_date
	if($_s_date && $_e_date){
		$start_date = $_s_date." ".NM_TIME_HI_start;
		$end_date = $_e_date." ".NM_TIME_HI_end;
	}else if($_s_date){
		$start_date = $_s_date." ".NM_TIME_HI_start;
		$end_date = NM_TIME_YMD." ".NM_TIME_HI_end;
	}
	if($start_date && $end_date){
		switch($_date_type){
			case 'all' : $comics_where.= "and ( c.cm_episode_date <= '$end_date' and c.cm_episode_date >= '$start_date' or c.cm_reg_date <= '$end_date' and c.cm_reg_date >= '$start_date') ";
			break;

			case 'cm_reg_date' : $comics_where.= "and c.cm_reg_date <= '$end_date' and c.cm_reg_date >= '$start_date' ";
			break;

			case 'cm_episode_date' : $comics_where.= "and c.cm_episode_date <= '$end_date' and c.cm_episode_date >= '$start_date' ";
			break;

			default: $comics_where.= " ";
			break;
		}
	}
}else{
	$_s_date = "";
	$_e_date = "";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "cm_reg_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$comics_order = "order by c.".$_order_field." ".$_order;

$comics_sql = "";

// 가져올 필드 정하기
$comics_field = "	c.*, 
					c_publ.cp_name as publ_name, 
					c_prof.cp_name as prof_name, c_prof_sub.cp_name as prof_name_sub, c_prof_thi.cp_name as prof_name_thi,
					c_prov.cp_name as prov_name, c_prov_sub.cp_name as prov_name_sub "; 
$comics_limit = "";

$comics_sql = "select $comics_field $comics_join where 1 $comics_where $comics_order $comics_limit";
$result = sql_query($comics_sql);
$row_size = sql_num_rows($result);

/*
echo "<br/>";
echo $comics_sql;
echo "<br/>";
*/

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','cm_no',0));
array_push($fetch_row, array('썸네일','cm_cover_sub',0));
array_push($fetch_row, array('작품명<br/>작가표기','cm_series',0));
array_push($fetch_row, array('장르<br/>출판사','cm_publisher',0));
array_push($fetch_row, array('작가','cm_professional',0));
array_push($fetch_row, array('제공사','cm_provider',0));
array_push($fetch_row, array('서비스<br/>완결구분','cm_service',0));
array_push($fetch_row, array('코믹스 등록일<br/>화 최신등록일','cm_date',0));
array_push($fetch_row, array('관리','cm_modify',0));
array_push($fetch_row, array('화 등록<br/>총 화수','cm_episode',0));

$cs_calendar_on = $cs_submit_h = '';
if($_date_type){
	$cs_calendar_on = 'cs_calendar_on';
	$cs_submit_h = 'cs_submit_on';
}

$cms_head_title = "컨텐츠 정보";
if($cm_big != ''){ $cms_head_title = $nm_config['cf_big'][$cm_big]; }

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>서비스 중인 컨텐츠 : <?=number_format($comics_cm_service_total);?> 건</strong>
		<strong> + </strong>
		<strong>중지인 컨텐츠 : <?=number_format($comics_stop_total);?> 건</strong>
		<strong> = </strong>
		<strong>총 컨텐츠 : <?=number_format($comics_cm_service_total+$comics_stop_total);?> 건</strong>
	</div>
	<div class="write">
		<? /* 상단 페이지 라면 버튼 전부 출력 */
		foreach($nm_config['cf_big'] as $big_key => $big_val){ 
			if($big_key == 0 || ($cm_big != '' && $cm_big != $big_key)){ continue; } ?>
			<button onclick="popup('<?=$_cms_folder_write;?>?big=<?=$big_key?>','book_wirte', <?=$popup_cms_width;?>, 900);"><?=$big_val?> 등록</button>
		<?} /* foreach($nm_config['cf_big'] as $big_key => $big_val){  */ /* if($cm_big != ''){ */ ?>
		<!-- <button class="excel_btn" onclick="location.href='../excel/list_excel.php?comics_where=<?=addslashes($comics_where);?>&comics_order=<?=$comics_order;?>'">엑셀 다운로드</button> 180205 임시 수정-->
		<button class="excel_btn" onclick="location.href='../excel/list_excel.php?comics_where=<?=urlencode($comics_where);?>&comics_order=<?=urlencode($comics_order);?>'">엑셀 다운로드</button>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="cms_search_form" id="cms_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return cms_search_submit();">
		<input type="hidden" name="big" id="big" value="<?=$cm_big?>">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_s_type_btn">
				<?	$s_type_list = array('작품명','작가','출판사');
					tag_radios($s_type_list, "s_type", $_s_type, 'y', '전체', ''); ?>
				</div>
				<div class="cs_search_btn">
					<?	tag_selects($nm_config['cf_small'], "cm_small", $_cm_small, 'y', '장르 전체', ''); ?>
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
				<div class="cs_date_btn">
					<?	tag_selects($d_cm_end, "cm_end", $_cm_end, 'y', '완결구분 전체', ''); ?>
					<?	tag_selects($d_cm_service, "cm_service", $_cm_service, 'y', '서비스구분 전체', ''); ?>
					<?	tag_selects($d_adult, "cm_adult", $_cm_adult, 'y', '성인구분 전체', ''); ?>
				</div>
				<div class="cs_date_btn">
					<?	tag_selects($d_order, "order", $_order, 'n'); ?>
					<?	tag_selects($d_cm_date_type, "date_type", $_date_type, 'y', '최초/최신등록일 검색안함', ''); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="72" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="comics_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 서비스여부 */
					$cm_service = "제공";
					if($dvalue_val['cm_service']== 'n'){ $cm_service = "중지";}
					elseif ($dvalue_val['cm_service'] == 'r') { $cm_service = "예약";}

					/* 완결여부 */
					$cm_end = "완결";
					if($dvalue_val['cm_end']== 'n'){ $cm_end = "연재";}

					/* 이미지 표지 */
					$cm_cover_sub_url = img_url_para($dvalue_val['cm_cover_sub'], $dvalue_val['cm_reg_date'], $dvalue_val['cm_mod_date'],'cm_cover_sub','tn75x75');

					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_folder_write."?big=".$dvalue_val['cm_big']."&comics=".$dvalue_val['cm_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					/* 컨텐츠 화 등록/리스트보기 */
					$popup_episode_reg_url = $_cms_folder."/episode.write.php?big=".$dvalue_val['cm_big']."&comics=".$dvalue_val['cm_no'];
					$popup_episode_list_url = $_cms_folder."/episode.list.php?big=".$dvalue_val['cm_big']."&comics=".$dvalue_val['cm_no'];

					/* 대분류 */
					$row_big_text = '';
					if($th_big_text != ''){ $row_big_text = "<p class='big_list'>".$nm_config['cf_big'][$dvalue_val['big']]."</p>"; }

					/* 등록일 */
					$db_cm_reg_date_ymd = get_ymd($dvalue_val['cm_reg_date']);
					$db_cm_reg_date_his = get_his($dvalue_val['cm_reg_date']);

					/* 화등록일 */
					$db_cm_episode_date_ymd = get_ymd($dvalue_val['cm_episode_date']);
					$db_cm_episode_date_his = get_his($dvalue_val['cm_episode_date']);
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center">
						<?=$dvalue_val['cm_no'];?>
					</td>
					<td class="<?=$fetch_row[1][1]?>">
						<img src="<?=$cm_cover_sub_url;?>" width="75" height="75" alt="<?= $dvalue_val['cm_series'];?>표지"/>
					</td>
					<td class="<?=$fetch_row[2][1]?>">
						<a href="<?=NM_URL?>/comics.php?comics=<?=$dvalue_val['cm_no'];?>"><?=$row_big_text?><?=$dvalue_val['cm_series'];?></a>
						<? echo $dvalue_val['cm_professional_info']==''?"":"<p>".$dvalue_val['cm_professional_info']."</p>";?>
					</td>
					<td class="<?=$fetch_row[3][1]?> text_center">
						<?=$nm_config['cf_small'][$dvalue_val['cm_small']];?><br/>
						<?=$dvalue_val['publ_name'];?>
					</td>
					<td class="<?=$fetch_row[4][1]?> text_center">
						<?=$dvalue_val['prof_name'];?>
						<? echo $dvalue_val['prof_name_sub']==''?"":"<br/>".$dvalue_val['prof_name_sub'];?>
						<? echo $dvalue_val['prof_name_thi']==''?"":"<br/>".$dvalue_val['prof_name_thi'];?>
					</td>
					<td class="<?=$fetch_row[5][1]?> text_center">
						<?=$dvalue_val['prov_name'];?>
						<? echo $dvalue_val['prov_name_sub']==''?"":"<br/>".$dvalue_val['prov_name_sub'];?>
					</td>
					<td class="<?=$fetch_row[6][1]?> text_center">
						<?=$cm_service;?><br/>						
						<?=$cm_end?>					
					</td>
					<td class="<?=$fetch_row[7][1]?> text_center">
						<?=$db_cm_reg_date_ymd;?> <?=$db_cm_reg_date_his;?><br/>
						<?=$db_cm_episode_date_ymd;?> <?=$db_cm_episode_date_his;?>
					</td>
					<td class="<?=$fetch_row[8][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','book_mod_wirte', <?=$popup_cms_width;?>, 900);">수정</button>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','book_del_wirte', <?=$popup_cms_width;?>, 400);">삭제</button>
					</td>
					<td class="<?=$fetch_row[9][1]?> text_center">		
						<button class="reg_btn" onclick="popup('<?=$popup_episode_reg_url;?>','book_episode_reg_wirte', <?=$popup_cms_width;?>, 700);">(화) 등록</button><br/>
						<button class="list_btn" onclick="popup('<?=$popup_episode_list_url;?>','book_episode_list', <?=$popup_cms_width;?>, 900);"><?=number_format($dvalue_val['cm_episode_total']);?> 화</button>				
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- cms_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>