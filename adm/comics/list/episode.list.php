<?
include_once '_common.php'; // 공통

/* episode_db */
$episode_db = $nm_config['episode_db'][$_big];

$sql_comics = "SELECT * FROM comics WHERE `cm_no` = '$_comics' ";
$row_comics = sql_fetch($sql_comics);

$sql_episode = "SELECT * FROM comics_episode_".$_big." WHERE `ce_comics` = '$_comics' order by ce_order desc ";
$result = sql_query($sql_episode);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('정렬순서','ce_order',0));
array_push($fetch_row, array('정렬버튼','ce_order_btn',0));
array_push($fetch_row, array('화-차수<br/>or<br/>화이름','ce_chapter',0));
array_push($fetch_row, array('화<br/>배너이미지','ce_cover',0));
array_push($fetch_row, array('작품명','ce_title',1));
array_push($fetch_row, array('보기조건','ce_login',2));

array_push($fetch_row, array('서비스상태','ce_service_state',1));
array_push($fetch_row, array('서비스날짜','ce_service_date',2));

array_push($fetch_row, array('무료상태','ce_free_state',1));
array_push($fetch_row, array('무료날짜','ce_free_date',2));

array_push($fetch_row, array('가격 단위:'.$nm_config['cf_cach_point_unit'].'','ce_pay',1));
array_push($fetch_row, array('화 파일수','ce_file_count',2));

array_push($fetch_row, array('등록날짜','ce_reg_date',1));
array_push($fetch_row, array('수정날짜','ce_mod_date',2));

array_push($fetch_row, array('관리','ce_management',0));



/* 대분류로 제목 */
$cms_head_title = $row_comics['cm_series'];
$mode_text = "리스트";

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
if($_s_limit == ''){ $_s_limit = 10; }

include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()

?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<script type="text/javascript" src="<?=NM_URL;?>/js/jquery.dragsort.js"></script><!-- dragsort -->

<div class="flex_view flex_view_title">
	<section id="cms_page_title" class="episode_list_title">
		<h1><?=$cms_head_title;?> (화) <?=$mode_text;?></h1>
		<div id="s_limit_select">
			<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
		</div>
	</section><!-- flieupload_head -->
</div>

<section id="episode_list">
	<form name="episode_list_form" id="episode_list_form" enctype="multipart/form-data" method="post" action="<?=$_cms_folder;?>/episode.listupdate.php" onsubmit="return episode_list_submit();">
		<input type="hidden" id="mode" name="mode" value="chapter_list"/><!-- 입력모드 -->
		<input type="hidden" id="big" name="big" value="<?=$_big?>"/><!-- 대분류 -->
		<input type="hidden" id="ce_comics" name="ce_comics" value="<?=$_comics;?>"/><!-- 수정/삭제시 컨텐트번호 -->

		<div id="fr_bg">
			<div class="flex_view flex_view_head">
				<ul class="chapter_list">
					<li class="chapter_head">
						<ul>
						<? foreach($fetch_row as $fetch_key => $fetch_val){
							if($fetch_val[2] == 1){
								$fetch_name = $fetch_val[0];
								$fetch_class = $fetch_val[1];
								continue;
							}else if($fetch_val[2] == 2){
								$fetch_name.= "<p>".$fetch_val[0]."</p>";
							}else{
								$fetch_name = $fetch_val[0];
								$fetch_class = $fetch_val[1];
							}
							?>
							<li class="<?=$fetch_class?>"><div><?=$fetch_name?></div></li>
						<? } /* foreach($fetch_row as $dhead_key => $dhead_val){ end */ ?>
						</ul>
					</li>
				</ul>
			</div>
			<ul id="chapter_list_s" class="chapter_list flex_view_next">
				<? $episode_no_list = $ce_order_list = "";
				   foreach($row_data as $dvalue_key => $dvalue_val){
						$episode_no_list.= $dvalue_val['ce_no'].","; /* 화 DB 번호 리스트 */
						$ce_order_list.= $dvalue_val['ce_order'].",";	 /* 화 정렬순서 리스트 */					

						/* 번호 마이너스일때 화이름으로 넣기 */
						$row_ce_chapter = $dvalue_val['ce_chapter'];
						if($row_ce_chapter < 1){ $row_ce_chapter = $dvalue_val['ce_notice']; }
						if($dvalue_val['ce_outer'] =='y'){$row_ce_chapter = "외전".$dvalue_val['ce_outer_chapter'] ;}
						/* 썸네일 */
						$row_banner_img = "<p class='noimage'>No Image</p>";
						if($dvalue_val['ce_cover'] != ''){
							$banner_url = img_url_para($dvalue_val['ce_cover'], $dvalue_val['ce_reg_date'], $dvalue_val['ce_mod_date'], 76, 76);
							$banner_alt = $dvalue_val['ce_title'];
							if($banner_alt == ""){ $banner_alt = $row_comics['cm_series']; }
							$row_banner_img = "<img src='".$banner_url."' alt='".$banner_alt."표지'  width='76' height='76'/>";
						}
						/* 서비스 상태 */
						$row_ce_service_state = $row_service_date = "";
						switch($dvalue_val['ce_service_state']){
							case 'y': $row_ce_service_state = "서비스 제공"; $row_service_date = $dvalue_val['ce_service_date'];  break;
							case 'n': $row_ce_service_state = "서비스 중지(소장제공)"; $row_service_date = $dvalue_val['ce_service_stop_date'];  break;
							case 'r': $row_ce_service_state = "서비스 예약함"; $row_service_date = $dvalue_val['ce_service_date'];  break;
							case 'x': $row_ce_service_state = "서비스 완전중지"; $row_service_date = $dvalue_val['ce_service_date'];  break;
							default: $row_ce_service_state = "이용에러(확인요망)"; $row_service_date = "서비스 time error";  break;

						}
						/* 무료 상태 */
						$row_ce_free_state = $row_free_date = "";
						switch($dvalue_val['ce_free_state']){
							case 'y': $row_ce_free_state = "무료제공"; $row_free_date = $dvalue_val['ce_free_date'];  break;
							case 'n': $row_ce_free_state = "무료안함"; $row_free_date = $dvalue_val['ce_free_date'];  break;
							case 'r': $row_ce_free_state = "무료제공예약"; $row_free_date = $dvalue_val['ce_free_date'];  break;
							default: $row_ce_free_state = "무료에러(확인요망)"; $row_free_date = "무료 time error";  break;

						}
						/* 작품명 */
						$row_series = $dvalue_val['ce_title'];
						if($row_series == ''){$row_series= "작품명:無";}
						/* 보기조건 */
						$row_ce_login = "";
						switch($dvalue_val['ce_login']){
							case 'y': $row_ce_login = "로그인 후 보기"; break;
							case 'n': $row_ce_login = "로그인 전 보기"; break;
							default: $row_ce_login = "로그에러(확인요망)"; break;

						}
						$popup_chapter_url = $_cms_folder."/episode.write.php?big=".$row_comics['cm_big']."&comics=".$dvalue_val['ce_comics']."&episode=".$dvalue_val['ce_no'];
						$popup_chapter_mod_url = $popup_chapter_url."&mode=mod";
						$popup_chapter_del_url = $popup_chapter_url."&mode=del";

						$ce_chapter_url = get_episode_url($dvalue_val['ce_comics'], $dvalue_val['ce_no']);

						$row_service_date_ymd = get_ymd($row_service_date);
						$row_service_date_his = get_his($row_service_date);

						$db_ce_reg_date_ymd = get_ymd($dvalue_val['ce_reg_date']);
						$db_ce_reg_date_his = get_his($dvalue_val['ce_reg_date']);

						$db_ce_mod_date_ymd = get_ymd($dvalue_val['ce_mod_date']);
						$db_ce_mod_date_his = get_his($dvalue_val['ce_mod_date']);

						?>
				<li class="chapter_episode result_hover">
					<div class="chapter_episode_bg">
						<ul>
							<li class="<?=$fetch_row[0][1]?>" data-ce_order="<?=$dvalue_val['ce_order'];?>"><?=$dvalue_val['ce_order'];?></li>
							<li class="<?=$fetch_row[1][1]?>"><img src="<?=NM_IMG?>cms/chapter_order.png" alt="신 정렬이동" /></li>
							<li class="<?=$fetch_row[2][1]?>" data-ce_chapter = "<?=$dvalue_val['ce_no'];?>"><a href="<?=$ce_chapter_url;?>"><?=$row_ce_chapter;?></a></li>
							<li class="<?=$fetch_row[3][1]?>"><?=$row_banner_img;?></li>
							<li class="<?=$fetch_row[4][1]?>"><?=$row_series;?><p><?= $row_ce_login;?></p></li>
							<li class="<?=$fetch_row[6][1]?>"><?=$row_ce_service_state;?><p><?=$row_service_date_ymd;?><br/><?=$row_service_date_his;?></p></li>
							<li class="<?=$fetch_row[8][1]?>"><?=$row_ce_free_state;?><p><?=$row_free_date;?></p></li>
							<li class="<?=$fetch_row[10][1]?>"><?=cash_point_view($dvalue_val['ce_pay']);?><p><?=number_format($dvalue_val['ce_file_count']);?>개</p></li>
							<li class="<?=$fetch_row[12][1]?>">
								<?=$db_ce_reg_date_ymd;?><br/><?=$db_ce_reg_date_his;?>
								<p class="paddingtop5"><?=$db_ce_mod_date_ymd;?><br/><?=$db_ce_mod_date_his;?></p>
							</li>
							<li class="<?=$fetch_row[14][1]?>">
								<a class="mod_btn" href="<?=$popup_chapter_mod_url?>">수정</a>
								<a class="del_btn" href="<?=$popup_chapter_del_url?>">삭제</a>	
							</li>
						</ul>
					</div>
				</li>
				<?}?>
			</ul>
			<?if($row_size > 0){?>
			<ul id="chapter_submit">
				<li class="chapter_submit_btn">
					<input type="submit" value="정렬 순서 저장" />
					<input type="reset" value="닫기" onclick="self.close()" />
				</li>
			</ul>
			<?}?>
			<?	
				$episode_no_list = substr($episode_no_list,0,strrpos($episode_no_list, ","));
				$ce_order_list = substr($ce_order_list,0,strrpos($ce_order_list, ","));
			?>
			<input name="episode_no_list" value="<?=$episode_no_list?>" type="hidden" />
			<input name="ce_order_list" value="<?=$ce_order_list?>" type="hidden" />
			<input name="episode_no_list_origin" value="<?=$episode_no_list?>" type="hidden" />
			<input name="ce_order_list_origin" value="<?=$ce_order_list?>" type="hidden" />
		</div>
	</form>
</section><!-- episode_list -->
<script type="text/javascript">
	$("#chapter_list_s").dragsort({ dragSelector: "div ul li.ce_order_btn", dragBetween: true, dragEnd: saveOrder, placeHolderTemplate: "<li class=' placeHolder'></li>" });
	
	function saveOrder() {
		var data = $("#chapter_list_s div ul li.ce_order").map(function() { return $(this).attr('data-ce_order'); }).get();
		$("input[name=ce_order_list]").val(data.join(","));
		var order_origin_list = $("input[name=ce_order_list_origin]").val();
		var order_origin_arr = order_origin_list.split(",");
		$("#chapter_list_s div ul li.ce_order").each(function(i) {
			$(this).html(order_origin_arr[i]+"("+$(this).attr('data-ce_order')+")");
		});


		var data_num = $("#chapter_list_s div ul li.ce_chapter").map(function() { return $(this).attr('data-ce_chapter'); }).get();
		$("input[name=episode_no_list]").val(data_num.join(","));
	};
</script>

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>