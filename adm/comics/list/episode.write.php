<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);

$sql_comics = "SELECT * FROM comics WHERE `cm_no` = '$_comics' ";
$row_comics = sql_fetch($sql_comics);
$sql_episode = "SELECT * FROM comics_episode_".$_big." WHERE `ce_comics` = '$_comics' AND `ce_no` = '$_episode' ";
$row_episode = sql_fetch($sql_episode);

$episode_num_val = $mode_text = "";
$episode_num_max_sql = $episode_max = "";
$episode_num_min_sql = $episode_min = "";
$episode_outer_max_sql = $episode_outer_max = "";
// MAX
$episode_num_max_sql = "SELECT max(ce_chapter)as chapter_max FROM  comics_episode_".$_big." WHERE  `ce_comics` ='$_comics' ";
$episode_max = (int)sql_count($episode_num_max_sql, 'chapter_max');
// MIN
$episode_num_min_sql = "SELECT min(ce_chapter)as chapter_min FROM  comics_episode_".$_big." WHERE  `ce_comics` ='$_comics' ";
$episode_min = (int)sql_count($episode_num_min_sql, 'chapter_min');

// 외전
$episode_outer_max_sql = "SELECT max(ce_outer_chapter)as ce_outer_chapter_max FROM  comics_episode_".$_big." WHERE  `ce_comics` ='$_comics' AND  `ce_outer` = 'y' ";
$episode_outer_max = (int)sql_count($episode_outer_max_sql, 'ce_outer_chapter_max');

/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
				$chapter_val = $row_episode['ce_chapter'];
				$chapter_max_total = $episode_max;
				$chapter_min_total = $episode_min;
				// 외전
				$chapter_outer_val = $row_episode['ce_outer_chapter']; 
				$chapter_outer_total = $episode_outer_max;
	break;
	case "del": $mode_text = "삭제";
				$chapter_val = $row_episode['ce_chapter'];
				$chapter_max_total = $episode_max;
				$chapter_min_total = $episode_min;
				// 외전
				$chapter_outer_val = $row_episode['ce_outer_chapter']; 
				$chapter_outer_total = $episode_outer_max;
	break;
	default: $mode_text = "등록";
			 $sql = '';
			 $chapter_max_total = $episode_max;
			 $chapter_min_total = $episode_min;

			 $chapter_val = $episode_max+1;
			 if($chapter_val < 1){ $chapter_val = 1; }
			 if($chapter_max_total < 0){ $chapter_max_total = 0; }
			 // 외전
			 $chapter_outer_total = $episode_outer_max;
			 $chapter_outer_val = $episode_outer_max+1;
			 if($chapter_outer_val < 1){ $chapter_outer_val = 1; }
			 if($chapter_outer_total < 0){ $chapter_outer_total = 0; }
	break;
}
/* 압축파일 넓이 높이 설정 */
$ce_file_width = $ce_file_height = 0;
switch($_big){
	case "1":	$ce_file_width = 200;
				$ce_file_height = 250;
	break;
	default:	$ce_file_width = 200;
				$ce_file_height = 650;
	break;
}

// 마스터 관리자만 수정가능하게...
$readonly = "";
if($nm_member['mb_level'] < '25'){ $readonly = "readonly"; }
$ce_chapter_readonly = "";
if($nm_member['mb_level'] != '30'){ $ce_chapter_readonly = "readonly"; }

$res_date_hide = "res_date_hide";
$ce_service_date = NM_TIME_YMD;
if($row_episode['ce_service_state'] == 'r'){
	$res_date_hide = "";
	$ce_service_date = substr($row_episode['ce_service_date'], 0, 10);
}

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$cms_head_title = $row_comics['cm_series'];

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> (화) <?=$mode_text;?></h1>
</section><!-- flieupload_head -->

<section id="episode_write">
	<form name="episode_write_form" id="episode_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_folder;?>/episode.update.php" onsubmit="return episode_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="big" name="big" value="<?=$_big;?>"/><!-- 대분류번호 -->
		<input type="hidden" id="comics" name="comics" value="<?=$_comics;?>"/><!-- 수정/삭제시 코믹스번호 -->
		<input type="hidden" id="episode" name="episode" value="<?=$_episode;?>"/><!-- 수정/삭제시 에피소드번호 -->

		<? if($_mode == 'del') { ?>
			<p class="note">
			<span class="red">이 화를 구매한 회원이 있다면 삭제가 되지 않습니다.</span><br/>
			삭제시, 이 화와 관련된 다음 항목들이 같이 삭제됩니다.<br/>
			<ol>
				<li>화 이미지 및 폴더</li>
				<li>화 구매 기록</li>
				<li>화 구매 통계</li>
			</ol>
			<br/>
			</p>
		<? } // end if ?>

		<table>
			<tbody>
				<tr>
					<th><label for="ce_title">작품명</label></th>
					<td><input type="text" placeholder="화제목명 입력해주세요" name="ce_title" id="ce_title" value="<?=$row_episode['ce_title'];?>" autocomplete="off" /></td>
				</tr>
				<tr>
					<th><label for="chapter_notice"><?=$required_arr[0];?>화-차수</label></th>
					<td>
						<?if($chapter_max_total != 0){?>
							<p>등록된 마지막 (화) 차수 : <?=$chapter_max_total;?> (화)</p>
							<p>등록된 마지막 공지사항&외전 (화) 차수 : <?=$chapter_min_total;?> (화)</p>
							<!--
							<p class="red">수정시 위 마지막공지사항&외전와 마지막 차수 차수를 넘지 마세요.</p>
							<p class="blue"><?=$chapter_min_total;?> <= 입력차수 <= <?=$chapter_max_total;?></p>
							-->
						<?}?>
						
						<label for="ce_chapter">차수 </label>
						<input type="text" class="ce_chapter <?=$ce_chapter_readonly;?>" <?=$ce_chapter_readonly;?> placeholder="화-차수 입력해주세요" name="ce_chapter" id="ce_chapter" value="<?=$chapter_val;?>" autocomplete="off" />
						<label for="episode_num">(화)</label>
						<label for="ce_notice"> ex. 1 2 3... </label>
					</td>
				</tr>
				<tr>
					<th><label for="chapter_notice">화 공지사항</label></th>
					<td>
						<label for="ce_notice"> 화 공지사항 </label>
						<input type="text" class="ce_notice <?echo $row_episode['ce_outer']=='y'?$readonly:"";?>" placeholder="화 공지사항 입력해주세요" name="ce_notice" id="ce_notice" value="<?=$row_episode['ce_notice'];?>" autocomplete="off" <?echo $row_episode['ce_outer']=='y'?$readonly:"";?> />
						<label for="ce_notice"> ex. 프롤로그,공지,휴재안내 등... </label>
					</td>
				</tr>
				<tr>
					<th><label for="ce_outer">외전 여부</label></th>
					<td>
						<div id="ce_outer" class="input_btn">
							<input type="radio" name="ce_outer" id="ce_outern" value="n" <?=get_checked('', $row_episode['ce_outer']);?> <?=get_checked('n', $row_episode['ce_outer']);?> onclick="ce_ce_outer_chk(this.value);" />
							<label for="ce_outern">외전 아님</label>
							<input type="radio" name="ce_outer" id="ce_outery" value="y" <?=get_checked('y', $row_episode['ce_outer']);?>  onclick="ce_ce_outer_chk(this.value);" />
							<label for="ce_outery">외전 적용</label>
						</div>
						<div id="ce_outer_chapter" class="ce_outer_chapter<?echo $row_episode['ce_outer']=='y'?"":"_hide";?>">
							<?if($chapter_outer_total > 0){?><p>등록된 마지막 (화) 차수 : <?=$chapter_outer_total;?> (화)</p><?}?>
							
							<label for="ce_chapter">외전  화-차수 </label>
							<input type="text" class="ce_chapter <?=$readonly;?>" <?=$readonly;?> placeholder="화-차수 입력해주세요" name="ce_outer_chapter" id="ce_outer_chapter" value="<?=$chapter_outer_val;?>" autocomplete="off" />
							<label for="ce_outer_chapter">(화)</label>
							<label for="ce_notice"> ex. 1 2 3... </label>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="ce_service_state0"><?=$required_arr[0];?>서비스 여부</label></th>
					<td>
						<div id="ce_service_state" class="input_btn res_input_btn">
							<input type="radio" name="ce_service_state" id="ce_service_statey" value="y" <?=get_checked('', $row_episode['ce_service_state']);?> <?=get_checked('y', $row_episode['ce_service_state']);?> onclick="ce_service_state_chk(this.value);" />
							<label for="ce_service_statey">서비스 제공</label>
							<input type="radio" name="ce_service_state" id="ce_service_staten" value="n" <?=get_checked('n', $row_episode['ce_service_state']);?> onclick="ce_service_state_chk(this.value);" />
							<label for="ce_service_staten">서비스 중지(소장제공)</label>
							<input type="radio" name="ce_service_state" id="ce_service_stater" value="r" <?=get_checked('r', $row_episode['ce_service_state']);?> onclick="ce_service_state_chk(this.value);" />
							<label for="ce_service_stater">서비스 예약함</label>
								
							<span class="<?=$res_date_hide;?> res_service_date_view">
								<input type="text" class="res_date <?=$readonly;?>" placeholder="서비스일 입력해주세요" name="ce_service_date" id="ce_service_date" value="<?=$ce_service_date;?>" autocomplete="off" <?=$readonly;?> />
								<span for="ce_service_state2_explan">후 서비스제공으로 변경</span>
							</span>
							<?if($row_episode['res_service_date'] != '' || $row_episode['res_service_stop_date'] != ''){
								$res_service_date = $row_episode['res_service_date'];
								$res_service_date_text = "서비스제공일";
								if($row_episode['ce_service_state'] == 'r'){ $res_service_date_text = "서비스예약일"; }
								if($row_episode['ce_service_state'] == 'n'){ $res_service_date = $row_episode['res_service_stop_date']; $res_service_date_text = "서비스중지일";  }
								?>
							<span class="res_service_date_load"><?=$res_service_date_text?>:<?=$res_service_date?></span>
							<?}?>

							<br/>
							<input type="radio" name="ce_service_state" id="ce_service_statex" value="x" <?=get_checked('x', $row_episode['ce_service_state']);?> onclick="ce_service_state_chk(this.value);" />
							<label for="ce_service_statex">서비스 완전중지</label>
						</div>
					</td>
				</tr>
				<? if($_mode == "mod") { ?>
				<tr>
					<th><label for="ce_service_date_val"><?=$required_arr[0];?>서비스일<br/>(서비스제공일때만 <br/>수정가능)</label></th>
					<td>
						<div class="date_type_view input_btn">
							<input class="ce_service_date_val s_date_chk <? echo mb_is_admin() == true? "": "readonly"; ?>" type="text" name="ce_service_date_val" id="ce_service_date_val" value="<?=$row_episode['ce_service_date'];?>" <? echo mb_is_admin() == true? "": "readonly"; ?> autocomplete="off" />
							<input type="checkbox" name="s_date_chk" value="<?php echo NM_TIME_YMDHIS; ?>" id="s_date_chk" onclick="if (this.checked == true) this.form.ce_service_date_val.value=this.form.s_date_chk.value; else this.form.ce_service_date_val.value = this.form.ce_service_date_val.defaultValue;">
							<label for="s_date_chk">날짜를 오늘로</label>
							
							<input type="radio" name="ce_service_date_val_save" id="ce_service_date_val_saven" value="n" checked />
							<label for="ce_service_date_val_saven">서비스일 저장안함</label>
							<input type="radio" name="ce_service_date_val_save" id="ce_service_date_val_savey" value="y" />
							<label for="ce_service_date_val_savey">서비스일 저장</label>

						</div>
					</td>
				</tr>
				<? } // end if ?>
				<tr>
					<th><label for="res_free_on1"><?=$required_arr[0];?>기다리면 무료일</label></th>
					<td>
						<div id="ce_free_state" class="input_btn res_input_btn">
							<input type="radio" name="ce_free_state" id="res_free_on1" value="n" <?=get_checked('', $row_episode['ce_free_state']);?>  <?=get_checked('n', $row_episode['ce_free_state']);?> onclick="ce_free_state_chk(this.value);" />
							<label for="res_free_on1">무료안함</label>
							<input type="radio" name="ce_free_state" id="res_free_on0" value="y"<?=get_checked('y', $row_episode['ce_free_state']);?> onclick="ce_free_state_chk(this.value);" />
							<label for="res_free_on0">무료제공</label>
							<input type="radio" name="ce_free_state" id="res_free_on2" value="r" <?=get_checked('r', $row_episode['ce_free_state']);?> onclick="ce_free_state_chk(this.value);" />
							<label for="res_free_on2">무료제공예약</label>
						</div>
						<span class="res_date_hide res_free_date_view">
							<input type="text" class="res_date <?=$readonly;?>" placeholder="기다리면 무료일 입력해주세요" name="ce_free_date" id="ce_free_date" value="<?echo NM_TIME_YMD;?>" autocomplete="off" <?=$readonly;?> />
							<span for="res_service_on2_explan">후 무료제공으로 변경</span>
						</span>
						<?if($row_episode['res_free_date'] != ''){ 
							$res_free_date_text = "무료제공일";
							if($row_episode['ce_free_state'] == 'r'){ $res_free_date_text = "무료예약일"; }
						?>
						<span class="res_free_date_load"><?=$res_free_date_text?>:<?=$row_episode['res_free_date']?></span>
						<?}?>
					</td>
				</tr>
				<tr>
					<th><label for="service_pay"><?=$required_arr[0];?>가격 (단위:<?=$nm_config['cf_cash_point_unit_ko']?>-<?=$nm_config['cf_cash_point_unit']?>)</label></th>
					<td>
						<div id="service_pay" class="input_btn">
						<?	$ce_pay = $row_episode['ce_pay']; $p_no=0;
							if($row_episode['ce_pay'] == ''){ $ce_pay = $row_comics['cm_pay']; }
							for($p=0; $p<=$row_comics['cm_pay']; $p++){ ?>
								<input type="radio" name="ce_pay" id="ce_pay<?=$p_no?>" value="<?=$p?>" <?=get_checked($p, $ce_pay);?> />
								<label for="ce_pay<?=$p_no?>"><?=cash_point_view($p);?></label>
						<? if(($p_no+1) % 8 == 0){?><br/><?}?>
						<? $p_no++; } /* for($p=0; $p<=$pay; $p+=100){ end */?>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="ce_login"><?=$required_arr[0];?>로그인 설정<br/>(로그인 전 보기 설정시<br/>마케팅용으로<br/>오픈되오니<br/>해당 화-확인요망)</label></th>
					<td>
						<div id="ce_login" class="input_btn">
							<input type="radio" name="ce_login" id="ce_loginy" value="y" <?=get_checked('', $row_episode['ce_login']);?> <?=get_checked('y', $row_episode['ce_login']);?> />
							<label for="ce_loginy">로그인 후 보기</label>
							<input type="radio" name="ce_login" id="ce_loginn" value="n" <?=get_checked('n', $row_episode['ce_login']);?> />
							<label for="ce_loginn">로그인 전 보기</label>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="ce_cover">화 <br/>배너 이미지</label></th>
					<td>
						<? if($row_episode['ce_cover']!=''){ 
							$cover_src = img_url_para($row_episode['ce_cover'], $row_episode['ce_reg_date'], $row_episode['ce_mod_date']);
						} ?>
						<input type="file" name="ce_cover" id="ce_cover" data-value="<?=$cover_src?>" />
						<? if(cs_is_admin()){ ?>
							<input type="checkbox" name="ce_cover_noresizing" id="ce_cover_noresizing" value="y" class="noresizing" />
							<label for="ce_cover_noresizing">RESIZING-사용안함</label>
						<? } ?>
						<p class="episode_image_explan">이미지 업로드 정보 : width:200px, height:98px, 이미지파일 확장자:jpg, gif, png</p>
						<?if($row_episode['ce_cover']!=''){?>
						<div class="episode_image">
							<p class="view_btn"><a href="#cover" onclick="a_click_false();">메인배너보기</a></p>
							<p class="episode_image_hide"><img src="<?=$cover_src?>" alt="<?=$row_comics['cm_series']." ".$chapter_val."화 배너"?>" /></p>
						</div>
						<?}?>
					</td>
				</tr>
				<tr>
					<th><label for="ce_file"><?=$required_arr[0];?>화 에피소드 <br/>압축파일</label></th>
					<td>
						<input type="file" name="ce_file" id="ce_file" data-value="<?=$row_episode['ce_file']?>" />
						<? if(cs_is_admin()){ ?>
							<input type="checkbox" name="ce_file_noresizing" id="ce_file_noresizing" value="y" class="noresizing" />
							<label for="ce_file_noresizing">RESIZING-사용안함</label>
						<? } ?>
						<p class="episode_image_explan">압축파일 업로드 정보 : 압축파일 확장자:zip</p>
						<? if($row_episode['ce_file']!=''){ $ce_file_list = kt_storage_list($row_episode['ce_file']);?>
						<div id="ce_file_list" class="episode_image">
							<p class="view_btn"><a href="#ce_file" onclick="a_click_false();">압축파일보기</a></p>
							<p class="episode_image_hide ce_file_list">
							<? foreach($ce_file_list as $file_list_key => $file_list_name){ // 파일 리스트
								$ce_file_src = img_url_para($file_list_name, $row_episode['ce_reg_date'], $row_episode['ce_mod_date'], $ce_file_width, $ce_file_height);
							?>
								<img src="<?=$ce_file_src?>" alt="<?=$row_comics['cm_series']." ".$file_list_name;?>" width="<?=$ce_file_width;?>" height"<?=$ce_file_height;?>" />
							<? } // if($row_episode['ce_file']!=''){ ?>
							</p>
							<div class="view_btn"><img class="this_view" src="" alt="" /></div>
						</div>
						<?}?>
					</td>
				</tr>
				<?if($row_episode['ce_file_count'] > 0){?>
				<tr>
					<th><label for="episode_total">화 컨텐츠 파일 갯수</label></th>
					<td><?=$row_episode['ce_file_count'];?>개 이미지 파일</td>
				</tr>
				<?}?>

				<? if($_mode == "mod") { ?>
				<tr>
					<th><label for="cdn_purge">Purge<br/>이미지 파일경로 복사</label></th>
					<td>
						<textarea id="copy_text"><? tn_get_info($row_episode['ce_cover'], 'ce_cover'); ?><? foreach($ce_file_list as $file_list_names){ echo '/'.$file_list_names."\n"; }?></textarea>
						<a class="copy">IMAGE COPY</a>
						<a class="purge" href="<?=NM_ADM_URL?>/cdn_purge.php" target="_blank">Purge 가기</a>
						<input type="hidden" id="copy_text_ajax"  value="<? tn_get_info($row_episode['ce_cover'], 'ce_cover', true); ?><? foreach($ce_file_list as $file_list_names){ echo '/'.$file_list_names."||"; }?>"/>
						<a class="copy_btn" onclick="cdn_api_purge_send();">Purge 호출</a>
					</td>
				</tr>
				<?}?>

				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
						<? if($_mode == 'mod' || $_mode == 'del'){ ?>
						<input class="back_btn" type="reset" value="화 리스트화면" onclick="back_url('<?=$_SERVER['HTTP_REFERER'];?>')" />
						<? } ?>
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- episode_write -->

<? include_once NM_ADM_PATH.'/clipboard.php'; // 복사 ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>