<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);

/* 작가, 출판사, 제공사 */
$partner_list = array('comics_professional', 'comics_publisher', 'comics_provider');
foreach($partner_list as $partner_key => $partner_val){
	${$partner_val."_list"} = array();
	$result_partner = sql_query("SELECT * FROM ".$partner_val." order by cp_name asc");
	while ($row_partner = mysql_fetch_array($result_partner)) {
		array_push(${$partner_val."_list"}, $row_partner);
	}
}

$comics_field = "	c.*, 
					c_publ.cp_name as publ_name, 
					c_prof.cp_name as prof_name, c_prof_sub.cp_name as prof_name_sub, c_prof_thi.cp_name as prof_name_thi,
					c_prov.cp_name as prov_name, c_prov_sub.cp_name as prov_name_sub "; // 가져올 필드 정하기

$sql = "		SELECT $comics_field FROM comics c 
				left JOIN comics_publisher c_publ ON c_publ.cp_no = c.cm_publisher 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional
				left JOIN comics_professional c_prof_sub ON c_prof_sub.cp_no = c.cm_professional_sub 
				left JOIN comics_professional c_prof_thi ON c_prof_thi.cp_no = c.cm_professional_thi 
				left JOIN comics_provider c_prov ON c_prov.cp_no = c.cm_provider 
				left JOIN comics_provider c_prov_sub ON c_prov_sub.cp_no = c.cm_provider_sub 	
				WHERE  c.`cm_no` = '$_comics'
";
			 
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
	break;
}
/* 수정 또는 삭제라면...*/
$this_ep_date_start = $this_ep_date_end = $this_ep_no = $this__cover = $this_ep_name = $this_ep_top_class = '';
if($sql != ''){
	$row = sql_fetch($sql);
	$_big = $row['cm_big'];
	/* 수정 또는 삭제라면 이벤트 정보 가져오기 */
	if($row['cm_event'] > 0){
		$sql_event = "SELECT * FROM event_pay WHERE ep_date_end >= '".NM_TIME_YMD."' AND ep_no=".$row['cm_event']."";
		$row_event = sql_fetch($sql_event);
		$this_ep_cover = img_url_para($row_event['ep_cover'], $row_event['ep_reg_date'], $row_event['ep_mod_date'], 75, 50);
		$this_ep_no = $row_event['ep_no'];
		$this_ep_name = $row_event['ep_name'];
		$this_ep_date_start = $row_event['ep_date_start'];
		$this_ep_date_end = $row_event['ep_date_end'];
		$this_ep_top_class = "hide";
	}
}
/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$cms_head_title = $nm_config['cf_big'][$_big];

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더

/* 이벤트 검색 */
$event_arr = array();
$sql_event = "SELECT * FROM event_pay WHERE ep_date_end >= '".NM_TIME_YMD."' OR ep_date_type = 'n' ";
$result_event = sql_query($sql_event);
if(sql_num_rows($result_event) != 0){
	while ($row_event = sql_fetch_array($result_event)) {
		array_push($event_arr, $row_event);
	}
}

/* 키워드 체크박스 */
$keyword_arr = $ck_id_arr = array();
$sql_keyword = "SELECT * FROM comics_keyword WHERE 1 ORDER BY ck_id asc";
$result_keyword = sql_query($sql_keyword);
if(sql_num_rows($result_keyword) != 0){
	while ($row_keyword = sql_fetch_array($result_keyword)) {
		array_push($keyword_arr, $row_keyword);
		array_push($ck_id_arr, $row_keyword['ck_id']);
	}
}
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- comics_head -->

<section id="comics_write">
	<form name="comics_write_form" id="comics_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_folder_update;?>" onsubmit="return comics_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="comics" name="comics" value="<?=$_comics;?>"/><!-- 수정/삭제시 컨텐트번호 -->
		<input type="hidden" name="cm_s_time" id="cm_s_time" value="1" ><!-- 단편 열람기간 -->
		<input type="hidden" name="cm_l_time" id="cm_l_time" value="7" ><!-- 전편 열람기간 -->
		<input type="hidden" name="cm_public_period_check" id="cm_public_period_check" value="<?=$row['cm_public_period'];?>">

		<div>
		<? if($_mode == 'del') { ?>
			<p class="note">
			<span class="red">이 단행본(웹툰)을 구매한 회원이 있다면 삭제가 되지 않습니다.</span><br/>
			삭제시, 이 단행본과 관련된 다음 항목들이 같이 삭제됩니다.<br/>
			<ol>
				<li>포함되어 있는 전체 화(이미지 및 폴더 포함)</li>
				<li>최근 본 목록 & 즐겨찾기</li>
				<li>랭킹</li>
				<li>이벤트 & 프로모션</li>
				<li>통계</li>
			</ol>
			<br/>
			</p>
		<? } // end if ?>
		<table>
			<tbody>
				<tr>
					<th><label for="cm_big"><?=$required_arr[0];?>대분류</label></th>
					<td><input type="hidden" id="cm_big" name="cm_big" value="<?=$_big;?>"/><?=$cms_head_title;?></td>
				</tr>
				<tr>
					<th><label for="cm_small"><?=$required_arr[0];?>장르</label></th>
					<td><? tag_selects($nm_config['cf_small'], "cm_small", $row['cm_small'], 'n'); ?></td>
				</tr>
				<?if($_mode != 'del'){?>
				<tr>
					<th><label for="depths">키워드 카테고리</label></th>
					<td>
						<div class="depths">
							<div class='depths1_head'><strong>1Depth</strong></div>
							<div class='depths2_head'><strong>2Depth</strong></div>
						</div>
						<div class="depths">
							<? /* 키워드 */
							$row_cm_ck_id = explode("|", $row['cm_ck_id']);
							$row_cm_ck_id_sub = explode("|", $row['cm_ck_id_sub']);
							$row_cm_ck_id_thi = explode("|", $row['cm_ck_id_thi']);
							/* 1Depth */
							foreach($keyword_arr as $keyword_key => $keyword_val){ 
							if(strlen($keyword_val['ck_id']) != 2){ continue; }
							$ck_name1 = $keyword_val['ck_name'];
							?>
							<div class='depth1'>
								<span>
									<input type="checkbox" name="cm_ck_id[]" id="depth1_<?=$keyword_val['ck_id']?>" value="<?=$keyword_val['ck_id']?>" <? echo in_array($keyword_val['ck_id'],$row_cm_ck_id)? "checked":""; ?>  />
									<label for="depth1_<?=$keyword_val['ck_id']?>"><?=$ck_name1;?></label>
								</span>
							</div>
							<div class='depth2'>
								<? /* 2Depth */
								foreach($keyword_arr as $keyword_key2 => $keyword_val2){ 
								if(strlen($keyword_val2['ck_id']) != 4){ continue; }	
								if($keyword_val['ck_id'] != substr($keyword_val2['ck_id'],0 , 2)){ continue; }	
								$ck_name2 = $keyword_val2['ck_name'];

								/* 3Depth 있는지 체크 */
								$depth3used = '';
								if(in_array($keyword_val2['ck_id'].'10',$ck_id_arr)){ $depth3used = 'y'; }

								?>
								<?if($depth3used == 'y'){?><p><?} /* 3Depth 있으면 p 태그 */?>
								<span>
									<input type="checkbox" name="cm_ck_id_sub[]" class="depth1_<?=substr($keyword_val2['ck_id'],0,2);?> " id="depth2_<?=$keyword_val2['ck_id']?>" value="<?=$keyword_val2['ck_id']?>"  <? echo in_array($keyword_val2['ck_id'],$row_cm_ck_id_sub)? "checked":""; ?> />
									<label for="depth2_<?=$keyword_val2['ck_id']?>"><?=$ck_name2;?></label>
								</span>
								<?if($depth3used == 'y'){?></p><?} /* 3Depth 있으면 p 태그 */?>
								
									<? /* 3Depth 있으면 하기 */
										if($depth3used == 'y'){ ?>
										<div class='depth3_list'>
											<img src="<?=NM_URL?>/img/cms/low.gif" alt="하위분류" />
											<div class='depth3_head'><strong>3Depth</strong></div>
											<div class='depth3'>
												<? /* 3Depth */
												foreach($keyword_arr as $keyword_key3 => $keyword_val3){ 
												if(strlen($keyword_val3['ck_id']) != 6){ continue; }	
												if($keyword_val2['ck_id'] != substr($keyword_val3['ck_id'],0 , 4)){ continue; }	
												$ck_name3 = $keyword_val3['ck_name'];
												?>
												<span>
													<input type="checkbox" name="cm_ck_id_thi[]" class="depth1_<?=substr($keyword_val3['ck_id'],0,2);?> depth2_<?=substr($keyword_val3['ck_id'],0,4);?>" id="depth3_<?=$keyword_val3['ck_id']?>" value="<?=$keyword_val3['ck_id']?>" <? echo in_array($keyword_val3['ck_id'],$row_cm_ck_id_thi)? "checked":""; ?> />
													<label for="depth3_<?=$keyword_val3['ck_id']?>"><?=$ck_name3;?></label>
												</span>
												<? } ?>
											</div>
										</div>
									<? } /* if($depth3used == 'y'){ end */ ?>
								<? }/* foreach($keyword_arr as $keyword_key2 => $keyword_val2){ end */ ?>
								<?if(in_array($keyword_val['ck_id'].'10',$ck_id_arr) == NULL){?><span><label class="depth2_null" for="depth2_null">&nbsp;</label></span><?}?>
							</div>
						<? } /* foreach($keyword_arr as $keyword_key => $keyword_val){ end */ ?>
						</div>
					</td>
				</tr>
				<?}?>
				<tr>
					<th><label for="cm_series"><?=$required_arr[0];?>작품명</label></th>
					<td><input type="text" placeholder="작품명 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="cm_series" id="cm_series" value="<?=stripslashes($row['cm_series']);?>" autocomplete="off" /></td>
				</tr>
				<?if($_mode != 'del'){?>
					<tr>
						<th><label for="cm_comment">한줄 코멘트</label></th>
						<td><input type="text" placeholder="한줄 코멘트 입력해주세요" name="cm_comment" id="cm_comment" value="<?=stripslashes($row['cm_comment']);?>" autocomplete="off" /></td>
					</tr>
					<tr>
						<th><label for="cm_somaery">줄거리</label></th>
						<td><textarea name="cm_somaery" placeholder="줄거리 입력해주세요" id="cm_somaery" rows="4" autocomplete="off"><?=stripslashes($row['cm_somaery']);?></textarea></td>
					</tr>
					<tr>
						<th><label for="prof_name_thi"><?=$required_arr[0];?>작가 정보<br/>사이트 표기 전용</label></th>
						<td>
							<input type="text" name="cm_professional_info" placeholder="작가 정보 입력해주세요<?=$required_arr[2];?> -> 예시) 선화: 달님 / 채색: 신가희 / 글 : 묘오" <?=$required_arr[1];?> id="cm_professional_info" value="<?=$row['cm_professional_info'];?>" autocomplete="off" />
						</td>
					</tr>
					<tr>
						<th><label for="prof_name"><?=$required_arr[0];?>작가</label></th>
						<td>
							<input class="partner" type="text" name="prof_name" placeholder="작가 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> id="prof_name" value="<?=$row['prof_name'];?>" autocomplete="off" onkeydown='comics_cp("professional","",this);' />
							<input type="hidden" id="cm_professional" name="cm_professional" value="<?=$row['cm_professional'];?>" />
							<select class="partner" id="prof_select" name="prof_select" onchange="prof_add(this)">
								<option value="">선택하세요</option>
								<? foreach($comics_professional_list as $c_prof_key => $c_prof_val){?>
									<option value="<?=$c_prof_val['cp_no']?>"><?=$c_prof_val['cp_name']?></option>
								<?}?>
							</select>
						</td>
					</tr>
					<tr>
						<th><label for="prof_name_sub">작가 추가1</label></th>
						<td>
							<input class="partner" type="text" name="prof_name_sub" placeholder="작가 추가1 - 있을시 선택해주세요 ▶▶" id="prof_name_sub" value="<?=$row['prof_name_sub'];?>" autocomplete="off" onkeydown='comics_cp("professional","sub",this);' />
							<input type="hidden" id="cm_professional_sub" name="cm_professional_sub" value="<?=$row['cm_professional_sub'];?>" />
							<select class="partner" id="prof_select_sub" name="prof_select" onchange="prof_add_sub(this)">
								<option value="">선택하세요</option>
								<? foreach($comics_professional_list as $c_prof_key => $c_prof_val){?>
									<option value="<?=$c_prof_val['cp_no']?>"><?=$c_prof_val['cp_name']?></option>
								<?}?>
							</select>
						</td>
					</tr>
					<tr>
						<th><label for="prof_name_thi">작가 추가2</label></th>
						<td>
							<input class="partner" type="text" name="prof_name_thi" placeholder="작가 추가2 - 있을시 선택해주세요 ▶▶" id="prof_name_thi" value="<?=$row['prof_name_thi'];?>" autocomplete="off" onkeydown='comics_cp("professional","thi",this);' />
							<input type="hidden" id="cm_professional_thi" name="cm_professional_thi" value="<?=$row['cm_professional_thi'];?>" />
							<select class="partner" id="prof_select_thi" name="prof_select" onchange="prof_add_thi(this)">
								<option value="">선택하세요</option>
								<? foreach($comics_professional_list as $c_prof_key => $c_prof_val){?>
									<option value="<?=$c_prof_val['cp_no']?>"><?=$c_prof_val['cp_name']?></option>
								<?}?>
							</select>
						</td>
					</tr>
					<tr>
						<th><label for="publ_name"><?=$required_arr[0];?>출판사</label></th>
						<td>
							<input class="partner" type="text" name="publ_name" placeholder="출판사 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> id="publ_name" value="<?=$row['publ_name'];?>" autocomplete="off" onkeydown='comics_cp("publisher","",this);' />
							<input type="hidden" id="cm_publisher" name="cm_publisher" value="<?=$row['cm_publisher'];?>" />
							<select class="partner" id="publ_select" name="maker_select" onchange="publ_add(this)">
								<option value="">선택하세요</option>
								<? foreach($comics_publisher_list as $c_publ_key => $c_publ_val){?>
									<option value="<?=$c_publ_val['cp_no']?>"><?=$c_publ_val['cp_name']?></option>
								<?}?>
							</select>
						</td>
					</tr>
					<tr>
						<th><label for="prov_name"><?=$required_arr[0];?>제공사</label></th>
						<td>
							<input class="partner readonly" type="text" name="prov_name" placeholder="제공사 선택해주세요<?=$required_arr[2];?> ▶▶" <?=$required_arr[1];?> id="prov_name" value="<?=$row['prov_name'];?>" readonly autocomplete="off" onkeydown='comics_cp("provider","",this);' />
							<input type="hidden" id="cm_provider" name="cm_provider" value="<?=$row['cm_provider'];?>" />
							<select class="partner" id="prov_select" name="prov_select" onchange="prov_add(this)">
								<option value="">선택하세요</option>
								<? foreach($comics_provider_list as $c_prov_key => $c_prov_val){?>
									<option value="<?=$c_prov_val['cp_no']?>"><?=$c_prov_val['cp_name']?></option>
								<?}?>
							</select>
						</td>
					</tr>
					<tr>
						<th><label for="prov_name_sub">제공사 추가</label></th>
						<td>
							<input class="partner readonly" type="text" name="prov_name_sub" placeholder="제공사 추가 있을시 선택해주세요 ▶▶" id="prov_name_sub" value="<?=$row['prov_name_sub'];?>" readonly autocomplete="off"  onkeydown='comics_cp("provider","sub",this);' />
							<input type="hidden" id="cm_provider_sub" name="cm_provider_sub" value="<?=$row['cm_provider_sub'];?>" />
							<select class="partner" id="prov_sub_select" name="prov_sub_select" onchange="prov_sub_add(this)">
								<option value="">선택하세요</option>
								<? foreach($comics_provider_list as $c_prov_key => $c_prov_val){?>
									<option value="<?=$c_prov_val['cp_no']?>"><?=$c_prov_val['cp_name']?></option>
								<?}?>
							</select>
						</td>
					</tr>
					<tr>
						<th><label for="cm_platform"><?=$required_arr[0];?>이용환경</label></th>
						<td>
							<div id="cm_platform" class="input_btn">
							<?  $platform_check_val = $row['cm_platform'];
								$platform_arr = array();
								$platform_krsort = $nm_config['cf_platform']; /* 키역순 임시 배열 */
								krsort($platform_krsort);
								foreach($platform_krsort as $plat_krsort_key => $plat_krsort_val){ 
									if($platform_check_val-$plat_krsort_val[0] >= 0 && $platform_check_val >= 0){
										array_push($platform_arr, $plat_krsort_val[0]);
										$platform_check_val-=$plat_krsort_val[0];
									}
								}
								foreach($nm_config['cf_platform'] as $plat_key => $plat_val){ 
									if($plat_key == 0){continue;}
									$this_checked = $first_checked = "";
									if($row['cm_platform'] == '' && $plat_key == 1){ $first_checked = " checked "; }
									if(in_array($plat_val[0], $platform_arr)){ $this_checked = " checked ";}
									?>
								<input type="checkbox" name="cm_platform[]" id="cm_platform<?=$plat_key?>" value="<?=$plat_val[0]?>" <?=$first_checked?> <?=$this_checked?> />
								<label for="cm_platform<?=$plat_key?>"><?=$plat_val[1]?></label>
							<?} ?>
							</div>
						</td>
					</tr>
					<tr>
						<th><label for="cm_public_period"><?=$required_arr[0];?>연재 주기</label></th>
						<td>
							<div id="cm_public_period" class="input_btn">
							<?  $public_period_check_val = $row['cm_public_period'];
								tag_radios($nm_config['cf_public_cycle_main'], "cm_public_period", ($row['cm_public_period']?$row['cm_public_period']:"0"), 'n'); ?>
							</div>
						</td>
					</tr>
					<tr class="cm_public_cycle">
						<th><label for="cm_public_cycle"><?=$required_arr[0];?>연재 요일</label></th>
						<td>
							<div id="cm_public_cycle" class="input_btn">
							<?  $public_cycle_check_val = $row['cm_public_cycle'];
								$public_cycle_arr = get_comics_cycle_val($public_cycle_check_val);

								foreach($nm_config['cf_public_cycle'] as $cycle_key => $cycle_val){ 
									if($cycle_key == 0 || $cycle_key == 8) { continue; } // 주기 없음, 보름 무시
									$this_checked = "";
									if(in_array($cycle_val[0], $public_cycle_arr)){ $this_checked = " checked ";}
									?>
								<input type="checkbox" name="cm_public_cycle[]" id="cm_public_cycle<?=$cycle_key?>" value="<?=$cycle_val[0]?>" onclick="cycle_chk(<?=$cycle_key?>);"  <?=$this_checked?>/>
								<label for="cm_public_cycle<?=$cycle_key?>"><?=$cycle_val[1]?></label>
							<?} /* print_r($public_cycle_arr); */ ?>
							</div>
						</td>
					</tr>
					<tr>
						<th><label for="cm_episode_total">화(권) 총수</label></th>
						<td><?	$up_kind_text = "화";
								if($row['cm_up_kind'] == 1){ $up_kind_text = "권"; }
							echo $row['cm_episode_total']?$row['cm_episode_total'].$up_kind_text:'화(권)이 아직 등록되지 않았습니다.'; ?>
						</td>
					</tr>
					<tr>
						<th><label for="cm_type">컨텐츠 유형</label></th>
						<td>
							<div id="cm_type" class="input_btn">
							<? 
							  foreach($nm_config['cf_comics_type'] as $comics_type_key => $comics_type_val){ 
									$first_checked = "";
								if($row['cm_episode_total'] == '' && $comics_type_key == 0){ $first_checked = ' checked="checked"'; }
								?>
								<input type="radio" name="cm_type" id="cm_type<?=$comics_type_key?>" value="<?=$comics_type_val[0]?>" <?=get_checked($comics_type_val[0], $row['cm_type']);?> <?=$first_checked?> />
								<label for="cm_type<?=$comics_type_key?>"><?=$comics_type_val[1]?></label>
							<?} ?>
							</div>
						</td>
					</tr>
					<tr>
						<th><label for="cm_page_way"><?=$required_arr[0];?>페이지 넘기는 방향</label></th>
						<td>
							<div id="cm_page_way" class="input_btn">
								<input type="radio" name="cm_page_way" id="page_ways" value="s" <? echo $_big!=1?"checked='checked'":"onclick='page_way_chk(this.value,".$_big.")'"?> <?=get_checked('s', $row['cm_page_way']);?> >
								<label for="page_ways">스크롤</label>
								<input type="radio" name="cm_page_way" id="page_wayl" value="l" <? echo $_big==1?"checked":"onclick=\"page_way_chk(this.value,".$_big.")\""?> <?=get_checked('l', $row['cm_page_way']);?>/>
								<label for="page_wayl">왼쪽</label>
								<input type="radio" name="cm_page_way" id="page_wayr" value="r" <? echo $_big==1?"":"onclick='page_way_chk(this.value,".$_big.")'"?> <?=get_checked('r', $row['cm_page_way']);?> />
								<label for="page_wayr">오른쪽(일본)</label>
							</div>
						</td>
					</tr>
					<tr>
						<th><label for="cm_color"><?=$required_arr[0];?>흑백/칼라구분</label></th>
						<td>
							<div id="cm_color" class="input_btn">
								<input type="radio" name="cm_color" id="colory" value="y" <?=get_checked('', $row['cm_color']);?> <?=get_checked('y', $row['cm_color']);?> />
								<label for="colory">칼라</label>
								<input type="radio" name="cm_color" id="colorn" value="n" <?=get_checked('n', $row['cm_color']);?> />
								<label for="colorn">흑백</label>
							</div>
						</td>
					</tr>
					<tr>
						<th><label for="cm_adult"><?=$required_arr[0];?>열람등급</label></th>
						<td>
							<div id="cm_adult" class="input_btn">
								<input type="radio" name="cm_adult" id="cm_adulty" value="y" <?=get_checked('', $row['cm_adult']);?> <?=get_checked('y', $row['cm_adult']);?> />
								<label for="cm_adulty">성인</label>
								<input type="radio" name="cm_adult" id="cm_adultn" value="n" <?=get_checked('n', $row['cm_adult']);?> />
								<label for="cm_adultn">청소년</label>
							</div>
						</td>
					</tr>
					<tr>
						<th>
							<label for="cm_pay"><?=$required_arr[0];?>가격 (화폐:<?=$nm_config['cf_cash_point_unit_ko']?>-<?=$nm_config['cf_cash_point_unit']?>)</label>
							<p><a href="<?=NM_ADM_URL?>/cmsconfig/env/basic.php" target="_blank">화폐설정링크</a></p>
						</th>
						<td>
							<div id="cm_pay" class="input_btn">
							<?	$pay = $row['cm_pay'];
								if($row['cm_pay']==''){ $pay = $nm_config['cf_pay_basic']; } 
								$pay_no = 0;
								foreach($nm_config['cf_pay'] as $pay_key => $pay_val){ 
									$pay_no++;
									if($pay_val == ""){ continue; }	
									if(count($nm_config['cf_pay']) > ($pay_no+2) && $pay_no % 7 == 0){?>
								</div>
								<div class="cm_pay input_btn">
									<?}
								?>
								<input type="radio" name="cm_pay" id="cm_pay<?=$pay_key?>" value="<?=$pay_val?>" <?=get_checked($pay_val, $pay);?> />
								<label for="cm_pay<?=$pay_key?>"><?=cash_point_view($pay_val);?></label>
							<?}?>
							</div>
						</td>
					</tr>
					<tr>
						<th><label for="cm_free_pay"><?=$required_arr[0];?>무료화 소장 가격</label></th>
						<td>
							<input class="cm_free_pay" type="text" name="cm_free_pay" id="cm_free_pay" value="<?=$row['cm_free_pay'];?>"/>
							<label>※ 무료화 소장 가격(단위 : 땅콩)</label>
						</td>
					</tr>
					<tr>
						<th><label for="cm_end"><?=$required_arr[0];?>완결 구분</label></th>
						<td>
							<div id="cm_end" class="input_btn">
								<input type="radio" name="cm_end" id="cm_endn" value="n" <?=get_checked('', $row['cm_end']);?> <?=get_checked('n', $row['cm_end']);?> />
								<label for="cm_endn">연재 (미완결)</label>
								<input type="radio" name="cm_end" id="cm_endy" value="y" <?=get_checked('y', $row['cm_end']);?> />
								<label for="cm_endy">완결</label>
							</div>
						</td>
					</tr>
					<tr>
						<th><label for="cm_service"><?=$required_arr[0];?>서비스승인</label></th>
						<td>
							<div id="cm_service" class="input_btn">
								<input type="radio" name="cm_service" id="cm_servicey" value="y" <?=get_checked('', $row['cm_service']);?> <?=get_checked('y', $row['cm_service']);?> onclick="ce_service_state_chk(this.value)"/>
								<label for="cm_servicey">제공</label>
								<input type="radio" name="cm_service" id="cm_servicen" value="n" <?=get_checked('n', $row['cm_service']);?> onclick="ce_service_state_chk(this.value)"/>
								<label for="cm_servicen">중지</label>
								<input type="radio" name="cm_service" id="cm_servicer" value="r" <?=get_checked('r', $row['cm_service']);?> onclick="ce_service_state_chk(this.value)"/>
                <label for="cm_servicer">예약</label>
							</div>
                <?php
                if ($row['cm_service'] == 'r' && $row['cm_res_open_date'] != '' && $row['cm_res_open_hour'] != '') {
                    $display = " ";
                } else {
                    $display = "none";
                }
                ?>
                <!-- 180202 서비스 오픈 예약(과장님 요청) -->
                <span class="res_service_date_view"  style="display: <?=$display?>">
                   예약 날짜 :
                    <input type="text" class="res_open_date <?=$readonly;?>" placeholder="입력해주세요" name="cm_res_open_date" id="cm_res_open_date" value="<?=$row['cm_res_open_date']?>" autocomplete="off" <?=$readonly;?> />
                    <label class="giho"> | </label>
                        <? set_hour('cm_res_open_hour', $row['cm_res_open_hour'], '서비스 오픈 시간설정', 'n'); ?>
                </span>
						</td>
					</tr>
					<? if($_mode == "mod") { ?>
					<tr>
						<th><label for="cm_episode_date"><?=$required_arr[0];?>화 업데이트 날짜</label></th>
						<td>
							<div class="date_type_view">
								<input class="cm_episode_date s_date_chk <? echo mb_is_admin() == true? "": "readonly"; ?>" type="text" name="cm_episode_date" id="cm_episode_date" value="<?=$row['cm_episode_date'];?>" <? echo mb_is_admin() == true? "": "readonly"; ?> autocomplete="off" />
								<input type="checkbox" name="s_date_chk" value="<?php echo NM_TIME_YMDHIS; ?>" id="s_date_chk" 
								onclick="if (this.checked == true) {this.form.cm_episode_date.value=this.form.s_date_chk.value; 
																	this.form.cm_push_date.value=s_date_chk.value.substr(0, 10);} 
										 else {this.form.cm_episode_date.value = this.form.cm_episode_date.defaultValue;
											   this.form.cm_episode_date.value = this.form.cm_push_date.defaultValue;}">
								<label for="s_date_chk">최종 업데이트/PUSH 알람 날짜를 오늘로</label>
							</div>
						</td>
					</tr>
					<tr>
						<th><label for="cm_episode_date"><?=$required_arr[0];?>APP PUSH 날짜</label></th>
						<td>
							<div class="date_type_view">
								<input class="cm_push_date s_date_chk <? echo mb_is_admin() == true? "": "readonly"; ?>" type="text" name="cm_push_date" id="cm_push_date" value="<?=$row['cm_push_date'];?>" <? echo mb_is_admin() == true? "": "readonly"; ?> autocomplete="off" />
								<label>※ 최종 업데이트와 다른 값으로 수정되면 PUSH 알림</label>
							</div>
						</td>
					</tr>
					<? } // end if ?>
					<tr>
						<th><label for="cm_up_kind"><?=$required_arr[0];?>화권구별</label></th>
						<td>
							<div id="cm_up_kind" class="input_btn">
								<input type="radio" name="cm_up_kind" id="cm_up_kind0" value="0" <?=get_checked('', $row['cm_up_kind']);?> <?=get_checked('0', $row['cm_up_kind']);?> />
								<label for="cm_up_kind0">화</label>
								<input type="radio" name="cm_up_kind" id="cm_up_kind1" value="1" <?=get_checked('1', $row['cm_up_kind']);?> />
								<label for="cm_up_kind1">권</label>
							</div>
						</td>
					</tr>
					<tr>
						<th><label for="cm_up_kind"><?=$required_arr[0];?>패키지 화 유무</label></th>
						<td>
							<div id="cm_package" class="input_btn">
								<input type="radio" name="cm_package" id="cm_package0" value="y" <?=get_checked('y', $row['cm_package']);?>  />
								<label for="cm_package0">있음</label>
								<input type="radio" name="cm_package" id="cm_package1" value="n" <?=get_checked('', $row['cm_package']);?> <?=get_checked('n', $row['cm_package']);?>/>
								<label for="cm_package1">없음</label>
							</div>
						</td>
					</tr>
					<!--
					<tr>
						<th><label for="cm_own_type">소장/대여구분</label></th>
						<td>
							<div id="cm_own_type" class="input_btn">
								<input type="radio" name="cm_own_type" id="cm_own_type1" value="1" <?=get_checked('', $row['cm_own_type']);?> <?=get_checked('1', $row['cm_own_type']);?> />
								<label for="cm_own_type1">소장</label>
								<input type="radio" name="cm_own_type" id="cm_own_type2" value="2" <?=get_checked('2', $row['cm_own_type']);?> />
								<label for="cm_own_type2">대여</label>
							</div>
						</td>
					</tr>
					-->
					<tr>
						<th><label for="res_service_date">예약시간</label></th>
						<td>
							<div id="res_service_date">
								<?=set_hour('cm_res_service_date', $row['cm_res_service_date'], '화(권) 서비스 시간설정', 'n');?>
								<label class="giho"> | </label>								
								<?=set_hour('cm_res_free_date', $row['cm_res_free_date'], '화(권) 기다리다무료 시간설정', 'n');?>
							</div>
						</td>
					</tr>
					<!-- 180109 서비스 중지 예약(배경호 과장님 요청) -->
					<tr>
						<th><label for="res_stop_date">서비스 중지 예약</label></th>
						<td>
							<div id="res_stop_date">날짜 설정 :
								<input type="text" class="res_stop_date <?=$readonly;?>" placeholder="입력해주세요" name="cm_res_stop_date" id="cm_res_stop_date" value="<?=$row['cm_res_stop_date']?>" autocomplete="off" <?=$readonly;?> />
								<label class="giho"> | </label>
								<? set_hour('cm_res_stop_hour', $row['cm_res_stop_hour'], '서비스 중지 시간설정', 'n'); ?>
								<label class="giho"> | </label>	
								<input type="radio" name="cm_res_stop_service" id="cm_res_stop_servicey" value="y" class="reservation" <?=get_checked('y', $row['cm_res_stop_service'])?> />
								<label for="cm_res_stop_servicey" >
									<font color="red">중지 예약 사용</font>
								</label>
								<input type="radio" name="cm_res_stop_service" id="cm_res_stop_servicen" value="n" class="reservation" <?=get_checked('', $row['cm_res_stop_service'])?> <?=get_checked('n', $row['cm_res_stop_service'])?> />
								<label for="cm_res_stop_servicen" >
									<font color="red">예약 미사용</font>
								</label>								
							</div>
						</td>
					</tr>
					<!-- end res_stop_service -->
					<tr>
						<th><label for="cm_service_stop_date">서비스 중지 시간</label></th>
						<td><? echo $row['cm_service_stop_date']?$row['cm_service_stop_date']:'서비스 중지 시간이 없습니다.'; ?></td>
					</tr>
					<tr>
						<th><label for="cm_reg_date">최초등록일</label></th>
						<td><? echo $row['cm_reg_date']?$row['cm_reg_date']:'최초등록일이 없습니다.'; ?></td>
					</tr>
					<tr>
						<th><label for="cm_mod_date">데이터수정일</label></th>
						<td><? echo $row['cm_mod_date']?$row['cm_mod_date']:'데이터수정일이 없습니다.'; ?></td>
					</tr>
					<tr>
						<th><label for=""><?=$required_arr[0];?>표지이미지</label></th>
						<td>
							<? if($row['cm_cover']!=''){ 
								$cm_cover_src = img_url_para($row['cm_cover'], $row['cm_reg_date'], $row['cm_mod_date'], 192, 277);
							} ?>
							<input type="file" name="cm_cover" id="cm_cover" data-value="<?=$cm_cover_src?>" />
							<? if(cs_is_admin()){ ?>
								<input type="checkbox" name="cm_cover_noresizing" id="cm_cover_noresizing" value="y" class="noresizing" />
								<label for="cm_cover_noresizing">RESIZING-사용안함</label>
							<? } ?>
							<p class="cm_cover_explan">이미지 업로드 정보 : width:557px, height:806px, 이미지파일 확장자:jpg, gif, png</p>

							<?if($row['cm_cover']!=''){?>
							<div class="image">
								<p><a href="#cm_cover" onclick="a_click_false();">메인배너보기</a></p>
								<p class="cm_cover_hide"><img src="<?=$cm_cover_src?>" alt="<?=$row['cm_series']."표지"?>" /></p>
							</div>
							<?}?>
						</td>
					</tr>
					<tr>
						<th><label for=""><?=$required_arr[0];?>작은표지이미지</label></th>
						<td>
							<? if($row['cm_cover_sub']!=''){ 
								$cm_cover_sub_src = img_url_para($row['cm_cover_sub'], $row['cm_reg_date'], $row['cm_mod_date']);
							} ?>
							<input type="file" name="cm_cover_sub" id="cm_cover_sub"  data-value="<?=$cm_cover_sub_src?>" />
							<? if(cs_is_admin()){ ?>
								<input type="checkbox" name="cm_cover_sub_noresizing" id="cm_cover_sub_noresizing" value="y" class="noresizing" />
								<label for="cm_cover_sub_noresizing">RESIZING-사용안함</label>
							<? } ?>
							<p class="image_explan">이미지 업로드 정보: width:222px, height:222px, 이미지파일 확장자:jpg, gif, png</p>
							<?if($row['cm_cover_sub']!=''){ ?>
							<div class="image">
								<p><a href="#cm_cover_sub_src" onclick="a_click_false();">섬네일보기</a></p>
								<p class="cm_cover_hide"><img src="<?=$cm_cover_sub_src?>" alt="<?=$row['cm_series']."작은표지"?>" /></p>
							</div>
							<?}?>
						</td>
					</tr>
					<tr>
						<th><label for=""><?=$required_arr[0];?>화이미지</label></th>
						<td>
							<? if($row['cm_cover_episode']!=''){ 
								$cm_cover_episode_src = img_url_para($row['cm_cover_episode'], $row['cm_reg_date'], $row['cm_mod_date']);
							} ?>
							<input type="file" name="cm_cover_episode" id="cm_cover_episode"  data-value="<?=$cm_cover_episode_src?>" />
							<? if(cs_is_admin()){ ?>
								<input type="checkbox" name="cm_cover_episode_noresizing" id="cm_cover_episode_noresizing" value="y" class="noresizing" />
								<label for="cm_cover_episode_noresizing">RESIZING-사용안함</label>
							<? } ?>
							<p class="image_explan">
								이미지 업로드 정보: width:222px, height:222px, 이미지파일 확장자:jpg, gif, png<br/>
								<span class="red">화등록에서 해당 화에 대한 이미지 없을때 나올 대처 이미지</span><br/>
								<span class="blue">메인화면에서 1위일때 보여줄 이미지</span><br/>
							</p>
							<?if($row['cm_cover_episode']!=''){ ?>
							<div class="image">
								<p><a href="#cm_cover_episode_src" onclick="a_click_false();">섬네일보기</a></p>
								<p class="cm_cover_hide"><img src="<?=$cm_cover_episode_src?>" alt="<?=$row['cm_series']."화이미지"?>" /></p>
							</div>
							<?}?>
						</td>
					</tr>

					<? if($_mode == "mod") { ?>
					<tr>
						<th><label for="cdn_purge">Purge<br/>이미지 파일경로 복사</label></th>
						<td>
							<textarea id="copy_text"><? tn_get_info($row['cm_cover'], 'cm_cover'); ?><? tn_get_info($row['cm_cover_sub'], 'cm_cover_sub'); ?><? tn_get_info($row['cm_cover_episode'], 'cm_cover_episode'); ?></textarea>
							<a class="copy">IMAGE COPY</a>
							<a class="purge" href="<?=NM_ADM_URL?>/cdn_purge.php" target="_blank">Purge 가기</a>
							<input type="hidden" id="copy_text_ajax"  value="<? tn_get_info($row['cm_cover'], 'cm_cover',true); ?><? tn_get_info($row['cm_cover_sub'], 'cm_cover_sub',true); ?><? tn_get_info($row['cm_cover_episode'], 'cm_cover_episode',true); ?>"/>
							<a class="copy_btn" onclick="cdn_api_purge_send();">Purge 호출</a>
						</td>
					</tr>
					<?}?>

					<tr>
						<th><label for="cm_free">무료 화수</label></th>
						<td><input class="onlynumber" type="text" placeholder="무료 화수 입력해주세요" name="cm_free" id="cm_free" autocomplete="off" value="<?=$row['cm_free'];?>" /></td>
					</tr>
					<!-- 
					<tr>
						<th><label for="">이벤트 설정</label></th>
						<td>
							<input type="hidden" name="cm_event" id="cm_event" value="<?=$row['cm_event']?>" >
							<ul id="event_list" class="event_list" data-class="hide">
								<li class="<?=$this_ep_top_class?>" data-ep_no="">선택하세요</li>
								<?if($this_ep_no > 0){?>
								<li class="on" data-ep_no="<?=$this_ep_no?>">
									<img src="<?=$this_ep_cover?>" alt="<?=$this_ep_name?>"/>
									<?=$this_ep_name?> (<?=$this_ep_date_start?> ~ <?=$this_ep_date_end?>)
								</li>
								<?}?>
							<?foreach($event_arr as $event_key => $event_val){
								if($this_ep_no == $event_val['ep_no']){continue;}
								
								$event_ep_cover = img_url_para($event_val['ep_cover'], $event_val['ep_reg_date'], $event_val['ep_mod_date'], 75, 50);
							?>
								<li class="hide" data-ep_no="<?=$event_val['ep_no']?>">
									<img src="<?=$event_ep_cover?>" alt="<?=$event_val['ep_name']?>"/>
									<?=$event_val['ep_name']?> (<?=$event_val['ep_date_start']?> ~ <?=$event_val['ep_date_end']?>)
								</li>
							<?}?>
							</ul>
						</td>
					</tr>
					-->
				<?} /* if($_mode != 'del'){ */?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
		</div>
	</form>
</section><!-- comics_write -->

<? include_once NM_ADM_PATH.'/clipboard.php'; // 복사 ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>