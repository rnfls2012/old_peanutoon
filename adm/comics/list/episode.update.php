<?
include_once '_common.php'; // 공통
include_once(NM_LIB_PATH.'/pclzip.lib.php'); /* zip 압축 라이브러리 */
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode','big', 'comics', 'episode');
array_push($para_list, 'ce_comics','ce_no');
array_push($para_list, 'ce_title','ce_chapter','ce_notice');
array_push($para_list, 'ce_service_state','ce_service_date');
array_push($para_list, 'ce_free_state','ce_free_date');
array_push($para_list, 'ce_pay', 'ce_login');
array_push($para_list, 'ce_order', 'ce_reg_date', 'ce_mod_date');
array_push($para_list, 'ce_outer', 'ce_outer_chapter');
array_push($para_list, 'ce_service_date_val','ce_service_date_val_save');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'big','comics','episode','ce_pay');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'ce_title','ce_chapter','ce_notice', 'ce_free_date');
array_push($blank_list, 'ce_outer','ce_outer_chapter');

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode','big','comics','episode');
array_push($db_field_exception, 'ce_service_date_val','ce_service_date_val_save');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

/* comics 정보 가져오기 */
$sql_comics = "SELECT * FROM  `comics` WHERE  `cm_no` ='".$_comics."'";

$row_comics = sql_fetch($sql_comics);

if($row_comics['cm_big'] == ''){echo "해당 comics 정보를 가져오지 못했습니다."; die;}
else{$cm_big = $row_comics['cm_big'];}

/* ce_service_state 처리 */
$cm_res_service_date_hour = " ".$row_comics['cm_res_service_date'].":00:00";
if($row_comics['cm_res_service_date'] == ''){$cm_res_service_date_hour = " 00:00:00";}
$_ce_service_stop_date = "";

$_ce_service_date;
switch($_ce_service_state){
	case "y": $_ce_service_date = NM_TIME_YMDHIS;
	break;
	case "n": 
	case "x": $_ce_service_stop_date = NM_TIME_YMDHIS;
			  $_ce_service_date = "";
	break;
	case "r": $_ce_service_date.= $cm_res_service_date_hour;
			  if($_ce_service_date < NM_TIME_YMD." 00:00:00"){ echo "해당 ce_service_date의 날짜 정보가 잘못되였습니다."; die; }
				
	break;
	default: echo "해당 ce_service_state 정보가 잘못되였습니다."; die;
	break;
}

/* ce_free_state 처리 */
$cm_res_free_date_hour = " ".$row_comics['cm_res_free_date'].":00:00";
if($row_comics['cm_res_free_date'] == ''){$cm_res_free_date_hour = " 00:00:00";}
switch($_ce_free_state){
	case "y": $_ce_free_date = NM_TIME_YMDHIS;
	break;
	case "n": $_ce_free_date = "";
	break;
	case "r": $_ce_free_date.= $cm_res_free_date_hour;
			  if($_ce_free_date < NM_TIME_YMD." 00:00:00"){ echo "해당 ce_free_date의 날짜 정보가 잘못되였습니다."; die; }
				
	break;
	default: echo "해당 ce_free_state 정보가 잘못되였습니다."; die;
	break;
}

/* ce_notice 일때 처리 || ce_outer 일때 처리 */
if($_ce_notice != '' || $_ce_outer == 'y'){ 
	$chapter_min = $sql_chapter_min = "";
	$sql_chapter_min = "SELECT min(ce_chapter)as chapter_min FROM comics_episode_".$_big." WHERE  `ce_comics` ='".$_comics."'";
	$chapter_min = (int)sql_count($sql_chapter_min, 'chapter_min');
	$chapter_min--;
	if($chapter_min >= 0){ $chapter_min = '-1';} /* 마이너스로 처리 */
	if($_mode != "mod" || ($_mode == "mod" && $_ce_chapter >= 0)){ $_ce_chapter = $chapter_min; }
}
// 외전 일때 화 공지사항 빈값 처리
if($_ce_outer == 'y'){ $_ce_notice = ''; }
else{ $_ce_outer_chapter = '0'; }

/* 최대 order => fileupload UPDATE 여기서부터 하기 */
$chapter_max = $order_max = $sql_episode_max = "";
$sql_episode_max = "SELECT max(ce_order)as order_max FROM  comics_episode_".$_big." WHERE  `ce_comics` ='".$_comics."'";
$order_max = (int)sql_count($sql_episode_max, 'order_max');
$order_max++;

$dbtable = "comics_episode_".$_big;
$dbt_primary = "ce_no";
$para_primary = "episode";
${'_'.$dbt_primary} = ${'_'.$para_primary};

$dbt_primary2 = "ce_comics";
$para_primary2 = "comics";
${'_'.$dbt_primary2} = ${'_'.$para_primary2};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

$db_ce_chapter_text = $_ce_chapter."화";
if($_ce_chapter < 0){ $db_ce_chapter_text = $_ce_notice; }

if($_mode == 'reg'){
	/* 고정값 */
	$_ce_reg_date = NM_TIME_YMDHIS; /* 최초등록일 */
	$_ce_order = $order_max; /* 화 순서 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);
	
	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = $row_comics['cm_series'].'의 '.$db_ce_chapter_text.' 데이터가 등록되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

	if($_ce_notice == '' && $_ce_chapter != '0' && $_ce_outer == 'n'){
		/* 최대 chapter => comics UPDATE 여기서부터 하기 */
		$sql_UPDATE_comics = "UPDATE `comics` SET cm_episode_total='".$_ce_chapter."' ";
		if($_ce_service_state == 'y'){ $sql_UPDATE_comics.= " , cm_episode_date ='".NM_TIME_YMDHIS."' "; }
		$sql_UPDATE_comics.= " WHERE  `cm_no` ='".$_comics."'";
		sql_query($sql_UPDATE_comics);
	}

/* 수정 */
}else if($_mode == 'mod'){
	$_ce_mod_date = NM_TIME_YMDHIS; /* 수정일 */
	if($_comics == '' && $_episode == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 comics 값, episode 값이 없습니다.';
	}else{
		/* 날짜 저장 처리 */
		if($_ce_service_date_val_save == 'y' && $_ce_service_state == 'y') {
			// 0823 주석 처리, 수정할 때 예약 날짜 수정 안 됨 -> $_ce_service_state == 'y' 추가 0901
			$_ce_service_date = $_ce_service_date_val; 
		} 

		/* episode 정보 가져오기 */
		$sql_episode = "SELECT * FROM  `".$dbtable."` WHERE ce_comics='".$_comics."' AND ce_no='".$_episode."'";
		$row_episode = sql_fetch($sql_episode);

		/* 서비스 시간 */
		// if ($_ce_service_state == $row_episode['ce_service_state']){ $_ce_service_stop_date = ''; $_ce_service_date = ''; }
		/* 무료 시간 */
		// if ($_ce_free_state == $row_episode['ce_free_state']){ $_ce_free_date = ''; }
		
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, 'comics', 'ce_no');
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";

		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $row_comics['cm_series'].'의 '.$db_ce_chapter_text.' 데이터가 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

		/* 화 최대값 넣기 */
		$ce_chapter_max_sql = "";
		$ce_chapter_max_sql = "SELECT max(ce_chapter)as chapter_max FROM  `".$dbtable."` WHERE  `ce_comics` ='$_comics' ";
		$episode_max = (int)sql_count($ce_chapter_max_sql, 'chapter_max');
		
		if($episode_max < $_ce_chapter){ $episode_max = $_ce_chapter; }

		$sql_UPDATE_comics = "UPDATE `comics` SET cm_episode_total='".$episode_max."'  WHERE  `cm_no` ='".$_comics."'";
		sql_query($sql_UPDATE_comics);
	}

/* 삭제 */
}else if($_mode == 'del'){
	if($_comics == '' && $_episode == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 comics 값, episode 값이 없습니다.';
	}else{
		$count_buy_episode = sql_count("SELECT count(*) as cnt FROM member_buy_episode_".$_big." WHERE mbe_comics='$_comics' AND mbe_episode='$_episode' AND mbe_own_type='1'", 'cnt');
		if($count_buy_episode > 0) {
			$db_result['state'] = 1;
			$db_result['msg'] = '구매한 회원이 존재하여 삭제할 수 없습니다. 관리자에게 문의하세요!';
		} else {
			/* 화 파일 삭제 */
			$result_episode = sql_query("SELECT * FROM ".$dbtable." WHERE ce_comics='$_comics' AND ce_no='$_episode' order by ce_chapter asc");
			$ce_order_del = "";
			while ($row_episode = sql_fetch_array($result_episode)) {			
				kt_storage_zip_delete($row_episode['ce_file']);
				kt_storage_delete($row_episode['ce_cover']);
			}
			
			/* 관련 테이블에서 데이터 삭제 */
			sql_query("DELETE FROM member_buy_episode_".$_big." WHERE mbe_comics='".$_comics."' AND mbe_episode='".$_episode."'"); // member_buy_episode
			sql_query("DELETE FROM sales_episode_".$_big." WHERE se_comics='".$_comics."' AND se_episode='".$_episode."'"); // sales_episode
			sql_query("DELETE FROM sales_episode_".$_big."_age_sex WHERE seas_comics='".$_comics."' AND seas_episode='".$_episode."'"); // sales_episode_age_sex

			sql_query("DELETE FROM ".$dbtable." WHERE ce_comics='$_comics' AND ce_no='$_episode'");

			/* 정렬순서 재정리 */
			$sql_order = "UPDATE ".$dbtable." SET ce_order=(ce_order-1) WHERE ce_comics='$_comics' AND ce_order > ".$ce_order_del."  ";
			sql_query($sql_order);

			/* 화 최대값 넣기 */
			$ce_chapter_max_sql = "";
			$ce_chapter_max_sql = "SELECT max(ce_chapter)as chapter_max FROM  `".$dbtable."` WHERE  ce_comics ='$_comics' ";
			$episode_max = (int)sql_count($ce_chapter_max_sql, 'chapter_max');

			$sql_UPDATE_comics = "UPDATE `comics` SET cm_episode_total='".$episode_max."'  WHERE  `cm_no` ='".$_comics."'";
			sql_query($sql_UPDATE_comics);
			
			$db_result['msg'] = $row_comics['cm_series'].'의 '.$db_ce_chapter_text.' 데이터가 삭제되였습니다.';
		} // end else
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바랍니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/* 이미지 처리 */
if($_mode != 'del' && $db_result['state'] == 0){
	/* 등록이라면~ [ ${'_'.$para_primary} 이 NULL 이라면 ] */
	$sql = '';
	if(${'_'.$para_primary} == ''){
		$sql = 'SELECT COALESCE( MAX( '.$dbt_primary.' ) , 0 ) AS '.$dbt_primary.'_new FROM '.$dbtable; /* 방금 등록번호 가져오기 */
		$row = sql_fetch($sql);
		${'_'.$dbt_primary} = $row[$dbt_primary.'_new'];
	}



/* 이미지 처리 ce_cover */
	/* 대분류 경로설정 - 폴더 체크 및 생성 */
	$ce_cover_path = 'data/'.$cm_big.'/'.$_comics.'/ce_cover/';
	
	/* 업로드한 이미지 명 */
	$ce_cover_tmp_name = $_FILES['ce_cover']['tmp_name'];
	$ce_cover_name = $_FILES['ce_cover']['name'];
	
	/* 리사이징 안함 DB 저장 안함 */
	$_ce_cover_noresizing	= etc_filter(num_eng_check(tag_filter($_REQUEST['ce_cover_noresizing'])));
	$_ce_file_noresizing	= etc_filter(num_eng_check(tag_filter($_REQUEST['ce_file_noresizing'])));

	/* 확장자 얻기 .png .jpg .gif */
	$ce_cover_extension = substr($ce_cover_name, strrpos($ce_cover_name, "."), strlen($ce_cover_name));

	/* 고정된 파일 명 */
	$ce_cover_name_define = 'chapter'.$_ce_chapter.strtolower($ce_cover_extension);

	/* 저장 경로 및 DB 파일명 및 파일경로 저장 */
	$ce_cover_src = $ce_cover_path.$ce_cover_name_define;

	/* 파일 업로드 */	
	if ($ce_cover_name != '') {	
		if($_ce_cover_noresizing == 'y'){
			$files_cm_cover_result = kt_storage_upload($ce_cover_tmp_name, $ce_cover_path, $ce_cover_name_define); 
			$files_cm_cover_result = tn_set_key_thumbnail($ce_cover_tmp_name, $ce_cover_path, 'ce_cover' ,'not', '', $ce_cover_name_define);	
			$files_cm_cover_result = tn_upload_thumbnail($ce_cover_tmp_name, $ce_cover_path, $ce_cover_name_define, 'ce_cover' ,'not', '', $ce_cover_name_define);	
		}else{
			$files_cm_cover_result = tn_set_key_thumbnail($ce_cover_tmp_name, $ce_cover_path, 'ce_cover' ,'all', '', $ce_cover_name_define);
			$files_cm_cover_result = tn_upload_thumbnail($ce_cover_tmp_name, $ce_cover_path, $ce_cover_name_define, 'ce_cover' ,'all', '', $ce_cover_name_define);
		}

		rmdirAll(NM_THUMB_PATH.'/'.$ce_cover_path); // kt storage - comics 디렉토리 삭제
		rmdirAll(NM_PATH.'/'.$ce_cover_path); // kt storage - comics 디렉토리 삭제
		
		$sql_ce_cover = " UPDATE ".$dbtable." SET ";
		$sql_ce_cover.= ' ce_cover = "'.$ce_cover_src.'" ';
		$sql_ce_cover.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";

		/* DB 저장 */
		if(sql_query($sql_ce_cover)){
			$db_result['msg'].= '\n'.$_comics.'의 이미지가 저장되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'].= '\n'.'이미지 에러가 발생하여 저장되지 않았습니다.';
			$db_result['error'].= $sql_ce_cover;
		}
	}
	
	/* 임시파일이 존재하는 경우 삭제 */
	if (file_exists($ce_cover_tmp_name) && is_file($ce_cover_name)) {
		unlink($ce_cover_tmp_name);
	}

/* 압축파일 처리 ce_file */
	$ce_file_name = $_FILES['ce_file']['name'];
	$ce_file_tmp_name = $_FILES['ce_file']['tmp_name'];
	
	if ($ce_file_name != '') {
		$ce_file_path = 'data/'.$cm_big.'/'.$_comics.'/'.$_ce_chapter;
		if($_ce_file_noresizing == 'y'){
			$zipfile_count = kt_storage_zip_upload($ce_file_tmp_name, $ce_file_path, $ce_file_name);
		}else{
			$zipfile_count = tn_thumbnail_storage_zip_upload($ce_file_tmp_name, $ce_file_path, $ce_file_name);
			rmdirAll(NM_THUMB_PATH.'/data/'.$cm_big.'/'.$_comics); // kt storage - comics 디렉토리 삭제
		}
		rmdirAll(NM_PATH.'/data/'.$cm_big.'/'.$_comics); // kt storage - comics 디렉토리 삭제

		// 파일 리스트 가져오기
		$kt_storage_list = kt_storage_list($ce_file_path);
		foreach($kt_storage_list as &$kt_storage_list_val){
			$kt_storage_list_val = str_replace($ce_file_path.'/', '', $kt_storage_list_val); // 파일명만
		}
		$ce_file_list = strtolower(implode(", ",$kt_storage_list));
		
		$sql_ce_file = " UPDATE ".$dbtable." SET ";
		$sql_ce_file.= ' ce_file = "'.$ce_file_path.'", ';
		$sql_ce_file.= ' ce_file_list = "'.$ce_file_list.'", ';
		$sql_ce_file.= ' ce_file_count = "'.$zipfile_count.'" ';
		$sql_ce_file.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
		/* DB 저장 */
		if(sql_query($sql_ce_file)){
			$db_result['msg'].= '\n'.$_comics.'의 이미지가 저장되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'].= '\n'.'이미지 에러가 발생하여 저장되지 않았습니다.';
			$db_result['error'].= $sql_ce_file;
		}
	}
}
/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/

// globalhu 여기부터
// 새 글 등록시 푸시 보내기
/* 2017-08-01 주석
if($_mode=="reg"){
	push($row_comics['cm_series'].'가 업로드 되었습니다.',$_comics);
}
*/
// 푸시 보내는 함수
// /lib/app.lib.php로 function 옮김
/*
function push($message,$idx)
{
	$idxs = array();
	$token = array();
	$token2 = array();

	$sql="select mcm_member from member_comics_mark where mcm_comics = '$idx'";
	$query=mysql_query($sql);
	while($result=mysql_fetch_array($query)){
		array_push($idxs,$result['mcm_member']);
	}
	$idx=implode(',',$idxs);
	$sql="select mb_token from member where mb_no in ($idx) and mb_device=0 and mb_push=1";
	$query=mysql_query($sql);

	while($result=mysql_fetch_array($query)){
		array_push($token,$result['mb_token']);
	}

	$sql="select mcm_member from member_comics_mark where mcm_comics = '$idx'";
	$query=mysql_query($sql);
	while($result=mysql_fetch_array($query)){
		array_push($idxs,$result['mcm_member']);
	}
	$idx=implode(',',$idxs);
	$sql="select mb_token from member where mb_no in ($idx) and mb_device=1 and mb_push=1";
	$query=mysql_query($sql);

	while($result=mysql_fetch_array($query)){
		array_push($token2,$result['mb_token']);
	}

	$content=$message;

	$push_i = array("body" => $content);
	$push_a = array("message" => $content, "imgurllink" => "", 'link' => "");

	fcm_send($token, $push_a);
	fcm_send($token2, $push_a,$push_i);
}
function fcm_send ($tokens, $message, $message2 = null)
{
	$url = 'https://fcm.googleapis.com/fcm/send';
	$fields = array(
			'registration_ids' => $tokens,
			'data' => $message,
			'notification' => $message2
		);

	$headers = array(
		'Authorization:key =' . 'AAAABMVhZIY:APA91bFoLd_Esx4XhU4AbsTcBXwWsw783QrmCCyT9eqR_7Is6Rn4M7I6vvXZ0UfnoTBDRLNMltzZh8ioSeYoSQwLxmxsLZdc1bDb_PWe1irkYEqMmObNMUVphkFiRmon0eVWDQhPIaAv',
		'Content-Type: application/json'
		);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	$result = curl_exec($ch);
	if ($result === FALSE) {
		die('Curl failed: ' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}
// globalhu 여기까지

*/
// pop_close($db_result['msg'], '', $db_result['error']);
pop_url($db_result['msg'], NM_ADM_URL.'/comics/list/episode.list.php?big='.$_big.'&comics='.$_comics, $db_result['error']);
die;

?>