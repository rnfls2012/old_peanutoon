<?php
use Classes\Push as PushServer;

include_once '_common.php'; // 공통
require_once NM_PATH.'/vendor/autoload.php'; // require composer autoload

include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode','comics','cm_no','cm_s_time','cm_l_time','cm_big','cm_small');
array_push($para_list, 'cm_ck_id', 'cm_ck_id_sub', 'cm_ck_id_thi');
array_push($para_list, 'cm_series','cm_comment','cm_somaery');

array_push($para_list, 'cm_professional','prof_name');
array_push($para_list, 'cm_professional_sub','prof_name_sub');
array_push($para_list, 'cm_professional_thi','prof_name_thi');
array_push($para_list, 'cm_professional_info');

array_push($para_list, 'cm_publisher','publ_name');
array_push($para_list, 'cm_provider','prov_name');
array_push($para_list, 'cm_provider_sub','prov_name_sub');

array_push($para_list, 'cm_platform','cm_public_period', 'cm_public_cycle','cm_type','cm_page_way','cm_color','cm_adult','cm_res_stop_service');
array_push($para_list, 'cm_pay', 'cm_free_pay', 'cm_end', 'cm_service', 'cm_up_kind', 'cm_package');
array_push($para_list, 'cm_res_service_date','cm_res_free_date','cm_res_open_date','cm_res_open_hour','cm_res_stop_date','cm_res_stop_hour');
array_push($para_list, 'cm_own_type','cm_cover','cm_cover_sub','cm_cover_episode');
array_push($para_list, 'cm_free','cm_event');

array_push($para_list, 'cm_reg_date','cm_mod_date','cm_episode_date','cm_push_date');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'comics','cm_big','cm_small','cm_pay','cm_free','cm_event','cm_public_period');

/* 체크박스배열 PARAMITER 체크 */
array_push($checkbox_list, 'cm_ck_id', 'cm_ck_id_sub', 'cm_ck_id_thi');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'cm_ck_id', 'cm_ck_id_sub', 'cm_ck_id_thi', 'cm_comment');
array_push($blank_list, 'cm_professional_sub','prof_name_sub');
array_push($blank_list, 'cm_professional_thi','prof_name_thi');
array_push($blank_list, 'cm_provider_sub','prov_name_sub');
array_push($blank_list, 'cm_free','cm_event');
array_push($blank_list, 'cm_res_service_date','cm_res_free_date','cm_res_open_date','cm_res_open_hour','cm_res_stop_date','cm_res_stop_hour');

/* 빈칸&숫자 PARAMITER 허용 */
array_push($null_int_list, 'cm_professional_sub', 'cm_professional_thi');
array_push($null_int_list, 'cm_provider_sub', 'cm_public_cycle');

/* 숫자 합계 처리 PARAMITER */
array_push($sum_list, 'cm_platform','cm_public_cycle');

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode', 'prof_name','publ_name', 'prov_name', 'prov_name_sub');
array_push($db_field_exception, 'prof_name_sub', 'prof_name_thi');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

// 무료화수
if($_cm_free == ''){$_cm_free = 0;}
// 이벤트번호
if($_cm_event == ''){$_cm_event = 0;}

if($_mode != 'del'){
	/* 작가, 출판사 처리 */
	$name_list = array('prof_name','prof_name_sub','prof_name_thi','publ_name');
	$input_name_list = array('cm_professional','cm_professional_sub','cm_professional_thi','cm_publisher');
	$name_list_db = array('comics_professional','comics_professional','comics_professional','comics_publisher');
	foreach($name_list as $name_key => $name_val){
		if(${'_'.$name_val} != ''){

			$sql_name_insert = "INSERT INTO ".$name_list_db[$name_key]."( cp_name, cp_date ) VALUES('".${'_'.$name_val}."','".NM_TIME_YMDHIS."')";
			@mysql_query($sql_name_insert );
			// echo $sql_name_insert."<br/>";

			$sql_name_check = "select * from ".$name_list_db[$name_key]." where cp_name = '".${'_'.$name_val}."'";
			$row_name_check = sql_fetch($sql_name_check);
			// echo $sql_name_check."<br/>";

			${'_'.$input_name_list[$name_key]} = $row_name_check['cp_no'];
			// echo ${'_'.$input_name_list[$name_key]}."<br/>";
		}
	}
}

$dbtable = "comics";
$dbt_primary = "cm_no";
$para_primary = "comics";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

if($_mode == 'reg'){
	/* 고정값 */
	$_cm_reg_date = NM_TIME_YMDHIS; /* 최초등록일 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);
	
	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = $_cm_series.'의 데이터가 등록되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '등록 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

/* 수정 */
}else if($_mode == 'mod'){
	/* 고정값 */
	$_cm_mod_date = NM_TIME_YMDHIS; /* 수정일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{		
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		if($_cm_public_period == 0) { $sql_mod .= ", cm_public_cycle = '0' "; };
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		$row_comics = get_comics(${'_'.$dbt_primary});

		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $_cm_series.'의 데이터가 수정되였습니다.';
			
			// APP PUSH
			if($_cm_push_date != $row_comics['cm_push_date']) {
                $msg = $row_comics['cm_series'].' 업로드 되었습니다.';
                $url = "dev.nexcube.co.kr/push_server/public/put.php";

                $newInstance = new PushServer($url);

                $mb_arr = array();
                $token_arr = array();

                /**
                 * START FETCH
                 * member mark-list and mb_token
                 */
                $sql="
                  SELECT mcm_member FROM member_comics_mark WHERE mcm_comics = ".$row_comics['cm_no']."
                ";
                $query = sql_query($sql);

                while ( $result = sql_fetch_array($query) ) {
                    array_push($mb_arr, $result['mcm_member']);
                }

                $mb_arr_to_str = implode(',',$mb_arr);
                $sql="
                 SELECT mb_token FROM member WHERE mb_no IN ($mb_arr_to_str) AND mb_device = 0 AND mb_push = 1 AND mb_state = 'y'
                ";
                $query = sql_query($sql);

                while ( $result = sql_fetch_array($query) ) {
                    array_push($token_arr, $result['mb_token']);
                }
                /**
                 * END FETCH
                 */

                $push_msg_arr = array(
                    'KEY_IDN'   => $row_comics['cm_no'],
                    'TYPE'      => 'NO_POPUP_DIRECTION',
                    'TITLE'     => '웹툰 업데이트 알림',
                    'CONTENT'   => $msg,
                    'EVENT'     => get_comics_url($row_comics['cm_no'])
                );

                $push_msg_arr['registration_ids'] = $token_arr;

                try {
                    $newInstance->sendPushMsg($push_msg_arr);
                } catch (\Exception $exception) {
                    echo $exception->getMessage()." / ".$exception->getCode();
                } // end push
			} // end if
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		$count_buy_comics = sql_count("SELECT count(*) as cnt FROM member_buy_comics WHERE mbc_comics='".${'_'.$dbt_primary}."' and mbc_own_type='1'", 'cnt');
		if($count_buy_comics > 0) {
			$db_result['state'] = 1;
			$db_result['msg'] = '구매한 회원이 존재하여 삭제할 수 없습니다. 관리자에게 문의하세요!';
		} else {
			/* 화 파일 삭제 */
			$result_episode = sql_query("SELECT * FROM comics_episode_".$_cm_big." WHERE ce_comics='".${'_'.$dbt_primary}."' ORDER BY ce_chapter ASC");
			while ($row_episode = sql_fetch_array($result_episode)) {
				kt_storage_delete($row_episode['ce_cover']);
				kt_storage_zip_delete($row_episode['ce_file']);
			}

			/* 이미지&폴더 삭제 */ 
			$row_comics = sql_fetch("SELECT * FROM ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
			kt_storage_delete($row_comics['cm_cover']);
			kt_storage_delete($row_comics['cm_cover_sub']);
			kt_storage_delete($row_comics['cm_cover_episode']);
			kt_storage_delete('data/'.$row_comics['cm_big'].'/'.$row_comics['cm_no']);
			kt_storage_delete('data/'.$row_comics['cm_big'].'/'.$row_comics['cm_no'].'/ce_cover');

			/* 최근 본 목록 & 즐겨찾기 삭제 */
			sql_query("DELETE FROM member_buy_comics WHERE mbc_comics='".${'_'.$dbt_primary}."'"); // 최근 본 목록
			sql_query("DELETE FROM member_comics_mark WHERE mcm_comics='".${'_'.$dbt_primary}."'"); // 즐겨찾기

			/* 랭킹 부분 삭제 */
			sql_query("DELETE FROM member_comics_mark WHERE mcm_comics='".${'_'.$dbt_primary}."'"); // 즐겨찾기

			/* 이벤트 부분 삭제 */
			sql_query("DELETE FROM epromotion_event WHERE eme_event_no='".${'_'.$dbt_primary}."'"); // 단행본 연결 프로모션
			sql_query("DELETE FROM event_pay_comics WHERE epc_comics='".${'_'.$dbt_primary}."'"); // 무료&할인 이벤트
			sql_query("DELETE FROM event_pay_comics WHERE epc_comics='".${'_'.$dbt_primary}."'"); // 무료&할인 이벤트

			/* 구매 에피소드 삭제(무료일 경우만) */
			sql_query("DELETE FROM member_buy_episode_".$_cm_big." WHERE mbe_comics='".${'_'.$dbt_primary}."'"); // 구매 에피소드

			/* Sales 부분(통계) */
			sql_query("DELETE FROM sales WHERE sl_comics='".${'_'.$dbt_primary}."'"); // Sales 부분
			sql_query("DELETE FROM sales_episode_".$_cm_big." WHERE se_comics='".${'_'.$dbt_primary}."'"); // Sales_episode 부분
			sql_query("DELETE FROM sales_episode_".$_cm_big."_age_sex WHERE seas_comics='".${'_'.$dbt_primary}."'"); // Sales_episode_age_sex 부분

			/* 데이터 삭제 */
			sql_query("DELETE FROM comics_episode_".$_cm_big." WHERE ce_comics='".${'_'.$dbt_primary}."'");
			sql_query("DELETE FROM ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
			
			$db_result['msg'] = $_cm_series.'의 데이터가 삭제되였습니다.';
		} // end else
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바랍니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}


/* 이미지 처리 */
if($_mode != 'del' && $db_result['state'] == 0){
	/* 등록이라면~ [ ${'_'.$para_primary} 이 NULL 이라면 ] */
	$cm_cover_comics = ${'_'.$para_primary};
	$sql = '';
	if(${'_'.$para_primary} == ''){
		$sql = 'SELECT COALESCE( MAX( '.$dbt_primary.' ) , 0 ) AS '.$dbt_primary.'_new FROM '.$dbtable; /* 방금 등록번호 가져오기 */
		$row = sql_fetch($sql);
		$cm_cover_comics = $row[$dbt_primary.'_new'];
	}

	/* 이미지 경로 */

	/* 대분류 경로설정 - 폴더 체크 및 생성 */
	$img_path = 'data/'.$cm_big.'/'.$cm_cover_comics.'/';

	/* 업로드한 이미지 명 */
	$files_cm_cover_tmp_name = $_FILES['cm_cover']['tmp_name'];
	$files_cm_cover_name = $_FILES['cm_cover']['name'];
	$files_cm_cover_sub_tmp_name = $_FILES['cm_cover_sub']['tmp_name'];
	$files_cm_cover_sub_name = $_FILES['cm_cover_sub']['name'];
	$files_cm_cover_episode_tmp_name = $_FILES['cm_cover_episode']['tmp_name'];
	$files_cm_cover_episode_name = $_FILES['cm_cover_episode']['name'];
	
	/* 리사이징 안함 DB 저장 안함 */
	$_cm_cover_noresizing			= etc_filter(num_eng_check(tag_filter($_REQUEST['cm_cover_noresizing'])));
	$_cm_cover_sub_noresizing		= etc_filter(num_eng_check(tag_filter($_REQUEST['cm_cover_sub_noresizing'])));
	$_cm_cover_episode_noresizing	= etc_filter(num_eng_check(tag_filter($_REQUEST['cm_cover_episode_noresizing'])));
	
	/* 확장자 얻기 .png .jpg .gif */
	$cm_cover_extension = substr($files_cm_cover_name, strrpos($files_cm_cover_name, "."), strlen($files_cm_cover_name));
	$cm_cover_sub_extension = substr($files_cm_cover_sub_name, strrpos($files_cm_cover_sub_name, "."), strlen($files_cm_cover_sub_name));
	$cm_cover_episode_extension = substr($files_cm_cover_episode_name, strrpos($files_cm_cover_episode_name, "."), strlen($files_cm_cover_episode_name));

	/* 고정된 파일 명 */
	$files_cm_cover_name_define = 'cover'.strtolower($cm_cover_extension);
	$files_cm_cover_sub_name_define = 'cover_sub'.strtolower($cm_cover_sub_extension);
	$files_cm_cover_episode_name_define = 'cover_episode'.strtolower($cm_cover_episode_extension);

	/* 저장 경로 및 DB 파일명 및 파일경로 저장 */
	$cm_cover_src = $img_path.$files_cm_cover_name_define;
	$cm_cover_sub_src = $img_path.$files_cm_cover_sub_name_define;
	$cm_cover_episode_src = $img_path.$files_cm_cover_episode_name_define;

	/* 파일 업로드 */ 
	$files_cm_cover_result = $files_cm_cover_sub_result = $files_cm_cover_episode_result = false;
	if ($files_cm_cover_name != '') { 
		if($_cm_cover_noresizing == 'y'){
			$files_cm_cover_result = kt_storage_upload($files_cm_cover_tmp_name, $img_path, $files_cm_cover_name_define); 
			$files_cm_cover_result = tn_set_key_thumbnail($files_cm_cover_tmp_name, $img_path, 'cm_cover' ,'not', '', $files_cm_cover_name_define);	
			$files_cm_cover_result = tn_upload_thumbnail($files_cm_cover_tmp_name, $img_path, $files_cm_cover_name_define, 'cm_cover' ,'not');	
		}else{
			$files_cm_cover_result = tn_set_key_thumbnail($files_cm_cover_tmp_name, $img_path, 'cm_cover' ,'all', '', $files_cm_cover_name_define);
			$files_cm_cover_result = tn_upload_thumbnail($files_cm_cover_tmp_name, $img_path, $files_cm_cover_name_define, 'cm_cover' ,'all');
		}
	}
	if ($files_cm_cover_sub_name != '') { 
		if($_cm_cover_sub_noresizing == 'y'){
			$files_cm_cover_sub_result = kt_storage_upload($files_cm_cover_sub_tmp_name, $img_path, $files_cm_cover_sub_name_define); 
			$files_cm_cover_sub_result = tn_set_key_thumbnail($files_cm_cover_sub_tmp_name, $img_path, 'cm_cover_sub' ,'not', '', $files_cm_cover_sub_name_define);	
			$files_cm_cover_sub_result = tn_upload_thumbnail($files_cm_cover_sub_tmp_name, $img_path, $files_cm_cover_sub_name_define, 'cm_cover_sub' ,'not');	
		}else{
			$files_cm_cover_sub_result = tn_set_key_thumbnail($files_cm_cover_sub_tmp_name, $img_path, 'cm_cover_sub' ,'all', '', $files_cm_cover_sub_name_define);
			$files_cm_cover_sub_result = tn_upload_thumbnail($files_cm_cover_sub_tmp_name, $img_path, $files_cm_cover_sub_name_define, 'cm_cover_sub' ,'all');
		}
	}
	if ($files_cm_cover_episode_name != '') { 
		if($_cm_cover_episode_noresizing == 'y'){
			$files_cm_cover_episode_result = kt_storage_upload($files_cm_cover_episode_tmp_name, $img_path, $files_cm_cover_episode_name_define); 
			$files_cm_cover_episode_result = tn_set_key_thumbnail($files_cm_cover_episode_tmp_name, $img_path, 'cm_cover_episode' ,'not', '', $files_cm_cover_episode_name_define);	
			$files_cm_cover_episode_result = tn_upload_thumbnail($files_cm_cover_episode_tmp_name, $img_path, $files_cm_cover_episode_name_define, 'cm_cover_episode' ,'not');	
		}else{
			$files_cm_cover_episode_result = tn_set_key_thumbnail($files_cm_cover_episode_tmp_name, $img_path, 'cm_cover_episode' ,'all', '', $files_cm_cover_episode_name_define);
			$files_cm_cover_episode_result = tn_upload_thumbnail($files_cm_cover_episode_tmp_name, $img_path, $files_cm_cover_episode_name_define, 'cm_cover_episode' ,'all');
		}
	}
	rmdirAll(NM_THUMB_PATH.'/'.$img_path); // kt storage - comics 디렉토리 삭제
	rmdirAll(NM_PATH.'/'.$img_path); // kt storage - comics 디렉토리 삭제
	
	/* 임시파일이 존재하는 경우 삭제 */
	if (file_exists($files_cm_cover_tmp_name) && is_file($files_cm_cover_tmp_name)) {
		unlink($files_cm_cover_tmp_name);
	}
	if (file_exists($files_cm_cover_sub_tmp_name) && is_file($files_cm_cover_sub_tmp_name)) {
		unlink($files_cm_cover_sub_tmp_name);
	}
	if (file_exists($files_cm_cover_episode_tmp_name) && is_file($files_cm_cover_episode_tmp_name)) {
		unlink($files_cm_cover_episode_tmp_name);
	}

	$sql_image = ' UPDATE '.$dbtable.' SET ';
	/* 이미지 field값 추가 */
	if ($files_cm_cover_name != '' && $files_cm_cover_result == true){ $sql_image.= ' cm_cover = "'.$cm_cover_src.'", '; }
	if ($files_cm_cover_sub_name != '' && $files_cm_cover_sub_result == true){ $sql_image.= ' cm_cover_sub = "'.$cm_cover_sub_src.'", '; }
	if ($files_cm_cover_episode_name != '' && $files_cm_cover_episode_result == true){ $sql_image.= ' cm_cover_episode = "'.$cm_cover_episode_src.'", '; }
	$sql_image = substr($sql_image,0,strrpos($sql_image, ","));
	$sql_image.= " WHERE ".$dbt_primary ."='".$cm_cover_comics."'";
	

	if ($files_cm_cover_name != '' || $files_cm_cover_sub_name != '' || $files_cm_cover_episode_name != ''){
		/* DB 저장 */
		if(sql_query($sql_image)){
			$db_result['msg'].= '\n'.$_cm_series.'의 이미지가 저장되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'].= '\n'.'이미지 에러가 발생하여 저장되지 않았습니다.';
			$db_result['error'].= $sql_image;
		}
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
pop_close($db_result['msg'], '', $db_result['error']);
die;