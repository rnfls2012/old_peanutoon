<?php
include_once '../list/_common.php'; // 공통

/*
if($mb_level < 6) { 
	alert_close($mb_level."접근권한이 없습니다.");
	die;
}
*/

$today = NM_TIME_YMD;
/* 180205 임시 수정
$comics_where = stripslashes($_REQUEST['comics_where']); // 검색 조건
$comics_order = $_REQUEST['comics_order']; // 정렬 조건
*/
$comics_where = stripslashes(stripslashes($_GET['comics_where'])); // 검색 조건
$comics_order = $_GET['comics_order']; // 정렬 조건

/* JOIN */
/*
$comics_join = " from comics c left JOIN comics_publisher c_publ ON c_publ.cp_no = c.cm_publisher 
									   left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional
									   left JOIN comics_provider c_prov ON c_prov.cp_no = c.cm_provider 
									   left JOIN comics_provider c_prov_sub ON c_prov_sub.cp_no = c.cm_provider_sub 									   
									   "; 180205 임시 수정
*/
$comics_join = "	from comics c 
					left JOIN comics_publisher c_publ ON c_publ.cp_no = c.cm_publisher 
					left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
					left JOIN comics_professional c_prof_sub ON c_prof_sub.cp_no = c.cm_professional_sub 
					left JOIN comics_professional c_prof_thi ON c_prof_thi.cp_no = c.cm_professional_thi 
					left JOIN comics_provider c_prov ON c_prov.cp_no = c.cm_provider 
					left JOIN comics_provider c_prov_sub ON c_prov_sub.cp_no = c.cm_provider_sub 
";

$comics_field = " c.*, c_publ.cp_name as publ_name, c_prof.cp_name as prof_name, c_prov.cp_name as prov_name, c_prov_sub.cp_name as prov_name_sub "; // 가져올 필드 정하기

$comics_sql = "select $comics_field $comics_join where 1 $comics_where $comics_order";

$result = mysql_query($comics_sql);  

// 엑셀 Library Import
include_once('../../../lib/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once('../../../lib/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
array_push($data_head, '번호');
array_push($data_head, '썸네일');
array_push($data_head, '작품명');
array_push($data_head, '장르');
array_push($data_head, '출판사');
array_push($data_head, '작가');
array_push($data_head, '제공사');
array_push($data_head, '서비스');
array_push($data_head, '등록일');
array_push($data_head, '총 권(화)수');
array_push($data_head, '완결 구분');
array_push($data_head, '성인 구분');
array_push($data_head, '줄거리');
/*
array_push($data_head, '클릭 수');
array_push($data_head, '구매 수');
*/

// DB 컬럼용 배열
$data_key = array();
array_push($data_key, 'cm_no');
array_push($data_key, 'cm_cover_sub');
array_push($data_key, 'cm_series');
array_push($data_key, 'cm_small'); 
array_push($data_key, 'publ_name'); 
array_push($data_key, 'prof_name'); 
array_push($data_key, 'prov_name');
array_push($data_key, 'cm_service');
array_push($data_key, 'cm_reg_date');
array_push($data_key, 'cm_episode_total');
array_push($data_key, 'cm_end'); 
array_push($data_key, 'cm_adult'); 
array_push($data_key, 'cm_somaery'); 
/*
array_push($data_key, 'cm_click');
array_push($data_key, 'cm_buy_count');
*/

/* 컬럼 이름 지정 부분 */

$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write(0, $col++, $cell);
} // end foreach

/* 데이터 전달 부분 */
for ($i=1; $row=mysql_fetch_array($result); $i++){
	foreach($data_key as $cell_key => $cell_val) {
		switch($cell_key) {
			case 3:
				$worksheet->write($i, $cell_key , iconv_cp949($nm_config['cf_small'][$row[$cell_val]]));
				break;
			
			case 7:
				$worksheet->write($i, $cell_key , iconv_cp949($d_cm_service[$row['cm_service']]));
				break;

			case 10:
				$worksheet->write($i, $cell_key , iconv_cp949($d_cm_end[$row['cm_end']]));
				break;

			case 11:
				$worksheet->write($i, $cell_key , iconv_cp949($d_adult[$row['cm_adult']]));
				break;

			case 12:
				$worksheet->write($i, $cell_key , iconv_cp949(stripslashes($row['cm_somaery'])));
				break;

			default:
				$worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]));
				break;
		} // end switch
	} // end foreach
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."comics_list_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"comics_list_".$today.".xls");
header("Content-Disposition: inline; filename=\"comics_list_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
?>