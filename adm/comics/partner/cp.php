<?
include_once '_common.php'; // 공통

/* PARAMITER 
cp_name // 제공사이름
*/

$cp_css = "c_prov_";


// 기본적으로 몇개 있는 지 체크;
$sql_provider_total = "select count(*) as total_provider from comics_provider where 1  ";
$total_provider = sql_count($sql_provider_total, 'total_provider');

// 삭제버튼 제외(fileupload에 등록되어 있는 제공사 제외)

/* 데이터 가져오기 */
$where_provider = "";

// 검색단어+ 검색타입
if($_s_text){
	$provider_where.= "and cp_name like '%$_s_text%' ";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "cp_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$provider_order = "order by ".$_order_field." ".$_order;

$sql_provider = "";
$field_provider = " * "; // 가져올 필드 정하기
$limit_provider = "";

$sql_provider = "select $field_provider FROM comics_provider where 1 $provider_where $provider_order $limit_provider";
$result = sql_query($sql_provider);
$row_size = sql_num_rows($result);


$cp_no_arrs = $cp_no_arr = $cp_no_sub_arr = array();
$comics_provider_list = sql_query("SELECT cm_provider, cm_provider_sub FROM comics WHERE 1 group by cm_provider, cm_provider_sub order by cm_provider", $connect);
while ($f_f_l_row = sql_fetch_array($comics_provider_list)) {
	array_push($cp_no_arr,  $f_f_l_row['cm_provider']);
	array_push($cp_no_sub_arr,  $f_f_l_row['cm_provider_sub']);
}
$cp_no_arrs = array_unique(array_merge($cp_no_arr, $cp_no_sub_arr));

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','cp_no',1));
array_push($fetch_row, array('제공사명','cp_name',1));
array_push($fetch_row, array('관리','cp_management',0));

$page_title = "제공사";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>총 <?=$page_title?> : <?=number_format($total_provider);?> 명</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','cp_wirte', <?=$popup_cms_width;?>, 300);"><?=$page_title?> 등록</button>
	</div>
</section><!-- partner_head -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="partner_search_form" id="partner_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return partner_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">

					<?	$s_limit_text = "줄";
						for($sll=10; $sll<=$nm_config['s_limit']; $sll+=10){
							$s_limit_list[$sll] = $sll.$s_limit_text; ; 
						}
						tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>

					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit partner_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="130"  value="검색" id="partner_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- partner_search -->

<section id="partner_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $cp_css.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?cp_no=".$dvalue_val['cp_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					/* 대분류 */
					$row_big_text = '';
					if($th_big_text != ''){ $row_big_text = "<p class='big_color'>".$nm_config['big'][$dvalue_val['big']]."</p>"; }

					/* 삭제버튼 */
					$del_btn_use = "ok";

					$get_comics_link_s = $get_comics_link_e = "";

					if(in_array($dvalue_val['cp_no'], $cp_no_arrs)){
						$del_btn_use = "";
						/* 사용되는 URL */
						$sql_cm_provider = " select * from comics where cm_provider='".$dvalue_val['cp_no']."' OR cm_provider_sub='".$dvalue_val['cp_no']."' ";
						$row_cm_provider = sql_fetch($sql_cm_provider);						
						$popup_comics_url = NM_ADM_URL."/comics/list/list.write.php?big=".$row_cm_provider['cm_big']."&comics=".$row_cm_provider['cm_no']."&mode=mod";
						
						$get_comics_link_s = "<a href='#등록수정' onclick='popup(\"".$popup_comics_url."\",\"book_episode_list\",".$popup_cms_width.", 900)'; >"; 
						$get_comics_link_e = "</a>";
					}
				?>
				<tr class="result_hover">
					<td class="<?=$cp_css.$fetch_row[0][1]?> text_center"><?=$dvalue_val['cp_no'];?></td>
					<td class="<?=$cp_css.$fetch_row[1][1]?> text_center"><?=$get_comics_link_s;?><?=$dvalue_val['cp_name'];?><?=$get_comics_link_e;?></td>
					<td class="<?=$cp_css.$fetch_row[2][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','cp_mod_wirte', <?=$popup_cms_width;?>, 300);">수정</button>
						<?if($del_btn_use == "ok"){?>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','cp_del_wirte', <?=$popup_cms_width;?>, 300);">삭제</button>
						<?}?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- partner_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>