<?
include_once '_common.php'; // 공통

/* PARAMITER 
keyword_name // 제공사이름
*/


// 기본적으로 몇개 있는 지 체크;
$sql_keyword_total = "select count(*) as total_keyword from comics_keyword where 1  ";
$total_keyword = sql_count($sql_keyword_total, 'total_keyword');

// 삭제버튼 제외(fileupload에 등록되어 있는 제공사 제외)

/* 데이터 가져오기 */
$where_keyword = "";

// 검색단어+ 검색타입
if($_s_text){
	$keyword_where.= "and ck_name like '%$_s_text%' ";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "ck_id"; }
if($_order == null || $_order == ""){ $_order = "asc"; }
$keyword_order = "order by ".$_order_field." ".$_order;

$sql_keyword = "";
$field_keyword = " * "; // 가져올 필드 정하기
$limit_keyword = "";

$sql_keyword = "select $field_keyword FROM comics_keyword where 1 $keyword_where $keyword_order $limit_keyword";
$result = sql_query($sql_keyword);
$row_size = sql_num_rows($result);

/* fileupload에서 사용되는 키워드 배열로 저장 */
$ck_id_arrs = $ck_id_arr = $ck_id_sub_arr = $ck_id_thi_arr = array();
$fileload_ck_id_list = sql_query("SELECT cm_no, cm_ck_id, cm_ck_id_sub, cm_ck_id_thi FROM comics WHERE cm_ck_id !='' group by cm_ck_id, cm_ck_id_sub, cm_ck_id_thi order by cm_ck_id, cm_ck_id_sub, cm_ck_id_thi", $connect);
while ($f_k_l_row = sql_fetch_array($fileload_ck_id_list)) {
	$ck_id_arr_list = explode("|",$f_k_l_row['cm_ck_id']);
	foreach($ck_id_arr_list as $ck_id_arr_key => $ck_id_arr_val){
		array_push($ck_id_arr,  $ck_id_arr_val);
	}
	$ck_id_sub_arr_list = explode("|",$f_k_l_row['cm_ck_id_sub']);
	foreach($ck_id_sub_arr_list as $ck_id_sub_arr_key => $ck_id_sub_arr_val){
		array_push($ck_id_sub_arr,  $ck_id_sub_arr_val);
	}
	$ck_id_thi_arr_list = explode("|",$f_k_l_row['cm_ck_id_thi']);
	foreach($ck_id_thi_arr_list as $ck_id_thi_arr_key => $ck_id_thi_arr_val){
		array_push($ck_id_thi_arr,  $ck_id_thi_arr_val);
	}
}
$ck_id_arrs = array_unique(array_merge($ck_id_arr, $ck_id_sub_arr, $ck_id_thi_arr));

/* 출력필드리스트 - array('표제목','정렬필드명', 정렬) */
$fetch_row = array();
array_push($fetch_row, array('키워드 분류코드','ck_id',0));
array_push($fetch_row, array('키워드 명','ck_name',0));
array_push($fetch_row, array('키워드 등급','ck_adult',0));
array_push($fetch_row, array('관리','ck_management',0));

$page_title = "키워드";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>총 <?=$page_title?> : <?=number_format($total_keyword);?> 명</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','keyword_wirte', <?=$popup_cms_width;?>, 300);"><?=$page_title?> 등록</button>
	</div>
</section><!-- partner_head -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="keyword_search_form" id="keyword_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return keyword_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">

					<?	$s_limit_text = "줄";
						for($sll=10; $sll<=$nm_config['s_limit']; $sll+=10){
							$s_limit_list[$sll] = $sll.$s_limit_text; ; 
						}
						tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>

					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit partner_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="130"  value="검색" id="partner_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- partner_search -->

<section id="partner_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){

					/* 하위분류코드 처리 */
					$ck_code_class = $ck_code_img = "";
					$ck_class_text = '2Depth';
					if(strlen($dvalue_val['ck_id']) == 4){ $ck_code_class = 'ck_class1'; $ck_class_text = ''; }
					if($ck_code_class != ""){ $ck_code_img = '<img class="'.$ck_code_class.'" src="'.NM_IMG.'cms/low.gif" alt="하위분류" />'; }

					/* 등급 */
					$ck_adult_text = '청소년';
					if($dvalue_val['ck_adult'] == '1'){ $ck_adult_text = '성인'; }

					/* 컨텐츠 수정/삭제 url */
					$popup_url = $_cms_write."?ck_id=".$dvalue_val['ck_id'];
					$popup_reg_url = $popup_url."&mode=reg";
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					/* 삭제버튼 */
					$del_btn_use = "ok";
					if(in_array($dvalue_val['ck_id'], $ck_id_arrs)){
						$del_btn_use = "";
					}
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['ck_id'];?></td>
					<td class="<?=$fetch_row[1][1]?>">
						<?=$ck_code_img;?>
						<?=$dvalue_val['ck_name'];?>
					</td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$ck_adult_text;?></td>
					<td class="<?=$fetch_row[3][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','keyword_mod_wirte', <?=$popup_cms_width;?>, 300);">수정</button>
						<?if($del_btn_use == "ok"){?>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','keyword_del_wirte', <?=$popup_cms_width;?>, 300);">삭제</button>
						<?}?>
						<?if(strlen($dvalue_val['ck_id']) < 4){?>
						<button class="add_btn" onclick="popup('<?=$popup_reg_url;?>','keyword_add_wirte', <?=$popup_cms_width;?>, 300);"><?=$ck_class_text?> 추가</button>
						<?}?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- partner_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>