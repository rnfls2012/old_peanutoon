<?
include_once '_common.php'; // 공통

/* PARAMITER 
menu_name // 제공사이름
*/
$_cn_id = tag_filter($_REQUEST['cn_id']);


// 기본적으로 몇개 있는 지 체크;
$sql_menu_total = "select count(*) as total_menu from comics_menu where 1 AND left(cn_id, 2) = '$_cn_id' AND LENGTH(cn_id)=4 AND cn_dev='n' ";
$total_menu = sql_count($sql_menu_total, 'total_menu');

// 삭제버튼 제외(fileupload에 등록되어 있는 제공사 제외)

/* 데이터 가져오기 */
$menu_where = " AND left(cn_id, 2) = '$_cn_id' AND LENGTH(cn_id)=4 AND cn_dev='n' ";

// 검색단어+ 검색타입
if($_s_text){
	$menu_where.= "and cn_name like '%$_s_text%' ";
}

// 정렬
$menu_order = "order by cn_display_check asc, cn_order asc, cn_id asc";

$sql_menu = "";
$field_menu = " *,  CASE WHEN cn_display = 'y' THEN 0 ELSE 1 END AS cn_display_check "; // 가져올 필드 정하기
$limit_menu = "";

$sql_menu = "select $field_menu FROM comics_menu where 1 $menu_where $menu_order $limit_menu";
$result = sql_query($sql_menu);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명', 정렬) */
$fetch_row = array();
array_push($fetch_row, array('메뉴 분류코드','cn_id',0));
array_push($fetch_row, array('메뉴 명','cn_name text_left',0));
array_push($fetch_row, array('메뉴 URL','cn_link text_left',0));
array_push($fetch_row, array('메뉴 등급','cn_adult',0));
array_push($fetch_row, array('메뉴 보기','cn_display',0));
array_push($fetch_row, array('메뉴 순서','cn_order',0));
array_push($fetch_row, array('관리','cn_management',0));

$page_title = "2depth 메뉴";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()

/* 메뉴 url */
$_cms_self_gnb = str_replace("menu_lnb.php", "menu.php", $_cms_self);

/* 컨텐츠 수정/삭제 url */
$_cms_write_lnb = str_replace("menu_lnb.write.php", "menu.write.php", $_cms_write);

?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>총 <?=$page_title?> : <?=number_format($total_menu);?> 건</strong>
	</div>
	<div class="write">
		<a href="<?=$_cms_self_gnb;?>">상단메뉴이동</a>
		<button onclick="popup('<?=$_cms_write_lnb;?>?cn_id=<?=$_cn_id;?>','menu_wirte', <?=$popup_cms_width;?>, 550);"><?=$page_title?> 등록</button>
	</div>
</section><!-- partner_head -->

<section id="partner_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
		<ul>
			<li>메뉴 순서 낮을 수록 우선순위 입니다.</li>
			<li>우선순위가 동일한 숫자시 메뉴 분류코드가 낮은 수록 우선순위 입니다.</li>
		</ul>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){

					/* 하위분류코드 처리 */
					$cn_id_class = $cn_id_img = "";
					$cn_class_text = '2Depth';
					if(strlen($dvalue_val['cn_id']) == 4){ $cn_id_class = 'cn_class1'; $cn_class_text = '3Depth'; }
					if(strlen($dvalue_val['cn_id']) == 6){ $cn_id_class = 'cn_class2'; $cn_class_text = ''; }
					if($cn_id_class != ""){ $cn_id_img = '<img class="'.$cn_id_class.'" src="'.NM_IMG.'cms/low.gif" alt="하위분류" />'; }
					

					/* 컨텐츠 수정/삭제 url - 상단 */
					$popup_url = $_cms_write_lnb."?cn_id=".$dvalue_val['cn_id'];
					$popup_reg_url = $popup_url."&mode=reg";
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					/* 메뉴 링크 */
					$db_cn_link = NM_URL."/".$dvalue_val['cn_link'];

					/* 메뉴 성인구분 */
					$db_cn_adult = $d_adult[$dvalue_val['cn_adult']];

					/* 메뉴 보임구분 */
					$db_cn_display = $d_class_display[$dvalue_val['cn_display']];
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['cn_id'];?></td>
					<td class="<?=$fetch_row[1][1]?>">
						<?=$cn_id_img;?>
						<?=$dvalue_val['cn_name'];?>
					</td>
					<td class="<?=$fetch_row[2][1]?>"><a href="<?=$db_cn_link?>" target="_self"><?=$db_cn_link?></a></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$db_cn_adult;?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$db_cn_display;?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$dvalue_val['cn_order'];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','menu_mod_wirte', <?=$popup_cms_width;?>, 550);">수정</button>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','menu_del_wirte', <?=$popup_cms_width;?>, 550);">삭제</button>
						<?if(strlen($dvalue_val['cn_id']) < 4){?>
						<button class="add_btn" onclick="popup('<?=$popup_reg_url;?>','menu_add_wirte', <?=$popup_cms_width;?>, 550);"><?=$cn_class_text?> 추가</button>
						<?}?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- partner_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>