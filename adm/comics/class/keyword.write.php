<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_ck_id = tag_filter($_REQUEST['ck_id']);

$sql = "SELECT * FROM comics_keyword WHERE ck_id = '$_ck_id' ";
				 
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
			/* 키워드 분류코드 */
			$len = strlen($_ck_id);
			// 2뎁스만 사용
			if ($len > 4){
				pop_close("키워드 분류코드를 더 이상 추가할 수 없습니다.\\n\\n2단계 키워드 분류코드까지만 가능합니다.");
				die;
			}
			if ($len > 1){
				$sql_ck_id_high = "SELECT * FROM comics_keyword WHERE ck_id = '$_ck_id' ";
				$row_ck_id_high = sql_fetch($sql_ck_id_high);
				$row['ck_adult'] = $row_ck_id_high['ck_adult'];
			}
			$len2 = $len + 1;
			$sql_len_max = "SELECT MAX(SUBSTRING(ck_id,$len2,2)) as max_ck_id FROM comics_keyword WHERE SUBSTRING(ck_id,1,$len) = '$_ck_id' ";
			$row_len_max = sql_fetch($sql_len_max);

			$base_convert = 36;
			$sub_ck_id = base_convert($row_len_max['max_ck_id'], $base_convert, 10);
			$sub_ck_id += 1;
			if ($sub_ck_id >= $base_convert * $base_convert)
			{
				alert("분류를 더 이상 추가할 수 없습니다.");
				// 빈상태로
				$sub_ck_id = "  ";
			}
			$sub_ck_id = base_convert($sub_ck_id, 10, $base_convert);
			$sub_ck_id = substr("0" . $sub_ck_id, -2);
			$sub_ck_id = $_ck_id . $sub_ck_id;
			$_ck_id = $sub_ck_id;

}
/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "키워드";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- partner_head -->
<? /* fileupload -> partner */ ?>
<section id="partner_write">
	<form name="keyword_write_form" id="keyword_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return keyword_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="ck_id" name="ck_id" value="<?=$_ck_id;?>"/><!-- 수정/삭제시 컨텐트번호 -->

		<table>
			<tbody>
				<tr>
					<th><label for="ck_name">키워드 분류코드</label></th>
					<td><?=$_ck_id;?></td>
				</tr>
				<tr>
					<th><label for="ck_name"><?=$required_arr[0];?>키워드명</label></th>
					<td><input type="text" placeholder="키워드명 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="ck_name" id="ck_name" value="<?=$row['ck_name'];?>" autocomplete="off" /></td>
				</tr>
				<tr>
					<th><label for="ck_adult"><?=$required_arr[0];?>키워드등급</label></th>
					<td>
						<div id="ck_adult" class="input_btn">
							<input type="radio" name="ck_adult" id="ck_adult0" value="0" <?=get_checked('', $row['ck_adult']);?> <?=get_checked('0', $row['ck_adult']);?> />
							<label for="ck_adult0">청소년</label>
							<input type="radio" name="ck_adult" id="ck_adult1" value="1" <?=get_checked('1', $row['ck_adult']);?> />
							<label for="ck_adult1">성인</label>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- partner_write -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>