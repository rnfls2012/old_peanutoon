<?
include_once '_common.php'; // 공통

/* 데이터에서 해당 장르 설정했는지 체크 */
$small_db_list = array();
$sql_fl_small = "SELECT * FROM  comics GROUP BY cm_small ORDER BY cm_small";
$result_fl_small = sql_query($sql_fl_small);
while ($row_fl_small = sql_fetch_array($result_fl_small)) {
	array_push($small_db_list,  $row_fl_small['cm_small']);
}

/* 장르갯수 */
$total_small = count($nm_config['cf_small']) -1;

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','cm_small_no',0));
array_push($fetch_row, array('장르명','cm_small_name',0));
array_push($fetch_row, array('관리','cm_small_management',0));

$page_title = "장르";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>총 <?=$page_title?> : <?=number_format($total_small);?> 건</strong>
	</div>
	<div class="write">
		<button onclick="small_add();"><?=$page_title?> 추가</button>
	</div>
</section><!-- partner_head -->

<section id="partner_result">
	<div id="cs_bg">
		<form name="small_list_form" id="small_list_form" method="post" action="<?=$_cms_update;?>" onsubmit="return small_list_submit();">
			<table>
				<thead id="cs_thead">
					<tr>
					<?
					//정렬표기
					$order_giho = "▼";
					$th_order = "desc";
					if($_order == 'desc'){ 
						$order_giho = "▲"; 
						$th_order = "asc";
					}
					foreach($fetch_row as $fetch_key => $fetch_val){

						$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
						if($fetch_val[2] == 1){
							$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
							$th_ahref_e = '</a>';
						}
						if($fetch_val[1] == $_order_field){
							$th_title_giho = $order_giho;
						}
						$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
						$th_class = $fetch_val[1];
					?>
						<th class="<?=$th_class?>"><?=$th_title?></th>
					<?}?>
					</tr>
				</thead>
				<tbody>
					<? foreach($nm_config['cf_small'] as $small_key => $small_val){
						if($small_key == 0){ continue; }
						/* 삭제버튼 */
						$del_btn_use = "ok";
						/*  디폴트값 갯수, fileupload의 small저장번호, fileupload의 small저장번호는 가장큰수 */
						if($small_key < count($small_default) || in_array($small_key, $small_db_list) || $small_key < max($small_db_list)){
							$del_btn_use = "";
						}
					?>
					<tr class="result_hover">
						<td class="<?=$fetch_row[0][1]?> text_center">
							<?=$small_key;?>
							<input class="input_small_no" type="hidden" name="small_no[]" value="<?=$small_key;?>" />
						</td>
						<td class="<?=$fetch_row[1][1]?> text_center">
							<input class="input_small_val" type="text" name="small_val[]" value="<?=$small_val;?>" /> 
						</td>
						<td class="<?=$fetch_row[2][1]?> text_center">
							<?if($del_btn_use == "ok"){?>
							<a class="del_btn" onclick="small_del('<?=($small_key)?>');">삭제</a>
							<?}else{?>삭제불가<?}?>
						</td>
					</tr>
					<?}?>
				</tbody>
			</table>
			<div id="cm_small_list_submit">
				<input type="submit" value="장르 리스트 내용 저장" />
			</div>
		</form>
	</div>
</section><!-- partner_result -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>