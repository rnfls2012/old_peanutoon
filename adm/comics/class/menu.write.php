<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_cn_id = tag_filter($_REQUEST['cn_id']);

$sql = "SELECT * FROM comics_menu WHERE cn_id = '$_cn_id' ";
				 
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: 			
			$cn_link_gnb = "";
			if($_mode == 'reg' && $_cn_id !='' && strlen($_cn_id) == 2 && $row['cn_link'] == ''){				
				//상단 URL 구하기
				$sql_where_cn_link_gnb = "AND LENGTH(cn_id)=2 AND cn_id='".$_cn_id."'";
				$sql_cn_link_gnb = "SELECT * FROM comics_menu WHERE 1 $sql_where_cn_link_gnb ";
				$row_cn_link_gnb = sql_fetch($sql_cn_link_gnb);
				$row['cn_link'] = $row_cn_link_gnb['cn_link'];
			}

			$mode_text = "등록";
			 $sql = "";
			/* 메뉴 분류코드 */
			$len = strlen($_cn_id);
			if ($len > 6){
				pop_close("메뉴 분류코드를 더 이상 추가할 수 없습니다.\\n\\n3단계 메뉴 분류코드까지만 가능합니다.");
				die;
			}
			if ($len > 1){
				$sql_cn_id_high = "SELECT * FROM comics_menu WHERE cn_id = '$_cn_id' AND cn_dev='n' ";
				$row_cn_id_high = sql_fetch($sql_cn_id_high);
				$row['cn_adult'] = $row_cn_id_high['cn_adult'];
			}
			$len2 = $len + 1;
			$sql_len_max = "SELECT MAX(SUBSTRING(cn_id,$len2,2)) as max_cn_id FROM comics_menu WHERE SUBSTRING(cn_id,1,$len) = '$_cn_id' AND cn_dev='n' ";
			$row_len_max = sql_fetch($sql_len_max);
			$sub_cn_id = base_convert($row_len_max['max_cn_id'], 36, 10);
			$sub_cn_id += 36;

			if ($sub_cn_id >= 36 * 36)
			{
				//alert("분류를 더 이상 추가할 수 없습니다.");
				// 빈상태로
				$sub_cn_id = "  ";
			}
			$sub_cn_id = base_convert($sub_cn_id, 10, 36);
			$sub_cn_id = substr("00" . $sub_cn_id, -2);
			$sub_cn_id = $_cn_id . $sub_cn_id;
			$_cn_id = $sub_cn_id;
			
			// 내서재, 고객센터 고정
			if(substr($_cn_id,0,2)=='y0' || substr($_cn_id,0,2)=='z0'){
				pop_close('등록이 불가능 합니다 개발자에게 문의해주세요.', '', $db_result['error']);
				die;
			}

			//정렬 기본값
			$row['cn_order'] = 10;
}
/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "메뉴";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- partner_head -->
<? /* fileupload -> partner */ ?>
<section id="partner_write">
	<form name="keyword_write_form" id="keyword_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return keyword_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="cn_id" name="cn_id" value="<?=$_cn_id;?>"/><!-- 수정/삭제시 컨텐트번호 -->

		<table>
			<tbody>
				<tr>
					<th><label for="cn_name">메뉴 분류코드</label></th>
					<td><?=$_cn_id;?></td>
				</tr>
				<tr>
					<th><label for="cn_name"><?=$required_arr[0];?>메뉴명</label></th>
					<td><input type="text" placeholder="메뉴명 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="cn_name" id="cn_name" value="<?=$row['cn_name'];?>" autocomplete="off" /></td>
				</tr>
				<tr>
					<th><label for="cn_link"><?=$required_arr[0];?>링크URL</label></th>
					<td><input type="text" placeholder="링크URL 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="cn_link" id="cn_link" value="<?=$row['cn_link'];?>" autocomplete="off" />
					<p class="explan"><?=NM_URL.'/'?>를 제외하고 입력해주세요.</p>
					</td>
				</tr>
				<tr>
					<th><label for="cn_adult"><?=$required_arr[0];?>메뉴등급</label></th>
					<td>
						<div id="cn_adult" class="input_btn">
							<input type="radio" name="cn_adult" id="cn_adult0" value="n" <?=get_checked('', $row['cn_adult']);?> <?=get_checked('n', $row['cn_adult']);?> />
							<label for="cn_adult0">청소년</label>
							<input type="radio" name="cn_adult" id="cn_adult1" value="y" <?=get_checked('y', $row['cn_adult']);?> />
							<label for="cn_adult1">성인</label>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="cn_display"><?=$required_arr[0];?>메뉴 보기</label></th>
					<td>
						<div id="cn_display" class="input_btn">
							<input type="radio" name="cn_display" id="cn_display0" value="y" <?=get_checked('', $row['cn_display']);?> <?=get_checked('y', $row['cn_display']);?> />
							<label for="cn_display0">모두보임</label>
							<input type="radio" name="cn_display" id="cn_display1" value="n" <?=get_checked('n', $row['cn_display']);?> />
							<label for="cn_display1">숨김</label>
							<!--
							<input type="radio" name="cn_display" id="cn_display2" value="web" <?=get_checked('web', $row['cn_display']);?>
							<label for="cn_display2">웹</label>
							<input type="radio" name="cn_display" id="cn_display3" value="mob" <?=get_checked('mob', $row['cn_display']);?>
							<label for="cn_display3">모바일</label>
							<input type="radio" name="cn_display" id="cn_display4" value="app" <?=get_checked('app', $row['cn_display']);?>
							<label for="cn_display4">앱</label>
							-->
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="cn_order"><?=$required_arr[0];?>메뉴순서</label></th>
					<td><input type="text" class="" placeholder="꼭 숫자로 넣어주세용<?=$required_arr[2];?>" <?=$required_arr[1];?> name="cn_order" id="cn_order" value="<?=$row['cn_order'];?>" autocomplete="off" />
					<p class="explan">꼭 숫자로 넣어주세용</p>
					</td>
				</tr>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- partner_write -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>