<?
include_once '_common.php'; // 공통

/* 화별 link */
$get_comics = get_comics($_cm_no);
$db_comics_url_para = "&cm_big=".$get_comics['cm_big']."&cm_no=".$get_comics['cm_no'];
$db_comics_episode_url = $_cms_self."?sl_mode=episode".$db_comics_url_para;

/* PARAMITER */
/* 데이터 가져오기 */
$sql_where.= " AND sl.sl_comics='".$_cm_no."'";

// 파트너일 경우 - 날짜
$_s_month		= mb_partner_e_month($nm_member, $_s_month);	// 종료일 현재 -1일 적용
$_e_month		= mb_partner_e_month($nm_member, $_e_month);	// 종료일 현재 -1일 적용

// 날짜
if($_s_month && $_e_month){ 
	$sql_where.= date_year($_s_month, $_e_month, 'sl.sl'); 
}else{
	// 날짜-파트너일 경우만
	if($nm_member['mb_class'] == 'p'){ 
		$sql_where.= " AND sl.sl_year_month <'".NM_TIME_YM."' "; 
	}
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sl_year_month"){ 
	$_order_field = "sl_year_month"; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
}

$sql_order = "order by ".$_order_field." ".$_order;

// 그룹
$sql_group = " group by sl.sl_year_month ";

// SQL문
$sql_field = "	sl.sl_year_month,  left(sl.sl_year_month,4) as sl_year, 

							sum(sl.sl_cash_point)as sl_cash_point_sum, 
							sum(sl.sl_cash_point*cup_won)as sl_cash_point_won_sum, 
							
							sum(sl.sl_all_pay+sl.sl_pay)as sl_cnt_sum, 
							sum(sl.sl_open)as sl_open_sum 
							"; // 가져올 필드 정하기

$sql_count	= $sql_field. " , count( sl.sl_year_month ) as cnt ";


$sql_select = "		select {$sql_field} ";
$sql_select_cnt = "	select {$sql_count} ";
$sql_table = "		FROM w_vsales sl
					JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type  ";

$sql_query		= " {$sql_select}		{$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_select_cnt}	{$sql_table} {$sql_where} ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts		= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data_cnts = sql_fetch($sql_query_cnt);	// SQL문 총값-레코드값
$rows_data	= rows_data($sql_querys, false);	// SQL문 결과 레코드값

$s_all_sum = number_format($rows_data_cnts['sl_cash_point_won_sum']);
$s_all_cnt = number_format($rows_data_cnts['sl_cnt_sum']);
$s_all_open_cnt = number_format($rows_data_cnts['sl_open_sum']);
$s_all_cash_point = number_format($rows_data_cnts['sl_cash_point_sum']);

// 연도별로
$sql_sales_y = "	{$sql_select}		{$sql_table} {$sql_where}
					group by sl_year 
					order by sl_year {$_order}
				";

$result_y = sql_query($sql_sales_y);
$row_size_y = sql_num_rows($result_y);

$years_data = array();
while ($row_y = sql_fetch_array($result_y)) {
	array_push($years_data, $row_y);
}

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sl_year_month',1));

/* cash_point */
array_push($fetch_row, array('열람건수(건)','sl_sum_open',4));
array_push($fetch_row, array('판매건수(건)','sl_sum_cnt1',4));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'('.$nm_config['cf_cash_point_unit'].')','sl_cash_point_sum1',4));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'금액(원)','sl_cash_point_won_sum1',4));


// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = $get_comics['cm_series']." 날짜별 매출";
$cms_head_title = $page_title;

if ($_mode == 'excel') {
	$result = sql_query($sql_querys);				// 쿼리 실행 후 Resource	
	$result_y = sql_query($sql_sales_y);		// 연도 별 합계 Resource
	
	$excel_path = 'excel/comics_all_date_excel.php';		// 엑셀 경로와 파일명

	if ($_cms_folder_path_push == '') {
		include_once './'.$excel_path;			// 같은 디렉토리 안에 있음.			
	} else {
		include_once $_cms_folder_path_push.'/'.$excel_path;			// 같은 디렉토리 안에 있음.	
	}	
}

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<script type="text/javascript">
<!--
	var cr_thead_mg_add = '22px'; /* fixed 테이블이 높이가 커서 그만큼 마진 */
//-->
</script>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1>
		<?=$cms_head_title?> <?=$cms_page_title?>
		<a class="a_btn episode" href="<?=$db_comics_episode_url?>">화별 보러가기</a>
	</h1>
	<div>&nbsp;</div>
	<div class="write">
		<input type="button" class="excel_down" value="<?=$page_title?> 엑셀 다운로드" onclick="location.href='<?=$_cms_self?>?<?=$_nm_paras;?>&mode=excel'"/>
	</div>
</section><!-- cms_title -->

<section id="sales_result">
	<h3 id="cr_thead_mg_add">
			<strong>검색 결과 기준</strong>
			<ul>
			<? if(mb_class_permission('a')) { ?>
				<li>관리자는 실시간 기준[<?=NM_TIME_YMDHIS;?>]으로 나옵니다.</li>
				<li>파트너사(작가,출판사,제공사,마커터 등등)은 현재시간 기준으로 전달[<?=NM_TIME_MON_M1;?>]까지만 나옵니다.<br/>(↓↓아래는 파트너일 경우에만 나오는 안내문입니다.)</li>
			<? } ?>
			<? if(mb_class_permission('p,a')) { ?>
				<li>전체 정산에서는 현재시간 기준으로 전달[<?=NM_TIME_MON_M1;?>]까지만 나옵니다.</li>	
			<? } ?>		
			</ul>
			<br/>
			<strong><?=$get_comics['cm_series'];?></strong><br/>
			연도별
			<table>
				<tr>
					<th class="topleft">연도</th>
					<th class="text_right">열람건수(건)</th>
					<th class="text_right">판매건수(건)</th>
					<th class="text_right"><?=$nm_config['cf_cash_point_unit_ko']?>(<?=$nm_config['cf_cash_point_unit']?>)</th>
					<th class="text_right topright"><?=$nm_config['cf_cash_point_unit_ko']?>금액(원)</th>
				</tr>
				<?	$all_sl_cash_point_sum = $all_sl_point_sum = $all_sl_cash_point_won_sum = $all_sl_point_won_sum = $all_sl_cnt_sum =  $all_sl_open_sum = 0;
					foreach($years_data as $years_val){ 
						$all_sl_cash_point_won_sum+=$years_val['sl_cash_point_won_sum'];
						$all_sl_cash_point_sum+=$years_val['sl_cash_point_sum'];
						$all_sl_cnt_sum+=$years_val['sl_cnt_sum'];
						$all_sl_open_sum+=$years_val['sl_open_sum'];
				?>
				<tr>
					<th><?=$years_val['sl_year'];?>년</th>
					<td class="text_right"><?=number_format($years_val['sl_open_sum']);?></td>
					<td class="text_right"><?=number_format($years_val['sl_cnt_sum']);?></td>
					<td class="text_right"><?=number_format($years_val['sl_cash_point_sum']);?></td>
					<td class="text_right"><?=number_format($years_val['sl_cash_point_won_sum']);?></td>
				</tr>
				<? } ?>
				<tr>
					<th>합계</th>
					<td class="text_right"><?=number_format($all_sl_open_sum);?></td>
					<td class="text_right"><?=number_format($all_sl_cnt_sum);?></td>
					<td class="text_right"><?=number_format($all_sl_cash_point_sum);?></td>
					<td class="text_right"><?=number_format($all_sl_cash_point_won_sum);?></td>
				</tr>
			</table>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
					if($fetch_val[2] == 4){
						$th_class.= " text_right";
					}
					if($fetch_val[2] == 3){
						$th_class.= " sl_point_bgcolor";
					}
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?
				$sl_cash_point_won	= 0; // 땅콩(원)
				$sl_cash_sum		= 0; // 땅콩(P)
				$sl_cash_point_cnt	= 0; // 땅콩판매 수

				$sl_open_sum		= 0; // 열람 수
				$sl_pay_sum			= 0; // 판매 수
				
				foreach($rows_data as $dvalue_key => $dvalue_val){
					$d_sl_year_month = $dvalue_val['sl_year_month'];
					$d_sl_won_sum = number_format($dvalue_val['sl_cash_point_won_sum']+$dvalue_val['sl_point_won_sum']);
					$d_sl_cnt_sum = number_format($dvalue_val['sl_cnt_sum']);
					$d_sl_open_sum = number_format($dvalue_val['sl_open_sum']);

					$d_sl_cash_point_won_sum = number_format($dvalue_val['sl_cash_point_won_sum']);
					$d_sl_cash_point_sum = number_format($dvalue_val['sl_cash_point_sum']);
					
					$sl_title = stripslashes($sl_title);
					
					$sl_cash_point_won	+= intval($dvalue_val['sl_cash_point_won_sum']);	// 땅콩(원)
					$sl_cash_sum		+= intval($dvalue_val['sl_cash_point_sum']);			// 땅콩(P)
					$sl_cash_point_cnt	+= intval($dvalue_val['sl_cnt_sum']);	// 판매 수


					$sl_open_sum		+= intval($dvalue_val['sl_open_sum']);			// 열람 수
					$sl_pay_sum		+= intval($dvalue_val['sl_cnt_sum']);				// 판매 수

				?>
				<tr class="<?=$week_class;?>">
					<td class="<?=$fetch_row[0][1]?> text_right"><?=$d_sl_year_month;?></td>
					<td class="<?=$fetch_row[1][1]?> text_right"><?=$d_sl_open_sum;?></td>
					<td class="<?=$fetch_row[2][1]?> text_right"><?=$d_sl_cnt_sum;?></td>

					<td class="<?=$fetch_row[3][1]?> text_right"><?=$d_sl_cash_point_sum;?></td>
					<td class="<?=$fetch_row[4][1]?> text_right"><?=$d_sl_cash_point_won_sum;?></td>
				</tr>
				<?}?>
				<tr class="sl_total_tr">
					<td class="sl_total">총 합</td>
					<td class="<?=$css_suffix.$fetch_row[2][1]?> text_right"><?=number_format($sl_open_sum);?></td>
					<td class="<?=$css_suffix.$fetch_row[3][1]?> text_right"><?=number_format($sl_pay_sum);?></td>
					<td class="<?=$css_suffix.$fetch_row[4][1]?> text_right"><?=number_format($sl_cash_sum);?></td>
					<td class="<?=$css_suffix.$fetch_row[5][1]?> text_right"><?=number_format($sl_cash_point_won);?></td>
				</tr>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>