<?
include_once '_common.php'; // 공통

/* 작가, 출판사, 제공사 데이터 가져오기 */
// $cp_arr = array("professional", "publisher", "provider");
$professional_arr = $publisher_arr = $provider_arr = $cp_arr = array();
array_push($cp_arr , array('professional','작가'));
array_push($cp_arr , array('publisher','출판사'));
array_push($cp_arr , array('provider','제공사'));

foreach($cp_arr as $cp_val){
	$sql_cp = "select * from comics_".$cp_val[0]." order by cp_name asc";
	$result_cp = sql_query($sql_cp);	
	$row_cp_save = array();

	while($row_cp = sql_fetch_array($result_cp)) {
		$row_cp_save[$row_cp['cp_no']] = $row_cp['cp_name'];
		${$cp_val[0].'_arr'} = $row_cp_save;
	}
}

// 샐럭트 박스 선택이 있다면.... (작가, 출판사, 제공사)
$cm_no_arr = $cp_no_arr = $cm_no_txt_arr = array(); // 샐렉트박스&문자열, 샐렉트박스, 문자열검색 
$cp_no_bool = false; // 샐렉트박스 검색결과 있는지 확인
$cm_no_txt_bool = false; // 문자열검색 검색결과 있는지 확인

$sql_where_cp_bool = false;
$sql_where_cnts = 0;
$sql_where_cps = "";
foreach($cp_arr as $cp_val){
	$sql_where_cp = "";
	if(${'_'.$cp_val[0]} != ''){
		$sql_where_cp_bool = true;
		$sql_where_cnts++;
		switch($cp_val[0]){
			case 'professional' :	$sql_where_cp = "	(  cm_professional = '".${'_'.$cp_val[0]}."' 
														OR cm_professional_sub = '".${'_'.$cp_val[0]}."'  
														OR cm_professional_thi = '".${'_'.$cp_val[0]}."' ) ";
			break;
			case 'publisher' :		$sql_where_cp = "	(  cm_publisher = '".${'_'.$cp_val[0]}."' ) ";
			break;
			case 'provider' :		$sql_where_cp = "	(  cm_provider = '".${'_'.$cp_val[0]}."' 
														OR cm_provider_sub = '".${'_'.$cp_val[0]}."' ) ";
			break;
		}

		if($sql_where_cnts == 1){
			$sql_where_cps.= $sql_where_cp;
		}else{
			$sql_where_cps.= " AND ".$sql_where_cp;
		}
	}
}
$cp_no_arr = get_cm_no_arr($sql_where_cps);

if(count($cp_no_arr) > 0 && $sql_where_cp_bool == true){ 
	$cp_no_bool = true;
} // 샐럭트박스 검색결과가 있다면...


// 2.  검색단어+ 검색타입
if($_s_text){
	switch($_s_type){
		case '0': $cm_no_txt_arr = array_merge(get_comics_series($_s_text));
		break;
		case '1': $cm_no_txt_arr = array_merge(get_cm_no_arr(get_cp_comics_sql($_s_text, 'professional')));
		break;
		case '2': $cm_no_txt_arr = array_merge(get_cm_no_arr(get_cp_comics_sql($_s_text, 'publisher')));
		break;
		case '3': $cm_no_txt_arr = array_merge(get_cm_no_arr(get_cp_comics_sql($_s_text, 'provider')));
		break;
		default:  $cm_no_txt_arr = array_merge(get_comics_series($_s_text), get_cm_no_arr(get_cp_comics_sql($_s_text, '')));
		break;
	}
}

if(count($cm_no_txt_arr) > 0 && $_s_text){
	$cm_no_txt_bool = true;
}

// 샐렉트박스&문자열
if($cp_no_bool == true && $cm_no_txt_bool == true){
	$cm_no_arr = array_values(array_intersect($cp_no_arr, $cm_no_txt_arr));
}else{
	if($cp_no_bool == true && $cm_no_txt_bool == false){
		$cm_no_arr = $cp_no_arr;
	}else if($cp_no_bool == false && $cm_no_txt_bool == true){
		$cm_no_arr = $cm_no_txt_arr;
	}else{
		if($sql_where_cp_bool == true || $_s_text){
			$sql_where.= " AND sl_comics = 0 "; 
		}
	}
}

// 샐렉트박스&문자열에서 나온 결과의 코믹스번호리스트
if(count($cm_no_arr) > 0){
	$sql_where.= " AND sl_comics in ( ".sql_array_merge($cm_no_arr)." ) ";
}

// 카테고리(단행본, 웹툰, 단행본웹툰)
if($_big > 0){
	$sql_where.= " AND sl_big =".$_big." ";
}

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

// 파트너일 경우
$sql_mb_cm_cp	= mb_cm_cp_sql($nm_member);		// 파트너 SQL
if($sql_mb_cm_cp !=''){
	$sql_where.= " AND sl_comics in ( ".sql_array_merge(get_cm_no_arr(" 1 ".$sql_mb_cm_cp))." ) ";
}

// 파트너일 경우 - 날짜
$_e_date		= mb_partner_e_date($nm_member, $_e_date);	// 종료일 현재 -1일 적용


// 날짜
if($_s_date && $_e_date){ 
	$sql_where.= date_year_month($_s_date, $_e_date, 'sl.sl'); 
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sl_cash_point_sum"){ 
	$_order_field = "sl_cash_point_sum"; 
	$_order_field_add = " , sl_comics "; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
	$_order_add= "desc"; 
}

// 요일 정렬시
if($_order_field == "sl_week"){
	$_order_field_add = " , sl.sl_year_month desc "; 
	$_order_add = " , sl.sl_day desc ";
}

$sql_order = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

// 그룹
$sql_group = " group by sl.sl_comics ";

// SQL문
$sql_field = "		sl.sl_comics, 
					sum(sl.sl_cash_point)as sl_cash_point_sum, 
					sum(sl.sl_cash_point)as sl_cash_point_cnt, 
					sum(sl.sl_cash_point*cup_won)as sl_cash_point_won_sum, 

					sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
					sum(sl.sl_open)as sl_open_sum 
"; 
$sql_count	= $sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$sql_select = "		select {$sql_field} ";
$sql_select_cnt = "	select {$sql_count} ";
$sql_table = "		FROM vsales sl
					LEFT JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type ";

$sql_query		= " {$sql_select}		{$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_select_cnt}	{$sql_table} {$sql_where} ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts		= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data_cnts = sql_fetch($sql_query_cnt);	// SQL문 총값-레코드값

$s_all_sum = number_format($rows_data_cnts['sl_cash_point_won_sum']);
$s_all_cnt = number_format($rows_data_cnts['sl_pay_sum']);
$s_all_open_cnt = number_format($rows_data_cnts['sl_open_sum']);
$s_all_cash_point = number_format($rows_data_cnts['sl_cash_point_sum']);


/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('코믹스번호','calc_sl_comics',0));
array_push($fetch_row, array('코믹스명','calc_cm_series',0));
array_push($fetch_row, array('보기타입','calc_cm_btn',0));
array_push($fetch_row, array('작가명<br/>출판사명','calc_sl_cp_name',0));
array_push($fetch_row, array('제공사명','calc_sl_prov_cp_name',0));
array_push($fetch_row, array('코믹스 등록일<br/>화 최신등록일','calc_cm_reg_date',0));
array_push($fetch_row, array('열람건수(건)','calc_sl_sum_cnt',4));
array_push($fetch_row, array('판매건수(건)','calc_sl_sum_cnt',4));


/* cash_point */
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'('.$nm_config['cf_cash_point_unit'].')','calc_sl_cash_point_sum',4));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'금액(원)','calc_sl_cash_point_won_sum',4));

// css 클래스 접두사
$css_suffix = "cm_";

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "코믹스별 매출";
$cms_head_title = $page_title;

if($_mode == 'excel') {	
	$result	= sql_query($sql_querys);		// SQL문 결과 레코드값
	
	$excel_path = 'excel/calc_comics_excel.php';		// 엑셀 경로와 파일명

	if ($_cms_folder_path_push == '') {
		include_once './'.$excel_path;			// 같은 디렉토리 안에 있음.			
	} else {
		include_once $_cms_folder_path_push.'/'.$excel_path;			// 같은 디렉토리 안에 있음.	
	}	
}else{ // end if
	$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값
}

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<script type="text/javascript">
<!--
	var cr_thead_mg_add = '16px'; /* fixed 테이블이 높이가 커서 그만큼 마진 */
//-->
</script>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<? if (!$is_partner) { ?>
	<div class="write">
		<input type="button" class="excel_down" value="검색 결과 엑셀 다운로드" onclick="location.href='<?=$_cms_self;?>?<?=$_nm_paras;?>&mode=excel'"/>
	</div>
	<? } ?>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<?if(mb_partner_cp($nm_member) == false){?>
					<div class="cs_s_type_btn">
					<?	$s_type_list = array('작품명','작가','출판사','제공사');
						tag_radios($s_type_list, "s_type", $_s_type, 'y', '전체', ''); ?>
					</div>
					<div class="cs_search_btn">
						<?	tag_selects($nm_config['cf_big'], "big", $_big, 'y', '전체', ''); ?>
						<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
						<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
					</div>
					
					<? foreach($cp_arr as $cp_val){ /* 작가, 출판사, 제공사 데이터 출력 */ ?>
						<div class="cs_search_btn">
							<?=$cp_val[1];?> 리스트 
							<?	tag_selects(${$cp_val[0].'_arr'}, $cp_val[0], ${'_'.$cp_val[0]}, 'y', '전체', ''); ?>
						</div>
					<? } // foreach ?>
				<? } // if end ?>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<h3 id="cr_thead_mg_add">
		<strong>검색 결과 기준</strong>
		<ul>
		<? if(mb_class_permission('a')) { ?>
			<li>관리자는 실시간 기준[<?=NM_TIME_YMDHIS;?>]으로 나옵니다.</li>
			<li>파트너사(작가,출판사,제공사,마커터 등등)은 현재시간 기준으로 전일[<?=NM_TIME_M1;?>]까지만 나옵니다.<br/>(↓↓아래는 파트너일 경우에만 나오는 안내문입니다.)</li>
		<? } ?>
		<? if(mb_class_permission('p,a')) { ?>
			<li>전체 정산에서는 현재시간 기준으로 전일[<?=NM_TIME_M1;?>]까지만 나옵니다.</li>	
		<? } ?>		
		</ul>
		<br/>
		검색 리스트 <strong>검색 결과
	</h3>
	<h4 class="table_list">
		<?if($s_all_sum!= ''){?>
			<table>
				<tr>
					<th class="text_right topleft">판매건수(건)</th>
					<th class="text_right"><?=$nm_config['cf_cash_point_unit_ko']?>(<?=$nm_config['cf_cash_point_unit']?>)</th>
					<th class="text_right topright"><?=$nm_config['cf_cash_point_unit_ko']?>금액(원)</th>
				</tr>
				<tr>
					<td class="text_right"><?=$s_all_cnt;?></td>
					<td class="text_right"><?=$s_all_cash_point;?></td>
					<td class="text_right"><?=$s_all_sum;?></td>
				</tr>
			</table>
		<?}?>
	</h4>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $css_suffix.$fetch_val[1];
					
					if($fetch_val[2] == 4){
						$th_class.= " text_right";
					}
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){
					$get_comics = get_comics($dvalue_val['sl_comics']); /* 변경 */

					$db_cm_big = $get_comics['cm_big'];
					$db_comics = $dvalue_val['sl_comics'];
					$db_series = $get_comics['cm_series'];

					$db_sum_won = number_format($dvalue_val['sl_won_sum']);
					$db_sum_cnt = number_format($dvalue_val['sl_pay_sum']);

					// 충전땅콩 / 소진율
					$db_sql_sales_recharge = "select sr_year_month, sr_day, sr_week, 
												sum(sr_cash_point)as sr_cash_point_sum, 
												sum(sr_point)as sr_point_sum 
												FROM sales_recharge 
												where 1 AND sr_year_month = '".$dvalue_val['sl_year_month']."' 
												AND sr_day = '".$dvalue_val['sl_day']."'
												group by sr_year_month, sr_day ";					
					$db_row_sales_recharge = sql_fetch($db_sql_sales_recharge);
					
					/* cash_point  */					
					$db_cash_point_sum = number_format($dvalue_val['sl_cash_point_sum']);
					$db_cash_point_cnt= number_format($dvalue_val['sl_cash_point_cnt']);
					$db_cash_point_won_sum = number_format($dvalue_val['sl_cash_point_won_sum']);

					/* point */
					$db_point_sum = number_format($dvalue_val['sl_point_sum']);
					$db_point_cnt= number_format($dvalue_val['sl_point_cnt']);
					$db_point_won_sum = number_format($dvalue_val['sl_point_won_sum']);
					
					/* 열람수 */
					$db_open_sum = number_format($dvalue_val['sl_open_sum']);

					/* 등록일 */
					$db_cm_reg_date_ymd = get_ymd($get_comics['cm_reg_date']);
					$db_cm_reg_date_his = get_his($get_comics['cm_reg_date']);

					/* 에피소드 등록일 */
					$db_cm_episode_date_ymd = get_ymd($get_comics['cm_episode_date']);
					$db_cm_episode_date_his = get_his($get_comics['cm_episode_date']);

					/* 요일별 class */
					$week_class = $week_en[$dvalue_val['sl_week']];

					/* 파라미터 */
					$db_comics_url_para = "&cm_big=".$db_cm_big."&cm_no=".$db_comics."&s_date=".$_s_date."&e_date=".$_e_date;

					/* 날짜별 link */
					$db_comics_date_url = $_cms_self."?sl_mode=date".$db_comics_url_para;

					/* 화별 link */
					$db_comics_episode_url = $_cms_self."?sl_mode=episode".$db_comics_url_para;

					$comics_url = get_comics_url($dvalue_val['sl_comics']);
				?>
				<tr class="<?=$week_class;?>">
					<td class="<?=$css_suffix.$fetch_row[0][1]?> text_center"><?=$db_comics;?></td>
					<td class="<?=$css_suffix.$fetch_row[1][1]?> text_center">
						<a href="<?=$comics_url?>"><?=$db_series;?></a>
					</td>
					<td class="<?=$css_suffix.$fetch_row[2][1]?> text_center">
						<a class="date" href="<?=$db_comics_date_url?>">날짜별</a><br/>
						<a class="episode" href="<?=$db_comics_episode_url?>">화별</a>
					</td>
					<td class="<?=$css_suffix.$fetch_row[3][1]?> text_center">
						<?=$get_comics['prof_name'];?>
						<? echo $get_comics['prof_name_sub']==''?"":"<br/>".$get_comics['prof_name_sub'];?>
						<? echo $get_comics['prof_name_thi']==''?"":"<br/>".$get_comics['prof_name_thi'];?>
						<br/><?=$get_comics['publ_name'];?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[4][1]?> text_center">
						<?=$get_comics['prov_name'];?>
						<? echo $get_comics['prov_name_sub']==''?"":"<br/>".$get_comics['prov_name_sub'];?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[5][1]?> text_center">
						<?=$db_cm_reg_date_ymd;?> <?=$db_cm_reg_date_his;?><br/>
						<?=$db_cm_episode_date_ymd;?> <?=$db_cm_episode_date_his;?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[6][1]?> text_right">
						<?=$db_open_sum;?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[7][1]?> text_right">
						<?=$db_sum_cnt;?>
					</td>

					<td class="<?=$css_suffix.$fetch_row[8][1]?> text_right">
						<?=$db_cash_point_sum;?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[9][1]?> text_right"><?=$db_cash_point_won_sum;?></td>					
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? rows_page(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>