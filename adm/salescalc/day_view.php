<?php
 include_once '_common.php'; // 공통
 
 $year_month_day = base_filter($_GET['year_month_day']);
 
 $field_salescalc = "vsl.sl_hour, SUM(vsl.sl_all_pay+vsl.sl_pay) AS vsl_sumpay, SUM(vsl.sl_cash_point) AS vsl_cash_cnt, SUM(vsl.sl_cash_point * cup.cup_won) AS vsl_cash";
 
 $where_salescalc = "AND sl_year_month = '".substr($year_month_day,0,7)."' ";
 $where_salescalc .= "AND sl_day = '".substr($year_month_day,8,2)."' ";
 
 //day.php 검색했을 때 검색 결과에 해당되는 comics를 받아옴
 if ($comics = base_filter($_GET['comics'])) {
 	$comics = unserialize(preg_replace("/(\\\)/", "", urldecode($comics))); // urlencode를 했을 때 '\"' 포함 됨 -> '\"' 를 '"' 로 변경
 	
 	$where_salescalc .= "AND sl_comics IN (".implode(',',$comics).") ";
 }
 
 $group_salescalc = "sl_hour";
 
 $sql_salescalc = "
 SELECT $field_salescalc FROM vsales vsl
 LEFT JOIN config_unit_pay cup ON vsl.sl_cash_type = cup.cup_type
 WHERE 1 $where_salescalc 
 GROUP BY $group_salescalc 
 ";
 
 $result = sql_query($sql_salescalc);
 
 $temp_arr = array();
 while ($row = sql_fetch_array($result)) {
 	array_push($temp_arr, $row);
 }
 
 $fetch_row = array();
 array_push($fetch_row, array('시간대', 'sl_hour'));
 array_push($fetch_row, array('판매건수(건)', 'vsl_sumpay'));
 array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].' ('.$nm_config['cf_cash_point_unit'].')', 'vsl_cash_cnt'));
 array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'금액(원)', 'vsl_cash'));
 
 $cms_head_title = "시간대별 결제 통계 ".$year_month_day;
 
 if($_mode == 'excel') {
 	$result = sql_query($sql_sales_recharge);
	include_once $_cms_folder_top_path.'/excel/day_view_excel.php';
 }

 $head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
 include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="sales_result">
	<h1 class="day_view">시간대별 결제 통계 : <?=$year_month_day ?></h1>
	<!--
	<div class="write">
		<input type="button" class="excel_down" value="엑셀 다운로드"	onclick="location.href='<?=$_cms_self?>?<?=$_nm_paras;?>&mode=excel'">
	</div>	
	-->
	<div id="cr_bg">
		<table class="list">
			<thead>
				<tr>
				<?
				foreach($fetch_row as $key => $val){
					$th_title = $val[0];
				?>
					<th><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
			<?php	foreach ($temp_arr as $key => $val) {	?>
				<tr>
					<td><?=$val['sl_hour'] ?>:00</td>
					<td><?=number_format($val['vsl_sumpay'])?></td>
					<td><?=number_format($val['vsl_cash_cnt'])?></td>
					<td><?=number_format($val['vsl_cash'])?></td>
				</tr>
			<? } ?>			
				<tr>
					<td colspan="4" class="btn">
						<input type="reset" value="닫기" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</section>