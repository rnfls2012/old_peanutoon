<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_ccp_no = tag_filter($_REQUEST['ccp_no']);

$sql = "SELECT * FROM comics_calc_percentage WHERE ccp_no = '$_ccp_no' ";
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
	break;
}

/* 작가, 출판사, 제공사 데이터 가져오기 */
$class_arr = array("pf" => "작가", "pv" => "제공사", "pb" => "출판사");

$professional_arr = $publisher_arr = $provider_arr = $cp_arr = array();
array_push($cp_arr , array('professional','작가'));
array_push($cp_arr , array('publisher','출판사'));
array_push($cp_arr , array('provider','제공사'));

foreach($cp_arr as $cp_val){
	$sql_cp = "select * from comics_".$cp_val[0]." order by cp_name asc";
	$result_cp = sql_query($sql_cp);	
	$row_cp_save = array();

	while($row_cp = sql_fetch_array($result_cp)) {
		$row_cp_save[$row_cp['cp_no']] = $row_cp['cp_name'];
		${$cp_val[0].'_arr'} = $row_cp_save;
	} // end while
} // end foreach

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
	$comics_row = get_comics($row['ccp_comics']);
}

/* 대분류로 제목 */
$page_title = "코믹스 RS";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- event_free_dc_head -->

<section id="sales_write">
	<form name="sales_ccp_write_form" id="sales_ccp_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return sales_ccp_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="ccp_no" name="ccp_no" value="<?=$_ccp_no;?>"/><!-- 번호 -->
		<input type="hidden" id="ccp_reg_date" name="ccp_reg_date" value="<?=$row['ccp_reg_date'];?>"/>
		<input type="hidden" id="cm_pv" name="cm_pv" value="<?=$comics_row['cm_provider'];?>"/><!-- 제공사 -->
		<input type="hidden" id="cm_pv_sub" name="cm_pv_sub" value="<?=$comics_row['cm_provider_sub'];?>"/><!-- 제공사 서브 -->
		<input type="hidden" id="cm_pf" name="cm_pf" value="<?=$comics_row['cm_professional'];?>"/><!-- 작가 -->
		<input type="hidden" id="cm_pf_sub" name="cm_pf_sub" value="<?=$comics_row['cm_professional_sub'];?>"/><!-- 작가 서브 -->
		<input type="hidden" id="cm_pb" name="cm_pb" value="<?=$comics_row['cm_publisher'];?>"/><!-- 출판사 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th>번호</th>
					<td><?=$_ccp_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="comics_list"><?=$required_arr[0];?>코믹스 정보</label></th>
					<td id="comics_list">
						<ul>
						<? if($comics_row['cm_no'] != "") { ?>
							<li id="li_add_comics<?=$comics_row['cm_no'];?>" class="result_hover" data_comics="<?=$comics_row['cm_no'];?>">
								<input type="hidden" name="ccp_comics" value="<?=$comics_row['cm_no'];?>">
								<p>
								<?=$comics_row['cm_series'];?>(가격:<?=cash_point_view($comics_row['cm_pay']);?>, 총 화:<?=$comics_row['cm_episode_total'];?>화,		<?=$d_adult[$comics_row['cm_adult']];?>)
									<a href="#comics_list_del" onclick="comics_list_del(<?=$comics_row['cm_no'];?>);">
										<img src="<?=NM_IMG?>cms/free_dc_del.png" alt="삭제">
									</a>
								</p>
							</li>
						<? } // end if ?>
						</ul>
					</td>
				</tr>
					<th><label for="comics_search"><?=$required_arr[0];?>코믹스 검색</label></th>
					<td>
						<!-- 이벤트 선택 리스트 -->
						<input type="text" class="comics_search" placeholder="검색하세요" id="comics_search" value="" autocomplete="off" onkeydown='comics_search_chk();' />
						<a id="comics_search_btn" class="comics_search_btn" href='#comics_search' onclick='a_click_false()'>검색</a>
						<div id="comics_search_result"></div>
						<!-- 이벤트 검색 리스트 -->
					</td>
				<tr>
				</tr>
				<tr>
					<th><label for="ccp_class"><?=$required_arr[0];?>분류 1</label></th>
					<td>
						<? tag_selects($class_arr, "ccp_class", $row['ccp_class'], 'y', '선택', ''); ?>	
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>분류 1 선택</label></th>
					<td class="display_control">
					<? foreach($cp_arr as $cp_val) { 
						$ccp_no_class = "";
						switch($cp_val[0]) { 
							case "professional" :
								$ccp_no_class = "pf";
								break;
							
							case "publisher" : 
								$ccp_no_class = "pb";
								break;
								
							case "provider" : 
								$ccp_no_class = "pv";
								break; 
						} // end switch
						
						$tag_display = "display: none;";
						if($_mode != "reg" && $ccp_no_class == $row['ccp_class']) {
							$tag_display = "";
						} // end if
						
						?>
						<div class="<?=$ccp_no_class;?>" style="<?=$tag_display;?>">
							<? tag_selects(${$cp_val[0].'_arr'}, $ccp_no_class."_arr", $row['ccp_class_no'], 'y', '선택', ''); ?>
						</div>
					<? } // foreach ?>
					</td>
				</tr>
				<tr>
					<th><label for="ccp_class_per"><?=$required_arr[0];?>분류 1 배율</label></th>
					<td>
						<input type="text" class="ccp_class_per" id="ccp_class_per" name="ccp_class_per" placeholder="배율을 입력하세요"  value="<?=($row['ccp_class_per']!="")?$row['ccp_class_per']:0;?>" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<th><label for="ccp_class">분류 2</label></th>
					<td>
						<? tag_selects($class_arr, "ccp_class_sub", $row['ccp_class_sub'], 'y', '선택', ''); ?>	
					</td>
				</tr>
				<tr>
					<th><label>분류 2 선택</label></th>
					<td class="display_control_sub">
					<? foreach($cp_arr as $cp_val) { 
						$ccp_no_sub_class = "";
						switch($cp_val[0]) { 
							case "professional" :
								$ccp_no_sub_class = "pf";
								break;
							
							case "publisher" : 
								$ccp_no_sub_class = "pb";
								break;
								
							case "provider" : 
								$ccp_no_sub_class = "pv";
								break; 
						} // end switch

						$tag_display_sub = "display: none;";
						if($_mode != "reg" && $ccp_no_sub_class == $row['ccp_class_sub']) {
							$tag_display_sub = "";
						} // end if
						
						?>
						
						<div class="<?=$ccp_no_sub_class;?>_sub" style="<?=$tag_display_sub;?>">
							<? tag_selects(${$cp_val[0].'_arr'}, $ccp_no_sub_class.'_sub_arr', $row['ccp_class_sub_no'], 'y', '선택', ''); ?>
						</div>
					<? } // foreach ?>
					</td>
				</tr>
				<tr>
					<th><label for="ccp_class_sub_per">분류 2 배율</label></th>
					<td>
						<input type="text" class="ccp_class_sub_per" id="ccp_class_sub_per" name="ccp_class_sub_per" placeholder="배율을 입력하세요"  value="<?=($row['ccp_class_sub_per']!="")?$row['ccp_class_sub_per']:0;?>" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- event_free_dc_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>