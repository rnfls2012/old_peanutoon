<?

include_once NM_ADM_PATH.'/_common.php';		// ADM 전용 공통
$today = date("Ymd");

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet('Search Keyword');

// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 해당 시트 당 컬럼 크기 변경
$worksheet->set_column(0, 1, 15);

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));

$center  = $workbook -> addformat(array(
								'align' => 'center',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));

//문서 출력일자 셀 출력
$worksheet->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt = 2; //합계 데이터 출력 행 cnt.

/**************  worksheet  *****************/

/**************  필터링 값 출력 ***************/

//검색 날짜 별 결과 항목 열 지정.
$search_keyword = array();
array_push($search_keyword, "분류");
array_push($search_keyword, "검색어");
array_push($search_keyword, "검색 작가");
array_push($search_keyword, "검색 출판사");
array_push($search_keyword, "검색 제공사");

//검색 결과 배열 저장. 순서 바꾸기 X
$search_result = array();
array_push($search_result, $nm_config['cf_big'][$_big]);
array_push($search_result, $s_text);
array_push($search_result, $professional_arr[${'_'.$cp_arr[0][0]}]);
array_push($search_result, $publisher_arr[${'_'.$cp_arr[1][0]}]);
array_push($search_result, $provider_arr[${'_'.$cp_arr[2][0]}]);

//필터링 결과 리스트(통합리스트)
$search_list = array();
array_push($search_list, array("코믹스번호", "sl_comics"));
array_push($search_list, array("코믹스명", "cm_series"));
array_push($search_list, array("보기타입", "cm_big"));
array_push($search_list, array("작가명", "cm_professional"));
array_push($search_list, array("출판사명", "cm_publisher"));
array_push($search_list, array("제공사명", "cm_provider"));
array_push($search_list, array("코믹스 등록일", "cm_reg_date"));
array_push($search_list, array("에피소드 등록일", "cm_episode_date"));
array_push($search_list, array("열람건수(건)", "sl_open_sum"));
array_push($search_list, array("판매건수(건)", "sl_pay_sum"));
array_push($search_list, array("총 화수", "episode_total"));
array_push($search_list, array($nm_config['cf_cash_point_unit_ko'].'('.$nm_config['cf_cash_point_unit'].')', "sl_cash_point_sum"));
array_push($search_list, array($nm_config['cf_cash_point_unit_ko']."금액(원)", "sl_cash_point_won_sum"));

//시작 날짜, 끝 날짜 하나라도 있을 경우
if ($_s_month || $_e_month) {
 array_push($search_keyword, "검색 날짜");
 array_push($search_result, $_s_month."~".$_e_month);
}

if ($_s_date || $_e_date) {
 array_push($search_keyword, "검색 날짜");
 array_push($search_result, $_s_date."~".$_e_date);
}

//각 필터링 항목 별 count
$sk_cnt = count($search_keyword);
$sr_cnt = count($search_result);
$sl_cnt = count($search_list);

// DB 컬럼용 배열
$data_key = array();
foreach($search_list as $key => $val) {
	array_push($data_key, $val[1]);
} // end foreach

//필터링 항목 출력
for ($i=1; $i <= $sk_cnt; $i++) {
	$worksheet->write($i+$row_cnt, 0 , iconv_cp949($search_keyword[$i-1]), $heading);
}

//필터링 결과 출력
for($i=1; $i<=$sr_cnt; $i++){
	$worksheet->write($i+$row_cnt, 1, iconv_cp949($search_result[$i-1]), $right);
}

//필터링 리스트 출력
for($i=1; $i<=$sl_cnt; $i++){
	$worksheet->write($sk_cnt+4, $i-1, iconv_cp949($search_list[$i-1][0]), $heading);
}

$sk_cnt+=$row_cnt+3;

for ($i=$sk_cnt; $row=sql_fetch_array($result); $i++) {
	foreach ($data_key as $cell_key => $cell_val) {
		$comics_arr = get_comics($row['sl_comics']);

		/* 총 화수 cm_episode_total은 외전은 제외시키기에 직접 쿼리로 구함 180314 - 배경호 과장님 요청 */
		$episode_total_sql = "SELECT COUNT(*) episode_total FROM comics_episode_".$comics_arr['cm_big']." WHERE ce_comics='".$comics_arr['cm_no']."' AND ce_service_state='y'";
		$episode_total = sql_count($episode_total_sql, 'episode_total');
		
		switch($cell_val) {
			case 'sl_comics' :
				$sl_comics = $row['sl_comics'];
				$worksheet->write($i, $cell_key , iconv_cp949($sl_comics), $center);
				break;
				
			case 'cm_series' :
				$cm_series = $comics_arr['cm_series'];
				$worksheet->write($i, $cell_key , iconv_cp949($cm_series), $center);
				break;
				
			case 'cm_big' :
				$cm_big = $comics_arr['cm_big'];
				$worksheet->write($i, $cell_key , iconv_cp949($nm_config['cf_big'][$cm_big]), $center);
			 	break;
			 	
			case 'cm_professional' :
				$cm_professional_fst = $comics_arr['prof_name'];
				
				if ($cm_professional_snd = $comics_arr['prof_name_sub'] && $cm_professional_trd = $comics_arr['prof_name_thi']) {
					$cm_professional = $cm_professional_fst." / ".$cm_professional_snd." / ".$cm_professional_trd;
				} else if ($cm_professional_snd = $comics_arr['prof_name_sub']) {
					$cm_professional = $cm_professional_fst." / ".$cm_professional_snd;
				} else {
					$cm_professional = $cm_professional_fst;
				}
				
				$worksheet->write($i, $cell_key , iconv_cp949($cm_professional), $center);
			 	break;
			 	
			case 'cm_publisher' :
				$cm_publisher = $comics_arr['publ_name'];
				$worksheet->write($i, $cell_key , iconv_cp949($cm_publisher), $center);
			 	break;
			 	
			case 'cm_provider' :
				$cm_provider_fst = $comics_arr['prov_name'];
				
				if ($cm_provider_snd = $comics_arr['prov_name_sub']) {
					$cm_provider = $cm_provider_fst." / ".$cm_provider_snd;
				} else {
					$cm_provider = $cm_provider_fst;
				}
				
				$worksheet->write($i, $cell_key , iconv_cp949($cm_provider), $center);
			 	break;
			 	
			case 'cm_reg_date' :
				$cm_reg_date = $comics_arr['cm_reg_date'];
				$worksheet->write($i, $cell_key , iconv_cp949($cm_reg_date), $center);
			 	break;
			 	
			case 'cm_episode_date' :
				$cm_episode_date = $comics_arr['cm_episode_date'];
				$worksheet->write($i, $cell_key , iconv_cp949($cm_episode_date), $center);
			 	break;

			case 'episode_total' :
				$worksheet->write($i, $cell_key , iconv_cp949($episode_total), $center);
				break;
			 	
			case 'sl_open_sum' :
				$sl_open_sum = $row['sl_open_sum'];
				$worksheet->write($i, $cell_key , iconv_cp949($sl_open_sum), $f_price);
			 	break;
			 	
			case 'sl_pay_sum' :
				$sl_pay_sum = $row['sl_pay_sum'];
				$worksheet->write($i, $cell_key , iconv_cp949($sl_pay_sum), $f_price);
			 	break;
			 	
			default :
				$worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]), $f_price);
				break;
		}
	} // end foreach
	$fetch_row_cnt++;
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename=".$nm_config['cf_title']."_calc_comics_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"".$nm_config['cf_title']."_calc_comics_".$today.".xls");
header("Content-Disposition: inline; filename=\"".$nm_config['cf_title']."_calc_comics_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die();
?>