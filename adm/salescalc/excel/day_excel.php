<?php
$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet2 = $workbook->addworksheet('Result');
$worksheet1 = $workbook->addworksheet('Search Keyword');


// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 해당 시트 당 컬럼 크기 변경
$worksheet1->set_column(0, 4, 20);

$worksheet2->set_column(0, 5, 15);

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));
$center  = $workbook -> addformat(array(
								'align' => 'center',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));

//문서 출력일자 셀 출력
$worksheet1->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet1->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet1->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt1 = 2; //합계 데이터 출력 행 cnt.

/**************  worksheet1  *****************/

/**************  필터링 값 출력 ***************/

//검색 날짜 별 결과 항목 열 지정.
$search_keyword = array();
array_push($search_keyword, "검색 종류");
array_push($search_keyword, "검색 작가");
array_push($search_keyword, "검색 출판사");
array_push($search_keyword, "검색 제공사");

//검색 결과 배열 저장. 순서 바꾸기 X
$search_result = array();
array_push($search_result, $nm_config['cf_big'][$_big]);
array_push($search_result, $professional_arr[${'_'.$cp_arr[0][0]}]);
array_push($search_result, $publisher_arr[${'_'.$cp_arr[1][0]}]);
array_push($search_result, $provider_arr[${'_'.$cp_arr[2][0]}]);

if ($_s_month || $_e_month) {
 array_push($search_keyword, "검색 날짜");
 array_push($search_result, $_s_month."~".$_e_month);
}

//필터링 결과 리스트(통합리스트)
$search_list = array();
array_push($search_list, "코믹스리스트");
array_push($search_list, "출판사리스트");
array_push($search_list, "작가리스트");
array_push($search_list, "제공사리스트");

//각 필터링 항목 별 count
$sk_cnt = count($search_keyword);
$sr_cnt = count($search_result);
$sl_cnt = count($search_list);

//필터링 항목 출력
for ($i=1; $i <= $sk_cnt; $i++) {
	$worksheet1->write($i+$row_cnt1, 0 , iconv_cp949($search_keyword[$i-1]), $heading);
}

//필터링 결과 출력
for($i=1; $i<=$sr_cnt; $i++){
	$worksheet1->write($i+$row_cnt1, 1, iconv_cp949($search_result[$i-1]), $right);
}

//필터링 리스트 출력
for($i=1; $i<=$sl_cnt; $i++){
	$worksheet1->write($sk_cnt+4, $i-1, iconv_cp949($search_list[$i-1]), $heading);
}

//필터링 리스트 결과 출력
foreach($cm_arr as $cm_key => $cm_val){
	
	//다중 제공자 처리
	if ($cm_val['cm_provider_sub'] != 0 && $cm_val['cm_provider_sub'] > 0) {
		$temp_arr = array();
		$temp1_str = "";

		array_push($temp_arr, $cm_val['cm_provider']);
		array_push($temp_arr, $cm_val['cm_provider_sub']);
		
		$temp_str = implode(",", $temp_arr);
				
		$sql_cm_prov = "SELECT * FROM comics_provider WHERE cp_no IN (".$temp_str.")";	
		$result_cm_prov = sql_query($sql_cm_prov);
		
		while($row_cm_prov = sql_fetch_array($result_cm_prov)){
			$temp1_str .= $row_cm_prov['cp_name']."/";
		}
		
		$row_cm_prov['cp_name'] = $temp1_str;
		
	} else {
		$sql_cm_prov = "SELECT * FROM comics_provider WHERE cp_no = ".$cm_val['cm_provider'];	
		$result_cm_prov = sql_query($sql_cm_prov);
		$row_cm_prov = sql_fetch_array($result_cm_prov);
	}
	
	$sql_cm_publ = "SELECT * FROM comics_publisher WHERE cp_no = ".$cm_val['cm_publisher'];

	$result_cm_publ = sql_query($sql_cm_publ);

	$row_cm_publ = sql_fetch_array($result_cm_publ);

	$worksheet1->write($cm_key+$sk_cnt+5, 0, iconv_cp949($cm_val['cm_series']), $right); //배열의 시작은 0, 리스트 결과값 출력위해 +1(행)	
	$worksheet1->write($cm_key+$sk_cnt+5, 1, iconv_cp949($row_cm_publ['cp_name']), $right); //배열의 시작은 0, 리스트 결과값 출력위해 +1	
	$worksheet1->write($cm_key+$sk_cnt+5, 2, iconv_cp949($cm_val['cm_professional_info']), $right); //배열의 시작은 0, 리스트 결과값 출력위해 +1	
	$worksheet1->write($cm_key+$sk_cnt+5, 3, iconv_cp949($row_cm_prov['cp_name']), $right); //배열의 시작은 0, 리스트 결과값 출력위해 +1	
}

/* end worksheet1 */

/**************  worksheet2  *****************/

//문서 출력일자 출력
$worksheet2->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet2->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet2->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt2 = 3; //합계 데이터 출력 행 cnt.

/**************  검색 결과값 출력  ****************/

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
} // end foreach

$fetch_row_cnt = count($fetch_row);

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet2->write(3, $col++, $cell, $heading);
} // end foreach

/* 데이터 전달 부분 */
for ($i=4; $row=mysql_fetch_array($result); $i++){
	foreach($data_key as $cell_key => $cell_val) {
		switch($cell_val) {
			case "day_calc_sl_year_month" :
				$day_calc_sl_year_month = $row['sl_year_month']."-".$row['sl_day'];
				$worksheet2->write($i, $cell_key , iconv_cp949($day_calc_sl_year_month), $center);
				break;

			case "day_calc_sl_week" :
				$day_calc_sl_week = get_yoil($row['sl_week'],1,1);
				$worksheet2->write($i, $cell_key , iconv_cp949($day_calc_sl_week), $center);
				break;

			case "day_calc_sl_cash_point_cnt" :
				$cash_point_cnt_total += $row['sl_pay_sum'];
				$day_calc_sl_cash_point_cnt = $row['sl_pay_sum'];
				$worksheet2->write($i, $cell_key , iconv_cp949($day_calc_sl_cash_point_cnt), $f_price);
				break;

			case "day_calc_sl_cash_point_sum" :
				$cash_point_sum_total += $row['sl_cash_point_sum'];
				$day_calc_sl_cash_point_sum = $row['sl_cash_point_sum'];
				$worksheet2->write($i, $cell_key , iconv_cp949($day_calc_sl_cash_point_sum), $f_price);
				break;
				
			case "day_calc_sl_cash_point_won_sum" :				
				$cash_point_won_sum_total += $row['sl_cash_point_won_sum'];
				$day_calc_sl_cash_point_won_sum = $row['sl_cash_point_won_sum'];
				$worksheet2->write($i, $cell_key , iconv_cp949($day_calc_sl_cash_point_won_sum), $f_price);
				break;
				
		}
	} // end foreach
	$fetch_row_cnt++;
} // end for 쿼리 조회결과가 없을때까지 조회

$fetch_row_cnt++;

$worksheet2->write($fetch_row_cnt, 1, iconv_cp949("합 계"), $heading);
$worksheet2->write($fetch_row_cnt, 2, iconv_cp949($cash_point_cnt_total), $f_price);
$worksheet2->write($fetch_row_cnt, 3, iconv_cp949($cash_point_sum_total), $f_price);
$worksheet2->write($fetch_row_cnt, 4, iconv_cp949($cash_point_won_sum_total), $f_price);

$workbook->close();

header("Content-Disposition: attachment;filename="."day_salescalc_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"day_salescalc_".$today.".xls");
header("Content-Disposition: inline; filename=\"day_salescalc_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;
?>