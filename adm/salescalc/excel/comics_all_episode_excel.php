<?php
$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/

// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-salescalc.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet2 = $workbook->addworksheet('Result');

// 해당 시트 당 컬럼 크기 변경
$worksheet2->set_column(1, 1, 25);
$worksheet2->set_column(2, 5, 20);

// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));
								
$center  = $workbook -> addformat(array(
								'align' => 'center',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));
								
/**************  worksheet2  *****************/

//문서 출력일자 출력
$worksheet2->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet2->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet2->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt2 = 3; //합계 데이터 출력 행 cnt.

/***************  본문 출력   **************/

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
} // end foreach

// 컬럼 이름 지정
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet2->write($row_cnt2, $col++, $cell, $heading);
} // end foreach

$row_cnt2++;

// 데이터 출력
for ($i=$row_cnt2; $row=sql_fetch_array($result); $i++) {
	foreach($data_key as $cell_key => $cell_val) {
		
		$ce_up_kind = '화';
			if($row['cm_up_kind'] == '1'){ $ce_up_kind = '권'; }				
			if($row['ce_chapter'] > 0){
				$se_title = $row['cm_series']." ".$row['se_chapter'].$ce_up_kind;
				if($row['ce_title'] != ''){ $se_title = $row['cm_series']." ".$row['se_chapter'].$ce_up_kind." - ".$row['ce_title']; }	
			}else{
				$se_title = $row['ce_notice'];
				if($row['ce_notice'] == '' && $row['ce_outer'] == 'y'){
					$se_title = "외전".$row['ce_outer_chapter'].$ce_up_kind;
				}
			}
					
		switch($cell_val) {			
			case "se_episode" :
				$se_episode = $row['se_episode'];
				$worksheet2->write($i, $cell_key , iconv_cp949($se_episode), $center);
				break;
				
			case "se_title" :
				$cm_up_kind = '화';
				
				if ($row['cm_up_kind'] == 1) {
					$cm_up_kind = '권';
				}
				
				if ($row['ce_chapter'] > 0) {
						$se_title = $row['cm_series']." ".$row['se_chapter'].$cm_up_kind;
						
						if ($row['ce_title'] != '') { 
							$se_title = $row['cm_series']." ".$row['se_chapter'].$cm_up_kind." - ".$row['ce_title']; 
						}	else if ($row['ce_notice'] != '') {
							$se_title = $row['cm_series']." ".$row['se_chapter'].$cm_up_kind." - ".$row['ce_notice']; 
						}
				} else {
					$se_title = $row['ce_notice'];
					
					if ($row['ce_notice'] == '' && $row['ce_outer'] == 'y') {
						$se_title = "외전".$row['ce_outer_chapter'].$cm_up_kind;
					}
				}
				
				$worksheet2->write($i, $cell_key , iconv_cp949($se_title), $center);
				break;
				
			case "se_open_sum" :	
				$se_open_sum = $row['se_open_sum'];
				$all_se_open_sum += $se_open_sum;
				$worksheet2->write($i, $cell_key , iconv_cp949($se_open_sum), $f_price);
				break;

			case "se_buy_sum" :				
				$se_buy_sum = $row['se_pay_sum'];
				$all_se_buy_sum += $se_buy_sum;
				$worksheet2->write($i, $cell_key , iconv_cp949($se_buy_sum), $f_price);
				break;	
				
			case "se_cash_sum" :				
				$se_cash_num = $row['se_cash_point_sum'];
				$all_se_cash_sum += $se_cash_num;
				$worksheet2->write($i, $cell_key , iconv_cp949($se_cash_num), $f_price);
				break;	
				
			case "se_cash_point_won" :				
				$se_cash_point_won = $row['se_cash_point_won_sum'];
				$all_se_cash_point_won += $se_cash_point_won;
				$worksheet2->write($i, $cell_key , iconv_cp949($se_cash_point_won), $f_price);
				break;
		}
	} // end foreach
	$row_cnt2++;
} // end for 쿼리 조회결과가 없을때까지 조회

$row_cnt2++;

$worksheet2->write($row_cnt2, 1, iconv_cp949("합계"), $heading);
$worksheet2->write($row_cnt2, 2, iconv_cp949($all_se_open_sum), $f_price);
$worksheet2->write($row_cnt2, 3, iconv_cp949($all_se_buy_sum), $f_price);
$worksheet2->write($row_cnt2, 4, iconv_cp949($all_se_cash_sum), $f_price);
$worksheet2->write($row_cnt2, 5, iconv_cp949($all_se_cash_point_won), $f_price);
	
$workbook->close();

header("Content-Disposition: attachment;filename="."comics_all_episode_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"comics_all_episode_".$today.".xls");
header("Content-Disposition: inline; filename=\"comics_all_episode_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;
