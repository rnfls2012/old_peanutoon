<?php
$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/

// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-salescalc.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet2 = $workbook->addworksheet('Result');

// 해당 시트 당 컬럼 크기 변경

$worksheet2->set_column(1, 5, 15);

// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));

/**************  worksheet1  *****************/

/************** end worksheet1 ***************/

/**************  worksheet2  *****************/

//문서 출력일자 출력
$worksheet2->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet2->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet2->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt2 = 3; //합계 데이터 출력 행 cnt.

/**************  검색 결과값 출력  ****************/

/**************  연도 별 합계 출력   **************/

//합계 결과 항목 열 지정.
$years_sum_arr = array();
array_push($years_sum_arr, array('연도','sl_year'));
array_push($years_sum_arr, array('열람건수(건)','sl_open_sum'));
array_push($years_sum_arr, array('판매건수(건)','sl_cnt_sum'));
array_push($years_sum_arr, array($nm_config['cf_cash_point_unit_ko'].' (P)','sl_cash_point_sum'));
array_push($years_sum_arr, array($nm_config['cf_cash_point_unit_ko'].' (원)','sl_cash_point_won_sum'));

$ysa_cnt = count($years_sum_arr);

// EXCEL용 배열
$data_head_ysm = $data_key_ysm = array(); // 분류값(컬럼) 및 데이터 출력 저장 배열
foreach($years_sum_arr as $key => $val) {
	array_push($data_head_ysm, $val[0]);
	array_push($data_key_ysm, $val[1]);
}

// 컬럼 이름 지정 부분
$col = 0;
$data_head_ysm = array_map('iconv_cp949', $data_head_ysm);
foreach($data_head_ysm as $cell) {
	$worksheet2->write($row_cnt2, $col++, $cell, $heading);
}

$row_cnt2++;

// 연도 별 합계 데이터 출력
for ($i=$row_cnt2; $row=mysql_fetch_array($result_y); $i++){
	foreach($data_key_ysm as $cell_key => $cell_val) {		
		switch($cell_val) {
			case "sl_year" :
				$d_sl_year = $row['sl_year'].' 년';
				$worksheet2->write($i, $cell_key , iconv_cp949($d_sl_year), $heading);
				break;
				
			case "sl_open_sum" :
				$d_sl_open_sum = $row['sl_open_sum'];				
				$all_sl_open_sum += $d_sl_open_sum;
				$worksheet2->write($i, $cell_key , iconv_cp949($d_sl_open_sum), $f_price);
				break;
				
			case "sl_cnt_sum" :
				$d_sl_cnt_sum = $row['sl_cnt_sum'];				
				$all_sl_cnt_sum += $d_sl_cnt_sum;
				$worksheet2->write($i, $cell_key , iconv_cp949($d_sl_cnt_sum), $f_price);
				break;

			case "sl_cash_point_sum" :
				$d_sl_cash_point_sum = $row['sl_cash_point_sum'];
				$all_sl_cash_point_sum += $d_sl_cash_point_sum;
				$worksheet2->write($i, $cell_key , iconv_cp949($d_sl_cash_point_sum), $f_price);
				break;

			case "sl_cash_point_won_sum" :
				$d_sl_cash_point_won_sum = $row['sl_cash_point_won_sum'];
				$all_sl_cash_point_won_sum += $d_sl_cash_point_won_sum;
				$worksheet2->write($i, $cell_key , iconv_cp949($d_sl_cash_point_won_sum), $f_price);
				break;

			default :
				$worksheet2->write($i, $cell_key , iconv_cp949($row[$cell_val]), $f_price);
				break;
		}
	}
	$row_cnt2++; // 모든 열 출력 후 행 cnt+1
}

// 총 합계
$worksheet2->write($row_cnt2, 0, iconv_cp949('합계'), $heading);
$worksheet2->write($row_cnt2, 1, iconv_cp949($all_sl_open_sum), $f_price);
$worksheet2->write($row_cnt2, 2, iconv_cp949($all_sl_cnt_sum), $f_price);
$worksheet2->write($row_cnt2, 3, iconv_cp949($all_sl_cash_point_sum), $f_price);
$worksheet2->write($row_cnt2, 4, iconv_cp949($all_sl_cash_point_won_sum), $f_price);

$row_cnt2+=2; // 합계 출력 한 후 공백, 본문 출력 위함.

/***************  본문 출력   **************/

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
} // end foreach

// 컬럼 이름 지정
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet2->write($row_cnt2, $col++, $cell, $heading);
} // end foreach

$row_cnt2++;

// 데이터 출력
for ($i=$row_cnt2; $row=mysql_fetch_array($result); $i++){
	foreach($data_key as $cell_key => $cell_val) {
		switch($cell_val) {
			case "sl_year_month" :
				switch($row['sl_year']) {
					case "2015" :
						$format = $workbook->addformat(array('fg_color' => 23, 'align' => 'center', 'border' => 1));	// color : 0x16
						$worksheet2->write($i, $cell_key , iconv_cp949($row['sl_year_month']), $format);
						break;

					case "2016" :				
						$format = $workbook->addformat(array('fg_color' => 22, 'align' => 'center', 'border' => 1));	// color : 0x1A
						$worksheet2->write($i, $cell_key , iconv_cp949($row['sl_year_month']), $format);
						break;

					case "2017" :				
						$format = $workbook->addformat(array('fg_color' => 26, 'align' => 'center', 'border' => 1));	// color : 0x1B
						$worksheet2->write($i, $cell_key , iconv_cp949($row['sl_year_month']), $format);
						break;

					case "2018" :				
						$format = $workbook->addformat(array('fg_color' => 44, 'align' => 'center', 'border' => 1));	// color : 0x2C
						$worksheet2->write($i, $cell_key , iconv_cp949($row['sl_year_month']), $format);
						break;
				}
			break;
			
			case "sl_sum_open" :
				$d_sl_open_sum = $row['sl_open_sum'];
				$all_sl_sum_open += $d_sl_open_sum;
				$worksheet2->write($i, $cell_key , iconv_cp949($d_sl_open_sum), $f_price);
				break;
				
			case "sl_sum_cnt1" :
				$d_sl_cnt_sum = $row['sl_cnt_sum'];
				$all_sl_sum_cnt1 += $d_sl_cnt_sum;
				$worksheet2->write($i, $cell_key , iconv_cp949($d_sl_cnt_sum), $f_price);
				break;
				
			case "sl_cash_point_sum1" :	
				$day_calc_sl_cash_point_cnt =$row['sl_cash_point_sum'];
				$all_sl_cash_point_sum1 += $day_calc_sl_cash_point_cnt;
				$worksheet2->write($i, $cell_key , iconv_cp949($day_calc_sl_cash_point_cnt), $f_price);
				break;

			case "sl_cash_point_won_sum1" :				
				$day_calc_sl_cash_point_sum = $row['sl_cash_point_won_sum'];
				$all_sl_cash_point_won_sum1 += $day_calc_sl_cash_point_sum;
				$worksheet2->write($i, $cell_key , iconv_cp949($day_calc_sl_cash_point_sum), $f_price);
				break;
		}
	} // end foreach
	$row_cnt2++;
} // end for 쿼리 조회결과가 없을때까지 조회
/* end worksheet2 */

$row_cnt2++;

$worksheet2->write($row_cnt2, 0, iconv_cp949("합계"), $heading); //배열의 시작은 0, 리스트 결과값 출력위해 +1(행)	
$worksheet2->write($row_cnt2, 1, iconv_cp949($all_sl_sum_open), $f_price); //배열의 시작은 0, 리스트 결과값 출력위해 +1	
$worksheet2->write($row_cnt2, 2, iconv_cp949($all_sl_sum_cnt1), $f_price); //배열의 시작은 0, 리스트 결과값 출력위해 +1	
$worksheet2->write($row_cnt2, 3, iconv_cp949($all_sl_cash_point_sum1), $f_price); //배열의 시작은 0, 리스트 결과값 출력위해 +1	
$worksheet2->write($row_cnt2, 4, iconv_cp949($all_sl_cash_point_won_sum1), $f_price); //배열의 시작은 0, 리스트 결과값 출력위해 +1	
	
$workbook->close();

header("Content-Disposition: attachment;filename="."comics_all_date_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"comics_all_date_".$today.".xls");
header("Content-Disposition: inline; filename=\"comics_all_date_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;
