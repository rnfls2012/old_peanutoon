<?
include_once '_common.php'; // 공통

/* 작가, 출판사, 제공사 데이터 가져오기 */
// $cp_arr = array("professional", "publisher", "provider");
$professional_arr = $publisher_arr = $provider_arr = $cp_arr = array();
array_push($cp_arr , array('professional','작가'));
array_push($cp_arr , array('publisher','출판사'));
array_push($cp_arr , array('provider','제공사'));

foreach($cp_arr as $cp_val){
	$sql_cp = "select * from comics_".$cp_val[0]." order by cp_name asc";
	$result_cp = sql_query($sql_cp);	
	$row_cp_save = array();

	while($row_cp = sql_fetch_array($result_cp)) {
		$row_cp_save[$row_cp['cp_no']] = $row_cp['cp_name'];
		${$cp_val[0].'_arr'} = $row_cp_save;
	}
}

// 샐럭트 박스 선택이 있다면.... (작가, 출판사, 제공사)
$cm_no_arr = $cp_no_arr = $cm_no_txt_arr = array(); // 샐렉트박스&문자열, 샐렉트박스, 문자열검색 
$cp_no_bool = false; // 샐렉트박스 검색결과 있는지 확인
$cm_no_txt_bool = false; // 문자열검색 검색결과 있는지 확인

$sql_where_cp_bool = false;
$sql_where_cnts = 0;
$sql_where_cps = "";
foreach($cp_arr as $cp_val){
	$sql_where_cp = "";
	if(${'_'.$cp_val[0]} != ''){
		$sql_where_cp_bool = true;
		$sql_where_cnts++;
		switch($cp_val[0]){
			case 'professional' :	$sql_where_cp = "	(  cm_professional = '".${'_'.$cp_val[0]}."' 
														OR cm_professional_sub = '".${'_'.$cp_val[0]}."'  
														OR cm_professional_thi = '".${'_'.$cp_val[0]}."' ) ";
			break;
			case 'publisher' :		$sql_where_cp = "	(  cm_publisher = '".${'_'.$cp_val[0]}."' ) ";
			break;
			case 'provider' :		$sql_where_cp = "	(  cm_provider = '".${'_'.$cp_val[0]}."' 
														OR cm_provider_sub = '".${'_'.$cp_val[0]}."' ) ";
			break;
		}

		if($sql_where_cnts == 1){
			$sql_where_cps.= $sql_where_cp;
		}else{
			$sql_where_cps.= " AND ".$sql_where_cp;
		}
	}
}
$cp_no_arr = get_cm_no_arr($sql_where_cps);

if(count($cp_no_arr) > 0 && $sql_where_cp_bool == true){ 
	$cp_no_bool = true;
} // 샐럭트박스 검색결과가 있다면...


// 2.  검색단어+ 검색타입
if($_s_text){
	switch($_s_type){
		case '0': $cm_no_txt_arr = array_merge(get_comics_series($_s_text));
		break;
		case '1': $cm_no_txt_arr = array_merge(get_cm_no_arr(get_cp_comics_sql($_s_text, 'professional')));
		break;
		case '2': $cm_no_txt_arr = array_merge(get_cm_no_arr(get_cp_comics_sql($_s_text, 'publisher')));
		break;
		case '3': $cm_no_txt_arr = array_merge(get_cm_no_arr(get_cp_comics_sql($_s_text, 'provider')));
		break;
		default:  $cm_no_txt_arr = array_merge(get_comics_series($_s_text), get_cm_no_arr(get_cp_comics_sql($_s_text, '')));
		break;
	}
}

if(count($cm_no_txt_arr) > 0 && $_s_text){
	$cm_no_txt_bool = true;
}

// 샐렉트박스&문자열
if($cp_no_bool == true && $cm_no_txt_bool == true){
	$cm_no_arr = array_values(array_intersect($cp_no_arr, $cm_no_txt_arr));
}else{
	if($cp_no_bool == true && $cm_no_txt_bool == false){
		$cm_no_arr = $cp_no_arr;
	}else if($cp_no_bool == false && $cm_no_txt_bool == true){
		$cm_no_arr = $cm_no_txt_arr;
	}else{
		if($sql_where_cp_bool == true || $_s_text){
			$sql_where.= " AND sl_comics = 0 "; 
		}
	}
}

// 샐렉트박스&문자열에서 나온 결과의 코믹스번호리스트
if(count($cm_no_arr) > 0){
	$sql_where.= " AND sl_comics in ( ".sql_array_merge($cm_no_arr)." ) ";
}

// 카테고리(단행본, 웹툰, 단행본웹툰)
if($_big > 0){
	$sql_where.= " AND sl_big =".$_big." ";
}

// 파트너일 경우
$sql_mb_cm_cp	= mb_cm_cp_sql($nm_member);		// 파트너 SQL
if($sql_mb_cm_cp !=''){
	$sql_where.= " AND sl_comics in ( ".sql_array_merge(get_cm_no_arr(" 1 ".$sql_mb_cm_cp))." ) ";
}

// 파트너일 경우 - 날짜
$_s_month		= mb_partner_e_month($nm_member, $_s_month);	// 종료일 현재 -1일 적용
$_e_month		= mb_partner_e_month($nm_member, $_e_month);	// 종료일 현재 -1일 적용

// 날짜
if($_s_month || $_e_month){ 
	$sql_where.= date_year($_s_month, $_e_month, 'sl.sl'); 
}else{
	// 날짜-파트너일 경우만
	if($nm_member['mb_class'] == 'p'){ 
		$sql_where.= " AND sl.sl_year_month <'".NM_TIME_YM."' "; 
	}
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sl_year_month"){ 
	$_order_field = "sl_year_month"; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
}

$sql_order = "order by ".$_order_field." ".$_order;

// 그룹
$sql_group = " group by sl.sl_year_month ";

// SQL문
$sql_field = "		sl.sl_year_month, left(sl.sl_year_month,4) as sl_year, 

					sum(sl.sl_cash_point)as sl_cash_point_sum, 
					sum(sl.sl_cash_point*cup_won)as sl_cash_point_won_sum, 

					sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
					sum(sl.sl_open)as sl_open_sum 
"; 
$sql_count	= $sql_field. " , count( distinct sl.sl_year_month ) as cnt ";

$sql_select = "		select {$sql_field} ";
$sql_select_cnt = "	select {$sql_count} ";
$sql_table = "		FROM w_vsales sl
					LEFT JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type ";

$sql_query		= " {$sql_select}		{$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_select_cnt}	{$sql_table} {$sql_where} ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts		= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data_cnts = sql_fetch($sql_query_cnt);	// SQL문 총값-레코드값
$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값

// 연도별로
$sql_group_y = " group by sl_year "; // 그룹
$sql_query_y = " {$sql_select}		{$sql_table} {$sql_where} {$sql_group_y}  ";
$sql_query_y_cnt	= " {$sql_select_cnt}	{$sql_table} {$sql_where} ";
$sql_querys_y		= " {$sql_query_y} {$sql_order} ";

$result_y = sql_query($sql_querys_y);
$row_size_y = sql_num_rows($result_y);

$years_data = array();
while ($row_y = sql_fetch_array($result_y)) {
	array_push($years_data, $row_y);
}

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sl_year_month',1));

/* cash_point */
array_push($fetch_row, array('판매건수(건)','day_calc_sl_cash_point_cnt',4));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'('.$nm_config['cf_cash_point_unit'].')','day_calc_sl_cash_point_sum',4));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'금액(원)','day_calc_sl_cash_point_won_sum',4));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "월별 매출";
$cms_head_title = $page_title;

$comics_list_line = 10;
$cm_provider_arr = $cm_publisher_arr = $cm_professional_arr = $cm_comics_arr = array();
$cm_prov_arr = $cm_publ_arr = $cm_prof_arr = $cm_arr = array();
if(count($cm_no_arr) > 0){
	$sql_cm_comics = " select * from comics where cm_no in (".implode(",",$cm_no_arr).") order by cm_series";
	$result_cm_comics = sql_query($sql_cm_comics);	
	while($row_cm_comics = sql_fetch_array($result_cm_comics)) {
		array_push($cm_arr, $row_cm_comics);
		if($row_cm_comics['cm_publisher'] > 0){ array_push($cm_publisher_arr, $row_cm_comics['cm_publisher']); }
		if($row_cm_comics['cm_professional'] > 0){ array_push($cm_professional_arr, $row_cm_comics['cm_professional']); }
		if($row_cm_comics['cm_professional_sub'] > 0){ array_push($cm_professional_arr, $row_cm_comics['cm_professional_sub']); }
		if($row_cm_comics['cm_professional_thi'] > 0){ array_push($cm_professional_arr, $row_cm_comics['cm_professional_thi']); }
		if($row_cm_comics['cm_provider'] > 0){ array_push($cm_provider_arr, $row_cm_comics['cm_provider']); }
		if($row_cm_comics['cm_provider_sub'] > 0){ array_push($cm_provider_arr, $row_cm_comics['cm_provider_sub']); }
	}

	/* 위 출판사와 작가 정보 중복 제거 */ 
	$cm_publ_arr = array_unique($cm_publisher_arr);
	$cm_prof_arr = array_unique($cm_professional_arr);
	$cm_prov_arr = array_unique($cm_provider_arr);
}

if ($_mode == 'excel') {
	$result = sql_query($sql_querys);				// 쿼리 실행 후 Resource
	$result_y = sql_query($sql_querys_y);			// 연도 별 쿼리 실행 result	
	
	$excel_path = 'excel/all_excel.php';		// 엑셀 경로와 파일명

	if ($_cms_folder_path_push == '') {
		include_once './'.$excel_path;			// 같은 디렉토리 안에 있음.			
	} else {
		include_once $_cms_folder_path_push.'/'.$excel_path;			// 같은 디렉토리 안에 있음.	
	}	

} else {
	$rows_data	= rows_data($sql_querys);			// SQL문 결과 레코드값
}

/*
if($_mode == 'excel') {
	$result = sql_query($sql_querys);
	$result_y = sql_query($sql_querys_y); //연도 별 쿼리 실행 result
	include_once './excel/all_excel.php'; //같은 디렉토리 안에 있음.
}
*/

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;

include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
	<div class="write">
		<input type="button" class="excel_down" value="<?=$page_title?> 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?<?=$_nm_paras;?>&mode=excel'"/>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?if(mb_partner_cp($nm_member) == false){?>
						<div class="cs_s_type_btn">
						<?	$s_type_list = array('작품명','작가','출판사','제공사');
							tag_radios($s_type_list, "s_type", $_s_type, 'y', '전체', ''); ?>
						</div>
						<div class="cs_search_btn">
							<?	tag_selects($nm_config['cf_big'], "big", $_big, 'y', '전체', ''); ?>
							<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
							<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
						</div>
						
						<? foreach($cp_arr as $cp_val){ /* 작가, 출판사, 제공사 데이터 출력 */ ?>
							<div class="cs_search_btn">
								<?=$cp_val[1];?> 리스트 
								<?	tag_selects(${$cp_val[0].'_arr'}, $cp_val[0], ${'_'.$cp_val[0]}, 'y', '전체', ''); ?>
							</div>
						<? } // foreach ?>
					<? } // if end ?>
					<? calendar_month($_s_month, $_e_month, $cs_calendar_on); ?>
					<!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
				</div>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<h3 id="cr_thead_mg_add" class="table_list">
		<strong>검색 결과 기준</strong>
		<ul>
		<? if(mb_class_permission('a')) { ?>
			<li>관리자는 실시간 기준[<?=NM_TIME_YMDHIS;?>]으로 나옵니다.</li>
			<li>파트너사(작가,출판사,제공사,마커터 등등)은 현재시간 기준으로 전달[<?=NM_TIME_MON_M1;?>]까지만 나옵니다.<br/>(↓↓아래는 파트너일 경우에만 나오는 안내문입니다.)</li>
		<? } ?>
		<? if(mb_class_permission('p,a')) { ?>
			<li>전체 정산에서는 현재시간 기준으로 전달[<?=NM_TIME_MON_M1;?>]까지만 나옵니다.</li>	
		<? } ?>		
		</ul>
		<br/>
		검색 리스트 <strong>검색 결과</strong>
	</h3>
	<h4 class="table_list">
		<div>
			<table>
				<caption>연도별</caption>
				<tr>
					<th class="topleft">연도</th>
					<th class="text_right">판매건수(건)</th>
					<th class="text_right"><?=$nm_config['cf_cash_point_unit_ko']?>(<?=$nm_config['cf_cash_point_unit']?>)</th>
					<th class="text_right topright"><?=$nm_config['cf_cash_point_unit_ko']?>금액(원)</th>
				</tr>
				<?	$all_sl_cash_point_sum = $all_sl_point_sum = $all_sl_cash_point_won_sum = $all_sl_point_won_sum = $all_sl_pay_sum = 0;
					foreach($years_data as $years_val){ 
						$all_sl_pay_sum+=$years_val['sl_pay_sum'];
						$all_sl_cash_point_won_sum+=$years_val['sl_cash_point_won_sum'];
						$all_sl_cash_point_sum+=$years_val['sl_cash_point_sum'];
				?>
				<tr>
					<th><?=$years_val['sl_year'];?>년</th>
					<td class="text_right"><?=number_format($years_val['sl_pay_sum']);?></td>
					<td class="text_right"><?=number_format($years_val['sl_cash_point_sum']);?></td>
					<td class="text_right"><?=number_format($years_val['sl_cash_point_won_sum']);?></td>
				</tr>
				<? } ?>
				<tr>
					<th>합계</th>
					<td class="text_right"><?=number_format($all_sl_pay_sum);?></td>
					<td class="text_right"><?=number_format($all_sl_cash_point_sum);?></td>
					<td class="text_right"><?=number_format($all_sl_cash_point_won_sum);?></td>
				</tr>
			</table>
		</div>
		<? if(count($cm_arr) > 0){?>
		<div>
			<ol>
				<li><button>코믹스리스트(<?=count($cm_no_arr);?>)</button></li>
				<li><button>출판사리스트(<?=count($cm_publ_arr);?>)</button></li>
				<li><button>작가리스트(<?=count($cm_prof_arr);?>)</button></li>
				<li><button>제공사(CP)리스트(<?=count($cm_prov_arr);?>)</button></li>	
				<li><button class="close">리스트닫기</button></li>			
			</ol>
			<table class="comics_list">
				<caption>코믹스 리스트</caption>
				<tr>
				<?  foreach($cm_arr as $cm_key => $cm_val){ 
						if($cm_key % $comics_list_line == 0 && $cm_key > 0){ ?></tr><tr><? } ?>
					<td>
						<a href="<?=NM_ADM_URL?>/salescalc/comics_all.php?sl_mode=date&cm_big=<?=$cm_val['cm_big'];?>&cm_no=<?=$cm_val['cm_no'];?>&s_month=<?=$_s_month;?>&e_month=<?=$_e_month;?>" target="_blank"><?=$cm_val['cm_series'];?></a>
					</td>
				<? } ?>
				</tr>
			</table>
			<? if(count($cm_publ_arr) > 0){?>

				<table class="comics_list">
					<caption>출판사 리스트</caption>
					<tr>
				<?
					$sql_cm_publ = "SELECT * FROM comics_publisher WHERE cp_no in (".implode(",",$cm_publ_arr).") ORDER BY cp_name";
					$result_cm_publ = sql_query($sql_cm_publ);
					for($i=0; $row_cm_publ = sql_fetch_array($result_cm_publ); $i++){?>
					<? if($i % $comics_list_line == 0 && $i > 0){ ?></tr><tr><? } ?>
					<td><?=$row_cm_publ['cp_name'];?></td>
					<? } ?>
					</tr>
				</table>
			<? } /* end if(count($cm_publ_arr) > 0)*/?>
			<? if(count($cm_prof_arr) > 0){?>

				<table class="comics_list">
					<caption>작가 리스트</caption>
					<tr>
				<?
					$sql_cm_prof = "SELECT * FROM comics_professional WHERE cp_no in (".implode(",",$cm_prof_arr).") ORDER BY cp_name";
					$result_cm_prof = sql_query($sql_cm_prof);
					for($j=0; $row_cm_prof = sql_fetch_array($result_cm_prof); $j++){?>
					<? if($j % $comics_list_line == 0 && $j > 0){ ?></tr><tr><? } ?>
					<td><?=$row_cm_prof['cp_name'];?></td>
					<? } ?>
					</tr>
				</table>
			<? } /* end if(count($cm_prof_arr) > 0)*/?>
			<? if(count($cm_prov_arr) > 0){?>

				<table class="comics_list">
					<caption>제공사(CP) 리스트</caption>
					<tr>
				<?
					$sql_cm_prov = "SELECT * FROM comics_provider WHERE cp_no in (".implode(",",$cm_prov_arr).") ORDER BY cp_name";
					$result_cm_prov = sql_query($sql_cm_prov);
					for($j=0; $row_cm_prov = sql_fetch_array($result_cm_prov); $j++){?>
					<? if($j % $comics_list_line == 0 && $j > 0){ ?></tr><tr><? } ?>
					<td><?=$row_cm_prov['cp_name'];?></td>
					<? } ?>
					</tr>
				</table>
			<? } /* end if(count($cm_prov_arr) > 0)*/?>			
		</div>
		<? } /* end if(count($cm_arr) > 0) */ ?>
	</h4>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?> text_right"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){
					$db_date = $dvalue_val['sl_year_month'];
					$db_week = get_yoil($dvalue_val['sl_week'],1,1); 
					$db_sum_won = number_format($dvalue_val['sl_cash_point_won_sum'] + $dvalue_val['sl_point_won_sum']);
					$db_sum_cnt = number_format($dvalue_val['sl_cash_point_cnt'] + $dvalue_val['sl_point_cnt']);
					
					/* cash_point  */					
					$db_cash_point_sum = number_format($dvalue_val['sl_cash_point_sum']);
					$db_cash_point_sum.= "<br/>";
					$db_sr_cash_point_sum = intval($db_row_sales_recharge['sr_cash_point_sum']);


					$db_cnt= number_format($dvalue_val['sl_pay_sum']);
					$db_cash_point_won_sum = number_format($dvalue_val['sl_cash_point_won_sum']);

					/* 요일별 class */
					$week_class = $week_en[$dvalue_val['sl_week']];
				?>
				<tr class="<?=$week_class;?>">
					<td class="<?=$fetch_row[0][1]?> text_right"><?=$db_date;?></td>

					<td class="<?=$fetch_row[1][1]?> text_right"><?=$db_cnt;?></td>
					<td class="<?=$fetch_row[2][1]?> text_right"><?=$db_cash_point_sum;?></td>
					<td class="<?=$fetch_row[3][1]?> text_right"><?=$db_cash_point_won_sum;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? rows_page(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>