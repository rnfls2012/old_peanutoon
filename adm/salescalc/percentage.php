<? include_once '_common.php'; // 공통

$class_arr = array("pf" => "작가", "pv" => "제공사", "pb" => "출판사");

// 검색단어+ 검색타입
if($_s_text){
	$cm_no_arr = array();
	$comics_sql = "SELECT cm_no FROM comics WHERE cm_series LIKE '%".$_s_text."%'";
	$comics_result = sql_query($comics_sql);
	while($comics_row = sql_fetch_array($comics_result)) {
		array_push($cm_no_arr, $comics_row['cm_no']);
	} // end while

	$ccp_comics = implode(", ", $cm_no_arr);
	$sql_where .= " AND ccp_comics IN(".$ccp_comics.") ";
} // end if

// 그룹
$sql_group = "";

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "ccp_reg_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$ccp_date_add = '';
if($_order_field == "ccp_reg_date"){  $ccp_date_add = " , ccp_mod_date DESC "; }
$sql_order = "order by ".$_order_field." ".$_order." ".$ccp_date_add ;

// SQL문
$sql_field		= " * ";
$sql_count		= " count(*) as cnt ";

$sql_table		= "select {$sql_field} from comics_calc_percentage";
$sql_table_cnt	= "select {$sql_count} from comics_calc_percentage";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_table_cnt} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts	= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값

$total_ccp = total_ccp(); // 회원 총수 - function 선언 하단

$th_class = $td_class = "";	// 상단테이블 css-class

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호', 'ccp_no', 0));
array_push($fetch_row, array('작품명', 'ccp_comics_name', 0));
array_push($fetch_row, array('분류1', 'ccp_class', 0));
array_push($fetch_row, array('분류1_이름', 'ccp_class_no', 0));
array_push($fetch_row, array('분류1_배분율', 'ccp_class_per', 0));
array_push($fetch_row, array('분류2', 'ccp_class_sub', 0));
array_push($fetch_row, array('분류2_이름', 'ccp_class_sub_no', 0));
array_push($fetch_row, array('분류2_배분율', 'ccp_class_sub_per', 0));
array_push($fetch_row, array('등록일', 'ccp_reg_date', 0));
array_push($fetch_row, array('수정일', 'ccp_mod_date', 0));
array_push($fetch_row, array('관리','management', 0));

$page_title = "코믹스 RS";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$cms_page_title;?></h1>
	<div>
		<strong><?=$page_title;?> : <?=number_format($total_ccp);?>건</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','ccp_write', <?=$popup_cms_width;?>, 700);">RS 등록</button>
	</div>
</section> <!-- member_head -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="ccp_search_form" id="ccp_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return ccp_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit ccp_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="72" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="sales_result">
	<h3 id="cr_thead_mg_add">검색 리스트
		<strong>검색 결과 수 : <?=number_format($rows_cnts);?>명</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){ $th_title_giho = $order_giho; }
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<? foreach($rows_data as $dvalue_key => $dvalue_val){ 
					$popup_url = $_cms_write."?ccp_no=".$dvalue_val['ccp_no'];

					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					switch($dvalue_val['ccp_class']) {
						case "pf" : 
							$ccp_class = "comics_professional";
							break;

						case "pv" :
							$ccp_class = "comics_provider";
							break;

						case "pb" : 
							$ccp_class = "comics_publisher";
							break;
					} // end switch

					$class_row = sql_fetch("SELECT * FROM ".$ccp_class." WHERE cp_no = ".$dvalue_val['ccp_class_no']);

					$class_sub_name = "";
					if($dvalue_val['ccp_class_sub'] != "") {
						switch($dvalue_val['ccp_class_sub']) {
						case "pf" : 
							$ccp_class_sub = "comics_professional";
							break;

						case "pv" :
							$ccp_class_sub = "comics_provider";
							break;

						case "pb" : 
							$ccp_class_sub = "comics_publisher";
							break;
						} // end switch

						$class_sub_row = sql_fetch("SELECT * FROM ".$ccp_class_sub." WHERE cp_no = ".$dvalue_val['ccp_class_sub_no']);
						$class_sub_name = $class_sub_row['cp_name'];
					} // end if
					$comics_row = get_comics($dvalue_val['ccp_comics']);
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['ccp_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$comics_row['cm_series'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$class_arr[$dvalue_val['ccp_class']];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$class_row['cp_name'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['ccp_class_per'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$class_arr[$dvalue_val['ccp_class_sub']];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$class_sub_name;?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['ccp_class_sub_per'];?></td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=get_ymd($dvalue_val['ccp_reg_date']);?></td>
					<td class="<?=$fetch_row[9][1]?> text_center"><?=get_ymd($dvalue_val['ccp_mod_date']);?></td>
					<td class="<?=$fetch_row[10][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','ccp_mod_write', <?=$popup_cms_width;?>, 700);">수정</button>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','ccp_del_write', <?=$popup_cms_width;?>, 700);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>
<? rows_page(); ?>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>

<?	/* 해당 페이지에서 사용하는 function */
// 회원 총수
function total_ccp()
{
	$sql_ccp = "select count(*) as total_ccp from comics_calc_percentage ";
	$total_ccp = sql_count($sql_ccp, 'total_ccp');
	return $total_ccp; 
}


?>