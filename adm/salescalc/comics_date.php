<?
include_once '_common.php'; // 공통

/* 화별 link */
$get_comics = get_comics($_cm_no);
$db_comics_url_para = "&cm_big=".$get_comics['cm_big']."&cm_no=".$get_comics['cm_no']."&s_date=".$_s_date."&e_date=".$_e_date;
$db_comics_episode_url = $_cms_self."?sl_mode=episode".$db_comics_url_para;

/* PARAMITER */
$cm_big; // 코믹스 대분류
$cm_no; // 코믹스 번호
$s_date; // 시작 날짜
$e_date; // 끝 날짜

/* 데이터 가져오기 */
$sql_where.= " and se.se_comics = ".$cm_no." "; /* 충전 */

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

// 파트너일 경우
$sql_mb_cm_cp	= mb_cm_cp_sql($nm_member);		// 파트너 SQL
$_e_date		= mb_partner_e_date($nm_member, $_e_date);	// 종료일 현재 -1일 적용
$sql_where.= $sql_mb_cm_cp;

// 날짜
if($_s_date && $_e_date){
	$sql_where.= date_year_month($_s_date, $_e_date, 'se.se');
}

$cmp_date = $_e_date; // 비교용 날짜

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "se.se_episode"){ 
	$_order_field = "se.se_episode"; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc";  
}

$sql_order = "order by se.se_year_month desc, se.se_day desc, ".$_order_field." ".$_order;

// 그룹
$sql_group = " GROUP BY se.se_year_month, se.se_day, se.se_episode ";

// SQL문
/* cash_point와 point 함계 결제 건수는 cash_point 건수에 포함 */
$sql_field = "	ce.*, c.cm_up_kind, c.cm_series, 
					se.se_episode, se_day, se.se_chapter, 

					sum(se_cash_point)as se_cash_point_sum, 
					sum(se.se_all_pay+se_pay)as se_cash_point_cnt, 
					sum(se_cash_point* cup.cup_won)as se_cash_point_won_sum, 

					sum(se.se_all_pay+se.se_pay)as se_pay_sum, 
					sum(se.se_open)as se_open_sum, 

					max(CONCAT(se.se_year_month,'-',se.se_day)) as se_date "; // 가져올 필드 정하기

$sql_count	= $sql_field. " , count( distinct se.se_year_month ) as cnt ";


$sql_select = "		select {$sql_field} ";
$sql_select_cnt = "	select {$sql_count} ";
$sql_table = "		FROM vsales_episode_".$cm_big." se 
					LEFT JOIN comics_episode_".$cm_big." ce ON se.se_episode = ce.ce_no 
					LEFT JOIN config_unit_pay cup ON se.se_cash_type = cup.cup_type 
					LEFT JOIN comics c ON se.se_comics = c.cm_no ";

$sql_query		= " {$sql_select}		{$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_select_cnt}	{$sql_table} {$sql_where} ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts		= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data_cnts = sql_fetch($sql_query_cnt);	// SQL문 총값-레코드값
$rows_data	= rows_data($sql_querys, false);	// SQL문 결과 레코드값

$s_all_sum = number_format($rows_data_cnts['se_cash_point_won_sum']);
$s_all_cnt = number_format($rows_data_cnts['se_pay_sum']);
$s_all_open_cnt = number_format($rows_data_cnts['se_open_sum']);
$s_all_cash_point = number_format($rows_data_cnts['se_cash_point_sum']);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('화 번호','se_episode', 0));
array_push($fetch_row, array('코믹스 명+화','se_title', 0));

/* cash_point */
array_push($fetch_row, array('열람건수(건)','se_open_sum', 4));
array_push($fetch_row, array('판매건수(건)','se_buy_sum', 4));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko']."(".$nm_config['cf_cash_point_unit'].")", 'se_cash_point_sum', 4));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko']."금액(원)", 'se_cash_point_won_sum', 4));
array_push($fetch_row, array('열람/판매 날짜','se_date', 0));

// css 클래스 접두사
$css_suffix = "cal_";

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "코믹스별 매출 날짜별";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<script type="text/javascript">

</script>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1>
		<?=$cms_head_title?> <?=$cms_page_title?>
		<a class="a_btn episode" href="<?=$db_comics_episode_url?>">화별 보러가기</a>
	</h1>
	<div>&nbsp;</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
			<input type="hidden" name="cm_big" id="cm_big" value="<?=$cm_big;?>">
			<input type="hidden" name="cm_no" id="cm_no" value="<?=$cm_no;?>">
			<input type="hidden" name="sl_mode" id="sl_mode" value="<?=$_sl_mode;?>">
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<h3 id="cr_thead_mg_add">		
		<strong>검색 결과 기준</strong>
		<ul>
		<? if(mb_class_permission('a')) { ?>
			<li>관리자는 실시간 기준[<?=NM_TIME_YMDHIS;?>]으로 나옵니다.</li>
			<li>파트너사(작가,출판사,제공사,마커터 등등)은 현재시간 기준으로 전일[<?=NM_TIME_M1;?>]까지만 나옵니다.<br/>(↓↓아래는 파트너일 경우에만 나오는 안내문입니다.)</li>
		<? } ?>
		<? if(mb_class_permission('p,a')) { ?>
			<li>전체 정산에서는 현재시간 기준으로 전일[<?=NM_TIME_M1;?>]까지만 나옵니다.</li>	
		<? } ?>		
		</ul>
		<br/>
		검색 리스트 
		<?if($s_all_sum!= ''){?>
			<table>
				<tr>
					<th class="text_right topleft">열람건수(건)</th>
					<th class="text_right">판매건수(건)</th>
					<th class="text_right"><?=$nm_config['cf_cash_point_unit_ko']?>(<?=$nm_config['cf_cash_point_unit']?>)</th>
					<th class="text_right topright"><?=$nm_config['cf_cash_point_unit_ko']?>금액(원)</th>
				</tr>
				<tr>
					<td class="text_right"><?=$s_all_open_cnt;?></td>
					<td class="text_right"><?=$s_all_cnt;?></td>
					<td class="text_right"><?=$s_all_cash_point;?></td>
					<td class="text_right"><?=$s_all_sum;?></td>
				</tr>
			</table>
		<?}?>	
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $css_suffix.$fetch_val[1];
					if($fetch_val[2] == 4){
						$th_class.= " text_right";
					}
					if($fetch_val[2] == 3){
						$th_class.= " ".$css_suffix."sl_point_bgcolor";
					}
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<? 
				/* 통계 변수 */
				$se_cash_point_won_sum_month = 0; // 월 소진 땅콩 금액
				$se_cash_point_sum_month = 0; // 월 소진 땅콩
				$se_point_won_sum_month = 0; // 월 소진 미니땅콩 금액
				$se_point_sum_month = 0; // 월 소진 미니땅콩
				$se_cash_point_cnt_month = 0; // 월 소진 건수
				$se_open_sum_month = 0; // 월 열람수

				$se_cash_point_won_sum_total = 0; // 총 소진 땅콩 금액
				$se_cash_point_sum_total = 0; // 총 소진 땅콩
				$se_point_won_sum_total = 0; // 총 소진 미니땅콩 금액
				$se_point_sum_total = 0; // 총 소진 미니땅콩
				$se_cash_point_cnt_total = 0; // 총 소진 건수
				$se_open_sum_total = 0; // 총 열람수

				$cnt = 1; // 카운트
				foreach($rows_data as $dvalue_key => $dvalue_val){ 
					$se_cash_point_won_sum_total += $dvalue_val['se_cash_point_won_sum']; // 총 소진 땅콩 금액
					$se_cash_point_sum_total += $dvalue_val['se_cash_point_sum']; // 총 소진 땅콩
					$se_cash_point_cnt_total += $dvalue_val['se_cash_point_cnt']; // 총 소진 건수
					$se_open_sum_total += $dvalue_val['se_open_sum']; // 총 열람수 
					
					$ce_up_kind = '화';
					if($dvalue_val['cm_up_kind'] == '1'){ $ce_up_kind = '권'; }				
					if($dvalue_val['ce_chapter'] > 0){
						$se_title = $dvalue_val['cm_series']." ".$dvalue_val['se_chapter'].$ce_up_kind;
						if($dvalue_val['ce_title'] != ''){ $se_title = $dvalue_val['cm_series']." ".$dvalue_val['se_chapter'].$ce_up_kind." - ".$dvalue_val['ce_title']; }	
					}else{
						$se_title = $dvalue_val['ce_notice'];
						if($dvalue_val['ce_notice'] == '' && $dvalue_val['ce_outer'] == 'y'){
							$se_title = "외전".$dvalue_val['ce_outer_chapter'].$ce_up_kind;
						}
					}
					$se_title = stripslashes($se_title);
					
					if($cmp_date != $dvalue_val['se_date']) { ?>
				<tr class="se_month_total_tr">
					<td colspan="2" class="se_month_total"><?=$cmp_date." 통계"?></td>
					<td class="<?=$css_suffix.$fetch_row[2][1]?> text_right"><?=number_format($se_open_sum_month);?></td>
					<td class="<?=$css_suffix.$fetch_row[3][1]?> text_right"><?=number_format($se_cash_point_cnt_month);?></td>
					<td class="<?=$css_suffix.$fetch_row[4][1]?> text_right"><?=number_format($se_cash_point_sum_month);?></td>
					<td class="<?=$css_suffix.$fetch_row[5][1]?> text_right"><?=number_format($se_cash_point_won_sum_month);?></td>
					<td class="<?=$css_suffix.$fetch_row[6][1]?> text_right"></td>
				</tr>
				<? 
					$se_cash_point_won_sum_month = $dvalue_val['se_cash_point_won_sum']; // 월 소진 땅콩 금액 초기화
					$se_cash_point_sum_month = $dvalue_val['se_cash_point_sum']; // 월 소진 땅콩 초기화
					$se_cash_point_cnt_month = $dvalue_val['se_cash_point_cnt']; // 월 소진 건수 초기화
					$se_open_sum_month = $dvalue_val['se_open_sum']; // 월 열람수 초기화
					$cmp_date = $dvalue_val['se_date'];
				 } else { 
				 	$se_cash_point_won_sum_month += $dvalue_val['se_cash_point_won_sum']; // 월 소진 땅콩 금액
					$se_cash_point_sum_month += $dvalue_val['se_cash_point_sum']; // 월 소진 땅콩
					$se_cash_point_cnt_month += $dvalue_val['se_cash_point_cnt']; // 월 소진 건수
					$se_open_sum_month += $dvalue_val['se_open_sum']; // 월 열람수 
				 } // end else ?>
				<tr>
					<td class="<?=$css_suffix.$fetch_row[0][1]?> text_center"><?=$dvalue_val['se_episode'];?></td>
					<td class="<?=$css_suffix.$fetch_row[1][1]?> text_center"><?=$se_title;?></td>
					<td class="<?=$css_suffix.$fetch_row[2][1]?> text_right"><?=number_format($dvalue_val['se_open_sum']);?></td>
					<td class="<?=$css_suffix.$fetch_row[3][1]?> text_right"><?=number_format($dvalue_val['se_cash_point_cnt']);?></td>
					<td class="<?=$css_suffix.$fetch_row[4][1]?> text_right"><?=number_format($dvalue_val['se_cash_point_sum']);?></td>
					<td class="<?=$css_suffix.$fetch_row[5][1]?> text_right"><?=number_format($dvalue_val['se_cash_point_won_sum']);?></td>
					<td class="<?=$css_suffix.$fetch_row[6][1]?> text_center"><?=$dvalue_val['se_date'];?></td>
				</tr>
				<? if($cnt == count($rows_data)) { ?>
				<tr class="se_month_total_tr">
					<td colspan="2" class="se_month_total"><?=$cmp_date." 통계"?></td>
					<td class="<?=$css_suffix.$fetch_row[2][1]?> text_right"><?=number_format($se_open_sum_month);?></td>
					<td class="<?=$css_suffix.$fetch_row[3][1]?> text_right"><?=number_format($se_cash_point_cnt_month);?></td>
					<td class="<?=$css_suffix.$fetch_row[4][1]?> text_right"><?=number_format($se_cash_point_sum_month);?></td>
					<td class="<?=$css_suffix.$fetch_row[5][1]?> text_right"><?=number_format($se_cash_point_won_sum_month);?></td>
					<td class="<?=$css_suffix.$fetch_row[6][1]?> text_center"></td>
				</tr>
				<tr>
					<td colspan="9"></td>
				</tr>
				<tr class="se_total_tr">
					<td colspan="2" class="se_total">총 합</td>
					<td class="<?=$css_suffix.$fetch_row[2][1]?> text_right"><?=number_format($se_open_sum_total);?></td>
					<td class="<?=$css_suffix.$fetch_row[3][1]?> text_right"><?=number_format($se_cash_point_cnt_total);?></td>
					<td class="<?=$css_suffix.$fetch_row[4][1]?> text_right"><?=number_format($se_cash_point_sum_total);?></td>
					<td class="<?=$css_suffix.$fetch_row[5][1]?> text_right"><?=number_format($se_cash_point_won_sum_total);?></td>
					<td class="<?=$css_suffix.$fetch_row[6][1]?> text_center"><?=$_s_date."~".$_e_date;?></td>
				</tr>
				<?	} // end if ?>
				<? $cnt++;
				} // end while ?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>