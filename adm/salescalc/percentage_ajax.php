<?
include_once '_common.php'; // 공통

$comics_search = base_filter($_REQUEST['comics_search']);
$comics_list_except = base_filter($_REQUEST['comics_list_except']);
$comics_list_except_arr = array();

// 이미 등록되어 있는 작품은 제외
$comics_list_reg_sql = "SELECT * FROM comics_calc_percentage";

$comics_list_reg_result = sql_query($comics_list_reg_sql);
while($except_row = sql_fetch_array($comics_list_reg_result)) {
	array_push($comics_list_except_arr, $except_row['ccp_comics']);
} // end while

if($comics_list_except != "") {
	array_push($comics_list_except_arr, $comics_list_except);
} // end if

if(count($comics_list_except_arr) > 0) {
	$comics_list_except = implode(", ", $comics_list_except_arr);
} // end if

$sql_comics_list_except = '';
if($comics_list_except != ''){
	$sql_comics_list_except = "AND cm_no not in ($comics_list_except) ";
} // end if

if($comics_search != ''){
	// LIMIT 걸려있어서, 해제함 (단행본 다 안 나옴) 170828
	// $sql = "SELECT * FROM  `comics` WHERE `cm_series` LIKE '%".$comics_search."%' $sql_comics_list_except LIMIT 0 , 10 ";
	$sql = "SELECT * FROM comics WHERE cm_service='y' AND cm_series LIKE '%".$comics_search."%' ".$sql_comics_list_except;

	$result = sql_query($sql);
	for ($i=0; $row=sql_fetch_array($result); $i++) {
		$up_kind_text = '화';
		if($row['project'] == 1){ $up_kind_text = '권'; }
		$para_result[$i]['comics'] = $row['cm_no'];
		$para_result[$i]['series'] = $row['cm_series'];
		$para_result[$i]['adult'] = $d_adult[$row['cm_adult']];
		$para_result[$i]['pay_unit'] = $nm_config['cf_cash_point_unit'];
		$para_result[$i]['pay'] = cash_point_view($row['cm_pay'], 'y','n');
		$para_result[$i]['pay_view'] = cash_point_view($row['cm_pay']);
		$para_result[$i]['episode_total'] = $row['cm_episode_total'];
		$para_result[$i]['episode_total_view'] = number_format($row['cm_episode_total']).$d_cm_up_kind[$row['cm_up_kind']];
		$para_result[$i]['pv'] = $row['cm_provider'];
		$para_result[$i]['pv_sub'] = $row['cm_provider_sub'];
		$para_result[$i]['pf'] = $row['cm_professional'];
		$para_result[$i]['pf_sub'] = $row['cm_professional_sub'];
		$para_result[$i]['pb'] = $row['cm_publisher'];
	}
	header( "content-type: application/json; charset=utf-8" );
	echo json_encode($para_result);
}
die;
?>