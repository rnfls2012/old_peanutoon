<?php
include_once '_common.php'; // 공통
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER
include_once NM_PATH.'/config/kt_connect.php'; //kt 연동

/* PARAMITER CHECK */
array_push($para_list, 'mode','id', 'mb_no');
array_push($para_list, 'type', 'title', 'contents', 'event');
array_push($para_list, 'reg_date', 'res_date');
array_push($para_list, 'img_file_name', 'img_file_path');
array_push($para_list, 'state');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'id','push_res_h');

/* 빈칸 PARAMITER 허용 */

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode', 'id');
array_push($db_field_exception, 'push_img');

/**
 * ********* merge reservation date *********
 */
$res_flag = FALSE;
if ( isset($push_res_h) && $push_res_h != '' ) {
    $res_flag = TRUE;
    array_push($para_list, 'push_res_ymd', 'push_res_h');
    array_push($blank_list, 'push_res_ymd', 'push_res_h');
    array_push($db_field_exception, 'push_res_ymd', 'push_res_h');
}

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

/**
 ********** Upload Image ***********
 */

$push_img_info = array();
$push_img_file = $_FILES['push_img'];

if ( isset($push_img_file['name']) && $push_img_file['name'] != '' ) {

    foreach ( $push_img_file as $key => $value ) {
        $push_img_info[$key] = $value;
    }

    /* 대분류 경로설정 - 폴더 체크 및 생성 */
    $img_path = 'data/push/'.NM_TIME_YMD.'/';

    /* 업로드한 이미지 명 */
    $img_tmp_name = $push_img_info['tmp_name'];
    $img_real_name = trim($push_img_info['name']);

    /* .png .jpg .gif ... */
    $img_extension = substr($push_img_info['name'], strrpos($push_img_info['name'], "."), strlen($push_img_info['name']));

    /* 저장 경로 및 DB 파일명 및 파일경로 저장 */
    $ce_cover_src = $img_path.$img_real_name;

    /* 파일 업로드 */
    if ( $img_real_name != '' ) {

        // 100KB 이하
       if ( $push_img_file['size'] < 1024 * 100 )
            $img_upload_result = kt_storage_upload($img_tmp_name, $img_path, $img_real_name);
        else
            alert('이미지의 용량을 축소해주시기 바랍니다.', 'push.write.php');
    }

    // 테이블 컬럼과 값 대입
    $_img_file_name = $img_real_name;
    $_img_file_path = $img_path.$img_real_name;
}


/**
 *********** End Upload ***********
 */


$db_tb      = "push_notification_info"; // TABLE NAME
$db_tb_pk   = "id";                  // TABLE PRIMARY KEY
$param_pk   = "id";                  // PARAMETER PRIMARY

${'_'.$db_tb_pk} = ${'_'.$param_pk};

// initialize array key, value
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

/* process data */

// normal, reservation, complete
$_state = 'n';

if ( $res_flag ) {
    $_state     = 'r';
    $_res_date  = $push_res_ymd . " " . $push_res_h;
}

$_mb_no = $nm_member['mb_no'];

if ( $_mode == 'reg' ) {

    /* 파라미터 sql-insert문 생성 */
    $sql_reg = para_sql_insert($db_tb);

    /* DB 저장 */
    if ( sql_query($sql_reg) ) {
        $db_result['msg'] = '푸시가 등록되었습니다.';
    } else {
        $db_result['state'] = 1;
        $db_result['msg'] = '등록 에러가 발생하여 저장되지 않았습니다.\n';
        $db_result['error'] = $sql_reg;
    }

} elseif ( $_mode == 'mod' ) {

    if ( ${'_'.$param_pk} == '' ) {
        $db_result['state'] = 1;
        $db_result['msg'] = '필수 변수인 '.$param_pk.'값이 없습니다.';
    } else {
        /* 파라미터 sql-update문 생성 */
        $sql_mod = para_sql_update($db_tb, $param_pk, $db_tb_pk);
        $sql_mod.= " WHERE ".$db_tb_pk."='".${'_'.$db_tb_pk}."'";

        /* DB 저장 */
        if ( sql_query($sql_mod) ) {
            $db_result['msg'] = $_id.'의 데이터가 수정되었습니다.';
        } else {
            $db_result['state'] = 1;
            $db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
            $db_result['error'] = $sql_mod;
        }
    }
} elseif ( $_mode == 'del' ) {
    /* 삭제 */
    if( ${'_'.$param_pk} == '' ) {
        $db_result['state'] = 1;
        $db_result['msg'] = '필수 변수인 '.$param_pk.'값이 없습니다.';
    } else {
        /* 데이터 삭제 */
		sql_query("UPDATE ".$db_tb." SET is_valid = 0 WHERE ".$db_tb_pk."='".${'_'.$db_tb_pk}."'");

        $db_result['msg'] = $_id.'의 데이터가 삭제되었습니다.';
    }
} else {
    /* 예외, 넘어온 값 검사 */
    foreach ( $para_list as $para_key => $para_val ) {
        echo $para_val.":".${'_'.$para_val}."<br/><br/>";
    }
    echo "mode를 다시 확인해주시기 바랍니다.";

    die;
}

pop_close($db_result['msg'], '', $db_result['error']);
