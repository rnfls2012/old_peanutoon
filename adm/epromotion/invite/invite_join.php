<?
include_once '_common.php'; // 공통

// 기본적으로 몇개 있는 지 체크;
$sql_invite_join_total = "SELECT count(*) AS total_invite_join FROM invite_join WHERE 1 ";
$total_invite_join = sql_count($sql_invite_join_total, "total_invite_join");

/* 데이터 가져오기 */
$invite_join_where = "";

// 시작일/종료일 날짜
$start_date = $end_date = "";
if($_s_date && $_e_date){
	$start_date = $_s_date;
	$end_date = date("Y-m-d", strtotime($_e_date."+1day"));
}else if($_s_date){
	$start_date = $_s_date;
	$end_date = date("Y-m-d", strtotime(NM_TIME_YMD."+1day"));
}
if($start_date && $end_date){
	$invite_join_where.= "AND (ij_date >= '$start_date' AND ij_date < '$end_date') ";
}else{
	$_s_date = "";
	$_e_date = "";
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == ""){ $_order_field = "ij_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }

$sql_order = "ORDER BY ".$_order_field." ".$_order;

$invite_join_sql = "";
$invite_join_field = " * "; // 가져올 필드 정하기
$invite_join_limit = "";

// 전체 데이터
$invite_join_sql = "SELECT $invite_join_field FROM invite_join WHERE 1 $invite_join_where $sql_order $invite_join_limit";
$result = sql_query($invite_join_sql);
$row_size = sql_num_rows($result);

// 초대 회원(중복 제거)
$invite_join_cnt_sql = "SELECT $invite_join_field FROM invite_join WHERE 1 $invite_join_where GROUP BY ij_to_member";
$cnt_result = sql_query($invite_join_cnt_sql);
$cnt_row_size = sql_num_rows($cnt_result);

// 결제 회원
$invite_join_recharge_sql = "SELECT $invite_join_field FROM invite_join WHERE 1 $invite_join_where AND ij_recharge = '1'";
$recharge_result = sql_query($invite_join_recharge_sql);
$recharge_row_size = sql_num_rows($recharge_result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호', 'ij_no', 0));
array_push($fetch_row, array('초대 회원번호', 'ij_to_member', 0));
array_push($fetch_row, array('초대 회원 ID', 'ij_to_member_id', 0));
array_push($fetch_row, array('가입 회원번호', 'ij_be_member', 0));
array_push($fetch_row, array('가입 회원 ID', 'ij_be_member_id', 0));
array_push($fetch_row, array('가입 경로', 'ij_be_member_sns_type', 0));
array_push($fetch_row, array('결제 여부', 'ij_recharge', 0));
array_push($fetch_row, array('보상 지급 여부', 'ij_invite_reward', 0));
array_push($fetch_row, array('가입 날짜', 'ij_date', 1));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "친구초대";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> 가입자 수 : 총 <?=number_format($total_invite_join);?> 명</strong>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="invite_join_search_form" id="invite_join_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return invite_join_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_date_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit popup_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="75" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- event_search -->

<section id="epromotion_result">
	<h3>검색 리스트 
		<table>
			<tr>
				<th class="topleft">초대 회원(중복 제거)</th>
				<th>가입 회원</th>
				<th class="topright">결제 회원</th>
			</tr>
			<tr>
				<td><?=number_format($cnt_row_size);?> 명</td>
				<td><?=number_format($row_size);?> 명</td>
				<td><?=number_format($recharge_row_size);?> 명</td>
			</tr>
		</table>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$ordemp_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$ordemp_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&ordemp_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_ordemp_field){
						$th_title_giho = $ordemp_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					$mb_to_row = mb_get_no($dvalue_val['ij_to_member']);
					$mb_be_row = mb_get_no($dvalue_val['ij_be_member']);
					$recharge_class = "red";
					$recharge_text = "미결제";
					$reward_class = "red";
					$reward_text = "미지급";
					if($dvalue_val['ij_recharge'] == '1') {
						$recharge_class = "blue";
						$recharge_text = "결제";
					} // end if

					if($dvalue_val['ij_invite_reward'] == '1') {
						$reward_class = "blue";
						$reward_text = "지급";
					} // end if
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['ij_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['ij_to_member'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center">
						<a href="#" onclick="popup('<?=NM_ADM_URL;?>/members/member_view.php?mb_id=<?=$mb_to_row['mb_id'];?>','member_view', <?=$popup_cms_width?>, 550);">
							<?=$mb_to_row['mb_id'];?>
						</a> 
					</td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['ij_be_member'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center">
						<a href="#" onclick="popup('<?=NM_ADM_URL;?>/members/member_view.php?mb_id=<?=$mb_be_row['mb_id'];?>','member_view', <?=$popup_cms_width?>, 550);">
							<?=$mb_be_row['mb_id'];?>
						</a>
					</td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$d_mb_sns_type[$dvalue_val['ij_be_member_sns_type']];?></td>
					<td class="<?=$fetch_row[6][1]?> <?=$recharge_class;?> text_center"><?=$recharge_text;?></td>
					<td class="<?=$fetch_row[7][1]?> <?=$reward_class;?> text_center"><?=$reward_text;?></td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=$dvalue_val['ij_date'];?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- event_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>