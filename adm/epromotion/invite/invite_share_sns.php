<?
include_once '_common.php'; // 공통

/* 데이터 가져오기 */
$sns_array = array("kakaotalk" => "카카오", "facebook" => "페이스북", "twitter" => "트위터");
$invite_share_sns_where = "slss_type = 'invite'";

// 기본적으로 몇개 있는 지 체크;
$sql_invite_share_sns_total = "SELECT count(*) AS total_invite_share_sns FROM stats_log_share_sns WHERE $invite_share_sns_where ";
$total_invite_share_sns = sql_count($sql_invite_share_sns_total, "total_invite_share_sns");

// 시작일/종료일 날짜
$start_date = $end_date = "";
if($_s_date && $_e_date){
	$start_date = $_s_date;
	$end_date = date("Y-m-d", strtotime($_e_date."+1day"));
}else if($_s_date){
	$start_date = $_s_date;
	$end_date = date("Y-m-d", strtotime(NM_TIME_YMD."+1day"));
}
if($start_date && $end_date){
	$invite_share_sns_where.= "AND (slss_date >= '$start_date' AND slss_date < '$end_date') ";
}else{
	$_s_date = "";
	$_e_date = "";
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == ""){ $_order_field = "slss_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }

$sql_order = "ORDER BY ".$_order_field." ".$_order;

$invite_share_sns_sql = "";
$invite_share_sns_field = " * "; // 가져올 필드 정하기
$invite_share_sns_limit = "";

// 전체 데이터
$invite_share_sns_sql = "SELECT $invite_share_sns_field FROM stats_log_share_sns WHERE $invite_share_sns_where $sql_order $invite_share_sns_limit";
$result = sql_query($invite_share_sns_sql);
$row_size = sql_num_rows($result);

// 공유 회원(중복 제거)
$invite_share_sns_cnt_sql = "SELECT $invite_share_sns_field FROM stats_log_share_sns WHERE $invite_share_sns_where GROUP BY slss_member";
$cnt_result = sql_query($invite_share_sns_cnt_sql);
$cnt_row_size = sql_num_rows($cnt_result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호', 'slss_no', 0));
array_push($fetch_row, array('공유 회원번호', 'slss_member', 0));
array_push($fetch_row, array('공유 회원 ID', 'slss_member_id', 0));
array_push($fetch_row, array('공유 SNS 종류', 'slss_type', 0));
array_push($fetch_row, array('공유 날짜', 'slss_date', 0));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "친구초대 SNS 공유";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> 공유 수 : 총 <?=number_format($total_invite_share_sns);?> 건</strong>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="invite_share_sns_search_form" id="invite_share_sns_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return invite_share_sns_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_date_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit popup_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="75" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- event_search -->

<section id="epromotion_result">
	<h3>검색 리스트 
		<table>
			<tr>
				<th class="topleft">총 공유 수</th>
				<th class="topright">공유 수(중복제거)</th>
			</tr>
			<tr>
				<td><?=number_format($row_size);?> 명</td>
				<td><?=number_format($cnt_row_size);?> 명</td>
			</tr>
		</table>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$ordemp_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$ordemp_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&ordemp_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_ordemp_field){
						$th_title_giho = $ordemp_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					$mb_row = mb_get_no($dvalue_val['slss_member']);
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['slss_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['slss_member'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center">
						<a href="#" onclick="popup('<?=NM_ADM_URL;?>/members/member_view.php?mb_id=<?=$mb_row['mb_id'];?>','member_view', <?=$popup_cms_width?>, 550);">
							<?=$mb_row['mb_id'];?>
						</a> 
					</td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$sns_array[$dvalue_val['slss_sns']];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['slss_date'];?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- event_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>