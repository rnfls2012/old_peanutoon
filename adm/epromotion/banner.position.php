<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_embc_position = tag_filter($_REQUEST['embc_position']);

// 기본적으로 몇개 있는 지 체크;
$sql_epromotion_total = "select count(*) as total_epromotion from epromotion_banner_cover where 1 and embc_position = '".$_embc_position."' ";
$total_epromotion = sql_count($sql_epromotion_total, 'total_epromotion');

/* 데이터 가져오기 */
$epromotion_banner_where = " and embc_position = '".$_embc_position."' ";

// 검색단어+ 검색타입
if($_s_text){
	$epromotion_banner_where.= " and emb_name like '%$_s_text%' ";
}

// 상태
if($_emb_state){
	$epromotion_banner_where.= " and emb_state = '$_emb_state' ";
}

// 시작일/종료일 날짜
$start_date = $end_date = "";
if($_s_date && $_e_date){
	$start_date = $_s_date;
	$end_date = $_e_date;
}else if($_s_date){
	$start_date = $_s_date;
	$end_date = NM_TIME_YMD;
}
if($start_date && $end_date){
	$epromotion_banner_where.= "and ( emb_date_start >= '$start_date' and emb_date_end <= '$end_date') ";
}else{
	$_s_date = "";
	$_e_date = "";
}

// 정렬
if($_ordemb_field == null || $_ordemb_field == ""){ $_ordemb_field = " IF(embc_order=0,99999,embc_order) "; }
if($_order == null || $_order == ""){ $_order = "ASC"; }
// 정렬시
$_order_field_add = $_order_add = "";
$_order_field_add = " , embc_no "; $_order_add = "DESC ";
$epromotion_banner_order = "ORDER BY ".$_ordemb_field." ".$_order." ".$_order_field_add." ".$_order_add;

$epromotion_banner_sql = "";
$epromotion_banner_field = " * "; // 가져올 필드 정하기
$epromotion_banner_limit = "";

$epromotion_banner_sql = "	SELECT $epromotion_banner_field 
							FROM epromotion_banner_cover embc 
							JOIN epromotion_banner emb ON embc.embc_emb_no = emb.emb_no 
							where 1 $epromotion_banner_where 
							$epromotion_banner_order 
							$epromotion_banner_limit";
$result = sql_query($epromotion_banner_sql);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','embc_no',0));
array_push($fetch_row, array('제목<br/>URL','embc_name',0));
array_push($fetch_row, array('이미지','embc_position',0));

array_push($fetch_row, array('시작일','embc_date_start',0));
array_push($fetch_row, array('마감일','embc_date_end',0));
array_push($fetch_row, array('정렬순서','embc_order',0));

array_push($fetch_row, array('정렬버튼','embc_order_btn',0));

array_push($fetch_row, array('성인여부','emb_adult',0));

array_push($fetch_row, array('관리','embc_management',0));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "배너";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<script type="text/javascript" src="<?=NM_URL;?>/js/jquery.dragsort.js"></script><!-- dragsort -->

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> 이벤트 : 총 <?=number_format($total_epromotion);?> 건</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','epromotion_banner_wirte', <?=$popup_cms_width;?>, 880);"><?=$page_title?> 등록</button>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="epromotion_banner_search_form" id="epromotion_banner_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return epromotion_banner_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_date_btn">
					<?	tag_selects($d_state, "emb_state", $_emb_state, 'y', '상태 전체', ''); ?>
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" placeholder="배너명 입력해주세요" value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit banner_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="75" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- event_search -->

<section id="cms_btnlist">
	<ul>
		<li class="<?echo $_embc_position == ''?'current':'';?>"><a href="<?=$_cms_self?>">전체</a></li>
	<? foreach($d_banner_position as $d_banner_position_key => $d_banner_position_val){ 
		/* 구분 */
		$d_banner_class = get_eng($d_banner_position_val);
		$d_clear = $d_mgl = "";
		if($d_banner_position_key == 6){ $d_clear = "clear"; $d_mgl = "mgl52"; }
		?>
		<li class="<?echo $_embc_position == $d_banner_position_key?'current':'';?> <?=$d_banner_class?> <?=$d_clear?> <?=$d_mgl?>"><a href="<?=$_cms_self?>?embc_position=<?=$d_banner_position_key;?>"><?=$d_banner_position_val?></a></li>
	<? } ?>
	</ul>
</section><!-- cms_btnlist -->

<section id="epromotion_result">
	<form name="episode_list_form" id="episode_list_form" enctype="multipart/form-data" method="post" action="<?=$_cms_folder;?>/banner.positionupdate.php" onsubmit="return episode_list_submit();">
		<input type="hidden" id="mode" name="mode" value="embc_list"/><!-- 입력모드 -->
		<input type="hidden" id="big" name="big" value="<?=$_big?>"/><!-- 대분류 -->
		<input type="hidden" id="ce_comics" name="ce_comics" value="<?=$_comics;?>"/><!-- 수정/삭제시 컨텐트번호 -->

		<h3>검색 리스트 
			<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
		</h3>
		<div id="cr_bg">
			<table>
				<thead id="cr_thead">
					<tr>
					<?
					//정렬표기
					$ordemb_giho = "▼";
					$th_order = "desc";
					if($_order == 'desc'){ 
						$ordemb_giho = "▲"; 
						$th_order = "asc";
					}
					foreach($fetch_row as $fetch_key => $fetch_val){

						$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
						if($fetch_val[2] == 1){
							$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&ordemb_field='.$fetch_val[1].'&order='.$th_order.'">';
							$th_ahref_e = '</a>';
						}
						if($fetch_val[1] == $_ordemb_field){
							$th_title_giho = $ordemb_giho;
						}
						$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
						$th_class = $fetch_val[1];
					?>
						<th class="<?=$th_class?>"><?=$th_title?></th>
					<?}?>
					</tr>
				</thead>
			</table>
			<ul id="embc_list_s" class="embc_list flex_view_next">
				<?	$embc_no_list = $embc_order_list = "";
					$embc_order = $start_page;
				   foreach($row_data as $dvalue_key => $dvalue_val){	

					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_folder."/banner.position.write.php?embc_no=".$dvalue_val['embc_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					// 기간시작
					$db_emb_date_start_ymd = get_ymd($dvalue_val['emb_date_start']);
					$db_emb_date_start_his = get_his($dvalue_val['emb_date_start']);

					// 기간마감
					$db_emb_date_end_ymd = get_ymd($dvalue_val['emb_date_end']);
					$db_emb_date_end_his = get_his($dvalue_val['emb_date_end']);


					$db_emb_no_date_text = "무기간";
					if(($dvalue_val['emb_date_start'] == '' && $dvalue_val['emb_date_end'] == '') && $dvalue_val['emb_date_type'] != 'n'){
						$db_emb_no_date_text = "시작일&마감일 없어서 종료됩니다.";
					}

					// 정렬순서
					$embc_order++;
					   
					$embc_no_list.= $dvalue_val['embc_no'].","; /* 번호 리스트 */
					$embc_order_list.= $embc_order.",";	 /* 정렬순서 리스트 */	

					// 위치 이미지
					$embc_cover = img_url_para($dvalue_val['embc_cover'], $dvalue_val['embc_date'], '', 250, 130, 'RK');

				?>
				<li class="embc_episode result_hover">
					<div class="embc_episode_bg">
						<ul>
							<li class="<?=$fetch_row[0][1]?>" data-embc_no="<?=$dvalue_val['embc_no'];?>"><?=$dvalue_val['embc_no'];?></li>
							<li class="<?=$fetch_row[1][1]?>"><?=$dvalue_val['emb_name'];?><br/><?=$dvalue_val['emb_url'];?></li>
							<li class="<?=$fetch_row[2][1]?>">
								<img class="embc_cover" src="<?=$embc_cover?>" alt="<?=$dvalue_val['emb_name'];?>" />
							</li>
							<?if($dvalue_val['emb_date_start'] != '' && $dvalue_val['emb_date_end'] != ''){?>
								<li class="<?=$fetch_row[3][1]?>"><?=$db_emb_date_start_ymd;?><br/><?=$db_emb_date_start_his;?></li>
								<li class="<?=$fetch_row[4][1]?>"><?=$db_emb_date_end_ymd;?><br/><?=$db_emb_date_end_his;?></li>
							<?}else{?>
								<li class="embc_date_no"><?=$db_emb_no_date_text?></li>
							<?}?>
							<li class="<?=$fetch_row[5][1]?>" data-embc_order="<?=$embc_order;?>"><?=$embc_order;?></li>
							<li class="<?=$fetch_row[6][1]?>">
								<img src="<?=NM_IMG?>cms/embc_order.png" alt="신 정렬이동" />
							</li>
							<li class="<?=$fetch_row[7][1]?>">
								<?=$d_adult[$dvalue_val['emb_adult']];?>
							</li>
							<li class="<?=$fetch_row[8][1]?>">
								<a class="mod_btn" href="#mod_btn" onclick="popup('<?=$popup_mod_url;?>','epromotion_banner_mod_wirte', <?=$popup_cms_width;?>, 900);">수정</a><br/>
								<a class="del_btn" href="#del_btn" onclick="popup('<?=$popup_del_url;?>','epromotion_banner_del_wirte', <?=$popup_cms_width;?>, 900);">삭제</a>	
							</li>
						</ul>
					</div>
				</li>
				<?}?>
			</ul>
			<?if($row_size > 0){?>
			<ul id="embc_submit">
				<li class="embc_submit_btn">
					<input type="submit" value="정렬 순서 저장" />
				</li>
			</ul>
			<?}?>
			<?	
				$embc_no_list = substr($embc_no_list,0,strrpos($embc_no_list, ","));
				$embc_order_list = substr($embc_order_list,0,strrpos($embc_order_list, ","));
			?>
			<input name="embc_no_list" value="<?=$embc_no_list?>" type="hidden" />
			<input name="embc_order_list" value="<?=$embc_order_list?>" type="hidden" />
			<input name="embc_no_list_origin" value="<?=$embc_no_list?>" type="hidden" />
			<input name="embc_order_list_origin" value="<?=$embc_order_list?>" type="hidden" />
		</div>
	</form>
</section><!-- episode_list -->
<script type="text/javascript">
	$("#embc_list_s").dragsort({ dragSelector: "div ul li.embc_order_btn", dragBetween: true, dragEnd: saveOrder, placeHolderTemplate: "<li class=' placeHolder'></li>" });
	
	function saveOrder() {
		var data = $("#embc_list_s div ul li.embc_order").map(function() { return $(this).attr('data-embc_order'); }).get();
		$("input[name=embc_order_list]").val(data.join(","));
		var order_origin_list = $("input[name=embc_order_list_origin]").val();
		var order_origin_arr = order_origin_list.split(",");
		$("#embc_list_s div ul li.embc_order").each(function(i) {
			$(this).html(order_origin_arr[i]+"("+$(this).attr('data-embc_order')+")");
		});


		var data_num = $("#embc_list_s div ul li.embc_no").map(function() { return $(this).attr('data-embc_no'); }).get();
		$("input[name=embc_no_list]").val(data_num.join(","));
	};
</script>

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>