<?
include_once '_common.php'; // 공통

/* initialize variable */
$disabled       = '';
$push_res_ymd   = NM_TIME_YMD;

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_id = tag_filter($_REQUEST['id']);

$sql_select = " 
SELECT * FROM push_notification_info pni LEFT JOIN push_result_info pri ON pni.id=pri.push_notification_info_id WHERE pni.id = '$_id'
";

/* 모드설정 */
if ( $_mode == '' )
    $_mode = 'reg'; /* 등록 */

switch ( $_mode ) {
	case "mod":
	    $mode_text = "수정";
    break;

	case "del":
	    $mode_text = "삭제";
    break;

    case "see":
        $disabled = "disabled";
    break;

    default:
        $mode_text = "저장";
        $sql_select = "";
		$sql_list = "";
	break;
}

/* 수정 또는 삭제라면...*/
if ( $sql_select != '' ) {
    $row = sql_fetch($sql_select);

    $push_res_h     = substr(get_his($row['res_date']), 0, 2);
    $push_res_ymd   = get_ymd($row['res_date']);
}

/* 대분류로 제목 */
$page_title = "APP 푸시";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;

include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
    <link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
    <script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

    <section id="cms_page_title">
        <h1><?=$cms_head_title."작성";?></h1>
    </section>

    <section id="cms_explain">
        <ul>
            <li>랜딩 페이지는 http://www.peanutoon.com/comics.php?comics=2024 형식으로 URL을 입력해주시면 됩니다.</li>
            <li>시간 별로 예약이 가능하며 0~23까지의 시간을 입력하시면 됩니다.</li>
            <li>한 자리의 시간은 0의 여부에 상관 없습니다. (ex. 9시 = 09시 둘 중 무얼 입력하든 상관없습니다.)</li>
        </ul>
    </section>
    <section id="epromotion_write">
        <form name="push_write_form" id="push_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return push_write_submit();">
            <input type="hidden" name="id" value="<?=$_id?>" />
            <input type="hidden" name="mode" value="<?=$_mode?>" />
            <input type="hidden" name="reg_date" value="<?=NM_TIME_YMDHIS?>" />
            <table>
                <tbody>
                <tr>
                    <th><label for="reserve">푸시 형식</label></th>
                    <td>
                        <div class="reserve_select">
                            <input type="radio" name="type" id="click_type" value="NO_POPUP_DIRECT" <?= get_checked('NO_POPUP_DIRECT', $row['type']) ?> <?= get_checked('', '') ?> <?=$disabled?> />
                            <label for="click_type">클릭 형식</label>
<!--
                            <input type="radio" name="type" id="popup_type" value="PUSH_RECV" />
                            <label for="popup_type">팝업 형식</label>
-->
                        </div>
                    </td>
                </tr>

                <tr>
                    <th><label for="title"><?=$required_arr[0];?> 제목 </label></th>
                    <td><input type="text" placeholder="<?= $required_arr[2] ?> 제목을 입력해주세요" <?= $required_arr[1] ?> name="title" id="title" autocomplete="off" value="<?=$row['title']?>" <?=$disabled?> /></td>
                </tr>

                <tr>
                    <th><label for="contents"><?=$required_arr[0];?> 내용 </label></th>
                    <td><input type="text" placeholder="<?= $required_arr[2] ?> 메시지를 입력해주세요" <?= $required_arr[1] ?> name="contents" id="contents" autocomplete="off" value="<?=$row['contents']?>" <?=$disabled?> /></td>
                </tr>

                <tr>
                    <th><label for="event"><?=$required_arr[0];?> 연결 URL </label></th>
                    <td><input type="text" placeholder="<?= $required_arr[2] ?> 연결시킬 URL을 입력해주세요" <?= $required_arr[1] ?> name="event" id="link" autocomplete="off" value="<?=$row['event']?>" <?=$disabled?> /></td>
                </tr>

                <tr>
                    <th><label for="push"> 이미지 파일 </label></th>
                    <td>
                        <input type="file" name="push_img" id="push_img" <?=$disabled?>/>
                        <? if ( $row['img_file_name'] != '' ) { ?>
                        <div class="push_img">
                            <p><a href="#push_img" onclick="a_click_false();">푸시 이미지 보기</a></p>
                            <p class="push_img_hide">
                                <img src="<?=img_url_para($row['img_file_path'], $push_res_date)?>" alt="push_image" />
                            </p>
                        </div>
                        <? } ?>
                        * 이미지의 용량은 100KB 이하로 설정.
                    </td>
                </tr>

                <tr>
                    <th><label for="reserve">예약</label></th>
                    <td>
                        <div class="reserve_select">
                            <span class="push_res_date">
								<input type="text" class="push_res_ymd" placeholder="예약일을 입력해주세요" name="push_res_ymd" id="push_res_ymd" value="<?=$push_res_ymd?>" autocomplete="off" readonly="readonly" <?=$disabled?> />
								<input type="text" class="push_res_h" placeholder="예약시간 입력" name="push_res_h" id="push_res_h" value="<?=$push_res_h?>"  autocomplete="off" onkeydown="onlyNumber(this)" maxlength="2" <?=$disabled?> />
								<span for="res_date_explain">시 예약</span>
							</span>
                        </div>
                    </td>
                </tr>

                <?php
								if ( isset($row['send_date']) && $row['send_date'] != '' ) {
								?>
                <tr>
                    <th><label for="send_date">전송일</label></th>
                    <td>
                    	<?=$row['send_date']?>
                    </td>
                </tr>
                
               <tr>
                	<th>
                		성공 수
                	</th>
                	<td>
                	  <?=$row['success_cnt']?>
                	</td>
                </tr>
                
                <tr>
                	<th>
                		실패 수
                	</th>
                	<td>
                		<?=$row['fail_cnt']?>
                	</td>
                </tr>
                
								<? } ?>

                <tr>
                    <td colspan='2' class="submit_btn">
                        <?php
                        if ($_mode != 'see') {
                        ?>
                        <input type="submit" value="<?=$mode_text;?>" />
                        <? } ?>
                        <input type="reset" value="취소" onclick="self.close()" />
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </section><!-- push_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>