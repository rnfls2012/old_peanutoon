<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list,'mode','emb_no');
array_push($para_list, 'emb_member','emb_member_idx');
array_push($para_list, 'emb_name','emb_url');
array_push($para_list, 'emb_script','emb_target');
array_push($para_list, 'emb_adult','emb_test');
array_push($para_list, 'emb_date_type', 'emb_state', 'emb_state_mod', 'emb_date_end','emb_date_start');
array_push($para_list, 'emb_hour_start','emb_hour_end');
array_push($para_list, 'emb_holiday','emb_holiweek');
array_push($para_list, 'emb_bgcolor');
array_push($para_list, 'emb_date');

/* 이미지 */
foreach($nm_config['cf_banner_position'] as $cf_bp_key => $cf_bp_val){
	array_push($para_list, 'embc_cover'.$cf_bp_key);
}

/* 배너 커버 리스트 DB처리 */
array_push($para_banner_cover_list, 'embc_emb_no','embc_position','embc_cover','embc_date');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'emb_no');
array_push($para_num_list, 'emb_event_no','emb_holiday','emb_holiweek');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'emb_event_no','emb_holiday','emb_holiweek');
array_push($blank_list, 'emb_date_end','emb_date_start');

/* 빈칸 PARAMITER 시 NULL 처리 */
array_push($null_list, 'emb_holiday','emb_holiweek');

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode', 'emb_state_mod');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

/* URL에 NM_URL 포함되어있으면 빈값으로 치환 */
if(strpos($_emb_url, NM_URL) !== false) {
	$_emb_url = substr_replace($_emb_url, "", strpos($_emb_url, NM_URL), strlen(NM_URL));
	if(substr($_emb_url, 0, 1) == "/") {
		$_emb_url = substr_replace($_emb_url, "", strpos($_emb_url, "/"), 1);
	} // end if
} // end if

// 상태와 날짜
$get_date_type = get_date_type($_emb_date_type, $_emb_date_start, $_emb_date_end, $_emb_hour_start, $_emb_hour_end);
if($_mode == "reg" || !($_emb_state_mod)) { $_emb_state = $get_date_type['state']; }
$_emb_date_start = $get_date_type['date_start'];
$_emb_date_end = $get_date_type['date_end'];

/* 로그인 아이디로 등록 */
$_emb_member = $nm_member['mb_no'];
$_emb_member_idx = $nm_member['mb_idx'];

$dbtable = "epromotion_banner";
$dbt_primary = "emb_no";
$para_primary = "emb_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

if($_mode == 'reg'){	
	/* 고정값 */
	$_emb_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);

	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = '배너이 등록되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '등록 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

	/* //////////////////// 배너 이미지 저장 /////////////////// */
	if($db_result['state'] == 0){ para_banner_list_save(); }

/* 수정 */
}else if($_mode == 'mod'){
	/* 고정값 */
	$_emb_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";

		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $_emb_no.'의 데이터가 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

		/* //////////////////// 배너 이미지 저장 /////////////////// */
		if($db_result['state'] == 0){ para_banner_list_save(); }
	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 배너 이미지 삭제 */
		$result_embc = sql_query("select * from ".$dbtable."_cover WHERE embc_".$dbt_primary."='".${'_'.$dbt_primary}."'");
		while ($row_embc = sql_fetch_array($result_embc)) {
			kt_storage_delete($row_embc['embc_cover']);
		}
		kt_storage_delete('epromotion/banner/'.${'_'.$dbt_primary});

		/* 데이터 삭제 */
		sql_query("delete from ".$dbtable."_cover WHERE embc_".$dbt_primary."='".${'_'.$dbt_primary}."'");

		/* 데이터 삭제 */
		sql_query("delete from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
		
		$db_result['msg'] = $emb_name.'의 데이터가 삭제되였습니다.';
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

function para_banner_list_save(){
    global $_mode, $para_list, $para_banner_cover_list, $nm_config, $_emb_no, $dbtable2 ,$dbt_primary2, $dbt_primary2, $dbtable ,$dbt_primary, $dbt_primary;
	foreach($para_list as $para_bc_key => $para_bc_val){
		global ${'_'.$para_bc_val};
	}

	/* 이미지 처리 */
	if($_mode != 'del' && $db_result['state'] == 0){
		/* 등록이라면~ [ eventnum 이 NULL 이라면 ] */
		if($_emb_no == ''){
			$sql_max = 'select COALESCE(max('.$dbt_primary.'),0) as '.$dbt_primary.'_new from '.$dbtable; /* 방금 등록번호 가져오기 */
			$row_max = sql_fetch($sql_max);
			$_emb_no = $row_max[$dbt_primary.'_new'];
		}


		/* 이미지 경로 */

		/* 대분류 경로설정 - 폴더 체크 및 생성 */
		$path_banner = 'epromotion/banner/'.$_emb_no.'/';
		
		foreach($nm_config['cf_banner_position'] as $cf_bp_key => $cf_bp_val){	
			/* 업로드한 이미지 명 */
			$image_banner_tmp_name = $_FILES['embc_cover'.$cf_bp_key]['tmp_name'];
			$image_banner_name = $_FILES['embc_cover'.$cf_bp_key]['name'];
	
			/* 리사이징 안함 DB 저장 안함 */
			${'_embc_cover'.$cf_bp_key.'noresizing'}	= etc_filter(num_eng_check(tag_filter($_REQUEST['embc_cover'.$cf_bp_key.'noresizing'])));

			/* 확장자 얻기 .png .jpg .gif */
			$image_banner_extension = substr($image_banner_name, strrpos($image_banner_name, "."), strlen($image_banner_name));

			/* 고정된 파일 명 */
			$image_banner_name_define = 'banner'.$cf_bp_key.strtolower($image_banner_extension);

			/* 저장 경로 및 DB 파일명 및 파일경로 저장 */
			$image_banner_src = $path_banner.$image_banner_name_define;

			/* 파일 업로드 */	 
			$image_banner_result = false;
			if ($image_banner_name != '') { 
				if(${'_embc_cover'.$cf_bp_key.'noresizing'} == 'y'){
					$image_banner_result = kt_storage_upload($image_banner_tmp_name, $path_banner, $image_banner_name_define); 
					$image_banner_result = tn_set_key_thumbnail($image_banner_tmp_name, $path_banner, 'embc_cover' ,$cf_bp_key, '', $image_banner_name_define);	
					$image_banner_result = tn_upload_thumbnail($image_banner_tmp_name, $path_banner, $image_banner_name_define, 'embc_cover' ,$cf_bp_key, '', $image_banner_name_define);	
				}else{
					$image_banner_result = tn_set_key_thumbnail($image_banner_tmp_name, $path_banner, 'embc_cover' ,$cf_bp_key, '', $image_banner_name_define);
					$image_banner_result = tn_upload_thumbnail($image_banner_tmp_name, $path_banner, $image_banner_name_define, 'embc_cover' ,$cf_bp_key, '', $image_banner_name_define);
				}
			}
			rmdirAll(NM_THUMB_PATH.'/'.$path_banner); // kt storage - comics 디렉토리 삭제
			rmdirAll(NM_PATH.'/'.$path_banner); // kt storage - comics 디렉토리 삭제
			
			/* 임시파일이 존재하는 경우 삭제 */
			if (file_exists($image_banner_tmp_name) && is_file($image_banner_tmp_name)) {
				unlink($image_banner_tmp_name);
			}			

			/* 고정값 */
			$_embc_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
			$_embc_emb_no = $_emb_no;
			${'_embc_cover'} = $image_banner_src;
			${'_embc_position'} = $cf_bp_key;

			// 있는지 검사
			$sql_image_chk = "SELECT count(*) as epromotion_banner_cover_total FROM  `epromotion_banner_cover` WHERE  `embc_emb_no` = '".$_emb_no."' AND  `embc_position` = '".$cf_bp_key."'";
			$row_embc_total = sql_count($sql_image_chk, 'epromotion_banner_cover_total');
			if($row_embc_total == 0){

				/* field명 */
				$sql_image = 'INSERT INTO '.$dbtable.'_cover ( ';
				foreach($para_banner_cover_list as $para_bc_key2 => $para_bc_val2){
					/* sql문구 */
					$sql_image.= $para_bc_val2.', ';
				}
				$sql_image = substr($sql_image,0,strrpos($sql_image, ","));
				/* VALUES 시작 */
				$sql_image.= ' )VALUES( ';

				/* field값 */
				foreach($para_banner_cover_list as $para_bc_key2 => $para_bc_val2){
					/* sql문구 */
					$sql_image.= '"'.${'_'.$para_bc_val2}.'", ';
				}
				$sql_image = substr($sql_image,0,strrpos($sql_image, ","));

				/* SQL문 마무리 */
				$sql_image.= ' ) ';

			}else{

				/* field명 */
				$sql_image = 'UPDATE '.$dbtable.'_cover SET ';
				foreach($para_banner_cover_list as $para_bc_key2 => $para_bc_val2){
					/* sql문구 */
					$sql_image.= $para_bc_val2." = '".${'_'.$para_bc_val2}."', ";

				}
				$sql_image = substr($sql_image,0,strrpos($sql_image, ","));
				$sql_image.= " WHERE embc_".$dbt_primary."='".$_emb_no."'";
				$sql_image.= " AND embc_position = '".$cf_bp_key."' ";

			}
			
			if (($image_banner_name != '' || $image_banner_name != '')  && $image_banner_result == true){
				/* DB 저장 */
				if(sql_query($sql_image)){
					$db_result['msg'].= '\n'.$emb_name.'의 이미지가 저장되였습니다.';
				}else{
					$db_result['state'] = 1;
					$db_result['msg'].= '\n'.'이미지 에러가 발생하여 저장되지 않았습니다.';
					$db_result['error'].= $sql_image;
				}
			}
		}
	}
}
/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
pop_close($db_result['msg'], '', $db_result['error']);
die;

?>