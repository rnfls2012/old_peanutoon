<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_emb_no = tag_filter($_REQUEST['emb_no']);

$sql = "SELECT * FROM `epromotion_banner` WHERE  `emb_no` = '$_emb_no' ";

/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
			 $row['emb_hour_end']=23; // 등록시 마감시간 23로 고정
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "배너";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<script type="text/javascript" src="<?=NM_URL."/js/jscolor.js";?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- epromotion_event_head -->

<section id="epromotion_write">
	<form name="epromotion_banner_write_form" id="epromotion_banner_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return epromotion_banner_write_submit(this);">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="emb_no" name="emb_no" value="<?=$_emb_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="emb_no">배너 번호</label></th>
					<td><?=$_emb_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="emb_name"><?=$required_arr[0];?>제목</label></th>
					<td>
						<input type="text" class="emb_name" placeholder="배너제목을 입력하세요<?=$required_arr[2];?>"  name="emb_name" id="emb_name" value="<?=$row['emb_name'];?>" <?=$required_arr[1];?> autocomplete="off" />
					</td>
				</tr>
				<tr>
					<th><label for="emb_url"><?=$required_arr[0];?>링크주소</label></th>
					<td>
						<input type="text" class="emb_url" placeholder="링크주소을 입력하세요<?=$required_arr[2];?>"  name="emb_url" id="emb_url" value="<?=$row['emb_url'];?>" <?=$required_arr[1];?> autocomplete="off" />
					</td>
				</tr>
				<tr>
					<th><label for="emb_script"><?=$required_arr[0];?>Script 사용여부</label></th>
					<td>
						<div id="emb_script" class="input_btn">
							<input type="radio" name="emb_script" id="emb_scriptn" value="n" <?=get_checked('', $row['emb_script']);?> <?=get_checked('n', $row['emb_script']);?> />
							<label for="emb_scriptn">일반주소</label>
							<input type="radio" name="emb_script" id="emb_scripty" value="y" <?=get_checked('y', $row['emb_script']);?> />
							<label for="emb_scripty">Script</label>
							(Script 선택시 링크주소에 checkAdult(); 넣어야합니다. )
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="emb_target"><?=$required_arr[0];?>오픈 타겟</label></th>
					<td>
						<div id="emb_target" class="input_btn">
							<input type="radio" name="emb_target" id="emb_target_self" value="_self" <?=get_checked('', $row['emb_target']);?> <?=get_checked('_self', $row['emb_target']);?> />
							<label for="emb_target_self">현재창</label>
							<input type="radio" name="emb_target" id="emb_target_blank" value="_blank" <?=get_checked('_blank', $row['emb_target']);?> />
							<label for="emb_target_blank">새창</label>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="emb_adult"><?=$required_arr[0];?>배너 등급</label></th>
					<td>
						<div id="emb_adult" class="input_btn">
							<input type="radio" name="emb_adult" id="emb_adulty" value="y" <?=get_checked('', $row['emb_adult']);?> <?=get_checked('y', $row['emb_adult']);?> />
							<label for="emb_adulty">성인</label>
							<input type="radio" name="emb_adult" id="emb_adultn" value="n" <?=get_checked('n', $row['emb_adult']);?> />
							<label for="emb_adultn">청소년</label>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="emb_test"><?=$required_arr[0];?>배너 테스트 여부</label></th>
					<td>
						<div id="emb_test" class="input_btn">
							<input type="radio" name="emb_test" id="emb_testy" value="y" <?=get_checked('y', $row['emb_test']);?> />
							<label for="emb_testy">테스트 적용</label>
							<input type="radio" name="emb_test" id="emb_testn" value="n" <?=get_checked('', $row['emb_test']);?> <?=get_checked('n', $row['emb_test']);?> />
							<label for="emb_testn">테스트 미적용</label>
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>배너 기간</label></th>
					<td>
						<div id="service_color" class="input_btn">
							<input type="radio" name="emb_date_type" id="emb_date_type0" value="y" <?=get_checked('', $row['emb_date_type']);?> <?=get_checked('y', $row['emb_date_type']);?>  onclick="emb_date_type_chk(this.value);" />
							<label for="emb_date_type0">기간만적용</label>
							<input class="still" type="radio" name="emb_date_type" id="emb_date_type1" value="m" <?=get_checked('m', $row['emb_date_type']);?> onclick="emb_date_type_chk(this.value);" />
							<label class="still" for="emb_date_type1">매월(날짜빈칸시 무기간)</label>
							<input class="still" type="radio" name="emb_date_type" id="emb_date_type2" value="w" <?=get_checked('w', $row['emb_date_type']);?> onclick="emb_date_type_chk(this.value);" />
							<label class="still" for="emb_date_type2">매주(날짜빈칸시 무기간)</label>
							<input type="radio" name="emb_date_type" id="emb_date_type3" value="n" <?=get_checked('n', $row['emb_date_type']);?> onclick="emb_date_type_chk(this.value);" />
							<label for="emb_date_type3">무기간</label>
						</div>
						<div class="<? echo ($row['emb_date_type'] != 'n')?"":"still";?> emb_date_type_view margin_top10">
							<span for="s_date">시작일 :</span>
							<input type="text" class="s_date readonly" placeholder="클릭하세요" name="emb_date_start" id="s_date" value="<?=$row['emb_date_start'];?>" autocomplete="off" readonly />
							<?=set_hour('emb_hour_start', $row['emb_hour_start'], '시간 :', 'n');?>00분
							~ 
							<span for="e_date">종료일 :</span>
							<input type="text" class="e_date readonly" placeholder="클릭하세요" name="emb_date_end" id="e_date" value="<?=$row['emb_date_end'];?>" autocomplete="off" readonly />
							<?=set_hour('emb_hour_end', $row['emb_hour_end'], '시간 :', 'n');?>59분
						</div>
						<div class="emb_holiday_view still margin_top10">
							<span for="emb_holiweek">매월</span>
							<?	tag_selects($d_emb_day_list, "emb_holiday", $row['emb_holiday'], 'y', '사용안함', ''); ?>
						</div>
						<div class="emb_holiweek_view still margin_top10">
							<span for="emb_holiweek">매주</span>
							<?	tag_selects($d_emb_week_list, "emb_holiweek", $row['emb_holiweek'], 'y', '사용안함', ''); ?>
						</div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="emb_state">배너 상태</label></th>
					<? if($_mode == "del" || $row['emb_state'] == 'r') { ?>
					<td><?=$d_state[$row['emb_state']];?></td>
					<? } else { ?>
					<td>
						<? tag_selects(array_splice($d_state, 1, 2), "emb_state", $row['emb_state'], 'n'); ?>
						<input type="checkbox" id="emb_state_mod" name="emb_state_mod" value="y"/> 
						<label for="emb_state_mod">배너 상태 적용</label>
					</td>
					<? } // end else ?>
				</tr>
				<?}?>
				<tr>
					<th><label for="emb_bgcolor"><?=$required_arr[0];?>배경색<br/>[Web]메인슬라이드</label></th>
					<td>
						<input type="text" <?=$required_arr[1];?> name="emb_bgcolor" id="emb_bgcolor" class="color" value="<?=$row['emb_bgcolor'];?>" autocomplete="off" />
					</td>
				</tr>
				<?if($_mode != 'del'){?>
				<tr>
					<th><label for="emb_cover"><?=$required_arr[0];?>배너 표지</label></th>
					<td>
						<? 
						foreach($nm_config['cf_banner_position'] as $cf_bp_key => $cf_bp_val){
							if($cf_bp_val == ''){ continue; }
							$cf_bp_arr = explode(": ", $cf_bp_val);
							$cf_bp_name = $cf_bp_arr[0];
							$cf_bp_explan = $cf_bp_arr[1];

							// embc_cover
							$sql_epromotion_banner_cover = "SELECT * FROM epromotion_banner_cover WHERE embc_emb_no = '".$row['emb_no']."' AND embc_position='".$cf_bp_key."' ";
							$row_embc = sql_fetch($sql_epromotion_banner_cover);
						
						?>	
							<div class="file_cover">
								<? if($row_embc['embc_cover']!=''){ $embc_cover_src = img_url_para($row_embc['embc_cover'], $row_embc['embc_date']); } ?>
								<input type="file" name="embc_cover<?=$cf_bp_key?>" id="embc_cover<?=$cf_bp_key?>" data-value="<?=$embc_cover_src?>" />
								<? if(cs_is_admin()){ ?>
									<input type="checkbox" name="embc_cover<?=$cf_bp_key?>_noresizing" id="embc_cover<?=$cf_bp_key?>_noresizing" value="y" class="noresizing" />
									<label for="embc_cover<?=$cf_bp_key?>_noresizing">RESIZING-사용안함</label>
								<? } ?>
								<?if($row_embc['embc_cover']!=''){?>
									<div class="embc_cover">
										<p><a href="#emb_cover" onclick="a_click_false();"><?=$cf_bp_name?> 보기</a></p>
										<p class="embc_cover_hide"><img src="<?=$embc_cover_src?>" alt="<?=$row['emb_name']."배너배너"?>" /></p>
									</div>
								<?}else{?>
									<p class="cover_explan file_name">
										<?=$cf_bp_name?>
									</p>
								<?}?>
								<p class="cover_explan">이미지 업로드 정보 : <?=$cf_bp_explan?> - 확장자:jpg, gif, png</p>
							</div>
						<?}?>

					</td>
				</tr>
				<?}?>				
				<? if($_mode == "mod") { ?>
				<tr>
					<th><label for="cdn_purge">Purge<br/>이미지 파일경로 복사</label></th>
					<td>
						<textarea id="copy_text">
<? $copy_text_ajax = array();
	foreach($nm_config['cf_banner_position'] as $cf_bp_key2 => $cf_bp_val2){
	if($cf_bp_val2 == ''){ continue; }
	$cf_bp_arr2 = explode(": ", $cf_bp_val2);
	$cf_bp_name2 = $cf_bp_arr2[0];
	$cf_bp_explan2 = $cf_bp_arr2[1];

	// embc_cover
	$sql_epromotion_banner_cover2 = "SELECT * FROM epromotion_banner_cover WHERE embc_emb_no = '".$row['emb_no']."' AND embc_position='".$cf_bp_key2."' ";
	$row_embc2 = sql_fetch($sql_epromotion_banner_cover2);
if($row_embc2['embc_cover'] !=''){?><?='/'.$row_embc2['embc_cover'];
array_push($copy_text_ajax, '/'.$row_embc2['embc_cover'].'||');
?>

<? } 
}?></textarea>
						<a class="copy">IMAGE COPY</a>
						<a class="purge" href="<?=NM_ADM_URL?>/cdn_purge.php" target="_blank">Purge 가기</a>
						<input type="hidden" id="copy_text_ajax"  value="<? foreach($copy_text_ajax as $copy_text_ajax_val){ echo $copy_text_ajax_val; }?>"/>
						<a class="copy_btn" onclick="cdn_api_purge_send();">Purge 호출</a>
					</td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="emb_state">저장일</label></th>
					<td><?=$row['emb_date'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- epromotion_event_write -->
<script type="text/javascript">
<!--
	function epromotion_event_write_submit(f){

			

		/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
		var onlynumber = onlynumber_text = "";
		var onlynumber_check = true;
		$('.onlynumber').each(function() {
			onlynumber = $(this).val();
			onlynumber_text = '숫자만 입력해주세요';
			if(onlynumber != ''){
				var pattern = /(^[0-9]+$)/;
				if (!pattern.test(onlynumber)) {
					alert(onlynumber_text);
					$(this).val('');
					$(this).focus();
					onlynumber_check = false;
					return false; 
				}
			}
		});
		if(onlynumber_check == false){ return false; }

		/* 날짜검사 */
		switch(f.emb_date_type.value){
			case 'y': 
				if(f.emb_date_start.value == ""){alert('시작일을 선택해주세요');return false;}
				if(f.emb_date_end.value == ""){alert('종료일을 선택해주세요');return false;}
			break;

		}
	}
//-->
</script>

<? include_once NM_ADM_PATH.'/clipboard.php'; // 복사 ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>