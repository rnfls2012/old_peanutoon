<?
include_once '_common.php'; // 공통

/* PARAMITER 
emp_type //이벤트타입
emp_state //이벤트상태
emp_calc_way //이벤트 지급 계산법
emp_cash_point //이벤트 지급 코인
emp_point //이벤트 지급 보너스 코인
date_type // 날짜검색타입
s_date //이벤트시작일
e_date //이벤트마감일
emp_reg_date //등록일
emp_mod_date //수정일
*/

// 기본적으로 몇개 있는 지 체크;
$sql_epromotion_total = "select count(*) as total_epromotion from epromotion_popup where 1  ";
$total_epromotion = sql_count($sql_epromotion_total, 'total_epromotion');

/* 데이터 가져오기 */
$epromotion_popup_where = '';

// 검색단어+ 검색타입
if($_s_text){
	$epromotion_popup_where.= " and emp_name like '%$_s_text%' ";
}

// 상태
if($_emp_state){
	$epromotion_popup_where.= " and emp_state = '$_emp_state' ";
}


// 시작일/종료일 날짜
$start_date = $end_date = "";
if($_s_date && $_e_date){
	$start_date = $_s_date;
	$end_date = $_e_date;
}else if($_s_date){
	$start_date = $_s_date;
	$end_date = NM_TIME_YMD;
}
if($start_date && $end_date){
	$epromotion_popup_where.= "and ( emp_date_start >= '$start_date' and emp_date_end <= '$end_date') ";
}else{
	$_s_date = "";
	$_e_date = "";
}

// 정렬
if($_ordemp_field == null || $_ordemp_field == ""){ $_ordemp_field = " emp_no "; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$epromotion_popup_order = "order by ".$_ordemp_field." ".$_order;

$epromotion_popup_sql = "";
$epromotion_popup_field = " * "; // 가져올 필드 정하기
$epromotion_popup_limit = "";

$epromotion_popup_sql = "SELECT $epromotion_popup_field FROM epromotion_popup where 1 $epromotion_popup_where $epromotion_popup_order $epromotion_popup_limit";
$result = sql_query($epromotion_popup_sql);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','emp_no',0));
array_push($fetch_row, array('제목','emp_title',0));
array_push($fetch_row, array('시작일','	emp_date_start',0));
array_push($fetch_row, array('마감일','emp_date_end',0));
array_push($fetch_row, array('상태','emp_state',0));
array_push($fetch_row, array('접속기기','emp_device',0));
array_push($fetch_row, array('숨김시간<br/>접기기능','emp_disable_hours',0));
array_push($fetch_row, array('TOP위치<br/>LEFT위치','emp_left_top',0));
array_push($fetch_row, array('PC 넓이 / Mobile 넓이<br/>높이','emp_width_height',0));
array_push($fetch_row, array('관리','emp_management',0));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "팝업";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> 이벤트 : 총 <?=number_format($total_epromotion);?> 건</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','epromotion_popup_wirte', <?=$popup_cms_width;?>, 880);"><?=$page_title?> 등록</button>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="epromotion_popup_search_form" id="epromotion_popup_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return epromotion_popup_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_date_btn">
					<?	tag_selects($d_state, "emp_state", $_emp_state, 'y', '상태 전체', ''); ?>
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" placeholder="팝업명 입력해주세요" value="<?=$_s_text?>">
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit popup_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="75" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- event_search -->

<section id="epromotion_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$ordemp_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$ordemp_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&ordemp_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_ordemp_field){
						$th_title_giho = $ordemp_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?emp_no=".$dvalue_val['emp_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					$db_emp_no_date_text = "";
					if($dvalue_val['emp_date_start'] == '' && $dvalue_val['emp_date_end'] == ''){
						$db_emp_no_date_text = "시작일&마감일 없어서 종료됩니다.";
					}
					$db_emp_width_pc = $dvalue_val['emp_width_pc']." px";
					$emp_width_mo = $dvalue_val['emp_width_mo']." %";
					$db_emp_height = $dvalue_val['emp_height']." px";
					if($dvalue_val['emp_height'] == '0'){$db_emp_height = "auto";}

					$db_emp_disable_hours = $dvalue_val['emp_disable_hours']." 시간";
					if($dvalue_val['emp_disable_hours'] % 24 == 0){
						$db_emp_disable_hours = "하루";
					}
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['emp_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['emp_name'];?></td>
					<?if($dvalue_val['emp_date_start'] != '' && $dvalue_val['emp_date_end'] != ''){?>
						<td class="<?=$fetch_row[2][1]?> text_center">
							<?=$dvalue_val['emp_date_start'];?>일 <br/><?=$dvalue_val['emp_hour_start'];?>시00분
						</td>
						<td class="<?=$fetch_row[3][1]?> text_center">
							<?=$dvalue_val['emp_date_end'];?>일 <br/><?=$dvalue_val['emp_hour_end'];?>시59분
						</td>
					<?}else{?>
						<td colspan='2' class="emp_date_no text_center"><?=$db_emp_no_date_text?></td>
					<?}?>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$d_state[$dvalue_val['emp_state']];?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$d_emp_device[$dvalue_val['emp_device']];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center">
						<?=$db_emp_disable_hours;?><br/>
						<? echo $dvalue_val['emp_fold']=="y"?"사용":"미사용"; ?>
					</td>
					<td class="<?=$fetch_row[7][1]?> text_center">
						<?=$dvalue_val['emp_left'];?> px (TOP)<br/>
						<?=$dvalue_val['emp_top'];?> px (LEFT)
					</td>
					<td class="<?=$fetch_row[8][1]?> text_center">
						<?=$db_emp_width_pc;?> / <?=$emp_width_mo;?><br/>
						<?=$db_emp_height;?>
					</td>
					<td class="<?=$fetch_row[9][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','epromotion_popup_mod_wirte', <?=$popup_cms_width;?>, 900);">수정</button>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','epromotion_popup_del_wirte', <?=$popup_cms_width;?>, 900);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- event_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>