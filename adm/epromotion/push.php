<?
include_once '_common.php'; // 공통

/* PARAMITER */

// 기본적으로 몇개 있는 지 체크;
$sql_push_total = "select count(*) as push_count from push_notification_info where 1 ";
$total_push = sql_count($sql_push_total, 'push_count');

/* 데이터 가져오기 */
$push_where = "";

// 검색단어+ 검색타입
if($_s_text){
	$push_where.= "and contents like '%$_s_text%' ";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "id"; }
if($_order == null || $_order == ""){ $_order = "DESC"; }
$order_by = "ORDER BY ".$_order_field." ".$_order;

$sql_push = "";
$field_push = " * "; // 가져올 필드 정하기
$order_push = "id";
$limit_push = "";

$sql_push = "
SELECT $field_push FROM push_notification_info WHERE 1 AND is_valid = 1 $push_where $order_by $limit_push
";

$result = sql_query($sql_push);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','id',1));
array_push($fetch_row, array('제목','title',0));
array_push($fetch_row, array('내용','contents',0));
array_push($fetch_row, array('랜딩 페이지','link_url',0));
array_push($fetch_row, array('상태','state',0));
array_push($fetch_row, array('등록일','reg_date',1));
array_push($fetch_row, array('예약일','res_date',1));
array_push($fetch_row, array('전송 완료일','send_date',1));
array_push($fetch_row, array('관리','push_manage',1));

$state_arr = array('c'=>'완료', 'r'=>'예약', 'n'=>'대기', 's'=>'전송 중');

$page_title = "APP 푸시";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>총 <?=$page_title?> : <?=number_format($total_push);?> 개</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write?>','push_write', <?=$popup_cms_width?>, 500);"><?=$page_title?> 보내기</button>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="push_search_form" id="push_search_form" method="post" action="<?=$_cms_self?>" onsubmit="return push_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">

					<? tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>

					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit push_submit">
				<input type="submit" class="<?=$cs_submit_h?>" data-base_h="30" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="epromotion_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size)?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $cp_css.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<? foreach ( $row_data as $dvalue_key => $dvalue_val ) {
                    $popup_send_url =  $_cms_write."?mode=send&id=".$dvalue_val['id'];
                    $popup_mod_url = $_cms_write."?mode=mod&id=".$dvalue_val['id'];
                    $popup_del_url = $_cms_write."?mode=del&id=".$dvalue_val['id'];
                    $popup_resend_url =  $_cms_write."?mode=see&id=".$dvalue_val['id'];
				?>
				<tr class="result_hover">
					<td class="<?=$cp_css.$fetch_row[0][1]?> text_center"><?=$dvalue_val['id']?></td>
                    <td class="<?=$cp_css.$fetch_row[1][1]?> text_center"><?=$dvalue_val['title']?></td>
					<td class="<?=$cp_css.$fetch_row[2][1]?> text_center"><?=$dvalue_val['contents']?></td>
					<td class="<?=$cp_css.$fetch_row[3][1]?> text_center"><?=$dvalue_val['event']?></td>
					<td class="<?=$cp_css.$fetch_row[4][1]?> text_center"><?=$state_arr[$dvalue_val['state']]?></td>
					<td class="<?=$cp_css.$fetch_row[5][1]?> text_center"><?=$dvalue_val['reg_date']?></td>
					<td class="<?=$cp_css.$fetch_row[6][1]?> text_center"><?=$dvalue_val['res_date']?></td>
					<td class="<?=$cp_css.$fetch_row[7][1]?> text_center"><?=$dvalue_val['send_date']?></td>
					<td class="<?=$cp_css.$fetch_row[8][1]?> text_center">

						<? if ( $dvalue_val['state'] == 'r' || $dvalue_val['state'] == 'n' ) { ?>
                            <button class="send_btn" onclick="send_push_data(<?=$dvalue_val['id']?>)">전송</button>
                            <button class="mod_btn" onclick="popup('<?=$popup_mod_url?>','push_mod_write', <?=$popup_cms_width?>, 500);">수정</button>
                            <button class="del_btn" onclick="popup('<?=$popup_del_url?>','push_del_write', <?=$popup_cms_width?>, 500);">삭제</button>
						<? } elseif ( $dvalue_val['state'] == 'c' && isset($dvalue_val['send_date']) ) { ?>
                            <button class="send_btn" onclick="popup('<?=$popup_resend_url?>','push_mod_write', <?=$popup_cms_width?>, 600);">보기</button>
                        <? } ?>

					</td>
				</tr>
				<?} // end foreach?>
			</tbody>
		</table>
	</div>
</section><!-- epromotion_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>