<?
include_once '_common.php'; // 공통
include_once (NM_EDITOR_LIB);

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_eme_no = tag_filter($_REQUEST['eme_no']);

$sql = "SELECT * FROM `epromotion_event` WHERE `eme_no` = '$_eme_no' ";
	
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
			 $row['eme_hour_end']=23; // 등록시 마감시간 23로 고정
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
	/* 수정 또는 삭제라면 이벤트 정보 가져오기 */
	if($row['eme_event_no'] > 0 && $row['eme_event_type'] == 'e'){
		$sql_event = "SELECT * FROM event_pay WHERE (ep_state = 'y' OR  ep_state = 'r') AND ep_no = ".$row['eme_event_no']."";
		$row_event = sql_fetch($sql_event);
		$this_ep_cover = NM_IMG.$row_event['ep_cover'];
		$this_ep_no = $row_event['ep_no'];
		$this_ep_name = $row_event['ep_name'];
		$this_ep_date_text = $row_event['ep_date_start']." ~ ".$row_event['ep_date_end'];
		if($row_event['ep_date_type'] == "n"){ $this_ep_date_text = "무기간"; }
		$this_ep_top_class = "hide";
	}

	// 단일 코믹스 번호
	if($row['eme_event_no'] > 0 && $row['eme_event_type'] == 'c'){
		$sql_comics = "SELECT * FROM  `comics` WHERE  `cm_no` = ".$row['eme_event_no']."";
		$row_comics = sql_fetch($sql_comics);
	}
}


/* 이벤트 검색 */
$event_arr = array();
$sql_event = "SELECT * FROM event_pay WHERE ep_state = 'y' OR  ep_state = 'r' ";
$result_event = sql_query($sql_event);
if(sql_num_rows($result_event) != 0){
	while ($row_event = sql_fetch_array($result_event)) {
		$row_event['ep_date_text'] = $row_event['ep_date_start']." ~ ".$row_event['ep_date_end'];
		if($row_event['ep_date_type'] == "n"){ $row_event['ep_date_text'] = "무기간"; }
		array_push($event_arr, $row_event);
	}
}
/*  */

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "이벤트";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
echo $editor_content_js;
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- epromotion_event_head -->

<section id="epromotion_write" class="editor_html">
	<form name="epromotion_event_write_form" id="epromotion_event_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return epromotion_event_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="eme_no" name="eme_no" value="<?=$_eme_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->
		<input type="hidden" id="eme_event_no" name="eme_event_no"  value="<?=$row['eme_event_no'];?>"/><!-- 이벤트연결번호(코믹스번호, 이벤트번호) -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="eme_no">이벤트 번호</label></th>
					<td><?=$_eme_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="eme_event_type"><?=$required_arr[0];?>이벤트 타입</label></th>
					<td>
						<? tag_selects($d_eme_event_type, "eme_event_type", $row['eme_event_type'], 'n'); ?>
						 이벤트연결번호: <span id="eme_event_no_text"><?echo ($row['eme_event_no']=='' || $row['eme_event_no']==0)?'없음':$row['eme_event_no']; ?></span>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>이벤트 기간</label></th>
					<td>
						<div id="service_color" class="input_btn">
							<input type="radio" name="eme_date_type" id="eme_date_type0" value="y" <?=get_checked('', $row['eme_date_type']);?> <?=get_checked('y', $row['eme_date_type']);?>  onclick="eme_date_type_chk(this.value);" />
							<label for="eme_date_type0">기간만적용</label>
							<input class="still" type="radio" name="eme_date_type" id="eme_date_type1" value="m" <?=get_checked('m', $row['eme_date_type']);?> onclick="eme_date_type_chk(this.value);" />
							<label class="still" for="eme_date_type1">매월(날짜빈칸시 무기간)</label>
							<input class="still" type="radio" name="eme_date_type" id="eme_date_type2" value="w" <?=get_checked('w', $row['eme_date_type']);?> onclick="eme_date_type_chk(this.value);" />
							<label class="still" for="eme_date_type2">매주(날짜빈칸시 무기간)</label>
							<input type="radio" name="eme_date_type" id="eme_date_type3" value="n" <?=get_checked('n', $row['eme_date_type']);?> onclick="eme_date_type_chk(this.value);" />
							<label for="eme_date_type3">무기간</label>
						</div>
						<div class="<? echo ($row['eme_date_type'] != 'n')?"":"still";?> eme_date_type_view margin_top10">
							<span for="s_date">시작일 :</span>
							<input type="text" class="s_date readonly" placeholder="클릭하세요" name="eme_date_start" id="s_date" value="<?=$row['eme_date_start'];?>" autocomplete="off" readonly /> 
							<?=set_hour('eme_hour_start', $row['eme_hour_start'], '시간 :', 'n');?>00분
							~ 
							<span for="e_date">종료일 :</span>
							<input type="text" class="e_date readonly" placeholder="클릭하세요" name="eme_date_end" id="e_date" value="<?=$row['eme_date_end'];?>" autocomplete="off" readonly />
							<?=set_hour('eme_hour_end', $row['eme_hour_end'], '시간 :', 'n');?>59분
						</div>
						<div class="eme_holiday_view still margin_top10">
							<span for="eme_holiweek">매월</span>
							<?	tag_selects($day_list, "eme_holiday", $row['eme_holiday'], 'y', '사용안함', ''); ?>
						</div>
						<div class="eme_holiweek_view still margin_top10">
							<span for="eme_holiweek">매주</span>
							<?	tag_selects($week_list, "eme_holiweek", $row['eme_holiweek'], 'y', '사용안함', ''); ?>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="eme_adult"><?=$required_arr[0];?>이벤트 등급</label></th>
					<td>
						<div id="eme_adult" class="input_btn">
							<input type="radio" name="eme_adult" id="eme_adulty" value="y" <?=get_checked('', $row['eme_adult']);?> <?=get_checked('y', $row['eme_adult']);?> />
							<label for="eme_adulty">성인</label>
							<input type="radio" name="eme_adult" id="eme_adultn" value="n" <?=get_checked('n', $row['eme_adult']);?> />
							<label for="eme_adultn">청소년</label>
						</div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="eme_state">이벤트 상태</label></th>
					<td><?=$d_state[$row['eme_state']];?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="eme_cover"><?=$required_arr[0];?>이벤트 표지</label></th>
					<td>
						<? if($row['eme_cover']!=''){ $eme_cover_src = img_url_para($row['eme_cover'], $row['eme_date']); } ?>
						<input type="file" name="eme_cover" id="eme_cover" data-value="<?=$eme_cover_src?>" />
						<? if(cs_is_admin()){ ?>
							<input type="checkbox" name="eme_cover_noresizing" id="eme_cover_noresizing" value="y" class="noresizing" />
							<label for="eme_cover_noresizing">RESIZING-사용안함</label>
						<? } ?>
						<p class="eme_cover_explan">이미지 업로드 정보 : width:1000px, height:417px, 이미지파일 확장자:jpg, gif, png</p>
						<?if($row['eme_cover']!=''){?>
						<div class="eme_cover">
							<p><a href="#eme_cover" onclick="a_click_false();">메인배너보기</a></p>
							<p class="eme_cover_hide"><img src="<?=$eme_cover_src?>" alt="<?=$row['eme_name']."이벤트배너"?>" /></p>
						</div>
						<?}?>
					</td>
				</tr>

				<? if($_mode == "mod") { ?>
				<tr>
					<th><label for="cdn_purge">Purge<br/>이미지 파일경로 복사</label></th>
					<td>
						<textarea id="copy_text"><? tn_get_info($row['eme_cover'], 'eme_cover'); ?></textarea>
						<a class="copy">IMAGE COPY</a>
						<a class="purge" href="<?=NM_ADM_URL?>/cdn_purge.php" target="_blank">Purge 가기</a>
						<input type="hidden" id="copy_text_ajax"  value="<? tn_get_info($row['eme_cover'], 'eme_cover',true); ?>"/>
						<a class="copy_btn" onclick="cdn_api_purge_send();">Purge 호출</a>
					</td>
				</tr>
				<?}?>

				<tr>
					<th><label for="eme_name"><?=$required_arr[0];?>제목</label></th>
					<td>
						<input type="text" class="eme_name" placeholder="이벤트제목을 입력하세요<?=$required_arr[2];?>"  name="eme_name" id="eme_name" value="<?=$row['eme_name'];?>" <?=$required_arr[1];?> autocomplete="off" />
					</td>
				</tr>
                <tr>
                    <th><label for="eme_order">우선순위</label></th>
                    <td>
                        <input type="text" class="eme_order" placeholder="우선순위를 정하세요."  name="eme_order" id="eme_order" value="<?=$row['eme_order'];?>" autocomplete="off" />
                    </td>
                </tr>
				<tr>
					<th><label for="eme_summary">이벤트 요약<br>(1~2줄 내 작성)</label></th>
					<td>
						<textarea id="eme_summary" name="eme_summary"><?=stripslashes($row['eme_summary']);?></textarea>
					</td>
				</tr>
				<tr class="<?echo ($row['eme_event_type']=='c')?'':'display_none'; ?>" id="comics_type_no">
					<th><label><?=$required_arr[0];?>단일코믹스번호</label></th>
					<td>
						<div id="comics_list">
							<? if($row['eme_event_no'] > 0 && $row['eme_event_type']=='c'){ ?>
								<dl>
									<dt>
										<?=$row_comics['cm_no']?>:<?=$row_comics['cm_series']?>
									</dt>
									<dd>
										<img src="<?=NM_IMG.$row_comics['cm_cover']?>" alt="<?=$row_comics['cm_series']?>" />
									</dd>
								<dl>
							<? }else{ ?>
								선택한 코믹스가 없습니다.
							<? } ?>
						</div>
						<div class="margin_top10">
							<!-- 코믹스 선택 리스트 -->
							<input type="text" class="comics_search text_ct_200" placeholder="검색하세요" id="comics_search" value="" autocomplete="off" onkeydown='comics_search_chk();' />
							<a id="comics_search_btn" class="comics_search_btn" href='#comics_search' onclick='a_click_false()'>검색</a>
							<div id="comics_search_result"></div>
							<!-- 코믹스 검색 리스트 -->
						</div>
					</td>
				</tr>
				<tr class="<?echo ($row['eme_event_type']=='e')?'':'display_none'; ?>" id="event_type_no">
					<th><label for="eme_event_no">이벤트 번호</label></th>
					<td>
						<!-- 코믹스일 경우 -->
						<div>
							<ul id="event_list" class="event_list" data-class="hide">
								<li class="<?=$this_ep_top_class?>" data-ep_no="">선택하세요</li>
								<?if($this_ep_no > 0 && $row['eme_event_type']=='e'){?>
								<li class="on" data-ep_no="<?=$this_ep_no?>">
									<img src="<?=$this_ep_cover?>" alt="<?=$this_ep_name?>" width="75" height="50" />
									<?=$this_ep_no?>:<?=$this_ep_name?> (<?=$this_ep_date_text?>)
								</li>
								<?}?>
							<?foreach($event_arr as $event_key => $event_val){
								if($this_ep_no == $event_val['ep_no']){continue;}
								$event_ep_cover = NM_IMG.$event_val['ep_cover'];
								/* 선택한것만 보이게 하기 */
							?>
								<li class="hide" data-ep_no="<?=$event_val['ep_no']?>">
									<img src="<?=$event_ep_cover?>" alt="<?=$event_val['ep_name']?>" />
									<?=$event_val['ep_no']?>:<?=$event_val['ep_name']?> (<?=$event_val['ep_date_text']?>)
								</li>
							<?}?>
							</ul>
						</div>
					</td>
				</tr>
				<tr class="<?echo ($row['eme_event_type']=='t' || $row['eme_event_type']=='')?'':'display_none'; ?>" id="editor_html">
					<th><label for="eme_text">내용</label></th>
					<td><?php echo editor_html('eme_text', $row['eme_text']); ?></td>
				</tr>
				<tr class="<?echo ($row['eme_event_type']=='u')?'':'display_none'; ?>" id="event_url">
					<th><label for="eme_event_no">연결 URL 주소</label></th>
					<td>
						<input type="text" class="eme_direct_url" placeholder="연결 URL을 입력하세요"  name="eme_direct_url" id="eme_direct_url" value="<?=$row['eme_direct_url'];?>">
						<div style="margin-top: 5px;">
						※주소를 제외한 파일명만을 입력하시면 됩니다.
						<br>
						예시) peanutoon.com/test.php(X) / test.php(O)
						<div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="eme_state">저장일</label></th>
					<td><?=$row['eme_date'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- epromotion_event_write -->
<script type="text/javascript">
<!--
	function epromotion_event_write_submit(f){
		<?php echo get_editor_js('eme_text'); ?>

			

		/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
		var onlynumber = onlynumber_text = "";
		var onlynumber_check = true;
		$('.onlynumber').each(function() {
			onlynumber = $(this).val();
			onlynumber_text = '숫자만 입력해주세요';
			if(onlynumber != ''){
				var pattern = /(^[0-9]+$)/;
				if (!pattern.test(onlynumber)) {
					alert(onlynumber_text);
					$(this).val('');
					$(this).focus();
					onlynumber_check = false;
					return false; 
				}
			}
		});
		if(onlynumber_check == false){ return false; }

		/* 검사 */
		switch(f.eme_event_type.value){
			case 't': if(f.eme_text.value == ""){alert('내용을 입력해주세요');return false;}
			break;

			case 'c': if(f.eme_event_no.value == ""){alert('코믹스을 선택해주세요');return false;}
			break;

			case 'e': if(f.eme_event_no.value == ""){alert('이벤트을 선택해주세요');return false;}
			break;
		}
		/* 날짜검사 */
		switch(f.eme_date_type.value){
			case 'y': 
				if(f.eme_date_start.value == ""){alert('시작일을 선택해주세요');return false;}
				if(f.eme_date_end.value == ""){alert('종료일을 선택해주세요');return false;}
			break;

		}
	}
//-->
</script>

<? include_once NM_ADM_PATH.'/clipboard.php'; // 복사 ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>