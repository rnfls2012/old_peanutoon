<?php
/**
 * Created by PhpStorm.
 * User: Ye Ji Wan
 * Date: 2018-06-28
 * Time: 오후 3:36
 */

use Classes\Push as PushServer;

include_once '_common.php'; // 공통
require_once NM_PATH.'/vendor/autoload.php'; // require composer autoload

$mb_token_arr = array();
$push_id = base_filter($_REQUEST['id']);

if ( isset($push_id) && $push_id != '' ) {

    // make new instance
    $url = "dev.nexcube.co.kr/push_server/public/put.php";
    $newInstance = new PushServer($url);

    // fetch push_notification_info DB
    $sql_select = "
        SELECT * FROM push_notification_info WHERE id = $push_id AND is_valid = 1
        ";

    $result = sql_query($sql_select);
    $row_data = sql_fetch_array($result);

    $push_msg_arr = array(
        'KEY_IDN'   => $row_data['id'],
        'TYPE'      => $row_data['type'],
        'TITLE'     => $row_data['title'],
        'CONTENT'  => $row_data['contents'],
        'EVENT'     => $row_data['event']
    );

    // check image file
    if ( isset($row_data['img_file_path']) && $row_data['img_file_path'] != '' ) {
        $img_url = img_url_para($row_data['img_file_path'], $row_data['reg_date']);
        $push_msg_arr['IMAGE_URL'] = $img_url;
    }

    // fetch member DB device token value
    $sql_mb_select = "
            SELECT DISTINCT mb_token 
            FROM member 
            WHERE mb_apk_is_count > 0 AND mb_device = '0' AND mb_push = '1' AND mb_state = 'y' AND mb_token IS NOT NULL
    ";

    $mb_result = sql_query($sql_mb_select);

    for ($i=0; $row_mb_data=sql_fetch_array($mb_result); $i++) {
        array_push($mb_token_arr, $row_mb_data['mb_token']);
    }
    
    $push_msg_arr['registration_ids'] = $mb_token_arr;

    try {
        if ($row_data['state'] == 'c') {
            echo "완료된 푸시입니다.";
        } else {
            $push_result = $newInstance->sendPushMsg($push_msg_arr);

            $sql_update = "
                UPDATE push_notification_info SET state = 's' WHERE id = $push_id;
            ";

            sql_query($sql_update);

            echo "메세지를 서버에 전달하였습니다.";
        }
    } catch (Exception $exception) {
        echo $exception->getMessage()." / ".$exception->getCode();
    }
}