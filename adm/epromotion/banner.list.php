<?
include_once '_common.php'; // 공통

/* PARAMITER 
emb_type //이벤트타입
emb_state //이벤트상태
emb_calc_way //이벤트 지급 계산법
emb_cash_point //이벤트 지급 코인
emb_point //이벤트 지급 보너스 코인
date_type // 날짜검색타입
s_date //이벤트시작일
e_date //이벤트마감일
emb_reg_date //등록일
emb_mod_date //수정일
*/

// 기본적으로 몇개 있는 지 체크;
$sql_epromotion_total = "select count(*) as total_epromotion from epromotion_banner where 1  ";
$total_epromotion = sql_count($sql_epromotion_total, 'total_epromotion');

/* 데이터 가져오기 */
$epromotion_banner_where = '';

// 검색단어+ 검색타입
if($_s_text){
	$epromotion_banner_where.= " and emb_name like '%$_s_text%' ";
}

// 상태
if($_emb_state){
	$epromotion_banner_where.= " and emb_state = '$_emb_state' ";
}

// 시작일/종료일 날짜
$start_date = $end_date = "";
if($_s_date && $_e_date){
	$start_date = $_s_date;
	$end_date = $_e_date;
}else if($_s_date){
	$start_date = $_s_date;
	$end_date = NM_TIME_YMD;
}
if($start_date && $end_date){
	$epromotion_banner_where.= "and ( emb_date_start >= '$start_date' and emb_date_end <= '$end_date') ";
}else{
	$_s_date = "";
	$_e_date = "";
}

// 정렬
if($_ordemb_field == null || $_ordemb_field == ""){ $_ordemb_field = "( CASE emb_state WHEN 'r' THEN 1 WHEN 'y' THEN 2 WHEN 'n' THEN 3 ELSE 4 END ), emb_date "; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$epromotion_banner_order = "order by ".$_ordemb_field." ".$_order;

$epromotion_banner_sql = "";
$epromotion_banner_field = " * "; // 가져올 필드 정하기
$epromotion_banner_limit = "";

$epromotion_banner_sql = "SELECT $epromotion_banner_field FROM epromotion_banner where 1 $epromotion_banner_where $epromotion_banner_order $epromotion_banner_limit";
$result = sql_query($epromotion_banner_sql);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','emb_no',1));
array_push($fetch_row, array('제목<br/>URL','emb_name',0));
array_push($fetch_row, array('위치','emb_position',0));

array_push($fetch_row, array('시작일','emb_date_start',1));
array_push($fetch_row, array('마감일','emb_date_end',1));
array_push($fetch_row, array('상태','emb_state',1));

array_push($fetch_row, array('Script사용<br/>오픈 타겟','emb_script',0));

array_push($fetch_row, array('성인여부','emb_adult',0));

array_push($fetch_row, array('관리','emb_management',0));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "배너";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> 이벤트 : 총 <?=number_format($total_epromotion);?> 건</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','epromotion_banner_wirte', <?=$popup_cms_width;?>, 880);"><?=$page_title?> 등록</button>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="epromotion_banner_search_form" id="epromotion_banner_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return epromotion_banner_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_date_btn">
					<?	tag_selects($d_state, "emb_state", $_emb_state, 'y', '상태 전체', ''); ?>
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" placeholder="배너명 입력해주세요" value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit banner_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="75" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- event_search -->

<section id="cms_btnlist">
	<ul>
		<li class="<?echo $_embc_position == ''?'current':'';?>"><a href="<?=$_cms_self?>">전체</a></li>
	<? foreach($d_banner_position as $d_banner_position_key => $d_banner_position_val){ 
		/* 구분 */
		$d_banner_class = get_eng($d_banner_position_val);
		$d_clear = $d_mgl = "";
		if($d_banner_position_key == 6){ $d_clear = "clear"; $d_mgl = "mgl52"; }
		?>
		<li class="<?echo $_embc_position == $d_banner_position_key?'current':'';?> <?=$d_banner_class?> <?=$d_clear?> <?=$d_mgl?>"><a href="<?=$_cms_self?>?embc_position=<?=$d_banner_position_key;?>"><?=$d_banner_position_val?></a></li>
	<? } ?>
	</ul>
</section><!-- cms_btnlist -->

<section id="epromotion_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$ordemb_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$ordemb_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&ordemb_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_ordemb_field){
						$th_title_giho = $ordemb_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?emb_no=".$dvalue_val['emb_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					// 기간시작
					$db_emb_date_start_ymd = get_ymd($dvalue_val['emb_date_start']);
					$db_emb_date_start_his = get_his($dvalue_val['emb_date_start']);

					// 기간마감
					$db_emb_date_end_ymd = get_ymd($dvalue_val['emb_date_end']);
					$db_emb_date_end_his = get_his($dvalue_val['emb_date_end']);


					$db_emb_no_date_text = "무기간";
					if(($dvalue_val['emb_date_start'] == '' && $dvalue_val['emb_date_end'] == '') && $dvalue_val['emb_date_type'] != 'n'){
						$db_emb_no_date_text = "시작일&마감일 없어서 종료됩니다.";
					}

					// 위치
					$db_position = array();
					$sql_position = "SELECT * FROM  `epromotion_banner_cover` WHERE  `embc_emb_no` = '".$dvalue_val['emb_no']."' ORDER BY embc_position";
					$result_position = sql_query($sql_position);
					while ($row_position = sql_fetch_array($result_position)) {
						array_push($db_position, $row_position);
					}

				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['emb_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['emb_name'];?><br/><?=$dvalue_val['emb_url'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center">
						<? foreach($db_position as $db_position_key => $db_position_val){
							$banner_position_arr = explode(": ", $nm_config['cf_banner_position'][$db_position_val['embc_position']]);
							$banner_position_text = $banner_position_arr[0];
							echo $banner_position_text;?><br/>
						<? } ?>
					</td>
					<?if($dvalue_val['emb_date_start'] != '' && $dvalue_val['emb_date_end'] != ''){?>
						<td class="<?=$fetch_row[3][1]?> text_center">
							<?=$dvalue_val['emb_date_start']?>일 <?=$dvalue_val['emb_hour_start'];?>시00분
						</td>
						<td class="<?=$fetch_row[4][1]?> text_center">
							<?=$dvalue_val['emb_date_end']?>일 <?=$dvalue_val['emb_hour_end'];?>시59분
						</td>
					<?}else{?>
						<td colspan='2' class="emb_date_no text_center"><?=$db_emb_no_date_text?></td>
					<?}?>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$d_state[$dvalue_val['emb_state']];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center">
						<?=$d_emb_script[$dvalue_val['emb_script']];?><br/>
						<?=$d_emb_target[$dvalue_val['emb_target']];?>
					</td>
					<td class="<?=$fetch_row[7][1]?> text_center">
						<?=$d_adult[$dvalue_val['emb_adult']];?>
					</td>
					<td class="<?=$fetch_row[8][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','epromotion_banner_mod_wirte', <?=$popup_cms_width;?>, 900);">수정</button>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','epromotion_banner_del_wirte', <?=$popup_cms_width;?>, 900);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- event_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>