<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode','eme_no');
array_push($para_list, 'eme_member','eme_member_idx');
array_push($para_list, 'eme_cover','eme_adult');
array_push($para_list, 'eme_date_type', 'eme_state','eme_date_end','eme_date_start');
array_push($para_list, 'eme_hour_start','eme_hour_end');
array_push($para_list, 'eme_holiday','eme_holiweek');
array_push($para_list, 'eme_event_type','eme_event_no');
array_push($para_list, 'eme_name','eme_text','eme_summary','eme_text_html','eme_date','eme_direct_url', 'eme_order');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'eme_no');
array_push($para_num_list, 'eme_event_no','eme_holiday','eme_holiweek');

/* 에디터 PARAMITER 체크 */
array_push($editor_list, 'eme_content');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'eme_event_no','eme_holiday','eme_holiweek', 'eme_order');
array_push($blank_list, 'eme_date_end','eme_date_start','eme_summary','eme_direct_url');

/* 빈칸 PARAMITER 시 NULL 처리 */
array_push($null_list, 'eme_event_no','eme_holiday','eme_holiweek','eme_summary','eme_direct_url', 'eme_order');

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* 에디터 PARAMITER 체크 */
array_push($editor_list, 'eme_text');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

// 상태와 날짜
$get_date_type = get_date_type($_eme_date_type, $_eme_date_start, $_eme_date_end, $_eme_hour_start, $_eme_hour_end);
$_eme_state = $get_date_type['state'];
$_eme_date_start = $get_date_type['date_start'];
$_eme_date_end = $get_date_type['date_end'];

/* 에디터 사용 */
$_eme_content_html = '1';

/* 로그인 아이디로 등록 */
$_eme_member = $nm_member['mb_no'];
$_eme_member_idx = $nm_member['mb_idx'];

$dbtable = "epromotion_event";
$dbt_primary = "eme_no";
$para_primary = "eme_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

if($_mode == 'reg'){	
	/* 고정값 */
	$_eme_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);

	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = '팝업이 등록되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = ' 등록 에러가 발생하여 저장되지 않았습니다.\n 우선순위를 확인하거나 관리자에게 문의 바랍니다.\n';
		$db_result['error'] = $sql_reg;
	}

/* 수정 */
}else if($_mode == 'mod'){
	/* 고정값 */
	$_eme_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";

		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $_eme_no.'의 데이터가 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = ' 수정 에러가 발생하여 저장되지 않았습니다.\n 우선순위를 확인하거나 관리자에게 문의 바랍니다.\n';
			$db_result['error'] = $sql_mod;
		}

	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 배너 이미지 삭제 */
		$row_eme = sql_fetch("select * from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
		kt_storage_delete($row_eme['eme_cover']);

		/* 데이터 삭제 */
		sql_query("delete from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
		
		$db_result['msg'] = $eme_name.'의 데이터가 삭제되였습니다.';
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/* 이미지 처리 */
if($_mode != 'del' && $db_result['state'] == 0){
	/* 등록이라면~ [ eventnum 이 NULL 이라면 ] */
	if($_eme_no == ''){
		$sql_max = 'select COALESCE(max('.$dbt_primary.'),0) as '.$dbt_primary.'_new from '.$dbtable; /* 방금 등록번호 가져오기 */
		$row_max = sql_fetch($sql_max);
		$_eme_no = $row_max[$dbt_primary.'_new'];
	}

	/* 이미지 경로 */

	/* 대분류 경로설정 - 폴더 체크 및 생성 */
	$path_cover = 'epromotion/event/';
	
	/* 업로드한 이미지 명 */
	$image_cover_tmp_name = $_FILES['eme_cover']['tmp_name'];
	$image_cover_name = $_FILES['eme_cover']['name'];
	
	/* 리사이징 안함 DB 저장 안함 */
	$_eme_cover_noresizing			= etc_filter(num_eng_check(tag_filter($_REQUEST['eme_cover_noresizing'])));

	/* 확장자 얻기 .png .jpg .gif */
	$image_cover_extension = substr($image_cover_name, strrpos($image_cover_name, "."), strlen($image_cover_name));

	/* 고정된 파일 명 */
	$image_cover_name_define = 'event_'.$_eme_no.strtolower($image_cover_extension);

	/* 저장 경로 및 DB 파일명 및 파일경로 저장 */
	$image_cover_src = $path_cover.$image_cover_name_define;
	
	/* 파일 업로드 */ 
	$image_cover_result = false;
	if ($image_cover_name != '') { 
		if($_eme_cover_noresizing == 'y'){
			$image_cover_result = kt_storage_upload($image_cover_tmp_name, $path_cover, $image_cover_name_define); 
			$image_cover_result = tn_set_key_thumbnail($image_cover_tmp_name, $path_cover, 'eme_cover' ,'not', '', $image_cover_name_define);	
			$image_cover_result = tn_upload_thumbnail($image_cover_tmp_name, $path_cover, $image_cover_name_define, 'eme_cover' ,'not', '', $image_cover_name_define);	
		}else{
			$image_cover_result = tn_set_key_thumbnail($image_cover_tmp_name, $path_cover, 'eme_cover' ,'all', '', $image_cover_name_define);
			$image_cover_result = tn_upload_thumbnail($image_cover_tmp_name, $path_cover, $image_cover_name_define, 'eme_cover' ,'all', '', $image_cover_name_define);
		}
	}
	rmdirAll(NM_THUMB_PATH.'/'.$path_cover); // kt storage - epromotion 디렉토리 삭제
	rmdirAll(NM_PATH.'/'.$path_cover); // kt storage - epromotion 디렉토리 삭제
	
	/* 임시파일이 존재하는 경우 삭제 */
	if (file_exists($image_cover_tmp_name) && is_file($image_cover_tmp_name)) {
		unlink($image_cover_tmp_name);
	}
	
	$sql_image = ' UPDATE '.$dbtable.' SET ';
	/* 이미지 field값 추가 */
	if ($image_cover_name != '' && $image_cover_result == true){ $sql_image.= ' eme_cover = "'.$image_cover_src.'" '; }
	$sql_image.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";

	if ($image_cover_name != '' && $image_cover_result == true){
		/* DB 저장 */
		if(sql_query($sql_image)){
			$db_result['msg'].= '\n'.$eme_name.'의 이미지가 저장되었습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'].= '\n'.'이미지 에러가 발생하여 저장되지 않았습니다.';
			$db_result['error'].= $sql_image;
		}
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
pop_close($db_result['msg'], '', $db_result['error']);
die;

?>