<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_embc_no = tag_filter($_REQUEST['embc_no']);

$sql = "SELECT * FROM epromotion_banner_cover embc 
				 JOIN epromotion_banner emb ON embc.embc_emb_no = emb.emb_no  WHERE  `embc_no` = '$_embc_no' ";

/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}

/* 배너 표지 이미지 */
if($row['embc_cover']!=''){ $embc_cover_src = NM_IMG.$row['embc_cover']; }
$embc_position_text = $nm_config['cf_banner_position'][$row['embc_position']];
$cf_bp_arr = explode(": ", $embc_position_text);
$cf_bp_name = $cf_bp_arr[0];
$cf_bp_explan = $cf_bp_arr[1];

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "배너";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- epromotion_event_head -->

<section id="epromotion_write">
	<form name="epromotion_banner_write_form" id="epromotion_banner_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_folder;?>/banner.position.write.update.php" onsubmit="return epromotion_banner_write_submit(this);">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="emb_no" name="embc_no" value="<?=$_embc_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->
		<input type="hidden" id="embc_emb_no" name="embc_emb_no" value="<?=$row['embc_emb_no'];?>"/><!-- 수정/삭제시 충전/지급 번호 -->
		<input type="hidden" id="embc_position" name="embc_position" value="<?=$row['embc_position'];?>"/><!-- 수정/삭제시 충전/지급 번호 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="embc_no">배너 번호</label></th>
					<td><?=$_embc_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="emb_name"><?=$required_arr[0];?>제목</label></th>
					<td>
						<?=$row['emb_name'];?>
					</td>
				</tr>
				<tr>
					<th><label for="emb_url"><?=$required_arr[0];?>링크주소</label></th>
					<td>
						<?=$row['emb_url'];?>
					</td>
				</tr>
				<tr>
					<th><label for="emb_adult"><?=$required_arr[0];?>배너 등급</label></th>
					<td>
						<?=$d_adult[$row['emb_adult']]?>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="emb_state">배너 상태</label></th>
					<td><?=$d_state[$row['emb_state']];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'del'){?>
				<tr>
					<th><label for="emb_cover"><?=$required_arr[0];?>배너 표지</label></th>
					<td>	
						<div class="file_cover">
							<input type="file" name="embc_cover" id="embc_cover" data-value="<?=$embc_cover_src?>" />
							<? if(cs_is_admin()){ ?>
								<input type="checkbox" name="embc_cover_noresizing" id="embc_cover_noresizing" value="y" class="noresizing" />
								<label for="embc_cover_noresizing">RESIZING-사용안함</label>
							<? } ?>
							<?if($row['embc_cover']!=''){?>
								<div class="embc_cover">
									<p><a href="#emb_cover" onclick="a_click_false();"><?=$cf_bp_name?> 보기</a></p>
									<p class="embc_cover_hide"><img src="<?=$embc_cover_src?>" alt="<?=$row['emb_name']."배너배너"?>" /></p>
								</div>
							<?}else{?>
								<p class="cover_explan file_name">
									<?=$cf_bp_name?>
								</p>
							<?}?>
							<p class="cover_explan">이미지 업로드 정보 : <?=$cf_bp_explan?> - 확장자:jpg, gif, png</p>
						</div>

					</td>
				</tr>
				<?}?>				

				<? if($_mode == "mod") { ?>
				<tr>
					<th><label for="cdn_purge">Purge<br/>이미지 파일경로 복사</label></th>
					<td>
						<textarea id="copy_text"><?= '/'.$row['embc_cover']; ?></textarea>
						<a class="copy">IMAGE COPY</a>
						<a class="purge" href="<?=NM_ADM_URL?>/cdn_purge.php" target="_blank">Purge 가기</a>
						<input type="hidden" id="copy_text_ajax"  value="<?= '/'.$row['embc_cover'].'||'; ?>"/>
						<a class="copy_btn" onclick="cdn_api_purge_send();">Purge 호출</a>
					</td>
				</tr>
				<?}?>

				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="embc_date">저장일</label></th>
					<td><?=$row['embc_date'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- epromotion_event_write -->
<script type="text/javascript">
<!--
	function epromotion_event_write_submit(f){

			

		/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
		var onlynumber = onlynumber_text = "";
		var onlynumber_check = true;
		$('.onlynumber').each(function() {
			onlynumber = $(this).val();
			onlynumber_text = '숫자만 입력해주세요';
			if(onlynumber != ''){
				var pattern = /(^[0-9]+$)/;
				if (!pattern.test(onlynumber)) {
					alert(onlynumber_text);
					$(this).val('');
					$(this).focus();
					onlynumber_check = false;
					return false; 
				}
			}
		});
		if(onlynumber_check == false){ return false; }

		/* 날짜검사 */
		switch(f.emb_date_type.value){
			case 'y': 
				if(f.emb_date_start.value == ""){alert('시작일을 선택해주세요');return false;}
				if(f.emb_date_end.value == ""){alert('종료일을 선택해주세요');return false;}
			break;

		}
	}
//-->
</script>

<? include_once NM_ADM_PATH.'/clipboard.php'; // 복사 ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>