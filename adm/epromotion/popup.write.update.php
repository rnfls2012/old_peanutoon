<?
include_once '_common.php'; // 공통
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode','emp_no');
array_push($para_list, 'emp_member','emp_member_idx');
array_push($para_list, 'emp_device','emp_disable_hours', 'emp_adult');
array_push($para_list, 'emp_state','emp_date_end','emp_date_start');
array_push($para_list, 'emp_hour_start','emp_hour_end');
array_push($para_list, 'emp_width_pc','emp_width_mo','emp_height');
array_push($para_list, 'emp_left','emp_top','emp_center');
array_push($para_list, 'emp_name','emp_text','emp_text_html','emp_date');
array_push($para_list, 'emp_exposure_referer','emp_exposure_mb_sns_type','emp_exposure_mb_sns_type_used');
array_push($para_list, 'emp_fold');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'emp_no','emp_disable_hours');
array_push($para_num_list, 'emp_left','emp_top','emp_height','emp_width');

/* 에디터 PARAMITER 체크 */
array_push($editor_list, 'emp_text');

/* 빈칸&숫자 PARAMITER 허용 */
array_push($null_int_list, 'emp_width_pc','emp_width_mo','emp_height');
array_push($null_int_list, 'emp_left','emp_top');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'emp_exposure_referer','emp_exposure_mb_sns_type');

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

// 기본값 없으면 강제 넣기(넓이/높이)
if(intval($_emp_width_pc) < 1){ $_emp_width_pc = 400;}
if(intval($_emp_width_mo) < 1){ $_emp_width_mo = 100;}

// 상태와 날짜
$get_date_type = get_date_type('y', $_emp_date_start, $_emp_date_end, $_emp_hour_start, $_emp_hour_end);
$_emp_state = $get_date_type['state'];
$_emp_date_start = $get_date_type['date_start'];
$_emp_date_end = $get_date_type['date_end'];

/* 에디터 사용 */
$_emp_content_html = '1';

/* 로그인 아이디로 등록 */
$_emp_member = $nm_member['mb_no'];
$_emp_member_idx = $nm_member['mb_idx'];

$dbtable = "epromotion_popup";
$dbt_primary = "emp_no";
$para_primary = "emp_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

/* 링크일 경우 우선 링크 안되게 막기(통계) */
// $_emp_text = str_replace("<a href=", "<a data-href=", $_emp_text);
// 18-04-20 추가
$_emp_text = str_replace("<a href=", "<a data-popuptitle=".$_emp_name." data-href=", $_emp_text);
$_emp_text = str_replace("<a data-href=", "<a data-popuptitle=".$_emp_name." data-href=", $_emp_text);

if($_mode == 'reg'){	
	/* 고정값 */
	$_emp_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);

	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = '팝업이 등록되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '등록 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

/* 수정 */
}else if($_mode == 'mod'){
	/* 고정값 */
	$_emp_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $_emp_no.'의 데이터가 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 데이터 삭제 */
		sql_query("delete from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
		
		$db_result['msg'] = $_emp_no.'의 데이터가 삭제되였습니다.';
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
pop_close($db_result['msg'], '', $db_result['error']);
die;

?>