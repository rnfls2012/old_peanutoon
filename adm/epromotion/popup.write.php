<?
include_once '_common.php'; // 공통
include_once (NM_EDITOR_LIB);

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_emp_no = tag_filter($_REQUEST['emp_no']);

$sql = "SELECT * FROM `epromotion_popup` WHERE  `emp_no` = '$_emp_no' ";
	
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
			 $row['emp_hour_end']=23; // 등록시 마감시간 23로 고정
			 $row['emp_disable_hours']=24; // 숨김시간 24로 고정
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}
/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "팝업";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
echo $editor_content_js;
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?> (팝업은 메인화면에서만 나옵니다.)</h1>
</section><!-- epromotion_popup_head -->

<section id="epromotion_write" class="editor_html">
	<form name="epromotion_popup_write_form" id="epromotion_popup_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return epromotion_popup_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="emp_no" name="emp_no" value="<?=$_emp_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="emp_no">팝업 번호</label></th>
					<td><?=$_emp_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="emp_device"><?=$required_arr[0];?>접속기기</label></th>
					<td><? tag_selects($d_emp_device, "emp_device", $row['emp_device'], 'n'); ?></td>
				</tr>
				<!--
				<tr>
					<th><label for="emp_disable_hours"><?=$required_arr[0];?>숨김시간</label></th>
					<td>
						<input type="text" class="onlynumber text_rt_100" <?=$required_arr[1];?>  placeholder="숫자만<?=$required_arr[2];?>" name="emp_disable_hours" id="emp_disable_hours" value="<?=intval($row['emp_disable_hours']);?>" autocomplete="off" maxlength="5" /> 
						<label for="emp_disable_hours">시간</label>
					</td>
				</tr>
				-->
				<tr>
					<th><label><?=$required_arr[0];?>팝업 기간</label></th>
					<td>
						<div class="date_type_view margin_top10">
							<p>
								<span for="s_date">시작일 :</span>
								<input type="text" class="s_date readonly" placeholder="클릭하세요" name="emp_date_start" id="s_date" value="<?=$row['emp_date_start'];?>" autocomplete="off" readonly />
								<?=set_hour('emp_hour_start', $row['emp_hour_start'], '시간 :', 'n');?>00분<br/>
								<input type="checkbox" name="s_date_chk" value="<?php echo NM_TIME_YMD; ?>" id="s_date_chk" onclick="if (this.checked == true) this.form.emp_date_start.value=this.form.s_date_chk.value; else this.form.emp_date_start.value = this.form.emp_date_start.defaultValue;">
								<label for="s_date_chk">시작일 오늘로</label>
							</p>
							
							<p>
								<span for="e_date">종료일 :</span>
								<input type="text" class="e_date readonly" placeholder="클릭하세요" name="emp_date_end" id="e_date" value="<?=$row['emp_date_end'];?>" autocomplete="off" readonly />
								<?=set_hour('emp_hour_end', $row['emp_hour_end'], '시간 :', 'n');?>59분<br/>
								<input type="checkbox" name="e_date_chk" value="<?php echo date("Y-m-d", NM_SERVER_TIME+(60*60*24*7)); ?>" id="e_date_chk" onclick="if (this.checked == true) this.form.emp_date_end.value=this.form.e_date_chk.value; else this.form.emp_date_end.value = this.form.emp_date_end.defaultValue;">
								<label for="e_date_chk">종료일 오늘로부터 7일 후로</label>
							<p>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="emp_adult"><?=$required_arr[0];?>팝업 등급</label></th>
					<td>
						<div id="emp_adult" class="input_btn">
							<input type="radio" name="emp_adult" id="emp_adultn" value="n" <?=get_checked('', $row['emp_adult']);?> <?=get_checked('n', $row['emp_adult']);?> />
							<label for="emp_adultn">전체</label>
							<input type="radio" name="emp_adult" id="emp_adulty" value="y" <?=get_checked('y', $row['emp_adult']);?> />
							<label for="emp_adulty">성인만</label>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="emp_fold"><?=$required_arr[0];?>잠시 접기 사용 기능 여부</label></th>
					<td>
						<div id="emp_fold" class="input_btn">
							<input type="radio" name="emp_fold" id="emp_foldn" value="n" <?=get_checked('', $row['emp_fold']);?> <?=get_checked('n', $row['emp_fold']);?> />
							<label for="emp_foldn">미사용</label>
							<input type="radio" name="emp_fold" id="emp_foldy" value="y" <?=get_checked('y', $row['emp_fold']);?> />
							<label for="emp_foldy">사용</label>
						</div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="emp_state">이벤트 상태</label></th>
					<td><?=$d_state[$row['emp_state']];?></td>
				</tr>
				<?}?>
				<tr>
					<th><label><?=$required_arr[0];?>팝업 넓이/높이</label></th>
					<td>
						<?
							$emp_width_pc = intval($row['emp_width_pc']);
							$emp_width_mo = intval($row['emp_width_mo']);

							if($emp_width_pc < 1){ $emp_width_pc = 400;}
							if($emp_width_mo < 1){ $emp_width_mo = 100;}
						?>
						<div class="emp_width_height margin_top10">
							<div class="margin_top10">
								<span for="emp_width">PC 넓이 :</span>
								<input type="text" class="onlynumber text_rt_100" placeholder="숫지만" name="emp_width_pc" id="emp_width_pc" value="<?=$emp_width_pc;?>" autocomplete="off" />px [400px(기본값)]
								<span for="emp_width">Mobile 넓이 :</span>
								<input type="text" class="onlynumber text_rt_100" placeholder="숫지만" name="emp_width_mo" id="emp_width_mo" value="<?=$emp_width_mo;?>" autocomplete="off" />% [100%(기본값)] 								
							</div> 
							<div class="margin_top10">
								<span for="emp_height">높이 :</span>
								<input type="text" class="onlynumber text_rt_100" placeholder="숫지만" name="emp_height" id="emp_height" value="<?=intval($row['emp_height']);?>" autocomplete="off" />px
								[0 일 경우 - auto]
							</div> 
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>팝업 위치<br/>메뉴아래부분에서 시작</label></th>
					<td>
						<div class="emp_left_top margin_top10">
							<div class="margin_top10">
								<span for="emp_top">TOP위치[가로](y축) :</span>
								<input type="text" class="onlynumber text_rt_100" placeholder="숫지만" name="emp_top" id="emp_top" value="<?=intval($row['emp_top']);?>" autocomplete="off" />
							</div> 
							<div class="margin_top10">
								<span for="emp_left">LEFT위치[세로](x축) :</span>
								<input type="text" class="onlynumber text_rt_100" placeholder="숫지만" name="emp_left" id="emp_left" value="<?=intval($row['emp_left']);?>" autocomplete="off" />
								[모바일경우 자동 - 0 px ]
							</div> 
						</div>
						<div id="emp_center" class="input_btn margin_top10">
							<input type="radio" name="emp_center" id="emp_centern" value="n" <?=get_checked('', $row['emp_center']);?>  <?=get_checked('n', $row['emp_center']);?> />
							<label for="emp_centern">위 설정 위치</label>
							<input type="radio" name="emp_center" id="emp_centery" value="y" <?=get_checked('y', $row['emp_center']);?> />
							<label for="emp_centery">위 설정 위치무시하고 중앙 위치</label>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="emp_exposure_referer">이전 페이지 조건 노출</label></th>
					<td>
						<p>
							이젠페이지 정보에서 다음과 같은 단어 있을시 노출이며, 빈칸 시 조건없이 노출로 처리됩니다.<br/>
							(구분 표기는 , 이며 띄어쓰기는 단어로 표기됩니다.)<br/>
							[예시 : facebook,naver,daum,cacao]
						</p>
						<div class="margin_top10">
							<input type="text" class="emp_exposure_referer" placeholder="이전 페이지노출 조건을 입력하세요"  name="emp_exposure_referer" id="emp_exposure_referer" value="<?=$row['emp_exposure_referer'];?>" autocomplete="off" />
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="emp_exposure_mb_sns_type">회원 조건 노출</label></th>
					<td>
						<div class="input_btn">
							<? tag_selects($d_mb_sns_type, "emp_exposure_mb_sns_type", $row['emp_exposure_mb_sns_type'], 'n'); ?>
							&nbsp;
							<input type="radio" name="emp_exposure_mb_sns_type_used" id="emp_exposure_mb_sns_type_usedn" value="n" <?=get_checked('', $row['emp_exposure_mb_sns_type_used']);?> <?=get_checked('n', $row['emp_exposure_mb_sns_type_used']);?> />
							<label for="emp_exposure_mb_sns_type_usedn">회원 조건 노출 사용안함</label>					
							<input type="radio" name="emp_exposure_mb_sns_type_used" id="emp_exposure_mb_sns_type_usedy" value="y" <?=get_checked('y', $row['emp_exposure_mb_sns_type_used']);?> />
							<label for="emp_exposure_mb_sns_type_usedy">회원 조건 노출 사용함</label>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="emp_name"><?=$required_arr[0];?>제목</label></th>
					<td>
						<input type="text" class="emp_name" placeholder="팝업제목을 입력하세요<?=$required_arr[2];?>"  name="emp_name" id="emp_name" value="<?=$row['emp_name'];?>" <?=$required_arr[1];?> autocomplete="off" />
					</td>
				</tr>
				<tr>
					<th><label for="emp_text">내용</label></th>
					<td><?php echo editor_html('emp_text', $row['emp_text']); ?></td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="emp_state">저장일</label></th>
					<td><?=$row['emp_date'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- epromotion_popup_write -->
<script type="text/javascript">
<!--
	function epromotion_popup_write_submit(f){
		<?php echo get_editor_js('emp_text'); ?>

			

		/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
		var onlynumber = onlynumber_text = "";
		var onlynumber_check = true;
		$('.onlynumber').each(function() {
			onlynumber = $(this).val();
			onlynumber_text = '숫자만 입력해주세요';
			if(onlynumber != ''){
				var pattern = /(^[0-9]+$)/;
				if (!pattern.test(onlynumber)) {
					alert(onlynumber_text);
					$(this).val('');
					$(this).focus();
					onlynumber_check = false;
					return false; 
				}
			}
		});
		if(onlynumber_check == false){ return false; }
	}
//-->
</script>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>