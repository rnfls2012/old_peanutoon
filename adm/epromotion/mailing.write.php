<?
include_once '_common.php'; // 공통
include_once (NM_EDITOR_LIB);

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "메일링";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
echo $editor_content_js;
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?></h1>
</section><!-- cms_page_title -->

<section id="epromotion_write" class="editor_html">
	<form name="epromotion_mailing_write_form" id="epromotion_mailing_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return epromotion_mailing_write_submit();">
		<input type="hidden" id="bn_member" name="bn_member" value="<?=$nm_member['mb_no'];?>"/><!-- 작성자 번호 -->
		<input type="hidden" id="bn_member_idx" name="bn_member_idx" value="<?=$nm_member['mb_idx'];?>"/><!-- 작성자 idx -->

		<table>
			<tbody>
				<tr>
					<th><label for="goto_url"><?=$required_arr[0]?>연결 URL</label></th>
					<td>
						<input type="text" placeholder="연결 URL을 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="goto_url" id="goto_url" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="utm_source"><?=$required_arr[0]?>UTM Source</label></th>
					<td>
						<input type="text" placeholder="UTM Source를 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="utm_source" id="utm_source" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="utm_medium"><?=$required_arr[0]?>UTM Medium</label></th>
					<td>
						<input type="text" placeholder="UTM Medium을 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="utm_medium" id="utm_medium" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="utm_campaign"><?=$required_arr[0]?>UTM Campaign</label></th>
					<td>
						<input type="text" placeholder="UTM Campaign을 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="utm_campaign" id="utm_campaign" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th>발신자</th>
					<td><input type="text" name="sender" value="피너툰"/></td>
				</tr>
				<tr>
					<th>발신자 메일</th>
					<td><input type="text" name='email' value="peanutoon@nexcube.co.kr"/></td>
				</tr>
				<tr>
					<th>수신자</th>
					<td>
						<input type="radio" name="mailing_target" id="mailing_target_all" value="all" checked="checked" />
						<label for="mailing_target_all">전체</label>
						<input type="radio" name="mailing_target" id="mailing_target_human" value="human" />
						<label for="mailing_target_human">휴면 전환</label>
						<input type="radio" name="mailing_target" id="mailing_target_1month" value="1month" />
						<label for="mailing_target_1month">가입 후 1개월</label>
						<input type="radio" name="mailing_target" id="mailing_target_2month" value="2month" />
						<label for="mailing_target_2month">가입 후 2개월</label>
						<input type="radio" name="mailing_target" id="mailing_target_3month" value="3month" />
						<label for="mailing_target_3month">가입 후 3개월</label>
					</td>
				</tr>
				<tr>
					<th><label for="subject"><?=$required_arr[0]?>제목</label></th>
					<td>
						<input type="text" placeholder="제목을 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="subject" id="subject" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="content"><?=$required_arr[0]?>내용</label></th>
					<td>
						<? echo editor_html('content', nl2br(stripslashes($row['content']))); ?>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="submit_btn">
						<input type="submit" value="발송">
						<input type="reset" value="취소" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
	 </form>
</section><!-- board_write -->
<script type="text/javascript">
<!--
	function epromotion_mailing_write_submit(f){
		<?php echo get_editor_js('content'); ?>

			

		/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
		var onlynumber = onlynumber_text = "";
		var onlynumber_check = true;
		$('.onlynumber').each(function() {
			onlynumber = $(this).val();
			onlynumber_text = '숫자만 입력해주세요';
			if(onlynumber != ''){
				var pattern = /(^[0-9]+$)/;
				if (!pattern.test(onlynumber)) {
					alert(onlynumber_text);
					$(this).val('');
					$(this).focus();
					onlynumber_check = false;
					return false; 
				}
			}
		});
		if(onlynumber_check == false){ return false; }
	}
//-->
</script>


<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>