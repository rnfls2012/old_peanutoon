<?
include_once '_common.php'; // 공통

/* PARAMITER */

// 기본적으로 몇개 있는 지 체크;
$sql_epromotion_total = "select count(*) as total_epromotion from epromotion_event where 1  ";
$total_epromotion = sql_count($sql_epromotion_total, 'total_epromotion');

/* 데이터 가져오기 */
$epromotion_event_where = '';

// 검색단어+ 검색타입
if($_eme_event_type){
	$epromotion_event_where.= " and eme_event_type = '$_eme_event_type' ";
}
// 검색단어+ 검색타입
if($_s_text){
	$epromotion_event_where.= " and eme_name like '%$_s_text%' ";
}


// 시작일/종료일 날짜
$start_date = $end_date = "";
if($_s_date && $_e_date){
	$start_date = $_s_date;
	$end_date = $_e_date;
}else if($_s_date){
	$start_date = $_s_date;
	$end_date = NM_TIME_YMD;
}
if($start_date && $end_date){
	$epromotion_event_where.= "and ( eme_date_start >= '$start_date' and eme_date_end <= '$end_date') ";
}else{
	$_s_date = "";
	$_e_date = "";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "( CASE eme_state WHEN 'r' THEN 1 WHEN 'y' THEN 2 WHEN 'n' THEN 3 ELSE 4 END ), eme_date "; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$epromotion_event_order = "order by ".$_order_field." ".$_order;

$epromotion_event_sql = "";
$epromotion_event_field = " * "; // 가져올 필드 정하기
$epromotion_event_limit = "";

$epromotion_event_sql = "SELECT $epromotion_event_field FROM epromotion_event where 1 $epromotion_event_where $epromotion_event_order $epromotion_event_limit";
$result = sql_query($epromotion_event_sql);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','eme_no',1));
array_push($fetch_row, array('커버','eme_cover',0));
array_push($fetch_row, array('제목','eme_name',1));
array_push($fetch_row, array('우선 순위','eme_order',1));
array_push($fetch_row, array('이벤트타입','eme_event_type',1));
array_push($fetch_row, array('이벤트등급','eme_adult',1));
array_push($fetch_row, array('상태','eme_state',1));
array_push($fetch_row, array('시작일','	eme_date_start',1));
array_push($fetch_row, array('마감일','eme_date_end',1));

array_push($fetch_row, array('관리','eme_management',0));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "이벤트";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> 이벤트 : 총 <?=number_format($total_epromotion);?> 건</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','epromotion_event_wirte', <?=$popup_cms_width;?>, 880);"><?=$page_title?> 등록</button>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="epromotion_event_search_form" id="epromotion_event_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return epromotion_event_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_date_btn">
					<?	tag_selects($d_eme_event_type, "eme_event_type", $_eme_event_type, 'y', '이벤트타입 전체', ''); ?>
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" placeholder="팝업명 입력해주세요" value="<?=$_s_text?>">
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit popup_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="75" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- event_search -->

<section id="epromotion_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$ordeme_giho = "▲";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$ordeme_giho = "▼";
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $ordeme_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 이미지 표지 */
					$image_bener_url = img_url_para($dvalue_val['eme_cover'], $dvalue_val['eme_date']);

					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?eme_no=".$dvalue_val['eme_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					// 기간시작
					$db_eme_date_start_ymd = get_ymd($dvalue_val['eme_date_start']);
					$db_eme_date_start_his = get_his($dvalue_val['eme_date_start']);

					// 기간마감
					$db_eme_date_end_ymd = get_ymd($dvalue_val['eme_date_end']);
					$db_eme_date_end_his = get_his($dvalue_val['eme_date_end']);


					$db_eme_no_date_text = "무기간";
					if(($dvalue_val['eme_date_start'] == '' || $dvalue_val['eme_date_end'] == '') && $dvalue_val['eme_date_type'] != 'n'){
						$db_eme_no_date_text = "시작일&마감일 없어서 종료됩니다.";
					}

					// 이벤트 링크
					$dvalue_val['eme_url'] = "";
					switch($dvalue_val['eme_event_type']) {
						case "t": $dvalue_val['eme_url'] = NM_URL."/eventview.php?event=".$dvalue_val['eme_no'];
						break;
						case "c": $dvalue_val['eme_url'] = NM_URL."/comics.php?comics=".$dvalue_val['eme_event_no'];
						break;
						case "e": $dvalue_val['eme_url'] = NM_URL."/eventlist.php?event=".$dvalue_val['eme_event_no'];
						break;
						case "u": 
								// HTTP 들어갈 경우
								if(strpos($dvalue_val['eme_direct_url'], '://') > 0){
									$dvalue_val['eme_url'] = $dvalue_val['eme_direct_url'];
								}else{
									if(substr($dvalue_val['eme_direct_url'], 0, 1) != '/'){
										$dvalue_val['eme_url'] = NM_URL.'/'.$dvalue_val['eme_direct_url'];
									}else{
										$dvalue_val['eme_url'] = NM_URL.$dvalue_val['eme_direct_url'];
									}
								}
						break;
					} // end switch
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['eme_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><img src="<?=$image_bener_url;?>" alt="<?=$dvalue_val['eme_name'];?>"/></td>
					<td class="<?=$fetch_row[2][1]?> text_center">
						<?=$dvalue_val['eme_name'];?><br/>
						<a href="<?=$dvalue_val['eme_url'];?>" target="_blank"><?=$dvalue_val['eme_url'];?></a>
					</td>
                    <td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['eme_order']?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$d_eme_event_type[$dvalue_val['eme_event_type']];?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$d_adult[$dvalue_val['eme_adult']];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$d_state[$dvalue_val['eme_state']];?></td>
					<?if($dvalue_val['eme_date_start'] != '' && $dvalue_val['eme_date_end'] != '' && $dvalue_val['eme_date_type'] != 'n'){?>
						<td class="<?=$fetch_row[7][1]?> text_center">
							<?=$dvalue_val['eme_date_start'];?>일 <?=$dvalue_val['eme_hour_start'];?>시00분
						</td>
						<td class="<?=$fetch_row[8][1]?> text_center">
							<?=$dvalue_val['eme_date_end'];?>일 <?=$dvalue_val['eme_hour_end'];?>시59분
						</td>
					<?}else{?>
						<td colspan='2' class="eme_date_no text_center"><?=$db_eme_no_date_text?></td>
					<?}?>
					<td class="<?=$fetch_row[9][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','epromotion_event_mod_wirte', <?=$popup_cms_width;?>, 900);">수정</button>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','epromotion_event_del_wirte', <?=$popup_cms_width;?>, 900);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- event_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>