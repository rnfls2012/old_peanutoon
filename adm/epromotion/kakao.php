<?
include_once '_common.php'; // 공통

/* =============================
       * SQL 조건 및 순서
 =============================== */

/* 데이터 가져오기 */
$send_kakaotalk_field       = " * "; // 가져올 필드 정하기
$send_kakaotalk_field_cnt   = " COUNT(*) as cnt"; // 가져올 필드 정하기

$send_kakaotalk_where = "";

// 시작일/종료일 날짜
$start_date = $end_date = "";

if ( $_s_date && $_e_date ){
    $start_date = $_s_date;
    $end_date = date("Y-m-d", strtotime($_e_date."+1day"));
} else if ( $_s_date ) {
    $start_date = $_s_date;
    $end_date = date("Y-m-d", strtotime(NM_TIME_YMD."+1day"));
}

if ( $start_date && $end_date ) {
    $send_kakaotalk_where.= "AND (z_sk_date >= '$start_date' AND z_sk_date < '$end_date') ";
} else {
    $_s_date = "";
    $_e_date = "";
}

// 정렬
$_order_field_add = $_order_add = "";
if ( $_order_field == null || $_order_field == "" ) {
    $_order_field = "z_sk_date";
}

if( $_order == null || $_order == "" ){
    $_order = "desc";
}

$sql_order = $_order_field." ".$_order;

/* 에러일경우 넘어오는 값이 0 이므로 IF 블록 실행 안됨               다시 */

if ( $_is_error == 'e' ) {
    $send_kakaotalk_where .= " AND z_sk_state_code = 0 ";
} else if ($_is_error =='s') {
    $send_kakaotalk_where .= " AND z_sk_error_code = 0 ";
} else {
	  $send_kakaotalk_where .= " ";
}

/* =============================
    * SQL문 작성 및 데이터 저장
 =============================== */

// 로그 체크
$sql_send_kakaotalk_total = "
    SELECT z_sk_temple_no, COUNT(*) AS total_kakao_cnt
    FROM z_send_kakaotalk 
    WHERE 1 $send_kakaotalk_where
    GROUP BY z_sk_temple_no
    ";

$total_result = sql_query($sql_send_kakaotalk_total);

$total_cnt = 0;
$total_arr = array();
while ( $total_row = sql_fetch_array($total_result) ) {
    $total_cnt += $total_row['total_kakao_cnt'];
    array_push($total_arr, $total_row);
}

// 전체 데이터 COUNT
$send_kakaotalk_cnt = "
  SELECT $send_kakaotalk_field_cnt 
  FROM z_send_kakaotalk 
  WHERE 1 $send_kakaotalk_where 
  ORDER BY $sql_order 
  ";

// 전체 데이터
$send_kakaotalk_sql = "
  SELECT $send_kakaotalk_field 
  FROM z_send_kakaotalk z_kt
  LEFT JOIN member mb ON z_kt.z_sk_mb_no = mb.mb_no 
  WHERE 1 $send_kakaotalk_where 
  ORDER BY $sql_order
  ";

$rows_cnts = rows_cnts($send_kakaotalk_cnt);
$rows_data = rows_data($send_kakaotalk_sql);

/* ==============================
      데이터 출력위한 배열 선언
 ================================*/
/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호', 'z_sk_no', 0));
array_push($fetch_row, array('회원번호', 'z_sk_mb_no', 0));
array_push($fetch_row, array('회원 ID', 'mb_id', 0));
array_push($fetch_row, array('템플릿 번호(용도)', 'z_sk_temple_no', 0));
array_push($fetch_row, array('상태코드', 'z_sk_state_code', 0));
array_push($fetch_row, array('에러코드', 'z_sk_error_code', 0));
array_push($fetch_row, array('에러메세지', 'z_sk_error_msg', 0));
array_push($fetch_row, array('날짜', 'z_sk_date', 0));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "알림톡";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
    <link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
    <script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

    <section id="cms_title">
        <h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
        <div>
            <strong><?=$page_title?> 검색 수 : 총 <?=$total_cnt?> 건 </strong>
        </div>
    </section><!-- cms_title -->

    <section id="cms_search">
        <h2 class="hidden"><?=$cms_head_title?> 검색</h2>
        <form name="send_kakao_search_form" id="send_kakao_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return;">
            <div class="cs_bg">
                <div class="cs_form">
                    <div class="cs_date_btn">
                        <?	tag_selects($d_is_error, "is_error", $_is_error, 'y', '에러/정상'); ?>
                        <?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
                    </div>
                    <? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
                </div>
                <div class="cs_submit popup_submit">
                    <input type="submit" class="<?=$cs_submit_h;?>" data-base_h="75" data-on_h="130"  value="검색" id="cms_submit">
                </div>
            </div><!-- cs_bg -->
        </form>
    </section><!-- event_search -->

    <section id="epromotion_result">
        <h3>검색 리스트
            <ul>
                <li>에러코드가 500 일 경우 해당 API 회사 측 오류입니다.</li>
            </ul>
            <table>
                <tr>
                    <th class="topleft">구분</th>
                    <?php
                    foreach ( $total_arr as $total_val ) {
                        $row_kt = select_template($total_val['z_sk_temple_no']);
                        ?>
                        <th><?=$row_kt['kt_temp_usage']?></th>
                    <? } ?>
                    <th class="topright">합계</th>
                </tr>
                <tr>
                    <td class="btmleft">톡 요청 수</td>
                    <?php
                    $sum_cnt = 0;
                    foreach ( $total_arr as $total_val ) {
                        $sum_cnt += $total_val['total_kakao_cnt'];
                        ?>
                        <td><?=$total_val['total_kakao_cnt']?> 건</td>
                    <? } ?>
                    <td class="btmright"><?=$sum_cnt?> 건</td>
                </tr>
            </table>
        </h3>
        <div id="cr_bg">
            <table>
                <thead id="cr_thead">
                <tr>
                    <?
                    //정렬표기
                    $ordemp_giho = "▼";
                    $th_order = "desc";
                    if($_order == 'desc'){
                        $ordemp_giho = "▲";
                        $th_order = "asc";
                    }
                    foreach($fetch_row as $fetch_key => $fetch_val){

                        $th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
                        if($fetch_val[2] == 1){
                            $th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&ordemp_field='.$fetch_val[1].'&order='.$th_order.'">';
                            $th_ahref_e = '</a>';
                        }
                        if($fetch_val[1] == $_ordemp_field){
                            $th_title_giho = $ordemp_giho;
                        }
                        $th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
                        $th_class = $fetch_val[1];
                        ?>
                        <th class="<?=$th_class?>"><?=$th_title?></th>
                    <?}?>
                </tr>
                </thead>
                <tbody>
                <?
                foreach($rows_data as $dvalue_key => $dvalue_val){
                    $row_kt = select_template($dvalue_val['z_sk_temple_no']);
                    ?>
                    <tr class="result_hover">
                        <td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val[$fetch_row[0][1]]?></td>
                        <td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val[$fetch_row[1][1]]?></td>
                        <td class="<?=$fetch_row[2][1]?> text_center">
                            <a href="#" onclick="popup('<?=NM_ADM_URL;?>/members/member_view.php?mb_id=<?=$dvalue_val[$fetch_row[2][1]]?>','member_view', <?=$popup_cms_width?>, 550);">
                                <?=$dvalue_val[$fetch_row[2][1]]?>
                            </a>
                        </td>
                        <td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val[$fetch_row[3][1]]?>(<?=$row_kt['kt_temp_usage']?>)</td>
                        <td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val[$fetch_row[4][1]]?></td>
                        <td class="<?=$fetch_row[5][1]?> text_center"><?=$dvalue_val[$fetch_row[5][1]]?></td>
                        <td class="<?=$fetch_row[6][1]?> text_center"><?=$dvalue_val[$fetch_row[6][1]]?></td>
                        <td class="<?=$fetch_row[7][1]?> text_center"><?=$dvalue_val[$fetch_row[7][1]]?></td>
                    </tr>
                <?}?>
                </tbody>
            </table>
        </div>
    </section><!-- event_result -->

<? rows_page(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>