<?PHP
include_once '_common.php'; // 공통
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

$mailing_target = tag_filter($_REQUEST['mailing_target']); // 메일링 타겟 human: 휴면 안내, 1month: 가입 후 1개월, 2month: 가입 후 2개월, 3month: 가입 후 3개월
$goto_url = tag_filter($_REQUEST['goto_url']); // 연결 URL
$utm_source = tag_filter($_REQUEST['utm_source']); // UTM Source
$utm_medium = tag_filter($_REQUEST['utm_medium']); // UTM Medium
$utm_campaign = tag_filter($_REQUEST['utm_campaign']); // UTM Campaign

$utm_para = gtm_utm($utm_source, $utm_medium, $utm_campaign); // UTM QueryString
$goto_url = cs_para_attach(NM_REAL_URL."/".$goto_url, $utm_para); // 최종 URL

if($mailing_target == "human") { // 휴면 전환 메일
	$cmp_date = date("Y-m-d", strtotime("-1 year")); // 오늘로 부터 1년 전인 날짜를 저장
	$cmp_date_3 = date("Y-m-d", strtotime("-3 year")); // 오늘로 부터 3년 전인 날짜를 저장
	$cmp_date_5 = date("Y-m-d", strtotime("-5 year")); // 오늘로 부터 5년 전인 날짜를 저장
		
	$target_sql = "SELECT * FROM member WHERE 1 AND (mb_state='y' AND mb_state_sub='y') AND ((mb_login_today<'$cmp_date' AND mb_login_date<'$cmp_date' AND mb_human_prevent='') OR (mb_login_today<'$cmp_date_3' AND mb_login_date<'$cmp_date_3' AND mb_human_prevent<=3) OR (mb_login_today<'$cmp_date_5' AND mb_login_date<'$cmp_date_5' AND mb_human_prevent<=5)) AND mb_human_prevent!='out'"; // 탈퇴가 아닌 상태에서, 정상 계정을 의미
} else if($mailing_target == "all") {
	// $target_sql = "SELECT * FROM member WHERE mb_class = 'a' AND (mb_level = 29 OR mb_level = 30) AND mb_no NOT IN(846263, 896607)";
	$target_sql = "SELECT * FROM member WHERE mb_email REGEXP '^[a-zA-Z0-9][a-zA-Z0-9._-]*[a-zA-Z0-9._-]@[a-zA-Z0-9][a-zA-Z0-9._-]*[a-zA-Z0-9]\\.[a-zA-Z]{2,10}$' AND mb_post = 'y' AND mb_state = 'y' AND mb_state_sub != 's'  AND mb_email != '';"; // 메일 정규화 한 상태에서 수신 동의한 모두에게 전송
} else { // 그 외 가입기간 타겟
	switch($mailing_target) {
		case "1month" :
			$cmp_date = date("Y-m-d", strtotime("-1 month")); // 한달 전
			break;

		case "2month" :
			$cmp_date = date("Y-m-d", strtotime("-2 month")); // 두달 전
			break;

		case "3month" :
			$cmp_date = date("Y-m-d", strtotime("-3 month")); // 세달 전
			break;
	} // end switch

	$target_sql = "SELECT * FROM member WHERE 1 AND (mb_state='y' AND mb_state_sub='y' AND mb_post='y') AND (mb_join_date<='$cmp_date' AND mb_login_today<='$cmp_date' AND mb_login_date<='$cmp_date')"; // 가입하고 그 뒤로 한 번도 접속하지 않은 회원들
} // end else

$target_sql = "SELECT * FROM member WHERE mb_join_date >= '2017-12-01' AND mb_join_date < '2018-02-01' AND mb_state = 'y' AND mb_state_sub = 'y' AND mb_email != '' AND mb_email REGEXP '^[a-zA-Z0-9][a-zA-Z0-9._-]*[a-zA-Z0-9._-]@[a-zA-Z0-9][a-zA-Z0-9._-]*[a-zA-Z0-9]\\.[a-zA-Z]{2,10}$'";
// $target_sql = "SELECT * FROM member WHERE mb_class = 'a' AND (mb_level = 29 OR mb_level = 30) AND mb_no NOT IN(846263, 896607)";
/******************** 메일 타겟 설정(receiverlist 설정) ********************/
$target_result = sql_query($target_sql);
$result_arr = array();
while($target_row = sql_fetch_array($target_result)) {
	$target_arr = array($target_row['mb_name'] == ""?$target_row['mb_id']:$target_row['mb_name'], $target_row['mb_email']);
	array_push($result_arr, $target_arr);
} // end while

$receiverlist = ""; // receiverlist 초기화
$result_count = count($result_arr); // 검색된 개수

/* 10개 미만/30,000개 이상 발송 안 됨 */
if($result_count >= 30000) {
	echo "30000명 이상!";
	die;
} // end if

foreach($result_arr as $result_key => $result_val) {
	$receiverlist .= $result_val[0].",".$result_val[1];
	if(($result_count - 1) != $result_key) { // 줄바꿈으로 구분해야 함.
		$receiverlist .= "\n";
	} // end if
} // end foreach
$receiverlist .= "이익희, leeih@nexcube.co.kr";
// 임시로 끼워넣기 나중에 지우시오!!!!
/*
$receiverlist .= "김선규, ingaha@nexcube.co.kr";
$receiverlist .= "\n";
$receiverlist .= "이익희, leeih@nexcube.co.kr";
$receiverlist .= "\n";
$receiverlist .= "예지완, rnfls2012@nexcube.co.kr";
*/

/******************** 인증정보 ********************/
// 대량메일 인증 관련
$sendmail_url = "https://nexcube.sendmail.cafe24.com/sendmail_api.php"; // 전송요청 URL (필수)
$secureKey = "a3825156fe8e163856e038ada6d1e4b5"; // 인증키 (필수)
$userId = "nexcube"; // 발송자ID (필수)

/******************** 요청변수 처리 ********************/
// 메일발송 관련
$sender = ($_POST['sender']) ? $_POST['sender'] : ''; // 발송자 이름 (필수)
$email = ($_POST['email']) ? $_POST['email'] : ''; // 발송자 이메일 (필수)
// $receiverlist = ($_POST['receiverlist']) ? $_POST['receiverlist'] : '';  수신자 리스트 (필수) 위에서 미리 처리함.
$receiverlistUrl= ($_POST['receiverlistUrl']) ? $_POST['receiverlistUrl'] : ''; // 수신자 리스트 URL

// 메일내용 관련
$subject = ($_POST['subject']) ? $_POST['subject'] : ''; // 메일 제목
/*
$content .= ($_POST['content']) ? $_POST['content'] : ''; // 메일 내용
*/
// 임시 메일 내용
$content = "<a href='".$goto_url."'>"; 
$content .= "<img src='".NM_IMG."coupon3.png'/>";
$content .= "</a>";

// 수신자 처리 관련
$rejectType = ($_POST['rejectType']) ? $_POST['rejectType'] : 2; // 수신거부자 발송여부(2: 제외발송, 3:포함발송)
$overlapType = 2;

// 예약발송 관련
$sendType = ($_POST['sendType']) ? $_POST['sendType'] : 0; // 예약발송 여부(0:즉시발송, 1:예약발송)
$sendDate = ($_POST['sendDate']) ? $_POST['sendDate'] : ''; // 예약발송 시간(년-월-일 시:분:초)

// 파일첨부 관련
$file_name = $_FILES['addfile']['name'];
$tmp_name = $_FILES['addfile']['tmp_name'];
$content_type = $_FILES['addfile']['type'];

// 수신거부 기능 관련
$useRejectMemo = ($_POST['useRejectMemo']) ? $_POST['useRejectMemo'] : 0; // 수신거부 사용여부(0: 사용안함, 1: 사용)

// 메일주소 중복발송 관련
$overlapType = $_POST['overlapType'] == '1' ? '1' : '2';

// 요청 테스트
$testFlag = ($_POST['testFlag']) ? $_POST['testFlag'] : 0; // 요청 테스트 사용여부(0: 사용안함, 1: 사용)

/******************** 요청변수 처리 ********************/
$mail['secureKey'] = $secureKey;
$mail['userId'] = $userId;
$mail['sender'] = base64_encode($sender);
$mail['email'] = base64_encode($email);
$mail['receiverlist'] = base64_encode($receiverlist);
$mail['receiverlistUrl'] = base64_encode($receiverlistUrl);
$mail['subject'] = base64_encode($subject);
$mail['content'] = base64_encode($content);
$mail['rejectType'] = $rejectType;
$mail['overlapType'] = $overlapType;
$mail['sendType'] = $sendType;
$mail['sendDate'] = $sendDate;
$mail['useRejectMemo'] = $useRejectMemo;
$mail['testFlag'] = $testFlag;

$host_info = explode("/", $sendmail_url);
$host = $host_info[2];
$path = $host_info[3]."/".$host_info[4];

srand((double)microtime()*1000000);
$boundary = "---------------------".substr(md5(rand(0,32000)),0,10);

// 헤더 생성
$header = "POST /".$path ." HTTP/1.0\r\n";
$header .= "Host: ".$host."\r\n";
$header .= "Content-type: multipart/form-data, boundary=".$boundary."\r\n";

// 본문 생성
foreach($mail AS $index => $value){
	$data .="--$boundary\r\n";
	$data .= "Content-Disposition: form-data; name=\"".$index."\"\r\n";
	$data .= "\r\n".$value."\r\n";
	$data .="--$boundary\r\n";
}

// 첨부파일
if (is_uploaded_file($_FILES['addfile']['tmp_name'])) { 
	$data .= "--$boundary\r\n";
	$content_file = join("", file($tmp_name));
	$data .="Content-Disposition: form-data; name=\"addfile\"; filename=\"".$file_name."\"\r\n";
	$data .= "Content-Type: $content_type\r\n\r\n";
	$data .= "".$content_file."\r\n";
	$data .="--$boundary--\r\n";
}
$header .= "Content-length: " . strlen($data) . "\r\n\r\n";

$fp = fsockopen($host, 80);

if ($fp) { 
	fputs($fp, $header.$data);

	$rsp = '';
    while(!feof($fp)) { 
		$rsp .= fgets($fp,8192); 
	}	

	fclose($fp);

	$msg = explode("\r\n\r\n",trim($rsp));
	/*
	CALLBACK LIST
	SUCCESS : 발송 성공
	TEST SUCCESS : 테스트 요청 성공
	101 : 인증키 공백 오류
	102 : 발송자 ID 공백 오류
	103 : 발송자 이름 공백 오류
	104 : 발송자 이메일 공백 오류
	105 : 수신자 리스트 공백 오류 or 수신자 리스트 URL 공백 오류
	106 : 메일 제목 공백 오류
	107 : 메일 내용 공백 오류
	108 : 예약발송 시간 오류(발송요청 시간이 현재보다 과거이거나 7일 이상인 경우)
	109 : 사용자 인증 실패 오류
	202 : 첨부파일 용량이 2MB 를 넘었을때 발생하는 오류
	203	: 첨부파일이 첨부할 수 없는 파일 확장자일때 발생하는 오류
	301 : 수신자 리스트 발송통수가 3만통이 넘을때 발생하는 오류
	302	: 잔여건수가 메일 발송수보다 적을때 발생하는 오류
	303 : 수신자 리스트 URL 발송통수가 10만통이 넘었을때 발생하는 오류
	304	: 동일 IP 로 10건 이상 API 요청 시 5분 이내 동일 IP 로 API 요청 시 오류
	305	: 대량발송 API 사용 때 10통 미만 요청 시 발생하는 오류

	echo $msg[1]; // CALLBACK MSG
	*/

	// 휴면 대상자 메일일때, 휴면 대기 전환
	if($mailing_target == "human") {
		if($msg[1] == "SUCCESS") {
			$send_date = NM_TIME_YMDHIS;
			while($target_row = sql_fetch_array($target_result)) {
				$stand_by_sql = "UPDATE member SET mb_state_sub = 't', mb_human_email_date = '".$send_date."' WHERE mb_no = '".$target_row['mb_no']."' AND mb_idx = '".$target_row['mb_idx']."'";
				sql_query($stand_by_sql);
			} // end while
		} else {
			echo $msg[1];
		} // end else
	} else {
		echo $mailing_target;
		echo $msg[1];
	} // end else
}
else {
	echo "Connection Failed";
}
?>