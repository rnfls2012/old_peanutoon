<?php
include_once '_common.php'; // 공통

mb_partner_acceess($nm_member); // 파트너 접근 제한

include_once NM_ADM_PATH.'/_adm.lib.php'; // adm 메뉴
include_once NM_ADM_PATH.'/_head.current.php'; // 현재 위치

$steps1_link_boolean = false;
foreach($menu as $menu_key => $menu_val){
	/* 링크로 연동 */
	$steps1_class = $menu_val[0][2];	// 클라스명
	$steps1_link = $menu_val[0][1];		// 링크URL

	if(is_array($menu_val[1][3])){
		$steps1_link = $menu_val[1][3][1];
	}else{
		$steps1_link = $menu_val[1][1];
	}

	$mb_cms_level_access = mb_cms_level_access($nm_member, $menu_val[0][2], 'y');
	if($mb_cms_level_access == false){ continue; }

	/* 하위폴더 링크로 연동 */	
	foreach($menu_val as $menu_key2 => $menu_val2){ if($menu_key2 == 0){continue;} 
		if(is_array($menu_val2[3])){
			$steps1_class = $menu_val2[2];		// 클라스명
			$steps1_link = $menu_val2[3][1];	// 링크URL

			$mb_cms_level_access2 = mb_cms_level_access($nm_member, $menu_val2[2], 'y');
			if($mb_cms_level_access2 == false){ continue; }
			else{ break; }

			// echo $menu_val2[0]."-".$menu_val2[2]."<br/>".$menu_val2[3][1]."<br/><br/>";
		}

	}
	// echo $steps1_link; die;
	$steps1_link_boolean = true;
	goto_url($steps1_link);
}
if($steps1_link_boolean == false){
	alert("접근 권한이 없습니다.",NM_URL);
}

// print_r($nm_member);
// print_r($cfmb_level_permission_arr);

?>