<?
/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','mkt_date'));
array_push($fetch_row, array('요일','mkt_week'));

foreach($mkt_arr as $mkt_key => $mkt_val) {
	if($mkt_val != "") {
		array_push($fetch_row, array($mkt_val['mkt_title'], "mkt_no".$mkt_val['mkt_no']."_sum", $mkt_val['mkt_type']));
	} // end if
} // end foreach

$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('center');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// merge 포맷 추가
$merge_1 = $workbook -> addformat();
$merge_1 -> set_align('center');
$merge_1 -> set_border('1');
$merge_1 -> set_fg_color(34);
$merge_1 -> set_bold(1);
$merge_1 -> set_merge();

$merge_2 = $workbook -> addformat();
$merge_2 -> set_align('center');
$merge_2 -> set_border('1');
$merge_2 -> set_fg_color(34);
$merge_2 -> set_bold(1);
$merge_2 -> set_merge();

// 해당 시트 당 컬럼 크기 변경
$worksheet->set_column(0, 0, 10); // 날짜
$worksheet->set_column(1, 1, 10); // 요일
$worksheet->set_column(2, count($fetch_row)-1, 15); // 요일

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));

$center  = $workbook -> addformat(array(
								'align' => 'center',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 34
								));

$in_mkt = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 45
								));

$out_mkt = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 50
								));

$sns_mkt = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));

//문서 출력일자 셀 출력
$worksheet->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet->write(1, 1 , iconv_cp949(date("Y-m-d")));
$worksheet->write_blank(2, 0 , $in_mkt);
$worksheet->write(2, 1 , iconv_cp949("내부 이벤트"), $date_format);
$worksheet->write_blank(3, 0 , $out_mkt);
$worksheet->write(3, 1 , iconv_cp949("외부 이벤트"), $date_format);
$worksheet->write_blank(4, 0 , $sns_mkt);
$worksheet->write(4, 1 , iconv_cp949("SNS 이벤트"), $date_format);

$row_cnt = 6; //합계 데이터 출력 행 cnt.

/***************  본문 결과 출력   **************/

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	$temp_head_arr = array($val[0], $val[2]);
	array_push($data_head, $temp_head_arr);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
	${$val[1]."_sum"} = 0;
} // end foreach

/* 컬럼 이름 지정 부분 */
$col = 0;

foreach($data_head as $cell => $cell_val) {
	switch($cell_val[1]) {
		case "i" :
			$worksheet->write($row_cnt, $col++, iconv_cp949($cell_val[0]), $in_mkt);
			break;

		case "o" :
			$worksheet->write($row_cnt, $col++, iconv_cp949($cell_val[0]), $out_mkt);
			break;

		case "s" :
			$worksheet->write($row_cnt, $col++, iconv_cp949($cell_val[0]), $sns_mkt);
			break;

		default :
			$worksheet->write($row_cnt, $col++, iconv_cp949($cell_val[0]), $heading);
			break;
	} // end switch
} // end foreach

$row_cnt++;

/* 데이터 전달 부분 */
foreach($rows_data as $rows_key => $rows_val) {
	foreach($data_key as $cell_key => $cell_val) {
		switch($cell_val) {
			case "mkt_date" :
				$mkt_date = $rows_val[$mkt_year_month_str]."-".$rows_val[$mkt_day_str];
				$worksheet->write($row_cnt, $cell_key , iconv_cp949($mkt_date), $center);
				break;
				
			case "mkt_week" :
				$mkt_week = get_yoil($rows_val[$mkt_week_str], 1, 1);
				$worksheet->write($row_cnt, $cell_key , iconv_cp949($mkt_week), $center);
				break;

			default :
				$worksheet->write($row_cnt, $cell_key , iconv_cp949($rows_val[$cell_val]), $f_price);
				${$cell_val."_sum"} += $rows_val[$cell_val];
				break;
		} // end switch
	} // end foreach
	$row_cnt++;
} // end foreach

$row_cnt++;

$worksheet -> write($row_cnt, 0, iconv_cp949("합계"), $merge_1);
$worksheet -> write_blank($row_cnt, 1, $merge_2);
foreach($fetch_row as $key => $val) {
	if($key >= 2) {
		$worksheet -> write($row_cnt, $key, iconv_cp949(${$val[1]."_sum"}), $f_price);
	} // end if
} // end foreach

$workbook->close();

header("Content-Disposition: attachment;filename=".$mkt_file_name.$today.".xls");
header("Content-Type: application/x-msexcel; name=\"".$mkt_file_name.$today.".xls");
header("Content-Disposition: inline; filename=\"".$mkt_file_name.$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);

die;
?>