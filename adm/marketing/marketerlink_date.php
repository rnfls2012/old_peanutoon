<?
include_once '_common.php'; // 공통

/* PARAMITER 
bh_no // 글 번호
*/
$ver = "v=".substr(NM_VERSION,1,1);

$sql_where = " AND mktl_no='".$mktl_no."' ";

// 기본적으로 몇개 있는 지 체크;
$sql_total = "select count(*) as total_stats_marketer_link from stats_marketer_link where 1 ".$sql_where."  ";
$sql_count = sql_count($sql_total, 'total_stats_marketer_link');


// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "slmktl_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$sql_order = "order by ".$_order_field." ".$_order;

$sql_help = "";
$field_help = " * "; // 가져올 필드 정하기
$limit_help = "";

$sql= "select $field_help FROM stats_marketer_link where 1 $sql_where $sql_order $limit_help";
$result = sql_query($sql);
$row_size = sql_num_rows($result);


// 업체 리스트
$sql_mktl = "select * from marketer_link where mktl_no='".$mktl_no."'  ";
$row_mktl = sql_fetch($sql_mktl);

$sql_mkt = "select * from marketer where mkt_no='".$row_mktl['mkt_no']."'  ";
$row_mkt = sql_fetch($sql_mkt);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','slmktl_no',0));
array_push($fetch_row, array('날짜','slmktl_date',0));
array_push($fetch_row, array('시간','slmktl_time',0));
array_push($fetch_row, array('클릭수','slmktl_click',0));

$page_title = "마케터링크-".$row_mkt['mkt_title']."-".$mktl_no;
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>총 <?=$page_title?> : <?=number_format($sql_count);?> 개</strong>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="help_search_form" id="help_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return help_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<? tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
				</div>
			</div>
			<div class="cs_submit board_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="marketer_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $cp_css.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
				?>
				<tr class="result_hover">
					<td class="<?=$cp_css.$fetch_row[0][1]?> text_center"><?=$dvalue_val['slmktl_no'];?></td>
					<td class="<?=$cp_css.$fetch_row[1][1]?> text_center"><?=$dvalue_val['slmktl_year_month'];?>-<?=$dvalue_val['slmktl_day'];?></td>
					<td class="<?=$cp_css.$fetch_row[2][1]?> text_center"><?=$dvalue_val['slmktl_hour'];?></td>
					<td class="<?=$cp_css.$fetch_row[3][1]?> text_center"><?=$dvalue_val['slmktl_click'];?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- board_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>