<?
include_once '_common.php'; // 공통

/* PARAMITER 
bh_no // 글 번호
*/

// 캠페인
$d_tksk_campaign = array();
$sql_tksk = "select * from ticketsocket order by tksk_no";
$result_tksk = sql_query($sql_tksk);
while ($row_tksk = sql_fetch_array($result_tksk)) {
	$d_tksk_campaign[$row_tksk['tksk_campaign']] = $row_tksk['tksk_campaign'];
	if(intval($row_tksk['tksk_campaign_complete']) >0){
		$d_tksk_campaign[$row_tksk['tksk_campaign_complete']] = $row_tksk['tksk_campaign_complete'];
	}
}

// 랜딩페이지
$d_tksk_page = array();
$sql_z_tksk = "select * from z_ticketsocket group by z_tksk_page";
$result_z_tksk = sql_query($sql_z_tksk);
while ($row_z_tksk = sql_fetch_array($result_z_tksk)) {
	$d_z_tksk_page_kr = "";
	switch($row_z_tksk['z_tksk_page']){
		case "index":				$d_z_tksk_page_kr = "랜딩 페이지";
		break;
		case "recharge_receipt":	$d_z_tksk_page_kr = "결제 영수증 페이지";
		break;
		case "ics".$row_z_tksk['z_tksk_ics']."_test":	$d_z_tksk_page_kr = "ics".$row_z_tksk['z_tksk_ics']." 테스트 페이지";
		break;
		case "complete":			$d_z_tksk_page_kr = "ics".$row_z_tksk['z_tksk_ics']." APP 다운로드 완료 페이지";
		break;
		case "popselect":			$d_z_tksk_page_kr = "팝업 선택 페이지";
		break;
		default:					$d_z_tksk_page_kr = "없음";
		break;
	}

	$d_tksk_page[$row_z_tksk['z_tksk_page']] = $d_z_tksk_page_kr;
}

// 기본적으로 몇개 있는 지 체크;
$sql_total = "select count(*) as total_z_ticketsocket from z_ticketsocket where 1  ";
$sql_count = sql_count($sql_total, 'total_z_ticketsocket');


/* 데이터 가져오기 */
$sql_where = "";

// 캠페인
if($_tksk_campaign){
	$sql_where.= " and z_tksk_campaign = '$_tksk_campaign' ";
}

// 랜딩페이지
if($_tksk_page){
	$sql_where.= " and z_tksk_page = '$_tksk_page' ";
}

// 시작일/종료일 날짜
$start_date = $end_date = "";
if($_s_date && $_e_date){
	$start_date = $_s_date;
	$end_date = $_e_date;
}else if($_s_date){
	$start_date = $_s_date;
	$end_date = NM_TIME_YMD;
}
if($start_date && $end_date){
	$sql_where.= "and ( z_tksk_date >= '".$start_date." ".NM_TIME_HI_start."' and z_tksk_date <= '".$end_date." ".NM_TIME_HI_end."') ";
}else{
	$_s_date = "";
	$_e_date = "";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "z_tksk_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$sql_order = "order by ".$_order_field." ".$_order;

$sql_help = "";
$field_help = " * "; // 가져올 필드 정하기
$limit_help = "";

$sql= "select $field_help FROM z_ticketsocket where 1 $sql_where $sql_order $limit_help";
$result = sql_query($sql);
$row_size = sql_num_rows($result);

if($_mode == 'excel') {
	include_once $_cms_folder_path.'/excel/ticketsocket_ics_excel.php';
	die;
} // end if

$mktl_z_tksk_arrs = array();
$result_mktl = sql_query("select * from z_ticketsocket_link group by z_tksk_no");
while ($row_mktl = sql_fetch_array($result_mktl)) {
	array_push($mktl_z_tksk_arrs,  $row_mktl['z_tksk_no']);
}

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','z_tksk_no',0));
array_push($fetch_row, array('ICS번호<br/>[필수]','z_tksk_ics',0));
array_push($fetch_row, array('캠페인ID<br/>[필수]','z_tksk_campaign',0));
array_push($fetch_row, array('공유ICS페이지<br/>[피너툰페이지구분용]','z_tksk_page',0));
array_push($fetch_row, array('공유번호(orderId)<br/>[필수]','z_tksk_number',0));
array_push($fetch_row, array('공유email(email)<br/>[필수]','z_tksk_email',0));
array_push($fetch_row, array('공유가격(revenue)<br/>[필수]','z_tksk_revenue',0));
array_push($fetch_row, array('공유회원번호(name)<br/>[옵션]<br/>피너툰아이디','z_tksk_name',0));
array_push($fetch_row, array('주문상품명(productName)<br/>[옵션]','z_tksk_productname',0));
array_push($fetch_row, array('주문상품URL(ProductUrl)<br/>[옵션-미사용]','z_tksk_producturl',0));
array_push($fetch_row, array('등록일','z_tksk_date',0));

if($_s_limit == ''){ $_s_limit = 10; }

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "ICS 공유회원";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
	<div class="write">
		<input type="button" class="excel_down" value="검색 결과 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?s_date=<?=$_s_date;?>&e_date=<?=$_e_date;?>&tksk_campaign=<?=$tksk_campaign;?>&mode=excel'"/>
	</div>
	<div>
		<strong><?=$page_title?> : 총 <?=number_format($sql_count);?> 건</strong>
	</div>
</section>

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="event_recharge_search_form" id="event_recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return event_recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_date_btn">
					<?	tag_selects($d_tksk_page, "tksk_page", $_tksk_page, 'y', '페이지 전체', ''); ?>
					<?	tag_selects($d_tksk_campaign, "tksk_campaign", $_tksk_campaign, 'y', '캠페인 전체', ''); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit free_dc_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="72" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="ticketsocket_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?z_tksk_no=".$dvalue_val['z_tksk_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					$mb_info = mb_get_no($dvalue_val['z_tksk_name']);
					$mb_id = $mb_info['mb_id'];

					// 등록일
					$db_z_tksk_date_ymd = get_ymd($dvalue_val['z_tksk_date']);
					$db_z_tksk_date_his = get_his($dvalue_val['z_tksk_date']);

					// 피너툰페이지구분용
					$z_tksk_page_kr = "";
					switch($dvalue_val['z_tksk_page']){
						case "index":				$z_tksk_page_kr = "ics".$dvalue_val['z_tksk_ics']." 랜딩 페이지";
						break;
						case "recharge_receipt":	$z_tksk_page_kr = "결제 영수증 페이지";
						break;
						case "ics".$dvalue_val['z_tksk_ics']."_test":	$z_tksk_page_kr = "ics".$dvalue_val['z_tksk_ics']." 테스트 페이지";
						break;
						case "complete":			$z_tksk_page_kr = "ics".$dvalue_val['z_tksk_ics']." APP 다운로드 완료 페이지";
						break;
						case "popselect":			$z_tksk_page_kr = "ics".$dvalue_val['z_tksk_ics']." 팝업 선택 페이지";
						break;
						default:					$z_tksk_page_kr = "없음";
						break;
					}

					if($z_tksk_page_kr == "없음" && $dvalue_val['z_tksk_page'] !=""){
						$z_tksk_page_kr = $dvalue_val['z_tksk_page'];
					}

				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['z_tksk_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['z_tksk_ics'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center">
						<? echo $dvalue_val['z_tksk_campaign'] == ""?"없음":$dvalue_val['z_tksk_campaign'];?>
					</td>
					<td class="<?=$fetch_row[3][1]?> text_center">
						<? echo $dvalue_val['z_tksk_page'] == ""?"없음":$dvalue_val['z_tksk_page'];?>
						<br/>[<?=$z_tksk_page_kr?>]
					</td>
					<td class="<?=$fetch_row[4][1]?> text_center">
						<? echo $dvalue_val['z_tksk_number'] == ""?"없음":$dvalue_val['z_tksk_number'];?>
					</td>
					<td class="<?=$fetch_row[5][1]?> text_center">
						<? echo $dvalue_val['z_tksk_email'] == ""?"없음":$dvalue_val['z_tksk_email'];?>
					</td>
					<td class="<?=$fetch_row[6][1]?> text_center">
						<? echo $dvalue_val['z_tksk_revenue'] == ""?"없음":$dvalue_val['z_tksk_revenue'];?>
					</td>
					<td class="<?=$fetch_row[7][1]?> text_center">
						<?=$mb_info['mb_no'];?><br/>
						<a href="#" onclick="popup('<?=NM_ADM_URL?>/members/member_view.php?mb_id=<?=$mb_id;?>','member_view', <?=$popup_cms_width;?>, 550);">
							<?=$mb_info['mb_id'];?>
						</a>
					</td>
					<td class="<?=$fetch_row[8][1]?> text_center">
						<? echo $dvalue_val['z_tksk_productname'] == 0?"없음":$dvalue_val['z_tksk_productname'];?>
					</td>
					<td class="<?=$fetch_row[9][1]?> text_center">
						미사용
					</td>
					<td class="<?=$fetch_row[10][1]?> text_center">
						<?=$db_z_tksk_date_ymd;?><br/><?=$db_z_tksk_date_his;?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- event_recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>