<?
include_once '_common.php'; // 공통

/* PARAMITER 
select stksk_year_month, stksk_day, stksk_week, stksk_campaign, 
sum(stksk_open)as stksk_open_cnt, sum(stksk_share)as stksk_share_cnt FROM stats_ticketsocket 
where 1 AND ( (stksk_year_month > '2017-11' AND stksk_year_month < '2017-11' ) 
OR ( (stksk_year_month = '2017-11' AND stksk_day >= '01' ) AND (stksk_year_month = '2017-11' AND stksk_day <= '26' ) ) ) 
group by stksk_campaign, stksk_year_month, stksk_day order by stksk_year_month, stksk_day desc
*/


// 캠페인
$d_tksk_campaign = array();
$sql_tksk = "select * from ticketsocket order by tksk_no";
$result_tksk = sql_query($sql_tksk);
while ($row_tksk = sql_fetch_array($result_tksk)) {
	$d_tksk_campaign[$row_tksk['tksk_campaign']] = $row_tksk['tksk_campaign'];
	if(intval($row_tksk['tksk_campaign_complete']) >0){
		$d_tksk_campaign[$row_tksk['tksk_campaign_complete']] = $row_tksk['tksk_campaign_complete'];
	}
}

// 랜딩페이지
$d_stksk_ics_page = array();
$sql_stksk = "select * from stats_ticketsocket group by stksk_ics_page";
$result_stksk = sql_query($sql_stksk);
while ($row_stksk= sql_fetch_array($result_stksk)) {
	$d_z_tksk_ics_page_kr = "";
	switch($row_stksk['stksk_ics_page']){
		case "index":				$d_z_tksk_ics_page_kr = "랜딩 페이지";
		break;
		case "recharge_receipt":	$d_z_tksk_ics_page_kr = "결제 영수증 페이지";
		break;
		case "ics".$row_stksk['stksk_ics']."_test":	$d_z_tksk_ics_page_kr = "ics".$row_stksk['stksk_ics']." 테스트 페이지";
		break;
		case "complete":			$d_z_tksk_ics_page_kr = "ics".$row_stksk['stksk_ics']." APP 다운로드 완료 페이지";
		break;
		case "popselect":			$d_z_tksk_ics_page_kr = "팝업 선택 페이지";
		break;
		default:					$d_z_tksk_ics_page_kr = "없음";
		break;
	}
	$d_stksk_ics_page[$row_stksk['stksk_ics_page']] = $d_z_tksk_ics_page_kr;
}

/* 데이터 가져오기 */
$where_stats_ticketsocket = ""; /* 충전 */

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

// 파트너일 경우
$_e_date		= mb_partner_e_date($nm_member, $_e_date);	// 종료일 현재 -1일 적용

if($_s_date && $_e_date){
	$where_stats_ticketsocket.= date_year_month($_s_date, $_e_date, 'stksk');
}

// 랜딩페이지
if($_tksk_page){
	$where_stats_ticketsocket.= " and stksk_ics_page = '$_tksk_page' ";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "stksk_year_month, stksk_day"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 요일 정렬시
if($_order_field == "stksk_week"){$_order_field_add = " , stksk_date "; $_order_add = "desc ";}
// 날짜 정렬시
if($_order_field == "stksk_year_month"){$_order_field_add = " , stksk_date "; $_order_add = $_order;}
$order_stats_ticketsocket = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add." , stksk_campaign desc ";

$sql_stats_ticketsocket = "";
$field_stats_ticketsocket = "  stksk_year_month, stksk_day, stksk_week, stksk_ics, stksk_ics_page, stksk_campaign, sum(stksk_open)as stksk_open_cnt, sum(stksk_share)as stksk_share_cnt "; // 가져올 필드 정하기
$group_stats_ticketsocket = " group by stksk_ics, stksk_ics_page, stksk_campaign, stksk_year_month, stksk_day ";
$limit_stats_ticketsocket = "";

$sql_stats_ticketsocket = "select $field_stats_ticketsocket FROM stats_ticketsocket where 1 $where_stats_ticketsocket $group_stats_ticketsocket $order_stats_ticketsocket $limit_stats_ticketsocket";
$result = sql_query($sql_stats_ticketsocket);
$row_size = sql_num_rows($result);

if($_mode == 'excel') {
	include_once $_cms_folder_top_path.'/excel/day_buy_excel.php';
} // end if

/* 검색 전체 구하기 - group by 빼고 sum, count함수 이용 */
$sql_stats_ticketsocket_s_all = "select $field_stats_ticketsocket FROM stats_ticketsocket where 1 $where_stats_ticketsocket $order_stats_ticketsocket $limit_stats_ticketsocket";
$sql_stats_ticketsocket_s_all_row = sql_fetch($sql_stats_ticketsocket_s_all );
$s_all_sumpay = $sql_stats_ticketsocket_s_all_row['sumpay'];
$s_all_sumcnt = $sql_stats_ticketsocket_s_all_row['sumcnt'];
$s_all_repay_cnt = $sql_stats_ticketsocket_s_all_row['repay_cnt'];

$s_all_sumpay_text = number_format($s_all_sumpay)." 원";
$s_all_sumcnt_text = number_format($s_all_sumcnt)." 건";
if($s_all_sumcnt == 0){ $s_all_sumcnt = 1; }
$s_all_repay_cnt_text  = number_format($s_all_repay_cnt)." 건 [ ".intval(($s_all_repay_cnt / $s_all_sumcnt * 1000) / 10)."% ]";


/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','stksk_year_month',2));
array_push($fetch_row, array('요일','stksk_week',2));
array_push($fetch_row, array('ICS번호','stksk_ics',0));
array_push($fetch_row, array('캠페인번호','stksk_campaign',0));
array_push($fetch_row, array('페이지','stksk_ics_page',0));
array_push($fetch_row, array('랜딩페이지 유입수','stksk_open',0));
array_push($fetch_row, array('공유 클릭수','stksk_share',0));
array_push($fetch_row, array('공유 로그수','stksk_share',0));
// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "ICS 통계";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()

?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
	<!--
	<div class="write">
		<input type="button" class="excel_down" value="검색 결과 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?s_date=<?=$_s_date;?>&e_date=<?=$_e_date;?>&order_field=<?=$_order_field;?>&order=<?=$_order;?>&mode=excel'"/>
	</div>
	-->
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($d_stksk_ics_page, "tksk_page", $_tksk_page, 'y', '페이지 전체', ''); ?>
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="ticketsocket_result">
	<h3>검색 리스트 
		<strong>검색 결과</strong> <span style="font-family:Nanum Gothic,dotum; font-weight:normal;">[ 주문번호는 중복이 불가능하게 해서 공유클릭수와 공유수가 다를수 있습니다. ]</span>
	</h3>
		
	
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = $th_rowspan = $th_colspan = "";
					switch($fetch_val[2]){
						case '4': $th_rowspan = 'rowspan="3"';
						break;
						case '3': $th_colspan = 'colspan="3"';
						break;
						case '2': $th_rowspan = 'rowspan="2"';
						break;

					}
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th <?=$th_rowspan?> <?=$th_colspan?> class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?	
					$stksk_date_arr = array(array());
					foreach($row_data as $dvalue_key => $dvalue_val){
					// 피너툰페이지구분용
					$z_tksk_page_kr = "";
					switch($dvalue_val['stksk_ics_page']){
						case "index":				$stksk_ics_page_kr = "ics".$dvalue_val['stksk_ics']." 랜딩 페이지";
						break;
						case "recharge_receipt":	$stksk_ics_page_kr = "결제 영수증 페이지";
						break;
						case "ics".$dvalue_val['stksk_ics']."_test":	$stksk_ics_page_kr = "ics".$dvalue_val['stksk_ics']." 테스트 페이지";
						break;
						case "complete":			$stksk_ics_page_kr = "ics".$dvalue_val['stksk_ics']." APP 다운로드 완료 페이지";
						break;
						case "popselect":			$stksk_ics_page_kr = "ics".$dvalue_val['stksk_ics']." 팝업 선택 페이지";
						break;
						default:					$stksk_ics_page_kr = "없음";
						break;
					}

					if($stksk_ics_page_kr == "없음" && $dvalue_val['stksk_ics_page'] !=""){
						$stksk_ics_page_kr = $dvalue_val['stksk_ics_page'];
					}
					
					/* 날짜 요일 결제-금액 결제-건수 결제-재구매건[율] */
					$stksk_year_month_day = $dvalue_val['stksk_year_month']."-".$dvalue_val['stksk_day'];
					$stksk_week = get_yoil($dvalue_val['stksk_week'],1,1); 
					$week_class = $week_en[$dvalue_val['stksk_week']];	/* 요일별 class */
					
						$stksk_date_arr[$stksk_year_month_day]['week'] = $stksk_week;
						$stksk_date_arr[$stksk_year_month_day]['class'] = $week_class;
						$stksk_date_arr[$stksk_year_month_day]['date'] = $stksk_year_month_day;
						array_push($stksk_date_arr[$stksk_year_month_day], $dvalue_val['stksk_ics']);
						array_push($stksk_date_arr[$stksk_year_month_day], $dvalue_val['stksk_campaign']);
						array_push($stksk_date_arr[$stksk_year_month_day], $dvalue_val['stksk_ics_page']."<br/>[".$stksk_ics_page_kr."]");

						array_push($stksk_date_arr[$stksk_year_month_day], number_format($dvalue_val['stksk_open_cnt'])." 건");
						array_push($stksk_date_arr[$stksk_year_month_day], number_format($dvalue_val['stksk_share_cnt'])." 건");
						$sql_z_tksk = "select count(*) as total_z_ticketsocket from z_ticketsocket where 1 
										AND z_tksk_ics= '".$dvalue_val['stksk_ics']."' 
										AND z_tksk_page= '".$dvalue_val['stksk_ics_page']."' 
										AND z_tksk_campaign= '".$dvalue_val['stksk_campaign']."' 
										AND z_tksk_year_month= '".$dvalue_val['stksk_year_month']."' 
										AND z_tksk_day= '".$dvalue_val['stksk_day']."' 
										AND z_tksk_week= '".$dvalue_val['stksk_week']."' 
						";
						$sql_z_count = sql_count($sql_z_tksk, 'total_z_ticketsocket');
						array_push($stksk_date_arr[$stksk_year_month_day], number_format($sql_z_count)." 건");
					}
				foreach($stksk_date_arr as $stksk_date_key => $stksk_date_val){
					if($stksk_date_key == 0){ continue;}
					$stksk_date_loop = count($stksk_date_val)-3; 
					$td_row = intval($stksk_date_loop) / 6;
				?>
				<tr class="<?=$stksk_date_val['class'];?> result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center" rowspan="<?=$td_row;?>">
						<?=$stksk_date_val['date'];?>
					</td>
					<td class="<?=$fetch_row[1][1]?> text_center" rowspan="<?=$td_row;?>">
						<?=$stksk_date_val['week'];?>
					</td>
					<? for($i=0; $i<$stksk_date_loop; $i++){ 
						$tr_i = $i+1;
					?>
					<td class="<?=$fetch_row[$i+2][1]?> text_center">
						<?=$stksk_date_val[$i];?>
					</td>
					<? if($tr_i % 6 == 0){ echo "</tr><tr>"; }					
					} ?>
				</tr>
				<? } ?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>