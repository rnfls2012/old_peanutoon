<?	include_once '_common.php'; // 공통

// 기본적으로 몇개 있는 지 체크;
$sql_tksk_total = "select count(*) as total_tksk from ticketsocket where 1  ";
$total_tksk = sql_count($sql_tksk_total, 'total_tksk');

/* 데이터 가져오기 */
$tksk_where = '';

// 정렬
/*
if($_order_field == null || $_order_field == ""){ $_order_field = "( CASE tksk_state WHEN 'r' THEN 1 WHEN 'y' THEN 2 WHEN 'n' THEN 3 ELSE 4 END ), tksk_reg_date "; }
if($_order == null || $_order == ""){ $_order = "desc"; }
*/
$tksk_order = "order by tksk_no desc";

$tksk_sql = "";
$tksk_field = " * "; // 가져올 필드 정하기
$tksk_limit = "";
$tksk_sql = "SELECT $tksk_field from ticketsocket where 1 $tksk_where $tksk_order $tksk_limit";
$result = sql_query($tksk_sql);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('ICS번호','tksk_no',0));
array_push($fetch_row, array('ICS명','tksk_name',0));
array_push($fetch_row, array('캠페인ID','tksk_campaign',0));
array_push($fetch_row, array('캠페인ID링크리스트','tksk_campaign_link',0));
array_push($fetch_row, array('캠페인ID연결URL<br/>(바로연결)','tksk_link',0));
array_push($fetch_row, array('시작일','tksk_date_start',0));
array_push($fetch_row, array('마감일','tksk_date_end',0));
array_push($fetch_row, array('ICS등급','tksk_adult',0));
array_push($fetch_row, array('ICS상태','tksk_state',0));
array_push($fetch_row, array('등록일<br/>수정일','tksk_date',0));

$cs_calendar_on = $cs_submit_h = '';
if($_date_type){
	$cs_calendar_on = 'cs_calendar_on';
	$cs_submit_h = 'cs_submit_on';
}

$page_title = "티켓소켓";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> ICS : 총 <?=number_format($total_tksk);?> 건</strong>
	</div>
</section>

<section id="ticketsocket_result">
	<div id="cr_bg">
		<table>
			<thead id="">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 이미지 표지 */
					$image_bener_url = img_url_para($dvalue_val['tksk_cover'], $dvalue_val['tksk_reg_date'], $dvalue_val['tksk_mod_date'], 100, 65);
					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?tksk_no=".$dvalue_val['tksk_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					// 등록일
					$db_tksk_reg_date_ymd = get_ymd($dvalue_val['tksk_reg_date']);
					$db_tksk_reg_date_his = get_his($dvalue_val['tksk_reg_date']);

					// 수정일
					$db_tksk_mod_date_ymd = get_ymd($dvalue_val['tksk_mod_date']);
					$db_tksk_mod_date_his = get_his($dvalue_val['tksk_mod_date']);


					$db_tksk_no_date_text = "무기간";
					if($dvalue_val['tksk_date_type'] != 'n' && $dvalue_val['tksk_date_start'] == '' && $dvalue_val['tksk_date_end'] == ''){
						$db_tksk_no_date_text = "시작일&마감일 없어서 종료됩니다.";
					}

					// 이벤트 링크
					$db_tksk_ics_link = $db_tksk_ics_link_test = $db_tksk_ics_link_complete = $db_tksk_ics_link_pop = "";
					$db_tksk_ics_link		= NM_TKSK_URL."/".tksk_ics($dvalue_val['tksk_no']);
					$db_tksk_ics_link_test	= NM_TKSK_URL."/".tksk_ics($dvalue_val['tksk_no'])."/test";
					if(intval($dvalue_val['tksk_campaign_complete']) > 0 && $dvalue_val['tksk_campaign_complete'] != ''){
						$db_tksk_ics_link_complete	= NM_TKSK_URL."/".tksk_ics($dvalue_val['tksk_no'])."/complete";
					}
					if($dvalue_val['tksk_pop'] == 'y'){
						$db_tksk_ics_link_pop	= NM_TKSK_URL."/".tksk_ics($dvalue_val['tksk_no'])."/popselect";
					}
					
					// 이벤트 링크
					$db_tksk_link = $dvalue_val['tksk_link'];
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center">
						<?=$dvalue_val['tksk_no'];?>
					</td>
					<td class="<?=$fetch_row[1][1]?> text_center">
						<?=$dvalue_val['tksk_name'];?>
					</td>
					<td class="<?=$fetch_row[2][1]?> text_center">
						<? echo $dvalue_val['tksk_campaign'] == ""?"없음":$dvalue_val['tksk_campaign'].'</a><br/>';?>
						<? echo $dvalue_val['tksk_campaign_complete'] == ""?"":$dvalue_val['tksk_campaign_complete'].'</a><br/>';?>
					</td>
					<td class="<?=$fetch_row[3][1]?>">
						<? echo $db_tksk_ics_link == ""?"":'<a href="'.$db_tksk_ics_link.'" target="_blank">'.$db_tksk_ics_link.'</a><br/>';?>
						<? echo $db_tksk_ics_link_test == ""?"":'<a href="'.$db_tksk_ics_link_test.'" target="_blank">'.$db_tksk_ics_link_test.'</a><br/>';?>
						<? echo $db_tksk_ics_link_complete == ""?"":'<a href="'.$db_tksk_ics_link_complete.'" target="_blank">'.$db_tksk_ics_link_complete.'</a><br/>';?>
						<? echo $db_tksk_ics_link_pop == ""?"":'<a href="'.$db_tksk_ics_link_pop.'" target="_blank">'.$db_tksk_ics_link_pop.'</a><br/>';?>
					</td>
					<td class="<?=$fetch_row[4][1]?> text_center">
						<? echo $db_tksk_link == ""?"없음":'<a href="'.$db_tksk_link.'" target="_blank">'.$db_tksk_link.'</a><br/>';?>
					</td>
					<?if($dvalue_val['tksk_date_type'] != 'n' && $dvalue_val['tksk_date_start'] != '' && $dvalue_val['tksk_date_end'] != ''){?>
						<td class="<?=$fetch_row[5][1]?> text_center"><?=$dvalue_val['tksk_date_start'];?></td>
						<td class="<?=$fetch_row[6][1]?> text_center"><?=$dvalue_val['tksk_date_end'];?></td>
					<?}else{?>
						<td colspan='2' class="tksk_date_no text_center"><?=$db_tksk_no_date_text?></td>
					<?}?>
					<td class="<?=$fetch_row[7][1]?> text_center"><?=$d_adult[$dvalue_val['tksk_adult']];?></td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=$d_state[$dvalue_val['tksk_state']];?></td>
					<td class="<?=$fetch_row[9][1]?> text_center">
						<?=$db_tksk_reg_date_ymd;?><?=$db_tksk_reg_date_his;?><br/>
						<?=$db_tksk_mod_date_ymd;?><?=$db_tksk_mod_date_his;?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- event_recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>