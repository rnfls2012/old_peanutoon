<?
include_once '_common.php'; // 공통

/* PARAMITER */

$marketing_name = "";
switch($_mode) {
	case 'date' :  $marketing_name = "marketerlink_date.php";	// 날짜별
	break;

	case 'join' :  $marketing_name = "marketerlink_join.php";	// 가입자-날짜별
	break;

	case 'list' :  $marketing_name = "marketerlink_list.php";	// 리스트
	break;

	default : $marketing_name = "marketerlink_list.php";
	break;
}

/* 181001

ALTER TABLE `marketer_link`
	ADD COLUMN `mktl_link_utm_source` VARCHAR(1000) NOT NULL DEFAULT '' COMMENT '연결 링크-utm_source' AFTER `mktl_link`,
	ADD COLUMN `mktl_link_utm_medium` VARCHAR(1000) NOT NULL DEFAULT '' COMMENT '연결 링크-utm_medium' AFTER `mktl_link_utm_source`,
	ADD COLUMN `mktl_link_utm_campaign` VARCHAR(1000) NOT NULL DEFAULT '' COMMENT '연결 링크-utm_campaign' AFTER `mktl_link_utm_medium`,
	ADD COLUMN `mktl_link_utm_term` VARCHAR(1000) NOT NULL DEFAULT '' COMMENT '연결 링크-utm_term' AFTER `mktl_link_utm_campaign`,
	ADD COLUMN `mktl_link_utm_content` VARCHAR(1000) NOT NULL DEFAULT '' COMMENT '연결 링크-utm_content' AFTER `mktl_link_utm_term`;
위 DB 적용함
*/

include_once(NM_ADM_PATH."/marketing/".$marketing_name);
