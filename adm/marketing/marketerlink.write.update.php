<?
include_once '_common.php'; // 공통
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMETER CHECK */
array_push($para_list, 'mode','mkt_no','mktl_no','mktl_link','mktl_short_link','mktl_short_link_idx','mktl_date');
array_push($para_list, 'mktl_link_utm_source', 'mktl_link_utm_medium', 'mktl_link_utm_campaign', 'mktl_link_utm_term', 'mktl_link_utm_content');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'mktl_link_utm_medium', 'mktl_link_utm_campaign', 'mktl_link_utm_term', 'mktl_link_utm_content');

/* 숫자 PARAMETER 체크 */
array_push($para_num_list, 'mkt_no');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

$dbtable = "marketer_link";
$dbt_primary = "mktl_no";
$para_primary = "mktl_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

/* DB field 아닌 목록 */
$db_field_exception = array('mode'); 

if($_mode == 'reg'){
	/* 고정값 */
	$_mktl_date = NM_TIME_YMDHIS; /* 최초등록일 */

	/* 간편 링크  */
	$date = sprintf("%03d", date("z")); // 365일 날짜
	$year = substr(NM_TIME_Y, 3, 1); // 년도 마지막 숫자
	$head = $year.$date; // 년도 마지막 숫자 + 365일 날짜 (쿠폰 머리)
	$body = strtolower(random_string("alnum", 12)); // 쿠폰 몸통
	$_mktl_short_link_idx = substr($body, 0, 1); // 쿠폰 5번째 자리 idx
	$_mktl_short_link = $head.$body; // 쿠폰 코드
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);

	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = $_bh_title.'의 데이터가 등록되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

/* 수정 */
}else if($_mode == 'mod'){
	if(${'_'.$dbt_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$dbt_primary.'값이 없습니다.';
	}else{		
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= ' WHERE '.$dbt_primary.'="'.${'_'.$dbt_primary}.'"';
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $_bh_title.'의 데이터가 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$dbt_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$dbt_primary.'값이 없습니다.';
	}else{
		/* 데이터 삭제 */
		$sql_del = 'delete from '.$dbtable.' where '.$dbt_primary.'="'.${'_'.$dbt_primary}.'"';
		sql_query($sql_del);		
		$db_result['msg'] = $_bh_title.'의 데이터가 삭제되였습니다.';
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
pop_close($db_result['msg'], '', $db_result['error']);
die;

?>