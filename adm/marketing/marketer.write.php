<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_mkt_no = tag_filter($_REQUEST['mkt_no']);

$sql = "select * from marketer where mkt_no = '$_mkt_no'";

/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */} // end if
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
	break;
} // end switch

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "마케터";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
echo $editor_content_js;
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<script type="text/javascript">
<!--
	window.onload = function(){popup_resize('<?=$_mode;?>');}
//-->
</script>

<section id="cms_marketer_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- cms_member_title -->

<section id="marketer_write">
	<form name="marketer_write_form" id="marketer_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return marketer_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/> <!-- 입력모드 -->
		<input type="hidden" id="mkt" name="mkt_no" value="<?=$_mkt_no;?>"/><!-- 수정/삭제시 글 번호 -->

		<table>
			<tbody>
				<tr>
					<th><label for="mkt_title"><?=$required_arr[0]?>업체명</label></th>
					<td>
						<input type="text" placeholder="업체명 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="mkt_title" id="mkt_title" value="<?=$row['mkt_title'];?>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="mkt_title_en">업체명_영문<br/>(숫자,_포함해서 띄어쓰기없이 부탁드려요)<br/>[utm_source]</label></th>
					<td>
						<input type="text" placeholder="업체명 입력해주세요" name="mkt_title_en" id="mkt_title_en" value="<?=$row['mkt_title_en'];?>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="mkt_url"><?=$required_arr[0]?>업체-URL</label></th>
					<td>
						<input type="text" placeholder="업체-URL 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="mkt_url" id="mkt_url" value="<?=$row['mkt_url'];?>" autocomplete="off"/>
					</td>
				</tr>
				<!--
				<tr>
					<th><label for="mkt_para_join">가입파라미터</label></th>
					<td>
						<input type="text" placeholder="가입파라미터 입력해주세요" name="mkt_para_join" id="mkt_para_join" value="<?=$row['mkt_para_join'];?>" autocomplete="off"/>
					</td>
				</tr>
				-->
				<tr>
					<th><label for="mkt_state"><?=$required_arr[0]?>상태</label></th>
					<td>
						<div id="mkt_state" class="input_btn">
							<input type="radio" name="mkt_state" id="mkt_statey" value="y" <?=get_checked('', $row['mkt_state']);?> <?=get_checked('y', $row['mkt_state']);?> />
							<label for="mkt_statey">사용</label>
							<input type="radio" name="mkt_state" id="mkt_staten" value="n" <?=get_checked('n', $row['mkt_state']);?> />
							<label for="mkt_staten">차단</label>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="mkt_type"><?=$required_arr[0]?>업체 타입</label></th>
					<td>
						<? tag_selects($d_mkt_type, "mkt_type", $row['mkt_type'], 'n'); ?>
					</td>
				</tr>
				<!--
				<tr>
					<th><label for="mkt_medium">업체 수단<br/>[utm_medium]</label></th>
					<td>
						<? tag_selects($d_mkt_medium, "mkt_medium", $row['mkt_medium'], 'n'); ?>
					</td>
				</tr>
				-->
				<tr>
					<td colspan="2" class="submit_btn">
						<input type="submit" value="<?=$mode_text?>">
						<input type="reset" value="취소" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
	 </form>
</section><!-- board_write -->
<script type="text/javascript">
<!--
	function marketer_write_submit(f){
		<?php echo get_editor_js('mkt_text'); ?>

			

		/* 입력 받는 곳 중 숫자만 해야 되는 곳 검사 */
		var onlynumber = onlynumber_text = "";
		var onlynumber_check = true;
		$('.onlynumber').each(function() {
			onlynumber = $(this).val();
			onlynumber_text = '숫자만 입력해주세요';
			if(onlynumber != ''){
				var pattern = /(^[0-9]+$)/;
				if (!pattern.test(onlynumber)) {
					alert(onlynumber_text);
					$(this).val('');
					$(this).focus();
					onlynumber_check = false;
					return false; 
				}
			}
		});
		if(onlynumber_check == false){ return false; }
	}
//-->
</script>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>