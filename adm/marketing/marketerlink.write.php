<? include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_mktl_no = tag_filter($_REQUEST['mktl_no']);

$sql = "select * from marketer_link where mktl_no = '$_mktl_no'";

/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */} // end if
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
	break;
} // end switch

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}

// 업체 리스트
$array_mkt = array();
$sql_mkt = "select * from marketer ";
$result_mkt = sql_query($sql_mkt);
while ($row_mkt = sql_fetch_array($result_mkt)) {
	$array_mkt[$row_mkt['mkt_no']] = $row_mkt['mkt_title'];
	$array_mkt_en[$row_mkt['mkt_no']] = $row_mkt['mkt_title_en'];
}

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "마케터링크";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
echo $editor_content_js;
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<script type="text/javascript">
<!--
	window.onload = function(){popup_resize('<?=$_mode;?>');}
//-->
</script>

<section id="cms_help_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- cms_member_title -->

<section id="marketerlink_write">
	<form name="mktl_write_form" id="mktl_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return mktl_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/> <!-- 입력모드 -->
		<input type="hidden" id="mktl_no" name="mktl_no" value="<?=$_mktl_no;?>"/><!-- 수정/삭제시 글 번호 -->

		<table>
			<tbody>
				<tr>
					<th><label for="mkt_no"><?=$required_arr[0]?>업체</label></th>
					<td>
						<? tag_selects($array_mkt, "mkt_no", $row['mkt_no'], 'y', '선택'); ?>
					</td>
				</tr>
				<tr>
					<th><label for="mktl_link"><?=$required_arr[0]?>연결 링크<br/>(간편링크는 자동생성)</label></th>
					<td>
						<input type="text" placeholder="연결링크 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="mktl_link" id="mktl_link" value="<?=$row['mktl_link'];?>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th colspan=2><label for="mktl_link_utm">google-utm설정 ↓</label></th>
				</tr>
				<tr>
					<th><label for="mktl_link_utm_source"><?=$required_arr[0]?>Campaign Source</label></th>
					<td>
						<input type="text" class="utm_input" placeholder="Campaign Source 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="mktl_link_utm_source" id="mktl_link_utm_source" value="<?=$row['mktl_link_utm_source'];?>" autocomplete="off"/>
						<p>The referrer: (e.g. google, newsletter)</p>
					</td>
				</tr>
				<tr>
					<th><label for="mktl_link_utm_medium">Campaign Medium</label></th>
					<td>
						<input type="text" class="utm_input" placeholder="Campaign Medium 입력해주세요" name="mktl_link_utm_medium" id="mktl_link_utm_medium" value="<?=$row['mktl_link_utm_medium'];?>" autocomplete="off"/>
						<p>Marketing medium: (e.g. cpc, banner, email)</p>
					</td>
				</tr>
				<tr>
					<th><label for="mktl_link_utm_campaign">Campaign Name</label></th>
					<td>
						<input type="text" class="utm_input" placeholder="Campaign Name 입력해주세요" name="mktl_link_utm_campaign" id="mktl_link_utm_campaign" value="<?=$row['mktl_link_utm_campaign'];?>" autocomplete="off"/>
						<p>Product, promo code, or slogan (e.g. spring_sale)</p>
					</td>
				</tr>
				<tr>
					<th><label for="mktl_link_utm_term">Campaign Term</label></th>
					<td>
						<input type="text" class="utm_input" placeholder="Campaign Term 입력해주세요" name="mktl_link_utm_term" id="mktl_link_utm_term" value="<?=$row['mktl_link_utm_term'];?>" autocomplete="off"/>
						<p>Identify the paid keywords</p>
					</td>
				</tr>
				<tr>
					<th><label for="mktl_link_utm_content">Campaign Content</label></th>
					<td>
						<input type="text" class="utm_input" placeholder="Campaign Content 입력해주세요" name="mktl_link_utm_content" id="mktl_link_utm_content" value="<?=$row['mktl_link_utm_content'];?>" autocomplete="off"/>
						<p>Use to differentiate ads</p>
					</td>
				</tr>
				<tr>
					<th><label for="mktl_link_utm_content">Share the generated campaign URL</label></th>
					<td id="utm_result">
						<div style="word-break: break-all;">
						<?=$utm_url = gtm_utm_url($row);?>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="submit_btn">
						<input type="submit" value="<?=$mode_text?>">
						<input type="reset" value="취소" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
	 </form>
</section><!-- board_write -->
<script type="text/javascript">
<!--
	var mkt_arr = new Array();
	<? foreach($array_mkt_en as $mkt_en_key => $mkt_en_val){ ?>
		mkt_arr[<?=$mkt_en_key;?>] = "<?=$mkt_en_val;?>";

	<? } ?>

	$(function() {
		$(".utm_input").on("keyup", function(e) {
			// 혹시 나중에 키 제약 걸때
			// var chr = event.keyCode;
			// console.log(chr);	
			this_utm_url();
		});

		$("#mkt_no").on("change", function(e) {
			$("#mktl_link_utm_source").val(mkt_arr[this.value]);
			this_utm_url();
		});
	});
	function this_utm_url(){
		var utm_arr = Array('utm_source','utm_medium','utm_campaign','utm_term','utm_content');
		var utm_url = "";
		$(".utm_input").each(function( idx, val ){
			if(val.value != ''){
				utm_url+="&"+utm_arr[idx]+"="+val.value;
			}
		});
		$('#utm_result div').html("<strong>"+utm_url+"</strong>");	
	}
//-->
</script>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>