<?
include_once '_common.php'; // 공통

/* PARAMITER
bh_no // 글 번호
*/
$ver = "v=".substr(NM_VERSION,1,1);

$marketers_arr = array();

// marketer 종류
$sql_select = " SELECT * FROM marketer WHERE mkt_state = 'y' ";
$result = sql_query($sql_select);

while($row = sql_fetch_array($result)) {
	$marketers_arr[$row['mkt_no']] = $row['mkt_title'];
} // end while

// 기본적으로 몇개 있는 지 체크;
$sql_total = " SELECT count(*) AS total_marketer_link FROM marketer_link ";
$sql_count = sql_count($sql_total, 'total_marketer_link');

$sql_where = "";
$sql_order = "";
$field_help = " * "; // 가져올 필드 정하기

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "mktl_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }

$sql_order = $_order_field." ".$_order;

if($marketer) {
    $sql_where .= "AND mkt_no='".$marketer."' ";
} // end if

// 검색단어+ 검색타입
if($_s_text){
    switch($_s_type){
        case '0': $sql_where.= "AND mktl_no = '$_s_text' ";
            break;

        case '1': $sql_where.= "AND mktl_link like '%$_s_text%'";
            break;

        default: $sql_where.= "AND ( mktl_no = '$_s_text' or mktl_link like '%$_s_text%' )";
            break;
    }
}

$sql= "
    SELECT $field_help FROM marketer_link 
    WHERE 1 $sql_where 
    ORDER BY $sql_order
";

$result = sql_query($sql);
$row_size = sql_num_rows($result);


// 업체 리스트
$array_mkt = array(0);
$sql_mkt = "SELECT * FROM marketer ";
$result_mkt = sql_query($sql_mkt);
while ($row_mkt = sql_fetch_array($result_mkt)) {
	$array_mkt[$row_mkt['mkt_no']] = $row_mkt; // 키 값과 mkt_no가 맞지 않게되면 어긋나게 돼서 array_push 대신 직접 집어넣음. 171207
	//array_push($array_mkt, $row_mkt);
}

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('링크 번호','mktl_no',0));
array_push($fetch_row, array('업체','mkt_no',0));
array_push($fetch_row, array('연결 링크','mktl_link',0));
array_push($fetch_row, array('간편 링크(마케터용)','mktl_short_link',0));
array_push($fetch_row, array('클릭수','mktl_click',0));
array_push($fetch_row, array('가입수','mktl_join',0));
array_push($fetch_row, array('등록일','mktl_date',0));
array_push($fetch_row, array('관리','mktl_management',0));

$page_title = "마케터링크";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>총 <?=$page_title?> : <?=number_format($sql_count)?> 개</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','mktl_write', <?=$popup_cms_width;?>, 650);"><?=$page_title?> 등록</button>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="help_search_form" id="help_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return help_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
                <div class="cs_s_type_btn">
                    <?
                    //검색 필터링(radio)
                    $s_type_list = array('링크번호','연결링크');
                    tag_radios($s_type_list, "s_type", $_s_type, 'y', '전체', '');
                    ?>
                </div>
				<div class="cs_search_btn">
					<? tag_selects($marketers_arr, "marketer", $marketer, 'y'); ?>
					<? tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
                    <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit board_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="marketer_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $cp_css.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?mktl_no=".$dvalue_val['mktl_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";
					
					/* 삭제버튼 */
					$del_btn_use = "ok";
					if(intval($dvalue_val['mktl_click']) > 0){
						$del_btn_use = "";
					}

					/* 날짜 링크 */					
					$mktl_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&mode=date&mktl_no='.$dvalue_val['mktl_no'].'">';
					$mktl_ahref_e = '</a>';

					$mktl_click = $mktl_ahref_s.$dvalue_val['mktl_click'].$mktl_ahref_e;

					/* 가입 링크 */
					$mktj_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&mode=join&mktl_no='.$dvalue_val['mktl_no'].'">';
					$mktj_ahref_e = '</a>';

					$mktl_join = $mktj_ahref_s.$dvalue_val['mktl_join'].$mktj_ahref_e;

					$total_click_cnt += $dvalue_val['mktl_click'];
					$total_join_cnt += $dvalue_val['mktl_join'];

					/* utm-url */
					$utm_url = gtm_utm_url($dvalue_val);
					$mktl_link = cs_para_attach($dvalue_val['mktl_link'], $utm_url);
				?>
				<tr class="result_hover">
					<td class="<?=$cp_css.$fetch_row[0][1]?> text_center"><?=$dvalue_val['mktl_no']?></td>
					<td class="<?=$cp_css.$fetch_row[1][1]?> text_center"><?=$array_mkt[$dvalue_val['mkt_no']]['mkt_title']?></td>
					<td class="<?=$cp_css.$fetch_row[2][1]?> text_center">
						<a href="<?=$mktl_link?>" target="_block">
							<?=$mktl_link?><br/>
							<p style="color:gray">utm : <?=$utm_url?></p>
						</a>
					</td>
					<td class="<?=$cp_css.$fetch_row[3][1]?> text_center">
						<a href="<?=NM_MKT_URL."/".$dvalue_val['mkt_no']."/".$dvalue_val['mktl_short_link']?>?<?=$ver?>&adpx_be_cd=226_2683_4296_74012" target="_block"><?=NM_MKT_URL."/".$dvalue_val['mkt_no']."/".$dvalue_val['mktl_short_link']?>?<?=$ver?></a>
					</td>
					<td class="<?=$cp_css.$fetch_row[4][1]?> text_center"><?=$mktl_click?></td>
                    <td class="<?=$cp_css.$fetch_row[5][1]?> text_center"><?=$mktl_join?></td>
					<td class="<?=$cp_css.$fetch_row[6][1]?> text_center"><?=$dvalue_val['mktl_date']?></td>
					<td class="<?=$cp_css.$fetch_row[7][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url?>','mkt_mod_write', <?=$popup_cms_width?>, 650);">수정</button>
						<?if($del_btn_use == "ok"){?>
						<button class="del_btn" onclick="popup('<?=$popup_del_url?>','mkt_del_write', <?=$popup_cms_width?>, 650);">삭제</button>
						<?}?>
					</td>
				</tr>
				<?}?>
				<tr class="table-bottom">
            <td align="center">합계</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="center"><?=$total_click_cnt?></td>
            <td align="center"><?=$total_join_cnt?></td>
            <td></td>
            <td></td>
        </tr>
			</tbody>
		</table>
	</div>
</section><!-- board_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>