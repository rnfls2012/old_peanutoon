<?
include_once '_common.php'; // 공통

// 업체 리스트
$mkt_arr = array(0);
$sql_mkt = "select * from marketer ";
$result_mkt = sql_query($sql_mkt);
while ($row_mkt = sql_fetch_array($result_mkt)) {
	array_push($mkt_arr, $row_mkt);
}

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

// 파트너일 경우
$sql_mb_cm_cp	= mb_cm_cp_sql($nm_member);		// 파트너 SQL
$_e_date		= mb_partner_e_date($nm_member, $_e_date);	// 종료일 현재 -1일 적용
if($sql_mb_cm_cp !=''){
	$sql_where.= " AND mkt_no = '".$sql_mb_cm_cp."'";
}


// 날짜
if($_s_date && $_e_date){ 
	$sql_where.= date_year_month($_s_date, $_e_date, 'slmktl'); 
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "slmktl_year_month"){ 
	$_order_field = "slmktl_year_month"; 
	$_order_field_add = " , slmktl_day "; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
	$_order_add= "desc"; 
}

// 요일 정렬시
if($_order_field == "slmktl_week"){
	$_order_field_add = " , slmktl_year_month desc "; 
	$_order_add = " , slmktl_day desc ";
}

$sql_order = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

// 그룹
$sql_group = " group by slmktl_year_month, slmktl_day ";

// SQL문
$sql_mkt_field = "";
foreach($mkt_arr as $mkt_key => $mkt_val){
	if($mkt_key == 0){ continue; }
	$sql_mkt_field.= "sum(if(slmktl.mkt_no = '".$mkt_val['mkt_no']."',slmktl_click,0))as mkt_no".$mkt_val['mkt_no']."_sum, ";
}

$sql_field = $sql_mkt_field." slmktl.* "; 
$sql_count	= $sql_field. " , count( distinct slmktl_year_month, slmktl_day ) as cnt ";

$sql_select = "		select {$sql_field} ";
$sql_select_cnt = "	select {$sql_count} ";
$sql_table = "		FROM stats_marketer_link slmktl
					LEFT JOIN marketer mkt ON slmktl.mkt_no = mkt.mkt_no ";

$sql_query		= " {$sql_select}		{$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_select_cnt}	{$sql_table} {$sql_where} ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts		= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data_cnts = sql_fetch($sql_query_cnt);	// SQL문 총값-레코드값
$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','slmktl_year_month',1));
array_push($fetch_row, array('요일','slmktl_week',1));
foreach($mkt_arr as $mkt_key => $mkt_val){
	if($mkt_key == 0){ continue; }
	array_push($fetch_row, array($mkt_val['mkt_title'],'slmktl_mkt',0));
}

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "일별 링크 클릭수";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>
<?
$slmktl_width				= 1400;
$slmktl_year_month_width	= 170;
$slmktl_week_width			= 170;
$slmktl_mkt_width = $slmktl_width - $slmktl_year_month_width - $slmktl_week_width;
$mkt_count = count($mkt_arr) -1;

$slmktl_mkt_no_width_first = $slmktl_mkt_no_width = 0;
if($mkt_count > 0){
	$slmktl_mkt_no_width = intval($slmktl_mkt_width / $mkt_count);
	$slmktl_mkt_no_width_first = $slmktl_mkt_no_width + ($slmktl_mkt_width - intval($mkt_count)  * intval($slmktl_mkt_width / intval($mkt_count)));
}

?>
<style type="text/css">
	#marketer_result div .slmktl_year_month { width: 170px; }
	#marketer_result div .slmktl_week { width: 170px; }
	#marketer_result div .slmktl_mkt{ width:<?=$slmktl_mkt_no_width;?>px; }
	#marketer_result div .slmktl_mkt:first-child{ width:<?=$slmktl_mkt_no_width_first;?>px; } 
</style>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="cms_search_form" id="cms_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return cms_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="marketer_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 건 : <?=number_format($rows_cnts);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){
					$db_date = $dvalue_val['slmktl_year_month']."-".$dvalue_val['slmktl_day'];
					$db_week = get_yoil($dvalue_val['slmktl_week'],1,1); 

					$db_mkt = "";
					
					foreach($mkt_arr as $mkt_key => $mkt_val){
						if($mkt_key == 0){ continue; }
						$db_mkt.= $dvalue_val['title'].$dvalue_val['mkt_no'.$mkt_val['mkt_no'].'_sum'];
					}

					/* 요일별 class */
					$week_class = $week_en[$dvalue_val['slmktl_week']];
				?>
				<tr class="<?=$week_class;?>">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$db_date;?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$db_week;?></td>
					
					<? foreach($mkt_arr as $mkt_key => $mkt_val){ 
						if($mkt_key == 0){ continue; }
						$fetch_no = $mkt_key + 2;
					?>
					<td class="<?=$fetch_row[$fetch_no][1]?> text_center">
						<?=$dvalue_val['mkt_no'.$mkt_val['mkt_no'].'_sum'];?>
					</td>
					<? } ?>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? rows_page(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>