<?php
include_once '_common.php';

// cms 접근 권한
mb_cms_access();

include_once NM_ADM_PATH.'/_adm.lib.php';

if($head_title==""){ $head_title = $nm_config['cf_title']."-CMS"; }
include_once NM_ADM_PATH.'/_head.sub.php'; // 해더
include_once NM_ADM_PATH.'/_head.current.php'; // 현재 위치

// cms 접근 회원별 권한
mb_cms_level_access($nm_member, $_cms_current_folder);

?>