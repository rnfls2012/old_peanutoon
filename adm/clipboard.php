<?
include_once '_common.php'; // 공통

?>


<link rel="stylesheet" href="<?=NM_URL?>/css/clipboard.css">
<script src="<?=NM_URL?>/js/clipboard.min.js"></script>
<script src="<?=NM_URL?>/js/clipboard.tooltip.js"></script>
<style type="text/css">
.copy, .copys, .copy_btn, .purge{
	display: inline-block;
	font-size: 15px;
	font-family: Nanum Gothic,dotum;
	font-weight: bold;
	background: #fb6b6b;
	padding: 5px 10px;
	color: #fff;
	border-radius: 50px;
	vertical-align: top;
	margin-top:5px;
	cursor: pointer;
}
.copy_btn{background: #ad58c2;}

#copy_text{width:100%;}

.pop_toast {
	display: block;
	opacity: 0.8;
	position: fixed;
	padding: 7px 14px;
	background-color: #000;
	color: #fff;
	text-align: center;
	border-radius: 50em;
	-webkit-transition: all 0.3s ease-in-out;
	-moz-transition: all 0.3s ease-in-out;
	-o-transition: all 0.3s ease-in-out;
	-ms-transition: all 0.3s ease-in-out;
	transition: all 0.3s ease-in-out;
	z-index: 300;
}

</style>

<script>
var clipboard = new Clipboard('.copy', {
    target: function(trigger) {
        return trigger.previousElementSibling;
    }
});

clipboard.on('success',function(e) {
    e.clearSelection();
    showTooltip(e.trigger,'Copied!');
});

clipboard.on('error',function(e) {
    showTooltip(e.trigger,fallbackMessage(e.action));
});
</script>


<!-- purge api -->
<script type="text/javascript">
<!--
	var onToast;
	function toast(msg)
	{
		if (onToast)
		{
			onToast.remove();
			onToast = null;
		}
		onToast = $("<div class='pop_toast'>" + msg + "</div>").appendTo("body");
		onToast.css({ "left" : ($(window).width() - onToast.width()) / 2, "top" : $(window).height() / 2 });
		onToast.delay(600).fadeOut(200, function()
		{
			$(this).remove();
			onToast = null;
		});
	}

	function cdn_api_purge_send(){
		cdn_api_purge($('#copy_text_ajax').val());	
	}

	function cdn_api_purge(storagedata){
		var sub_list_access = $.ajax({
			url: nm_url +"/adm/cdn_purge_api.php",
			dataType: "json",
			type: "POST",
			data: { storagedata: storagedata},
			beforeSend: function( data ) {
				toast('purge 진행중 입니다.');
			}
		})
		sub_list_access.done(function( data ) {
			toast(data);
			setTimeout(location.reload.bind(location), 1000);
		});
		sub_list_access.fail(function( data ) {
			alert('purge 실패 관리자에게 문의해주세요');
		});
	}
//-->
</script>
<!-- purge api end -->
