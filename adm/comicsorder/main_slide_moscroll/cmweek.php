<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_week_list_val = num_eng_check(tag_get_filter($_REQUEST['week_list_val']));

/* 데이터 가져오기 */
$where_sales = " AND cm_service = 'y' AND cm_public_cycle > 0 AND cm_end='n' "; /* 충전 */
$where_comics = " AND cm_service = 'y' ";

$where_adult = "";
if($cmweek_adult == 'n'){
	$where_sales.= " AND cm_adult='n' ";
	$where_comics.= " AND cm_adult='n' ";
}

// 검색단어+ 검색타입
if($_s_text){
	$where_sales.= "AND c.cm_series like '%$_s_text%' ";
	$where_comics .= "AND cm_series like '%$_s_text%' ";
} // end if

$where_sales_r = $where_sales;

// 전주 각 요일 값 계산
/*
$_days = 7-intval($cmweek);
$week_days = date('w');
$_days+= $week_days;
$week_date = date('Y-m-d', strtotime('-'.$_days.' days', NM_SERVER_TIME));
*/

// 날짜
if($_s_date == ''){ $_s_date = NM_DATE_MON_M1; }
if($_e_date == ''){ $_e_date = NM_TIME_M1; }
if($_s_date && $_e_date){
	$where_sales.= date_year_month($_s_date, $_e_date, 'sl', $_s_hour, $_e_hour);
}

// 작품-요일별
$cmserial_cycle_in_arr = $sql_cm_public_cycle_in = $get_comics_cycle_fix = "";

if($_week_list_val == ''){ 
	$get_comics_cycle_fix = $cmweek; 
	$_week_list_val = $cmweek +1; 
}else{
	$get_comics_cycle_fix = $_week_list_val; 
}

if($_week_list_val != '0'){
	if($get_comics_cycle_fix == 0 || $get_comics_cycle_fix == '0'){ $get_comics_cycle_fix = 7; }
	$cmweek_cycle_in_arr = get_comics_cycle_fix($get_comics_cycle_fix);
	$sql_cm_public_cycle_in = " AND c.cm_public_cycle IN(".$cmweek_cycle_in_arr.") ";
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sl_won_sum"){ 
	$_order_field = "sl_won_sum"; 
	$_order_field_add = " , sl_open_sum "; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
	$_order_add= "desc"; 
}

// 요일 정렬시
if($_order_field == "sl_week"){
	$_order_field_add = " , sl.sl_year_month desc "; 
	$_order_add = " , sl.sl_day desc ";
}


$order_sales = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add." , sl_comics ASC ";

$sql_sales = "";
$field_sales = "		c.cm_adult, c.cm_big, c.cm_no, c.cm_adult,
					sl.sl_comics, c.cm_small, c.cm_series, c.cm_service, 

					sum(if(sl.sl_cash_point>0,sl_cash_point,0))as sl_cash_point_sum, 
					sum(if(sl.sl_cash_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_cash_point_cnt, 
					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won)as sl_cash_point_won_sum, 

					sum(if(sl.sl_point>0,sl_point,0))as sl_point_sum, 
					sum(if(sl.sl_cash_point=0 && sl.sl_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_point_cnt, 
					sum(floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_point_won_sum, 

					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_won_sum, 

					sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
					sum(sl.sl_open)as sl_open_sum 
							"; // 가져올 필드 정하기
$group_sales = " group by c.cm_no ";
$limit_sales = " LIMIT 0, 20 ";



if($_s_text != '') {
	$sql_sales = " select *, cm_no as sl_comics from comics where 1 $where_cr_class $where_comics order by cm_no DESC LIMIT 0, 100";
} else {
	$sql_sales = "	select $field_sales 
							FROM comics c 
							LEFT JOIN sales sl ON sl.sl_comics = c.cm_no 
							LEFT JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type 							
							where 1 $where_cr_class $where_sales $sql_cm_public_cycle_in
							$group_sales 
							$order_sales 
							$limit_sales";
}

$result = sql_query($sql_sales);
$row_size = sql_num_rows($result);

// 순위 통계 비교 및 적용
$row_data  = array();
$no = 0;
$gap_p = 20;
$gap_m = -20;
//'n','ff','f','m','p','pp'
while ($row = sql_fetch_array($result)) {
	/*
	$no++;
	// 순위차이와 순위표기
	$cr_ranking_gap = '';
	$cr_ranking_mark = 'n';
	foreach($row_data_r as $row_r_key => $row_r_val){
		$rank = $row_r_key+1;
		if($row_r_val['sl_comics'] == $row['sl_comics']){
			$cr_ranking_gap = $rank - $no;
			if($cr_ranking_gap == 0){
				$cr_ranking_mark = 'm';
			}else if($cr_ranking_gap > 0){
				$cr_ranking_mark = 'f';
				if($cr_ranking_gap > $gap_p){ $cr_ranking_mark = 'ff'; }
			}else if($cr_ranking_gap < 0){
				$cr_ranking_mark = 'p';
				if($cr_ranking_gap < $gap_m){ $cr_ranking_mark = 'pp'; }
			}
		}
	}
	$row['cr_ranking_mark'] = $cr_ranking_mark;
	$row['cr_ranking_gap'] = $cr_ranking_gap;

	// 순위 제외시
	if($_s_type == '1') { $row['sl_comics'] = $row['cm_no']; }
	*/
	array_push($row_data, $row);
}


$cms_class = $cmweek;
$page_adult = '';
if($cmweek_adult == 'n') { 
	$page_adult = '-청소년'; 
	$cms_class.= '_t';
}

$page_title = $week_list[$cms_class];
$sql_cms_where = "";
$sql_cms_where.= " AND cms_class='".$cms_class."' ";

$where_adult = "";
if($cmweek_adult == 'n'){
	$sql_where.= " AND cm_adult='n' ";
	$sql_cms_where.= " AND cms_adult='n' ";
}


$sql_cms = "SELECT * FROM comics_main_slide_moscroll WHERE 1 $sql_cms_where ORDER BY cms_ranking ASC LIMIT 0, 100;";
$result_cms = sql_query($sql_cms);
$row_size_cms = sql_num_rows($result_cms);
$row_data_cms_comics  = $row_data_cms  = array();
while ($row_cms = mysql_fetch_array($result_cms)) {
	array_push($row_data_cms, $row_cms);
	array_push($row_data_cms_comics, $row_cms['cms_comics']);
}

// 보기 카운터
$show_list = 20;

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$cms_head_title = '업데이트 작품(main) '.$page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
// 페이지 처리 안함

$small_js = str_replace("cms_comics.js", "cms_comics_small.js", $_cms_js);
$small_update = str_replace("main_slide_moscroll.update.php", "cmweek.update.php", $_cms_folder_update);
?>
<style type="text/css">
	#lnbs ul li:nth-child(<?=count($week_list)+1?>) {clear: both;}
	#lnbs_end {margin-bottom: 54px;}
</style>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<!-- <script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script> -->
<script type="text/javascript" src="<?=$small_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<!--
	<div>
		<strong><?=$page_title?> 이벤트 : 총 <?=number_format($total_epromotion);?> 건</strong>
	</div>
	-->
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="ranking_search_form" id="ranking_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return ranking_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<h3>날짜&요일 순위 검색</h3>
					<div class="cs_calendar_btn">
						<label for="s_date">시작일</label>
						<input type="text" name="s_date" value="<?=$_s_date;?>" id="s_date" class="d_date readonly" readonly /> 
						<label for="s_date_view">일</label>&nbsp;
						<!-- <?=set_hour('s_hour', $_s_hour, '시작시간');?> -->

						<label for="e_date">종료일</label>
						<input type="text" name="e_date" value="<?=$_e_date;?>" id="e_date" class="d_date readonly" readonly />
						<label for="e_date_view">일</label>&nbsp;
						<!-- <?=set_hour('e_hour', $_e_hour, '종료시간');?> -->
					</div>
					<div style="margin-top:10px;">
						<label for="s_date">작품-요일별</label>
						<?	array_unshift($week_list, "전체");
						tag_selects($week_list, "week_list_val", $_week_list_val, 'n'); ?>
					</div>
				</div>
				<div class="cs_search_btn">
					<h3>작품검색( ↑↑ 위 검색 조건 무시 )</h3>
					<input type="text" id="s_text" name="s_text" placeholder="코믹스명을 입력해주세요." value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit cmweek_submit">
				<input type="submit" style="height: 186px;"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="comics_result" class="clear">
	<div class="comics_ranking_list">

		<!-- //////////////////////// 순위 통계 등급 //////////////////////// -->

		<div class="comics_sales">
		<h3>검색 결과</h3>
			<ul id="sortable1" class="connectedSortable">
			<? $comics_ranking_no = 0;
			foreach($row_data as $dvalue_key => $dvalue_val){
				if($dvalue_key == $show_list){ break; }
				$comics_ranking_no++;

				/* 컨텐츠 정보 */
				$get_comics = get_comics($dvalue_val['sl_comics']);
				
				/* 컨텐츠 제목 */
				$db_series = $get_comics['cm_series'];

				/* 이미지 표지 */
				$cm_cover_sub_url = img_url_para($get_comics['cm_cover_sub'], $get_comics['cm_reg_date'], $get_comics['cm_mod_date'],75,75);

				/* 컨텐츠 등급 */
				$db_adult = $d_adult[$get_comics['cm_adult']];

				/* 순위차이 */
				$db_ranking_gap = $dvalue_val['cms_ranking_gap'];

				/* 순위표기 */
				$db_ranking_mark = $d_ranking_mark[$dvalue_val['cms_ranking_mark']];

				/* 순위표기 아이콘 */
				$db_ranking_mark_icon = $d_ranking_mark_icon[$dvalue_val['cms_ranking_mark']];

				/* 저장할 정보 */
				$db_cms_adult = $get_comics['cm_adult'];

				/* 컨텐츠 완결 */
				$db_cm_end = $d_cm_end[$get_comics['cm_end']];

				/* 컨텐츠 주기 */
				$db_cm_public_period = $d_cm_public_period[$get_comics['cm_public_period']];

				/* 요일 */
				$cm_public_cycle_txt = cs_get_comics_week($get_comics);

				$db_cms_lock = 'n';
				$db_cms_comics = $get_comics['cm_no'];
				$db_cms_big = $get_comics['cm_big'];

				$db_cms_ranking_gap = $dvalue_val['cms_ranking_gap'];
				if($dvalue_val['cms_ranking_gap'] == 0){ $db_cms_ranking_gap = ''; }
				$db_cms_ranking_mark = $dvalue_val['cms_ranking_mark'];

				/* 전체 순위 있는 건 고정 */
				$ui_state_disabled = "";
				if(in_array($db_cms_comics, $row_data_cms_comics)){
					$ui_state_disabled = "ui-state-disabled";					
				}
				
			?>
				<li id="row_data_<?=$db_cms_comics?>" class="ui-state-default <?=$ui_state_disabled;?>">
					<dl>
						<dt><img src="<?=$cm_cover_sub_url;?>" alt="<?= $db_series;?>표지" width="75" height="75" /></dt>
						<dd>
							<p class="bold">No<?=$comics_ranking_no;?> (등급: <?= $db_adult;?> / 요일: <?= $cm_public_cycle_txt;?>) 
								<i class="fa fa-<?=$db_ranking_mark_icon?>" aria-hidden="true"></i>
								<span><?=$db_ranking_gap?></span>
							</p>
							<p><?= $db_series;?></p>
							<p>정보 : <?= $db_cm_end;?> / <?= $db_cm_public_period;?></p>
						</dd>
					</dl>
					<i class="fa fa-times-circle" aria-hidden="true"></i>
					<!-- <i class="ranking_lock fa fa-unlock" aria-hidden="true"></i> -->
					<input type="hidden" name="cms_adult[]" class="cms_adult" value="<?=$db_cms_adult?>">
					<input type="hidden" name="cms_lock[]" class="cms_lock" value="<?=$db_cms_lock?>">
					<input type="hidden" name="cms_comics[]" class="cms_comics" value="<?=$get_comics['cm_no']?>">
					<input type="hidden" name="cms_big[]" class="cms_big" value="<?=$db_cms_big?>">

					<input type="hidden" name="cms_ranking_gap[]" class="cms_ranking_gap" value="<?=$db_cms_ranking_gap?>">
					<input type="hidden" name="cms_ranking_mark[]" class="cms_ranking_mark" value="<?=$db_cms_ranking_mark?>">
				</li>
			<?}?>
			<li class="ui-state-default ui-state-disabled">COMICS</li>
			</ul>
		</div>

		<!-- //////////////////////// 전체순위 //////////////////////// -->

		<div class="comics_ranking">
		<!--<form name="comics_ranking_form" id="comics_ranking_form" method="post" action="<?=$_cms_folder_update;?>" onsubmit="return comics_ranking_submit();">-->
			<form name="comics_ranking_form" id="comics_ranking_form" method="post" action="<?=$small_update;?>" onsubmit="return comics_ranking_submit();">
					<input type="hidden" name="cms_class" id="cms_class" value="<?=$cms_class?>">

					<input type="hidden" name="s_text" id="s_text" value="<?=$s_text?>">

				<h3>
					<?=$page_title?>
					<input type="submit" class="" value="적용" id="ranking_submit">
				</h3>
				<ul id="sortable2" class="connectedSortable">
				<? $comics_ranking_no = 0;
				foreach($row_data_cms as $dvalue_key => $dvalue_val){
					if($dvalue_key == $show_list){ break; }
					$comics_ranking_no++;

					/* 컨텐츠 정보 */
					$get_comics = get_comics($dvalue_val['cms_comics']);
					
					/* 컨텐츠 제목 */
					$db_series = $get_comics['cm_series'];

					/* 이미지 표지 */
					$cm_cover_sub_url = img_url_para($get_comics['cm_cover_sub'], $get_comics['cm_reg_date'], $get_comics['cm_mod_date'],'cm_cover_sub','tn75x75');

					/* 컨텐츠 등급 */
					$db_adult = $d_adult[$get_comics['cm_adult']];

					/* 순위차이 */
					$db_ranking_gap = $dvalue_val['cms_ranking_gap'];
					if($dvalue_val['cms_ranking_gap'] == 0){ $db_ranking_gap = ''; }

					/* 순위표기 */
					$db_ranking_mark = $d_ranking_mark[$dvalue_val['cms_ranking_mark']];

					/* 순위표기 아이콘 */
					$db_ranking_mark_icon = $d_ranking_mark_icon[$dvalue_val['cms_ranking_mark']];

					/* 저장할 정보 */
					$db_cms_adult = $get_comics['cm_adult'];

					/* 컨텐츠 완결 */
					$db_cm_end = $d_cm_end[$get_comics['cm_end']];

					/* 컨텐츠 주기 */
					$db_cm_public_period = $d_cm_public_period[$get_comics['cm_public_period']];

					/* 요일 */
					$cm_public_cycle_txt = cs_get_comics_week($get_comics);

					$db_cms_lock = $dvalue_val['cms_lock'];
					$db_cms_lock_icon = 'fa-unlock';
					if($db_cms_lock == 'y'){
						$db_cms_lock_icon = 'fa-lock';
					}
					$db_cms_comics = $dvalue_val['cms_comics'];
					$db_cms_big = $get_comics['cm_big'];

					$db_cms_ranking_gap = $dvalue_val['cms_ranking_gap'];
					$db_cms_ranking_mark = $dvalue_val['cms_ranking_mark'];				
				?>
					<li id="row_data_<?=$db_cms_comics?>" class="ui-state-default">
						<dl>
							<dt><img src="<?=$cm_cover_sub_url;?>" alt="<?= $db_series;?>표지" width="75" height="75" /></dt>
							<dd>
								<p class="bold">No<?=$comics_ranking_no;?> (등급: <?= $db_adult;?> / 요일: <?= $cm_public_cycle_txt;?>) 
									<i class="fa fa-<?=$db_ranking_mark_icon?>" aria-hidden="true"></i>
									<span><?=$db_ranking_gap?></span>
								</p>
								<p><?= $db_series;?></p>
							<p>정보 : <?= $db_cm_end;?> / <?= $db_cm_public_period;?></p>
							</dd>
						</dl>
						<i class="fa fa-times-circle" aria-hidden="true"></i>
						<!-- <i class="ranking_lock fa <?=$db_cms_lock_icon?>" aria-hidden="true"></i> -->
						<input type="hidden" name="cms_adult[]" class="cms_adult" value="<?=$db_cms_adult?>">
						<input type="hidden" name="cms_lock[]" class="cms_lock" value="<?=$db_cms_lock?>">
						<input type="hidden" name="cms_comics[]" class="cms_comics" value="<?=$db_cms_comics?>">
						<input type="hidden" name="cms_big[]" class="cms_big" value="<?=$db_cms_big?>">

						<input type="hidden" name="cms_ranking_gap[]" class="cms_ranking_gap" value="<?=$db_cms_ranking_gap?>">
						<input type="hidden" name="cms_ranking_mark[]" class="cms_ranking_mark" value="<?=$db_cms_ranking_mark?>">
					</li>
				<?}?>
				<li class="ui-state-default ui-state-disabled">COMICS</li>
				</ul>
			</form>
		</div>
	</div>
</section><!-- comics_result -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>