<?
include_once '_common.php'; // 공통

/* 화별 link */
$get_comics = get_comics($_cm_no);
$db_comics_url_para = "&cm_big=".$get_comics['cm_big']."&cm_no=".$get_comics['cm_no']."&s_date=".$_s_date."&e_date=".$_e_date;
$db_comics_date_url = $_cms_self."?sl_mode=date".$db_comics_url_para;

/* PARAMITER */
$cm_big; // 코믹스 대분류
$cm_no; // 코믹스 번호
$s_date; // 시작 날짜
$e_date; // 끝 날짜

/* 데이터 가져오기 */
$where_sales = " and se.se_comics = ".$cm_no." "; /* 충전 */

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

// 파트너일 경우
$sql_mb_cm_cp	= mb_cm_cp_sql($nm_member);		// 파트너 SQL
$_e_date		= mb_partner_e_date($nm_member, $_e_date);	// 종료일 현재 -1일 적용
$where_sales.= $sql_mb_cm_cp;

// 날짜
if($_s_date && $_e_date){
	$where_sales.= date_year_month($_s_date, $_e_date, 'se.se');
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "se.se_episode"){ 
	$_order_field = "se.se_episode"; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc";  
}

$order_sales = "order by ".$_order_field." ".$_order;

$sql_sales = "";
/*
					sum(if(se.se_cash_point>0 && se.se_point=0,se_cash_point,0))as se_cash_sum, 
					sum(if(se.se_cash_point>0 && se.se_point=0,se.se_all_pay+se_pay,0))as se_cash_point_cnt, 
					sum(if(se.se_cash_point>0 && se.se_point=0,se_cash_point,0)* cup.cup_won)as se_cash_point_won, 

					sum(if(se.se_point>0,se_point,0))as se_point_sum, 
					sum(if(se.se_cash_point=0 && se.se_point>0,se.se_all_pay+se.se_pay,0))as se_point_cnt, 
					sum(floor(if(se.se_point>0,se_point,0)*cup_won/".NM_POINT."))as se_point_won_sum, 

					sum(if(se.se_cash_point>0 && se.se_point=0,se_cash_point,0)*cup_won+floor(if(se.se_point>0,se_point,0)*cup_won/".NM_POINT."))as se_won_sum, 

					sum(se.se_all_pay+se.se_pay)as se_pay_sum, 
					sum(se.se_open)as se_open_sum, 
*/
/* cash_point와 point 함계 결제 건수는 cash_point 건수에 포함 */
$field_sales = "	ce.*, c.cm_up_kind, c.cm_series, 
					se.se_episode, se_day, se.se_chapter, 

					sum(if(se.se_cash_point>0,se_cash_point,0))as se_cash_sum, 
					sum(if(se.se_cash_point>0,se.se_all_pay+se_pay,0))as se_cash_point_cnt, 
					sum(if(se.se_cash_point>0,se_cash_point,0)* cup.cup_won)as se_cash_point_won, 

					sum(if(se.se_point>0,se_point,0))as se_point_sum, 
					sum(if(se.se_cash_point=0 && se.se_point>0,se.se_all_pay+se.se_pay,0))as se_point_cnt, 
					sum(floor(if(se.se_point>0,se_point,0)*cup_won/".NM_POINT."))as se_point_won,  

					sum(if(se.se_cash_point>0,se_cash_point,0)*cup_won+floor(if(se.se_point>0,se_point,0)*cup_won/".NM_POINT."))as se_won_sum, 

					sum(se.se_all_pay+se.se_pay)as se_pay_sum, 
					sum(se.se_open)as se_open_sum, 

					max(CONCAT(se.se_year_month,'-',se.se_day)) as se_date "; // 가져올 필드 정하기

$group_sales = " group by se.se_episode ";
$limit_sales = "";

$sql_sales = "	select $field_sales 
						FROM sales_episode_".$cm_big." se
						LEFT JOIN comics_episode_".$cm_big." ce ON se.se_episode = ce.ce_no 
						LEFT JOIN config_unit_pay cup ON se.se_cash_type = cup.cup_type 
						LEFT JOIN comics c ON se.se_comics = c.cm_no
						where 1 $where_sales 
						$group_sales 
						$order_sales 
						$limit_sales";						
$result = sql_query($sql_sales);
$row_size = sql_num_rows($result);

/* 검색 전체 구하기 - group by 빼고 sum, count함수 이용 */
$sql_sales_s_all = "	select $field_sales 
						FROM sales_episode_".$cm_big." se 
						LEFT JOIN comics_episode_".$cm_big." ce ON se.se_episode = ce.ce_no 
						LEFT JOIN config_unit_pay cup ON se.se_cash_type = cup.cup_type 
						LEFT JOIN comics c ON se.se_comics = c.cm_no
						where 1 $where_sales 
						$order_sales 
						$limit_sales";

$row_sales_s_all = sql_fetch($sql_sales_s_all );
/*
$s_all_cash_point_sum = $row_sales_s_all['se_cash_sum'];
$s_all_cash_point_won_sum = $row_sales_s_all['se_cash_point_won'];
$s_all_point_sum = $row_sales_s_all['se_point_sum'];
$s_all_point_won_sum = $row_sales_s_all['se_point_won'];

$s_all_sum = number_format($s_all_cash_point_won_sum)." 원";
$s_all_cash_point_sum = $row_sales_s_all['se_cash_sum']; // 땅콩 합계
$s_all_cash_point_won_sum = $row_sales_s_all['se_cash_point_won']; // 땅콩 금액 합계
$s_all_point_sum = $row_sales_s_all['se_point_sum']; // 미니땅콩 합계
$s_all_point_won_sum = $row_sales_s_all['se_point_won']; // 미니땅콩 금액 합계

$s_all_buy_sum = number_format($row_sales_s_all['se_cash_point_cnt'])." 건";

$s_all_sum = ($s_all_cash_point_sum); // 땅콩
$s_all_won_sum = number_format(($s_all_cash_point_won_sum))." 원(땅콩)"; // 땅콩 금액 + 미니땅콩 금액 (원)
*/

$row_sales_s_all = sql_fetch($sql_sales_s_all );
$s_all_cash_point_sum = $row_sales_s_all['se_cash_sum']; // 땅콩 합계
$s_all_cash_point_won_sum = $row_sales_s_all['se_cash_point_won']; // 땅콩 금액 합계
$s_all_point_sum = $row_sales_s_all['se_point_sum']; // 미니땅콩 합계
$s_all_point_won_sum = $row_sales_s_all['se_point_won']; // 미니땅콩 금액 합계

$s_all_buy_sum = number_format($row_sales_s_all['se_pay_sum'])." 건";

$s_all_sum = ($s_all_cash_point_sum); // 땅콩 + 미니땅콩
$s_all_won_sum = number_format(($s_all_cash_point_won_sum + $s_all_point_won_sum))." 원(땅콩+미니땅콩)"; // 땅콩 금액 + 미니땅콩 금액 (원)

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('화 번호','se_episode', 0));
array_push($fetch_row, array('코믹스 명+화','se_title', 0));

/* cash_point */
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko']."(원)", 'se_cash_point_won', 4));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko']."(".$nm_config['cf_cash_point_unit'].")", 'se_cash_sum', 4));

/* point */
array_push($fetch_row, array($nm_config['cf_point_unit_ko']."(원)", 'se_point_won', 3));
array_push($fetch_row, array($nm_config['cf_point_unit_ko']."(".$nm_config['cf_point_unit'].")", 'se_point_sum', 3));

array_push($fetch_row, array('판매 수','se_buy_sum', 0));
array_push($fetch_row, array('열람 수','se_open_sum', 0));
array_push($fetch_row, array('열람/판매 날짜','se_date', 0));

// css 클래스 접두사
$css_suffix = "coi_";

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "코믹스별 사용";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<script type="text/javascript">

</script>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
			<input type="hidden" name="cm_big" id="cm_big" value="<?=$cm_big;?>">
			<input type="hidden" name="cm_no" id="cm_no" value="<?=$cm_no;?>">
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<? if($nm_member['mb_class'] == 'a'){ ?>
	<div class="result_explain">
		<ul>
			<li>
				<?=$nm_config['cf_cash_point_unit_ko']?>와 <?=$nm_config['cf_point_unit_ko']?> 함께 결제 건수는 
				<?=$nm_config['cf_cash_point_unit_ko']?>-건수에 포함 됩니다.(관리지만 보이는 메세지 입니다.)
			</li>
		</ul>
	</div>
	<? } ?>
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 - 총금액: <?=$s_all_won_sum;?> / 총 판매 수: <?=$s_all_buy_sum;?></strong>
		<a class="a_btn date" href="<?=$db_comics_date_url?>">날짜별</a>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $css_suffix.$fetch_val[1];
					if($fetch_val[2] == 4){
						$th_class.= " ".$css_suffix."sl_cash_point_bgcolor";
					}
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?
				$se_cash_point_won	= 0; // 땅콩(원)
				$se_cash_sum		= 0; // 땅콩(P)
				$se_cash_point_cnt	= 0; // 땅콩판매 수
				
				$se_point_won		= 0; // 미니땅콩(원)
				$se_point_sum		= 0; // 미니땅콩(P)
				$se_point_cnt		= 0; // 미니판매 수

				$se_open_sum		= 0; // 열람 수
				$se_pay_sum			= 0; // 판매 수

				while($row = sql_fetch_array($result)) { 
					
					$ce_up_kind = '화';
					if($row['cm_up_kind'] == '1'){ $ce_up_kind = '권'; }				
					if($row['ce_chapter'] > 0){
						$se_title = $row['cm_series']." ".$row['se_chapter'].$ce_up_kind;
						if($row['ce_title'] != ''){ $se_title = $row['cm_series']." ".$row['se_chapter'].$ce_up_kind." - ".$row['ce_title']; }	
					}else{
						$se_title = $row['ce_notice'];
						if($row['ce_notice'] == '' && $row['ce_outer'] == 'y'){
							$se_title = "외전".$row['ce_outer_chapter'].$ce_up_kind;
						}
					}
					$se_title = stripslashes($se_title);
					
					$se_cash_point_won	+= intval($row['se_cash_point_won']);	// 땅콩(원)
					$se_cash_sum		+= intval($row['se_cash_sum']);			// 땅콩(P)
					$se_cash_point_cnt	+= intval($row['se_cash_point_cnt']);	// 판매 수

					$se_point_won	+= intval($row['se_point_won']);			// 미니땅콩(원)
					$se_point_sum		+= intval($row['se_point_sum']);		// 미니땅콩(P)
					$se_point_cnt	+= intval($row['se_point_cnt']);			// 미니판매 수


					$se_open_sum		+= intval($row['se_open_sum']);			// 열람 수
					$se_pay_sum		+= intval($row['se_pay_sum']);				// 판매 수
				
				?>
				<tr>
					<td class="<?=$css_suffix.$fetch_row[0][1]?> text_center"><?=$row['se_episode'];?></td>
					<td class="<?=$css_suffix.$fetch_row[1][1]?> text_center"><?=$se_title;?></td>
					<td class="<?=$css_suffix.$fetch_row[2][1]?> text_center cm_sl_cash_point_bgcolor"><?=number_format($row['se_cash_point_won'])." 원";?></td>
					<td class="<?=$css_suffix.$fetch_row[3][1]?> text_center cm_sl_cash_point_bgcolor"><?=number_format($row['se_cash_sum'])." ".$nm_config['cf_cash_point_unit'];?></td>
					<td class="<?=$css_suffix.$fetch_row[4][1]?> text_center cm_sl_point_bgcolor"><?=number_format($row['se_point_won'])." 원";?></td>
					<td class="<?=$css_suffix.$fetch_row[5][1]?> text_center cm_sl_point_bgcolor"><?=number_format($row['se_point_sum'])." ".$nm_config['cf_cash_point_unit'];?></td>
					<td class="<?=$css_suffix.$fetch_row[6][1]?> text_center"><?=number_format($row['se_pay_sum']);?></td>
					<td class="<?=$css_suffix.$fetch_row[7][1]?> text_center"><?=number_format($row['se_open_sum']);?></td>
					<td class="<?=$css_suffix.$fetch_row[8][1]?> text_center"><?=$row['se_date'];?></td>
				</tr>
				<?}?>
				<tr class="se_total_tr">
					<td colspan="2" class="se_total">총 합</td>
					<td class="<?=$css_suffix.$fetch_row[2][1]?> text_center cm_sl_cash_point_bgcolor"><?=number_format($se_cash_point_won)." 원";?></td>
					<td class="<?=$css_suffix.$fetch_row[3][1]?> text_center cm_sl_cash_point_bgcolor"><?=number_format($se_cash_sum)." ".$nm_config['cf_cash_point_unit'];?></td>
					<td class="<?=$css_suffix.$fetch_row[4][1]?> text_center cm_sl_point_bgcolor"><?=number_format($se_point_won)." 원";?></td>
					<td class="<?=$css_suffix.$fetch_row[5][1]?> text_center cm_sl_point_bgcolor"><?=number_format($se_point_sum)." ".$nm_config['cf_cash_point_unit'];?></td>
					<td class="<?=$css_suffix.$fetch_row[6][1]?> text_center"><?=number_format($se_pay_sum);?></td>
					<td class="<?=$css_suffix.$fetch_row[7][1]?> text_center"><?=number_format($se_open_sum);?></td>
					<td class="<?=$css_suffix.$fetch_row[8][1]?> text_center">&nbsp;</td>
				</tr>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>