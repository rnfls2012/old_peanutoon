<?
include_once '_common.php'; // 공통

/* PARAMITER 
maker_name // 제공사이름
*/


/* 데이터 가져오기 */
$where_sales = ""; /* 충전 */

// 카테고리(단행본, 웹툰, 단행본웹툰)
if($_big) {
	$where_sales .= " AND sas_big = ".$_big." ";
} // end if

if($nm_member['mb_class'] == 'p' && $nm_member['mb_level'] == '14') {
	$p_sql = "select * from comics where cm_provider = '".$nm_member['mb_class_no']."'";
	$p_result = sql_query($p_sql);
	$p_comics_arr = array();
	while($row = sql_fetch_array($p_result)) {
		array_push($p_comics_arr,$row['cm_no']);
	} // end while
	$p_comics_list = implode($p_comics_arr, ", ");
	$where_sales.= " and sas.sas_comics in (".$p_comics_list.") ";
} // end if


if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }
if($_s_date && $_e_date){
	$where_sales.= date_year_month($_s_date, $_e_date, 'sas');
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "all_sum" || $mb_sex == ""){ 
	$_order_field = "all_sum"; 
	$_order_field_add = " , sas_comics "; 
}
if($mb_sex == "m"){
	$_order_field = "m_sum"; 
	$_order_field_add = " , sas_comics "; 
}
if($mb_sex == "w"){
	$_order_field = "w_sum"; 
	$_order_field_add = " , sas_comics "; 
}
if($mb_sex == "n"){
	$_order_field = "n_sum"; 
	$_order_field_add = " , sas_comics "; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
	$_order_add= "desc"; 
}

// 요일 정렬시
if($_order_field == "sas_week"){
	$_order_field_add = " , sas.sas_year_month desc "; 
	$_order_add = " , sas.sas_day desc ";
}

$order_sales = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;
$sql_sales = "";

if($mb_sex == 'm'){
	$field_sales ="  sas_year_month, 
							sas_day, 
							sas_week, 
							sas_comics, 

							sum(sas_man)as m_sum, 
							sum(sas_man10)as m10_sum, 
							sum(sas_man20)as m20_sum, 
							sum(sas_man30)as m30_sum, 
							sum(sas_man40)as m40_sum, 
							sum(sas_man50)as m50_sum, 
							sum(sas_man60)as m60_sum";
	$where_sales .= " AND sas_man > 0 ";
} elseif($mb_sex == 'w'){
	$field_sales =" sas_year_month, 
							sas_day, 
							sas_week, 
							sas_comics, 

							sum(sas_woman)as w_sum, 
							sum(sas_woman10)as w10_sum, 
							sum(sas_woman20)as w20_sum, 
							sum(sas_woman30)as w30_sum, 
							sum(sas_woman40)as w40_sum, 
							sum(sas_woman50)as w50_sum, 
							sum(sas_woman60)as w60_sum";
	$where_sales .= " AND sas_woman > 0 ";
} elseif($mb_sex == 'n'){
	$field_sales =" sas_year_month, 
							sas_day, 
							sas_week, 
							sas_comics, 

							sum(sas_nocertify)as n_sum";
	$where_sales .= " AND sas_nocertify > 0 ";
} else {
$field_sales = "   sas_year_month, 
							sas_day, 
							sas_week, 
							sas_comics, 
							sum(sas_man)as m_sum, 
							sum(sas_woman)as w_sum, 
							sum(sas_nocertify)as n_sum, 
							
							(sum(sas_man)+sum(sas_woman)+sum(sas_nocertify))as all_sum, 
							
							sum(sas_man10)as m10_sum, 
							sum(sas_man20)as m20_sum, 
							sum(sas_man30)as m30_sum, 
							sum(sas_man40)as m40_sum, 
							sum(sas_man50)as m50_sum, 
							sum(sas_man60)as m60_sum, 

							sum(sas_woman10)as w10_sum, 
							sum(sas_woman20)as w20_sum, 
							sum(sas_woman30)as w30_sum, 
							sum(sas_woman40)as w40_sum, 
							sum(sas_woman50)as w50_sum, 
							sum(sas_woman60)as w60_sum, 

							(sum(sas_man10) + sum(sas_woman10))as all10_sum, 
							(sum(sas_man20) + sum(sas_woman20))as all20_sum, 
							(sum(sas_man30) + sum(sas_woman30))as all30_sum, 
							(sum(sas_man40) + sum(sas_woman40))as all40_sum, 
							(sum(sas_man50) + sum(sas_woman50))as all50_sum, 
							(sum(sas_man60) + sum(sas_woman60))as all60_sum
							"; // 가져올 필드 정하기
}
$group_sales = " group by sas_comics ";
$limit_sales = "";

$sql_sales = "	select $field_sales 
						FROM sales_age_sex sas 
						where 1 $where_sales 
						$group_sales 
						$order_sales 
						$limit_sales";

$result = sql_query($sql_sales);
$row_size = sql_num_rows($result);

/* 검색 전체 구하기 - group by 빼고 sum, count함수 이용 */
$sql_sales_s_all = "select $field_sales 
						FROM sales_age_sex sas 
						where 1 
						$where_sales 
						$order_sales 
						$limit_sales";
$db_s_all_row = $sql_sales_s_all_row = sql_fetch($sql_sales_s_all );

$s_all_m_sum = number_format($sql_sales_s_all_row['m_sum']).' 건';
$s_all_w_sum = number_format($sql_sales_s_all_row['w_sum']).' 건';
$s_all_n_sum = number_format($sql_sales_s_all_row['n_sum']).' 건';
$s_all_all_sum = number_format($sql_sales_s_all_row['all_sum']).' 건';


// 성별&연령별 재구매
$age_arr = array('', 10,20,30,40,50,60);

/*
// 미인증
$s_all_n_sum = $sql_sales_s_all_row['n_sum'];
if($s_all_n_sum == 0){ $s_all_n_sum = 1; } // 성별&연령별 재구매
$s_n_sum_repay_pct = round((intval($sql_sales_s_all_row['n_repay_sum']) / $s_all_n_sum * 1000) / 10, 1)."%";
*/

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('코믹스번호','sas_comics',2));
array_push($fetch_row, array('코믹스명','cm_series',2));

foreach($age_arr as $age_key => $age_val){
	$age_field = "".$age_val;
	$age_text = $age_val."대";
	if($age_val == ''){
		$age_text = "연령전체";
		$age_field = "all";
	}
	if($age_val == 60){
		$age_text = $age_val."대이상";
	}

	array_push($fetch_row, array($age_text, $age_field, $age_key));
}
array_push($fetch_row, array('미인증','nocertify',7));

//die(print_r($fetch_row));

// css 클래스 접두사
$css_suffix = "csa_";

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$sex_arr = array(array('m','남자','man'),array('w','여자','woman'));

$page_title = "일별 사용 성별&연령별";
$cms_head_title = $page_title;

if($_mode == 'excel') {
	include_once $_cms_folder_top_path.'/excel/comics_sex_age_excel.php';
}

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;

include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?>"/>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
	<? if($mb_sex || $big){ ?>
	<div class="write">
		<input type="button" class="excel_down" value="<?=$page_title?> 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?<?=$_nm_paras;?>&mode=excel'"/>
	</div>
	<? } ?>
</section>

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($nm_config['cf_big'], "big", $_big, 'y', '전체', ''); ?>
					<?	tag_selects($d_mb_sex, "mb_sex", $mb_sex, 'y', '전체성별' ,''); ?>
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_sex_age_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result" class="csa_sex_age_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 - 총건수: 
		<?
			if($mb_sex==''){ echo $s_all_all_sum; }
			if($mb_sex=='m'){ echo $s_all_m_sum; }
			if($mb_sex=='w'){ echo $s_all_w_sum; }
			if($mb_sex=='n'){ echo $s_all_n_sum; }
		?>
		</strong>
		<?if($s_all_all_sum != ''){?>
			<table>
				<tr>
					<th class="topleft">성별</th>
					<? foreach($age_arr as $age_key => $age_val){ 
						$age_text = $age_val."대";
						if($age_val == ''){
							$age_text = "연령전체";
						}
						if($age_val == 60){
							$age_text = $age_val."대이상";
						}
					?>
						<th><?=$age_text;?></th>
					<?}?>
						<th class="topright">미인증</th>
				</tr>
				<tr>
					<th class="man">남자</th>
					<? foreach($age_arr as $age_key => $age_val){ ?>
						<td class="man"><?=number_format($db_s_all_row['m'.$age_val.'_sum']);?> 건</td>
					<?}?>
					<? /* 미인증 */ ?>
					<td class="no_certify"><?=number_format($db_s_all_row['n_sum']);?> 건</td>
				</tr>
				<tr>
					<th class="woman">여자</th>
					<? foreach($age_arr as $age_key => $age_val){ ?>
						<td class="woman"><?=number_format($db_s_all_row['w'.$age_val.'_sum']);?> 건</td>
					<?}?>
				</tr>
			</table>
		<?}?>
	</h3>
	<div id="cr_bg">		
		<h4 class="all_sex">전체</h4>
		<div id="cr_bg">
			<table>
				<thead id="cr_thead">
					<tr>
					<?
					//정렬표기
					$order_giho = "▼";
					$th_order = "desc";
					if($_order == 'desc'){ 
						$order_giho = "▲"; 
						$th_order = "asc";
					}
					foreach($fetch_row as $fetch_key => $fetch_val){

						$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
						if($fetch_val[2] == 1){
							$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
							$th_ahref_e = '</a>';
						}
						if($fetch_val[1] == $_order_field){
							$th_title_giho = $order_giho;
						}
						$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
						$th_class = $css_suffix.$fetch_val[1];

						// 배열의 key순으로 설정 - 171226
						$rowspan = $colspan = $th_class_btm_w1 = $th_class_color = $thead_tr = "";
						switch($fetch_val[2]){
							case 7 :
								if($mb_sex=='m' || $mb_sex=='w'){ $display = "style='display: none;'"; }
							case 6 : 
							case 5 : 
							case 4 : 
							case 2 :
							case 1 : $th_title = $fetch_val[0]; //해당 배열의 자리에 a href 추가 되었으므로 덮어씀.
							case 0 :
								break;
						}
					?>
					
						<?=$thead_tr?>
						<th <?=$display?> class="<?=$th_class?> <?=$th_class_color?> <?=$th_class_btm_w1?>"><?=$th_title?></th>
					<?}?>
					</tr>
				</thead>
				<tbody>
					<?
					foreach($row_data as $dvalue_key => $dvalue_val){
						/* 코믹스번호 */
						$db_sas_comics = $dvalue_val['sas_comics'];

						/* 코믹스명 */
						$comics_row = get_comics($dvalue_val['sas_comics']);
						$db_cm_series = $comics_row['cm_series'];

						/* 미인증 */
						$n_age_sum_txt = number_format($dvalue_val['n_sum']).'건';

					?>
					<tr>
						<td class="<?=$css_suffix?><?=$fetch_row[0][1]?> btm_not text_center"><strong><?=$db_sas_comics;?></strong></td>
						<td class="<?=$css_suffix?><?=$fetch_row[1][1]?> btm_not text_center"><strong><?=$db_cm_series;?></strong></td>
						<? 
						foreach($age_arr as $age_key => $age_val){ 	
							/* 전체 */
							$all_age_sum_txt = number_format($dvalue_val['all'.$age_val.'_sum']).'건';
							/* 남자 */
							$m_age_sum_txt = number_format($dvalue_val['m'.$age_val.'_sum']).'건';
							/* 여자 */
							$w_age_sum_txt = number_format($dvalue_val['w'.$age_val.'_sum']).'건';
						?>
							<td class="<?=$css_suffix?>fl<?=$age_val?> <?=$css_suffix?><?=$age_val?>_sum text_center">
								<ol>
									<? if($mb_sex == 'n' || $mb_sex==''){ ?>
									<li>인증:<?=$all_age_sum_txt?></li>
									<? } if($mb_sex== 'm' || $mb_sex==''){ ?>
										<li class="blue">남:<?=$m_age_sum_txt?></li>									
									<? } if($mb_sex== 'w' || $mb_sex==''){ ?>
									<li class="red">여:<?=$w_age_sum_txt?></li>
									<? } ?>
								</ol>
							</td>
						<?}?>
						<? if($mb_sex=='n' || $mb_sex==''){ ?>
						<td class="<?=$css_suffix?><?=$fetch_row[9][1]?> btm_not text_center"><strong><?=$n_age_sum_txt;?></strong></td>
						<? } ?>
					</tr>
					<?}?>
				</tbody>
			</table>
		</div>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>