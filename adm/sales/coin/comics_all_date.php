<?
include_once '_common.php'; // 공통

/* 화별 link */
$get_comics = get_comics($_cm_no);
$db_comics_url_para = "&cm_big=".$get_comics['cm_big']."&cm_no=".$get_comics['cm_no'];
$db_comics_episode_url = $_cms_self."?sl_mode=episode".$db_comics_url_para;

/* PARAMITER */
/* 데이터 가져오기 */
$where_sales = " AND sl.sl_comics='".$_cm_no."'";

// 날짜-파트너일 경우만
if($nm_member['mb_class'] == 'p'){ 
	$where_sales.= " AND sl.sl_year_month <'".NM_TIME_YM."' "; 
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sl_year_month"){ 
	$_order_field = "sl_year_month"; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
}

// 요일 정렬시
if($_order_field == "sl_week"){
	$_order_field_add = " , sl.sl_year_month desc "; 
	$_order_add = " , sl.sl_day desc ";
}

$order_sales = "order by ".$_order_field." ".$_order;

$sql_sales = "";
/* 17년 06월 08일 수정
sum(sl.sl_cash_point)as sl_cash_point_sum, 
count(if(sl.sl_cash_point>0,1,null))as sl_cash_point_cnt, 

sum(sl.sl_point)as sl_point_sum, 
count(if(sl.sl_point>0,1,null))as sl_point_cnt, 

sum(sl.sl_cash_point*cup_won)as sl_cash_point_won_sum, 
sum(floor(sl.sl_point*cup_won/".NM_POINT."))as sl_point_won_sum */

/* cash_point와 point 함계 결제 건수는 cash_point 건수에 포함 */
$field_sales = "	sl.sl_year_month,  left(sl.sl_year_month,4) as sl_year, 

							sum(sl.sl_cash_point)as sl_cash_point_sum, 
							sum(sl.sl_cash_point*cup_won)as sl_cash_point_won_sum, 

							sum(sl.sl_point)as sl_point_sum, 
							sum(floor(sl.sl_point)*cup_won/".NM_POINT.")as sl_point_won_sum, 
							
							sum(sl.sl_all_pay+sl.sl_pay)as sl_cnt
							"; // 가져올 필드 정하기
$group_sales = " group by sl.sl_year_month ";
$limit_sales = "";

$sql_sales = "	select $field_sales 
						FROM w_sales sl
						JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type 
						where 1 $where_sales 
						$group_sales 
						$order_sales 
						$limit_sales";
$result = sql_query($sql_sales);
$row_size = sql_num_rows($result);

// 연도별로
$sql_sales_y = "	select $field_sales 
						FROM w_sales sl
						JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type 
						where 1 $where_sales 
						group by sl_year 
						order by sl_year ".$_order."
						";
$result_y = sql_query($sql_sales_y);
$row_size_y = sql_num_rows($result_y);

$years_data = array();
while ($row_y = sql_fetch_array($result_y)) {
	array_push($years_data, $row_y);
}

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sl_year_month',1));
array_push($fetch_row, array('총금액','sl_sum_won',0));
array_push($fetch_row, array('총건수','sl_sum_cnt',0));

/* cash_point */
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'-원','sl_cash_point_won_sum',4));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'-'.$nm_config['cf_cash_point_unit'],'sl_cash_point_sum',4));

/* point */
array_push($fetch_row, array($nm_config['cf_point_unit_ko'].'-원','sl_point_won_sum',3));
array_push($fetch_row, array($nm_config['cf_point_unit_ko'].'-'.$nm_config['cf_point_unit'],'sl_point_sum',3));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = $get_comics['cm_series']." 날짜별 사용";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<script type="text/javascript">
<!--
	var cr_thead_mg_add = '22px'; /* fixed 테이블이 높이가 커서 그만큼 마진 */
//-->
</script>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1>
		<?=$cms_head_title?> <?=$cms_page_title?>
		<a class="a_btn episode" href="<?=$db_comics_episode_url?>">화별</a>
	</h1>
	<div>&nbsp;</div>
</section><!-- cms_title -->

<? /*
<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->
*/ ?>

<section id="sales_result">
	<div class="result_explain">
		<ul>
			<li>
				<?=$nm_config['cf_cash_point_unit_ko']?>와 <?=$nm_config['cf_point_unit_ko']?> 함께 결제 건수는 
				<?=$nm_config['cf_cash_point_unit_ko']?>-건수에 포함 됩니다.
			</li>
		</ul>
	</div>
	<h3 id="cr_thead_mg_add">
			<strong><?=$get_comics['cm_series'];?></strong><br/>
			연도별
			<table>
				<tr>
					<th class="topleft">연도</th>
					<th>총-금액</th>
					<th>총건수</th>
					<th><?=$nm_config['cf_cash_point_unit_ko']?> (원)</th>
					<th><?=$nm_config['cf_cash_point_unit_ko']?> (<?=$nm_config['cf_cash_point_unit']?>)</th>
					<th><?=$nm_config['cf_point_unit_ko']?> (원)</th>
					<th><?=$nm_config['cf_point_unit_ko']?> (<?=$nm_config['cf_point_unit']?>)</th>
				</tr>
				<?	$all_sl_cash_point_sum = $all_sl_point_sum = $all_sl_cash_point_won_sum = $all_sl_point_won_sum = $all_sl_cnt = 0;
					foreach($years_data as $years_val){ 
						$all_sl_cnt+=$years_val['sl_cnt'];
						$all_sl_cash_point_won_sum+=$years_val['sl_cash_point_won_sum'];
						$all_sl_cash_point_sum+=$years_val['sl_cash_point_sum'];
						$all_sl_point_won_sum+=$years_val['sl_point_won_sum'];
						$all_sl_point_sum+=$years_val['sl_point_sum'];
				?>
				<tr>
					<th><?=$years_val['sl_year'];?>년</th>
					<td><?=number_format($years_val['sl_cash_point_won_sum']+$years_val['sl_point_won_sum']);?> 원</td>
					<td><?=number_format($years_val['sl_cnt']);?> 건</td>
					<td><?=number_format($years_val['sl_cash_point_won_sum']);?> 원</td>
					<td><?=number_format($years_val['sl_cash_point_sum']);?> <?=$nm_config['cf_cash_point_unit']?></td>
					<td><?=number_format($years_val['sl_point_won_sum']);?> 원</td>
					<td><?=number_format($years_val['sl_point_sum']);?> <?=$nm_config['cf_point_unit']?></td>
				</tr>
				<? } ?>
				<tr>
					<th>합계</th>
					<td><?=number_format($all_sl_cash_point_won_sum+$all_sl_point_won_sum);?> 원</td>
					<td><?=number_format($all_sl_cnt);?> 건</td>
					<td><?=number_format($all_sl_cash_point_won_sum);?> 원</td>
					<td><?=number_format($all_sl_cash_point_sum);?>  <?=$nm_config['cf_cash_point_unit']?></td>
					<td><?=number_format($all_sl_point_won_sum);?> 원</td>
					<td><?=number_format($all_sl_point_sum);?> <?=$nm_config['cf_point_unit']?></td>
				</tr>
			</table>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
					if($fetch_val[2] == 4){
						$th_class.= " sl_cash_point_bgcolor";
					}
					if($fetch_val[2] == 3){
						$th_class.= " sl_point_bgcolor";
					}
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?
				
				foreach($row_data as $dvalue_key => $dvalue_val){
					$d_sl_year_month = $dvalue_val['sl_year_month'];
					$d_sl_won_sum = number_format($dvalue_val['sl_cash_point_won_sum']+$dvalue_val['sl_point_won_sum']).' 원';
					$d_sl_cnt = number_format($dvalue_val['sl_cnt']).' 건';

					$d_sl_cash_point_won_sum = number_format($dvalue_val['sl_cash_point_won_sum']).' 원';
					$d_sl_point_won_sum = number_format($dvalue_val['sl_point_won_sum']).' 원';
					$d_sl_cash_point_sum = number_format($dvalue_val['sl_cash_point_sum']).' '.$nm_config['cf_cash_point_unit'];
					$d_sl_point_sum = number_format($dvalue_val['sl_point_sum']).' '.$nm_config['cf_point_unit'];

				?>
				<tr class="<?=$week_class;?>">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$d_sl_year_month;?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$d_sl_won_sum ;?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$d_sl_cnt;?></td>

					<td class="<?=$fetch_row[3][1]?> text_center sl_cash_point_bgcolor"><?=$d_sl_cash_point_won_sum;?></td>
					<td class="<?=$fetch_row[4][1]?> text_center sl_cash_point_bgcolor"><?=$d_sl_cash_point_sum;?></td>

					<td class="<?=$fetch_row[5][1]?> text_center sl_point_bgcolor"><?=$d_sl_point_won_sum;?></td>
					<td class="<?=$fetch_row[6][1]?> text_center sl_point_bgcolor"><?=$d_sl_point_sum;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>