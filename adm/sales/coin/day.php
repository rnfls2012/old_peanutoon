<?
include_once '_common.php'; // 공통

/* PARAMITER */

/* 데이터 가져오기 */
$where_sales = ""; /* 충전 */

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }
if($_s_date && $_e_date){
	$where_sales.= date_year_month($_s_date, $_e_date, 'sl.sl');
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sl_year_month"){ 
	$_order_field = "sl_year_month"; 
	$_order_field_add = " , sl_day "; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
	$_order_add= "desc"; 
}

// 요일 정렬시
if($_order_field == "sl_week"){
	$_order_field_add = " , sl.sl_year_month desc "; 
	$_order_add = " , sl.sl_day desc ";
}

$order_sales = "ORDER BY ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

$sql_sales = "";
/* 17년 06월 08일 수정
sum(sl.sl_cash_point)as sl_cash_point_sum, 
count(if(sl.sl_cash_point>0,1,null))as sl_cash_point_cnt, 

sum(sl.sl_point)as sl_point_sum, 
count(if(sl.sl_point>0,1,null))as sl_point_cnt, 

sum(sl.sl_cash_point*cup_won)as sl_cash_point_won_sum, 
sum(floor(sl.sl_point*cup_won/".NM_POINT."))as sl_point_won_sum */

/* cash_point와 point 함계 결제 건수는 cash_point 건수에 포함 */
$field_sales = "	sl.sl_year_month, sl.sl_day, sl.sl_week, 

							sum(if(sl.sl_cash_point>0,sl_cash_point,0))as sl_cash_point_sum, 
							sum(if(sl.sl_cash_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_cash_point_cnt, 
							sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won)as sl_cash_point_won_sum, 
							sum(if(sl.sl_point>0,sl_point,0))as sl_point_sum, 
							sum(if( sl.sl_cash_point=0 && sl.sl_point>0 || 
									sl.sl_cash_point=0 && sl.sl_point=0,sl.sl_all_pay+sl.sl_pay,0)) 
									as sl_point_cnt, 
							sum(floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_point_won_sum 
							"; // 가져올 필드 정하기
$group_sales = " GROUP BY sl.sl_year_month, sl.sl_day ";
$limit_sales = "";

$sql_sales = "	SELECT $field_sales 
						FROM sales sl
						JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type 
						WHERE 1 $where_sales 
						$group_sales 
						$order_sales 
						$limit_sales";
$result = sql_query($sql_sales);
$row_size = sql_num_rows($result);

/* 검색 전체 구하기 - group by 빼고 sum, count함수 이용 */
$sql_sales_s_all = "	SELECT $field_sales 
								FROM sales sl
								JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type 
								WHERE 1 $where_sales 
								$order_sales 
								$limit_sales";
$row_sales_s_all = sql_fetch($sql_sales_s_all );
$s_all_cash_point_sum = $row_sales_s_all['sl_cash_point_sum'];
$s_all_cash_point_cnt = $row_sales_s_all['sl_cash_point_cnt'];
$s_all_point_sum = $row_sales_s_all['sl_point_sum'];
$s_all_point_cnt = $row_sales_s_all['sl_point_cnt'];
$s_all_cash_point_won_sum = $row_sales_s_all['sl_cash_point_won_sum'];
$s_all_point_won_sum = $row_sales_s_all['sl_point_won_sum'];

/* 충전 전체 구하기 */
$where_sales_recharge = ""; /* 충전 */
if($_s_date && $_e_date){ 
	$where_sales_recharge = date_year_month($_s_date, $_e_date, 'sr'); 
}
$sql_sales_recharge = "SELECT sr_year_month, sr_day, sr_week, 
						 sum(sr_cash_point)as sr_cash_point_sum, 
						 sum(sr_point)as sr_point_sum
						 FROM sales_recharge 
						 WHERE 1 $where_sales_recharge
						 ";
$row_sales_recharge = sql_fetch($sql_sales_recharge);
$s_all_sr_cash_point_sum = intval($row_sales_recharge['sr_cash_point_sum']);
$s_all_sr_point_sum = intval($row_sales_recharge['sr_point_sum']);
if($s_all_sr_cash_point_sum == 0){$s_all_sr_cash_point_sum = 1;}
if($s_all_sr_point_sum == 0){$s_all_sr_point_sum = 1;}

$s_all_cash_point_runout = intval(($s_all_cash_point_sum / $s_all_sr_cash_point_sum * 1000) / 10)."%";
$s_all_point_runout  = intval(($s_all_point_sum / $s_all_sr_point_sum * 1000) / 10)."%";

$s_all_sum = number_format($s_all_cash_point_won_sum + $s_all_point_won_sum)." 원";
$s_all_cnt = number_format($s_all_cash_point_cnt + $s_all_point_cnt)." 건";

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sl_year_month',1));
array_push($fetch_row, array('요일','sl_week',1));
array_push($fetch_row, array('총금액','sl_sum_won',0));
array_push($fetch_row, array('총건수','sl_sum_cnt',0));

/* cash_point */
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'-'.$nm_config['cf_cash_point_unit'].'<br/>(충전'.$nm_config['cf_cash_point_unit_ko'].' / 소진율)','sl_cash_point_sum',4));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'-건수','sl_cash_point_cnt',4));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'-원','sl_cash_point_won_sum',4));

/* point */
array_push($fetch_row, array($nm_config['cf_point_unit_ko'].'-'.$nm_config['cf_point_unit'].'<br/>(충전'.$nm_config['cf_point_unit_ko'].' / 소진율)','sl_point_sum',3));
array_push($fetch_row, array($nm_config['cf_point_unit_ko'].'-건수','sl_point_cnt',3));
array_push($fetch_row, array($nm_config['cf_point_unit_ko'].'-원','sl_point_won_sum',3));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "일별 사용";
$cms_head_title = $page_title;

if($_mode == 'excel') {
	include_once $_cms_folder_top_path.'/excel/day_excel.php';
}

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<script type="text/javascript">
<!--
	var cr_thead_mg_add = '22px'; /* fixed 테이블이 높이가 커서 그만큼 마진 */
//-->
</script>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
	<div class="write">
		<input type="button" class="excel_down" value="<?=$page_title?> 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?<?=$_nm_paras;?>&mode=excel'"/>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<div class="result_explain">
		<ul>
			<li>
				<?=$nm_config['cf_cash_point_unit_ko']?>와 <?=$nm_config['cf_point_unit_ko']?> 함께 결제 건수는 
				<?=$nm_config['cf_cash_point_unit_ko']?>-건수에 포함 됩니다.
			</li>
		</ul>
	</div>
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 - 총금액: <?=$s_all_sum;?> / 총건수: <?=$s_all_cnt;?> ( <?=$nm_config['cf_cash_point_unit_ko']?>+<?=$nm_config['cf_point_unit_ko']?>)</strong>
		<?if($s_all_point_won_sum!= ''){?>
			<table>
				<tr>
					<th class="topleft">화페별</th>
					<th><?=$nm_config['cf_cash_point_unit_ko']?></th>
					<th class="topright"><?=$nm_config['cf_point_unit_ko']?></th>
				</tr>
				<tr>
					<th>금액</th>
					<td><?=number_format($s_all_cash_point_won_sum);?> 원</td>
					<td><?=number_format($s_all_point_won_sum );?> 원</td>
				</tr>
				<tr>
					<th>건수</th>
					<td><?=number_format($s_all_cash_point_cnt);?> 건</td>
					<td><?=number_format($s_all_point_cnt);?> 건</td>
				</tr>
				<tr>
					<th>사용매출</th>
					<td><?=number_format($s_all_cash_point_sum);?> <?=$nm_config['cf_cash_point_unit']?></td>
					<td><?=number_format($s_all_point_sum );?> <?=$nm_config['cf_point_unit']?></td>
				</tr>
				<tr>
					<th>충전매출</th>
					<td><?=number_format($s_all_sr_cash_point_sum);?> <?=$nm_config['cf_cash_point_unit']?></td>
					<td><?=number_format($s_all_sr_point_sum );?> <?=$nm_config['cf_point_unit']?></td>
				</tr>
				<tr>
					<th>소진율</th>
					<td><?=$s_all_cash_point_runout;?></td>
					<td><?=$s_all_point_runout;?></td>
				</tr>
			</table>
		<?}?>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
					if($fetch_val[2] == 4){
						$th_class.= " sl_cash_point_bgcolor";
					}
					if($fetch_val[2] == 3){
						$th_class.= " sl_point_bgcolor";
					}
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					$db_date = $dvalue_val['sl_year_month']."-".$dvalue_val['sl_day'];
					$db_week = get_yoil($dvalue_val['sl_week'],1,1); 
					$db_sum_won = number_format($dvalue_val['sl_cash_point_won_sum'] + $dvalue_val['sl_point_won_sum'])." 원";
					$db_sum_cnt = number_format($dvalue_val['sl_cash_point_cnt'] + $dvalue_val['sl_point_cnt'])." 건";

					// 충전땅콩 / 소진율
					$db_sql_sales_recharge = "SELECT sr_year_month, sr_day, sr_week, 
												sum(sr_cash_point)as sr_cash_point_sum, 
												sum(sr_point)as sr_point_sum 
												FROM sales_recharge 
												WHERE 1 AND sr_year_month = '".$dvalue_val['sl_year_month']."' 
												AND sr_day = '".$dvalue_val['sl_day']."'
												GROUP BY sr_year_month, sr_day ";					
					$db_row_sales_recharge = sql_fetch($db_sql_sales_recharge);
					
					/* cash_point  */					
					$db_cash_point_sum = number_format($dvalue_val['sl_cash_point_sum']).' '.$nm_config['cf_cash_point_unit'];
					$db_cash_point_sum.= "<br/>";
					$db_sr_cash_point_sum = intval($db_row_sales_recharge['sr_cash_point_sum']);
					if($db_sr_cash_point_sum == 0 ){$db_sr_cash_point_sum = 1;}
					$db_cash_point_runout  = intval(($dvalue_val['sl_cash_point_sum'] / $db_sr_cash_point_sum * 1000) / 10)."%";
					$db_cash_point_sum.= "(".$db_row_sales_recharge['sr_cash_point_sum'].' '.$nm_config['cf_cash_point_unit']." / ";
					$db_cash_point_sum.= $db_cash_point_runout.")";

					$db_cash_point_cnt= number_format($dvalue_val['sl_cash_point_cnt'])." 건";
					$db_cash_point_won_sum = number_format($dvalue_val['sl_cash_point_won_sum'])." 원";

					/* point */
					$db_point_sum = number_format($dvalue_val['sl_point_sum']);
					$db_point_sum.= "<br/>";					
					$db_sr_point_sum = intval($db_row_sales_recharge['sr_point_sum']).' '.$nm_config['cf_point_unit'];
					if($db_sr_point_sum == 0 ){$db_sr_point_sum = 1;}
					$db_point_runout  = intval(($dvalue_val['sl_point_sum'] / $db_sr_point_sum * 1000) / 10)."%";
					$db_point_sum.= "(".$db_row_sales_recharge['sr_point_sum'].' '.$nm_config['cf_point_unit']." / ";
					$db_point_sum.= $db_point_runout.")";

					$db_point_cnt= number_format($dvalue_val['sl_point_cnt'])." 건";
					$db_point_won_sum = number_format($dvalue_val['sl_point_won_sum'])." 원";

					/* 요일별 class */
					$week_class = $week_en[$dvalue_val['sl_week']];
				?>
				<tr class="<?=$week_class;?>">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$db_date;?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$db_week;?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$db_sum_won;?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$db_sum_cnt;?></td>

					<td class="<?=$fetch_row[4][1]?> text_center sl_cash_point_bgcolor"><?=$db_cash_point_sum;?></td>
					<td class="<?=$fetch_row[5][1]?> text_center sl_cash_point_bgcolor"><?=$db_cash_point_cnt;?></td>
					<td class="<?=$fetch_row[6][1]?> text_center sl_cash_point_bgcolor"><?=$db_cash_point_won_sum;?></td>

					<td class="<?=$fetch_row[7][1]?> text_center sl_point_bgcolor"><?=$db_point_sum;?></td>
					<td class="<?=$fetch_row[8][1]?> text_center sl_point_bgcolor"><?=$db_point_cnt;?></td>
					<td class="<?=$fetch_row[9][1]?> text_center sl_point_bgcolor"><?=$db_point_won_sum;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>