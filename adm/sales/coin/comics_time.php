<?
include_once '_common.php'; // 공통

/* 작가, 출판사, 제공사 데이터 가져오기 */
// $cp_arr = array("professional", "publisher", "provider");
$professional_arr = $publisher_arr = $provider_arr = $cp_arr = array();
array_push($cp_arr , array('professional','작가'));
array_push($cp_arr , array('publisher','출판사'));
array_push($cp_arr , array('provider','제공사'));

foreach($cp_arr as $cp_val){
	$sql_cp = "select * from comics_".$cp_val[0]." order by cp_name asc";
	$result_cp = sql_query($sql_cp);	
	$row_cp_save = array();

	while($row_cp = sql_fetch_array($result_cp)) {
		$row_cp_save[$row_cp['cp_no']] = $row_cp['cp_name'];
		${$cp_val[0].'_arr'} = $row_cp_save;
	}
}

// 샐럭트 박스 선택이 있다면.... (작가, 출판사, 제공사)
$cm_no_arr = array();
$sql_where_cp_bool = false;
$sql_where_cnts = 0;
$sql_where_cps = "";
foreach($cp_arr as $cp_val){
	$sql_where_cp = "";
	if(${'_'.$cp_val[0]} != ''){
		$sql_where_cp_bool = true;
		$sql_where_cnts++;
		switch($cp_val[0]){
			case 'professional' :	$sql_where_cp = "	(  cm_professional = '".${'_'.$cp_val[0]}."' 
														OR cm_professional_sub = '".${'_'.$cp_val[0]}."'  
														OR cm_professional_thi = '".${'_'.$cp_val[0]}."' ) ";
			break;
			case 'publisher' :		$sql_where_cp = "	(  cm_publisher = '".${'_'.$cp_val[0]}."' ) ";
			break;
			case 'provider' :		$sql_where_cp = "	(  cm_provider = '".${'_'.$cp_val[0]}."' 
														OR cm_provider_sub = '".${'_'.$cp_val[0]}."' ) ";
			break;
		}

		if($sql_where_cnts == 1){
			$sql_where_cps.= $sql_where_cp;
		}else{
			$sql_where_cps.= " AND ".$sql_where_cp;
		}
	}
}
$cm_no_arr = get_cm_no_arr($sql_where_cps);
if(count($cm_no_arr) == 0 && $sql_where_cp_bool == true){ $sql_where.= " AND sl_comics = 0 "; } // 샐럭트박스 검색결과가 0이라면...

// 카테고리(단행본, 웹툰, 단행본웹툰)
if($_big) {
	$sql_where .= " AND sl_big = ".$_big." ";
} // end if

// 2.  검색단어+ 검색타입
if($sql_where_cp_bool == false){ // 샐럭트 박스 선택이 없다면....
	if($_s_text){
		switch($_s_type){
			case '0': $cm_no_arr = array_merge(get_comics_series($_s_text));
			break;
			case '1': $cm_no_arr = array_merge(get_cm_no_arr(get_cp_comics_sql($_s_text, 'professional')));
			break;
			case '2': $cm_no_arr = array_merge(get_cm_no_arr(get_cp_comics_sql($_s_text, 'publisher')));
			break;
			case '3': $cm_no_arr = array_merge(get_cm_no_arr(get_cp_comics_sql($_s_text, 'provider')));
			break;
			default:  $cm_no_arr = array_merge(get_comics_series($_s_text), get_cm_no_arr(get_cp_comics_sql($_s_text, '')));
			break;
		}
	}
}

if(count($cm_no_arr) > 0){
	$sql_where.= " AND sl_comics in ( ".sql_array_merge($cm_no_arr)." ) ";
}

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

// 파트너일 경우
$sql_mb_cm_cp	= mb_cm_cp_sql($nm_member);		// 파트너 SQL
$_e_date		= mb_partner_e_date($nm_member, $_e_date);	// 종료일 현재 -1일 적용
$sql_where.= $sql_mb_cm_cp;

// 날짜
if($_s_date && $_e_date){ 
	$sql_where.= date_year_month($_s_date, $_e_date, 'sl.sl'); 
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sl_won_sum"){ 
	$_order_field = "sl_won_sum"; 
	$_order_field_add = " , sl_comics "; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
	$_order_add= "desc"; 
}

// 요일 정렬시
if($_order_field == "sl_week"){
	$_order_field_add = " , sl.sl_year_month desc "; 
	$_order_add = " , sl.sl_day desc ";
}

$sql_order = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

// 그룹
$sql_group = " group by sl.sl_comics ";

// SQL문
$sql_field = "		sl.sl_comics, 

					sum(if(sl.sl_hour='00',sl.sl_all_pay+sl.sl_pay,0))as sl_time00,
					sum(if(sl.sl_hour='01',sl.sl_all_pay+sl.sl_pay,0))as sl_time01,
					sum(if(sl.sl_hour='02',sl.sl_all_pay+sl.sl_pay,0))as sl_time02,
					sum(if(sl.sl_hour='03',sl.sl_all_pay+sl.sl_pay,0))as sl_time03,
					sum(if(sl.sl_hour='04',sl.sl_all_pay+sl.sl_pay,0))as sl_time04,
					sum(if(sl.sl_hour='05',sl.sl_all_pay+sl.sl_pay,0))as sl_time05,
					sum(if(sl.sl_hour='06',sl.sl_all_pay+sl.sl_pay,0))as sl_time06,
					sum(if(sl.sl_hour='07',sl.sl_all_pay+sl.sl_pay,0))as sl_time07,
					sum(if(sl.sl_hour='08',sl.sl_all_pay+sl.sl_pay,0))as sl_time08,
					sum(if(sl.sl_hour='09',sl.sl_all_pay+sl.sl_pay,0))as sl_time09,
					sum(if(sl.sl_hour='10',sl.sl_all_pay+sl.sl_pay,0))as sl_time10,
					sum(if(sl.sl_hour='11',sl.sl_all_pay+sl.sl_pay,0))as sl_time11,
					sum(if(sl.sl_hour='12',sl.sl_all_pay+sl.sl_pay,0))as sl_time12,
					sum(if(sl.sl_hour='13',sl.sl_all_pay+sl.sl_pay,0))as sl_time13,
					sum(if(sl.sl_hour='14',sl.sl_all_pay+sl.sl_pay,0))as sl_time14,
					sum(if(sl.sl_hour='15',sl.sl_all_pay+sl.sl_pay,0))as sl_time15,
					sum(if(sl.sl_hour='16',sl.sl_all_pay+sl.sl_pay,0))as sl_time16,
					sum(if(sl.sl_hour='17',sl.sl_all_pay+sl.sl_pay,0))as sl_time17,
					sum(if(sl.sl_hour='18',sl.sl_all_pay+sl.sl_pay,0))as sl_time18,
					sum(if(sl.sl_hour='19',sl.sl_all_pay+sl.sl_pay,0))as sl_time19,
					sum(if(sl.sl_hour='20',sl.sl_all_pay+sl.sl_pay,0))as sl_time20,
					sum(if(sl.sl_hour='21',sl.sl_all_pay+sl.sl_pay,0))as sl_time21,
					sum(if(sl.sl_hour='22',sl.sl_all_pay+sl.sl_pay,0))as sl_time22,
					sum(if(sl.sl_hour='23',sl.sl_all_pay+sl.sl_pay,0))as sl_time23, 

					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_won_sum, 

					sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
					sum(sl.sl_open)as sl_open_sum 
"; 
$sql_count	= $sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$sql_select = "		select {$sql_field} ";
$sql_select_cnt = "	select {$sql_count} ";
$sql_table = "		FROM sales sl
					LEFT JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type ";

$sql_query		= " {$sql_select}		{$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_select_cnt}	{$sql_table} {$sql_where} ";

$sql_querys		= " {$sql_query} {$sql_order} ";
// SQL처리
$rows_cnts		= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data_cnts = sql_fetch($sql_query_cnt);	// SQL문 총값-레코드값

if($_mode == 'excel') {
	$rows_data	= rows_data($sql_querys, false);		// SQL문 결과 레코드값
	include_once $_cms_folder_top_path.'/excel/comics_excel.php';
}else{ // end if
	$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값
}

$s_all_cash_point_sum = $rows_data_cnts['sl_cash_point_sum'];
$s_all_point_sum = $rows_data_cnts['sl_point_sum'];
$s_all_cash_point_won_sum = $rows_data_cnts['sl_cash_point_won_sum'];
$s_all_point_won_sum = $rows_data_cnts['sl_point_won_sum'];
$s_all_won_sum = $rows_data_cnts['sl_won_sum'];


$s_all_sum = number_format($s_all_won_sum)." 원";
$s_all_cnt = number_format($rows_data_cnts['sl_pay_sum'])." 건";


/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('코믹스번호','sl_comics',0));
array_push($fetch_row, array('코믹스명','cm_series',0));

for($h=0; $h<24; $h++){ 
	$cm_h = $h."시"; 
	if($h <10){$cm_h = "0".$h."시";}
	array_push($fetch_row, array($cm_h,'sl_time'.$cm_h,0));
	
}/* for($rs=0; $rs<24; $rs++){ */

// array_push($fetch_row, array('코믹스 등록일<br/>화 최신등록일','cm_reg_date',0));

// css 클래스 접두사
$css_suffix = "cm_";

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "코믹스별 사용";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<script type="text/javascript">
<!--
	var cr_thead_mg_add = '38px'; /* fixed 테이블이 높이가 커서 그만큼 마진 */
//-->
</script>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<!--
	<div class="write">
		<input type="button" class="excel_down" value="검색 결과 엑셀 다운로드" onclick="location.href='<?=$_cms_self;?>?<?=$_nm_paras;?>&mode=excel'"/>
	</div>
	-->
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<?if(mb_partner_cp($nm_member) == false){?>
					<div class="cs_s_type_btn">
					<?	$s_type_list = array('작품명','작가','출판사','제공사');
						tag_radios($s_type_list, "s_type", $_s_type, 'y', '전체', ''); ?>
					</div>
					<div class="cs_search_btn">
						<?	tag_selects($nm_config['cf_big'], "big", $_big, 'y', '전체', ''); ?>
						<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
						<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
					</div>
					
					<? foreach($cp_arr as $cp_val){ /* 작가, 출판사, 제공사 데이터 출력 */ ?>
						<div class="cs_search_btn">
							<?=$cp_val[1];?> 리스트 
							<?	tag_selects(${$cp_val[0].'_arr'}, $cp_val[0], ${'_'.$cp_val[0]}, 'y', '전체', ''); ?>
						</div>
					<? } // foreach ?>
				<? } // if end ?>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 - 총금액: <?=$s_all_sum;?> / 총건수: <?=$s_all_cnt;?></strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $css_suffix.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){
					$get_comics = get_comics($dvalue_val['sl_comics']); /* 변경 */

					$db_cm_big = $get_comics['cm_big'];
					$db_comics = $dvalue_val['sl_comics'];
					$db_series = $get_comics['cm_series'];

					$comics_url = get_comics_url($dvalue_val['sl_comics']);
				?>
				<tr class="<?=$week_class;?>">
					<td class="<?=$css_suffix.$fetch_row[0][1]?> text_center"><?=$db_comics;?></td>
					<td class="<?=$css_suffix.$fetch_row[1][1]?> text_center">
						<a href="<?=$db_comics_date_url?>"><?=$db_series;?></a>
					</td>
					<? for($h=0; $h<24; $h++){ 
						$cm_h = $h; 
						if($h <10){$cm_h = "0".$h;}?>
						<td class="<?=$css_suffix.$fetch_row[$h+2][1]?> text_center"><?=number_format($dvalue_val['sl_time'.$cm_h]);?></td>
						
					<? }/* for($rs=0; $rs<24; $rs++){ */ ?>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? rows_page(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>