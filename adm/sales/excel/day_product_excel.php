<?
/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sr_year_month'));
array_push($fetch_row, array('요일','sr_week'));
array_push($fetch_row, array('총 결제-금액','sumpay'));
array_push($fetch_row, array('총 결제-건수','sumcnt'));

foreach($product_arr as $product_key => $product_val){
	array_push($fetch_row, array($product_val['sr_product']."(￦".number_format($product_val['sr_pay']).") 금액",$product_val['sr_product'].'_sum'));
	array_push($fetch_row, array($product_val['sr_product']."(￦".number_format($product_val['sr_pay']).") 건수",$product_val['sr_product'].'_cnt'));
} // end foreach

$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once('../../../lib/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once('../../../lib/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
} // end foreach

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write(0, $col++, $cell);
} // end foreach

/* 데이터 전달 부분 */
for ($i=1; $row=mysql_fetch_array($result); $i++){
	foreach($data_key as $cell_key => $cell_val) {
		$worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]));
	} // end foreach
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."day_product_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"day_product_".$today.".xls");
header("Content-Disposition: inline; filename=\"day_product_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;
?>