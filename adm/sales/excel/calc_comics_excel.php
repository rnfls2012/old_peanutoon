<?
/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('코믹스번호','sl_comics'));
array_push($fetch_row, array('코믹스명','cm_series'));
array_push($fetch_row, array('작가명','cm_author'));
array_push($fetch_row, array('출판사명','cm_publisher'));
array_push($fetch_row, array('제공사명','cm_provider'));
array_push($fetch_row, array('서브제공사명','cm_provider_sub'));
array_push($fetch_row, array('열람수','sl_open_sum'));

array_push($fetch_row, array('등록일','cm_reg_date',0));
array_push($fetch_row, array('에피소드 등록일','cm_episode_date',0));

/* cash_point */
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'('.$nm_config['cf_cash_point_unit'].')', 'sl_cash_point_sum'));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'(건수)','sl_cash_point_cnt'));

array_push($fetch_row, array('총 가격','sl_cash_point_won_sum'));

$today = date("Ymd");

// 엑셀 Library Import
include_once('../../../lib/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once('../../../lib/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
} // end foreach

/* 컬럼 이름 지정 부분 */
$worksheet->write(0, 0, iconv_cp949('플랫폼'));
$worksheet->write(1, 0, iconv_cp949('검색어'));
$worksheet->write(2, 0, iconv_cp949('검색 작가'));
$worksheet->write(3, 0, iconv_cp949('검색 출판사'));
$worksheet->write(4, 0, iconv_cp949('검색 제공사'));
$worksheet->write(5, 0, iconv_cp949('검색 날짜'));

$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write(7, $col++, $cell);
} // end foreach

/* 데이터 전달 부분 */
$worksheet->write(0, 1, iconv_cp949($nm_config['cf_title']));
$worksheet->write(1, 1, iconv_cp949($_s_text));
$worksheet->write(2, 1, iconv_cp949($professional_arr[$_professional]));
$worksheet->write(3, 1, iconv_cp949($publisher_arr[$_publisher]));
$worksheet->write(4, 1, iconv_cp949($provider_arr[$_provider]));
$worksheet->write(5, 1, iconv_cp949($_s_date."~".$_e_date));

$i=8;
foreach($rows_data as $dvalue_key => $dvalue_val){
	foreach($data_key as $cell_key => $cell_val) {
		$get_comics = get_comics($dvalue_val['sl_comics']);

		switch($cell_val) {
			case "cm_series" :
			case "cm_reg_date" :
			case "cm_episode_date" :
				$worksheet->write($i, $cell_key , iconv_cp949($get_comics[$cell_val]));
				break;

			case "cm_author" :
				$worksheet->write($i, $cell_key , iconv_cp949($get_comics['prof_name']));
				break;

			case "cm_publisher" :
				$worksheet->write($i, $cell_key , iconv_cp949($get_comics['publ_name']));
				break;

			case "cm_provider" :
				$worksheet->write($i, $cell_key , iconv_cp949($get_comics['prov_name']));
				break;

			case "cm_provider_sub" : 
				$worksheet->write($i, $cell_key , iconv_cp949($get_comics['prov_name_sub']));
				break;

			default :
				$worksheet->write($i, $cell_key , iconv_cp949($dvalue_val[$cell_val]));
				break;
		} // end switch

	} // end foreach
	$i++;
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename=".$nm_config['cf_title']."_calc_comics_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"".$nm_config['cf_title']."_calc_comics_".$today.".xls");
header("Content-Disposition: inline; filename=\"".$nm_config['cf_title']."_calc_comics_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
?>