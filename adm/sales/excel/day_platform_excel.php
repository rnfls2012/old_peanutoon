<?/* 미완성 */
include_once '../recharge/_common.php'; // 공통

/* PARAMITER 
maker_name // 제공사이름
*/


/* 데이터 가져오기 */
$where_sales_recharge = ""; /* 충전 */

if($_s_date == ''){ $_s_date = date('Y-m-01', NM_SERVER_TIME); }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }


if($_s_date && $_e_date){
	$_s_date_y_m = substr($_s_date,0,7);
	$_e_date_y_m = substr($_e_date,0,7);
	$_s_date_d = substr($_s_date,8,2);
	$_e_date_d = substr($_e_date,8,2);
	
	/*
	$where_sales_recharge.= "and sr_year_month >= '$_s_date_y_m' and sr_year_month <= '$_e_date_y_m' ";
	$where_sales_recharge.= "and sr_day >= '$_s_date_d' and sr_day <= '$_e_date_d' ";
	*/
	
	$date_and_or = "OR";
	if($_s_date_y_m == $_e_date_y_m){ $date_and_or = "AND"; }
	$where_sales_recharge.= "AND (sr_year_month >  '$_s_date_y_m' AND sr_year_month <  '$_e_date_y_m' ) ";
	$where_sales_recharge.= "OR ( (sr_year_month =  '$_s_date_y_m' AND sr_day >=  '$_s_date_d') ";
	$where_sales_recharge.= "$date_and_or (sr_year_month =  '$_e_date_y_m' AND sr_day <=  '$_e_date_d') ) ";
}
// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "sr_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 요일 정렬시
if($_order_field == "sr_week"){$_order_field_add = " , sr_date "; $_order_add = "desc ";}
// 날짜 정렬시
if($_order_field == "sr_year_month"){$_order_field_add = " , sr_date "; $_order_add = $_order;}
$order_sales_recharge = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

$sql_sales_recharge = "";
$field_sales_recharge = "   sr_year_month, 
							sr_day, 
							sr_week, 
							sum(sr_pay)as sumpay, 
							count(sr_day)as sumcnt, 
							sum(sr_re_pay)as repay_cnt, 

							count(if(sr_platform='1' OR sr_platform='3',1,null))as web_cnt, 
							sum(if(sr_platform='1' OR sr_platform='3',sr_pay,null))as web_sum, 

							count(if(sr_platform='2' OR sr_platform='3',1,null))as mob_cnt, 
							sum(if(sr_platform='2' OR sr_platform='3',sr_pay,null))as mob_sum  "; // 가져올 필드 정하기
$group_sales_recharge = " group by sr_year_month, sr_day ";
$limit_sales_recharge = "";

$sql_sales_recharge = "select $field_sales_recharge FROM sales_recharge where 1 $where_sales_recharge $group_sales_recharge $order_sales_recharge $limit_sales_recharge";
$result = sql_query($sql_sales_recharge);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sr_year_month'));
array_push($fetch_row, array('요일','sr_week'));
array_push($fetch_row, array('총 결제-금액','sumpay'));
array_push($fetch_row, array('총 결제-건수','sumcnt'));

array_push($fetch_row, array('PC 결제-금액','sumpay'));
array_push($fetch_row, array('PC 결제-건수','sumcnt'));

array_push($fetch_row, array('모바일 결제-금액','sumpay'));
array_push($fetch_row, array('모바일 결제-건수','sumcnt'));
?>