<?php
$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// 해당 시트 당 컬럼 크기 변경
$worksheet->set_column(0, 4, 15);

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
    'align' => 'right',
    'border' => 1
));

$center  = $workbook -> addformat(array(
    'align' => 'center',
    'border' => 1
));

$heading = $workbook -> addformat(array(
    'align' => 'center',
    'border' => 1,
    'bold' => 1,
    'fg_color' => 27
));

$date_format = $workbook -> addformat(array(
    'bold' => 1
));

$cell_format = $workbook -> addformat(array(
    'size' => 15,
    'bold' => 1
));

//문서 출력일자 셀 출력
$worksheet->write(0, 0 , iconv_cp949($cms_head_title." ".$hour."시"), $cell_format);
$worksheet->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet->write(1, 1 , iconv_cp949(date("Y-m-d")));

/***************  본문 출력   **************/

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
    array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
    array_push($data_key, $val[1]);
} // end foreach

$fetch_row_cnt = count($fetch_row);

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
    $worksheet->write(3, $col++, $cell, $heading);
} // end foreach

/* 데이터 전달 부분 */
for ($i=$fetch_row_cnt; $row=sql_fetch_array($result); $i++){
    foreach($data_key as $cell_key => $cell_val) {
        switch($cell_val) {
            case "sr_min" :
                if (count($row['sr_min']) < 2) {
                    $sr_min = $hour.":".$row['sr_min'];
                }

                $worksheet->write($i, $cell_key , iconv_cp949($sr_min), $center);
                break;

            case "sr_pay" :
                $sr_pay = number_format($row['sr_pay']);
                $total_sum += $row['sr_pay'];

                $worksheet->write($i, $cell_key , iconv_cp949($sr_pay), $right);
                break;

            default :
                $worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]), $right);
                break;
        }
    } // end foreach
    $fetch_row_cnt++;
} // end for 쿼리 조회결과가 없을때까지 조회

$fetch_row_cnt++;

$worksheet->write($fetch_row_cnt, 0, iconv_cp949("합 계"), $heading);
$worksheet->write($fetch_row_cnt, 1, iconv_cp949(number_format($total_sum)." 원"), $right);

$workbook->close();

header("Content-Disposition: attachment;filename="."day_hour_view_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"day_hour_view_".$today.".xls");
header("Content-Disposition: inline; filename=\"day_hour_view_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;