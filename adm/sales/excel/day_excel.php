<?php
$today = date("Ymd");

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/

// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// 해당 시트 컬럼 크기 변경
$worksheet->set_column(0, 1, 10);
$worksheet->set_column(1, 6, 15);

// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));

$center  = $workbook -> addformat(array(
								'align' => 'center',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));
								
//문서 출력일자 셀 출력
$worksheet->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt = 3;

/***************  검색 날짜 별 총 합 출력   **************/

//검색 날짜 별 결과 항목 열 지정.
$search_ver = array();
array_push($search_ver, $nm_config['cf_cash_point_unit_ko']);
array_push($search_ver, $nm_config['cf_point_unit_ko']);


//검색 날짜 별 결과 항목 행 지정.
$search_hor = array();
array_push($search_hor, '금액');
array_push($search_hor, '건수');
array_push($search_hor, '사용매출');
array_push($search_hor, '충전매출');
array_push($search_hor, '소진율');


//총 합 결과 배열 저장. 순서 바꾸기 X
$search_result = array();
array_push($search_result, array(number_format($s_all_cash_point_won_sum), number_format($s_all_point_won_sum))); 
array_push($search_result, array(number_format($s_all_cash_point_cnt), number_format($s_all_point_cnt)));
array_push($search_result, array(number_format($s_all_cash_point_sum), number_format($s_all_point_sum)));
array_push($search_result, array(number_format($s_all_sr_cash_point_sum), number_format($s_all_sr_point_sum)));
array_push($search_result, array($s_all_cash_point_runout, $s_all_point_runout));

//각 배열당 크기
$ssv_cnt = count($search_ver);
$ssh_cnt = count($search_hor);

$worksheet->write($row_cnt, 0, iconv_cp949('구분'), $heading);

//검색 날짜 별 항목(열)
for($i=1; $i<=$ssv_cnt; $i++){
	$worksheet->write($row_cnt, $i, iconv_cp949($search_ver[$i-1]), $heading);
}

//검색 날짜 별 항목(행)
for($i=1; $i<=$ssh_cnt; $i++){
	$worksheet->write($i+$row_cnt, 0, iconv_cp949($search_hor[$i-1]), $heading);
}

//검색 날짜 별 항목(총 합 결과)
for($i=1; $i<=2; $i++){
	foreach($search_result as $key => $value){
		$worksheet->write($key+$row_cnt+1, $i, iconv_cp949($value[$i-1]), $right);
	}
}

$ssh_cnt++;

/***************  본문 출력   **************/

// EXCEL용 배열
$data_head = $data_key = array(); // 분류값(컬럼) 및 데이터 출력 저장 배열
foreach($fetch_row as $key => $val) {
	if(strpos($val[0], '<br/>') === false){
		array_push($data_head, $val[0]);
		array_push($data_key, $val[1]);
	} else {
		if($val[1] == 'sl_cash_point_sum') { 
			$sl_cash_point_sum = substr($val[0], 0, 8); 	
			array_push($data_head, $sl_cash_point_sum);
		} else { 
			$sl_point_sum = substr($val[0], 0, 15); 			
			array_push($data_head, $sl_point_sum);
		}
		array_push($data_key, $val[1]);
	}
} // end foreach

//컬럼 헤드 출력
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write($ssh_cnt+$row_cnt+1, $col++, $cell, $heading);
} // end foreach

$ssh_cnt++;

/* 데이터 전달 부분 */
for ($i=$ssh_cnt+$row_cnt+1; $row=mysql_fetch_array($result); $i++){
	foreach($data_key as $cell_key => $cell_val) {
		switch($cell_val) {
			case "sl_year_month" :
				$sl_year_month = $row['sl_year_month']."-".$row['sl_day'];;
				$worksheet->write($i, $cell_key , iconv_cp949($sl_year_month), $center);
				break;

			case "sl_week" :				
				$sl_week = get_yoil($row['sl_week'],1,1); 
				$worksheet->write($i, $cell_key , iconv_cp949($sl_week), $center);
				break;

			case "sl_sum_won" :
				$sl_sum_won = number_format($row['sl_cash_point_won_sum'] + $row['sl_point_won_sum']);
				$worksheet->write($i, $cell_key , iconv_cp949($sl_sum_won), $f_price);
				break;

			case "sl_sum_cnt" :
				$sl_sum_cnt = number_format($row['sl_cash_point_cnt'] + $row['sl_point_cnt']);
				$worksheet->write($i, $cell_key , iconv_cp949($sl_sum_cnt), $f_price);
				break;

			default :
				$worksheet->write($i, $cell_key , iconv_cp949(number_format($row[$cell_val])), $f_price);
				break;
		}
	}
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."day_buy_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"day_buy_".$today.".xls");
header("Content-Disposition: inline; filename=\"day_buy_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;
?>