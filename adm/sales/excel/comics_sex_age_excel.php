<?php
$today = date("Ymd");

// 엑셀 Library Import
include_once('../../../lib/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once('../../../lib/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/

// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

/***************  검색 날짜 별 총 합 출력   **************/

//검색 날짜 별 결과 항목 열 지정.
$search_sum_ver = array();
array_push($search_sum_ver, '성별'); //첫 부분 
foreach($age_arr as $age_key => $age_val){ 
	if($age_val == ''){
		$age_text = "연령전체";
	} elseif($age_val == 60){
		$age_text = $age_val."대이상";
	} else {
		$age_text = $age_val."대";
	}
	array_push($search_sum_ver, $age_text);
}
array_push($search_sum_ver,'미인증');

$ssv_cnt = count($search_sum_ver);

$search_sum_hor = array();
array_push($search_sum_hor, '남자');
array_push($search_sum_hor, '여자');

$ssh_cnt = count($search_sum_hor);

//미인증 출력
$worksheet->write(1, 8, iconv_cp949(number_format($db_s_all_row['n_sum'])." 건"));

//검색 날짜 별 항목(열)
for($i=0; $i<=$ssv_cnt; $i++){
	$worksheet->write(0, $i, iconv_cp949($search_sum_ver[$i]));
}

//검색 날짜 별 항목(행)
for($i=1; $i<=$ssh_cnt; $i++){
	$worksheet->write($i, 0, iconv_cp949($search_sum_hor[$i-1]));
}

//검색 날짜 별 항목(총 합 결과)
for($i=1; $i<=$ssh_cnt; $i++){
	if($i==1){
		foreach($age_arr as $key => $value){
			$m_value = number_format($db_s_all_row['m'.$value.'_sum'])." 건";
			$worksheet->write($i, $key+1, iconv_cp949($m_value));
		}
	} elseif($i==2){
		foreach($age_arr as $key => $value){
			$w_value = number_format($db_s_all_row['w'.$value.'_sum'])." 건";
			$worksheet->write($i, $key+1, iconv_cp949($w_value));
		}
	}	
}

$ssh_cnt+=2;

/***************  본문 출력   **************/

// EXCEL용 배열
$data_head = $data_key = array(); // 분류값(컬럼) 및 데이터 출력 저장 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
	array_push($data_key, $val[1]);
} // end foreach

//컬럼 헤드 출력
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write($ssh_cnt, $col++, $cell);
} // end foreach



/* 데이터 전달 부분 */
for ($i=$ssh_cnt+1; $row=mysql_fetch_array($result); $i++){
	foreach($data_key as $cell_key => $cell_val) {
		switch($cell_val) {
			case "sas_comics" :
				$worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]));
				break;

			case "cm_series" :
				$comics_row = get_comics($row['sas_comics']);
				$db_cm_series = $comics_row['cm_series'];
				$worksheet->write($i, $cell_key , iconv_cp949($db_cm_series));
				break;

			case "all" :
				if($mb_sex=='m'){
					$all_txt = number_format($row['m_sum']).'건';
				} elseif($mb_sex=='w'){
					$all_txt = number_format($row['w_sum']).'건';
				} else {
					$all_txt = $row[$cell_val];
				}
				$worksheet->write($i, $cell_key , iconv_cp949($all_txt));
				break;

			default :
				if($mb_sex=='m'){
					$sum_txt = number_format($row['m'.$cell_val.'_sum']).'건';
				} elseif($mb_sex=='w'){
					$sum_txt = number_format($row['w'.$cell_val.'_sum']).'건';
				} elseif($mb_sex=='n'){
					$sum_txt = number_format($row['n_sum']).'건';
				}
				$worksheet->write($i, $cell_key , iconv_cp949($sum_txt));
				break;
		}
	}
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."comics_sex_age_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"comics_sex_age_".$today.".xls");
header("Content-Disposition: inline; filename=\"comics_sex_age_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;

?>