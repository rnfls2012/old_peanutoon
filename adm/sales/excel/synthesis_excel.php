<?php
$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once('../../../lib/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once('../../../lib/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

/***************  본문 출력   **************/

$temp = number_format($row_size);

// EXCEL용 배열
$data_head = $data_key = array(); // 분류값(컬럼), 본문 결과값 배열
foreach($fetch_row as $key => $val) {
	//컬럼 내용에 html 태그 제거
	if(strpos($val[0],"<br>") === false){
		array_push($data_head, $val[0]);
	} else {
		$temp=strip_tags($val[0]);
		array_push($data_head, $temp);		
	}
	array_push($data_key, $val[1]);
}

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write(0, $col++, $cell);
} // end foreach

/* 데이터 전달 부분 */
for ($i=1; $row=mysql_fetch_array($result); $i++){
	foreach($data_key as $cell_key => $cell_val) {
		switch($cell_val) {
			case "mc_member_id":
				$mb_temp = mb_get_no($row['mc_member']);
				$mc_member_id = $mb_temp['mb_id']." "; //숫자 아이디일 경우 뒤에 공백을 합쳐 엑셀 파일 내에서 문자로 취급함.
				$worksheet->write($i, $cell_key , iconv_cp949($mc_member_id));
				break;

			case "mc_won" :
				$mc_won = number_format($row['mc_won']).' 원';
				$worksheet->write($i, $cell_key , iconv_cp949($mc_won));
				break;
				
			case "mc_pg" :
				foreach($nm_config['cf_payway'] as $key => $val) {
					if($val[0] == $row['mc_pg'] && $val[1] == $row['mc_way']) {
						$authty = strtoupper($val[0])."(".$val[2].")";
					}
				}
				$worksheet->write($i, $cell_key , iconv_cp949($authty));
				break;

			case "mc_cash_point" :
				$mc_cash_point = number_format($row['mc_cash_point']).$nm_config['cf_cash_point_unit'];
				$worksheet->write($i, $cell_key , iconv_cp949($mc_cash_point));
				break;

			case "mc_point" :
				$mc_point = number_format($row['mc_point']).$nm_config['cf_point_unit'];
				$worksheet->write($i, $cell_key , iconv_cp949($mc_point));
				break;

			default :
				$worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]." "));
				break;
		}
	} // end foreach
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."synthesis_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"synthesis_".$today.".xls");
header("Content-Disposition: inline; filename=\"synthesis_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;
?>