<?
/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sr_year_month'));
array_push($fetch_row, array('요일','sr_week'));
array_push($fetch_row, array('총 결제-금액','sumpay'));
array_push($fetch_row, array('총 결제-건수','sumcnt'));
array_push($fetch_row, array('총 재구매건[율]','repay_cnt'));
array_push($fetch_row, array('남자 전체-금액','m_sum'));
array_push($fetch_row, array('남자 전체-건수','m_cnt'));
array_push($fetch_row, array('남자 전체-재구매건[율]','m_repay_cnt'));
array_push($fetch_row, array('남자 10대-금액','m10_sum'));
array_push($fetch_row, array('남자 10대-건수','m10_cnt'));
array_push($fetch_row, array('남자 10대-재구매건[율]','m10_repay_cnt'));
array_push($fetch_row, array('남자 20대-금액','m20_sum'));
array_push($fetch_row, array('남자 20대-건수','m20_cnt'));
array_push($fetch_row, array('남자 20대-재구매건[율]','m20_repay_cnt'));
array_push($fetch_row, array('남자 30대-금액','m30_sum'));
array_push($fetch_row, array('남자 30대-건수','m30_cnt'));
array_push($fetch_row, array('남자 30대-재구매건[율]','m30_repay_cnt'));
array_push($fetch_row, array('남자 40대-금액','m40_sum'));
array_push($fetch_row, array('남자 40대-건수','m40_cnt'));
array_push($fetch_row, array('남자 40대-재구매건[율]','m40_repay_cnt'));
array_push($fetch_row, array('남자 50대-금액','m50_sum'));
array_push($fetch_row, array('남자 50대-건수','m50_cnt'));
array_push($fetch_row, array('남자 50대-재구매건[율]','m50_repay_cnt'));
array_push($fetch_row, array('남자 60대-금액','m60_sum'));
array_push($fetch_row, array('남자 60대-건수','m60_cnt'));
array_push($fetch_row, array('남자 60대-재구매건[율]','m60_repay_cnt'));

$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// 해당 시트 컬럼 크기 변경
$worksheet->set_column(0, 1, 10);
$worksheet->set_column(1, 6, 15);

// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));

$center  = $workbook -> addformat(array(
								'align' => 'center',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));
								
//문서 출력일자 셀 출력
$worksheet->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt = 3;

/***************  검색 날짜 별 총 합 출력   **************/

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
} // end foreach

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write($row_cnt, $col++, $cell, $heading);
} // end foreach

$row_cnt++;

/* 데이터 전달 부분 */
for ($i=$row_cnt; $row=mysql_fetch_array($result); $i++){
	foreach($data_key as $cell_key => $cell_val) {
		$row['sr_year_month'] = $row['sr_year_month']."-".$row['sr_day'];
		$repay_cnt_val = $row['repay_cnt'];
		$sumcnt_val = $row['sumcnt'];
		if($row['sumcnt'] == 0){ $sumcnt_val = 1; }
		$repct_calc_val = intval(($repay_cnt_val / $sumcnt_val * 1000) / 10);
		$row['repay_cnt'] = number_format($row['repay_cnt'])." [ ".$repct_calc_val."% ]";
		
		// 연령별 구분
		for($j=0; $j<=60; $j+=10) {
			if($j == 0) {
				$age = "";
			} else {
				$age = $j;
			} // end else

			$repay_cnt_val = $row['m'.$age.'_repay_cnt'];
			$sumcnt_val = $row['m'.$age.'_cnt'];
			if($row['m'.$age.'_cnt'] == 0){ $sumcnt_val = 1; }
			$repct_calc_val = intval(($repay_cnt_val / $sumcnt_val * 1000) / 10);
			$row['m'.$age.'_repay_cnt'] = number_format($row['m'.$age.'_repay_cnt'])." [ ".$repct_calc_val."% ]";
		} // end for

		if($cell_val == "sr_week") {
			$sr_week = get_yoil($row[$cell_val],1,1);
			$worksheet->write($i, $cell_key , iconv_cp949($sr_week), $center);
		} else {
			$worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]), $f_price);
		} // end else
	} // end foreach
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."day_sex_age_man_excel_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"day_sex_age_man_excel_".$today.".xls");
header("Content-Disposition: inline; filename=\"day_sex_age_man_excel_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;
?>