<?
/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sr_year_month'));
array_push($fetch_row, array('요일','sr_week'));
array_push($fetch_row, array('총 결제-금액','sumpay'));
array_push($fetch_row, array('총 결제-건수','sumcnt'));

foreach($d_sr_way_payco as $d_sr_way_payco_key => $d_sr_way_payco_val) {
	if(in_array($d_sr_way_payco_key, $use_payco_code)) {
		array_push($fetch_row, array($d_sr_way_payco_val.' 건수',$d_sr_way_payco_key.'_cnt'));
		array_push($fetch_row, array($d_sr_way_payco_val.' 금액',$d_sr_way_payco_key.'_sum'));
	} // end if
} // end foreach

$today = date("Ymd");

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 해당 시트 당 컬럼 크기 변경
$worksheet->set_column(0, 1, 15);

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));

$center  = $workbook -> addformat(array(
								'align' => 'center',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));

//문서 출력일자 셀 출력
$worksheet->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt = 3; //합계 데이터 출력 행 cnt.

/**************  worksheet  *****************/

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
} // end foreach

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write($row_cnt, $col++, $cell, $heading);
} // end foreach

$row_cnt++;

/* 데이터 전달 부분 */
for ($i=$row_cnt; $row=mysql_fetch_array($result); $i++){
	foreach($data_key as $cell_key => $cell_val) {
		if($cell_val == "sr_week") {
			$sr_week = get_yoil($row[$cell_val],1,1);
			$worksheet->write($i, $cell_key , iconv_cp949($sr_week), $center);
		} else if($cell_val == "sr_year_month") {
			$sr_year_month = $row['sr_year_month']."-".$row['sr_day'];
			$worksheet->write($i, $cell_key , iconv_cp949($sr_year_month), $center);
		} else {
			$worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]), $f_price);
		} // end else
	} // end foreach
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."day_payway_payco".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"day_payway_payco".$today.".xls");
header("Content-Disposition: inline; filename=\"day_payway_payco".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;
?>