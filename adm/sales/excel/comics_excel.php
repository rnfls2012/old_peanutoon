<?
/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('코믹스번호','sl_comics'));
array_push($fetch_row, array('코믹스분류','sl_big'));
array_push($fetch_row, array('코믹스명','cm_series'));
array_push($fetch_row, array('장르','cm_small'));
array_push($fetch_row, array('완결 여부','cm_end'));
array_push($fetch_row, array('작가명','cm_author'));
array_push($fetch_row, array('출판사명','cm_publisher'));
array_push($fetch_row, array('총 가격','sl_won_sum'));
array_push($fetch_row, array('열람수','sl_open_sum'));
array_push($fetch_row, array('총 건수','sl_pay_sum'));
array_push($fetch_row, array('코믹스 등록일', 'cm_reg_date'));
array_push($fetch_row, array('에피소드 등록일', 'cm_episode_date'));

/* cash_point */
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'('.$nm_config['cf_cash_point_unit'].')','sl_cash_point_sum'));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].'(원)','sl_cash_point_won_sum'));

/* point */
array_push($fetch_row, array($nm_config['cf_point_unit_ko'].'('.$nm_config['cf_point_unit'].')','sl_point_sum'));
array_push($fetch_row, array($nm_config['cf_point_unit_ko'].'(원)','sl_point_won_sum'));

$today = date("Ymd");
if($_big == "") {
	$_big = 0;
}
$big_arr = array('전체', '단행본', '웹툰', '단행본 웹툰');
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 해당 시트 당 컬럼 크기 변경

$worksheet->set_column(0, 1, 10);
$worksheet->set_column(1, 6, 15);

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));

$center  = $workbook -> addformat(array(
								'align' => 'center',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));

//문서 출력일자 셀 출력
$worksheet->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt = 2; //합계 데이터 출력 행 cnt.

/***************  본문 출력   **************/

/**************  필터링 값 출력 ***************/

//검색 날짜 별 결과 항목 열 지정.
$search_keyword = array();
array_push($search_keyword, "분류");
array_push($search_keyword, "검색어");
array_push($search_keyword, "검색 작가");
array_push($search_keyword, "검색 출판사");
array_push($search_keyword, "검색 제공사");

//검색 결과 배열 저장. 순서 바꾸기 X
$search_result = array();
array_push($search_result, $nm_config['cf_big'][$_big]);
array_push($search_result, $s_text);
array_push($search_result, $professional_arr[${'_'.$cp_arr[0][0]}]);
array_push($search_result, $publisher_arr[${'_'.$cp_arr[1][0]}]);
array_push($search_result, $provider_arr[${'_'.$cp_arr[2][0]}]);

//시작 날짜, 끝 날짜 하나라도 있을 경우
if ($_s_date || $_e_date) {
 array_push($search_keyword, "검색 날짜");
 array_push($search_result, $_s_date."~".$_e_date);
}

//각 필터링 항목 별 count
$sk_cnt = count($search_keyword);
$sr_cnt = count($search_result);

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
} // end foreach

//필터링 항목 출력
for ($i=1; $i <= $sk_cnt; $i++) {
	$worksheet->write($i+$row_cnt, 0 , iconv_cp949($search_keyword[$i-1]), $heading);
}

//필터링 결과 출력
for($i=1; $i<=$sr_cnt; $i++){
	$worksheet->write($i+$row_cnt, 1, iconv_cp949($search_result[$i-1]), $right);
}

$row_cnt+=2;

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write($sk_cnt+$row_cnt, $col++, $cell, $heading);
} // end foreach

$row_cnt++;

/* 데이터 전달 부분 */

$i=$row_cnt+$sk_cnt;

foreach ($rows_data as $dvalue_key => $dvalue_val) {
	foreach ($data_key as $cell_key => $cell_val) {
		
		$get_comics = get_comics($dvalue_val['sl_comics']);

		switch($cell_val) {
			case "cm_series" :
			case "cm_reg_date" :
			case "cm_episode_date" :
				$worksheet->write($i, $cell_key , iconv_cp949($get_comics[$cell_val]), $center);
				break;

			case "cm_author" :
				$worksheet->write($i, $cell_key , iconv_cp949($get_comics['prof_name']), $center);
				break;

			case "cm_publisher" :
				$worksheet->write($i, $cell_key , iconv_cp949($get_comics['publ_name']), $center);
				break;

			case "cm_small" :
				$worksheet->write($i, $cell_key , iconv_cp949($nm_config['cf_small'][$get_comics['cm_small']]), $center);
				break;

			case "cm_end" :
				$worksheet->write($i, $cell_key , iconv_cp949($d_cm_end[$get_comics['cm_end']]), $center);
				break;

			case "sl_big" : 
				$worksheet->write($i, $cell_key , iconv_cp949($big_arr[$get_comics['cm_big']]), $center);
				break;

			default :
				$worksheet->write($i, $cell_key , iconv_cp949($dvalue_val[$cell_val]), $f_price);
				break;
		} // end switch

	} // end foreach
	$i++;
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."comics_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"comics_".$today.".xls");
header("Content-Disposition: inline; filename=\"comics_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
?>