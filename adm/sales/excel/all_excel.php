<?php
$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once('../../../lib/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once('../../../lib/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 해당 시트 당 컬럼 크기 변경

$worksheet->set_column(0, 1, 10);
$worksheet->set_column(1, 6, 15);

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));

$center  = $workbook -> addformat(array(
								'align' => 'center',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));

//문서 출력일자 셀 출력
$worksheet->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt = 2; //합계 데이터 출력 행 cnt.

/***************  연도 별 합계 출력   **************/

//합계 결과 항목 열 지정.
$years_sum_arr = array();
array_push($years_sum_arr, array('연도','sl_year'));
array_push($years_sum_arr, array('총 금액','total_year_sum'));
array_push($years_sum_arr, array('총건수','total_year_sum_cnt'));
array_push($years_sum_arr, array($nm_config['cf_cash_point_unit_ko'].' (원)','sl_cash_point_won_sum'));
array_push($years_sum_arr, array($nm_config['cf_cash_point_unit_ko'].' (P)','sl_cash_point_sum'));
array_push($years_sum_arr, array($nm_config['cf_point_unit_ko'].' (원)','sl_point_won_sum'));
array_push($years_sum_arr, array($nm_config['cf_point_unit_ko'].' (mP)','sl_point_sum'));

$ysa_cnt = count($years_sum_arr);

// EXCEL용 배열
$data_head_ysm = $data_key_ysm = array(); // 분류값(컬럼) 및 데이터 출력 저장 배열
foreach($years_sum_arr as $key => $val) {
	array_push($data_head_ysm, $val[0]);
	array_push($data_key_ysm, $val[1]);
}

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head_ysm = array_map('iconv_cp949', $data_head_ysm);
foreach($data_head_ysm as $cell) {
	$worksheet->write($row_cnt, $col++, $cell, $heading);
}

$row_cnt++;

// 합계 데이터 출력
for ($i=$row_cnt; $row=mysql_fetch_array($result_y); $i++){
	foreach($data_key_ysm as $cell_key => $cell_val) {		
		switch($cell_val) {
			case "sl_year" :
				$sl_year = $row['sl_year'].' 년';
				$worksheet->write($i, $cell_key , iconv_cp949($sl_year), $heading);
				break;

			case "total_year_sum" :
				$sl_won_sum = $row['sl_cash_point_won_sum']+$row['sl_point_won_sum'];
				$total_1 += $sl_won_sum;
				$worksheet->write($i, $cell_key , iconv_cp949($sl_won_sum), $f_price);
				break;

			case "total_year_sum_cnt" :
				$sl_cnt = $row['sl_cnt'];
				$total_2 += $sl_cnt;
				$worksheet->write($i, $cell_key , iconv_cp949($sl_cnt), $f_price);
				break;

			case "sl_cash_point_won_sum" :
				$sl_cash_point_won_sum = $row['sl_cash_point_won_sum'];
				$total_3 += $sl_cash_point_won_sum;
				$worksheet->write($i, $cell_key , iconv_cp949($sl_cash_point_won_sum), $f_price);
				break;

			case "sl_cash_point_sum" :
				$sl_cash_point_sum = $row['sl_cash_point_sum'];
				$total_4 += $sl_cash_point_sum;
				$worksheet->write($i, $cell_key , iconv_cp949($sl_cash_point_sum), $f_price);
				break;

			case "sl_point_won_sum" : 
				$sl_point_won_sum = $row['sl_point_won_sum'];
				$total_5 += $sl_point_won_sum;
				$worksheet->write($i, $cell_key , iconv_cp949($sl_point_won_sum), $f_price);
				break;

			case "sl_point_sum" :
				$sl_point_sum =$row['sl_point_sum'];
				$total_6 += $sl_point_sum;
				$worksheet->write($i, $cell_key , iconv_cp949($sl_point_sum), $f_price);
				break;

		/* 합계를 위한 결과이므로 디폴트 설정 안함
			default :
				$worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]));
				break;
		*/
		}
	}
	$row_cnt++; //모든 열 출력 후 행 cnt+1
}

/* 연도 별 총 합계 출력 */
$worksheet->write($row_cnt, 0, iconv_cp949("합 계"), $heading);
$worksheet->write($row_cnt, 1, iconv_cp949($total_1), $f_price);
$worksheet->write($row_cnt, 2, iconv_cp949($total_2), $f_price);
$worksheet->write($row_cnt, 3, iconv_cp949($total_3), $f_price);
$worksheet->write($row_cnt, 4, iconv_cp949($total_4), $f_price);
$worksheet->write($row_cnt, 5, iconv_cp949($total_5), $f_price);
$worksheet->write($row_cnt, 6, iconv_cp949($total_6), $f_price);

$row_cnt+=2;
/***************  본문 출력   **************/

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
} // end foreach

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write($row_cnt, $col++, $cell, $heading);
} // end foreach

$row_cnt++;

/* 데이터 전달 부분 */
for ($i=$row_cnt; $row=mysql_fetch_array($result); $i++){
	foreach($data_key as $cell_key => $cell_val) {
		switch($cell_val) {
			case "sl_year_month" :
				switch($row['sl_year']) {
						case "2015" :
							$format = $workbook->addformat(array('fg_color' => 23, 'align' => 'center', 'border' => 1));	// color : 0x16
							$worksheet->write($i, $cell_key , iconv_cp949($row['sl_year_month']), $format);
							break;

						case "2016" :				
							$format = $workbook->addformat(array('fg_color' => 22, 'align' => 'center', 'border' => 1));	// color : 0x1A
							$worksheet->write($i, $cell_key , iconv_cp949($row['sl_year_month']), $format);
							break;

						case "2017" :				
							$format = $workbook->addformat(array('fg_color' => 26, 'align' => 'center', 'border' => 1));	// color : 0x1B
							$worksheet->write($i, $cell_key , iconv_cp949($row['sl_year_month']), $format);
							break;

						case "2018" :				
							$format = $workbook->addformat(array('fg_color' => 44, 'align' => 'center', 'border' => 1));	// color : 0x2C
							$worksheet->write($i, $cell_key , iconv_cp949($row['sl_year_month']), $format);
							break;
					}
				break;
				
			case "sl_sum_won" :
				$sl_sum_won = $row['sl_cash_point_won_sum']+$row['sl_point_won_sum'];
				$worksheet->write($i, $cell_key , iconv_cp949($sl_sum_won), $f_price);
				break;

			case "sl_sum_cnt" :				
				$sl_sum_cnt = $row['sl_cnt'];
				$worksheet->write($i, $cell_key , iconv_cp949($sl_sum_cnt), $f_price);
				break;

			case "sl_cash_point_won_sum" :
				$sl_cash_point_won_sum = $row['sl_cash_point_won_sum'];
				$worksheet->write($i, $cell_key , iconv_cp949($sl_cash_point_won_sum), $f_price);
				break;

			case "sl_cash_point_sum" :
				$sl_cash_point_sum = $row['sl_cash_point_sum'];
				$worksheet->write($i, $cell_key , iconv_cp949($sl_cash_point_sum), $f_price);
				break;

			case "sl_point_won_sum" : 
				$sl_point_won_sum = $row['sl_point_won_sum'];
				$worksheet->write($i, $cell_key , iconv_cp949($sl_point_won_sum), $f_price);
				break;

			case "sl_point_sum" :
				$sl_point_sum = $row['sl_point_sum'];
				$worksheet->write($i, $cell_key , iconv_cp949($sl_point_sum), $f_price);
				break;

			default :
				$worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]), $center);
				break;
		}
	} // end foreach
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."all_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"all_".$today.".xls");
header("Content-Disposition: inline; filename=\"all_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;
?>