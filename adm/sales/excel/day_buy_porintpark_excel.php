<?php
/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','srp_year_month'));
array_push($fetch_row, array('요일','srp_week'));
array_push($fetch_row, array('금액','sumpay'));
array_push($fetch_row, array('건수','sumcnt'));
array_push($fetch_row, array('재구매건[율]','repay_cnt'));

$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();
// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 해당 시트 당 컬럼 크기 변경

$worksheet->set_column(0, 1, 10);
$worksheet->set_column(1, 6, 15);

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
    'align' => 'right',
    'border' => 1
));

$center  = $workbook -> addformat(array(
    'align' => 'center',
    'border' => 1
));

$heading = $workbook -> addformat(array(
    'align' => 'center',
    'border' => 1,
    'bold' => 1,
    'fg_color' => 27
));

$date_format = $workbook -> addformat(array(
    'bold' => 1
));

$cell_format = $workbook -> addformat(array(
    'size' => 15,
    'bold' => 1
));

//문서 출력일자 셀 출력
$worksheet->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt = 3; //합계 데이터 출력 행 cnt.

/***************  본문 결과 출력   **************/

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
    array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
    array_push($data_key, $val[1]);
} // end foreach

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
    $worksheet->write($row_cnt, $col++, $cell, $heading);
} // end foreach

$row_cnt++;

/* 데이터 전달 부분 */
for ($i=$row_cnt; $row=mysql_fetch_array($result); $i++){
    foreach($data_key as $cell_key => $cell_val) {
        switch($cell_val) {
            case "srp_year_month" :
                $srp_year_month = $row['srp_year_month']."-".$row['srp_day'];
                $worksheet->write($i, $cell_key , iconv_cp949($srp_year_month), $center);
            break;

            case "srp_week" :
                $srp_week = get_yoil($row['srp_week'], 1, 1);
                $worksheet->write($i, $cell_key , iconv_cp949($srp_week), $center);
            break;

            case "sumpay" :
                $sumpay = $row['sumpay'];
                $total_sum += $sumpay;
                $worksheet->write($i, $cell_key , iconv_cp949($sumpay), $f_price);
            break;

            case "sumcnt" :
                $sumcnt = $row['sumcnt'];
                $total_cnt += $sumcnt;
                $worksheet->write($i, $cell_key , iconv_cp949($sumcnt), $f_price);
            break;

            case "repay_cnt" :
                $repay_cnt = $row['repay_cnt'];
                $repay_rate = intval(($repay_cnt / $sumcnt) * 100);
                $repay = $row['repay_cnt']." 건 [".$repay_rate." % ]";
                $worksheet->write($i, $cell_key , iconv_cp949($repay), $f_price);
            break;

            default :
                $worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]), $f_price);
            break;
        }
    } // end foreach
    $row_cnt++;
} // end for 쿼리 조회결과가 없을때까지 조회

$row_cnt++;

$worksheet -> write($row_cnt, 1, iconv_cp949("합계"), $heading);
$worksheet -> write($row_cnt, 2, iconv_cp949($total_sum), $f_price);
$worksheet -> write($row_cnt, 3, iconv_cp949($total_cnt), $f_price);
$workbook->close();

$workbook->close();

header("Content-Disposition: attachment;filename="."day_buy_pointpark_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"day_buy_pointpark_".$today.".xls");
header("Content-Disposition: inline; filename=\"day_buy_pointpark_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;