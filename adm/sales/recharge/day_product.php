<?
include_once '_common.php'; // 공통

/* PARAMITER 
maker_name // 제공사이름
*/


/* 데이터 가져오기 */
$where_sales_recharge = ""; /* 충전 */

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }
if($_s_date && $_e_date){
	$where_sales_recharge.= date_year_month($_s_date, $_e_date, 'sr');
}

// 그룹으로 가져오기 - 날짜별 가격만
$recharge_price_group_arr = array();
$sql_rpg = " select * from sales_recharge where 1 ".date_year_month($_s_date, $_e_date, 'sr')." group by sr_pay, sr_cash_point order by sr_cash_point asc, sr_pay asc";
$result_rpg = sql_query($sql_rpg);
while ($row_rpg = mysql_fetch_array($result_rpg)) {
	array_push($recharge_price_group_arr, $row_rpg);
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "sr_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 요일 정렬시
if($_order_field == "sr_week"){$_order_field_add = " , sr_date "; $_order_add = "desc ";}
// 날짜 정렬시
if($_order_field == "sr_year_month"){$_order_field_add = " , sr_date "; $_order_add = $_order;}
$order_sales_recharge = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;


// 상품리스트
$product_arr = array();
$sql_sales_recharge_product = "select sr_cash_point, sr_pay, sr_pay FROM sales_recharge where 1 $where_sales_recharge group by sr_cash_point order by sr_cash_point *1";
$result_product = sql_query($sql_sales_recharge_product);
while ($row_product = mysql_fetch_array($result_product)) {
	array_push($product_arr, $row_product);
}
$sql_sales_recharge = "";
$field_sales_recharge = "   sr_cash_point, 
							sr_year_month, 
							sr_day, 
							sr_week, 
							sum(sr_pay)as sumpay, 
							count(sr_day)as sumcnt, 
							sum(sr_re_pay)as repay_cnt,
							sum(sr_event)as event_cnt, ";
foreach($product_arr as $product_key => $product_val){
$field_sales_recharge.= "
							count(if(sr_pay='".$product_val['sr_pay']."',1,null))as ".$product_val['sr_pay']."_cnt, 
							sum(if(sr_pay='".$product_val['sr_pay']."',sr_pay,null))as ".$product_val['sr_pay']."_sum, 
							sum(if(sr_pay='".$product_val['sr_pay']."',sr_re_pay,null))as ".$product_val['sr_pay']."_repay_cnt,
							sum(if(sr_pay='".$product_val['sr_pay']."',sr_event,null))as ".$product_val['sr_pay']."_event_cnt, ";
						
}
// 끝에 , 빼기.
$field_sales_recharge = substr($field_sales_recharge,0,strrpos($field_sales_recharge, ","));
$group_sales_recharge.= " group by sr_year_month, sr_day ";
$limit_sales_recharge = "";

$sql_sales_recharge = "select $field_sales_recharge FROM sales_recharge where 1 $where_sales_recharge $group_sales_recharge $order_sales_recharge $limit_sales_recharge";
$result = sql_query($sql_sales_recharge);
$row_size = sql_num_rows($result);

if($_mode == 'excel') {
	include_once $_cms_folder_top_path.'/excel/day_product_excel.php';
} // end if

/* 검색 전체 구하기 - group by 빼고 sum, count함수 이용 */
$sql_sales_recharge_s_all = "select $field_sales_recharge FROM sales_recharge where 1 $where_sales_recharge $order_sales_recharge $limit_sales_recharge";
$sql_sales_recharge_s_all_row = sql_fetch($sql_sales_recharge_s_all );
$s_all_sumpay = $sql_sales_recharge_s_all_row['sumpay'];
$s_all_sumcnt = $sql_sales_recharge_s_all_row['sumcnt'];
$s_all_repay_cnt = $sql_sales_recharge_s_all_row['repay_cnt'];

$s_all_sumpay_text = number_format($s_all_sumpay)." 원";
$s_all_sumcnt_text = number_format($s_all_sumcnt)." 건";
if($s_all_sumcnt == 0){ $s_all_sumcnt = 1; }
$s_all_repay_cnt_text = number_format($s_all_repay_cnt)." 건 [ ".intval(($s_all_repay_cnt / $s_all_sumcnt * 1000) / 10)."% ]";

/* 상품별 */
foreach($product_arr as $product_key => $product_val){

	${$product_val.'_cnt'} = $sql_sales_recharge_s_all_row[$product_val['sr_pay'].'_cnt'];
	if(${$product_val.'_cnt'} == 0){ ${$product_val.'_cnt'} = 1; }

	${'s_'.$product_val['sr_pay'].'_repct'} = round((intval($sql_sales_recharge_s_all_row[$product_val['sr_pay'].'_repay_cnt']) / ${$product_val.'_cnt'} * 1000) / 10, 1)."%";
	${'s_'.$product_val['sr_pay'].'_evpct'} = round((intval($sql_sales_recharge_s_all_row[$product_val['sr_pay'].'_event_cnt']) / ${$product_val.'_cnt'} * 1000) / 10, 1)."%";
}

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sr_year_month',9));
array_push($fetch_row, array('요일','sr_week',8));

foreach($product_arr as $product_key => $product_val){
	$recharge_price_group_txt = array();
	foreach($recharge_price_group_arr as $recharge_price_group_val){
		if($recharge_price_group_val['sr_cash_point']==$product_val['sr_cash_point']){
			array_push($recharge_price_group_txt, "<br/>".$recharge_price_group_val['sr_pay']."원");			
		}
	}
	array_push($fetch_row, array(number_format($product_val['sr_cash_point']).$nm_config['cf_cash_point_unit'].implode("",$recharge_price_group_txt),$product_val['sr_pay'],7));
}

array_push($fetch_row, array('결제-금액','sumpay',6));
array_push($fetch_row, array('결제-건수','sumcnt',5));
array_push($fetch_row, array('결제-재구매건[율]','pct',5));

foreach($product_arr as $product_key => $product_val){
	array_push($fetch_row, array('금액',$product_val['sr_pay'].'_sum',4));
	array_push($fetch_row, array('건수',$product_val['sr_pay'].'_cnt',4));
}
foreach($product_arr as $product_key => $product_val){
	$field_val = 0;
	if($product_key == 0){ $field_val = 6; }
	array_push($fetch_row, array('재구매건[율]',$product_val['sr_pay'].'_repay_cnt',$field_val));
	array_push($fetch_row, array('이벤트건[율]',$product_val['sr_pay'].'_event_cnt',0));
}

// css 클래스 접두사
$css_suffix = "dpt_";

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "일별 결제 상품별";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<script type="text/javascript">
<!--
	var cr_thead_mg_add = '66px'; /* fixed 테이블이 높이가 커서 그만큼 마진 */
//-->
</script>

<style type="text/css">
/* 상품리스트에 따른 width값 처리 - 결제-금액, 결제-건수, 결제-재구매건[율]제외 
cms_sales.css에서 #recharge_result div .dpt_sumpay, #recharge_result div .dpt_sumcnt, #recharge_result div .dpt_pct
값를 제외한 나머지가 1130입니다. 또한 css3 기법인 box-sizing 처리해줘야 합니다.
전체는 1400이고요
*/
<? 
$totol_width = 1130;
$product_count = intval(count($product_arr) * 2) ;
$product_width = intval($totol_width / $product_count);
$product_width_last = $product_width + ($totol_width - ($product_count * $product_width));
?>
	
	#recharge_result div .cash_line{width:<?=$product_width;?>px;}
	#recharge_result div .cash_line:last-child{width:<?=$product_width_last;?>px;}
<? /* 배경색 */
$product_bgcolor = array('#f5a3ad','#fdba99','#f6fd99','#bef5a3','#a3cff5','#b4a3f5','#c57cc3');
foreach($product_arr as $product_key => $product_val){ ?>
	#recharge_result div .<?=$css_suffix.$product_val['sr_pay']?>{background:<?=$product_bgcolor[$product_key]?>}
<?}?>
</style>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
	<div class="write">
		<input type="button" class="excel_down" value="검색 결과 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?s_date=<?=$_s_date;?>&e_date=<?=$_e_date;?>&order_field=<?=$_order_field;?>&order=<?=$_order;?>&mode=excel'"/>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_product_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="sales_result" class="day_product_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 - 총금액: <?=$s_all_sumpay_text;?> / 총건수: <?=$s_all_sumcnt_text;?> / 총 재구매건[율]: <?=$s_all_repay_cnt_text;?></strong>
		<?if($s_all_sumpay!= ''){?>
			<table>
				<tr>
					<th class="topleft">상품별</th>
					<? foreach($product_arr as $product_key => $product_val){ 
						$th_class = "";
						if((count($product_arr)-1) == $product_key){ $th_class = " class='topright' "; }
					?>
						<th <?=$th_class;?>>
							<?=$product_val['sr_cash_point'];?><?=$nm_config['cf_cash_point_unit']?>
							<? foreach($recharge_price_group_arr as $recharge_price_group_val){ 
								echo $recharge_price_group_val['sr_cash_point']==$product_val['sr_cash_point']?"<br/>".$recharge_price_group_val['sr_pay']."원":"";
							} ?>
						</th>
					<?}?>
				</tr>
				<tr>
					<th>금액</th>
					<? foreach($product_arr as $product_key => $product_val){  ?>
						<td><?=number_format($sql_sales_recharge_s_all_row[$product_val['sr_pay'].'_sum']);?> 원</td>
					<?}?>
				</tr>
				<tr>
					<th>건수</th>
					<? foreach($product_arr as $product_key => $product_val){  ?>
						<td><?=number_format($sql_sales_recharge_s_all_row[$product_val['sr_pay'].'_cnt']);?> 건</td>
					<?}?>
				</tr>
				<tr>
					<th>재구매건[율]</th>
					<? foreach($product_arr as $product_key => $product_val){  ?>
						<td><?=number_format($sql_sales_recharge_s_all_row[$product_val['sr_pay'].'_repay_cnt']);?> 건 [<?=${'s_'.$product_val['sr_pay'].'_repct'};?>]</td>
					<?}?>
				</tr>
				<tr>
					<th>이벤트건[율]</th>
					<? foreach($product_arr as $product_key => $product_val){  ?>
						<td><?=number_format($sql_sales_recharge_s_all_row[$product_val['sr_pay'].'_event_cnt']);?> 건 [<?=${'s_'.$product_val['sr_pay'].'_evpct'};?>]</td>
					<?}?>
				</tr>
			</table>
		<?}?>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1 || $fetch_val[2] == 9 || $fetch_val[2] == 8){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $css_suffix.$fetch_val[1];

					if(strpos($th_class, '_event_cnt') > 0){$th_class.= " dpt_event_cnt "; }

					/* 번호에 의한 rowspan, colspan 설정 */
					$rowspan = $colspan = $th_class_btm_w1 = $th_class_color = $thead_tr = "";
					switch($fetch_val[2]){
						case 9 : $colspan = " colspan='2' "; $th_class_btm_w1 = " btm_w1 ";
						break;

						case 8 : $th_class_btm_w1 = " btm_w1 cash_line";
						break;

						case 7 : $colspan = " colspan='2' "; $th_class_color = $th_class."_color"; $th_class_btm_w1 = " btm_w1 ";
						break;

						case 6 : $rowspan = " rowspan='2' "; $thead_tr = " </tr>\n<tr>\n ";
						break;

						case 5 : $rowspan = " rowspan='2' ";
						break;

						case 4 : $th_class_btm_w1 = " btm_w1 cash_line";
						break;
					}

				?>
					<?=$thead_tr?>
					<th <?=$rowspan?> <?=$colspan?> class="<?=$th_class?> <?=$th_class_color?> <?=$th_class_btm_w1?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 날짜 요일 결제-금액 결제-건수 결제-재구매건[율] */
					$sr_year_month_day = $dvalue_val['sr_year_month']."-".$dvalue_val['sr_day'];
					$sr_week = get_yoil($dvalue_val['sr_week'],1,1); 
					$sumpay = number_format($dvalue_val['sumpay'])." 원";
					$sumcnt = number_format($dvalue_val['sumcnt'])." 건";
					$repay_cnt_val = $dvalue_val['repay_cnt'];
					$sumcnt_val = $dvalue_val['sumcnt'];
					if($dvalue_val['sumcnt'] == 0){ $sumcnt_val = 1; }
					$repct_calc_val = intval(($repay_cnt_val / $sumcnt_val * 1000) / 10);
					$repct = number_format($dvalue_val['repay_cnt'])." 건 [ ".$repct_calc_val."% ]";

					/* 요일별 class */
					$week_class = $week_en[$dvalue_val['sr_week']];
				?>
				<tr class="<?=$week_class?>">
					<td colspan='2' class="dpt_sr_year_month btm_not text_center"><strong><?=$sr_year_month_day;?></strong></td>
					<td class="dpt_sr_week btm_not text_center"><strong><?=$sr_week;?></strong></td>

					<? foreach($product_arr as $product_key => $product_val){ 
					
						${$product_val['sr_pay'].'_cnt'} = $dvalue_val[$product_val['sr_pay'].'_cnt'];
						if(${$product_val['sr_pay'].'_cnt'} == 0){ ${$product_val['sr_pay'].'_cnt'} = 1; }


						${'db_'.$product_val['sr_pay'].'_cnt_repay_pct'} = round((intval($dvalue_val[$product_val['sr_pay'].'_repay_cnt']) / ${$product_val['sr_pay'].'_cnt'} * 1000) / 10, 1)."%";
					?>
						<td class="<?=$css_suffix?>fl<?=$product_val['sr_pay']?> cash_line <?=$css_suffix.$product_val['sr_pay']?> text_center"><?=number_format($dvalue_val[$product_val['sr_pay'].'_sum']);?>원</td>
						<td class="<?=$css_suffix?>fl<?=$product_val['sr_pay']?> cash_line <?=$css_suffix.$product_val['sr_pay']?> text_center"><?=number_format($dvalue_val[$product_val['sr_pay'].'_cnt']);?>건</td>
					<?}?>
				</tr>
				<tr class="<?=$week_class?>">
					<td class="<?=$css_suffix?>sumpay text_center"><?=$sumpay;?></td>
					<td class="<?=$css_suffix?>sumcnt text_center"><?=$sumcnt;?></td>
					<td class="<?=$css_suffix?>pct text_center"><?=$repct;?></td>
					<? foreach($product_arr as $product_key => $product_val){ 
					
						${$product_val['sr_pay'].'_cnt'} = $dvalue_val[$product_val['sr_pay'].'_cnt'];
						if(${$product_val['sr_pay'].'_cnt'} == 0){ ${$product_val['sr_pay'].'_cnt'} = 1; }


						${'db_'.$product_val['sr_pay'].'_cnt_repay_pct'} = round((intval($dvalue_val[$product_val['sr_pay'].'_repay_cnt']) / ${$product_val['sr_pay'].'_cnt'} * 1000) / 10, 1)."%";
						${'db_'.$product_val['sr_pay'].'_cnt_event_pct'} = round((intval($dvalue_val[$product_val['sr_pay'].'_event_cnt']) / ${$product_val['sr_pay'].'_cnt'} * 1000) / 10, 1)."%";
					?>
						<td class="<?=$css_suffix?>fl<?=$product_val['sr_pay']?> cash_line <?=$css_suffix.$product_val['sr_pay']?> text_center"><?=number_format($dvalue_val[$product_val['sr_pay'].'_repay_cnt']);?>건[<?=${'db_'.$product_val['sr_pay'].'_cnt_repay_pct'};?>]</td>
						<td class="<?=$css_suffix?>fl<?=$product_val['sr_pay']?> cash_line <?=$css_suffix.$product_val['sr_pay']?> <?=$css_suffix?>event_cnt text_center"><?=number_format($dvalue_val[$product_val['sr_pay'].'_event_cnt']);?>건[<?=${'db_'.$product_val['sr_pay'].'_cnt_event_pct'};?>]</td>
					<?}?>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>