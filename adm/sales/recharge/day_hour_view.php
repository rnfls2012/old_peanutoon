<?php
include_once '_common.php'; // 공통

$year_month_day     = base_filter($_GET['year_month_day']);
$hour               = base_filter($_GET['hour']);

$year_month         = substr($year_month_day,0,7);
$day                = substr($year_month_day,8,2);

$pay_info = array(
        ''     => '에러',
        'mobx' => '휴대폰',
        'card' => '카드결제',
        'acnt' => '계좌이체',
        'scbl' => '도서문화상품권',
        'schm' => '해피머니상품권',
        'sccl' => '문화상품권',
        'toss' => '토스',
        'payco'=> '페이코'
);

$platform_into = array();
foreach ( $pay_info as $pay_key => $pay_val ) {
    $case_when .= "WHEN '".$pay_key."' THEN '".$pay_val."' ";
}

$select_field = "sr_pay, sr_cash_point, sr_point, sr_way, sr_min";
$select_case_field = "
(CASE sr_way $case_when ELSE sr_way END) AS payInfo,
(CASE sr_platform WHEN 1 THEN 'WEB' WHEN 2 THEN 'Android' WHEN 4 THEN 'IOS' WHEN 8 THEN 'AppMarket' WHEN 16 THEN 'AppSetApk' ELSE sr_platform END) AS platform
";

$where_field = "AND sr_year_month = '".$year_month."' ";
$where_field .= "AND sr_day = '".$day."' ";
$where_field .= "AND sr_hour = '".$hour."' ";

$sql_sales_recharge = "
SELECT $select_field, $select_case_field FROM sales_recharge 
WHERE 1 $where_field
";

$result = sql_query($sql_sales_recharge);

$temp_arr = array();
while($row = sql_fetch_array($result)){
    array_push($temp_arr, $row);
}

$fetch_row = array();
array_push($fetch_row, array('시간대', 'sr_min'));
array_push($fetch_row, array('결제금액', 'sr_pay'));
array_push($fetch_row, array('결제방법', 'payInfo'));
array_push($fetch_row, array('결제환경', 'platform'));

$cms_head_title = "시간대별 상세 통계 ".$year_month_day;

if($_mode == 'excel') {
    $result = sql_query($sql_sales_recharge);
    include_once $_cms_folder_top_path.'/excel/day_hour_view_excel.php';
}

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="sales_result">
    <h1 class="day_view">시간대별 상세 통계 : <?=$year_month_day ?>  <?=$hour?>시</h1>
    <div class="write">
        <input type="button" class="excel_down" value="엑셀 다운로드"	onclick="location.href='<?=$_cms_self?>?<?=$_nm_paras;?>&hour=<?=$hour?>&mode=excel'">
    </div>
    <div id="cr_bg">
        <table class="list">
            <thead>
            <tr>
                <th rowspan="2">시간대</th>
                <th colspan="3">결제 정보</th>
            </tr>
            <tr>
                <th>결제금액</th>
                <th>결제방법</th>
                <th>결제환경</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($temp_arr as $key => $val) {
                ?>
                <tr>
                    <td><?=$hour?>:<?=$val['sr_min'] ?></td>
                    <td><?=number_format($val['sr_pay'])?></td>
                    <td><?=$val['payInfo'] ?></td>
                    <td><?=$val['platform'] ?></td>
                </tr>
                <?
            }
            ?>
            <tr>
                <td colspan="4" class="btn">
                    <input type="reset" value="닫기" onclick="self.close()">
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</section>