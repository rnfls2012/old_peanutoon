<?
include_once '_common.php'; // 공통

/* PARAMITER 
maker_name // 제공사이름
*/

/* 데이터 가져오기 */
$where_sales_recharge = ""; /* 충전 */

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }
if($_s_date && $_e_date){
	$where_sales_recharge.= date_year_month($_s_date, $_e_date, 'sr');
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "sr_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 요일 정렬시
if($_order_field == "sr_week"){$_order_field_add = " , sr_date "; $_order_add = "desc ";}
// 날짜 정렬시
if($_order_field == "sr_year_month"){$_order_field_add = " , sr_date "; $_order_add = $_order;}
$order_sales_recharge = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

$sql_sales_recharge = "";
$field_sales_recharge = "sr_year_month,
						 sr_day,
						 sr_week,
						 sum(sr_pay) as sumpay,
						 count(sr_day) as sumcnt,
						 sum(sr_re_pay) as repay_cnt, ";

foreach($d_sr_way as $d_sr_way_key => $d_sr_way_val) {
	if(end(array_keys($d_sr_way)) != $d_sr_way_key) {
		$field_sales_recharge .= "count(if(sr_way='".$d_sr_way_key."',1,null)) as ".$d_sr_way_key."_cnt, 
								  sum(if(sr_way='".$d_sr_way_key."',sr_pay,0)) as ".$d_sr_way_key."_sum, ";
	} else {
		$field_sales_recharge .= "count(if(sr_way='".$d_sr_way_key."',1,null)) as ".$d_sr_way_key."_cnt, 
								  sum(if(sr_way='".$d_sr_way_key."',sr_pay,0)) as ".$d_sr_way_key."_sum";
	} // end else
} // end foreach

$group_sales_recharge = " group by sr_year_month, sr_day ";
$limit_sales_recharge = "";

$sql_sales_recharge = "select $field_sales_recharge FROM sales_recharge where 1 $where_sales_recharge $group_sales_recharge $order_sales_recharge $limit_sales_recharge";
$result = sql_query($sql_sales_recharge);
$row_size = sql_num_rows($result);

/* 검색 전체 구하기 - group by 빼고 sum, count함수 이용 */
$sql_sales_recharge_s_all = "select $field_sales_recharge FROM sales_recharge where 1 $where_sales_recharge $order_sales_recharge $limit_sales_recharge";
$sql_sales_recharge_s_all_row = sql_fetch($sql_sales_recharge_s_all );
$s_all_sumpay = $sql_sales_recharge_s_all_row['sumpay'];
$s_all_sumcnt = $sql_sales_recharge_s_all_row['sumcnt'];
$s_all_repay_cnt = $sql_sales_recharge_s_all_row['repay_cnt'];

$s_all_sumpay_text = number_format($s_all_sumpay)." 원";
$s_all_sumcnt_text = number_format($s_all_sumcnt)." 건";
if($s_all_sumcnt == 0){ $s_all_sumcnt = 1; }

foreach($d_sr_way as $d_sr_way_key => $d_sr_way_val) {
	${"s_".$d_sr_way_key."_pct"} = round((intval($sql_sales_recharge_s_all_row[$d_sr_way_key.'_cnt']) / $s_all_sumcnt * 1000) / 10, 1)."%";
} // end foreach

$s_all_repay_cnt_text = number_format($s_all_repay_cnt)." 건 [ ".intval(($s_all_repay_cnt / $s_all_sumcnt * 1000) / 10)."% ]";

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sr_year_month',9));
array_push($fetch_row, array('요일','sr_week',8));

foreach($d_sr_way as $d_sr_way_key => $d_sr_way_val) {
	array_push($fetch_row, array($d_sr_way_val, $d_sr_way_key ,7));
} // end foreach

array_push($fetch_row, array('결제-금액','sumpay',6));
array_push($fetch_row, array('결제-건수','sumcnt',5));
array_push($fetch_row, array('결제-재구매건[율]','pct',5));

foreach($d_sr_way as $d_sr_way_key => $d_sr_way_val) {
	array_push($fetch_row, array('금액'.'<br>'.'건수[이용율]',$d_sr_way_key.'_sum_cnt',0));
} // end foreach

// css 클래스 접두사
$css_suffix = "dp_";

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "일별 결제 종류별";
$cms_head_title = $page_title;

if($_mode == 'excel') {
	include_once $_cms_folder_top_path.'/excel/day_payway_excel.php';
} // end if

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<script type="text/javascript">
<!--
	var cr_thead_mg_add = '35px'; /* fixed 테이블이 높이가 커서 그만큼 마진 */
//-->
</script>

<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
	<div class="write">
		<input type="button" class="excel_down" value="검색 결과 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?s_date=<?=$_s_date;?>&e_date=<?=$_e_date;?>&order_field=<?=$_order_field;?>&order=<?=$_order;?>&mode=excel'"/>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 - 총금액: <?=$s_all_sumpay_text;?> / 총건수: <?=$s_all_sumcnt_text;?> / 총 재구매건[율]: <?=$s_all_repay_cnt_text;?></strong>
		<?if($s_all_sumpay!= ''){?>
			<table>
				<tr>
					<th class="topleft">종류별</th>
					<? foreach($d_sr_way as $d_sr_way_key => $d_sr_way_val) { 
						if(end(array_keys($d_sr_way)) != $d_sr_way_key) { ?>
							<th><?=$d_sr_way_val;?></th>
					<?  } else { ?>
							<th class="topright"><?=$d_sr_way_val;?></th>
					<?  } // end else
					} // end foreach ?>
				</tr>
				<tr>
					<th>금액</th>
					<? foreach($d_sr_way as $d_sr_way_key => $d_sr_way_val) { ?>
						<td><?=number_format($sql_sales_recharge_s_all_row[$d_sr_way_key.'_sum']);?>원</td>
					<? } // end foreach ?>
				</tr>
				<tr>
					<th>건수</th>
					<? foreach($d_sr_way as $d_sr_way_key => $d_sr_way_val) { ?>
						<td><?=number_format($sql_sales_recharge_s_all_row[$d_sr_way_key.'_cnt']);?>건</td>
					<? } // end foreach ?>
				</tr>
				<tr>
					<th>이용율</th>
					<? foreach($d_sr_way as $d_sr_way_key => $d_sr_way_val) { ?>
						<td><?=${"s_".$d_sr_way_key."_pct"};?></td>
					<? } // end foreach ?>
				</tr>
			</table>
		<?}?>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1 || $fetch_val[2] == 8 || $fetch_val[2] == 9){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $css_suffix.$fetch_val[1];

					/* 번호에 의한 rowspan, colspan 설정 */
					$rowspan = $colspan = $th_class_btm_w1 = $th_class_color = $thead_tr = "";
					switch($fetch_val[2]){
						case 9 : $colspan = " colspan='2' "; $th_class_btm_w1 = " btm_w1 ";
						break;

						case 8 : $th_class_btm_w1 = " btm_w1 ";
						break;

						case 7 : $th_class_color = $th_class."_color"; $th_class_btm_w1 = " btm_w1 ";
						break;

						case 6 : $thead_tr = " </tr>\n<tr>\n ";
						break;
					}
				?>
					<?=$thead_tr?>
					<th <?=$rowspan?> <?=$colspan?> class="<?=$th_class?> <?=$th_class_color?> <?=$th_class_btm_w1?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 날짜 요일 결제-금액 결제-건수 결제-재구매건[율] */
					$sr_year_month_day = $dvalue_val['sr_year_month']."-".$dvalue_val['sr_day'];
					$sr_week = get_yoil($dvalue_val['sr_week'],1,1); 
					$sumpay = number_format($dvalue_val['sumpay'])." 원";
					$sumcnt = number_format($dvalue_val['sumcnt'])." 건";
					$repay_cnt_val = $dvalue_val['repay_cnt'];
					$sumcnt_val = $dvalue_val['sumcnt'];
					if($dvalue_val['sumcnt'] == 0){ $sumcnt_val = 1; }
					$repct_calc_val = intval(($repay_cnt_val / $sumcnt_val * 1000) / 10);
					$repct = number_format($dvalue_val['repay_cnt'])." 건 [ ".$repct_calc_val."% ]";
					$i = 2; // 배열용 카운트

					/* 핸드폰, 카드, 토스, 가상계좌 */
					foreach($d_sr_way as $d_sr_way_key => $d_sr_way_val) {
						${"dp_".$d_sr_way_key."_pct"} = '';
					} // end foreach

					foreach($d_sr_way as $d_sr_way_key => $d_sr_way_val) {
						${"dp_".$d_sr_way_key."_pct"} = round((intval($dvalue_val[$d_sr_way_key.'_cnt']) / $sumcnt_val * 1000) / 10, 1)."%";
					} // end foreach 
					
					foreach($d_sr_way as $d_sr_way_key => $d_sr_way_val) {
						${$d_sr_way_key."_sum_cnt"} = number_format(intval($dvalue_val[$d_sr_way_key.'_sum']))." 원"."<br>".
							number_format(intval($dvalue_val[$d_sr_way_key.'_cnt']))." 건 [".${"dp_".$d_sr_way_key."_pct"}."]";
					} // end foreach 

					/* 요일별 class */
					$week_class = $week_en[$dvalue_val['sr_week']];
				?>
				<tr class="<?=$week_class;?>">
					<td colspan='2' class="<?=$css_suffix.$fetch_row[0][1]?> btm_not text_center"><strong><?=$sr_year_month_day;?></strong></td>
					<td class="<?=$css_suffix.$fetch_row[1][1]?> btm_not text_center"><strong><?=$sr_week;?></strong></td>
				 
				 <? foreach($d_sr_way as $d_sr_way_key => $d_sr_way_val) { ?>
						<td rowspan='2' class="<?=$css_suffix.$fetch_row[$i][1]?> dp_<?=$d_sr_way_key;?>_color text_center"><?=${$d_sr_way_key."_sum_cnt"};?></td>						
				 <?		$i++;
					} // end foreach ?>
				</tr>
				<tr class="<?=$week_class;?>">

					<td class="<?=$css_suffix.$fetch_row[10][1]?> text_center"><?=$sumpay;?></td>
					<td class="<?=$css_suffix.$fetch_row[11][1]?> text_center"><?=$sumcnt;?></td>
					<td class="<?=$css_suffix.$fetch_row[12][1]?> text_center"><?=$repct;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>