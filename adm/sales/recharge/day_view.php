<?php
 include_once '_common.php'; // 공통
 
 $year_month_day = base_filter($_GET['year_month_day']);
 
 $field_sales_recharge = "sr_hour, SUM(sr_pay) AS sumpay, SUM(sr_re_pay) AS repay, COUNT(sr_pay) AS sumpay_cnt, COUNT(sr_re_pay) AS repay_cnt";

 $where_sales_recharge = "AND sr_year_month = '".substr($year_month_day,0,7)."' ";
 $where_sales_recharge .= "AND sr_day = '".substr($year_month_day,8,2)."' ";

 $group_sales_recharge = "sr_hour";
 
 $sql_sales_recharge = "SELECT $field_sales_recharge FROM sales_recharge WHERE 1 $where_sales_recharge GROUP BY $group_sales_recharge";
 
 $result = sql_query($sql_sales_recharge);
 
 $temp_arr = array();
 while($row = sql_fetch_array($result)){
 	array_push($temp_arr, $row);
 }
 
 $fetch_row = array();
 array_push($fetch_row, array('시간대', 'sr_hour'));
 array_push($fetch_row, array('금액(원)', 'sumpay'));
 array_push($fetch_row, array('건수(건)', 'sumpay_cnt'));
 array_push($fetch_row, array('재구매건/율', 'repay'));
 
 $cms_head_title = "시간대별 결제 통계 ".$year_month_day;
 
 if($_mode == 'excel') {
 	$result = sql_query($sql_sales_recharge);
	include_once $_cms_folder_top_path.'/excel/day_view_excel.php';
 }

 $head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
 include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="sales_result">
	<h1 class="day_view">시간대별 결제 통계 : <?=$year_month_day ?></h1>
	<div class="write">
		<input type="button" class="excel_down" value="엑셀 다운로드"	onclick="location.href='<?=$_cms_self?>?<?=$_nm_paras;?>&mode=excel'">
	</div>	
	<div id="cr_bg">
		<table class="list">
			<thead>
				<tr>
					<th rowspan="2">시간대</th>
					<th colspan="3">결제 금액</th>
				</tr>
				<tr>
					<th>금액(원)</th>
					<th>건수(건)</th>
					<th>재구매건/율</th>
				</tr>
			</thead>
			<tbody>
			<?php	foreach ($temp_arr as $key => $val) {	?>
				<tr>
					<td>
                        <a href="day_hour_view.php?year_month_day=<?=$year_month_day?>&hour=<?=$val['sr_hour']?>" class="<?=$fetch_row[0][1]?>" target="_self"><?=$val['sr_hour'] ?>:00</a>
                    </td>
					<td><?=number_format($val['sumpay']) ?></td>
					<td><?=$val['sumpay_cnt'] ?></td>
					<td><?=$val['repay'] ?> / <?=round($val['repay']/$val['repay_cnt']*100, 1, PHP_ROUND_HALF_DOWN) ?>%</td>
				</tr>
			<? } ?>			
				<tr>
					<td colspan="4" class="btn">
						<input type="reset" value="닫기" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</section>