<?
include_once '_common.php'; // 공통

/* PARAMITER 
maker_name // 제공사이름
*/


/* 데이터 가져오기 */
$where_sales_recharge = ""; /* 충전 */

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }
if($_s_date && $_e_date){
	$where_sales_recharge.= date_year_month($_s_date, $_e_date, 'sr');
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "sr_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 요일 정렬시
if($_order_field == "sr_week"){$_order_field_add = " , sr_date "; $_order_add = "desc ";}
// 날짜 정렬시
if($_order_field == "sr_year_month"){$_order_field_add = " , sr_date "; $_order_add = $_order;}
$order_sales_recharge = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

$sql_sales_recharge = "";
$field_sales_recharge = "   sr_year_month, 
							sr_day, 
							sr_week, 
							sum(sr_pay)as sumpay, 
							count(sr_day)as sumcnt, 
							sum(sr_re_pay)as repay_cnt, 

							count(if(sr_sex='m' and sr_age>0,1,null))as m_cnt, 
							sum(if(sr_sex='m' and sr_age>0,sr_pay,0))as m_sum, 
							sum(if(sr_sex='m' and sr_age>0,sr_re_pay,0))as m_repay_cnt, 

							count(if(sr_sex='m' and sr_age=10 ,1,null))as m10_cnt, 
							sum(if(sr_sex='m' and sr_age=10,sr_pay,0))as m10_sum, 
							sum(if(sr_sex='m' and sr_age=10,sr_re_pay,0))as m10_repay_cnt, 

							count(if(sr_sex='m' and sr_age=20 ,1,null))as m20_cnt, 
							sum(if(sr_sex='m' and sr_age=20,sr_pay,0))as m20_sum, 
							sum(if(sr_sex='m' and sr_age=20,sr_re_pay,0))as m20_repay_cnt, 

							count(if(sr_sex='m' and sr_age=30 ,1,null))as m30_cnt, 
							sum(if(sr_sex='m' and sr_age=30,sr_pay,0))as m30_sum, 
							sum(if(sr_sex='m' and sr_age=30,sr_re_pay,0))as m30_repay_cnt, 

							count(if(sr_sex='m' and sr_age=40 ,1,null))as m40_cnt, 
							sum(if(sr_sex='m' and sr_age=40,sr_pay,0))as m40_sum, 
							sum(if(sr_sex='m' and sr_age=40,sr_re_pay,0))as m40_repay_cnt, 

							count(if(sr_sex='m' and sr_age=50 ,1,null))as m50_cnt, 
							sum(if(sr_sex='m' and sr_age=50,sr_pay,0))as m50_sum, 
							sum(if(sr_sex='m' and sr_age=50,sr_re_pay,0))as m50_repay_cnt, 

							count(if(sr_sex='m' and sr_age>=60 ,1,null))as m60_cnt, 
							sum(if(sr_sex='m' and sr_age>=60,sr_pay,0))as m60_sum, 
							sum(if(sr_sex='m' and sr_age>=60,sr_re_pay,0))as m60_repay_cnt, 

							count(if(sr_sex='w' and sr_age>0,1,null))as y_cnt, 
							sum(if(sr_sex='w' and sr_age>0,sr_pay,0))as y_sum, 
							sum(if(sr_sex='w' and sr_age>0,sr_re_pay,0))as y_repay_cnt, 

							count(if(sr_sex='w' and sr_age=10 ,1,null))as y10_cnt, 
							sum(if(sr_sex='w' and sr_age=10,sr_pay,0))as y10_sum, 
							sum(if(sr_sex='w' and sr_age=10,sr_re_pay,0))as y10_repay_cnt, 

							count(if(sr_sex='w' and sr_age=20 ,1,null))as y20_cnt, 
							sum(if(sr_sex='w' and sr_age=20,sr_pay,0))as y20_sum, 
							sum(if(sr_sex='w' and sr_age=20,sr_re_pay,0))as y20_repay_cnt, 

							count(if(sr_sex='w' and sr_age=30 ,1,null))as y30_cnt, 
							sum(if(sr_sex='w' and sr_age=30,sr_pay,0))as y30_sum, 
							sum(if(sr_sex='w' and sr_age=30,sr_re_pay,0))as y30_repay_cnt, 

							count(if(sr_sex='w' and sr_age=40 ,1,null))as y40_cnt, 
							sum(if(sr_sex='w' and sr_age=40,sr_pay,0))as y40_sum, 
							sum(if(sr_sex='w' and sr_age=40,sr_re_pay,0))as y40_repay_cnt, 

							count(if(sr_sex='w' and sr_age=50 ,1,null))as y50_cnt, 
							sum(if(sr_sex='w' and sr_age=50,sr_pay,0))as y50_sum, 
							sum(if(sr_sex='w' and sr_age=50,sr_re_pay,0))as y50_repay_cnt, 

							count(if(sr_sex='w' and sr_age>=60 ,1,null))as y60_cnt, 
							sum(if(sr_sex='w' and sr_age>=60,sr_pay,0))as y60_sum, 
							sum(if(sr_sex='w' and sr_age>=60,sr_re_pay,0))as y60_repay_cnt, 

							count(if(sr_sex='n' OR sr_age=0,1,null))as n_cnt, 
							sum(if(sr_sex='n' OR sr_age=0,sr_pay,0))as n_sum, 
							sum(if(sr_sex='n' OR sr_age=0,sr_re_pay,0))as n_repay_cnt 
							
							"; // 가져올 필드 정하기
$group_sales_recharge = " group by sr_year_month, sr_day ";
$limit_sales_recharge = "";

$sql_sales_recharge = "select $field_sales_recharge FROM sales_recharge where 1 $where_sales_recharge $group_sales_recharge $order_sales_recharge $limit_sales_recharge";
$result = sql_query($sql_sales_recharge);
$row_size = sql_num_rows($result);

/* 검색 전체 구하기 - group by 빼고 sum, count함수 이용 */
$sql_sales_recharge_s_all = "select $field_sales_recharge FROM sales_recharge where 1 $where_sales_recharge $order_sales_recharge $limit_sales_recharge";
$db_s_all_row = $sql_sales_recharge_s_all_row = sql_fetch($sql_sales_recharge_s_all );
$s_all_sumpay = $sql_sales_recharge_s_all_row['sumpay'];
$s_all_sumcnt = $sql_sales_recharge_s_all_row['sumcnt'];
$s_all_repay_cnt = $sql_sales_recharge_s_all_row['repay_cnt'];

$s_all_sumpay_text = number_format($s_all_sumpay)." 원";
$s_all_sumcnt_text = number_format($s_all_sumcnt)." 건"; 
if($s_all_sumcnt == 0){ $s_all_sumcnt = 1; }

$s_all_repay_cnt_text = number_format($s_all_repay_cnt)." 건 [ ".intval(($s_all_repay_cnt / $s_all_sumcnt * 1000) / 10)."% ]";


// 성별&연령별 재구매
$age_arr = array('', 10,20,30,40,50,60);

$s_all_m_repay_cnt = $sql_sales_recharge_s_all_row['m_repay_cnt'];
if($s_all_m_repay_cnt == 0){ $s_all_m_repay_cnt = 1; } // 성별&연령별 재구매
foreach($age_arr as $age_key => $age_val){
	/* 남자 */
	${'m'.$age_val.'_cnt'} = $sql_sales_recharge_s_all_row['m'.$age_val.'_cnt'];
	if(${'m'.$age_val.'_cnt'} == 0){ ${'m'.$age_val.'_cnt'} = 1; }


	${'s_m'.$age_val.'_cnt_repay_pct'} = round((intval($sql_sales_recharge_s_all_row['m'.$age_val.'_repay_cnt']) / ${'m'.$age_val.'_cnt'} * 1000) / 10, 1)."%";

	/* 여자 */
	${'y'.$age_val.'_cnt'} = $sql_sales_recharge_s_all_row['y'.$age_val.'_cnt'];
	if(${'y'.$age_val.'_cnt'} == 0){ ${'y'.$age_val.'_cnt'} = 1; }

	${'s_y'.$age_val.'_cnt_repay_pct'} = round((intval($sql_sales_recharge_s_all_row['y'.$age_val.'_repay_cnt']) / ${'y'.$age_val.'_cnt'} * 1000) / 10, 1)."%";

}

// 미인증
$s_all_n_cnt = $sql_sales_recharge_s_all_row['n_cnt'];
if($s_all_n_cnt == 0){ $s_all_n_cnt = 1; } // 성별&연령별 재구매
$s_n_cnt_repay_pct = round((intval($sql_sales_recharge_s_all_row['n_repay_cnt']) / $s_all_n_cnt * 1000) / 10, 1)."%";

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sr_year_month',9));
array_push($fetch_row, array('요일','sr_week',2));

foreach($age_arr as $age_key => $age_val){
	$age_field = "".$age_val;
	$age_text = $age_val."대";
	if($age_val == ''){
		$age_text = "연령전체";
		$age_field = "all";
	}
	if($age_val == 60){
		$age_text = $age_val."대이상";
	}

	array_push($fetch_row, array($age_text,$age_field,7));
}
array_push($fetch_row, array('결제-금액','sumpay',6));
array_push($fetch_row, array('결제-건수','sumcnt',5));
array_push($fetch_row, array('결제-재구매건[율]','repay_pct',5));

foreach($age_arr as $age_key => $age_val){
	array_push($fetch_row, array('금액',''.$age_val.'_sum',5));
	array_push($fetch_row, array('건수',''.$age_val.'_cnt',5));
	array_push($fetch_row, array('재구매건[율]',''.$age_val.'_repay_cnt',5));
}

// css 클래스 접두사
$css_suffix = "dsa_";

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$week_text = array('일','월','화','수','목','금','토');

$sex_arr = array(array('m','남자','man'),array('y','여자','woman'));

// 10줄부터
if($_s_limit == ''){$_s_limit = 10;}

$page_title = "일별 결제 성별&연령별";
$cms_head_title = $page_title;

if($_mode != '') {
	switch($_mode) {
		case 'man': 
			include_once $_cms_folder_top_path.'/excel/day_sex_age_man_excel.php';
			break;
		case 'woman':
			include_once $_cms_folder_top_path.'/excel/day_sex_age_woman_excel.php';
			break;
		case 'none' :
			include_once $_cms_folder_top_path.'/excel/day_sex_age_none_excel.php';
			break;
	} // end switch
} // end if

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?>"/>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
	<div class="write">
		<input type="button" class="excel_down" value="남자 부분 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?s_date=<?=$_s_date;?>&e_date=<?=$_e_date;?>&order_field=<?=$_order_field;?>&order=<?=$_order;?>&mode=man'"/>
		<input type="button" class="excel_down" value="여자 부분 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?s_date=<?=$_s_date;?>&e_date=<?=$_e_date;?>&order_field=<?=$_order_field;?>&order=<?=$_order;?>&mode=woman'"/>
		<input type="button" class="excel_down" value="미인증 부분 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?s_date=<?=$_s_date;?>&e_date=<?=$_e_date;?>&order_field=<?=$_order_field;?>&order=<?=$_order;?>&mode=none'"/>
	</div>
</section><!-- recharge_head -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_sex_age_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result" class="day_sex_age_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 - 총금액: <?=$s_all_sumpay_text;?> / 총건수: <?=$s_all_sumcnt_text;?> / 총 재구매건[율]: <?=$s_all_repay_cnt_text;?></strong>
		<?if($s_all_sumpay!= ''){?>
			<table>
				<tr>
					<th class="topleft">성별</th>
					<th>구분</th>
					<? foreach($age_arr as $age_key => $age_val){ 
						$age_text = $age_val."대";
						if($age_val == ''){
							$age_text = "연령전체";
						}
						if($age_val == 60){
							$age_text = $age_val."대이상";
						}
					?>
						<th><?=$age_text;?></th>
					<?}?>
						<th class="topright">미인증</th>
				</tr>
				<tr>
					<th class="man" rowspan="3">남자</th>
					<th class="man">금액</th>
					<? foreach($age_arr as $age_key => $age_val){ ?>
						<td class="man"><?=number_format($db_s_all_row['m'.$age_val.'_sum']);?> 원</td>
					<?}?>
					<? /* 미인증 */ ?>
					<td class="no_certify"><?=number_format($db_s_all_row['n_sum']);?> 원</td>
				</tr>
				<tr>
					<th class="man">건수</th>
					<? foreach($age_arr as $age_key => $age_val){ ?>
						<td class="man"><?=number_format($db_s_all_row['m'.$age_val.'_cnt']);?> 건</td>
					<?}?>
					<? /* 미인증 */ ?>
					<td class="no_certify"><?=number_format($db_s_all_row['n_cnt']);?> 건</td>
				</tr>
				<tr>
					<th class="man">재구매건[율]</th>
					<? foreach($age_arr as $age_key => $age_val){ ?>
						<td class="man"><?=number_format($db_s_all_row['m'.$age_val.'_repay_cnt']);?> 건 [<?=${'s_m'.$age_val.'_cnt_repay_pct'};?>]</td>
					<?}?>
					<? /* 미인증 */ ?>
					<td class="no_certify"><?=number_format($db_s_all_row['n_repay_cnt']);?> 건 [<?=$s_n_cnt_repay_pct;?>]</td>
				</tr>
				<tr>
					<th class="woman" rowspan="3">여자</th>
					<th class="woman">금액</th>
					<? foreach($age_arr as $age_key => $age_val){ ?>
						<td class="woman"><?=number_format($db_s_all_row['y'.$age_val.'_sum']);?> 원</td>
					<?}?>
					<? /* 미인증 */ ?>
					<td class="no_certify" rowspan='3'>&nbsp;</td>
				</tr>
				<tr>
					<th class="woman">건수</th>
					<? foreach($age_arr as $age_key => $age_val){ ?>
						<td class="woman"><?=number_format($db_s_all_row['y'.$age_val.'_cnt']);?> 건</td>
					<?}?>
				</tr>
				<tr>
					<th class="woman">재구매건[율]</th>
					<? foreach($age_arr as $age_key => $age_val){ ?>
						<td class="woman"><?=number_format($db_s_all_row['y'.$age_val.'_repay_cnt']);?> 건 [<?=${'s_y'.$age_val.'_cnt_repay_pct'};?>]</td>
					<?}?>
				</tr>
			</table>
		<?}?>
	</h3>
	<div id="cr_bg">
	<?foreach($sex_arr as $sex_key => $sex_val){ ?>
		<h4 class="<?=$sex_val[2];?>"><?=$sex_val[1];?></h4>
		<table>
			<thead>
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1 || $fetch_val[2] == 9 || $fetch_val[2] == 2){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $css_suffix.$fetch_val[1];

					/* 번호에 의한 rowspan, colspan 설정 */
					$rowspan = $colspan = $th_class_btm_w1 = $th_class_color = $thead_tr = "";
					switch($fetch_val[2]){
						case 9 : $colspan = " colspan='2' "; $th_class_btm_w1 = " btm_w1 ";
						break;

						case 8 : $colspan = " colspan='3' "; $th_class_btm_w1 = " btm_w1 ";
						break;

						case 7 : $colspan = " colspan='3' "; $th_class_color = $th_class."_color"; $th_class_btm_w1 = " btm_w1 ";
						break;

						case 6 : $rowspan = " rowspan='2' "; $thead_tr = " </tr>\n<tr>\n ";
						break;

						case 5 : $rowspan = " rowspan='2' ";
						break;

						case 4 : $thead_tr = " </tr>\n<tr>\n ";
						break;

						case 2 : $th_class_btm_w1 = " btm_w1 ";
						break;

					}
				?>
					<?=$thead_tr?>
					<th <?=$rowspan?> <?=$colspan?> class="<?=$th_class?> <?=$th_class_color?> <?=$th_class_btm_w1?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 날짜 요일 결제-금액 결제-건수 결제-재구매건[율] */
					$sr_year_month_day = $dvalue_val['sr_year_month']."-".$dvalue_val['sr_day'];
					$sr_week = get_yoil($dvalue_val['sr_week'],1,1); 
					$sumpay = number_format($dvalue_val['sumpay'])." 원";
					$sumcnt = number_format($dvalue_val['sumcnt'])." 건";
					$repay_cnt_val = $dvalue_val['repay_cnt'];
					$sumcnt_val = $dvalue_val['sumcnt'];
					if($dvalue_val['sumcnt'] == 0){ $sumcnt_val = 1; }
					$repct_calc_val = intval(($repay_cnt_val / $sumcnt_val * 1000) / 10);
					$repct = number_format($dvalue_val['repay_cnt'])." 건[".$repct_calc_val."%]";

					/* 요일별 class */
					$week_class = $week_en[$dvalue_val['sr_week']];

					/*연령전체 연령별*/

				?>
				<tr class="<?=$week_class?>">
					<td colspan='2' class="<?=$css_suffix?>sr_year_month btm_not text_center"><strong><?=$sr_year_month_day;?></strong></td>
					<td class="<?=$css_suffix?>sr_week btm_not text_center"><strong><?=$sr_week;?></strong></td>
					<? foreach($age_arr as $age_key => $age_val){ 
					
						${$sex_val[0].$age_val.'_cnt'} = $dvalue_val[$sex_val[0].$age_val.'_cnt'];
						if(${$sex_val[0].$age_val.'_cnt'} == 0){ ${$sex_val[0].$age_val.'_cnt'} = 1; }


						${'db_'.$sex_val[0].$age_val.'_cnt_repay_pct'} = round((intval($dvalue_val[$sex_val[0].$age_val.'_repay_cnt']) / ${$sex_val[0].$age_val.'_cnt'} * 1000) / 10, 1)."%";
					?>
						<td rowspan='2' class="<?=$css_suffix?>fl<?=$age_val?> <?=$css_suffix?><?=$sex_val[0]?><?=$age_val?>_sum text_center"><?=number_format($dvalue_val[$sex_val[0].$age_val.'_sum']);?>원</td>
						<td rowspan='2' class="<?=$css_suffix?>fl<?=$age_val?> <?=$css_suffix?><?=$sex_val[0]?><?=$age_val?>_cnt text_center"><?=number_format($dvalue_val[$sex_val[0].$age_val.'_cnt']);?>건</td>
						<td rowspan='2' class="<?=$css_suffix?>fl<?=$age_val?> <?=$css_suffix?><?=$sex_val[0]?><?=$age_val?>_repay_cnt text_center"><?=number_format($dvalue_val[$sex_val[0].$age_val.'_repay_cnt']);?>건[<?=${'db_'.$sex_val[0].$age_val.'_cnt_repay_pct'};?>]</td>
					<?}?>
				</tr>
				<tr class="<?=$week_class?>">
					<td class="<?=$css_suffix?>sumpay text_center"><?=$sumpay;?></td>
					<td class="<?=$css_suffix?>sumcnt text_center"><?=$sumcnt;?></td>
					<td class="<?=$css_suffix?>pct text_center"><?=$repct;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	<?}?>
		<?/* 미인증 */?>
		<h4 class="no_certify">미인증</h4>
		<table>
			<thead>
				<tr>
					<th>날짜</th>
					<th>요일</th>
					<th>결제-금액</th>
					<th>결제-건수</th>
					<th>결제-재구매건[율]</th>
					<th class="no_certify">미인증-금액</th>
					<th class="no_certify">미인증-건수</th>
					<th class="no_certify">미인증-재구매건[율]</th>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 날짜 요일 결제-금액 결제-건수 결제-재구매건[율] */
					$sr_year_month_day = $dvalue_val['sr_year_month']."-".$dvalue_val['sr_day'];
					$sr_week = get_yoil($dvalue_val['sr_week'],1,1); 
					$sumpay = number_format($dvalue_val['sumpay'])." 원";
					$sumcnt = number_format($dvalue_val['sumcnt'])." 건";
					$repay_cnt_val = $dvalue_val['repay_cnt'];
					$sumcnt_val = $dvalue_val['sumcnt'];
					if($dvalue_val['sumcnt'] == 0){ $sumcnt_val = 1; }
					$repct_calc_val = intval(($repay_cnt_val / $sumcnt_val * 1000) / 10);
					$repct = number_format($dvalue_val['repay_cnt'])." 건 [".$repct_calc_val."%]";

					
					/* 요일별 class */
					$week_class = $week_en[$dvalue_val['sr_week']];

					/* 미인증 */
					$no_cnt = $dvalue_val['n_cnt'];
					if($dvalue_val['n_cnt'] == 0){ $no_cnt = 1; }
					$no_sum = number_format($dvalue_val['n_sum']);
					$no_repay_cnt = number_format($dvalue_val['n_repay_cnt']);
					
					$no_repct_calc_val = intval(($no_repay_cnt / $no_cnt * 1000) / 10);
					$no_repct = number_format($dvalue_val['n_repay_cnt'])." 건[".$no_repct_calc_val."%]";
				?>
				<tr class="<?=$week_class;?>">
					<td class="<?=$css_suffix?>sr_year_month text_center"><strong><?=$sr_year_month_day;?></strong></td>
					<td class="<?=$css_suffix?>sr_week text_center"><strong><?=$sr_week;?></strong></td>
					<td class="<?=$css_suffix?>sumpay text_center"><?=$no_sum;?></td>
					<td class="<?=$css_suffix?>sumcnt text_center"><?=$no_repay_cnt;?></td>
					<td class="<?=$css_suffix?>pct text_center"><?=$no_repct;?></td>

					<td class="<?=$css_suffix?>n_sum no_certify text_center"><?=number_format($dvalue_val['n_sum']);?>원</td>
					<td class="<?=$css_suffix?>n_cnt no_certify text_center"><?=number_format($dvalue_val['n_cnt'])?>건</td>
					<td class="<?=$css_suffix?>repct no_certify text_center"><?=$no_repct;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>