<?
include_once '_common.php'; // 공통

/* PARAMITER 
maker_name // 제공사이름
*/


/* 데이터 가져오기 */
$where_sales_recharge = ""; /* 충전 */

if($_s_date == ''){ $_s_date = date('Y-m-01', NM_SERVER_TIME); }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }


if($_s_date && $_e_date){
	$_s_date_y_m = substr($_s_date,0,7);
	$_e_date_y_m = substr($_e_date,0,7);
	$_s_date_d = substr($_s_date,8,2);
	$_e_date_d = substr($_e_date,8,2);
	
	/*
	$where_sales_recharge.= "and sr_year_month >= '$_s_date_y_m' and sr_year_month <= '$_e_date_y_m' ";
	$where_sales_recharge.= "and sr_day >= '$_s_date_d' and sr_day <= '$_e_date_d' ";
	*/
	
	$where_sales_recharge.= "AND (sr_year_month >  '$_s_date_y_m' AND sr_year_month <  '$_e_date_y_m' ) ";
	$where_sales_recharge.= "OR( (sr_year_month =  '$_s_date_y_m' AND sr_day >=  '$_s_date_d') ";
	$where_sales_recharge.= "AND(sr_year_month =  '$_e_date_y_m' AND sr_day <=  '$_e_date_d') ) ";
}
// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "sr_date"; }
if($_order == null || $_order == ""){ $_order = "asc"; }
$order_sales_recharge = "order by ".$_order_field." ".$_order;

$sql_sales_recharge = "";
$field_sales_recharge = " * "; // 가져올 필드 정하기
$limit_sales_recharge = "";

$sql_sales_recharge = "select $field_sales_recharge FROM sales_recharge where 1 $where_sales_recharge $order_sales_recharge $limit_sales_recharge";
$result = sql_query($sql_sales_recharge);
$row_size = sql_num_rows($result);

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "전체 매출";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<? /*<script type="text/javascript" src="<?=$_cms_js;?>"></script> 여기에서는 사용하지 않습니다.(에러발생) */ ?>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_calendar <?=$cs_calendar_on;?>">
					<label for="s_date">시작일</label>
					<input type="text" name="s_date" value="<?=$_s_date;?>" id="s_date" class="d_date readonly" readonly /> 
					<label for="s_date_view">일</label>
					<label for="date_"> / </label>
					<label for="e_date">종료일</label>
					<input type="text" name="e_date" value="<?=$_e_date;?>" id="e_date" class="d_date readonly" readonly />
					<label for="e_date_view">일</label>
					<div class="cs_calendar_btn">
						<a href="#이주전" onclick="a_click_false();" data-fr_date="<?=$week2_term?>" data-to_date="<?=NM_TIME_YMD;?>">이주전</a>
						<a href="#이번달" onclick="a_click_false();" data-fr_date="<?=$month_term?>" data-to_date="<?=NM_TIME_YMD;?>">이번달</a>
						<a href="#최근2개월" onclick="a_click_false();" data-fr_date="<?=$month2_term?>" data-to_date="<?=NM_TIME_YMD;?>">최근2개월</a>
						<a href="#최근3개월" onclick="a_click_false();" data-fr_date="<?=$month3_term?>" data-to_date="<?=NM_TIME_YMD;?>">최근3개월</a>
					</div>
				</div>
			</div>
			<div class="cs_submit all_buy">
				<input type="submit" class="<?=$cs_submit_h;?> all_buy" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<h3>검색 결과</h3>
	<div id="cr_bg">
		<table>
			<thead>
				<tr>
					<th>매출 종류</th>
					<th>3개월 평균</th>
					<th>지난달</th>
					<th>조회 기간</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>충전금액</td>
					<td>개발중</td>
					<td>개발중</td>
					<td>개발중</td>
				</tr>
				<tr>
					<td class="buy_info" colspan="4">매출정보</td>
				</tr>
				<tr>
					<td>매출화수</td>
					<td>개발중</td>
					<td>개발중</td>
					<td>개발중</td>
				</tr>
				<tr>
					<td>매출액(코인)</td>
					<td>개발중</td>
					<td>개발중</td>
					<td>개발중</td>
				</tr>
				<tr>
					<td>매출액(미니코인)</td>
					<td>개발중</td>
					<td>개발중</td>
					<td>개발중</td>
				</tr>
				<tr>
					<td>정산액</td>
					<td>개발중</td>
					<td>개발중</td>
					<td>개발중</td>
				</tr>
				<tr>
					<td class="buy_info" colspan="4">그래프준비 - 위 리스트와 남녀미인증, 연령대, 기타, 판매화수</td>
				</tr>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>