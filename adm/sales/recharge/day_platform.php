<?
include_once '_common.php'; // 공통

/* PARAMITER 
maker_name // 제공사이름
*/


/* 데이터 가져오기 */
$where_sales_recharge = ""; /* 충전 */

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }
if($_s_date && $_e_date){
	$where_sales_recharge.= date_year_month($_s_date, $_e_date, 'sr');
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "sr_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 요일 정렬시
if($_order_field == "sr_week"){$_order_field_add = " , sr_date "; $_order_add = "desc ";}
// 날짜 정렬시
if($_order_field == "sr_year_month"){$_order_field_add = " , sr_date "; $_order_add = $_order;}
$order_sales_recharge = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

// 이용환경(1:웹, 2:Android, 4:iOS, 8:AppMarket, 16:AppSetApk)

$sql_sales_recharge = "";
$field_sales_recharge = "   sr_year_month, 
							sr_day, 
							sr_week, 
							sum(sr_pay)as sumpay, 
							count(sr_day)as sumcnt, 
							sum(sr_re_pay)as repay_cnt, 

							count(if(sr_platform='1',1,null))as web_cnt, 
							sum(if(sr_platform='1',sr_pay,0))as web_sum, 

							count(if(sr_platform='2',1,null))as android_cnt, 
							sum(if(sr_platform='2',sr_pay,0))as android_sum, 

							count(if(sr_platform='4',1,null))as ios_cnt, 
							sum(if(sr_platform='4',sr_pay,0))as ios_sum, 

							count(if(sr_platform='8',1,null))as appmarket_cnt, 
							sum(if(sr_platform='8',sr_pay,0))as appmarket_sum, 

							count(if(sr_platform='16',1,null))as appsetapk_cnt, 
							sum(if(sr_platform='16',sr_pay,0))as appsetapk_sum 							
							"; // 가져올 필드 정하기
$group_sales_recharge = " group by sr_year_month, sr_day ";
$limit_sales_recharge = "";

$sql_sales_recharge = "select $field_sales_recharge FROM sales_recharge where 1 $where_sales_recharge $group_sales_recharge $order_sales_recharge $limit_sales_recharge";
$result = sql_query($sql_sales_recharge);
$row_size = sql_num_rows($result);

/* 검색 전체 구하기 - group by 빼고 sum, count함수 이용 */
$sql_sales_recharge_s_all = "select $field_sales_recharge FROM sales_recharge where 1 $where_sales_recharge $order_sales_recharge $limit_sales_recharge";
$sql_sales_recharge_s_all_row = sql_fetch($sql_sales_recharge_s_all);
$s_all_sumpay = $sql_sales_recharge_s_all_row['sumpay'];
$s_all_sumcnt = $sql_sales_recharge_s_all_row['sumcnt'];
$s_all_repay_cnt = $sql_sales_recharge_s_all_row['repay_cnt'];

$s_all_sumpay_text = number_format($s_all_sumpay)." 원";
$s_all_sumcnt_text = number_format($s_all_sumcnt)." 건";
if($s_all_sumcnt == 0){ $s_all_sumcnt = 1; }

$s_web_pct = round((intval($sql_sales_recharge_s_all_row['web_cnt']) / $s_all_sumcnt * 1000) / 10, 1)."%";
$s_android_pct = round((intval($sql_sales_recharge_s_all_row['android_cnt']) / $s_all_sumcnt * 1000) / 10, 1)."%";
$s_ios_pct = round((intval($sql_sales_recharge_s_all_row['ios_cnt']) / $s_all_sumcnt * 1000) / 10, 1)."%";
$s_appmarket_pct = round((intval($sql_sales_recharge_s_all_row['appmarket_cnt']) / $s_all_sumcnt * 1000) / 10, 1)."%";
$s_appsetapk_pct = round((intval($sql_sales_recharge_s_all_row['appsetapk_cnt']) / $s_all_sumcnt * 1000) / 10, 1)."%";

$s_all_repay_cnt_text = number_format($s_all_repay_cnt)." 건 [ ".intval(($s_all_repay_cnt / $s_all_sumcnt * 1000) / 10)."% ]";

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sr_year_month',9));
array_push($fetch_row, array('요일','sr_week',8));

array_push($fetch_row, array('PC','web',7));
array_push($fetch_row, array('MobileWeb-Android','android',7));
array_push($fetch_row, array('MobileWeb-iOS','ios',7));
array_push($fetch_row, array('APP-Market','appmarket',7));
array_push($fetch_row, array('APP-Setapk','appsetapk',7));

array_push($fetch_row, array('결제-금액','sumpay',6));
array_push($fetch_row, array('결제-건수','sumcnt',5));
array_push($fetch_row, array('결제-재구매건[율]','pct',5));

array_push($fetch_row, array('금액','web_sum',0));
array_push($fetch_row, array('건수[이용율]','web_cnt',0));
array_push($fetch_row, array('금액','android_sum',0));
array_push($fetch_row, array('건수[이용율]','android_cnt',0));
array_push($fetch_row, array('금액','ios_sum',0));
array_push($fetch_row, array('건수[이용율]','ios_cnt',0));
array_push($fetch_row, array('금액','appmarket_sum',0));
array_push($fetch_row, array('건수[이용율]','appmarket_cnt',0));
array_push($fetch_row, array('금액','appsetapk_sum',0));
array_push($fetch_row, array('건수[이용율]','appsetapk_cnt',0));

// css 클래스 접두사
$css_suffix = "dpf_";

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "일별 결제 플랫폼별";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
</section><!-- recharge_head -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_platform_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="sales_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 - 총금액: <?=$s_all_sumpay_text;?> / 총건수: <?=$s_all_sumcnt_text;?> / 총 재구매건[율]: <?=$s_all_repay_cnt_text;?></strong>
		<?if($s_all_sumpay!= ''){?>
			<table>
				<tr>
					<th class="topleft">종류별</th>
					<th>PC</th>
					<th>MobileWeb-Android</th>
					<th>MobileWeb-iOS</th>
					<th>APP-Market</th>
					<th class="topright">APP-Setapk</th>
				</tr>
				<tr>
					<th>금액</th>
					<td><?=number_format($sql_sales_recharge_s_all_row['web_sum']);?>원</td>
					<td><?=number_format($sql_sales_recharge_s_all_row['android_sum']);?>원</td>
					<td><?=number_format($sql_sales_recharge_s_all_row['ios_sum']);?>원</td>
					<td><?=number_format($sql_sales_recharge_s_all_row['appmarket_sum']);?>원</td>
					<td><?=number_format($sql_sales_recharge_s_all_row['appsetapk_sum']);?>원</td>
				</tr>
				<tr>
					<th>건수</th>
					<td><?=number_format($sql_sales_recharge_s_all_row['web_cnt']);?>건</td>
					<td><?=number_format($sql_sales_recharge_s_all_row['android_cnt']);?>건</td>
					<td><?=number_format($sql_sales_recharge_s_all_row['ios_cnt']);?>건</td>
					<td><?=number_format($sql_sales_recharge_s_all_row['appmarket_cnt']);?>건</td>
					<td><?=number_format($sql_sales_recharge_s_all_row['appsetapk_cnt']);?>건</td>
				</tr>
				<tr>
					<th>이용율</th>
					<td><?=$s_web_pct;?></td>
					<td><?=$s_android_pct;?></td>
					<td><?=$s_ios_pct;?></td>
					<td><?=$s_appmarket_pct;?></td>
					<td><?=$s_appsetapk_pct;?></td>
				</tr>
			</table>
		<?}?>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1 || $fetch_val[2] == 9 || $fetch_val[2] == 8){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $css_suffix.$fetch_val[1];

					/* 번호에 의한 rowspan, colspan 설정 */
					$rowspan = $colspan = $th_class_btm_w1 = $th_class_color = $thead_tr = "";
					switch($fetch_val[2]){
						case 9 : $colspan = " colspan='2' "; $th_class_btm_w1 = " btm_w1 ";
						break;

						case 8 : $th_class_btm_w1 = " btm_w1 ";
						break;

						case 7 : $colspan = " colspan='2' "; $th_class_color = $th_class."_color"; $th_class_btm_w1 = " btm_w1 ";
						break;

						case 6 : $thead_tr = " </tr>\n<tr>\n ";
						break;
					}
				?>
					<?=$thead_tr?>
					<th <?=$rowspan?> <?=$colspan?> class="<?=$th_class?> <?=$th_class_color?> <?=$th_class_btm_w1?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 날짜 요일 결제-금액 결제-건수 결제-재구매건[율] */
					$sr_year_month_day = $dvalue_val['sr_year_month']."-".$dvalue_val['sr_day'];
					$sr_week = get_yoil($dvalue_val['sr_week'],1,1); 
					$sumpay = number_format($dvalue_val['sumpay'])." 원";
					$sumcnt = number_format($dvalue_val['sumcnt'])." 건";
					$repay_cnt_val = $dvalue_val['repay_cnt'];
					$sumcnt_val = $dvalue_val['sumcnt'];
					if($dvalue_val['sumcnt'] == 0){ $sumcnt_val = 1; }
					$repct_calc_val = intval(($repay_cnt_val / $sumcnt_val * 1000) / 10);
					$repct = number_format($dvalue_val['repay_cnt'])." 건 [ ".$repct_calc_val."% ]";

					// 이용환경(1:웹, 2:Android, 4:iOS, 8:AppMarket, 16:AppSetApk)
					$dpf_web_pct = round((intval($dvalue_val['web_cnt']) / $sumcnt_val * 1000) / 10, 1)."%";					
					$web_sum = number_format(intval($dvalue_val['web_sum']))." 원";
					$web_cnt = number_format(intval($dvalue_val['web_cnt']))." 건 [".$dpf_web_pct."]";
					
					$dpf_android_pct = round((intval($dvalue_val['android_cnt']) / $sumcnt_val * 1000) / 10, 1)."%";
					$android_sum = number_format(intval($dvalue_val['android_sum']))." 원";
					$android_cnt = number_format(intval($dvalue_val['android_cnt']))." 건 [".$dpf_android_pct."]";
					
					$dpf_ios_pct = round((intval($dvalue_val['ios_cnt']) / $sumcnt_val * 1000) / 10, 1)."%";
					$ios_sum = number_format(intval($dvalue_val['ios_sum']))." 원";
					$ios_cnt = number_format(intval($dvalue_val['ios_cnt']))." 건 [".$dpf_ios_pct."]";
					
					$dpf_appmarket_pct = round((intval($dvalue_val['appmarket_cnt']) / $sumcnt_val * 1000) / 10, 1)."%";
					$appmarket_sum = number_format(intval($dvalue_val['appmarket_sum']))." 원";
					$appmarket_cnt = number_format(intval($dvalue_val['appmarket_cnt']))." 건 [".$dpf_appmarket_pct."]";
					
					$dpf_appsetapk_pct = round((intval($dvalue_val['appsetapk_cnt']) / $sumcnt_val * 1000) / 10, 1)."%";
					$appsetapk_sum = number_format(intval($dvalue_val['appsetapk_sum']))." 원";
					$appsetapk_cnt = number_format(intval($dvalue_val['appsetapk_cnt']))." 건 [".$dpf_appsetapk_pct."]";

					/* 요일별 class */
					$week_class = $week_en[$dvalue_val['sr_week']];
				?>
				<tr class="<?=$week_class;?>">
					<td colspan='2' class="<?=$css_suffix?>sr_year_month btm_not text_center"><strong><?=$sr_year_month_day;?></strong></td>
					<td class="<?=$css_suffix?>sr_week btm_not text_center"><strong><?=$sr_week;?></strong></td>
					<td rowspan='2' class="<?=$css_suffix?>web_sum <?=$css_suffix?>web_color text_center"><?=$web_sum;?></td>
					<td rowspan='2' class="<?=$css_suffix?>web_cnt <?=$css_suffix?>web_color text_center"><?=$web_cnt;?></td>

					<td rowspan='2' class="<?=$css_suffix?>android_sum <?=$css_suffix?>android_color text_center"><?=$android_sum;?></td>
					<td rowspan='2' class="<?=$css_suffix?>android_cnt <?=$css_suffix?>android_color text_center"><?=$android_cnt;?></td>

					<td rowspan='2' class="<?=$css_suffix?>ios_sum <?=$css_suffix?>ios_color text_center"><?=$ios_sum;?></td>
					<td rowspan='2' class="<?=$css_suffix?>ios_cnt <?=$css_suffix?>ios_color text_center"><?=$ios_cnt;?></td>

					<td rowspan='2' class="<?=$css_suffix?>appmarket_sum <?=$css_suffix?>appmarket_color text_center"><?=$appmarket_sum;?></td>
					<td rowspan='2' class="<?=$css_suffix?>appmarket_cnt <?=$css_suffix?>appmarket_color text_center"><?=$appmarket_cnt;?></td>

					<td rowspan='2' class="<?=$css_suffix?>appsetapk_sum <?=$css_suffix?>appsetapk_color text_center"><?=$appsetapk_sum;?></td>
					<td rowspan='2' class="<?=$css_suffix?>appsetapk_cnt <?=$css_suffix?>appsetapk_color text_center"><?=$appsetapk_cnt;?></td>
				</tr>
				<tr class="<?=$week_class;?>">
					<td class="<?=$css_suffix?>sumpay text_center"><?=$sumpay;?></td>
					<td class="<?=$css_suffix?>sumcnt text_center"><?=$sumcnt;?></td>
					<td class="<?=$css_suffix?>pct text_center"><?=$repct;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>