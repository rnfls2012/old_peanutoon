<?
include_once '_common.php'; // 공통

/* PARAMITER
maker_name // 제공사이름
*/


/* 데이터 가져오기 */
$where_sales_recharge = ""; /* 충전 */

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

// 파트너일 경우
$_e_date		= mb_partner_e_date($nm_member, $_e_date);	// 종료일 현재 -1일 적용

if($_s_date && $_e_date){
    $where_sales_recharge.= date_year_month($_s_date, $_e_date, 'srp');
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "srp_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 요일 정렬시
if($_order_field == "srp_week"){$_order_field_add = " , srp_date "; $_order_add = "desc ";}
// 날짜 정렬시
if($_order_field == "srp_year_month"){$_order_field_add = " , srp_date "; $_order_add = $_order;}
$order_sales_recharge = $_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

$sql_sales_recharge = "";
$field_sales_recharge = "srp_year_month, srp_day, srp_week, SUM(srp_pay)as sumpay, COUNT(srp_day)as sumcnt, SUM(srp_re_pay)as repay_cnt "; // 가져올 필드 정하기
$group_sales_recharge = "srp_year_month, srp_day";
$limit_sales_recharge = "";

$sql_sales_recharge = "
SELECT $field_sales_recharge FROM sales_recharge_pointpark 
WHERE 1 $where_sales_recharge 
GROUP BY $group_sales_recharge 
ORDER BY $order_sales_recharge
$limit_sales_recharge
";

$result = sql_query($sql_sales_recharge);
$row_size = sql_num_rows($result);

/* 검색 전체 구하기 - group by 빼고 sum, count함수 이용 */
$sql_sales_recharge_s_all = "
SELECT $field_sales_recharge FROM sales_recharge_pointpark 
WHERE 1 $where_sales_recharge 
ORDER BY $order_sales_recharge 
$limit_sales_recharge
";

$sql_sales_recharge_s_all_row = sql_fetch($sql_sales_recharge_s_all );
$s_all_sumpay = $sql_sales_recharge_s_all_row['sumpay'];
$s_all_sumcnt = $sql_sales_recharge_s_all_row['sumcnt'];
$s_all_repay_cnt = $sql_sales_recharge_s_all_row['repay_cnt'];

$s_all_sumpay_text = number_format($s_all_sumpay)." point";
$s_all_sumcnt_text = number_format($s_all_sumcnt)." 건";
if($s_all_sumcnt == 0){ $s_all_sumcnt = 1; }
$s_all_repay_cnt_text  = number_format($s_all_repay_cnt)." 건 [ ".intval(($s_all_repay_cnt / $s_all_sumcnt * 1000) / 10)."% ]";


/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','srp_year_month',1));
array_push($fetch_row, array('요일','srp_week',1));
array_push($fetch_row, array('금액','sumpay',1));
array_push($fetch_row, array('건수','sumcnt',1));
//array_push($fetch_row, array('재구매건[율]','repay_cnt',1));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "일별 결제";
$cms_head_title = $page_title;

if($_mode == 'excel') {
    $result = sql_query($sql_sales_recharge);
    include_once $_cms_folder_top_path.'/excel/day_buy_porintpark_excel.php';
} // end if

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()

?>
    <link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
    <script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

    <section id="cms_title">
        <h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
        <div>&nbsp;</div>
        <div class="write">
           <!-- <input type="button" class="excel_down" value="검색 결과 엑셀 다운로드"
                   onclick="location.href='<?=$_cms_self?>?s_date=<?=$_s_date;?>&e_date=<?=$_e_date;?>&order_field=<?=$_order_field;?>&order=<?=$_order;?>&mode=excel'"/> -->
        </div>
    </section><!-- cms_title -->

    <section id="cms_search">
        <h2 class="hidden"><?=$cms_head_title?> 검색</h2>
        <form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
            <div class="cs_bg">
                <div class="cs_form">
                    <div class="cs_search_btn">
                        <?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
                        <!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
                    </div>
                    <? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
                </div>
                <div class="cs_submit day_buy_submit">
                    <input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
                </div>
            </div><!-- cs_bg -->
        </form>
    </section><!-- recharge_search -->

    <section id="sales_result">
        <h3>검색 리스트
            <strong>검색 결과 - 총금액: <?=$s_all_sumpay_text;?> / 총건수: <?=$s_all_sumcnt_text;?> <!-- / 총 재구매건[율]: <?=$s_all_repay_cnt_text;?> --> </strong>
        </h3>

        <div id="cr_bg">
            <table>
                <thead id="cr_thead">
                <tr>
                    <?
                    //정렬표기
                    $order_giho = "▼";
                    $th_order = "desc";
                    if($_order == 'desc'){
                        $order_giho = "▲";
                        $th_order = "asc";
                    }
                    foreach($fetch_row as $fetch_key => $fetch_val){

                        $th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
                        if($fetch_val[2] == 1){
                            $th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
                            $th_ahref_e = '</a>';
                        }
                        if($fetch_val[1] == $_order_field){
                            $th_title_giho = $order_giho;
                        }
                        $th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
                        $th_class = $fetch_val[1];
                        ?>
                        <th class="<?=$th_class?>"><?=$th_title?></th>
                    <?}?>
                </tr>
                </thead>
                <tbody>
                <?foreach($row_data as $dvalue_key => $dvalue_val){
                    /* 날짜 요일 결제-금액 결제-건수 결제-재구매건[율] */
                    $srp_year_month_day = $dvalue_val['srp_year_month']."-".$dvalue_val['srp_day'];
                    $srp_week = get_yoil($dvalue_val['srp_week'],1,1);
                    $sumpay = number_format($dvalue_val['sumpay'])." point";
                    $sumcnt = number_format($dvalue_val['sumcnt'])." 건";
                    $repay_cnt_val = $dvalue_val['repay_cnt'];
                    $sumcnt_val = $dvalue_val['sumcnt'];
                    if($dvalue_val['sumcnt'] == 0){ $sumcnt_val = 1; }
                    $repct_calc_val = intval(($repay_cnt_val / $sumcnt_val * 1000) / 10);
                    $repct = number_format($dvalue_val['repay_cnt'])." 건 [ ".intval(($repay_cnt_val / $sumcnt_val * 1000) / 10)."% ]";

                    /* 요일별 class */
                    $week_class = $week_en[$dvalue_val['srp_week']];
                    ?>
                    <tr class="<?=$week_class;?> result_hover">
                        <td class="<?=$fetch_row[0][1]?> text_center">
                        	<?=$srp_year_month_day;?>
                          <!--<a href="#" class="<?=$fetch_row[0][1]?>" onclick="popup('day_view_pointpark.php?year_month_day=<?=$srp_year_month_day;?>','day_view', 600, 760);"><?=$srp_year_month_day;?></a>-->
                        </td>
                        <td class="<?=$fetch_row[1][1]?> text_center"><?=$srp_week;?></td>
                        <td class="<?=$fetch_row[2][1]?> text_center"><?=$sumpay;?></td>
                        <td class="<?=$fetch_row[3][1]?> text_center"><?=$sumcnt;?></td>
                        <!--<td class="<?=$fetch_row[4][1]?> text_center"><?=$repct;?></td>-->
                    </tr>
                <?}?>
                </tbody>
            </table>
        </div>
    </section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>