<?
include_once '_common.php'; // 공통

/* PARAMITER 
maker_name // 제공사이름
*/


/* 데이터 가져오기 */
$where_sales_recharge = ""; /* 충전 */

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

// 파트너일 경우
$_e_date		= mb_partner_e_date($nm_member, $_e_date);	// 종료일 현재 -1일 적용

if($_s_date && $_e_date){
	$where_sales_recharge.= date_year_month($_s_date, $_e_date, 'sr');
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "sr_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 요일 정렬시
if($_order_field == "sr_week"){$_order_field_add = " , sr_date "; $_order_add = "desc ";}
// 날짜 정렬시
if($_order_field == "sr_year_month"){$_order_field_add = " , sr_date "; $_order_add = $_order;}
$order_sales_recharge = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

$sql_sales_recharge = "";
$field_sales_recharge = "  
sr_year_month, sr_day, sr_week, 
count(if(sr_hour='00',1,null))as sr_time00,
count(if(sr_hour='01',1,null))as sr_time01,
count(if(sr_hour='02',1,null))as sr_time02,
count(if(sr_hour='03',1,null))as sr_time03,
count(if(sr_hour='04',1,null))as sr_time04,
count(if(sr_hour='05',1,null))as sr_time05,
count(if(sr_hour='06',1,null))as sr_time06,
count(if(sr_hour='07',1,null))as sr_time07,
count(if(sr_hour='08',1,null))as sr_time08,
count(if(sr_hour='09',1,null))as sr_time09,
count(if(sr_hour='10',1,null))as sr_time10,
count(if(sr_hour='11',1,null))as sr_time11,
count(if(sr_hour='12',1,null))as sr_time12,
count(if(sr_hour='13',1,null))as sr_time13,
count(if(sr_hour='14',1,null))as sr_time14,
count(if(sr_hour='15',1,null))as sr_time15,
count(if(sr_hour='16',1,null))as sr_time16,
count(if(sr_hour='17',1,null))as sr_time17,
count(if(sr_hour='18',1,null))as sr_time18,
count(if(sr_hour='19',1,null))as sr_time19,
count(if(sr_hour='20',1,null))as sr_time20,
count(if(sr_hour='21',1,null))as sr_time21,
count(if(sr_hour='22',1,null))as sr_time22,
count(if(sr_hour='23',1,null))as sr_time23

 "; // 가져올 필드 정하기
$group_sales_recharge = " group by sr_year_month, sr_day ";
$limit_sales_recharge = "";

$sql_sales_recharge = "select $field_sales_recharge FROM sales_recharge where 1 $where_sales_recharge $group_sales_recharge $order_sales_recharge $limit_sales_recharge";
$result = sql_query($sql_sales_recharge);
$row_size = sql_num_rows($result);

if($_mode == 'excel') {
	include_once $_cms_folder_top_path.'/excel/day_buy_excel.php'; // 이거 다시 해야 함
} // end if

/* 검색 전체 구하기 - group by 빼고 sum, count함수 이용 */
$sql_sales_recharge_s_all = "select $field_sales_recharge FROM sales_recharge where 1 $where_sales_recharge $order_sales_recharge $limit_sales_recharge";
$sql_sales_recharge_s_all_row = sql_fetch($sql_sales_recharge_s_all );
$s_all_sumpay = $sql_sales_recharge_s_all_row['sumpay'];
$s_all_sumcnt = $sql_sales_recharge_s_all_row['sumcnt'];
$s_all_repay_cnt = $sql_sales_recharge_s_all_row['repay_cnt'];

$s_all_sumpay_text = number_format($s_all_sumpay)." 원";
$s_all_sumcnt_text = number_format($s_all_sumcnt)." 건";
if($s_all_sumcnt == 0){ $s_all_sumcnt = 1; }
$s_all_repay_cnt_text  = number_format($s_all_repay_cnt)." 건 [ ".intval(($s_all_repay_cnt / $s_all_sumcnt * 1000) / 10)."% ]";


/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sr_year_month',1));
array_push($fetch_row, array('요일','sr_week',1));

for($h=0; $h<24; $h++){ 
	$cm_h = $h."시"; 
	if($h <10){$cm_h = "0".$h."시";}
	array_push($fetch_row, array($cm_h,'sl_time'.$cm_h,0));
	
}/* for($rs=0; $rs<24; $rs++){ */

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "일별 결제";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()

?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
	<!--
	<div class="write">
		<input type="button" class="excel_down" value="검색 결과 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?s_date=<?=$_s_date;?>&e_date=<?=$_e_date;?>&order_field=<?=$_order_field;?>&order=<?=$_order;?>&mode=excel'"/>
	</div>
	-->
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<h3>검색 리스트 
		<strong>검색 결과 - 총금액: <?=$s_all_sumpay_text;?> / 총건수: <?=$s_all_sumcnt_text;?> / 총 재구매건[율]: <?=$s_all_repay_cnt_text;?></strong>
	</h3>
	
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 날짜 요일 결제-금액 결제-건수 결제-재구매건[율] */
					$sr_year_month_day = $dvalue_val['sr_year_month']."-".$dvalue_val['sr_day'];
					$sr_week = get_yoil($dvalue_val['sr_week'],1,1); 
					$sumpay = number_format($dvalue_val['sumpay'])." 원";
					$sumcnt = number_format($dvalue_val['sumcnt'])." 건";
					$repay_cnt_val = $dvalue_val['repay_cnt'];
					$sumcnt_val = $dvalue_val['sumcnt'];
					if($dvalue_val['sumcnt'] == 0){ $sumcnt_val = 1; }
					$repct_calc_val = intval(($repay_cnt_val / $sumcnt_val * 1000) / 10);
					$repct = number_format($dvalue_val['repay_cnt'])." 건 [ ".intval(($repay_cnt_val / $sumcnt_val * 1000) / 10)."% ]";
					
					/* 요일별 class */
					$week_class = $week_en[$dvalue_val['sr_week']];
				?>
				<tr class="<?=$week_class;?> result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$sr_year_month_day;?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$sr_week;?></td>
					<? for($h=0; $h<24; $h++){ 
						$cm_h = $h; 
						if($h <10){$cm_h = "0".$h;}?>
						<td class="<?=$css_suffix.$fetch_row[$h+2][1]?> text_center"><?=number_format($dvalue_val['sr_time'.$cm_h]);?></td>
						
					<? }/* for($rs=0; $rs<24; $rs++){ */ ?>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>