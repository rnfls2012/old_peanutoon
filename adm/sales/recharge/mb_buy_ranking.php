<?
include_once '_common.php'; // 공통

/* PARAMITER 
maker_name // 제공사이름
*/


/* 데이터 가져오기 */
$where_member_point_income = " AND mpi.mpi_state=1 "; /* 충전 */

if($_mb_post !=''){
	$where_member_point_income.= "AND mb.mb_post = '$_mb_post' ";
}

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }
if($_s_date && $_e_date){
	$where_member_point_income.= date_year_month($_s_date, $_e_date, 'mpi.mpi');
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "sumwon"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$order_member_point_income = "order by ".$_order_field." ".$_order;

$sql_member_point_income = "";
$field_member_point_income = "   mpi.mpi_member, 
							sum(mpi.mpi_won)as sumwon, 
							count(mpi.mpi_day)as sumcnt, 

							mb.mb_id, 
							mb.mb_name, 
							mb.mb_email, 
							mb.mb_adult, 
							mb.mb_sex, 
							mb.mb_post, 
							mb.mb_level 
							"; // 가져올 필드 정하기
$group_member_point_income = " group by mpi.mpi_member ";
$group_member_point_income.= " having sumwon > 0 ";
$limit_member_point_income = "";

$sql_member_point_income = "select $field_member_point_income FROM member_point_income mpi left JOIN member mb ON mpi.mpi_member=mb.mb_no where 1 $where_member_point_income $group_member_point_income $order_member_point_income $limit_member_point_income";
$result = sql_query($sql_member_point_income);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();

array_push($fetch_row, array('번호','mbr_lank',0));
array_push($fetch_row, array('ID','mpi_member',1));
array_push($fetch_row, array('이름','mb_name',1));
array_push($fetch_row, array('이메일','mb_email',1));
array_push($fetch_row, array('메일수신동의여부','mb_post',0));
array_push($fetch_row, array('성인여부','mb_adult',0));
array_push($fetch_row, array('성별','mb_sex',0));
array_push($fetch_row, array('결제금액','sumwon',1));
array_push($fetch_row, array('결제횟수','sumcnt',1));

// css 클래스 접두사
$css_suffix = "rank_";

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "결제 회원 리스트";
$cms_head_title = $page_title;

if($_mode == 'excel') {
	include_once $_cms_folder_top_path.'/excel/mb_buy_ranking_excel.php';
} // end if

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
	<div class="write">
		<input type="button" class="excel_down" value="검색 결과 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?s_date=<?=$_s_date;?>&e_date=<?=$_e_date;?>&order_field=<?=$_order_field;?>&order=<?=$_order;?>&mb_post=<?=$_mb_post;?>&mode=excel'"/>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_radios($d_mb_post, "mb_post", $_mb_post, 'y', '메일수신전체', ''); ?>
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="sales_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 - <?=number_format($row_size);?>명</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&s_limit='.$s_limit.'&s_date='.$_s_date.'&e_date='.$_e_date.'&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $css_suffix.$fetch_val[1];

					/* 번호에 의한 rowspan, colspan 설정 */
					$rowspan = $colspan = $th_class_btm_w1 = $th_class_color = $thead_tr = "";
				?>
					<?=$thead_tr?>
					<th <?=$rowspan?> <?=$colspan?> class="<?=$th_class?> <?=$th_class_color?> <?=$th_class_btm_w1?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){

				$db_no = $start_page + ($dvalue_key+1); //$start_page 변수는 /adm/_page.php에서 가져옴

				$db_mb_id = $dvalue_val['mb_id'];
				$db_member_name = $dvalue_val['mb_name'];
				$db_member_email = $dvalue_val['mb_email'];
				$db_member_news = "수신".$d_mb_post[$dvalue_val['mb_post']];
				$db_member_adult = $d_adult[$dvalue_val['mb_adult']];
				$db_member_sex = $d_mb_sex[$dvalue_val['mb_sex']];

				$db_sumwon = number_format($dvalue_val['sumwon']).'원';
				$db_sumcnt = number_format($dvalue_val['sumcnt']).'건';

				?>
				<tr class="result_hover">
					<td class="<?=$css_suffix.$fetch_row[0][1]?> text_center"><?=$db_no;?></td>
					<td class="<?=$css_suffix.$fetch_row[1][1]?> text_center"><?=$db_mb_id;?></td>
					<td class="<?=$css_suffix.$fetch_row[2][1]?> text_center"><?=$db_member_name;?></td>
					<td class="<?=$css_suffix.$fetch_row[3][1]?> text_center"><?=$db_member_email;?></td>
					<td class="<?=$css_suffix.$fetch_row[4][1]?> text_center"><?=$db_member_news;?></td>
					<td class="<?=$css_suffix.$fetch_row[5][1]?> text_center"><?=$db_member_adult;?></td>
					<td class="<?=$css_suffix.$fetch_row[6][1]?> text_center"><?=$db_member_sex;?></td>
					<td class="<?=$css_suffix.$fetch_row[7][1]?> text_center"><?=$db_sumwon;?></td>
					<td class="<?=$css_suffix.$fetch_row[8][1]?> text_center"><?=$db_sumcnt;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>