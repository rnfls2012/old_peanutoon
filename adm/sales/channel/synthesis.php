<?
include_once '_common.php'; // 공통

/* PARAMITER 

*/
/* 결제 수단 */
					
/* 데이터 가져오기 */
$where_cash = "AND mc_way != '' "; /* 충전 */

// 기본적으로 몇개 있는지 체크
$sql_cash_total = "SELECT count(*) AS total_cash FROM member_cash WHERE 1 $where_cash";
$total_cash = sql_count($sql_cash_total, 'total_cash');

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

/* 검색 필터링 */

if($_s_date && $_e_date){	
	$where_cash.= "AND (mc_date  > '".$_s_date." ".NM_TIME_HI_start."' AND mc_date  < '".$_e_date." ".NM_TIME_HI_end."') ";
}

if($_s_text) {
	$where_cash.= "AND mc_member like '%$_s_text%' ";
} // end if

if($_mc_pg) {
	$where_cash .= "AND mc_pg ='".$_mc_pg."' ";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "mc_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 날짜 정렬시
$order_cash = "ORDER BY ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

$sql_cash = "";
$field_cash = " * "; // 가져올 필드 정하기
$limit_cash = "";

$sql_cash = "SELECT $field_cash FROM member_cash WHERE 1 $where_cash $order_cash $limit_cash";
$result = sql_query($sql_cash);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('아이디','mc_member_id',0));
array_push($fetch_row, array('결제 금액','mc_won',0));
array_push($fetch_row, array('결제사'.'<br>'.'(결제 수단)','mc_pg',0));
array_push($fetch_row, array('충전 날짜','mc_date',1));
array_push($fetch_row, array('지급 '.$nm_config['cf_cash_point_unit_ko'],'mc_cash_point',0));
array_push($fetch_row, array('지급 '.$nm_config['cf_point_unit_ko'],'mc_point',0));
array_push($fetch_row, array('주문 번호','mc_order',0));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "종합 결제 정보";
$cms_head_title = $page_title;

if($_mode == 'excel') {
	include_once $_cms_folder_top_path.'/excel/synthesis_excel.php';
}

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title;?> 결제자 : <?=number_format($total_cash);?>명</strong>
	</div>
	<? if($_mc_pg) { ?>
	<div class="write">
		<input type="button" class="excel_down" value="<?=$page_title?> 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?<?=$_nm_paras;?>&mode=excel'"/>
	</div>
	<? } ?>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="cash_form" id="cash_form" method="post" action="<?=$_cms_self;?>" onsubmit="return cash_form_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	
						tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); 

						//결제사
						tag_selects($d_sr_pg, "mc_pg", $_mc_pg, 'y', '결제사 전체');
					?>
					<input type="text" id="s_text" name="s_text" placeholder="회원번호 검색" value="<?=$_s_text?>">
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<h3>검색 리스트
		<strong>검색 결과 수 : <?=number_format($row_size);?>명</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 회원 정보 */
					$mb_row = mb_get_no($dvalue_val['mc_member']);
					
					/* 땅콩, 미니땅콩 */
					$cash_point = number_format($dvalue_val['mc_cash_point']).$nm_config['cf_cash_point_unit'];
					$point = number_format($dvalue_val['mc_point']).$nm_config['cf_point_unit'];

					/* 결제사 & 결제 수단 */
					foreach($nm_config['cf_payway'] as $key => $val) {
						if($val[0] == $dvalue_val['mc_pg'] && $val[1] == $dvalue_val['mc_way']) {
							$authty = strtoupper($val[0])."<br>(".$val[2].")";
						} // end if
					} // end foreach 
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center">
						<a href="#" onclick="popup('../../members/member_view.php?mb_id=<?=$mb_row['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 550);">
							<?=$mb_row['mb_id'];?>
						</a>
					</td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=number_format($dvalue_val['mc_won'])."원";?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$authty;?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['mc_date'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=cash_point_view($dvalue_val['mc_cash_point']);?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=point_view($dvalue_val['mc_point'], 'y');?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$dvalue_val['mc_order'];?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>