<?
include_once '_common.php'; // 공통

/* PARAMITER 

*/

/* 데이터 가져오기 */
$where_toss = "AND toss_status != '' "; /* 충전 */

// 기본적으로 몇개 있는지 체크
$sql_toss_total = "select count(*) as total_toss from pg_channel_toss where 1 $where_toss";
$total_toss = sql_count($sql_toss_total, 'total_toss');

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

if($_s_date && $_e_date){	
	$where_toss.= "AND (toss_date >  '".$_s_date." ".NM_TIME_HI_start."' AND toss_date <  '".$_e_date." ".NM_TIME_HI_end."' ) ";
}

if($_s_text) {
	$where_toss.= "AND toss_member_id like '%$_s_text%' ";
} // end if

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "toss_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 날짜 정렬시
$order_toss = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

$sql_toss = "";
$field_toss = " * "; // 가져올 필드 정하기
$limit_toss = "";

$sql_toss = "select $field_toss FROM pg_channel_toss where 1 $where_toss $order_toss $limit_toss";
$result = sql_query($sql_toss);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('아이디','toss_member_id',0));
array_push($fetch_row, array('구매 상품','toss_product',0));
array_push($fetch_row, array('주문 번호','toss_order',0));
array_push($fetch_row, array('충전 날짜','toss_date',1));
array_push($fetch_row, array('지급 '.$nm_config['cf_cash_point_unit_ko']."<br>"."(이벤트)",'toss_cash_point',0));
array_push($fetch_row, array('지급 '.$nm_config['cf_point_unit_ko']."<br>"."(이벤트)",'toss_point',0));
array_push($fetch_row, array('결제 정보','toss_status',0));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "토스";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title;?> 결제자 : <?=number_format($total_toss);?>명</strong>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="toss_form" id="toss_form" method="post" action="<?=$_cms_self;?>" onsubmit="return toss_form_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" placeholder="아이디 검색" value="<?=$_s_text?>">
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<h3>검색 리스트
		<strong>검색 결과 수 : <?=number_format($row_size);?>명</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 땅콩, 미니땅콩 */
					$cash_point = cash_point_view($dvalue_val['toss_cash_point'], 'y')."<br>(".cash_point_view($dvalue_val['toss_event_cash_point'], 'y').")";
					$point = point_view($dvalue_val['toss_point'], 'y')."<br>(".point_view($dvalue_val['toss_event_point'], 'y').")";
					/* 결제 상태 */
					$toss_status = $d_toss_status[$dvalue_val['toss_status']];
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center">
						<a href="#" onclick="popup('../../members/member_view.php?mb_id=<?=$dvalue_val['toss_member_id'];?>','member_view', <?=$popup_cms_width;?>, 550);">
							<?=$dvalue_val['toss_member_id'];?>
						</a>
					</td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['toss_product'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['toss_order'];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['toss_date'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$cash_point;?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$point;?></td>
					<td class="<?=$fetch_row[6][1]?> text_center <?=($toss_status=='완료')?'blue':'red'?>"><?=$toss_status;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>