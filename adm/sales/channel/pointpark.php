<?
include_once '_common.php'; // 공통

/* PARAMITER */
$state_arr = array(0=>"정상", 1=>"에러", 2=>"전체");
if($_mode == "") {
	$_mode = '0';
} // end if
/* 데이터 가져오기 */
switch($_mode) {
	case '0' :
		$where_pointpark = "AND code='0' ";
		break;

	case '1' :
		$where_pointpark = "AND code!='0' ";
		break;

	case '2' :
		$where_pointpark = "";
		break;
} // end switch

// 기본적으로 몇개 있는지 체크
$sql_pointpark_total = "select count(*) as total_pointpark from pg_channel_pointpark where 1 $where_pointpark";
$total_pointpark = sql_count($sql_pointpark_total, 'total_pointpark');

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

if($_s_date && $_e_date){	
	$where_pointpark .= "AND (pointpark_date > '".$_s_date." ".NM_TIME_HI_start."' AND pointpark_date < '".$_e_date." ".NM_TIME_HI_end."') ";
}

if($_s_text) {
	$where_pointpark.= "AND pointpark_member_id like '%$_s_text%' ";
} // end if

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "pointpark_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 날짜 정렬시
$order_pointpark = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

$sql_pointpark = "";
$field_pointpark = " * "; // 가져올 필드 정하기
$limit_pointpark = "";

$sql_pointpark = "select $field_pointpark FROM pg_channel_pointpark where 1 $where_pointpark $order_pointpark $limit_pointpark";
$result = sql_query($sql_pointpark);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','pointpark_no',0));
array_push($fetch_row, array('회원번호','pointpark_member',0));
array_push($fetch_row, array('아이디','pointpark_member_id',0));
array_push($fetch_row, array('주문번호','pointpark_order',0));
array_push($fetch_row, array('구매 '.$nm_config['cf_cash_point_unit_ko'],'pointpark_cash_point',0));
array_push($fetch_row, array('사용 포인트','totalpoint',0));
array_push($fetch_row, array('상태','code',0));
array_push($fetch_row, array('정상 : 포인트파크 거래 번호'.'<br>'.'에러 : 에러 메시지','tradeno',0));
array_push($fetch_row, array('날짜','pointpark_date',0));

/* 데이터 리스트 */

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "포인트 파크";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title;?> 결제자 : <?=number_format($total_pointpark);?>명</strong>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="poinrpark_form" id="poinrpark_form" method="post" action="<?=$_cms_self;?>" onsubmit="return poinrpark_form_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($state_arr, "mode", $_mode, 'n'); ?>

					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>

					<input type="text" id="s_text" name="s_text" placeholder="아이디 검색" value="<?=$_s_text?>">
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<h3>검색 리스트
		<strong>검색 결과 수 : <?=number_format($row_size);?>명</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 땅콩, 미니땅콩 */
					$cash_point = cash_point_view($dvalue_val['pointpark_cash_point'], 'y');
					$pointpark_point = number_format($dvalue_val['totalpoint'])." Point";

					/* 상태 & 주문번호,메시지 */
					$state = "정상";
					$state_class = "success";
					$tradeno_msg = $dvalue_val['tradeno'];
					if($dvalue_val['code'] != '0') {
						$state = "에러";
						$state_class = "error";
						$tradeno_msg = $dvalue_val['message'];
					} // end if
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['pointpark_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['pointpark_member'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center">
						<a href="#" onclick="popup('../../members/member_view.php?mb_id=<?=$dvalue_val['pointpark_member_id'];?>','member_view', <?=$popup_cms_width;?>, 550);">
							<?=$dvalue_val['pointpark_member_id'];?>
						</a>
					</td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['pointpark_order'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$cash_point;?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$pointpark_point;?></td>
					<td class="<?=$fetch_row[6][1]?> text_center <?=$state_class;?>"><?=$state;?></td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=$tradeno_msg;?></td>
					<td class="<?=$fetch_row[9][1]?> text_center"><?=$dvalue_val['pointpark_date'];?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>