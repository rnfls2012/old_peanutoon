<?
include_once '_common.php'; // 공통

/* PARAMITER 

*/

/* 데이터 가져오기 */
$where_channeling = ""; /* 충전 */

if($_s_date == ''){ $_s_date = date('Y-m-01', NM_SERVER_TIME); }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

if($_s_date && $_e_date){	
	$where_channeling.= "AND (ags_date >  '$_s_date' AND ags_date <  '$_e_date' ) ";
}

if($_s_text) {
	$where_channeling.= "AND rOrdNm like '%$_s_text%' ";
} // end if

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "ags_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 날짜 정렬시
if($_order_field == "sr_year_month"){$_order_field_add = " , ags_date "; $_order_add = $_order;}
$order_channeling = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

$sql_channeling = "";
$field_channeling = " * "; // 가져올 필드 정하기
$limit_channeling = "";

$sql_channeling = "select $field_channeling FROM pg_channel_allthegate where 1 $where_channeling $order_channeling $limit_channeling";
$result = sql_query($sql_channeling);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('아이디','rOrdNm',0));
array_push($fetch_row, array('구매 상품','rProdNm',0));
array_push($fetch_row, array('결제 수단','AuthTy',0));
array_push($fetch_row, array('충전 날짜','ags_date',1));
array_push($fetch_row, array('주문 번호','rOrdNo',0));
array_push($fetch_row, array('지급 '.$nm_config['cf_cash_point_unit_ko'],'Column1',0));
array_push($fetch_row, array('지급 '.$nm_config['cf_point_unit_ko'],'Column2',0));
array_push($fetch_row, array('성공 여부','rSuccYn',0));
array_push($fetch_row, array('결제 정보','note',0));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "올더게이트";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="channeling_form" id="channeling_form" method="post" action="<?=$_cms_self;?>" onsubmit="return channeling_form_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">

					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>

					<input type="text" id="s_text" name="s_text" placeholder="아이디 검색" value="<?=$_s_text?>">
				</div>
				<div class="cs_calendar <?=$cs_calendar_on;?>">
					<label for="s_date">시작일</label>
					<input type="text" name="s_date" value="<?=$_s_date;?>" id="s_date" class="d_date readonly" readonly /> 
					<label for="s_date_view">일</label>
					<label for="date_"> / </label>
					<label for="e_date">종료일</label>
					<input type="text" name="e_date" value="<?=$_e_date;?>" id="e_date" class="d_date readonly" readonly />
					<label for="e_date_view">일</label>
					<div class="cs_calendar_btn">
						<a href="#이주전" onclick="a_click_false();" data-fr_date="<?=$week2_term?>" data-to_date="<?=NM_TIME_YMD;?>">이주전</a>
						<a href="#이번달" onclick="a_click_false();" data-fr_date="<?=$month_term?>" data-to_date="<?=NM_TIME_YMD;?>">이번달</a>
						<a href="#최근2개월" onclick="a_click_false();" data-fr_date="<?=$month2_term?>" data-to_date="<?=NM_TIME_YMD;?>">최근2개월</a>
						<a href="#최근3개월" onclick="a_click_false();" data-fr_date="<?=$month3_term?>" data-to_date="<?=NM_TIME_YMD;?>">최근3개월</a>
					</div>
				</div>
			</div>
			<div class="cs_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<h3>검색 리스트</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 땅콩, 미니땅콩 */
					$cash_point = number_format($dvalue_val['Column1']).$nm_config['cf_cash_point_unit'];
					$point = number_format($dvalue_val['Column2']).$nm_config['cf_point_unit'];

					/* 결제 수단 */
					$authty = $d_authty[$dvalue_val['AuthTy']];

					/* 성공 여부 */
					$rsuccyn = $d_rsuccyn[$dvalue_val['rSuccYn']];

					/* 결제 정보 */
					switch($dvalue_val['AuthTy']) {
						case "hp" :
							$note = "통신사 : ".$dvalue_val['rHP_COMPANY']." / "."결제 핸드폰 번호 : ".$dvalue_val['rHP_HANDPHONE'];
							break;
						
						case "card" :
							$note = "카드 승인번호 : ".$dvalue_val['rApprNo']." / "."카드사 명 : ".$dvalue_val['rCardNm'];
							break;
						
						case "virtual" :
							$note = "가상계좌번호 : ".$dvalue_val['rVirNo'];
							break;

						default :
							$note = "결제 정보가 없습니다.";
							break;
					} // end switch
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center">
						<a href="#" onclick="popup('../../members/member_view.php?mb_id=<?=$dvalue_val['rOrdNm'];?>','member_view', <?=$popup_cms_width;?>, 550);">
							<?=$dvalue_val['rOrdNm'];?>
						</a>
					</td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['rProdNm'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$authty;?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['ags_date'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['rOrdNo'];?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$cash_point;?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$point;?></td>
					<td class="<?=$fetch_row[7][1]?> text_center <?=($dvalue_val['rSuccYn']=='y')?'blue':'red'?>"><?=$rsuccyn;?></td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=$note;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>