<?
include_once '_common.php'; // 공통

/* PARAMITER 

*/

/* 데이터 가져오기 */
$where_payco = "AND payco_payment_completion_yn='y' ";
// $where_payco = "AND payco_status != '' and payco_status != 'return' ";

// 기본적으로 몇개 있는지 체크
$sql_payco_total = "select count(*) as total_payco from pg_channel_payco where 1 $where_payco";
$total_payco = sql_count($sql_payco_total, 'total_payco');

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

if($_s_date && $_e_date){	
	$where_payco.= "AND (payco_date > '".$_s_date." ".NM_TIME_HI_start."' AND payco_date < '".$_e_date." ".NM_TIME_HI_end."') ";
}

if($_s_text) {
	$where_payco.= "AND payco_member_id like '%$_s_text%' ";
} // end if

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "payco_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 날짜 정렬시
$order_payco = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

$sql_payco = "";
$field_payco = " * "; // 가져올 필드 정하기
$limit_payco = "";

$sql_payco = "select $field_payco FROM pg_channel_payco where 1 $where_payco $order_payco $limit_payco";
$result = sql_query($sql_payco);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('아이디','payco_member_id',0));
array_push($fetch_row, array('구매 상품','payco_product',0));
array_push($fetch_row, array('충전 날짜','payco_date',1));
array_push($fetch_row, array('지급 '.$nm_config['cf_cash_point_unit_ko']."<br>"."(이벤트)",'payco_cash_point',0));
array_push($fetch_row, array('지급 '.$nm_config['cf_point_unit_ko']."<br>"."(이벤트)",'payco_point',0));
array_push($fetch_row, array('결제 수단','payco_payment_method_code',0));
array_push($fetch_row, array('주문번호'.'<br>'.'(결제 상태)','payco_status',0));
array_push($fetch_row, array('결제 정보','payco_note',0));

/* 데이터 리스트 */
$payment_method = array("01"=>"신용카드(일반)", "02"=>"무통장 입금", "04"=>"계좌이체", "05"=>"휴대폰(일반)", "31" =>"신용카드", "35"=>"바로이체", "60"=>"휴대폰", 
"98"=>"PAYCO 포인트", "75"=>"PAYCO 쿠폰", "76"=>"카드 쿠폰", "77"=>"가맹점 쿠폰", "96"=>"충전금 환불"); // 결제 수단

$payco_status = array("return"=>"결제 인증", "approval"=>"결제승인 요청", "complete"=>"결제 완료", "cancle"=>"결제 취소", "refund"=>"결제 환급", "error"=>"결제 에러"); // 결제 절차&상태

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "PAYCO";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title;?> 결제자 : <?=number_format($total_payco);?>명</strong>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="payco_form" id="payco_form" method="post" action="<?=$_cms_self;?>" onsubmit="return payco_form_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">

					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>

					<input type="text" id="s_text" name="s_text" placeholder="아이디 검색" value="<?=$_s_text?>">
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="sales_result">
	<h3>검색 리스트
		<strong>검색 결과 수 : <?=number_format($row_size);?>명</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 땅콩, 미니땅콩 */
					$cash_point = cash_point_view($dvalue_val['payco_cash_point'], 'y')."<br>(".cash_point_view($dvalue_val['payco_event_cash_point'], 'y').")";
					$point = point_view($dvalue_val['payco_point'], 'y')."<br>(".point_view($dvalue_val['payco_event_point'], 'y').")";

					/* 결제 정보 */
					switch($dvalue_val['payco_payment_method_code']) {
						case "01" :
						case "31" :
							$note = "카드 사 : ".$dvalue_val['payco_card_company_name']." / 카드 번호 : ".$dvalue_val['payco_card_no'];
							break;

						case "05" :
						case "60" :
							$note = "결제 휴대폰 번호 : ".$dvalue_val['payco_cellphone_no'];
							break;

						case "02" :
						case "04" :
						case "35" :
							$note = "은행 명 : ".$dvalue_val['payco_bank_name']." / 계좌 번호 : ".$dvalue_val['payco_account_no'];
							break;

						default :
							$note = "결제 정보가 없습니다.";
							break;
					} // end switch

				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center">
						<a href="#" onclick="popup('../../members/member_view.php?mb_id=<?=$dvalue_val['payco_member_id'];?>','member_view', <?=$popup_cms_width;?>, 550);">
							<?=$dvalue_val['payco_member_id'];?>
						</a>
					</td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['payco_product'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['payco_date'];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$cash_point;?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$point;?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$payment_method[$dvalue_val['payco_payment_method_code']];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center">
						<?=$dvalue_val['payco_order'].'<br>'.'('.$payco_status[$dvalue_val['payco_status']]." [".$dvalue_val['payco_status']."])";?></td>
					<td class="<?=$fetch_row[7][1]?> text_center"><?=$note;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>