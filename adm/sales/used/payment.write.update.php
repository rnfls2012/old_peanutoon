<? include_once '_common.php'; // 공통
define('_RECHARGE_', true);

$result['state'] = 0;
$result['msg'] = $_mpu_no."가 환불 되였습니다.";

$sql_mpu = " SELECT * FROM member_point_used mpu
			JOIN config_unit_pay cup ON mpu.mpu_cash_type = cup.cup_type WHERE 1 
			AND mpu_member		= '$_mpu_member' 
			AND mpu_member_idx	= '$_mpu_member_idx' 
			AND mpu_class		= '$_mpu_class' 
			AND mpu_no			= '$_mpu_no' 
			AND mpu_refund		= '0' 
			";
$row_mpu = sql_fetch($sql_mpu);

$db_mpu_member = mb_get_no($row_mpu['mpu_member']);

if($row_mpu['mpu_cancel'] == 'y' || $row_mpu['mpu_no_cancel'] != 0 || $row_mpu['mpu_refund'] != 0 || $row_mpu['mpu_class'] == 'e' || $dvalue_val['mpu_class'] == 'b'){
	$result['msg'] = "취소불가한 번호입니다.";
	$result['state'] = 1;
}else{
	// PG사 정보 추출
	$pg_arr = array('kcp','payco','toss');
	$mpu_from_pg = "PG사_".$row_mpu['mpu_from'];
	$pg = "";

	foreach($pg_arr as $pa_val){
		if(strpos($mpu_from_pg, $pa_val) > 0){
			$pg = $pa_val;
		}
	}

	$nm_member_refund  = mb_get_no($row_mpu['mpu_member']);
	$transaction = array();
	$payway_code = array();
	$from_cancel = '-관리자-환불처리';

	switch($pg){
		case 'kcp' :
			$row_cancel = cs_pg_channel_kcp($nm_member_refund, $row_mpu['mpu_order']);
			$transaction['tno'] = $row_cancel['tno'];
			$transaction['mod_desc'] = $from_cancel;
			$payway_code[0] =  $row_cancel['kcp_payway'];
		break;
		case 'payco' :
			include(NM_PAYCO_PATH."/cfg/payco_config.php");
			$row_cancel = cs_pg_channel_payco($nm_member_refund, $row_mpu['mpu_order']);
			$transaction['reserveOrderNo'] = $row_cancel['payco_reserve_order_no'];
			$transaction['sellerOrderReferenceKey'] = $row_cancel['payco_seller_order_reference_key'];
			$transaction['sellerKey'] = $sellerKey;
			$payway_code[0] =  $nm_config['cf_payway'][2][1];
			$payway_code[1] =  $nm_config['cf_payway'][3][1];
		break;
		case 'toss' :
			include NM_TOSS_PATH.'/cfg/site_conf_inc.php';
			$row_cancel = cs_pg_channel_toss($nm_member_refund, $row_mpu['mpu_order']);
			$transaction['apikey'] = $apikey;
			$transaction['payToken'] = $row_cancel['toss_pay_token'];
			$transaction['amount'] = $row_cancel['toss_amt'];
			$transaction['amountTaxFree'] = $row_cancel['toss_amt'];
			$payway_code[0] =  $nm_config['cf_payway'][4][1];
		break;
		default: 
			$row_cancel = "";	
		break;
	}
			
	// 쿠폰정보
	$coupondc = '';
	$randombox_coupon_use = false;
	$erc_payway_no = array();
	foreach($nm_config['cf_payway'] as $payway_key => $payway_val){
		if($payway_val[0] == $pg){
			if(in_array($payway_val[1], $payway_code)){
				array_push($erc_payway_no, $payway_key);
			}
		}
	}
	$erc_crp_no = $row_cancel[$pg.'_crp_no'];
	$erc_date = $row_mpu['mpu_date'];
	$sql_erc = " select * from event_randombox_coupon 
	             where erc_member=".$nm_member_refund['mb_no']." 
				 and erc_crp_no=".$erc_crp_no." 
				 and erc_payway_no in (".implode(",",$erc_payway_no).") 
				 and erc_date='".$erc_date."';";
	$row_erc = sql_fetch($sql_erc);
	if(intval($row_erc['erc_no']) > 0){
		$coupondc = $row_erc['erc_no'];
		$randombox_coupon_use = true;
	}
	
	if($row_cancel != '' && count($transaction) > 0 && intval($nm_member_refund['mb_no']) > 0){
		$boolean_mpi = true;
		$boolean_cash = true;
		$boolean_sr = true;
		$db_result = cs_recharge_save_cancel($nm_member_refund, $pg, $row_mpu['mpu_order'], $transaction, $coupondc, $boolean_mpi, $boolean_cash, $boolean_sr, $randombox_coupon_use, $from_cancel);
	}
	/*
	$mpu_cancel = 'y';
	$mpu_no_cancel = $row_mpu['mpu_no'];

	if($row_mpu['mpu_class'] == 'b'){ // 구매일때

		$mb_cash_point_use = -1 * intval($row_mpu['mpu_cash_point']);
		$mb_point_use = -1 * intval($row_mpu['mpu_point']);
		$mb_cash_point_balance =  intval($db_mpu_member['mb_cash_point']) + abs($mb_cash_point_use);
		$mb_point_balance = intval($db_mpu_member['mb_point']) + abs($mb_point_use);

		// $row_mpu['mpu_count'];

	}else if($row_mpu['mpu_class'] == 'r' || $row_mpu['mpu_class'] == 'e'){ // 충전일때

		$mb_cash_point_use = -1 * intval($row_mpu['mpu_recharge_cash_point']);
		$mb_point_use = -1 * intval($row_mpu['mpu_recharge_point']);
		$mb_cash_point_balance =  intval($db_mpu_member['mb_cash_point']) - abs($mb_cash_point_use);
		$mb_point_balance = intval($db_mpu_member['mb_point']) - abs($mb_point_use);

	}else{
		$result['msg'] = "에러 입니다.";
		$result['state'] = 2;
	}

	if($mb_cash_point_balance < 0 || $mb_point_balance < 0){
		$result['msg'] = "잔액이 부족합니다.";
		$result['state'] = 3;
	}

	if($result['state'] == 0){
		if($row_mpu['mpu_class'] == 'b'){
			
			$sql_mpec_update = "
			UPDATE `member_point_expen_comics` SET 
			`mpec_big` = '".$row_mpu['mpu_big']."', 
			`mpec_small` = '".$row_mpu['mpu_small']."', 

			`mpec_cash_type` = '".$row_mpu['mpu_cash_type']."', 
			`mpec_cash_point` = '".$mb_cash_point_use."', 
			`mpec_point` = '".$mb_point_use."', 
			`mpec_count`= (mpec_count-".$row_mpu['mpu_count']."), 
			`mpec_cash_point_balance` = '".$mb_cash_point_balance."', 
			`mpec_point_balance` = '".$mb_point_balance."', 

			`mpec_state` = '3', 
			`mpec_way` = '".$row_mpu['mpu_way']."', 
			`mpec_age` = '".$row_mpu['mpu_age']."', 
			`mpec_sex` = '".$row_mpu['mpu_sex']."', 

			`mpec_os` = '".$row_mpu['mpu_os']."', 
			`mpec_brow` = '".$row_mpu['mpu_brow']."', 
			`mpec_user_agent` = '".$row_mpu['mpu_user_agent']."', 
			`mpec_version` = '".$row_mpu['mpu_version']."', 

			`mpec_year_month` = '".NM_TIME_YM."', 
			`mpec_year` = '".NM_TIME_Y."', 
			`mpec_month` = '".NM_TIME_M."', 
			`mpec_day` = '".NM_TIME_D."', 
			`mpec_hour` = '".NM_TIME_H."', 
			`mpec_week` = '".NM_TIME_W."', 
			`mpec_date` = '".NM_TIME_YMDHIS."' 

			WHERE 1 AND mpec_comics = '".$row_mpu['mpu_comics']."' 
					AND mpec_member = '".$db_mpu_member['mb_no']."' 
					AND mpec_member_idx = '".$db_mpu_member['mb_idx']."' 
					AND mpec_big = '".$row_mpu['mpu_big']."' 
					AND mpec_small = '".$row_mpu['mpu_small']."' ";
			sql_query($sql_mpec_update);
				
			// 코믹스 구매 번호 구하기 - member_point_expen
			$sql_mpec_chk = "SELECT * FROM member_point_expen_comics WHERE 1 AND mpec_comics = '".$row_mpu['mpu_comics']."' 
					AND mpec_member = '".$db_mpu_member['mb_no']."' 
					AND mpec_member_idx = '".$db_mpu_member['mb_idx']."' 
					AND mpec_big = '".$row_mpu['mpu_big']."' 
					AND mpec_small = '".$row_mpu['mpu_small']."' ";
			$row_mpec_chk = sql_fetch($sql_mpec_chk);

			// 코믹스 에피소드 구매 입력
			$sql_mpee_insert = "
			INSERT INTO `member_point_expen_episode_".$row_mpu['mpu_big']."` (
			`mpee_member`, `mpee_member_idx`, 
			`mpec_no`, `mpee_comics`, `mpee_small`, 
			`mpee_cash_type`, `mpee_cash_point`, `mpee_point`, `mpee_cash_point_balance`, `mpee_point_balance`, 
			`mpee_state`, `mpee_age`, `mpee_way`, `mpee_sex`, 
			`mpee_os`, `mpee_brow`, `mpee_user_agent`, `mpee_version`, 
			`mpee_year_month`, `mpee_year`, `mpee_month`, `mpee_day`, `mpee_hour`, `mpee_week`, `mpee_date`
			) VALUES (
			'".$db_mpu_member['mb_no']."', '".$db_mpu_member['mb_idx']."', 
			'".$row_mpec_chk['mpec_no']."', '".$row_mpu['mpu_comics']."', '".$row_mpu['mpu_small']."', 
			'".$row_mpu['mpu_cash_type']."', '".$mb_cash_point_use."', '".$mb_point_use."', '".$mb_cash_point_balance."', '".$mb_point_balance."', 
			'3', '".$row_mpu['mpu_age']."', '".$row_mpu['mpu_way']."', '".$row_mpu['mpu_sex']."', 
			'".$row_mpu['mpu_os']."', '".$row_mpu['mpu_brow']."', '".$row_mpu['mpu_user_agent']."', '".$row_mpu['mpu_version']."', 
			'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', '".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."' 
			)";
			sql_query($sql_mpee_insert);

		}else{

		}
	}
	*/
}
pop_close($result['msg']);
die;
?>