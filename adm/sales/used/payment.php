<?
include_once '_common.php'; // 공통

/* 데이터 가져오기 */
$where_mpu = "";
$_s_type = $_POST['search_type'];

$d_mpu_class = array('', '획득', '사용', '환불중', '환불완료', '이벤트획득');

// 검색단어+ 검색타입
if($_s_text){
	switch($_s_type){
		case '0': 
			$where_mpu.= "and mb.mb_id like '%$_s_text%' ";
			$checked_id = "checked='checked'";
			break;

		case '1': 
			$where_mpu.= "and mpu.mpu_from like '%$_s_text%' ";
			$checked_from = "checked='checked'";
			break;

		default: 
			$where_mpu.= "and ( mb.mb_id like '%$_s_text%' or mpu.mpu_from like '%$_s_text%' )";
			$checked_all = "checked='checked'";
			break;
	} // end switch
} // end if

// 구분(r:충전, e:이벤트충전, b:구매)
/*
if($_mpu_class){
	$where_mpu.= " AND mpu.mpu_class = '$_mpu_class' ";
}
*/

if($mpu_class != "") {
	if($mpu_class == 0) {

	}else if($mpu_class == 1) {
		$where_mpu.= " AND (mpu_class='r') AND mpu_refund=0  ";
	}else if($mpu_class == 2) {
		$where_mpu.= " AND mpu_class='b' ";
	}else if($mpu_class == 3) {
		$point_class_where = " AND (mpu_class='r' AND mpu_refund=1 AND mpu_recharge_won>0) ";
	}else if($mpu_class == 4) {
		$point_class_where = " AND (mpu_class='r' AND (mpu_refund=2 OR (mpu_refund=1 AND mpu_recharge_won<0))) ";
	} else {
		$where_mpu.= " AND mpu_class='e' ";
	} // end else


} // end if

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }
if($_s_date && $_e_date){
	$where_mpu.= date_year_month($_s_date, $_e_date, 'mpu');
}

// 기본적으로 몇개 있는지 체크
//  로우커리 18-06-01
// $sql_total_mpu = "select count(*) as total_mpu from member_point_used mpu left join member mb on mpu.mpu_member = mb.mb_no where 1 $mpu_where";
// $total_mpu = sql_count($sql_total_mpu, 'total_mpu');

// 정렬
$order_mpu = " ORDER BY mpu_no DESC ";

$sql_mpu = "";
$field_mpu = "	 mpu.* , mb.mb_id,
						IF(mpu_class='b', '사용', 
							IF(mpu_refund=1 && mpu_recharge_won>0, '환불중',
								IF(mpu_refund=1 && mpu_recharge_won<0, '환불완료', 
									IF(mpu_refund=2, '환불완료', 
										IF(mpu_recharge_cash_point<0 || mpu_recharge_point<0, '사용',
											IF(mpu_class='e', '이벤트획득','획득')))))) as class, 

				IF(mpu.mpu_class='b', mpu.mpu_cash_point, mpu.mpu_recharge_cash_point) as mpu_cash_point_result, 
				IF(mpu.mpu_class='b', mpu.mpu_point, mpu.mpu_recharge_point) as mpu_point_result, 
				IF(mpu.mpu_class='b', 'red', IF(mpu.mpu_recharge_cash_point<0 || mpu.mpu_recharge_point<0, 'red', 'blue')) as mpu_color 
"; // 가져올 필드 정하기
$limit_mpu = ""; // 가져올 갯수 정하기

$sql_mpu = "SELECT $field_mpu FROM member_point_used mpu left join member mb on mpu.mpu_member = mb.mb_no WHERE 1 $where_mpu $order_mpu $limit_mpu";
$result = sql_query($sql_mpu); // 페이징 처리 필수 변수명
$row_size = sql_num_rows($result); // 페이징 처리 필수 변수명

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호', 'mpu_no', 3));
array_push($fetch_row, array('회원아이디', 'mpu_member', 3));
array_push($fetch_row, array('날짜', 'mpu_date', 3));
array_push($fetch_row, array('주문번호','mpu_order',3));
array_push($fetch_row, array('내역 상세','mpu_from1',3));
array_push($fetch_row, array('구별','class',3));
array_push($fetch_row, array('충전/사용 내역','mpu_result',2));
array_push($fetch_row, array('잔여 내역','mpu_balance',2));
array_push($fetch_row, array('환불여부','mpu_cancle',3)); /* 181001 */

$cs_calendar_on = 'cs_calendar_on';
$cs_submit_h = 'cs_submit_on';

$page_title = "회원 충전/결제 내역";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<style type="text/css">
	.result_hover .mpu_cancle button {
		display: inline-block;
		font-size: 15px;
		font-family: Nanum Gothic,dotum;
		font-weight: bold;
		background: #fb6b6b;
		padding: 5px 10px;
		margin-left: 5px;
		color: #fff;
		border-radius: 50px;
	}
	.result_hover .mpu_cancle button.see_btn {
		background: #f1b530;
	}

	#sales_result div .mpu_cancle {
		width: 150px;
	}
</style>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$cms_page_title;?></h1>
	<div>
		<!-- <strong><?=$page_title;?> : <?/* =number_format($total_mpu);*/ ?>건</strong> -->
	</div>
</section> <!-- member_head -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="member_search_form" id="member_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return member_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_s_type_btn">
				<?	$mpu_s_type = array('아이디', '내역 상세');
					tag_radios($mpu_s_type, "search_type", $_s_type, 'y', '전체', ''); ?>
				</div>
				<div class="cs_search_btn">
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
				<div class="cs_search_btn">				
				<?  tag_selects($d_mpu_class, 'mpu_class', $_mpu_class, 'y', '전체', '');?>

				<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit member_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="72" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="sales_result">
	<h3 id="cr_thead_mg_add">검색 리스트
		<strong>검색 결과 수 : <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
					<?
					//정렬표기
					$order_giho = "▼";
					$th_order = "desc";
					if($_order == 'desc'){ 
						$order_giho = "▲"; 
						$th_order = "asc";
					}
					foreach($fetch_row as $fetch_key => $fetch_val){
						$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = $th_rowspan = $th_colspan = "";
						switch($fetch_val[2]){
							case '3': $th_rowspan = 'rowspan="2"';
							break;
							case '2': $th_colspan = 'colspan="2"';
							break;

						}
						if($fetch_val[2] == 1){
							$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
							$th_ahref_e = '</a>';
						}
						if($fetch_val[1] == $_order_field){
							$th_title_giho = $order_giho;
						}
						$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
						$th_class = $fetch_val[1];
					?>
						<th <?=$th_rowspan?> <?=$th_colspan?> class="<?=$th_class?>"><?=$th_title?></th>
					<?}?>
				</tr>
				<tr>
				<?for($u=0;$u<2;$u++){?>
					<th class="cash_point_unit"><?=$nm_config['cf_cash_point_unit_ko']?></th>
					<th class="point_unit"><?=$nm_config['cf_point_unit_ko'];?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){ 

					// 회원정보
					$db_mpu_member = mb_get_no($dvalue_val['mpu_member']);

					// 결제-코인
					$db_mpu_cash_point_result = cash_point_view($dvalue_val['mpu_cash_point_result'], 'y');
					// 결제-보너스코인
					$db_mpu_point_result = point_view($dvalue_val['mpu_point_result'], 'y');

					// 잔여-코인
					$db_mpu_cash_point_balance = cash_point_view($dvalue_val['mpu_cash_point_balance'], 'y');
					// 잔여-보너스코인
					$db_mpu_point_balance = point_view($dvalue_val['mpu_point_balance'], 'y');

					// 결제버튼
					// $popup_mpu_cancel_url = $_cms_write."?mpu_no=".$dvalue_val['mpu_no']."&mpu_member=".$dvalue_val['mpu_member']."&mpu_member_idx=".$dvalue_val['mpu_member_idx']."&mpu_class=".$dvalue_val['mpu_class'];.

					$popup_mpu_cancel_link = $_cms_write;
					$popup_mpu_cancel_para = "?mpu_no=".$dvalue_val['mpu_no']."&mpu_member=".$dvalue_val['mpu_member']."&mpu_member_idx=".$dvalue_val['mpu_member_idx']."&mpu_class=".$dvalue_val['mpu_class'];
					
					$db_mpu_cancel_btn_txt = "환불가능";
					$db_mpu_cancel_btn_class = "mod_btn";

					if($dvalue_val['mpu_cancel'] == 'y' || $dvalue_val['mpu_no_cancel'] != 0 || $dvalue_val['mpu_refund'] !=0 || $dvalue_val['mpu_class'] == 'e' || $dvalue_val['mpu_class'] == 'b'){
						$popup_mpu_cancel_link = str_replace(".write.", ".view.", $_cms_write);
						$db_mpu_cancel_btn_txt = '환불불가';
						$db_mpu_cancel_btn_class = "see_btn";
						
					}
					$popup_mpu_cancel_url = $popup_mpu_cancel_link.$popup_mpu_cancel_para;
					$db_mpu_cancel_btn = '<button class="'.$db_mpu_cancel_btn_class.'" onclick="popup(\''.$popup_mpu_cancel_url.'\',\'book_mod_wirte\', '.$popup_cms_width.', 650);">'.$db_mpu_cancel_btn_txt.'</button>';

					if($dvalue_val['mpu_class'] == 'b' || $dvalue_val['mpu_class'] == 'e'){
						$popup_mpu_cancel_url = "";
						$db_mpu_cancel_btn = $db_mpu_cancel_btn_txt;
					}

					?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?>"><?=$dvalue_val['mpu_no'];?></td>
					<td class="<?=$fetch_row[1][1]?>"><?=$db_mpu_member['mb_id'];?></td>
					<td class="<?=$fetch_row[2][1]?>"><?=$dvalue_val['mpu_date'];?></td>
					<td class="<?=$fetch_row[3][1]?>"><?=$dvalue_val['mpu_order'];?></td>
					<td class="<?=$fetch_row[4][1]?>"><?=$dvalue_val['mpu_from'];?></td>
					<td class="<?=$fetch_row[5][1]." ".$dvalue_val['mpu_color']?>"><?=$dvalue_val['class'];?></td>
					<td class="<?=$fetch_row[6][1]?> cash_point_unit"><?=$db_mpu_cash_point_result;?></td>
					<td class="<?=$fetch_row[6][1]?> point_unit"><?=$db_mpu_point_result;?></td>
					<td class="<?=$fetch_row[7][1]?> cash_point_unit"><?=$db_mpu_cash_point_balance;?></td>
					<td class="<?=$fetch_row[7][1]?> point_unit"><?=$db_mpu_point_balance;?></td>
					<!-- 181001 주석 처리 -->
					<td class="<?=$fetch_row[8][1]?>">
						<?=$db_mpu_cancel_btn;?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>

<? page_view(); /* 페이지처리 */ ?>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>