<?
include_once '_common.php'; // 공통

/*
echo $_mpu_no."<br/>";
echo $_mpu_member."<br/>";
echo $_mpu_member_idx."<br/>";
echo $_mpu_class."<br/>";
*/

$result['state'] = 0;
$result['msg'] = $_mpu_no."가 환불 취소되였습니다.";

$sql_mpu = " SELECT * , 
						IF(mpu_class='b', '사용', 
							IF(mpu_refund=1 && mpu_recharge_won>0, '환불중',
								IF(mpu_refund=1 && mpu_recharge_won<0, '환불완료', 
									IF(mpu_refund=2, '환불완료', 
										IF(mpu_recharge_cash_point<0 || mpu_recharge_point<0, '사용',
											IF(mpu_class='e', '이벤트획득','획득')))))) as class, 
				IF(mpu_class='b', mpu_cash_point, mpu_recharge_cash_point) as mpu_cash_point_result, 
				IF(mpu_class='b', mpu_point, mpu_recharge_point) as mpu_point_result, 
				IF(mpu_class='b', 'red', IF(mpu_recharge_cash_point<0 || mpu_recharge_point<0, 'red', 'blue')) as mpu_color  
			FROM member_point_used mpu
			JOIN config_unit_pay cup ON mpu.mpu_cash_type = cup.cup_type 
			WHERE 1
			AND mpu_member		= '$_mpu_member' 
			AND mpu_member_idx	= '$_mpu_member_idx' 
			AND mpu_class		= '$_mpu_class' 
			AND mpu_no			= '$_mpu_no' 
			";
$row_mpu = sql_fetch($sql_mpu);

// 결제-코인
$db_mpu_cash_point_result = cash_point_view($row_mpu['mpu_cash_point_result'], 'y');
// 결제-보너스코인
$db_mpu_point_result = point_view($row_mpu['mpu_point_result'], 'y');

// 회원정보
$db_mpu_member = mb_get_no($row_mpu['mpu_member']);

if($row_mpu['mpu_class'] == 'b'){ 
	
	$mb_cash_point_use = $mb_point_use = 0;
	$mpu_comics = get_comics($row_mpu['mpu_comics']);

	$ce_up_kind = '화';
	if($mpu_comics['cm_up_kind'] == '1'){ $ce_up_kind = '권'; }

	$episode_arr = array();
	$sql_mpee = " SELECT * FROM member_point_expen_episode_".$row_mpu['mpu_big']." mpee 
				JOIN config_unit_pay cup ON mpee.mpee_cash_type = cup.cup_type 
				JOIN comics_episode_".$row_mpu['mpu_big']." ce ON mpee.mpee_episode = ce.ce_no 
				WHERE 1
				AND mpee_member		= '$_mpu_member' 
				AND mpee_member_idx	= '$_mpu_member_idx' 
				AND mpee_comics		= '".$row_mpu['mpu_comics']."'  
				AND mpee_small		= '".$row_mpu['mpu_small']."' 
				AND mpee_state		= 1 
				ORDER BY ce_order DESC
				";
	$result_mpee = sql_query($sql_mpee);			
	if(sql_num_rows($result_mpee) != 0){
		while ($row_mpee = sql_fetch_array($result_mpee)) {
			if($row_mpee['ce_chapter'] > 0){
				$ce_txt = $row_mpee['ce_chapter'].$ce_up_kind;
				if($row_mpee['ce_title'] != ''){ $ce_txt = $row_mpee['ce_chapter'].$ce_up_kind." - ".$row_mpee['ce_title']; }
			}else{
				$ce_txt = $row_mpee['ce_notice'];
			}
			$row_mpee['ce_txt'] = $ce_txt;

			$mb_cash_point_use += intval($row_mpee['mpee_cash_point']);
			$mb_point_use += intval($row_mpee['mpee_point']);

			array_push($episode_arr, $row_mpee);
		}
	}
	if(count($episode_arr) > 0){ 
		$row_mpu['mpu_from'].= " 외 ".(count($episode_arr)-1).$ce_up_kind; 
		// 결제-코인
		$db_mpu_cash_point_result = cash_point_view($mb_cash_point_use, 'y');
		// 결제-보너스코인
		$db_mpu_point_result = point_view($mb_point_use, 'y');
	}
}


// PG사 정보 추출
$pg_arr = array('kcp','payco','toss');
$pg_url_arr = array();
array_push($pg_url_arr, 'https://admin8.kcp.co.kr');
array_push($pg_url_arr, 'https://partner.payco.com');
array_push($pg_url_arr, 'https://pay.toss.im/pay/');
$mpu_from_pg = "PG사_".$row_mpu['mpu_from'];
$pg = "";
$pg_url = "";

foreach($pg_arr as $pa_key => $pa_val){
	if(strpos($mpu_from_pg, $pa_val) > 0){
		$pg = $pa_val;
		$pg_url = $pg_url_arr[$pa_key];
	}
}

if(defined("_PAYMENT_VIEW_")){
	$form_bool = false;
	$mpu_from_txt = "환불-불가";
}else{
	$form_bool = true;
	$mpu_from_txt = "환불-가능";
}

$head_title = "회원결제내역";
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<style type="text/css">	
	.pg_payment {
		display: inline-block;
		font-size: 15px;
		font-family: Nanum Gothic,dotum;
		font-weight: bold;
		background: #fb6b6b;
		padding: 5px 10px;
		margin-left: 5px;
		color: #fff;
		border-radius: 50px;
	}
</style>
<section id="cms_page_title">
	<h1><?=$head_title;?></h1>
</section><!-- comics_head -->
<section id="sales_write">
	<? if($form_bool) {?>
	<form name="form_bool" id="form_bool" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return comics_write_submit();">
		<input type="hidden" id="mpu_member" name="mpu_member" value="<?=$_mpu_member;?>"/>				<!-- 입력모드 -->
		<input type="hidden" id="mpu_member_idx" name="mpu_member_idx" value="<?=$_mpu_member_idx;?>"/>	<!-- 입력모드 -->
		<input type="hidden" id="mpu_class" name="mpu_class" value="<?=$_mpu_class;?>"/>				<!-- 입력모드 -->
		<input type="hidden" id="mpu_no" name="mpu_no" value="<?=$_mpu_no;?>"/>							<!-- 입력모드 -->
	<? } /* $form_bool */?>
		<table>
			<tbody>
				<tr>
					<th><label for="mb_id">회원아이디</label></th>
					<td><?=$db_mpu_member['mb_id'];?></td>
				</tr>
				<tr>
					<th><label for="mpu_date">날짜</label></th>
					<td><?=$row_mpu['mpu_date'];?></td>
				</tr>
				<tr>
					<th><label for="mpu_order">주문번호</label></th>
					<td>
						<?=$row_mpu['mpu_order'];?>
					</td>
				</tr>
				<tr>
					<th><label for="mpu_order">PG사 URL</label></th>
					<td>
						<a class="pg_payment" href="<?=$pg_url;?>" target="_blank"><?=$pg;?> 상점관리자이동</a>
					</td>
				</tr>
				<tr>
					<th><label for="mpu_from">내역 상세</label></th>
					<td>
						<?=$row_mpu['mpu_from'];?>
					</td>
				</tr>
				<? if($row_mpu['mpu_class'] == 'b'){ ?>
				<tr>
					<th><label for="mpu_from">코믹스 화 리스트</label></th>
					<td class="mpee_episode_list">
						<ul>
						<? foreach($episode_arr as $episode_key => $episode_val){ ?>
							<li>
								<input type="checkbox" name="mpee_episode[]" id="mpee_episode<?=$episode_key;?>" value="<?=$episode_val['mpee_episode']?>">
								<label for="mpee_episode<?=$episode_key;?>"><?=$episode_val['ce_txt'];?></label>
							</li>
						<? } ?>
						</ul>						
					</td>
				</tr>
				<? } ?>
				<tr>
					<th><label for="mpu_from">구별</label></th>
					<td class="<?=$row_mpu['mpu_color'];?>"><?=$row_mpu['class'];?></td>
				</tr>
				<? if($row_mpu['mpu_class'] == 'r'){ ?>
				<tr>
					<th><label for="mpu_from"><?=$row_mpu['class']?> 내역</label></th>
					<td>
						<?=$nm_config['cf_cash_point_unit_ko'];?> : <?=$db_mpu_cash_point_result;?><br/>
						<?=$nm_config['cf_point_unit_ko'];?> : <?=$db_mpu_point_result;?>
					</td>
				</tr>
				<tr>
					<th><label for="mpu_from">금액</label></th>
					<td class="red"><?=number_format($row_mpu['mpu_recharge_won']);?> 원</td>
				</tr>
				<? } ?>
				<tr>
					<th><label for="mpu_from">환불여부</label></th>
					<td><?=$mpu_from_txt;?></td>
				</tr>
				<tr>
					<td colspan='2' class="submit_btn">
						<? if($form_bool) {?>
						<input type="submit" value="환불" />
						<? } /* $form_bool */?>
						<input type="reset" value="닫기" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	<? if($form_bool) {?>
	</form>
	<? } /* $form_bool */?>
</section><!-- comics_write -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>