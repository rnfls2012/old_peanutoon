<?php
include_once NM_ADM_PATH.'/_common.php';

/* http://jqueryui.com/datepicker/ */
?>
<link type="text/css" href="<?=NM_ADM_URL?>/_date/date-ui.css" rel="stylesheet" />
<link type="text/css" href="<?=NM_ADM_URL?>/_date/style.css">

<script type="text/javascript" src="<?=NM_ADM_URL?>/_date/date-ui.min.js"></script>
<script type="text/javascript">
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();
</script>
<script>
jQuery(function($){
    $.datepicker.regional["ko"] = {
        closeText: "닫기",
        prevText: "이전달",
        nextText: "다음달",
        currentText: "오늘",
        monthNames: ["1월(JAN)","2월(FEB)","3월(MAR)","4월(APR)","5월(MAY)","6월(JUN)", "7월(JUL)","8월(AUG)","9월(SEP)","10월(OCT)","11월(NOV)","12월(DEC)"],
        monthNamesShort: ["1월","2월","3월","4월","5월","6월", "7월","8월","9월","10월","11월","12월"],
        dayNames: ["일","월","화","수","목","금","토"],
        dayNamesShort: ["일","월","화","수","목","금","토"],
        dayNamesMin: ["일","월","화","수","목","금","토"],
        weekHeader: "Wk",
        dateFormat: "yy-mm-dd",
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: true,
        yearSuffix: ""
    };
	$.datepicker.setDefaults($.datepicker.regional["ko"]);
});
</script>

<script type="text/javascript">
<!--
	
$(function(){
	$( "#s_date" ).datepicker({
	  // minDate: '-6M',
      changeMonth: true,
	  changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
      onClose: function( selectedDate ) {
        $( "#e_date" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#e_date" ).datepicker({
      changeMonth: true,
	  changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
      onClose: function( selectedDate ) {
        $( "#s_date" ).datepicker( "option", "maxDate", selectedDate );
      }
    });

	$( "#sr_date" ).datepicker({
      changeMonth: true,
	  changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
      onClose: function( selectedDate ) {
        $( "#er_date" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#er_date" ).datepicker({
      changeMonth: true,
	  changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
      onClose: function( selectedDate ) {
        $( "#sr_date" ).datepicker( "option", "maxDate", selectedDate );
      }
    });

    $( "#ce_service_date" ).datepicker({
      changeMonth: true,
	  changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
	  minDate: 0, /* 오늘 날짜 이후부터 선택 가능하도록 설정하기 */
      onClose: function( selectedDate ) {

      }
    });
    $( "#ce_free_date" ).datepicker({
      changeMonth: true,
	  changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
	  minDate: 0, /* 오늘 날짜 이후부터 선택 가능하도록 설정하기 */
      onClose: function( selectedDate ) {
      }
    });
    
    //180109 - 서비스 중지 예약 요청(배경호 과장님 요청사항), list.write.php
		$( "#cm_res_stop_date" ).datepicker({
      changeMonth: true,
	  changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
	  minDate: 0, /* 오늘 날짜 이후부터 선택 가능하도록 설정하기 */
      onClose: function( selectedDate ) {
      }
    });
    
    //180202 - 서비스 오픈 예약 요청(배경호 과장님 요청사항), list.write.php
    $( "#cm_res_open_date" ).datepicker({
        changeMonth: true,
        changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
        minDate: 0, /* 오늘 날짜 이후부터 선택 가능하도록 설정하기 */
        onClose: function( selectedDate ) {
        }
    });
    
    $( "#all_date" ).datepicker({
      changeMonth: true,
	  changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
      onClose: function( selectedDate ) {
      }
    });
	 $( "#res_date_ymd" ).datepicker({
      changeMonth: true,
	  changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
      onClose: function( selectedDate ) {
      }
    });

	// 180212 랜덤박스 이벤트 CMS용 추가
	$( "#s_er_date" ).datepicker({
      changeMonth: true,
	  changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
	  minDate: 0, /* 오늘 날짜 이후부터 선택 가능하도록 설정하기 */
      onClose: function( selectedDate ) {
      }
    });

	$( "#e_er_date" ).datepicker({
      changeMonth: true,
	  changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
	  minDate: 0, /* 오늘 날짜 이후부터 선택 가능하도록 설정하기 */
      onClose: function( selectedDate ) {
      }
    });

	$( "#s_available_date" ).datepicker({
      changeMonth: true,
	  changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
	  minDate: 0, /* 오늘 날짜 이후부터 선택 가능하도록 설정하기 */
      onClose: function( selectedDate ) {
      }
    });

	$( "#e_available_date" ).datepicker({
      changeMonth: true,
	  changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
	  minDate: 0, /* 오늘 날짜 이후부터 선택 가능하도록 설정하기 */
      onClose: function( selectedDate ) {
      }
    });
    
    //180626 - PUSH registration => push.write.php
    $( "#push_res_ymd" ).datepicker({
        changeMonth: true,
        changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true,
        minDate: 0, /* 오늘 날짜 이후부터 선택 가능하도록 설정하기 */
        onClose: function( selectedDate ) {
        }
    });
});
//-->
</script>