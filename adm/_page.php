<?php
include_once NM_ADM_PATH.'/_common.php'; // 공통

$sql_query		= "";			// SQL문(필드+조건+그룹)
$sql_query_cnt	= "";			// SQL문(필드+조건+그룹)-카운트
$sql_querys		= "";			// SQL문+정렬+제한

$sql_field		= "";			// 필드
$sql_count		= "";			// 필드-카운트

$sql_table		= "";			// 테이블
$sql_table_cnt	= "";			// 테이블-카운트

$sql_where		= " where 1 ";	// 조건
$sql_group		= "";			// 그룹
$sql_order		= "";			// 정렬

$sql_limit		= "";			// 제한
$sql_record		= "";			// 제한-레코드수
$sql_offset		= "";			// 제한-시작 레코드 수

$sql_num_rows	= 0;			// SQL문 총수 레코드값
$sql_result		= "";			// SQL문 결과
$sql_rows		= 0;			// SQL문 결과 레코드값
$sql_pages		= 0;			// SQL문 결과 페이지값

function rows_cnts($sql_query_cnt){
	global $nm_config, $sql_num_rows, $_page, $_s_limit, $sql_limit, $_pages_num, $sql_pages, $_nm_paras;

	$sql_num_rows = intval(sql_count($sql_query_cnt, 'cnt'));
	return $sql_num_rows;
}

function rows_data($sql_querys, $limit=true){
	global $nm_config, $sql_num_rows, $_page, $_s_limit, $sql_limit, $_pages_num, $sql_pages, $_nm_paras;

	$sql_offset = intval($_s_limit);
	if($sql_offset < 1){ 
		$sql_offset = intval($nm_config['s_limit']); 
		$_s_limit = $sql_offset;
	}
	if($_page < 1 || $_page==null || $_page==""){ $_page=1; }

	$sql_pages = ceil($sql_num_rows / $sql_offset); // 전체 페이지 계산
	$sql_record = ($_page - 1) * $sql_offset;		// 시작 열을 구함
	
	if($limit == true){ // 페이지처리 사용 안할 경우도 있어서...
		$sql_limit		= " limit {$sql_record}, {$sql_offset} ";
	}
	
	$sql_querys  = " {$sql_querys} {$sql_limit}"; 

	$rows_data = array();
	if($sql_offset > 0) {
		$sql_result = sql_query($sql_querys);
		while ($sql_rows = sql_fetch_array($sql_result)) {
			array_push($rows_data, $sql_rows);
		}
	}
	return $rows_data;
}

function rows_page(){
	global $nm_config, $sql_num_rows, $_page, $_s_limit, $sql_limit, $_pages_num, $sql_pages, $_nm_paras;
	
	// 페이지 처리 계산
	$pages_num_max = 10;

	if($_page > $pages_num_max){
		if($_page % $pages_num_max == 0){
			$_pages_num = (floor($_page / $pages_num_max) * $pages_num_max +1) - $pages_num_max;
		}else{
			$_pages_num = floor($_page / $pages_num_max) * $pages_num_max +1;
		}
	}

	if($_pages_num==null || $_pages_num=="" || $_pages_num < 1){ $_pages_num = 1; }
	
	//url에서 페이지 빼기
	$_nm_paras = str_replace("&page=".$_page, "", $_nm_paras);
	$_nm_paras = str_replace("page=".$_page, "", $_nm_paras);

	$_page_url = $now_url."?".$_nm_paras;
	$_page_no_arr = array();
	$_page_prev_on = "off";
	$_page_next_on = "off";
	$_page_prev_url = "";
	$_page_next_url = "";

	
	$_page_no_count = 0;
	$_page_width  = $_page_width_last = 100;

	if($sql_pages <= 1){ 

	}else{
		if($_pages_num >= $pages_num_max){ /* 이전 */
			$_page_prev_on = "on";
			$_page_prev_url = $_page_url."&page=".intval($_pages_num - $pages_num_max);
		}
		
		/* 넘버링 */
		$end_i = 0;
		if($_pages_num + $pages_num_max > $sql_pages){ $end_i = $sql_pages; }
		else{ $end_i = $_pages_num + ($pages_num_max -1); }

		for($i=$_pages_num; $i<=$end_i; $i++){
			$pg_current = "";
			if($_page == $i){ $pg_current = "page_current"; }
			/* 넘버, 링크, 일치 */
			array_push($_page_no_arr, array($i, $_page_url."&page=".$i, $pg_current));
		}

		if($_pages_num + $pages_num_max <= $sql_pages){ /* 다음 */
			$_page_next_on = "on";
			$_page_next_url = $_page_url."&page=".intval($end_i+1);
		}


		/* page 숫자 갯수 width 값 구하기 */
		/* page */
		$_page_count = 0;
		$_page_count = count($_page_no_arr);
		if($_page_count > 0){
			$_page_width = intval(100 / $_page_count);
			$_page_width_last = $_page_width + (100 - intval($_page_count)  * intval(100 / intval($_page_count)));
			$head1_last = intval($_page_count)-1;
		}
	}

	// 페이지 처리 VIEW
	if($sql_pages <= 1){

	}else{
		?>
		<link rel="stylesheet" type="text/css" href="<?=NM_URL?>/css/cms_page.css<?=vs_para();?>"/>
		<style type="text/css">
			#page_no_list .pnl_page_left p{ background:#aaa url('<?=NM_IMG?>cms/list_pre.png<?=vs_para();?>') 15% 47% no-repeat; }
			#page_no_list .pnl_page_right p{ background:#aaa url('<?=NM_IMG?>cms/list_next.png<?=vs_para();?>') 85% 47% no-repeat; }
		</style>
		<style type="text/css">
			/* main_menu */
			#page_no_list .pnl_page_center ul li{width:<?=$_page_width;?>%;}
			#page_no_list .pnl_page_center ul li.last{width:<?=$_page_width_last;?>%;}
		</style>
		<div id="page_no_list">
			<?php
				if($sql_pages <= 1){ ?>
					<div class="pnl_page_left">
						<p class="<?=$_page_prev_on?>">이전</p>
					</div>
					<div class="pnl_page_center">
						<ul>
							<li>
								<strong>1</strong>
							</li>
						</ul>
					</div>
					<div class="pnl_page_right">
						<p class="<?=$_page_next_on?>">다음</p>
					</div>
				<?
				}else{?>
					<div class="pnl_page_left">
						<?echo ($_page_prev_url ? "<a href=".$_page_prev_url.">":""); ?>
							<p class="<?=$_page_prev_on?>">
								이전
							</p>
						<?echo ($_page_prev_url ? "</a>":""); ?>
					</div>
					
					<div class="pnl_page_center">
						<ul>
						<?foreach($_page_no_arr as $_page_key => $_page_val){
							$_page_last = "";
							if(($_page_key+1) == $_page_count){ $_page_last = "last"; }
							?>
								<li class="<?=$_page_last?>">
									<a href="<?=$_page_val[1];?>">
										<?echo ($_page_val[2] ? "<strong>":""); ?>
										<?=$_page_val[0];?>		
										<?echo ($_page_val[2] ? "</strong>":""); ?>		
									</a>
								</li>
							<?}?>
						</ul>
					</div>

					<div class="pnl_page_right">
						<?echo ($_page_next_url ? "<a href=".$_page_next_url.">":""); ?>
							<p class="<?=$_page_next_on?>">
								다음
							</p>
						<?echo ($_page_next_url ? "</a>":""); ?>
					</div>

			<?  } ?>
		</div>
	<?} /* if($sql_pages <= 1){ */?>
<? } ?>