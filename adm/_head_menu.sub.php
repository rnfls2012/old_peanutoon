<?php
include_once '_common.php';

mb_partner_acceess($nm_member); // 파트너 접근 제한

include_once NM_ADM_PATH.'/_head.cms.php'; // 해더

/* 현재 접속한 페이지가 steps2에서 steps3이 있는 메뉴 */
$lnbs = array();

/* CMS 메뉴 생성 */
?>
<link rel="stylesheet" type="text/css" href="<?=NM_URL?>/css/cms.css<?=vs_para();?>">
<script type="text/javascript" src="<?=NM_URL?>/js/cms.js<?=vs_para();?>"></script>

<h1 id="head_title"><a href="#head_title"><?=$head_title?></a></h1>

<div id="cms_header">
	<nav id="tnbs">
		<ul>
			<li><a href="<?=NM_URL?>/adm/">CMS</a></li>
			<li><a href="<?=NM_URL?>">HOME</a></li>
			<li><a href="<?=NM_URL?>/event.php">EVENT</a></li>
			<li><a href="<?=NM_URL?>/proc/ctlogout.php">LOGOUT</a></li>
			<? if($nm_member['mb_class'] == 'a'){ ?>
			<li><a href="<?=NM_ADM_CHANGE_URL;?><?=$_SERVER['REQUEST_URI'];?>"><?=NM_ADM_CHANGE_TEXT;?></a></li>
			<? } ?>
			<? if(intval($nm_member['mb_level']) > 28){ ?>
			<li><a href="<?=NM_ADM_URL?>/_ready">ADM_ready</a></li>
			<? } ?>
			<li><a href="<?=NM_ADM_URL?>/info.php" target="blank">phpinfo</a></li>
			<? if($_SESSION['ss_adm_id']){ ?>
			<li><a href="<?=NM_PROC_URL."/ss_mb_id.php";?>" class="cs_center">관리자모드</a></li>
			<? } ?>
			<? if(is_nexcube()){?>
			<li style="color:#fb6b6b;"><?=substr($_SERVER['SERVER_ADDR'],strlen($_SERVER['SERVER_ADDR'])-3,3);?>서버</li>
			<? } ?>

		</ul>
	</nav>
	<nav id="gnbs">
		<ul class="steps1">
		<? foreach($menu as $menu_key => $menu_val){
			/* steps2 글자 길이 구하기 */
			$steps1_len = strlen($menu_val[0][0]);
			$steps2_len = $steps2_len_tmp = 0;
			$steps1_nbsp = "";
			// $steps1_len = strlen($menu_val[0][0]);
			foreach($menu_val as $menu_key2 => $menu_val2){
				$steps2_len_tmp = strlen($menu_val2[0]);
				if($steps2_len_tmp > $steps2_len){
					$steps2_len = $steps2_len_tmp;
				}
			}
			if($steps1_len <= $steps2_len){
				$steps1_len = $steps2_len - $steps1_len +2;
			}
			/* steps2 글자 길이만큼 빈공간 넣기 */
			for($n=0; $n<$steps1_len; $n++){ $steps1_nbsp.= "&nbsp;";}

			/* steps1 현재위치 */
			$current = "";
			if(in_array($menu_val[0][2], $_cms_current_arr)){ 
				$current = "current1"; 
				$lnbs =array_merge($menu_val); /* lnbs 버튼 출력 리스트 */
			}
			/* 하위폴더 링크로 연동 */
			$steps1_link = $menu_val[0][1];
			if(is_array($menu_val[1][3])){
				$steps1_link = $menu_val[1][3][1];
			}else{
				$steps1_link = $menu_val[1][1];
			}

			/* 권한이 있는 메뉴만 보기 */
			$mb_cms_level_access = mb_cms_level_access($nm_member, $menu_val[0][2], 'y');
			if($mb_cms_level_access == false){ continue; }

			/* step2 권한 있는 첫번째 링크 연결 */
			$firstlink = mb_cms_level_access_firstlink($nm_member, $menu_val);
			?>
			<li><a class="<?=$current?>" href="<?=$firstlink?>"><?=$menu_val[0][0]?><?=$steps1_nbsp?></a>
				<ul class="steps2">
				<? foreach($menu_val as $menu_key2 => $menu_val2){ if($menu_key2 == 0){continue;} 
					$is_array_check = "";
					if(is_array($menu_val2[3])){ $is_array_check = "steps3_list"; }
					/* steps2 현재위치 */
					$current = "";
					if($is_array_check){
						if(in_array($menu_val2[2], $_cms_current_arr)){ 
							$current = "current2"; 
							$lnbs =array_merge($menu_val2); /* lnbs 버튼 출력 리스트 */
						}
					}else{
						if(strpos($menu_val2[1], "/".$_cms_current_folder."/".$_cms_current_file) > 0){ $current = "current2"; }
					}
					
					/* 하위폴더 링크로 연동 */
					$steps2_link = $menu_val2[1];
					if(is_array($menu_val2[3])){
						$steps2_link = $menu_val2[3][1];
					}
					

					/* 권한이 있는 메뉴만 보기 */
					$mb_cms_level_access2 = mb_cms_level_access($nm_member, $menu_val2[2], 'y');
					if($mb_cms_level_access2 == false){ continue; }

					?>
					<li class="<?=$is_array_check?>"><a class="<?=$current?>" href="<?=$steps2_link?>"><?=$menu_val2[0]?></a>
						<?if($is_array_check){?>
							<ul class="steps3">
							<?foreach($menu_val2 as $menu_key3 => $menu_val3){ if($menu_val3 == 0){continue;}
							/* steps3 현재위치 */
							$current = "";
							if(strpos($menu_val3[1], "/".$_cms_current_folder."/".$_cms_current_file) > 0){ $current = "current3"; }					
							?>
								<li><a class="<?=$current?>" href="<?=$menu_val3[1]?>"><?=$menu_val3[0]?></a></li>
							<?} /* foreach($menu_val2[3] as $menu_key3 => $menu_val3){ */?>
							</ul>
						<?} /* if(count($menu_val2[3]) > 0){ */?>
					</li>
				<?} /* foreach($menu_val as $menu_key2 => $menu_val2){ */?>
				</ul>
			</li>
		<?} /* foreach($menu as $menu_key => $menu_val ){ */?>
		</ul>
	</nav>
	<?if($lnbs){ /* lnbs 버튼 출력 리스트 */ ?>
	<nav id="lnbs">
		<ul>
		<? // print_r($_cms_current_steps3);
		foreach($lnbs as $lnbs_key => $lnbs_val){ if($lnbs_val == 0){continue;} if($lnbs_key == 0){continue;}
		/* steps3 현재위치 표기 */
		$current = "";
		if(strpos($lnbs_val[1], "/".$_cms_current_folder."/".$_cms_current_file) > 0){ $current = "current3"; } ?>
			<li><a class="<?=$current?>" href="<?=$lnbs_val[1]?>"><?=$lnbs_val[0]?></a></li>
		<?} /* foreach($lnbs as $lnbs_key => $lnbs_val){ */?>
		</ul>
	</nav>
	<?} /* if($lnbs){ */?>
</div>
<div id="cms_header_end">&nbsp;</div>
<?if($lnbs){?>
	<div id="lnbs_end">&nbsp;</div>
<?} /* if($lnbs){ */?>
<div id="container">