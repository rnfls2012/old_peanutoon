<?
include_once '_common.php'; // 공통
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode', 'ec_no');
array_push($para_list, 'ec_name', 'ec_loss', 'ec_loss_percent');
array_push($para_list, 'ec_date', 'ec_state');
array_push($para_list, 'ec_reg_date', 'ec_mod_date');

/* 빈칸 PARAMITER 허용 */

/* 빈칸&숫자 PARAMITER 허용 */

/* 빈칸 PARAMITER 시 NULL 처리 */

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

/* 배열 데이터 받기 */
$_ecc_select_no = $_POST['ecc_select_no'];
$_ecc_name = $_POST['ecc_name'];
$_ecc_is_goods = $_POST['ecc_is_goods'];
$_ecc_point = $_POST['ecc_point'];
$_ecc_limit = $_POST['ecc_limit'];
$_ecc_weight = $_POST['ecc_weight'];

// 위 배열 빈값 있으면 리턴
$post_arr_bool = true;
$post_arr = array();
array_push($post_arr, 'ecc_select_no', 'ecc_name', 'ecc_is_goods', 'ecc_point', 'ecc_limit', 'ecc_weight');
if(count($_POST['ecc_select_no']) > 0){
	foreach($post_arr as $post_val){
		for($p=0; $p<count($_POST[$post_val]); $p++){
			if($_POST[$post_val][$p] == ''){
				$post_arr_bool = false;
			}
		}
	}
}else{
	$post_arr_bool = false;
}

if($post_arr_bool == false){	
	pop_url('선물 상자 리스트 설정 확인하세요', $_SERVER['HTTP_REFERER']);
	die;
}


$dbtable = "event_christmas";
$dbt_primary = "ec_no";
$para_primary = "ec_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

if($_mode == 'reg'){	
	/* 고정값 */
	$_ec_reg_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);
	
	/* DB 저장 */
	if(sql_query($sql_reg)){
		ecc_insert();
		$db_result['msg'] = '이벤트가 등록되었습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '등록 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

/* 수정 */
}else if($_mode == 'mod'){
	if(event_christmas_ctrk_bool($_ec_no) != 'enable'){
		pop_close("수정불가", '', $db_result['error']);
		die;
	}

	$_ec_mod_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $_ec_no.'의 데이터가 수정되었습니다.';
			ec_data_delete();
			ecc_insert();
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

	}

/* 삭제 */
}else if($_mode == 'del'){
	if(event_christmas_ctrk_bool($_ec_no) != 'enable'){
		pop_close("삭제불가", '', $db_result['error']);
		die;
	}
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 데이터 삭제 */
		sql_query("delete from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");		
		$db_result['msg'] = $_ec_no.'의 데이터가 삭제되었습니다.';
		ec_data_delete();
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}
/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
pop_close($db_result['msg'], '', $db_result['error']);
die;

/////////////////////////////////// function ///////////////////////////////////////////
// event_christmas_category
function ecc_insert(){
	global $dbtable, $dbt_primary, $_ec_no, $_ecc_select_no, $_ecc_name, $_ecc_is_goods, $_ecc_point, $_ecc_limit, $_ecc_weight;

	if($_ec_no == ''){
		$sql_max = 'select COALESCE(max('.$dbt_primary.'),0) as '.$dbt_primary.'_new from '.$dbtable; /* 방금 등록번호 가져오기 */
		$row_max = sql_fetch($sql_max);
		$_ec_no = $row_max[$dbt_primary.'_new'];
	}

	$ecc_field = array();
	array_push($ecc_field, 'ecc_ec_no', 'ecc_name', 'ecc_select_no', 'ecc_weight', 'ecc_limit', 'ecc_is_goods', 'ecc_point', 'ecc_reg_date');

	for($i=0; $i<count($_ecc_select_no); $i++){
		$_ecc_ec_no[$i] = $_ec_no; // 이벤트 번호(ec_no)
		$_ecc_reg_date[$i] = substr(NM_TIME_YMDHIS, 0, 16); // 최초등록일
		/* field명 */
		$sql_ecc_reg = 'INSERT INTO event_christmas_category (';
		foreach($ecc_field as $ecc_field_key => $ecc_field_val){
			/* sql문구 */
			$sql_ecc_reg.= $ecc_field_val.', ';
		}
		$sql_ecc_reg = substr($sql_ecc_reg,0,strrpos($sql_ecc_reg, ","));
		/* VALUES 시작 */
		$sql_ecc_reg.= ' )VALUES( ';

		/* field값 */
		foreach($ecc_field as $ecc_field_key => $ecc_field_val){
			/* sql문구 */
			$sql_ecc_reg.= '"'.${'_'.$ecc_field_val}[$i].'", ';
		}
		$sql_ecc_reg = substr($sql_ecc_reg,0,strrpos($sql_ecc_reg, ","));

		/* SQL문 마무리 */
		$sql_ecc_reg.= ' ) ';
		sql_query($sql_ecc_reg);
	}

	// item insert
	eci_insert();
}

// event_christmas_item
function eci_insert(){
	global $dbtable, $dbt_primary, $_ec_no;

	if($_ec_no == ''){
		$sql_max = 'select COALESCE(max('.$dbt_primary.'),0) as '.$dbt_primary.'_new from '.$dbtable; /* 방금 등록번호 가져오기 */
		$row_max = sql_fetch($sql_max);
		$_ec_no = $row_max[$dbt_primary.'_new'];
	}
	$_eci_reg_date = substr(NM_TIME_YMDHIS, 0, 16); // 최초등록일

	// event_christmas_category
	$sql_ecc = "SELECT * FROM event_christmas_category WHERE ecc_ec_no=".$_ec_no;
	$result_ecc = sql_query($sql_ecc);	
	while ($row_ecc = sql_fetch_array($result_ecc)) {
		if(intval($row_ecc['ecc_limit']) > 0){
			for($i=0; $i<intval($row_ecc['ecc_limit']); $i++){
				$sql_eci = "INSERT INTO event_christmas_item (
				eci_ec_no, eci_ecc_no, eci_ecc_select_no, eci_reg_date
				 )VALUES(
				 '".$row_ecc['ecc_ec_no']."','".$row_ecc['ecc_no']."','".$row_ecc['ecc_select_no']."','".$_eci_reg_date."' )
				 ";
				 sql_query($sql_eci);
			}
		}
	}
}

// event_christmas_category + event_christmas_item
function ec_data_delete(){
	global $dbtable, $dbt_primary, $_ec_no;

	if($_ec_no == ''){
		$sql_max = 'select COALESCE(max('.$dbt_primary.'),0) as '.$dbt_primary.'_new from '.$dbtable; /* 방금 등록번호 가져오기 */
		$row_max = sql_fetch($sql_max);
		$_ec_no = $row_max[$dbt_primary.'_new'];
	}
	$sql_ecc = "delete FROM event_christmas_category WHERE ecc_ec_no=".$_ec_no;
	sql_query($sql_ecc);	
	$sql_eci = "delete FROM event_christmas_item WHERE eci_ec_no=".$_ec_no;
	sql_query($sql_eci);	
}

?>