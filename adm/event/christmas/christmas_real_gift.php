<? include_once '_common.php'; // 공통

$gift_type = array();
$gift_type['y'] = "현물";
$gift_type['m'] = "기프티콘";

// 이벤트 크리스마스 카테고리 -> /lib/event.lib.php
$event_christmas_category = event_christmas_category();

$sql_where_all = $sql_where .= " AND ecc_is_goods!='n' ";
// 실경품, 기프트콘 갯수
$sql_is_goods_item = "
	select sum(if(ecc_is_goods='y',ecc_limit,0))as is_goods_y, sum(if(ecc_is_goods='m',ecc_limit,0))as is_goods_m 
	from event_christmas_category ".$sql_where_all.";
";
$sql_is_goods_gift = "
	select sum(if(ecc_is_goods='y',1,0))as is_goods_y, sum(if(ecc_is_goods='m',1,0))as is_goods_m 
	from event_christmas_gift ".$sql_where_all.";
";
$row_is_goods_item = sql_fetch($sql_is_goods_item);
$row_is_goods_gift = sql_fetch($sql_is_goods_gift);

// 실경품, 기프트콘의 종류별 갯수
$category_item_arr = $category_gift_arr = $category_gift_select_no_arr = array();
$sql_category_gift = "
	select ecc_select_no, count(ecg_no)as category_sum 
	from event_christmas_gift ".$sql_where_all." 
	group by ecc_select_no order by ecc_select_no asc;
";
$sql_category_item = "
	select ecc_select_no, sum(ecc_limit)as category_sum, ecc_is_goods 
	from event_christmas_category ".$sql_where_all." 
	group by ecc_select_no order by ecc_select_no asc;
";

$result_category_gift = sql_query($sql_category_gift);
while ($row_category_gift = mysql_fetch_array($result_category_gift)) {
	array_push($category_gift_arr, $row_category_gift);
}

$result_category_item = sql_query($sql_category_item);
while ($row_category_item = mysql_fetch_array($result_category_item)) {
	$row_category_item['category_sum_gift'] = 0;
	foreach($category_gift_arr as $category_gift_val){
		if($row_category_item['ecc_select_no'] == $category_gift_val['ecc_select_no']){
			$row_category_item['category_sum_gift'] = $category_gift_val['category_sum'];
		}
	}
	array_push($category_item_arr, $row_category_item);
}

// where 추가
$_ecc_is_goods = num_eng_check(tag_get_filter($_REQUEST['ecc_is_goods']));
$_ecc_select_no = num_eng_check(tag_get_filter($_REQUEST['ecc_select_no']));

if($_ecc_is_goods != ''){
	$sql_where .= " AND ecc_is_goods ='".$_ecc_is_goods."' ";
	$_nm_paras.= "&ecc_is_goods=".$_ecc_is_goods;
}

if(intval($_ecc_select_no) > 0){
	$sql_where .= " AND ecc_select_no ='".$_ecc_select_no."' ";
	$_nm_paras.= "&ecc_select_no=".$ecc_select_no;
}

if($_s_text != ''){
	$sql_mb_arr = array();
	$sql_mb = "select * from member where mb_state='y' and  mb_state_sub='y' 
			   and mb_id like '%$_s_text%'  ";
	$result_mb = sql_query($sql_mb);
	while($row_mb = sql_fetch_array($result_mb)) {
		array_push($sql_mb_arr , $row_mb['mb_no']);
	}
	$ecg_member = " AND ecg_member in (0) "; 
	if(count($sql_mb_arr) > 0){
		$sql_where .= " AND ecg_member in (".implode(",",$sql_mb_arr).") ";
	}
}


$sql_group = "";
$sql_order = " ORDER BY ecg_goods_info_date DESC ";

// SQL문
$sql_field		= " * ";
$sql_count		= " count(*) as cnt ";

$sql_table		= "select {$sql_field} from event_christmas_gift ecg LEFT JOIN event_christmas_member ecm ON ecg.ecg_member=ecm.ecm_member ";
$sql_table_cnt	= "select {$sql_count} from event_christmas_gift";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_table_cnt} {$sql_where} {$sql_group}  ";
$sql_query_all	= " {$sql_table_cnt} {$sql_where_all} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

if($_mode == 'excel') {
	$rows_data	= rows_data($sql_querys, false);		// SQL문 결과 레코드값
	include_once $_cms_folder_path.'/excel/christmas_real_gift_excel.php';
	die;
}else{
	// SQL처리
	$rows_cnts	= rows_cnts($sql_query_cnt);	// SQL문 총값
	$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값
	$rows_cnts_all	= rows_cnts($sql_query_all);	// SQL문 총값
}


$th_class = $td_class = "";	// 상단테이블 css-class

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호', 'ecg_no', 0));
array_push($fetch_row, array('아이디', 'mb_id', 0));
array_push($fetch_row, array('선물명', 'ecc_name', 0));
array_push($fetch_row, array('선물타입', 'ecc_is_goods', 0));
array_push($fetch_row, array('개인정보확인 (O:있음, X:없음)<br/>성명,핸드폰,주소,우편', 'ecc_is_goods', 0));
array_push($fetch_row, array('등록일', 'ecg_reg_date', 0));
array_push($fetch_row, array('개인정보-등록일', 'ecg_goods_info_date', 0));

$page_title = "선물상자 실경품 당첨자";
$cms_head_title = $page_title;
/* 여기부터 다시 */
$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<style type="text/css">
	#cms_search .cms_excel .excel_down {
    display: inline-block;
    font-size: 15px;
    font-family: Nanum Gothic,dotum;
    font-weight: bold;
    background: #e86ac0;
    padding: 5px 10px;
    margin-left: 5px;
    color: #fff;
    border-radius: 50px;
    vertical-align: middle;
}
.vertical_top{
	vertical-align: top !important;
}
.text_left{
    text-align: left !important;
}
</style>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>
<script type="text/javascript">
<!--
	function event_christmas_real_search_submit(){

	}
//-->
</script>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$cms_page_title;?></h1>
	<div>
		<strong><?=$page_title;?> : <?=number_format($rows_cnts_all);?></strong>
	</div>
</section>

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="event_christmas_real_search_form" id="event_christmas_real_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return event_christmas_real_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<? tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<? tag_selects($gift_type, "ecc_is_goods", $_ecc_is_goods, 'y', '경품타입', '');?>
					<? $ecc_selected = '';
					if(intval($_ecc_select_no) == 0){ $ecc_selected = ' selected="selected" '; }
					?>
					<select name="ecc_select_no" id="ecc_select_no" class="ecc_select_no">
						<option value="0" <?=$ecc_selected;?>>선물리스트</option>
						<? foreach($event_christmas_category as $ecc_key => $ecc_val){
							$ecc_selected = ''
						?>
							<? if($ecc_val[1] == 'n'){ continue; } ?>
							<? if(intval($_ecc_select_no) == $ecc_key){ $ecc_selected = ' selected="selected" '; } ?>
							<option value="<?=$ecc_key?>" <?=$ecc_selected;?>><?=$ecc_val[0];?></option>
						<? } ?>
					</select>
				</div>
				<div class="cs_search_btn">
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>" placeholder="아이디로 검색">
				</div>
			</div>
			<div class="cs_submit event_christmas_real_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
	<div class="cms_excel">
		<input type="button" class="excel_down" value="검색 결과 엑셀 다운로드" onclick="location.href='<?=$_cms_self;?>?<?=$_nm_paras;?>&mode=excel'"/>
	</div>
</section><!-- cms_search -->

<section id="event_result">
	<h3 id="cr_thead_mg_add">
		<table>
			<tr>
				<th class="topleft" style="vertical-align: middle;">구분</th>
				<th>실물 리스트</th>
				<th>모바일 리스트</th>
				<th class="topright">전체</th>
			</tr>
			<tr>
				<td><strong>선물상자리스트</strong></td>
				<td class='vertical_top'>
					<table>
						<tr>
							<td class="text_left">선물상자명</td>
							<td class="red">설정수</td>
							<td class="blue">당첨수</td>
						</tr>
						<? foreach($category_item_arr as $category_item_key => $category_item_val) { 
							if($category_item_val['ecc_is_goods'] != 'y'){ continue;}
							// 상품명 
							$gift_name_tmp = $event_christmas_category[$category_item_val['ecc_select_no']][0];
							// $gift_name = str_replace("-", "-<br/>", $gift_name_tmp);
							$gift_name = $gift_name_tmp;
						?>
						<tr>
							<td class="text_left"><?=$gift_name;?></td>
							<td class="red"><?=$category_item_val['category_sum'];?></td>
							<td class="blue"><?=$category_item_val['category_sum_gift'];?></td>
						</tr>
						<? } ?>
					</table>
				</td>

				<td class='vertical_top'>
					<table>
						<tr>
							<td class="text_left">선물상자명</td>
							<td class="red">설정수</td>
							<td class="blue">당첨수</td>
						</tr>
						<? foreach($category_item_arr as $category_item_key => $category_item_val) { 
							if($category_item_val['ecc_is_goods'] != 'm'){ continue;}
							// 상품명 
							$gift_name_tmp = $event_christmas_category[$category_item_val['ecc_select_no']][0];
							// $gift_name = str_replace("-", "-<br/>", $gift_name_tmp);
							$gift_name = $gift_name_tmp;
						?>
						<tr>
							<td class="text_left"><?=$gift_name;?></td>
							<td class="red"><?=$category_item_val['category_sum'];?></td>
							<td class="blue"><?=$category_item_val['category_sum_gift'];?></td>
						</tr>
						<? } ?>
					</table>
				</td>
				<td class='vertical_top'>
					<table>
						<tr>
							<td class="text_left">구분</td>
							<td class="red">설정수</td>
							<td class="blue">당첨수</td>
						</tr>
						<? $ecc_is_goods_sum = array();
						$ecc_is_goods_sum['y'] = 0;
						$ecc_is_goods_sum['y_gift'] = 0;
						$ecc_is_goods_sum['m'] = 0;
						$ecc_is_goods_sum['m_gift'] = 0;
						foreach($category_item_arr as $category_item_key => $category_item_val) { 
							if($category_item_val['ecc_is_goods'] == 'y'){
								$ecc_is_goods_sum['y']+=intval($category_item_val['category_sum']);
								$ecc_is_goods_sum['y_gift']+=intval($category_item_val['category_sum_gift']);
							}
							if($category_item_val['ecc_is_goods'] == 'm'){
								$ecc_is_goods_sum['m']+=intval($category_item_val['category_sum']);
								$ecc_is_goods_sum['m_gift']+=intval($category_item_val['category_sum_gift']);
							}
						}
						?>
						<tr>
							<td class="text_left">실물 리스트</td>
							<td class="red"><?=$ecc_is_goods_sum['y'];?></td>
							<td class="blue"><?=$ecc_is_goods_sum['y_gift'];?></td>
						</tr>
						<tr>
							<td class="text_left">모바일 리스트</td>
							<td class="red"><?=$ecc_is_goods_sum['m'];?></td>
							<td class="blue"><?=$ecc_is_goods_sum['m_gift'];?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br/>
		<strong>검색 결과 수 : <?=number_format($rows_cnts);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){ $th_title_giho = $order_giho; }
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<? 
				$ecm_field = array('ecm_name','ecm_tel','ecm_address','ecm_postal');
				$ecm_field_kr = array('성명','핸드폰','주소','우편');

				foreach($rows_data as $dvalue_key => $dvalue_val){ 
				$db_mb = mb_get_no($dvalue_val['ecg_member']);
				$db_gift_name = $event_christmas_category[$dvalue_val['ecc_select_no']][0];

				$ecm_info = array();
				foreach($ecm_field as $ecm_key => $ecm_name){
					$ecm_info[$ecm_name] = $ecm_field_kr[$ecm_key];
					if($dvalue_val[$ecm_name] !=''){
						$ecm_info[$ecm_name].= ":O";
					}else{
						$ecm_info[$ecm_name].= ":X";
					}
				}
				$ecm_info_text = implode(", ",$ecm_info);



				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['ecg_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center">
						<a href="#" onclick="popup('<?=NM_ADM_URL?>/members/member_view.php?mb_no=<?=$db_mb['mb_no'];?>','member_view', <?=$popup_cms_width;?>, 550);"><?=$db_mb['mb_id'];?></a>
					</td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$db_gift_name;?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$gift_type[$dvalue_val['ecc_is_goods']];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$ecm_info_text;?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$dvalue_val['ecg_reg_date'];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$dvalue_val['ecg_goods_info_date'];?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>
<? rows_page(); ?>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>