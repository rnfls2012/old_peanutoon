<?
/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','ecg_no'));
array_push($fetch_row, array('회원번호','ecg_member'));
array_push($fetch_row, array('회원아이디','ecg_member_id'));
array_push($fetch_row, array('이벤트번호','ec_no'));
array_push($fetch_row, array('이벤트카테고리','ecc_no'));
array_push($fetch_row, array('이벤트경품번호','eci_no'));
array_push($fetch_row, array('경품명','ecc_select_no'));
array_push($fetch_row, array('경품취득날짜','ecg_reg_date'));
array_push($fetch_row, array('개인정보-성명','ecm_name'));
array_push($fetch_row, array('개인정보-핸드폰','ecm_tel'));
array_push($fetch_row, array('개인정보-주소','ecm_address'));
array_push($fetch_row, array('개인정보-우편번호','ecm_postal'));
array_push($fetch_row, array('개인정보&동의 등록일','ecg_goods_info_date'));

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 해당 시트 당 컬럼 크기 변경

$worksheet->set_column(0, 1, 10);
$worksheet->set_column(1, 6, 15);

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));

$center  = $workbook -> addformat(array(
								'align' => 'center',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));

//문서 출력일자 셀 출력
$worksheet->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt = 2; //합계 데이터 출력 행 cnt.

/***************  본문 출력   **************/

/**************  필터링 값 출력 ***************/

//검색 날짜 별 결과 항목 열 지정.
$search_keyword = array();
array_push($search_keyword, "검색 조건");
$search_text = "";

//검색 결과
if($_ecc_is_goods != ''){
	$search_text.= $gift_type[$_ecc_is_goods].", ";
}
if(intval($_ecc_select_no) > 0){
	$search_text.= $event_christmas_category[$_ecc_select_no][0].", ";
}
if($_s_text != ''){
	$search_text.= "아이디(text)단어:".$_s_text.", ";
}
$search_text = substr($search_text,0,strrpos($search_text, ","));

$search_result = array();
array_push($search_result, $search_text);

//시작 날짜, 끝 날짜 하나라도 있을 경우
if ($_s_date || $_e_date) {
 array_push($search_keyword, "검색 날짜");
 array_push($search_result, $_s_date."~".$_e_date);
}

//각 필터링 항목 별 count
$sk_cnt = count($search_keyword);
$sr_cnt = count($search_result);

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
} // end foreach

//필터링 항목 출력
for ($i=1; $i <= $sk_cnt; $i++) {
	$worksheet->write($i+$row_cnt, 0 , iconv_cp949($search_keyword[$i-1]), $heading);
}

//필터링 결과 출력
for($i=1; $i<=$sr_cnt; $i++){
	$worksheet->write($i+$row_cnt, 1, iconv_cp949($search_result[$i-1]), $right);
}

$row_cnt+=2;

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write($sk_cnt+$row_cnt, $col++, $cell, $heading);
} // end foreach

$row_cnt++;

/* 데이터 전달 부분 */

$i=$row_cnt+$sk_cnt;

foreach ($rows_data as $dvalue_key => $dvalue_val) {
	foreach ($data_key as $cell_key => $cell_val) {
		
		$get_comics = get_comics($dvalue_val['sl_comics']);

		switch($cell_val) {
			case "ecg_member_id" :
				$worksheet->write($i, $cell_key , iconv_cp949($nm_config['cf_small'][$get_comics['cm_small']]), $center);
				break;

			case "cm_end" :
				$worksheet->write($i, $cell_key , iconv_cp949($d_cm_end[$get_comics['cm_end']]), $center);
				break;

			case "sl_big" : 
				$worksheet->write($i, $cell_key , iconv_cp949($big_arr[$get_comics['cm_big']]), $center);
				break;

			default :
				$worksheet->write($i, $cell_key , iconv_cp949($dvalue_val[$cell_val]), $f_price);
				break;
		} // end switch

	} // end foreach
	$i++;
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."christmas_real_gift_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"christmas_real_gift_".$today.".xls");
header("Content-Disposition: inline; filename=\"christmas_real_gift_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
?>