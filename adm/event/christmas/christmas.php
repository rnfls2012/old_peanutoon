<? include_once '_common.php'; // 공통

if($_s_limit == ''){ $_s_limit = 10; }

$ecc_bool_txt = array();
$ecc_bool_txt['error'] = "에러";
$ecc_bool_txt['unable'] = "수정불가";
$ecc_bool_txt['enable'] = "수정가능";

$sql_order = "order by ec_date DESC";

// SQL문
$sql_field		= " * ";
$sql_count		= " count(*) as cnt ";

$sql_table		= "select {$sql_field} from event_christmas";
$sql_table_cnt	= "select {$sql_count} from event_christmas";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_table_cnt} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts	= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값

$total_event = total_event_total(); // 이벤트 총수 - function 선언 하단

$th_class = $td_class = "";	// 상단테이블 css-class

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('이벤트 번호', 'ec_no', 0));
array_push($fetch_row, array('이벤트 명', 'ec_name', 0));
array_push($fetch_row, array('이벤트 기간', 'ec_date', 0));
array_push($fetch_row, array('이벤트 상태', 'ec_state', 0));
array_push($fetch_row, array('선물상자 리스트', 'ec_list', 0));
array_push($fetch_row, array('등록일', 'ec_reg_date', 0));
array_push($fetch_row, array('수정일', 'ec_mod_date', 0));
array_push($fetch_row, array('관리', 'ec_management', 0));

$page_title = "크리스마스 선물상자 설정";
$cms_head_title = $page_title;
/* 여기부터 다시 */
$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>
<style type="text/css">
	.btn {
		display: inline-block;
		font-size: 15px;
		font-family: Nanum Gothic,dotum;
		font-weight: bold;
		background: #41cf84;
		padding: 5px 10px;
		margin-left: 5px;
		color: #fff;
		border-radius: 50px;
		vertical-align: middle;
	}
	.del_btn{
		background: #af70e2;
	}
	.gift_list{

	}
	.gift_list th{
		border-bottom: 0px solid #fff !important;
		text-align:left;
	}
	.gift_list td{
		vertical-align: text-top; border-bottom:0px solid #fff !important;
	}
	
</style>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$cms_page_title;?></h1>
	<div>
		<strong><?=$page_title;?> : <?=number_format($total_event);?></strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_folder_write;?>','event_christmas_write', <?=$popup_cms_width;?>, 850);"><?=$page_title;?> 등록</button>
	</div>
</section>

<section id="event_result">
	<strong>크리스마스 적용 설명</strong>
	<ul>
		<li>1.이벤트 날짜는 같은 날짜(날짜중복) 안됩니다.</li>
		<li>2.이벤트 날짜에 회원이 1명이라도 서비스 받았다면 수정&삭제 불가 합니다.</li>	
		<li>3.선물 상자 리스트 설정는 운영파트에 받는 선물 리스트로 고정으로 했습니다.(수정시 문의 바람)</li>	
	</ul>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){ $th_title_giho = $order_giho; }
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){ 
					$popup_url = $_cms_write."?ec_no=".$dvalue_val['ec_no'];

					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					// 선물상자 리스트
					$ecc_arr = array();
					$ecc_list = "";
					$sql_ecc = "SELECT * FROM event_christmas_category WHERE ecc_ec_no=".$dvalue_val['ec_no'];
					$result_ecc = sql_query($sql_ecc);

					$goods_y_arr = array();
					$goods_m_arr = array();
					$goods_n_arr = array();

					while ($row_ecc = sql_fetch_array($result_ecc)) {
						if($row_ecc['ecc_is_goods'] == 'y'){
							array_push($goods_y_arr, $row_ecc['ecc_name']."(".$row_ecc['ecc_limit']."개)");
						}else if($row_ecc['ecc_is_goods'] == 'm'){
							array_push($goods_m_arr, $row_ecc['ecc_name']."(".$row_ecc['ecc_limit']."개)");
						}else{
							array_push($goods_n_arr, $row_ecc['ecc_name']."(".$row_ecc['ecc_limit']."개)");
						}
					}
					$ecc_list = implode(", ",$ecc_arr);

					$ecc_bool = event_christmas_ctrk_bool($dvalue_val['ec_no']);
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['ec_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['ec_name'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['ec_date'];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['ec_state'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center">
						<table class="gift_list">
							<tr>
								<th>경품 리스트</th>
								<th>기프티콘 리스트</th>
								<th>미니땅콩 리스트</th>
							</tr>
							<tr>
								<td><!-- 실물 리스트-->
									<ul>
										<? foreach($goods_y_arr as $goods_y_val){ ?>
										<li><?=$goods_y_val;?></li>
										<? } ?>
									</ul>
								</td>
								<td><!-- 모바일 리스트-->
									<ul>
										<? foreach($goods_m_arr as $goods_m_val){ ?>
										<li><?=$goods_m_val;?></li>
										<? } ?>
									</ul>
								</td>
								<td><!-- 미니땅콩 리스트-->
									<ul>
										<? foreach($goods_n_arr as $goods_n_val){ ?>
										<li><?=$goods_n_val;?></li>
										<? } ?>
									</ul>
								</td>
							</tr>
						</table>
					</td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$dvalue_val['ec_reg_date'];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$dvalue_val['ec_mod_date'];?></td>
					<td class="<?=$fetch_row[7][1]?> text_center">
						<? if($ecc_bool == "enable"){ ?>
							<button class="btn" onclick="popup('<?=$popup_mod_url;?>','event_random_mod_wirte', <?=$popup_cms_width;?>, 850);">수정</button>
							<button class="btn del_btn" onclick="popup('<?=$popup_del_url;?>','event_random_del_wirte', <?=$popup_cms_width;?>, 850);">삭제</button>
						<?}else{?>
							<?=$ecc_bool_txt[$ecc_bool];?>
						<?}?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>
<? rows_page(); ?>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>

<?	/* 해당 페이지에서 사용하는 function */
// 이벤트 총수
function total_event_total()
{
	$sql_event_total = "select count(*) as total from event_christmas ";
	$total_event_total = sql_count($sql_event_total, 'total');
	return $total_event_total; 
}


?>