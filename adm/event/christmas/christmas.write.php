<?
include_once '_common.php'; // 공통

// 이벤트 크리스마스 카테고리 -> /lib/event.lib.php
$event_christmas_category = event_christmas_category();

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_ec_no = tag_filter($_REQUEST['ec_no']);

$sql = "SELECT * FROM event_christmas WHERE ec_no = '$_ec_no' ";

/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
			 $sql_list = "";
	break;
}

// 선물상자 리스트
$row_ecc_arr = array();
$ecc_select_no_arr = array();
/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
	$sql_ecc = "SELECT * FROM event_christmas_category WHERE ecc_ec_no=".$_ec_no;
	$result_ecc = sql_query($sql_ecc);
	while ($row_ecc = sql_fetch_array($result_ecc)) {
		array_push($row_ecc_arr, $row_ecc);
		array_push($ecc_select_no_arr, $row_ecc['ecc_select_no']);
	}
}

/* 대분류로 제목 */
$page_title = "크리스마스 선물상자";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<style type="text/css">
	#event_write table td input[type=text].loss_ctrl{ width: 20%; }
	.mgt5{ margin-top: 5px; }
	.catogory_name{ display:inline-block; width: 100px !important; }
	.catogory_item{ width: 50px !important; }
	.del_btn {
		display: inline-block;
		font-size: 15px;
		font-family: Nanum Gothic,dotum;
		font-weight: bold;
		background: #af70e2;
		padding: 5px 10px;
		margin-left: 5px;
		color: #fff;
		border-radius: 50px;
		vertical-align: middle;
	}
</style>
<script type="text/javascript">
<!--
	$(function(){
		ec_loss_weight();

		$('#ecc').change(function() {
			$selected = $(this).find('option:selected');
			var ecc_select_no = $selected.val();
			var ecc_name = $selected.text();
			var ecc_is_goods = $selected.attr('data-is_goods');
			var ecc_point = $selected.attr('data-point');
			var html_arr = new Array();
			html_arr.push('<li class="mgt5" id="ecc_selct_no_'+ecc_select_no+'">');
			html_arr.push('	<input type="hidden" name="ecc_select_no[]" value="'+ecc_select_no+'"/><!-- 카테고리 번호 -->');
			html_arr.push('	<input type="hidden" name="ecc_name[]" value="'+ecc_name+'"/><!-- 선물명 -->');
			html_arr.push('	<input type="hidden" name="ecc_is_goods[]" value="'+ecc_is_goods+'"/><!-- 현물유무NY -->');
			html_arr.push('	<input type="hidden" name="ecc_point[]" value="'+ecc_point+'"/><!-- 미니땅콩 -->');
			html_arr.push('	<label class="catogory_name" for="ecc_name">'+ecc_name+'</label>');
			html_arr.push('	<label for="ec_loss_percent">=&gt;일일 개수 : </label>');
			html_arr.push('	<input type="text" class="catogory_item" placeholder="숫자만" name="ecc_limit[]" id="ecc_limit" value="" autocomplete="off" />');
			html_arr.push('	/ ');
			html_arr.push('	<label for="ec_loss_percent">확률(가중치값) : </label>');
			html_arr.push('	<input type="text" class="catogory_item" placeholder="숫자만" name="ecc_weight[]" id="ecc_weight" value="" autocomplete="off" />');
			html_arr.push('<a class="del_btn" onclick="ecc_selct_delete(\'ecc_selct_no_'+ecc_select_no+'\');">삭제</a>');
			html_arr.push('</li>');
			$('#ecc_list').append(html_arr.join("\n"));
			$selected.remove();
			ec_loss_weight();
		});
		
		$("#ecc_list input[name='ecc_weight[]']").change(function() {
			ec_loss_weight();
		});
		
		$("#ec_loss_percent").change(function() {
			ec_loss_weight();
		});
	});


	function ecc_selct_delete(ecc_selct_no_id){
		$ecc_selct_no = $('#'+ecc_selct_no_id);

		var ecc_selct_no = $ecc_selct_no.find("input[name='ecc_select_no[]']").val();
		var ecc_name = $ecc_selct_no.find("input[name='ecc_name[]']").val();
		var ecc_is_goods = $ecc_selct_no.find("input[name='ecc_is_goods[]']").val();
		var ecc_point = $ecc_selct_no.find("input[name='ecc_point[]']").val();
		
		var html_arr = new Array();
		html_arr.push('<option value="'+ecc_selct_no+'" data-is_goods="'+ecc_is_goods+'" data-point="'+ecc_point+'">'+ecc_name+'</option>');
		$('#ecc').append(html_arr.join("\n"));
		$ecc_selct_no.remove();
		ec_loss_weight();
	}

	function ec_loss_weight(){
		var all_weight_total = 0;
		var ecc_weight_total = 0;
		var ec_loss_weight = 0;
		var ec_loss_percent = Number($('#ec_loss_percent').val());
		var ecc_weight_percent = ec_loss_weight_percent = 0;

		$("#ecc_list input[name='ecc_weight[]']").each(function( index ) {
			ecc_weight_total+= Number($(this).val());
		});

		$('#ec_loss_weight_calc').text("선물상자 리스트 설정의 모든 확률 리스트 합계 : "+ecc_weight_total);
		ec_loss_weight = Math.round(ecc_weight_total / 100 * ec_loss_percent) * 2;

		$('#ec_loss_weight').html(" // 꽝-가중치값 : "+ec_loss_weight);		

		if(ecc_weight_total > 0){
			all_weight_total = ecc_weight_total + ec_loss_weight; // 합계
			ecc_weight_percent =ecc_weight_total / all_weight_total * 100;
			ec_loss_weight_percent    = ec_loss_weight / all_weight_total * 100;
			}
		/*
		console.log(ecc_weight_total);
		console.log(ec_loss_weight);
		console.log(ecc_weight_percent);
		console.log(ec_loss_weight_percent);
		*/
		$("#weight_percent").text("리스트="+ecc_weight_percent.toFixed(1)+"% : 꽝="+ec_loss_weight_percent.toFixed(1)+"%");
	}
	
	function event_christmas_write_submit(){
	}
//-->
</script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- event_free_dc_head -->

<section id="event_write">
	<form name="event_christmas_write_form" id="event_christmas_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return event_christmas_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="ec_no" name="ec_no" value="<?=$_ec_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ec_no">이벤트 번호</label></th>
					<td><?=$_ec_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="ec_name"><?=$required_arr[0];?>이벤트 명</label></th>
					<td>
						<input type="text" placeholder="이벤트 명을 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="ec_name" id="ec_name" value="<?=$row['ec_name'];?>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="ec_category_list"><?=$required_arr[0];?>선물 상자 리스트 설정</label></th>
					<td>
						<div>
							<select id="ecc">
								<? print_r($ecc_no_arr); ?>
								<option value="0">선택하세요</option>
								<? foreach($event_christmas_category as $ecc_key => $ecc_val){ 
									if(count($ecc_select_no_arr) > 0){
										if(in_array($ecc_key, $ecc_select_no_arr)){ continue; }
									}
								?>
								<option value="<?=$ecc_key?>" data-is_goods="<?=$ecc_val[1]?>" data-point="<?=$ecc_val[2]?>"><?=$ecc_val[0]?></option>
								<?}?>
							</select>
							<ul id="ecc_list">
								<? 
								if(count($row_ecc_arr) > 0){
									foreach($row_ecc_arr as $row_ecc_key => $row_ecc_val){ ?>
									<li class="mgt5" id="ecc_selct_no_<?=$row_ecc_val['ecc_select_no']?>">
										<input type="hidden" name="ecc_select_no[]" value="<?=$row_ecc_val['ecc_select_no']?>"><!-- 카테고리 번호 -->
										<input type="hidden" name="ecc_name[]" value="<?=$row_ecc_val['ecc_name']?>"><!-- 선물명 -->
										<input type="hidden" name="ecc_is_goods[]" value="<?=$row_ecc_val['ecc_is_goods']?>"><!-- 현물유무NY -->
										<input type="hidden" name="ecc_point[]" value="<?=$row_ecc_val['ecc_point']?>"><!-- 미니땅콩 -->
										<label class="catogory_name" for="ecc_name"><?=$row_ecc_val['ecc_name']?> </label>
										<label for="ec_loss_percent">=&gt;일일 개수 : </label>
										<input type="text" class="catogory_item" placeholder="숫자만" name="ecc_limit[]" id="ecc_limit" value="<?=$row_ecc_val['ecc_limit']?>" autocomplete="off">
										/ 
										<label for="ec_loss_percent">확률(가중치값) : </label>
										<input type="text" class="catogory_item" placeholder="숫자만" name="ecc_weight[]" id="ecc_weight" value="<?=$row_ecc_val['ecc_weight']?>" autocomplete="off">
										<a class="del_btn" onclick="ecc_selct_delete('ecc_selct_no_<?=$row_ecc_val['ecc_select_no']?>');">삭제</a>
									</li>

								<?
									}
								}
								?>
							</ul>
							<p class="mgt5">확률(가중치)는 높을수록 잘 뽑입니다.</p>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="ec_loss"><?=$required_arr[0];?>선물 상자 꽝 설정</label></th>
					<td>
						<div>
							<span for="ec_loss">미니땅콩 : </span>
							<input type="text" class="catogory_item" placeholder="숫자만" name="ec_loss" id="ec_loss" value="<?=$row['ec_loss'];?>" autocomplete="off" /> / 
							<span for="ec_loss_percent">확률(가중치%) : </span>
							<input type="text" class="catogory_item" placeholder="숫자만" name="ec_loss_percent" id="ec_loss_percent" value="<?=$row['ec_loss_percent'];?>" autocomplete="off" /> %
							<p class="mgt5">
								참조 : 선물상자 리스트 설정의 모든 확률(가중치값) 리스트 합계의 % 계산 (소수점 반올림)<br/>
								<span id="ec_loss_weight_calc"></span><span id="ec_loss_weight"></span><br/>
								<strong id="weight_percent"></strong>
							</p>
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>이벤트 날짜</label></th>
					<td>
						<div class="ec_available_date_type_view">
							<span for="s_date">D-Day :</span>
							<input type="text" class="s_date readonly" placeholder="클릭하세요" name="ec_date" id="s_date" value="<?=$row['ec_date'];?>" autocomplete="off" readonly />
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>사용 여부</label></th>
					<td>
						<div id="ec_state" class="input_btn">
							<input type="radio" name="ec_state" id="ec_statey" value="y" <?=get_checked('', $row['ec_state']);?> <?=get_checked('y', $row['ec_state']);?> />
							<label for="ec_statey">사용</label>
							<input type="radio" name="ec_state" id="ec_staten" value="n" <?=get_checked('n', $row['ec_state']);?> />
							<label for="ec_staten">미사용</label>
						</div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ec_reg_date">등록일</label></th>
					<td><?=$row['ec_reg_date'];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ec_mod_date">수정일</label></th>
					<td><?=$row['ec_mod_date'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- event_free_dc_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>