<?
include_once '_common.php'; // 공통

$month_date = array(); // 월,일 저장 배열
for($i=1; $i<=12; $i++) {
	$date = date("t", mktime(0, 0, 0, $i, 1, NM_TIME_Y));
	array_push($month_date, array("month"=>$i, "day"=>$date));
} // end for

/* db정보 가져오기 */
$db_ea_arr = array(); /* 월 */
$result_ea = sql_query("SELECT * FROM event_attend2 order by ea_month ");
while ($row_ea = sql_fetch_array($result_ea)) {
	array_push($db_ea_arr,  $row_ea);
} // end while

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('월','ea_month',0));
array_push($fetch_row, array('사용여부','ea_use',0));
array_push($fetch_row, array('시작일','ea_start_date',0));
array_push($fetch_row, array('마감일','ea_end_date',0));
array_push($fetch_row, array('1일출석 지급','ea_oneday_point',0));
array_push($fetch_row, array('연속출석 일수/지급','ea_continue',0));
array_push($fetch_row, array('연속출석 사용','ea_cont_use',0));
array_push($fetch_row, array('개근일','ea_regu_date',0));
array_push($fetch_row, array('개근 지급','ea_regu_point',0));
array_push($fetch_row, array('개근 사용','ea_regu_use',0));

$page_title = "출석이벤트";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
</section>

<section id="event_result">
	<h3> 
		<strong>출석이벤트 적용 설명</strong>
		<ul>
			<li>사용 체크하시면 해당 월에만 적용이 됩니다.</li>
			<li>출석일시 서비스지급은 <?=$nm_config['cf_point_unit_ko']?>과 <?=$nm_config['cf_cash_point_unit_ko']?> 지급할 액수만 적어주시면 되며, 미입력시 지급되지 않습니다.</li>
		</ul>
	</h3>
	<form name="attent_form" id="attend_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return event_att_submit();">
		<div id="cr_bg">
			<table class="attend_table">
				<thead id="cr_thead">
					<tr>
					<?
					//정렬표기
					$order_giho = "▼";
					$th_order = "desc";
					if($_order == 'desc'){ 
						$order_giho = "▲"; 
						$th_order = "asc";
					}
					foreach($fetch_row as $fetch_key => $fetch_val){

						$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
						if($fetch_val[2] == 1){
							$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order'.$th_order.'">';
							$th_ahref_e = '</a>';
						}
						if($fetch_val[1] == $_order_field){
							$th_title_giho = $order_giho;
						}
						$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
						$th_class = $fetch_val[1];
					?>
						<th class="<?=$th_class?>"><?=$th_title?></th>
					<?}?>
					</tr>
				</thead>
				<tbody>
				<? foreach($month_date as $attend_key => $attend_val) {	?>
					<tr class="result_hover">
						<td class="ea_month text_center">
							<label><?=$attend_val["month"];?>월</label>
							<input type="hidden" name="ea_month_<?=$attend_val["month"];?>" id="ea_month_<?=$attend_val["month"];?>" value="<?=$attend_val["month"];?>" />
						</td>
						<td class="ea_use text_center">
							<input type="checkbox" name="ea_use_<?=$attend_val["month"];?>" id="ea_use_<?=$attend_val["month"];?>" value="y" <?=get_checked('y', $db_ea_arr[$attend_key]['ea_use']);?>>
							<label for="ea_use_<?=$attend_val["month"];?>">사용</label>
						</td>
						<td class="ea_start_date text_center">
							<select id="ea_start_date_<?=$attend_val["month"];?>" name="ea_start_date_<?=$attend_val["month"];?>" class="ea_start_date_select" data-month="<?=$attend_val["month"];?>">
								<? for($i=1; $i<=$attend_val["day"]; $i++) { 
									$selected = "asdf";
									if($i == $db_ea_arr[$attend_key]["ea_start_date"]) { $selected = 'selected="selected"'; } ?>
								<option value="<?=$i;?>" <?=$selected;?>><?=$i;?></option>
								<? } // end for ?>
							</select>
						</td>
						<td class="ea_end_date text_center">
							<select id="ea_end_date_<?=$attend_val["month"];?>" name="ea_end_date_<?=$attend_val["month"];?>" class="ea_end_date_select" data-month="<?=$attend_val["month"];?>">
								<? for($i=1; $i<=$attend_val["day"]; $i++) { 
									$selected = "";
									if($i == $db_ea_arr[$attend_key]["ea_end_date"]) { $selected = 'selected="selected"'; } ?>
								<option value="<?=$i;?>" <?=$selected;?>><?=$i;?></option>
								<? } // end for ?>
							</select>
						</td>
						<td class="ea_oneday_point text_center">
							<input class="onlynumber" type="text" placeholder="지급 <?=$nm_config['cf_point_unit_ko']?>" name="ea_oneday_point_<?=$attend_val["month"];?>" id="ea_oneday_point_<?=$attend_val["month"];?>" value="<?=$db_ea_arr[$attend_key]['ea_oneday_point']?>"/>
						</td>
						<td class="ea_continue text_center">
							<?  for($i=1; $i<=3; $i++) { ?>
							<div class="ea_continue_<?=$i;?>">
								<input class="onlynumber" type="text" placeholder="연속출석 일수" name="ea_cont_date_<?=$i;?>_<?=$attend_val["month"];?>" id="ea_cont_date_<?=$i?>_<?=$attend_val["month"];?>" value="<?=$db_ea_arr[$attend_key]['ea_cont_date_'.$i]?>"/> / <input class="onlynumber" type="text" placeholder="지급 <?=$nm_config['cf_point_unit_ko'];?>" name="ea_cont_point_<?=$i;?>_<?=$attend_val["month"];?>" id="ea_cont_point_<?=$i;?>_<?=$attend_val["month"];?>" value="<?=$db_ea_arr[$attend_key]['ea_cont_point_'.$i]?>"/>
							</div>
							<? } // end for ?>
						</td>
						<td class="ea_cont_use text_center">
							<input type="checkbox" name="ea_cont_use_<?=$attend_val["month"];?>" id="ea_cont_use_<?=$attend_val["month"];?>" class="ea_cont_date_check" value="y" data-month="<?=$attend_val["month"];?>" <?=get_checked('y', $db_ea_arr[$attend_key]['ea_cont_use']);?>>
							<label for="ea_cont_use_<?=$attend_val["month"];?>">사용</label>
						</td>
						<td class="ea_regu_date text_center">
							<input type="text" name="ea_regu_date_<?=$attend_val["month"];?>" id="ea_regu_date_<?=$attend_val["month"];?>" class="ea_regu_date_check" data-month="<?=$attend_val["month"];?>" value="<?=$db_ea_arr[$attend_key]['ea_regu_date']?$db_ea_arr[$attend_key]['ea_regu_date']:"1";?>" readonly="readonly"/>
						</td>
						<td class="ea_regu_point text_center">
							<input class="onlynumber" type="text" placeholder="지급 <?=$nm_config['cf_point_unit_ko']?>" name="ea_regu_point_<?=$attend_val["month"];?>" id="ea_regu_point_<?=$attend_val["month"];?>" value="<?=$db_ea_arr[$attend_key]['ea_regu_point'];?>"/>
						</td>
						<td class="ea_regu_use text_center">
							<input type="checkbox" name="ea_regu_use_<?=$attend_val["month"];?>" id="ea_regu_use_<?=$attend_val["month"];?>" value="y" <?=get_checked('y', $db_ea_arr[$attend_key]['ea_regu_use']);?>>
							<label for="ea_regu_use_<?=$attend_val["month"];?>">사용</label>
						</td>
					</tr>
				<? } // end foreach ?>
				</tbody>
			</table>
		</div>
		<div id="event_att_submit">
			<input type="submit" value="출석 이벤트 적용" />
		</div>
	</form>
</section><!-- partner_result -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>