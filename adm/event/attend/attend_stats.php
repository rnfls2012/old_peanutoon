<?
include_once '_common.php'; // 공통

/* PARAMITER 
maker_name // 제공사이름
*/


/* 데이터 가져오기 */
$where_stats_event_attend = ""; /* 충전 */

if($_s_date == ''){ $_s_date = NM_TIME_MON_01; }
if($_e_date == ''){ $_e_date = NM_TIME_YMD; }

// 파트너일 경우
$_e_date		= mb_partner_e_date($nm_member, $_e_date);	// 종료일 현재 -1일 적용

if($_s_date && $_e_date){
	$where_stats_event_attend.= date_year_month($_s_date, $_e_date, 'sea');
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "sea_year_month, sea_day"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
// 정렬시
$_order_field_add = $_order_add = "";
// 요일 정렬시
if($_order_field == "sea_week"){$_order_field_add = " , sea_date "; $_order_add = "desc ";}
// 날짜 정렬시
if($_order_field == "sea_year_month"){$_order_field_add = " , sea_date "; $_order_add = $_order;}
$order_stats_event_attend = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

$sql_stats_event_attend = "";
$field_stats_event_attend = "  sea_year_month, sea_day, sea_week, 
																sum(sea_oneday_point)as sea_oneday_point_cnt, 
																sum(sea_cont_point_1)as sea_cont_point_1_cnt, 
																sum(sea_cont_point_2)as sea_cont_point_2_cnt, 
																sum(sea_cont_point_3)as sea_cont_point_3_cnt, 
																sum(sea_regu_point)as sea_regu_point_cnt "; // 가져올 필드 정하기
$group_stats_event_attend = " group by sea_year_month, sea_day ";
$limit_stats_event_attend = "";

$sql_stats_event_attend = "select $field_stats_event_attend FROM stats_event_attend where 1 $where_stats_event_attend $group_stats_event_attend $order_stats_event_attend $limit_stats_event_attend";
$result = sql_query($sql_stats_event_attend);
$row_size = sql_num_rows($result);

if($_mode == 'excel') {
	include_once $_cms_folder_top_path.'/excel/day_buy_excel.php';
} // end if

/* 검색 전체 구하기 - group by 빼고 sum, count함수 이용 */
$sql_stats_event_attend_s_all = "select $field_stats_event_attend FROM stats_event_attend where 1 $where_stats_event_attend $order_stats_event_attend $limit_stats_event_attend";
$sql_stats_event_attend_s_all_row = sql_fetch($sql_stats_event_attend_s_all );

$s_all_sea_oneday_point_cnt = $sql_stats_event_attend_s_all_row['sea_oneday_point_cnt'];
$s_all_sea_oneday_point_cnt_text = number_format($s_all_sea_oneday_point_cnt)." 건";
$s_all_sea_cont_point_1_cnt = $sql_stats_event_attend_s_all_row['sea_cont_point_1_cnt'];
$s_all_sea_cont_point_1_cnt_text = number_format($s_all_sea_cont_point_1_cnt)." 건";
$s_all_sea_cont_point_2_cnt = $sql_stats_event_attend_s_all_row['sea_cont_point_2_cnt'];
$s_all_sea_cont_point_2_cnt_text = number_format($s_all_sea_cont_point_2_cnt)." 건";
$s_all_sea_cont_point_3_cnt = $sql_stats_event_attend_s_all_row['sea_cont_point_3_cnt'];
$s_all_sea_cont_point_3_cnt_text = number_format($s_all_sea_cont_point_3_cnt)." 건";
$s_all_sea_regu_point_cnt = $sql_stats_event_attend_s_all_row['sea_regu_point_cnt'];
$s_all_sea_regu_point_cnt_text = number_format($s_all_sea_regu_point_cnt)." 건";


/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','sea_year_month',0));
array_push($fetch_row, array('요일','sea_week',0));
array_push($fetch_row, array('1일 출석 건수','sea_oneday_point',0));
array_push($fetch_row, array('연속출석1 건수','sea_cont_point_1',0));
array_push($fetch_row, array('연속출석2 건수','sea_cont_point_2',0));
array_push($fetch_row, array('연속출석3 건수','sea_cont_point_3',0));
array_push($fetch_row, array('개근 건수','sea_regu_point',0));

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "출석이벤트 통계";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()

?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
	<!--
	<div class="write">
		<input type="button" class="excel_down" value="검색 결과 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?s_date=<?=$_s_date;?>&e_date=<?=$_e_date;?>&order_field=<?=$_order_field;?>&order=<?=$_order;?>&mode=excel'"/>
	</div>
	-->
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<!-- <input type="text" id="s_text" name="s_text" value="<?=$_s_text?>"> -->
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="collaboration_result">
	<h3>검색 리스트 
		<table>
			<tr>
				<th class="topleft">1일 출석</th>
				<th>연속 출석1</th>
				<th>연속 출석2</th>
				<th>연속 출석3</th>
				<th class="topright">개근</th>
			</tr>
			<tr>
				<td class="btmleft"><?=$s_all_sea_oneday_point_cnt_text;?></td>
				<td><?=$s_all_sea_cont_point_1_cnt_text;?></td>
				<td><?=$s_all_sea_cont_point_2_cnt_text;?></td>
				<td><?=$s_all_sea_cont_point_3_cnt_text;?></td>
				<td class="btmright"><?=$s_all_sea_regu_point_cnt_text;?></td>
			</tr>
		</table>
	</h3>
	
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 날짜 요일 결제-금액 결제-건수 결제-재구매건[율] */
					$sea_year_month_day = $dvalue_val['sea_year_month']."-".$dvalue_val['sea_day'];
					$sea_week = get_yoil($dvalue_val['sea_week'],1,1); 
					$sea_oneday_point_cnt = number_format($dvalue_val['sea_oneday_point_cnt'])." 건";
					$sea_cont_point_1_cnt = number_format($dvalue_val['sea_cont_point_1_cnt'])." 건";
					$sea_cont_point_2_cnt = number_format($dvalue_val['sea_cont_point_2_cnt'])." 건";
					$sea_cont_point_3_cnt = number_format($dvalue_val['sea_cont_point_3_cnt'])." 건";
					$sea_regu_point_cnt = number_format($dvalue_val['sea_regu_point_cnt'])." 건";
					/* 요일별 class */
					$week_class = $week_en[$dvalue_val['sea_week']];
				?>
				<tr class="<?=$week_class;?> result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$sea_year_month_day;?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$sea_week;?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$sea_oneday_point_cnt;?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$sea_cont_point_1_cnt;?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$sea_cont_point_2_cnt;?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$sea_cont_point_3_cnt;?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$sea_regu_point_cnt;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>