<?
include_once '_common.php'; // 공통

/* PARAMITER CHECK */
$para_list = array();
array_push($para_list, 'ecm_no', 'ecm_mode');

/* 숫자 PARAMITER 체크 */
$para_num_list = array();
array_push($para_num_list, 'ecm_no');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
foreach($para_list as $para_key => $para_val){
	if(in_array($para_val,$para_num_list)){
		if(num_check($_POST[$para_val]) == false && $_POST[$para_val] != '' && $_POST[$para_val] != '0'){
			alert($para_val."값에 숫자가 아닌 값이 들어 있습니다.", $_SERVER['HTTP_REFERER']);
			die;
		}
	} // end if

	${'_'.$para_val} = base_filter($_POST[$para_val]); /* 변수담기 */
} // end foreach

/* 쿠폰 정보 가져오기 */
$couponment_sql = "select * from event_couponment where ecm_no = '".$_ecm_no."'";
$couponment_row = sql_fetch($couponment_sql);

if($_ecm_state == 'n' && $couponment_row['ecm_end_date'] <= NM_TIME_YMDHIS) {
	alert("유효 기간이 지난 쿠폰입니다.", $_SERVER['HTTP_REFERER']);
	die;
} // end if

$dbtable = "event_couponment";
$dbwhere = "ecm_no='".$_ecm_no."'";
$dbset = "";

/* 수정 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

/* 쿠폰 제어 */
if($_ecm_mode == "state") { // 사용 & 중지
	$change_state = "y";
	if($couponment_row['ecm_state'] == "y") {
		$change_state = "n";
	} // end if
	$dbset = "ecm_state='".$change_state."'";
} else if($_ecm_mode == "overlap") { // 중복
	$change_overlap = "y";
	if($couponment_row['ecm_overlap'] == "y") {
		$change_overlap = "n";
	} // end if
	$dbset = "ecm_overlap='".$change_overlap."'";
} // end else if

$coupon_sql = "UPDATE ".$dbtable." SET ".$dbset." WHERE ".$dbwhere;
$coupon_result = sql_query($coupon_sql);

if($coupon_result) {
	$db_result['msg'] = "쿠폰의 상태가 변경되었습니다.";
} else {
	$db_result['state'] = 1;
	$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
	$db_result['error'] = $issue_sql;
} // end else

alert($db_result['msg'], HTTP_REFERER);
?>