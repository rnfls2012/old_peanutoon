<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-07-16
 * Time: 오후 19:20
 */

include_once '_common.php'; // 공통
// do not include_once '../_array_update.php'

/* PARAMETER CHECK */
$para_list = array('give_reason', 'give_point');

/* 숫자 PARAMETER 체크 */
$para_num_list = array('give_point');

/* 빈칸 PARAMETER 허용 */
$null_list = array(); // 사용 안 함

/* PARAMETER 숫자 검사하면서 $_PARAMETER로 값 대입 */
foreach($para_list as $para_key => $para_val) {
    if(in_array($para_val, $para_num_list)) {
        if(num_check($_POST[$para_val]) == false && $_POST[$para_val] != '' && $_POST[$para_val] != '0') {
            alert($para_val."값에 숫자가 아닌 값이 들어 있습니다.", $_SERVER['HTTP_REFERER']);
            die;
        } // end if
    } // end if
    ${'_'.$para_val} = base_filter($_POST[$para_val]); /* 변수담기 */
} // end foreach

/* 지급 */
$msg = "";
$sql_select = "
SELECT *
FROM member AS mb
LEFT JOIN event_cheerup AS ec ON mb.mb_no = ec.mb_no
WHERE ec.reward_YN = 'n' AND ec.blind_YN != 'y'
";

$result = sql_query($sql_select);

while($row = sql_fetch_array($result)) {
    $point_sum = $row['mb_point'] + $_give_point;

    $sql_update = "
    UPDATE event_cheerup AS ec SET ec.reward_YN = 'y', ec.give_date = now()
    WHERE ec.mb_no= '".$row['mb_no']."' AND ec.blind_YN = 'n' AND ec.reward_YN = 'n'
    ";

    $point_result = sql_query($sql_update);

    if(mysql_affected_rows() > 0) {
        $sql_mb_update = "
        UPDATE member SET mb_point = '".$point_sum."' WHERE mb_no = '".$row['mb_no']."'
        ";

        if (sql_query($sql_mb_update)) {
            mb_set_point_income($row, 'mb_point', 0, $_give_point, $_give_reason, '5', 'y');
            $msg = "일괄 지급 완료!";
        } else {
            $msg = "지급할 회원이 존재하지 않습니다.";
        }

    } else {
        $msg = "데이터 갱신 이상이 발생했습니다. 관리자에게 문의 바랍니다.";
    }
} // end while

pop_close($msg, NM_ADM_URL.'/event/comments.php');
die;
