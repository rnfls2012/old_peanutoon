<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
/* _array_update.php 처리 불가능 */

$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['date'] = NM_TIME_YMDHIS; // 날짜 저장

$attend_month = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
$attend_fetch = array('ea_month', 'ea_use', 'ea_start_date', 'ea_end_date', 'ea_oneday_point', 'ea_cont_date_1', 'ea_cont_point_1', 'ea_cont_date_2', 'ea_cont_point_2', 'ea_cont_date_3', 'ea_cont_point_3', 'ea_cont_use', 'ea_regu_date', 'ea_regu_point', 'ea_regu_use', 'ea_bgcolor', 'ea_cal_bgcolor', 'ea_cal_bandcolor', 'ea_etc_bgcolor', 'ea_header_img_pc', 'ea_reward_img_pc', 'ea_warning_img_pc', 'ea_header_img_mobile', 'ea_reward_img_mobile', 'ea_warning_img_mobile');
array_push($attend_fetch, 'ea_date'); // 날짜 저장
$attend_img_fetch = array('ea_header_img_pc', 'ea_reward_img_pc', 'ea_warning_img_pc', 'ea_header_img_mobile', 'ea_reward_img_mobile', 'ea_warning_img_mobile'); // 이미지 배열

/* _변수에 담기 */
foreach($attend_month as $month_key => $month_val) {
	foreach($attend_fetch as $fetch_key => $fetch_val) {
		${'_'.$fetch_val.'_'.$month_val} = tag_filter($_REQUEST[$fetch_val.'_'.$month_val]);
		
		// 날짜 저장
		if($fetch_val == 'ea_date') {
			${'_'.$fetch_val.'_'.$month_val} = $db_result['date'];
		} // end if
	} // end foreach
} // end foreach

/* 데이터 확인 */
$sql_count = "select count(*) as attend_count from event_attend";
$attend_count = sql_count($sql_count, "attend_count");

if($attend_count == 0) { // 없으면 INSERT
	foreach($attend_month as $month_key => $month_val) {
		$sql_insert = "INSERT INTO event_attend(";

		foreach($attend_fetch as $fetch_key => $fetch_val) {
			if(${'_'.$fetch_val.'_'.$month_val} != "") {
				$sql_insert .= $fetch_val.", "; 
			} // end if
		} // end foreach

		$sql_insert = substr($sql_insert, 0, strrpos($sql_insert,", "));
		$sql_insert .= ") VALUES('";

		foreach($attend_fetch as $fetch_key => $fetch_val) {
			if(${'_'.$fetch_val.'_'.$month_val} != "") {
				$sql_insert .= ${'_'.$fetch_val.'_'.$month_val}."', '";
			} // end if
		} // end foreach

		$sql_insert = substr($sql_insert, 0, strrpos($sql_insert,", "));
		$sql_insert .= ")";

		if(!sql_query($sql_insert)) {
			$db_result['state'] = 1;
			$db_result['msg'] = $month_val.'월이 저장되지 않았습니다.\n관리자에게 문의하세요!\n'.$sql_insert;
			echo $db_result['msg'];
			die;
		} // end if
	} // end foreach
} else { // 있으면 UPDATE
	foreach($attend_month as $month_key => $month_val) {
		$sql_update = "UPDATE event_attend SET ";

		foreach($attend_fetch as $fetch_key => $fetch_val) {
			if(${'_'.$fetch_val.'_'.$month_val} == "" && ($fetch_val == "ea_oneday_point" || $fetch_val == "ea_cont_date_1" || $fetch_val == "ea_cont_point_1" || $fetch_val == "ea_cont_date_2" || $fetch_val == "ea_cont_point_2" || $fetch_val == "ea_cont_date_3" || $fetch_val == "ea_cont_point_3")) {
				$sql_update .= $fetch_val." = NULL, "; 
			} else if(${'_'.$fetch_val.'_'.$month_val} == "" && ($fetch_val == "ea_use" || $fetch_val == "ea_cont_use" || $fetch_val == "ea_regu_use")) {
				$sql_update .= $fetch_val." = 'n', "; 
			} else if(${'_'.$fetch_val.'_'.$month_val} != "" && $fetch_val != "ea_month") {
				$sql_update .= $fetch_val." = '".${'_'.$fetch_val.'_'.$month_val}."', "; 
			} // end elseif
		} // end foreach

		$sql_update = substr($sql_update, 0, strrpos($sql_update,", "));
		$sql_update .= " WHERE ea_month = '".$month_val."'";
		
		/* 연속출석일이 개근일보다 크거나 같을때 취소 */
		if((${'_ea_cont_date_1_'.$month_val} != "" || ${'_ea_cont_date_2_'.$month_val} != "" || ${'_ea_cont_date_3_'.$month_val} != "") && (${'_ea_regu_date_'.$month_val} <= ${'_ea_cont_date_1_'.$month_val} || ${'_ea_regu_date_'.$month_val} <= ${'_ea_cont_date_2_'.$month_val} || ${'_ea_regu_date_'.$month_val} <= ${'_ea_cont_date_3_'.$month_val})) {
			alert($month_val."월의 연속출석일수가 개근일보다 크거나 같습니다.", $_SERVER['HTTP_REFERER']);
			die;
		} // end if

		if(!sql_query($sql_update)) {
			$db_result['state'] = 1;
			$db_result['msg'] = $month_val.'월이 수정되지 않았습니다.\n관리자에게 문의하세요!\n'.$sql_update;
			alert($db_result['msg'], $_SERVER['HTTP_REFERER']);
			die;
		} // end if
	} // end foreach
} // end else

/* 이미지 처리 */
if($db_result['state'] == 0) {
	foreach($attend_month as $month_key => $month_val) {
		foreach($attend_img_fetch as $img_fetch_key => $img_fetch_val) {
			/* 이미지 경로 */

			/* 대분류 경로설정 - 폴더 체크 및 생성 */
			$path = "_pc";
			if(strpos($img_fetch_val, "mobile")) {
				$path = "_mobile";
			} // end if

			$path_cover = $path.'/event/attend/'.$month_val."/";
			
			/* 업로드한 이미지 명 */
			$image_cover_tmp_name = $_FILES[$img_fetch_val."_".$month_val]['tmp_name'];
			$image_cover_name = $_FILES[$img_fetch_val."_".$month_val]['name'];

			/* 확장자 얻기 .png .jpg .gif */
			$image_cover_extension = substr($image_cover_name, strrpos($image_cover_name, "."), strlen($image_cover_name));

			/* 고정된 파일 명 */
			$image_cover_name_define = $img_fetch_val.'_'.$month_val.strtolower($image_cover_extension);

			/* 저장 경로 및 DB 파일명 및 파일경로 저장 */
			$image_cover_src = $path_cover.$image_cover_name_define;
			
			/* 파일 업로드 */ 
			$image_cover_result = false;
			
			if ($image_cover_name != '') { 
				
				$image_cover_result = kt_storage_upload($image_cover_tmp_name, $path_cover, $image_cover_name_define);
				/* 리사이징 안 함
				$image_cover_result = tn_set_key_thumbnail($image_cover_tmp_name, $path_cover, 'eme_cover' ,'not', '', $image_cover_name_define);	
				$image_cover_result = tn_upload_thumbnail($image_cover_tmp_name, $path_cover, $image_cover_name_define, 'eme_cover' ,'not', '', $image_cover_name_define);	
				
				$image_cover_result = tn_set_key_thumbnail($image_cover_tmp_name, $path_cover, 'eme_cover' ,'all', '', $image_cover_name_define);
				$image_cover_result = tn_upload_thumbnail($image_cover_tmp_name, $path_cover, $image_cover_name_define, 'eme_cover' ,'all', '', $image_cover_name_define);
				*/
			}
			rmdirAll(NM_THUMB_PATH.'/'.$path_cover); // kt storage - epromotion 디렉토리 삭제
			rmdirAll(NM_PATH.'/'.$path_cover); // kt storage - epromotion 디렉토리 삭제
			
			/* 임시파일이 존재하는 경우 삭제 */
			if (file_exists($image_cover_tmp_name) && is_file($image_cover_tmp_name)) {
				unlink($image_cover_tmp_name);
			}
			
			$sql_image = " UPDATE event_attend SET ";
			/* 이미지 field값 추가 */
			if ($image_cover_name != '' && $image_cover_result == true){ $sql_image.= $img_fetch_val." = '".$image_cover_src."' "; }
			$sql_image.= " WHERE ea_month = '".$month_val."'";

			if ($image_cover_name != '' && $image_cover_result == true){
				/* DB 저장 */
				if(sql_query($sql_image)){
					
				}else{
					$db_result['state'] = 1;
					$db_result['msg'].= $month_val."월의 이미지가 저장되지 않았습니다.";
					$db_result['error'].= $sql_image;
					break;
					alert($db_result['msg'], $_SERVER['HTTP_REFERER']);
					die;
				}
			}
		} // end foreach
	} // end foreach
}

alert($db_result['date'].' 출석이벤트 내용이 적용되였습니다.',$_SERVER['HTTP_REFERER']);
?>