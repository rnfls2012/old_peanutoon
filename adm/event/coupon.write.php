<?
include_once '_common.php'; // 공통

/* PARAMITER */
$ecm_overlap = array("y"=>"중복 사용 가능", "n"=>"중복 사용 불가");

/* 모드설정 */

/* 대분류로 제목 */
$page_title = "쿠폰";
$mode_text = "발급";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<script type="text/javascript" src="<?=NM_URL."/js/jscolor.js";?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- event_free_dc_head -->

<section id="event_write">
	<form name="event_couponment_write_form" id="event_couponment_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return event_couponment_write_submit();">
	<input type="hidden" name="ecm_days" id="ecm_days" value=<?=date("z");?>>
	<input type="hidden" name="ecm_start_date" id="ecm_start_date" value="<?=NM_TIME_YMDHIS;?>">
	<input type="hidden" name="ecm_state" id="ecm_state" value="y">
		<table>
			<tbody>
				<tr>
					<th><label for="ecm_cash_point"><?=$required_arr[0];?>지급 <?=$nm_config['cf_cash_point_unit_ko'];?></label></th>
					<td>
						<input type="text" placeholder="지급 <?=$nm_config['cf_cash_point_unit_ko'];?> 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="ecm_cash_point" id="ecm_cash_point" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<th><label for="ecm_point"><?=$required_arr[0];?>지급 <?=$nm_config['cf_point_unit_ko'];?></label></th>
					<td>
						<input type="text" placeholder="지급 <?=$nm_config['cf_point_unit_ko'];?> 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="ecm_point" id="ecm_point" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<th><label for="ecm_lot"><?=$required_arr[0];?>발급 매수</label></th>
					<td>
						<input type="text" <?=$required_arr[1];?> name="ecm_lot" id="ecm_lot" class="ecm_lot" value="100" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<th><label for="ecm_overlap"><?=$required_arr[0];?>중복 사용 여부</label></th>
					<td>
						<? tag_radios($ecm_overlap, "ecm_overlap", "y", "n"); ?>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>유효 기간</label></th>
					<td>
						<input type="text" class="e_date readonly" placeholder="클릭하세요" name="e_date" id="e_date" autocomplete="off" readonly /> 까지
					</td>
				</tr>
				<tr>
					<th><label for="ep_adult"><?=$required_arr[0];?>사용처</label></th>
					<td>
						<input type="text" placeholder="사용처 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="ecm_ticket" id="ecm_ticket" class="ecm_ticket" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- event_free_dc_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>