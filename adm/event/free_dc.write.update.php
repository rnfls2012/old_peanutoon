<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode','ep_no','ep_name','ep_text','ep_bgcolor','ep_cover','ep_adult');
array_push($para_list, 'ep_date_type','ep_state', 'ep_date_start','ep_date_end');
array_push($para_list, 'ep_hour_start','ep_hour_end');
array_push($para_list, 'state', 'event_reg_date','event_mod_date','ep_holiday','ep_holiweek');
array_push($para_list, 'ep_reg_date', 'ep_mod_date');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'ep_no');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'ep_date_start', 'ep_date_end', 'ep_holiday', 'ep_holiweek');

/* 빈칸 PARAMITER 시 NULL 처리 */
array_push($null_list, 'ep_holiday','ep_holiweek');

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

/* 이벤트리스트DB처리 */
$para_event_list = array('epc_comics','epc_sale_pay','epc_sale_e','epc_free_e');
/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
foreach($para_event_list as $para_event_key => $para_event_val){
	for($i=0; $i<count($_POST[$para_event_val]); $i++){
		if(num_check($_POST[$para_event_val][$i]) == false && $_POST[$para_event_val][$i] != '' && $_POST[$para_event_val][$i] != '0'){
			alert($_POST[$para_event_val][$i]."값에 숫자가 아닌 값이 들어 있습니다.", $_SERVER['HTTP_REFERER']);
			die;
		}
		$_POST[$para_event_val][$i] = intval($_POST[$para_event_val][$i]);
	}
	${'_'.$para_event_val} = $_POST[$para_event_val]; /* 변수담기 */
}

// 상태와 날짜
$get_date_type = get_date_type($_ep_date_type, $_ep_date_start, $_ep_date_end, $_ep_hour_start, $_ep_hour_end);
$_ep_state = $get_date_type['state'];
$_ep_date_start = $get_date_type['date_start'];
$_ep_date_end = $get_date_type['date_end'];

$dbtable = "event_pay";
$dbt_primary = "ep_no";
$para_primary = "ep_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

$dbtable2 = "event_pay_comics";
$dbt_primary2 = "epc_ep_no";
$para_primary2 = "ep_no";
${'_'.$dbt_primary2} = ${'_'.$para_primary2};


/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

if($_mode == 'reg'){
	/* 고정값 */
	$_ep_reg_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);
	
	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = $ep_name.'의 데이터가 등록되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

	/* ////////////////////이벤트리스트 저장/////////////////// */
	para_event_list_save();

/* 수정 */
}else if($_mode == 'mod'){
	$_ep_mod_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{		
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $ep_name.'의 데이터가 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

		/* ////////////////////이벤트 리스트 처리//////////////////// */
		/* 기존에 있던 이벤트 리스트 삭제 */
		$sql_mod_epc_del = "DELETE FROM `".$dbtable2."` WHERE `".$dbt_primary2."` ='".${'_'.$dbt_primary2}."'";
		sql_query($sql_mod_epc_del);

		/* 이벤트리스트 저장 */
		para_event_list_save();
	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 배너 이미지 삭제 */
		$row_ep = sql_fetch("select * from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
		kt_storage_delete($row_ep['ep_cover']);
		kt_storage_delete($row_ep['ep_collection_top']);

		/* 데이터 삭제 */
		sql_query("delete from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
		
		$db_result['msg'] = $ep_name.'의 데이터가 삭제되였습니다.';

		/* 기존에 있던 이벤트 리스트 삭제 */
		$sql_mod_epc_del = "DELETE FROM `".$dbtable2."` WHERE `".$dbt_primary2."` ='".${'_'.$dbt_primary2}."'";
		sql_query($sql_mod_epc_del);
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/* 이미지 처리 */
if($_mode != 'del' && $db_result['state'] == 0){
	/* 등록이라면~ [ eventnum 이 NULL 이라면 ] */
	if($_ep_no == ''){
		$sql_max = 'select COALESCE(max('.$dbt_primary.'),0) as '.$dbt_primary.'_new from '.$dbtable; /* 방금 등록번호 가져오기 */
		$row_max = sql_fetch($sql_max);
		$_ep_no = $row_max[$dbt_primary.'_new'];
	}

	/* 이미지 경로 */

	/* 대분류 경로설정 - 폴더 체크 및 생성 */
	$path_banner = 'event/';
	
	/* 업로드한 이미지 명 */
	$image_banner_tmp_name = $_FILES['ep_cover']['tmp_name'];
	$image_banner_name = $_FILES['ep_cover']['name'];

	$image_collection_tmp_name = $_FILES['ep_collection_top']['tmp_name'];
	$image_collection_name = $_FILES['ep_collection_top']['name'];
	
	/* 리사이징 안함 DB 저장 안함 */
	$_ep_cover_noresizing			= etc_filter(num_eng_check(tag_filter($_REQUEST['ep_cover_noresizing'])));
	$_ep_collection_top_noresizing	= etc_filter(num_eng_check(tag_filter($_REQUEST['ep_collection_top_noresizing'])));

	/* 확장자 얻기 .png .jpg .gif */
	$image_banner_extension = substr($image_banner_name, strrpos($image_banner_name, "."), strlen($image_banner_name));
	$image_collection_extension = substr($image_collection_name, strrpos($image_collection_name, "."), strlen($image_collection_name));

	/* 고정된 파일 명 */
	$image_banner_name_define = 'event_'.$_ep_no.strtolower($image_banner_extension);
	$image_collection_name_define = 'event_collection_'.$_ep_no.strtolower($image_collection_extension);

	/* 저장 경로 및 DB 파일명 및 파일경로 저장 */
	$image_banner_src = $path_banner.$image_banner_name_define;
	$image_collection_src = $path_banner.$image_collection_name_define;

	/* 파일 업로드 */
	$image_banner_result = false;
	if ($image_banner_name != '') {		
		if($_ep_cover_noresizing == 'y'){
			$image_banner_result = kt_storage_upload($image_banner_tmp_name, $path_banner, $image_banner_name_define); 
			$image_banner_result = tn_set_key_thumbnail($image_banner_tmp_name, $path_banner, 'ep_cover' ,'not', '', $image_banner_name_define);	
			$image_banner_result = tn_upload_thumbnail($image_banner_tmp_name, $path_banner, $image_banner_name_define, 'ep_cover' ,'not', '', $image_banner_name_define);	
		}else{
			$image_banner_result = tn_set_key_thumbnail($image_banner_tmp_name, $path_banner, 'ep_cover' ,'all', '', $image_banner_name_define);
			$image_banner_result = tn_upload_thumbnail($image_banner_tmp_name, $path_banner, $image_banner_name_define, 'ep_cover' ,'all', '', $image_banner_name_define);
		}
	} // end if

	$image_collection_result = false;
	if ($image_collection_name != '') {		
		if($_ep_collection_top_noresizing == 'y'){
			$image_collection_result = kt_storage_upload($image_collection_tmp_name, $path_banner, $image_collection_name_define); 
			$image_collection_result = tn_set_key_thumbnail($image_collection_tmp_name, $path_banner, 'ep_collection_top' ,'not', '', $image_collection_name_define);	
			$image_collection_result = tn_upload_thumbnail($image_collection_tmp_name, $path_banner, $image_collection_name_define, 'ep_collection_top' ,'not', '', $image_collection_name_define);	
		}else{
			$image_collection_result = tn_set_key_thumbnail($image_collection_tmp_name, $path_banner, 'ep_collection_top' ,'all', '', $image_collection_name_define);
			$image_collection_result = tn_upload_thumbnail($image_collection_tmp_name, $path_banner, $image_collection_name_define, 'ep_collection_top' ,'all', '', $image_collection_name_define);
		}
	} // end if
	rmdirAll(NM_THUMB_PATH.'/'.$path_banner); // kt storage - comics 디렉토리 삭제
	rmdirAll(NM_PATH.'/'.$path_banner); // kt storage - comics 디렉토리 삭제
	
	/* 임시파일이 존재하는 경우 삭제 */
	if (file_exists($image_banner_tmp_name) && is_file($image_banner_tmp_name)) {
		unlink($image_banner_tmp_name);
	}
	if (file_exists($image_collection_tmp_name) && is_file($image_collection_tmp_name)) {
		unlink($image_collection_tmp_name);
	}
	
	$sql_image = ' UPDATE '.$dbtable.' SET ';
	/* 이미지 field값 추가 */
	if ($image_banner_name != ''){ $sql_image.= ' ep_cover = "'.$image_banner_src.'", '; }
	if ($image_collection_name != ''){ $sql_image.= ' ep_collection_top = "'.$image_collection_src.'", '; }
	$sql_image = substr($sql_image,0,strrpos($sql_image, ","));
	$sql_image.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";

	if (($image_banner_name != '' && $image_banner_result == true) || $image_collection_name != '' && $image_collection_result == true) {
		/* DB 저장 */
		if(sql_query($sql_image)){
			$db_result['msg'].= '\n'.$ep_name.'의 이미지가 저장되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'].= '\n'.'이미지 에러가 발생하여 저장되지 않았습니다.';
			$db_result['error'].= $sql_image;
		}
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
pop_close($db_result['msg'], '', $db_result['error']);
die;

/* ////////////////////이벤트리스트 저장/////////////////// */
function para_event_list_save(){
    global $_mode, $para_event_list, $_ep_no, $dbtable2 ,$dbt_primary2, $dbt_primary2, $dbtable ,$dbt_primary, $dbt_primary;
	foreach($para_event_list as $para_event_key => $para_event_val){
		global ${'_'.$para_event_val};
	}

	if($_ep_no == ''){
		$sql_max = 'select COALESCE(max('.$dbt_primary.'),0) as '.$dbt_primary.'_new from '.$dbtable; /* 방금 등록번호 가져오기 */
		$row_max = sql_fetch($sql_max);
		$_ep_no = $row_max[$dbt_primary.'_new'];
	}
	
	$_event_save_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	array_push($para_event_list, 'epc_date', 'epc_ep_no', 'epc_free', 'epc_sale', 'epc_order');
	
	for($i=0; $i<count(${'_'.$para_event_list[0]}); $i++){
		$_epc_date[$i] = $_event_save_date;
		$_epc_ep_no[$i] = $_ep_no;
		$_epc_free[$i] = 'n';
		$_epc_sale[$i] = 'n';
		$_epc_order[$i] = $i;
		/* 값을 있을시 */
		if($_epc_free_e[$i] > 0 && $_epc_free_e[$i] != ''){ $_epc_free[$i] = 'y'; }
		if($_epc_sale_e[$i] > 0 && $_epc_sale_pay[$i] > 0 && $_epc_sale_e[$i] != '' && $_epc_sale_pay[$i] != ''){ $_epc_sale[$i] = 'y'; }

		/* field명 */
		$sql_epc_reg = 'INSERT INTO '.$dbtable2.'(';
		foreach($para_event_list as $para_event_key2 => $para_event_val2){
			/* sql문구 */
			$sql_epc_reg.= $para_event_val2.', ';
		}
		$sql_epc_reg = substr($sql_epc_reg,0,strrpos($sql_epc_reg, ","));
		/* VALUES 시작 */
		$sql_epc_reg.= ' )VALUES( ';

		/* field값 */
		foreach($para_event_list as $para_event_key2 => $para_event_val2){
			/* sql문구 */
			$sql_epc_reg.= '"'.${'_'.$para_event_val2}[$i].'", ';
		}
		$sql_epc_reg = substr($sql_epc_reg,0,strrpos($sql_epc_reg, ","));

		/* SQL문 마무리 */
		$sql_epc_reg.= ' ) ';

		/* DB 저장 */
		if(sql_query($sql_epc_reg)){
			$db_result['msg'] = $_ep_no.'의 이벤트 데이터가 등록되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '에러가 발생하여 이벤트 데이터가 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_epc_reg;

			// alert($db_result['msg']);
		}
	}
}

?>