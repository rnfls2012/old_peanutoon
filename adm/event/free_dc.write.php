<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_ep_no = tag_filter($_REQUEST['ep_no']);

$sql = "SELECT * FROM `event_pay` WHERE ep_no = '$_ep_no' ";
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
			 $sql_list = "";
			 $row['ep_hour_end']=23; // 등록시 마감시간 23로 고정
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
	
	$db_comics_list = $db_comics_info = array();
	/* 이벤트 리스트 가져오기 */
	$sql_event_pay_comics = "SELECT * FROM `event_pay_comics` epc left JOIN comics c ON epc.epc_comics = c.cm_no WHERE epc.epc_ep_no = '".$row['ep_no']."' order by epc.epc_order ASC";
	$result_epc = sql_query($sql_event_pay_comics);
	while ($row_epc = sql_fetch_array($result_epc)) {
		array_push($db_comics_list, $row_epc);
	}
}

/* 대분류로 제목 */
$page_title = "무료&코인할인";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<script type="text/javascript" src="<?=NM_URL."/js/jscolor.js";?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- event_free_dc_head -->

<section id="event_write">
	<form name="event_free_dc_write_form" id="event_free_dc_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return event_free_dc_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="ep_no" name="ep_no" value="<?=$_ep_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ep_no">이벤트 번호</label></th>
					<td><?=$_ep_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="ep_name"><?=$required_arr[0];?>이벤트명</label></th>
					<td>
						<input type="text" placeholder="이벤트명 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="ep_name" id="ep_name" value="<?=$row['ep_name'];?>" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<th><label for="ep_text"><?=$required_arr[0];?>이벤트내용</label></th>
					<td>
						<input type="text" placeholder="이벤트내용 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="ep_text" id="ep_text" value="<?=$row['ep_text'];?>" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>이벤트 기간</label></th>
					<td>
						<div id="service_color" class="input_btn">
							<input type="radio" name="ep_date_type" id="ep_date_type0" value="y" <?=get_checked('', $row['ep_date_type']);?> <?=get_checked('y', $row['ep_date_type']);?>  onclick="ep_date_type_chk(this.value);" />
							<label for="ep_date_type0">기간만적용</label>
							<input class="still" type="radio" name="ep_date_type" id="ep_date_type1" value="m" <?=get_checked('m', $row['ep_date_type']);?> onclick="ep_date_type_chk(this.value);" />
							<label class="still" for="ep_date_type1">매월(날짜빈칸시 무기간)</label>
							<input class="still" type="radio" name="ep_date_type" id="ep_date_type2" value="w" <?=get_checked('w', $row['ep_date_type']);?> onclick="ep_date_type_chk(this.value);" />
							<label class="still" for="ep_date_type2">매주(날짜빈칸시 무기간)</label>
							<input type="radio" name="ep_date_type" id="ep_date_type3" value="n" <?=get_checked('n', $row['ep_date_type']);?> onclick="ep_date_type_chk(this.value);" />
							<label for="ep_date_type3">무기간</label>
						</div>
						<div class="<? echo ($row['ep_date_type'] != 'n')?"":"still";?> ep_date_type_view margin_top10">
							<span for="s_date">시작일 :</span>
							<input type="text" class="s_date readonly" placeholder="클릭하세요" name="ep_date_start" id="s_date" value="<?=$row['ep_date_start'];?>" autocomplete="off" readonly /> 
							<?=set_hour('ep_hour_start', $row['ep_hour_start'], '시간 :', 'n');?>00분
							~ 
							<span for="e_date">종료일 :</span>
							<input type="text" class="e_date readonly" placeholder="클릭하세요" name="ep_date_end" id="e_date" value="<?=$row['ep_date_end'];?>" autocomplete="off" readonly />
							<?=set_hour('ep_hour_end', $row['ep_hour_end'], '시간 :', 'n');?>59분
						</div>
						<div class="ep_holiday_view still margin_top10">
							<span for="ep_holiweek">매월</span>
							<?	tag_selects($day_list, "ep_holiday", $row['ep_holiday'], 'y', '사용안함', ''); ?>
						</div>
						<div class="ep_holiweek_view still margin_top10">
							<span for="ep_holiweek">매주</span>
							<?	tag_selects($week_list, "ep_holiweek", $row['ep_holiweek'], 'y', '사용안함', ''); ?>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="ep_adult"><?=$required_arr[0];?>이벤트 등급</label></th>
					<td>
						<div id="ep_adult" class="input_btn">
							<input type="radio" name="ep_adult" id="ep_adulty" value="y" <?=get_checked('', $row['ep_adult']);?> <?=get_checked('y', $row['ep_adult']);?> />
							<label for="ep_adulty">성인</label>
							<input type="radio" name="ep_adult" id="ep_adultn" value="n" <?=get_checked('n', $row['ep_adult']);?> />
							<label for="ep_adultn">청소년</label>
						</div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="state">이벤트 상태</label></th>
					<td><?=$d_state[$row['ep_state']];?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for=""><?=$required_arr[0];?>이벤트 <br/>메인 이미지</label></th>
					<td>
						<? if($row['ep_cover']!=''){ $ep_cover_src = img_url_para($row['ep_cover'], $row['ep_reg_date'], $row['ep_mod_date']); } ?>
						<input type="file" name="ep_cover" id="ep_cover" data-value="<?=$ep_cover_src?>" />
						<? if(cs_is_admin()){ ?>
							<input type="checkbox" name="ep_cover_noresizing" id="ep_cover_noresizing" value="y" class="noresizing" />
							<label for="ep_cover_noresizing">RESIZING-사용안함</label>
						<? } ?>
						<p class="ep_cover_explan">이미지 업로드 정보 : width:1000px, height:417px, 이미지파일 확장자:jpg, gif, png</p>
						<?if($row['ep_cover']!=''){?>
						<div class="ep_cover">
							<p><a href="#ep_cover" onclick="a_click_false();">메인배너보기</a></p>
							<p class="ep_cover_hide"><img src="<?=$ep_cover_src?>" alt="<?=$row['ep_name']."이벤트배너"?>" /></p>
						</div>
						<?}?>
					</td>
				</tr>
				<tr>
					<th><label for=""><?=$required_arr[0];?>콜렉션 상단 이미지</label></th>
					<td>
						<? if($row['ep_collection_top']!=''){ $ep_collection_top_src = img_url_para($row['ep_collection_top'], $row['ep_reg_date'], $row['ep_mod_date']); } ?>
						<input type="file" name="ep_collection_top" id="ep_collection_top" data-value="<?=$ep_collection_top_src?>" />
						<? if(cs_is_admin()){ ?>
							<input type="checkbox" name="ep_collection_top_noresizing" id="ep_collection_top_noresizing" value="y" class="noresizing" />
							<label for="ep_collection_top_noresizing">RESIZING-사용안함</label>
						<? } ?>
						<p class="ep_collection_top_explan">이미지 업로드 정보 : width:1000px, height:417px, 이미지파일 확장자:jpg, gif, png</p>
						<?if($row['ep_collection_top']!=''){?>
						<div class="ep_collection_top">
							<p><a href="#ep_collection_top" onclick="a_click_false();">콜렉션 상단 이미지보기</a></p>
							<p class="ep_collection_top_hide"><img src="<?=$ep_collection_top_src?>" alt="<?=$row['ep_name']."콜렉션 상단 이미지"?>" /></p>
						</div>
						<?}?>
					</td>
				</tr>

				<? if($_mode == "mod") { ?>
				<tr>
					<th><label for="cdn_purge">Purge<br/>이미지 파일경로 복사</label></th>
					<td>
						<textarea id="copy_text"><? tn_get_info($row['ep_cover'], 'ep_cover'); ?><? tn_get_info($row['ep_collection_top'], 'ep_collection_top'); ?></textarea>
						<a class="copy">IMAGE COPY</a>
						<a class="purge" href="<?=NM_ADM_URL?>/cdn_purge.php" target="_blank">Purge 가기</a>
						<input type="hidden" id="copy_text_ajax"  value="<? tn_get_info($row['ep_cover'], 'ep_cover',true); ?><? tn_get_info($row['ep_collection_top'], 'ep_collection_top',true); ?>"/>
						<a class="copy_btn" onclick="cdn_api_purge_send();">Purge 호출</a>
					</td>
				</tr>
				<?}?>

				<tr>
					<th><label for="ep_bgcolor"><?=$required_arr[0];?>배경색</label></th>
					<td>
						<input type="text" <?=$required_arr[1];?> name="ep_bgcolor" id="ep_bgcolor" class="color" value="<?=$row['ep_bgcolor'];?>" autocomplete="off" />
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ep_reg_date">이벤트 등록일</label></th>
					<td><?=$row['ep_reg_date'];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ep_mod_date">이벤트 수정일</label></th>
					<td><?=$row['ep_mod_date'];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ep_click">이벤트 클릭수</label></th>
					<td><?=$row['ep_click'];?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="comics_list"><?=$required_arr[0];?>이벤트 컨텐츠 리스트</label></th>
					<td id="comics_list">
						<ul>
						<? if(count($db_comics_list) > 0){ 
							foreach($db_comics_list as $comics_list_key => $comics_list_val){?>
							<li id="li_add_comics<?=$comics_list_val['epc_comics'];?>" class="result_hover" data_comics="<?=$comics_list_val['epc_comics'];?>">
								<input type="hidden" name="epc_comics[]" value="<?=$comics_list_val['epc_comics'];?>">
								<p>
								<?=$comics_list_val['cm_series'];?>(가격:<?=cash_point_view($comics_list_val['cm_pay']);?>, 총화:<?=$comics_list_val['cm_episode_total'];?>화, <?=$d_adult[$comics_list_val['cm_adult']];?>)
								<a href="#comics_list_del" onclick="comics_list_del(<?=$comics_list_val['epc_comics'];?>);">
								<img src="<?=NM_IMG?>cms/free_dc_del.png" alt="삭제">
								</a>
								</p>
								<label>이벤트할인가격 : </label><input type="text" class="comics_list_add_input onlynumber" placeholder="숫자만" name="epc_sale_pay[]" value="<?=cash_point_view($comics_list_val['epc_sale_pay'],'y','n');?>" onchange="data_limit_check(this);" autocomplete="off" maxlength="5" data_limit="<?=cash_point_view($comics_list_val['cm_pay'],'y','n');?>"><?=$nm_config['cf_cash_point_unit'];?>
								 , 
								<label>이벤트할인화수: </label><input type="text" class="comics_list_add_input onlynumber" placeholder="숫자만" name="epc_sale_e[]" value="<?=$comics_list_val['epc_sale_e'];?>" onchange="data_limit_check(this);" autocomplete="off" maxlength="5" data_limit="<?=$comics_list_val['cm_episode_total'];?>">
								 , 
								<label>이벤트무료화수 : </label><input type="text" class="comics_list_add_input onlynumber" placeholder="숫자만" name="epc_free_e[]" value="<?=$comics_list_val['epc_free_e'];?>" onchange="data_limit_check(this);" autocomplete="off" maxlength="5" data_limit="<?=$comics_list_val['cm_episode_total'];?>">
							</li>
							<? } ?>
						<? } ?>
						</ul>
						<!-- 이벤트 검색 리스트 -->
					</td>
				</tr>
				<tr>
					<th><label for="comics_search">컨텐츠 검색</label></th>
					<td>
						<!-- 이벤트 선택 리스트 -->
						<input type="text" class="comics_search" placeholder="검색하세요" id="comics_search" value="" autocomplete="off" onkeydown='comics_search_chk();' />
						<a id="comics_search_btn" class="comics_search_btn" href='#comics_search' onclick='a_click_false()'>검색</a>
						<div id="comics_search_result"></div>
						<!-- 이벤트 검색 리스트 -->
					</td>
				</tr>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- event_free_dc_write -->

<? include_once NM_ADM_PATH.'/clipboard.php'; // 복사 ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>