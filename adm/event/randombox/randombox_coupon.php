<? include_once '_common.php'; // 공통

// 검색용 배열
$discount_arr = array();
$used_arr = array("y" => "사용", "n" => "미사용");

// 할인권 설정 정보 가져오기
$couponment_arr = array();
$couponment_sql = "SELECT * FROM event_randombox_couponment";
$couponment_result = sql_query($couponment_sql);
while($couponment_row = sql_fetch_array($couponment_result)) {
	${"_".$couponment_row['ercm_no']} = 0;
	${"_".$couponment_row['ercm_no']."_use"} = 0;
	$discount_arr[$couponment_row['ercm_no']] = $couponment_row['ercm_discount_rate']."%";
	array_push($couponment_arr, $couponment_row);
} // end while

// 할인권 총 사용 수
$total_coupon_use = 0;

// 이벤트 정보 가져오기
$event_arr = array();
$event_sql = "SELECT * FROM event_randombox";
$event_result = sql_query($event_sql);
while($event_row = sql_fetch_array($event_result)) {
	array_push($event_arr, $event_row);
} // end while

// 할인권 정보 가져오기
$coupon_arr = array();
$coupon_sql = "SELECT * FROM event_randombox_coupon";
$coupon_result = sql_query($coupon_sql);
while($coupon_row = sql_fetch_array($coupon_result)) {
	array_push($coupon_arr, $coupon_row);
} // end while
foreach($couponment_arr as $couponment_key => $couponment_val) {
	foreach($coupon_arr as $coupon_key => $coupon_val) {
		// 발급 수
		if($coupon_val['erc_ercm_no'] == $couponment_val['ercm_no']) {
			${"_".$couponment_val['ercm_no']}++;

			// 사용 수
			if($coupon_val['erc_use'] == "y") {
				${"_".$couponment_val['ercm_no']."_use"}++;
			} // end if
		} // end if
	} // end foreach

	$total_coupon_use += ${"_".$couponment_val['ercm_no']."_use"};
} // end foreach

if($_s_text) {
	$erc_members = array();
	$member_sql = "SELECT mb_no FROM member WHERE mb_id like '%$_s_text%'";
	$member_result = sql_query($member_sql);
	while($member_row = sql_fetch_array($member_result)) {
		array_push($erc_members, $member_row['mb_no']);
	} // end while
	$erc_members = implode(", ", $erc_members);

	$sql_where .= "and erc_member in(".$erc_members.") ";
} // end if

if($_ercm_no) {
	$sql_where .= "and erc_ercm_no='".$_ercm_no."' ";
} // end if

if($_erc_use_state) {
	$sql_where .= "and erc_use='".$_erc_use_state."' ";
} // end if

// 그룹
$sql_group = "";

if($_s_limit == ''){ $_s_limit = 30; }

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "erc_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$event_randombox_coupon_order_add = '';
if($_order_field == "erc_state"){  $event_randomobx_order_add = " , erc_no DESC "; }
$sql_order = "order by ".$_order_field." ".$_order." ".$event_randombox_coupon_order_add ;

// SQL문
$sql_field		= " * ";
$sql_count		= " count(*) as cnt ";

$sql_table		= "select {$sql_field} from event_randombox_coupon";
$sql_table_cnt	= "select {$sql_count} from event_randombox_coupon";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_table_cnt} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts	= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값

$total_event_randombox_coupon = total_event_randombox_coupon(); // 이벤트 총수 - function 선언 하단

$th_class = $td_class = "";	// 상단테이블 css-class

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('쿠폰 번호', 'erc_no', 0));
array_push($fetch_row, array('아이디', 'mb_id', 0));
array_push($fetch_row, array('이벤트 명', 'er_name', 0));
array_push($fetch_row, array('할인율', 'ercm_discount_rate', 0));
array_push($fetch_row, array('사용 가능 여부', 'erc_state', 0));
array_push($fetch_row, array('사용 여부', 'erc_use', 1));
array_push($fetch_row, array('유효기간', 'er_available_date', 0));
array_push($fetch_row, array('사용 날짜', 'erc_use_date', 0));
array_push($fetch_row, array('획득 날짜', 'erc_date', 0));

$page_title = "랜덤박스 이벤트 쿠폰";
$cms_head_title = $page_title;
/* 여기부터 다시 */
$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$cms_page_title;?></h1>
	<div>
		<strong><?=$page_title;?> : <?=number_format($total_event_randombox_coupon);?></strong>
	</div>
</section>

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="event_randombox_coupon_search_form" id="event_randombox_coupon_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return event_randombox_coupon_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<? tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<? tag_selects($discount_arr, "ercm_no", $_ercm_no, 'y', '할인율', ''); ?>
					<? tag_selects($used_arr, "erc_use_state", $_erc_use_state, 'y', '사용여부', ''); ?>
				</div>
				<div class="cs_search_btn">
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>" placeholder="아이디로 검색">
				</div>
			</div>
			<div class="cs_submit event_randombox_coupon_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="event_result">
	<h3 id="cr_thead_mg_add">
		<table>
			<tr>
				<th class="topleft" rowspan="2" style="vertical-align: middle;">총 발급 쿠폰</th>
				<th rowspan="2" style="vertical-align: middle;">총 사용 쿠폰</th>
				<? foreach($couponment_arr as $couponment_key => $couponment_val) { ?>
				<th colspan="2" class="<?=sizeof($couponment_arr) == $couponment_key+1?'topright':'';?>"><?=$couponment_val['ercm_discount_rate'];?>%</th>
				<? } // end foreach ?>
			</tr>
			<tr>
				<? foreach($couponment_arr as $couponment_key => $couponment_val) { ?>
				<th class="red">발급 수</th>
				<th class="blue">사용 수</th>
				<? } // end foreach ?>
			</tr>
			<tr>
				<td><?=$total_event_randombox_coupon;?></td>
				<td><?=$total_coupon_use;?></td>
				<? foreach($couponment_arr as $couponment_key => $couponment_val) { ?>
				<td class="red"><?=${"_".$couponment_val['ercm_no']};?></td>
				<td class="blue"><?=${"_".$couponment_val['ercm_no']."_use"};?></td>
				<? } // end foreach ?>
			</tr>
		</table>
		<br/>
		<strong>검색 결과 수 : <?=number_format($rows_cnts);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){ $th_title_giho = $order_giho; }
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){ 
					// 할인율 가져오기
					foreach($couponment_arr as $couponment_key => $couponment_val) {
						if($couponment_val['ercm_no'] == $dvalue_val['erc_ercm_no']) {
							$discount_rate = $couponment_val['ercm_discount_rate'];
						} // end if
					} // end foreach

					// 이벤트 명, 쿠폰 유효기간 가져오기
					foreach($event_arr as $event_key => $event_val) {
						if($event_val['er_no'] == $dvalue_val['erc_er_no']) {
							$event_name = $event_val['er_name'];
							$available_date = $event_val['er_available_date_start']." ~ ".$event_val['er_available_date_end'];
						} // end if
					} // end foreach

					// 사용 날짜, 획득 날짜
					$use_date = get_ymd($dvalue_val['erc_use_date']);
					$get_date = get_ymd($dvalue_val['erc_date']);

					// 회원 정보 가져오기
					$mb_row = mb_get_no($dvalue_val['erc_member']);

					// 사용가능 여부 및 사용 여부
					$erc_state = "불가";
					$erc_use = "미사용";
					$erc_state_class = $erc_use_class = "red";
					
					if($dvalue_val['erc_state'] == "y") {
						$erc_state = "가능";
						$erc_state_class = "blue";
					} // end if

					if($dvalue_val['erc_use'] == "y") {
						$erc_use = "사용";
						$erc_use_class = "blue";
					} // end if
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['erc_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><a href="#" onclick="popup('<?=NM_ADM_URL;?>/members/member_view.php?mb_id=<?=$mb_row['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 550);"><?=$mb_row['mb_id'];?></a></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$event_name;?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$discount_rate;?>%</td>
					<td class="<?=$fetch_row[4][1]?> text_center <?=$erc_state_class;?>"><?=$erc_state;?></td>
					<td class="<?=$fetch_row[5][1]?> text_center <?=$erc_use_class;?>"><?=$erc_use;?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$available_date;?></td>
					<td class="<?=$fetch_row[7][1]?> text_center"><?=$use_date;?></td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=$get_date;?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>
<? rows_page(); ?>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>

<?	/* 해당 페이지에서 사용하는 function */
// 이벤트 총수
function total_event_randombox_coupon()
{
	$sql_event_randombox_coupon_total = "select count(*) as total_event_randombox_coupon from event_randombox_coupon ";
	$total_event_randombox_coupon = sql_count($sql_event_randombox_coupon_total, 'total_event_randombox_coupon');
	return $total_event_randombox_coupon; 
}


?>