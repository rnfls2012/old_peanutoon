<? include_once '_common.php'; // 공통

$er_event_type_arr = array("r"=>"랜덤박스", "f"=>"선착순"); // 이벤트 타입 추가 180227

// 선택박스 검색 데이터 쿼리문 생성
$nm_paras_check = array('er_state');
foreach($nm_paras_check as $nm_paras_key => $nm_paras_val){
	if(${"_".$nm_paras_val} != ''){
		$event_randombox_where.= "and ".$nm_paras_val." = '".${"_".$nm_paras_val}."' ";
	}
}

// 그룹
$sql_group = "";

if($_s_limit == ''){ $_s_limit = 10; }

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "er_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$event_randomobx_order_add = '';
if($_order_field == "er_state"){  $event_randomobx_order_add = " , er_no DESC "; }
$sql_order = "order by ".$_order_field." ".$_order." ".$event_randomobx_order_add ;

// SQL문
$sql_field		= " * ";
$sql_count		= " count(*) as cnt ";

$sql_table		= "select {$sql_field} from event_randombox";
$sql_table_cnt	= "select {$sql_count} from event_randombox";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_table_cnt} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts	= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값

$total_event_randombox = total_event_randombox(); // 이벤트 총수 - function 선언 하단

$th_class = $td_class = "";	// 상단테이블 css-class

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('이벤트 번호', 'er_no', 0));
array_push($fetch_row, array('이벤트 명', 'er_name', 0));
array_push($fetch_row, array('이벤트 타입', 'er_event_type', 0));
array_push($fetch_row, array('할인권 목록', 'er_discount_rate_list', 0));
array_push($fetch_row, array('이벤트 기간', 'er_date', 0));
array_push($fetch_row, array('이벤트 상태', 'er_state', 1));
array_push($fetch_row, array('할인권 유효기간', 'er_available_date', 0));
array_push($fetch_row, array('할인권 상태', 'er_available_state', 0));
array_push($fetch_row, array('등록일', 'er_reg_date', 0));
array_push($fetch_row, array('수정일', 'er_mod_date', 0));
array_push($fetch_row, array('관리', 'er_management', 0));

$page_title = "랜덤박스 이벤트";
$cms_head_title = $page_title;
/* 여기부터 다시 */
$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$cms_page_title;?></h1>
	<div>
		<strong><?=$page_title;?> : <?=number_format($total_event_randombox);?></strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_folder_write;?>','event_randombox_write', <?=$popup_cms_width;?>, 550);"><?=$page_title;?> 등록</button>
	</div>
</section>

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="event_randombox_search_form" id="event_randombox_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return event_randombox_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<? tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
				</div>
			</div>
			<div class="cs_submit event_randombox_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="event_result">
	<h3 id="cr_thead_mg_add">검색 리스트
		<strong>검색 결과 수 : <?=number_format($rows_cnts);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){ $th_title_giho = $order_giho; }
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){ 
					$popup_url = $_cms_write."?er_no=".$dvalue_val['er_no'];

					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					// 할인권 목록
					$coupon_list = implode("%, ", randombox_discount_list_get(randombox_coupon_list_get($dvalue_val['er_no'])))."%";

					// 이벤트 기간
					$er_start_date = $dvalue_val['er_date_start']." (".$dvalue_val['er_date_start_hour'].":00:00)";;
					$er_end_date = $dvalue_val['er_date_end']." (".$dvalue_val['er_date_end_hour'].":59:59)";
					$er_date = $er_start_date."<br/>".$er_end_date;

					// 할인권 유효 기간
					$er_available_date = $dvalue_val['er_available_date_start']." ~ ".$dvalue_val['er_available_date_end'];
					
					// URL
					$event_link = "eventrandombox.php";
					if($dvalue_val['er_event_type'] == "f") {
						$event_link = "eventfullmoon.php";
						if(strpos($dvalue_val['er_name'], "봄") !== false) {
							$event_link = "eventspring.php";
						} // end if
					} // end if
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['er_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><a href="<?=NM_URL.'/'.$event_link;?>"><?=$dvalue_val['er_name'];?></a></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$er_event_type_arr[$dvalue_val['er_event_type']];?></td> <!-- 이벤트 타입 추가 180227 -->
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$coupon_list;?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$er_date;?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$d_state[$dvalue_val['er_state']];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$er_available_date;?></td>
					<td class="<?=$fetch_row[7][1]?> text_center"><?=$d_state[$dvalue_val['er_available_state']];?></td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=$dvalue_val['er_reg_date'];?></td>
					<td class="<?=$fetch_row[9][1]?> text_center"><?=$dvalue_val['er_mod_date'];?></td>
					<td class="<?=$fetch_row[10][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','event_random_mod_wirte', <?=$popup_cms_width;?>, 650);">수정</button>
						<button class="del_random_btn" onclick="popup('<?=$popup_del_url;?>','event_random_del_wirte', <?=$popup_cms_width;?>, 650);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>
<? rows_page(); ?>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>

<?	/* 해당 페이지에서 사용하는 function */
// 이벤트 총수
function total_event_randombox()
{
	$sql_event_randombox_total = "select count(*) as total_event_randombox from event_randombox ";
	$total_event_randombox = sql_count($sql_event_randombox_total, 'total_event_randombox');
	return $total_event_randombox; 
}


?>