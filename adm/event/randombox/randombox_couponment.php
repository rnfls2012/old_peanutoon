<? include_once '_common.php'; // 공통

// 선택박스 검색 데이터 쿼리문 생성
$nm_paras_check = array('ercm_state');
foreach($nm_paras_check as $nm_paras_key => $nm_paras_val){
	if(${"_".$nm_paras_val} != ''){
		$ercm_where.= "and ".$nm_paras_val." = '".${"_".$nm_paras_val}."' ";
	}
}

// 그룹
$sql_group = "";

if($_s_limit == ''){ $_s_limit = 10; }

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "ercm_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$ercm_order_add = '';
if($_order_field == "ercm_state"){  $ercm_order_add = " , ercm_no DESC "; }
$sql_order = "order by ".$_order_field." ".$_order." ".$ercm_order_add ;

// SQL문
$sql_field		= " * ";
$sql_count		= " count(*) as cnt ";

$sql_table		= "select {$sql_field} from event_randombox_couponment";
$sql_table_cnt	= "select {$sql_count} from event_randombox_couponment";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_table_cnt} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts	= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값

$total_ercm = total_ercm(); // 이벤트 총수 - function 선언 하단

$th_class = $td_class = "";	// 상단테이블 css-class

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('할인권 번호', 'ercm_no', 0));
array_push($fetch_row, array('할인율', 'ercm_discount_rate', 0));
array_push($fetch_row, array('확률(가중치)', 'ercm_weight', 0));
array_push($fetch_row, array('할인권 상태', 'ercm_state', 0));
array_push($fetch_row, array('등록일', 'ercm_reg_date', 0));
array_push($fetch_row, array('수정일', 'ercm_mod_date', 0));
array_push($fetch_row, array('관리', 'er_management', 0));

/* 할인권 삭제 제어 */
$select_randombox_event = "SELECT er_no FROM event_randombox WHERE er_state='y'";
$randombox_event_result = sql_query($select_randombox_event);
$ercm_no_array = array();

while($randombox_event_row = sql_fetch_array($randombox_event_result)) {
	array_push($ercm_no_array, randombox_coupon_list_get($randombox_event_row['er_no']));
} // end while

$page_title = "할인권";
$cms_head_title = $page_title;
/* 여기부터 다시 */
$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$cms_page_title;?></h1>
	<div>
		<strong><?=$page_title;?> : <?=number_format($total_ercm);?> 종류</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','ercm_write', <?=$popup_cms_width;?>, 300);"><?=$page_title;?> 등록</button>
	</div>
</section>

<section id="cms_explain">
	<ul>
		<li>할인율은 기존 금액에 x% 대비 할인율입니다. </li>
		<li>확률은 비율로써, 100%를 굳이 만들지 않아도 됩니다. </li>
		<li>더 많이 나오게 하고싶은 할인권에 더 높은 수를 주면 됩니다. </li>
	</ul>
</section><!-- cms_explain -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="event_randombox_couponment_search_form" id="event_randombox_couponment_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return event_randombox_couponment_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
					<? tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
				</div>
			</div>
			<div class="cs_submit event_randombox_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="event_result">
	<h3 id="cr_thead_mg_add">검색 리스트
		<strong>검색 결과 수 : <?=number_format($rows_cnts);?> 종류</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){ $th_title_giho = $order_giho; }
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){ 
					$popup_url = $_cms_write."?ercm_no=".$dvalue_val['ercm_no'];

					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					// 할인권 삭제 제어
					$ercm_del = "y";
					foreach($ercm_no_array as $ercm_no_key => $ercm_no_val) {
						if(in_array($dvalue_val['ercm_no'], $ercm_no_val)) {
							$ercm_del = "n";
						} // end if
					} // end foreach
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['ercm_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['ercm_discount_rate'];?>%</td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['ercm_weight'];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$d_state[$dvalue_val['ercm_state']];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['ercm_reg_date'];?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$dvalue_val['ercm_mod_date'];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','ercm_mod_wirte', <?=$popup_cms_width;?>, 500);">수정</button>
						<? if($ercm_del == "y") { ?>
						<button class="del_random_btn" onclick="popup('<?=$popup_del_url;?>','ercm_del_wirte', <?=$popup_cms_width;?>, 500);">삭제</button>
						<? } // end if ?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>
<? rows_page(); ?>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>

<?	/* 해당 페이지에서 사용하는 function */
// 할인권 총수
function total_ercm()
{
	$sql_ercm_total = "select count(*) as total_ercm from event_randombox_couponment ";
	$total_ercm = sql_count($sql_ercm_total, 'total_ercm');
	return $total_ercm; 
}


?>