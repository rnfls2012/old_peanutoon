<?
include_once '_common.php'; // 공통
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode', 'er_no');
array_push($para_list, 'er_name', 'er_discount_rate_list');
array_push($para_list, 'er_date_start', 'er_date_end', 'er_state');
array_push($para_list, 'er_available_date_start', 'er_available_date_end', 'er_available_state');
array_push($para_list, 'er_reg_date', 'er_mod_date');
array_push($para_list, 'er_date_start_hour', 'er_date_end_hour'); // 시간 추가 180227
array_push($para_list, 'er_event_type'); // 이벤트 타입 추가 180227
array_push($para_list, 'er_first_count'); // 이벤트 타입 추가 180302

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'er_no', 'er_first_count');

/* 빈칸 PARAMITER 허용 */

/* 빈칸&숫자 PARAMITER 허용 */

/* 빈칸 PARAMITER 시 NULL 처리 */

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

// 할인권 번호
$_er_discount_rate_list = implode("||", $_er_discount_rate_list);

// 날짜, 시간 이어 붙이기 180227
$date_start = $_er_date_start." ".$_er_date_start_hour.":00:00";
$date_end = $_er_date_end." ".$_er_date_end_hour.":59:59";

if($date_start > $date_end) {
	pop_close("시작 날짜와 종료 날짜를 확인해 주세요!");
	die;
}

if($_er_available_date_start > $_er_available_date_end) {
	pop_close("할인권 유효기간의 시작 날짜와 종료 날짜를 확인해 주세요!");
	die;
}

// 상태와 날짜 180227
if($date_end < NM_TIME_YMDHIS) {
	$_er_state = "n";
} // end if

if($_er_available_date_end < NM_TIME_YMD) {
	$_er_available_state = "n";
} // end if

if($date_start > NM_TIME_YMDHIS) {
	$_er_state = "r";
} // end if

if($_er_available_date_start > NM_TIME_YMD) {
	$_er_available_state = "r";
} // end if

if($date_start <= NM_TIME_YMDHIS && $date_end > NM_TIME_YMDHIS) {
	$_er_state = "y";
} // end if

if($_er_available_date_start <= NM_TIME_YMD && $_er_available_date_end > NM_TIME_YMD) {
	$_er_available_state = "y";
} // end if

$dbtable = "event_randombox";
$dbt_primary = "er_no";
$para_primary = "er_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';
if($_mode == 'reg'){	
	/* 고정값 */
	$_er_reg_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);
	
	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = '이벤트가 등록되었습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '등록 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

/* 수정 */
}else if($_mode == 'mod'){
	$_er_mod_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $_er_no.'의 데이터가 수정되었습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 데이터 삭제 */
		sql_query("delete from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
		
		$db_result['msg'] = $_er_no.'의 데이터가 삭제되었습니다.';
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
pop_close($db_result['msg'], '', $db_result['error']);
die;

?>