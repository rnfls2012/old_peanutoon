<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_ercm_no = tag_filter($_REQUEST['ercm_no']);

$sql = "SELECT * FROM event_randombox_couponment WHERE ercm_no = '$_ercm_no' ";
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
			 $sql_list = "";
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
	
	$db_comics_list = $db_comics_info = array();
	/* 이벤트 리스트 가져오기 */
	$sql_event_pay_comics = "SELECT * FROM `event_pay_comics` epc left JOIN comics c ON epc.epc_comics = c.cm_no WHERE epc.epc_ep_no = '".$row['ep_no']."' order by epc.epc_order ASC";
	$result_epc = sql_query($sql_event_pay_comics);
	while ($row_epc = sql_fetch_array($result_epc)) {
		array_push($db_comics_list, $row_epc);
	}
}

/* 대분류로 제목 */
$page_title = "랜덤박스 할인권";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- event_free_dc_head -->

<section id="event_write">
	<form name="event_randombox_write_form" id="event_randombox_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return event_randombox_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="ercm_no" name="ercm_no" value="<?=$_ercm_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->
		<input type="hidden" id="ercm_reg_date" name="ercm_reg_date" value="<?=$row['ercm_reg_date'];?>"/><!-- 등록일 -->
		<input type="hidden" id="ercm_mod_date" name="ercm_mod_date" value="<?=$row['ercm_mod_date'];?>"/><!-- 수정일 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ercm_no">할인권 번호</label></th>
					<td><?=$_ercm_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="ercm_discount_rate"><?=$required_arr[0];?>할인율(%)</label></th>
					<td>
						<input type="text" placeholder="할인율을 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="ercm_discount_rate" id="ercm_discount_rate" value="<?=$row['ercm_discount_rate'];?>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="ercm_weight"><?=$required_arr[0];?>확률(비율)</label></th>
					<td>
						<input type="text" placeholder="확률을 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="ercm_weight" id="ercm_weight" value="<?=$row['ercm_weight'];?>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="ercm_state"><?=$required_arr[0];?>사용 여부</label></th>
					<td>
						<div id="er_state" class="input_btn">
							<input type="radio" name="ercm_state" id="ercm_statey" value="y" <?=get_checked('', $row['ercm_state']);?> <?=get_checked('y', $row['ercm_state']);?> />
							<label for="ercm_statey">사용</label>
							<input type="radio" name="ercm_state" id="ercm_staten" value="n" <?=get_checked('n', $row['ercm_state']);?> />
							<label for="ercm_staten">미사용</label>
						</div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ercm_reg_date">등록일</label></th>
					<td><?=$row['ercm_reg_date'];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ercm_mod_date">수정일</label></th>
					<td><?=$row['ercm_mod_date'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- event_free_dc_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>