<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_er_no = tag_filter($_REQUEST['er_no']);

$sql = "SELECT * FROM event_randombox WHERE er_no = '$_er_no' ";
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
			 $sql_list = "";
			 $row['er_date_end_hour'] = 23; // 등록시 마감시간 23로 고정 180227
			 $row['er_available_date_end_hour'] = 23; // 등록시 마감시간 23로 고정 180227
	break;
}

/* 이벤트 타입 */
$er_event_type_arr = array("r"=>"랜덤박스", "f"=>"선착순"); // 이벤트 타입 추가 180227

/* 할인권 가져오기 */
$coupon_arr = randombox_coupon_get(); // 현재 진행 중인 할인권 목록
$er_coupon = array(); // 이벤트에 등록되어 있는 할인권 목록

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
	$er_coupon = randombox_coupon_list_get($row['er_no']);
} // end if

/* 대분류로 제목 */
$page_title = "랜덤박스 이벤트";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- event_free_dc_head -->

<section id="event_write">
	<form name="event_randombox_write_form" id="event_randombox_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return event_randombox_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="er_no" name="er_no" value="<?=$_er_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="er_no">이벤트 번호</label></th>
					<td><?=$_er_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="er_name"><?=$required_arr[0];?>이벤트 명</label></th>
					<td>
						<input type="text" placeholder="이벤트 명을 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="er_name" id="er_name" value="<?=$row['er_name'];?>" autocomplete="off"/>
					</td>
				</tr>
				<!-- 이벤트 타입 추가 180227 -->
				<tr>
					<th><label for="er_event_type"><?=$required_arr[0];?>이벤트 타입</label></th>
					<td>
						<? tag_selects($er_event_type_arr, "er_event_type", $row['er_event_type'], 'n'); ?>	
						<input type="text" placeholder="선착순 마감 숫자를 입력해주세요" name="er_first_count" id="er_first_count" value="<?=$row['er_first_count'];?>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="er_discount_rate_list"><?=$required_arr[0];?>할인권 선택</label></th>
					<td>
						<? foreach($coupon_arr as $coupon_key => $coupon_val) { 
							$checked = "";
							if(in_array($coupon_val['ercm_no'], $er_coupon)) { $checked = "checked='checked'"; }
						?>
						<input type="checkbox" name="er_discount_rate_list[]" id="er_discount_rate_list<?=$coupon_key?>" value="<?=$coupon_val['ercm_no'];?>" <?=$checked;?>/>
						<label for="er_discount_rate_list<?=$coupon_key?>"><?=$coupon_val['ercm_discount_rate'];?>%</label>
						<? } // end foreach ?>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>이벤트 기간</label></th>
					<td>
						<div class="er_date_type_view">
							<span for="s_date">시작일 :</span>
							<input type="text" class="s_date readonly" placeholder="클릭하세요" name="er_date_start" id="s_er_date" value="<?=$row['er_date_start'];?>" autocomplete="off" readonly />
							<?=set_hour('er_date_start_hour', $row['er_date_start_hour'], '시간 :', 'n');?> 00분 <!-- 시간 추가 180227 -->
							<br/>
							<span for="e_date">종료일 :</span>
							<input type="text" class="e_date readonly" placeholder="클릭하세요" name="er_date_end" id="e_er_date" value="<?=$row['er_date_end'];?>" autocomplete="off" readonly />
							<?=set_hour('er_date_end_hour', $row['er_date_end_hour'], '시간 :', 'n');?> 59분 <!-- 시간 추가 180227 -->
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>사용 여부</label></th>
					<td>
						<div id="er_state" class="input_btn">
							<input type="radio" name="er_state" id="er_statey" value="y" <?=get_checked('', $row['er_state']);?> <?=get_checked('y', $row['er_state']);?> />
							<label for="er_statey">사용</label>
							<input type="radio" name="er_state" id="er_staten" value="n" <?=get_checked('n', $row['er_state']);?> />
							<label for="er_staten">미사용</label>
							<input type="radio" name="er_state" id="er_staten" value="r" <?=get_checked('r', $row['er_state']);?> />
							<label for="er_staten">예약</label>
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>할인권 유효기간</label></th>
					<td>
						<div class="er_available_date_type_view">
							<span for="s_date">시작일 :</span>
							<input type="text" class="s_date readonly" placeholder="클릭하세요" name="er_available_date_start" id="s_available_date" value="<?=$row['er_available_date_start'];?>" autocomplete="off" readonly /> ~ 
							<span for="e_date">종료일 :</span>
							<input type="text" class="e_date readonly" placeholder="클릭하세요" name="er_available_date_end" id="e_available_date" value="<?=$row['er_available_date_end'];?>" autocomplete="off" readonly />
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>할인권 사용 여부</label></th>
					<td>
						<div id="er_available_state" class="input_btn">
							<input type="radio" name="er_available_state" id="er_available_statey" value="y" <?=get_checked('', $row['er_available_state']);?> <?=get_checked('y', $row['er_available_state']);?> />
							<label for="er_available_statey">사용</label>
							<input type="radio" name="er_available_state" id="er_available_staten" value="n" <?=get_checked('n', $row['er_available_state']);?> />
							<label for="er_available_staten">미사용</label>
							<!--
							<input type="radio" name="er_available_state" id="er_available_staten" value="r" <?=get_checked('r', $row['er_available_state']);?> />
							<label for="er_available_staten">예약</label>
							-->
						</div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="er_reg_date">등록일</label></th>
					<td><?=$row['er_reg_date'];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="er_mod_date">수정일</label></th>
					<td><?=$row['er_mod_date'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- event_free_dc_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>