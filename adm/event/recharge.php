<?
include_once '_common.php'; // 공통

/* PARAMITER 
er_type //이벤트타입
er_state //이벤트상태
er_calc_way //이벤트 지급 계산법
er_cash_point //이벤트 지급 코인
er_point //이벤트 지급 보너스 코인
date_type // 날짜검색타입
s_date //이벤트시작일
e_date //이벤트마감일
er_reg_date //등록일
er_mod_date //수정일
*/

// 기본적으로 몇개 있는 지 체크;
$sql_recharge_total = "select count(*) as total_recharge from event_recharge where 1  ";
$total_recharge = sql_count($sql_recharge_total, 'total_recharge');

/* 데이터 가져오기 */
$event_recharge_where = '';

// 선택박스 검색 데이터 커리문 생성
$nm_paras_check = array('er_type', 'er_state', 'er_calc_way');
foreach($nm_paras_check as $nm_paras_key => $nm_paras_val){
	if(${"_".$nm_paras_val} != ''){
		$event_recharge_where.= "and ".$nm_paras_val." = '".${"_".$nm_paras_val}."' ";
	}
}

// 시작일/종료일 날짜
$start_date = $end_date = "";
if($_date_type){ //all, upload_date, new_date
	if($_s_date && $_e_date){
		$start_date = $_s_date;
		$end_date = $_e_date;
	}else if($_s_date){
		$start_date = $_s_date;
		$end_date = NM_TIME_YMD;
	}
	switch($_date_type){
		case 'all' : $event_recharge_where.= "and ( er_date_start >= '$start_date' and er_date_end <= '$end_date') ";
		break;

		case 'er_date_start' : $event_recharge_where.= "and er_date_start >= '$start_date' ";
		break;

		case 'er_date_end' : $event_recharge_where.= "and er_date_end <= '$end_date' ";
		break;

		default: $event_recharge_where.= " ";
		break;
	}
}else{
	$_s_date = "";
	$_e_date = "";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "( CASE er_state WHEN 'r' THEN 1 WHEN 'y' THEN 2 WHEN 'n' THEN 3 ELSE 4 END ), er_reg_date "; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$event_recharge_order = "order by ".$_order_field." ".$_order;

$event_recharge_sql = "";
$event_recharge_field = " * "; // 가져올 필드 정하기
$event_recharge_limit = "";

$event_recharge_sql = "SELECT $event_recharge_field FROM event_recharge where 1 $event_recharge_where $event_recharge_order $event_recharge_limit";
$result = sql_query($event_recharge_sql);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','er_no',1));
array_push($fetch_row, array('타입','er_type',1));
array_push($fetch_row, array('시작일','er_date_start',1));
array_push($fetch_row, array('마감일','er_date_end',1));
array_push($fetch_row, array('상태','er_state',1));
array_push($fetch_row, array('충전-지급&할인 방법','er_calc_way',0));
array_push($fetch_row, array('충전-지급&할인 계산','er_calc',2));
array_push($fetch_row, array('등록일','er_reg_date',0));
array_push($fetch_row, array('수정일','er_mod_date',0));
array_push($fetch_row, array('관리','er_management',0));

$cs_calendar_on = $cs_submit_h = '';
if($_date_type){
	$cs_calendar_on = 'cs_calendar_on';
	$cs_submit_h = 'cs_submit_on';
}

$page_title = "충전-지급&할인";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> 이벤트 : 총 <?=number_format($total_recharge);?> 건</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','recharge_wirte', <?=$popup_cms_width;?>, 480);"><?=$page_title?> 등록</button>
	</div>
</section><!-- cms_title -->

<section id="cms_explain">
	<ul>
		<li>같은 이벤트타입의 일정이 중복될경우 최근 등록된 1개만 적용됩니다. </li>
		<li>지급방법이 Percent(%) 경우는 계산시 실수가 될 경우 소수점이하는 버립니다.</li>
		<li><span style='color:red'>통계에서 이벤트 충전/지급이 없다면 삭제 가능하게 하기 - 현재는 기능 체크겸 삭제기능 유지</span></li>
	</ul>
</section><!-- event_recharge_head -->
<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="event_recharge_search_form" id="event_recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return event_recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_er_type_btn">
					<?	tag_selects($nm_config['cf_er_type'], "er_type", $_er_type, 'y', '이벤트타입전체', ''); ?>

					<?	tag_selects($d_state, "er_state", $_er_state, 'y', '상태 전체', ''); ?>
 
				</div>
				<div class="cs_search_btn">
					<?	tag_selects($d_er_calc_way, "er_calc_way", $_er_calc_way, 'y', '지급 방법 전체', ''); ?>
				</div>
				<div class="cs_date_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<?	tag_selects($d_er_date_type, "date_type", $_date_type, 'y', '이벤트날짜 검색안함', ''); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit recharge_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="75" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- event_search -->

<section id="event_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_colspan = $th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 2){
						$th_colspan=" colspan='2' ";
					}
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>" <?=$th_colspan?>><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?er_no=".$dvalue_val['er_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					$er_calc_text = $er_calc_colspan = '';

					$er_cash_point_text = $nm_config['cf_cash_point_unit_ko'].' ';
					$er_point_text = $nm_config['cf_point_unit_ko'].' ';

					$er_state_type = "충전-지급";

					if($dvalue_val['er_calc_way'] == 'a'){
						$er_cash_point_text.= '+ '.number_format($dvalue_val['er_cash_point']).' '.$nm_config['cf_cash_point_unit'];
						$er_point_text.= '+ '.number_format($dvalue_val['er_point']).' '.$nm_config['cf_point_unit'];
					}else if($dvalue_val['er_calc_way'] == 'p'){
						$er_cash_point_text.= number_format($dvalue_val['er_cash_point']).' %';
						$er_point_text.= number_format($dvalue_val['er_point']).' %';
					}else if($dvalue_val['er_calc_way'] == 'm'){
						$er_cash_point_text.= 'x '.number_format($dvalue_val['er_cash_point']);
						$er_point_text.= 'x '.number_format($dvalue_val['er_point']);
					}else{
						$er_calc_colspan = " colspan='2' ";
						$er_calc_text = "가격 할인 ".number_format($dvalue_val['er_dc_percent']).' %';
						$er_state_type = "충전-할인";
					}

					// 등록일
					$db_er_reg_date_ymd = get_ymd($dvalue_val['er_reg_date']);
					$db_er_reg_date_his = get_his($dvalue_val['er_reg_date']);

					// 수정일
					$db_er_mod_date_ymd = get_ymd($dvalue_val['er_mod_date']);
					$db_er_mod_date_his = get_his($dvalue_val['er_mod_date']);


					$db_er_no_date_text = "무기간";
					if($dvalue_val['er_date_type'] != 'n' && $dvalue_val['er_date_start'] == '' && $dvalue_val['er_date_end'] == ''){
						$db_er_no_date_text = "시작일&마감일 없어서 종료됩니다.";
					}
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['er_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$nm_config['cf_er_type'][$dvalue_val['er_type']];?></td>
					<?if($dvalue_val['er_date_type'] != 'n' && $dvalue_val['er_date_start'] != '' && $dvalue_val['er_date_end'] != ''){?>
						<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['er_date_start'];?></td>
						<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['er_date_end'];?></td>
					<?}else{?>
						<td colspan='2' class="er_date_no text_center"><?=$db_er_no_date_text?></td>
					<?}?>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$er_state_type;?> <?=$d_state[$dvalue_val['er_state']];?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$d_er_calc_way[$dvalue_val['er_calc_way']];?></td>
					<?if($er_calc_colspan==""){?>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$er_cash_point_text;?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$er_point_text;?></td>
					<?}else{?>
					<td class="<?=$fetch_row[6][1]?> text_center" <?=$er_calc_colspan;?>><?=$er_calc_text;?></td>
					<?}?>

					<td class="<?=$fetch_row[7][1]?> text_center"><?=$db_er_reg_date_ymd;?><br/><?=$db_er_reg_date_his;?></td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=$db_er_mod_date_ymd;?><br/><?=$db_er_mod_date_his;?></td>
					<td class="<?=$fetch_row[9][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','recharge_mod_wirte', <?=$popup_cms_width;?>, 500);">수정</button>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','recharge_del_wirte', <?=$popup_cms_width;?>, 500);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- event_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>