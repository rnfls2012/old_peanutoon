<?
include_once '_common.php'; // 공통
/* _array_update.php 처리 불가능 */

$db_result['state'] = 0;
$db_result['msg'] = '';

$attend_month = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
$attend_fetch = array('ea_month', 'ea_use', 'ea_start_date', 'ea_end_date', 'ea_oneday_point', 'ea_cont_date_1', 'ea_cont_point_1', 'ea_cont_date_2', 'ea_cont_point_2', 'ea_cont_date_3', 'ea_cont_point_3', 'ea_cont_use', 'ea_regu_date', 'ea_regu_point', 'ea_regu_use');

/* _변수에 담기 */
foreach($attend_month as $month_key => $month_val) {
	foreach($attend_fetch as $fetch_key => $fetch_val) {
		${'_'.$fetch_val.'_'.$month_val} = tag_filter($_REQUEST[$fetch_val.'_'.$month_val]);
	} // end foreach
} // end foreach

/* 데이터 확인 */
$sql_count = "select count(*) as attend_count from event_attend2";
$attend_count = sql_count($sql_count, "attend_count");

if($attend_count == 0) { // 없으면 INSERT
	foreach($attend_month as $month_key => $month_val) {
		$sql_insert = "INSERT INTO event_attend2(";

		foreach($attend_fetch as $fetch_key => $fetch_val) {
			if(${'_'.$fetch_val.'_'.$month_val} != "") {
				$sql_insert .= $fetch_val.", "; 
			} // end if
		} // end foreach

		$sql_insert = substr($sql_insert, 0, strrpos($sql_insert,", "));
		$sql_insert .= ") VALUES('";

		foreach($attend_fetch as $fetch_key => $fetch_val) {
			if(${'_'.$fetch_val.'_'.$month_val} != "") {
				$sql_insert .= ${'_'.$fetch_val.'_'.$month_val}."', '";
			} // end if
		} // end foreach

		$sql_insert = substr($sql_insert, 0, strrpos($sql_insert,", "));
		$sql_insert .= ")";

		if(!sql_query($sql_insert)) {
			$db_result['state'] = 1;
			$db_result['msg'] = $month_val.'월이 저장되지 않았습니다.\n관리자에게 문의하세요!\n'.$sql_insert;
			echo $db_result['msg'];
			die;
		} // end if
	} // end foreach
} else { // 있으면 UPDATE
	foreach($attend_month as $month_key => $month_val) {
		$sql_update = "UPDATE event_attend2 SET ";

		foreach($attend_fetch as $fetch_key => $fetch_val) {
			if(${'_'.$fetch_val.'_'.$month_val} == "" && ($fetch_val == "ea_oneday_point" || $fetch_val == "ea_cont_date_1" || $fetch_val == "ea_cont_point_1" || $fetch_val == "ea_cont_date_2" || $fetch_val == "ea_cont_point_2" || $fetch_val == "ea_cont_date_3" || $fetch_val == "ea_cont_point_3")) {
				$sql_update .= $fetch_val." = NULL, "; 
			} else if(${'_'.$fetch_val.'_'.$month_val} == "" && ($fetch_val == "ea_use" || $fetch_val == "ea_cont_use" || $fetch_val == "ea_regu_use")) {
				$sql_update .= $fetch_val." = 'n', "; 
			} else if(${'_'.$fetch_val.'_'.$month_val} != "" && $fetch_val != "ea_month") {
				$sql_update .= $fetch_val." = '".${'_'.$fetch_val.'_'.$month_val}."', "; 
			} // end elseif
		} // end foreach

		$sql_update = substr($sql_update, 0, strrpos($sql_update,", "));
		$sql_update .= " WHERE ea_month = '".$month_val."'";
		
		/* 연속출석일이 개근일보다 크거나 같을때 취소 */
		if((${'_ea_cont_date_1_'.$month_val} != "" || ${'_ea_cont_date_2_'.$month_val} != "" || ${'_ea_cont_date_3_'.$month_val} != "") && (${'_ea_regu_date_'.$month_val} <= ${'_ea_cont_date_1_'.$month_val} || ${'_ea_regu_date_'.$month_val} <= ${'_ea_cont_date_2_'.$month_val} || ${'_ea_regu_date_'.$month_val} <= ${'_ea_cont_date_3_'.$month_val})) {
			alert($month_val."월의 연속출석일수가 개근일보다 크거나 같습니다.", $_SERVER['HTTP_REFERER']);
			die;
		} // end if

		if(!sql_query($sql_update)) {
			$db_result['state'] = 1;
			$db_result['msg'] = $month_val.'월이 수정되지 않았습니다.\n관리자에게 문의하세요!\n'.$sql_update;
			echo $db_result['msg'];
			die;
		} // end if
	} // end foreach
} // end else

alert(NM_TIME_YMDHIS.' 출석이벤트 내용이 적용되였습니다.',$_SERVER['HTTP_REFERER']);
?>