<?
include_once '_common.php'; // 공통

$comics_search = base_filter($_REQUEST['comics_search']);
$comics_list_except = base_filter($_REQUEST['comics_list_except']);
$sql_comics_list_except = '';
if($comics_list_except != ''){
	$sql_comics_list_except = "AND cm_no not in ($comics_list_except) ";
}
if($comics_search != ''){
	// LIMIT 걸려있어서, 해제함 (단행본 다 안 나옴) 170828
	// $sql = "SELECT * FROM  `comics` WHERE `cm_series` LIKE '%".$comics_search."%' $sql_comics_list_except LIMIT 0 , 10 ";
	$sql = "SELECT * FROM  `comics` WHERE `cm_series` LIKE '%".$comics_search."%' $sql_comics_list_except ";
	$result = sql_query($sql);
	for ($i=0; $row=sql_fetch_array($result); $i++) {
		$up_kind_text = '화';
		if($row['project'] == 1){ $up_kind_text = '권'; }
		$para_result[$i]['comics'] = $row['cm_no'];
		$para_result[$i]['series'] = $row['cm_series'];
		$para_result[$i]['adult'] = $d_adult[$row['cm_adult']];
		$para_result[$i]['pay_unit'] = $nm_config['cf_cash_point_unit'];
		$para_result[$i]['pay'] = cash_point_view($row['cm_pay'], 'y','n');
		$para_result[$i]['pay_view'] = cash_point_view($row['cm_pay']);
		$para_result[$i]['episode_total'] = $row['cm_episode_total'];
		$para_result[$i]['episode_total_view'] = number_format($row['cm_episode_total']).$d_cm_up_kind[$row['cm_up_kind']];
	}
	header( "content-type: application/json; charset=utf-8" );
	echo json_encode($para_result);
}
die;
?>