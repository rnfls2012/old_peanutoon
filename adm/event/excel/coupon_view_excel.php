<?php
$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 해당 시트 당 컬럼 크기 변경

$worksheet->set_column(0, 1, 10);
$worksheet->set_column(1, 5, 23);

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));
								
$left  = $workbook -> addformat(array(
								'align' => 'left',
								'border' => 1
								));

$center  = $workbook -> addformat(array(
								'align' => 'center',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));

//문서 출력일자 셀 출력
$worksheet->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt = 3; //합계 데이터 출력 행 cnt.

/***************  본문 결과 출력   **************/

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('발급번호','ec_no'));
array_push($fetch_row, array('쿠폰코드','ec_code_hyphen'));
array_push($fetch_row, array('사용자','ec_member'));
array_push($fetch_row, array('사용날짜','ec_use_date'));
array_push($fetch_row, array('쿠폰코드(-제거)','ec_code'));

$notice_arr = array();
array_push($notice_arr, array('쿠폰번호', $couponment_row['ecm_no']));
array_push($notice_arr, array('지급'.$nm_config['cf_cash_point_unit_ko'], $couponment_row['ecm_cash_point']));
array_push($notice_arr, array('지급'.$nm_config['cf_point_unit_ko'], $couponment_row['ecm_point']));
array_push($notice_arr, array('유효기간', $couponment_row['ecm_end_date']));
array_push($notice_arr, array('발급 수', $couponment_row['ecm_lot']));
array_push($notice_arr, array('중복 사용 여부', $couponment_row['ecm_overlap']=='y'?'허용':'불가'));
array_push($notice_arr, array('상태', $couponment_row['ecm_state']=='y'?'사용':'중지'));

$notice_head = array();
foreach ($notice_arr as $key => $val) {
	array_push($notice_head, $val[0]);
}

$notice_cont = array();
foreach ($notice_arr as $key => $val) {
	array_push($notice_cont, $val[1]);
}

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
} // end foreach

$temp_1 = 3; // 해당 쿠폰의 정보 (행) 출력위한 임시 변수.
$notice_head = array_map('iconv_cp949', $notice_head);
foreach($notice_head as $cell) {
	$worksheet->write($temp_1++, 0, $cell, $heading);
	$row_cnt++;
} // end foreach

$temp_2 = 3; // 해당 쿠폰의 정보 (행) 출력위한 임시 변수.
$notice_cont = array_map('iconv_cp949', $notice_cont);
foreach($notice_cont as $cell) {
	$worksheet->write($temp_2++, 1, $cell, $right);
} // end foreach

$row_cnt++;

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write($row_cnt, $col++, $cell, $heading);
} // end foreach

$row_cnt++;

/* 데이터 전달 부분 */
for ($i=$row_cnt; $row=mysql_fetch_array($result); $i++) {
	
	$mb_row = mb_get_no($row['ec_member']);
	
	foreach($data_key as $cell_key => $cell_val) {
		switch($cell_val) {				
			case "ec_code_hyphen" :
				$ec_code_hyphon = $row['ec_code'];
				
				for ($j=4; $j<strlen($ec_code); $j+=5) {
					$ec_code_hyphon = substr_replace($ec_code_hyphon, "-", $j, 0);
				}
				
				$worksheet->write($i, $cell_key , iconv_cp949($ec_code_hyphon), $left);
				break;
			
			case "ec_code" :
				$ec_code = $row['ec_code'];
				$worksheet->write($i, $cell_key , iconv_cp949($ec_code), $left);
				break;
				
			case "ec_member" :
				$ec_member = $mb_row['mb_email'];
				$worksheet->write($i, $cell_key , iconv_cp949($ec_member), $center);
				break;
			
			default :
				$worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]), $center);
				break;
		}
	} // end foreach
	$row_cnt++;
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."coupon_view_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"coupon_view_".$today.".xls");
header("Content-Disposition: inline; filename=\"coupon_view_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;