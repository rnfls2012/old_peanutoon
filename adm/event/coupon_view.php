<?
include_once '_common.php'; // 공통

/* PARAMITER */
if(!$_ecm_no){
	alert("정보를 받아올 수 없습니다.", $_SERVER['HTTP_REFERER']);
	die;
} // end if

/* DB */
// 쿠폰 내용
$couponment_sql = "select * from event_couponment where ecm_no = '".$_ecm_no."'";
$couponment_row = sql_fetch($couponment_sql);

// 개별 쿠폰
$coupon_sql = "select * from event_coupon where ec_ecm_no = '".$_ecm_no."' order by ec_member desc, ec_use_date desc";
$coupon_result = sql_query($coupon_sql);
$row = sql_fetch($coupon_sql);
$ec_code = $row['ec_code'];

// 사용/중지
$couponment_overlap = "불가";
$couponment_overlap_class = "stop";
if($couponment_row['ecm_overlap'] == 'n') {
	$couponment_overlap = "가능";
	$couponment_overlap_class = "start";
} // end if

$couponment_use = "중지";
$couponment_use_class = "stop";
if($couponment_row['ecm_state'] == 'n') {
	$couponment_use = "재개";
	$couponment_use_class = "start";
} // end if

/* 필수 속성 */
$cms_head_title = $couponment_row['ecm_ticket'];

//엑셀 파일 다운로드 시 (query_string 으로 값 전달받음.)
if($_mode == 'excel') {
	$result = sql_query($coupon_sql); //연도 별 쿼리 실행 result
	include_once $_cms_folder_path.'/excel/coupon_view_excel.php';
}

$head_title = "엠짱-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
	<div class="couponment">
		쿠폰 번호 : <?=$couponment_row['ecm_no'];?> <br/>
		지급 <?=$nm_config['cf_cash_point_unit_ko'];?> : <?=$couponment_row['ecm_cash_point'];?> <?=$nm_config['cf_cash_point_unit_ko'];?> <br/>
		지급 <?=$nm_config['cf_point_unit_ko'];?> : <?=$couponment_row['ecm_point'];?> <?=$nm_config['cf_point_unit_ko'];?> <br/>
		유효 기간 : 지급 <?=$couponment_row['ecm_end_date'];?> 까지 <br/>
		발급 수 : <?=$couponment_row['ecm_lot'];?> <br/>
		중복 사용 여부 : <?=$couponment_row['ecm_overlap']=='y'?'허용':'불가';?> <br/>
		상태 : <?=$couponment_row['ecm_state']=='y'?'사용':'중지';?>
	</div>
	<? if($couponment_row['ecm_end_date'] > NM_TIME_YMDHIS) { ?>
	<div class="couponment_btn">		
		<div class="stop_use">
		<form name="event_couponment_view_form" id="event_couponment_view_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return event_couponment_view_submit();">
			<input type="hidden" id="ecm_mode" name="ecm_mode" value="">
			<input type="hidden" name="ecm_no" value="<?=$couponment_row['ecm_no'];?>">
			<!--<button id="ecm_overlap" class="<?=$couponment_overlap_class;?>">중복 사용 <?=$couponment_overlap?></button>-->
			<button id="ecm_state" class="<?=$couponment_use_class?>">사용 <?=$couponment_use?></button>
		</form>		
		</div>		
		
		
		<div class="excel">
			<input type="button" class="excel_down" value="<?=$cms_head_title?> 엑셀 다운로드" onclick="location.href='<?=$_cms_self?>?<?=$_nm_paras;?>&mode=excel'"/>
		</div>
		
		
		
	</div>
	<? } // end if ?>
</section> <!-- cms_page_title -->

<section id="coupon_view_info">
	<table>
		<tr>
			<th>발급 번호</th>
			<th>쿠폰 코드</th>
			<th>사용자</th>
			<th>사용 날짜</th>
		</tr>
	<? while($row = sql_fetch_array($coupon_result)) { 
			$mb_row = mb_get_no($row['ec_member']);
			$ec_code = $row['ec_code'];
			for($i=4; $i<strlen($ec_code); $i+=5) {
				$ec_code = substr_replace($ec_code, "-", $i, 0);
			} // end for ?>
		<tr>
			<td><?=$row['ec_no'];?></td>
			<td><?=$ec_code;?></td>
			<td>
				<a href="#" onclick="popup('../members/member_view.php?mb_id=<?=$mb_row['mb_id'];?>','member_view', <?=$popup_cms_width;?>, 550);">
					<?=$mb_row['mb_email'];?>
				</a>
			</td>
			<td><?=$row['ec_use_date'];?></td>
		</tr>
	<? } //end while ?>
	</table>
	<div class="close_btn">
		<input type="button" onclick="javascript:self.close()" value="닫기"/>
	</div>
</section>
