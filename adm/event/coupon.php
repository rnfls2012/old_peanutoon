<?
include_once '_common.php'; // 공통

// 기본적으로 몇개 있는 지 체크
$sql_coupon_total = "select count(*) as total_event_coupon from event_couponment where 1  ";
$total_coupon = sql_count($sql_coupon_total, 'total_event_coupon');

/* 데이터 가져오기 */
$event_couponment_where = '';

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "ecm_start_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$event_couponment_order = "order by ".$_order_field." ".$_order;

$event_couponment_sql = "";
$event_couponment_field = " * "; // 가져올 필드 정하기
$event_couponment_limit = "";

$event_couponment_sql = "SELECT $event_couponment_field FROM event_couponment where 1 $event_couponment_where $event_couponment_order $event_couponment_limit";
$result = sql_query($event_couponment_sql);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','ecm_no',0));
array_push($fetch_row, array('발급일','ecm_start_date',1));
array_push($fetch_row, array('지급 '.$nm_config['cf_cash_point_unit_ko'],'ecm_cash_point',0));
array_push($fetch_row, array('지급 '.$nm_config['cf_point_unit_ko'],'ecm_point',0));
array_push($fetch_row, array('발급 쿠폰','ecm_lot',0));
array_push($fetch_row, array('사용 쿠폰','ecm_use',0));
array_push($fetch_row, array('유효 기간','ecm_end_date',0));
array_push($fetch_row, array('사용처','ecm_ticket',0));
array_push($fetch_row, array('중복 사용','ecm_overlap',0));
array_push($fetch_row, array('상태','ecm_state',0));
array_push($fetch_row, array('관리','ecm_admin',0));

$page_title = "쿠폰";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> : 총 <?=number_format($total_coupon);?> 건</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','coupon_write', <?=$popup_cms_width;?>, 450);"><?=$page_title?> 발행</button>
	</div>
</section>

<section id="event_result">
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<? foreach($row_data as $dvalue_key => $dvalue_val) { 
						$use_count = sql_count("select count(*) as count_event_coupon from event_coupon where ec_ecm_no='".$dvalue_val['ecm_no']."' 
						and ec_member != 0 and ec_member_idx != ''", "count_event_coupon");?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['ecm_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['ecm_start_date'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['ecm_cash_point'];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['ecm_point'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['ecm_lot'];?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$use_count;?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$dvalue_val['ecm_end_date']." 까지";?></td>
					<td class="<?=$fetch_row[7][1]?> text_center"><?=$dvalue_val['ecm_ticket'];?></td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=$dvalue_val['ecm_overlap']=='y'?'허용':'불가';?></td>
					<td class="<?=$fetch_row[9][1]?> text_center"><?=$dvalue_val['ecm_state']=='y'?'사용':'중지';?></td>
					<td class="<?=$fetch_row[10][1]?> text_center">
						<button class="view_btn" onclick="popup('coupon_view.php?ecm_no=<?=$dvalue_val['ecm_no'];?>','coupon_view', <?=$popup_cms_width;?>, 700);">상세 내역</button>
					</td>
				</tr>
				<? } ?>
			</tbody>
		</table>
	</div>
</section><!-- event_recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>