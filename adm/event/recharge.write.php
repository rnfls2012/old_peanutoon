<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_er_no = tag_filter($_REQUEST['er_no']);

$sql = "SELECT * FROM `event_recharge` WHERE  `er_no` = '$_er_no' ";
	
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}
/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "충전-지급&할인";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- event_recharge_head -->

<section id="event_write">
	<form name="event_recharge_write_form" id="event_recharge_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return event_recharge_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="er_no" name="er_no" value="<?=$_er_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="er_no">이벤트 번호</label></th>
					<td><?=$_er_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th>
						<label for="er_type"><?=$required_arr[0];?>이벤트 타입</label>
						<p><a href="<?=NM_ADM_URL?>/cmsconfig/env/basic.php" target="_blank">이벤트설정링크</a></p>
					</th>
					<td><? tag_selects($nm_config['cf_er_type'], "er_type", $row['er_type'], 'n'); ?></td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>이벤트 기간</label></th>
					<td>
						<div id="service_color" class="input_btn">
							<input type="radio" name="er_date_type" id="er_date_type0" value="y" <?=get_checked('', $row['er_date_type']);?> <?=get_checked('y', $row['er_date_type']);?>  onclick="er_date_type_chk(this.value);" />
							<label for="er_date_type0">기간만적용</label>
							<input class="still" type="radio" name="er_date_type" id="er_date_type1" value="m" <?=get_checked('m', $row['er_date_type']);?> onclick="er_date_type_chk(this.value);" />
							<label class="still" for="er_date_type1">매월(날짜빈칸시 무기간)</label>
							<input class="still" type="radio" name="er_date_type" id="er_date_type2" value="w" <?=get_checked('w', $row['er_date_type']);?> onclick="er_date_type_chk(this.value);" />
							<label class="still" for="er_date_type2">매주(날짜빈칸시 무기간)</label>
							<input type="radio" name="er_date_type" id="er_date_type3" value="n" <?=get_checked('n', $row['er_date_type']);?> onclick="er_date_type_chk(this.value);" />
							<label for="er_date_type3">무기간</label>
						</div>
						<div class="<? echo ($row['er_date_type'] != 'n')?"":"still";?> er_date_type_view margin_top10">
							<span for="s_date">시작일 :</span>
							<input type="text" class="s_date readonly" placeholder="클릭하세요" name="er_date_start" id="s_date" value="<?=$row['er_date_start'];?>" autocomplete="off" readonly /> ~ 
							<span for="e_date">종료일 :</span>
							<input type="text" class="e_date readonly" placeholder="클릭하세요" name="er_date_end" id="e_date" value="<?=$row['er_date_end'];?>" autocomplete="off" readonly />
						</div>
						<div class="er_holiday_view still margin_top10">
							<span for="er_holiweek">매월</span>
							<?	tag_selects($day_list, "er_holiday", $row['er_holiday'], 'y', '사용안함', ''); ?>
						</div>
						<div class="er_holiweek_view still margin_top10">
							<span for="er_holiweek">매주</span>
							<?	tag_selects($week_list, "er_holiweek", $row['er_holiweek'], 'y', '사용안함', ''); ?>
						</div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="er_state">이벤트 상태</label></th>
					<td><?=$d_state[$row['er_state']];?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="er_calc_way"><?=$required_arr[0];?>지급방법</label></th>
					<td>
						<?	tag_selects($d_er_calc_way, "er_calc_way", $row['er_calc_way'], 'n'); ?>
					</td>
				</tr>
				<tr>
					<th><label for="er_cash_point">지급 <?=$nm_config['cf_cash_point_unit_ko'];?>(<?=$nm_config['cf_cash_point_unit'];?>)</label></th>
					<td>
						<input type="text" class="onlynumber text_rt_130" placeholder="숫자만 입력해주세요" name="er_cash_point" id="er_cash_point" value="<?=$row['er_cash_point'];?>" autocomplete="off" maxlength="5" /> 
						<label for="er_cash_point"><?=$nm_config['cf_cash_point_unit'];?></label>
					</td>
				</tr>
				<tr>
					<th><label for="er_point">지급 <?=$nm_config['cf_point_unit_ko'];?>(<?=$nm_config['cf_point_unit'];?>)</label></th>
					<td>
						<input type="text" class="onlynumber text_rt_130" placeholder="숫자만 입력해주세요" name="er_point" id="er_point" value="<?=$row['er_point'];?>" autocomplete="off" maxlength="5" /> 
						<label for="er_point"><?=$nm_config['cf_point_unit'];?></label>
					</td>
				</tr>
				<tr>
					<th><label for="er_dc_percent">가격 할인 (%)</label></th>
					<td>
						<input type="text" class="onlynumber text_rt_130" placeholder="숫자만 입력해주세요" name="er_dc_percent" id="er_dc_percent" value="<?=$row['er_dc_percent'];?>" autocomplete="off" maxlength="5" /> 
						<label for="er_dc_percent">%</label>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="er_state">이벤트 등록일</label></th>
					<td><?=$row['er_reg_date'];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="er_state">이벤트 수정일</label></th>
					<td><?=$row['er_mod_date'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- event_recharge_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>