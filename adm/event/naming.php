<?
include_once '_common.php'; // 공통

/* PARAMITER 

*/

// 기본적으로 몇개 있는 지 체크;
$sql_naming_total = "select count(*) as total_naming from event_naming where 1";
$total_naming = sql_count($sql_naming_total, 'total_naming');

$naming_where = "";

// 검색단어+ 검색타입
if($_s_text){
	switch($_s_type){
		case '0': $naming_where.= "and en_s_name like '%$_s_text%' ";
		break;
		case '1': $naming_where.= "and en_p_name like '%$_s_text%'";
		break;
		case '2': $naming_where.= "and en_mp_name like '%$_s_text%' ";
		break;
		case '3': $naming_where.= "and en_mb_id like '%$_s_text%' ";
		break;
		default: $naming_where.= "and ( en_s_name like '%$_s_text%' or en_p_name like '%$_s_text%' or en_mp_name like '%$_s_text%' or en_mb_id like '%$_s_text%' )";
		break;
	}
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "en_reg_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$naming_order = "order by ".$_order_field." ".$_order;

$naming_sql = "";
$naming_field = " * "; // 가져올 필드 정하기
$naming_limit = "";

$naming_sql = "select $naming_field from event_naming where 1 $naming_where $naming_order $naming_limit";
$result = sql_query($naming_sql);
$row_size = sql_num_rows($result);

if($_mode == 'excel') {
	include_once './excel/naming_excel.php';
} // end if

/*
echo "<br/>";
echo $comics_sql;
echo "<br/>";
*/

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','en_no',0));
array_push($fetch_row, array('회원 아이디','en_mb_id',0));
array_push($fetch_row, array('다람쥐 이름','en_s_name',0));
array_push($fetch_row, array($nm_config['cf_cash_point_unit_ko'].' 이름','en_p_name',0));
array_push($fetch_row, array($nm_config['cf_point_unit_ko'].' 이름','en_mp_name',0));
array_push($fetch_row, array('등록 날짜','en_reg_date',1));

$cms_head_title = "네이밍 이벤트";

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div class="write">
		<input type="button" class="excel_down" value="검색 결과 엑셀 다운로드"
			onclick="location.href='<?=$_cms_self?>?s_date=<?=$_s_date;?>&e_date=<?=$_e_date;?>&order_field=<?=$_order_field;?>&order=<?=$_order;?>&mode=excel'"/>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="cms_search_form" id="cms_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return cms_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_s_type_btn">
				<?	$s_type_list = array('다람쥐 이름',$nm_config['cf_cash_point_unit_ko'].' 이름', $nm_config['cf_point_unit_ko'].' 이름', '아이디');
					tag_radios($s_type_list, "s_type", $_s_type, 'y', '전체', ''); ?>
				</div>
				<div class="cs_search_btn">
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit naming_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="event_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<? foreach($row_data as $dvalue_key => $dvalue_val){	?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['en_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['en_mb_id'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['en_s_name'];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['en_p_name'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['en_mp_name'];?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$dvalue_val['en_reg_date'];?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- cms_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>