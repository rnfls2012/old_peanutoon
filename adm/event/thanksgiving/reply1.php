<?php
include_once '_common.php'; // 공통

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호', 'mb_no', 0));
array_push($fetch_row, array('회원 ID' ,'mb_id', 0));
array_push($fetch_row, array('이름', 'mb_name', 0));
array_push($fetch_row, array('응원글', 'comments', 0));
array_push($fetch_row, array('SNS 타입', 'mb_sns_type', 0));
array_push($fetch_row, array('블라인드 유무', 'blind_YN', 0));
array_push($fetch_row, array('등록일','reg_date', 1));
array_push($fetch_row, array('블라인드 전환일','blind_date', 0));
array_push($fetch_row, array('관리','management',0));
array_push($fetch_row, array('미니땅콩 지급 유무', 'reward_YN', 0));
$mb_sns_type = array(
    'nid' => '네이버',
    'fcb' => '페이스북',
    'kko' => '카카오톡',
    'twt' => '트위터'
);

$blind_YN = array(
    'y' => '블라인드',
    'n' => ''
);

$reward_YN = array(
    'y' => '지급',
    'n' => ''
);

$cs_calendar_on = $cs_submit_h = '';

$page_title = "신작 기대평 댓글";
$cms_head_title = $page_title;

/**
 * START SQL 작업
 */
// SQL문
$sql_field		= " * ";
$sql_count		= "COUNT(*) AS cnt ";
$sql_where      = "WHERE 1 ";

// 정렬
if($_order_field == null || $_order_field == ""){
    $_order_field = "reg_date";
}
if($_order == null || $_order == ""){
    $_order = "DESC";
}

$sql_order 		= "ORDER BY ".$_order_field." ".$_order;


// 검색단어+ 검색타입
if($_s_text){
    switch($_s_type){
        case '0': $sql_where.= "and mb_name like '%$_s_text%' ";
            break;

        case '1': $sql_where.= "and mb_id like '%$_s_text%'";
            break;

        case '2': $sql_where.= "and comments like '%$_s_text%' ";
            break;

        default: $sql_where.= "and ( mb_id like '%$_s_text%' or mb_id like '%$_s_text%' or comments like '%$_s_text%' )";
            break;
    }
}

// 최근 로그인/탈퇴일 날짜
$start_date = $end_date = "";
if($_date_type) { //all, upload_date, new_date

    $cs_calendar_on = 'cs_calendar_on';
    $cs_submit_h = 'cs_submit_on';

    if($_s_date && $_e_date) {
        $start_date = $_s_date." ".NM_TIME_HI_start;
        $end_date = $_e_date." ".NM_TIME_HI_end;
    } else if($_s_date) {
        $start_date = $_s_date." ".NM_TIME_HI_start;
        $end_date = NM_TIME_YMD." ".NM_TIME_HI_end;
    }

    switch($_date_type){
        case 'mb_login_date' :
            $sql_where.= "and mb_login_date >= '$start_date' and mb_login_date <= '$end_date'";
            $_order_field = "mb_login_date";
            break;

        case 'mb_join_date' :
            $sql_where.= "and mb_join_date >= '$start_date' and mb_join_date <= '$end_date'";
            $_order_field = "mb_join_date";
            break;

        case 'mb_out_date' :
            $sql_where.= "and mb_out_date >= '$start_date' and mb_out_date <= '$end_date'";
            $_order_field = "mb_out_date";
            break;

        default: $sql_where.= "";
            break;
    }
} else {
    $_s_date = "";
    $_e_date = "";
}

// sns 종류
if($_mb_sns_type){
    switch($_mb_sns_type){
        case 'nid': $sql_where .= "AND mb_sns_type = 'nid' ";
            break;
        case 'fcb': $sql_where .= "AND mb_sns_type = 'fcb' ";
            break;
        case 'twt': $sql_where .= "AND mb_sns_type = 'twt' ";
            break;
        case 'kko': $sql_where .= "AND mb_sns_type = 'kko' ";
            break;
        case '': $sql_where .= "AND mb_sns_type = '' ";
            break;
    }
}

$sql_table		= "
	SELECT $sql_field
	FROM event_thanksgiving_reply AS ec 
	LEFT JOIN member ON ec.mb_no = member.mb_no
";

$sql_table_cnt	= "
	SELECT $sql_count
	FROM event_thanksgiving_reply AS ec 
	LEFT JOIN member ON ec.mb_no = member.mb_no
";


$sql_query		= " {$sql_table} {$sql_where} {$sql_group} {$sql_order} ";
$sql_query_cnt	= " {$sql_table_cnt} {$sql_where} {$sql_group}  ";

// SQL처리
$rows_cnt	= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data	= rows_data($sql_query);		// SQL문 결과 레코드값

/**
 * END SQL 작업
 */

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>

	<link rel="stylesheet" type="text/css" href="<?=$_cms_css?><?=vs_para()?>"/>
	<script type="text/javascript" src="<?=$_cms_js?><?=vs_para()?>"></script>
	<script type="text/javascript">
	<!--
		function set_event_thanksgiving_Blind(mb_no) {
			var boolean = confirm("상태를 변경하시겠습니까?");

			if (boolean) {
				$.ajax({
					url: './reply1_ajax.php',
					method: "POST",
					data: {mb_no: mb_no}
				})
					.done(function (data) {
					// success

					/**
					 * TODO : process callback data
					 */
					alert(data);
					location.reload();
					})
					.fail(function () {
						// fail
						alert('관리자에게 문의 바랍니다.');
					})
					.always(function () {
						// always execute
					})

			}
		}
	//-->
	</script>

	<section id="cms_title">
		<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
		<div>
			<strong><?=$page_title?> : 총 <?=number_format($rows_cnt)?> 건</strong>
		</div>
		<div class="write">
			<!-- <button onclick="popup('<?=NM_ADM_URL;?>/event/comments.write.php','comments_give', 560, 280);">땅콩 일괄지급</button> -->
			
			<a href="<?=$_cms_folder;?>/reply.php?sl_mode=1">신작 기대평 댓글 관리</a>
			<a href="<?=$_cms_folder;?>/reply.php?sl_mode=2">이벤트 기간 내 작품 통계(매출,시간대,구매건,조회수)</a>
		</div>
	</section>

	<!-- START CMS SEARCH-->
	<section id="cms_search">
		<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
		<form name="member_search_form" id="member_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return member_search_submit();">
			<div class="cs_bg">
				<div class="cs_form">
					<div class="cs_s_type_btn">
                        <?
                        //검색 필터링(radio)
                        $s_type_list = array('이름', '아이디', '응원글');
                        tag_radios($s_type_list, "s_type", $_s_type, 'y', '전체', '');
                        ?>
					</div>
					<div class="cs_search_btn">
                        <?
                        //마케터
                        tag_selects($mb_sns_type, "mb_sns_type", $_mb_sns_type, 'y');
                        ?>
						<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
					</div>
					<div class="cs_search_btn">
                        <?
                        //페이지 행 개수
                        tag_selects($s_limit_list, "s_limit", $_s_limit, 'n');

                        //날짜 검색
                        tag_selects($d_mb_date_type, "date_type", $_date_type, 'y', '날짜 검색안함', '');
                        ?>
					</div>
                    <? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
				</div>
				<div class="cs_submit member_submit">
					<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="72" data-on_h="130"  value="검색" id="cms_submit">
				</div>
			</div><!-- cs_bg -->
		</form>
	<div class="cms_excel"><a href="<?=$_cms_folder;?>/excel/reply1_excel.php" target="_blank">엑셀 다운로드</a></div>
	</section>
	<!-- END CMS SEARCH -->

	<!-- START CONTAINER -->
	<section id="event_result">
		<div id="cr_bg">
			<table>
				<thead id="cr_thead">
				<tr>
                    <?
                    //정렬표기
                    $order_giho = "▼";
                    $th_order = "desc";
                    if($_order == 'desc'){
                        $order_giho = "▲";
                        $th_order = "asc";
                    }
                    foreach($fetch_row as $fetch_key => $fetch_val){

                        $th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
                        if($fetch_val[2] == 1){
                            $th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
                            $th_ahref_e = '</a>';
                        }
                        if($fetch_val[1] == $_order_field){
                            $th_title_giho = $order_giho;
                        }
                        $th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
                        $th_class = $fetch_val[1];
                        ?>
						<th class="<?=$th_class?>"><?=$th_title?></th>
                    <?}?>
				</tr>
				</thead>
				<tbody>
                <?php foreach($rows_data as $dvalue_key => $dvalue_val) { ?>

					<tr class="result_hover">
						<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val[$fetch_row[0][1]]?></td>
						<td class="<?=$fetch_row[1][1]?> text_center">
                <a href="#" onclick="popup('<?=NM_ADM_URL?>/members/member_view.php?mb_no=<?=$dvalue_val['mb_no'];?>','member_view', <?=$popup_cms_width;?>, 550);"><?=$dvalue_val[$fetch_row[1][1]]?></a>
            </td>
						<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val[$fetch_row[2][1]]?></td>
                        <td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val[$fetch_row[3][1]]?></td>
						<td class="<?=$fetch_row[4][1]?> text_center"><?=$mb_sns_type[$dvalue_val[$fetch_row[4][1]]]?></td>
						<td class="<?=$fetch_row[5][1]?> text_center"><?=$blind_YN[$dvalue_val[$fetch_row[5][1]]]?></td>
						<td class="<?=$fetch_row[6][1]?> text_center"><?=$dvalue_val[$fetch_row[6][1]]?></td>
						<td class="<?=$fetch_row[7][1]?> text_center"><?=$dvalue_val[$fetch_row[7][1]]?></td>
						<td class="<?=$fetch_row[8][1]?> text_center">
							<button class="view_btn" onclick="set_event_thanksgiving_Blind(<?=$dvalue_val[$fetch_row[0][1]]?>)">블라인드 처리</button>
						</td>
						<td class="<?=$fetch_row[9][1]?> text_center"><?=$reward_YN[$dvalue_val[$fetch_row[9][1]]]?></td>
					</tr>
                <? } ?>
				</tbody>
			</table>
		</div>
	</section>
	<!-- END CONTAINER -->

<?php rows_page(); /* 페이지처리 */ ?>

<?php include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>