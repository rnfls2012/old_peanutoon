<? include_once '_common.php'; // 공통

$config_time_name = "2018년 추석 타입세일 이벤트";

$coupon_t_w = "from event_pay where ep_text='".$config_time_name."' order by ep_date_start asc, ep_hour_start asc;";
// 테스트
/*
$coupon_t_w = "from event_pay where ep_text='78화 무료' or ep_text='순정 만화 2땅콩 할인 이벤트'  or ep_text='로맨스 만화 무료 이벤트' 
                              order by ep_date_start asc, ep_hour_start asc;";
*/
$sql = "select * ".$coupon_t_w ;
$result = sql_query($sql);
$time_arr = array();
$time_date_temp_arr = array();
for($i=0; $row = sql_fetch_array($result); $i++){
	$time_arr[$i] = $row;
	$sql_time1 = "select * from event_pay_comics where epc_ep_no=".$row['ep_no'].";";
	// echo $sql_time1."<br/>";
	array_push($time_date_temp_arr, $row['ep_date_start']);
	$result_time1 = sql_query($sql_time1);
	for($j=0; $row_time1 = sql_fetch_array($result_time1); $j++){
		$time_arr[$i]['comics_list'][$j] = $row_time1;
	}
}

$_s_date = $time_date_temp_arr[0];
$_e_date = $time_date_temp_arr[count($time_date_temp_arr)-1];

$event_date_time_text = "기간 : ".$_s_date." ~ ".$_e_date;

// echo $sql."<br/>";

// print_r($time_arr);

$row_data = $time_arr;

// 엑셀 Library Import
include_once('../../../../lib/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once('../../../../lib/php_writeexcel/class.writeexcel_worksheet.inc.php');


/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-thanksgiving.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
array_push($data_head, '무료&코인할인 번호');
array_push($data_head, '할인기간');
array_push($data_head, '코믹스명');
array_push($data_head, '조회수(건)');
array_push($data_head, '매출(원)');

$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write(0, $col++, $cell);
} 


/* 데이터 전달 부분 */
$i=1;
foreach($row_data as $dvalue_key => $dvalue_val){	
	
	// 데이터 초기화

	// 코믹스 번호
	$row_rows_data_arr['time_comics_info'] = array();
	// 조회수
	$row_rows_data_arr['sl_open_sum'] = array();
	// 매출
	$row_rows_data_arr['sl_won_sum'] = array();	

	foreach($dvalue_val['comics_list'] as $row_data_arr){ 
		// 코믹스명
		$get_comics = get_comics($row_data_arr['epc_comics']);
		$time_comics_info = $get_comics['cm_series']."(".$row_data_arr['epc_comics'].")";

		// $time_date_year_month = date_year_month($dvalue_val['ep_date_start'], $dvalue_val['ep_date_end'], 'sl.sl', $dvalue_val['ep_hour_start'], $dvalue_val['ep_hour_end']);
		// 이벤트의 날짜 시간이 새벽에 끝나서;;;
		$time_date_year_month = this_date_year_month($dvalue_val['ep_date_start'], $dvalue_val['ep_date_end'], 'sl.sl', $dvalue_val['ep_hour_start'], $dvalue_val['ep_hour_end']);


		$sql_rows_data = "  select sl.sl_comics, cm.cm_small, 
							sum(if(sl.sl_cash_point>0,sl_cash_point,0))as sl_cash_point_sum, 
							sum(if(sl.sl_cash_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_cash_point_cnt, 
							sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won)as sl_cash_point_won_sum, 
							sum(if(sl.sl_point>0,sl_point,0))as sl_point_sum, 
							sum(if(sl.sl_cash_point=0 && sl.sl_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_point_cnt, 
							sum(floor(if(sl.sl_point>0,sl_point,0)*cup_won/10))as sl_point_won_sum, 
							sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/10))as sl_won_sum, 
							sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
							sum(sl.sl_open)as sl_open_sum 
							FROM sales sl 
							LEFT JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type 
							LEFT JOIN comics cm ON sl.sl_comics = cm.cm_no 
							where 1 and sl_comics=".$row_data_arr['epc_comics']." 
							".$time_date_year_month."
		;";

		$result_rows_data = sql_query($sql_rows_data);
		while($row_rows_data = sql_fetch_array($result_rows_data)) {
			array_push($row_rows_data_arr['time_comics_info'], $time_comics_info);
			array_push($row_rows_data_arr['sl_open_sum'], $row_rows_data['sl_open_sum']);
			array_push($row_rows_data_arr['sl_won_sum'], $row_rows_data['sl_won_sum']);
		}
	}


	$f3 = $f2 = $f1 = $i;
	foreach($data_head as $data_key2 => $data_val2){ 
		switch($data_key2) {
			case 0:
				$worksheet->write($i, $data_key2 , iconv_cp949($dvalue_val['ep_no']));
			break;
			case 1:
				$ep_date = $dvalue_val['ep_date_start'].'일 '.$dvalue_val['ep_hour_start'].' 시 ~';
				$ep_date.= $dvalue_val['ep_date_end'].'일 '.$dvalue_val['ep_hour_end'].' 시';
				$worksheet->write($i, $data_key2 , iconv_cp949($ep_date));
			break;
			case 2:
				$i = $f1;
				foreach($row_rows_data_arr['time_comics_info'] as $time_comics_info){
					$worksheet->write($i, $data_key2 , iconv_cp949($time_comics_info));
					$i++;
				}
			break;
			case 3:
				$i = $f2;
				foreach($row_rows_data_arr['sl_open_sum'] as $time_sl_open_sum){
					$worksheet->write($i, $data_key2 , iconv_cp949($time_sl_open_sum));
					$i++;
				}
			break;
			case 4:
				$i = $f3;
				foreach($row_rows_data_arr['sl_won_sum'] as $time_sl_won_sum){
					$worksheet->write($i, $data_key2 , iconv_cp949($time_sl_won_sum));
					$i++;
				}
			break;
		}

	} // end foreach
	$i++;
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."thanksgiving_time_1_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"thanksgiving_time_1_".$today.".xls");
header("Content-Disposition: inline; filename=\"thanksgiving_time_1_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
?>


<?
function this_date_year_month($_s_date, $_e_date, $field, $_sr_hour='', $_er_hour=''){
{
	$_s_date_y_m = substr($_s_date,0,7);
	$_e_date_y_m = substr($_e_date,0,7);
	$_s_date_d = substr($_s_date,8,2);
	$_e_date_d = substr($_e_date,8,2);

	$date_and_or = "OR";
	if($_s_date_y_m == $_e_date_y_m){ $date_and_or = "AND"; }
	
	// 시간
	$sql_sr_hour = $sql_er_hour = "";
	if($_sr_hour != ''){
		$sql_sr_hour = " AND sl_hour >= '$_sr_hour' ";
	}
	if($_er_hour != ''){
		$sql_er_hour = " AND sl_hour <= '$_er_hour' ";
	}

    if($_s_date == $_e_date){
		$where_date = "AND 
						(
							(".$field."_year_month >  '$_s_date_y_m' AND ".$field."_year_month <  '$_e_date_y_m' ) 
							OR  ( 
									(".$field."_year_month =  '$_s_date_y_m' AND ".$field."_day >=  '$_s_date_d' $sql_sr_hour ) 
									$date_and_or (".$field."_year_month =  '$_e_date_y_m' AND ".$field."_day <=  '$_e_date_d' $sql_er_hour ) 
								)
						) ";
	}else{
		$where_date = "AND 
						(
							(".$field."_year_month >  '$_s_date_y_m' AND ".$field."_year_month <  '$_e_date_y_m' ) 
							OR  ( 
									(".$field."_year_month =  '$_s_date_y_m' AND ".$field."_day =  '$_s_date_d' $sql_sr_hour ) 
									$date_and_or (".$field."_year_month =  '$_e_date_y_m' OR ".$field."_day =  '$_e_date_d' $sql_er_hour ) 
								)
						) ";
	}

	return $where_date;
}

}
?>