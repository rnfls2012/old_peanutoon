<? include_once '_common.php'; // 공통

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호', 'mb_no', 0));
array_push($fetch_row, array('회원 ID' ,'mb_id', 0));
array_push($fetch_row, array('이름', 'mb_name', 0));
array_push($fetch_row, array('응원글', 'comments', 0));
array_push($fetch_row, array('SNS 타입', 'mb_sns_type', 0));
array_push($fetch_row, array('블라인드 유무', 'blind_YN', 0));
array_push($fetch_row, array('등록일','reg_date', 1));
array_push($fetch_row, array('블라인드 전환일','blind_date', 0));
array_push($fetch_row, array('관리','management',0));
array_push($fetch_row, array('미니땅콩 지급 유무', 'reward_YN', 0));

$mb_sns_type = array(
    'nid' => '네이버',
    'fcb' => '페이스북',
    'kko' => '카카오톡',
    'twt' => '트위터'
);

$blind_YN = array(
    'y' => '블라인드',
    'n' => ''
);

$reward_YN = array(
    'y' => '지급',
    'n' => ''
);

$cs_calendar_on = $cs_submit_h = '';

$page_title = "신작 기대평 댓글";
$cms_head_title = $page_title;

/**
 * START SQL 작업
 */
// SQL문
$sql_field		= " * ";
$sql_count		= "COUNT(*) AS cnt ";
$sql_where      = "WHERE 1 ";

// 정렬
if($_order_field == null || $_order_field == ""){
    $_order_field = "reg_date";
}
if($_order == null || $_order == ""){
    $_order = "DESC";
}

$sql_order 		= "ORDER BY ".$_order_field." ".$_order;


// 검색단어+ 검색타입
if($_s_text){
    switch($_s_type){
        case '0': $sql_where.= "and mb_name like '%$_s_text%' ";
            break;

        case '1': $sql_where.= "and mb_id like '%$_s_text%'";
            break;

        case '2': $sql_where.= "and comments like '%$_s_text%' ";
            break;

        default: $sql_where.= "and ( mb_id like '%$_s_text%' or mb_id like '%$_s_text%' or comments like '%$_s_text%' )";
            break;
    }
}

// 최근 로그인/탈퇴일 날짜
$start_date = $end_date = "";
if($_date_type) { //all, upload_date, new_date

    $cs_calendar_on = 'cs_calendar_on';
    $cs_submit_h = 'cs_submit_on';

    if($_s_date && $_e_date) {
        $start_date = $_s_date." ".NM_TIME_HI_start;
        $end_date = $_e_date." ".NM_TIME_HI_end;
    } else if($_s_date) {
        $start_date = $_s_date." ".NM_TIME_HI_start;
        $end_date = NM_TIME_YMD." ".NM_TIME_HI_end;
    }

    switch($_date_type){
        case 'mb_login_date' :
            $sql_where.= "and mb_login_date >= '$start_date' and mb_login_date <= '$end_date'";
            $_order_field = "mb_login_date";
            break;

        case 'mb_join_date' :
            $sql_where.= "and mb_join_date >= '$start_date' and mb_join_date <= '$end_date'";
            $_order_field = "mb_join_date";
            break;

        case 'mb_out_date' :
            $sql_where.= "and mb_out_date >= '$start_date' and mb_out_date <= '$end_date'";
            $_order_field = "mb_out_date";
            break;

        default: $sql_where.= "";
            break;
    }
} else {
    $_s_date = "";
    $_e_date = "";
}

// sns 종류
if($_mb_sns_type){
    switch($_mb_sns_type){
        case 'nid': $sql_where .= "AND mb_sns_type = 'nid' ";
            break;
        case 'fcb': $sql_where .= "AND mb_sns_type = 'fcb' ";
            break;
        case 'twt': $sql_where .= "AND mb_sns_type = 'twt' ";
            break;
        case 'kko': $sql_where .= "AND mb_sns_type = 'kko' ";
            break;
        case '': $sql_where .= "AND mb_sns_type = '' ";
            break;
    }
}

$sql_table		= "
	SELECT $sql_field
	FROM event_thanksgiving_reply AS ec 
	LEFT JOIN member ON ec.mb_no = member.mb_no
";



$sql_query		= " {$sql_table} {$sql_where} {$sql_group} {$sql_order} ";


$rows_data	= rows_data($sql_query, false);		// SQL문 결과 레코드값

// 엑셀 Library Import
include_once('../../../../lib/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once('../../../../lib/php_writeexcel/class.writeexcel_worksheet.inc.php');


/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-thanksgiving.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $fetch_val) {
	array_push($data_head, $fetch_val[0]);
}

$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write(0, $col++, data_head_time_mark($cell));
} 


/* 데이터 전달 부분 */
$i=1;
foreach($rows_data as $dvalue_key => $dvalue_val){
	$get_comics = get_comics($dvalue_val['sl_comics']); /* 변경 */

	$db_cm_big = $get_comics['cm_big'];
	$db_comics = $dvalue_val['sl_comics'];
	$db_series = $get_comics['cm_series'];

	$db_sum_won = $dvalue_val['sl_won_sum'];
	$db_sum_cnt = $dvalue_val['sl_pay_sum'];

	// 충전땅콩 / 소진율
	$db_sql_sales_recharge = "select sr_year_month, sr_day, sr_week, 
								sum(sr_cash_point)as sr_cash_point_sum, 
								sum(sr_point)as sr_point_sum 
								FROM sales_recharge 
								where 1 AND sr_year_month = '".$dvalue_val['sl_year_month']."' 
								AND sr_day = '".$dvalue_val['sl_day']."'
								group by sr_year_month, sr_day ";					
	$db_row_sales_recharge = sql_fetch($db_sql_sales_recharge);
	
	/* cash_point  */					
	$db_cash_point_sum = $dvalue_val['sl_cash_point_sum'];
	$db_cash_point_cnt= $dvalue_val['sl_cash_point_cnt'];
	$db_cash_point_won_sum = $dvalue_val['sl_cash_point_won_sum'];

	/* point */
	$db_point_sum = $dvalue_val['sl_point_sum'];
	$db_point_cnt= $dvalue_val['sl_point_cnt'];
	$db_point_won_sum = $dvalue_val['sl_point_won_sum'];
	
	/* 열람수 */
	$db_open_sum = $dvalue_val['sl_open_sum'];

	/* 등록일 */
	$db_cm_reg_date_ymd = get_ymd($get_comics['cm_reg_date']);
	$db_cm_reg_date_his = get_his($get_comics['cm_reg_date']);

	/* 에피소드 등록일 */
	$db_cm_episode_date_ymd = get_ymd($get_comics['cm_episode_date']);
	$db_cm_episode_date_his = get_his($get_comics['cm_episode_date']);

	/* 요일별 class */
	$week_class = $week_en[$dvalue_val['sl_week']];

	/* 파라미터 */
	$db_comics_url_para = "&cm_big=".$db_cm_big."&cm_no=".$db_comics."&s_date=".$_s_date."&e_date=".$_e_date;

	/* 날짜별 link */
	$db_comics_date_url = $_cms_self."?sl_mode=date".$db_comics_url_para;

	/* 화별 link */
	$db_comics_episode_url = $_cms_self."?sl_mode=episode".$db_comics_url_para;

	$comics_url = get_comics_url($dvalue_val['sl_comics']);

	// 시간대
	$data_hour = array();
	for($h=0; $h<24; $h++){
		$data_hour[$h] = 0;
	}
	$sql_rows_data = "select ".this_time_field()." {$sql_table} {$sql_where}
					 AND sl_comics=".$dvalue_val['sl_comics']."";
	$result_rows_data = sql_query($sql_rows_data);
	// echo $sql_rows_data."<br/><br/>";
	while($row_rows_data = sql_fetch_array($result_rows_data)) {
		for($h=0; $h<24; $h++){
			$cm_h = $h; if($h <10){$cm_h = "0".$h;}
			$data_hour[$h] = intval($row_rows_data['sl_time'.$cm_h]);
			// echo $cm_h.":".$row_rows_data['sl_time'.$cm_h]."<br/>";
		}
	}

	foreach($data_head as $data_key2 => $data_val2){ 
		switch($data_key2) {
			case 0:
				$worksheet->write($i, $data_key2 , iconv_cp949($dvalue_val[$fetch_row[0][1]]));
			break;
			case 1:
				$worksheet->write($i, $data_key2 , iconv_cp949($dvalue_val[$fetch_row[1][1]]));
			break;
			case 2:
				$worksheet->write($i, $data_key2 , iconv_cp949($dvalue_val[$fetch_row[2][1]]));
			break;
			case 3:
				$worksheet->write($i, $data_key2 , iconv_cp949($dvalue_val[$fetch_row[3][1]]));
			break;
			case 4:
				$worksheet->write($i, $data_key2 , iconv_cp949($mb_sns_type[$dvalue_val[$fetch_row[4][1]]]));
			break;
			case 5:
				$worksheet->write($i, $data_key2 , iconv_cp949($blind_YN[$dvalue_val[$fetch_row[5][1]]]));
			break;
			case 6:
				$worksheet->write($i, $data_key2 , iconv_cp949($dvalue_val[$fetch_row[6][1]]));
			break;
			case 7:
				$worksheet->write($i, $data_key2 , iconv_cp949($dvalue_val[$fetch_row[7][1]]));
			break;
			case 9:
				$worksheet->write($i, $data_key2 , iconv_cp949($reward_YN[$dvalue_val[$fetch_row[9][1]]]));
			break;
		}

	} // end foreach
	$i++;
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."thanksgiving_reply_1_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"thanksgiving_reply_1_".$today.".xls");
header("Content-Disposition: inline; filename=\"thanksgiving_reply_1_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
?>
<?

////////////////////////////// funcion //////////////////////////////

function this_sale_comics_arr(){
	// sale comics 데이터
	$evt_thanksgiving_sale_comic = array();
	// array_push(변수, array(코믹스번호, 성인yn, 에피소드1화/오픈예정, a_tag, 구매yn, 이미지번호, 땅콩소비));
	array_push($evt_thanksgiving_sale_comic, array(3211, 'n', 31954             , '', 'n', '01', 46)); // 편의점 빌런
	array_push($evt_thanksgiving_sale_comic, array(3255, 'n', 32376             , '', 'n', '02', 43)); // 지주 : 구슬을 가지다
	array_push($evt_thanksgiving_sale_comic, array(3265, 'n', 32403             , '', 'n', '03', 78)); // 남고생과 동거중!
	array_push($evt_thanksgiving_sale_comic, array(3269, 'n', 32412             , '', 'n', '04', 52)); // 사립 남자아이 고등학교
	array_push($evt_thanksgiving_sale_comic, array(0   , 'n', '10월 오픈 예정입니다.', '', 'n', '05', 79)); // 황금정원 -> 날짜 임시 넣기
	array_push($evt_thanksgiving_sale_comic, array(2562, 'n', 28642             , '', 'n', '06', 56)); // 로빈의 법칙 시즌 2



	$sale_comic_arr = array();
	foreach($evt_thanksgiving_sale_comic as &$et_reply_comic_val){
		array_push($sale_comic_arr,$et_reply_comic_val[0]);
	}
	return $sale_comic_arr;
}

function this_time_field(){
	
	// SQL문
	$sql_field = "		/* sl.sl_comics, */

						sum(if(sl.sl_hour='00',sl.sl_all_pay+sl.sl_pay,0))as sl_time00,
						sum(if(sl.sl_hour='01',sl.sl_all_pay+sl.sl_pay,0))as sl_time01,
						sum(if(sl.sl_hour='02',sl.sl_all_pay+sl.sl_pay,0))as sl_time02,
						sum(if(sl.sl_hour='03',sl.sl_all_pay+sl.sl_pay,0))as sl_time03,
						sum(if(sl.sl_hour='04',sl.sl_all_pay+sl.sl_pay,0))as sl_time04,
						sum(if(sl.sl_hour='05',sl.sl_all_pay+sl.sl_pay,0))as sl_time05,
						sum(if(sl.sl_hour='06',sl.sl_all_pay+sl.sl_pay,0))as sl_time06,
						sum(if(sl.sl_hour='07',sl.sl_all_pay+sl.sl_pay,0))as sl_time07,
						sum(if(sl.sl_hour='08',sl.sl_all_pay+sl.sl_pay,0))as sl_time08,
						sum(if(sl.sl_hour='09',sl.sl_all_pay+sl.sl_pay,0))as sl_time09,
						sum(if(sl.sl_hour='10',sl.sl_all_pay+sl.sl_pay,0))as sl_time10,
						sum(if(sl.sl_hour='11',sl.sl_all_pay+sl.sl_pay,0))as sl_time11,
						sum(if(sl.sl_hour='12',sl.sl_all_pay+sl.sl_pay,0))as sl_time12,
						sum(if(sl.sl_hour='13',sl.sl_all_pay+sl.sl_pay,0))as sl_time13,
						sum(if(sl.sl_hour='14',sl.sl_all_pay+sl.sl_pay,0))as sl_time14,
						sum(if(sl.sl_hour='15',sl.sl_all_pay+sl.sl_pay,0))as sl_time15,
						sum(if(sl.sl_hour='16',sl.sl_all_pay+sl.sl_pay,0))as sl_time16,
						sum(if(sl.sl_hour='17',sl.sl_all_pay+sl.sl_pay,0))as sl_time17,
						sum(if(sl.sl_hour='18',sl.sl_all_pay+sl.sl_pay,0))as sl_time18,
						sum(if(sl.sl_hour='19',sl.sl_all_pay+sl.sl_pay,0))as sl_time19,
						sum(if(sl.sl_hour='20',sl.sl_all_pay+sl.sl_pay,0))as sl_time20,
						sum(if(sl.sl_hour='21',sl.sl_all_pay+sl.sl_pay,0))as sl_time21,
						sum(if(sl.sl_hour='22',sl.sl_all_pay+sl.sl_pay,0))as sl_time22,
						sum(if(sl.sl_hour='23',sl.sl_all_pay+sl.sl_pay,0))as sl_time23 

						/* 

						sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_won_sum, 

						sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
						sum(sl.sl_open)as sl_open_sum 
						*/
	"; 
	return $sql_field;
}




function data_head_time_mark($time){
	$mark = $time;
	for($h=0; $h<24; $h++){ $cm_h = $h; if($h <10){$cm_h = "0".$h;}
		if($time == $cm_h){
			$mark = $time.' hour';
		}
	}
	return $mark;
}


?>