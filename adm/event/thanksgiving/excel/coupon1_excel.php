<? include_once '_common.php'; // 공통

/* 쿠폰 통계 가져오기 */
$config_coupon_use_count = 10000000;
$coupon_t_w = "from event_randombox where er_first_count=".$config_coupon_use_count.";";
$sql = "select * ".$coupon_t_w ;
$result = sql_query($sql);
$coupon_arr = array();
$time_date_temp_arr = array();
for($i=0; $row = sql_fetch_array($result); $i++){
	array_push($coupon_arr, $row['er_no']);
	array_push($time_date_temp_arr, $row['er_date_start']);
}

$_s_date = $time_date_temp_arr[0];
$_e_date = $time_date_temp_arr[count($time_date_temp_arr)-1];

$event_date_time_text = "기간 : ".$_s_date." ~ ".$_e_date;

// SQL문
$sql_field		= " count(*) as erc_su , left(erc_date,10)as erc_date_time ";

$sql_table		= " select {$sql_field} from event_randombox_coupon ";
$sql_where		= " where erc_er_no in (".implode(",",$coupon_arr).") ";
$sql_group		= " group by erc_date_time ";
$sql_order		= " ORDER BY erc_date_time ";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_data	= rows_data($sql_querys, false);		// SQL문 결과 레코드값

// 엑셀 Library Import
include_once('../../../../lib/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once('../../../../lib/php_writeexcel/class.writeexcel_worksheet.inc.php');


/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-thanksgiving.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
array_push($data_head, '날짜');
array_push($data_head, '명수');
array_push($data_head, '시간대');
$data_hour = array();
for($h=0; $h<24; $h++){ $cm_h = $h; if($h <10){$cm_h = "0".$h;}
	array_push($data_head, $cm_h);
}
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write(0, $col++, data_head_time_mark($cell));
} 


/* 데이터 전달 부분 */
$i=1;
foreach($rows_data as $dvalue_key => $dvalue_val){
	
	// 데이터 초기화
	$sql_data = "";
	$data_hour = array();
	for($h=0; $h<24; $h++){
		$data_hour[$h] = 0;
	}
	
	// 데이터 구하기
	$sql_rows_data = " select ".$sql_field.",
					   left(erc_date,13)as erc_date_time_hour, 
					   mid(erc_date,12,2)as erc_date_hour  
					   from event_randombox_coupon 
					   ".$sql_where."
					   group by erc_date_time_hour
					   having erc_date_time='".$dvalue_val['erc_date_time']."';";
	$result_rows_data = sql_query($sql_rows_data);
	while($row_rows_data = sql_fetch_array($result_rows_data)) {
		$row_rows_data_hour = $row_rows_data['erc_date_hour'];
		$data_hour[$row_rows_data_hour] = intval($row_rows_data['erc_su']);
	}

	foreach($data_head as $data_key2 => $data_val2){ 
		switch($data_key2) {
			case 0:
				$worksheet->write($i, $data_key2 , iconv_cp949($dvalue_val['erc_date_time']));
			break;
			case 1:
				$worksheet->write($i, $data_key2 , iconv_cp949(intval($dvalue_val['erc_su'])));
			break;
			case 2:
				$worksheet->write($i, $data_key2 , '->');
			break;

			default:
				$worksheet->write($i, $data_key2 , iconv_cp949(intval($data_hour[$data_val2])));
			break;
		}

	} // end foreach
	$i++;
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."thanksgiving_coupon_1_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"thanksgiving_coupon_1_".$today.".xls");
header("Content-Disposition: inline; filename=\"thanksgiving_coupon_1_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
?>

<?
function data_head_time_mark($time){
	$mark = $time;
	for($h=0; $h<24; $h++){ $cm_h = $h; if($h <10){$cm_h = "0".$h;}
		if($time == $cm_h){
			$mark = $time.' hour';
		}
	}
	return $mark;
}

?>