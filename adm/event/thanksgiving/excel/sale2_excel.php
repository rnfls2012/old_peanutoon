<? include_once '_common.php'; // 공통

/*
select * from event_thanksgiving_all_sale group by mpec_comics;
*/

$sale_comics_arr = this_sale_comics_arr();

$sql_where.= " AND cm_no in ( ".implode(",",$sale_comics_arr)." ) ";


$sql_sale_comics_orde_field = " CASE ";
foreach($sale_comics_arr as $sale_comics_order_key => $sale_comics_order_val){
	$sql_then = 99 - intval($sale_comics_order_key);
	$sql_sale_comics_orde_field.= " WHEN cm_no = '".$sale_comics_order_val."' THEN ".$sql_then." ";
}
$sql_sale_comics_orde_field.= "ELSE 0 ";
$sql_sale_comics_orde_field.= "END AS cm_no_special ";

$sql_sale_comicno_order = "cm_no_special DESC ";

$sql_order = "order by ".$sql_sale_comicno_order.$_order_field." ".$_order.";";

// SQL문
$sql_field = "		*, ".$sql_sale_comics_orde_field." "; 

$sql_select = "		select {$sql_field} ";
$sql_table = "		FROM comics ";

$sql_group = " ";

$sql_query		= " {$sql_select} {$sql_table} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

$rows_data	= rows_data($sql_querys, false);		// SQL문 결과 레코드값

// 엑셀 Library Import
include_once('../../../../lib/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once('../../../../lib/php_writeexcel/class.writeexcel_worksheet.inc.php');


/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-thanksgiving.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
array_push($data_head, '코믹스번호');
array_push($data_head, '코믹스명');
array_push($data_head, '기존구매자 수(사람)-2018-09-22이전');
array_push($data_head, '기존구매자 화수(에피스드건수)-2018-09-22이전');
array_push($data_head, '기존구매자 중 전체할인 재구매자수(사람)');
array_push($data_head, '전체할인 재구매자수(사람)');
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write(0, $col++, data_head_time_mark($cell));
} 


/* 데이터 전달 부분 */
$i=1;
foreach($rows_data as $dvalue_key => $dvalue_val){

	$buy_cnt=0;
	$buy_member_cnt=0;
	$sale_re_buy_member_cnt=0;
	$sale_buy_member_cnt=0;

	$sql_rows_data = "SELECT *, count(mbe_member) as buy_cnt FROM member_buy_episode_".$dvalue_val['cm_big']."
					  WHERE mbe_comics=".$dvalue_val['cm_no']." 
					  AND mbe_date <='2018-09-22 00:00:00'
					  GROUP BY mbe_member;";
	// echo $sql_rows_data;
	// die;
	$result_rows_data = sql_query($sql_rows_data);
	while($row_rows_data = sql_fetch_array($result_rows_data)) {
		$buy_cnt+= intval($row_rows_data['buy_cnt']);
		$buy_member_cnt++;
		// 기존 구매자중 전체할인 재구매자수 확인
		$sql_rows_data_sale_re = "select count(*) as sale_re_cnt from event_thanksgiving_all_sale where mpec_comics=".$dvalue_val['cm_no']." AND mpec_member=".$row_rows_data['mbe_member']."; ";
		$row_rows_data_sale_re = sql_fetch($sql_rows_data_sale_re);
		$sale_re_buy_member_cnt+= intval($row_rows_data_sale_re['sale_re_cnt']);
		if(intval($row_rows_data_sale_re['sale_re_cnt']) > 0){
			// echo $sql_rows_data_sale_re."<br/>";
			// echo $row_rows_data_sale_re['sale_re_cnt']."<br/>";
		}
	}
	// 전체할인 재구매자수 확인
	$sql_rows_data_sale = "select count(*) as sale_cnt from event_thanksgiving_all_sale where mpec_comics=".$dvalue_val['cm_no']." GROUP BY mpec_comics; ";
	// echo $sql_rows_data_sale;
	// die;
	$row_rows_data_sale = sql_fetch($sql_rows_data_sale);
	$sale_buy_member_cnt+= intval($row_rows_data_sale['sale_cnt']);

	foreach($data_head as $data_key2 => $data_val2){ 
		switch($data_key2) {
			case 0:
				$worksheet->write($i, $data_key2 , iconv_cp949($dvalue_val['cm_no']));
			break;
			case 1:
				$worksheet->write($i, $data_key2 , iconv_cp949($dvalue_val['cm_series']));
			break;
			case 2:
				$worksheet->write($i, $data_key2 , iconv_cp949($buy_member_cnt));
			break;
			case 3:
				$worksheet->write($i, $data_key2 , iconv_cp949($buy_cnt));
			break;
			case 4:
				$worksheet->write($i, $data_key2 , iconv_cp949($sale_re_buy_member_cnt));
			break;
			case 5:
				$worksheet->write($i, $data_key2 , iconv_cp949($sale_buy_member_cnt));
			break;
		}

	} // end foreach
	$i++;
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."thanksgiving_sale_2_".$today.".xls");
header("Content-Type: application/x-msexcel; name=\"thanksgiving_sale_2_".$today.".xls");
header("Content-Disposition: inline; filename=\"thanksgiving_sale_2_".$today.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
?>
<?

////////////////////////////// funcion //////////////////////////////

function this_sale_comics_arr(){
	// sale comics 데이터
	$evt_thanksgiving_sale_comic = array();
	// array_push(변수, array(코믹스번호, 성인yn, 에피소드1화, a_tag, 구매yn, 이미지번호, 땅콩소비));
	array_push($evt_thanksgiving_sale_comic, array(2630, 'n', 29064, '', 'n', '01', 44)); // 소담빌라
	array_push($evt_thanksgiving_sale_comic, array(2660, 'n', 29253, '', 'n', '02', 44)); // 플레이 하우스
	array_push($evt_thanksgiving_sale_comic, array(2151, 'n', 25852, '', 'n', '03', 78)); // 난봉꾼과 왕자님
	array_push($evt_thanksgiving_sale_comic, array(602 , 'n', 6199 , '', 'n', '04', 52)); // 불가항력 그대 -상수와 하공-
	array_push($evt_thanksgiving_sale_comic, array(1365, 'n', 15207, '', 'n', '05', 79)); // 머리 괜찮냐?!
	array_push($evt_thanksgiving_sale_comic, array(596 , 'n', 6117 , '', 'n', '06', 56)); // 여고생과 편의점
	array_push($evt_thanksgiving_sale_comic, array(2832, 'n', 29887, '', 'n', '07',  6)); // 천사씨와 악마님 -외전-
	array_push($evt_thanksgiving_sale_comic, array(1964, 'n', 22597, '', 'n', '08',  9)); // [웹툰판] 교사가 AV 감독과 사랑해도 괜찮을까
	array_push($evt_thanksgiving_sale_comic, array(1572, 'n', 17421, '', 'n', '09', 11)); // [웹툰판] 순결 증정식 -날 안도록 해-
	array_push($evt_thanksgiving_sale_comic, array(2097, 'n', 24672, '', 'n', '10', 13)); // [웹툰판] 오빠는 달콤한 내 몸에 중독되었어


	$sale_comic_arr = array();
	foreach($evt_thanksgiving_sale_comic as &$et_sale_comic_val){
		array_push($sale_comic_arr,$et_sale_comic_val[0]);
	}
	return $sale_comic_arr;
}


function this_time_field(){
	
	// SQL문
	$sql_field = "		/* sl.sl_comics, */

						sum(if(sl.sl_hour='00',sl.sl_all_pay+sl.sl_pay,0))as sl_time00,
						sum(if(sl.sl_hour='01',sl.sl_all_pay+sl.sl_pay,0))as sl_time01,
						sum(if(sl.sl_hour='02',sl.sl_all_pay+sl.sl_pay,0))as sl_time02,
						sum(if(sl.sl_hour='03',sl.sl_all_pay+sl.sl_pay,0))as sl_time03,
						sum(if(sl.sl_hour='04',sl.sl_all_pay+sl.sl_pay,0))as sl_time04,
						sum(if(sl.sl_hour='05',sl.sl_all_pay+sl.sl_pay,0))as sl_time05,
						sum(if(sl.sl_hour='06',sl.sl_all_pay+sl.sl_pay,0))as sl_time06,
						sum(if(sl.sl_hour='07',sl.sl_all_pay+sl.sl_pay,0))as sl_time07,
						sum(if(sl.sl_hour='08',sl.sl_all_pay+sl.sl_pay,0))as sl_time08,
						sum(if(sl.sl_hour='09',sl.sl_all_pay+sl.sl_pay,0))as sl_time09,
						sum(if(sl.sl_hour='10',sl.sl_all_pay+sl.sl_pay,0))as sl_time10,
						sum(if(sl.sl_hour='11',sl.sl_all_pay+sl.sl_pay,0))as sl_time11,
						sum(if(sl.sl_hour='12',sl.sl_all_pay+sl.sl_pay,0))as sl_time12,
						sum(if(sl.sl_hour='13',sl.sl_all_pay+sl.sl_pay,0))as sl_time13,
						sum(if(sl.sl_hour='14',sl.sl_all_pay+sl.sl_pay,0))as sl_time14,
						sum(if(sl.sl_hour='15',sl.sl_all_pay+sl.sl_pay,0))as sl_time15,
						sum(if(sl.sl_hour='16',sl.sl_all_pay+sl.sl_pay,0))as sl_time16,
						sum(if(sl.sl_hour='17',sl.sl_all_pay+sl.sl_pay,0))as sl_time17,
						sum(if(sl.sl_hour='18',sl.sl_all_pay+sl.sl_pay,0))as sl_time18,
						sum(if(sl.sl_hour='19',sl.sl_all_pay+sl.sl_pay,0))as sl_time19,
						sum(if(sl.sl_hour='20',sl.sl_all_pay+sl.sl_pay,0))as sl_time20,
						sum(if(sl.sl_hour='21',sl.sl_all_pay+sl.sl_pay,0))as sl_time21,
						sum(if(sl.sl_hour='22',sl.sl_all_pay+sl.sl_pay,0))as sl_time22,
						sum(if(sl.sl_hour='23',sl.sl_all_pay+sl.sl_pay,0))as sl_time23 

						/* 

						sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_won_sum, 

						sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
						sum(sl.sl_open)as sl_open_sum 
						*/
	"; 
	return $sql_field;
}



function data_head_time_mark($time){
	$mark = $time;
	for($h=0; $h<24; $h++){ $cm_h = $h; if($h <10){$cm_h = "0".$h;}
		if($time == $cm_h){
			$mark = $time.' hour';
		}
	}
	return $mark;
}


?>