<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-07-16
 * Time: 오후 10:16
 */

include_once '_common.php'; // 공통

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

$cms_head_title = "라미 응원 ".$nm_config['cf_point_unit_ko']." 지급";

$head_title = "피너툰-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?></h1>
</section><!-- cms_page_title -->

<section id="event_comment">
    <form method="post" action="<?=$_cms_update;?>" onsubmit="return event_comment_write_submit();">
        <table>
            <tbody>
            <tr>
                <th><?=$required_arr[0]?>지급 내용</th>
                <td>
                    <input type="text" placeholder="지급 내용을 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> name="give_reason" id="give_reason" autocomplete="off" />
                </td>
            </tr>
            <tr>
                <th><?=$required_arr[0]?>지급 <?=$nm_config['cf_point_unit_ko'];?></th>
                <td><input type="text" placeholder="지급 <?=$nm_config['cf_point_unit_ko'].$required_arr[2];?>" <?=$required_arr[1]?> name="give_point" id="give_point" autocomplete="off" /></td>
            </tr>
            <tr>
                <td colspan="2" class="submit_btn">
                    <input type="submit" value="지급">
                    <input type="reset" value="취소" onclick="self.close()">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</section><!-- mb_write -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>