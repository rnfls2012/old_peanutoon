<?
include_once '_common.php'; // 공통


$config_time_name = "2018년 추석 타입세일 이벤트";

$coupon_t_w = "from event_pay where ep_text='".$config_time_name."' order by ep_date_start asc, ep_hour_start asc;";
// 테스트
/*
$coupon_t_w = "from event_pay where ep_text='78화 무료' or ep_text='순정 만화 2땅콩 할인 이벤트'  or ep_text='로맨스 만화 무료 이벤트' 
                              order by ep_date_start asc, ep_hour_start asc;";
*/
$sql = "select * ".$coupon_t_w ;
$result = sql_query($sql);
$time_arr = array();
$time_date_temp_arr = array();
for($i=0; $row = sql_fetch_array($result); $i++){
	$time_arr[$i] = $row;
	$sql_time1 = "select * from event_pay_comics where epc_ep_no=".$row['ep_no'].";";
	// echo $sql_time1."<br/>";
	array_push($time_date_temp_arr, $row['ep_date_start']);
	$result_time1 = sql_query($sql_time1);
	for($j=0; $row_time1 = sql_fetch_array($result_time1); $j++){
		$time_arr[$i]['comics_list'][$j] = $row_time1;
	}
}

$_s_date = $time_date_temp_arr[0];
$_e_date = $time_date_temp_arr[count($time_date_temp_arr)-1];

$event_date_time_text = "기간 : ".$_s_date." ~ ".$_e_date;

// echo $sql."<br/>";

// print_r($time_arr);

$row_data = $time_arr;

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('무료&코인할인<br/>번호','ep_no',0));
array_push($fetch_row, array('할인기간','ep_date',0));
array_push($fetch_row, array('코믹스명','ep_comic_name',0));
array_push($fetch_row, array('조회수','ep_click',0));
array_push($fetch_row, array('매출','ep_pay',0));

$cs_calendar_on = $cs_submit_h = '';
if($_date_type){
	$cs_calendar_on = 'cs_calendar_on';
	$cs_submit_h = 'cs_submit_on';
}

$page_title = "할인 기간내 작품별 조회/매출 통계";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
// include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div class="write">
		<a href="<?=$_cms_folder;?>/time.php?sl_mode=1">할인 기간내 <br/>작품별 조회/매출 통계</a>
		<a href="<?=$_cms_folder;?>/time.php?sl_mode=2">이벤트 기간 내 이벤트 작품 통계(매출,시간대,구매건)<br/>이벤트 페이지 통한 조회수(조회수)</a>
	</div>
</section>

<section id="cms_search">
	<?=$event_date_time_text?> <br/>
	<span>땅콩+미니땅콩 데이터 입니다.</span>
	<div class="cms_excel"><a href="<?=$_cms_folder;?>/excel/time1_excel.php" target="_blank">엑셀 다운로드</a></div>
</section><!-- cms_search -->

<section id="event_result">
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<? foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = "";
					$th_title = $fetch_val[0];
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){	

					// 코믹스 번호
					$row_rows_data_arr['time_comics_info'] = array();
					// 조회수
					$row_rows_data_arr['sl_open_sum'] = array();
					// 매출
					$row_rows_data_arr['sl_won_sum'] = array();

					foreach($dvalue_val['comics_list'] as $row_data_arr){ 
						// 코믹스명
						$get_comics = get_comics($row_data_arr['epc_comics']);
						$time_comics_info = $get_comics['cm_series']."(".$row_data_arr['epc_comics'].")";

						// $time_date_year_month = date_year_month($dvalue_val['ep_date_start'], $dvalue_val['ep_date_end'], 'sl.sl', $dvalue_val['ep_hour_start'], $dvalue_val['ep_hour_end']);
						// 이벤트의 날짜 시간이 새벽에 끝나서;;;
						$time_date_year_month = this_date_year_month($dvalue_val['ep_date_start'], $dvalue_val['ep_date_end'], 'sl.sl', $dvalue_val['ep_hour_start'], $dvalue_val['ep_hour_end']);


						$sql_rows_data = "  select sl.sl_comics, cm.cm_small, 
											sum(if(sl.sl_cash_point>0,sl_cash_point,0))as sl_cash_point_sum, 
											sum(if(sl.sl_cash_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_cash_point_cnt, 
											sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won)as sl_cash_point_won_sum, 
											sum(if(sl.sl_point>0,sl_point,0))as sl_point_sum, 
											sum(if(sl.sl_cash_point=0 && sl.sl_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_point_cnt, 
											sum(floor(if(sl.sl_point>0,sl_point,0)*cup_won/10))as sl_point_won_sum, 
											sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/10))as sl_won_sum, 
											sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
											sum(sl.sl_open)as sl_open_sum 
											FROM sales sl 
											LEFT JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type 
											LEFT JOIN comics cm ON sl.sl_comics = cm.cm_no 
											where 1 and sl_comics=".$row_data_arr['epc_comics']." 
											".$time_date_year_month."
						;";

						$result_rows_data = sql_query($sql_rows_data);
						while($row_rows_data = sql_fetch_array($result_rows_data)) {
							array_push($row_rows_data_arr['time_comics_info'], $time_comics_info);
							array_push($row_rows_data_arr['sl_open_sum'], $row_rows_data['sl_open_sum']);
							array_push($row_rows_data_arr['sl_won_sum'], $row_rows_data['sl_won_sum']);
						}
					}
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['ep_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center">
						<?=$dvalue_val['ep_date_start'];?>일 <?=$dvalue_val['ep_hour_start'];?> 시 <br/>
						<?=$dvalue_val['ep_date_end'];?>일   <?=$dvalue_val['ep_hour_end'];?>   시 <br/>
					</td>
					<td class="<?=$fetch_row[2][1]?> text_center">
						<table>
							<? foreach($row_rows_data_arr['time_comics_info'] as $time_comics_info){ ?>
							<tr>
								<td class="text_right"><?=$time_comics_info;?></td>
							</tr>
							<? } ?>
						</table>
					</td>
					<td class="<?=$fetch_row[3][1]?> text_center">
						<table>
							<? foreach($row_rows_data_arr['sl_open_sum'] as $time_sl_open_sum){ ?>
							<tr>
								<td class="text_right"><?=number_format($time_sl_open_sum);?> 건</td>
							</tr>
							<? } ?>
						</table>
					</td>
					<td class="<?=$fetch_row[4][1]?> text_center">
						<table>
							<? foreach($row_rows_data_arr['sl_won_sum'] as $time_sl_won_sum){ ?>
							<tr>
								<td class="text_right"><?=number_format($time_sl_won_sum);?> 원</td>
							</tr>
							<? } ?>
						</table>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- event_recharge_result -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>

<?
function this_date_year_month($_s_date, $_e_date, $field, $_sr_hour='', $_er_hour=''){
{
	$_s_date_y_m = substr($_s_date,0,7);
	$_e_date_y_m = substr($_e_date,0,7);
	$_s_date_d = substr($_s_date,8,2);
	$_e_date_d = substr($_e_date,8,2);

	$date_and_or = "OR";
	if($_s_date_y_m == $_e_date_y_m){ $date_and_or = "AND"; }
	
	// 시간
	$sql_sr_hour = $sql_er_hour = "";
	if($_sr_hour != ''){
		$sql_sr_hour = " AND sl_hour >= '$_sr_hour' ";
	}
	if($_er_hour != ''){
		$sql_er_hour = " AND sl_hour <= '$_er_hour' ";
	}

    if($_s_date == $_e_date){
		$where_date = "AND 
						(
							(".$field."_year_month >  '$_s_date_y_m' AND ".$field."_year_month <  '$_e_date_y_m' ) 
							OR  ( 
									(".$field."_year_month =  '$_s_date_y_m' AND ".$field."_day >=  '$_s_date_d' $sql_sr_hour ) 
									$date_and_or (".$field."_year_month =  '$_e_date_y_m' AND ".$field."_day <=  '$_e_date_d' $sql_er_hour ) 
								)
						) ";
	}else{
		$where_date = "AND 
						(
							(".$field."_year_month >  '$_s_date_y_m' AND ".$field."_year_month <  '$_e_date_y_m' ) 
							OR  ( 
									(".$field."_year_month =  '$_s_date_y_m' AND ".$field."_day =  '$_s_date_d' $sql_sr_hour ) 
									$date_and_or (".$field."_year_month =  '$_e_date_y_m' OR ".$field."_day =  '$_e_date_d' $sql_er_hour ) 
								)
						) ";
	}

	return $where_date;
}

}
?>