<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-07-13
 * Time: 오후 3:31
 */

include_once '_common.php'; // 공통

if (isset($_POST['mb_no']) && $_POST['mb_no'] != '') {

    $mb_no = base_filter($_POST['mb_no']);
    $blind_date = NM_TIME_YMDHIS;

    $sql_select = "
        SELECT * FROM event_thanksgiving_reply WHERE mb_no = $mb_no
    ";

    $result = sql_query($sql_select);
    $row = sql_fetch_array($result);

    if ( count($row) > 0 ) {
        if ( $row['blind_YN'] == 'y' ) {
            $blind_YN = 'n';
            $blind_date = '';
        } else {
            $blind_YN = 'y';
        }

        $sql_update = "
            UPDATE event_thanksgiving_reply SET blind_YN = '".$blind_YN."', blind_date = '$blind_date' WHERE mb_no = $mb_no 
        ";

        sql_query($sql_update);

        if ( mysql_affected_rows() > 0 )
            echo "상태변환에 성공했습니다.";

    } else {
        echo "데이터가 존재하지 않습니다.";
    }

} else {
    echo "블라인드 상태 변환에 실패했습니다.\n";
    echo "관리자에게 문의 바랍니다.";
}