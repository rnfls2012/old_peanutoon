<?
include_once '_common.php'; // 공통

$config_time_name = "2018년 추석 타입세일 이벤트";

$coupon_t_w = "from event_pay where ep_text='".$config_time_name."' order by ep_date_start asc, ep_hour_start asc;";
// 테스트
/*
$coupon_t_w = "from event_pay where ep_text='78화 무료' or ep_text='순정 만화 2땅콩 할인 이벤트'  or ep_text='로맨스 만화 무료 이벤트' 
                              order by ep_date_start asc, ep_hour_start asc;";
*/
$sql = "select * ".$coupon_t_w ;
$result = sql_query($sql);
$sale_arr = array();
$sale_comics_arr = array();
$sale_date_arr = $sale_date_temp_arr = array();
for($i=0; $row = sql_fetch_array($result); $i++){
	$sale_arr[$i] = $row;
	$sql_sale1 = "select * from event_pay_comics where epc_ep_no=".$row['ep_no']." order by epc_order asc;";
	// echo $sql_sale1."<br/>";
	array_push($sale_date_temp_arr, $row['ep_date_start']);
}

// print_r($sale_date_temp_arr);

$sale_comics_arr = this_sale_comics_arr();
$sql_where.= " AND sl_comics in ( ".implode(",",$sale_comics_arr)." ) ";
$sale_date_arr = array_unique($sale_date_temp_arr);

$_s_date = $sale_date_temp_arr[0];
$_e_date = $sale_date_temp_arr[count($sale_date_temp_arr)-1];

$event_date_time_text = "기간 : ".$_s_date." ~ ".$_e_date;

//추석이벤트만

// 날짜
if($_s_date && $_e_date){ 
	$sql_where.= date_year_month($_s_date, $_e_date, 'sl.sl'); 
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sl_won_sum"){ 
	$_order_field = "sl_won_sum"; 
	$_order_field_add = " , sl_comics "; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
	$_order_add= "desc"; 
}

// 요일 정렬시
if($_order_field == "sl_week"){
	$_order_field_add = " , sl.sl_year_month desc "; 
	$_order_add = " , sl.sl_day desc ";
}

// 그룹
$sql_group = " group by sl.sl_comics ";


$sql_sale_comics_orde_field = " CASE ";
foreach($sale_comics_arr as $sale_comics_order_key => $sale_comics_order_val){
	$sql_then = 99 - intval($sale_comics_order_key);
	$sql_sale_comics_orde_field.= " WHEN sl.sl_comics = '".$sale_comics_order_val."' THEN ".$sql_then." ";
}
$sql_sale_comics_orde_field.= "ELSE 0 ";
$sql_sale_comics_orde_field.= "END AS cm_no_special, ";

$sql_sale_comicno_order = "cm_no_special DESC, ";

$sql_order = "order by ".$sql_sale_comicno_order.$_order_field." ".$_order.";";

// SQL문
$sql_field = "		sl.sl_comics, cm.cm_small,
					".$sql_sale_comics_orde_field."
					sum(if(sl.sl_cash_point>0,sl_cash_point,0))as sl_cash_point_sum, 
					sum(if(sl.sl_cash_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_cash_point_cnt, 
					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won)as sl_cash_point_won_sum, 

					sum(if(sl.sl_point>0,sl_point,0))as sl_point_sum, 
					sum(if(sl.sl_cash_point=0 && sl.sl_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_point_cnt, 
					sum(floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_point_won_sum, 

					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_won_sum, 

					sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
					sum(sl.sl_open)as sl_open_sum 
"; 

$sql_select = "		select {$sql_field} ";
$sql_table = "		FROM sales sl
					LEFT JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type 
					LEFT JOIN comics cm ON sl.sl_comics = cm.cm_no";

$sql_query		= " {$sql_select}		{$sql_table} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

$rows_data	= rows_data($sql_querys, false);		// SQL문 결과 레코드값


/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('코믹스번호','sl_comics',0));
array_push($fetch_row, array('코믹스명','cm_series',0));
array_push($fetch_row, array('매출','sl_won_sum',0));
array_push($fetch_row, array('시간대 구매건','sl_time',0));
array_push($fetch_row, array('구매건','sl_pay_sum',0));
array_push($fetch_row, array('조회수','sl_open_sum',0));

// array_push($fetch_row, array('코믹스 등록일<br/>화 최신등록일','cm_reg_date',0));

// css 클래스 접두사
$css_suffix = "cm_";

$page_title = "이벤트 기간 내 이벤트 작품 통계";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<script type="text/javascript">
<!--
	var cr_thead_mg_add = '38px'; /* fixed 테이블이 높이가 커서 그만큼 마진 */
//-->
</script>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div class="write">
		<a href="<?=$_cms_folder;?>/reply.php?sl_mode=1">신작 기대평 댓글 관리</a>
		<a href="<?=$_cms_folder;?>/reply.php?sl_mode=2">이벤트 기간 내 작품 통계(매출,시간대,구매건,조회수)</a>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<?=$event_date_time_text?> <br/>
	<span>땅콩+미니땅콩 데이터 입니다.</span>
	<div class="cms_excel"><a href="<?=$_cms_folder;?>/excel/reply2_excel.php" target="_blank">엑셀 다운로드</a></div>
</section><!-- recharge_search -->

<section id="event_result">
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<? foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = "";
					$th_title = $fetch_val[0];
					$th_class = $css_suffix.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){
					$get_comics = get_comics($dvalue_val['sl_comics']); /* 변경 */

					$db_cm_big = $get_comics['cm_big'];
					$db_comics = $dvalue_val['sl_comics'];
					$db_series = $get_comics['cm_series'];

					$db_sum_won = number_format($dvalue_val['sl_won_sum'])." 원";
					$db_sum_cnt = number_format($dvalue_val['sl_pay_sum'])." 건";

					// 충전땅콩 / 소진율
					$db_sql_sales_recharge = "select sr_year_month, sr_day, sr_week, 
												sum(sr_cash_point)as sr_cash_point_sum, 
												sum(sr_point)as sr_point_sum 
												FROM sales_recharge 
												where 1 AND sr_year_month = '".$dvalue_val['sl_year_month']."' 
												AND sr_day = '".$dvalue_val['sl_day']."'
												group by sr_year_month, sr_day ";					
					$db_row_sales_recharge = sql_fetch($db_sql_sales_recharge);
					
					/* cash_point  */					
					$db_cash_point_sum = number_format($dvalue_val['sl_cash_point_sum']).' '.$nm_config['cf_cash_point_unit'];
					$db_cash_point_cnt= number_format($dvalue_val['sl_cash_point_cnt'])." 건";
					$db_cash_point_won_sum = number_format($dvalue_val['sl_cash_point_won_sum'])." 원";

					/* point */
					$db_point_sum = number_format($dvalue_val['sl_point_sum']).' '.$nm_config['cf_point_unit'];
					$db_point_cnt= number_format($dvalue_val['sl_point_cnt'])." 건";
					$db_point_won_sum = number_format($dvalue_val['sl_point_won_sum'])." 원";
					
					/* 열람수 */
					$db_open_sum = number_format($dvalue_val['sl_open_sum'])." 건";

					/* 등록일 */
					$db_cm_reg_date_ymd = get_ymd($get_comics['cm_reg_date']);
					$db_cm_reg_date_his = get_his($get_comics['cm_reg_date']);

					/* 에피소드 등록일 */
					$db_cm_episode_date_ymd = get_ymd($get_comics['cm_episode_date']);
					$db_cm_episode_date_his = get_his($get_comics['cm_episode_date']);

					/* 요일별 class */
					$week_class = $week_en[$dvalue_val['sl_week']];

					/* 파라미터 */
					$db_comics_url_para = "&cm_big=".$db_cm_big."&cm_no=".$db_comics."&s_date=".$_s_date."&e_date=".$_e_date;

					/* 날짜별 link */
					$db_comics_date_url = $_cms_self."?sl_mode=date".$db_comics_url_para;

					/* 화별 link */
					$db_comics_episode_url = $_cms_self."?sl_mode=episode".$db_comics_url_para;

					$comics_url = get_comics_url($dvalue_val['sl_comics']);

					// 시간대
					$data_hour = array();
					for($h=0; $h<24; $h++){
						$data_hour[$h] = 0;
					}
					$sql_rows_data = "select ".this_time_field()." {$sql_table} {$sql_where}
					                 AND sl_comics=".$dvalue_val['sl_comics']."";
					$result_rows_data = sql_query($sql_rows_data);
					// echo $sql_rows_data."<br/><br/>";
					while($row_rows_data = sql_fetch_array($result_rows_data)) {
						for($h=0; $h<24; $h++){
							$cm_h = $h; if($h <10){$cm_h = "0".$h;}
                            $data_hour[$h] = intval($row_rows_data['sl_time'.$cm_h]);
							// echo $cm_h.":".$row_rows_data['sl_time'.$cm_h]."<br/>";
						}
					}

				?>
				<tr class="<?=$week_class;?>">
					<td class="<?=$css_suffix.$fetch_row[0][1]?> text_center"><?=$db_comics;?></td>
					<td class="<?=$css_suffix.$fetch_row[1][1]?> text_center">
						<?=$db_series;?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[2][1]?> text_right">
						<?=$db_sum_won;?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[3][1]?> text_center">
						<table>
							<tr>
							<? for($h=0; $h<24; $h++){ $cm_h = $h; if($h <10){$cm_h = "0".$h;}?>
								<td class="text_right"><?=$cm_h?></td>
							<? } ?>
							</tr>
							<tr>
							<? for($h=0; $h<24; $h++){ ?>
								<td class="text_right"><?=number_format($data_hour[$h])?></td>
							<? } ?>
							</tr>
						</table>
					</td>
					<td class="<?=$css_suffix.$fetch_row[4][1]?> text_right">
						<?=$db_sum_cnt;?>
					</td>	
					<td class="<?=$css_suffix.$fetch_row[5][1]?> text_right">
						<?=$db_open_sum;?>
					</td>	
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 


////////////////////////////// funcion //////////////////////////////

function this_sale_comics_arr(){
	// sale comics 데이터
	$evt_thanksgiving_sale_comic = array();
	// array_push(변수, array(코믹스번호, 성인yn, 에피소드1화/오픈예정, a_tag, 구매yn, 이미지번호, 땅콩소비));
	array_push($evt_thanksgiving_sale_comic, array(3211, 'n', 31954             , '', 'n', '01', 46)); // 편의점 빌런
	array_push($evt_thanksgiving_sale_comic, array(3255, 'n', 32376             , '', 'n', '02', 43)); // 지주 : 구슬을 가지다
	array_push($evt_thanksgiving_sale_comic, array(3265, 'n', 32403             , '', 'n', '03', 78)); // 남고생과 동거중!
	array_push($evt_thanksgiving_sale_comic, array(3269, 'n', 32412             , '', 'n', '04', 52)); // 사립 남자아이 고등학교
	array_push($evt_thanksgiving_sale_comic, array(0   , 'n', '10월 오픈 예정입니다.', '', 'n', '05', 79)); // 황금정원 -> 날짜 임시 넣기
	array_push($evt_thanksgiving_sale_comic, array(2562, 'n', 28642             , '', 'n', '06', 56)); // 로빈의 법칙 시즌 2



	$sale_comic_arr = array();
	foreach($evt_thanksgiving_sale_comic as &$et_reply_comic_val){
		array_push($sale_comic_arr,$et_reply_comic_val[0]);
	}
	return $sale_comic_arr;
}

function this_time_field(){
	
	// SQL문
	$sql_field = "		/* sl.sl_comics, */

						sum(if(sl.sl_hour='00',sl.sl_all_pay+sl.sl_pay,0))as sl_time00,
						sum(if(sl.sl_hour='01',sl.sl_all_pay+sl.sl_pay,0))as sl_time01,
						sum(if(sl.sl_hour='02',sl.sl_all_pay+sl.sl_pay,0))as sl_time02,
						sum(if(sl.sl_hour='03',sl.sl_all_pay+sl.sl_pay,0))as sl_time03,
						sum(if(sl.sl_hour='04',sl.sl_all_pay+sl.sl_pay,0))as sl_time04,
						sum(if(sl.sl_hour='05',sl.sl_all_pay+sl.sl_pay,0))as sl_time05,
						sum(if(sl.sl_hour='06',sl.sl_all_pay+sl.sl_pay,0))as sl_time06,
						sum(if(sl.sl_hour='07',sl.sl_all_pay+sl.sl_pay,0))as sl_time07,
						sum(if(sl.sl_hour='08',sl.sl_all_pay+sl.sl_pay,0))as sl_time08,
						sum(if(sl.sl_hour='09',sl.sl_all_pay+sl.sl_pay,0))as sl_time09,
						sum(if(sl.sl_hour='10',sl.sl_all_pay+sl.sl_pay,0))as sl_time10,
						sum(if(sl.sl_hour='11',sl.sl_all_pay+sl.sl_pay,0))as sl_time11,
						sum(if(sl.sl_hour='12',sl.sl_all_pay+sl.sl_pay,0))as sl_time12,
						sum(if(sl.sl_hour='13',sl.sl_all_pay+sl.sl_pay,0))as sl_time13,
						sum(if(sl.sl_hour='14',sl.sl_all_pay+sl.sl_pay,0))as sl_time14,
						sum(if(sl.sl_hour='15',sl.sl_all_pay+sl.sl_pay,0))as sl_time15,
						sum(if(sl.sl_hour='16',sl.sl_all_pay+sl.sl_pay,0))as sl_time16,
						sum(if(sl.sl_hour='17',sl.sl_all_pay+sl.sl_pay,0))as sl_time17,
						sum(if(sl.sl_hour='18',sl.sl_all_pay+sl.sl_pay,0))as sl_time18,
						sum(if(sl.sl_hour='19',sl.sl_all_pay+sl.sl_pay,0))as sl_time19,
						sum(if(sl.sl_hour='20',sl.sl_all_pay+sl.sl_pay,0))as sl_time20,
						sum(if(sl.sl_hour='21',sl.sl_all_pay+sl.sl_pay,0))as sl_time21,
						sum(if(sl.sl_hour='22',sl.sl_all_pay+sl.sl_pay,0))as sl_time22,
						sum(if(sl.sl_hour='23',sl.sl_all_pay+sl.sl_pay,0))as sl_time23 

						/* 

						sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_won_sum, 

						sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
						sum(sl.sl_open)as sl_open_sum 
						*/
	"; 
	return $sql_field;
}




?>