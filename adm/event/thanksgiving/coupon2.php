<? include_once '_common.php'; // 공통

/* 쿠폰 통계 가져오기 */
$config_coupon_use_count = 10000000;
$coupon_t_w = "from event_randombox where er_first_count=".$config_coupon_use_count.";";
$sql = "select * ".$coupon_t_w ;
$result = sql_query($sql);
$coupon_arr = array();
$time_date_temp_arr = array();
for($i=0; $row = sql_fetch_array($result); $i++){
	array_push($coupon_arr, $row['er_no']);
	array_push($time_date_temp_arr, $row['er_date_start']);
}

$_s_date = $time_date_temp_arr[0];
$_e_date = $time_date_temp_arr[count($time_date_temp_arr)-1];

$event_date_time_text = "기간 : ".$_s_date." ~ ".$_e_date;

// 결제정보
$crp_arr = array();
$sql_crp = "SELECT * from config_recharge_price WHERE crp_state = 'y' ORDER BY crp_cash_point, crp_date desc";
$result_crp = sql_query($sql_crp);
for($i=0; $row_crp = sql_fetch_array($result_crp); $i++){
	$crp_arr[$i] = array();
	array_push($crp_arr[$i], $row_crp);
}

// echo $crp_arr[1][0]['crp_won'];


// 테스트 때문에...;;
// $coupon_arr = array(21,22,23,24,25);

// SQL문
$sql_field		= " count(*) as erc_su , left(erc_use_date,10)as erc_use_date_time ";

$sql_table		= " select {$sql_field} from event_randombox_coupon ";
$sql_where		= " where erc_use_date !='' and erc_use='y' 
                    and erc_er_no in (".implode(",",$coupon_arr).") ";
$sql_group		= " group by erc_use_date_time ";
$sql_order		= " ORDER BY erc_use_date_time ";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_data	= rows_data($sql_querys, false);		// SQL문 결과 레코드값

$th_class = $td_class = "";	// 상단테이블 css-class

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜', 'ef_date', 0));
array_push($fetch_row, array('명수', 'ef_member', 0));
array_push($fetch_row, array('시간대', 'ef_hour', 0));
array_push($fetch_row, array('매출', 'ef_pay', 0));

$page_title = "일별 쿠폰 이용 통계";
$cms_head_title = $page_title;
/* 여기부터 다시 */
$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$cms_page_title;?></h1>
	<div class="write">
		<a href="<?=$_cms_folder;?>/coupon.php?sl_mode=1">일별 쿠폰 발급 통계</a>
		<a href="<?=$_cms_folder;?>/coupon.php?sl_mode=2">일별 쿠폰 이용 통계</a>
	</div>
</section>


<section id="cms_search">
	<?=$event_date_time_text?>
	<div class="cms_excel"><a href="<?=$_cms_folder;?>/excel/coupon2_excel.php" target="_blank">엑셀 다운로드</a></div>
</section><!-- cms_search -->


<section id="event_result">
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<? foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = "";
					$th_title = $fetch_val[0];
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){ 

					$sql_data = "";
					$data_hour = array();
					for($h=0; $h<24; $h++){
						$data_hour[$h] = 0;
					}
					$sum_crp_won = 0;

					$sql_rows_data = " select ".$sql_field.",
					                   left(erc_use_date,13)as erc_use_date_time_hour, 
					                   mid(erc_use_date,12,2)as erc_use_date_hour  
					                   from event_randombox_coupon 
									   ".$sql_where."
									   group by erc_use_date_time_hour
								       having erc_use_date_time='".$dvalue_val['erc_use_date_time']."';";
					$result_rows_data = sql_query($sql_rows_data);
					// echo $sql_rows_data."<br/><br/><br/>";
					while($row_rows_data = sql_fetch_array($result_rows_data)) {
						$row_rows_data_hour = intval($row_rows_data['erc_use_date_hour']);
						$data_hour[$row_rows_data_hour] = $row_rows_data['erc_su'];

						$sql_rows_data2 = " select left(erc_use_date,13)as erc_use_date_time_hour, 
						                    erc_discount_rate, erc_crp_no
											from event_randombox_coupon
									        ".$sql_where."
									        group by erc_no 
								            having erc_use_date_time_hour='".$row_rows_data['erc_use_date_time_hour']."'
											;";
											// echo $sql_rows_data2."<br/>";
						$result_rows_data2 = sql_query($sql_rows_data2);
						// 할인권 쿠폰 매출 
						while($row_rows_data2 = sql_fetch_array($result_rows_data2)) {
							$sum_crp_won+= intval($row_rows_data2['sum_crp_won']);
							
							/* 4. 할인권 쿠폰일 경우 */
							$crp_won_calc = $crp_arr[$row_rows_data2['erc_crp_no']][0]['crp_won'];

							$crp_won_calc = intval($crp_won_calc) - ( intval($crp_won_calc) / 100 * intval($row_rows_data2['erc_discount_rate']));
								// 0.01 곱으로 십자리까지 소수점 만들어서 floor함수로 소수점 버리고 십자리까지 00 으로 처리
							$crp_won_calc = floor($crp_won_calc * 0.01) * 100;
							$sum_crp_won+= $crp_won_calc ;
						}
					}

				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['erc_use_date_time'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=number_format($dvalue_val['erc_su']);?> 명</td>
					<td class="<?=$fetch_row[2][1]?> text_center">
						<table>
							<tr>
							<? for($h=0; $h<24; $h++){ $cm_h = $h; if($h <10){$cm_h = "0".$h;}?>
								<td class="text_right"><?=$cm_h?></td>
							<? } ?>
							</tr>
							<tr>
							<? for($h=0; $h<24; $h++){ ?>
								<td class="text_right"><?=number_format($data_hour[$h])?></td>
							<? } ?>
							</tr>
						</table>
					</td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=number_format($sum_crp_won);?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>

<?	/* 참조 */

/*
select count(*) as erc_su , 
left(erc_use_date,10)as erc_use_date_time,
if(crp_won > 0,
(Floor(
(sum(crp_won)-sum(crp_won)/100 * erc_discount_rate)* 0.01)*100),
0) as sum_crp_won
from event_randombox_coupon erc
left join config_recharge_price crp 
on erc.erc_crp_no=crp.crp_no
where erc_use_date !='' and erc_use='y' 
and erc_er_no in (21,22,23,24,25) group by erc_use_date_time ORDER BY erc_use_date_time
*/
?>