<?
include_once '_common.php'; // 공통

/*
select * from event_thanksgiving_all_sale group by mpec_comics;
*/

$sale_comics_arr = this_sale_comics_arr();

$sql_where.= " AND cm_no in ( ".implode(",",$sale_comics_arr)." ) ";


$sql_sale_comics_orde_field = " CASE ";
foreach($sale_comics_arr as $sale_comics_order_key => $sale_comics_order_val){
	$sql_then = 99 - intval($sale_comics_order_key);
	$sql_sale_comics_orde_field.= " WHEN cm_no = '".$sale_comics_order_val."' THEN ".$sql_then." ";
}
$sql_sale_comics_orde_field.= "ELSE 0 ";
$sql_sale_comics_orde_field.= "END AS cm_no_special ";

$sql_sale_comicno_order = "cm_no_special DESC ";

$sql_order = "order by ".$sql_sale_comicno_order.$_order_field." ".$_order.";";

// SQL문
$sql_field = "		*, ".$sql_sale_comics_orde_field." "; 

$sql_select = "		select {$sql_field} ";
$sql_table = "		FROM comics ";

$sql_group = " ";

$sql_query		= " {$sql_select} {$sql_table} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

$rows_data	= rows_data($sql_querys, false);		// SQL문 결과 레코드값

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('코믹스번호','sl_comics',0));
array_push($fetch_row, array('코믹스명','cm_series',0));
array_push($fetch_row, array('기존구매자 수(사람)-2018-09-22이전','sl_won_sum',2));
array_push($fetch_row, array('기존구매자 화수(에피스드건수)-2018-09-22이전','sl_open_sum',2));
array_push($fetch_row, array('기존구매자 중 전체할인 재구매자수(사람)','sl_time',2));
array_push($fetch_row, array('전체할인 재구매자수(사람)','sl_pay_sum',2));

// css 클래스 접두사
$css_suffix = "cm_";

$page_title = "이벤트 기간 내 이벤트 작품 통계";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<script type="text/javascript">
<!--
	var cr_thead_mg_add = '38px'; /* fixed 테이블이 높이가 커서 그만큼 마진 */
//-->
</script>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div class="write">
		<a href="<?=$_cms_folder;?>/sale.php?sl_mode=1">이벤트 기간 내 작품 통계(매출,시간대,구매건,조회수)</a>
		<a href="<?=$_cms_folder;?>/sale.php?sl_mode=2">기존 구매자 중 전체할인 재구매자 수 / 기존 구매 화수</a>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<?=$event_date_time_text?> <br/>
	<span>땅콩+미니땅콩 데이터 입니다.</span>
	<div class="cms_excel"><a href="<?=$_cms_folder;?>/excel/sale2_excel.php" target="_blank">엑셀 다운로드</a></div>
</section><!-- recharge_search -->

<section id="event_result">
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<? foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = "";
					$th_class_align = "";
					$th_title = $fetch_val[0];
					$th_class = $css_suffix.$fetch_val[1];
					if($fetch_val[2] == 2){
						$th_class_align = 'text_right';
					}
				?>
					<th class="<?=$th_class?> <?=$th_class_align?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){

					$buy_cnt=0;
					$buy_member_cnt=0;
					$sale_re_buy_member_cnt=0;
					$sale_buy_member_cnt=0;

					$sql_rows_data = "SELECT *, count(mbe_member) as buy_cnt FROM member_buy_episode_".$dvalue_val['cm_big']."
					                  WHERE mbe_comics=".$dvalue_val['cm_no']." 
									  AND mbe_date <='2018-09-22 00:00:00'
									  GROUP BY mbe_member;";
					// echo $sql_rows_data;
					// die;
					$result_rows_data = sql_query($sql_rows_data);
					while($row_rows_data = sql_fetch_array($result_rows_data)) {
						$buy_cnt+= intval($row_rows_data['buy_cnt']);
						$buy_member_cnt++;
						// 기존 구매자중 전체할인 재구매자수 확인
						$sql_rows_data_sale_re = "select count(*) as sale_re_cnt from event_thanksgiving_all_sale where mpec_comics=".$dvalue_val['cm_no']." AND mpec_member=".$row_rows_data['mbe_member']."; ";
						$row_rows_data_sale_re = sql_fetch($sql_rows_data_sale_re);
						$sale_re_buy_member_cnt+= intval($row_rows_data_sale_re['sale_re_cnt']);
						if(intval($row_rows_data_sale_re['sale_re_cnt']) > 0){
							// echo $sql_rows_data_sale_re."<br/>";
							// echo $row_rows_data_sale_re['sale_re_cnt']."<br/>";
						}
					}
					// 전체할인 재구매자수 확인
					$sql_rows_data_sale = "select count(*) as sale_cnt from event_thanksgiving_all_sale where mpec_comics=".$dvalue_val['cm_no']." GROUP BY mpec_comics; ";
					// echo $sql_rows_data_sale;
					// die;
					$row_rows_data_sale = sql_fetch($sql_rows_data_sale);
					$sale_buy_member_cnt+= intval($row_rows_data_sale['sale_cnt']);

				?>
				<tr class="<?=$week_class;?>">
					<td class="<?=$css_suffix.$fetch_row[0][1]?> text_center">
						<?=$dvalue_val['cm_no'];?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[1][1]?> text_center">
						<?=$dvalue_val['cm_series'];?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[2][1]?> text_right">
						<?=number_format($buy_member_cnt);?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[3][1]?> text_right">
						<?=number_format($buy_cnt);?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[4][1]?> text_right">
						<?=number_format($sale_re_buy_member_cnt);?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[5][1]?> text_right">
						<?=number_format($sale_buy_member_cnt);?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
</section><!-- recharge_result -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 


////////////////////////////// funcion //////////////////////////////

function this_sale_comics_arr(){
	// sale comics 데이터
	$evt_thanksgiving_sale_comic = array();
	// array_push(변수, array(코믹스번호, 성인yn, 에피소드1화, a_tag, 구매yn, 이미지번호, 땅콩소비));
	array_push($evt_thanksgiving_sale_comic, array(2630, 'n', 29064, '', 'n', '01', 44)); // 소담빌라
	array_push($evt_thanksgiving_sale_comic, array(2660, 'n', 29253, '', 'n', '02', 44)); // 플레이 하우스
	array_push($evt_thanksgiving_sale_comic, array(2151, 'n', 25852, '', 'n', '03', 78)); // 난봉꾼과 왕자님
	array_push($evt_thanksgiving_sale_comic, array(602 , 'n', 6199 , '', 'n', '04', 52)); // 불가항력 그대 -상수와 하공-
	array_push($evt_thanksgiving_sale_comic, array(1365, 'n', 15207, '', 'n', '05', 79)); // 머리 괜찮냐?!
	array_push($evt_thanksgiving_sale_comic, array(596 , 'n', 6117 , '', 'n', '06', 56)); // 여고생과 편의점
	array_push($evt_thanksgiving_sale_comic, array(2832, 'n', 29887, '', 'n', '07',  6)); // 천사씨와 악마님 -외전-
	array_push($evt_thanksgiving_sale_comic, array(1964, 'n', 22597, '', 'n', '08',  9)); // [웹툰판] 교사가 AV 감독과 사랑해도 괜찮을까
	array_push($evt_thanksgiving_sale_comic, array(1572, 'n', 17421, '', 'n', '09', 11)); // [웹툰판] 순결 증정식 -날 안도록 해-
	array_push($evt_thanksgiving_sale_comic, array(2097, 'n', 24672, '', 'n', '10', 13)); // [웹툰판] 오빠는 달콤한 내 몸에 중독되었어


	$sale_comic_arr = array();
	foreach($evt_thanksgiving_sale_comic as &$et_sale_comic_val){
		array_push($sale_comic_arr,$et_sale_comic_val[0]);
	}
	return $sale_comic_arr;
}


function this_time_field(){
	
	// SQL문
	$sql_field = "		/* sl.sl_comics, */

						sum(if(sl.sl_hour='00',sl.sl_all_pay+sl.sl_pay,0))as sl_time00,
						sum(if(sl.sl_hour='01',sl.sl_all_pay+sl.sl_pay,0))as sl_time01,
						sum(if(sl.sl_hour='02',sl.sl_all_pay+sl.sl_pay,0))as sl_time02,
						sum(if(sl.sl_hour='03',sl.sl_all_pay+sl.sl_pay,0))as sl_time03,
						sum(if(sl.sl_hour='04',sl.sl_all_pay+sl.sl_pay,0))as sl_time04,
						sum(if(sl.sl_hour='05',sl.sl_all_pay+sl.sl_pay,0))as sl_time05,
						sum(if(sl.sl_hour='06',sl.sl_all_pay+sl.sl_pay,0))as sl_time06,
						sum(if(sl.sl_hour='07',sl.sl_all_pay+sl.sl_pay,0))as sl_time07,
						sum(if(sl.sl_hour='08',sl.sl_all_pay+sl.sl_pay,0))as sl_time08,
						sum(if(sl.sl_hour='09',sl.sl_all_pay+sl.sl_pay,0))as sl_time09,
						sum(if(sl.sl_hour='10',sl.sl_all_pay+sl.sl_pay,0))as sl_time10,
						sum(if(sl.sl_hour='11',sl.sl_all_pay+sl.sl_pay,0))as sl_time11,
						sum(if(sl.sl_hour='12',sl.sl_all_pay+sl.sl_pay,0))as sl_time12,
						sum(if(sl.sl_hour='13',sl.sl_all_pay+sl.sl_pay,0))as sl_time13,
						sum(if(sl.sl_hour='14',sl.sl_all_pay+sl.sl_pay,0))as sl_time14,
						sum(if(sl.sl_hour='15',sl.sl_all_pay+sl.sl_pay,0))as sl_time15,
						sum(if(sl.sl_hour='16',sl.sl_all_pay+sl.sl_pay,0))as sl_time16,
						sum(if(sl.sl_hour='17',sl.sl_all_pay+sl.sl_pay,0))as sl_time17,
						sum(if(sl.sl_hour='18',sl.sl_all_pay+sl.sl_pay,0))as sl_time18,
						sum(if(sl.sl_hour='19',sl.sl_all_pay+sl.sl_pay,0))as sl_time19,
						sum(if(sl.sl_hour='20',sl.sl_all_pay+sl.sl_pay,0))as sl_time20,
						sum(if(sl.sl_hour='21',sl.sl_all_pay+sl.sl_pay,0))as sl_time21,
						sum(if(sl.sl_hour='22',sl.sl_all_pay+sl.sl_pay,0))as sl_time22,
						sum(if(sl.sl_hour='23',sl.sl_all_pay+sl.sl_pay,0))as sl_time23 

						/* 

						sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_won_sum, 

						sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
						sum(sl.sl_open)as sl_open_sum 
						*/
	"; 
	return $sql_field;
}




?>