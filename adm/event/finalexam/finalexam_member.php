<? include_once '_common.php'; // 공통

$efp_no = num_check(tag_get_filter($_REQUEST['efp_no'])); // 교시값 파라미터

$efm_order = num_check(tag_get_filter($_REQUEST['efm_order'])); // 정렬

/* 시험설정 가져오기 */
$sql_ef = "SELECT * FROM event_finalexam_period ORDER BY efp_no";
$efm_arr = array();
$result_ef = sql_query($sql_ef);
while ($row_ef = sql_fetch_array($result_ef)) {
	$efm_arr[$row_ef['efp_no']] = $row_ef['efp_mark'].'교시';
}

// 당첨자
$efm_arr[4] = '당첨자';

// 조건문
if($efp_no > 0){
	switch($efp_no){
		case 1: $sql_where.= " AND efm_efp_no1 = '".$efp_no."' ";
		break;
		case 2: $sql_where.= " AND efm_efp_no2 = '".$efp_no."' ";
		break;
		case 3: $sql_where.= " AND efm_efp_no3 = '".$efp_no."' ";
		break;
		case 4: $sql_where.= " AND efm_win=1 ";
		break;
	}
	$_nm_paras.="&efp_no=".$efp_no;
}


// 그룹
$sql_group = "";

if($_s_limit == ''){ $_s_limit = 10; }

// 정렬
if($_ordefm_field == null || $_ordefm_field == ""){ $_ordefm_field = "efm_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
if($efm_order > 0){
	switch($efm_order){
		case 1: $_ordefm_field = "efm_highscore_sum"; $_order = "desc";
		break;
		case 2: $_ordefm_field = "efm_highscore1"; $_order = "desc";
		break;
		case 3: $_ordefm_field = "efm_highscore2"; $_order = "desc";
		break;
		case 4: $_ordefm_field = "efm_highscore3"; $_order = "desc";
		break;
	}
	$_nm_paras.="&efm_order=".$efm_order;
}


$sql_order = " ORDER BY ".$_ordefm_field." ".$_order;

// SQL문
$sql_field		= " *, (efm_highscore1+efm_highscore2+efm_highscore3)as efm_highscore_sum ";
$sql_count		= " count(*) as cnt ";

$sql_table		= "select {$sql_field} from event_finalexam_member";
$sql_table_cnt	= "select {$sql_count} from event_finalexam_member";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_table_cnt} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts	= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값

$th_class = $td_class = "";	// 상단테이블 css-class

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호', 'efm_no', 0));
array_push($fetch_row, array('회원', 'efm_member', 0));
array_push($fetch_row, array('시험 교시(번호)', 'efm_efp_no', 0));
array_push($fetch_row, array('시험응시수', 'efm_count', 0));
array_push($fetch_row, array('시험응시일', 'efm_question', 0));
array_push($fetch_row, array('시험점수', 'efm_highscore', 0));
array_push($fetch_row, array('시험점수합계', 'efm_highscore_sum', 0));
array_push($fetch_row, array('배송-연락처', 'efm_tel', 0));
array_push($fetch_row, array('배송-주소<br/>우편번호', 'efm_address', 0));
array_push($fetch_row, array('배송-이름', 'efm_name', 0));
array_push($fetch_row, array('저장일', 'efm_date', 0));

$page_title = "시험지결과-회원";
$cms_head_title = $page_title;
/* 여기부터 다시 */
$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$cms_page_title;?></h1>
	<div>
		<strong>검색 수 : <?=number_format($rows_cnts);?> 건</strong>
	</div>
</section>


<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="cms_search_form" id="cms_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return cms_search_submit();">
		<input type="hidden" name="big" id="big" value="<?=$cm_big?>">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_s_type_btn">
				<?	tag_radios($efm_arr, "efp_no", $efp_no, 'y', '전체', ''); ?><br/>
				
				<?	$efm_order = array('', '시험점수합계-높은순','1교시험점수-높은순','2교시험점수-높은순','3교시험점수-높은순');
				tag_selects($efm_order, "efm_order", $efm_order, 'y', '시험점수전체'); ?>
				</div>
			</div>
			<div class="cs_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="72" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="event_result">
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$ordefm_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$ordefm_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahrefm_s = $th_ahrefm_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahrefm_s = '<a href="'.$_cms_self.'?v=2&page=1&ordefm_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahrefm_e = '</a>';
					}
					if($fetch_val[1] == $_ordefm_field){ $th_title_giho = $ordefm_giho; }
					$th_title = $th_ahrefm_s.$fetch_val[0].$th_title_giho.$th_ahrefm_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){ 
					$db_mb = mb_get_no($dvalue_val['efm_member']);
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['efm_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center">
						<?=$db_mb['efm_member'];?>
						<a href="#" onclick="popup('<?=NM_ADM_URL?>/members/member_view.php?mb_no=<?=$db_mb['mb_no'];?>','member_view', <?=$popup_cms_width;?>, 550);"><?=$db_mb['mb_id'];?></a>
					</td>

					<td class="<?=$fetch_row[4][1]?> text_center">
						<?=$efm_arr[$dvalue_val['efm_efp_no1']];?>(<?=$dvalue_val['efm_efp_no1'];?>)<br/>
						<?=$efm_arr[$dvalue_val['efm_efp_no2']];?>(<?=$dvalue_val['efm_efp_no2'];?>)<br/>
						<?=$efm_arr[$dvalue_val['efm_efp_no3']];?>(<?=$dvalue_val['efm_efp_no3'];?>)
					</td>

					<td class="<?=$fetch_row[2][1]?> text_center">
						<?=$dvalue_val['efm_count1'];?><br/>
						<?=$dvalue_val['efm_count2'];?><br/>
						<?=$dvalue_val['efm_count3'];?>
					</td>

					<td class="<?=$fetch_row[3][1]?> text_center">
						<?=$dvalue_val['efm_date1'];?><br/>
						<?=$dvalue_val['efm_date2'];?><br/>
						<?=$dvalue_val['efm_date3'];?>
					</td>

					<td class="<?=$fetch_row[5][1]?> text_center">
						<?=$dvalue_val['efm_highscore1'];?><br/>
						<?=$dvalue_val['efm_highscore2'];?><br/>
						<?=$dvalue_val['efm_highscore3'];?>
					</td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$dvalue_val['efm_highscore_sum'];?></td>
					<td class="<?=$fetch_row[7][1]?> text_center"><?=$dvalue_val['efm_tel'];?></td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=$dvalue_val['efm_address'];?><br/><?=$dvalue_val['efm_postal'];?></td>
					<td class="<?=$fetch_row[9][1]?> text_center"><?=$dvalue_val['efm_name'];?></td>
					<td class="<?=$fetch_row[10][1]?> text_center"><?=$dvalue_val['efm_date'];?></td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>
<? rows_page(); ?>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>