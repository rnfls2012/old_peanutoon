<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode', 'ef_no');
array_push($para_list, 'ef_efp_no', 'ef_type', 'ef_question');
array_push($para_list, 'ef_answer');
array_push($para_list, 'ef_state');
array_push($para_list, 'ef_reg_date', 'ef_mod_date');

for($i=1; $i<=5; $i++){ 
	array_push($para_list, 'ef_choice'.$i);
	array_push($blank_list, 'ef_choice'.$i); /* 빈칸 PARAMITER 허용 */
}


/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'ef_no', 'ef_answer');

/* 빈칸 PARAMITER 허용 */

/* 빈칸&숫자 PARAMITER 허용 */

/* 빈칸 PARAMITER 시 NULL 처리 */

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();
if($_ef_date_start > $_ef_date_end) {
	pop_close("시작 날짜와 종료 날짜를 확인해 주세요!");
	die;
}

$dbtable = "event_finalexam";
$dbt_primary = "ef_no";
$para_primary = "ef_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';
if($_mode == 'reg'){	
	/* 고정값 */
	$_ef_reg_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);
	
	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = '이벤트가 등록되었습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '등록 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

/* 수정 */
}else if($_mode == 'mod'){
	$_ef_mod_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $_ef_no.'의 데이터가 수정되었습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 데이터 삭제 */
		sql_query("delete from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
		
		$db_result['msg'] = $_ef_no.'의 데이터가 삭제되었습니다.';
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
echo $db_result['error']."<br/>";
die
*/


/* 이미지 처리 */
if($_mode != 'del' && $db_result['state'] == 0){
	/* 등록이라면~ [ eventnum 이 NULL 이라면 ] */
	if($_ef_no == ''){
		$sql_max = 'select COALESCE(max('.$dbt_primary.'),0) as '.$dbt_primary.'_new from '.$dbtable; /* 방금 등록번호 가져오기 */
		$row_max = sql_fetch($sql_max);
		$_ef_no = $row_max[$dbt_primary.'_new'];
	}
	/* 이미지 경로 */

	/* 대분류 경로설정 - 폴더 체크 및 생성 */
	$path_finalexam = 'finalexam/'.$_ef_efp_no.'/'.$_ef_no.'/';

	
	
	/* ef_example_img */

		/* 업로드한 이미지 명 */
		$ef_example_img_tmp_name = $_FILES['ef_example_img']['tmp_name'];
		$ef_example_img_name = $_FILES['ef_example_img']['name'];

		/* 확장자 얻기 .png .jpg .gif */
		$ef_example_img_extension = substr($ef_example_img_name, strrpos($ef_example_img_name, "."), strlen($ef_example_img_name));

		/* 고정된 파일 명 */
		$ef_example_img_name_define = 'ef_example_img'.$_ef_no.strtolower($ef_example_img_extension);

		/* 저장 경로 및 DB 파일명 및 파일경로 저장 */
		$ef_example_img_src = $path_finalexam.$ef_example_img_name_define;

		/* 파일 업로드 */	
		$ef_example_img_result = false;
		if ($ef_example_img_name != '') { 	
			$ef_example_img_result = kt_storage_upload($ef_example_img_tmp_name, $path_finalexam, $ef_example_img_name_define); 
		}
		
		/* 임시파일이 존재하는 경우 삭제 */
		if (file_exists($ef_example_img_tmp_name) && is_file($ef_example_img_tmp_name)) {
			unlink($ef_example_img_tmp_name);
		}



	/* ef_choice_img */
	for($c=1; $c<=5; $c++){

		/* 업로드한 이미지 명 */
		${'ef_choice'.$c.'_img_tmp_name'} = $_FILES['ef_choice'.$c.'_img']['tmp_name'];
		${'ef_choice'.$c.'_img_name'} = $_FILES['ef_choice'.$c.'_img']['name'];

		/* 확장자 얻기 .png .jpg .gif */
		${'ef_choice'.$c.'_img_extension'} = substr(${'ef_choice'.$c.'_img_name'}, strrpos(${'ef_choice'.$c.'_img_name'}, "."), strlen(${'ef_choice'.$c.'_img_name'}));

		/* 고정된 파일 명 */
		${'ef_choice'.$c.'_img_name_define'} = 'ef_choice'.$c.'_img'.$_ef_no.strtolower(${'ef_choice'.$c.'_img_extension'});

		/* 저장 경로 및 DB 파일명 및 파일경로 저장 */
		${'ef_choice'.$c.'_img_src'} = $path_finalexam.${'ef_choice'.$c.'_img_name_define'};


		/* 파일 업로드 */	
		${'ef_choice'.$c.'_img_result'} = false;
		if (${'ef_choice'.$c.'_img_name'} != '') { 		
			${'ef_choice'.$c.'_img_result'} = kt_storage_upload(${'ef_choice'.$c.'_img_tmp_name'}, $path_finalexam, ${'ef_choice'.$c.'_img_name_define'}); 
		}
		
		/* 임시파일이 존재하는 경우 삭제 */
		if (file_exists(${'ef_choice'.$c.'_img_tmp_name'}) && is_file(${'ef_choice'.$c.'_img_tmp_name'})) {
			unlink(${'ef_choice'.$c.'_img_tmp_name'});
		}
	}

	$sql_image_bool = false;

	$sql_image = ' UPDATE '.$dbtable.' SET ';
	/* 이미지 field값 추가 */
	if ($ef_example_img_name != ''){ 
		$sql_image.= ' ef_example_img = "'.$ef_example_img_src.'", '; 
		$sql_image_bool = true;
	}

	for($c=1; $c<=5; $c++){
		if (${'ef_choice'.$c.'_img_name'} != ''){ 
			$sql_image.= ' ef_choice'.$c.'_img = "'.${'ef_choice'.$c.'_img_src'}.'", '; 
			$sql_image_bool = true;
		}
	}


	$sql_image = substr($sql_image,0,strrpos($sql_image, ","));
	$sql_image.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";

	
	if($sql_image_bool == true){
		/* DB 저장 */
		if(sql_query($sql_image)){
			$db_result['msg'].= '\n'.$_ef_no.'의 이미지가 저장되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'].= '\n'.'이미지 에러가 발생하여 저장되지 않았습니다.';
			$db_result['error'].= $sql_image;
		}
	}

	// 타입에 따라서 재 업데이트
	$sql_update_type_img = ' UPDATE '.$dbtable.' SET ';
	$sql_update_type_img_case = "";
	switch($ef_type){
		case '1' : 
		case 1 : 
			$sql_update_type_img_case.= ' ef_example_img = "", '; 
			for($c=1; $c<=5; $c++){
				$sql_update_type_img_case.= ' ef_choice'.$c.'_img = "", '; 
			}
		break;
		case '2' : 
		case 2 : 
			$sql_update_type_img_case.= ' ef_example_img = "", '; 
		break;
		case '3' : 
		case 3 : 
			for($c=1; $c<=5; $c++){
				$sql_update_type_img_case.= ' ef_choice'.$c.'_img = "", '; 
			}
		break;
		default: $sql_update_type_img = $sql_update_type_img_case;
		break;
	}
	if($sql_update_type_img !='' && $sql_update_type_img_case !=''){
		$sql_update_type_img_case = substr($sql_update_type_img_case,0,strrpos($sql_update_type_img_case, ","));

		$sql_update_type_img.= $sql_update_type_img_case." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
	
		/* DB 저장 */
		if(sql_query($sql_update_type_img)){
			$db_result['msg'].= '\n'.$_ef_no.'의 이미지&타입이가 저장되였습니다.';
		}
	}
}

pop_close($db_result['msg'], '', $db_result['error']);
die;

?>