<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_ef_no = tag_filter($_REQUEST['ef_no']);

/* 시험설정 가져오기 */
$sql_ef = "SELECT * FROM event_finalexam_period ORDER BY efp_no";
$ef_arr = array();
$result_ef = sql_query($sql_ef);
while ($row_ef = sql_fetch_array($result_ef)) {
	$ef_arr[$row_ef['efp_no']] = $row_ef['efp_mark'].'교시';
}



$sql = "SELECT * FROM event_finalexam WHERE ef_no = '$_ef_no' ";
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
			 $sql_list = "";
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
} // end if

/* 대분류로 제목 */
$page_title = "시험설정";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- event_free_dc_head -->

<section id="event_write">
	<form name="event_finalexam_write_form" id="event_finalexam_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return event_finalexam_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="ef_no" name="ef_no" value="<?=$_ef_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ef_no">이벤트 번호</label></th>
					<td><?=$_ef_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="ef_efp_no"><?=$required_arr[0];?>교시</label></th>
					<td>
						<? tag_selects($ef_arr, "ef_efp_no", $row['ef_efp_no'], 'n'); ?>
					</td>
				</tr>
				<tr>
					<th><label for="ef_type"><?=$required_arr[0];?>시험 타입</label></th>
					<td>
						시험지-객관식<br/>
						<? $ef_type_arr = array(1=>'텍스트-텍스트',2=>'텍스트-이미지',3=>'보기이미지-텍스트',4=>'보기이미지-이미지');
						tag_selects($ef_type_arr, "ef_type", $row['ef_type'], 'n'); ?>
					</td>
				</tr>

				<tr>
					<th><label for="ef_question"><?=$required_arr[0];?>시험 문제</label></th>
					<td>
						<input type="text" placeholder="시험 문제 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="ef_question" id="ef_question" value="<?=$row['ef_question'];?>" autocomplete="off"/>
					</td>
				</tr>
				<tr class="ef_type3 ef_type4">
					<th><label for="ef_example_img">시험 문제 보기 이미지</label></th>
					<td>
						<?  $ef_example_img_url = "";
							if($row['ef_example_img']!=''){ 
							$ef_example_img_url = img_url_para($row['ef_example_img'], $row['ef_reg_date'], $row['ef_mod_date']);
						?>
						<img src="<?=$ef_example_img_url?>" alt="시험 문제 보기 이미지" style="width:200px;">
						<? } ?>
						<input type="file" name="ef_example_img" id="ef_example_img" data-value="<?=$row['ef_example_img']?>" />
					</td>
				</tr>
				
				<tr class="ef_type1 ef_type3">
					<th><label for="ef_choice">시험 문제 <br/>객관식-텍스트</label></th>
					<td>
						<? for($i=1; $i<=5; $i++){ ?>
						<label for="ef_choice<?=$i;?>"><?=$i;?>.</label>
						<input style="width:90%; margin-bottom:5px;" type="text" placeholder="객관식-텍스트<?=$i;?> 입력해주세요" name="ef_choice<?=$i;?>" id="ef_choice<?=$i;?>" value="<?=$row['ef_choice'.$i];?>" autocomplete="off"/>
						<br/>
						<? } ?>
					</td>
				</tr>
				
				<tr class="ef_type2 ef_type4">
					<th><label for="ef_choice_img">시험 문제 <br/>객관식-이미지</label></th>
					<td>
						<? for($c=1; $c<=5; $c++){ ?>
							<label for="ef_choice<?=$c;?>_img"><?=$c;?>.</label>
							
							<? ${'ef_choice'.$c.'_img_url'} = img_url_para($row['ef_choice'.$c.'_img'], $row['ef_reg_date'], $row['ef_mod_date']);
							if($row['ef_choice'.$c.'_img']!=''){ ?>
							<img src="<?=${'ef_choice'.$c.'_img_url'};?>" alt="시험 문제 객관식-이미지<?=$c;?>" style="width:100px;">
							<? } ?>
							<input style="margin-bottom:5px;" type="file" name="ef_choice<?=$c;?>_img" id="ef_choice<?=$c;?>_img" data-value="<?=$row['ef_choice'.$c.'_img']?>" />
							<br/>
						<? } ?>
					</td>
				</tr>
				
				<tr>
					<th><label for="ef_answer"><?=$required_arr[0];?>시험 문제 답</label></th>
					<td>
						<? $ef_answer_arr = array(1=>'1번',2=>'2번',3=>'3번',4=>'4번',5=>'5번');
						tag_selects($ef_answer_arr, "ef_answer", $row['ef_answer'], 'n'); ?>
					</td>
				</tr>

				<tr>
					<th><label><?=$required_arr[0];?>시험지 사용 여부</label></th>
					<td>
						<div id="ef_state" class="input_btn">
							<input type="radio" name="ef_state" id="ef_statey" value="y" <?=get_checked('', $row['ef_state']);?> <?=get_checked('y', $row['ef_state']);?> />
							<label for="ef_statey">사용</label>
							<input type="radio" name="ef_state" id="ef_staten" value="n" <?=get_checked('n', $row['ef_state']);?> />
							<label for="ef_staten">미사용</label>
						</div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ef_reg_date">등록일</label></th>
					<td><?=$row['ef_reg_date'];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ef_mod_date">수정일</label></th>
					<td><?=$row['ef_mod_date'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- event_free_dc_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>