<? include_once '_common.php'; // 공통


// 선택박스 검색 데이터 쿼리문 생성
$nm_paras_check = array('efp_state');
foreach($nm_paras_check as $nm_paras_key => $nm_paras_val){
	if(${"_".$nm_paras_val} != ''){
		$event_randombox_where.= "and ".$nm_paras_val." = '".${"_".$nm_paras_val}."' ";
	}
}

// 그룹
$sql_group = "";

if($_s_limit == ''){ $_s_limit = 10; }

// 정렬
if($_ordefp_field == null || $_ordefp_field == ""){ $_ordefp_field = "efp_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }

// SQL문
$sql_field		= " * ";
$sql_count		= " count(*) as cnt ";

$sql_table		= "select {$sql_field} from event_finalexam_period";
$sql_table_cnt	= "select {$sql_count} from event_finalexam_period";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_table_cnt} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts	= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값

$th_class = $td_class = "";	// 상단테이블 css-class

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('시험설정 번호', 'efp_no', 0));
array_push($fetch_row, array('시험 교시표기', 'efp_mark', 0));
array_push($fetch_row, array('시험 교시명', 'efp_title', 0));
array_push($fetch_row, array('이벤트 기간', 'efp_date', 0));
array_push($fetch_row, array('이벤트 상태', 'efp_state', 0));
array_push($fetch_row, array('등록일', 'efp_date', 0));
array_push($fetch_row, array('수정일', 'efp_mod_date', 0));
array_push($fetch_row, array('관리', 'efp_management', 0));

$page_title = "시험설정";
$cms_head_title = $page_title;
/* 여기부터 다시 */
$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$cms_page_title;?></h1>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','finalexam_period_write', <?=$popup_cms_width;?>, 550);"><?=$page_title;?> 등록</button>
	</div>
</section>

<section id="event_result">
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$ordefp_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$ordefp_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&ordefp_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_ordefp_field){ $th_title_giho = $ordefp_giho; }
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){ 
					$popup_url = $_cms_write."?efp_no=".$dvalue_val['efp_no'];

					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					// 이벤트 기간
					$efp_start_date = $dvalue_val['efp_date_start']." (".$dvalue_val['efp_date_start_hour'].":00:00)";
					$efp_end_date = $dvalue_val['efp_date_end']." (".$dvalue_val['efp_date_end_hour'].":59:59)";
					$efp_date = $efp_start_date."<br/>".$efp_end_date;

					// 할인권 유효 기간
					$efp_available_date = $dvalue_val['efp_available_date_start']." ~ ".$dvalue_val['efp_available_date_end'];
					
					// URL
					$event_link = "eventrandombox.php";
					if($dvalue_val['efp_event_type'] == "f") {
						$event_link = "eventfullmoon.php";
						if(strpos($dvalue_val['efp_name'], "봄") !== false) {
							$event_link = "eventspring.php";
						} // end if
					} // end if
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['efp_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['efp_mark'];?>교시</td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['efp_title'];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$efp_date;?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$d_state[$dvalue_val['efp_state']];?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$dvalue_val['efp_reg_date'];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$dvalue_val['efp_mod_date'];?></td>
					<td class="<?=$fetch_row[7][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','event_random_mod_wirte', <?=$popup_cms_width;?>, 650);">수정</button>
						<button class="del_random_btn" onclick="popup('<?=$popup_del_url;?>','event_random_del_wirte', <?=$popup_cms_width;?>, 650);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>
<? rows_page(); ?>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>

<?	/* 해당 페이지에서 사용하는 function */
// 이벤트 총수
function total_event_randombox()
{
	$sql_event_randombox_total = "select count(*) as total_event_randombox from event_randombox ";
	$total_event_randombox = sql_count($sql_event_randombox_total, 'total_event_randombox');
	return $total_event_randombox; 
}


?>