<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_efp_no = tag_filter($_REQUEST['efp_no']);

$sql = "SELECT * FROM event_finalexam_period WHERE efp_no = '$_efp_no' ";
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
			 $sql_list = "";
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
} // end if

/* 대분류로 제목 */
$page_title = "시험설정";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- event_free_dc_head -->

<section id="event_write">
	<form name="event_finalexam_period_write_form" id="event_finalexam_period_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return event_finalexam_period_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="efp_no" name="efp_no" value="<?=$_efp_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="efp_no">이벤트 번호</label></th>
					<td><?=$_efp_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="efp_name"><?=$required_arr[0];?>교시표기</label></th>
					<td>
						<? $efp_mark_arr = array(1=>'1교시',2=>'2교시',3=>'3교시');
						tag_selects($efp_mark_arr, "efp_mark", $row['efp_mark'], 'n'); ?>
					</td>
				</tr>
				<!-- 이벤트 타입 추가 180227 -->
				<tr>
					<th><label for="efp_title"><?=$required_arr[0];?>교시명</label></th>
					<td>
						<input type="text" placeholder="교시명 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="efp_title" id="efp_title" value="<?=$row['efp_title'];?>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<th><label for="efp_memo">시험 범위</label></th>
					<td>
						<textarea style="width:100%;height:100px;" name="efp_memo"><?=$row['efp_memo'];?></textarea>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>시험 기간</label></th>
					<td>
						<div class="efp_date_type_view">
							<span for="s_date">시작일 :</span>
							<input type="text" class="s_date readonly" placeholder="클릭하세요" name="efp_date_start" id="s_date" value="<?=$row['efp_date_start'];?>" autocomplete="off" readonly /> ~ 
							<span for="e_date">종료일 :</span>
							<input type="text" class="e_date readonly" placeholder="클릭하세요" name="efp_date_end" id="e_date" value="<?=$row['efp_date_end'];?>" autocomplete="off" readonly />
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>사용 여부</label></th>
					<td>
						<div id="efp_state" class="input_btn">
							<input type="radio" name="efp_state" id="efp_statey" value="y" <?=get_checked('', $row['efp_state']);?> <?=get_checked('y', $row['efp_state']);?> />
							<label for="efp_statey">사용</label>
							<input type="radio" name="efp_state" id="efp_staten" value="n" <?=get_checked('n', $row['efp_state']);?> />
							<label for="efp_staten">미사용</label>
							<!--
							<input type="radio" name="efp_state" id="efp_stater" value="r" <?=get_checked('r', $row['efp_state']);?> />
							<label for="efp_stater">예약</label>
							-->
						</div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="efp_reg_date">등록일</label></th>
					<td><?=$row['efp_reg_date'];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="efp_mod_date">수정일</label></th>
					<td><?=$row['efp_mod_date'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- event_free_dc_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>