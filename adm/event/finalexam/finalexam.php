<? include_once '_common.php'; // 공통

$efp_no = num_check(tag_get_filter($_REQUEST['efp_no'])); // 교시값 파라미터

/* 시험설정 가져오기 */
$sql_ef = "SELECT * FROM event_finalexam_period ORDER BY efp_no";
$ef_arr = array();
$result_ef = sql_query($sql_ef);
while ($row_ef = sql_fetch_array($result_ef)) {
	$ef_arr[$row_ef['efp_no']] = $row_ef['efp_mark'].'교시';
}

// 조건문
if($efp_no > 0){
	$sql_where.= " AND ef_efp_no = '".$efp_no."' ";
}

// 그룹
$sql_group = "";

if($_s_limit == ''){ $_s_limit = 30; }

// 정렬
if($_ordef_field == null || $_ordef_field == ""){ $_ordef_field = "ef_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }

// SQL문
$sql_field		= " * ";
$sql_count		= " count(*) as cnt ";

$sql_table		= "select {$sql_field} from event_finalexam";
$sql_table_cnt	= "select {$sql_count} from event_finalexam";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_table_cnt} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts	= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값

$th_class = $td_class = "";	// 상단테이블 css-class

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('시험지 번호', 'ef_no', 0));
array_push($fetch_row, array('시험 교시 (시험설정번호)', 'ef_efp_no', 0));
array_push($fetch_row, array('시험 타입', 'ef_type', 0));
array_push($fetch_row, array('시험 문제', 'ef_question', 0));
array_push($fetch_row, array('시험 정답', 'ef_answer', 0));
array_push($fetch_row, array('시험 상태', 'ef_state', 0));
array_push($fetch_row, array('등록일', 'ef_date', 0));
array_push($fetch_row, array('수정일', 'ef_mod_date', 0));
array_push($fetch_row, array('관리', 'ef_management', 0));

$page_title = "시험지";
$cms_head_title = $page_title;
/* 여기부터 다시 */
$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$cms_page_title;?></h1>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','finalexam_period_write', <?=$popup_cms_width;?>, 550);"><?=$page_title;?> 등록</button>
	</div>
</section>


<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="cms_search_form" id="cms_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return cms_search_submit();">
		<input type="hidden" name="big" id="big" value="<?=$cm_big?>">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_s_type_btn">
				<?	tag_radios($ef_arr, "efp_no", $efp_no, 'y', '전체', ''); ?>
				</div>
			</div>
			<div class="cs_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="72" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->


<section id="event_result">
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$ordef_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$ordef_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&ordef_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_ordef_field){ $th_title_giho = $ordef_giho; }
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){ 
					$popup_url = $_cms_write."?ef_no=".$dvalue_val['ef_no'];

					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					$ef_type_arr = array(1=>'텍스트-텍스트',2=>'텍스트-이미지',3=>'보기이미지-텍스트',4=>'보기이미지-이미지');
					$ef_state = array('n'=>'사용안함','y'=>'사용함');

				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['ef_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center">
						<?=$ef_arr[$dvalue_val['ef_efp_no']];?> (<?=$dvalue_val['ef_efp_no'];?>)
					</td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$ef_type_arr[$dvalue_val['ef_type']];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['ef_question'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['ef_answer'];?></td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$ef_state[$dvalue_val['ef_state']];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$dvalue_val['ef_reg_date'];?></td>
					<td class="<?=$fetch_row[7][1]?> text_center"><?=$dvalue_val['ef_mod_date'];?></td>
					<td class="<?=$fetch_row[8][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','event_random_mod_wirte', <?=$popup_cms_width;?>, 650);">수정</button>
						<button class="del_random_btn" onclick="popup('<?=$popup_del_url;?>','event_random_del_wirte', <?=$popup_cms_width;?>, 650);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>
<? rows_page(); ?>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>

<?	/* 해당 페이지에서 사용하는 function */
// 이벤트 총수
function total_event_randombox()
{
	$sql_event_randombox_total = "select count(*) as total_event_randombox from event_randombox ";
	$total_event_randombox = sql_count($sql_event_randombox_total, 'total_event_randombox');
	return $total_event_randombox; 
}


?>