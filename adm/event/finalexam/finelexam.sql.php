CREATE TABLE event_finalexam_period (
	efp_no INT(11) NOT NULL AUTO_INCREMENT COMMENT '번호',
	efp_mark TINYINT(4) NOT NULL DEFAULT '0' COMMENT '교시표시',
	efp_title VARCHAR(20) NOT NULL DEFAULT '' COMMENT '교시명',
	efp_memo VARCHAR(200) NOT NULL DEFAULT '' COMMENT '교시메모',

	efp_start_date VARCHAR(20) NOT NULL DEFAULT '' COMMENT '시험 시작',
	efp_date_start_hour VARCHAR(2) NOT NULL DEFAULT '00' COMMENT '시험 시작 시간',
	efp_end_date VARCHAR(20) NOT NULL DEFAULT '' COMMENT '시험 마감',
	efp_date_end_hour VARCHAR(2) NOT NULL DEFAULT '23' COMMENT '시험 마감 시간',
	
	efp_chance TINYINT(4) NOT NULL DEFAULT '3' COMMENT '시험 응시 횟수',
	efp_time TINYINT(4) NOT NULL DEFAULT '15' COMMENT '시험 시간',

	efp_state VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '사용유무(y: 사용, n: 미사용)',

	efp_reg_date VARCHAR(20) NOT NULL DEFAULT '' COMMENT '등록일',
	efp_mod_date VARCHAR(20) NOT NULL DEFAULT '' COMMENT '수정일',


	PRIMARY KEY (efp_no),
	INDEX efp_state (efp_state)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;


CREATE TABLE event_finalexam (
	ef_no INT(11) NOT NULL AUTO_INCREMENT COMMENT '번호',
	ef_efp_no INT(11) NOT NULL DEFAULT '0' COMMENT '시험 교시 번호',

	ef_type TINYINT(4) NOT NULL DEFAULT '0' COMMENT '시험 타입:1~4',

	ef_question VARCHAR(400) NOT NULL DEFAULT '' COMMENT '시험 문제',
	ef_example_img VARCHAR(100) NOT NULL DEFAULT '' COMMENT '시험 문제 보기 이미지',

	ef_choice1 VARCHAR(100) NOT NULL DEFAULT '' COMMENT '시험 문제 객관식1',
	ef_choice2 VARCHAR(100) NOT NULL DEFAULT '' COMMENT '시험 문제 객관식2',	
	ef_choice3 VARCHAR(100) NOT NULL DEFAULT '' COMMENT '시험 문제 객관식3',
	ef_choice4 VARCHAR(100) NOT NULL DEFAULT '' COMMENT '시험 문제 객관식4',
	ef_choice5 VARCHAR(100) NOT NULL DEFAULT '' COMMENT '시험 문제 객관식5',

	ef_choice1_img VARCHAR(100) NOT NULL DEFAULT '' COMMENT '시험 문제 객관식1 이미지',
	ef_choice2_img VARCHAR(100) NOT NULL DEFAULT '' COMMENT '시험 문제 객관식2 이미지',	
	ef_choice3_img VARCHAR(100) NOT NULL DEFAULT '' COMMENT '시험 문제 객관식3 이미지',
	ef_choice4_img VARCHAR(100) NOT NULL DEFAULT '' COMMENT '시험 문제 객관식4 이미지',
	ef_choice5_img VARCHAR(100) NOT NULL DEFAULT '' COMMENT '시험 문제 객관식5 이미지',

	ef_answer TINYINT(4) NOT NULL DEFAULT '0' COMMENT '시험 문제 답',

	ef_reg_date VARCHAR(20) NOT NULL DEFAULT '' COMMENT '등록일',
	ef_mod_date VARCHAR(20) NOT NULL DEFAULT '' COMMENT '수정일',

	PRIMARY KEY (ef_no),
	INDEX ef_period_no (ef_period_no),
	INDEX ef_type (ef_type)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

CREATE TABLE event_finalexam_member (
	efm_no INT(11) NOT NULL AUTO_INCREMENT COMMENT '번호',
	efm_efp_no INT(11) NOT NULL DEFAULT '0' COMMENT '시험 교시 번호',
	efm_member INT(11) NOT NULL DEFAULT '0' COMMENT '회원 번호',
	efm_member_idx VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원 ID INDEX',
	efm_count TINYINT(4) NOT NULL DEFAULT '0' COMMENT '시험응시수',
	efm_highscore1 TINYINT(4) NOT NULL DEFAULT '0' COMMENT '시험점수',
	efm_highscore2 TINYINT(4) NOT NULL DEFAULT '0' COMMENT '시험점수',
	efm_highscore3 TINYINT(4) NOT NULL DEFAULT '0' COMMENT '시험점수',
	efm_reg_date VARCHAR(20) NOT NULL DEFAULT '' COMMENT '등록일',
	PRIMARY KEY (efm_no),
	INDEX efm_member_idx (efm_member_idx)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;



CREATE TABLE z_event_finalexam_member (
	z_efm_no INT(11) NOT NULL AUTO_INCREMENT COMMENT '번호',
	z_efm_efp_no INT(11) NOT NULL DEFAULT '0' COMMENT '시험 교시 번호',

	z_efm_member INT(11) NOT NULL DEFAULT '0' COMMENT '회원 번호',
	z_efm_member_idx VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원 ID INDEX',

	z_efm_count TINYINT(4) NOT NULL DEFAULT '0' COMMENT '시험응시수',

	z_efm_test VARCHAR(100) NOT NULL DEFAULT '' COMMENT '시험 문제 리스트',
	z_efm_answer VARCHAR(100) NOT NULL DEFAULT '' COMMENT '시험 문제답 리스트',
	z_efm_score TINYINT(4) NOT NULL DEFAULT '0' COMMENT '시험점수',

	z_efm_reg_date VARCHAR(20) NOT NULL DEFAULT '' COMMENT '등록일',

	PRIMARY KEY (z_efm_no),
	INDEX z_efm_member_idx (z_efm_member_idx)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;


