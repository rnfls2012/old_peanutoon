<?
include_once '_common.php'; // 공통

// 기본적으로 몇개 있는 지 체크;
$sql_event_randombox_total = "select count(*) as total_event_randombox from event_randombox where 1  ";
$total_event_randombox = sql_count($sql_event_randombox_total, 'total_event_randombox');

/* 데이터 가져오기 */
$event_randombox_where = '';

// 선택박스 검색 데이터 쿼리문 생성
$nm_paras_check = array('er_state');
foreach($nm_paras_check as $nm_paras_key => $nm_paras_val){
	if(${"_".$nm_paras_val} != ''){
		$event_randombox_where.= "and ".$nm_paras_val." = '".${"_".$nm_paras_val}."' ";
	}
}

// 시작일/종료일 날짜
$start_date = $end_date = "";
if($_date_type){ //all, ep_date_start, ep_date_end
	if($_s_date && $_e_date){
		$start_date = $_s_date;
		$end_date = $_e_date;
	}else if($_s_date){
		$start_date = $_s_date;
		$end_date = NM_TIME_YMD;
	}
	switch($_date_type){
		case 'all' : $event_randombox_where.= "and ( er_date_start >= '$start_date' and er_date_end <= '$end_date') ";
		break;

		case 'er_date_start' : $event_randombox_where.= "and er_date_start >= '$start_date' ";
		break;

		case 'er_date_end' : $event_randombox_where.= "and er_date_end <= '$end_date' ";
		break;

		default: $event_randombox_where.= " ";
		break;
	}
}else{
	$_s_date = "";
	$_e_date = "";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "er_weight "; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$event_randombox_order = "order by ".$_order_field." ".$_order;

$event_randombox_sql = "";
$event_randombox_field = " * "; // 가져올 필드 정하기
$event_randombox_limit = "";

$event_randombox_sql = "SELECT $event_randombox_field FROM event_randombox where 1 $event_randombox_where $event_randombox_order $event_randombox_limit";
$result = sql_query($event_randombox_sql);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','er_no',1));
array_push($fetch_row, array('할인율','er_discount_rate',1));
array_push($fetch_row, array('확률(비율)','er_weight',1));
array_push($fetch_row, array('시작일','er_date_start',1));
array_push($fetch_row, array('마감일','er_date_end',1));
array_push($fetch_row, array('할인권 상태','er_state',1));
array_push($fetch_row, array('등록일','er_reg_date',1));
array_push($fetch_row, array('수정일','er_mod_date',1));
array_push($fetch_row, array('관리','er_management',0));

$cs_calendar_on = $cs_submit_h = '';
if($_date_type){
	$cs_calendar_on = 'cs_calendar_on';
	$cs_submit_h = 'cs_submit_on';
}

$page_title = "랜덤박스";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> 할인권 : 총 <?=number_format($total_event_pay);?> 종류</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','randombox_wirte', <?=$popup_cms_width;?>, 450);"><?=$page_title?> 할인권 등록</button>
	</div>
</section>

<section id="cms_explain">
	<ul>
		<li>할인율은 기존 금액에 x% 대비 할인율입니다. </li>
		<li>확률은 비율로써, 100%를 굳이 만들지 않아도 됩니다. </li>
		<li>더 많이 나오게 하고싶은 할인권에 더 높은 수를 주면 됩니다. </li>
	</ul>
</section><!-- cms_explain -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="event_randombox_search_form" id="event_randombox_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return event_randombox_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_er_type_btn">
					<?	 tag_selects($d_state, "er_state", $_er_state, 'y', '상태 전체', ''); ?> <!-- 여기 수정 배열 -->
				</div>
				<div class="cs_date_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<?	tag_selects($d_er_date_type, "date_type", $_date_type, 'y', '사용가능날짜 검색안함', ''); ?> <!-- 여기 수정 배열 -->
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit randombox_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="45" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="event_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 할인권 수정/삭제 */
					$popup_url = $_cms_write."?er_no=".$dvalue_val['er_no'];

					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					// 등록일
					$db_er_reg_date_ymd = get_ymd($dvalue_val['er_reg_date']);
					$db_er_reg_date_his = get_his($dvalue_val['er_reg_date']);

					// 수정일
					$db_er_mod_date_ymd = get_ymd($dvalue_val['er_mod_date']);
					$db_er_mod_date_his = get_his($dvalue_val['er_mod_date']);

					$db_er_no_date_text = "무기간";
					if($dvalue_val['er_date_type'] != 'n' && $dvalue_val['er_date_start'] == '' && $dvalue_val['er_date_end'] == ''){
						$db_er_no_date_text = "시작일&마감일이 없어 사용이 중지됩니다.";
					} // end if
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['er_no'];?></a></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['er_discount_rate']." %";?></a></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['er_weight'];?></td>
					<?if($dvalue_val['er_date_type'] != 'n' && $dvalue_val['er_date_start'] != '' && $dvalue_val['er_date_end'] != ''){?>
						<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['er_date_start'];?></td>
						<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['er_date_end'];?></td>
					<?}else{?>
						<td colspan='2' class="ep_date_no text_center"><?=$db_er_no_date_text?></td>
					<?}?>		
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$d_state[$dvalue_val['er_state']];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$db_er_reg_date_ymd;?><br/><?=$db_er_reg_date_his;?></td>
					<td class="<?=$fetch_row[7][1]?> text_center"><?=$db_er_mod_date_ymd;?><br/><?=$db_er_mod_date_his;?></td>
					<td class="<?=$fetch_row[8][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','event_random_mod_wirte', <?=$popup_cms_width;?>, 500);">수정</button>
						<button class="del_random_btn" onclick="popup('<?=$popup_del_url;?>','event_random_del_wirte', <?=$popup_cms_width;?>, 500);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>