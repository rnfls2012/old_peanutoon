<?
include_once '_common.php'; // 공통

// 기본적으로 몇개 있는 지 체크;
$sql_event_pay_total = "select count(*) as total_event_pay from event_pay where 1  ";
$total_event_pay = sql_count($sql_event_pay_total, 'total_event_pay');

/* 데이터 가져오기 */
$event_pay_free_dc_where = '';

// 선택박스 검색 데이터 커리문 생성
$nm_paras_check = array('ep_state', 'ep_adult');
foreach($nm_paras_check as $nm_paras_key => $nm_paras_val){
	if(${"_".$nm_paras_val} != ''){
		$event_pay_free_dc_where.= "and ".$nm_paras_val." = '".${"_".$nm_paras_val}."' ";
	}
}

// 검색단어
if($_s_text){
	$event_pay_free_dc_where.= "and ( ep_name like '%$_s_text%' or ep_text like '%$_s_text%' ) ";
}


// 시작일/종료일 날짜
$start_date = $end_date = "";
if($_date_type){ //all, ep_date_start, ep_date_end
	if($_s_date && $_e_date){
		$start_date = $_s_date;
		$end_date = $_e_date;
	}else if($_s_date){
		$start_date = $_s_date;
		$end_date = NM_TIME_YMD;
	}
	switch($_date_type){
		case 'all' : $event_pay_free_dc_where.= "and ( ep_date_start >= '$start_date' and ep_date_end <= '$end_date') ";
		break;

		case 'ep_date_start' : $event_pay_free_dc_where.= "and ep_date_start >= '$start_date' ";
		break;

		case 'ep_date_end' : $event_pay_free_dc_where.= "and ep_date_end <= '$end_date' ";
		break;

		default: $event_pay_free_dc_where.= " ";
		break;
	}
}else{
	$_s_date = "";
	$_e_date = "";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "( CASE ep_state WHEN 'r' THEN 1 WHEN 'y' THEN 2 WHEN 'n' THEN 3 ELSE 4 END ), ep_reg_date "; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$event_pay_free_dc_order = "order by ".$_order_field." ".$_order;

$event_pay_free_dc_sql = "";
$event_pay_free_dc_field = " * "; // 가져올 필드 정하기
$event_pay_free_dc_limit = "";

$event_pay_free_dc_sql = "SELECT $event_pay_free_dc_field FROM event_pay where 1 $event_pay_free_dc_where $event_pay_free_dc_order $event_pay_free_dc_limit";
$result = sql_query($event_pay_free_dc_sql);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','ep_no',1));
array_push($fetch_row, array('배너','ep_cover',0));
array_push($fetch_row, array('이벤트명','ep_name',1));
array_push($fetch_row, array('시작일','ep_date_start',1));
array_push($fetch_row, array('마감일','ep_date_end',1));
array_push($fetch_row, array('이벤트등급','ep_adult',1));
array_push($fetch_row, array('이벤트상태','ep_state',1));
array_push($fetch_row, array('등록일','ep_reg_date',1));
array_push($fetch_row, array('수정일','ep_mod_date',1));
array_push($fetch_row, array('클릭수','ep_click',1));
array_push($fetch_row, array('관리','ep_management',0));

$cs_calendar_on = $cs_submit_h = '';
if($_date_type){
	$cs_calendar_on = 'cs_calendar_on';
	$cs_submit_h = 'cs_submit_on';
}

$page_title = "무료&코인할인";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> 이벤트 : 총 <?=number_format($total_event_pay);?> 건</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','free_dc_wirte', <?=$popup_cms_width;?>, 700);"><?=$page_title?> 등록</button>
	</div>
</section>

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="event_recharge_search_form" id="event_recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return event_recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_er_type_btn">
					<?	 tag_selects($d_state, "ep_state", $_ep_state, 'y', '상태 전체', ''); ?>
					<?	 tag_selects($d_adult, "ep_adult", $_ep_adult, 'y', '등급 전체', ''); ?>
				</div>
				<div class="cs_date_btn">
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>

				<div class="cs_date_btn">
					<?	tag_selects($d_ep_date_type, "date_type", $_date_type, 'y', '이벤트날짜 검색안함', ''); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit free_dc_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="72" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="event_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){
					/* 이미지 표지 */
					$image_bener_url = img_url_para($dvalue_val['ep_cover'], $dvalue_val['ep_reg_date'], $dvalue_val['ep_mod_date'], 100, 65);
					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?ep_no=".$dvalue_val['ep_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					// 등록일
					$db_ep_reg_date_ymd = get_ymd($dvalue_val['ep_reg_date']);
					$db_ep_reg_date_his = get_his($dvalue_val['ep_reg_date']);

					// 수정일
					$db_ep_mod_date_ymd = get_ymd($dvalue_val['ep_mod_date']);
					$db_ep_mod_date_his = get_his($dvalue_val['ep_mod_date']);


					$db_ep_no_date_text = "무기간";
					if($dvalue_val['ep_date_type'] != 'n' && $dvalue_val['ep_date_start'] == '' && $dvalue_val['ep_date_end'] == ''){
						$db_ep_no_date_text = "시작일&마감일 없어서 종료됩니다.";
					}


					if($dvalue_val['er_calc_way'] == 'p'){
						$er_cash_point_text = number_format($dvalue_val['ep_cash_point']).' %';
						$ep_point_text = number_format($dvalue_val['ep_point']).' %';
					}else if($dvalue_val['ep_calc_way'] == 'm'){
						$ep_cash_point_text = 'x '.number_format($dvalue_val['ep_cash_point']);
						$ep_point_text = 'x '.number_format($dvalue_val['ep_point']);
					}else{
						$ep_cash_point_text = '+ '.number_format($dvalue_val['ep_cash_point']).' '.$nm_config['cf_cash_point_unit'];
						$ep_point_text = '+ '.number_format($dvalue_val['ep_point']).' '.$nm_config['cf_point_unit'];
					}

					// 이벤트 링크
					$db_ep_no_link = NM_URL."/eventlist.php?event=".$dvalue_val['ep_no'];
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><a href="<?=$db_ep_no_link;?>"><?=$dvalue_val['ep_no'];?></a></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><a href="<?=$db_ep_no_link;?>"><img src="<?=$image_bener_url;?>" alt="<?=$dvalue_val['ep_name'];?>"/></a></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><a href="<?=$db_ep_no_link;?>"><?=$dvalue_val['ep_name'];?></a></td>
					<?if($dvalue_val['ep_date_type'] != 'n' && $dvalue_val['ep_date_start'] != '' && $dvalue_val['ep_date_end'] != ''){?>
						<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['ep_date_start'];?>일 <?=$dvalue_val['ep_hour_start'];?>시00분</td>
						<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['ep_date_end'];?>일 <?=$dvalue_val['ep_hour_end'];?>시59분</td>
					<?}else{?>
						<td colspan='2' class="ep_date_no text_center"><?=$db_ep_no_date_text?></td>
					<?}?>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$d_adult[$dvalue_val['ep_adult']];?></td>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$d_state[$dvalue_val['ep_state']];?></td>
					<td class="<?=$fetch_row[7][1]?> text_center"><?=$db_ep_reg_date_ymd;?><br/><?=$db_ep_reg_date_his;?></td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=$db_ep_mod_date_ymd;?><br/><?=$db_ep_mod_date_his;?></td>
					<td class="<?=$fetch_row[9][1]?> text_center"><?=number_format($dvalue_val['ep_click']);?></td>
					<td class="<?=$fetch_row[10][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','free_dc_mod_wirte', <?=$popup_cms_width;?>, 500);">수정</button>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','free_dc_del_wirte', <?=$popup_cms_width;?>, 500);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- event_recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>