<?
include_once '_common.php'; // 공통

/* PARAMITER CHECK */
$para_list = array();
array_push($para_list, 'ecm_ticket', 'ecm_cash_point', 'ecm_point', 'ecm_lot', 'ecm_overlap');
array_push($para_list, 'ecm_state', 'ecm_days', 'ecm_start_date', 'e_date', 'ecm_end_date');

/* 숫자 PARAMITER 체크 */
$para_num_list = array();
array_push($para_num_list, 'ecm_cash_point', 'ecm_point', 'ecm_days', 'ecm_lot');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
foreach($para_list as $para_key => $para_val){
	if(in_array($para_val,$para_num_list)){
		if(num_check($_POST[$para_val]) == false && $_POST[$para_val] != '' && $_POST[$para_val] != '0'){
			alert($para_val."값에 숫자가 아닌 값이 들어 있습니다.", $_SERVER['HTTP_REFERER']);
			die;
		}
	} // end if
	
	// e_date를 ecm_end_date로 교체
	if($para_val == "ecm_end_date") {
		${'_'.$para_val} = base_filter($_POST['e_date'])." 23:59:59";
		continue;
	} // end if
	
	${'_'.$para_val} = base_filter($_POST[$para_val]); /* 변수담기 */
} // end foreach

// e_date 배열에서 제거
unset($para_list[implode(array_keys($para_list, 'e_date'))]);

/* 지급되는 캐쉬, 포인트가 둘 다 0일때 */
if($_ecm_cash_point == 0 && $_ecm_point == 0) {
	alert("지급될 ".$nm_config['cf_cash_point_unit_ko']." 또는 ".$nm_config['cf_point_unit_ko']." 을 입력해주세요!", $_SERVER['HTTP_REFERER']);
	die;
} // end if

/* 발급 매수가 0일때 */
if($_ecm_lot == 0) {
	alert("발급 매수를 설정해주세요!", $_SERVER['HTTP_REFERER']);
	die;
} // end if

/* 사용처가 없을때 */
if($_ecm_ticket == "") {
	alert("사용처를 입력해주세요!", $_SERVER['HTTP_REFERER']);
	die;
} // end if

/* 날짜가 비었을때 */
if($_ecm_end_date == "" || ($_e_date < NM_TIME_YMDHIS)) {
	alert("유효한 날짜를 입력해주세요!", $_SERVER['HTTP_REFERER']);
	die;
} // end if

$dbtable = "event_couponment";

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

//echo strtoupper(random_string("alnum", 12));
/* 쿠폰 등록 */
$coupon_sql = "INSERT INTO event_couponment(";
foreach($para_list as $para_key => $para_val){
	if($para_key == count($para_list)) {
		$coupon_sql .= $para_val.") VALUES ('";
		continue;
	} // end if
	
	$coupon_sql .= $para_val.", ";
} // end foreach

foreach($para_list as $para_key => $para_val){
	if($para_key == count($para_list)) {
		$coupon_sql .= ${'_'.$para_val}."')";
		continue;
	} // end if
	
	$coupon_sql .= ${'_'.$para_val}."', '";
} // end foreach

$coupon_result = sql_query($coupon_sql);
if($coupon_result) {
	/* 쿠폰 발급 */
	$row = sql_fetch("select * from event_couponment where ecm_start_date = '$_ecm_start_date' and ecm_end_date = '$_ecm_end_date' and ecm_ticket = '$_ecm_ticket'");
	$date = sprintf("%03d", date("z")); // 365일 날짜
	$year = substr(NM_TIME_Y, 3, 1); // 년도 마지막 숫자
	$head = $year.$date; // 년도 마지막 숫자 + 365일 날짜 (쿠폰 머리)
	$issue_sql = "INSERT INTO event_coupon(ec_ecm_no, ec_days, ec_code, ec_code_idx, ec_reg_date) VALUES";
	for($i=1; $i<=$_ecm_lot; $i++) {
		$body = strtoupper(random_string("alnum", 12)); // 쿠폰 몸통
		$body_idx = substr($body, 0, 1); // 쿠폰 5번째 자리 idx
		$code = $head.$body; // 쿠폰 코드
		$issue_sql .= "('".$row['ecm_no']."', '".$date."', '".$code."', '".$body_idx."', '".$row['ecm_start_date']."'";

		if($i < $_ecm_lot) {
			$issue_sql .= "), ";
		} else {
			$issue_sql .= ")";
		} // end else
	} // end for

	$issue_result = sql_query($issue_sql);
	if($issue_result) {
		$db_result['msg'] = $_ecm_ticket."의 쿠폰이 ".$_ecm_lot."장 발급되었습니다.";
	} else {
		$db_result['state'] = 1;
		$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $issue_sql;
	} // end else
} else {
	$db_result['state'] = 1;
	$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
	$db_result['error'] = $coupon_sql;
}

pop_close($db_result['msg'], '', $db_result['error']);

?>