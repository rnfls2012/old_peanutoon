<?
include_once '_common.php'; // 공통



/* 투표구분 가져오기 */
$evc_no_arrs = array();
$result_evc_no = sql_query("SELECT * FROM event_vote_category order by evc_no");
while ($row_evc_no = sql_fetch_array($result_evc_no)) {
	$evc_no_arrs[$row_evc_no['evc_no']] = $row_evc_no['evc_name'];
}

/* 상단에 각 구분 투표 작품 */
$sev_vote_cnt_sum = $ev_evc_no = 0;
$ev_evc_no_arrs = array();
$sql_ev_evc_no = "select *, sum(sev_vote_cnt)as sev_vote_cnt_sum FROM stats_event_vote sev INNER JOIN event_vote ev ON sev.sev_ev_no = ev.ev_no group by sev_ev_no ORDER BY ev_evc_no, sev_vote_cnt_sum DESC ";
$result_ev_evc_no = sql_query($sql_ev_evc_no );
while ($row_ev_evc_no = sql_fetch_array($result_ev_evc_no)) {
	$temp_comics = get_comics($row_ev_evc_no['sev_comics']); /* 변경 */
	$row_ev_evc_no['sev_series'] = $temp_comics['cm_series']; // 코믹스 이름

	/* 높은 숫자 올리기 - 버블 오름차순 알고리즘 변형 응용 */
	if($ev_evc_no < $row_ev_evc_no['ev_evc_no']){
		$ev_evc_no = $row_ev_evc_no['ev_evc_no'];
		$sev_vote_cnt_sum = $row_ev_evc_no['sev_vote_cnt_sum'];
		$ev_evc_no_arrs[$ev_evc_no] = $row_ev_evc_no;
	}else{
		$temp_sev_vote_cnt_sum = $sev_vote_cnt_sum;
		if($temp_sev_vote_cnt_sum < $row_ev_evc_no['sev_vote_cnt_sum']){
			$ev_evc_no = $row_ev_evc_no['ev_evc_no'];
			$sev_vote_cnt_sum = $row_ev_evc_no['sev_vote_cnt_sum'];
			$ev_evc_no_arrs[$ev_evc_no] = $row_ev_evc_no;
		}
	}
}

// 이벤트 번호 -> 필수
$sql_where.=" AND sev.sev_ev_no ='".$_event."' ";

// 날짜
if($_s_date && $_e_date){ 
	$sql_where.= date_year_month($_s_date, $_e_date, 'sev.sl'); 
}

// 카테고리
if($_evc_no){ 
	$sql_where.= " AND ev.ev_evc_no ='".$_evc_no."' ";
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sev_vote_cnt"){ 
	$_order_field = ""; 
}
if($_order == null || $_order == ""){ 
	$_order = ""; 
}

$sql_order = "order by sev_year_month DESC, sev_day DESC ".$_order_field." ".$_order;

// 그룹
$sql_group = " group by sev_year_month, sev_day, sev.sev_ev_no ";

// SQL문
$sql_field = "		sev.sev_comics, sev.sev_ev_no, ev.ev_evc_no, 
					sev_year_month, sev_day, 
					
					sum(sev_vote_cnt)as sev_vote_cnt_sum, 
					sum(sev_view_cnt)as sev_view_cnt_sum, 
					sum(sev_comicsview_cnt)as sev_comicsview_cnt_sum, 
					sum(sev_pay_cnt)as sev_pay_cnt_sum, 

					sum(sev_cash_point)as sev_cash_point_sum, 
					sum(sev_point)as sev_point_sum, 
					sum(sev_cash_point_cnt)as sev_cash_point_cnt_sum, 
					sum(sev_point_cnt)as sev_point_cnt_sum  
"; 
$sql_count	= $sql_field. " , count( distinct sev.sev_ev_no ) as cnt ";

$sql_select = "		select {$sql_field} ";
$sql_select_cnt = "	select {$sql_count} ";
$sql_table = "		FROM stats_event_vote sev
					INNER JOIN event_vote ev ON sev.sev_ev_no = ev.ev_no ";

$sql_query		= " {$sql_select}		{$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_select_cnt}	{$sql_table} {$sql_where} ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts		= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data_cnts = sql_fetch($sql_query_cnt);	// SQL문 총값-레코드값

// 결과값-상단표기
$title_sev_comics_txt = get_comics($rows_data_cnts['sev_comics'])['cm_series']."(".$rows_data_cnts['sev_comics'].")";
$title_ev_evc_txt = $evc_no_arrs[$rows_data_cnts['ev_evc_no']];


if($_mode == 'excel') {
	$rows_data	= rows_data($sql_querys, false);		// SQL문 결과 레코드값
	include_once $_cms_folder_top_path.'/excel/comics_excel.php';
}else{ // end if
	$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값
}


/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('날짜','ev_evc_no',0));
array_push($fetch_row, array('투표하기(건수)','ev_vote_cnt',0));
array_push($fetch_row, array('보러가기(건수)','sev_view_cnt',0));
array_push($fetch_row, array('작품화본수(건수)','sev_comicsview_cnt',0));
array_push($fetch_row, array('작품결제(건수)','sev_pay_cnt',0));
array_push($fetch_row, array('작품결제['.$nm_config['cf_cash_point_unit_ko'].']','sev_cash_point',0));
array_push($fetch_row, array('작품결제['.$nm_config['cf_point_unit_ko'].']','sev_point',0));

// array_push($fetch_row, array('코믹스 등록일<br/>화 최신등록일','cm_reg_date',0));

// css 클래스 접두사
$css_suffix = "cm_";

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "투표 이벤트-".$title_ev_evc_txt."-".$title_sev_comics_txt;
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<script type="text/javascript">
<!--
	var cr_thead_mg_add = '38px'; /* fixed 테이블이 높이가 커서 그만큼 마진 */
//-->
</script>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div class="write">
		<!-- 복사해갖고 온거라 해당 엑셀파일 수정해야함 <input type="button" class="excel_down" value="검색 결과 엑셀 다운로드" onclick="location.href='<?=$_cms_self;?>?<?=$_nm_paras;?>&mode=excel'"/> -->
	</div>
</section><!-- cms_title -->

<section id="event_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<table>
			<caption style="text-align:left; padding-left:10px;">검색 결과</caption>
			<tr>
				<th class="topleft">총 투표하기(건수)</th>
				<th>보러가기(건수)</th>
				<th>작품화본수(건수)</th>
				<th>작품결제(건수)</th>
				<th>작품<?=$nm_config['cf_cash_point_unit_ko'];?>결제</th>
				<th class="topright">작품<?=$nm_config['cf_point_unit_ko'];?>결제</th>
			</tr>
			<tr>
				<td><?=$rows_data_cnts['sev_vote_cnt_sum'];?> 건</td>
				<td><?=$rows_data_cnts['sev_view_cnt_sum'];?> 건</td>
				<td><?=$rows_data_cnts['sev_comicsview_cnt_sum'];?> 건</td>
				<td><?=$rows_data_cnts['sev_pay_cnt_sum'];?> 건</td>
				<td><?=$rows_data_cnts['sev_cash_point_sum'];?> <?=$nm_config['cf_cash_point_unit'];?> </td>
				<td><?=$rows_data_cnts['sev_point_sum'];?> <?=$nm_config['cf_point_unit'];?> </td>
		</table>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $css_suffix.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){
					$db_sev_comics = get_comics($dvalue_val['sev_comics']); /* 변경 */

					$db_comics_date_url = $_cms_self.'?'.$_nm_paras.'&page=1&mode=date&event='.$dvalue_val['sev_ev_no'];

				?>
				<tr>
					<td class="<?=$css_suffix.$fetch_row[0][1]?> text_center">
						<?=$dvalue_val['sev_year_month']."-".$dvalue_val['sev_day']?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[1][1]?> text_center">
						<?=number_format($dvalue_val['sev_vote_cnt_sum']);?> 건
					</td>	
					<td class="<?=$css_suffix.$fetch_row[2][1]?> text_center">
						<?=number_format($dvalue_val['sev_view_cnt_sum']);?> 건
					</td>	
					<td class="<?=$css_suffix.$fetch_row[3][1]?> text_center">
						<?=number_format($dvalue_val['sev_comicsview_cnt_sum']);?> 건
					</td>	
					<td class="<?=$css_suffix.$fetch_row[4][1]?> text_center">
						<?=number_format($dvalue_val['sev_pay_cnt_sum']);?> 건
					</td>	
					
					<td class="<?=$css_suffix.$fetch_row[5][1]?> text_center">
						<?=number_format($dvalue_val['sev_cash_point_sum'])." ".$nm_config['cf_cash_point_unit'];?>
					</td>	
					<td class="<?=$css_suffix.$fetch_row[6][1]?> text_center">
						<?=number_format($dvalue_val['sev_point_sum'])." ".$nm_config['cf_point_unit'];?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? rows_page(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>