<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_ev_no = tag_filter($_REQUEST['ev_no']);

$sql = "SELECT * FROM event_vote WHERE ev_no = '$_ev_no' ";
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
			 $sql_list = "";
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);	
	/* 작품정보 가져오기 */
	$ev_comics = get_comics($row['ev_comics']);
}

/* 투표구분 가져오기 */
$evc_no_arrs = array();
$result_evc_no = sql_query("SELECT * FROM event_vote_category order by evc_no");
while ($row_evc_no = sql_fetch_array($result_evc_no)) {
	$evc_no_arrs[$row_evc_no['evc_no']] = $row_evc_no['evc_name'];
}

/* 대분류로 제목 */
$page_title = "작품투표";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<script type="text/javascript" src="<?=NM_URL."/js/jscolor.js";?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- event_vote_head -->

<section id="event_write">
	<form name="event_vote_write_form" id="event_vote_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return event_vote_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="ev_no" name="ev_no" value="<?=$_ev_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->

		<table>
			<tbody>
				<tr>
					<th><label for="ev_comics"><?=$required_arr[0];?>작품번호&정보<br/>작품번호 모르시면 <br/>작품검색 이용해주세요.</label></th>
					<td>
						<input type="text" placeholder="작품번호 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="ev_comics" id="ev_comics" value="<?=$row['ev_comics'];?>" autocomplete="off" />
						<div id="comics_vote"><?=$ev_comics['cm_series']?></div>
					</td>
				</tr>
				<tr>
					<th><label for="comics_search">작품 검색</label></th>
					<td>
						<!-- 이벤트 선택 리스트 -->
						<input type="text" class="comics_search" placeholder="검색하세요" id="comics_search" value="" autocomplete="off" onkeydown='comics_search_vote_chk();' />
						<a id="comics_search_vote_btn" class="comics_search_btn" href='#comics_search' onclick='a_click_false()'>검색</a>
						<div id="comics_search_vote_result"></div>
						<!-- 이벤트 검색 리스트 -->
					</td>
				</tr>
				<tr>
					<th><label for="ep_text"><?=$required_arr[0];?>투표구분</label></th>
					<td><? tag_selects($evc_no_arrs, "ev_evc_no", $row['ev_evc_no'], 'n'); ?></td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="state">작품투표연도<br/>등록시 자동저장입니다.</label></th>
					<td><?=$row['ev_year'];?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for=""><?=$required_arr[0];?>PC 이미지</label></th>
					<td>
						<? if($row['ev_cover_pc']!=''){ $ev_cover_pc_src = img_url_para($row['ev_cover_pc'], $row['ev_reg_date'], $row['ev_mod_date']); } ?>
						<input type="file" name="ev_cover_pc" id="ev_cover_pc" data-value="<?=$ev_cover_pc_src?>" />
						<p class="ev_cover_pc_explan">이미지 업로드 정보 : width:1000px, height:417px, 이미지파일 확장자:jpg, gif, png</p>
						<?if($row['ev_cover_pc']!=''){?>
						<div class="ev_cover_pc">
							<p><a href="#ev_cover_pc" onclick="a_click_false();">PC 이미지보기</a></p>
							<p class="ev_cover_pc_hide"><img src="<?=$ev_cover_pc_src?>" alt="<?=$row['ev_comics']."PC 이미지"?>" /></p>
						</div>
						<?}?>
					</td>
				</tr>
				<tr>
					<th><label for=""><?=$required_arr[0];?>모바일 이미지</label></th>
					<td>
						<? if($row['ev_cover_mo']!=''){ $ev_cover_mo_src = img_url_para($row['ev_cover_mo'], $row['ev_reg_date'], $row['ev_mod_date']); } ?>
						<input type="file" name="ev_cover_mo" id="ev_cover_mo" data-value="<?=$ev_cover_mo_src?>" />
						<p class="ev_cover_mo_explan">이미지 업로드 정보 : width:1000px, height:417px, 이미지파일 확장자:jpg, gif, png</p>
						<?if($row['ev_cover_mo']!=''){?>
						<div class="ev_cover_mo">
							<p><a href="#ev_cover_mo" onclick="a_click_false();">모바일 이미지보기</a></p>
							<p class="ev_cover_mo_hide"><img src="<?=$ev_cover_mo_src?>" alt="<?=$row['ev_comics']."모바일 이미지"?>" /></p>
						</div>
						<?}?>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ev_reg_date">이벤트 등록일</label></th>
					<td><?=$row['ev_reg_date'];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ev_mod_date">이벤트 수정일</label></th>
					<td><?=$row['ev_mod_date'];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="ev_vote_cnt">이벤트 투표수</label></th>
					<td><?=$row['ev_vote_cnt'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- event_vote_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>