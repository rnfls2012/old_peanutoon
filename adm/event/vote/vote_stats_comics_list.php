<?
include_once '_common.php'; // 공통



/* 투표구분 가져오기 */
$evc_no_arrs = array();
$result_evc_no = sql_query("SELECT * FROM event_vote_category order by evc_no");
while ($row_evc_no = sql_fetch_array($result_evc_no)) {
	$evc_no_arrs[$row_evc_no['evc_no']] = $row_evc_no['evc_name'];
}

/* 상단에 각 구분 투표 작품 */
$sev_vote_cnt_sum = $ev_evc_no = 0;
$ev_evc_no_arrs = array();
$sql_ev_evc_no = "select *, sum(sev_vote_cnt)as sev_vote_cnt_sum FROM stats_event_vote sev INNER JOIN event_vote ev ON sev.sev_ev_no = ev.ev_no group by sev_ev_no ORDER BY ev_evc_no, sev_vote_cnt_sum DESC ";
$result_ev_evc_no = sql_query($sql_ev_evc_no );
while ($row_ev_evc_no = sql_fetch_array($result_ev_evc_no)) {
	$temp_comics = get_comics($row_ev_evc_no['sev_comics']); /* 변경 */
	$row_ev_evc_no['sev_series'] = $temp_comics['cm_series']; // 코믹스 이름

	/* 높은 숫자 올리기 - 버블 오름차순 알고리즘 변형 응용 */
	if($ev_evc_no < $row_ev_evc_no['ev_evc_no']){
		$ev_evc_no = $row_ev_evc_no['ev_evc_no'];
		$sev_vote_cnt_sum = $row_ev_evc_no['sev_vote_cnt_sum'];
		$ev_evc_no_arrs[$ev_evc_no] = $row_ev_evc_no;
	}else{
		$temp_sev_vote_cnt_sum = $sev_vote_cnt_sum;
		if($temp_sev_vote_cnt_sum < $row_ev_evc_no['sev_vote_cnt_sum']){
			$ev_evc_no = $row_ev_evc_no['ev_evc_no'];
			$sev_vote_cnt_sum = $row_ev_evc_no['sev_vote_cnt_sum'];
			$ev_evc_no_arrs[$ev_evc_no] = $row_ev_evc_no;
		}
	}
}

$sql_where="";
// 날짜
if($_s_date && $_e_date){ 
	$sql_where.= date_year_month($_s_date, $_e_date, 'sev.sl'); 
}

// 카테고리
if($_evc_no){ 
	$sql_where.= " AND ev.ev_evc_no ='".$_evc_no."' ";
}

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sev_vote_cnt"){ 
	$_order_field = "sev_vote_cnt_sum"; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
}

$sql_order = "order by ev.ev_evc_no asc,  ".$_order_field." ".$_order;

// 그룹
$sql_group = " group by sev.sev_ev_no ";

// SQL문
$sql_field = "		sev.sev_comics, sev.sev_ev_no, ev.ev_evc_no, 
					sum(sev_vote_cnt)as sev_vote_cnt_sum, 
					sum(sev_view_cnt)as sev_view_cnt_sum, 
					sum(sev_comicsview_cnt)as sev_comicsview_cnt_sum, 
					sum(sev_pay_cnt)as sev_pay_cnt_sum, 

					sum(sev_cash_point)as sev_cash_point_sum, 
					sum(sev_point)as sev_point_sum, 
					sum(sev_cash_point_cnt)as sev_cash_point_cnt_sum, 
					sum(sev_point_cnt)as sev_point_cnt_sum  
"; 
$sql_count	= $sql_field. " , count( distinct sev.sev_ev_no ) as cnt ";

$sql_select = "		select {$sql_field} ";
$sql_select_cnt = "	select {$sql_count} ";
$sql_table = "		FROM stats_event_vote sev
					INNER JOIN event_vote ev ON sev.sev_ev_no = ev.ev_no ";

$sql_query		= " {$sql_select}		{$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_select_cnt}	{$sql_table} {$sql_where} ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts		= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data_cnts = sql_fetch($sql_query_cnt);	// SQL문 총값-레코드값

if($_mode == 'excel') {
	$rows_data	= rows_data($sql_querys, false);		// SQL문 결과 레코드값
	include_once $_cms_folder_top_path.'/excel/comics_excel.php';
}else{ // end if
	$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값
}


/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('구분','ev_evc_no',0));
array_push($fetch_row, array('코믹스명','ev_comics',0));
array_push($fetch_row, array('투표하기(건수)','ev_vote_cnt',0));
array_push($fetch_row, array('보러가기(건수)','sev_view_cnt',0));
array_push($fetch_row, array('작품화본수(건수)','sev_comicsview_cnt',0));
array_push($fetch_row, array('작품결제(건수)','sev_pay_cnt',0));
array_push($fetch_row, array('작품결제['.$nm_config['cf_cash_point_unit_ko'].']','sev_cash_point',0));
array_push($fetch_row, array('작품결제['.$nm_config['cf_point_unit_ko'].']','sev_point',0));

// array_push($fetch_row, array('코믹스 등록일<br/>화 최신등록일','cm_reg_date',0));

// css 클래스 접두사
$css_suffix = "cm_";

// 날짜 보이게...
$cs_calendar_on = 'cs_calendar_on';

$page_title = "코믹스별 매출";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<script type="text/javascript">
<!--
	var cr_thead_mg_add = '38px'; /* fixed 테이블이 높이가 커서 그만큼 마진 */
//-->
</script>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div class="write">
		<!-- 복사해갖고 온거라 해당 엑셀파일 수정해야함 <input type="button" class="excel_down" value="검색 결과 엑셀 다운로드" onclick="location.href='<?=$_cms_self;?>?<?=$_nm_paras;?>&mode=excel'"/> -->
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="recharge_search_form" id="recharge_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return recharge_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<?	tag_selects($evc_no_arrs, "evc_no", $_evc_no, 'y', '구분 전체'); ?>
			</div>
			<div class="cs_submit day_buy_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="86" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- recharge_search -->

<section id="event_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		
		<table>
			<caption style="text-align:left; padding-left:10px;">검색 결과</caption>
			<tr>
				<th class="topleft">총 투표하기(건수)</th>
				<th>보러가기(건수)</th>
				<th>작품화본수(건수)</th>
				<th>작품결제(건수)</th>
				<th>작품<?=$nm_config['cf_cash_point_unit_ko'];?>결제</th>
				<th class="topright">작품<?=$nm_config['cf_point_unit_ko'];?>결제</th>
			</tr>
			<tr>
				<td><?=$rows_data_cnts['sev_vote_cnt_sum'];?> 건</td>
				<td><?=$rows_data_cnts['sev_view_cnt_sum'];?> 건</td>
				<td><?=$rows_data_cnts['sev_comicsview_cnt_sum'];?> 건</td>
				<td><?=$rows_data_cnts['sev_pay_cnt_sum'];?> 건</td>
				<td><?=$rows_data_cnts['sev_cash_point_sum'];?> <?=$nm_config['cf_cash_point_unit'];?> </td>
				<td><?=$rows_data_cnts['sev_point_sum'];?> <?=$nm_config['cf_point_unit'];?> </td>
		</table>
		<table>
			<caption style="text-align:left; padding-left:10px;"><?=NM_VOTE_YEAR;?>연도 부분별 최다 득표 코믹스</caption>
			<tr>
				<th class="topleft">구분</th>
				<th>완결작</th>
				<th>연재작</th>
				<th>기대작</th>
				<th class="topright">단행본</th>
			</tr>
			<tr>
				<td>작품명</td>
			<? foreach($ev_evc_no_arrs as $ev_evc_no_val){ ?>
				<td><?=$ev_evc_no_val['sev_series'];?></td>
			<? } ?>
			</tr>
		</table>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){
					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?'.$_nm_paras.'&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $css_suffix.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){
					$db_sev_comics = get_comics($dvalue_val['sev_comics']); /* 변경 */

					$db_comics_date_url = $_cms_self.'?'.$_nm_paras.'&page=1&mode=date&event='.$dvalue_val['sev_ev_no'];

				?>
				<tr>
					<td class="<?=$css_suffix.$fetch_row[0][1]?> text_center">
						<?=$evc_no_arrs[$dvalue_val['ev_evc_no']]?>
					</td>
					<td class="<?=$css_suffix.$fetch_row[1][1]?> text_center">
						<a href="<?=$db_comics_date_url?>"><?=$db_sev_comics['cm_series'];?>(<?=$dvalue_val['sev_comics'];?>)</a>
					</td>
					<td class="<?=$css_suffix.$fetch_row[2][1]?> text_center">
						<?=number_format($dvalue_val['sev_vote_cnt_sum']);?> 건
					</td>	
					<td class="<?=$css_suffix.$fetch_row[3][1]?> text_center">
						<?=number_format($dvalue_val['sev_view_cnt_sum']);?> 건
					</td>	
					<td class="<?=$css_suffix.$fetch_row[4][1]?> text_center">
						<?=number_format($dvalue_val['sev_comicsview_cnt_sum']);?> 건
					</td>	
					<td class="<?=$css_suffix.$fetch_row[5][1]?> text_center">
						<?=number_format($dvalue_val['sev_pay_cnt_sum']);?> 건
					</td>	
					
					<td class="<?=$css_suffix.$fetch_row[6][1]?> text_center">
						<?=number_format($dvalue_val['sev_cash_point_sum'])." ".$nm_config['cf_cash_point_unit'];?>
					</td>	
					<td class="<?=$css_suffix.$fetch_row[7][1]?> text_center">
						<?=number_format($dvalue_val['sev_point_sum'])." ".$nm_config['cf_point_unit'];?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- recharge_result -->

<? rows_page(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>