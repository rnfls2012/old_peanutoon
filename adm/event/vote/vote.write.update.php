<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode','ev_no','ev_comics','ev_evc_no','ev_year','ev_reg_date','ev_mod_date');

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

$dbtable = "event_vote";
$dbt_primary = "ev_no";
$para_primary = "ev_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

if($_mode == 'reg'){
	/* 고정값 */
	$_ev_reg_date = substr(NM_TIME_YMDHIS, 0, 16);	/* 최초등록일 */
	$_ev_year = substr($_ev_reg_date, 0, 4);		/* 최초등록연도 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);

	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = '데이터가 등록되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

/* 수정 */
}else if($_mode == 'mod'){
	$_ev_mod_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$dbt_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$dbt_primary.'값이 없습니다.';
	}else{
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= ' WHERE '.$dbt_primary.'="'.${'_'.$dbt_primary}.'"';
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = ${'_'.$dbt_primary}.'의 데이터가 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$dbt_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$dbt_primary.'값이 없습니다.';
	}else{
		/* 데이터 삭제 */
		$sql_del = 'delete from '.$dbtable.' where '.$dbt_primary.'="'.${'_'.$dbt_primary}.'"';
		sql_query($sql_del);		
		$db_result['msg'] = ${'_'.$dbt_primary}.'의 데이터가 삭제되였습니다.';
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/* 이미지 처리 */
if($_mode != 'del' && $db_result['state'] == 0){
	/* 등록이라면~ [ eventnum 이 NULL 이라면 ] */
	if($_ev_no == ''){
		$sql_max = 'select COALESCE(max('.$dbt_primary.'),0) as '.$dbt_primary.'_new from '.$dbtable; /* 방금 등록번호 가져오기 */
		$row_max = sql_fetch($sql_max);
		$_ev_no = $row_max[$dbt_primary.'_new'];
	}

	/* 이미지 경로 */

	/* 대분류 경로설정 - 폴더 체크 및 생성 */
	$path_ev_cover_pc = '_pc/vote/';
	$path_ev_cover_mo = '_mobile/vote/';
	
	/* 업로드한 이미지 명 */
	$ev_cover_pc_tmp_name = $_FILES['ev_cover_pc']['tmp_name'];
	$ev_cover_pc_name = trim($_FILES['ev_cover_pc']['name']);

	$ev_cover_mo_tmp_name = $_FILES['ev_cover_mo']['tmp_name'];
	$ev_cover_mo_name = trim($_FILES['ev_cover_mo']['name']);	

	/* 확장자 얻기 .png .jpg .gif */
	$ev_cover_pc_extension = substr($ev_cover_pc_name, strrpos($ev_cover_pc_name, "."), strlen($ev_cover_pc_name));
	$ev_cover_mo_extension = substr($ev_cover_mo_name, strrpos($ev_cover_mo_name, "."), strlen($ev_cover_mo_name));	

	/* 고정된 파일 명 */
	$ev_cover_pc_name_define = 'ev_cover_pc_'.$_ev_no.strtolower($ev_cover_pc_extension);
	$ev_cover_mo_name_define = 'ev_cover_mo_'.$_ev_no.strtolower($ev_cover_mo_extension);

	/* 저장 경로 및 DB 파일명 및 파일경로 저장 */
	$ev_cover_pc_src = $path_ev_cover_pc.$ev_cover_pc_name_define;
	$ev_cover_mo_src = $path_ev_cover_mo.$ev_cover_mo_name_define;
	
	
	/* 파일 업로드 */ 
	$ev_cover_pc_result = false;
	if ($ev_cover_pc_name != '') {  
		$ev_cover_pc_result = kt_storage_upload($ev_cover_pc_tmp_name, $path_ev_cover_pc, $ev_cover_pc_name_define); 
	}

	$ev_cover_mo_result = false;
	if ($ev_cover_mo_name != '') {  
		$ev_cover_mo_result = kt_storage_upload($ev_cover_mo_tmp_name, $path_ev_cover_mo, $ev_cover_mo_name_define); 
	}

	/* 임시파일이 존재하는 경우 삭제 */
	if (file_exists($ev_cover_pc_tmp_name) && is_file($ev_cover_pc_tmp_name)) {
		unlink($ev_cover_pc_tmp_name);
	}

	if (file_exists($ev_cover_mo_tmp_name) && is_file($ev_cover_mo_tmp_name)) {
		unlink($ev_cover_mo_tmp_name);
	}
	
	
	$sql_image = ' UPDATE '.$dbtable.' SET ';
	/* 이미지 field값 추가 */
	if ($ev_cover_pc_name != '' && $ev_cover_pc_result == true){ $sql_image.= ' ev_cover_pc = "'.$ev_cover_pc_src.'" '; }
	if (($ev_cover_pc_name != '' && $ev_cover_pc_result == true) && ($ev_cover_mo_name != '' && $ev_cover_mo_result == true)){
		$sql_image.= ', ';
	}
	if ($ev_cover_mo_name != '' && $ev_cover_mo_result == true){ $sql_image.= ' ev_cover_mo = "'.$ev_cover_mo_src.'" '; }
	$sql_image.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
	
	if (($ev_cover_pc_name != '' && $ev_cover_pc_result == true) || ($ev_cover_mo_name != '' && $ev_cover_mo_result == true)){
		/* DB 저장 */
		if(sql_query($sql_image)){
			$db_result['msg'].= '\n'.${'_'.$dbt_primary}.'의 이미지가 저장되었습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'].= '\n'.'이미지 에러가 발생하여 저장되지 않았습니다.';
			$db_result['error'].= $sql_image;
		}
	}


}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
pop_close($db_result['msg'], '', $db_result['error']);
die;

?>