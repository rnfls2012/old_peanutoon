<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_evc_no = tag_filter($_REQUEST['evc_no']);

$sql = "SELECT * FROM event_vote_category WHERE evc_no = '$_evc_no'";
				 
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
	break;
}
/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "투표 구분";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더

?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- partner_head -->
<? /* fileupload -> partner */ ?>
<section id="event_write">
	<form name="event_vote_category_write_form" id="event_vote_category_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return event_vote_category_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="evc_no" name="evc_no" value="<?=$_evc_no;?>"/><!-- 수정/삭제시 컨텐트번호 -->

		<table>
			<tbody>
				<tr>
					<th><label for="evc_name"><?=$required_arr[0];?>구분명</label></th>
					<td><input type="text" placeholder="구분명을 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="evc_name" id="evc_name" value="<?=$row['evc_name'];?>" autocomplete="off" /></td>
				</tr>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- partner_write -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>