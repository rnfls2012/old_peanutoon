<? include_once '_common.php'; // 공통
// /adm/_page.php 에서 사용하는 변수 보기

// 사용중인 경우 삭제 불가
$evc_no_arrs = array();
$result_evc_no = sql_query("SELECT * FROM event_vote WHERE 1 group by ev_evc_no order by ev_evc_no");
while ($row_evc_no = sql_fetch_array($result_evc_no)) {
	array_push($evc_no_arrs,  $row_evc_no['ev_evc_no']);
}


// 검색단어
if($_s_text){
	$sql_where.= "and evc_name like '%$_s_text%' ";
} // end if

// 그룹
$sql_group = "";

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "evc_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$sql_order = "order by ".$_order_field." ".$_order." " ;

// SQL문
$sql_field		= " * ";
$sql_count		= " count(*) as cnt ";

$sql_table		= "select {$sql_field} from event_vote_category";
$sql_table_cnt	= "select {$sql_count} from event_vote_category";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_table_cnt} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts	= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값

$th_class = $td_class = "";	// 상단테이블 css-class

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','evc_no',1));
array_push($fetch_row, array('구분명','evc_name',0));
array_push($fetch_row, array('구분링크','evc_like',0));
array_push($fetch_row, array('등록날짜','evc_reg_date',0));
array_push($fetch_row, array('수정날짜','evc_mod_date',0));
array_push($fetch_row, array('관리','evc_management',0));

$page_title = "투표 구분";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','vote_category_wirte', <?=$popup_cms_width;?>, 300);"><?=$page_title?> 등록</button>
	</div>
</section><!-- partner_head -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="partner_search_form" id="partner_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return partner_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">

					<?	$s_limit_text = "줄";
						for($sll=10; $sll<=$nm_config['s_limit']; $sll+=10){
							$s_limit_list[$sll] = $sll.$s_limit_text; ; 
						}
						tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>

					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit partner_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="130"  value="검색" id="partner_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- partner_search -->

<section id="event_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($rows_cnts);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $cp_css.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($rows_data as $dvalue_key => $dvalue_val){
					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?evc_no=".$dvalue_val['evc_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					/* 삭제버튼 */
					$del_btn_use = "ok";
					if(in_array($dvalue_val['evc_no'], $evc_no_arrs)){
						$del_btn_use = "";
					}
					
					$evc_link = NM_VOTE_URL."/year".NM_VOTE_YEAR.".php?menu=".$dvalue_val['evc_no'];

				?>
				<tr class="result_hover">
					<td class="<?=$cp_css.$fetch_row[0][1]?> text_center"><?=$dvalue_val['evc_no'];?></td>
					<td class="<?=$cp_css.$fetch_row[1][1]?> text_center"><?=$dvalue_val['evc_name'];?></td>
					<td class="<?=$cp_css.$fetch_row[2][1]?> text_center">
						<a href="<?=$evc_link;?>"><?=$evc_link;?></a>
					</td>
					<td class="<?=$cp_css.$fetch_row[3][1]?> text_center"><?=$dvalue_val['evc_reg_date'];?></td>
					<td class="<?=$cp_css.$fetch_row[4][1]?> text_center"><?=$dvalue_val['evc_mod_date'];?></td>
					<td class="<?=$cp_css.$fetch_row[5][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','vote_category_mod_wirte', <?=$popup_cms_width;?>, 200);">수정</button>
						<?if($del_btn_use == "ok"){?>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','vote_category_del_wirte', <?=$popup_cms_width;?>, 200);">삭제</button>
						<?}?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- event_result -->
<? rows_page(); ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>