<? include_once '../_common.php'; // DB 연결

/* PARAMITER CHECK */
$para_list = array();

/* 숫자 PARAMITER 체크 */
$para_num_list = array();

/* 체크박스배열 PARAMITER 체크 */
$checkbox_list = array();

/* 빈칸 PARAMITER 허용 */
$blank_list = array();

/* 빈칸 PARAMITER 시 NULL 처리 */
$null_list = array();

/* 빈칸&숫자 PARAMITER 허용 */
$null_int_list = array();

/* 숫자 합계 처리 PARAMITER */
$sum_list = array();

/* 이메일 PARAMETER 체크 */
$para_email_list = array(); 

/* 에디터 PARAMITER 체크 */
$editor_list = array(); 

/* 배너 커버 리스트 DB처리 */
$para_banner_cover_list = array(); 

/* DB field 아닌 목록 */
$db_field_exception = array();

function para_checked()
{
	global $para_list, $para_num_list, $checkbox_list, $blank_list, $null_list, $null_int_list, $sum_list, $para_email_list, $editor_list, $para_banner_cover_list, $db_field_exception;
	foreach($para_list as $para_key => $para_val){
		global ${'_'.$para_val};
		if(in_array($para_val,$para_num_list)){
			if(num_check($_POST[$para_val]) == false && $_POST[$para_val] != '' && $_POST[$para_val] != '0'){
				alert($para_val."값에 숫자가 아닌 값이 들어 있습니다.", $_SERVER['HTTP_REFERER']);
				die;
			}
		}
		if(is_array($_POST[$para_val])){
			if(in_array($para_val, $checkbox_list)){
				${'_'.$para_val} = implode("|",$_POST[$para_val]); // 배열처리
			}else if(in_array($para_val, $sum_list)){
				${'_'.$para_val} = array_sum($_POST[$para_val]); // 체크박스경우 숫자합산 변수담기
			}else{
				${'_'.$para_val} = $_POST[$para_val]; /* 변수담기 */
			}
		}else{
			if(in_array($para_val, $editor_list)){
				${'_'.$para_val} = trim($_POST[$para_val]); /* 변수담기 */
			}else if(in_array($para_val, $null_int_list) && $_POST[$para_val] == ''){
				${'_'.$para_val} = 0; /* 변수담기 */
			} else {
				${'_'.$para_val} = base_filter($_POST[$para_val]); /* 변수담기 */
			}
		}
	}
}

function para_sql_insert($dbtable)
{
	global $para_list, $para_num_list, $checkbox_list, $blank_list, $null_list, $null_int_list, $sum_list, $para_email_list, $editor_list, $para_banner_cover_list, $db_field_exception;
	foreach($para_list as $para_key => $para_val){ global ${'_'.$para_val};	}
	
	/* field명 */
	$sql_reg = 'INSERT INTO '.$dbtable.'(';
	foreach($para_list as $para_key => $para_val){
		if(in_array($para_val, $db_field_exception)){continue;} /* DB field 아닌 목록 제외 */
		if(${'_'.$para_val} == '' && in_array($para_val, $blank_list) == false){ continue; } /* DB field에 값 없는거 빼기 */

		/* sql문구 */
		$sql_reg.= $para_val.', ';
	}
	$sql_reg = substr($sql_reg,0,strrpos($sql_reg, ","));

	/* VALUES 시작 */
	$sql_reg.= ' )VALUES( ';

	/* field값 */
	foreach($para_list as $para_key => $para_val){
		if(in_array($para_val, $db_field_exception)){continue;} /* DB field 아닌 목록 제외 */
		if(${'_'.$para_val} == '' && in_array($para_val, $blank_list) == false){ continue; } /* DB field에 값 없는거 빼기 */

		/* sql문구 */
		if(in_array($para_val, $null_list) && ${'_'.$para_val} == ''){ /* DEFAULT NULL 경우에만 */
			$sql_reg.= 'NULL, ';
		}else{
			$sql_reg.= '"'.${'_'.$para_val}.'", ';
		}
	}
	$sql_reg = substr($sql_reg,0,strrpos($sql_reg, ","));

	/* SQL문 마무리 */
	$sql_reg.= ' ) ';

	return $sql_reg;
	
}


function para_sql_update($dbtable, $para_primary, $dbt_primary)
{
	global $para_list, $para_num_list, $checkbox_list, $blank_list, $null_list, $null_int_list, $sum_list, $para_email_list, $editor_list, $para_banner_cover_list, $db_field_exception;
	foreach($para_list as $para_key => $para_val){ global ${'_'.$para_val};	}

		$sql_mod = ' UPDATE '.$dbtable.' SET ';
		foreach($para_list as $para_key => $para_val){
			if(in_array($para_val, $db_field_exception)){continue;} /* DB field 아닌 목록 제외 */
			if(${'_'.$para_val} == '' && in_array($para_val, $blank_list) == false){ continue; } /* DB field에 값 없는거 빼기 */
			if($para_val == $para_primary || $para_val == $dbt_primary){ continue; } /* where문 빼기 */

			/* sql문구 */
			if(in_array($para_val, $null_list) && ${'_'.$para_val} == ''){ /* DEFAULT NULL 경우에만 */
				$sql_mod.= $para_val." = NULL, ";
			}else{
				$sql_mod.= $para_val." = '".${'_'.$para_val}."', ";
			}
		}

		$sql_mod = substr($sql_mod,0,strrpos($sql_mod, ","));
		
	return $sql_mod;
}

?>