<?
/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','z_tksk_no',0));
array_push($fetch_row, array('ICS번호[필수]','z_tksk_ics',0));
array_push($fetch_row, array('캠페인ID[필수]','z_tksk_campaign',0));
array_push($fetch_row, array('공유ICS페이지[피너툰페이지구분용]','z_tksk_page',0));
array_push($fetch_row, array('공유번호(orderId)[필수]','z_tksk_number',0));
array_push($fetch_row, array('공유email(email)[필수]','z_tksk_email',0));
array_push($fetch_row, array('공유가격(revenue)[필수]','z_tksk_revenue',0));
array_push($fetch_row, array('공유회원번호(name)[옵션]','z_tksk_name',0));
array_push($fetch_row, array('주문상품명(productName)[옵션]','z_tksk_productname',0));
array_push($fetch_row, array('주문상품URL(ProductUrl)[옵션-미사용]','z_tksk_producturl',0));
array_push($fetch_row, array('등록일','z_tksk_date',0));

$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');

/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
} // end foreach

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
} // end foreach

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write(0, $col++, $cell);
} // end foreach

/* 데이터 전달 부분 */
for ($i=1; $row=mysql_fetch_array($result); $i++){
	foreach($data_key as $cell_key => $cell_val) {
		if($cell_val == "z_tksk_number" || $cell_val == "z_tksk_name"){
			$worksheet->write($i, $cell_key , '`'.iconv_cp949($row[$cell_val]));
		}else{
			$worksheet->write($i, $cell_key , iconv_cp949($row[$cell_val]));
		}
	} // end foreach
} // end for 쿼리 조회결과가 없을때까지 조회

$workbook->close();

header("Content-Disposition: attachment;filename="."ticketsocket_ics_".NM_TIME_YMDHIS.".xls");
header("Content-Type: application/x-msexcel; name=\"ticketsocket_ics_".NM_TIME_YMDHIS.".xls");
header("Content-Disposition: inline; filename=\"ticketsocket_ics_".NM_TIME_YMDHIS.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;
?>