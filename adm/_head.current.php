<?php
include_once '_common.php';

/* 현재 접속한 정보 저장 */
$_cms_current_page = $_SERVER['PHP_SELF'];
$_cms_current_arr = explode("/", $_cms_current_page);
$_cms_current_file = $_cms_current_arr[count($_cms_current_arr)-1]; // 현재 접속한 파일명
$_cms_current_folder = $_cms_current_arr[count($_cms_current_arr)-2]; // 현재 접속한 폴더명
$_cms_current_topfolder = $_cms_current_arr[count($_cms_current_arr)-3]; // 현재 접속한 상위폴더명
$_cms_current_filename = str_replace(".php", "", $_cms_current_file); // 파일명만(확장자빼고)

/* 연결경로 */
$_cms_css = NM_URL."/css/cms_".$_cms_current_topfolder.".css";
$_cms_js = NM_URL."/js/cms_".$_cms_current_topfolder.".js";
$_cms_pop_css = NM_URL."/css/cms_pop_".$_cms_current_topfolder.".css";
$_cms_pop_js = NM_URL."/js/cms_pop_".$_cms_current_topfolder.".js";

/* 해당 폴더 및 파일 */
$_cms_folder = NM_ADM_URL."/".$_cms_current_topfolder."/".$_cms_current_folder; /* 폴더 */
$_cms_self = $_cms_folder."/".$_cms_current_file; /* 자기자신파일 */

$_cms_folder_path = NM_ADM_PATH."/".$_cms_current_topfolder."/".$_cms_current_folder; /* 폴더 */
$_cms_folder_top_path = NM_ADM_PATH."/".$_cms_current_topfolder; /* 폴더 */
$_cms_self_path = $_cms_folder_path."/".$_cms_current_file; /* 자기자신파일 */

/* 공통으로 쓸때 */
$_cms_folder_write = $_cms_folder."/".$_cms_current_folder.".write.php"; /* 등록/수정 */
$_cms_folder_update = $_cms_folder."/".$_cms_current_folder.".update.php"; /* 업데이트 */
$_cms_folder_delete = $_cms_folder."/".$_cms_current_folder.".delete.php"; /* 삭제 */

/* 개개인별로 쓸때 */
$_cms_write= $_cms_folder."/".$_cms_current_filename.".write.php";/* 등록/수정 */
$_cms_update = $_cms_folder."/".$_cms_current_filename.".update.php"; /* 업데이트 */
$_cms_delete = $_cms_folder."/".$_cms_current_filename.".delete.php"; /* 삭제 */

/* 2뎁스일때 */
if($_cms_current_topfolder == "adm"){
	$_cms_css = NM_URL."/css/cms_".$_cms_current_folder.".css";
	$_cms_js = NM_URL."/js/cms_".$_cms_current_folder.".js";
	$_cms_pop_css = NM_URL."/css/cms_pop_".$_cms_current_folder.".css";
	$_cms_pop_js = NM_URL."/js/cms_pop_".$_cms_current_folder.".js";
	
	/* 해당 폴더 및 파일 */
	$_cms_folder = NM_ADM_URL."/".$_cms_current_folder; /* 폴더 */
	$_cms_self = $_cms_folder."/".$_cms_current_file; /* 자기자신파일 */

	$_cms_folder_path = NM_ADM_PATH."/".$_cms_current_folder; /* 폴더 */
	$_cms_folder_top_path = NM_ADM_PATH; /* 폴더 */
	$_cms_self_path = $_cms_folder_path."/".$_cms_current_file; /* 자기자신파일 */

	/* 공통으로 쓸때 */
	$_cms_folder_write = $_cms_folder."/".$_cms_current_folder.".write.php"; /* 등록/수정 */
	$_cms_folder_update = $_cms_folder."/".$_cms_current_folder.".update.php"; /* 업데이트 */
	$_cms_folder_delete = $_cms_folder."/".$_cms_current_folder.".delete.php"; /* 삭제 */

	/* 개개인별로 쓸때 */
	$_cms_write = $_cms_folder."/".$_cms_current_filename.".write.php"; /* 등록/수정 */
	$_cms_update = $_cms_folder."/".$_cms_current_filename.".update.php"; /* 업데이트 */
	$_cms_delete = $_cms_folder."/".$_cms_current_filename.".delete.php"; /* 삭제 */
}

// 강제로 css, js 넣기
if($_cms_css_push != ''){ $_cms_css = $_cms_css_push; }
if($_cms_js_push != ''){ $_cms_js = $_cms_js_push; }

if($_cms_folder_path_push != ''){ 
	$_cms_folder_path = $_cms_folder_path_push; 
	$_cms_folder = $_cms_folder_url_push; /* 폴더 */
}
if($_cms_folder_top_path_push != ''){ 
	$_cms_folder_top_path = $_cms_folder_top_path_push;
}
if($_cms_self_path_push != ''){ $_cms_self_path = $_cms_self_path_push; }
if($_cms_update_url_push != ''){ $_cms_update = $_cms_update_url_push; } /* 업데이트 */

// echo $_cms_current_topfolder."<br/>";
// echo $_cms_current_folder."<br/>";
?>