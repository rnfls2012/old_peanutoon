<?php
include_once NM_ADM_PATH.'/_common.php'; // 공통


// 페이지 처리
if($_page_mode==null || $_page_mode==""){  $_page_mode = 'sql'; }
if($_page < 1 || $_page==null || $_page==""){ $_page=1; }
if($_s_limit < 1 || $_s_limit==null || $_s_limit==""){ $_s_limit = $nm_config['s_limit']; } /* 몇개 보여줄지 */
$show_list = $_s_limit;

$start_page = $_page*$show_list -$show_list;

if($_page_mode == 'array'){
	$row_size = count($point_arr); // 총 데이터의 수 (중요)
	$view_num = ceil($row_size/$_s_limit); // 총 페이지의 수 (중요)
}


/* 페이지가 마이너스 || 결과값이 $start_page 작을때 || 결과값이 0일때 */
if($start_page < 0 || $row_size < $start_page || $row_size == 0){
	$start_page=0;
}else{
	if($_page_mode == 'array'){
		$start_view = ($_page-1)*$_s_limit; // 보여줘야 할 데이터의 시작 번호
		$end_view = ($_page)*$_s_limit; // 보여줘야 할 데이터의 시작 번호
		if($end_view > $row_size){
			$end_view = $row_size;
		}
	}else{
		sql_data_seek($result, $start_page);
	}
}
$chek2 = $row_size - ( ($_page-1) * $show_list);
$view_num = ceil($row_size / $show_list);

if($view_num < $_page){ /* 페이지가 sql page보다 높을 경우 */
	$_page = $view_num;
}

$_page = $_REQUEST['page'];
$now_url = $_SERVER['PHP_SELF'];

$row_count = 0;
$row_data = array();
if($_page_mode == 'array'){

}else{
	while ($row = sql_fetch_array($result)) {
		$row_count++;
		array_push($row_data, $row);
		if($row_count == $show_list){ break; }
	}

}




/* 각 페이지의 sql의 총갯수, 보여줄 갯수, 페이지번호 */
$row_size;
$show_list;
$view_num;

$view_num_max = 10;

if($_page > $view_num_max){
	if($_page % $view_num_max == 0){
		$big_page = (floor($_page / $view_num_max) * $view_num_max +1) - $view_num_max;
	}else{
		$big_page = floor($_page / $view_num_max) * $view_num_max +1;
	}
}


if($big_page==null || $big_page=="" || $big_page < 1){ $big_page = 1; }

//url에서 페이지 빼기
$_nm_paras = str_replace("&page=".$_page, "", $_nm_paras);
$_nm_paras = str_replace("page=".$_page, "", $_nm_paras);

$_page_url = $now_url."?".$_nm_paras;
$_page_no_arr = array();
$_page_prev_on = "off";
$_page_next_on = "off";
$_page_prev_url = "";
$_page_next_url = "";

$_page_no_count = 0;
$_page_width  = $_page_width_last = 100;
if($view_num <= 1){ 
}else{
	if($big_page >= $view_num_max){ /* 이전 */
		$_page_prev_on = "on";
		$_page_prev_url = $_page_url."&page=".intval($big_page - $view_num_max);
	}
	
	/* 넘버링 */
	$end_i = 0;
	if($big_page + $view_num_max > $view_num){ $end_i = $view_num; }
	else{ $end_i = $big_page + ($view_num_max -1); }

	for($i=$big_page;$i<=$end_i;$i++){
		$pg_current = "";
		if($_page == $i){ $pg_current = "page_current"; }
		/* 넘버, 링크, 일치 */
		array_push($_page_no_arr, array($i, $_page_url."&page=".$i, $pg_current));
	}

	if($big_page + $view_num_max < $view_num){ /* 다음 */
		$_page_next_on = "on";
		$_page_next_url = $_page_url."&page=".intval($end_i+1);
	}


	/* page 숫자 갯수 width 값 구하기 */
	/* page */
	$_page_count = 0;
	$_page_count = count($_page_no_arr);
	if($_page_count > 0){
		$_page_width = intval(100 / $_page_count);
		$_page_width_last = $_page_width + (100 - intval($_page_count)  * intval(100 / intval($_page_count)));
		$head1_last = intval($_page_count)-1;
	}
}

function page_view(){
	global $_page_width, $_page_width_last, $view_num, $_page_prev_on, $_page_next_on, $_page_prev_url, $_page_no_arr, $_page_next_url;
	if($view_num <= 1){

	}else{
		?>
		<link rel="stylesheet" type="text/css" href="<?=NM_URL?>/css/cms_page.css<?=vs_para();?>"/>
		<style type="text/css">
			#page_no_list .pnl_page_left p{ background:#aaa url('<?=NM_IMG?>cms/list_pre.png<?=vs_para();?>') 15% 47% no-repeat; }
			#page_no_list .pnl_page_right p{ background:#aaa url('<?=NM_IMG?>cms/list_next.png<?=vs_para();?>') 85% 47% no-repeat; }
		</style>
		<style type="text/css">
			/* main_menu */
			#page_no_list .pnl_page_center ul li{width:<?=$_page_width;?>%;}
			#page_no_list .pnl_page_center ul li.last{width:<?=$_page_width_last;?>%;}
		</style>
		<div id="page_no_list">
			<?php
				if($view_num <= 1){ ?>
					<div class="pnl_page_left">
						<p class="<?=$_page_prev_on?>">이전</p>
					</div>
					<div class="pnl_page_center">
						<ul>
							<li>
								<strong>1</strong>
							</li>
						</ul>
					</div>
					<div class="pnl_page_right">
						<p class="<?=$_page_next_on?>">다음</p>
					</div>
				<?
				}else{?>
					<div class="pnl_page_left">
						<?echo ($_page_prev_url ? "<a href=".$_page_prev_url.">":""); ?>
							<p class="<?=$_page_prev_on?>">
								이전
							</p>
						<?echo ($_page_prev_url ? "</a>":""); ?>
					</div>
					
					<div class="pnl_page_center">
						<ul>
						<?foreach($_page_no_arr as $_page_key => $_page_val){
							$_page_last = "";
							if(($_page_key+1) == $_page_count){ $_page_last = "last"; }
							?>
								<li class="<?=$_page_last?>">
									<a href="<?=$_page_val[1];?>">
										<?echo ($_page_val[2] ? "<strong>":""); ?>
										<?=$_page_val[0];?>		
										<?echo ($_page_val[2] ? "</strong>":""); ?>		
									</a>
								</li>
							<?}?>
						</ul>
					</div>

					<div class="pnl_page_right">
						<?echo ($_page_next_url ? "<a href=".$_page_next_url.">":""); ?>
							<p class="<?=$_page_next_on?>">
								다음
							</p>
						<?echo ($_page_next_url ? "</a>":""); ?>
					</div>

			<?  } ?>
		</div>
	<?} /* if($view_num <= 1){ */?>
<?} /* page_view(){ */?>