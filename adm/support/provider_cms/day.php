<?
include_once '_common.php'; // 공통

$_cms_folder_push = '/salescalc';
$_cms_folder_top_push = '';
$_cms_self_push = '/salescalc/day.php';
$_cms_update_push = '';

// 파일 위치
$_cms_folder_path_push = NM_ADM_PATH.$_cms_folder_push;
$_cms_folder_top_path_push = NM_ADM_PATH.$_cms_folder_top_push;
$_cms_self_path_push = NM_ADM_PATH.$_cms_self_push;
$_cms_update_path_push = NM_ADM_PATH.$_cms_update_push;

// 파일 url
$_cms_folder_url_push = NM_ADM_URL.$_cms_folder_push;
$_cms_folder_top_url_push = NM_ADM_URL.$_cms_folder_top_push;
$_cms_self_url_push = NM_ADM_URL.$_cms_self_push;
$_cms_update_url_push = NM_ADM_URL.$_cms_update_push;

$_cms_css_push = NM_URL."/css/cms_salescalc.css";
$_cms_js_push = NM_URL."/js/cms_salescalc.js";

$sql_mb_cm_cp	= mb_cm_cp_sql($nm_member);		// 파트너 SQL
$_e_date		= mb_partner_e_date($nm_member, $_e_date);	// 종료일 현재 -1일 적용

include_once NM_ADM_PATH.'/_head.current.php'; // 현재 위치
include_once NM_ADM_PATH.$_cms_self_push; // 연재문의

?>