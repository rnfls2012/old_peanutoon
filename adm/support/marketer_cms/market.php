<? include_once '_common.php'; // 공통
// /adm/_page.php 에서 사용하는 변수 보기

/* 2018-03-16 **************************************************************
1.CMS-회원-마케터 복사
2.회원정보삭제
3.회원검색삭제
4.엑셀다운로드삭제
****************************************************************************/


/* PARAMETER
	member_level // 대분류(회원 레벨)
	s_type // 검색 타입 -> 1. 전체, 2. 아이디, 3. 이름, 4. 이메일
	s_text // 검색 단어
	order_field // 정렬 필드
	date_type // 날짜 타입
	s_date // 날짜-시작
	e_date // 날짜-끝
	s_limit // 보여줄 갯수
*/

// 업체 리스트
$array_mkt = $marketers_arr = array();
$sql_mkt = "select * from marketer  where mkt_state = 'y'";
$result_mkt = sql_query($sql_mkt);
while ($row_mkt = sql_fetch_array($result_mkt)) {
	$marketers_arr[$row_mkt['mkt_no']] = $row_mkt['mkt_title'];
	$array_mkt[$row_mkt['mkt_no']] = $row_mkt;
}

//마케터 검색(select box)
if($mb_mkt) {
	$sql_where .= "and mb_mkt='".$_mb_mkt."' ";
} else {
	$sql_where.= "and mb_mkt > 0 "; //선택 안했을 시.
}

//결제, 미결제
if($_mb_won){
	switch($_mb_won){
		case '1': $sql_where.= "and mb_won > 0 ";
		break;

		case '2': $sql_where.= "and mb_won = 0 ";
		break;
	}
}

// 검색단어+ 검색타입
if($_s_text){
	switch($_s_type){
		case '0': $sql_where.= "and mb_id like '%$_s_text%' ";
		break;

		case '1': $sql_where.= "and mb_email like '%$_s_text%'";
		break;

		case '2': $sql_where.= "and mb_no like '%$_s_text%' ";
		break;

		default: $sql_where.= "and ( mb_id like '%$_s_text%' or mb_name like '%$_s_text%' or mb_email like '%$_s_text%' or mb_no like '%$_s_text%' )";
		break;
	} 
}

// 최근 로그인/탈퇴일 날짜
$start_date = $end_date = "";
if($_date_type) { //all, upload_date, new_date

	$cs_calendar_on = 'cs_calendar_on';
	$cs_submit_h = 'cs_submit_on';

	if($_s_date && $_e_date) {
		$start_date = $_s_date." ".NM_TIME_HI_start;
		$end_date = $_e_date." ".NM_TIME_HI_end;
	} else if($_s_date) {
		$start_date = $_s_date." ".NM_TIME_HI_start;
		$end_date = NM_TIME_YMD." ".NM_TIME_HI_end;
	}
	switch($_date_type){
		case 'mb_login_date' : 
			$sql_where.= "and mb_login_date >= '$start_date' and mb_login_date <= '$end_date'";
			$_order_field = "mb_login_date";
		break;

		case 'mb_join_date' : 
			$sql_where.= "and mb_join_date >= '$start_date' and mb_join_date <= '$end_date'";
			$_order_field = "mb_join_date";
		break;

		case 'mb_out_date' : 
			$sql_where.= "and mb_out_date >= '$start_date' and mb_out_date <= '$end_date'";
			$_order_field = "mb_out_date";
		break;

		default: $sql_where.= "";
		break;
	}
} else {
	$_s_date = "";
	$_e_date = "";
}

// 그룹
$sql_group = "";

// 정렬
if($_order_field == null || $_order_field == ""){ 
	$_order_field = "mb_join_date"; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
}
$sql_order = "order by ".$_order_field." ".$_order;

// SQL문
$sql_field		= " * ";
$sql_count		= " count(*) as cnt ";

$sql_table		= "select {$sql_field} from member";
$sql_table_cnt	= "select {$sql_count} from member";

$sql_query		= " {$sql_table} {$sql_where} {$sql_group}  ";
$sql_query_cnt	= " {$sql_table_cnt} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} ";

// SQL처리
$rows_cnts	= rows_cnts($sql_query_cnt);	// SQL문 총값
$rows_data	= rows_data($sql_querys);		// SQL문 결과 레코드값

$th_class = $td_class = "";	// 상단테이블 css-class

/* 출력 필드 리스트 - array('표제목', '정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('회원번호', 'mb_no', 0));
array_push($fetch_row, array('가입날짜', 'mb_join_date', 0));
array_push($fetch_row, array('로그인날짜', 'mb_login_date', 0));
array_push($fetch_row, array('마케팅이름(번호)', 'mb_mkt', 0));
array_push($fetch_row, array('충전 누적금액', 'mb_won', 0));
array_push($fetch_row, array('충전 횟수', 'mb_won_count', 0));

$page_title = "마케터 회원정보";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;

//엑셀 파일 다운로드 시 (query_string 으로 값 넘김.)
if($_mode == 'excel') {
	$result = sql_query($sql_querys);
	include_once $_cms_folder_path.'/excel/member_mkt_excel.php';
} // end if

// css, js 참조
$_cms_css_push = NM_URL."/css/cms_members.css";
$_cms_js_push = NM_URL."/js/cms_members.js";

include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()


?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>&nbsp;</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="member_search_form" id="member_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return member_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_search_btn">
				<?	
					//마케터
					tag_selects($marketers_arr, "mb_mkt", $_mb_mkt, 'y');

					//마케터 결제
					tag_selects($d_mb_won, "mb_won", $_mb_won, 'y');

					//페이지 행 개수
					tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); 
				?>
				</div>
				<div class="cs_search_btn">
				<?				
					//날짜 검색
					tag_selects($d_mb_date_type, "date_type", $_date_type, 'y', '날짜 검색안함', '');					
				?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit member_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="72" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="member_result">
	<h3 id="cr_thead_mg_add">검색 리스트
		<strong>검색 결과 수 : <?=number_format($rows_cnts);?>명</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▲";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▼"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){ $th_title_giho = $order_giho; }
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				
				<? foreach($rows_data as $dvalue_key => $dvalue_val){ ?>				
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['mb_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['mb_join_date'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['mb_login_date'];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center">
						<?=$array_mkt[$dvalue_val['mb_mkt']]['mkt_title'];?>(<?=$dvalue_val['mb_mkt']?>)
					</td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=number_format($dvalue_val['mb_won']);?> 원</td>
					<td class="<?=$fetch_row[5][1]?> text_center"><?=$dvalue_val['mb_won_count'];?> 번</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section>
<? rows_page(); ?>
<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>
