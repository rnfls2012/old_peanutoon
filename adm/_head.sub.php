<?php
include_once '_common.php';
if($head_title == ""){ $head_title = $nm_config['cf_title']; } 
if($head_thumbnail == "") { $head_thumbnail = NM_IMG.$nm_config['cf_meta']; } ?>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8">
		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<title><?=$head_title?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="keywords" content="한국 일본 최신 웹툰, 단행본, 요일별, 장르별 프리미엄 만화 전문 플랫폼">
		<meta name="description" content="한국 일본 최신 웹툰, 단행본, 요일별, 장르별 프리미엄 만화 전문 플랫폼">
		
		<meta name="author" content="Nexcube Development Team">
		<meta property="Nexcube:Develop_Manager" content="Sunkyu Kim (roydest)" />
		<meta property="Nexcube:Develop_Assistants" content="Ikhee Lee" />
		<meta property="Nexcube:Design&Publishing" content="Sunkyu Kim" />

		<meta property="fb:app_id" content="<?=NM_FACEBOOK_CLIENT_ID?>" />
		<meta property="og:type" content="website" />
		<meta property="og:description" content="한국 일본 최신 웹툰, 단행본, 요일별, 장르별 프리미엄 만화 전문 플랫폼" />
		<meta property="og:url" content="<?=NM_URL?><?=$_SERVER['REQUEST_URI'];?>">
		<meta property="og:title" content="<?=$head_title?>">
		<meta property="og:image" content="<?=$head_thumbnail;?>">
		<meta property="og:image:width" content="400" />
		<meta property="og:image:height" content="210" />

		<meta name="twitter:url" content="<?=NM_URL?><?=$_SERVER['REQUEST_URI'];?>">
		<meta name="twitter:title" content="<?=$head_title?>">
		<meta name="twitter:description" content="한국 일본 최신 웹툰, 단행본, 요일별, 장르별 프리미엄 만화 전문 플랫폼">
		<meta name="twitter:image:src" content="<?=$head_thumbnail;?>">

		<? // https://developers.kakao.com/docs/cache 에서 이미지 URL 캐쉬삭제 가능 ?>
		<meta name="image" content="<?=$head_thumbnail;?>">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<?php /* 모바일 접속시 바로가기(즐겨찾기) 아이콘 및 썸네일 
				shortcut icon href에 _넣으면 안됩니다. 그래서 경로가 NM_URL 입니다. */
			if(stristr(HTTP_USER_AGENT, "iPhone") || stristr(HTTP_USER_AGENT, "iPod")){ ?>
				<meta name="apple-mobile-web-app-capable" content="yes">
				<meta name="apple-mobile-web-app-status-bar-style" content="black">			
				<link rel="apple-touch-icon" href="<?=NM_IMG?><?=$nm_config['cf_favicon'];?>">
		<?	}else if(stristr(HTTP_USER_AGENT, "iPad")){ ?>
				<meta name="apple-mobile-web-app-capable" content="yes">
				<meta name="apple-mobile-web-app-status-bar-style" content="black">
				<link rel="apple-touch-icon" sizes="72*72" href="<?=NM_IMG?><?=$nm_config['cf_favicon'];?>">
		<?	}else if(stristr(HTTP_USER_AGENT, "Android")){ ?>
				<link rel="shortcut icon" href="<?=NM_IMG?><?=$nm_config['cf_favicon'];?>">
		<?	}else{ ?>
				<!-- Favicon  -->
				<link rel="shortcut icon" href="<?=NM_IMG?><?=$nm_config['cf_favicon'];?>">
		<?	} ?>
		
		<!--[if lt IE 7]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<!--[if lt IE 9]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
		<![endif]-->
		
		<!--[if lte IE 8]>
		<script src="<?=NM_PC_URL?>/js/html5.js"></script>
		<![endif]-->

		<link rel="stylesheet" type="text/css" href="<?=NM_URL?>/css/font.css?vs=2">
		<link rel="stylesheet" type="text/css" href="<?=NM_URL?>/css/common.css<?=vs_para();?>">
		<link rel="stylesheet" href="<?=NM_URL?>/fontawesome/css/font-awesome.min.css">
		<script type="text/javascript" src="<?=NM_URL?>/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?=NM_URL?>/js/common.js<?=vs_para();?>"></script>
		<script type="text/javascript" src="<?=NM_URL?>/js/jquery.cookie.js"></script>
		<script type="text/javascript">
		<!--
			/* // 자바스크립트에서 사용하는 전역변수 선언 */
			var nm_url = "<?=NM_URL;?>";
			var nm_img = "<?=NM_IMG;?>";
			var nm_cms_img = "<?=NM_IMG;?>cms";
			var nm_mo_img = "<?=NM_MO_IMG;?>";
			var nm_pc_img = "<?=NM_PC_IMG;?>";
			var nm_comics = "<?=NM_URL;?>/comics.php";
			var nm_comicsview = "<?=NM_URL;?>/comicsview.php";

			var nm_cash_point_unit = "<?=$nm_config['cf_cash_point_unit'];?>";
			var nm_cash_point_unit_ko = "<?=$nm_config['cf_cash_point_unit_ko'];?>";
			var nm_point_unit = "<?=$nm_config['cf_point_unit'];?>";
			var nm_point_unit_ko = "<?=$nm_config['cf_point_unit_ko'];?>";
		//-->
		</script>
</head>
	<?php
	$body = "";
	if(!(is_nexcube() || $nm_member['mb_level'] > 28)){
		$body = ' onload="javascript:window.scrollTo(0, 1);" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"';
	}
	?>
	<body <?php echo $body; ?>>
	<!-- 시작 -->
        <div id="wrapper">