<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동

sleep(10);

$php_self_file = pathinfo($_SERVER['PHP_SELF']);
$php_self_file_name  = strtolower($php_self_file['filename']);

$limit_start = $_GET['limit_start'];

if($limit_start == '' || $limit_start == 0){
	$limit_start = 0;
}

// 전체 수
$sql_total_comics = "SELECT count(*) as total_comics FROM comics WHERE 1 AND cm_service='y' AND cm_cover!='' 
                     AND ( cm_cover like 'aws/%'); ";
$row_total_comics = sql_count($sql_total_comics, 'total_comics');

// 검색 수
$limit_comics = " LIMIT $limit_start , 100 ";
$sql_comics = " SELECT * FROM  `comics` WHERE 1 AND cm_service='y' AND cm_cover!='' 
                     AND ( cm_cover like 'aws/%') ORDER BY cm_no DESC $limit_comics ";
$result_comics = sql_query($sql_comics);

$limit_start += 100;

// 끝
if($limit_start > (intval($row_total_comics) + 100)){
	echo $sql_comics."<br/>";
	echo $limit_start."<br/>";
	echo (intval($row_total_comics) + 100)."<br/>";
	die;
}

// 디렉토리 생성
$storage_down = NM_THUMB_STORAGE_PATH;
while ($row_comics = sql_fetch_array($result_comics)) {

	$cm_cover_info			= pathinfo($row_comics['cm_cover']);
	$cm_cover_name			= strtolower($cm_cover_info['filename']);
	$cm_cover_extension		= strtolower($cm_cover_info['extension']);
	$cm_cover_file			= $cm_cover_name.".".$cm_cover_extension;

	$cm_cover_path = str_replace("/".$cm_cover_file, "", $row_comics['cm_cover']);
	$cm_cover_storage_save_path = $cm_cover_storage_down_path = $storage_down.'/'.$cm_cover_path;

	$cm_cover_storage_down_path = str_replace(NM_PATH, '', $cm_cover_storage_down_path);
	mkdirAll($cm_cover_storage_down_path);

	$cm_cover_save_to_filename = $cm_cover_storage_save_path.'/'.$cm_cover_file;

	$object = $container->get_object($row_comics['cm_cover']); // 파일박스에서 다운로드 받고자 하는 오브젝트
	$object->save_to_filename($cm_cover_save_to_filename); //실제 로컬에 저장 받을 시, 저장되는 파일 네임

	echo $cm_cover_storage_save_path."<br/>";
	echo $cm_cover_file."<br/>";
	echo $cm_cover_save_to_filename."<br/>";
	die;
}

// 다음
if($limit_start < (intval($row_total_comics) + 100)){
	goto_url(NM_ADM_URL.'/_ready/34aws_img_resizing/'.$php_self_file_name.'.php?limit_start='.$limit_start);
}

?>