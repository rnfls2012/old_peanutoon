<?
	include_once '../../../_common.php'; // DB 연결
	include_once '../index.php'; // 리스트

	
function aws_tn_auto_set_key_thumbnail($get_path_file, $set_path, $key, $wxh='all', $set_nick='', $mod_file='')
{
	global $thumbnail;

	$mod_file_info				= pathinfo($mod_file);
	$mod_file_name				= strtolower($mod_file_info['filename']);
	$mod_file_dir				= strtolower($mod_file_info['dirname']);
	$mod_file_extension			= strtolower($mod_file_info['extension']);

	$thumb_list = array();
	
	foreach($thumbnail[$key] as $thumbnail_key => $thumbnail_val){
		foreach($thumbnail_val as $thumb_key => $thumb_val){
			if($wxh == 'all'){
					array_push($thumb_list, array($thumb_key => array('width' => intval($thumb_val['width']),	
																	  'height' => intval($thumb_val['height']))));
			}else{
				if($wxh == $thumb_key){
					array_push($thumb_list, array($thumb_key => array('width' => intval($thumb_val['width']),	
																	  'height' => intval($thumb_val['height']))));
				}
			}
		}
	}
	
	foreach($thumb_list as $thumb_list_key => $thumb_list_val){
		foreach($thumb_list_val as $nail_key => $nail_val){
			$set_file_name = '';
			if($nail_key == 'tn'){
				$set_file_name = $key;
				if($mod_file != ''){ $set_file_name = $mod_file_name; }
			}else{
				$set_file_name = $key.'_'.$nail_key;
				if($mod_file != ''){ $set_file_name = $mod_file_name.'_'.$nail_key; }
			}
			/*
			echo $nail_key."<br/>";
			if($nail_key == 'tn229x117'){
				echo $mod_file."<br/>";
				echo $get_path_file."<br/>";
				echo NM_THUMB_PATH.'/'.$set_path."<br/>";
				echo $set_file_name."<br/>";
				echo $nail_val['width']."<br/>";
				echo $nail_val['height']."<br/>";
				// die;
				//aws_tn_auto_set_thumbnail($get_path_file, NM_THUMB_PATH.'/'.$set_path, $set_file_name, $nail_val['width'], $nail_val['height']);
			}
			*/
			aws_tn_auto_set_thumbnail($get_path_file, NM_THUMB_PATH.'/'.$set_path, $set_file_name, $nail_val['width'], $nail_val['height']);
		}
	}
}

function aws_tn_auto_set_thumbnail($get_path_file, $set_path, $set_file, $thumb_width=0, $thumb_height=0, $thumb_quality=100)
{
	// 이미지 보다 큰 크기 사이즈면 이미지 사이즈로...
	$size = @getimagesize($get_path_file);
	if($size[2] < 1 || $size[2] > 3){ /* gif, jpg, png 에 대해서만 적용 */ return false; }

	$get_width = $size[0];
	$get_height = $size[1];	

	$set_width = $thumb_width;
	$set_height = $thumb_height;

	if($thumb_width == 0 || $get_width < $thumb_width){ $set_width = $get_width; }
	if($thumb_height == 0 || $get_height < $thumb_height){ $set_height = $get_height; }

	$set_path_file = aws_tn_auto_thumbnail($get_path_file, $set_path, $set_file, $set_width, $set_height, $thumb_quality);
	
	if($set_path_file == false){ $set_path_file = $get_path_file; } // false시 복사할 이미지 경로&파일명

	return basename($set_path_file);
}

// 썸네일
function aws_tn_auto_thumbnail($get_path_file, $set_path, $set_file, $thumb_width=0, $thumb_height=0, $thumb_quality=100)
{
	$tmp_file = pathinfo($get_path_file);
	$tmp_file_name  = strtolower($tmp_file['filename']);
	$tmp_dirname = strtolower($tmp_file['dirname']);
	$tmp_extension = strtolower($tmp_file['extension']);
	
	$set_file_info = pathinfo($set_file);
	$set_file_name = strtolower($set_file_info['filename']);
	$set_file_extension = strtolower($set_file_info['extension']);

	$set_path_file = $set_path.'/'.$set_file_name.'.'.$tmp_extension; // 저장경로파일

	// echo "aws_tn_auto_thumbnail : ".$set_path_file."<br/>";

	$size = @getimagesize($get_path_file);
	if($size[2] < 1 || $size[2] > 3){ /* gif, jpg, png 에 대해서만 적용 */ return false; }

	$get_width = $size[0];
	$get_height = $size[1];	

	$set_width = $thumb_width;
	$set_height = $thumb_height;

	if($thumb_width == 0){ $set_width = $get_width; }
	if($thumb_height == 0){ $set_height = $get_height; }
	
	$tmp_set_path = str_replace(NM_PATH, '', $set_path);
	mkdirAll($tmp_set_path);

    // 디렉토리가 존재하지 않거나 쓰기 권한이 없으면 썸네일 생성하지 않음
    if(!(is_dir($set_path) && is_writable($set_path))){ return false; }

	if (in_array(strtolower($tmp_file['extension']), array("jpg","gif","bmp","png"))){

		// Animated GIF는 썸네일 생성하지 않음
		if($size[2] == 1) {
			if(tn_auto_is_animated_gif($get_path_file))
				return basename($get_path_file);
		}

		$ext = array(1 => 'gif', 2 => 'jpg', 3 => 'png');
		
		if (file_exists($set_path_file)) { return false; } // 파일이 있다면....

		// 원본파일의 GD 이미지 생성
		$src = null;
		$degree = 0;

		if ($size[2] == 1) {
			$src = @imagecreatefromgif($get_path_file);
			$src_transparency = @imagecolortransparent($src);
		} else if ($size[2] == 2) {
			$src = @imagecreatefromjpeg($get_path_file);

			if(function_exists('exif_read_data')) {
				// exif 정보를 기준으로 회전각도 구함
				$exif = @exif_read_data($source_file);
				if(!empty($exif['Orientation'])) {
					switch($exif['Orientation']) {
						case 8:
							$degree = 90;
							break;
						case 3:
							$degree = 180;
							break;
						case 6:
							$degree = -90;
							break;
					}

					// 회전각도 있으면 이미지 회전
					if($degree) {
						$src = imagerotate($src, $degree, 0);

						// 세로사진의 경우 가로, 세로 값 바꿈
						if($degree == 90 || $degree == -90) {
							$tmp = $size;
							$size[0] = $tmp[1];
							$size[1] = $tmp[0];
						}
					}
				}
			}
		} else if ($size[2] == 3) {
			$src = @imagecreatefrompng($get_path_file);
			@imagealphablending($src, true);
		} else {
			return;
		}

		if(!$src){ return; }

		$is_large = true;
		// width, height 설정
		if($set_width) {
			if(!$set_height) {
				$set_height = round(($set_width * $size[1]) / $size[0]);
			} else {
				if($size[0] < $set_width || $size[1] < $set_height)
					$is_large = false;
			}
		} else {
			if($set_height) {
				$set_width = round(($set_height * $size[0]) / $size[1]);
			}
		}

		$dst_x = 0;
		$dst_y = 0;
		$src_x = 0;
		$src_y = 0;
		$dst_w = $set_width;
		$dst_h = $set_height;
		$src_w = $size[0];
		$src_h = $size[1];

		$ratio = $dst_h / $dst_w;

		if($is_large) {
			// 크롭처리
			if($is_crop) {
				switch($crop_mode)
				{
					case 'center':
						if($size[1] / $size[0] >= $ratio) {
							$src_h = round($src_w * $ratio);
							$src_y = round(($size[1] - $src_h) / 2);
						} else {
							$src_w = round($size[1] / $ratio);
							$src_x = round(($size[0] - $src_w) / 2);
						}
						break;
					default:
						if($size[1] / $size[0] >= $ratio) {
							$src_h = round($src_w * $ratio);
						} else {
							$src_w = round($size[1] / $ratio);
						}
						break;
				}
			}

			$dst = @imagecreatetruecolor($dst_w, $dst_h);

			if($size[2] == 3) {
				@imagealphablending($dst, false);
				@imagesavealpha($dst, true);
			} else if($size[2] == 1) {
				$palletsize = @imagecolorstotal($src);
				if($src_transparency >= 0 && $src_transparency < $palletsize) {
					$transparent_color   = @imagecolorsforindex($src, $src_transparency);
					$current_transparent = @imagecolorallocate($dst, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
					@imagefill($dst, 0, 0, $current_transparent);
					@imagecolortransparent($dst, $current_transparent);
				}
			}
		} else {
			$dst = @imagecreatetruecolor($dst_w, $dst_h);
			$bgcolor = @imagecolorallocate($dst, 255, 255, 255); // 배경색

			if($src_w < $dst_w) {
				if($src_h >= $dst_h) {
					$dst_x = round(($dst_w - $src_w) / 2);
					$src_h = $dst_h;
				} else {
					$dst_x = round(($dst_w - $src_w) / 2);
					$dst_y = round(($dst_h - $src_h) / 2);
					$dst_w = $src_w;
					$dst_h = $src_h;
				}
			} else {
				if($src_h < $dst_h) {
					$dst_y = round(($dst_h - $src_h) / 2);
					$dst_h = $src_h;
					$src_w = $dst_w;
				}
			}

			if($size[2] == 3) {
				$bgcolor = @imagecolorallocatealpha($dst, 0, 0, 0, 127);
				@imagefill($dst, 0, 0, $bgcolor);
				@imagealphablending($dst, false);
				@imagesavealpha($dst, true);
			} else if($size[2] == 1) {
				$palletsize = @imagecolorstotal($src);
				if($src_transparency >= 0 && $src_transparency < $palletsize) {
					$transparent_color   = @imagecolorsforindex($src, $src_transparency);
					$current_transparent = @imagecolorallocate($dst, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
					@imagefill($dst, 0, 0, $current_transparent);
					@imagecolortransparent($dst, $current_transparent);
				} else {
					@imagefill($dst, 0, 0, $bgcolor);
				}
			} else {
				@imagefill($dst, 0, 0, $bgcolor);
			}
		}

		@imagecopyresampled($dst, $src, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

		// sharpen 적용
		if($is_sharpen && $is_large) {
			$val = explode('/', $um_value);
			UnsharpMask($dst, $val[0], $val[1], $val[2]);
		}

		if($size[2] == 1) {
			if($thumb_quality < 85){ $thumb_quality = 85; } // 최소품질
			imagegif($dst, $set_path_file, $thumb_quality);
		} else if($size[2] == 3) {
			$png_compress = round(10 - ($thumb_quality / 10));
			if($png_compress < 3){ $png_compress = 2; } // 최소품질

			imagepng($dst, $set_path_file, $png_compress);
		} else { 
			if($thumb_quality < 85){ $thumb_quality = 85; } // 최소품질
			imagejpeg($dst, $set_path_file, $thumb_quality); 
		}

		chmod($set_path_file, 0777); // 추후 삭제를 위하여 파일모드 변경

		imagedestroy($src);
		imagedestroy($dst);

		return basename($set_path_file);
	}
}


?>