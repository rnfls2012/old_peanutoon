<?
include_once '_common.php'; // 공통

/* 회원 소장 코믹스 리스트 */
// member_point_expen_episode_1 -> 구매 코믹스 화 단행본리스트 화관련 데이터가 없어서 1화에 구매했단 식으로 처리해야함

/*
SELECT 
mpec_comics, mpec_big, 
mpec_year_month, 
mpec_year, 
mpec_month, 
mpec_day, 
mpec_hour, 
mpec_date, 
SUM(mpec_count)as mpec_count_sum, 
SUM(mpec_point)as mpec_point_sum, 
SUM(mpec_cash_point)as mpec_cash_point_sum, 
mpec_cash_type
FROM  member_point_expen_comics 
GROUP BY mpec_comics, mpec_day, mpec_hour ORDER BY mpec_no
*/

$dbtable = "sales";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

$sql_chk_cnt = "SELECT count(*) as cnt FROM `member_point_expen_comics` ";
$row_chk_cnt = sql_count($sql_chk_cnt, 'cnt');

echo $sql2_cnt."<br/>";

echo $sql_chk_cnt."<br/>";

$sql_cp_point = "SELECT 
mpec_comics, mpec_big, 
mpec_year_month, 
mpec_year, 
mpec_month, 
mpec_day, 
mpec_hour, 
mpec_week, 
mpec_date, 
SUM(mpec_count)as mpec_count_sum, 
SUM(mpec_point)as mpec_point_sum, 
SUM(mpec_cash_point)as mpec_cash_point_sum, 
mpec_cash_type
FROM  member_point_expen_comics 
GROUP BY mpec_comics, mpec_day, mpec_hour ORDER BY mpec_no ";
$result_cp_point = sql_query($sql_cp_point);
echo $sql_cp_point."<br/>";

$sql_cp_list = array('sl_comics', 'sl_big');
array_push($sql_cp_list, 'sl_year_month', 'sl_year', 'sl_month', 'sl_day', 'sl_hour', 'sl_week');
array_push($sql_cp_list, 'sl_open');
array_push($sql_cp_list, 'sl_pay');
array_push($sql_cp_list, 'sl_point', 'sl_cash_point');
array_push($sql_cp_list, 'sl_cash_type');


$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_point)) {
		$row_cp['sl_comics'] = $row_cp['mpec_comics'];
		$row_cp['sl_big'] = $row_cp['mpec_big'];

		$row_cp['sl_year_month'] = $row_cp['mpec_year_month'];
		$row_cp['sl_year'] = $row_cp['mpec_year'];
		$row_cp['sl_month'] = $row_cp['mpec_month'];
		$row_cp['sl_day'] = $row_cp['mpec_day'];
		$row_cp['sl_hour'] = $row_cp['mpec_hour'];
		$row_cp['sl_week'] = $row_cp['mpec_week'];

		$row_cp['sl_open'] = $row_cp['mpec_count_sum']*2.7;

		$row_cp['sl_pay'] = $row_cp['mpec_count_sum'];

		$row_cp['sl_point'] = $row_cp['mpec_point_sum'];
		$row_cp['sl_cash_point'] = $row_cp['mpec_cash_point_sum'];

		$row_cp['sl_cash_type'] = $row_cp['mpec_cash_type'];



		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
				$sql2_insert.= 'NULL, ';
			}else{
				$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
			}
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>