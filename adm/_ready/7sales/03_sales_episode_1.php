<?
include_once '_common.php'; // 공통

/* 회원 소장 코믹스 리스트 */
// member_point_expen_episode_1 -> 구매 코믹스 화 단행본리스트 화관련 데이터가 없어서 1화에 구매했단 식으로 처리해야함

/*
SELECT 
mpee_comics, mpee_big, 
mpee_year_month, 
mpee_year, 
mpee_month, 
mpee_day, 
mpee_hour, 
mpee_date, 
SUM(mpee_count)as mpee_count_sum, 
SUM(mpee_point)as mpee_point_sum, 
SUM(mpee_cash_point)as mpee_cash_point_sum, 
mpee_cash_type
FROM  member_point_expen_comics 
GROUP BY mpee_comics, mpee_day, mpee_hour ORDER BY mpee_no
*/

$dbtable = "sales_episode_1";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

$sql_chk_cnt = "SELECT count(*) as cnt FROM `member_point_expen_episode_1` ";
$row_chk_cnt = sql_count($sql_chk_cnt, 'cnt');

echo $sql2_cnt."<br/>";

echo $sql_chk_cnt."<br/>";

$sql_cp_point = "SELECT 
mpee_comics, mpee_big, 
mpee_year_month, 
mpee_year, 
mpee_month, 
mpee_day, 
mpee_hour, 
mpee_week, 
mpee_date, 

mpee_episode, 
mpee_chapter, 

SUM(mpec_count)as mpee_count_sum, 
SUM(mpec_point)as mpee_point_sum, 
SUM(mpec_cash_point)as mpee_cash_point_sum, 
mpee_cash_type 
FROM  member_point_expen_episode_1 mpee 
JOIN member_point_expen_comics mpec ON mpee.mpec_no = mpec.mpec_no 
GROUP BY mpee_comics, mpee_day, mpee_hour ORDER BY mpee_no ";
$result_cp_point = sql_query($sql_cp_point);
echo $sql_cp_point."<br/>";

$sql_cp_list = array('se_comics', 'se_big', 'se_episode', 'se_chapter');
array_push($sql_cp_list, 'se_year_month', 'se_year', 'se_month', 'se_day', 'se_hour', 'se_week');
array_push($sql_cp_list, 'se_open');
array_push($sql_cp_list, 'se_pay');
array_push($sql_cp_list, 'se_point', 'se_cash_point');
array_push($sql_cp_list, 'se_cash_type');


$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_point)) {
		$row_cp['se_comics'] = $row_cp['mpee_comics'];
		$row_cp['se_big'] = $row_cp['mpee_big'];

		$row_cp['se_episode'] = $row_cp['mpee_episode'];
		$row_cp['se_chapter'] = $row_cp['mpee_chapter'];

		$row_cp['se_year_month'] = $row_cp['mpee_year_month'];
		$row_cp['se_year'] = $row_cp['mpee_year'];
		$row_cp['se_month'] = $row_cp['mpee_month'];
		$row_cp['se_day'] = $row_cp['mpee_day'];
		$row_cp['se_hour'] = $row_cp['mpee_hour'];
		$row_cp['se_week'] = $row_cp['mpee_week'];

		$row_cp['se_open'] = $row_cp['mpee_count_sum']*2.7;

		$row_cp['se_pay'] = $row_cp['mpee_count_sum'];

		$row_cp['se_point'] = $row_cp['mpee_point_sum'];
		$row_cp['se_cash_point'] = $row_cp['mpee_cash_point_sum'];

		$row_cp['se_cash_type'] = $row_cp['mpee_cash_type'];



		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
				$sql2_insert.= 'NULL, ';
			}else{
				$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
			}
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>