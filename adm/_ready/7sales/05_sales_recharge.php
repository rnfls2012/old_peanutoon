<?
include_once '_common.php'; // 공통

/* 매출 충전 통계 */
// member_point_expen_episode_1 -> 구매 코믹스 화 단행본리스트 화관련 데이터가 없어서 1화에 구매했단 식으로 처리해야함

/*
SELECT *, ( (FLOOR ( (YEAR( NOW( ) ) - LEFT( m.mb_birth, 4 ) +0) /10) )*10 ) as mb_age FROM  `member_cash` c  
left JOIN member m ON c.mc_member = m.mb_no 
order by c.mc_member, c.mc_date
*/

$dbtable = "sales_recharge";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

$sql_chk_cnt = "SELECT count(*) as cnt FROM `member_point_expen_comics` ";
$row_chk_cnt = sql_count($sql_chk_cnt, 'cnt');

echo $sql2_cnt."<br/>";

echo $sql_chk_cnt."<br/>";

$sql_cp_point = "SELECT *, ( (FLOOR ( (YEAR( NOW( ) ) - LEFT( m.mb_birth, 4 ) +0) /10) )*10 ) as mb_age FROM  `member_cash` c  
left JOIN member m ON c.mc_member = m.mb_no 
order by c.mc_member, c.mc_date ";
$result_cp_point = sql_query($sql_cp_point);
echo $sql_cp_point."<br/>";

$sql_cp_list = array('sr_year_month', 'sr_year', 'sr_month', 'sr_day', 'sr_hour', 'sr_min', 'sr_week');
array_push($sql_cp_list, 'sr_product', 'sr_way', 'sr_platform');
array_push($sql_cp_list, 'sr_cash_point', 'sr_point');
array_push($sql_cp_list, 'sr_pay', 'sr_re_pay');
array_push($sql_cp_list, 'sr_sex', 'sr_age');
array_push($sql_cp_list, 'sr_adult', 'sr_date');


$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_point)) {
		$row_cp['sr_year_month'] = substr($row_cp['mc_date'], 0, 7);
		$row_cp['sr_year'] = substr($row_cp['mc_date'], 0, 4);
		$row_cp['sr_month'] = substr($row_cp['mc_date'], 5, 2);
		$row_cp['sr_day'] = substr($row_cp['mc_date'], 8, 2);
		$row_cp['sr_hour'] = substr($row_cp['mc_date'], 11, 2);
		$row_cp['sr_min'] = substr($row_cp['mc_date'], 14, 2);
		$row_cp['sr_week'] = date("w",strtotime($row_cp['mc_date']));
		
		$row_cp['sr_product'] = $row_cp['mc_product'];
		$row_cp['sr_way'] = $row_cp['mc_way'];
		
		$row_cp['sr_platform'] = 1;
		if($row_cp['mc_order_access'] =="mob"){
			$row_cp['sr_platform'] = 2;
		}

		$row_cp['sr_cash_point'] = $row_cp['mc_cash_point'];
		$row_cp['sr_point'] = $row_cp['mc_point'];

		$row_cp['sr_pay'] = $row_cp['mc_won'];
		$row_cp['sr_re_pay'] = $row_cp['mc_re'];
		
		$row_cp['sr_sex'] = $row_cp['mb_sex'];
		$row_cp['sr_age'] = $row_cp['mb_age'];
		
		$row_cp['sr_adult'] = $row_cp['mb_adult'];
		$row_cp['sr_date'] = $row_cp['mc_date'];


		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
				$sql2_insert.= 'NULL, ';
			}else{
				$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
			}
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>