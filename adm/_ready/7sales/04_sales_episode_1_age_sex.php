<?
include_once '_common.php'; // 공통

/* 회원 소장 코믹스 리스트 */
// member_point_expen_episode_1 -> 구매 코믹스 화 단행본리스트 화관련 데이터가 없어서 1화에 구매했단 식으로 처리해야함

/*
SELECT 
mpee_comics, mpee_big, 
mpee_year_month, 
mpee_year, 
mpee_month, 
mpee_day, 
mpee_hour, 
mpee_week, 
mpee_date, 

count(if(mpee_sex='M' and mpee_age>0,1,null))as m_cnt, 
count(if(mpee_sex='Y' and mpee_age>0,1,null))as y_cnt, 
count(if(mpee_sex is NULL  OR mpee_sex=''  OR  mpee_age=0,1,null))as n_cnt,

count(if(mpee_sex='M' and mpee_age=10 ,1,null))as m10_cnt, 
count(if(mpee_sex='M' and mpee_age=20 ,1,null))as m20_cnt, 
count(if(mpee_sex='M' and mpee_age=30 ,1,null))as m30_cnt, 
count(if(mpee_sex='M' and mpee_age=40 ,1,null))as m40_cnt, 
count(if(mpee_sex='M' and mpee_age=50 ,1,null))as m50_cnt, 
count(if(mpee_sex='M' and mpee_age=60 ,1,null))as m60_cnt,

count(if(mpee_sex='Y' and mpee_age=10 ,1,null))as y10_cnt, 
count(if(mpee_sex='Y' and mpee_age=20 ,1,null))as y20_cnt, 
count(if(mpee_sex='Y' and mpee_age=30 ,1,null))as y30_cnt, 
count(if(mpee_sex='Y' and mpee_age=40 ,1,null))as y40_cnt, 
count(if(mpee_sex='Y' and mpee_age=50 ,1,null))as y50_cnt, 
count(if(mpee_sex='Y' and mpee_age=60 ,1,null))as y60_cnt

FROM  member_point_expen_comics 
GROUP BY mpee_comics, mpee_day, mpee_hour ORDER BY mpee_no

*/

$dbtable = "sales_episode_1_age_sex";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

$sql_chk_cnt = "SELECT count(*) as cnt FROM `member_point_expen_episode_1` ";
$row_chk_cnt = sql_count($sql_chk_cnt, 'cnt');

echo $sql2_cnt."<br/>";

echo $sql_chk_cnt."<br/>";

$sql_cp_point = "
SELECT 
mpee_comics, mpee_big, 
mpee_year_month, 
mpee_year, 
mpee_month, 
mpee_day, 
mpee_hour, 
mpee_week, 
mpee_date, 

mpee_episode, 
mpee_chapter,

count(if(mpec_sex='m' and mpec_age>0,1,null))as m_cnt, 
count(if(mpec_sex='w' and mpec_age>0,1,null))as y_cnt, 
count(if(mpec_sex is NULL OR mpec_sex='n' OR mpec_sex=''  OR  mpec_age=0,1,null))as n_cnt,

count(if(mpec_sex='m' and mpec_age=10 ,1,null))as m10_cnt, 
count(if(mpec_sex='m' and mpec_age=20 ,1,null))as m20_cnt, 
count(if(mpec_sex='m' and mpec_age=30 ,1,null))as m30_cnt, 
count(if(mpec_sex='m' and mpec_age=40 ,1,null))as m40_cnt, 
count(if(mpec_sex='m' and mpec_age=50 ,1,null))as m50_cnt, 
count(if(mpec_sex='m' and mpec_age=60 ,1,null))as m60_cnt,

count(if(mpec_sex='w' and mpec_age=10 ,1,null))as y10_cnt, 
count(if(mpec_sex='w' and mpec_age=20 ,1,null))as y20_cnt, 
count(if(mpec_sex='w' and mpec_age=30 ,1,null))as y30_cnt, 
count(if(mpec_sex='w' and mpec_age=40 ,1,null))as y40_cnt, 
count(if(mpec_sex='w' and mpec_age=50 ,1,null))as y50_cnt, 
count(if(mpec_sex='w' and mpec_age=60 ,1,null))as y60_cnt

FROM  member_point_expen_episode_1 mpee 
JOIN member_point_expen_comics mpec ON mpee.mpec_no = mpec.mpec_no 
GROUP BY mpee_comics, mpee_day, mpee_hour ORDER BY mpee_no  ";
$result_cp_point = sql_query($sql_cp_point);
echo $sql_cp_point."<br/>";

$sql_cp_list = array('seas_comics', 'seas_big', 'seas_episode', 'seas_chapter');
array_push($sql_cp_list, 'seas_year_month', 'seas_year', 'seas_month', 'seas_day', 'seas_hour', 'seas_week');
array_push($sql_cp_list, 'seas_man', 'seas_woman', 'seas_nocertify');
array_push($sql_cp_list, 'seas_man10', 'seas_man20', 'seas_man30', 'seas_man40', 'seas_man50', 'seas_man60');
array_push($sql_cp_list, 'seas_woman10', 'seas_woman20', 'seas_woman30', 'seas_woman40', 'seas_woman50', 'seas_woman60');


$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_point)) {
		$row_cp['seas_comics'] = $row_cp['mpee_comics'];
		$row_cp['seas_big'] = $row_cp['mpee_big'];

		$row_cp['seas_episode'] = $row_cp['mpee_episode'];
		$row_cp['seas_chapter'] = $row_cp['mpee_chapter'];

		$row_cp['seas_year_month'] = $row_cp['mpee_year_month'];
		$row_cp['seas_year'] = $row_cp['mpee_year'];
		$row_cp['seas_month'] = $row_cp['mpee_month'];
		$row_cp['seas_day'] = $row_cp['mpee_day'];
		$row_cp['seas_hour'] = $row_cp['mpee_hour'];
		$row_cp['seas_week'] = $row_cp['mpee_week'];

		$row_cp['seas_man'] = $row_cp['m_cnt'];
		$row_cp['seas_woman'] = $row_cp['y_cnt'];
		$row_cp['seas_nocertify'] = $row_cp['n_cnt'];
		
		$row_cp['seas_man10'] = $row_cp['m10_cnt'];
		$row_cp['seas_man20'] = $row_cp['m20_cnt'];
		$row_cp['seas_man30'] = $row_cp['m30_cnt'];
		$row_cp['seas_man40'] = $row_cp['m40_cnt'];
		$row_cp['seas_man50'] = $row_cp['m50_cnt'];
		$row_cp['seas_man60'] = $row_cp['m60_cnt'];

		$row_cp['seas_woman10'] = $row_cp['y10_cnt'];
		$row_cp['seas_woman20'] = $row_cp['y20_cnt'];
		$row_cp['seas_woman30'] = $row_cp['y30_cnt'];
		$row_cp['seas_woman40'] = $row_cp['y40_cnt'];
		$row_cp['seas_woman50'] = $row_cp['y50_cnt'];
		$row_cp['seas_woman60'] = $row_cp['y60_cnt'];



		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
				$sql2_insert.= 'NULL, ';
			}else{
				$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
			}
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>