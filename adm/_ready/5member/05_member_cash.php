<?
include_once '_common.php'; // 공통

/* 회원 소장 코믹스 리스트 */


$dbtable = "member_cash";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

$sql_chk_cnt = "SELECT count(*) as cnt FROM `cash_back` ";
$row_chk_cnt = sql_count($sql_chk_cnt, 'cnt');

echo $sql2_cnt."<br/>";

echo $sql_chk_cnt."<br/>";

if($row_chk_cnt == 0 && $row2_cnt == 0){
	/* 테이블 카피 */
	$sql_cp_member_back = "CREATE TABLE `m_mangazzang2`.`cash_back` 
							SELECT * FROM  `m_mangazzang_cp`.`cash` c 
							LEFT JOIN `m_mangazzang2`.`member` m ON c.cash_id = m.mb_id 
							LEFT JOIN `m_mangazzang2`.`config_recharge_price` crp ON crp.crp_won = c.cash_point 
							WHERE m.mb_id IS NOT NULL AND crp.crp_no IS NOT NULL ORDER BY c.cash_no ASC ";
					
	sql_query($sql_cp_member_back);
	echo $sql_cp_member_back."<br/>";
	// mc_re 처리
	$sql = "ALTER TABLE `cash_back` ADD `cash_re` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '재구매여부(0:아님, 1:재구매)' ";
	sql_query($sql);
	/* 데이터 넣기 - cash, member 데이터 */
	$sql_cash_mb = "SELECT * FROM `cash_back` c  
					left JOIN member m ON c.cash_id = m.mb_id  
					order by cash_id, cash_date, cash_date2, cash_date3";
	$result_cash_mb = sql_query($sql_cash_mb);
	$cash_id_chk = '';
	$cash_re = $cash_id_count = 0;
	echo "mc_re 처리중...<br/>";
	while ($row_cash_mb = sql_fetch_array($result_cash_mb)) {
		if($cash_id_chk != $row_cash_mb['cash_id']){
			$cash_id_chk = $row_cash_mb['cash_id'];
			$cash_re = $cash_id_count = 0;
		}else{
			$cash_id_count++;
			if($cash_id_count > 0 && $row_cash_mb['mb_won_count'] > 1){
				$cash_re = 1;
			}
		}
		$sql_cash_re = "UPDATE `cash_back` set cash_re = '".$cash_re."' where cash_no='".$row_cash_mb['cash_no']."' ";
		//echo $sql_cash_re."<br/>";
		sql_query($sql_cash_re);
	}
	die("mc_re 처리완료:다시 새로고침해주세요.");
}else{
	echo $dbtable."_back 테이블이 복사 되였으며, 파일경로도 수정업데이트 되였습니다."."<br/>";
}

$sql_cp_member = "SELECT * FROM  `cash_back` WHERE 1 ORDER BY cash_no ASC ";
$result_cp_member = sql_query($sql_cp_member);
echo $sql_cp_member."<br/>";

$sql_cp_list = array('mc_no', 'mc_member', 'mc_member_idx');
array_push($sql_cp_list, 'mc_won', 'mc_cash_point', 'mc_point');
array_push($sql_cp_list, 'mc_product', 'mc_way');
array_push($sql_cp_list, 'mc_order', 'mc_order_access');
// array_push($sql_cp_list, 'mc_pg', 'mc_pg_no'); -> 따로 처리해서 넣기
array_push($sql_cp_list, 'mc_crp');
array_push($sql_cp_list, 'mc_re');
array_push($sql_cp_list, 'mc_useragent', 'mc_version');
array_push($sql_cp_list, 'mc_date'); /* 저장일 */


$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_member)) {
		$row_cp['mc_no'] = $row_cp['cash_no'];
		$row_cp['mc_member'] = $row_cp['mb_no'];
		$row_cp['mc_member_idx'] = $row_cp['mb_idx'];

		$row_cp['mc_won'] = $row_cp['cash_point'];
		$row_cp['mc_cash_point'] = ceil($row_cp['cash_point'] / 100);
		$row_cp['mc_point'] = ceil($row_cp['cash_point_add'] / 100);

		$row_cp['mc_product'] = $row_cp['cash_point']."Cash";
		$row_cp['mc_way'] = $row_cp['cash_mode'];

		$db_mc_order = $row_cp['cash_rOrdNo'];
		if($row_cp['cash_rOrdNo'] == ""){
			$db_mc_order = "100001";
		}
		$row_cp['mc_order'] = $db_mc_order;

		$db_mc_order_access = "mob";
		if($row_cp['cash_rOrdNo']){
			if(substr($row_cp['cash_rOrdNo'], 0, 3) == "app"){
				$db_mc_order_access = "app";
			}
			else if(preg_match('/'.NM_MOBILE_AGENT.'/i', $row_cp['cash_useragent'])){
				$db_mc_order_access = "mob";
			}else{
				$db_mc_order_access = "web";
			}
		}
		$row_cp['mc_order_access'] = $db_mc_order_access;

		$row_cp['mc_crp'] = $row_cp['crp_no'];
		$row_cp['mc_re'] = $row_cp['cash_re'];

		$row_cp['mc_useragent'] = $row_cp['cash_useragent'];
		$row_cp['mc_version'] = NM_VERSION;

		$db_mc_date = $row_cp['cash_save_date'];
		if($row_cp['cash_save_date'] == "0000-00-00 00:00:00"){
			$cash_date2 = $row_cp['cash_date2'];
			$cash_date3 = $row_cp['cash_date3'];
			if(intval($cash_date2) < 10){ $cash_date2 = '0'.intval($row_cp['cash_date2']); }
			if(intval($cash_date3) < 10){ $cash_date3 = '0'.intval($row_cp['cash_date3']); }
			$db_mc_date = $row_cp['cash_date']." ".$cash_date2.":".$cash_date3.":00";
		}
		$row_cp['mc_date'] = $db_mc_date;

		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
				$sql2_insert.= 'NULL, ';
			}else{
				$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
			}
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
	// $sql_rm_member_back = "DROP TABLE cash_back ";
	// sql_query($sql_rm_member_back);	
	// 주석처리: 데이터 생성하는데 너무 오래걸림;;;
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>