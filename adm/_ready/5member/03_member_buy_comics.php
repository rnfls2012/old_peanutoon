<?
include_once '_common.php'; // 공통

/* 회원 소장 코믹스 리스트 */


$dbtable = "member_buy_comics";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

$sql_chk_cnt = "SELECT count(*) as cnt FROM `buy_member_back` ";
$row_chk_cnt = sql_count($sql_chk_cnt, 'cnt');

echo $sql2_cnt."<br/>";

echo $sql_chk_cnt."<br/>";

if($row_chk_cnt == 0 && $row2_cnt == 0){
	/* 테이블 카피 */
	$sql_cp_member_back = "CREATE TABLE `m_mangazzang2`.`buy_member_back` 
							SELECT b.*, c.*, m.mb_no, m.mb_id, m.mb_idx, m.mb_name, m.mb_level FROM  `m_mangazzang_cp`.`buy_member` b 
							LEFT JOIN `m_mangazzang2`.`comics_episode_1` c ON b.buy_content_num = c.ce_no 
							LEFT JOIN `m_mangazzang2`.`member` m ON b.buy_id = m.mb_id 
							WHERE c.ce_no IS NOT NULL AND b.buy_filenum !=0 AND m.mb_id IS NOT NULL
							GROUP BY b.buy_filenum, b.buy_id ORDER BY c.ce_comics ASC ";
					
	sql_query($sql_cp_member_back);
	echo$sql_cp_member_back."<br/>";
}else{
	echo $dbtable."_back 테이블이 복사 되였으며, 파일경로도 수정업데이트 되였습니다."."<br/>";
}

$sql_cp_member = "SELECT * FROM  `buy_member_back` WHERE 1 ORDER BY buy_date, ce_comics, ce_chapter ASC  ";
$result_cp_member = sql_query($sql_cp_member);
echo $sql_cp_member."<br/>";

$sql_cp_list = array('mbc_member', 'mbc_member_idx');
array_push($sql_cp_list, 'mbc_comics', 'mbc_big');
array_push($sql_cp_list, 'mbc_view', 'mbc_recent_chapter', 'mbc_own_type');
array_push($sql_cp_list, 'mbc_date'); /* 저장일 */


$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_member)) {
		$row_cp['mbc_member'] = $row_cp['mb_no'];
		$row_cp['mbc_member_idx'] = $row_cp['mb_idx'];

		$row_cp['mbc_comics'] = $row_cp['ce_comics'];
		$row_cp['mbc_big'] = 1;

		$db_mbc_own_type = 1;
		$db_mbc_view = 'n';
		if($row_cp['buy_end_date'] != "3000-01-01"){ 
			$db_mbc_own_type = 2; 
			$db_mbc_view = 'y';
		}
		$row_cp['mbc_view'] = $db_mbc_view;
		$row_cp['mbc_recent_chapter'] = $row_cp['ce_chapter'];
		$row_cp['mbc_own_type'] = $db_mbc_own_type;
		
		$row_cp['mbc_date'] = NM_TIME_YMDHIS; // 저장일

		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
				$sql2_insert.= 'NULL, ';
			}else{
				$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
			}
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
	$sql_rm_member_back = "DROP TABLE buy_member_back ";
	sql_query($sql_rm_member_back);
	
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>