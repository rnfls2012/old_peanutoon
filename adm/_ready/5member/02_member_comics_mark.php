<?
include_once '_common.php'; // 공통

/* 회원 코믹스 즐겨찾기 */
$sql_cp_member = "	SELECT * FROM  `m_mangazzang_cp`.`jjim` j 
					LEFT JOIN `m_mangazzang2`.`comics_episode_1` c ON j.j_filenum = c.ce_comics 
					LEFT JOIN `m_mangazzang2`.`member` m ON j.j_id = m.mb_id 
					LEFT JOIN `m_mangazzang2`.`comics` cm ON j.j_filenum = cm.cm_no 
					WHERE c.ce_no IS NOT NULL 
					AND m.mb_id IS NOT NULL 
					AND j.j_filenum > 0 
					ORDER BY j.j_filenum ASC ";
$result_cp_member = sql_query($sql_cp_member); 

echo $sql_cp_member."<br/>";

$dbtable = "member_comics_mark";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

echo $sql2_cnt."<br/>";

$sql_cp_list = array('mcm_no','mcm_member','mcm_member_idx');
array_push($sql_cp_list, 'mcm_comics', 'mcm_big', 'mcm_small');
array_push($sql_cp_list, 'mcm_date'); /* 저장일 */

$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_member)) {
		$row_cp['cm_no'] = $row_cp['j_no'];
		$row_cp['mcm_member'] = $row_cp['mb_no'];
		$row_cp['mcm_member_idx'] = $row_cp['mb_idx'];
		$row_cp['mcm_comics'] = $row_cp['cm_no'];
		$row_cp['mcm_big'] = $row_cp['cm_big'];
		$row_cp['mcm_small'] = $row_cp['cm_small'];
		$row_cp['mcm_date'] = $row_cp['j_date'];


		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>