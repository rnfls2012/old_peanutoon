<?
include_once '_common.php'; // 공통

/* 회원정보 */


$dbtable = "member";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

$sql_chk_cnt = "SELECT count(*) as cnt FROM `".$dbtable."_back` ";
$row_chk_cnt = sql_count($sql_chk_cnt, 'cnt');

echo $sql2_cnt."<br/>";

echo $sql_chk_cnt."<br/>";
if($row_chk_cnt == 0 && $row2_cnt == 0){
	/* 테이블 카피 */
	$sql_cp_member_back = "CREATE TABLE `m_mangazzang2`.`".$dbtable."_back` SELECT * FROM `m_mangazzang_cp`.`".$dbtable."`  WHERE `member_pass` !=  '' ";
	sql_query($sql_cp_member_back);

	// 가입 중복 필드 넣기
	$sql = "ALTER TABLE `".$dbtable."_back` ADD `member_overlap_key` int(11) NOT NULL COMMENT '회원 중복 체크 및 원본 넘버_0_중복아님'  AFTER `member_info` ";
	sql_query($sql);
	// echo $sql."<br/>";
	$sql_search_overlap = "SELECT member_num, member_id, member_name, member_email, member_join_date, member_out_date, member_login_date, member_cash_num, member_cash, member_cash_point, member_point, member_sex, member_adult, member_info, COUNT(member_info) as info_cnt 
	FROM `".$dbtable."_back` 
	WHERE member_overlap_key=0 and ( member_info!='' OR member_info is null )
	GROUP BY member_info HAVING COUNT(member_info)>1 order by member_num asc";
	// echo $sql_search_overlap."<br/>";
	$result_search_overlap = sql_query($sql_search_overlap);

	while($row = sql_fetch_array($result_search_overlap)) {
		$personal_overlap_sql = "select * from `".$dbtable."_back` where member_info='".$row['member_info']."' and member_info is not null and member_overlap_key=0";
		$result = sql_query($personal_overlap_sql);
		$count = sql_num_rows($result);
		// echo $personal_overlap_sql."<br>";
		if($count > 1) {
			$overlap_update_sql = "update `".$dbtable."_back` set member_overlap_key='".$row['member_num']."' where member_info = '".$row['member_info']."'";
			sql_query($overlap_update_sql);
			// echo $overlap_update_sql."<br>";
		} // end if
	} // end while

	// 이메일 중복값 제거
	$sql_search_email = "SELECT member_num, member_id, member_name, member_email, member_join_date, member_out_date, member_login_date, member_cash_num, member_cash, member_cash_point, member_point, member_sex, member_adult, member_info, COUNT(member_email) as email_cnt 
	FROM `".$dbtable."_back` 
	WHERE  member_email!='' OR member_email is null 
	GROUP BY member_email HAVING COUNT(member_email)>1 order by member_num asc";
	// echo $sql_search_email."<br/>";
	$result_search_email = sql_query($sql_search_email);

	while($row = sql_fetch_array($result_search_email)) {
		$personal_email_sql = "select * from `".$dbtable."_back` where member_email='".$row['member_email']."' and member_email is not null ";
		$result = sql_query($personal_email_sql);
		$count = sql_num_rows($result);
		// echo $personal_email_sql."<br>";
		if($count > 1) {
			$email_update_sql = "update `".$dbtable."_back` set member_email= NULL where member_email='".$row['member_email']."' and member_email is not null ";
			sql_query($email_update_sql);
			// echo $email_update_sql."<br>";
		} // end if
	} // end while
	
}else{
	echo $dbtable."_back 테이블이 복사 되였으며, 파일경로도 수정업데이트 되였습니다."."<br/>";
}

$sql_cp_member = "	SELECT * FROM  `".$dbtable."_back` WHERE `member_pass` !=  '' ORDER BY `member_num` ASC ";
$result_cp_member = sql_query($sql_cp_member);

echo $sql_cp_member."<br/>";

$sql_cp_list = array('mb_no', 'mb_id', 'mb_idx', 'mb_pass', 'mb_name', 'mb_level', 'mb_class');
array_push($sql_cp_list, 'mb_email', 'mb_phone');
array_push($sql_cp_list, 'mb_state');
array_push($sql_cp_list, 'mb_cash_point', 'mb_point');
array_push($sql_cp_list, 'mb_sex', 'mb_post', 'mb_adult', 'mb_ipin', 'mb_overlap');
array_push($sql_cp_list, 'mb_won', 'mb_won_count', 'mb_birth');
array_push($sql_cp_list, 'mb_join_date', 'mb_out_date', 'mb_login_date', 'mb_human_date', 'mb_human_email_date');

$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_member)) {
		$row_cp['mb_no'] = $row_cp['member_num'];
		$row_cp['mb_id'] = $row_cp['member_id'];
		$row_cp['mb_idx'] = $row_cp['member_id_idx'];
		$row_cp['mb_pass'] = $row_cp['member_pass'];
		$row_cp['mb_name'] = $row_cp['member_name'];

		$db_mb_level = 2;
		if($row_cp['member_level'] == "7"){ 
			$db_mb_level = 21; 
		}else if($row_cp['member_level'] == "9"){ 
			$db_mb_level = 30;
		}
		$row_cp['mb_level'] = $db_mb_level;

		$db_mb_class = 'c';
		if($row_cp['member_level'] == "7"){ 
			$db_mb_class = 'a'; 
		}else if($row_cp['member_level'] == "9"){ 
			$db_mb_class = 'a'; 
		}
		$row_cp['mb_class'] = $db_mb_class;

		$row_cp['mb_email'] = $row_cp['member_email'];
		$row_cp['mb_phone'] = $row_cp['member_phone'].$row_cp['member_phone2'].$row_cp['member_phone3'];

		$db_mb_state = 'y';
		if($row_cp['member_out_date'] != ""){
			$db_mb_state = 'n';
		}
		$row_cp['mb_state'] = $db_mb_state;

		$row_cp['mb_cash_point'] = ceil($row_cp['member_cash_point']/100);
		$row_cp['mb_point'] = ceil($row_cp['member_point']/100);

		$db_mb_sex = 'n';
		if($row_cp['member_sex'] == "M" && $row_cp['member_birth'] != ""){ 
			$db_mb_sex = 'm'; 
		}else if($row_cp['member_sex'] == "Y" && $row_cp['member_birth'] != ""){ 
			$db_mb_sex = 'w';
		}
		$row_cp['mb_sex'] = $db_mb_sex;

		$db_mb_post = 'n';
		if($row_cp['member_out_date'] == "Y"){
			$db_mb_post = 'y';
		}
		$row_cp['mb_post'] = $db_mb_post;

		$db_mb_adult = 'n';
		if($row_cp['member_adult'] == "1" && $row_cp['mb_sex'] != "n" || $row_cp['mb_level'] > 9){
			$db_mb_adult = 'y';
		}
		$row_cp['mb_adult'] = $db_mb_adult;

		$row_cp['mb_ipin'] = $row_cp['member_info'];
		$row_cp['mb_overlap'] = $row_cp['member_overlap_key'];

		$row_cp['mb_won'] = $row_cp['member_cash'];
		$row_cp['mb_won_count'] = $row_cp['member_cash_num'];
		$row_cp['mb_birth'] = $row_cp['member_birth'];

		$db_mb_join_date = '';
		if($row_cp['member_join_date'] != ""){
			$db_mb_join_date = $row_cp['member_join_date']." 00:00:00";
		}
		$row_cp['mb_join_date'] = $db_mb_join_date ;
		

		$db_mb_out_date = '';
		if($row_cp['member_out_date'] != ""){
			$db_mb_out_date = $row_cp['member_out_date']." 00:00:00";
		}
		$row_cp['mb_out_date'] = $db_mb_out_date ;

		$row_cp['mb_login_date'] = $row_cp['member_login_date'];
		$row_cp['mb_human_date'] = $row_cp['human_date'];
		$row_cp['mb_human_email_date'] = $row_cp['human_email_date'];

		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
				$sql2_insert.= 'NULL, ';
			}else{
				$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
			}
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
	$sql_rm_member_back = "DROP TABLE member_back ";
	sql_query($sql_rm_member_back);
	
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>