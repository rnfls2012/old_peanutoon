<?
include_once '_common.php'; // 공통

/* 기본설정 */
$sql_cp_config = "SELECT * FROM  `m_mangazzang_cp`.`config` WHERE `cf_no` = 1 ";
$result_cp_config = sql_query($sql_cp_config);

echo $sql_cp_config."<br/>";

$dbtable = "config";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

echo $sql2_cnt."<br/>";

$sql_cp_list = array('cf_title','cf_admin_email', 'cf_admin_email_name');
array_push($sql_cp_list, 'cf_company','cf_ceo','cf_business_no','cf_mail_order_sales','cf_address','cf_tel','cf_copy');
array_push($sql_cp_list, 'cf_admin_list','cf_partner_list','cf_client_list');
array_push($sql_cp_list, 'cf_big', 'cf_small', 'cf_toss_test');
array_push($sql_cp_list, 'cf_date'); /* 저장일 */
$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_config)) {
		$row_cp['cf_big'] = implode("|",$big_default); // 코믹스대분류
		$row_cp['cf_small'] = implode("|",$small_default); // 코믹스장르

		$row_cp['cf_admin_list'] = implode("|",$mb_admin_level_list_default); // 관리자
		$row_cp['cf_partner_list'] = implode("|",$mb_parter_level_list_default); // 파트너
		$row_cp['cf_client_list'] = implode("|",$mb_client_list_default); // 고객
		
		$row_cp['cf_toss_test'] = '0'; // 테스트모드 1:사용, 0:안함
		$row_cp['cf_date'] = NM_TIME_YMDHIS; // 저장일

		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>