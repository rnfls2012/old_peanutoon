<?
include_once '_common.php'; // 공통

// 데이터 있으면 작동안함
sql_data_check("peanutoon.t_comic_cover");

$row_comics = sql_row_comics();

mysql_query("SET GLOBAL FOREIGN_KEY_CHECKS=0;"); // 외래 키 제약 조건을 전역 해제

/* ////////////////////////////////////// peanutoon.t_comic_highlight + t_comic_serial  ////////////////////////////////////////////// */

foreach($row_comics as $row_key => $row){
	// t_comic_highlight 추가
	$t_comic_highlight="";
	if(intval($row['cm_type']) > 0){
		switch(intval($row['cm_type'])){
			case 1: $t_comic_highlight="pd추천";
			break;
			case 2: $t_comic_highlight="hot";
			break;
			case 3: $t_comic_highlight="best";
			break;
		}
	}
	if($t_comic_highlight != ""){
		$sql_insert1 = " INSERT INTO peanutoon.t_comic_highlight (t_comic_info_idx, highlight_name, is_valid) VALUES 
                         ( ".$row['t_comic_info_idx'].",'".$t_comic_highlight."',1);";
		sql_query($sql_insert1);
	}

	// t_serial_cycle 추가
	$t_serial_cycle="";
	if(intval($row['cm_public_period']) > 0){
		switch(intval($row['cm_public_period'])){
			case 1: $t_serial_cycle="매주";
			break;
			case 2: $t_serial_cycle="격주";
			break;
			case 3: $t_serial_cycle="월간";
			break;
		}
	}
	if($t_serial_cycle != ""){
		$sql_insert2 = " INSERT INTO peanutoon.t_serial_cycle (t_comic_info_idx, cycle_name, is_valid) VALUES 
                         ( ".$row['t_comic_info_idx'].",'".$t_serial_cycle."',1);";
		sql_query($sql_insert2);
	}

	// t_serial_weekly 추가
	if(intval($row['cm_public_cycle']) > 0){
		$tmp_arr[0] = $tmp_arr[1] = 0;
		$tmp1 = $tmp2 = "";
		$tmp_arr = get_comics_cycle_val($row['cm_public_cycle']);
		if(count($tmp_arr) > 2){
			echo "cm_no : ".$row['cm_no']."<br/>";
			echo "cm_public_cycle : ".$row['cm_public_cycle']."<br/>";
			echo "요일 3개 이상 있음 에러";
			die;
		}else{
			$tmp1 = sql_t_serial_weekly_idx($tmp_arr[0]);
			$tmp2 = sql_t_serial_weekly_idx($tmp_arr[1]);
			if($tmp1 != ""){ 
				$sql_insert3 = " INSERT INTO peanutoon.t_serial_weekly (t_comic_info_idx, day_name, is_valid) VALUES 
								 ( ".$row['t_comic_info_idx'].",'".$tmp1."',1);";
				sql_query($sql_insert3);
			}
			if($tmp2 != ""){ 
				$sql_insert4 = " INSERT INTO peanutoon.t_serial_weekly (t_comic_info_idx, day_name, is_valid) VALUES 
								 ( ".$row['t_comic_info_idx'].",'".$tmp2."',1);";
				sql_query($sql_insert4);
			}
		}

	}
}



/* ////////////////////////////////////// peanutoon.t_files -> t_comic_cover ////////////////////////////////////////////// */

/* peanutoon.t_file_host */
$sql_insert = "INSERT INTO peanutoon.t_file_host (description, hostname) VALUES 
               ( 'kt Cloud CDN URL', 'http://qi01-ie5524.ktics.co.kr/');";
sql_query($sql_insert);

/* // peanutoon.t_comic_episode_file -> t_comic_info_idx 저장 */
	foreach($row_comics as $row_key => &$row){
		if($row['cm_big'] == 4){ continue; } // 4는 셀시스뷰어이고 서비스 종료 및 반환해서 제외 => 갯수 88개
		// 파일경로 + 파일이름
		$row['cm_cover_filepath'] = $row['cm_cover_sub_filepath'] = $row['cm_cover_episode_filepath'] = "";
		$row['cm_cover_filename'] = $row['cm_cover_sub_filename'] = $row['cm_cover_episode_filename'] = "";

		$tmp1 = $tmp2 = $tmp3 = array();
		if($row['cm_cover'] != ''){
			$tmp1 = sql_file_division($row['cm_cover']);
			$row['cm_cover_filepath'] = $tmp1[0];
			$row['cm_cover_filename'] = $tmp1[1];		
		}
		if($row['cm_cover_sub'] != ''){
			$tmp2 = sql_file_division($row['cm_cover_sub']);
			$row['cm_cover_sub_filepath'] = $tmp2[0];
			$row['cm_cover_sub_filename'] = $tmp2[1];		
		}
		if($row['cm_cover_episode'] != ''){
			$tmp3 = sql_file_division($row['cm_cover_episode']);
			$row['cm_cover_episode_filepath'] = $tmp3[0];
			$row['cm_cover_episode_filename'] = $tmp3[1];			
		}
	}

/* // peanutoon.t_comic_cover + t_file_path + t_file // */

	foreach($row_comics as $row_key => &$row){
		$t_comic_cover_fetch = "";
		$t_comic_cover_fetch_val = "";
		// t_file_path
		if($row['cm_cover'] != ''){
			$t_file_cover_path_idx = sql_t_file_path($row['cm_cover_filepath']);
			$t_file_cover_idx = sql_t_file($t_file_cover_path_idx, $row['cm_cover_filename']);
			$t_comic_cover_fetch.= " t_file_idx_cover,";
			$t_comic_cover_fetch_val.= $t_file_cover_idx.",";
		}
		if($row['cm_cover_sub'] != ''){
			$t_file_cover_sub_path_idx = sql_t_file_path($row['cm_cover_sub_filepath']);
			$t_file_cover_sub_idx = sql_t_file($t_file_cover_sub_path_idx, $row['cm_cover_sub_filename']);
			$t_comic_cover_fetch.= " t_file_idx_cover_sm,";
			$t_comic_cover_fetch_val.= $t_file_cover_sub_idx.",";
		}
		if($row['cm_cover_episode'] != ''){
			$t_file_cover_episode_path_idx = sql_t_file_path($row['cm_cover_episode_filepath']);
			$t_file_cover_episode_idx = sql_t_file($t_file_cover_episode_path_idx, $row['cm_cover_episode_filename']);
			$t_comic_cover_fetch.= " t_file_idx_episode_cover,";
			$t_comic_cover_fetch_val.= $t_file_cover_episode_idx.",";
		}
		if($row['cm_cover'] != '' || $row['cm_cover_sub'] != '' || $row['cm_cover_episode'] != ''){
			$t_comic_cover_fetch = substr($t_comic_cover_fetch,0,strrpos($t_comic_cover_fetch, ","));	
			$t_comic_cover_fetch_val = substr($t_comic_cover_fetch_val,0,strrpos($t_comic_cover_fetch_val, ","));

			$sql_insert = " INSERT INTO peanutoon.t_comic_cover (t_comic_info_idx, ".$t_comic_cover_fetch.") VALUES (".$row['t_comic_info_idx'].",".$t_comic_cover_fetch_val.");";
			sql_query($sql_insert);
		}
		//if($row['cm_no'] ==3132){
			// print_r($row);
			// echo "<br/>";
			// echo $sql_insert."<br/>";
			// die;
		// }
	}


mysql_query("SET GLOBAL FOREIGN_KEY_CHECKS=1;"); // 끝나면 다시 설정
echo "02_comics_file.php";
?>