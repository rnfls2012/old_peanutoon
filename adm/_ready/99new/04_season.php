<?
include_once '_common.php'; // 공통

// 데이터 있으면 작동안함
sql_data_check("peanutoon.t_season");

$row_comics = sql_row_comics();

mysql_query("SET GLOBAL FOREIGN_KEY_CHECKS=0;"); // 외래 키 제약 조건을 전역 해제

/* ////////////////////////////////////// peanutoon.t_season_type ////////////////////////////////////////////// */

/* peanutoon.t_season_type */
$now = array('','본편','외전','공지','특집','후기','휴재','지연','프롤로그','미리보기','광고','패키지');
foreach($now as $now_key => $now_val){
	if($now_key == 0){ continue; }
	$sql_insert = "INSERT INTO peanutoon.t_season_type (type_name, is_valid) VALUES ('".$now_val."', 1);";
	sql_query($sql_insert);
}


/* ////////////////////////////////////// peanutoon.t_season ////////////////////////////////////////////// */
/*
select * from comics_episode_1 where ce_title!='' OR ce_notice!='' group by ce_title, ce_notice order by ce_notice, ce_title;
select * from comics_episode_2 where ce_title!='' OR ce_notice!='' group by ce_title, ce_notice order by ce_notice, ce_title;
select * from comics_episode_3 where ce_title!='' OR ce_notice!='' group by ce_title, ce_notice order by ce_notice, ce_title;
*/

$tmp_arr = array();
$sql = "SELECT * FROM peanutoonDB.t_season_type ORDER BY idx ";
$result = sql_query($sql);
while ($row = sql_fetch_array($result)) {
	$tmp_arr[$row['idx']] = $row['type_name'];
	array_push($tmp_arr, $row);
}
// 1 본편, 2 외전, 3 공지, 4 특집, 5 후기, 6 휴재, 7 지연, 8 프롤로그, 9 미리보기, 10 광고, 11 패키지

$sql1 = "select * from comics_episode_1 where ce_title!='' OR ce_notice!='' order by ce_notice, ce_title;";
$sql2 = "select * from comics_episode_2 where ce_title!='' OR ce_notice!='' order by ce_notice, ce_title;";
$sql3 = "select * from comics_episode_3 where ce_title!='' OR ce_notice!='' order by ce_notice, ce_title;";

// $tmp -> 공지-ce_notice / $temp -> 제목-ce_title

$tamp1 = $tamp2 = $tamp3 = $tamp4 = $tamp5 = $tamp6 = $tamp7 = $tamp8 = $tamp9 = $tamp10 = $tamp11 = array();
for($s=1; $s<=3; $s++){
	${'result'.$s} = sql_query(${'sql'.$s});
	while (${'row'.$s} = sql_fetch_array(${'result'.$s})) {
		for($t=1;$t<=11;$t++){
			if(count(${'temp'.$t}) > 0){
				foreach(${'temp'.$t} as ${'temp'.$t.'key'} => ${'temp'.$t.'row'}){
					if(${'row'.$s}['ce_title'] == ${'temp'.$t.'row'}){
						array_push(${'tamp'.$t}, ${'row'.$s}['ce_comics']);
					}
				}
			}
			if(count(${'tmp'.$t}) > 0){
				foreach(${'tmp'.$t} as ${'tmp'.$t.'key'} => ${'tmp'.$t.'row'}){
					if(${'row'.$s}['ce_notice'] == ${'tmp'.$t.'row'}){
						array_push(${'tamp'.$t}, ${'row'.$s}['ce_comics']);
					}
				}
			}
		}
	}
}

for($t=1;$t<=11;$t++){
	${'tsmp'.$t} = array_unique(${'tamp'.$t});
}

$new_value = array();
$new_value['n'] = 0;
$new_value['y'] = 1;

/* // $season2_cm_arr = $season2_ce_arr || $season3_cm_arr = $season3_ce_arr // */

$tsmp_arr = array();
foreach($row_comics as $row_key => $row){
	$sql_insert = " INSERT INTO peanutoon.t_season (season, t_comic_info_idx, t_season_type_idx, is_over19, is_valid) 
					VALUES (1, ".$row['t_comic_info_idx'].", 1, ".$new_value[$row['cm_adult']].", 1);";
	sql_query($sql_insert);
	if(in_array($row['cm_no'],$season2_cm_arr)){
		$sql_insert2 = " INSERT INTO peanutoon.t_season (season, t_comic_info_idx, t_season_type_idx, is_over19, is_valid) 
						VALUES (2, ".$row['t_comic_info_idx'].", 1, ".$new_value[$row['cm_adult']].", 1);";
		sql_query($sql_insert2);
	}
	if(in_array($row['cm_no'],$season3_cm_arr)){
		$sql_insert3 = " INSERT INTO peanutoon.t_season (season, t_comic_info_idx, t_season_type_idx, is_over19, is_valid) 
						VALUES (3, ".$row['t_comic_info_idx'].", 1, ".$new_value[$row['cm_adult']].", 1);";
		sql_query($sql_insert3);
	}
	// echo $sql_insert."<br/>";
	$tsmp_arr[$row['cm_no']] = $row['t_comic_info_idx'];
}

for($t=1;$t<=11;$t++){
	if(count(${'tsmp'.$t}) > 0){
		foreach(${'tsmp'.$t} as ${'tsmp'.$t.'key'} => ${'tsmp'.$t.'row'}){
			$sql_insert = " INSERT INTO peanutoon.t_season (season, t_comic_info_idx, t_season_type_idx, is_valid) 
							VALUES (1, ".$tsmp_arr[${'tsmp'.$t.'row'}].", ".$t.", 1);";
			sql_query($sql_insert);
			if(in_array(${'tsmp'.$t.'row'},$season2_cm_arr)){
				$sql_insert2 = " INSERT INTO peanutoon.t_season (season, t_comic_info_idx, t_season_type_idx, is_valid) 
								VALUES (2, ".$tsmp_arr[${'tsmp'.$t.'row'}].", ".$t.", 1);";
				sql_query($sql_insert2);
			}
			if(in_array(${'tsmp'.$t.'row'},$season3_cm_arr)){
				$sql_insert3 = " INSERT INTO peanutoon.t_season (season, t_comic_info_idx, t_season_type_idx, is_valid) 
								VALUES (3, ".$tsmp_arr[${'tsmp'.$t.'row'}].", ".$t.", 1);";
				sql_query($sql_insert3);
			}
			// echo $sql_insert."<br/>";
		}
	}
}

foreach($row_comics as $row_key => $row){
	$sql_update = " UPDATE peanutoon.t_season set is_over19 = ".$new_value[$row['cm_adult']]." 
	                WHERE t_comic_info_idx=".$row['t_comic_info_idx'].";";
	sql_query($sql_update);
	// echo $sql_update."<br/>";
}

mysql_query("SET GLOBAL FOREIGN_KEY_CHECKS=1;"); // 끝나면 다시 설정
echo "04_season.php";
?>