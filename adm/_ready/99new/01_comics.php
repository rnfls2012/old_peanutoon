<?
include_once '_common.php'; // 공통

// 데이터 있으면 작동안함
sql_data_check("peanutoon.t_comic_info");

mysql_query("SET GLOBAL FOREIGN_KEY_CHECKS=0;"); // 외래 키 제약 조건을 전역 해제

/* ////////////////////////////////////// peanutoon.t_comic ////////////////////////////////////////////// */
/* peanutoon.t_genre */
$sql = "SELECT * FROM peanutoonDB.config WHERE cf_no=1";
$row = sql_fetch($sql);
$now = explode("|", $row['cf_small']);
foreach($now as $now_key => $now_val){
	if($now_key == 0){ continue; }
	$sql_insert = "INSERT INTO peanutoon.t_genre (genre, is_valid) VALUES ('".$now_val."', 1);";
	sql_query($sql_insert);
}
// CTO님 지시사항
$sql_insert = "INSERT INTO peanutoon.t_genre (genre, is_valid) VALUES ( '전체', 1);";
sql_query($sql_insert);
$sql_insert = "INSERT INTO peanutoon.t_genre (genre, is_valid) VALUES ( '신작', 0);";
sql_query($sql_insert);
$sql_insert = "INSERT INTO peanutoon.t_genre (genre, is_valid) VALUES ( '베스트', 0);";
sql_query($sql_insert);



/* peanutoon.t_comic_type */
$now = array('','단행본','웹툰','단행본웹툰');
foreach($now as $now_key => $now_val){
	if($now_key == 0){ continue; }
	$sql_insert = "INSERT INTO peanutoon.t_comic_type (type, is_valid) VALUES ('".$now_val."', 1);";
	sql_query($sql_insert);
}


/* peanutoon.t_viewer_type */
$now = array('','왼쪽','스크롤','오른쪽');
foreach($now as $now_key => $now_val){
	if($now_key == 0){ continue; }
	$sql_insert = "INSERT INTO peanutoon.t_viewer_type (type_name, is_valid) VALUES ('".$now_val."', 1);";
	sql_query($sql_insert);
}



/* peanutoon.t_comic */
// 현재 데이터 삭제
	mysql_query("TRUNCATE peanutoon.t_comic;");
$row_comics = array(); // 코믹스 정보 저장
$now_fetch = $new_fetch = array();
$new_value = array();
array_push($now_fetch, 'cm_no', 'cm_big', 'cm_end', 'cm_page_way', 'cm_service'); 
array_push($new_fetch, 'idx', 't_comic_type_idx', 'is_finish', 't_viewer_type_idx', 'is_valid'); 
array_push($new_value, '', '', 'n|y', '|l|s|r', 'n|y|r'); 

$now = array();
$sql = "SELECT * FROM peanutoonDB.comics WHERE cm_big!=4 ORDER BY cm_no "; // 4는 셀시스뷰어이고 서비스 종료 및 반환해서 제외 => 갯수 88개
$result = sql_query($sql);
while ($row = sql_fetch_array($result)) {
	// t_comic
	foreach($now_fetch as $now_fetch_key => $now_fetch_val){
		$row[$new_fetch[$now_fetch_key]] = $row[$now_fetch_val];
		if($new_value[$now_fetch_key] !=''){
			// new_value
			$new_value_arr = explode("|", $new_value[$now_fetch_key]);			
			$new_value_arr_flip = array_flip($new_value_arr); // 키 -> 값 교체
			if($now_fetch_val == "cm_service" && $new_value_arr_flip[$row[$now_fetch_val]] > 1){ //-> 예약처리는 서비스로...
				$new_value_arr_flip[$row[$now_fetch_val]] = 1;
			}
			$row[$new_fetch[$now_fetch_key]] = $new_value_arr_flip[$row[$now_fetch_val]];
		}
	}
	$sql_insert = sql_insert("peanutoon.t_comic",$new_fetch,$row);
	sql_query($sql_insert);

	array_push($row_comics, $row);
}


/* peanutoon.t_comic_genre */
// 현재 데이터 삭제
	mysql_query("TRUNCATE peanutoon.t_comic_genre;");
$now_fetch = $new_fetch = array();
array_push($now_fetch, 'cm_small', 'cm_no'); 
array_push($new_fetch, 't_genre_idx', 't_comic_idx'); 
foreach($row_comics as $row){
	foreach($now_fetch as $now_fetch_key => $now_fetch_val){
		$row[$new_fetch[$now_fetch_key]] = $row[$now_fetch_val];
	}
	$sql_insert = sql_insert("peanutoon.t_comic_genre",$new_fetch,$row);
	sql_query($sql_insert);
}


/* ////////////////////////////////////// peanutoon.t_comic_info - $row_comics ////////////////////////////////////////////// */

/* peanutoon.t_language */
$sql_insert = "INSERT INTO peanutoon.t_language (iso_code, name_ko, is_valid) VALUES ('ko','대한민국', 1);";
sql_query($sql_insert);


		/* ////////////////////////////////////// t_comic_people ////////////////////////////////////////////// */

/* peanutoon.t_publisher */
// 현재 데이터 삭제
	mysql_query("TRUNCATE peanutoon.t_publisher;");
$now = array();
$sql = "select * from peanutoonDB.comics_publisher order by cp_no; ";
$result = sql_query($sql);
while ($row = sql_fetch_array($result)) {
	$name = "";
	$name = sql_str_return($row['cp_name']);
	$sql_insert = " INSERT INTO peanutoon.t_publisher (idx, name, is_valid, insertdate) VALUES 
				    ('".$row['cp_no']."', '".$name."', 1, '".$row['cp_date']."');";
	sql_query($sql_insert);
}


/* peanutoon.t_provider */
// 현재 데이터 삭제
	mysql_query("TRUNCATE peanutoon.t_provider;");
$now = array();
$sql = "select * from peanutoonDB.comics_provider order by cp_no; ";
$result = sql_query($sql);
while ($row = sql_fetch_array($result)) {
	$name = "";
	$name = sql_str_return($row['cp_name']);
	$sql_insert = " INSERT INTO peanutoon.t_provider (idx, name, is_valid, insertdate) VALUES 
				    ('".$row['cp_no']."', '".$name."', 1, '".$row['cp_date']."');";
	sql_query($sql_insert);
}


/* peanutoon.t_author */
// 현재 데이터 삭제
	mysql_query("TRUNCATE peanutoon.t_author;");
$now = array();
$sql = "select * from peanutoonDB.comics_professional order by cp_no; ";
$result = sql_query($sql);
while ($row = sql_fetch_array($result)) {
	$name = "";
	$name = sql_str_return($row['cp_name']);
	$sql_insert = "INSERT INTO peanutoon.t_author (idx, name, is_valid, insertdate) VALUES 
				    ('".$row['cp_no']."', '".$name."', 1, '".$row['cp_date']."');";
	sql_query($sql_insert);
}


		/* ////////////////////////////////////// t_comic_serial ////////////////////////////////////////////// */


/* peanutoon.t_comic_info */
// 현재 데이터 삭제
	mysql_query("TRUNCATE peanutoon.t_comic_info;");
$now_fetch = $new_fetch = array();
$new_value = array();
// now_fetch
array_push($now_fetch, 'cm_own_type', 'cm_series', 'cm_somaery', 'cm_no');  // cm_own_type -> 소장값이 1이라서 그냥 한국어로 대처함
array_push($now_fetch, 'cm_provider', 'cm_publisher', 'cm_professional', 'cm_professional_sub', 'cm_professional_info'); 
array_push($now_fetch, 'cm_episode_total', 'cm_free_cnt', 'cm_service', 'cm_reg_date', 'cm_episode_date'); 
// new_fetch
array_push($new_fetch, 't_language_idx', 'title', 'summary', 't_comic_idx'); // cm_own_type -> 소장값이 1이라서 그냥 한국어로 대처함
array_push($new_fetch, 't_provider_contract_idx', 't_publisher_idx', 't_author_idx_1', 't_author_idx_2', 'display_author_name');
array_push($new_fetch, 'total_episode_cnt', 'free_episode_cnt', 'is_service', 'insertdate', 'episode_update');
// new_value
array_push($new_value, '', 'string', 'string', ''); 
array_push($new_value, 'exception', '', 'exception', 'continue', 'string'); 
array_push($new_value, '', '', 'n|y|r', 'string', 'string'); 
// array_push($new_value, '', '', 'n|y', '|l|s|r', 'n|y|r'); 


if(count($now_fetch) == count($new_fetch) && count($now_fetch) == count($new_value)){
}else{
	echo "숫자가 틀립니다."."<br/>";
	echo "now_fetch : ".count($now_fetch)."<br/>";
	echo "new_fetch : ".count($new_fetch)."<br/>";
	echo "new_value : ".count($new_value)."<br/>";
	die;
}

foreach($row_comics as $row_key => $row){
	foreach($now_fetch as $now_fetch_key => $now_fetch_val){
		// 예외처리에서 적용된 내용 넘어가기
		if($new_value[$now_fetch_key] == 'continue'){ continue; }

		$row[$new_fetch[$now_fetch_key]] = $row[$now_fetch_val];

		// string 처리
		if($new_value[$now_fetch_key] == 'string'){ 
			$tmp4 = "";
			$tmp4 = sql_str_return($row[$now_fetch_val]);
			$row[$new_fetch[$now_fetch_key]] = '"'.$tmp4.'"';
			continue;
		}

		if($new_value[$now_fetch_key] !=''){
			// new_value
			$new_value_arr = explode("|", $new_value[$now_fetch_key]);			
			$new_value_arr_flip = array_flip($new_value_arr); // 키 -> 값 교체
			$row[$new_fetch[$now_fetch_key]] = $new_value_arr_flip[$row[$now_fetch_val]];
		}
		// 예외처리
		if($new_value[$now_fetch_key] == 'exception'){
			if($now_fetch[$now_fetch_key] == 'cm_provider' && $new_fetch[$now_fetch_key] == 't_provider_contract_idx'){
				if($row_key == 0){
						mysql_query("TRUNCATE peanutoon.t_provider_contract_idx;");
				}
				
				$tmp3 = "NULL";
				if(intval($row['cm_provider_sub']) >0){ $tmp3 = $row['cm_provider_sub']; }
				$sql1 = " SELECT idx FROM peanutoon.t_provider_contract 
				          WHERE t_provider_idx_1=".$row['cm_provider'];
				if(intval($row['cm_provider_sub']) >0){ $sql1.= " AND t_provider_idx_2=".$row['cm_provider_sub']; }
				
				$row1 = sql_fetch($sql1);
				if(intval($row1['idx']) > 0){
					$row['t_provider_contract_idx'] = $row1['idx'];	
				}else{
					// t_provider_contract 저장
					$sql_insert2 = " INSERT INTO peanutoon.t_provider_contract (t_provider_idx_1, is_valid) VALUES 
									 (".$row['cm_provider'].", 1);";
					if(intval($row['cm_provider_sub']) > 0){
						$sql_insert2 = " INSERT INTO peanutoon.t_provider_contract (t_provider_idx_1, t_provider_idx_2, is_valid) VALUES 
										 (".$row['cm_provider'].",".$row['cm_provider_sub'].", 1);";
					}
					sql_query($sql_insert2);
					$row2 = sql_fetch($sql1);
					$row['t_provider_contract_idx'] = $row2['idx'];
				}
			}
			if($now_fetch[$now_fetch_key] == 'cm_professional' && $new_fetch[$now_fetch_key] == 't_author_idx_1'){
				$row['t_author_idx_1'] = $row[$now_fetch_val];
				$row['t_author_idx_2'] = "NULL";
				if($row['cm_professional_sub'] > 0){ $row['t_author_idx_2'] = $row['cm_professional_sub']; }
			}
		}
		if($new_value[$now_fetch_key] == 'null'){
			$row[$new_fetch[$now_fetch_key]] = "NULL";
			if($row[$now_fetch_val] > 0){ $row[$new_fetch[$now_fetch_key]] = $row[$now_fetch_val]; }
		}
	}
	$sql_insert = sql_insert("peanutoon.t_comic_info",$new_fetch,$row);
	mysql_query($sql_insert);

	/*
	if($row['cm_no'] == 47){
		echo $sql_insert."<br/>";
	}
	select count(*) from t_comic;

	select count(*) from t_comic_info;

	select * from t_comic c left join t_comic_info ci ON c.idx = ci.t_comic_idx order by c.idx
	*/
	// 39, 47
}


// t_comic_info -> is_valid 처리
mysql_query("update peanutoon.t_comic_info set is_valid=1;");

mysql_query("SET GLOBAL FOREIGN_KEY_CHECKS=1;"); // 끝나면 다시 설정
echo "01_comics.php";
?>