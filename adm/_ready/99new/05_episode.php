<?
include_once '_common.php'; // 공통

// 데이터 있으면 작동안함
sql_data_check("peanutoon.t_episode"); // 참고로 데이터 넣는데 10분정도 걸림

$row_t_comic_idx = sql_row_t_comic_idx();

mysql_query("SET GLOBAL FOREIGN_KEY_CHECKS=0;"); // 외래 키 제약 조건을 전역 해제

// $temp -> 제목-ce_title
$display_last_temp = array();
array_push($display_last_temp, '극지연애 (완)', '런치 도련님 (완)');
array_push($display_last_temp, '마지막화', '최종화', '최종화 ACCA라는 새가 날아오른 날', 'EVER AFTER (최종)');
array_push($display_last_temp, '살아남는다 (시즌1 최종)', '시즌 1 최종', '시즌 1 최종화', '시즌 2 최종화');
array_push($display_last_temp, '시즌1 완결', '시즌1 최종', '시즌1 최종화', '시즌2 최종', '시즌2 최종화', '시즌3 최종');
array_push($display_last_temp, '오늘, 야근할거야 (최종화)', '외전 1 최종화', '외전 2 최종화', '외전 최종화');
array_push($display_last_temp, '종전(시즌2최종)', '낙인(4)_ 시즌1 최종', '만화가 현실로 (하) (完)', '천생연분 (최종화)');

// $temp -> 제목-ce_notice
$display_last_tmp  = array();
array_push($display_last_tmp, '시즌1 최종화', '최종화', '1부 완결', '번외편8화(완)', '시즌1 최종화', '시즌2 최종화');
array_push($display_last_tmp, '외전 4화(완)', '외전 최종화');


$bool = array();
$bool['n'] = 0;
$bool['y'] = 1;
$bool['r'] = 2;


$t_episode_arr = array();
$sql1 = "select * from peanutoonDB.comics_episode_1 order by ce_no;";
$sql2 = "select * from peanutoonDB.comics_episode_2 order by ce_no;";
$sql3 = "select * from peanutoonDB.comics_episode_3 order by ce_no;";

$data_count = 0;
for($s=1; $s<=3; $s++){
	${'result'.$s} = sql_query(${'sql'.$s});
	for($z=1; ${'row'.$s} = sql_fetch_array(${'result'.$s}); $z++){		
		// t_comic 있는지 확인
		$sql_comics = "select * from peanutoonDB.comics where cm_no=".${'row'.$s}['ce_comics'].";";
		$row_comics = sql_fetch($sql_comics);
		if(intval($row_comics['cm_no']) == 0){ continue; /* 이전에 삭제한 자료 */ }
		if($row_comics['cm_big'] == 4){ continue; } // 4는 셀시스뷰어이고 서비스 종료 및 반환해서 제외 => 갯수 88개

		${'row'.$s}['t_comic_info_idx'] = $row_t_comic_idx[${'row'.$s}['ce_comics']];
		$ce_title = "";
		$ce_title = sql_str_return(${'row'.$s}['ce_title']);
		${'row'.$s}['display_title'] = '"'.$ce_title.'"';

		${'row'.$s}['display_episode_num'] = 0;
		if(intval(${'row'.$s}['ce_chapter']) > 0){ // 본편 번호
			${'row'.$s}['display_episode_num'] = ${'row'.$s}['ce_chapter'];
		}else{
			if(intval(${'row'.$s}['ce_outer_chapter']) > 0){ // 외전 번호
				${'row'.$s}['display_episode_num'] = ${'row'.$s}['ce_outer_chapter'];
			}
		}

		${'row'.$s}['display_order'] = ${'row'.$s}['ce_order']; // 순서
		
		${'row'.$s}['is_display_last'] = 0; // 최종화
		if(in_array($row['ce_title'], $display_last_temp)){
			${'row'.$s}['is_display_last'] = 1;
		}
		if(in_array($row['ce_notice'], $display_last_tmp)){
			${'row'.$s}['is_display_last'] = 1;
		}

		// is_free 있긴 한데 의미가 없음
		${'row'.$s}['price'] = ${'row'.$s}['ce_pay']; // 가격
		
		// price_free_storage는 나중에 합번에

		${'row'.$s}['is_login'] = $bool[${'row'.$s}['ce_login']]; // 로그인
		
		${'row'.$s}['is_service'] = $bool[${'row'.$s}['ce_service_state']]; // 서비스 상태

		${'row'.$s}['is_valid'] = 1;
		if(${'row'.$s}['ce_service_state'] == 'n'){
			${'row'.$s}['is_valid'] = 0;		
		}
		${'row'.$s}['service_began_date'] = '"'.${'row'.$s}['ce_service_date'].'"'; // 서비스일

		// legacy
		${'row'.$s}['legacy_comic_no']   = ${'row'.$s}['ce_comics'];
		${'row'.$s}['legacy_episode_no'] = ${'row'.$s}['ce_no'];
		${'row'.$s}['legacy_big_no']     = $s;

		
		// t_season_type
		$t_season_type_idx = 1;
		for($t=1;$t<=11;$t++){
			if(count(${'temp'.$t}) > 0){
				foreach(${'temp'.$t} as ${'temp'.$t.'key'} => ${'temp'.$t.'row'}){
					if(${'row'.$s}['ce_title'] == ${'temp'.$t.'row'}){
						$t_season_type_idx = $t;
					}
				}
			}
			if(count(${'tmp'.$t}) > 0){
				foreach(${'tmp'.$t} as ${'tmp'.$t.'key'} => ${'tmp'.$t.'row'}){
					if(${'row'.$s}['ce_notice'] == ${'tmp'.$t.'row'}){
						$t_season_type_idx = $t;
					}
				}
			}
		}


		// season
		$season = 1;
		if(in_array(${'row'.$s}['ce_comics'], $season2_cm_arr)){
			foreach($season2_cm_arr as $season2_cm_key => $season2_cm_row){
				if(${'row'.$s}['ce_comics'] == $season2_cm_row){
					if(${'row'.$s}['ce_no'] >= $season2_ce_arr[$season2_cm_key]){
						$season = 2;
					}
				}
			}
		}
		if(in_array(${'row'.$s}['ce_comics'], $season3_cm_arr)){
			foreach($season3_cm_arr as $season3_cm_key => $season3_cm_row){
				if(${'row'.$s}['ce_comics'] == $season3_cm_row){
					if(${'row'.$s}['ce_no'] >= $season3_ce_arr[$season2_cm_key]){
						$season = 3;
					}
				}
			}
		}

		
		// t_season_idx
		$sql_season = $row_season = "";
		$sql_season = "select * from peanutoon.t_season where season=".$season." 
		                                      AND t_comic_info_idx =".${'row'.$s}['t_comic_info_idx']." 
										      AND t_season_type_idx=".$t_season_type_idx.";";
		$row_season = sql_fetch($sql_season);
		if(intval($row_season['idx']) == 0 || $row_season == ''){
			echo "<br/>";
			print_r($row_season);
			echo "sql_season 에러 : ".$sql_season."<br/>";
			echo "ce_no : ".${'row'.$s}['ce_no']."<br/>";
			echo "s : ".$s."<br/>";
			echo "z : ".$z."<br/>";
			die;
		}else{
			${'row'.$s}['t_season_idx'] = $row_season['idx']; // 시즌번호
		}

		// t_file_idx
		${'row'.$s}['ce_cover_filepath'] = ${'row'.$s}['ce_cover_filename'] = "";
		$tmp1 = array();
		if(${'row'.$s}['ce_cover'] != ''){
			$tmp1 = sql_file_division(${'row'.$s}['ce_cover']);
			${'row'.$s}['ce_cover_filepath'] = $tmp1[0];
			${'row'.$s}['ce_cover_filename'] = $tmp1[1];	
			
			$t_file_cover_path_idx = sql_t_file_path(${'row'.$s}['ce_cover_filepath']);
			$t_file_cover_idx = sql_t_file($t_file_cover_path_idx, ${'row'.$s}['ce_cover_filename']);
			${'row'.$s}['t_file_idx'] = $t_file_cover_idx;
		}else{
			echo "ce_no : ".${'row'.$s}['ce_no']."<br/>";
			echo "s : ".$s."<br/>";
			echo "z : ".$z."<br/>";
			die;
		}
		array_push($t_episode_arr, ${'row'.$s});
		$data_count++; // 키운팅
	}
}

$new_fetch = array();
array_push($new_fetch, 't_comic_info_idx', 't_season_idx', 't_file_idx'); 
array_push($new_fetch, 'display_title', 'display_episode_num', 'display_order', 'is_display_last'); 
array_push($new_fetch, 'price', 'is_login', 'is_service', 'is_valid', 'service_began_date');
array_push($new_fetch, 'legacy_big_no', 'legacy_comic_no', 'legacy_episode_no');

foreach($t_episode_arr as $t_episode_key => $t_episode_row){	
	$sql_insert = sql_insert("peanutoon.t_episode",$new_fetch,$t_episode_row);
	mysql_query($sql_insert);
	// echo $sql_insert."<br/>";
	// die;
}

echo $data_count."<br/>";

mysql_query("SET GLOBAL FOREIGN_KEY_CHECKS=1;"); // 끝나면 다시 설정
echo "05_episode.php";
?>