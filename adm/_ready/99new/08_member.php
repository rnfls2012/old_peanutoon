<?
include_once '_common.php'; // 공통

// 데이터 있으면 작동안함
sql_data_check("peanutoon.t_user");
mysql_query("SET GLOBAL FOREIGN_KEY_CHECKS=0;"); // 외래 키 제약 조건을 전역 해제

/* peanutoon.t_join_source */
$now = array('');
array_push($now, 'programmed', 'email','facebook','twitter','naver','kakao'); // 로그인 타입
array_push($now, 'google'); // 구글로그인-미사용


    /**
     * 가입경로 - 프로그램 (슈퍼유저 생성시에만 사용)
    const JOIN_SOURCE_PROGRAMMED = "programmed";
     * 가입경로 - 이메일 
    const JOIN_SOURCE_EMAIL = "email";
     * 가입경로 - 페북
    const JOIN_SOURCE_FACEBOOK = "facebook";
     * 가입경로 - 트위터
    const JOIN_SOURCE_TWITTER = "twitter";
     * 가입경로 - 네이버
    const JOIN_SOURCE_NAVER = "naver";
     * 가입경로 - 카카오톡
    const JOIN_SOURCE_KAKAO = "kakao";
     * 휴면정책 - 휴면 안함
    const SLEEP_POLICY_NOSLEEP = "nosleep";
	**/


foreach($now as $now_key => $now_val){
	if($now_key == 0){ continue; }
	$sql_insert = "INSERT INTO peanutoon.t_join_source (display_name, is_valid) VALUES ('".$now_val."', 1);";
	sql_query($sql_insert);
}

/* peanutoon.t_sleep_policy */
$now = array('');
array_push($now, '1년', '3년','5년','탈퇴전'); // 휴면방지 - 1년 기본

foreach($now as $now_key => $now_val){
	if($now_key == 0){ continue; }
	$sql_insert = "INSERT INTO peanutoon.t_sleep_policy (policy_name) VALUES ('".$now_val."');";
	sql_query($sql_insert);
}

/* peanutoon.t_user_state */
$now = array('');
array_push($now, 'use','leave','stop','sleep','snooze'); // 사용자 상태
// -use:사용,leave:탈퇴,stop:중지,sleep:휴면,snooze:휴면대기

foreach($now as $now_key => $now_val){
	if($now_key == 0){ continue; }
	$sql_insert = "INSERT INTO peanutoon.t_user_state (state_name) VALUES ('".$now_val."');";
	sql_query($sql_insert);
}



// member 중복제거 - 회원번호 848700 이전은 중복없음
$update_ctn = 0; // 중복값 체크
$sql1 = "SELECT * FROM peanutoonDB.member where mb_state='y' AND mb_no > 848700 GROUP BY mb_id HAVING COUNT(mb_id) > 1 order by mb_id;";
$result1 = sql_query($sql1);
for($i=0; $row1 = sql_fetch_array($result1); $i++){
	$sql2 = "SELECT * FROM peanutoonDB.member where mb_id='".$row1['mb_id']."' AND mb_state='y' order by mb_no;";
	$result2 = sql_query($sql2);

	for($j=0; $row2 = sql_fetch_array($result2); $j++){
		if($row2['mb_invite_code'] != ""){ continue; } // 일반적인 가입 후 가입된 회원번호에 무조건 할당되는 유니크값
		$sql3 = "UPDATE peanutoonDB.member set mb_state='a' WHERE mb_state='y' AND mb_invite_code is NULL AND mb_no=".$row2['mb_no'].";";
		sql_query($sql3);
		$update_ctn++;
	}
}
echo "<br/> update_ctn : ".$update_ctn."<br/>"; // update 갯수

// member 데이터중 mb_name, mb_nick 이상있는 데이터 초기화
$sql_update1 = "UPDATE peanutoonDB.member set mb_nick='toou' where mb_no=420473;";
sql_query($sql_update1);
$sql_update2 = "UPDATE peanutoonDB.member set mb_name='', mb_nick='' where mb_no=679909;";
sql_query($sql_update2);


/* peanutoon.t_user t_verified_individual t_wallet t_personal_wallet t_user_verified   */
$mb_sex_arr = array('n'=>'u','m'=>'m','w'=>'f');
$t_user_fetch = array();
array_push($t_user_fetch, 'idx', 'identifier', 'email', 'oldpassword', 'nickname');
array_push($t_user_fetch, 't_sleep_policy_idx', 't_user_state_idx', 't_join_source_idx');
array_push($t_user_fetch, 'infoagree', 'joindate');

$t_verified_individual_fetch = array();
array_push($t_verified_individual_fetch, 'idx', 'real_name', 'birth_date', 'is_over19', 'mobile_phone_no', 'gender');

$t_personal_wallet_fetch = array();
array_push($t_personal_wallet_fetch, 'idx', 'amout_peanut', 'amout_minipeanut', 't_user_idx');

$sql1 = "SELECT * FROM peanutoonDB.member where mb_state='y' order by mb_no;";
$result1 = sql_query($sql1);
for($i=0; $row1 = sql_fetch_array($result1); $i++){
// peanutoon.t_user 
	$t_user_tmp = array();
	$t_user_tmp['idx'] = $row1['mb_no'];
	$t_user_tmp['identifier'] = "'".addslashes($row1['mb_id'])."'";
	$t_user_tmp['email'] = "'".addslashes($row1['mb_email'])."'";;
	$t_user_tmp['oldpassword'] = "'".$row1['mb_pass']."'";
	$t_user_tmp['nickname'] = "'".sql_str_return($row1['mb_nick'])."'";
	
	switch($row1['mb_human_prevent']){ // select mb_human_prevent from peanutoonDB.member where mb_state='y' group by mb_human_prevent;
		case '3' :	 $t_user_tmp['t_sleep_policy_idx'] = 2;
		break;
		case '5' :	 $t_user_tmp['t_sleep_policy_idx'] = 3;
		break;
		case 'out' : $t_user_tmp['t_sleep_policy_idx'] = 4;
		break;
		default:	 $t_user_tmp['t_sleep_policy_idx'] = 1;
		break;
	}
	
	switch($row1['mb_state_sub']){ // select mb_state_sub from peanutoonDB.member where mb_state='y' group by mb_state_sub;
		case 'h' :	 $t_user_tmp['t_user_state_idx'] = 4;
		break;
		case 's' :	 $t_user_tmp['t_user_state_idx'] = 3;
		break;
		case 't' :   $t_user_tmp['t_user_state_idx'] = 5;
		break;
		default:	 $t_user_tmp['t_user_state_idx'] = 1;
		break;
	}
	
	switch($row1['mb_sns_type']){ // select mb_sns_type from peanutoonDB.member where mb_state='y' group by mb_sns_type;
		case 'fcb' :	 $t_user_tmp['t_join_source_idx'] = 3;
		break;
		case 'kko' :	 $t_user_tmp['t_join_source_idx'] = 6;
		break;
		case 'nid' :     $t_user_tmp['t_join_source_idx'] = 5;
		break;
		case 'twt' :     $t_user_tmp['t_join_source_idx'] = 4;
		break;
		default:	     $t_user_tmp['t_join_source_idx'] = 2;
		break;
	}

	$t_user_tmp['infoagree'] = 0;
	if($row1['mb_post'] == 'y'){ $t_user_tmp['infoagree'] = 1; }
	$t_user_tmp['joindate'] = "'".$row1['mb_join_date']."'";
	
	$sql_insert_t_user = sql_insert("peanutoon.t_user",$t_user_fetch,$t_user_tmp);

	// echo $sql_insert_t_user."<br/>";
	// sql_query($sql_insert_t_user);
	if(sql_query($sql_insert_t_user)){}else{ echo "sql_insert_t_user :".$sql_insert_t_user."<br/>"; }
	
// peanutoon.t_user end

// peanutoon.t_verified_individual - 관리자가 ID생성해서 넣어 줄때 birth_date, mobile_phone_no 필드 경우 NULL 있음
	$t_verified_individual_tmp = array();
	$t_verified_individual_tmp['idx'] = $row1['mb_no'];
	$t_verified_individual_tmp['real_name'] = "'".sql_str_return($row1['mb_name'])."'";
	$t_verified_individual_tmp['birth_date'] = "NULL";
	if($row1['mb_birth']!=""){
		$t_verified_individual_tmp['birth_date'] = "'".$row1['mb_birth']."'";
	}
	$t_verified_individual_tmp['is_over19'] = 0;
	if($row1['mb_adult'] == 'y'){		
		$t_verified_individual_tmp['is_over19'] = 1;
	}
	$t_verified_individual_tmp['mobile_phone_no'] = "NULL";
	if($row1['mb_phone']!=""){
		$t_verified_individual_tmp['mobile_phone_no'] = "'".$row1['mb_phone']."'";
	}
	$t_verified_individual_tmp['gender'] = "'".$mb_sex_arr[$row1['mb_sex']]."'";
	$sql_insert_t_verified_individual = sql_insert("peanutoon.t_verified_individual",$t_verified_individual_fetch,$t_verified_individual_tmp);

	// echo $sql_insert_t_verified_individual."<br/>";
	// sql_query($sql_insert_t_verified_individual);
	if(sql_query($sql_insert_t_verified_individual)){}else{ echo "sql_insert_t_verified_individual :".$sql_insert_t_verified_individual."<br/>"; }
	
// peanutoon.t_verified_individual end

// peanutoon.t_wallet
	$t_personal_wallet_tmp = array();
	$t_personal_wallet_tmp['idx'] = $row1['mb_no'];
	$t_personal_wallet_tmp['amout_peanut'] = $row1['mb_cash_point'];
	$t_personal_wallet_tmp['amout_minipeanut'] = $row1['mb_point'];
	$t_personal_wallet_tmp['t_user_idx'] = $row1['mb_no'];

	$sql_insert_t_wallet = sql_insert("peanutoon.t_wallet",$t_personal_wallet_fetch,$t_personal_wallet_tmp);
	// echo $sql_insert_t_wallet."<br/>";
	// sql_query($sql_insert_t_wallet);
	if(sql_query($sql_insert_t_wallet)){}else{ echo "sql_insert_t_wallet :".$sql_insert_t_wallet."<br/>"; }

// peanutoon.t_wallet end
	
	$sql_insert_t_user_verified = " 
		INSERT INTO peanutoon.t_user_verified (t_verified_individual_idx, t_user_idx) VALUES 
		( ".$row1['mb_no'].", ".$row1['mb_no'].");"; // t_user_verified
	// echo $sql_insert_t_user_verified."<br/>";
	// sql_query($sql_insert_t_user_verified);
	if(sql_query($sql_insert_t_user_verified)){}else{ echo "sql_insert_t_user_verified :".$sql_insert_t_user_verified."<br/>"; }

	$sql_insert_t_personal_wallet = " 
		INSERT INTO peanutoon.t_personal_wallet (t_verified_individual_idx, t_wallet_idx) VALUES 
		( ".$row1['mb_no'].", ".$row1['mb_no'].");"; // t_personal_wallet
	// echo $sql_insert_t_personal_wallet."<br/>";
	// sql_query($sql_insert_t_personal_wallet);
	if(sql_query($sql_insert_t_personal_wallet)){}else{ echo "sql_insert_t_personal_wallet :".$sql_insert_t_personal_wallet."<br/>"; }
}

/*
검증 
select count(*) as new_ctn from peanutoon.t_user_verified;
select count(*) as new_ctn from peanutoon.t_personal_wallet;
select count(*) as new_ctn from peanutoon.t_wallet;
select count(*) as new_ctn from peanutoon.t_verified_individual;
select count(*) as new_ctn from peanutoon.t_user;

select count(*) as old_ctn from peanutoonDB.member where mb_state='y';


*/


















/* peanutoon.t_role */
// 어떻게 사용 할지 몰라서 애매함
/*
$now = array('');
$now_value = array('');
array_push($now, 'admin'); // role
array_push($now_value, '관리자'); // role설명

array_push($now, 'sysop', 'manager', 'business'); // role
array_push($now_value, '운영자', '경영자', ''); // role설명

array_push($now, 'silver','gold','vip'); // role
array_push($now_value, '실버등급', '골드등급', 'vip등급'); // role설명

foreach($now as $now_key => $now_val){
	if($now_key == 0){ continue; }
	$sql_insert = "INSERT INTO peanutoon.t_role (display_name, is_valid) VALUES ('".$now_val."', 1);";
	sql_query($sql_insert);
}
*/
/*

array_push($now, 'email'); // 이메일마케팅

array_push($now, 'adplex', 'blog.naver', 'youtube', 'my-progress'); // 외부마케팅1
array_push($now, 'post.naver', 'story.kakao', 'tv.kakao', 'kakao-plus'); // 외부마케팅2
array_push($now, 'blogger', 'band', 'ads.google', 'news.peanutoon'); // 외부마케팅3

array_push($now, 'peanutoon_pc_main_banner', 'peanutoon_mobile_main_banner'); // 내부마케팅1
array_push($now, 'peanutoon_event', 'peanutoon_friend_invite'); // 내부마케팅2
array_push($now, 'supporters_posting', 'supporters_marketer_link'); // 내부마케팅3

array_push($now, 'facebook_aboutplatform', 'bm_communication', 'socialplan', 'monbox'); // 기타마케팅1
array_push($now, 'ticketsocket', 'instagram'); // 기타마케팅2

*/


mysql_query("SET GLOBAL FOREIGN_KEY_CHECKS=1;"); // 끝나면 다시 설정
echo "08_member.php";
?>