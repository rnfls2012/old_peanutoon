<?
include_once '_common.php'; // 공통

echo "<div><br/>";
echo "데이터 수 구하는 것은 제가 한 의도(SQL)을 알려드리고 최종 점검하기 위해 작성한 것 입니다."."</div>";

$new_arr = array();
$old_arr = array();

array_push($new_arr, 't_author', 't_comic'); // t_author 501번 안들어가서 insert 작업함(sql_str_return() 참조);
array_push($new_arr, 't_comic_cover');
array_push($new_arr, 't_comic_cover WHERE t_file_idx_cover > 0;');
array_push($new_arr, 't_comic_cover WHERE t_file_idx_cover_sm > 0;');
array_push($new_arr, 't_comic_cover WHERE t_file_idx_episode_cover > 0;');

array_push($new_arr, 't_comic_genre');
array_push($new_arr, 't_comic_genre WHERE t_genre_idx=1;');
array_push($new_arr, 't_comic_genre WHERE t_genre_idx=2;');
array_push($new_arr, 't_comic_genre WHERE t_genre_idx=3;');
array_push($new_arr, 't_comic_genre WHERE t_genre_idx=4;');
array_push($new_arr, 't_comic_genre WHERE t_genre_idx=5;');
array_push($new_arr, 't_comic_genre WHERE t_genre_idx=6;');
array_push($new_arr, 't_comic_genre WHERE t_genre_idx=7;');
array_push($new_arr, 't_comic_genre WHERE t_genre_idx=8;');

array_push($new_arr, 't_comic_highlight');
array_push($new_arr, 't_comic_info', 't_comic_type');
array_push($new_arr, 't_genre');
array_push($new_arr, 't_provider', 't_publisher');
array_push($new_arr, 't_serial_cycle');
// array_push($new_arr, 't_season'); // 시즌은 이번에 들어간 개념이라 갯수 구하는 거 생각좀 해야함;;;
array_push($new_arr, 't_temp_cm');
array_push($new_arr, 't_serial_weekly');
array_push($new_arr, 't_provider_contract');
array_push($new_arr, 't_episode', 't_episode_sequence');
// array_push($new_arr, 't_file_path'); // 시즌은 이번에 들어간 개념이라 갯수 구하는 거 생각좀 해야함;;;
array_push($new_arr, 't_file');

// array_push($new_arr, '1111');

echo "<br/>";


$new_ctn_arr = array();
foreach($new_arr as $new_key => $new_val){
	$sql = "select count(*) as new_ctn from peanutoon.".$new_val.";";
	$row = sql_fetch($sql);
	array_push($new_ctn_arr, $row['new_ctn']);
}


// old sql 구해서 넣기
$old_sql_fetch = "select count(*) as old_ctn from peanutoonDB.";
$old_sql_arr = array();
array_push($old_sql_arr, $old_sql_fetch.'comics_professional;');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4;');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND (cm_cover !="" OR cm_cover_sub !="" OR cm_cover_episode !="" );');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_cover !="";');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_cover_sub !="";');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_cover_episode !="";');

array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_small > 0;');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_small = 1;');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_small = 2;');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_small = 3;');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_small = 4;');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_small = 5;');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_small = 6;');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_small = 7;');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_small = 8;');

array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_type > 0;');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4;');
array_push($old_sql_arr, 'select count(distinct cm_big ) as old_ctn from peanutoonDB.comics WHERE cm_big!=4;');
array_push($old_sql_arr, 'select count(distinct cm_small ) as old_ctn from peanutoonDB.comics WHERE cm_big!=4;');
array_push($old_sql_arr, $old_sql_fetch.'comics_provider;');
array_push($old_sql_arr, $old_sql_fetch.'comics_publisher;');
array_push($old_sql_arr, $old_sql_fetch.'comics WHERE cm_big!=4 AND cm_public_period > 0;');

$old_ctn_arr = array();
foreach($old_sql_arr as $old_sql_key => $old_sql_val){
	$row = sql_fetch($old_sql_val);
	array_push($old_ctn_arr, $row['old_ctn']);
}


//////////////// 제외처리 1
//// t_temp_cm 
$old_sql_arr2 = array();
// '장르-BL','장르-GL','장르-로맨스','장르-드라마','장르-코믹','장르-성인','','장르-소설'
array_push($old_sql_arr2, "SELECT count(distinct cm_no) as old_ctn FROM peanutoonDB.comics c left JOIN peanutoonDB.comics_ranking_sales_cmgenre_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 and c.cm_service = 'y' AND crs.crs_class in ('1','2','3','4','5','6','8');");
// '장르-전체'
array_push($old_sql_arr2, "SELECT count(distinct cm_no) as old_ctn FROM peanutoonDB.comics c left JOIN peanutoonDB.comics_ranking_sales_cmgenre_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 and c.cm_service = 'y' AND crs.crs_class = '0' OR c.cm_no IN (2645, 2024, 1252, 2659, 805, 1979, 2381, 2147, 2322, 3115, 2207, 1866, 3009, 3033, 3022, 772, 3111, 2851, 2323, 3093, 978, 2073, 1308, 1840, 2082, 2376, 602, 1165, 2382);");
// 신작
array_push($old_sql_arr2, "SELECT count(distinct cm_no) as old_ctn FROM peanutoonDB.comics c left JOIN peanutoonDB.comics_ranking_sales_cmnew_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 AND crs.crs_class = '0' OR c.cm_no IN (3044, 3033, 3009, 2941, 2933, 2933);");
// 베스트
array_push($old_sql_arr2, "SELECT count(distinct cm_no) as old_ctn FROM peanutoonDB.comics c left JOIN comics_ranking_sales_cmtop_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 AND crs.crs_class = '0';");
// 무료
array_push($old_sql_arr2, "SELECT count(distinct cm_no) as old_ctn FROM peanutoonDB.comics c left JOIN comics_ranking_sales_cmfree_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 AND crs.crs_class = '0';");

//// t_serial_weekly
$old_sql_arr3 = array();
// 1:월, 2:화, 4:수, 8:목, 16:금, 32:토, 64:일
array_push($old_sql_arr3, "select count(distinct cm_no) as old_ctn from peanutoonDB.comics WHERE cm_big!=4 AND cm_public_cycle > 0 AND cm_public_cycle in (1,2,4,8,16,32,64);");
// 요일 2중
array_push($old_sql_arr3, "select count(distinct cm_no) as old_ctn from peanutoonDB.comics WHERE cm_big!=4 AND cm_public_cycle > 0 AND cm_public_cycle not in (1,2,4,8,16,32,64);");
// 요일 2중 => 2중이므로 한번 더 구해서 숫자 * 2
array_push($old_sql_arr3, "select count(distinct cm_no) as old_ctn from peanutoonDB.comics WHERE cm_big!=4 AND cm_public_cycle > 0 AND cm_public_cycle not in (1,2,4,8,16,32,64);");

//// t_provider_contract
$old_sql_arr4 = array();
array_push($old_sql_arr4, "select count(distinct cm_provider, cm_provider_sub) as old_ctn from peanutoonDB.comics WHERE cm_big!=4;");

//// t_episode
$old_sql_arr5 = array();
// 코믹스 번호 삭제되어서 없는 데이터 제외 처리
array_push($old_sql_arr5, "select count(*) as old_ctn from peanutoonDB.comics_episode_1 ce left join peanutoonDB.comics cm on ce.ce_comics=cm.cm_no where cm_no > 0;");
array_push($old_sql_arr5, "select count(*) as old_ctn from peanutoonDB.comics_episode_2 ce left join peanutoonDB.comics cm on ce.ce_comics=cm.cm_no where cm_no > 0;");
array_push($old_sql_arr5, "select count(*) as old_ctn from peanutoonDB.comics_episode_3 ce left join peanutoonDB.comics cm on ce.ce_comics=cm.cm_no where cm_no > 0;");

// 수 구하기
for($o=2; $o<=5; $o++){
	${'old_sql_arr'.$o.'_ctn'} = 0;
	foreach(${'old_sql_arr'.$o} as ${'old_sql_key'.$o} => ${'old_sql_val'.$o}){
		/*
		if($o == 4){
			echo ${'old_sql_val'.$o}."<br/>";
		}
		*/
		${'row'.$o} = sql_fetch(${'old_sql_val'.$o});
		${'old_sql_arr'.$o.'_ctn'} += intval(${'row'.$o} ['old_ctn']);
	}
	array_push($old_ctn_arr, ${'old_sql_arr'.$o.'_ctn'} );
}


//////////////// 제외처리 2
//// t_episode_sequence
$old_sql_arr6 = array();
// 코믹스 번호 삭제되어서 없는 데이터 제외 처리
array_push($old_sql_arr6, "select * from peanutoonDB.comics_episode_1 ce left join peanutoonDB.comics cm on ce.ce_comics=cm.cm_no where cm_no > 0;");
array_push($old_sql_arr6, "select * from peanutoonDB.comics_episode_2 ce left join peanutoonDB.comics cm on ce.ce_comics=cm.cm_no where cm_no > 0;");
array_push($old_sql_arr6, "select * from peanutoonDB.comics_episode_3 ce left join peanutoonDB.comics cm on ce.ce_comics=cm.cm_no where cm_no > 0;");
$old_sql_arr6_ctn=0;
foreach($old_sql_arr6 as $old_sql_val6){
	$result6 = sql_query($old_sql_val6);
	while ($row6 = sql_fetch_array($result6)) {
		$ce_file_list_arr = array();
		if($row6['ce_file'] == "" || $row6['ce_file_list'] == ""){ // 둘 중 하나라면 없으면 의미가 없어서... 데이터삭제 또는 옛 데이터로 없을수 있습니다.
			continue;
		}else{
			$ce_file_list_arr = explode(",", $row6['ce_file_list']);
			$old_sql_arr6_ctn+=count($ce_file_list_arr);
		}
	}
}
array_push($old_ctn_arr, $old_sql_arr6_ctn);

//// t_file 
$old_sql_arr7 = array();
array_push($old_sql_arr7, "select count(distinct ce_cover) as old_ctn from peanutoonDB.comics_episode_1 ce left join peanutoonDB.comics cm on ce.ce_comics=cm.cm_no where cm_no > 0 AND ce_cover !='';");
array_push($old_sql_arr7, "select count(distinct ce_cover) as old_ctn from peanutoonDB.comics_episode_2 ce left join peanutoonDB.comics cm on ce.ce_comics=cm.cm_no where cm_no > 0 AND ce_cover !='';");
array_push($old_sql_arr7, "select count(distinct ce_cover) as old_ctn from peanutoonDB.comics_episode_3 ce left join peanutoonDB.comics cm on ce.ce_comics=cm.cm_no where cm_no > 0 AND ce_cover !='';");
$old_sql_arr7_ctn=0;
foreach($old_sql_arr7 as $old_sql_val7){
	$result7 = sql_query($old_sql_val7);
	while ($row7 = sql_fetch_array($result7)) {
		$row7 = sql_fetch($old_sql_val7);
		$old_sql_arr7_ctn += intval($row7['old_ctn']);
	}
}
$old_sql_arr7_ctn+= $old_ctn_arr[3]+$old_ctn_arr[4]+$old_ctn_arr[5]+$old_ctn_arr[26];
array_push($old_ctn_arr, $old_sql_arr7_ctn);


foreach($new_arr as $new_key => $new_val){
	sql_match_mark($new_key, $new_val, $new_ctn_arr[$new_key], $old_ctn_arr[$new_key]);
}

echo "<br/>";

/* ////////////////////////////////////// function ////////////////////////////////////////////// */

function sql_match_mark($no, $dbname, $new, $old){
	$color_css = "style=color:red;";
	$giho = " != ";

	if(intval($new) == intval($old)){ 
		$giho = " == ";
		$color_css = "";
	}

	echo "<div ".$color_css.">";
	echo "No".$no."-".$dbname." : ".intval($new).$giho.intval($old);
	echo "</div>";
}

?>