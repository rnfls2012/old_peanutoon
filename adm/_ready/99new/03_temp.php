<?
include_once '_common.php'; // 공통

// 데이터 있으면 작동안함
sql_data_check("peanutoon.t_temp_cm");

mysql_query("SET GLOBAL FOREIGN_KEY_CHECKS=0;"); // 외래 키 제약 조건을 전역 해제

$sql_menu_arr = array('','장르-BL','장르-GL','장르-로맨스','장르-드라마','장르-코믹','장르-성인','무료','장르-소설','장르-전체','신작','베스트');
$sql_cm_arr = array();
/*  */ array_push($sql_cm_arr, "");
/* 장르-BL */ array_push($sql_cm_arr, "SELECT c.*, crs.crs_ranking, c_prof.cp_name as prof_name FROM peanutoonDB.comics c left JOIN peanutoonDB.comics_ranking_sales_cmgenre_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 and c.cm_service = 'y' AND crs.crs_class = '1' GROUP BY c.cm_no ORDER BY crs.crs_ranking ASC;");

/* 장르-GL */ array_push($sql_cm_arr, "SELECT c.*, crs.crs_ranking, c_prof.cp_name as prof_name FROM peanutoonDB.comics c left JOIN peanutoonDB.comics_ranking_sales_cmgenre_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 and c.cm_service = 'y' AND crs.crs_class = '2' GROUP BY c.cm_no ORDER BY crs.crs_ranking ASC;");

/* 장르-로맨스 */ array_push($sql_cm_arr, "SELECT c.*, crs.crs_ranking, c_prof.cp_name as prof_name FROM peanutoonDB.comics c left JOIN peanutoonDB.comics_ranking_sales_cmgenre_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 and c.cm_service = 'y' AND crs.crs_class = '3' GROUP BY c.cm_no ORDER BY crs.crs_ranking ASC;");

/* 장르-드라마 */ array_push($sql_cm_arr, "SELECT c.*, crs.crs_ranking, c_prof.cp_name as prof_name FROM peanutoonDB.comics c left JOIN peanutoonDB.comics_ranking_sales_cmgenre_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 and c.cm_service = 'y' AND crs.crs_class = '4' GROUP BY c.cm_no ORDER BY crs.crs_ranking ASC;");

/* 장르-코믹 */ array_push($sql_cm_arr, "SELECT c.*, crs.crs_ranking, c_prof.cp_name as prof_name FROM peanutoonDB.comics c left JOIN peanutoonDB.comics_ranking_sales_cmgenre_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 and c.cm_service = 'y' AND crs.crs_class = '5' GROUP BY c.cm_no ORDER BY crs.crs_ranking ASC;");

/* 장르-성인 */ array_push($sql_cm_arr, "SELECT c.*, crs.crs_ranking, c_prof.cp_name as prof_name FROM peanutoonDB.comics c left JOIN peanutoonDB.comics_ranking_sales_cmgenre_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 and c.cm_service = 'y' AND crs.crs_class = '6' GROUP BY c.cm_no ORDER BY crs.crs_ranking ASC;");

/* 무료 */ array_push($sql_cm_arr, "SELECT c.*, crs.crs_ranking, c_prof.cp_name as prof_name FROM peanutoonDB.comics c left JOIN comics_ranking_sales_cmfree_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 AND crs.crs_class = '0' GROUP BY c.cm_no ORDER BY crs.crs_ranking ASC;");

/* 장르-소설 */ array_push($sql_cm_arr, "SELECT c.*, crs.crs_ranking, c_prof.cp_name as prof_name FROM peanutoonDB.comics c left JOIN peanutoonDB.comics_ranking_sales_cmgenre_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 and c.cm_service = 'y' AND crs.crs_class = '8' GROUP BY c.cm_no ORDER BY crs.crs_ranking ASC;");

/* 장르-전체 */ array_push($sql_cm_arr, "SELECT c.*, crs.crs_ranking, CASE WHEN c.cm_no = '2645' THEN 99 WHEN c.cm_no = '2024' THEN 98 WHEN c.cm_no = '1252' THEN 97 WHEN c.cm_no = '2659' THEN 96 WHEN c.cm_no = '805' THEN 95 WHEN c.cm_no = '1979' THEN 94 WHEN c.cm_no = '2381' THEN 93 WHEN c.cm_no = '2147' THEN 92 WHEN c.cm_no = '2322' THEN 91 WHEN c.cm_no = '3115' THEN 90 WHEN c.cm_no = '2207' THEN 89 WHEN c.cm_no = '1866' THEN 88 WHEN c.cm_no = '3009' THEN 87 WHEN c.cm_no = '3033' THEN 86 WHEN c.cm_no = '3022' THEN 85 WHEN c.cm_no = '772' THEN 84 WHEN c.cm_no = '3111' THEN 83 WHEN c.cm_no = '2851' THEN 82 WHEN c.cm_no = '2323' THEN 81 WHEN c.cm_no = '3093' THEN 80 WHEN c.cm_no = '978' THEN 79 WHEN c.cm_no = '2073' THEN 78 WHEN c.cm_no = '1308' THEN 77 WHEN c.cm_no = '1840' THEN 76 WHEN c.cm_no = '2082' THEN 75 WHEN c.cm_no = '2376' THEN 74 WHEN c.cm_no = '602' THEN 73 WHEN c.cm_no = '1165' THEN 72 WHEN c.cm_no = '2382' THEN 71 ELSE 0 END AS cm_no_special, c_prof.cp_name as prof_name FROM peanutoonDB.comics c left JOIN peanutoonDB.comics_ranking_sales_cmgenre_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 and c.cm_service = 'y' AND crs.crs_class = '0' OR c.cm_no IN (2645, 2024, 1252, 2659, 805, 1979, 2381, 2147, 2322, 3115, 2207, 1866, 3009, 3033, 3022, 772, 3111, 2851, 2323, 3093, 978, 2073, 1308, 1840, 2082, 2376, 602, 1165, 2382) GROUP BY c.cm_no ORDER BY cm_no_special DESC, crs.crs_ranking ASC;"); 

/* 신작 */ array_push($sql_cm_arr, "SELECT c.*, crs.crs_ranking, CASE WHEN c.cm_no = '3044' THEN 99 WHEN c.cm_no = '3033' THEN 98 WHEN c.cm_no = '3009' THEN 97 WHEN c.cm_no = '2941' THEN 96 WHEN c.cm_no = '2933' THEN 95 WHEN c.cm_no = '2933' THEN 94 ELSE 0 END AS cm_no_special, c_prof.cp_name as prof_name FROM peanutoonDB.comics c left JOIN peanutoonDB.comics_ranking_sales_cmnew_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 AND crs.crs_class = '0' OR c.cm_no IN (3044, 3033, 3009, 2941, 2933, 2933) GROUP BY c.cm_no ORDER BY cm_no_special DESC, crs.crs_ranking ASC;");

/* 베스트 */ array_push($sql_cm_arr, "SELECT c.*, crs.crs_ranking, c_prof.cp_name as prof_name FROM peanutoonDB.comics c left JOIN comics_ranking_sales_cmtop_auto crs ON crs.crs_comics = c.cm_no left JOIN peanutoonDB.comics_professional c_prof ON c_prof.cp_no = c.cm_professional WHERE 1 AND crs.crs_class = '0' GROUP BY c.cm_no ORDER BY crs.crs_ranking ASC;");

$sql = "SELECT idx, t_comic_idx  FROM peanutoon.t_comic_info ORDER BY t_comic_idx ";
$result = sql_query($sql);
while ($row = sql_fetch_array($result)) {
	$tmp_arr[$row['t_comic_idx']] = $row['idx'];
}

foreach($sql_cm_arr  as $sql_cm_key =>  $sql_cm_val){
	if($sql_cm_val == ""){ continue; }
	$result_cm = sql_query($sql_cm_val);
	for($i=1; $row_cm = sql_fetch_array($result_cm); $i++ ){
		// t_comic_info_idx 추가
		$row_cm['t_comic_idx'] = $row_cm['cm_no'];
		$row_cm['t_comic_info_idx'] = $tmp_arr[$row_cm['cm_no']];

		$row_cm['cm_name'] = $sql_menu_arr[$sql_cm_key];
		$row_cm['cm_code'] = $sql_cm_key;
		$row_cm['ranking'] = $i;
		$row_cm['is_over19'] = 0;
		if($row_cm['cm_adult'] == 'y'){ $row_cm['is_over19'] = 1; }
		$sql_insert = " INSERT INTO peanutoon.t_temp_cm (t_comic_idx, t_comic_info_idx, cm_name, cm_code, ranking, is_over19) 
		                VALUES ('".$row_cm['t_comic_idx']."','".$row_cm['t_comic_info_idx']."','".$row_cm['cm_name']."','".$row_cm['cm_code']."','".$row_cm['ranking']."','".$row_cm['is_over19']."');";
		sql_query($sql_insert);
	}
}
mysql_query("SET GLOBAL FOREIGN_KEY_CHECKS=1;"); // 끝나면 다시 설정
echo "03_temp.php";
?>