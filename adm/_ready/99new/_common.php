<?
	include_once '../../../_common.php'; // DB 연결
	include_once '../index.php'; // 리스트

// 1 본편, 2 외전, 3 공지, 4 특집, 5 후기, 6 휴재, 7 지연, 8 프롤로그, 9 미리보기, 10 광고, 11 패키지
// $tmp -> 공지-ce_notice / $temp -> 제목-ce_title

// $temp -> 제목-ce_title
$temp1 = $temp2 = $temp3 = $temp4 = $temp5 = $temp6 = $temp7 = $temp8 = $temp9 = $temp10 = $temp11 = array();
array_push($temp11, '1~8화 - 1권 묶음');
array_push($temp11, '전권구매 1권(1~6화)', '전권구매 2권(7~13화)', '전권구매(1~4화)', '전권구매(1~8화)', '전권구매(5~9화)');
array_push($temp6, '휴재 공지');
array_push($temp4, '크리스마스 특집' ,'설&발렌타인 특집');
array_push($temp2, '외전 1 최종화', '외전 2', '외전 2 최종화', '외전 최종화', '외전11', '외전22', '외전33');

// $tmp -> 공지-ce_notice
$tmp1 = $tmp2 = $tmp3 = $tmp4 = $tmp5 = $tmp6 = $tmp7 = $tmp8 = $tmp9 = $tmp10 = $tmp11 = array();
array_push($tmp11, '1권', '전권 구매', '전권구매', '전권구매(1~5화)', '전권구매(1~5화)', '전권구매(1~8화)', '전권구매(1~9화)', '전체 화 구매');
array_push($tmp11, '패키지 1권', '패키지 2권', '패키지 3권', '패키지 4권', '패키지 5권', '패키지 6권', '패키지 7권');
array_push($tmp11, '패키지판 (1~9화 모음)', '패키지판 (10~19화 모음)', '패키지판 (20~28화 모음)', '패키지판 (29~37화 모음)');
array_push($tmp11, '패키지판 1권', '패키지판 1권(1~6화)', '패키지판 2권', '패키지판 2권(7~11화)', '패키지판 2권(7~13화)');
array_push($tmp11, '패키지판 4권', '패키지판 5권', '패키지판 6권', '패키지판 7권', '패키지판 8권', '패키지판 9권','패티지판 3권');

array_push($tmp9, '미리보기');

array_push($tmp8, '프롤로그', '시즌2 예고', '시즌2 프롤로그', '프롤로그+19');

array_push($tmp7, '서비스 지연 공지', '서비스지연 공지', '서비스지연공지');

array_push($tmp6, '휴재안내', '휴재', '휴재 공지', '휴재 예고 공지', '휴재공지', '휴재안내', '휴재예고', '휴재예정공지');

array_push($tmp5, '1부 후기', '후기', '먹다 남은 빵', '시즌 1 후기', '시즌1 후기', '시즌1후기', '시즌2 Q&A', '독자 Q&A', '시즌2 특별편&후기', '시즌2 후기');
array_push($tmp5, '에필로그', '에필로그&2부 예고', '에필로그&작품후기', '완결 후기', '작가님 축전 모음', '작품후기', '작품후기&외전연재공지');
array_push($tmp5, '후기', '후기 및 Q&A', '후기 예고', '후기+외전 예고', '후일담1', '후일담2', '후일담3');

array_push($tmp4, '메리 크리스마스','설 특집','어린이날 특집','추석 성인 특집','추석특집', '형제 스페셜', '새해인사', '수능특집(1)', '수능특집(2)', '신년 인사');
array_push($tmp4, '특별편', 'Q&A 특별편', '여름 특집(1)', '여름 특집(2)', '여름 특집(3)', '여름특집', '재난특집');
array_push($tmp4, '추석 특별 편', '추석특별편', '추석특집', '추석특집편(1)', '추석특집편(2)', '크리스마스 특별편', '특별편', '폭염특집');
array_push($tmp4, '할로윈 특집', '형제 스페셜', '환절기특집', '메리 크리스마스');

array_push($tmp3, '필독', '2부 연재 공지', '2부 연재공지', '2부연재공지', 'EVER AFTER 외전 공지', '격주 연재 공지', '공지', '당첨자 발표');
array_push($tmp3, '독자님 축전 모음', '번외편 연재공지', '서비스 공지', '서비스 안내', '서비스공지', '시즌2 공지', '시즌2 연재공지', '시즌3 공지', '신작공지');
array_push($tmp3, '연재 공지', '연재 재개 공지', '연재공지', '연재종료 공지', '연재중단공지', '외전공지', '작가 교체 공지', '작품공지', '필독!');

array_push($tmp2, '번외편', '번외편 1화', '번외편 2화', '번외편 3화', '번외편4화', '번외편5화', '번외편6화', '번외편7화', '번외편8화(완)');
array_push($tmp2, '외전 1화', '외전 1화', '외전 2화', '외전 3화', '외전 4화', '외전 4화(완)', '외전 5화', '외전 6화', '외전 7화', '외전 최종화'); 
array_push($tmp2, '외전1화', '외전2화', '외전3화', '외전4화', '외전5화', '외전6화', '외전7화', '외전8화'); 

/* // 시즌 2  // */
$season2_cm_arr = $season2_ce_arr = array(); // 16
array_push($season2_cm_arr, 1   ,4   ,35   ,446  ,557  ,805  ,843  ,1241 ,1308 ,1365 ,1655 ,1827 ,1846 ,1979 ,2147 ,2323 );
array_push($season2_ce_arr, 4133,4292,10798,16863,11481,19210,27567,29707,21625,27225,26900,26848,29285,30197,30441,29459);

/* // 시즌 3  // */
$season3_cm_arr = $season3_ce_arr = array(); // 5
array_push($season3_cm_arr, 557  ,805  ,843  ,1308 ,1655 );
array_push($season3_ce_arr, 28502,27266,27639,27538,30638);

/* ////////////////////////////////////// function ////////////////////////////////////////////// */

function sql_insert($sql_dbname, $sql_insert_fetch_arr, $sql_insert_value_arr){

	$sql_insert = "INSERT INTO ".$sql_dbname." ( ";

	/* field명 */
	foreach($sql_insert_fetch_arr as $insert_fetch){
		$sql_insert.= $insert_fetch.', ';
	}
	$sql_insert = substr($sql_insert,0,strrpos($sql_insert, ","));

	/* VALUES 시작 */
	$sql_insert.= ' )VALUES( ';

	/* field값 */
	foreach($sql_insert_fetch_arr as $insert_fetch){
		$sql_insert.= $sql_insert_value_arr[$insert_fetch].', ';
	}
	$sql_insert = substr($sql_insert,0,strrpos($sql_insert, ","));

	/* SQL문 마무리 */
	$sql_insert.= ' ); ';

	return $sql_insert;
}

function sql_file_division($file_info){
	$file_arr = array();
	$file_path_len = 0;
	$file_info_len = strlen($file_info);

	$file_path_len = strrpos($file_info, '/');
	$file_arr[0] = substr($file_info, 0, $file_path_len+1);
	$file_arr[1] = substr($file_info, $file_path_len+1, $file_info_len);

	return $file_arr;
}

function sql_t_file_path($file_path){
	$t_file_path_idx = 0;

	// 있는 지 검색
	$sql1 = "select * from peanutoon.t_file_path where filepath='".$file_path."' ";	
	$row1 = sql_fetch($sql1);
	if(intval($row1['idx']) > 0){
		$t_file_path_idx = $row1['idx'];
	}else{
		$sql_insert1 = " INSERT INTO peanutoon.t_file_path (filepath) VALUES ('".$file_path."');";
		sql_query($sql_insert1);
		$row2 = sql_fetch($sql1);
		$t_file_path_idx = $row2['idx'];
	}
	return $t_file_path_idx;
}

function sql_t_file($t_file_path_idx, $filename){
	$t_file_idx = 0;

	// 있는 지 검색
	$sql1 = "select * from peanutoon.t_file 
	         where t_file_host_idx = 1 
			 AND t_file_path_idx=".$t_file_path_idx." 
			 AND filename='".$filename."'";
	$row1 = sql_fetch($sql1);
	if(intval($row1['idx']) > 0){
		$t_file_idx = $row1['idx'];
	}else{
		$sql_insert1 = " INSERT INTO peanutoon.t_file (t_file_host_idx,t_file_path_idx,filename) VALUES (1, ".$t_file_path_idx.", '".$filename."');";
		sql_query($sql_insert1);
		$row2 = sql_fetch($sql1);
		$t_file_idx = $row2['idx'];
	}
	return $t_file_idx;
}


function sql_t_serial_weekly_idx($cm_public_cycle){
	// 1:월, 2:화, 4:수, 8:목, 16:금, 32:토, 64:일
	$serial_weekly_name = "";
	$cm_public_cycle_int = intval($cm_public_cycle);
	switch($cm_public_cycle_int){
		case 1 : $serial_weekly_name="월";
		break;
		case 2 : $serial_weekly_name="화";
		break;
		case 4 : $serial_weekly_name="수";
		break;
		case 8 : $serial_weekly_name="목";
		break;
		case 16 : $serial_weekly_name="금";
		break;
		case 32 : $serial_weekly_name="토";
		break;
		case 64 : $serial_weekly_name="일";
		break;
	}
	return $serial_weekly_name;
}

function sql_row_comics(){
	
/* // peanutoon.t_comic_episode_file -> t_comic_info_idx 저장 */
	$tmp_arr = array();
	$sql = "SELECT idx, t_comic_idx  FROM peanutoon.t_comic_info ORDER BY t_comic_idx ";
	$result = sql_query($sql);
	while ($row = sql_fetch_array($result)) {
		$tmp_arr[$row['t_comic_idx']] = $row['idx'];
	}

	$row_comics = array(); // 코믹스 정보 저장
	$sql = "SELECT * FROM peanutoonDB.comics WHERE cm_big!=4 ORDER BY cm_no "; // 4는 셀시스뷰어이며, 서비스 종료 및 반환해서 제외 => 갯수 88개
	$result = sql_query($sql);
	while ($row = sql_fetch_array($result)) {
		// t_comic_info_idx 추가
		$row['t_comic_info_idx'] = $tmp_arr[$row['cm_no']];

		array_push($row_comics, $row);
	}
	return $row_comics;
}

function sql_row_t_comic_idx(){
	$row_t_comic_idx = array();
	$sql = "SELECT idx, t_comic_idx  FROM peanutoon.t_comic_info ORDER BY t_comic_idx ";
	$result = sql_query($sql);
	while ($row = sql_fetch_array($result)) {
		$row_t_comic_idx[$row['t_comic_idx']] = $row['idx'];
	}
	return $row_t_comic_idx;
}

function sql_data_check($sql_dbname){
	$sql_check = "select count(*) as data_ctn from ".trim($sql_dbname)." ";
	$row_check = sql_fetch($sql_check);
	if(intval($row_check['data_ctn'])>0){
		echo "<br/><br/>";
		echo $row_check['data_ctn']."개 데이터 넣었음";
		die;
	}
}

function sql_str_return($str_val){
	$str_return = "";
	$str_return = $str_val; // 이름
	$str_return = str_replace("\'", "‘", $str_return);
	$str_return = str_replace("\\'", "‘", $str_return);
	$str_return = str_replace("'", "‘", $str_return);
	$str_return = str_replace('\"', "“", $str_return);
	$str_return = str_replace('\\"', "“", $str_return);
	$str_return = str_replace('"', "“", $str_return);
	return $str_return;
}

?>