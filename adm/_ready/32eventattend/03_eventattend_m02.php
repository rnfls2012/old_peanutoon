<? include_once '_common.php'; // 공통 

// 2월 2일 로그인 회원 검색
$login_sql = "SELECT DISTINCT(slm_member) FROM stats_log_member WHERE slm_date >= '2018-02-02 20:00:00' AND slm_date < '2018-02-03 00:00:00'";
$login_result = sql_query($login_sql);
$login_arr = array();
while($login_row = sql_fetch_array($login_result)) {
	array_push($login_arr, $login_row['slm_member']);
} // end while

// 2월 2일에 출석체크 한 회원 검색
$remove_sql = "SELECT * FROM z_member_attend WHERE z_ma_member IN (".$login_sql.") AND z_ma_attend_month = '02' AND z_ma_attend_day = '02'";
$remove_result = sql_query($remove_sql);
$remove_arr = array();
while($remove_row = sql_fetch_array($remove_result)) {
	array_push($remove_arr, $remove_row['z_ma_member']);
} // end while

// 보상 받을 전체 회원 배열
$reward_arr = array_diff($login_arr, $remove_arr);

die;

// 1일 출석한 회원
$day1_attend_sql = "SELECT * FROM z_member_attend WHERE z_ma_member IN(".implode($reward_arr, ", ").") AND z_ma_attend_month = '02' AND z_ma_attend_day = '01'";
$day1_attend_result = sql_query($day1_attend_sql);
$day1_attend_arr = array();
while($day1_attend_row = sql_fetch_array($day1_attend_result)) {
	array_push($day1_attend_arr, $day1_attend_row['z_ma_member']);
} // end while

// 보상 지급
$from = "출석 보상 2 ".$nm_config['cf_point_unit_ko']." 지급(2018-02-02출석보상[관리자])";

foreach($reward_arr as $reward_key => $reward_val) {
	$mb = mb_get_no($reward_val); // 사용자 정보
	$mb['mb_age'] = mb_get_age_group($mb['mb_birth']);

	if(in_array($reward_val, $day1_attend_arr)) { // 1일 출석이 있을때
		$sql_insert = "INSERT INTO z_member_attend(z_ma_member, z_ma_id, z_ma_idx, z_ma_sex, z_ma_age, z_ma_point, z_ma_attend_year, z_ma_attend_month, z_ma_attend_day, z_ma_cont_count, z_ma_attend_cont, z_ma_attend_regu, z_ma_attend_date, z_ma_attend_save_date, z_ma_user_agent) VALUES('".$mb['mb_no']."', '".$mb['mb_id']."', '".$mb['mb_idx']."', '".$mb['mb_sex']."', '".$mb['mb_age']."', '2', '2018', '02', '02', '2', 'n', 'n', '2018-02-02', '".NM_TIME_YMDHIS."', 'admin_ready_32eventattend_03_eventattend_m02.php')";

		$day_cont = '3';
	} else { // 2일이 첫 출석일 때
		$sql_insert = "INSERT INTO z_member_attend(z_ma_member, z_ma_id, z_ma_idx, z_ma_sex, z_ma_age, z_ma_point, z_ma_attend_year, z_ma_attend_month, z_ma_attend_day, z_ma_cont_count, z_ma_attend_cont, z_ma_attend_regu, z_ma_attend_date, z_ma_attend_save_date, z_ma_user_agent) VALUES('".$mb['mb_no']."', '".$mb['mb_id']."', '".$mb['mb_idx']."', '".$mb['mb_sex']."', '".$mb['mb_age']."', '2', '2018', '02', '02', '1', 'n', 'n', '2018-02-02', '".NM_TIME_YMDHIS."', 'admin_ready_32eventattend_03_eventattend_m02.php')";

		$day_cont = '2';
	} // end else

	sql_query($sql_insert);
	cs_set_member_point_income_event($mb, "0", 2, "attend", $from, "y");

	// 3일에 출석이 있을때
	$day3_attend_sql = "SELECT COUNT(*) day3_cnt FROM z_member_attend WHERE z_ma_member = '".$mb['mb_no']."' AND z_ma_attend_month = '02' AND z_ma_attend_day = '03'";
	$day3_count = sql_count($day3_attend_sql, 'day3_cnt');
	if($day3_count > 0) {
		// 3일차 업데이트
		$day3_update_sql = "UPDATE z_member_attend SET z_ma_cont_count = '".$day_cont."' WHERE z_ma_member = '".$mb['mb_no']."' AND z_ma_attend_month = '02' AND z_ma_attend_day = '03'";
		sql_query($day3_update_sql);

		// 4일에 출석이 있을때
		$day4_attend_sql = "SELECT COUNT(*) day4_cnt FROM z_member_attend WHERE z_ma_member = '".$mb['mb_no']."' AND z_ma_attend_month = '02' AND z_ma_attend_day = '04'";
		$day4_count = sql_count($day4_attend_sql, 'day4_cnt');
		if($day4_count > 0) {
			// 4일차 업데이트
			$day_cont++;
			$day4_update_sql = "UPDATE z_member_attend SET z_ma_cont_count = '".$day_cont."' WHERE z_ma_member = '".$mb['mb_no']."' AND z_ma_attend_month = '02' AND z_ma_attend_day = '04'";
			sql_query($day4_update_sql);

			// 5일에 출석이 있을때
			$day5_attend_sql = "SELECT COUNT(*) day5_cnt FROM z_member_attend WHERE z_ma_member = '".$mb['mb_no']."' AND z_ma_attend_month = '02' AND z_ma_attend_day = '05'";
			$day5_count = sql_count($day5_attend_sql, 'day5_cnt');
			if($day5_count > 0) {
				// 5일차 업데이트
				$day_cont++;
				$day5_update_sql = "UPDATE z_member_attend SET z_ma_cont_count = '".$day_cont."' WHERE z_ma_member = '".$mb['mb_no']."' AND z_ma_attend_month = '02' AND z_ma_attend_day = '05'";
				sql_query($day5_update_sql);
			} else {
				
			} // end else
		} else {
			
		} // end else
	} else {

	} // end else
} // end foreach
?>