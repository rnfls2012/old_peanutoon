<? include_once '_common.php'; // 공통 

die;
/*
1. 우선 뺄 사람 조회
	1) 출석체크 2회해서 받아간 회원
	select * from z_member_attend where z_ma_attend_year = '2018' AND z_ma_cont_count='2';
	2) 출석체크 1회해서 받아간 회원-> 안씀!!!!!!!!!!!!!!!!!!!!!! 안씀!!!!!!!!!!!!!!!!!!!!!!
	select * from z_member_attend where z_ma_attend_year = '2018' AND z_ma_cont_count='1';
	3) 출석체크 18-0101에서 1회해서 받아간 회원
	select * from z_member_attend where z_ma_attend_year = '2018' AND z_ma_attend_day='01' AND z_ma_cont_count='1';

	4) 출석체크 18-0102에서 1회해서 받아간 회원 - 이중에 아래 18-0101 로그인 된 사람 찾아야 함
	select * from z_member_attend where z_ma_attend_year = '2018' AND z_ma_attend_day='02' AND z_ma_cont_count='1';

2. 위 조회 리스트를 제외한 나머지 회원중에 조회
	1) 회원정보에서 2018년에 로그인 된 회원
	select * from member where mb_login_date >= '2018-01-01 00:00:00';
	2) 18-0101에 로그인 사람
	select * from stats_log_member where slm_date >= '2018-01-01 00:00:00' AND slm_date < '2018-01-02 00:00:00';
	3) 18-0102에 로그인 사람
	select * from stats_log_member where slm_date >= '2018-01-02 00:00:00';
*/

$t_arr = array();
array_push($t_arr, 't1_1','t1_2','t1_3','t1_4');
array_push($t_arr, 't2_1','t2_2','t2_3');

$mb_t_arr = array();
array_push($mb_t_arr, 'z_ma_member','z_ma_member','z_ma_member','z_ma_member');
array_push($mb_t_arr, 'mb_no','slm_member','slm_member');

$t1_1_sql = " select distinct(z_ma_member) from z_member_attend where z_ma_attend_year = '2018' AND z_ma_cont_count='2'; ";
$t1_2_sql = " select distinct(z_ma_member) from z_member_attend where z_ma_attend_year = '2018' AND z_ma_cont_count='1'; ";
$t1_3_sql = " select distinct(z_ma_member) from z_member_attend where z_ma_attend_year = '2018' AND z_ma_attend_day='01' AND z_ma_cont_count='1'; ";
$t1_4_sql = " select distinct(z_ma_member) from z_member_attend where z_ma_attend_year = '2018' AND z_ma_attend_day='02' AND z_ma_cont_count='1'; ";

$t2_1_sql = " select * from member where mb_login_date >= '2018-01-01 00:00:00'; ";
$t2_2_sql = " select distinct(slm_member) from stats_log_member where slm_date >= '2018-01-01 00:00:00' AND slm_date < '2018-01-02 00:00:00'; ";
$t2_3_sql = " select distinct(slm_member) from stats_log_member where slm_date >= '2018-01-02 00:00:00'; ";

// 배열선언
foreach($t_arr as $t_val){
	${$t_val.'_arr'} = array();
}

// DB에서 데이터 가져와서 배열에 넣기
foreach($t_arr as $t_key  => $t_val){
	$mb_field = $mb_t_arr[$t_key];
	${$t_val.'_result'} = sql_query(${$t_val.'_sql'});
	while (${$t_val.'_row'} = sql_fetch_array(${$t_val.'_result'})) {
		array_push(${$t_val.'_arr'}, ${$t_val.'_row'}[$mb_field]);
	}
}

// 로그인 회원 구하기
$mb_login_1_arr = $mb_login_2_arr = array();
foreach($t2_1_arr as $t2_1_key  => $t2_1_val){
	if(ready_mb_except($t2_1_val) == false){ continue; } // 제외 회원

	if(in_array($t2_1_val, $t2_2_arr) && in_array($t2_1_val, $t2_3_arr)){ 
		// 이틀 연속 로그인
		array_push($mb_login_2_arr, $t2_1_val);
	}else if(in_array($t2_1_val, $t2_2_arr)){
		// 18-0101 만 로그인
		array_push($mb_login_1_arr, $t2_1_val);
	}
}

/* 출석 이벤트 회원 구하기 */
$mb_attend_1_arr = $mb_attend_2_arr = array();
$mb_attend_1_arr = $mb_login_1_arr;
foreach($mb_login_2_arr as $mb_login_2_val){
	if(ready_mb_except($t2_1_val) == false){ continue; } // 제외 회원-확인겸사겸사

	if(in_array($mb_login_2_val, $t1_4_arr)){
		array_push($mb_attend_2_arr, $mb_login_2_val);		
	}
}

/* 출석 이벤트 주기 */
$from = "출석 보상 2 ".$nm_config['cf_point_unit_ko']." 지급(2018-01-01출석보상[관리자])";
foreach($mb_attend_2_arr as $mb_attend_2_val){
	$mb = mb_get_no($mb_attend_2_val); // 사용자 정보	
	$mb['mb_age'] = mb_get_age_group($mb['mb_birth']);

	$sql_insert = "INSERT INTO z_member_attend(z_ma_member, z_ma_id, z_ma_idx, z_ma_sex, z_ma_age, z_ma_point, z_ma_attend_year, z_ma_attend_month, z_ma_attend_day, z_ma_cont_count, z_ma_attend_cont, z_ma_attend_regu, z_ma_attend_date, z_ma_attend_save_date, z_ma_user_agent) VALUES('".$mb['mb_no']."', '".$mb['mb_id']."', '".$mb['mb_idx']."', '".$mb['mb_sex']."', '".$mb['mb_age']."', '2', '2018', '01', '01', '1', 'n', 'n', '2018-01-01', '".NM_TIME_YMDHIS."', 'admin_ready_32eventattend_01_eventattend.php')";
	
	sql_query($sql_insert);
	cs_set_member_point_income_event($mb, "0", 2, "attend", $from, "y");
}

// 위 회원은 2일 연속이므로 기존 데이터 업데이트
$sql_update = "UPDATE z_member_attend set z_ma_cont_count='2', z_ma_user_agent='admin_ready_32eventattend_01_eventattend.php_update' 
				where z_ma_member in (".implode(",",$mb_attend_2_arr).") AND z_ma_attend_year = '2018' AND z_ma_attend_day='02' AND z_ma_cont_count='1';  ";
sql_query($sql_update);

foreach($mb_attend_1_arr as $mb_attend_1_val){
	$mb = mb_get_no($mb_attend_1_val); // 사용자 정보	
	$mb['mb_age'] = mb_get_age_group($mb['mb_birth']);

	$sql_insert = "INSERT INTO z_member_attend(z_ma_member, z_ma_id, z_ma_idx, z_ma_sex, z_ma_age, z_ma_point, z_ma_attend_year, z_ma_attend_month, z_ma_attend_day, z_ma_cont_count, z_ma_attend_cont, z_ma_attend_regu, z_ma_attend_date, z_ma_attend_save_date, z_ma_user_agent) VALUES('".$mb['mb_no']."', '".$mb['mb_id']."', '".$mb['mb_idx']."', '".$mb['mb_sex']."', '".$mb['mb_age']."', '2', '2018', '01', '01', '1', 'n', 'n', '2018-01-01', '".NM_TIME_YMDHIS."', 'admin_ready_32eventattend_01_eventattend.php')";
	
	sql_query($sql_insert);
	cs_set_member_point_income_event($mb, "0", 2, "attend", $from, "y");
}



function ready_mb_except($mb_no){
	global $t1_1_arr, $t1_3_arr;

	$boolean = true;

	// 1. 1) 출석체크 2회해서 받아간 회원
	if(in_array($mb_no, $t1_1_arr)){ $boolean = false; }
	// 1. 3) 출석체크 18-0101에서 1회해서 받아간 회원
	if(in_array($mb_no, $t1_3_arr)){ $boolean = false; }

	return $boolean;
}

?>