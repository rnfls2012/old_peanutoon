<?
include_once '_common.php'; // 공통

$table_create = array(); /* array(테이블명, 테이블 기본키 ) */

/*  nm_uniqid
	유일한 키 만드는 테이블
    사용하는 곳 :
    1. 글쓰기시 미리 유일키를 얻어 파일 업로드 필드에 넣는다.
    2. 주문번호 생성시에 사용한다.
    3. 기타 유일키가 필요한 곳에서 사용한다.
	4. 어느기간 지날시 자동삭제 기능을 넣어준다.
*/
$t_nm_uniqid = "
CREATE TABLE IF NOT EXISTS `nm_uniqid` (
  `uq_no` INT(22) NOT NULL AUTO_INCREMENT,
  `uq_id` bigINT(20) unsigned NOT NULL  COMMENT '고유ID',
  `uq_ip` VARCHAR(50) NOT NULL COMMENT 'IP',
  `uq_year` VARCHAR(4) NOT NULL COMMENT '연도',
  `uq_month` VARCHAR(2) NOT NULL COMMENT '월',
  PRIMARY KEY (`uq_no`,`uq_id`),
  KEY `uq_year` (`uq_year`),
  KEY `uq_month` (`uq_month`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('nm_uniqid','uq_no'));

/* 설정시 긴글들 */
$t_config_textarea = "
CREATE TABLE IF NOT EXISTS `config_textarea` (
  `cf_no` INT(11) NOT NULL AUTO_INCREMENT,
  `cf_terms` text NOT NULL COMMENT '이용약관',
  `cf_pinfo` text NOT NULL COMMENT '개인정보취급방침',
  `cf_ipin` VARCHAR(300) NOT NULL COMMENT '본인인증',
  `cf_mb_leave` VARCHAR(300) NOT NULL COMMENT '회원탈퇴시', 
  PRIMARY KEY (`cf_no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
";
array_push($table_create, array('config_textarea','cf_no'));

/* 컨텐츠 키워드 */
$t_comics_keyword = "
CREATE TABLE IF NOT EXISTS `comics_keyword` (
  `ck_id` VARCHAR(10) NOT NULL DEFAULT '0' COMMENT '키워드고유ID',
  `ck_name` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '키워드명',
  `ck_adult` INT(2) NOT NULL DEFAULT '0' COMMENT '0_청소년_1_성인',
  `ck_order` INT(5) NOT NULL DEFAULT '100' COMMENT '키워드순서_숫자가 작을 수록 상위',
  PRIMARY KEY (`ck_id`),
  KEY `ck_order` (`ck_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('comics_keyword','ck_id'));

/* 출석체크 날짜설정 */
$t_event_att_month = "
CREATE TABLE IF NOT EXISTS `event_att_month` (
  `eam_id` INT(3) NOT NULL COMMENT '월',
  `eam_use` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '사용여부_y사용',
  `eam_end_day` INT(2) NOT NULL DEFAULT '0' COMMENT '기간_몇일까지로 사용',
  `eam_title` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '이벤트명',
  PRIMARY KEY (`eam_id`),
  KEY `eam_use` (`eam_use`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('event_att_month','eam_id'));

/* 출석체크 코인설정 */
$t_event_att_day = "
CREATE TABLE IF NOT EXISTS `event_att_day` (
  `ead_id` INT(3) NOT NULL COMMENT '일',
  `ead_cash_point` INT(6) NOT NULL DEFAULT '0' COMMENT '지급 코인',
  `ead_point` INT(6) NOT NULL DEFAULT '0' COMMENT '지급 보너스 코인',
  PRIMARY KEY (`ead_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('event_att_day','ead_id'));

/* 충전시 추가이벤트 */
$t_event_recharge = "
CREATE TABLE IF NOT EXISTS `event_recharge` (
  `er_id` INT(11) NOT NULL AUTO_INCREMENT,
  `er_type` INT(4) NOT NULL DEFAULT '0' COMMENT '이벤트타입',
  `er_date_use` INT(2) NOT NULL DEFAULT '0' COMMENT '0_기간있음_1_기간없음',
  `er_s_date` VARCHAR(20) NOT NULL COMMENT '이벤트시작일',
  `er_e_date` VARCHAR(20) NOT NULL COMMENT '이벤트마감일',
  `er_state` VARCHAR(2) NOT NULL COMMENT '이벤트상태_r_예약중_s_진행중_e_종료',
  `er_calc_way` VARCHAR(2) NOT NULL COMMENT '이벤트 지급 계산법(a_덧셈_p_퍼센트로덧셈_m_곱셉)',
  `er_cash_point` INT(11) NOT NULL DEFAULT '0' COMMENT '이벤트 지급 코인',
  `er_point` INT(11) NOT NULL DEFAULT '0' COMMENT '이벤트 지급 보너스 코인',
  `er_reg_date` datetime DEFAULT NULL COMMENT '등록일',
  `er_mod_date` datetime DEFAULT NULL COMMENT '수정일',
  PRIMARY KEY (`er_id`),
  KEY `er_type` (`er_type`),
  KEY `er_state` (`er_state`),
  KEY `er_calc_way` (`er_calc_way`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('event_recharge','er_id'));

/* 무료&코인이벤트 - 화리스트 */
$t_event_alist = "
CREATE TABLE IF NOT EXISTS `event_alist` ( 
  `eal_id` INT(11) NOT NULL AUTO_INCREMENT, 
  `eal_eventnum` INT(11) NOT NULL DEFAULT '0' COMMENT '이벤트번호', 
  `eal_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호', 

  `eal_free` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '무료_n_유료_y_무료임',
  `eal_free_s` INT(5) NOT NULL DEFAULT '1' COMMENT '무료_적용_화수_시작',
  `eal_free_e` INT(5) NOT NULL DEFAULT '0' COMMENT '무료_적용_화수_끝',
  
  `eal_sale` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '세일_n_안함_y_적용',
  `eal_sale_pay` INT(11) NOT NULL DEFAULT '0' COMMENT '세일금액',
  `eal_sale_s` INT(5) NOT NULL DEFAULT '1' COMMENT '세일_적용_화수_시작',
  `eal_sale_e` INT(5) NOT NULL DEFAULT '0' COMMENT '세일_적용_화수_끝',

  `eal_save_date` datetime DEFAULT NULL COMMENT '저장일',
  PRIMARY KEY (`eal_id`),
  KEY `eal_eventnum` (`eal_eventnum`),
  KEY `eal_comics` (`eal_comics`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('event_alist','eal_id'));

/* 매출 충전 통계 */
$t_sales_recharge = "
CREATE TABLE IF NOT EXISTS `sales_recharge` (
  `sr_no` INT(11) NOT NULL AUTO_INCREMENT,
  `sr_id` VARCHAR(30) NOT NULL COMMENT '회원ID',
  `sr_id_idx` VARCHAR(2) NOT NULL COMMENT '회원ID 인덱스',
  `sr_date` VARCHAR(20) NOT NULL COMMENT '원본 날짜',
  `sr_year_month` VARCHAR(8) NOT NULL COMMENT '연도_월',
  `sr_year` VARCHAR(5) NOT NULL COMMENT '연도',
  `sr_month` VARCHAR(3) NOT NULL COMMENT '월',
  `sr_day` VARCHAR(3) NOT NULL COMMENT '일',
  `sr_hour` VARCHAR(3) NOT NULL COMMENT '시',
  `sr_min` VARCHAR(3) NOT NULL COMMENT '분',
  `sr_week` VARCHAR(3) NOT NULL COMMENT '요일(0_일_1_월_2_화_3_수_4_목_5_금_6_토)',

  `sr_product_class` VARCHAR(10) NOT NULL COMMENT '결제상품종류',
  `sr_event` INT(2) NOT NULL DEFAULT '0' COMMENT '이벤트여부(0_아님_1_이벤트)',
  `sr_way` VARCHAR(10) NOT NULL COMMENT '결제수단',
  `sr_platform` VARCHAR(10) NOT NULL COMMENT '결제환경',
  `sr_point` INT(11) NOT NULL COMMENT '충전 보너스코인',
  `sr_cash_point` INT(11) NOT NULL COMMENT '충전 코인',
  `sr_pay` INT(11) NOT NULL COMMENT '결제금액',
  `sr_re_pay` INT(2) NOT NULL DEFAULT '0' COMMENT '재구매여부(0_아님_1_재구매)',
  `sr_sex` VARCHAR(3) NOT NULL COMMENT '성별(M_남자_Y_여자_빈칸_미인증)',
  `sr_age` INT(5) NOT NULL DEFAULT '0' COMMENT '나이',
  `sr_adult` VARCHAR(3) NOT NULL COMMENT '성인여부(n_청소년_y_성인_빈칸_미인증)',
  `sr_save_date` datetime DEFAULT NULL COMMENT '저장일',

  PRIMARY KEY (`sr_no`),
  KEY `sr_id_idx` (`sr_id_idx`),
  KEY `sr_year_month` (`sr_year_month`),
  KEY `sr_year` (`sr_year`),
  KEY `sr_month` (`sr_month`),
  KEY `sr_day` (`sr_day`),
  KEY `sr_hour` (`sr_hour`),
  KEY `sr_min` (`sr_min`),
  KEY `sr_week` (`sr_week`),

  KEY `sr_product_class` (`sr_product_class`),
  KEY `sr_event` (`sr_event`),
  KEY `sr_way` (`sr_way`),
  KEY `sr_platform` (`sr_platform`),
  KEY `sr_pay` (`sr_pay`),
  KEY `sr_re_pay` (`sr_re_pay`),
  KEY `sr_sex` (`sr_sex`),
  KEY `sr_age` (`sr_age`),
  KEY `sr_adult` (`sr_adult`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('sales_recharge','sr_no'));

/* 정산 단위 */
$t_config_unit_pay = "
CREATE TABLE IF NOT EXISTS `config_unit_pay` (
  `cup_tpye` INT(11) NOT NULL AUTO_INCREMENT,
  `cup_won` INT(11) NOT NULL COMMENT '정산금액',
  `cup_id` VARCHAR(30) NOT NULL COMMENT '회원ID',
  `cup_save_date` datetime DEFAULT NULL COMMENT '저장일',

  PRIMARY KEY (`cup_tpye`),
  UNIQUE KEY `cup_won` (`cup_won`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('config_unit_pay','cup_tpye'));

/* 매출 코믹스 통계 -> 기본 엠짱 통계는 삭제 */
$t_sales =  "
CREATE TABLE IF NOT EXISTS `sales` (
  `s_no` INT(11) NOT NULL AUTO_INCREMENT,
  `s_comics` INT(11) NOT NULL COMMENT '코믹스번호',
  `s_big` INT(11) NOT NULL COMMENT '코믹스분류',
  `s_year_month` VARCHAR(8) NOT NULL COMMENT '연도_월',
  `s_year` VARCHAR(5) NOT NULL COMMENT '연도',
  `s_month` VARCHAR(3) NOT NULL COMMENT '월',
  `s_day` VARCHAR(3) NOT NULL COMMENT '일',
  `s_hour` VARCHAR(3) NOT NULL COMMENT '시',
  `s_week` VARCHAR(3) NOT NULL COMMENT '요일(0_일_1_월_2_화_3_수_4_목_5_금_6_토)',

  `s_open` INT(5) unsigned NOT NULL COMMENT '오픈view횟수',
  `s_free` INT(5) unsigned NOT NULL COMMENT '무료view횟수',
  `s_sale` INT(5) unsigned NOT NULL COMMENT '세일view횟수',
  `s_all_pay` INT(5) unsigned NOT NULL COMMENT '전체구매횟수',
  `s_pay` INT(5) unsigned NOT NULL COMMENT '개별구매횟수',
  `s_event` INT(2) NOT NULL DEFAULT '0' COMMENT '이벤트여부(0_아님_1_이벤트)',
  `s_point` INT(11) NOT NULL DEFAULT '0' COMMENT '매출 보너스코인',
  `s_cash_point` INT(11) NOT NULL DEFAULT '0' COMMENT '매출 코인',
  `s_cash_type` INT(2) NOT NULL DEFAULT '0' COMMENT '정산금액타입',

  PRIMARY KEY (`s_no`),
  KEY `s_comics` (`s_comics`),
  KEY `s_big` (`s_big`),
  KEY `s_year_month` (`s_year_month`),
  KEY `s_year` (`s_year`),
  KEY `s_month` (`s_month`),
  KEY `s_day` (`s_day`),
  KEY `s_hour` (`s_hour`),
  KEY `s_week` (`s_week`),

  KEY `s_event` (`s_event`),
  KEY `s_cash_type` (`s_cash_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('sales','s_no'));

/* 매출 코믹스 나이, 성별 통계 */
$t_sales_age_sex =  "
CREATE TABLE IF NOT EXISTS `sales_age_sex` (
  `sas_no` INT(11) NOT NULL AUTO_INCREMENT,
  `sas_comics` INT(11) NOT NULL COMMENT '코믹스번호',
  `sas_big` INT(11) NOT NULL COMMENT '코믹스분류',
  `sas_year_month` VARCHAR(8) NOT NULL COMMENT '연도_월',
  `sas_year` VARCHAR(5) NOT NULL COMMENT '연도',
  `sas_month` VARCHAR(3) NOT NULL COMMENT '월',
  `sas_day` VARCHAR(3) NOT NULL COMMENT '일',
  `sas_hour` VARCHAR(3) NOT NULL COMMENT '시',
  `sas_week` VARCHAR(3) NOT NULL COMMENT '요일(0_일_1_월_2_화_3_수_4_목_5_금_6_토)',

  `sas_man` INT(5) unsigned NOT NULL COMMENT '남자구매횟수',
  `sas_woman` INT(5) unsigned NOT NULL COMMENT '여자구매횟수',
  `sas_no_certify` INT(5) unsigned NOT NULL COMMENT '미인증구매횟수',

  `sas_man10` INT(5) unsigned NOT NULL COMMENT '남자10구매횟수',
  `sas_man20` INT(5) unsigned NOT NULL COMMENT '남자20구매횟수',
  `sas_man30` INT(5) unsigned NOT NULL COMMENT '남자30구매횟수',
  `sas_man40` INT(5) unsigned NOT NULL COMMENT '남자40구매횟수',
  `sas_man50` INT(5) unsigned NOT NULL COMMENT '남자50구매횟수',
  `sas_man60` INT(5) unsigned NOT NULL COMMENT '남자60구매횟수',

  `sas_woman10` INT(5) unsigned NOT NULL COMMENT '여자10구매횟수',
  `sas_woman20` INT(5) unsigned NOT NULL COMMENT '여자20구매횟수',
  `sas_woman30` INT(5) unsigned NOT NULL COMMENT '여자30구매횟수',
  `sas_woman40` INT(5) unsigned NOT NULL COMMENT '여자40구매횟수',
  `sas_woman50` INT(5) unsigned NOT NULL COMMENT '여자50구매횟수',
  `sas_woman60` INT(5) unsigned NOT NULL COMMENT '여자60구매횟수',


  PRIMARY KEY (`sas_no`),
  KEY `sas_comics` (`sas_comics`),
  KEY `sas_big` (`sas_big`),
  KEY `sas_year_month` (`sas_year_month`),
  KEY `sas_year` (`sas_year`),
  KEY `sas_month` (`sas_month`),
  KEY `sas_day` (`sas_day`),
  KEY `sas_hour` (`sas_hour`),
  KEY `sas_week` (`sas_week`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('sales_age_sex','sas_no'));

/* 매출 코믹스 화-단행본별 통계 -> 단행본웹툰판, 웹툰은 복사 04_table_copy.php 처리 */
$t_sales_content = "
CREATE TABLE IF NOT EXISTS `sales_content` (
  `sc_no` INT(11) NOT NULL AUTO_INCREMENT,
  `sc_comics` INT(11) NOT NULL COMMENT '코믹스번호',
  `sc_content_no` INT(11) NOT NULL COMMENT '컨텐츠 db번호',
  `sc_content_num` INT(11) NOT NULL COMMENT '컨텐츠 신번호',
  `sc_big` INT(11) NOT NULL COMMENT '코믹스분류',
  `sc_year_month` VARCHAR(8) NOT NULL COMMENT '연도_월',
  `sc_year` VARCHAR(5) NOT NULL COMMENT '연도',
  `sc_month` VARCHAR(3) NOT NULL COMMENT '월',
  `sc_day` VARCHAR(3) NOT NULL COMMENT '일',
  `sc_hour` VARCHAR(3) NOT NULL COMMENT '시',
  `sc_week` VARCHAR(3) NOT NULL COMMENT '요일(0_일_1_월_2_화_3_수_4_목_5_금_6_토)',

  `sc_open` INT(5) unsigned NOT NULL COMMENT '오픈view횟수',
  `sc_free` INT(5) unsigned NOT NULL COMMENT '무료view횟수',
  `sc_sale` INT(5) unsigned NOT NULL COMMENT '세일view횟수',
  `sc_all_pay` INT(5) unsigned NOT NULL COMMENT '전체구매횟수',
  `sc_pay` INT(5) unsigned NOT NULL COMMENT '개별구매횟수',
  `sc_event` INT(2) NOT NULL DEFAULT '0' COMMENT '이벤트여부(0_아님_1_이벤트)',
  `sc_point` INT(11) NOT NULL DEFAULT '0' COMMENT '매출 보너스코인',
  `sc_cash_point` INT(11) NOT NULL DEFAULT '0' COMMENT '매출 코인',
  `sc_cash_type` INT(2) NOT NULL DEFAULT '0' COMMENT '정산금액타입',

  PRIMARY KEY (`sc_no`),
  KEY `sc_comics` (`sc_comics`),
  KEY `sc_content_no` (`sc_content_no`),
  KEY `sc_content_num` (`sc_content_num`),
  KEY `sc_big` (`sc_big`),
  KEY `sc_year_month` (`sc_year_month`),
  KEY `sc_year` (`sc_year`),
  KEY `sc_month` (`sc_month`),
  KEY `sc_day` (`sc_day`),
  KEY `sc_hour` (`sc_hour`),
  KEY `sc_week` (`sc_week`),

  KEY `sc_event` (`sc_event`),
  KEY `sc_cash_type` (`sc_cash_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('sales_content','sc_no'));

/* 매출 코믹스 화-단행본별 나이, 성별 통계 -> 단행본웹툰판, 웹툰은 복사 04_table_copy.php 처리 */
$t_sales_content_age_sex = "
CREATE TABLE IF NOT EXISTS `sales_content_age_sex` (
  `scas_no` INT(11) NOT NULL AUTO_INCREMENT,
  `scas_comics` INT(11) NOT NULL COMMENT '코믹스번호',
  `scas_content_no` INT(11) NOT NULL COMMENT '컨텐츠 db번호',
  `scas_content_num` INT(11) NOT NULL COMMENT '컨텐츠 신번호',
  `scas_big` INT(11) NOT NULL COMMENT '코믹스분류',
  `scas_year_month` VARCHAR(8) NOT NULL COMMENT '연도_월',
  `scas_year` VARCHAR(5) NOT NULL COMMENT '연도',
  `scas_month` VARCHAR(3) NOT NULL COMMENT '월',
  `scas_day` VARCHAR(3) NOT NULL COMMENT '일',
  `scas_hour` VARCHAR(3) NOT NULL COMMENT '시',
  `scas_week` VARCHAR(3) NOT NULL COMMENT '요일(0_일_1_월_2_화_3_수_4_목_5_금_6_토)',

  `scas_man` INT(5) unsigned NOT NULL COMMENT '남자구매횟수',
  `scas_woman` INT(5) unsigned NOT NULL COMMENT '여자구매횟수',
  `scas_no_certify` INT(5) unsigned NOT NULL COMMENT '미인증구매횟수',

  `scas_man10` INT(5) unsigned NOT NULL COMMENT '남자10구매횟수',
  `scas_man20` INT(5) unsigned NOT NULL COMMENT '남자20구매횟수',
  `scas_man30` INT(5) unsigned NOT NULL COMMENT '남자30구매횟수',
  `scas_man40` INT(5) unsigned NOT NULL COMMENT '남자40구매횟수',
  `scas_man50` INT(5) unsigned NOT NULL COMMENT '남자50구매횟수',
  `scas_man60` INT(5) unsigned NOT NULL COMMENT '남자60구매횟수',

  `scas_woman10` INT(5) unsigned NOT NULL COMMENT '여자10구매횟수',
  `scas_woman20` INT(5) unsigned NOT NULL COMMENT '여자20구매횟수',
  `scas_woman30` INT(5) unsigned NOT NULL COMMENT '여자30구매횟수',
  `scas_woman40` INT(5) unsigned NOT NULL COMMENT '여자40구매횟수',
  `scas_woman50` INT(5) unsigned NOT NULL COMMENT '여자50구매횟수',
  `scas_woman60` INT(5) unsigned NOT NULL COMMENT '여자60구매횟수',


  PRIMARY KEY (`scas_no`),
  KEY `scas_comics` (`scas_comics`),
  KEY `scas_big` (`scas_big`),
  KEY `scas_year_month` (`scas_year_month`),
  KEY `scas_year` (`scas_year`),
  KEY `scas_month` (`scas_month`),
  KEY `scas_day` (`scas_day`),
  KEY `scas_hour` (`scas_hour`),
  KEY `scas_week` (`scas_week`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('sales_content_age_sex','scas_no'));

/* 매출 코믹스 화-단행본별 나이, 성별 통계 -> 단행본웹툰판, 웹툰은 복사 04_table_copy.php 처리 */
$t_member_buy_content = "
CREATE TABLE IF NOT EXISTS `member_buy_content` (
  `mbc_no` INT(11) NOT NULL AUTO_INCREMENT,
  `mbc_id` VARCHAR(30) NOT NULL COMMENT '회원ID',
  `mbc_id_idx` VARCHAR(2) NOT NULL COMMENT '회원ID 인덱스',
  `mbc_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '구매 코믹스번호',
  `mbc_content_no` INT(11) NOT NULL DEFAULT '0' COMMENT '구매 컨텐츠 db번호',
  `mbc_content_num` INT(11) NOT NULL DEFAULT '0' COMMENT '구매 컨텐츠 신 번호',

  `mbc_way_type` INT(2) NOT NULL DEFAULT '0' COMMENT '구매방법(전체구매7/개별1)',
  `mbc_sojang_type` INT(2) NOT NULL DEFAULT '0' COMMENT '소장여부(0_무료_1_소장_2_대여)',
  `mbc_view_type` INT(2) NOT NULL DEFAULT '0' COMMENT '구독여부(0_보임_1_안봄)',
  `mbc_date` VARCHAR(20) NOT NULL COMMENT '구매날짜',
  `mbc_end_date` VARCHAR(20) NOT NULL COMMENT '구매보유_유효날짜',
  PRIMARY KEY (`mbc_no`),
  KEY `mbc_id` (`mbc_id`), 
  KEY `mbc_id_idx` (`mbc_id_idx`), 
  KEY `mbc_comics` (`mbc_comics`),
  KEY `mbc_content_no` (`mbc_content_no`),
  KEY `mbc_content_num` (`mbc_content_num`), 

  KEY `mbc_way_type` (`mbc_way_type`), 
  KEY `mbc_sojang_type` (`mbc_sojang_type`), 
  KEY `mbc_view_type` (`mbc_view_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('member_buy_content','mbc_no'));

/* 관리자가 CMS 메뉴 권한 등급별 권한 지정 */
$t_admin_cms_access = "
CREATE TABLE IF NOT EXISTS `admin_cms_access` (
  `aca_no` INT(11) NOT NULL AUTO_INCREMENT,
  `aca_member_class` VARCHAR(2) NOT NULL COMMENT '회원등급_c_고객_p_파트너_a_관리자',
  `aca_member_level` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '회원레벨',
  `aca_class_permission` VARCHAR(255) NOT NULL COMMENT '회원등급 권한_메인메뉴',
  `aca_level_permission` VARCHAR(255) NOT NULL COMMENT '회원레벨 권한_서브메뉴',
  `aca_level_premium` VARCHAR(30) NOT NULL COMMENT '회원레벨 혜택',
  `aca_save_date` datetime DEFAULT NULL COMMENT '저장일',

  PRIMARY KEY (`aca_no`),
  KEY `aca_member_class` (`aca_member_class`), 
  KEY `aca_member_level` (`aca_member_level`), 
  KEY `aca_class_permission` (`aca_class_permission`), 
  KEY `aca_level_permission` (`aca_level_permission`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('admin_cms_access','aca_no'));

/* CMS 이용자의 등록,수정,삭제, 다운로드 로그 */
$t_admin_cms_log = "
CREATE TABLE IF NOT EXISTS `admin_cms_log` (
  `acl_no` INT(11) NOT NULL AUTO_INCREMENT,
  `acl_member_id` VARCHAR(30) NOT NULL COMMENT '회원ID',
  `acl_member_id_idx` VARCHAR(2) NOT NULL COMMENT '회원ID 인덱스',
  `acl_member_level` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '회원레벨',
  `acl_file_name` VARCHAR(30) NOT NULL COMMENT '파일명',
  `acl_event` VARCHAR(2) NOT NULL COMMENT 'i_등록_d_삭제_u_업데이트',
  `acl_sql` VARCHAR(30) NOT NULL COMMENT 'SQL커리문',
  `acl_year_month` VARCHAR(8) NOT NULL COMMENT '연도_월',
  `acl_year` VARCHAR(5) NOT NULL COMMENT '연도',
  `acl_month` VARCHAR(3) NOT NULL COMMENT '월',
  `acl_day` VARCHAR(3) NOT NULL COMMENT '일',
  `acl_hour` VARCHAR(3) NOT NULL COMMENT '시',
  `acl_save_date` datetime DEFAULT NULL COMMENT '저장일',

  PRIMARY KEY (`acl_no`),
  KEY `acl_member_id` (`acl_member_id`), 
  KEY `acl_member_id_idx` (`acl_member_id_idx`), 
  KEY `acl_member_level` (`acl_member_level`), 
  KEY `acl_file_name` (`acl_file_name`), 
  KEY `acl_event` (`acl_event`), 
  KEY `acl_year_month` (`acl_year_month`), 
  KEY `acl_year` (`acl_year`), 
  KEY `acl_month` (`acl_month`), 
  KEY `acl_day` (`acl_day`), 
  KEY `acl_hour` (`acl_hour`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('admin_cms_log','acl_no'));

/* 컨텐츠 메뉴 */
$t_comics_menu = "
CREATE TABLE IF NOT EXISTS `comics_menu` (
  `cm_id` INT(11) NOT NULL AUTO_INCREMENT,
  `cm_code` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '메뉴코드',
  `cm_name` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '메뉴명',
  `cm_link` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '링크',
  `cm_target` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '링크연결시속성',
  `cm_order` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '정렬순서',
  `cm_adult` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '메뉴성인(0_청소년_1_성인)',
  `cr_use` VARCHAR(6) NOT NULL DEFAULT 'y' COMMENT '사용_y_사용안함_n_웹만사용_weby_모바일만사용_moby',
  `cm_save_date` datetime DEFAULT NULL COMMENT '저장일',
  PRIMARY KEY (`cm_id`),
  KEY `cm_order` (`cm_order`), 
  KEY `cm_adult` (`cm_adult`), 
  KEY `cr_use` (`cr_use`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('comics_menu','cm_id'));

/* 컨텐츠 순위 */
$t_comics_ranking = "
CREATE TABLE IF NOT EXISTS `comics_ranking` (
  `cr_id` INT(11) NOT NULL AUTO_INCREMENT,
  `cr_class` VARCHAR(4) NOT NULL DEFAULT '' COMMENT 'ho_실시간_da_일간_we_주간_co_완결_bo_단행본_wt_웹툰_is',
  `cr_lock` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '고정여부(0_고정안함_1_고정)',
  `cr_adult` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '메뉴성인(0_청소년_1_성인)',
  `cr_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호', 
  `cr_big` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스대분류', 
  `cr_ranking` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '순위',
  `cr_teens_ranking` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '청소년순위',
  `cr_prev_ranking` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '이전순위',
  `cr_save_date` datetime DEFAULT NULL COMMENT '저장일',
  PRIMARY KEY (`cr_id`),
  KEY `cr_class` (`cr_class`), 
  KEY `cr_lock` (`cr_lock`), 
  KEY `cr_adult` (`cr_adult`), 
  KEY `cr_comics` (`cr_comics`),
  KEY `cr_big` (`cr_big`), 
  KEY `cr_ranking` (`cr_ranking`), 
  KEY `cr_teens_ranking` (`cr_teens_ranking`), 
  KEY `cr_prev_ranking` (`cr_prev_ranking`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('comics_ranking','cr_id'));

/* 컨텐츠 급상승 리스트 */
$t_comics_issue = "
CREATE TABLE IF NOT EXISTS `comics_issue` (
  `ci_id` INT(11) NOT NULL AUTO_INCREMENT,
  `ci_lock` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '고정여부(0_고정안함_1_고정)',
  `ci_adult` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '메뉴성인(0_청소년_1_성인)',
  `ci_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호', 
  `ci_big` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스대분류', 
  `ci_ranking` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '순위',
  `ci_teens_ranking` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '청소년순위',
  `ci_prev_ranking` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '이전순위',
  `ci_save_date` datetime DEFAULT NULL COMMENT '저장일',
  PRIMARY KEY (`ci_id`),
  KEY `ci_class` (`ci_class`), 
  KEY `ci_lock` (`ci_lock`), 
  KEY `ci_adult` (`ci_adult`), 
  KEY `ci_comics` (`ci_comics`), 
  KEY `ci_big` (`ci_big`), 
  KEY `ci_ranking` (`ci_ranking`), 
  KEY `ci_teens_ranking` (`ci_teens_ranking`), 
  KEY `ci_prev_ranking` (`ci_prev_ranking`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('comics_issue','ci_id'));

/* 컨텐츠 추천문구 */
$t_comics_recommend_word = "
CREATE TABLE IF NOT EXISTS `comics_recommend_word` (
  `crw_id` INT(11) NOT NULL AUTO_INCREMENT,
  `crw_adult` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '성인여부(0_청소년_1_성인)',
  `crw_title` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '추천단어',
  `crw_save_date` datetime DEFAULT NULL COMMENT '저장일',
  PRIMARY KEY (`crw_id`),
  KEY `crw_adult` (`crw_adult`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('comics_recommend_word','crw_id'));

/* 충전시 가격 */
$t_config_price= "
CREATE TABLE IF NOT EXISTS `config_price` (
  `cp_id` INT(11) NOT NULL AUTO_INCREMENT,
  `cp_cash_point` INT(11) NOT NULL DEFAULT '0' COMMENT '충전 코인', 
  `cp_point` INT(11) NOT NULL DEFAULT '0' COMMENT '충전 보너스 코인', 
  `cp_point_percent` INT(4) NOT NULL DEFAULT '0' COMMENT '충전 추가 보너스 코인 퍼센트', 
  `cp_won` INT(11) NOT NULL DEFAULT '0' COMMENT '충전 가격', 
  `cp_payway_class` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '충전시 결제수단제약_빈칸_제약없음_ph_card_virtual_toss', 
  `cp_member_cash_num_event` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '이벤트 충전코인_해당숫자 충전시 보여지는 충전가격',
  `cp_member_cash_num_event_title` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '이벤트 충전코인 안내문',
  `cp_save_date` datetime DEFAULT NULL COMMENT '저장일',
  PRIMARY KEY (`cp_id`),
  KEY `cp_cash_point` (`cp_cash_point`), 
  KEY `cp_member_cash_num_event` (`cp_member_cash_num_event`), 
  KEY `cp_point` (`cp_point`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('config_price','cp_id'));


/* 위 충전시, 날짜타입, 날짜시작, 끝날짜 넣기 

	event_allbuy_point				화 전체 결제시 추가 보너스설정
	event_coupon					쿠폰

	epromotion						프로모션 리스트
	epromotion_place				프로모션 위치 설정
	epromotion_banner				프로모션 배너 설정
	epromotion_popup				프로모션 팝업 설정

	member_point_expen_content		회원 코믹스 구입 코믹스 화 단행본리스트
	member_point_expen_content_bwt	회원 코믹스 구입 코믹스 화 단행본웹툰판리스트
	member_point_expen_content_wt	회원 코믹스 구입 코믹스 화 웹툰리스트

	stats_content					코믹스 순위 통계

	stats_ui_main					UI_메인화면_통계
	stats_ui_comics					UI_코믹스화면_통계
	stats_ui_comics_content			UI_코믹스-화리스트화면_통계
	stats_ui_board					UI_게시판화면_통계
	stats_ui_event					UI_이벤트화면_통계
	stats_ui_banner					UI_배너화면_통계


	stats_log						로그

	필요함 - 위에 하면서 만들어야 할것 같음

*/



/* /////////////////////////// 생성 및 데이터 초기화 /////////////////////////// */


foreach($table_create as $t_create_key => $t_create_val){
	/* 테이블 */
	$sql_select = " SELECT ".$t_create_val[1]." FROM ".$t_create_val[0]." LIMIT 0, 1 ; ";
	$sql_delete = " DELETE FROM ".$t_create_val[0]." ; ";
	$sql_auto_increment = "ALTER TABLE ".$t_create_val[0]." AUTO_INCREMENT =1 ; ";
	
	echo "/////////////////////////// ".$t_create_val[0]." /////////////////////////// <br/>";
	echo $sql_select."<br/>";
	if(!sql_query($sql_select)) {
		sql_query(${'t_'.$t_create_val[0]});
		echo ${'t_'.$t_create_val[0]}."<br/>";
	}else{
		sql_query($sql_delete);
		echo $sql_delete."<br/>";
		sql_query($sql_auto_increment);
		echo $sql_auto_increment."<br/>";
	}
	
	echo "<br/><br/>";
	
}

?>