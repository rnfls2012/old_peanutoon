<?
include_once '_common.php'; // 공통

$rename = array(); /* array(엠짱 원래 이름, 수정될 이름) */
array_push($rename, array('faq','board_faq'));
array_push($rename, array('mentomen','board_mentomen'));
array_push($rename, array('review','board_error_report'));
array_push($rename, array('topic','board_topic'));
array_push($rename, array('fileupload','comics'));
array_push($rename, array('content','comics_content'));
array_push($rename, array('fromto','comics_partner_cp'));
array_push($rename, array('maker','comics_partner_publisher'));
array_push($rename, array('editer','comics_partner_writer'));
array_push($rename, array('jjim','member_bookmark'));
array_push($rename, array('buy_member','member_buy_comics'));
array_push($rename, array('cash','member_cash'));
array_push($rename, array('member_human','member_human'));
array_push($rename, array('join_fail_data','member_join_fail'));
array_push($rename, array('point_expen','member_point_expen_comics'));
array_push($rename, array('point_income','member_point_income'));
array_push($rename, array('ags_do_result','pg_channel_allthegate'));
array_push($rename, array('test_vir','pg_channel_allthegate_virtual_get'));
array_push($rename, array('virtual','pg_channel_allthegate_virtual_put'));
array_push($rename, array('payway_toss','pg_channel_toss'));
array_push($rename, array('login_log','stats_log_member'));


foreach($rename as $rename_key => $rename_val){
	$sql_rename = "RENAME TABLE ".$rename_val[0]." TO ".$rename_val[1]."; ";
	echo $sql_rename."<br/>";
	sql_query($sql_rename);
}

?>