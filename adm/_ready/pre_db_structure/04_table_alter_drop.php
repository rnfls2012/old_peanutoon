<?
include_once '_common.php'; // 공통

$table_field_drop = array(); /* array(테이블명, 삭제할속성명, 삭제할속성명, ...) */

array_push($table_field_drop, array('comics','maker','editer','sale','fromto','keyword')); 
// 코믹스 -> 기존 uploadfile가 변환된 컨턴츠정보

array_push($table_field_drop, array('comics_content','content_service','content_title','content_title_name','content_color','content_num','content_name','content_free','content_free','content_way','new_date')); 
// 단행본 화정보

array_push($table_field_drop, array('comics_partner_cp','fromto_jungsan','fromto_percent')); 
// 컨텐츠 CP

array_push($table_field_drop, array('config','cf_terms','cf_pinfo','cf_ipin','cf_mb_leave','cf_admin_sub_list','cf_level_arr','cf_pay_point_arr')); 
// 기본설정

array_push($table_field_drop, array('event','win_date','winner','free','sale','color_code','filenum','theme','web','url','adult')); 
// 이벤트

array_push($table_field_drop, array('member_point_expen_comics','ex_paytotal','ex_wt')); 
// 회원포인트사용

array_push($table_field_drop, array('member_point_income','in_free_date')); 
// 회원포인트충전내역

array_push($table_field_drop, array('member_buy_comics','buy_sex')); 
// 회원 소장 코믹스 리스트

array_push($table_field_drop, array('member_cash','cash_point','cash_point_add')); 
// 회원 충전

array_push($table_field_drop, array('pg_channel_allthegate','Column1','Column2')); 
// PG사 올더게이트 거래정보

/* 21_table_create_alter.php -> event 추가해야함 */
foreach($table_field_drop as $t_rm_key => $t_rm_val){
	foreach($t_rm_val as $t_rm_data_key => $t_rm_data_val){
		if($t_rm_data_key == 0){ continue; }
		$sql_t_rm_data = "ALTER TABLE ".$t_rm_val[0]." DROP ".$t_rm_data_val." ; ";
		echo $sql_t_rm_data."<br/>";
		sql_query($sql_t_rm_data);
	}
}

?>