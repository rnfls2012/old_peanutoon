<?
include_once '_common.php'; // 공통

$table_field_add = array(); /* array(테이블명, 추가할속성명, 추가할속성타입) */

/* ///////// comics ///////// */
array_push($table_field_add, array("comics","content_total","`content_total` INT( 11 ) UNSIGNED NOT NULL DEFAULT '0' COMMENT '컨텐츠 신 총수'  AFTER `sojang` "));
array_push($table_field_add, array("comics","comics_type","`comics_type` varchar(6) NOT NULL DEFAULT '0' COMMENT '컨텐츠 유형' AFTER `content_total` ")); // 코믹스 유형 필드 pd: PD추천만화 / hot: hot만화 / best: best만화 / good:좋은만화 
array_push($table_field_add, array("comics","platform","`platform` INT( 11 ) NOT NULL DEFAULT '1' COMMENT '이용환경'  AFTER `service` ")); // 코믹스 이용환경 0:선택없음 / 1:웹 / 2:Android / 4:Onestore / 8:iOS / 전체 선택: 15 -> N²승 증가
array_push($table_field_add, array("comics","public_cycle","`public_cycle` INT( 11 ) NOT NULL DEFAULT '0' COMMENT '연재주기'  AFTER `platform` ")); // 코믹스 이용환경 0:선택없음 / 1:웹 / 2:Android / 4:Onestore / 8:iOS / 전체 선택: 15 -> N²승 증가
array_push($table_field_add, array("comics","page_way","`page_way` varchar(6) NOT NULL COMMENT '페이지 방향'  AFTER `public_cycle` ")); // 페이지 넘기는 방향 NULL:없음 / left:왼쪽 / right:오른쪽
array_push($table_field_add, array("comics","comment","`comment` varchar(50) NOT NULL COMMENT '한줄 코멘트'  AFTER `somaery` "));
array_push($table_field_add, array("comics","res_service_date","`res_service_date` varchar(20) NOT NULL COMMENT '화 서비스 예약 시간'  AFTER `new_date` "));
array_push($table_field_add, array("comics","res_free_date","`res_free_date` varchar(20) NOT NULL COMMENT '화 기다리다무료 예약 시간'  AFTER `res_service_date "));
array_push($table_field_add, array("comics","service_stop_date","`service_stop_date` varchar(20) NOT NULL COMMENT '서비스 중지 시간'  AFTER `res_free_date` "));
array_push($table_field_add, array("comics","upload_mod_date","`upload_mod_date` varchar(20) NOT NULL COMMENT '데이터수정일'  AFTER `upload_date` "));
array_push($table_field_add, array("comics","kw_id","`kw_id` VARCHAR( 50 ) NOT NULL COMMENT  '키워드 대분류' AFTER `pay_money` "));
array_push($table_field_add, array("comics","kw_id_sub","`kw_id_sub` VARCHAR( 100 ) NOT NULL COMMENT  '키워드 중분류' AFTER `kw_id` "));
array_push($table_field_add, array("comics","kw_id_thi","`kw_id_thi` VARCHAR( 300 ) NOT NULL COMMENT  '키워드 소분류' AFTER `kw_id_sub` ")); 
array_push($table_field_add, array("comics","comics_partner_cp","`comics_partner_cp` INT( 11 ) NOT NULL COMMENT  '제공사번호' AFTER `small` "));
array_push($table_field_add, array("comics","comics_partner_cp_sub","`comics_partner_cp_sub` INT( 11 ) NOT NULL COMMENT  '제공사서브번호' AFTER `comics_partner_cp` "));
array_push($table_field_add, array("comics","total_cash_point","`total_cash_point` FLOAT( 11, 2 ) NOT NULL DEFAULT  '0' COMMENT '구매 누적 코인'  AFTER `pay_num` ")); 
array_push($table_field_add, array("comics","total_point","`total_point` FLOAT( 11, 2 ) NOT NULL DEFAULT  '0' COMMENT '구매 누적 보너스코인'  AFTER `pay_num` ")); 
array_push($table_field_add, array("comics","total_won","`total_won` INT( 11 ) NOT NULL DEFAULT  '0' COMMENT '구매 누적 금액'  AFTER `pay_num` ")); 
array_push($table_field_add, array("comics","new_date","`new_date` varchar(20) NOT NULL COMMENT '최신등록일' AFTER upload_date ")); 

/* ///////// comics_content -> 단행본 화정보 ///////// */
array_push($table_field_add, array("comics_content","comics","`comics` int(11) NOT NULL COMMENT '코믹스번호' AFTER `content_no` "));
array_push($table_field_add, array("comics_content","content_num","`content_num` INT( 11 ) NOT NULL COMMENT  '컨텐츠 신 번호' AFTER `comics` ")); 
array_push($table_field_add, array("comics_content","content_name","`content_name` varchar(100) NOT NULL COMMENT '컨텐츠명' AFTER `content_num` "));
array_push($table_field_add, array("comics_content","content_notice","`content_notice` varchar(100) DEFAULT NULL COMMENT '화 공지사항' AFTER `content_name` "));
array_push($table_field_add, array("comics_content","content_pay","`content_pay` INT( 11 ) NOT NULL COMMENT  '컨텐츠 가격' AFTER  `content_notice` "));
array_push($table_field_add, array("comics_content","content_login","`content_login` varchar(2) DEFAULT NULL COMMENT '로그인후보기' AFTER `content_notice` "));
array_push($table_field_add, array("comics_content","content_file_count","`content_file_count` INT( 5 ) NOT NULL COMMENT '압축안에 파일 갯수' AFTER `content_file` "));
array_push($table_field_add, array("comics_content","banner_file","`banner_file` VARCHAR( 100 ) NOT NULL COMMENT '배너파일 경로' AFTER `content_login` "));
array_push($table_field_add, array("comics_content","order_num","`order_num` INT( 5 ) NOT NULL COMMENT '화 순서'  AFTER `banner_file` "));
array_push($table_field_add, array("comics_content","res_service_date","`res_service_date` varchar(20) NOT NULL COMMENT '서비스 날짜'  AFTER `content_pay` "));
array_push($table_field_add, array("comics_content","res_service_stop_date","`res_service_stop_date` varchar(20) NOT NULL COMMENT '서비스 중지 날짜'  AFTER `res_service_date` "));
array_push($table_field_add, array("comics_content","res_free_date","`res_free_date` varchar(20) NOT NULL COMMENT '무료 날짜'  AFTER `res_service_date` "));

array_push($table_field_add, array("comics_content","service_state","`service_state` varchar(2) NOT NULL COMMENT '서비스 상태'  AFTER `content_pay` ")); 
array_push($table_field_add, array("comics_content","free_state","`free_state` varchar(2) NOT NULL COMMENT '무료 상태'  AFTER `res_service_date` ")); 

/* ///////// comics_partner_cp -> 컨텐츠 CP ///////// */
array_push($table_field_add, array("comics_partner_cp","provider_percent","`provider_percent` INT( 5 ) NOT NULL DEFAULT '0' COMMENT '일반 요율'  AFTER `fromto_name` "));
array_push($table_field_add, array("comics_partner_cp","writer_percent","`writer_percent` INT( 5 ) NOT NULL DEFAULT '0' COMMENT '글작가 요율'  AFTER `provider_percent` "));
array_push($table_field_add, array("comics_partner_cp","painter_percent","`painter_percent` INT( 5 ) NOT NULL DEFAULT '0' COMMENT '그림작가 요율'  AFTER `writer_percent` "));

/* ///////// config -> 기본설정 ///////// */
array_push($table_field_add, array("config","big","`big` varchar(100) NOT NULL COMMENT '대분류리스트'  AFTER cf_copy  "));
array_push($table_field_add, array("config","small","`small` varchar(100) NOT NULL COMMENT '장르리스트'  AFTER big  "));
array_push($table_field_add, array("config","mb_client_list","`mb_client_list` varchar(255) NOT NULL COMMENT '회원등급리스트-고객'  AFTER cf_copy "));
array_push($table_field_add, array("config","mb_parter_level_list","`mb_parter_level_list` varchar(255) NOT NULL COMMENT '회원등급리스트-파트너'  AFTER cf_copy "));
array_push($table_field_add, array("config","mb_admin_level_list","`mb_admin_level_list` varchar(255) NOT NULL COMMENT '회원등급리스트-관리자'  AFTER cf_copy "));
array_push($table_field_add, array("config","cf_cash_type","`cf_cash_type` int(2) NOT NULL DEFAULT  '1' COMMENT '정산금액타입'  AFTER `small` "));
array_push($table_field_add, array("config","pay"," `pay` VARCHAR(255) NOT NULL DEFAULT  '' COMMENT '코인_리스트'  AFTER `small` ")); 

/* ///////// event -> 무료&코인이벤트 - 코믹스리스트 ///////// */
array_push($table_field_add, array("event","date_use","`date_use` int(2) NOT NULL DEFAULT '0' COMMENT '0_기간있음_1_기간없음'  AFTER textt ")); 
array_push($table_field_add, array("event","state","`state` varchar(2) NOT NULL COMMENT '이벤트상태_r_예약중_s_진행중_e_종료'  AFTER bener ")); 
array_push($table_field_add, array("event","event_reg_date","`event_reg_date` datetime DEFAULT NULL COMMENT '등록일'  AFTER click ")); 
array_push($table_field_add, array("event","event_mod_date","`event_mod_date` datetime DEFAULT NULL COMMENT '수정일'  AFTER event_reg_date ")); 
array_push($table_field_add, array("event","adult","`adult` int(2) NOT NULL DEFAULT '0' COMMENT '0_청소년_1_성인'  AFTER date_end ")); 

/* ///////// member_cash -> 회원 충전 ///////// */
array_push($table_field_add, array("member_cash","cash_won","`cash_won` INT(11) NOT NULL COMMENT '결제금액'  AFTER `cash_mode` "));
array_push($table_field_add, array("member_cash","cash_point","`cash_point` INT(11) NOT NULL COMMENT  '충전한 코인'  AFTER `cash_won` "));
array_push($table_field_add, array("member_cash","point","`point` INT(11) NOT NULL COMMENT  '충전한 보너스코인' AFTER  `cash_point` "));

/* ///////// pg_channel_allthegate -> PG사 올더게이트 거래정보 ///////// */
array_push($table_field_add, array("pg_channel_allthegate","Column1","`Column1` INT(11) NOT NULL COMMENT '코인'  AFTER ES_SENDNO ")); 
array_push($table_field_add, array("pg_channel_allthegate","Column2","`Column2` INT(11) NOT NULL COMMENT '보너스코인'  AFTER Column1 ")); 
array_push($table_field_add, array("pg_channel_allthegate","Column3","`Column3` varchar(255) NOT NULL COMMENT '결제환경'  AFTER Column2 ")); 
array_push($table_field_add, array("pg_channel_allthegate","rOrdNm_idx","`rOrdNm_idx` varchar(2) NOT NULL COMMENT '주문자명(회원ID 인덱스)'  AFTER `rOrdNm` ")); 

/* ///////// member_point_expen_comics ->회원 코믹스 구입 정보 ///////// */
array_push($table_field_add, array("member_point_expen_comics","cash_type","`cash_type` int(2) NOT NULL DEFAULT '1' COMMENT '정산금액타입'  AFTER `ex_cash` ")); 
array_push($table_field_add, array("member_point_expen_comics","ex_state","`ex_state` int(2) NOT NULL DEFAULT '0' COMMENT '결재상태(0_완료_1_취소_2_환불_3_에러)'  AFTER `cash_type` ")); 
array_push($table_field_add, array("member_point_expen_comics","ex_buy_type","`ex_buy_type` int(2) NOT NULL DEFAULT '0' COMMENT '구매방법(0_에러_1_개별_7_전체)'  AFTER `ex_state` ")); 
array_push($table_field_add, array("member_point_expen_comics","ex_user_agent","`ex_user_agent` VARCHAR(255) NOT NULL COMMENT '구매자세한환경' AFTER `ex_kind` ")); 
array_push($table_field_add, array("member_point_expen_comics","ex_version","`ex_version` VARCHAR(20) NOT NULL COMMENT '구매버전' AFTER `ex_user_agent` ")); 

/* ///////// member_point_income ->회원 코믹스 화폐 충전 내역 ///////// */
array_push($table_field_add, array("member_point_income","in_cash_won","`in_cash_won` int(11) NOT NULL COMMENT '결제금액'  AFTER `in_cash` ")); 
array_push($table_field_add, array("member_point_income","in_date2","`in_date2` varchar(20) NOT NULL COMMENT '충전 상세날짜'  AFTER `in_from` ")); 
array_push($table_field_add, array("member_point_income","in_free","`in_free` int(2) NOT NULL DEFAULT '0' COMMENT '무료여부(0_무료아님_1_무료)'  AFTER `in_cash_won` ")); 
array_push($table_field_add, array("member_point_income","in_state","`in_state` int(2) NOT NULL DEFAULT '0' COMMENT '결재상태(0_완료_1_취소_2_환불_3_에러)'  AFTER `in_cash_won` ")); 
array_push($table_field_add, array("member_point_income","in_kind","`in_kind` VARCHAR(20) NOT NULL COMMENT '충전환경' AFTER `in_cash_balance` ")); 
array_push($table_field_add, array("member_point_income","in_user_agent","`in_user_agent` VARCHAR(255) NOT NULL COMMENT '충전자세한환경' AFTER `in_kind` ")); 
array_push($table_field_add, array("member_point_income","in_version","`in_version` VARCHAR(20) NOT NULL COMMENT '충전버전' AFTER `in_user_agent` ")); 


/* ///////// member ->회원 회원정보 화폐 충전 내역 ///////// */
array_push($table_field_add, array("member","member_stop_date","`member_stop_date` varchar(30) DEFAULT NULL COMMENT '회원 중지일'  AFTER `member_out_date` ")); 
array_push($table_field_add, array("member","member_state","`member_state` varchar(2) DEFAULT 'y' NULL COMMENT '회원상태_y_정상_s_정지_n_탈퇴'  AFTER `member_phone3` "));
array_push($table_field_add, array("member","member_overlap_key","`member_overlap_key` int(11) NOT NULL COMMENT '회원 중복 체크 및 원본 넘버_0_중복아님'  AFTER `member_info` ")); 

/* ///////// member_human ->탈퇴/휴면처리 회원 ///////// */
array_push($table_field_add, array("member_human","member_stop_date","`member_stop_date` varchar(30) DEFAULT NULL COMMENT '회원 중지일'  AFTER `member_out_date` "));
array_push($table_field_add, array("member_human","member_state","`member_state` varchar(2) DEFAULT 'y' NULL COMMENT '회원상태_y_정상_s_정지_n_탈퇴'  AFTER `member_phone3` "));
array_push($table_field_add, array("member_human","member_overlap_key","`member_overlap_key` int(11) NOT NULL COMMENT '회원 중복 체크 및 원본 넘버_0_중복아님'  AFTER `member_info` ")); 


/*
array_push($table_field_add, array("event","1111","1 ")); 
array_push($table_field_add, array("comics_content","1111","1 ")); // 11111111111
*/

/* 위 리스트 테이블 속성 추가 */
foreach($table_field_add as $t_mk_key => $t_mk_val){

	$sql_t_mk_data_chk = "SELECT ".$t_mk_val[1]." FROM `".$t_mk_val[0]."` LIMIT 0, 1";
	$sql_t_mk_data = "ALTER TABLE `".$t_mk_val[0]."` ADD ".$t_mk_val[2]." ;";






	echo $sql_t_mk_data_chk."<br/>";

	if(!sql_query($sql_t_mk_data_chk)) {
		sql_query($sql_t_mk_data);
		echo $sql_t_mk_data."<br/>";
	}else{
		echo " -> 생성되어 있음<br/>";
	}
	echo "<br/>";
}

?>