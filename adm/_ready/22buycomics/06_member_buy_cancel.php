<? include_once '_common.php'; // 공통 ?>
<?
   /* 1.
      3억의 가장 작은 존재:프롤로그 결제 취소 - 
      comics=2678 episode=29364 취소 - 16

	select * from member_point_expen_episode_2 
	where mpee_comics=2678 AND mpee_episode=29364 
	AND mpee_year_month='2018-01' AND mpee_day='21';

	select * from member_buy_episode_2 where mbe_comics=2678 AND mbe_episode=29364 AND mbe_date like '2018-01-21%';

   */

   /* 2.
   사귀는 건 아냐 외전 결제 취소 - 23
   comics=2436 &episode=28900
   comics=2436 &episode=29184
   comics=2436 &episode=29185
   comics=2436 &episode=29186

   select * from member_point_expen_episode_2 where mpee_comics=2436 AND mpee_episode=28900;
   select * from member_buy_episode_2 where mbe_comics=2436 AND mbe_episode=28900;

   검색해보니 외전 2,3,4는 판매없음

   */

   // buy 검색 
   $t_sql_arr[1] = "select * from member_point_expen_episode_2 	where mpee_comics=2678 AND mpee_episode=29364 AND mpee_year_month='2018-01' AND mpee_day='21' ";
   $t_sql_arr[2] = "select * from member_point_expen_episode_2 where mpee_comics=2436 AND mpee_episode=28900 ";

   // 소장정보 검색 
   $t_sql_buy_arr[1] = "select * from member_buy_episode_2 where mbe_comics=2678 AND mbe_episode=29364 AND mbe_date like '2018-01-21%' ";
   $t_sql_buy_arr[2] = "select * from member_buy_episode_2 where mbe_comics=2436 AND mbe_episode=28900 ";
   
   /*
   $t_sql_update_arr[1] = "select * from member_buy_episode_2 where mbe_comics=2678 AND mbe_episode=29364 AND mbe_date like '2018-01-21%' ";
   $t_sql_update_arr[2] = "select * from member_buy_episode_2 where mbe_comics=2436 AND mbe_episode=28900 ";
	*/
   
   $t_cnt = array(); // 갯수
   foreach($t_sql_arr as  $t_sql_key => $t_sql_val){
		$t_result = sql_query($t_sql_val);	   
		$t_cnt[$t_sql_key] = sql_num_rows($t_result);	   
		while ($t_row = sql_fetch_array($t_result)) {
			$t_sql_update = "update member_point_expen_episode_2 set mpee_state='3', mpee_date='".NM_TIME_YMDHIS."' 
			                 where mpee_no='".$t_row['mpee_no']."'";
			// echo $t_sql_update."<br/>";
			sql_query($t_sql_update);	
		}
   }
   // select * from member_point_expen_episode_2 where mpee_state='3';

   // 소장정보 취소
   $t_cnt_buy = array(); // 갯수
   foreach($t_sql_buy_arr as  $t_sql_buy_key => $t_sql_buy_val){
		$t_result_buy = sql_query($t_sql_buy_val);	   
		$t_cnt_buy[$t_sql_buy_key] = sql_num_rows($t_result_buy);	   
		while ($t_row_buy = sql_fetch_array($t_result_buy)) {
			$t_sql_update_buy = "update member_buy_episode_2 set mbe_own_type='4', mbe_end_date='".NM_TIME_YMD."' 
			                     where mbe_no='".$t_row_buy['mbe_no']."'";
			// echo $t_sql_update."<br/>";
			sql_query($t_sql_update_buy);	
		}
   }
   // select * from member_buy_episode_2 where mbe_own_type='4';

   // 통계 부분은 눈으로....;;
   /*
    sl_comics=2678
	UPDATE `peanutoonDB`.`sales` SET `sl_pay`='0', `sl_point`='0', `sl_cash_point`='0' WHERE  `sl_no`=822455;
	UPDATE `peanutoonDB`.`sales` SET `sl_pay`='0' WHERE  `sl_no`=825099;

   */

	/*
   foreach($t_cnt as  $t_cnt_key => $t_cnt_val){
	   if($t_cnt_val == $t_cnt_buy[$t_cnt_key]){
			// echo $t_sql_arr[$t_cnt_key]." AND mpee_state='3'"."<br/>";
			$t_cnt_sql = $t_sql_arr[$t_cnt_key]." AND mpee_state='3'";
			$t_cnt_result = sql_query($t_cnt_sql);	   
			while ($t_cnt_row = sql_fetch_array($t_cnt_result)) {
				
				// echo $t_cnt_row['mpee_no']."<br/>";
				// echo $t_cnt_row['mpee_year_month']."<br/>";
				// echo $t_cnt_row['mpee_year']."<br/>";
				// echo $t_cnt_row['mpee_month']."<br/>";
				// echo $t_cnt_row['mpee_day']."<br/>";
				// echo $t_cnt_row['mpee_hour']."<br/>";
				
				$t_cnt_sql_update_buy = "update sales set mbe_own_type='4', mbe_end_date='".NM_TIME_YMD."' 
										 where mbe_no='".$t_row_buy['mbe_no']."'";
			}

	   }
   }
   */

   
   // select * from member_point_expen_episode_2 where mpee_state='3' AND mpee_comics=2678;
   // select * from member_point_expen_episode_2 where mpee_state!='3' AND mpee_comics=2678 AND mpee_year_month='2018-01';
    
	// 일
	/* 1. */
	// select * from sales where sl_comics=2678 AND sl_year_month='2018-01' AND sl_day='21' ;
	/* 2. */
	// select * from sales_age_sex where sas_comics=2678 AND sas_year_month='2018-01' AND sas_day='21' ;
	/* 3. */
	// select * from sales_episode_2 where se_comics=2678 AND se_year_month='2018-01' AND se_day='21' ;
	/* 4. */
	// select * from sales_episode_2_age_sex where seas_comics=2678 AND seas_year_month='2018-01' AND seas_day='21' ;
	/* 5. */
	// select * from vsales where sl_comics=2678 AND sl_year_month='2018-01' AND sl_day='21' ;
	/* 6. */
	// select * from vsales_age_sex where sas_comics=2678 AND sas_year_month='2018-01' AND sas_day='21' ;
	/* 7. */
	// select * from vsales_episode_2 where se_comics=2678 AND se_year_month='2018-01' AND se_day='21' ;
	/* 8. */
	// select * from vsales_episode_2_age_sex where seas_comics=2678 AND seas_year_month='2018-01' AND seas_day='21' ;
    
	// 일
	/* 1. */
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='0', `sl_point`='0', `sl_cash_point`='0' WHERE  `sl_no`=822455");
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='0' WHERE  `sl_no`=825099");
	/* 2. */
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_man`='0', `sas_woman`='0', `sas_man50`='0', `sas_man60`='0', `sas_woman20`='0', `sas_woman30`='0' WHERE  `sas_no`=228085");
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='0', `sas_woman20`='0' WHERE  `sas_no`=228780");
	/* 3. */
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0', `se_point`='0', `se_cash_point`='0' WHERE  `se_no`=3405713");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0' WHERE  `se_no`=3417111");
	/* 4. */
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_man`='0', `seas_woman`='0', `seas_man50`='0', `seas_man60`='0', `seas_woman20`='0', `seas_woman30`='0' WHERE  `seas_no`=1003683");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=1006891");
	/* 5. */
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_pay`='0', `sl_cash_point`='0' WHERE  `sl_no`=1048350");
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_pay`='0' WHERE  `sl_no`=1050985");
	/* 6. */
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_man`='0', `sas_woman`='0', `sas_man50`='0', `sas_woman20`='0', `sas_woman30`='0' WHERE  `sas_no`=240418");
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_woman`='0', `sas_woman20`='0' WHERE  `sas_no`=241046");
	/* 7. */
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=4203310");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_pay`='0' WHERE  `se_no`=4214540");
	/* 8. */
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_man`='0', `seas_woman`='0', `seas_man50`='0', `seas_woman20`='0', `seas_woman30`='0' WHERE  `seas_no`=1065950");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=1068820");
	
	// 아래 월 해야 함;;; - se_point_cnt 제대로 안됨;;;

	// 월
	/* 11. */
	// select * from w_sales where sl_comics=2678 AND sl_year_month='2018-01' ;
	/* 12. */
	// select * from w_sales_age_sex where sas_comics=2678 AND sas_year_month='2018-01' ;
	/* 13. */
	// select * from w_sales_episode_2 where se_comics=2678 AND se_year_month='2018-01' ;
	/* 14. */
	// select * from w_sales_episode_2_age_sex where seas_comics=2678 AND seas_year_month='2018-01' ;
	/* 15. */
	// select * from w_vsales where sl_comics=2678 AND sl_year_month='2018-01' ;
	/* 16. */
	// select * from w_vsales_age_sex where sas_comics=2678 AND sas_year_month='2018-01' ;
	/* 17. */
	// select * from w_vsales_episode_2 where se_comics=2678 AND se_year_month='2018-01' ;
	/* 18. */
	// select * from w_vsales_episode_2_age_sex where seas_comics=2678 AND seas_year_month='2018-01' ;
    
	// 월
	/* 11. */
	sql_query("UPDATE `peanutoonDB`.`w_sales` SET `sl_pay`='2', `sl_point`='0', `sl_cash_point`='0' WHERE  `sl_no`=64180");
	/* 12. */
	sql_query("UPDATE `peanutoonDB`.`w_sales_age_sex` SET `sas_man`='0', `sas_woman`='2', `sas_man50`='0', `sas_man60`='0', `sas_woman20`='2', `sas_woman30`='0' WHERE  `sas_no`=57043");
	/* 13. */
	sql_query("UPDATE `peanutoonDB`.`w_sales_episode_2` SET `se_pay`='1' WHERE  `se_no`=143129");
	sql_query("UPDATE `peanutoonDB`.`w_sales_episode_2` SET `se_pay`='0', `se_point`='0', `se_cash_point`='0' WHERE  `se_no`=143531");
	sql_query("UPDATE `peanutoonDB`.`w_sales_episode_2` SET `se_pay`='1' WHERE  `se_no`=143735");
	/* 14. */
	sql_query("UPDATE `peanutoonDB`.`w_sales_episode_2_age_sex` SET `seas_woman`='1', `seas_woman20`='1' WHERE  `seas_no`=123607");
	sql_query("UPDATE `peanutoonDB`.`w_sales_episode_2_age_sex` SET `seas_man`='0', `seas_woman`='0', `seas_man50`='0', `seas_man60`='0', `seas_woman20`='0', `seas_woman30`='0' WHERE  `seas_no`=124075");
	sql_query("UPDATE `peanutoonDB`.`w_sales_episode_2_age_sex` SET `seas_woman`='1', `seas_woman20`='1' WHERE  `seas_no`=124283");
	/* 15. */
	sql_query("UPDATE `peanutoonDB`.`w_vsales` SET `sl_pay`='2', `sl_cash_point`='0' WHERE  `sl_no`=61851");
	/* 16. */
	sql_query("UPDATE `peanutoonDB`.`w_vsales_age_sex` SET `sas_man`='0', `sas_woman`='2', `sas_man50`='0', `sas_woman20`='2', `sas_woman30`='0' WHERE  `sas_no`=53691");
	/* 17. */
	sql_query("UPDATE `peanutoonDB`.`w_vsales_episode_2` SET `se_pay`='1' WHERE  `se_no`=139828");
	sql_query("UPDATE `peanutoonDB`.`w_vsales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=140234");
	sql_query("UPDATE `peanutoonDB`.`w_vsales_episode_2` SET `se_pay`='1' WHERE  `se_no`=140427");
	/* 18. */
	sql_query("UPDATE `peanutoonDB`.`w_vsales_episode_2_age_sex` SET `seas_woman`='1', `seas_woman20`='1' WHERE  `seas_no`=119150");
	sql_query("UPDATE `peanutoonDB`.`w_vsales_episode_2_age_sex` SET `seas_man`='0', `seas_woman`='0', `seas_man50`='0', `seas_woman20`='0', `seas_woman30`='0' WHERE  `seas_no`=119625");
	sql_query("UPDATE `peanutoonDB`.`w_vsales_episode_2_age_sex` SET `seas_woman`='1', `seas_woman20`='1' WHERE  `seas_no`=119851");




	///////////////////////////////////////////////////////////////// 2436 /////////////////////////////////////////////////////////////////////////

    // select * from member_point_expen_episode_2 where mpee_state='3' AND mpee_comics=2436;
	
	// 일
	/* 1. */
	// select * from sales where sl_comics=2436 AND sl_year_month='2018-01' AND sl_day =16 AND sl_hour in (11,12,13,15,16,19) ;
	// ->
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='0', `sl_point`='0', `sl_cash_point`='0' WHERE  `sl_no`=800749");
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='3', `sl_cash_point`='9' WHERE  `sl_no`=800940");
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='0', `sl_cash_point`='0' WHERE  `sl_no`=801209");
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='6', `sl_cash_point`='18' WHERE  `sl_no`=801647");
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='17', `sl_cash_point`='51' WHERE  `sl_no`=801728");
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='1', `sl_cash_point`='3' WHERE  `sl_no`=802304");

	// select * from sales where sl_comics=2436 AND sl_year_month='2018-01' AND sl_day =17 AND sl_hour in (00,09,11,21) ;
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_all_pay`='22', `sl_point`='660', `sl_pay`='1', `sl_cash_point`='3' WHERE  `sl_no`=803507");
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='2', `sl_cash_point`='3' WHERE  `sl_no`=805084");
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='6', `sl_cash_point`='18' WHERE  `sl_no`=805402");
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='15', `sl_cash_point`='45' WHERE  `sl_no`=807306");

	// select * from sales where sl_comics=2436 AND sl_year_month='2018-01' AND sl_day =18 AND sl_hour in (00) ;
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='1', `sl_point`='30' WHERE  `sl_no`=808197");

	// select * from sales where sl_comics=2436 AND sl_year_month='2018-01' AND sl_day =19 AND sl_hour in (18) ;
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='10', `sl_point`='20' WHERE  `sl_no`=815223");

	// select * from sales where sl_comics=2436 AND sl_year_month='2018-01' AND sl_day =20 AND sl_hour in (17) ;
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_all_pay`='5', `sl_cash_point`='15' WHERE  `sl_no`=819388");

	// select * from sales where sl_comics=2436 AND sl_year_month='2018-01' AND sl_day =21 AND sl_hour in (20) ;
	sql_query("UPDATE `peanutoonDB`.`sales` SET `sl_pay`='13', `sl_cash_point`='39' WHERE  `sl_no`=824340");
	
	/* 2. */
	// select * from sales_age_sex where sas_comics=2436 AND sas_year_month='2018-01' AND sas_day =16 AND sas_hour in (11,12,13,15,16,19) ;
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='0', `sas_woman20`='0', `sas_woman30`='0' WHERE  `sas_no`=222279");
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='3', `sas_woman20`='3' WHERE  `sas_no`=222330");
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='0', `sas_woman20`='0' WHERE  `sas_no`=222406");
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='6', `sas_woman20`='1' WHERE  `sas_no`=222510");
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='17', `sas_woman30`='17' WHERE  `sas_no`=222532");
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='1', `sas_woman20`='1' WHERE  `sas_no`=222699;");
	
	// select * from sales_age_sex where sas_comics=2436 AND sas_year_month='2018-01' AND sas_day =17 AND sas_hour in (00,09,11,21) ;
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='23', `sas_woman20`='23' WHERE  `sas_no`=223055");
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='2', `sas_woman50`='1' WHERE  `sas_no`=223377");
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='6', `sas_woman20`='6' WHERE  `sas_no`=223455");
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='15', `sas_woman50`='15' WHERE  `sas_no`=223909");

	// select * from sales_age_sex where sas_comics=2436 AND sas_year_month='2018-01' AND sas_day =18 AND sas_hour in (00) ;
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='1', `sas_woman20`='1' WHERE  `sas_no`=224157");
	
	// select * from sales_age_sex where sas_comics=2436 AND sas_year_month='2018-01' AND sas_day =19 AND sas_hour in (18) ;
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='10', `sas_woman20`='10' WHERE  `sas_no`=226147");

	// select * from sales_age_sex where sas_comics=2436 AND sas_year_month='2018-01' AND sas_day =20 AND sas_hour in (17) ;
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='5', `sas_woman20`='5' WHERE  `sas_no`=227244");
	
	// select * from sales_age_sex where sas_comics=2436 AND sas_year_month='2018-01' AND sas_day =21 AND sas_hour in (20) ;
	sql_query("UPDATE `peanutoonDB`.`sales_age_sex` SET `sas_woman`='13', `sas_woman20`='13' WHERE  `sas_no`=228533");
	
	/* 3. */
	// select * from sales_episode_2 where se_comics=2436 AND se_episode=28900 AND se_year_month='2018-01' AND se_day =16 AND se_hour in (11,12,13,15,16,19) ;
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0', `se_point`='0', `se_cash_point`='0' WHERE  `se_no`=3311731");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=3312650");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=3314085");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=3315839");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=3316775");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=3318989;");
	
	// select * from sales_episode_2 where se_comics=2436 AND se_episode=28900 AND se_year_month='2018-01' AND se_day =17 AND se_hour in (00,09,11,21) ;
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_all_pay`='0', `se_pay`='0', `se_point`='0', `se_cash_point`='0' WHERE  `se_no`=3324472");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=3329949");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=3331501");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=3339962");

	// select * from sales_episode_2 where se_comics=2436 AND se_episode=28900 AND se_year_month='2018-01' AND se_day =18 AND se_hour in (00) ;
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0', `se_point`='0' WHERE  `se_no`=3343396");

	// select * from sales_episode_2 where se_comics=2436 AND se_episode=28900 AND se_year_month='2018-01' AND se_day =19 AND se_hour in (18) ;
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0', `se_point`='0' WHERE  `se_no`=3374158");

	// select * from sales_episode_2 where se_comics=2436 AND se_episode=28900 AND se_year_month='2018-01' AND se_day =20 AND se_hour in (17) ;
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_all_pay`='0', `se_cash_point`='0' WHERE  `se_no`=3391199");

	// select * from sales_episode_2 where se_comics=2436 AND se_episode=28900 AND se_year_month='2018-01' AND se_day =21 AND se_hour in (20) ;
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=3414253");
	
	/* 4. */
	// select * from sales_episode_2_age_sex where seas_comics=2436 AND seas_episode=28900 AND seas_year_month='2018-01' AND seas_day =16 AND seas_hour in (11,12,13,15,16,19) ;
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0', `seas_woman30`='0' WHERE  `seas_no`=975907");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=976210");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=976678");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=977277");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman30`='0' WHERE  `seas_no`=977607");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=978074");


	// select * from sales_episode_2_age_sex where seas_comics=2436 AND seas_episode=28900 AND seas_year_month='2018-01' AND seas_day =17 AND seas_hour in (00,09,11,21) ;
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=980156");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman50`='0' WHERE  `seas_no`=981550");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=982103");
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman50`='0' WHERE  `seas_no`=984473"); 


	// select * from sales_episode_2_age_sex where seas_comics=2436 AND seas_episode=28900 AND seas_year_month='2018-01' AND seas_day =18 AND seas_hour in (00) ;
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=985589");

	// select * from sales_episode_2_age_sex where seas_comics=2436 AND seas_episode=28900 AND seas_year_month='2018-01' AND seas_day =19 AND seas_hour in (18) ;
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=994666");

	// select * from sales_episode_2_age_sex where seas_comics=2436 AND seas_episode=28900 AND seas_year_month='2018-01' AND seas_day =20 AND seas_hour in (17) ;
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=999552");

	// select * from sales_episode_2_age_sex where seas_comics=2436 AND seas_episode=28900 AND seas_year_month='2018-01' AND seas_day =21 AND seas_hour in (20) ;
	sql_query("UPDATE `peanutoonDB`.`sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=1006132");

	/* 5. */
	// select * from vsales where sl_comics=2436 AND sl_year_month='2018-01' AND sl_day =16 AND sl_hour in (11,12,13,15,16,19) ;
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_pay`='0', `sl_cash_point`='0' WHERE  `sl_no`=1026756");
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_pay`='3', `sl_cash_point`='9' WHERE  `sl_no`=1026946");
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_pay`='0', `sl_cash_point`='0' WHERE  `sl_no`=1027213");
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_pay`='6', `sl_cash_point`='18' WHERE  `sl_no`=1027651");
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_pay`='17', `sl_cash_point`='51' WHERE  `sl_no`=1027731");
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_pay`='1', `sl_cash_point`='3' WHERE  `sl_no`=1028307");

	// select * from vsales where sl_comics=2436 AND sl_year_month='2018-01' AND sl_day =17 AND sl_hour in (00,09,11,21) ;
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_pay`='1', `sl_cash_point`='3' WHERE  `sl_no`=1029500");
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_pay`='1', `sl_cash_point`='3' WHERE  `sl_no`=1031073");
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_pay`='6', `sl_cash_point`='18' WHERE  `sl_no`=1031387");
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_pay`='15', `sl_cash_point`='45' WHERE  `sl_no`=1033282");

	// select * from vsales where sl_comics=2436 AND sl_year_month='2018-01' AND sl_day =20 AND sl_hour in (17) ;
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_all_pay`='5', `sl_cash_point`='15' WHERE  `sl_no`=1045303");

	// select * from vsales where sl_comics=2436 AND sl_year_month='2018-01' AND sl_day =21 AND sl_hour in (20) ;
	sql_query("UPDATE `peanutoonDB`.`vsales` SET `sl_pay`='13', `sl_cash_point`='39' WHERE  `sl_no`=1050228");

	/* 6. */
	// select * from vsales_age_sex where sas_comics=2436 AND sas_year_month='2018-01' AND sas_day =16 AND sas_hour in (11,12,13,15,16,19) ;
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_woman`='0', `sas_woman20`='0', `sas_woman30`='0' WHERE  `sas_no`=235370");
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_woman`='3', `sas_woman20`='3' WHERE  `sas_no`=235417");
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_woman`='0', `sas_woman20`='0' WHERE  `sas_no`=235481");
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_woman`='6', `sas_woman20`='1' WHERE  `sas_no`=235568");
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_woman`='17', `sas_woman30`='17' WHERE  `sas_no`=235589");
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_woman`='1', `sas_woman20`='1' WHERE  `sas_no`=235739");

	// select * from vsales_age_sex where sas_comics=2436 AND sas_year_month='2018-01' AND sas_day =17 AND sas_hour in (00,09,11,21) ;
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_woman`='1', `sas_woman20`='1' WHERE  `sas_no`=236050");
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_woman`='1', `sas_woman50`='1' WHERE  `sas_no`=236321");
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_woman`='6', `sas_woman20`='6' WHERE  `sas_no`=236390");
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_woman`='15', `sas_woman50`='15' WHERE  `sas_no`=236783");

	// select * from vsales_age_sex where sas_comics=2436 AND sas_year_month='2018-01' AND sas_day =20 AND sas_hour in (17) ;
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_woman`='5', `sas_woman20`='5' WHERE  `sas_no`=239693");

	// select * from vsales_age_sex where sas_comics=2436 AND sas_year_month='2018-01' AND sas_day =21 AND sas_hour in (20) ;
	sql_query("UPDATE `peanutoonDB`.`vsales_age_sex` SET `sas_woman`='13', `sas_woman20`='13' WHERE  `sas_no`=240823");










	/* 7. */
	// select * from vsales_episode_2 where se_comics=2436 AND se_episode=28900 AND se_year_month='2018-01' AND se_day =16 AND se_hour in (11,12,13,15,16,19) ;
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=4111680");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=4112579");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=4113918");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=4115629");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=4116543");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=4118730");

	// select * from vsales_episode_2 where se_comics=2436 AND se_episode=28900 AND se_year_month='2018-01' AND se_day =17 AND se_hour in (00,09,11,21) ;
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=4124036");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=4129341");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=4130857");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=4139160");

	// select * from vsales_episode_2 where se_comics=2436 AND se_episode=28900 AND se_year_month='2018-01' AND se_day =20 AND se_hour in (17) ;
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_all_pay`='0', `se_cash_point`='0' WHERE  `se_no`=4189060");

	// select * from vsales_episode_2 where se_comics=2436 AND se_episode=28900 AND se_year_month='2018-01' AND se_day =21 AND se_hour in (20) ;
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2` SET `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=4211718");

	/* 8. */
	// select * from vsales_episode_2_age_sex where seas_comics=2436 AND seas_episode=28900 AND seas_year_month='2018-01' AND seas_day =16 AND seas_hour in (11,12,13,15,16,19) ;
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0', `seas_woman30`='0' WHERE  `seas_no`=1041954");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=1042222");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=1042587");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=1043104");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman30`='0' WHERE  `seas_no`=1043383");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=1043778");

	// select * from vsales_episode_2_age_sex where seas_comics=2436 AND seas_episode=28900 AND seas_year_month='2018-01' AND seas_day =17 AND seas_hour in (00,09,11,21) ;
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=1045602");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman50`='0' WHERE  `seas_no`=1046708");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=1047210");
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman50`='0' WHERE  `seas_no`=1049299");

	// select * from vsales_episode_2_age_sex where seas_comics=2436 AND seas_episode=28900 AND seas_year_month='2018-01' AND seas_day =20 AND seas_hour in (17) ;
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=1062260");

	// select * from vsales_episode_2_age_sex where seas_comics=2436 AND seas_episode=28900 AND seas_year_month='2018-01' AND seas_day =21 AND seas_hour in (20) ;
	sql_query("UPDATE `peanutoonDB`.`vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0' WHERE  `seas_no`=1068144");









	/* 11. */
	/* 올구매219 / 각구매403 cash_point 1509 / point 3540  */
	/* 전체 23 -> 올구매2 / 각구매21 cash_point 54 / point 150 */
	/* 전체 23 -> 올구매217 / 각구매382 cash_point 1455 / point 3390 */
	// select * from w_sales where sl_comics=2436 AND sl_year_month='2018-01' ;
	sql_query("UPDATE `peanutoonDB`.`w_sales` SET `sl_all_pay`='217', `sl_pay`='382', `sl_point`='3390', `sl_cash_point`='1455' WHERE  `sl_no`=62370");

	/* 12. */
	// select * from w_sales_age_sex where sas_comics=2436 AND sas_year_month='2018-01' ;
	sql_query("UPDATE `peanutoonDB`.`w_sales_age_sex` SET `sas_woman`='594', `sas_woman20`='406', `sas_woman30`='100', `sas_woman50`='45' WHERE  `sas_no`=56010");

	/* 13. */
	// select * from w_sales_episode_2 where se_comics=2436 AND se_episode=28900 AND se_year_month='2018-01' ;
	sql_query("UPDATE `peanutoonDB`.`w_sales_episode_2` SET `se_all_pay`='0', `se_pay`='0', `se_point`='0', `se_cash_point`='0' WHERE  `se_no`=143259");

	/* 14. */
	// select * from w_sales_episode_2_age_sex where seas_comics=2436 AND seas_episode=28900 AND seas_year_month='2018-01' ;
	sql_query("UPDATE `peanutoonDB`.`w_sales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0', `seas_woman30`='0', `seas_woman50`='0' WHERE  `seas_no`=123760");

	/* 15. */
	/* 올구매151 / 각구매350 cash_point 1500 */
	/* 전체 18 -> 올구매1 / 각구매17 cash_point 54 */
	/* 전체 18 -> 올구매150 / 각구매333 cash_point 1446 */
	// select * from w_vsales where sl_comics=2436 AND sl_year_month='2018-01' ;
	sql_query("UPDATE `peanutoonDB`.`w_vsales` SET `sl_all_pay`='150', `sl_pay`='333', `sl_cash_point`='1446' WHERE  `sl_no`=60044");

	/* 16. */
	// select * from w_vsales_age_sex where sas_comics=2436 AND sas_year_month='2018-01' ;
	sql_query("UPDATE `peanutoonDB`.`w_vsales_age_sex` SET `sas_woman`='479', `sas_woman20`='306', `sas_woman30`='89', `sas_woman50`='44' WHERE  `sas_no`=52730");

	/* 17. */
	// select * from w_vsales_episode_2 where se_comics=2436 AND se_episode=28900 AND se_year_month='2018-01' ;
	sql_query("UPDATE `peanutoonDB`.`w_vsales_episode_2` SET `se_all_pay`='0', `se_pay`='0', `se_cash_point`='0' WHERE  `se_no`=139967");

	/* 18. */
	// select * from w_vsales_episode_2_age_sex where seas_comics=2436 AND seas_episode=28900 AND seas_year_month='2018-01' ;
	sql_query("UPDATE `peanutoonDB`.`w_vsales_episode_2_age_sex` SET `seas_woman`='0', `seas_woman20`='0', `seas_woman30`='0', `seas_woman50`='0' WHERE  `seas_no`=119307");
    

?>