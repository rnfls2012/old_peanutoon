<?
include_once '_common.php'; // 공통

/*
SELECT substring(ce_file, 2, CHAR_LENGTH(ce_file)-1) from comics_episode_1;
UPDATE comics_episode_1 set cm_cover = substring(ce_file, 2, CHAR_LENGTH(ce_file))
*/

/*
SELECT substring(cm_cover, 2, CHAR_LENGTH(cm_cover)) from comics;
SELECT substring(cm_cover_sub, 2, CHAR_LENGTH(cm_cover_sub)) from comics;

UPDATE comics set cm_cover = substring(cm_cover, 2, CHAR_LENGTH(cm_cover))
UPDATE comics set cm_cover_sub = substring(cm_cover_sub, 2, CHAR_LENGTH(cm_cover_sub))


SELECT * FROM `comics_menu` WHERE 1 and cn_adult = '0'
UPDATE `comics_menu` SET cn_adult = 'n' WHERE 1 and cn_adult = '0'


SELECT * FROM `comics_menu` WHERE 1 and cn_adult = '1'
UPDATE `comics_menu` SET cn_adult = 'y' WHERE 1 and cn_adult = '1'


SELECT substring(ep_cover, 2, CHAR_LENGTH(ep_cover)) from event_pay;
UPDATE event_pay set ep_cover = substring(ep_cover, 2, CHAR_LENGTH(ep_cover))

// 닉네임 넣기
UPDATE member set mb_nick = mb_id;

*/

/*
CREATE TABLE IF NOT EXISTS `member_sns` (
	`mbs_no` int(11) NOT NULL AUTO_INCREMENT,
	`mbs_id` varchar(30) NOT NULL DEFAULT '' COMMENT '회원snsID',
	`mbs_idx` varchar(2) NOT NULL DEFAULT '' COMMENT '회원snsID-index',

	`mbs_member` INT(11) DEFAULT NULL DEFAULT '0' COMMENT '회원번호(추후연동대비)',
	`mbs_member_id` VARCHAR(30) DEFAULT NULL COMMENT '회원ID(추후연동대비)',
	`mbs_member_idx` VARCHAR(2) DEFAULT NULL DEFAULT '' COMMENT '회원ID-index(추후연동대비)',

	`mbs_service` varchar(30) NOT NULL DEFAULT '' COMMENT 'sns종류',
	`mbs_sns_type` varchar(8) NOT NULL DEFAULT '' COMMENT 'sns종류',
	`mbs_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

	PRIMARY KEY (`mbs_no`),
	UNIQUE KEY `mbs_id` (`mbs_id`, `mbs_service`),
	KEY `mbs_idx` (`mbs_idx`), 

	KEY `mbs_member_idx` (`mbs_member_idx`), 

	KEY `mbs_service` (`mbs_service`), 
	KEY `mbs_sns_type` (`mbs_sns_type`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8 


CREATE TABLE IF NOT EXISTS `config_sin_allpay` (
  `csa_no` INT(11) NOT NULL AUTO_INCREMENT,
  `csa_buy_count` INT(7) NOT NULL DEFAULT '0' COMMENT '구매화수', 
  `csa_buy_than` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '구매화수타입(y:이상, n:미만)', 

  `csa_point` INT(7) NOT NULL DEFAULT '0' COMMENT '구매시 지급 보너스 코인', 

  `csa_date_type` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '기간타입(y:기간만적용, n:무기간, m:매월&기간:날짜빈칸시무기간, w:매주:날짜빈칸시무기간)',
  `csa_state`  VARCHAR(2) NULL DEFAULT 'n' COMMENT '상태(y:진행중, n:중지, r:예약)',

  `csa_date_start` VARCHAR(20) DEFAULT NULL DEFAULT '' COMMENT '기간시작',
  `csa_date_end` VARCHAR(20) DEFAULT NULL DEFAULT '' COMMENT '기간마감',

  `csa_holiday` TINYINT(4) DEFAULT NULL COMMENT '매월특정일',
  `csa_holiweek` TINYINT(4) DEFAULT NULL COMMENT '매주특정요일',

  `csa_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`csa_no`),

  KEY `csa_buy_count` (`csa_buy_count`), 
  KEY `csa_buy_than` (`csa_buy_than`), 
  
  KEY `csa_point` (`csa_point`), 

  KEY `csa_date_type` (`csa_date_type`), 
  KEY `csa_state` (`csa_state`), 

  UNIQUE KEY `csa_holiday` (`csa_holiday`), 
  UNIQUE KEY `csa_holiweek` (`csa_holiweek`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;








CREATE TABLE IF NOT EXISTS `pg_channel_kcp` (
  `kcp_no` INT(11) NOT NULL AUTO_INCREMENT,
  `kcp_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `kcp_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',
  `kcp_recharge` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '캐쉬포인트 상품명_이벤트명',
  `kcp_amt` VARCHAR(12) NOT NULL DEFAULT '' COMMENT '거래금액',
  `kcp_payway` VARCHAR(12) NOT NULL DEFAULT '' COMMENT '결제수단(빈칸:에러, mobl:폰, card:카드, acnt:계좌이체, tikt_1001:도서문화상품권, tikt_1002:해피머니, tikt_1003:문화상품권)',

  `res_cd` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '결과코드',
  `res_msg` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '결과메시지',
  `res_en_msg` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '영문 결과메시지',
  `tno` VARCHAR(14) NOT NULL DEFAULT '' COMMENT 'NHN KCP 거래 고유번호', 
  `amount` VARCHAR(12) NOT NULL DEFAULT '' COMMENT '결제 금액', 
  `escw_yn` VARCHAR(1) NOT NULL DEFAULT '' COMMENT '에스크로 결제여부', 
  `app_time` VARCHAR(14) NOT NULL DEFAULT '' COMMENT '결제 건의 결제(승인) 시간', 

  `card_cd` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '카드-결제 건의 발급 사 코드',
  `card_name` VARCHAR(32) NOT NULL DEFAULT '' COMMENT '카드-결제 건의 발급 사 명',
  `card_no` VARCHAR(16) NOT NULL DEFAULT '' COMMENT '카드-결제 건의 카드번호', 
  `app_no` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '카드-결제 건의 승인번호',
  `noinf` VARCHAR(1) NOT NULL DEFAULT '' COMMENT '카드-결제 건의 무이자 여부', 
  `quota` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '카드-결제 건의 할부 기간',
  `card_mny` VARCHAR(12) NOT NULL DEFAULT '' COMMENT '카드-결제 건의 총 결제 금액 중 신용카드 결제금액',
  `coupon_mny` VARCHAR(12) NOT NULL DEFAULT '' COMMENT '카드-결제 건의 쿠폰 할인',
  `partcanc_yn` VARCHAR(1) NOT NULL DEFAULT '' COMMENT '카드-결제 건의 부분취소 가능 유무', 
  `card_bin_type_01` VARCHAR(1) NOT NULL DEFAULT '' COMMENT '카드-결제 건의 카드 구분 정보',
  `card_bin_type_02` VARCHAR(1) NOT NULL DEFAULT '' COMMENT '카드-결제 건의 카드 구분 정보',

  `bank_code` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '계좌이체-결제 건의 은행코드', 
  `bank_name` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '계좌이체-결제 건의 은행 명',  
  `bk_mny` VARCHAR(12) NOT NULL DEFAULT '' COMMENT '계좌이체-결제 건의 계좌이체 결제 금액',

  `bankname` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '가상계좌-결제 건의 은행 명',
  `bankcode` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '가상계좌-결제 건의 은행코드', 
  `account` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '가상계좌-결제 건의 가상계좌 번호',  
  `va_date` VARCHAR(14) NOT NULL DEFAULT '' COMMENT '가상계좌-입금마감일',

  `van_cd` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '휴대폰-결제 건의 결제 사 코드',
  `van_id` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '휴대폰-결제 건의 실물/컨텐츠 구분',
  `commid` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '휴대폰-결제 건의 통신사 코드',
  `mobile_no` VARCHAR(11) NOT NULL DEFAULT '' COMMENT '휴대폰-결제 건의 휴대폰 번호',

  
  `pnt_amount` VARCHAR(12) NOT NULL DEFAULT '' COMMENT '포인트-결제 건의 포인트 결제 금액',
  `pnt_issue` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '포인트-결제 건의 결제 사 코드',
  `pnt_app_no` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '포인트-결제 건의 포인트 승인번호',
  `pnt_app_time` VARCHAR(14) NOT NULL DEFAULT '' COMMENT '결제 건의 포인트 승인시각',
  `pnt_receipt_gubn` VARCHAR(1) NOT NULL DEFAULT '' COMMENT '포인트-결제 건의 현금영수증 등록유무',
  `add_pnt` VARCHAR(12) NOT NULL DEFAULT '' COMMENT '포인트-결제 건의 적립/사용 포인트',
  `use_pnt` VARCHAR(12) NOT NULL DEFAULT '' COMMENT '포인트-결제 건의 가용 포인트',
  `rsv_pnt` VARCHAR(12) NOT NULL DEFAULT '' COMMENT '포인트-결제 건의 총 포인트',
  
  `tk_mny` VARCHAR(12) NOT NULL DEFAULT '' COMMENT '상품권-결제 건의 총 포인트',
  `tk_van_code` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '상품권-결제 건의 총 포인트',
  `tk_app_no` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '상품권-결제 건의 총 포인트',
  `tk_app_time` VARCHAR(14) NOT NULL DEFAULT '' COMMENT '상품권-결제 건의 총 포인트',

  `cash_authno` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '현금영수증-현금영수증 승인번호',
  `cash_no` VARCHAR(14) NOT NULL DEFAULT '' COMMENT '현금영수증-현금영수증 거래번호',

  PRIMARY KEY (`kcp_no`),
  KEY `kcp_member` (`kcp_member`), 
  KEY `tno` (`tno`), 
  KEY `kcp_member_idx` (`kcp_member_idx`), 
  KEY `kcp_recharge` (`kcp_recharge`), 
  KEY `kcp_payway` (`kcp_payway`), 
  KEY `kcp_amt` (`kcp_amt`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
*/
?>