<? include_once '_common.php'; // 공통 

/*
SELECT c.*, c_prof.cp_name as prof_name FROM comics c left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
WHERE 1 AND c.cm_service = 'y' AND cm_reg_date >= '2018-04-20 00:00:00' AND cm_reg_date <= '2018-05-11 23:59:59' 
ORDER BY cm_reg_date DESC LIMIT 0, 720;

$sql_cmnew = " SELECT c.*, 
					".$sql_week_comicno_field."
					c_prof.cp_name as prof_name 
				FROM comics c 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 $sql_cm_adult  
				AND c.cm_service = 'y'  
				AND cm_reg_date >= '".NM_TIME_M21." ".NM_TIME_HI_start."'
				AND cm_reg_date <= '".NM_TIME_YMD." ".NM_TIME_HI_end."'
				ORDER BY ".$sql_week_comicno_order." 
				cm_reg_date DESC 
*/


/*
$sql_where = "
 WHERE 1 
 AND cm.cm_service = 'y' 
 AND cm.cm_reg_date >= '".NM_TIME_MON_M1." ".NM_TIME_HI_start."'
 AND cm.cm_reg_date <= '".NM_TIME_YMD." ".NM_TIME_HI_end."' 
";
*/
$sql_where = "
 WHERE 1 
 AND cm.cm_service = 'y' 
";

// 날짜
/*
$_s_date = NM_TIME_M2;
$_e_date = NM_TIME_M1;
if($_s_date && $_e_date){ 
	$sql_where.= date_year_month($_s_date, $_e_date, 'sl.sl'); 
}
*/


$field_date_year_month = substr(NM_TIME_M1, 0, 7);	
$field_date_year_day = substr(NM_TIME_M1, 8, 2);	 
$sql_field_date = " 
 if(sl.sl_year_month='".$field_date_year_month."', 
 if(sl.sl_day='".$field_date_year_day."',1,0)
 ,0)as sl_date_order, ";

$sql_field_date_reg = " 
 if(cm.cm_reg_date >= '".NM_TIME_M21." ".NM_TIME_HI_start."', 
  if(cm.cm_reg_date <= '".NM_TIME_YMD." ".NM_TIME_HI_end."',1,0)
 ,0)as sl_date_order_reg, ";

// 3 6 9 12개월
$sql_field_date_reg2 = " 
 if(cm.cm_reg_date < '".NM_TIME_MON_M12_S." ".NM_TIME_HI_start."', 1,
   if(cm.cm_reg_date < '".NM_TIME_MON_M9_S." ".NM_TIME_HI_start."', 2,
    if(cm.cm_reg_date < '".NM_TIME_MON_M6_S." ".NM_TIME_HI_start."', 3,
	 if(cm.cm_reg_date < '".NM_TIME_MON_M3_S." ".NM_TIME_HI_start."', 4,0)
    )
   )
 )as sl_date_order_reg2, ";

/*
$sql_field_date_concat = "
 concat(sl.sl_year_month,sl.sl_day) as sl_date_concat, ";
*/

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sl_won_sum"){ 
	$_order_field = "sl_won_sum"; 
	$_order_field_add = " , sl_comics "; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
	$_order_add= "desc"; 
}

// 요일 정렬시
if($_order_field == "sl_week"){
	$_order_field_add = " , sl.sl_year_month desc "; 
	$_order_add = " , sl.sl_day desc ";
}

$sql_order = "order by sl_date_order_reg desc, sl_date_order_reg2 desc, sl_date_order desc, ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add;

// 그룹
$sql_group = " group by sl.sl_comics ";

$sql_limit = " limit 0, 100 ";

// SQL문
$sql_field = "		cm.cm_adult, cm.cm_big,
					sl.sl_comics, cm.cm_small, cm.cm_series, 

					{$sql_field_date}
					{$sql_field_date_reg}
					{$sql_field_date_reg2}

					sum(if(sl.sl_cash_point>0,sl_cash_point,0))as sl_cash_point_sum, 
					sum(if(sl.sl_cash_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_cash_point_cnt, 
					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won)as sl_cash_point_won_sum, 

					sum(if(sl.sl_point>0,sl_point,0))as sl_point_sum, 
					sum(if(sl.sl_cash_point=0 && sl.sl_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_point_cnt, 
					sum(floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_point_won_sum, 

					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_won_sum, 

					sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
					sum(sl.sl_open)as sl_open_sum 
"; 
$sql_count	= $sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$sql_count	= $sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$sql_select = "		select {$sql_field} ";
$sql_table = "		FROM sales sl 
					LEFT JOIN comics cm ON sl.sl_comics = cm.cm_no 
					LEFT JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type ";

$sql_query		= " {$sql_select} {$sql_table} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} {$sql_limit} ";

$sql_where_t	= $sql_where." AND cm.cm_adult = 'n' "; 
$sql_query_t	= " {$sql_select} {$sql_table} {$sql_where_t} {$sql_group}  ";
$sql_querys_t	= " {$sql_query_t} {$sql_order} {$sql_limit} ";

echo $sql_querys;
echo "<br/><br/><br/><br/>";
echo $sql_querys_t;
echo "<br/><br/><br/><br/>";

// 넣기전에 삭제

$db_name = " comics_ranking_sales_cmnew_auto ";
$sql_del = " TRUNCATE ".$db_name." ";
sql_query($sql_del);

$result = sql_query($sql_querys);
for($i=1; $row = sql_fetch_array($result); $i++) {
	$sql_insert = " 
	 INSERT INTO ".$db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
	 '0', '".$row['cm_adult']."', '".$row['sl_comics']."', '".$row['cm_big']."', '".$i."', '".NM_TIME_YMDHIS."' );	
	";
	sql_query($sql_insert);
}

$result_t = sql_query($sql_querys_t);
for($j=1; $row_t = sql_fetch_array($result_t); $j++) {
	$sql_insert = " 
	 INSERT INTO ".$db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
	 '0_t', '".$row_t['cm_adult']."', '".$row_t['sl_comics']."', '".$row_t['cm_big']."', '".$j."', '".NM_TIME_YMDHIS."' );	
	";
	sql_query($sql_insert);
}





?>