<? include_once '_common.php'; // 공통 

$sql_where = "
 WHERE 1 
 AND cm.cm_service = 'y' 
";

// 날짜
$_s_date = NM_TIME_M1;
$_e_date = NM_TIME_M1;
if($_s_date && $_e_date){ 
	$sql_where.= date_year_month($_s_date, $_e_date, 'sl.sl'); 
}

/*
$field_date_year_month = substr(NM_TIME_M1, 0, 7);	
$field_date_year_day = substr(NM_TIME_M1, 8, 2);	 
$sql_field_date = " 
 if(sl.sl_year_month='".$field_date_year_month."', 
 if(sl.sl_day='".$field_date_year_day."',1,0)
 ,0)as sl_date_order, ";
*/

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sl_won_sum"){ 
	$_order_field = "sl_won_sum"; 
	$_order_field_add = " , sl_open_sum "; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
	$_order_add= "desc"; 
}

// 요일 정렬시
if($_order_field == "sl_week"){
	$_order_field_add = " , sl.sl_year_month desc "; 
	$_order_add = " , sl.sl_day desc ";
}

$sql_order = "order by ".$_order_field." ".$_order." ".$_order_field_add." ".$_order_add." , sl_comics ASC ";

// 그룹
$sql_group = " group by cm.cm_no ";

$sql_limit = "";

// SQL문
$sql_field = "		cm.cm_adult, cm.cm_big, cm.cm_no,
					sl.sl_comics, cm.cm_small, cm.cm_series, cm.cm_service, 

					sum(if(sl.sl_cash_point>0,sl_cash_point,0))as sl_cash_point_sum, 
					sum(if(sl.sl_cash_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_cash_point_cnt, 
					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won)as sl_cash_point_won_sum, 

					sum(if(sl.sl_point>0,sl_point,0))as sl_point_sum, 
					sum(if(sl.sl_cash_point=0 && sl.sl_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_point_cnt, 
					sum(floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_point_won_sum, 

					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_won_sum, 

					sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
					sum(sl.sl_open)as sl_open_sum 
"; 
$sql_count	= $sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$sql_count	= $sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$sql_select = "		select {$sql_field} ";
$sql_table = "		FROM comics cm 
					LEFT JOIN sales sl ON sl.sl_comics = cm.cm_no 
					LEFT JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type ";
// 0 전체
$sql_query[0]	= " {$sql_select} {$sql_table} {$sql_where} {$sql_group}  ";
$sql_querys[0]	= " ".$sql_query[0]." {$sql_order} {$sql_limit} ";

// 청소년
$sql_where_t	= $sql_where." AND cm.cm_adult = 'n' "; 
$sql_query_t[0]	= " {$sql_select} {$sql_table} {$sql_where_t} {$sql_group}  ";
$sql_querys_t[0]= " ".$sql_query_t[0]." {$sql_order} {$sql_limit} ";

// 장르별
for($s=1; $s<=8; $s++){
// 전체
	${'sql_where_small_'.$s} = $sql_where." AND cm.cm_small = '".$s."' "; 
	$sql_query[$s]	= " {$sql_select} {$sql_table} ".${'sql_where_small_'.$s}." {$sql_group}  ";
	$sql_querys[$s]	= " ".$sql_query[$s]." {$sql_order} {$sql_limit} ";

	// 청소년만
	${'sql_where_small_t_'.$s} = $sql_where." AND cm.cm_small = '".$s."' AND cm.cm_adult = 'n' "; 
	$sql_query_t[$s]	= " {$sql_select} {$sql_table} ".${'sql_where_small_t_'.$s}." {$sql_group}  ";
	$sql_querys_t[$s]	= " ".$sql_query_t[$s]." {$sql_order} {$sql_limit} ";

	if($s==4){
	
	echo $sql_querys[$s].";<br/><br/>";
	echo $sql_querys_t[$s].";<br/>";
	echo "<br/><br/><br/><br/>";
	}
}


	// echo $sql_querys[0].";<br/><br/>";
	// echo $sql_querys_t[0].";<br/>";
	// echo "<br/><br/><br/><br/>";

// die;

// 넣기전에 삭제

$db_name = " comics_ranking_sales_cmgenre_auto ";
$sql_del = " TRUNCATE ".$db_name." ";
sql_query($sql_del);

for($t=0; $t<=8; $t++){ 
	// 확인
	$cmgenre_arr[$t] = array(); 
	$cmgenre_t_arr[$t] = array(); 
}

for($s=0; $s<=8; $s++){
	$result = sql_query($sql_querys[$s]);
	for($i=1; $row = sql_fetch_array($result); $i++) {
		$sql_insert = " 
		 INSERT INTO ".$db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
		 '".$s."', '".$row['cm_adult']."', '".$row['cm_no']."', '".$row['cm_big']."', '".$i."', '".NM_TIME_YMDHIS."' );	
		";
		sql_query($sql_insert);
		array_push($cmgenre_arr[$s],$row['cm_no']);
	}

	$result_t = sql_query($sql_querys_t[$s]);
	for($j=1; $row_t = sql_fetch_array($result_t); $j++) {
		$sql_insert = " 
		 INSERT INTO ".$db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
		 '".$s."_t', '".$row_t['cm_adult']."', '".$row_t['cm_no']."', '".$row_t['cm_big']."', '".$j."', '".NM_TIME_YMDHIS."' );	
		";
		sql_query($sql_insert);
		array_push($cmgenre_t_arr[$s], $row_t['cm_no']);
	}
}

// 판대 안된 작품 추가
for($s=0; $s<=8; $s++){
	$sql_small_where = $sql_small_where_arr = "";
	
	$sql_small_where = " AND cm_service = 'y' ";
	if($s > 0){ $sql_small_where.= " AND cm_small = '".$s."' "; }
	$sql_small_where_arr = " AND cm_no not in (".implode(", ",$cmgenre_arr[$s]).") ";
	$sql_small = " select * FROM comics WHERE 1 {$sql_small_where} {$sql_small_where_arr} order by cm_no DESC ";
	$sql_small0 = " select * FROM comics WHERE 1 {$sql_small_where} order by cm_no DESC ";

	$sql_small_where_t = $sql_small_where_arr_t = "";
	$sql_small_where_t = " AND cm_service = 'y' AND cm_adult='n' ";
	if($s > 0){ $sql_small_where_t.= " AND cm_small = '".$s."' "; }
	$sql_small_where_arr_t = " AND cm_no not in (".implode(", ",$cmgenre_t_arr[$s]).") ";
	$sql_small_t = " select * FROM comics WHERE 1 {$sql_small_where_t} {$sql_small_where_arr} order by cm_no DESC ";
	$sql_small_t0 = " select * FROM comics WHERE 1 {$sql_small_where_t} order by cm_no DESC ";

	$cmgenre_arr_count = count($cmgenre_arr[$s]);
	if($cmgenre_arr_count > 0){		
		$result = sql_query($sql_small);
		for($i=$cmgenre_arr_count+1; $row = sql_fetch_array($result); $i++) {
			$sql_insert = " 
			 INSERT INTO ".$db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
			 '".$s."', '".$row['cm_adult']."', '".$row['cm_no']."', '".$row['cm_big']."', '".$i."', '".NM_TIME_YMDHIS."' );	
			";
			sql_query($sql_insert);
		}
	}else if($cmgenre_arr_count == 0){
		$result = sql_query($sql_small0);
		for($i=1; $row = sql_fetch_array($result); $i++) {
			$sql_insert = " 
			 INSERT INTO ".$db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
			 '".$s."', '".$row['cm_adult']."', '".$row['cm_no']."', '".$row['cm_big']."', '".$i."', '".NM_TIME_YMDHIS."' );	
			";
			sql_query($sql_insert);

		}
	}

	$cmgenre_t_arr_count = count($cmgenre_t_arr[$s]);
	if($cmgenre_t_arr_count > 0){
		$result_t = sql_query($sql_small_t);
		for($j=$cmgenre_t_arr_count+1; $row_t = sql_fetch_array($result_t); $j++) {
			$sql_insert = " 
			 INSERT INTO ".$db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
			 '".$s."_t', '".$row_t['cm_adult']."', '".$row_t['cm_no']."', '".$row_t['cm_big']."', '".$j."', '".NM_TIME_YMDHIS."' );	
			";
			sql_query($sql_insert);
		}
	}else if($cmgenre_t_arr_count == 0){
		$result_t = sql_query($sql_small_t0);
		for($j=1; $row_t = sql_fetch_array($result_t); $j++) {
			$sql_insert = " 
			 INSERT INTO ".$db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
			 '".$s."_t', '".$row_t['cm_adult']."', '".$row_t['cm_no']."', '".$row_t['cm_big']."', '".$j."', '".NM_TIME_YMDHIS."' );	
			";
			sql_query($sql_insert);

		}
	}	
}





?>