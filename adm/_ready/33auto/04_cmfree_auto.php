<? include_once '_common.php'; // 공통 


/* 무료화 데이터 안맞음 - 재정리 무료화 표기 속성 추가 */

for($i=1; $i<=3; $i++){
	$sql_ce = " select *, count(*)as ce_free_cnt
				from comics_episode_".$i." 
				where ce_chapter>0 and ce_service_state='y' and ce_pay=0 group by ce_comics ";
	
	$result_ce = sql_query($sql_ce);
	for($j=1; $row_ce = sql_fetch_array($result_ce); $j++) {
		$sql_cm = " select * from comics where cm_service='y' and cm_no='".$row_ce['ce_comics']."' ";
		$row_cm = sql_fetch($sql_cm);
		
		$save_free_cnt = 0; 
		if(intval($row_cm['cm_no'])>0){ // 실제 서비스 하는 애들만
			if(intval($row_cm['cm_episode_total'])>0){ // 에피소드 화 있는 작품만
				if(intval($row_ce['ce_free_cnt']) > 0 || intval($row_cm['cm_free']) > 0){

					// 설정 - 에피소드 무료화
					$save_free_cnt = $row_ce['ce_free_cnt'];

					// 설정 - 코믹스 무료 1부터 해당 수까지 무료제공
					if($row_cm['cm_free'] > $save_free_cnt){
						$save_free_cnt = $row_cm['cm_free'];
					}

					// 설정 - 코믹스 최고화 넘을 시 최고화 이하로...
					if($save_free_cnt > $row_cm['cm_episode_total']){
						$save_free_cnt = $row_cm['cm_episode_total'];
					}

					echo "<br/>";
					echo $sql_cm.";";
					echo " => ".$row_ce['ce_free_cnt']." / ".$row_cm['cm_free']." / ".$row_cm['cm_series']." / ".$row_cm['cm_episode_total']." /  =============>>".$save_free_cnt;
					echo "<br/><br/>";

					// update
					$sql_update = " update comics set cm_free_cnt='".$save_free_cnt."' where cm_service='y' and cm_no='".$row_ce['ce_comics']."' ";
					sql_query($sql_update);
				}
			}
		}
	}
}


$sql_where = "
 WHERE 1 
 AND cm.cm_service = 'y' 
";

// 날짜
$_s_date = NM_TIME_M1;
$_e_date = NM_TIME_M1;
if($_s_date && $_e_date){ 
	$sql_where.= date_year_month($_s_date, $_e_date, 'sl.sl'); 
}


$sql_field_free = " 
 if(cm.cm_free_cnt >= 100, 3,
   if(cm.cm_free_cnt >= 10, 4,
    if(cm.cm_free_cnt >= 4, 2,
	 if(cm.cm_free_cnt >= 3, 1,0)
	)
   )
 )as cm_free_order, ";

/*
$sql_field_free = " 
 if(cm.cm_free_cnt >=3,4,cm.cm_free_cnt) as cm_free_order, ";
*/

// 정렬
$_order_field_add = $_order_add = "";
if($_order_field == null || $_order_field == "" || $_order_field == "sl_won_sum"){ 
	$_order_field = "sl_won_sum"; 
	$_order_field_add = " , sl_comics "; 
}
if($_order == null || $_order == ""){ 
	$_order = "desc"; 
	$_order_add= "desc"; 
}

// 요일 정렬시
if($_order_field == "sl_week"){
	$_order_field_add = " , sl.sl_year_month desc "; 
	$_order_add = " , sl.sl_day desc ";
}

$sql_order = "order by cm_free_order DESC, ".$_order_field." ".$_order.", sl_open_sum DESC ".$_order_field_add." ".$_order_add;

// 그룹
$sql_group = " group by sl.sl_comics ";

$sql_limit = " limit 0, 100 ";

// SQL문
$sql_field = "		cm.cm_adult, cm.cm_big,
					sl.sl_comics, cm.cm_small, cm.cm_series, 

					{$sql_field_free}

					sum(if(sl.sl_cash_point>0,sl_cash_point,0))as sl_cash_point_sum, 
					sum(if(sl.sl_cash_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_cash_point_cnt, 
					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won)as sl_cash_point_won_sum, 

					sum(if(sl.sl_point>0,sl_point,0))as sl_point_sum, 
					sum(if(sl.sl_cash_point=0 && sl.sl_point>0,sl.sl_all_pay+sl.sl_pay,0))as sl_point_cnt, 
					sum(floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_point_won_sum, 

					sum(if(sl.sl_cash_point>0,sl_cash_point,0)*cup_won+floor(if(sl.sl_point>0,sl_point,0)*cup_won/".NM_POINT."))as sl_won_sum, 

					sum(sl.sl_all_pay+sl.sl_pay)as sl_pay_sum, 
					sum(sl.sl_open)as sl_open_sum 
"; 
$sql_count	= $sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$sql_count	= $sql_field. " , count( distinct sl.sl_comics ) as cnt ";

$sql_select = "		select {$sql_field} ";
$sql_table = "		FROM sales sl 
					LEFT JOIN comics cm ON sl.sl_comics = cm.cm_no 
					LEFT JOIN config_unit_pay cup ON sl.sl_cash_type = cup.cup_type ";

$sql_query		= " {$sql_select} {$sql_table} {$sql_where} {$sql_group}  ";

$sql_querys		= " {$sql_query} {$sql_order} {$sql_limit} ";

$sql_where_t	= $sql_where." AND cm.cm_adult = 'n' "; 
$sql_query_t	= " {$sql_select} {$sql_table} {$sql_where_t} {$sql_group}  ";
$sql_querys_t	= " {$sql_query_t} {$sql_order} {$sql_limit} ";

echo $sql_querys;
echo "<br/><br/><br/><br/>";
echo $sql_querys_t;
echo "<br/><br/><br/><br/>";

// 넣기전에 삭제

$db_name = " comics_ranking_sales_cmfree_auto ";
$sql_del = " TRUNCATE ".$db_name." ";
sql_query($sql_del);

$result = sql_query($sql_querys);
for($i=1; $row = sql_fetch_array($result); $i++) {
	$sql_insert = " 
	 INSERT INTO ".$db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
	 '0', '".$row['cm_adult']."', '".$row['sl_comics']."', '".$row['cm_big']."', '".$i."', '".NM_TIME_YMDHIS."' );	
	";
	sql_query($sql_insert);
}

$result_t = sql_query($sql_querys_t);
for($j=1; $row_t = sql_fetch_array($result_t); $j++) {
	$sql_insert = " 
	 INSERT INTO ".$db_name." ( crs_class, crs_adult, crs_comics, crs_big, crs_ranking, crs_date )VALUES( 
	 '0_t', '".$row_t['cm_adult']."', '".$row_t['sl_comics']."', '".$row_t['cm_big']."', '".$j."', '".NM_TIME_YMDHIS."' );	
	";
	sql_query($sql_insert);
}





?>