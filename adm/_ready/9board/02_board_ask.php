<?
include_once '_common.php'; // 공통

/* ingaha 아이디로 등록 */
$mb_id = get_member('ingaha');

$dbtable = "board_ask";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

$sql_chk_cnt = "SELECT count(*) as cnt FROM `m_mangazzang_cp`.`mentomen` ";
$row_chk_cnt = sql_count($sql_chk_cnt, 'cnt');

echo $sql2_cnt."<br/>";

echo $sql_chk_cnt."<br/>";

$sql_cp_board = "SELECT * FROM  `m_mangazzang_cp`.`mentomen` ORDER BY `men_no` ASC ";
$result_cp_board = sql_query($sql_cp_board);
echo $sql_cp_board."<br/>";

$sql_cp_list = array('ba_no', 'ba_member', 'ba_member_idx');
array_push($sql_cp_list, 'ba_title', 'ba_request', 'ba_date');
array_push($sql_cp_list, 'ba_mode', 'ba_state');
array_push($sql_cp_list, 'ba_admin_member', 'ba_admin_member_idx', 'ba_admin_answer', 'ba_admin_date');

$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_board)) {
		$row_cp['bh_no'] = $row_cp['men_no'];
		
		$men_q_id = get_member($row_cp['men_q_id']);
		$row_cp['ba_member'] = $men_q_id['mb_no'];
		$row_cp['ba_member_idx'] = $men_q_id['mb_idx'];
		if($men_q_id['mb_no'] == '' && $men_q_id['mb_idx'] == '' && $row_cp['men_re'] == 4){
			$row_cp['ba_member'] = $mb_id['mb_no'];
			$row_cp['ba_member_idx'] = $mb_id['mb_idx'];
		}

		$row_cp['ba_title'] = base_filter($row_cp['men_q_title']);
		$row_cp['ba_request'] = base_filter($row_cp['men_q_memo']);
		$row_cp['ba_date'] = $row_cp['men_q_date'];
		
		$row_cp['ba_mode'] = $row_cp['men_re'];
		if($row_cp['men_re'] != 4){
			$row_cp['ba_mode'] = $row_cp['men_q_con'];
		}
		$row_cp['ba_state'] = $row_cp['men_read'];

		$row_cp['ba_admin_member'] = $mb_id['mb_no'];
		$row_cp['ba_admin_member_idx'] = $mb_id['mb_idx'];
		$row_cp['ba_admin_answer'] = base_filter($row_cp['men_a_memo']);
		$row_cp['ba_admin_date'] = $row_cp['men_a_date'];



		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
				$sql2_insert.= 'NULL, ';
			}else{
				$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
			}
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>