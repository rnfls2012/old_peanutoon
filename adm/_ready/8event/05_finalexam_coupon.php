<?
include_once '_common.php'; // 공통

die;

// 교시번호만 지정하면 됨
$sql_no = 3;

// 1교시, 2교시, 3교시 선착순 할인권 등록

$sql_insert_er1 = "
INSERT INTO event_randombox (er_name, er_discount_rate_list, er_date_start, er_date_start_hour, er_date_end, er_date_end_hour, er_state, er_available_date_start, er_available_date_start_hour, er_available_date_end, er_available_date_end_hour, er_available_state, er_type, er_event_type, er_first_count, er_reg_date, er_mod_date) VALUES ('전국덕후학력평가 1교시 참여 보상', '4', '2018-07-24', '00', '2018-07-26', '23', 'n', '2018-07-26', '00', '2018-08-20', '23', 'y', 'd', 'f', 20000, '".NM_TIME_YMDHIS."', '');
";

$sql_insert_er2 = "
INSERT INTO event_randombox (er_name, er_discount_rate_list, er_date_start, er_date_start_hour, er_date_end, er_date_end_hour, er_state, er_available_date_start, er_available_date_start_hour, er_available_date_end, er_available_date_end_hour, er_available_state, er_type, er_event_type, er_first_count, er_reg_date, er_mod_date) VALUES ('전국덕후학력평가 2교시 참여 보상', '4', '2018-07-27', '00', '2018-07-29', '23', 'n', '2018-07-29', '00', '2018-08-20', '23', 'y', 'd', 'f', 20000, '".NM_TIME_YMDHIS."', '');
";

$sql_insert_er3 = "
INSERT INTO event_randombox (er_name, er_discount_rate_list, er_date_start, er_date_start_hour, er_date_end, er_date_end_hour, er_state, er_available_date_start, er_available_date_start_hour, er_available_date_end, er_available_date_end_hour, er_available_state, er_type, er_event_type, er_first_count, er_reg_date, er_mod_date) VALUES ('전국덕후학력평가 3교시 참여 보상', '4', '2018-07-30', '00', '2018-08-01', '23', 'n', '2018-08-01', '00', '2018-08-20', '23', 'y', 'd', 'f', 20000, '".NM_TIME_YMDHIS."', '');
";

//할인권 검색
$sql_er1 = " 
 select * from event_randombox where 1 
 AND er_date_start = '2018-07-24' AND er_date_end = '2018-07-26' 
 AND er_available_date_end = '2018-08-20' 
";
$sql_er2 = " 
 select * from event_randombox where 1 
 AND er_date_start = '2018-07-27' AND er_date_end = '2018-07-29' 
 AND er_available_date_end = '2018-08-20' 
";
$sql_er3 = " 
 select * from event_randombox where 1 
 AND er_date_start = '2018-07-30' AND er_date_end = '2018-08-01' 
 AND er_available_date_end = '2018-08-20' 
";

// 모의고사 응시자들
$sql_efm1 = " select * from event_finalexam_member where 1 AND efm_efp_no1 = '1' ORDER BY efm_no desc "; // 1교시 
$sql_efm2 = " select * from event_finalexam_member where 1 AND efm_efp_no2 = '2' ORDER BY efm_no desc "; // 2교시 
$sql_efm3 = " select * from event_finalexam_member where 1 AND efm_efp_no3 = '3' ORDER BY efm_no desc "; // 3교시 


$sql_arr = array(${'sql_er'.$sql_no},${'sql_insert_er'.$sql_no},${'sql_er'.$sql_no},${'sql_efm'.$sql_no});

/*
$sql_erc = "
 INSERT INTO event_randombox_coupon 
 (erc_er_no, erc_ercm_no, erc_member, erc_member_ndx, erc_member_idx, erc_discount_rate, erc_er_type, erc_state, erc_use, erc_crp_no, erc_payway_no, erc_use_date, erc_date) 
 VALUES (?er_no, 4, ?mb_no, '', '?mb_idx', 15, 'd', 'n', 'n', NULL, NULL, '', NM_TIME_YMDHIS);
";
*/

// print_r($sql_arr);
// die;

// 실행
$while_1 = $while_2 = array();
foreach ($sql_arr as $sql_key => $sql_val) {
	if($sql_key == 0) { 
		$result_1 = sql_query($sql_val);
		while ($row_1 = sql_fetch_array($result_1)) { 
			array_push($while_1, $row_1); 
		}
	}
	if($sql_key == 1) { 
		if(count($while_1) == 0){ 
			sql_query($sql_val); 
		}else{
			echo "등록되어 있습니다."."<br/>"; 
			echo "확인"."<br/>"; 
			// echo $sql_arr[1].";<br/>"; //-> insert문이라서
			echo $sql_arr[2].";<br/>"; 
			echo $sql_arr[3].";<br/>"; 
			$sql_erc_ch = "select * FROM event_randombox_coupon WHERE erc_er_no='".$while_1[0]['er_no']."';";
			echo $sql_erc_ch."<br/>"; 
			echo "삭제"."<br/><br/>"; 
			echo str_replace("select *", "delete ", $sql_arr[2]).";<br/><br/>"; 
			echo str_replace("select *", "delete ", $sql_erc_ch).";<br/><br/>"; 
			die;
		}
	}
	if($sql_key == 2) { 
		$result_2 = sql_query($sql_val);
		while ($row_2 = sql_fetch_array($result_2)) { array_push($while_2, $row_2); }
	}
	if($sql_key == 3) { 
		$result_3 = sql_query($sql_val);
		while ($row_3 = sql_fetch_array($result_3)) {
				$sql_erc = "
				 INSERT INTO event_randombox_coupon 
				 (erc_er_no, erc_ercm_no, erc_member, erc_member_ndx, erc_member_idx, erc_discount_rate, erc_er_type, erc_state, erc_use, erc_crp_no, erc_payway_no, erc_use_date, erc_date) 
				 VALUES (".$while_2[0]['er_no'].", 4, '".$row_3['efm_member']."', '', '".$row_3['efm_member_idx']."', 15, 'd', 'y', 'n', NULL, NULL, '', '".NM_TIME_YMDHIS."');
				";
				sql_query($sql_erc);
				// echo $sql_erc."<br/>";
		}
	}
}

/*

Array ( [0] => INSERT INTO event_randombox (er_name, er_discount_rate_list, er_date_start, er_date_start_hour, er_date_end, er_date_end_hour, er_state, er_available_date_start, er_available_date_start_hour, er_available_date_end, er_available_date_end_hour, er_available_state, er_type, er_event_type, er_first_count, er_reg_date, er_mod_date) VALUES ('피너툰 모의고사 1교시 응모 이벤트', '4', '2018-07-24', '00', '2018-07-26', '23', 'n', '2018-07-26', '00', '2018-08-20', '23', 'y', 'd', 'f', 20000, '2018-07-26 11:15:51', ''); [1] => select * from event_randombox where 1 AND er_date_start = '2018-07-24' AND er_date_end = '2018-07-26' AND er_available_date_end = '2018-08-20' [2] => select * from event_finalexam_member where 1 AND efm_efp_no1 = '1' ORDER BY efm_no desc )
*/


?>