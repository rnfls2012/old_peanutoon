<?
include_once '_common.php'; // 공통


// common

echo "추석세팅되어서 초기화 불가능";
die;

	$config_date = array();
	$config_date_start = 22;
	$config_date_end   = 26;

	for($d=$config_date_start; $d<=$config_date_end; $d++){
		array_push($config_date, '09-'.$d);
	}

// coupon

	// coupon 초기화

		$config_coupon_use_count = 10000000;
		$coupon_t_w = "from event_randombox where er_first_count=".$config_coupon_use_count.";";
		$sql = "select * ".$coupon_t_w ;
		$result = sql_query($sql);
		for($i=0; $row = sql_fetch_array($result); $i++){
			$sql_del1 = "delete from event_randombox_coupon where erc_er_no=".$row['er_no'].";";
			// echo $sql_del1."<br/>";
			sql_query($sql_del1);
		}
		$sql_del = "delete ".$coupon_t_w ;
		// echo $sql_del."<br/>";
		sql_query($sql_del);

	// coupon - setting

		$config_coupon_name = "피너툰 추석 대잔치 :: ";
		$config_coupon_date_s = "2018-".$config_date[0];
		$config_coupon_date_e = "2018-09-26";

		foreach($config_date as $date_key => $date_val){
			$date_count = $date_key + 1;

			$coupon_get_date_type = get_date_type('', '2018-'.$date_val, '2018-'.$date_val);
			$er_state = $coupon_get_date_type['state'];

			$sql_insert = "
			 INSERT INTO event_randombox (er_name, er_discount_rate_list, er_date_start, er_date_start_hour, er_date_end, er_date_end_hour, 
			 er_state, 
			 er_available_date_start, er_available_date_start_hour, er_available_date_end, er_available_date_end_hour, er_available_state, er_type, er_event_type, er_first_count, er_reg_date, er_mod_date) 
			 VALUES ('피너툰 추석 대잔치 :: ".$date_count."일차', '5', '2018-".$date_val."', '00', '2018-".$date_val."', '23', 
			 '".$er_state."', 
			 '".$config_coupon_date_s."', '00', '".$config_coupon_date_e."', '23', 'y', 'd', 'f', ".$config_coupon_use_count.", '".NM_TIME_YMDHIS."', '');";
			 // echo $sql_insert."<br/>";
			 sql_query($sql_insert);
		}

// time

	// 작품 에피소드 데이터 수정
	sql_comics_episode_2_date_update();

	// time 초기화

		$config_time_name = "2018년 추석 타입세일 이벤트";
		
		$coupon_t_w = "from event_pay where ep_text='".$config_time_name."';";
		$sql = "select * ".$coupon_t_w ;
		$result = sql_query($sql);
		for($i=0; $row = sql_fetch_array($result); $i++){
			$sql_del1 = "delete from event_pay_comics where epc_ep_no=".$row['ep_no'].";";
			// echo $sql_del1."<br/>";
			sql_query($sql_del1);
		}
		$sql_del = "delete ".$coupon_t_w ;
		// echo $sql_del."<br/>";
		sql_query($sql_del);

	// time - setting
		$time_data = array();
		foreach($config_date as $date_key => $date_val){
			array_push($time_data, array($date_val));
		}
		
		foreach($config_date as $date_key => $date_val){
			array_push($time_data[$date_key], array('am'));
			array_push($time_data[$date_key], array('pm'));
		}

		foreach($time_data as $time_key => $time_val){
			/*
			echo $time_val[0]."<br/>";
			echo $time_val[1][0]."<br/>";
			echo $time_val[2][0]."<br/>";
			echo $time_key."<br/><br/>";
			*/
			// array_push($time_data[$time_key][1], array(코믹스번호,'무료yn',무료화수,'세일yn',세일화수,세일땅콩));
			switch($time_key){
				case 0:					
					// am
					array_push($time_data[$time_key][1], array(1252, 'n', 0 ,'y', 114 , 2));
					array_push($time_data[$time_key][1], array(  36, 'n', 0 ,'y',  20  ,1));
					array_push($time_data[$time_key][1], array(3162, 'y', 3 ,'n',   0  ,0));
					array_push($time_data[$time_key][1], array(2354, 'y', 3 ,'n',   0  ,0));
					// pm
					array_push($time_data[$time_key][2], array(1187, 'n', 0 ,'y',  40 , 2));
					array_push($time_data[$time_key][2], array(2718, 'n', 0 ,'y',  20 , 1));
					array_push($time_data[$time_key][2], array(2210, 'n', 0 ,'y',  26 , 1));
					array_push($time_data[$time_key][2], array(2575, 'y', 3 ,'n',   0  ,0));
					array_push($time_data[$time_key][2], array(2551, 'y', 3 ,'n',   0  ,0));
				break;
				
				case 1:			
					// am
					array_push($time_data[$time_key][1], array(2206, 'y', 3 ,'n',   0  ,0));
					array_push($time_data[$time_key][1], array(2476, 'n', 0 ,'y',  30 , 1));
					array_push($time_data[$time_key][1], array(2133, 'n', 0 ,'y',  36 , 1));
					array_push($time_data[$time_key][1], array(2363, 'y', 3 ,'n',   0  ,0));
					// pm
					array_push($time_data[$time_key][2], array(2207, 'n', 0 ,'y',  45 , 2));
					array_push($time_data[$time_key][2], array( 866, 'n', 0 ,'y',  12 , 1));
					array_push($time_data[$time_key][2], array(2611, 'y', 3 ,'n',   0  ,0));
					array_push($time_data[$time_key][2], array(2552, 'y', 2 ,'n',   0  ,0));
				break;
				
				case 2:			
					// am
					array_push($time_data[$time_key][1], array( 805, 'n', 0 ,'y',  93 , 2));
					array_push($time_data[$time_key][1], array(2530, 'n', 0 ,'y',  40 , 1));
					array_push($time_data[$time_key][1], array(2803, 'y', 3 ,'n',   0  ,0));
					// pm
					array_push($time_data[$time_key][2], array( 772, 'n', 0 ,'y',  48 , 2));
					array_push($time_data[$time_key][2], array(1241, 'n', 0 ,'y',  40 , 1));
					array_push($time_data[$time_key][2], array(2636, 'y', 3 ,'n',   0  ,0));
					array_push($time_data[$time_key][2], array(2148, 'y', 3 ,'n',   0  ,0));
				break;
				
				case 3:			
					// am
					array_push($time_data[$time_key][1], array(2082, 'n', 0 ,'y',  30 , 2));
					array_push($time_data[$time_key][1], array(1278, 'n', 0 ,'y',  19 , 1));
					array_push($time_data[$time_key][1], array(2529, 'y', 3 ,'n',   0  ,0));
					// pm
					array_push($time_data[$time_key][2], array(2873, 'y', 4 ,'n',   0  ,0));
					array_push($time_data[$time_key][2], array(2212, 'n', 0 ,'y',  37 , 1));
					array_push($time_data[$time_key][2], array(2322, 'n', 0 ,'y',  29 , 1));
					array_push($time_data[$time_key][2], array(2107, 'y', 3 ,'n',   0  ,0));
				break;
				
				case 4:			
					// am
					array_push($time_data[$time_key][1], array( 446, 'n', 0 ,'y',  66 , 2));
					array_push($time_data[$time_key][1], array(1846, 'n', 0 ,'y',  38 , 1));
					array_push($time_data[$time_key][1], array(2553, 'y', 3 ,'n',   0  ,0));
					// pm
					array_push($time_data[$time_key][2], array(1308, 'n', 0 ,'y',  70 , 2));
					array_push($time_data[$time_key][2], array(2726, 'y', 3 ,'n',   0  ,0));
					array_push($time_data[$time_key][2], array(2310, 'y', 4 ,'n',   0  ,0));
					array_push($time_data[$time_key][2], array(2290, 'y', 8 ,'n',   0  ,0));
				break;
			}
		}

		
		// echo "<br/><br/>";

		foreach($time_data as $time_key => $time_val){
			// $time_val[0] => 날짜
			// echo $time_val[1]."<br/>"; => am
			// echo $time_val[2]."<br/><br/><br/>"; => pm
			
			// echo "<br/> 날짜 : ".$time_val[0]."<br/>";
			foreach($time_val[1] as $time_key1 => $time_val1){
				$ep_no = 0; // 초기화
				if(!is_array($time_val1)){
					// echo " 낮: ".$time_val1."<br/>";
					sql_event_pay_insert($time_val[0], $time_val1, $config_time_name);
					// event_pay
				}else{
					// echo $time_val[1][0]."<br/>"; => am
					$ep_no = sql_event_pay($time_val[0], $time_val[1][0], $config_time_name);
					if($ep_no > 0){
						$epc_order = intval($time_key1)-1;
						sql_event_pay_comics_insert($ep_no, $epc_order, $time_val1);
					}
				}
			}
			foreach($time_val[2] as $time_key2 => $time_val2){
				if(!is_array($time_val2)){
					// echo " 밤: ".$time_val2."<br/>";
					sql_event_pay_insert($time_val[0], $time_val2, $config_time_name);
				}else{
					// echo $time_val[2][0]."<br/>"; => pm
					$ep_no = sql_event_pay($time_val[0], $time_val[2][0], $config_time_name);
					if($ep_no > 0){
						$epc_order = intval($time_key2)-1;
						sql_event_pay_comics_insert($ep_no, $epc_order, $time_val2);
					}
				}
			}
		}

// reply
	// 데이터 초기화
	sql_query("TRUNCATE event_thanksgiving_reply;");
	sql_query("TRUNCATE event_thanksgiving_all_sale;");

	sql_query("TRUNCATE event_thanksgiving_all_time_open;");
	sql_query("TRUNCATE event_thanksgiving_all_time_open_episode_1;");
	sql_query("TRUNCATE event_thanksgiving_all_time_open_episode_2;");
	sql_query("TRUNCATE event_thanksgiving_all_time_open_episode_3;");

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function sql_event_pay_insert($time_val0, $time_val1, $config_time_name){
	// am
	$ep_date_start = '2018-'.$time_val0;
	$ep_hour_start = '09';
	$ep_date_end   = '2018-'.$time_val0;
	$ep_hour_end   = '17';

	// pm
	if($time_val1 == "pm"){
		$ep_date_start = '2018-'.$time_val0;
		$ep_hour_start = '18';
		$ep_date_end = date("Y-m-d", strtotime('2018-'.$time_val0.' '.NM_TIME_HI_start."+1 day")); // 내일 날짜 YMD 형태
		$ep_hour_end   = '02';
	}
	

	$get_date_type = get_date_type('', $ep_date_start, $ep_date_end, $ep_hour_start, $ep_hour_end);
	$ep_state = $get_date_type['state'];
	
	$img = '_pc/eventful/event_thanksgiving/tab02.png';
	$sql = "
	INSERT INTO event_pay (ep_name, ep_text, ep_bgcolor, ep_cover, ep_collection_top, ep_adult, ep_date_type, 
	ep_state, 
	ep_date_start, ep_hour_start, ep_date_end, ep_hour_end, ep_holiday, ep_holiweek, ep_click, ep_reg_date, ep_mod_date) VALUES ('피넛타입-".$time_val0."-".$time_val1."', '".$config_time_name."', 'FFFFFF', '".$img."', '".$img."', 'n', 'y', 
	'".$ep_state."', 
	'".$ep_date_start."', '".$ep_hour_start."', 
	'".$ep_date_end."', '".$ep_hour_end."', NULL, NULL, 0, '".NM_TIME_YMDHIS."', '');";
	sql_query($sql);
}

function sql_event_pay($time_val0, $time_val1, $config_time_name){
	$sql = "select * from event_pay where ep_name='피넛타입-".$time_val0."-".$time_val1."';";
	$row = sql_fetch($sql);
	$ep_no = 0;
	$ep_no = intval($row['ep_no']);
	if($ep_no == 0){ die('sql_event_pay Error!!!'); }
	return $ep_no;
}

function sql_event_pay_comics_insert($ep_no, $epc_order, $time_val1){

	/*
	echo $ep_no."<br/>";
	echo $epc_order."<br/>";
	echo "코믹스번호:". $time_val1[0];
	echo " / 무료yn:". $time_val1[1];
	echo " / 무료화수:".$time_val1[2];
	echo " / 세일yn:". $time_val1[3];
	echo " / 세일화수:". $time_val1[4];
	echo " / 세일땅콩:". $time_val1[5];
	echo "<br/>";
	*/

	$epc_ep_no=$epc_comics=$epc_free=$epc_free_s=$epc_free_e=$epc_sale=$epc_sale_s=$epc_sale_e=$epc_sale_pay="";

	$epc_ep_no = $ep_no;
	$epc_comics = $time_val1[0];
	// $epc_order = $epc_order; -> function epc_order임
	$epc_free = $time_val1[1];
	$epc_free_e = $time_val1[2];
	$epc_sale = $time_val1[3];
	$epc_sale_e = $time_val1[4];
	$epc_sale_pay = $time_val1[5];

	$sql = "INSERT INTO event_pay_comics (
	epc_ep_no, epc_comics, epc_order, 
	epc_free, epc_free_s, epc_free_e, 
	epc_sale, epc_sale_s, epc_sale_e, epc_sale_pay, 
	epc_date) 
	VALUES (".$epc_ep_no.", ".$epc_comics.", ".$epc_order.", 
	'".$epc_free."', 1, ".$epc_free_e.", 
	'".$epc_sale."', 1, ".$epc_sale_e.", ".$epc_sale_pay.", 
	'".NM_TIME_YMDHIS."');";
	// echo $sql."<br/>";
	sql_query($sql);
}

function sql_comics_episode_2_date_update(){
	// time
	$sql = "UPDATE comics_episode_2 SET ce_chapter='30', ce_title='최종화', ce_notice='' WHERE  ce_no=30468;";
	sql_query($sql);
	$sql = "UPDATE comics_episode_2 SET ce_chapter='26', ce_title='최종화', ce_notice='' WHERE  ce_no=29015;";
	sql_query($sql);
	$sql = "UPDATE comics_episode_2 SET ce_chapter='48', ce_notice='' WHERE  ce_no=18907;";
	sql_query($sql);
	$sql = "UPDATE comics_episode_2 SET ce_chapter='19', ce_title='최종화', ce_notice='' WHERE  ce_no=18800;";
	sql_query($sql);
	$sql = "UPDATE comics_episode_2 SET ce_chapter='38', ce_title='최종화', ce_notice='' WHERE  ce_no=29871;";
	sql_query($sql);
	
	//sale
	$sql = "UPDATE comics_episode_2 SET ce_chapter='82', ce_title='에필로그', ce_notice='' WHERE  ce_no=30995;";
	sql_query($sql);
}

function sql_state($datetime){

	$nm_time_ymd_strtotime = strtotime(NM_TIME_YMD); // 현재 시간
	
	if($nm_time_ymd_strtotime > strtotime($datetime)){				
		$sql_state = "n"; // 마감
	}else if($nm_time_ymd_strtotime < strtotime($datetime)){
		$sql_state = "r"; // 예약
	}else{
		$sql_state = "y"; // 진행
	}
	return $sql_state;

}

			// 혹시 몰라서 삭제 안함
			/*
		    // array_push($time_data, array('날짜',코믹스번호,'무료yn',무료화수,'세일yn',세일화수,세일땅콩));
			array_push($time_data, array('09-22', 'am', 1252, 'n', 0 ,'y', 114 , 2));
			array_push($time_data, array('09-22', 'am',   36, 'n', 0 ,'y',  20  ,1));
			array_push($time_data, array('09-22', 'am', 3162, 'y', 3 ,'n',   0  ,0));
			array_push($time_data, array('09-22', 'am', 2354, 'y', 3 ,'n',   0  ,0));
			
			array_push($time_data, array('09-22', 'pm', 1187, 'n', 0 ,'y',  40 , 2));
			array_push($time_data, array('09-22', 'pm', 2718, 'n', 0 ,'y',  20 , 1));
			array_push($time_data, array('09-22', 'pm', 2210, 'n', 0 ,'y',  26 , 1));
			array_push($time_data, array('09-22', 'pm', 2575, 'y', 3 ,'n',   0  ,0));
			array_push($time_data, array('09-22', 'pm', 2551, 'y', 3 ,'n',   0  ,0));
			
			array_push($time_data, array('09-23', 'am', 2206, 'y', 3 ,'n',   0  ,0));
			array_push($time_data, array('09-23', 'am', 2476, 'n', 0 ,'y',  30 , 1));
			array_push($time_data, array('09-23', 'am', 2133, 'n', 0 ,'y',  36 , 1));
			array_push($time_data, array('09-23', 'am', 2363, 'y', 3 ,'n',   0  ,0));
			
			array_push($time_data, array('09-23', 'pm', 2207, 'n', 0 ,'y',  45 , 2));
			array_push($time_data, array('09-23', 'pm',  866, 'n', 0 ,'y',  12 , 1));
			array_push($time_data, array('09-23', 'pm', 2611, 'y', 3 ,'n',   0  ,0));
			array_push($time_data, array('09-23', 'pm', 2552, 'y', 2 ,'n',   0  ,0));
			
			array_push($time_data, array('09-24', 'am',  805, 'n', 0 ,'y',  93 , 2));
			array_push($time_data, array('09-24', 'am', 2530, 'n', 0 ,'y',  40 , 1));
			array_push($time_data, array('09-24', 'am', 2803, 'y', 3 ,'n',   0  ,0));
			
			array_push($time_data, array('09-24', 'pm',  772, 'n', 0 ,'y',  48 , 2));
			array_push($time_data, array('09-24', 'pm', 1241, 'n', 0 ,'y',  40 , 1));
			array_push($time_data, array('09-24', 'pm', 2636, 'y', 3 ,'n',   0  ,0));
			array_push($time_data, array('09-24', 'pm', 2148, 'y', 3 ,'n',   0  ,0));

			array_push($time_data, array('09-25', 'am', 2082, 'n', 0 ,'y',  30 , 1));
			array_push($time_data, array('09-25', 'am', 1278, 'n', 0 ,'y',  19 , 1));
			array_push($time_data, array('09-25', 'am', 2529, 'y', 3 ,'n',   0  ,0));

			array_push($time_data, array('09-25', 'pm', 2873, 'y', 4 ,'n',   0  ,0));
			array_push($time_data, array('09-25', 'pm', 2212, 'n', 0 ,'y',  37 , 1));
			array_push($time_data, array('09-25', 'pm', 2322, 'n', 0 ,'y',  29 , 1));
			array_push($time_data, array('09-25', 'pm', 2107, 'y', 3 ,'n',   0  ,0));

			array_push($time_data, array('09-26', 'am',  446, 'n', 0 ,'y',  66 , 2));
			array_push($time_data, array('09-26', 'am', 1846, 'n', 0 ,'y',  38 , 1));
			array_push($time_data, array('09-26', 'am', 2553, 'y', 3 ,'n',   0  ,0));

			array_push($time_data, array('09-26', 'pm', 1308, 'n', 0 ,'y',  70 , 2));
			array_push($time_data, array('09-26', 'pm', 2726, 'y', 3 ,'n',   0  ,0));
			array_push($time_data, array('09-26', 'pm', 2310, 'y', 4 ,'n',   0  ,0));
			array_push($time_data, array('09-26', 'pm', 2290, 'y', 8 ,'n',   0  ,0));
			*/


/* ///////////////// DB 변경 및 추가 ///////////////// */	
/*


ALTER TABLE member_point_expen_episode_1
	CHANGE COLUMN mpee_way mpee_way TINYINT(4) NOT NULL DEFAULT '1' COMMENT '구매방법(1:개별, 7:전체구매, 8:전체세일구매)' AFTER mpee_sale;
ALTER TABLE member_point_expen_episode_2
	CHANGE COLUMN mpee_way mpee_way TINYINT(4) NOT NULL DEFAULT '1' COMMENT '구매방법(1:개별, 7:전체구매, 8:전체세일구매)' AFTER mpee_sale;
ALTER TABLE member_point_expen_episode_3
	CHANGE COLUMN mpee_way mpee_way TINYINT(4) NOT NULL DEFAULT '1' COMMENT '구매방법(1:개별, 7:전체구매, 8:전체세일구매)' AFTER mpee_sale;



CREATE TABLE event_thanksgiving_reply (
	mb_no INT(11) NOT NULL COMMENT '회원 번호',
	comments TEXT NULL COMMENT '댓글',
	blind_YN VARCHAR(2) NULL DEFAULT NULL COMMENT '댓글 블라인드 유무 (Y / N)',
	reward_YN VARCHAR(2) NULL DEFAULT NULL COMMENT '땅콩 지급 유무 (Y / N)',
	reg_date VARCHAR(50) NULL DEFAULT NULL COMMENT '댓글 등록일',
	blind_date VARCHAR(50) NULL DEFAULT NULL COMMENT '블라인드 적용일',
	give_date VARCHAR(50) NULL DEFAULT NULL COMMENT '재화 지급일',
	PRIMARY KEY (mb_no),
	UNIQUE INDEX mb_no (mb_no),
	INDEX date (reg_date, blind_date, give_date)
)
COMMENT='추석 회원 댓글 테이블'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;



CREATE TABLE event_thanksgiving_all_sale (
	mpec_no INT(11) NOT NULL AUTO_INCREMENT,
	mpec_member INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
	mpec_comics INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',
	mpec_cash_point INT(7) NOT NULL DEFAULT '0' COMMENT '구매 코인',
	mpec_point INT(7) NOT NULL DEFAULT '0' COMMENT '구매 보너스 코인',
	mpec_state TINYINT(4) NOT NULL DEFAULT '0' COMMENT '구매상태(0:결재전, 1:완료, 2:취소, 3:환불, 4:CMS제공, 5:이벤트, 6:에러)',
	mpec_age INT(5) NOT NULL DEFAULT '0' COMMENT '연령',
	mpec_sex VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성별(m:남자, w:여자, n:미인증)',
	mpec_os VARCHAR(30) NULL DEFAULT '' COMMENT '구매환경-os',
	mpec_brow VARCHAR(30) NULL DEFAULT '' COMMENT '구매환경-browser',
	mpec_user_agent VARCHAR(1000) NOT NULL DEFAULT '' COMMENT '구매자세한환경',
	mpec_version VARCHAR(200) NOT NULL DEFAULT '' COMMENT '구매버전',
	mpec_year_month VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
	mpec_year VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
	mpec_month VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
	mpec_day VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
	mpec_hour VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
	mpec_week VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',
	mpec_date VARCHAR(20) NOT NULL DEFAULT '' COMMENT '구매일',
	PRIMARY KEY (mpec_no),
	UNIQUE INDEX mpec_member_mpec_comics (mpec_member, mpec_comics),
	INDEX mpec_member (mpec_member),
	INDEX mpec_comics (mpec_comics),
	INDEX mpec_state (mpec_state),
	INDEX mpec_age (mpec_age),
	INDEX mpec_sex (mpec_sex),
	INDEX mpec_kind (mpec_os),
	INDEX mpec_brow (mpec_brow),
	INDEX mpec_year_month (mpec_year_month),
	INDEX mpec_year (mpec_year),
	INDEX mpec_month (mpec_month),
	INDEX mpec_day (mpec_day),
	INDEX mpec_hour (mpec_hour),
	INDEX mpec_week (mpec_week)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;






CREATE TABLE event_thanksgiving_all_time_open (
	sl_no INT(11) NOT NULL AUTO_INCREMENT,
	sl_comics INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',
	sl_big INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스대분류',
	sl_year_month VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
	sl_year VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
	sl_month VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
	sl_day VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
	sl_hour VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
	sl_week VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',
	sl_event_open INT(7) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'event오픈view횟수',
	sl_event_mode VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'event mode명',
	PRIMARY KEY (sl_no),
	UNIQUE INDEX sl_unique (sl_comics, sl_year_month, sl_year, sl_month, sl_day, sl_hour, sl_week, sl_event_mode),
	INDEX sl_comics (sl_comics),
	INDEX sl_big (sl_big),
	INDEX sl_year_month (sl_year_month),
	INDEX sl_year (sl_year),
	INDEX sl_month (sl_month),
	INDEX sl_day (sl_day),
	INDEX sl_hour (sl_hour),
	INDEX sl_week (sl_week),
	INDEX sl_event_mode (sl_event_mode)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
AUTO_INCREMENT=2
;





CREATE TABLE event_thanksgiving_all_time_open_episode_1 (
	se_no INT(11) NOT NULL AUTO_INCREMENT,
	se_comics INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',
	se_episode INT(11) NOT NULL DEFAULT '0' COMMENT '에피소드',
	se_year_month VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
	se_year VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
	se_month VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
	se_day VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
	se_hour VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
	se_week VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',
	se_event_open INT(7) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'event오픈view횟수',
	se_event_mode VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'event mode명',
	PRIMARY KEY (se_no),
	UNIQUE INDEX se_unique (se_comics, se_episode, se_year_month, se_year, se_month, se_day, se_hour, se_week, se_event_mode),
	INDEX se_comics (se_comics),
	INDEX se_episode (se_episode),
	INDEX se_year_month (se_year_month),
	INDEX se_year (se_year),
	INDEX se_month (se_month),
	INDEX se_day (se_day),
	INDEX se_hour (se_hour),
	INDEX se_week (se_week),
	INDEX se_event_mode (se_event_mode)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

CREATE TABLE event_thanksgiving_all_time_open_episode_2 (
	se_no INT(11) NOT NULL AUTO_INCREMENT,
	se_comics INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',
	se_episode INT(11) NOT NULL DEFAULT '0' COMMENT '에피소드',
	se_year_month VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
	se_year VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
	se_month VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
	se_day VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
	se_hour VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
	se_week VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',
	se_event_open INT(7) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'event오픈view횟수',
	se_event_mode VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'event mode명',
	PRIMARY KEY (se_no),
	UNIQUE INDEX se_unique (se_comics, se_episode, se_year_month, se_year, se_month, se_day, se_hour, se_week, se_event_mode),
	INDEX se_comics (se_comics),
	INDEX se_episode (se_episode),
	INDEX se_year_month (se_year_month),
	INDEX se_year (se_year),
	INDEX se_month (se_month),
	INDEX se_day (se_day),
	INDEX se_hour (se_hour),
	INDEX se_week (se_week),
	INDEX se_event_mode (se_event_mode)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

CREATE TABLE event_thanksgiving_all_time_open_episode_3 (
	se_no INT(11) NOT NULL AUTO_INCREMENT,
	se_comics INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',
	se_episode INT(11) NOT NULL DEFAULT '0' COMMENT '에피소드',
	se_year_month VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
	se_year VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
	se_month VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
	se_day VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
	se_hour VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
	se_week VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',
	se_event_open INT(7) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'event오픈view횟수',
	se_event_mode VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'event mode명',
	PRIMARY KEY (se_no),
	UNIQUE INDEX se_unique (se_comics, se_episode, se_year_month, se_year, se_month, se_day, se_hour, se_week, se_event_mode),
	INDEX se_comics (se_comics),
	INDEX se_episode (se_episode),
	INDEX se_year_month (se_year_month),
	INDEX se_year (se_year),
	INDEX se_month (se_month),
	INDEX se_day (se_day),
	INDEX se_hour (se_hour),
	INDEX se_week (se_week),
	INDEX se_event_mode (se_event_mode)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;



*/
?>


