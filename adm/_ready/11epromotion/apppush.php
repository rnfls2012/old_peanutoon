<?php
//if (!defined("_SH_HOUR_")) exit;

use Classes\Push as PushServer;

include_once "./_common.php";
require_once '/var/www/html/peanutoon/vendor/autoload.php'; // require composer autoload

$mb_token_arr = array();

// 시간내에 있는지 먼저 검색
$row_count_sql = "
  SELECT count(*) AS push_cnt
  FROM push_notification_info 
  WHERE state='r' AND res_date <= '".NM_TIME_YMDHIS."'
";

$row_count = sql_count($row_count_sql, "push_cnt", true);

if($row_count > 0) {

    $url = "dev.nexcube.co.kr/push_server/public/put.php";
    $newInstance = new PushServer($url);

    $res_select_sql = "
        SELECT * 
        FROM push_notification_info 
        WHERE state='r' AND res_date<='".NM_TIME_YMDHIS."'
    ";

    $res_select_result = sql_query($res_select_sql);

    while($row_data = sql_fetch_array($res_select_result)) {

        $push_msg_arr = array(
            'KEY_IDN'   => $row_data['id'],
            'TYPE'      => $row_data['type'],
            'TITLE'     => $row_data['title'],
            'CONTENT'   => $row_data['contents'],
            'EVENT'     => $row_data['event']
        );

        // check image file
        if ( isset($row_data['img_file_path']) && $row_data['img_file_path'] != '' ) {
            $img_url = img_url_para($row_data['img_file_path'], $row_data['reg_date']);
            $push_msg_arr['IMAGE_URL'] = $img_url;
        }

        // 보낼 회원의 토큰 값을 배열에 저장
        $sql_mb_select = "
          SELECT DISTINCT mb_token 
          FROM member 
          WHERE mb_apk_is_count > 0 AND mb_device = '0' AND mb_push = '1' AND mb_state = 'y' AND mb_token IS NOT NULL
        "; // 쿼리문에 안드로이드만 검색. mb.mb_device

        $mb_result = sql_query($sql_mb_select);

        for ($i=0; $row_mb_data=sql_fetch_array($mb_result); $i++) {
            array_push($mb_token_arr, $row_mb_data['mb_token']);
        }

        $mb_token_arr = array();

        array_push($mb_token_arr, 'eMgB8XT_7yM:APA91bF0lJqe9eLNwLnGwJaXD-ASjczVhObT3XCbIKp9KGBcxYtYvfbXDx7YizHiY5ZF-OUOTCQKXPN-paxI3WMR2R_TpgDhJ-_tqHjJKc4w6p6zb_6tMf7Wrce1kGRxHiuiQ6X6N2qd');
        array_push($mb_token_arr, 'e4cF3P3FNN0:APA91bF50UmtmADiQm-4bATX8MFfM-uEIuTjxgcohWmacayk0Y9vv5Xglf8Wku4KuvfRYPxw-x9bewNOW0fKVEgY0AzNExKOuEYxd2m-geJ_JYU7ilY7AOBv4BMzvQbqz9bn7joKd369');

        $push_msg_arr['registration_ids'] = $mb_token_arr;

        try {
            if ($row_data['state'] == 'c') {
                echo "완료된 푸시입니다.";
            } else {
                $push_result = $newInstance->sendPushMsg($push_msg_arr);

                $sql_update = "
                  UPDATE push_notification_info SET state = 's' WHERE id = '".$row_data['id']."';
                ";

                sql_query($sql_update);

                echo "메세지를 서버에 전달하였습니다.";
            }
        } catch (Exception $exception) {
            echo $exception->getMessage()." / ".$exception->getCode();
        }

    } // end while

} else {
    echo "예약된 푸시 정보가 없습니다.";
}// end if
