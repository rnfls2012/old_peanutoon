<?
include_once '_common.php'; // 공통

/* 기본설정 */
$sql_cp_comics = "SELECT * FROM  `m_mangazzang_cp`.`fileupload` WHERE `big`=1 ORDER BY filenum ";
$result_cp_comics = sql_query($sql_cp_comics); /* 야톡뺌 */

echo $sql_cp_comics."<br/>";

$dbtable = "comics";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

echo $sql2_cnt."<br/>";

$sql_cp_list = array('cm_no','cm_big','cm_small');
array_push($sql_cp_list, 'cm_provider', 'cm_publisher', 'cm_professional');
array_push($sql_cp_list, 'cm_service', 'cm_adult', 'cm_platform', 'cm_page_way', 'cm_end', 'cm_color');
array_push($sql_cp_list, 'cm_series', 'cm_somaery','cm_episode_total');
array_push($sql_cp_list, 'cm_pay', 'cm_ecn_pay');
array_push($sql_cp_list, 'cm_cover', 'cm_cover_sub');
array_push($sql_cp_list, 'cm_event', 'cm_free');
array_push($sql_cp_list, 'cm_click', 'cm_buy_count');
array_push($sql_cp_list, 'cm_up_kind', 'cm_own_type', 'cm_l_time', 'cm_s_time', 'cm_excel_down_date');
array_push($sql_cp_list, 'cm_reg_date', 'cm_episode_date');

/* 페이지 방향 */
$cm_no_right_arr = array();
$sql_page_way_right = "SELECT * FROM  `m_mangazzang_cp`.`content` WHERE `content_way` =  '오른쪽' group by content_title order by content_title, content_num";
$result_page_way_right = sql_query($sql_page_way_right);
while ($row_page_way_right = sql_fetch_array($result_page_way_right)) {
	array_push($cm_no_right_arr, $row_page_way_right['content_title']); 
}

/* 색상 */
$cm_no_color_arr = array();
$sql_color = "SELECT * FROM  `m_mangazzang_cp`.`content` WHERE `content_color` =  '칼라' group by content_title order by content_title, content_num";
$result_color= sql_query($sql_color);
while ($row_color = sql_fetch_array($result_color)) {
	array_push($cm_no_color_arr, $row_color['content_title']); 
}

$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_comics)) {
		$row_cp['cm_no'] = $row_cp['filenum'];
		$row_cp['cm_big'] = $row_cp['big'];
		$row_cp['cm_small'] = $row_cp['small'];

		$row_cp['cm_provider'] = '1'; /* 1 NEXCUBE*/
		$row_cp['cm_publisher'] = $row_cp['maker_no'];
		$row_cp['cm_professional'] = $row_cp['editer_no'];		

		$db_cm_service = "y";
		if($row_cp['service'] == "중지"){ $db_cm_service = "n"; }
		$row_cp['cm_service'] = $db_cm_service;

		$db_cm_adult= "y";
		if($row_cp['level'] == "청소년"){ $db_cm_adult = "n"; }
		$row_cp['cm_adult'] = $db_cm_adult;

		$row_cp['cm_platform'] = "3"; /* 1:웹, 2:Android */
		
		$db_cm_page_way = "l";
		if(in_array($row_cp['filenum'], $cm_no_right_arr)){ $db_cm_page_way = "r"; }
		$row_cp['cm_page_way'] = $db_cm_page_way ;

		$db_cm_color = "n";
		if(in_array($row_cp['filenum'], $cm_no_color_arr)){ $db_cm_color = "y"; }
		$row_cp['cm_color'] = $db_cm_color ;
		
		$db_cm_end= "y";
		if($row_cp['serize_end'] == "0"){ $db_cm_end = "n"; }
		$row_cp['cm_end'] = $db_cm_end;

		$row_cp['cm_series'] = addslashes($row_cp['project']);
		$row_cp['cm_somaery'] = addslashes($row_cp['somaery']);

		$sql_content_cnt = "SELECT max(content_num) as sin_cnt from `m_mangazzang_cp`.`content` where content_title=".$row_cp['filenum'];
		echo $sql_content_cnt."<br/>";	
		$row_content_cnt = sql_count($sql_content_cnt, 'sin_cnt');
		$row_cp['cm_episode_total'] = $row_content_cnt;

		$row_cp['cm_pay'] = intval($row_cp['pay']/100);
		$row_cp['cm_ecn_pay'] = intval($row_cp['ecn_pay']/100);

		// 이미지 파일 확장자 체크
		$image_extension = substr($row_cp['image'], -4);
		$image_small_extension = substr($row_cp['image_small'], -4);
		$image_data_path = "/data/".$row_cp['big']."/".$row_cp['filenum']."/cover".$image_extension;
		$image_small_data_path = "/data/".$row_cp['big']."/".$row_cp['filenum']."/cover_sub".$image_small_extension;
		$row_cp['cm_cover'] = $image_data_path ;
		$row_cp['cm_cover_sub'] = $image_small_data_path;

		$row_cp['cm_event'] = $row_cp['event'];
		$row_cp['cm_free'] = $row_cp['free'];

		$row_cp['cm_click'] = $row_cp['click'];
		$row_cp['cm_buy_count'] = $row_cp['pay_num'];

		$row_cp['cm_up_kind'] = $row_cp['up_kind'];


		$db_cm_own_type = "1";
		if($row_cp['sojang'] == "0"){ $db_cm_own_type = "2"; }
		$row_cp['cm_own_type'] = $db_cm_own_type;

		$row_cp['cm_l_time'] = $row_cp['l_time'];
		$row_cp['cm_s_time'] = $row_cp['s_time'];
		$row_cp['cm_excel_down_date'] = $row_cp['excel_down_date'];

		$row_cp['cm_reg_date'] = $row_cp['upload_date'];
		$row_cp['cm_episode_date'] = $row_cp['new_date'];



		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>