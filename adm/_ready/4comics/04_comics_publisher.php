<?
include_once '_common.php'; // 공통

/* 에피소드 출판사 */
$sql_cp_comics = "SELECT * FROM  `m_mangazzang_cp`.`maker` WHERE 1 ORDER BY `maker_no` ASC  ";
$result_cp_comics = sql_query($sql_cp_comics);

echo $sql_cp_comics."<br/>";

$dbtable = "comics_publisher";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

echo $sql2_cnt."<br/>";

$sql_cp_list = array('cp_no','cp_name','cp_ecn_pay');
array_push($sql_cp_list, 'cp_date'); /* 저장일 */

$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_comics)) {
		$row_cp['cp_no'] = $row_cp['maker_no'];
		$row_cp['cp_name'] = $row_cp['maker_name'];
		$row_cp['cp_ecn_pay'] = $row_cp['maker_ecn_pay'];
		$row_cp['cp_date'] = NM_TIME_YMDHIS; // 저장일


		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>