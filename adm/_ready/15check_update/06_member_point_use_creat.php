<?
include_once '_common.php'; // 공통

$sql = "
CREATE TABLE `member_point_used` (
	`mpu_no` INT(11) NOT NULL AUTO_INCREMENT,
	`mpu_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
	`mpu_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

	`mpu_class` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '구분(r:충전, e:이벤트충전, b:구매)',

	`mpu_recharge_won` INT(11) NOT NULL DEFAULT '0' COMMENT '결제금액',
	`mpu_recharge_cash_point` INT(7) NOT NULL DEFAULT '0' COMMENT '충전 코인',
	`mpu_recharge_point` INT(7) NOT NULL DEFAULT '0' COMMENT '충전 보너스 코인',
	`mpu_recharge_free` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '무료 충전 여부(y:무료, n:유료)',

	`mpu_cash_point` INT(7) NOT NULL DEFAULT '0' COMMENT '구매 코인',
	`mpu_point` INT(7) NOT NULL DEFAULT '0' COMMENT '구매 보너스 코인',
	`mpu_count` INT(11) NOT NULL DEFAULT '0' COMMENT '구매 횟수',
	
	`mpu_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',
	`mpu_big` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '코믹스분류',
	`mpu_small` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '장르',
	`mpu_cash_type` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '정산금액타입:config_unit_pay-연관',

	`mpu_cash_point_balance` INT(11) NOT NULL DEFAULT '0' COMMENT '회원 이용후 총 코인',
	`mpu_point_balance` INT(11) NOT NULL DEFAULT '0' COMMENT '회원 이용후 총 보너스 코인',
	
	`mpu_way` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '이용방법(0:충전, 1:개별, 7:전체구매)',

	`mpu_from` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '이용 내역' COLLATE 'utf8_unicode_ci',
	`mpu_order` VARCHAR(40) NOT NULL DEFAULT '' COMMENT '주문번호',
	`mpu_type` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '이벤트 구분',

	`mpu_age` INT(5) NOT NULL DEFAULT '0' COMMENT '연령',
	`mpu_sex` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성별(m:남자, w:여자, n:미인증)',
	`mpu_os` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '이용환경-os',
	`mpu_brow` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '이용환경-browser',
	`mpu_user_agent` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '이용자세한환경',
	`mpu_version` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '이용버전',
	`mpu_year_month` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
	`mpu_year` VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
	`mpu_month` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
	`mpu_day` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
	`mpu_hour` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
	`mpu_week` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',
	`mpu_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

	PRIMARY KEY (`mpu_no`),
	INDEX `mpu_member` (`mpu_member`),
	INDEX `mpu_member_idx` (`mpu_member_idx`),

	INDEX `mpu_class` (`mpu_class`),

	INDEX `mpu_comics` (`mpu_comics`),
	INDEX `mpu_big` (`mpu_big`),
	INDEX `mpu_small` (`mpu_small`),
	INDEX `mpu_cash_type` (`mpu_cash_type`),

	INDEX `mpu_way` (`mpu_way`),

	INDEX `mpu_age` (`mpu_age`),
	INDEX `mpu_sex` (`mpu_sex`),
	INDEX `mpu_kind` (`mpu_os`),
	INDEX `mpu_year_month` (`mpu_year_month`),
	INDEX `mpu_year` (`mpu_year`),
	INDEX `mpu_month` (`mpu_month`),
	INDEX `mpu_day` (`mpu_day`),
	INDEX `mpu_hour` (`mpu_hour`),
	INDEX `mpu_week` (`mpu_week`),
	INDEX `mpu_brow` (`mpu_brow`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;
";
sql_query($sql);

?>