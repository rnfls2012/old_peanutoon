<?
include_once '_common.php'; // 공통

$dbtable = array(
	'member_buy_comics'=>'mbc_member', 
	'member_buy_episode_1'=>'mbe_member', 
	'member_buy_episode_2'=>'mbe_member', 
	'member_buy_episode_3'=>'mbe_member', 
	'member_cash'=>'mc_member', 
	'member_comics_mark'=>'mcm_member', 
	'member_point_expen_comics'=>'mpec_member', 
	'member_point_expen_episode_1'=>'mpee_member', 
	'member_point_expen_episode_2'=>'mpee_member', 
	'member_point_expen_episode_3'=>'mpee_member', 
	'member_point_income'=>'mpi_member', 
	'member_point_income_event'=>'mpie_member', 
	'member_point_used'=>'mpu_member', 
	'pg_certify_kcp '=>'kcp_member', 
	'pg_channel_kcp'=>'kcp_member', 
	'pg_channel_payco'=>'payco_member', 
	'pg_channel_toss'=>'toss_member'
); // 관련 테이블 배열 // 무통장은 초기화 할 예정 pg_channel_payco_nonbank

$admin_no = array(); // 관리자 회원 배열
$error_arr = array(); // 에러 테이블 배열

$admin_mb_sql = "SELECT * FROM member WHERE mb_class='a'";
$admin_mb_result = sql_query($admin_mb_sql);
while($row = sql_fetch_array($admin_mb_result)) {
	array_push($admin_no, $row['mb_no']);
} // end while
// 테스트 ID 추가
array_push($admin_no, '35905'); // 메가코믹스 트위터
array_push($admin_no, '35904'); // 김선규 네이버 ID
array_push($admin_no, '35903'); // 이미희 테스트 ID
array_push($admin_no, '35902'); // 김선규 카카오 ID
array_push($admin_no, '35901'); // 이미희 페이스북 ID
array_push($admin_no, '35900'); // 김선규 구글 ID
array_push($admin_no, '35899'); // APP업체 구글 ID
array_push($admin_no, '35897'); // 배경호 카카오 ID
array_push($admin_no, '35896'); // 이익희 카카오 ID
array_push($admin_no, '35895'); // 김선규 카카오 ID
array_push($admin_no, '35894'); // 최정윤 카카오 ID
array_push($admin_no, '35893'); // 이익희 카카오 ID
array_push($admin_no, '35892'); // 조규진 카카오 ID
array_push($admin_no, '35891'); // 피너툰 트위터
array_push($admin_no, '35890'); // 이익희 페이스북 ID
array_push($admin_no, '35889'); // 김선규 네이버 ID
array_push($admin_no, '35888'); // 이익희 구글 ID
array_push($admin_no, '35887'); // 박병민 네이버 ID
array_push($admin_no, '35885'); // 이익희 네이버 ID
array_push($admin_no, '35884'); // 테스트 ID
array_push($admin_no, '35883'); // kcp ID
array_push($admin_no, '35882'); // 김선규 카카오 ID
array_push($admin_no, '35881'); // 테스트-b ID
array_push($admin_no, '35880'); // 테스트-a ID
array_push($admin_no, '32756'); // 김선규 옛날 관리자 ID


foreach($dbtable as $table => $pk) {
	$sql = "DELETE FROM ".$table." WHERE ".$pk." IN (".implode($admin_no, ',').")";
	$result = sql_query($sql);
	if(!$result) {
		array_push($error_arr, $table);
	} // end if
} // end foreach

/* 실패했을 경우 log 남기기 */
if(count($error_arr) > 0) {
	cs_error_log(NM_ADM_PATH."/_ready/log/".NM_TIME_YMD.".log", implode($error_arr, ','), $_SERVER['PHP_SELF']);
} // end if
?>