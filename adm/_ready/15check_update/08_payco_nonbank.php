<?
include_once '_common.php'; // 공통

$sql = "
CREATE TABLE `pg_channel_payco_nonbank` (
	`payco_no` INT(11) NOT NULL AUTO_INCREMENT,
	`payco_seller_order_reference_key` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '결재 외부가맹점에서 관리하는 주문연동Key',
	`payco_reserve_order_no` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '결재 거래 번호',
	`payco_order_no` VARCHAR(42) NOT NULL DEFAULT '' COMMENT '주문번호',
	`payco_member_name` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '주문자명(일부 마스킹처리)',
	`payco_member_email` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '주문자EMAIL(일부 마스킹처리)',
	`payco_order_channel` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '주문채널 (PC/MOBILE)',
	`payco_total_order_amt` VARCHAR(11) NOT NULL DEFAULT '' COMMENT '주문금액',
	`payco_total_payment_amt` VARCHAR(11) NOT NULL DEFAULT '' COMMENT '결제금액',
	`payco_order_product_no` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '주문상품번호',
	`payco_seller_order_product_reference_key` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '외부가맹점에서 발급한 주문상품연동Key',
	`payco_order_product_status_code` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '주문상품상태코드',
	`payco_order_product_status_name` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '주문상품상태명',
	`payco_cp_id` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '상점ID',
	`payco_product_id` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '상품ID',
	`payco_product_kind_code` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '상품코드',
	`payco_product_payment_amt` VARCHAR(11) NOT NULL DEFAULT '' COMMENT '상품금액',
	`payco_original_product_payment_amt` VARCHAR(11) NOT NULL DEFAULT '' COMMENT '본상품금액',
	`payco_bank_name` VARCHAR(15) NOT NULL DEFAULT '' COMMENT '은행명',
	`payco_bank_code` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '은행코드',
	`payco_account_no` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '계좌번호',
	`payco_payment_trade_no` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '결제번호',
	`payco_payment_method_code` VARCHAR(11) NOT NULL DEFAULT '' COMMENT '결제수단코드',
	`payco_payment_method_name` VARCHAR(15) NOT NULL DEFAULT '' COMMENT '결제수단명',
	`payco_payment_amt` VARCHAR(11) NOT NULL DEFAULT '' COMMENT '결제금액',
	`payco_taxfree_amt` VARCHAR(11) NOT NULL DEFAULT '' COMMENT '면세금액',
	`payco_taxable_amt` VARCHAR(11) NOT NULL DEFAULT '' COMMENT '과세금액',
	`payco_vat_amt` VARCHAR(11) NOT NULL DEFAULT '' COMMENT '부가세금액',
	`payco_service_amt` VARCHAR(11) NOT NULL DEFAULT '' COMMENT '봉사료금액',
	`payco_trade_ymdt` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '결제일시 (yyyyMMddHHmmss)',
	`payco_pg_admission_no` VARCHAR(20) NOT NULL DEFAULT '' COMMENT 'PG승인번호',
	`payco_pg_admission_ymdt` VARCHAR(20) NOT NULL DEFAULT '' COMMENT 'PG승인일시 (yyyyMMddHHmmss)',
	`payco_easy_payment_yn` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '간편결제여부 (Y/N)',
	`payco_order_certify_key` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '주문완료통보시 내려받은 인증 값',
	`payco_payment_completion_yn` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '결제완료여부 (Y/N)',
	`payco_payment_complete_ymdt` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '결제일시 (yyyyMMddHHmmss)',
	`payco_remote_addr` VARCHAR(20) NOT NULL DEFAULT '' COMMENT 'IP정보',
	`payco_save` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',
	PRIMARY KEY (`payco_no`),
	INDEX `payco_order_product_status_code` (`payco_order_product_status_code`),
	INDEX `payco_bank_code` (`payco_bank_code`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;
";
sql_query($sql);

?>