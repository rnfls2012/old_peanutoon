<? include_once '_common.php'; // 공통 

/* 이번 년도, 이번 달의 로그만 이동시켜 줍니다, 이동할 회원번호로 이미 로그가 하나라도 들어가 있으면 이동 불가합니다. 
날짜를 바꾸고 싶으면 아래의 날짜 변수를 수정하세요. */

$origin_no = '648607'; // old user number
$mig_no = '1023474'; // new user number

$year = NM_TIME_Y; // 이동할 년도(default : 현재 년도)
$month = NM_TIME_M; // 이동할 월(default : 현재 달)
$day = NM_TIME_D; // 이동할 일(default : 오늘)

// 가장 먼저 이동할 아이디에 로그가 있는지 확인
$mig_count = sql_count("SELECT COUNT(*) AS mig_count FROM z_member_attend WHERE z_ma_member='".$mig_no."' AND z_ma_attend_year='".$year."' AND z_ma_attend_month='".$month."'", "mig_count");
$origin_ndx = mb_get_ndx($origin_no);

// 로그가 있다면 die
if($mig_count > 0) {
	alert($year."-".$month." 의 데이터가 존재합니다");
	die;
} else {
	$origin_row = mb_get_no($origin_no);
	$origin_idx = mb_get_idx($origin_row['mb_id']);
	$origin_ndx = mb_get_ndx($origin_no);

	$mig_row = mb_get_no($mig_no);

	$mig_idx = mb_get_idx($mig_row['mb_id']);
	$mig_ndx = mb_get_ndx($mig_no);

	$temp_arr = array();
	$mode_arr = array();
	
	// 데이터 조회
	for($temp_day=1; $temp_day<=24; $temp_day++) {
		
		// 1. z_member_attend 테이블 데이터 UPDATE
			$z_ma_sql = "SELECT * FROM z_member_attend WHERE z_ma_member=".$origin_no." AND z_ma_idx='".$origin_idx."' AND z_ma_attend_year='".$year."' AND z_ma_attend_month='".$month."' AND z_ma_attend_day=".$temp_day;
			$z_ma_sql_result = sql_fetch($z_ma_sql);
			$temp_arr['attend'] = $z_ma_sql_result['z_ma_no'];
			
		// 2. member_point_income_event 테이블 데이터
			$mpie_sql = "SELECT * FROM member_point_income_event WHERE mpie_member=".$origin_no." AND mpie_member_idx='".$origin_idx."' AND mpie_type='attend' AND mpie_year='".$year."' AND mpie_month='".$month."' AND mpie_day=".$temp_day;
			$mpie_sql_result = sql_fetch($mpie_sql);
			$temp_arr['event'] = $mpie_sql_result['mpie_no'];
			
		// 3. member_point_used 테이블 데이터 UPDATE
			$mpu_sql = "SELECT * FROM member_point_used WHERE mpu_member=".$origin_no." AND mpu_member_idx='".$origin_idx."' AND mpu_class='e' AND mpu_recharge_free='y' AND mpu_type='attend' AND mpu_year='".$year."' AND mpu_month='".$month."' AND mpu_day=".$temp_day;
			echo $mpu_sql;
			echo "<br>";
			$mpu_sql_result = sql_fetch($mpu_sql);
			$temp_arr['used'] = $mpu_sql_result['mpu_no'];

			array_push($mode_arr, $temp_arr);			
	} // end for

	// 데이터 옮기기
	if(count($mode_arr) > 0) {
		foreach($mode_arr as $key => $val) {
			foreach($val as $s_key => $s_val) {
				switch($s_key) {
				
					case "attend" :
						$update_sql = "UPDATE z_member_attend SET z_ma_member=".$mig_no.", z_ma_id='".$mig_row['mb_id']."', z_ma_idx='".$mig_idx."' WHERE z_ma_no=".$s_val;
						if(sql_query($update_sql)) {
							$insert_sql = "INSERT INTO z_member_attend_mode(z_ma_no_mod, z_ma_member, z_ma_idx, z_ma_ndx, z_ma_member_mod, z_ma_idx_mod, z_ma_ndx_mod, z_mam_type, z_mam_content, z_mam_date) VALUES(".$s_val.", ".$origin_no.", '".$origin_idx	."', '".$origin_ndx."', '".$mig_no."', '".$mig_idx."', '".$mig_ndx."', 'auto', 'u', '".NM_TIME_YMDHIS."')";
							sql_query($insert_sql);
						} // end if
						break;
					
					case "event" :
						$update_sql = "UPDATE member_point_income_event SET mpie_member=".$mig_no.", mpie_member_idx='".$mig_idx."' WHERE mpie_no=".$s_val;
						if(sql_query($update_sql)) {
							$insert_sql = "INSERT INTO member_point_income_event_mode(mpie_no_mod, mpie_member, mpie_member_idx, mpie_member_ndx, mpie_member_mod, mpie_member_idx_mod, mpie_member_ndx_mod, mpiem_type, mpiem_content, mpiem_date) VALUES(".$s_val.", ".$origin_no.", '".$origin_idx	."', '".$origin_ndx."', '".$mig_no."', '".$mig_idx."', '".$mig_ndx."', 'auto', 'u', '".NM_TIME_YMDHIS."')";
							sql_query($insert_sql);
						} // end if
						
						break;
					
					case "used" :
						$update_sql = "UPDATE member_point_used SET mpu_member=".$mig_no.", mpu_member_idx='".$mig_idx."' WHERE mpu_no=".$s_val;
						if(sql_query($update_sql)) {
							$insert_sql = "INSERT INTO member_point_used_mode(mpu_no_mod, mpu_member, mpu_member_idx, mpu_member_ndx, mpu_member_mod, mpu_member_idx_mod, mpu_member_ndx_mod, mpum_type, mpum_content, mpum_date) VALUES(".$s_val.", ".$origin_no.", '".$origin_idx	."', '".$origin_ndx."', '".$mig_no."', '".$mig_idx."', '".$mig_ndx."', 'auto', 'u', '".NM_TIME_YMDHIS."')";
							sql_query($insert_sql);
						} // end if
						
						break;

				} // end switch
			} // end foreach
		} // end foreach
	} else {
		alert("옮길 데이터가 없습니다!");
		die;
	} // end else
} // end else

?>