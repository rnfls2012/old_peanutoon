<?
include_once '_common.php'; // 공통

/* 회원 소장 코믹스 리스트 */
// member_point_expen_episode_1 -> 구매 코믹스 화 단행본리스트 화관련 데이터가 없어서 1화에 구매했단 식으로 처리해야함

$dbtable = "member_point_expen_episode_1";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

$sql_chk_cnt = "SELECT count(*) as cnt FROM `member_point_expen_comics` ";
$row_chk_cnt = sql_count($sql_chk_cnt, 'cnt');

echo $sql2_cnt."<br/>";

echo $sql_chk_cnt."<br/>";

$sql_cp_point = "SELECT * FROM  `member_point_expen_comics` mpec JOIN comics c ON mpec.mpec_comics = c.cm_no 
								WHERE 1 ORDER BY mpec_no ASC  ";
$result_cp_point = sql_query($sql_cp_point);
echo $sql_cp_point."<br/>";

$sql_cp_list = array('mpee_no', 'mpee_member', 'mpee_member_idx');
array_push($sql_cp_list, 'mpec_no');
array_push($sql_cp_list, 'mpee_comics', 'mpee_big', 'mpee_small');
array_push($sql_cp_list, 'mpee_episode', 'mpee_chapter');
array_push($sql_cp_list, 'mpee_cash_type');
array_push($sql_cp_list, 'mpee_cash_point', 'mpee_point');
array_push($sql_cp_list, 'mpee_cash_point_balance', 'mpee_point_balance');
array_push($sql_cp_list, 'mpee_state', 'mpee_way');
array_push($sql_cp_list, 'mpee_age', 'mpee_sex');
array_push($sql_cp_list, 'mpee_kind', 'mpee_user_agent', 'mpee_version');
array_push($sql_cp_list, 'mpee_year_month', 'mpee_year', 'mpee_month', 'mpee_day', 'mpee_hour', 'mpee_week');
array_push($sql_cp_list, 'mpee_date'); 


$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_point)) {
		$row_cp['mpee_no'] = $row_cp['mpec_no'];
		$row_cp['mpee_member'] = $row_cp['mpec_member'];
		$row_cp['mpee_member_idx'] = $row_cp['mpec_member_idx'];
		
		$row_cp['mpec_no'] = $row_cp['mpec_no'];

		$row_cp['mpee_comics'] = $row_cp['mpec_comics'];
		$row_cp['mpee_big'] = $row_cp['mpec_big'];
		$row_cp['mpee_small'] = $row_cp['mpec_small'];
		
		//코믹스 화 랜덤
		$ce_chapter = rand(1,$row_cp['cm_episode_total']);
		$sql_mpee_episode = "SELECT * FROM  `comics_episode_1` WHERE  `ce_chapter` = ".$ce_chapter." AND  `ce_comics` = ".$row_cp['mpec_comics']." ";
		$row_mpee_episode = sql_fetch($sql_mpee_episode);
		$row_cp['mpee_episode'] = $row_mpee_episode['ce_no'];
		$row_cp['mpee_chapter'] = $ce_chapter;

		$row_cp['mpee_cash_type'] = $row_cp['mpec_cash_type'];

		$row_cp['mpee_cash_point'] = $row_cp['mpec_cash_point'];
		$row_cp['mpee_point'] = $row_cp['mpec_point'];

		$row_cp['mpee_cash_point_balance'] = $row_cp['mpec_cash_point_balance'];
		$row_cp['mpee_point_balance'] = $row_cp['mpec_point_balance'];

		$row_cp['mpee_state'] = $row_cp['mpec_state'];
		$row_cp['mpee_way'] = $row_cp['mpec_way'];

		$row_cp['mpee_age'] = $row_cp['mpec_age'];
		$row_cp['mpee_sex'] = $row_cp['mpec_sex'];

		$row_cp['mpee_kind'] = $row_cp['mpec_kind'];
		$row_cp['mpee_user_agent'] = $row_cp['mpec_user_agent'];
		$row_cp['mpee_version'] = $row_cp['mpec_version'];
		
		$row_cp['mpee_year_month'] = substr($row_cp['mpec_date'], 0, 7);
		$row_cp['mpee_year'] = substr($row_cp['mpec_date'], 0, 4);
		$row_cp['mpee_month'] = substr($row_cp['mpec_date'], 5, 2);
		$row_cp['mpee_day'] = substr($row_cp['mpec_date'], 8, 2);
		$row_cp['mpee_hour'] = substr($row_cp['mpec_date'], 11, 2);
		$row_cp['mpee_week'] = date("w",strtotime($row_cp['mpec_date']));

		$row_cp['mpee_date'] = $row_cp['mpec_date'];



		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
				$sql2_insert.= 'NULL, ';
			}else{
				$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
			}
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>