<?
include_once '_common.php'; // 공통

/* 회원 코믹스 화폐 충전 내역 */


$dbtable = "member_point_income";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

$sql_chk_cnt = "SELECT count(*) as cnt FROM `point_income_back` ";
$row_chk_cnt = sql_count($sql_chk_cnt, 'cnt');

echo $sql2_cnt."<br/>";

echo $sql_chk_cnt."<br/>";

if($row_chk_cnt == 0 && $row2_cnt == 0){
	/* 테이블 카피 */
	$sql_cp_point_back = "CREATE TABLE `m_mangazzang2`.`point_income_back` 
							SELECT pi.*, m.mb_no, m.mb_id, m.mb_idx, m.mb_name, m.mb_level, m.mb_sex, 
							( (FLOOR ( (YEAR( NOW( ) ) - LEFT( m.mb_birth, 4 ) +0) /10) )*10 ) as mb_age
							FROM `m_mangazzang_cp`.`point_income` pi 
							LEFT JOIN `m_mangazzang2`.`member` m ON pi.in_id = m.mb_id 
							WHERE m.mb_id IS NOT NULL
							ORDER BY pi.in_no ASC 							
							";
	sql_query($sql_cp_point_back);
	echo$sql_cp_point_back."<br/>";
	
	/* ---------------------------`point_income_back` ADD------------------------------ */
		$sql = "ALTER TABLE `point_income_back` ADD `in_cash_won` int(11) NOT NULL COMMENT '결제금액'  AFTER `in_cash` ";
		sql_query($sql);
		$sql = "UPDATE `point_income_back` SET `in_cash_won` = `in_cash` "; /* 기존 코인 데이터 필드 복사 */
		sql_query($sql);
		$sql = "ALTER TABLE `point_income_back` ADD `in_date2` varchar(20) NOT NULL COMMENT '충전 상세날짜'  AFTER `in_from` ";
		sql_query($sql);
		$sql = "ALTER TABLE `point_income_back` ADD `in_free` int(2) NOT NULL DEFAULT '0' COMMENT '무료여부(0_무료아님_1_무료)'  AFTER `in_cash_won` ";
		sql_query($sql);
		$sql = "ALTER TABLE `point_income_back` ADD `in_state` int(2) NOT NULL DEFAULT '0' COMMENT '결재상태(0_완료_1_취소_2_환불_3_에러)'  AFTER `in_cash_won` ";
		sql_query($sql);
		$sql = "ALTER TABLE `point_income_back` ADD `in_kind` VARCHAR(20) NOT NULL COMMENT '충전환경' AFTER `in_cash_balance` ";
		sql_query($sql);
		$sql = "ALTER TABLE `point_income_back` ADD `in_user_agent` VARCHAR(255) NOT NULL COMMENT '충전자세한환경' AFTER `in_kind` ";
		sql_query($sql);
		$sql = "ALTER TABLE `point_income_back` ADD `in_version` VARCHAR(20) NOT NULL COMMENT '충전버전' AFTER `in_user_agent` ";
		sql_query($sql);

	/* in_free_date가 빈값만 검사 */
	$sql_in_free_date = "UPDATE `point_income_back` 
						 SET in_date = CONCAT(in_date, ' 00:00:00'), 
						 in_date2 = CONCAT(in_date, ' 00:00:00') 
						 WHERE in_free_date = '' AND in_from like '%결재%' AND  in_date2 = ''; ";
	sql_query($sql_in_free_date);
	$sql_in_free_date = "UPDATE `point_income_back` 
						 SET in_date = CONCAT(in_date, ' 00:00:00'), 
						 in_date2 = CONCAT(in_date, ' 00:00:00'), 
						 in_free='1' 
						 WHERE in_free_date = '' AND in_from not like '%결재%' AND  in_date2 = '' AND in_free='0' ";
	sql_query($sql_in_free_date);

	/* in_date 길이가 15 미만이고, 결재 경우만 검사 */	
	$sql_in_date15 = "UPDATE `point_income_back` 
						 SET in_date2 = in_free_date,
						 in_date = in_free_date , 
						 in_free_date = NULL 
						 WHERE in_from like '%결재%' AND  LENGTH(in_date) < 15 AND in_date2 = '' AND INSTR(in_from, '결재') > 0 ; ";
	sql_query($sql_in_date15);
	$sql_in_date15 = "UPDATE `point_income_back` 
						 SET in_date2 = in_free_date,
						 in_free_date = NULL 
						 WHERE  LENGTH(in_date2) < 15 AND LENGTH(in_free_date) > 14 ; ";
	sql_query($sql_in_date15);

	/* in_date2 가 없는 데이터 검사 */
	$sql_in_date2 = "UPDATE `point_income_back` 
						 SET in_date2 = in_date 
						 WHERE in_date2 = '' AND in_from like '%결재%'; ";
	sql_query($sql_in_date2);
	$sql_in_date2 = "UPDATE `point_income_back` 
						 SET in_date2 = in_date,
						 in_free='1' 
						 WHERE in_date2 = '' AND in_from not like '%결재%' AND in_free='0' ";
	sql_query($sql_in_date2);

	sql_query("UPDATE `point_income_back` set `in_date` = left(in_date2,10);");/* in_date 업데이트 */
	
	/* 캐쉬, 포인트, 금액 0일 경우 삭제 */
	sql_query("DELETE FROM `point_income_back` WHERE  `in_point` = '0' AND  `in_cash` = '0' AND  `in_cash_won` = '0' ");

	/* 캐쉬, 포인트, 금액 0일 경우 삭제 */
	sql_query("DELETE FROM `point_income_back` WHERE  `in_point` = '0' AND  `in_cash` = '0' AND  `in_cash_won` = '0' ");

	/* 구매 갯수 */	
	sql_query("UPDATE `point_income_back` set `ex_count` = 1 WHERE `ex_count` = 0 ");
	die("`point_income_back` 처리완료:다시 새로고침해주세요.");
}else{
	echo $dbtable."_back 테이블이 복사 되였으며, 파일경로도 수정업데이트 되였습니다."."<br/>";
}

$sql_cp_point = "SELECT * FROM  `point_income_back` WHERE 1 ORDER BY in_date2 ASC  ";
$result_cp_point = sql_query($sql_cp_point);
echo $sql_cp_point."<br/>";


$sql_cp_list = array('mpi_member', 'mpi_member_idx');
array_push($sql_cp_list, 'mpi_won', 'mpi_cash_point', 'mpi_point');
array_push($sql_cp_list, 'mpi_cash_point_balance', 'mpi_point_balance');
array_push($sql_cp_list, 'mpi_state');
array_push($sql_cp_list, 'mpi_age', 'mpi_sex');
array_push($sql_cp_list, 'mpi_kind', 'mpi_user_agent', 'mpi_version');
array_push($sql_cp_list, 'mpi_from'); 
array_push($sql_cp_list, 'mpi_year_month', 'mpi_year', 'mpi_month', 'mpi_day', 'mpi_hour', 'mpi_week');
array_push($sql_cp_list, 'mpi_date'); 


$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_point)) {
		$row_cp['mpi_member'] = $row_cp['mb_no'];
		$row_cp['mpi_member_idx'] = $row_cp['mb_idx'];

		$row_cp['mpi_won'] = ceil($row_cp['in_cash'] / 100) * 100;
		$row_cp['mpi_cash_point'] = ceil($row_cp['in_cash'] / 100);
		$row_cp['mpi_point'] = ceil($row_cp['in_point'] / 100);

		$row_cp['mpi_cash_point_balance'] = ceil($row_cp['in_cash_balance'] / 100);
		$row_cp['mpi_point_balance'] = ceil($row_cp['in_balance'] / 100);

		$row_cp['mpi_state'] = 1;

		$row_cp['mpi_age'] = $row_cp['mb_age'];
		$row_cp['mpi_sex'] = $row_cp['mb_sex'];

		$row_cp['mpi_kind'] = '';
		$row_cp['mpi_user_agent'] = '';
		$row_cp['mpi_version'] = NM_VERSION;

		$row_cp['mpi_from'] = $row_cp['in_from'];
		
		$row_cp['mpi_year_month'] = substr($row_cp['in_date2'], 0, 7);
		$row_cp['mpi_year'] = substr($row_cp['in_date2'], 0, 4);
		$row_cp['mpi_month'] = substr($row_cp['in_date2'], 5, 2);
		$row_cp['mpi_day'] = substr($row_cp['in_date2'], 8, 2);
		$row_cp['mpi_hour'] = substr($row_cp['in_date2'], 11, 2);
		$row_cp['mpi_week'] = date("w",strtotime($row_cp['in_date2']));

		$row_cp['mpi_date'] = $row_cp['in_date2'];		

		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
				$sql2_insert.= 'NULL, ';
			}else{
				$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
			}
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>