<?
include_once '_common.php'; // 공통


/* 기본설정 & 관리자 회원등급 추가 및 등별 구별지정 */
$t_config = "
CREATE TABLE IF NOT EXISTS `config` (
  `cf_no` INT(11) NOT NULL AUTO_INCREMENT,
  `cf_title` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '홈페이지명',
  `cf_admin_email` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '관리자이메일',
  `cf_admin_email_name` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '관리자이메일명',

  `cf_company` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '기업명',
  `cf_ceo` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '사업자명',
  `cf_business_no` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '사업자등록번호',
  `cf_mail_order_sales` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '통신판매업신고번호',
  `cf_address` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '기업주소',
  `cf_tel` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '기업전화번호',
  `cf_copy` VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'COPYRIGHT',

  `cf_admin_list` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '회원등급리스트-관리자',
  `cf_partner_list` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '회원등급리스트-파트너',
  `cf_client_list` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '회원등급리스트-고객',
  
  `cf_admin_leve` TINYINT(4) NOT NULL DEFAULT '21' COMMENT '관계자등급레벨',
  `cf_parter_leve` TINYINT(4) NOT NULL DEFAULT '11' COMMENT '파트너등급레벨',

  `cf_big` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '코믹스분류리스트',
  `cf_small` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '장르리스트',

  `s_limit` TINYINT(4) NOT NULL DEFAULT '30' COMMENT '기본출력수',
  `s_limit_min` TINYINT(4) NOT NULL DEFAULT '10' COMMENT '최소출력수',
  `s_limit_max` TINYINT(4) NOT NULL DEFAULT '50' COMMENT '최대출력수',


  `cf_pay` VARCHAR(50) NOT NULL DEFAULT '0|1|2|3|4|5|10' COMMENT '코인리스트',
  `cf_pay_basic` TINYINT(4) NOT NULL DEFAULT '3' COMMENT '코인 디폴트',
  `cf_cash_point_unit` VARCHAR(2) NOT NULL DEFAULT 'C' COMMENT '코인단위',
  `cf_cash_point_unit_ko` VARCHAR(10) NOT NULL DEFAULT '코인' COMMENT '코인 한국어단위',
  `cf_point_unit` VARCHAR(2) NOT NULL DEFAULT 'M' COMMENT '보너스코인단위',
  `cf_point_unit_ko` VARCHAR(10) NOT NULL DEFAULT '보너스코인' COMMENT '보너스코인 한국어단위',

  `cf_cup_type` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '정산타입:config_unit_pay-연관',
  `cf_toss_test` VARCHAR(2) NOT NULL DEFAULT 'y' COMMENT '테스트여부(y:테스트모드, n:실결제모드)',
  
  `cf_public_cycle` VARCHAR(255) NOT NULL DEFAULT '주기없음|월|화|수|목|금|토|일|보름|월간' COMMENT '연재주기-0:주기없음,1:월,2:화,4:수,8:목,16:금,32:토,64:일,128:보름,256:월간',
  `cf_platform` VARCHAR(255) NOT NULL DEFAULT '웹|Android|iOS|Onestore' COMMENT '이용환경-1:웹,2:Android,4:iOS,8:Onestore',
  `cf_comics_type` VARCHAR(255) NOT NULL DEFAULT '사용안함|PD추천만화|hot만화|best만화' COMMENT '컨텐츠유형-0:사용안함,1:PD추천만화,2:hot만화,3:best만화',
  
  `cf_er_type` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '충전이벤트타입명',

  `cf_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`cf_no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('config','cf_no'));

/*  config_only_id
	유일한 키 만드는 테이블
    사용하는 곳 :
    1. 글쓰기시 미리 유일키를 얻어 파일 업로드 필드에 넣는다.
    2. 주문번호 생성시에 사용한다.
    3. 기타 유일키가 필요한 곳에서 사용한다.
	4. 어느기간 지날시 자동삭제 기능을 넣어준다.
*/
$t_config_only_id = "
CREATE TABLE IF NOT EXISTS `config_only_id` (
  `coi_no` INT(11) NOT NULL AUTO_INCREMENT,
  `coi_id` BIGINT(20) unsigned NOT NULL DEFAULT '0'  COMMENT '고유ID',
  `coi_ip` VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'IP',
  `coi_year` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '연도',
  `coi_month` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '월',
  `coi_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`coi_no`,`coi_id`),
  UNIQUE KEY `coi_id` (`coi_id`), 
  KEY `coi_year` (`coi_year`),
  KEY `coi_month` (`coi_month`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('config_only_id','coi_no'));

/* 설정시 긴글들 */
$t_config_long_text = "
CREATE TABLE IF NOT EXISTS `config_long_text` (
  `clt_no` INT(11) NOT NULL AUTO_INCREMENT,
  `clt_terms` TEXT NOT NULL DEFAULT '' COMMENT '이용약관',
  `clt_pinfo` TEXT NOT NULL DEFAULT '' COMMENT '개인정보취급방침',
  `clt_ipin` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '본인인증',
  `clt_mb_leave` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '회원탈퇴시',
  `clt_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`clt_no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('config_long_text','clt_no'));

/* 정산 단위 */
$t_config_unit_pay = "
CREATE TABLE IF NOT EXISTS `config_unit_pay` (
  `cup_no` INT(11) NOT NULL AUTO_INCREMENT,
  `cup_type` TINYINT(4)  DEFAULT NULL COMMENT '정산타입',
  `cup_won` INT(11)  DEFAULT NULL COMMENT '정산금액',
  `cup_member` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '저장회원',
  `cup_id` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '저장회원ID',
  `cup_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`cup_no`),
  UNIQUE KEY `cup_type` (`cup_type`), 
  UNIQUE KEY `cup_won` (`cup_won`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('config_unit_pay','cup_no'));

/* admin_cms_access -> 01_mzzang_t01_admin.php 여기서 생성
회원 혜택
관리자가 CMS 메뉴 권한 등급별 권한 지정
*/

/* 충전시 가격 */
$t_config_recharge_price= "
CREATE TABLE IF NOT EXISTS `config_recharge_price` (
  `crp_no` INT(11) NOT NULL AUTO_INCREMENT,
  `crp_name` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '충전 코인 명',

  `crp_cash_point` INT(7) NOT NULL DEFAULT '0' COMMENT '충전 코인', 
  `crp_point` INT(7) NOT NULL DEFAULT '0' COMMENT '충전 보너스 코인', 
  `crp_point_percent` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '충전 추가 보너스 코인 퍼센트', 
  `crp_won` INT(11) NOT NULL DEFAULT '0' COMMENT '충전 가격', 

  `crp_advise` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '충전 코인 추천(y:추천, n:비추천)',

  `crp_payway_limit` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '충전시 결제수단제약(빈칸:제약없음, ph:폰, card:카드, virtual:가상계좌, toss:토스)', 

  `crp_event_mb_won_count` VARCHAR(50) DEFAULT NULL COMMENT '충전 결제 카운터 이벤트:member-mb_won_count연관',

  `crp_date_type` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '기간타입(y:기간만적용, n:무기간, m:매월&기간:날짜빈칸시무기간, w:매주:날짜빈칸시무기간)',
  `crp_state`  VARCHAR(2) NULL DEFAULT 'n' COMMENT '상태(y:진행중, n:중지, r:예약)',

  `crp_date_start` VARCHAR(20) DEFAULT NULL DEFAULT '' COMMENT '기간시작',
  `crp_date_end` VARCHAR(20) DEFAULT NULL DEFAULT '' COMMENT '기간마감',

  `crp_holiday` TINYINT(4) DEFAULT NULL COMMENT '매월특정일',
  `crp_holiweek` TINYINT(4) DEFAULT NULL COMMENT '매주특정요일',

  `crp_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`crp_no`),

  KEY `crp_cash_point` (`crp_cash_point`), 
  KEY `crp_point` (`crp_point`), 
  
  KEY `crp_advise` (`crp_advise`), 

  KEY `crp_payway_limit` (`crp_payway_limit`),

  KEY `crp_event_mb_won_count` (`crp_event_mb_won_count`),

  KEY `crp_date_type` (`crp_date_type`), 
  KEY `crp_state` (`crp_state`), 

  UNIQUE KEY `crp_holiday` (`crp_holiday`), 
  UNIQUE KEY `crp_holiweek` (`crp_holiweek`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('config_recharge_price','crp_no'));


?>