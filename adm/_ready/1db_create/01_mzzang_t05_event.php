<?
include_once '_common.php'; // 공통


/* 무료&코인 설정 */
$t_event_pay = "
CREATE TABLE IF NOT EXISTS `event_pay` (
  `ep_no` INT(11) NOT NULL AUTO_INCREMENT,
  `ep_name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '이벤트명',
  `ep_text` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '이벤트내용',
  `ep_banner` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '이미지 파일경로',

  `ep_adult` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성인여부(y:성인, n:청소년)',

  `ep_date_type` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '기간타입(y:기간만적용, n:무기간, m:매월&기간:날짜빈칸시무기간, w:매주&기간:날짜빈칸시무기간)',
  `ep_state`  VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '상태(y:진행중, n:중지, r:예약)',

  `ep_date_start` VARCHAR(20) DEFAULT NULL DEFAULT '' COMMENT '기간시작',
  `ep_date_end` VARCHAR(20) DEFAULT NULL DEFAULT '' COMMENT '기간마감',

  `ep_holiday` TINYINT(4) DEFAULT NULL COMMENT '매월특정일',
  `ep_holiweek` TINYINT(4) DEFAULT NULL COMMENT '매주특정요일',

  `ep_click` INT(11) unsigned NOT NULL DEFAULT '0' COMMENT '배너클릭수',

  `ep_reg_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '등록일',
  `ep_mod_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '수정일',

  PRIMARY KEY (`ep_no`),

  KEY `ep_adult` (`ep_adult`), 

  KEY `ep_date_type` (`ep_date_type`), 
  KEY `ep_state` (`ep_state`), 

  UNIQUE KEY `ep_holiday` (`ep_holiday`), 
  UNIQUE KEY `ep_holiweek` (`ep_holiweek`)

) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('event_pay','ep_no'));

/* 무료&코인 설정- 코믹스리스트 */
$t_event_pay_comics = "
CREATE TABLE IF NOT EXISTS `event_pay_comics` (
  `epc_no` INT(11) NOT NULL AUTO_INCREMENT,
  `epc_ep_no` INT(11) NOT NULL DEFAULT '0' COMMENT '이벤트번호',
  `epc_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',

  `epc_free` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '무료 여부(y:무료, n:유료)',
  `epc_free_s` INT(7) NOT NULL DEFAULT '1' COMMENT '무료 에피소드 시작 화수 ',
  `epc_free_e` INT(7) NOT NULL DEFAULT '0' COMMENT '무료 에피소드 마감 화수 ',

  `epc_sale` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '세일 여부(y:무료, n:유료)',
  `epc_sale_s` INT(7) NOT NULL DEFAULT '1' COMMENT '세일 에피소드 시작 화수 ',
  `epc_sale_e` INT(7) NOT NULL DEFAULT '0' COMMENT '세일 에피소드 마감 화수 ',
  `epc_sale_pay` INT(7) NOT NULL DEFAULT '0' COMMENT '세일 가격',

  `epc_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`epc_no`),
  KEY `epc_event_pay` (`epc_event_pay`),
  KEY `epc_comics` (`epc_comics`),
  
  KEY `epc_free` (`epc_free`),
  
  KEY `epc_sale` (`epc_sale`),

  KEY `epc_sale_pay` (`epc_sale_pay`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('event_pay_comics','epc_no'));


/* 출석체크 날짜설정 */
$t_event_attend_date = "
CREATE TABLE IF NOT EXISTS `event_attend_date` (
  `ead_no` INT(11) NOT NULL AUTO_INCREMENT,
  `ead_month` TINYINT(4) DEFAULT NULL COMMENT '월',
  `ead_use` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '사용여부(y:사용, n:안함)',
  `ead_end_day` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '기간:몇일까지사용',
  `ead_title` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '이벤트명',
  `ead_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`ead_no`),
  UNIQUE KEY `ead_month` (`ead_month`), 
  KEY `ead_use` (`ead_use`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('event_attend_date','ead_no'));

/* 출석체크 코인설정 */
$t_event_attend_point = "
CREATE TABLE IF NOT EXISTS `event_attend_point` (
  `eap_no` INT(11) NOT NULL AUTO_INCREMENT,
  `eap_day` TINYINT(4) DEFAULT NULL COMMENT '일',
  `eap_cash_point` INT(7) NOT NULL DEFAULT '0' COMMENT '지급 코인',
  `eap_point` INT(7) NOT NULL DEFAULT '0' COMMENT '지급 보너스 코인',
  `eap_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`eap_no`),
  UNIQUE KEY `eap_day` (`eap_day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('event_attend_point','eap_no'));

/* 충전시 추가이벤트 */
$t_event_recharge = "
CREATE TABLE IF NOT EXISTS `event_recharge` (
  `er_no` INT(11) NOT NULL AUTO_INCREMENT,
  `er_type` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '이벤트타입',

  `er_date_type` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '기간타입(y:기간만적용, n:무기간, m:매월&기간:날짜빈칸시무기간, w:매주&기간:날짜빈칸시무기간)',
  `er_state`  VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '이벤트상태(y:진행중, n:중지, r:이벤트예약)',

  `er_date_start` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '기간시작',
  `er_date_end` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '기간마감',

  `er_holiday` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '매월특정일',
  `er_holiweek` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '매주특정요일',

  `er_calc_way` VARCHAR(2) NOT NULL DEFAULT 'p' COMMENT '이벤트 지급 계산법(p:퍼센트로덧셈, m:곱셉, a:덧셈)',

  `er_cash_point` INT(7) NOT NULL DEFAULT '0' COMMENT '이벤트 지급 코인',
  `er_point` INT(7) NOT NULL DEFAULT '0' COMMENT '이벤트 지급 보너스 코인',

  `er_reg_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '등록일',
  `er_mod_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '수정일',

  PRIMARY KEY (`er_no`),
  KEY `er_type` (`er_type`),
  KEY `er_state` (`er_state`),
  KEY `er_calc_way` (`er_calc_way`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('event_recharge','er_no'));

/* 화 전체 결제시 추가 보너스설정 */
$t_event_allbuy_point = "
CREATE TABLE IF NOT EXISTS `event_allbuy_point` (
  `eap_no` INT(11) NOT NULL AUTO_INCREMENT,
  `eap_episode_count` INT(7) NOT NULL DEFAULT '0' COMMENT '구매 화수', 
  `eap_point` INT(7) NOT NULL DEFAULT '0' COMMENT '지급될 보너스 코인', 

  `eap_date_type` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '기간타입(y:기간만적용, n:무기간, m:매월&기간:날짜빈칸시무기간, w:매주&기간:날짜빈칸시무기간)',
  `eap_state`  VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '이벤트상태(y:진행중, n:중지, r:이벤트예약)',

  `eap_date_start` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '기간시작',
  `eap_date_end` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '기간마감',

  `eap_holiday` TINYINT(4) DEFAULT NULL COMMENT '매월특정일',
  `eap_holiweek` TINYINT(4) DEFAULT NULL COMMENT '매주특정요일',

  `eap_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`eap_no`),

  KEY `eap_episode_count` (`eap_episode_count`), 
  KEY `eap_point` (`eap_point`), 

  KEY `eap_date_type` (`eap_date_type`), 
  KEY `eap_state` (`eap_state`), 

  UNIQUE KEY `eap_holiday` (`eap_holiday`), 
  UNIQUE KEY `eap_holiweek` (`eap_holiweek`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('event_allbuy_point','eap_no'));

/* 쿠폰 발급 */
$t_event_couponment = "
CREATE TABLE IF NOT EXISTS `event_couponment` (
  `ecm_no` INT(11) NOT NULL AUTO_INCREMENT,
  `ecm_ticket` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '쿠폰명',
  `ecm_class` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '쿠폰등급',
  `ecm_count` INT(7) NOT NULL DEFAULT '1' COMMENT '쿠폰사용 누적 횟수 제한(0:무제한)',

  `ecm_cash_point` INT(7) NOT NULL DEFAULT '0' COMMENT '쿠폰 코인',
  `ecm_point` INT(7) NOT NULL DEFAULT '0' COMMENT '쿠폰 보너스 코인',

  `ecm_state`  VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '쿠폰상태(y:진행중, n:중지)',
  `ecm_days`  INT(5) NOT NULL DEFAULT '0' COMMENT '일:365표기',

  `ecm_start_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '시작&등록일',
  `ecm_end_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '마감일',

  PRIMARY KEY (`ecm_no`),
  KEY `ecm_class` (`ecm_class`),
  
  KEY `ecm_state` (`ecm_state`), 
  KEY `ecm_days` (`ecm_days`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('event_couponment','ecm_no'));

/* 쿠폰 번호 */
$t_event_coupon = "
CREATE TABLE IF NOT EXISTS `event_coupon` (
  `ec_no` INT(11) NOT NULL AUTO_INCREMENT,
  `ec_couponment` INT(11) NOT NULL DEFAULT '0' COMMENT '쿠폰 발급 번호',
  `ec_days`  INT(5) NOT NULL DEFAULT '0' COMMENT '일:365표기',

  `ec_code` VARCHAR(20) DEFAULT NULL COMMENT '쿠폰번호16자리',
  `ec_count` INT(7) NOT NULL DEFAULT '0' COMMENT '쿠폰사용 누적 횟수',

  `ec_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호', 
  `ec_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `ec_reg_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '등록일',
  `ec_use_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '사용일',

  PRIMARY KEY (`ec_no`),
  UNIQUE KEY `ec_code` (`ec_code`), 

  KEY `ec_couponment` (`ec_couponment`),
  KEY `ec_days` (`ec_days`), 

  KEY `ec_count` (`ec_count`), 
  
  KEY `ec_member` (`ec_member`), 
  KEY `ec_member_idx` (`ec_member_idx`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('event_coupon','ec_no'));


?>