<?
include_once '_common.php'; // 공통



/* 회원 코믹스 구매 정보 */

$t_member_point_expen_comics = "
CREATE TABLE IF NOT EXISTS `member_point_expen_comics` (
  `mpec_no` INT(11) NOT NULL AUTO_INCREMENT,
  `mpec_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `mpec_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `mpec_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',
  `mpec_big` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '코믹스분류',
  `mpec_small` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '장르',

  `mpec_cash_type` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '정산금액타입:config_unit_pay-연관',

  `mpec_cash_point` INT(7) NOT NULL DEFAULT '0' COMMENT '구매 코인', 
  `mpec_point` INT(7) NOT NULL DEFAULT '0' COMMENT '구매 보너스 코인', 
  `mpec_count` INT(11) NOT NULL DEFAULT '0' COMMENT '구매 횟수',

  `mpec_cash_point_balance` INT(11) NOT NULL DEFAULT '0' COMMENT '회원 구매후 총 코인', 
  `mpec_point_balance` INT(11) NOT NULL DEFAULT '0' COMMENT '회원 구매후 총 보너스 코인', 
  
  `mpec_state` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '구매상태(0:결재전, 1:완료, 2:취소, 3:환불, 4:CMS제공, 5:이벤트, 6:에러)',
  `mpec_sale` INT(5) NOT NULL DEFAULT '0' COMMENT '세일 누적수',
  `mpec_free` INT(5) NOT NULL DEFAULT '0' COMMENT '무료 누적수',
  `mpec_way` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '구매방법(0:에러, 1:개별, 7:전체구매)',

  `mpec_age` INT(5) NOT NULL DEFAULT '0' COMMENT '연령',
  `mpec_sex` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성별(m:남자, w:여자, n:미인증)',

  `mpec_kind` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '구매환경',
  `mpec_user_agent` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '구매자세한환경',
  `mpec_version` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '구매버전',

  `mpec_year_month` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
  `mpec_year` VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
  `mpec_month` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
  `mpec_day` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
  `mpec_hour` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
  `mpec_week` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',

  `mpec_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '구매일',

  PRIMARY KEY (`mpec_no`),
  KEY `mpec_member` (`mpec_member`),
  KEY `mpec_member_idx` (`mpec_member_idx`),

  KEY `mpec_comics` (`mpec_comics`),
  KEY `mpec_big` (`mpec_big`),
  KEY `mpec_small` (`mpec_small`),

  KEY `mpec_cash_type` (`mpec_cash_type`),

  KEY `mpec_age` (`mpec_age`),
  KEY `mpec_sex` (`mpec_sex`),

  KEY `mpec_state` (`mpec_state`),
  KEY `mpec_sale` (`mpec_sale`),
  KEY `mpec_free` (`mpec_free`),
  KEY `mpec_way` (`mpec_way`),

  KEY `mpec_kind` (`mpec_kind`), 

  KEY `mpec_year_month` (`mpec_year_month`),
  KEY `mpec_year` (`mpec_year`),
  KEY `mpec_month` (`mpec_month`),
  KEY `mpec_day` (`mpec_day`),
  KEY `mpec_hour` (`mpec_hour`),
  KEY `mpec_week` (`mpec_week`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('member_point_expen_comics','mpec_no'));

/* 회원 코믹스 구매 코믹스 화 단행본리스트 */

$t_member_point_expen_episode_1 = "
CREATE TABLE IF NOT EXISTS `member_point_expen_episode_1` (
  `mpee_no` INT(11) NOT NULL AUTO_INCREMENT,
  `mpee_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `mpee_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `mpec_no` INT(11) NOT NULL DEFAULT '1' COMMENT '회원 코믹스 구매 정보:member_point_expen_comics-연관',
  `mpee_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',
  `mpee_big` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '코믹스분류',
  `mpee_small` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '장르',
  `mpee_episode` INT(11) NOT NULL DEFAULT '0' COMMENT '에피소드',
  `mpee_chapter` INT(11) NOT NULL DEFAULT '0' COMMENT '화번호',

  `mpee_cash_type` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '정산금액타입:config_unit_pay-연관',

  `mpee_cash_point` INT(7) NOT NULL DEFAULT '0' COMMENT '구매 코인', 
  `mpee_point` INT(7) NOT NULL DEFAULT '0' COMMENT '구매 보너스 코인', 

  `mpee_cash_point_balance` INT(11) NOT NULL DEFAULT '0' COMMENT '회원 구매후 총 코인', 
  `mpee_point_balance` INT(11) NOT NULL DEFAULT '0' COMMENT '회원 구매후 총 보너스 코인', 
  
  `mpee_state` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '구매상태(0:결재전, 1:완료, 2:취소, 3:환불, 4:CMS제공, 5:이벤트, 6:에러)',
  `mpee_sale` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '세일 여부(y:세일, n:정가)',
  `mpee_free` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '무료 여부(y:무료, n:유료)',
  `mpee_way` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '구매방법(1:개별, 7:전체구매)',

  `mpee_age` INT(5) NOT NULL DEFAULT '0' COMMENT '연령',
  `mpee_sex` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성별(m:남자, w:여자, n:미인증)',

  `mpee_kind` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '구매환경',
  `mpee_user_agent` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '구매자세한환경',
  `mpee_version` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '구매버전',

  `mpee_year_month` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
  `mpee_year` VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
  `mpee_month` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
  `mpee_day` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
  `mpee_hour` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
  `mpee_week` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',

  `mpee_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '구매일',

  PRIMARY KEY (`mpee_no`),
  KEY `mpee_member` (`mpee_member`),
  KEY `mpee_member_idx` (`mpee_member_idx`),

  KEY `mpec_no` (`mpec_no`),
  KEY `mpee_comics` (`mpee_comics`),
  KEY `mpee_big` (`mpee_big`),
  KEY `mpee_small` (`mpee_small`),
  KEY `mpee_episode` (`mpee_episode`),
  KEY `mpee_chapter` (`mpee_chapter`),

  KEY `mpee_cash_type` (`mpee_cash_type`),

  KEY `mpee_state` (`mpee_state`),
  KEY `mpee_sale` (`mpee_sale`),
  KEY `mpee_free` (`mpee_free`),
  KEY `mpee_way` (`mpee_way`),

  KEY `mpee_age` (`mpee_age`),
  KEY `mpee_sex` (`mpee_sex`),

  KEY `mpee_kind` (`mpee_kind`), 

  KEY `mpee_year_month` (`mpee_year_month`),
  KEY `mpee_year` (`mpee_year`),
  KEY `mpee_month` (`mpee_month`),
  KEY `mpee_day` (`mpee_day`),
  KEY `mpee_hour` (`mpee_hour`),
  KEY `mpee_week` (`mpee_week`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('member_point_expen_episode_1','mpee_no'));

/* 회원 코믹스 화폐 충전 내역 */
$t_member_point_income = "
CREATE TABLE IF NOT EXISTS `member_point_income` (
  `mpi_no` INT(11) NOT NULL AUTO_INCREMENT,
  `mpi_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `mpi_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `mpi_won` INT(11) NOT NULL DEFAULT '0' COMMENT '결제금액',
  `mpi_cash_point` INT(7) NOT NULL DEFAULT '0' COMMENT '충전 코인', 
  `mpi_point` INT(7) NOT NULL DEFAULT '0' COMMENT '충전 보너스 코인', 

  `mpi_cash_point_balance` INT(11) NOT NULL DEFAULT '0' COMMENT '회원 충전후 총 충전 코인', 
  `mpi_point_balance` INT(11) NOT NULL DEFAULT '0' COMMENT '회원 충전후 총  보너스 코인', 

  `mpi_state` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '구매상태구매상태(0:결재전, 1:완료, 2:취소, 3:환불, 4:CMS제공, 5:이벤트, 6:에러)',
  `mpi_free` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '무료 여부(y:무료, n:유료)',

  `mpi_age` INT(5) NOT NULL DEFAULT '0' COMMENT '연령',
  `mpi_sex` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성별(m:남자, w:여자, n:미인증)',

  `mpi_kind` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '구매환경',
  `mpi_user_agent` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '구매자세한환경',
  `mpi_version` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '구매버전',

  `mpi_from` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '충전 내역',

  `mpi_year_month` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
  `mpi_year` VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
  `mpi_month` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
  `mpi_day` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
  `mpi_hour` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
  `mpi_week` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',

  `mpi_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '충전일',

  PRIMARY KEY (`mpi_no`),
  KEY `mpi_member` (`mpi_member`),
  KEY `mpi_member_idx` (`mpi_member_idx`),

  KEY `mpi_state` (`mpi_state`),
  KEY `mpi_free` (`mpi_free`),

  KEY `mpi_age` (`mpi_age`),
  KEY `mpi_sex` (`mpi_sex`),
  KEY `mpi_kind` (`mpi_kind`), 

  KEY `mpi_year_month` (`mpi_year_month`),
  KEY `mpi_year` (`mpi_year`),
  KEY `mpi_month` (`mpi_month`),
  KEY `mpi_day` (`mpi_day`),
  KEY `mpi_hour` (`mpi_hour`),
  KEY `mpi_week` (`mpi_week`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('member_point_income','mpi_no'));


?>