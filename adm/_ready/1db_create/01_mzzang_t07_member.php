<?
include_once '_common.php'; // 공통



/* 회원정보 */
$t_member = "
CREATE TABLE IF NOT EXISTS `member` (
  `mb_no` INT(11) NOT NULL AUTO_INCREMENT,
  `mb_id` VARCHAR(30) DEFAULT NULL COMMENT '회원ID',
  `mb_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',
  `mb_pass` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '패스워드',
  `mb_name` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '성명',
  `mb_level` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '등급',
  `mb_class` VARCHAR(2) NOT NULL DEFAULT 'c' COMMENT '구분(c:고객, p:파트너, a:관리자)',

  `mb_email` VARCHAR(50) DEFAULT NULL COMMENT '이메일',
  `mb_phone` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '핸드폰 번호',

  `mb_state`  VARCHAR(2) NOT NULL DEFAULT 'y' COMMENT '상태(y:진행중, n:탈퇴, s:정지)',

  `mb_attend_day` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '출석일',
  `mb_attend_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '출석일저장일',

  `mb_cash_point` INT(11) NOT NULL DEFAULT '0' COMMENT '보유 코인',
  `mb_point` INT(11) NOT NULL DEFAULT '0' COMMENT '보유 보너스 코인',

  `mb_sex` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성별(m:남자, w:여자, n:미인증)',
  `mb_post` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '이메일수신(y:승인, n:거부)',
  `mb_adult` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성인여부(y:성인, n:청소년)',
  `mb_ipin` text NOT NULL DEFAULT '' COMMENT '본인인증코드',
  `mb_overlap` INT(11) NOT NULL DEFAULT '0' COMMENT '인증값중복 체크(0:미인증or첫인증, 0이상의숫자:중복가입-회원번호적용)',

  `mb_won` INT(11) NOT NULL DEFAULT '0' COMMENT '충전 누적 금액',
  `mb_won_count` INT(11) NOT NULL DEFAULT '0' COMMENT '충전 횟수',
  `mb_birth` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '생년월일',

  `mb_join_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '가입일',
  `mb_out_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '탈퇴일',
  `mb_stop_date` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '중지일',
  `mb_login_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '로그일',
  `mb_human_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '휴면일',
  `mb_human_email_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '휴면 메일송신일',
  `mb_human_note` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '휴면비고',

  PRIMARY KEY (`mb_no`),
  UNIQUE KEY `mb_id` (`mb_id`),

  KEY `mb_idx` (`mb_idx`),
  KEY `mb_level` (`mb_level`),
  KEY `mb_class` (`mb_class`),
  KEY `mb_state` (`mb_state`),

  KEY `mb_sex` (`mb_sex`),
  KEY `mb_post` (`mb_post`),
  KEY `mb_adult` (`mb_adult`),
  KEY `mb_overlap` (`mb_overlap`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('member','mb_no'));

/* 회원 코믹스 즐겨찾기 */
$t_member_comics_mark = "
CREATE TABLE IF NOT EXISTS `member_comics_mark` (
  `mcm_no` INT(11) NOT NULL AUTO_INCREMENT,
  `mcm_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `mcm_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `mcm_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',
  `mcm_big` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '코믹스분류',
  `mcm_small` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '장르',

  `mcm_alarm` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '업데이트 알람여부(y:알람, n:안함)',

  `mcm_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '즐겨찾기저장일',

  PRIMARY KEY (`mcm_no`),
  KEY `mcm_member` (`mcm_member`), 
  KEY `mcm_member_idx` (`mcm_member_idx`), 

  KEY `mcm_comics` (`mcm_comics`), 
  KEY `mcm_big` (`mcm_big`), 
  KEY `mcm_small` (`mcm_small`), 

  KEY `mcm_alarm` (`mcm_alarm`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('member_comics_mark','mcm_no'));

/* 회원 코믹스 최근본 */
$t_member_comics_open = "
CREATE TABLE IF NOT EXISTS `member_comics_open` (
  `mco_no` INT(11) NOT NULL AUTO_INCREMENT,
  `mco_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `mco_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `mco_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',
  `mco_big` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '코믹스분류',
  `mco_small` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '장르',
  `mco_chapter` INT(11) NOT NULL DEFAULT '0' COMMENT '화번호',

  `mco_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`mco_no`),
  KEY `mco_member` (`mco_member`), 
  KEY `mco_member_idx` (`mco_member_idx`), 

  KEY `mco_comics` (`mco_comics`), 
  KEY `mco_big` (`mco_big`), 
  KEY `mco_small` (`mco_small`), 
  KEY `mco_chapter` (`mco_chapter`)

) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('member_comics_open','mco_no'));

/* 회원 소장 코믹스 리스트 */
$t_member_buy_comics = "
CREATE TABLE IF NOT EXISTS `member_buy_comics` (
  `mbc_no` INT(11) NOT NULL AUTO_INCREMENT,
  `mbc_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `mbc_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `mbc_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',
  `mbc_big` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '코믹스분류',

  `mbc_view` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '소장숨김여부(y:숨김, n:공개)',
  `mbc_recent_chapter` INT(11) NOT NULL DEFAULT '0' COMMENT '최근 본 화번호',
  `mbc_own_type` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '소장타입(0:무료, 1:소장, 2:대여, 3:에러)',

  `mbc_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`mbc_no`),
  KEY `mbc_member` (`mbc_member`),
  KEY `mbc_member_idx` (`mbc_member_idx`),

  KEY `mbc_comics` (`mbc_comics`),
  KEY `mbc_big` (`mbc_big`),

  KEY `mbc_view` (`mbc_view`),
  KEY `mbc_recent_chapter` (`mbc_recent_chapter`), 
  KEY `mbc_own_type` (`mbc_own_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('member_buy_comics','mbc_no'));

/* 회원 소장 코믹스 리스트 */
$t_member_buy_episode_1 = "
CREATE TABLE IF NOT EXISTS `member_buy_episode_1` (
  `mbe_no` INT(11) NOT NULL AUTO_INCREMENT,
  `mbe_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `mbe_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',
  
  `mbe_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',
  `mbe_big` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '코믹스분류',

  `mbe_chapter` INT(11) NOT NULL DEFAULT '0' COMMENT '구매 화번호',
  `mbe_way` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '구매방법(0:에러, 1:개별, 7:전체구매)',
  `mbe_own_type` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '소장타입(0:무료, 1:소장, 2:대여, 3:에러)',

  `mbe_end_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '구매보유유효일',
  `mbe_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '구매일',

  PRIMARY KEY (`mbe_no`),
  KEY `mbe_member` (`mbe_member`),
  KEY `mbe_member_idx` (`mbe_member_idx`),

  KEY `mbe_comics` (`mbe_comics`),
  KEY `mbe_big` (`mbe_big`),

  KEY `mbe_chapter` (`mbe_chapter`),
  KEY `mbe_way` (`mbe_way`),
  KEY `mbe_own_type` (`mbe_own_type`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('member_buy_episode_1','mbe_no'));

/* 회원 충전 */
$t_member_cash = "
CREATE TABLE IF NOT EXISTS `member_cash` (
  `mc_no` INT(11) NOT NULL AUTO_INCREMENT,
  `mc_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `mc_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `mc_won` INT(11) NOT NULL DEFAULT '0' COMMENT '결제금액',
  `mc_cash_point` INT(7) NOT NULL DEFAULT '0' COMMENT '충전코인',
  `mc_point` INT(7) NOT NULL DEFAULT '0' COMMENT '충전보너스코인', 

  `mc_product` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '결제상품명',
  `mc_way` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '결제수단(빈칸:에러, ph:폰, card:카드, virtual:가상계좌, toss:토스)',

  `mc_order` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '결제주문번호:config_only_id-연관',
  `mc_order_access` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '결제접속:web:웹, mob:모바일, app:앱',

  `mc_pg` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '결제사:pg_channel_결제사DB-연관',
  `mc_pg_no` INT(11) NOT NULL DEFAULT '0' COMMENT '결제데이터번호:pg_channel_결제사DB-연관',
  `mc_crp` INT(11) NOT NULL DEFAULT '0' COMMENT '충전시 가격 DB번호:config_recharge_price-연관',
  `mc_event` INT(11) NOT NULL DEFAULT '0' COMMENT '충전시 추가 이벤트 DB번호:event_recharge-연관',

  `mc_re` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '재구매여부(0:아님, 1:재구매)',

  `mc_useragent` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '충전환경',
  `mc_version` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '충전버전',

  `mc_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`mc_no`),
  KEY `mc_member` (`mc_member`), 
  KEY `mc_member_idx` (`mc_member_idx`), 

  KEY `mc_won` (`mc_won`), 
  KEY `mc_cash_point` (`mc_cash_point`), 
  KEY `mc_point` (`mc_point`), 

  KEY `mc_product` (`mc_product`), 
  KEY `mc_way` (`mc_way`), 

  KEY `mc_order_access` (`mc_order_access`), 
  KEY `mc_pg` (`mc_pg`), 

  KEY `mc_crp` (`mc_crp`), 
  KEY `mc_event` (`mc_event`), 

  KEY `mc_re` (`mc_re`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('member_cash','mc_no'));

/* 탈퇴/휴면처리 회원:member_human -> member 복사 */

/* 가입시 실패 정보 */
$t_member_join_fail = "
CREATE TABLE IF NOT EXISTS `member_join_fail` (
  `mjf_no` INT(11) NOT NULL AUTO_INCREMENT,
  `mjf_member_id` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '회원ID',
  `mjf_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',
  `mjf_email` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '이메일',
  `mjf_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '가입일',

  PRIMARY KEY (`mjf_no`),
  KEY `mjf_member_id` (`mjf_member_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('member_join_fail','mjf_no'));

?>