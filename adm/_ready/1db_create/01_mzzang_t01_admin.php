<?
include_once '_common.php'; // 공통


/* 관리자가 CMS 메뉴 권한 등급별 권한 지정 */
$t_config_member_buff = "
CREATE TABLE IF NOT EXISTS `config_member_buff` (
  `cfmb_no` INT(11) NOT NULL AUTO_INCREMENT,
  `cfmb_member_class` VARCHAR(2) DEFAULT NULL COMMENT '회원구분(c:고객, p:파트너, a:관리자)',
  `cfmb_member_level` TINYINT(4) DEFAULT NULL COMMENT '회원레벨',

  `cfmb_class_premium` VARCHAR(255) DEFAULT NULL COMMENT '회원구분별 혜택',
  `cfmb_level_premium` VARCHAR(255) DEFAULT NULL COMMENT '회원등급별 혜택',

  `cfmb_permission` VARCHAR(255) DEFAULT NULL COMMENT '회원등급별 CMS 메뉴 접근 권한',
  `cfmb_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`cfmb_no`),
  UNIQUE KEY `cfmb_member_class` (`cfmb_member_class`), 
  UNIQUE KEY `cfmb_member_level` (`cfmb_member_level`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('config_member_buff','cfmb_no'));

/* CMS 이용자의 등록,수정,삭제, 다운로드 로그 */
$t_config_log = "
CREATE TABLE IF NOT EXISTS `config_log` (
  `cfl_no` INT(11) NOT NULL AUTO_INCREMENT,
  `cfl_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `cfl_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',
  `cfl_member_level` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '회원레벨',

  `cfl_file_name` VARCHAR(30) DEFAULT NULL DEFAULT '' COMMENT '파일명',
  `cfl_db_table` VARCHAR(30) DEFAULT NULL DEFAULT '' COMMENT '저장테이블',
  `cfl_event` VARCHAR(2) DEFAULT NULL DEFAULT '' COMMENT '커리문분류(i:등록, d:삭제, u:업데이트)',
  `cfl_sql` TEXT DEFAULT NULL DEFAULT '' COMMENT 'SQL커리문',

  `cfl_year_month` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
  `cfl_year` VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
  `cfl_month` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
  `cfl_day` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
  `cfl_hour` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
  `cfl_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`cfl_no`),
  KEY `cfl_member` (`cfl_member`), 
  KEY `cfl_member_idx` (`cfl_member_idx`), 
  KEY `cfl_member_level` (`cfl_member_level`), 

  KEY `cfl_file_name` (`cfl_file_name`), 
  KEY `cfl_event` (`cfl_event`), 

  KEY `cfl_year_month` (`cfl_year_month`), 
  KEY `cfl_year` (`cfl_year`), 
  KEY `cfl_month` (`cfl_month`), 
  KEY `cfl_day` (`cfl_day`), 
  KEY `cfl_hour` (`cfl_hour`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('config_log','cfl_no'));

?>