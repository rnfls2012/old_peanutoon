<?
include_once '_common.php'; // 공통


/* 프로모션 배너 설정 -  $nm_config['cf_banner_position'] 연동 */
$t_epromotion_banner = "
CREATE TABLE IF NOT EXISTS `epromotion_banner` (
	`emb_no` INT(11) NOT NULL AUTO_INCREMENT,
	`emb_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
	`emb_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

	`emb_name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '제목', 
	`emb_url` VARCHAR(255) NOT NULL COMMENT 'URL', 
	`emb_script` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT 'script사용여부(y:script사용, n:일반주소)',
	`emb_target` VARCHAR(11) NOT NULL DEFAULT '_self' COMMENT 'a태그속성', 

	`emb_adult` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성인여부(y:성인, n:청소년)',

	`emb_date_type` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '기간타입(y:기간만적용, n:무기간, m:매월&기간:날짜빈칸시무기간, w:매주&기간:날짜빈칸시무기간)',
	`emb_state`  VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '상태(y:진행중, n:중지, r:예약)',

	`emb_date_start` VARCHAR(20) DEFAULT NULL DEFAULT '' COMMENT '기간시작',
	`emb_date_end` VARCHAR(20) DEFAULT NULL DEFAULT '' COMMENT '기간마감',

	`emb_holiday` TINYINT(4) DEFAULT NULL COMMENT '매월특정일',
	`emb_holiweek` TINYINT(4) DEFAULT NULL COMMENT '매주특정요일',

	`emb_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

	PRIMARY KEY (`emb_no`), 
	KEY `emb_adult` (`emb_adult`),

	KEY `emb_date_type` (`emb_date_type`), 
	KEY `emb_state` (`emb_state`), 

	UNIQUE KEY `emb_holiday` (`emb_holiday`), 
	UNIQUE KEY `emb_holiweek` (`emb_holiweek`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8 
";
array_push($table_create, array('epromotion_banner','emb_no'));

/* 프로모션 배너 표지이미지 설정 */
$t_epromotion_banner_cover = "
CREATE TABLE IF NOT EXISTS `epromotion_banner_cover` (
	`embc_no` INT(11) NOT NULL AUTO_INCREMENT,

	`embc_emb_no` INT(11) NOT NULL COMMENT '배너번호:epromotion_banner-연관', 
	`embc_position` TINYINT(4) NOT NULL COMMENT '위치번호:cf_banner_position-연관', 
	
	`embc_cover` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '이미지 파일경로',
	`embc_order` INT(11) NOT NULL DEFAULT '0' COMMENT '순서',

	`embc_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

	PRIMARY KEY (`embc_no`), 
	KEY `embc_emb_no` (`embc_emb_no`), 
	KEY `embc_position` (`embc_position`), 
	KEY `embc_order` (`embc_order`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8 
";
array_push($table_create, array('epromotion_banner_cover','embc_no'));

/* 프로모션 팝업 설정 */
$t_epromotion_popup = "
CREATE TABLE IF NOT EXISTS `epromotion_popup` (
	`emp_no` INT(11) NOT NULL AUTO_INCREMENT,
	`emp_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
	`emp_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

	`emp_adult` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성인여부(y:성인, n:청소년)',

	`emp_device` VARCHAR(10) NOT NULL DEFAULT 'pc_mob' COMMENT '장치유형-pc:데스크탑,mob:모바일,pc_mob:데스크탑+모바일 ',
	
	`emp_state`  VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '이벤트상태(y:진행중, n:중지, r:이벤트예약)',
	`emp_date_start` VARCHAR(20) NOT NULL COMMENT '기간시작', 
	`emp_date_end` VARCHAR(20) NOT NULL COMMENT '기간마감', 

	`emp_disable_hours` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '팝업닫기쿠기설정',

	`emp_left` INT(7) NOT NULL DEFAULT '0' COMMENT '위치x값',
	`emp_top` INT(7) NOT NULL DEFAULT '0' COMMENT '위치y값',
	`emp_height` INT(7) NOT NULL DEFAULT '0' COMMENT '높이',
	`emp_width` INT(7) NOT NULL DEFAULT '0' COMMENT '넓이',

	`emp_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '제목',
	`emp_text` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '내용',
	`emp_text_html` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '에디터사용여부(0:안함, 1:사용)',
	`emp_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

	PRIMARY KEY (`emp_no`), 
	KEY `emp_member` (`emp_member`), 
	KEY `emp_member_idx` (`emp_member_idx`),

	KEY `emp_adult` (`emp_adult`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 
";
array_push($table_create, array('epromotion_popup','emp_no'));


/* 프로모션 이벤트 설정 */
$t_epromotion_event = "
CREATE TABLE IF NOT EXISTS `epromotion_event` (
	`eme_no` INT(11) NOT NULL AUTO_INCREMENT,
	`eme_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
	`eme_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index', 

	`eme_cover` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '이벤트 표지이미지',

	`eme_adult` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성인여부(y:성인, n:청소년)',

	`eme_date_type` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '기간타입(y:기간만적용, n:무기간, m:매월&기간:날짜빈칸시무기간, w:매주&기간:날짜빈칸시무기간)',
	`eme_state`  VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '상태(y:진행중, n:중지, r:예약)',

	`eme_date_start` VARCHAR(20) DEFAULT NULL DEFAULT '' COMMENT '기간시작',
	`eme_date_end` VARCHAR(20) DEFAULT NULL DEFAULT '' COMMENT '기간마감',

	`eme_holiday` TINYINT(4) DEFAULT NULL COMMENT '매월특정일',
	`eme_holiweek` TINYINT(4) DEFAULT NULL COMMENT '매주특정요일',
	
	`eme_event_type` VARCHAR(2) NOT NULL DEFAULT 't' COMMENT '이벤트타입(t:기본게시판, c:단일코믹스번호, e:이벤트번호)',
	`eme_event_no` INT(11) DEFAULT NULL COMMENT '이벤트연결번호(코믹스번호, 이벤트번호)',

	`eme_name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '이벤트명',
	`eme_text` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '내용',
	`eme_text_html` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '에디터사용여부(0:안함, 1:사용)',

	`eme_click` INT(11) unsigned NOT NULL DEFAULT '0' COMMENT '배너클릭수',

	`eme_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

	PRIMARY KEY (`eme_no`), 
	KEY `eme_member` (`eme_member`), 
	KEY `eme_member_idx` (`eme_member_idx`), 

	KEY `eme_adult` (`eme_adult`), 
	KEY `eme_date_type` (`eme_date_type`), 
	KEY `eme_state` (`eme_state`), 

	KEY `eme_holiday` (`eme_holiday`), 
	KEY `eme_holiweek` (`eme_holiweek`) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8 
";
array_push($table_create, array('epromotion_event','eme_no'));


?>