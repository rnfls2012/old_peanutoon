<?
include_once '_common.php'; // 공통


/* PG사 올더게이트 거래정보 */
$t_pg_channel_allthegate = "
CREATE TABLE IF NOT EXISTS `pg_channel_allthegate` (
  `ags_no` INT(11) NOT NULL AUTO_INCREMENT,
  `ags_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `AuthTy` VARCHAR(12) NOT NULL DEFAULT '' COMMENT '결제수단(빈칸:에러, ph:폰, card:카드, virtual:가상계좌)',
  `SubTy` VARCHAR(12) NOT NULL DEFAULT '' COMMENT '서브결제수단',
  `rStoreId` VARCHAR(22) NOT NULL DEFAULT '' COMMENT '업체ID',
  `rAmt` VARCHAR(14) NOT NULL DEFAULT '' COMMENT '거래금액',
  `rOrdNo` VARCHAR(42) NOT NULL DEFAULT '' COMMENT '주문번호',
  `rProdNm` VARCHAR(102) NOT NULL DEFAULT '' COMMENT '상품명',

  `rOrdNm` VARCHAR(42) NOT NULL DEFAULT '' COMMENT '주문자명(회원ID)',
  `rOrdNm_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '주문자명(회원ID)',

  `rSuccYn` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '성공여부',
  `rResMsg` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '실패사유',
  `rApprTm` VARCHAR(16) NOT NULL DEFAULT '' COMMENT '승인시각',

  `rBusiCd` VARCHAR(6) NOT NULL DEFAULT '' COMMENT '카드 전문코드',
  `rApprNo` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '카드 승인번호',
  `rCardCd` VARCHAR(6) NOT NULL DEFAULT '' COMMENT '카드 카드사코드',
  `rDealNo` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '카드 거래고유번호',
  `rCardNm` VARCHAR(22) NOT NULL DEFAULT '' COMMENT '카드 카드사명',
  `rMembNo` VARCHAR(16) NOT NULL DEFAULT '' COMMENT '카드 가맹점번호',
  `rAquiCd` VARCHAR(6) NOT NULL DEFAULT '' COMMENT '카드 매입사코드',
  `rAquiNm` VARCHAR(22) NOT NULL DEFAULT '' COMMENT '카드 매입사명',

  `rHP_TID` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '핸드폰 결제TID',
  `rHP_DATE` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '핸드폰 결제날짜',
  `rHP_HANDPHONE` VARCHAR(22) NOT NULL DEFAULT '' COMMENT '핸드폰 결제핸드폰번호',
  `rHP_COMPANY` VARCHAR(22) NOT NULL DEFAULT '' COMMENT '핸드폰 결제통신사명',

  `rVirNo` VARCHAR(102) NOT NULL DEFAULT '' COMMENT '가상계좌 가상계좌번호',
  `VIRTUAL_CENTERCD` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '가상계좌 입금은행코드',
  `ES_SENDNO` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '가상계좌 이지스에스크로',

  `Column1` INT(11) NOT NULL DEFAULT '0' COMMENT '코인',
  `Column2` INT(11) NOT NULL DEFAULT '0' COMMENT '보너스코인',
  `Column3` VARCHAR(255) DEFAULT NULL COMMENT '코인정보-이벤트번호',

  `useragent` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '사용자환경',
  `version` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '소스버전',
  `ags_version` VARCHAR(20) NOT NULL DEFAULT '' COMMENT 'PG사버전',
  `ags_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`ags_no`),
  KEY `ags_member` (`ags_member`), 
  KEY `rOrdNm` (`rOrdNm`), 
  KEY `rOrdNm_idx` (`rOrdNm_idx`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('pg_channel_allthegate','ags_no'));

/* PG사 올더게이트 가상계좌 입금된정보 */
$t_pg_channel_allthegate_virtual_get = "
CREATE TABLE IF NOT EXISTS `pg_channel_allthegate_virtual_get` (
  `no` INT(11) NOT NULL AUTO_INCREMENT,
  `trcode` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '거래코드',
  `service_id` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '상점ID',
  `orderdt` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '주문일자',
  `virno` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '가상계좌번호',
  `deal_won` INT(11) NOT NULL DEFAULT '0' COMMENT '입금액',
  `ordno` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '주문번호',
  `inputnm` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '입금자명',
  `ags_vg_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('pg_channel_allthegate_virtual_get','no'));

/* PG사 올더게이트 가상계좌 입금할정보 */
$t_pg_channel_allthegate_virtual_put = "
CREATE TABLE IF NOT EXISTS `pg_channel_allthegate_virtual_put` (
  `v_no` INT(11) NOT NULL AUTO_INCREMENT,
  `v_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `v_member_id` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '회원ID',
  `v_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `v_cash_point` INT(7) NOT NULL DEFAULT '0' COMMENT '구매 코인', 
  `v_point` INT(7) NOT NULL DEFAULT '0' COMMENT '구매 보너스 코인',

  `v_virnum` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '가상계좌번호',
  `v_pay` INT(11) NOT NULL DEFAULT '0' COMMENT '가상계좌 입금할 금액',

  `v_rStoreId` VARCHAR(22) NOT NULL DEFAULT '' COMMENT '업체ID',
  `v_rAmt` VARCHAR(14) NOT NULL DEFAULT '' COMMENT '거래금액',
  `v_rOrdNo` VARCHAR(42) NOT NULL DEFAULT '' COMMENT '주문번호',
  `v_rProdNm` VARCHAR(102) NOT NULL DEFAULT '' COMMENT '상품명',
  `v_rApprTm` VARCHAR(16) NOT NULL DEFAULT '' COMMENT '승인시각',

  `v_rVirNo` VARCHAR(102) NOT NULL DEFAULT '' COMMENT '가상계좌번호',
  `v_VIRTUAL_CENTERCD` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '가상계좌 입금은행코드',
  `v_ES_SENDNO` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '이지스에스크로',

  `v_useragent` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '저장환경',
  `v_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',
  PRIMARY KEY (`v_no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('pg_channel_allthegate_virtual_put','v_no'));

/* PG사 TOSS 거래정보 */
$t_pg_channel_toss = "
CREATE TABLE IF NOT EXISTS `pg_channel_toss` (
  `toss_no` int(11) NOT NULL AUTO_INCREMENT,
  `toss_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',

  `toss_id` varchar(30) NOT NULL COMMENT '회원ID',
  `toss_id_idx` varchar(2) NOT NULL COMMENT '회원ID 인덱스',

  `toss_orderNo` varchar(42) NOT NULL COMMENT '코인충전 주문번호',
  `toss_amount` int(11) NOT NULL COMMENT '결제 금액',
  `toss_cash_point` INT(7) NOT NULL DEFAULT '0' COMMENT '구매 코인', 
  `toss_point` INT(7) NOT NULL DEFAULT '0' COMMENT '구매 보너스 코인',

  `toss_productDesc` varchar(102) NOT NULL COMMENT '코인충전 상품명',
  `toss_date` varchar(20) NOT NULL COMMENT '코인충전날짜',

  `toss_useragent` varchar(255) NOT NULL COMMENT '코인충전 충전시환경',
  `toss_code` varchar(2) NOT NULL COMMENT '결재 생성 코드',
  `toss_pay_token` varchar(50) NOT NULL COMMENT '결재 생성 토큰',
  `toss_checkoutpage` varchar(255) NOT NULL COMMENT '결재 생성 URL',
  `toss_status` varchar(50) NOT NULL COMMENT '결제 최종 상태',
  `toss_callback_date` datetime NOT NULL COMMENT '결재 콜백 저장날짜',
  PRIMARY KEY (`toss_no`),
  KEY `toss_member` (`toss_member`),
  KEY `toss_id` (`toss_id`),
  KEY `toss_id_idx` (`toss_id_idx`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('pg_channel_toss','toss_no'));

?>