<?
include_once '_common.php'; // 공통


/* 로그 */
$t_stats_log_visit = "
CREATE TABLE IF NOT EXISTS `stats_log_visit` (
  `slv_no` int(11) NOT NULL AUTO_INCREMENT,
  `slv_ip` varchar(255) NOT NULL default '' COMMENT 'IP정보',

  `slv_referer` text NOT NULL DEFAULT '' COMMENT '이전페이지정보',

  `slv_useragent` VARCHAR(255) NOT NULL default '' COMMENT '사용자환경',
  `slv_version` VARCHAR(20) NOT NULL default '' COMMENT '버전',

  `slv_year_month` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
  `slv_year` VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
  `slv_month` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
  `slv_day` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
  `slv_hour` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
  `slv_week` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',

  `slv_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY  (`slv_no`),
  KEY `slv_ip` (`slv_ip`),

  KEY `slv_version` (`slv_version`),

  KEY `slv_year_month` (`slv_year_month`),
  KEY `slv_year` (`slv_year`),
  KEY `slv_month` (`slv_month`),
  KEY `slv_day` (`slv_day`),
  KEY `slv_hour` (`slv_hour`),
  KEY `slv_week` (`slv_week`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('stats_log_visit','slv_no'));

/* 회원로그인시 로그 */
$t_stats_log_member = "
CREATE TABLE IF NOT EXISTS `stats_log_member` (
  `slm_no` INT(11) NOT NULL AUTO_INCREMENT,
  `slm_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `slm_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `slm_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '로그인 날짜',
  `slm_type` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '로그인 환경',

  `slm_ip` varchar(20) NOT NULL default '' COMMENT 'IP정보',
  `slm_kind` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '접속환경',
  `slm_user_agent` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '접속자세한환경',
  `slm_version` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '접속버전',

  PRIMARY KEY (`slm_no`),
  KEY `slm_member` (`slm_member`), 
  KEY `slm_member_idx` (`slm_member_idx`), 

  KEY `slm_kind` (`slm_kind`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

";
array_push($table_create, array('stats_log_member','slm_no'));

?>