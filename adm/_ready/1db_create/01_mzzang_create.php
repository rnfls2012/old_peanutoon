<?
include_once '_common.php'; // 공통

/* 해당 데이터베이스의 테이블 일괄 삭제 -> http://bizmark.co.kr/?p=320

SET @tables = NULL;
 SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @tables
   FROM information_schema.tables 
   WHERE table_schema = 'DB명'; -- specify DB name here.
 SET @tables = CONCAT('DROP TABLE ', @tables);
 PREPARE stmt FROM @tables;
 EXECUTE stmt;
 DEALLOCATE PREPARE stmt;

*/
$str_repeat_len = 100;


echo "데이터베이스의 테이블 일괄 삭제"."<br/>";
$table_all_drop = "
	SET @tables = NULL;
	SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @tables 
	FROM information_schema.tables 
	WHERE table_schema = 'm_mangazzang2';
	SET @tables = CONCAT('DROP TABLE ', @tables);
	PREPARE stmt FROM @tables;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
";
//sql_query($table_all_drop);
echo $table_all_drop."<br/><br/>";
echo "일괄 삭제시 위 내용 복사해서 실행해주세요.<br/><br/>";
echo str_repeat("/", $str_repeat_len)."<br/><br/>";

// admin
$t_create_arr = array();
array_push($t_create_arr, '01_mzzang_t01_admin.php'); // 마스터
array_push($t_create_arr, '01_mzzang_t02_board.php'); // 게시판
array_push($t_create_arr, '01_mzzang_t03_comics.php'); // 컨텐츠
array_push($t_create_arr, '01_mzzang_t04_config.php'); // 설정
array_push($t_create_arr, '01_mzzang_t05_event.php'); // 이벤트
array_push($t_create_arr, '01_mzzang_t06_epromotion.php'); // 프로모션
array_push($t_create_arr, '01_mzzang_t07_member.php'); // 회원
array_push($t_create_arr, '01_mzzang_t08_member_point.php'); // 회원 포인트
array_push($t_create_arr, '01_mzzang_t09_channel.php'); // 채널사
array_push($t_create_arr, '01_mzzang_t10_sales.php'); // 판매통계
array_push($t_create_arr, '01_mzzang_t11_log.php'); // 로그


/* 위 충전시, 날짜타입, 날짜시작, 끝날짜 넣기 

	event_allbuy_point				화 전체 결제시 추가 보너스설정
	event_coupon					쿠폰

	epromotion						프로모션 리스트
	epromotion_place				프로모션 위치 설정
	epromotion_banner				프로모션 배너 설정
	epromotion_popup				프로모션 팝업 설정

	member_point_expen_content		회원 코믹스 구입 코믹스 화 단행본리스트
	member_point_expen_content_bwt	회원 코믹스 구입 코믹스 화 단행본웹툰판리스트
	member_point_expen_content_wt	회원 코믹스 구입 코믹스 화 웹툰리스트

	stats_content					코믹스 순위 통계

	stats_ui_main					UI_메인화면_통계
	stats_ui_comics					UI_코믹스화면_통계
	stats_ui_comics_content			UI_코믹스-화리스트화면_통계
	stats_ui_board					UI_게시판화면_통계
	stats_ui_event					UI_이벤트화면_통계
	stats_ui_banner					UI_배너화면_통계


	stats_log						로그

	필요함 - 위에 하면서 만들어야 할것 같음

*/


$table_create = array(); /* array(테이블명, 테이블 기본키 ) */

foreach($t_create_arr as $t_key => $t_val){

	include_once $t_val; 
}



foreach($table_create as $t_create_key => $t_create_val){
	/* 테이블 */
	$sql_select = " SELECT ".$t_create_val[1]." FROM ".$t_create_val[0]." LIMIT 0, 1 ; ";
	$sql_delete = " DELETE FROM ".$t_create_val[0]." ; ";
	$sql_auto_increment = "ALTER TABLE ".$t_create_val[0]." AUTO_INCREMENT =1 ; ";
	
	echo "/////////////////////////// ".$t_create_val[0]." /////////////////////////// <br/>";
	echo $sql_select."<br/>";
	if(!sql_query($sql_select)) {
		sql_query(${'t_'.$t_create_val[0]});
		echo ${'t_'.$t_create_val[0]}."<br/>";
	}else{
		sql_query($sql_delete);
		echo $sql_delete."<br/>";
		sql_query($sql_auto_increment);
		echo $sql_auto_increment."<br/>";
	}
	
	echo "<br/><br/>";
	
}
?>