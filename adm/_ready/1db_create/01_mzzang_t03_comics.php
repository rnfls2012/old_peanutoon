<?
include_once '_common.php'; // 공통



/* 기존 uploadfile가 변환된 코믹스정보 */
$t_comics= "
CREATE TABLE IF NOT EXISTS `comics` (
  `cm_no` INT(11) NOT NULL AUTO_INCREMENT,
  `cm_big` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '코믹스분류',
  `cm_small` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '장르',

  `cm_provider` INT(11) NOT NULL DEFAULT '0' COMMENT '제공사번호',
  `cm_provider_sub` INT(11) NOT NULL DEFAULT '0' COMMENT '제공사서브번호',
  `cm_publisher` INT(11) NOT NULL DEFAULT '0' COMMENT '출판사번호',
  `cm_professional` INT(11) NOT NULL DEFAULT '0' COMMENT '작가번호',

  `cm_service` VARCHAR(2) NOT NULL DEFAULT 'y' COMMENT '서비스여부(y:서비스, n:중지)',
  `cm_adult` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성인여부(y:성인, n:청소년)',
  `cm_platform` INT(11) NOT NULL DEFAULT '1' COMMENT '이용환경[2진수](1:웹, 2:Android, 4:iOS, 8:Onestore)',
  `cm_public_cycle` INT(11) NOT NULL DEFAULT '0' COMMENT '연재주기[2진수](0:주기없음, 1:월, 2:화, 4:수, 8:목, 16:금, 32:토, 64:일, 128:보름, 256:월간)',
  `cm_page_way` VARCHAR(2) NOT NULL DEFAULT 's' COMMENT '페이지 방향(r:오른쪽, l:왼쪽, s:스크롤)',
  `cm_end` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '완결여부(y:완결, n:연속)',
  `cm_type` VARCHAR(6) NOT NULL DEFAULT '0' COMMENT '코믹스유형(0:없음, pd:PD추천, hot:hot, best:best)',
  `cm_color` VARCHAR(2) NOT NULL DEFAULT 'y' COMMENT '색상여부(y:칼라, n:흑백)',

  `cm_ck_id` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '키워드 대분류:comics_keyword-연관',
  `cm_ck_id_sub` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '키워드 중분류:comics_keyword-연관',
  `cm_ck_id_thi` VARCHAR(300) NOT NULL DEFAULT '' COMMENT '키워드 소분류:comics_keyword-연관',

  `cm_series` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '코믹스명',
  `cm_somaery` VARCHAR(1500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '줄거리',
  `cm_comment` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '한줄 코멘트',
  `cm_episode_total` INT(11) unsigned NOT NULL DEFAULT '0' COMMENT '에피소드 총수(notice제외)',

  `cm_pay` INT(7) NOT NULL DEFAULT '0' COMMENT '가격',
  `cm_ecn_pay` INT(7) NOT NULL DEFAULT '0' COMMENT 'ecn대여가격',

  `cm_cover` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '표지이미지',
  `cm_cover_sub` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '작은표지이미지',

  `cm_event` INT(11) NOT NULL DEFAULT '0' COMMENT '이벤트번호',
  `cm_free` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '무료 횟수',

  `cm_click` INT(11) unsigned NOT NULL DEFAULT '0' COMMENT '화리스트 클릭수',
  `cm_buy_count` INT(11) unsigned NOT NULL DEFAULT '0' COMMENT '구매 누적 횟수',

  `cm_res_service_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '화 서비스 예약 시간',
  `cm_res_free_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '화 기다리다무료 예약 시간',
  `cm_service_stop_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '서비스 중지 시간',

  `cm_up_kind` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '화권구별(0:화, 1:권)',
  `cm_own_type` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '소장타입(0:무료, 1:소장, 2:대여, 3:에러)',
  `cm_l_time` TINYINT(4) NOT NULL DEFAULT '7' COMMENT '전편 열람기간',
  `cm_s_time` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '단편 열람기간',
  `cm_excel_down_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '엑셀다운일',

  `cm_reg_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '등록일',
  `cm_mod_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '수정일',
  `cm_episode_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '최신 에피소드 등록일',

  PRIMARY KEY (`cm_no`),
  KEY `cm_big` (`cm_big`),
  KEY `cm_small` (`cm_small`),
  KEY `cm_provider` (`cm_provider`),
  KEY `cm_provider_sub` (`cm_provider_sub`),
  KEY `cm_publisher` (`cm_publisher`),
  KEY `cm_professional` (`cm_professional`),

  KEY `cm_service` (`cm_service`),
  KEY `cm_adult` (`cm_adult`),
  KEY `cm_platform` (`cm_platform`),
  KEY `cm_public_cycle` (`cm_public_cycle`),
  KEY `cm_page_way` (`cm_page_way`),
  KEY `cm_end` (`cm_end`),
  KEY `cm_type` (`cm_type`),
  KEY `cm_color` (`cm_color`),

  KEY `cm_ck_id` (`cm_ck_id`),
  KEY `cm_ck_id_sub` (`cm_ck_id_sub`),
  KEY `cm_ck_id_thi` (`cm_ck_id_thi`),

  KEY `cm_pay` (`cm_pay`),
  KEY `cm_ecn_pay` (`cm_ecn_pay`),

  KEY `cm_event` (`cm_event`),
  KEY `cm_free` (`cm_free`),

  KEY `cm_up_kind` (`cm_up_kind`),
  KEY `cm_own_type` (`cm_own_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('comics','cm_no'));


/* 단행본 화정보 */
$t_comics_episode_1 = "
CREATE TABLE IF NOT EXISTS `comics_episode_1` (
  `ce_no` INT(11) NOT NULL AUTO_INCREMENT,
  `ce_chapter` INT(11) NOT NULL DEFAULT '0' COMMENT '에피소드:화번호',
  `ce_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',

  `ce_title` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '에피소드명',
  `ce_notice` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '화 공지사항',
  `ce_pay` INT(7) NOT NULL DEFAULT '0' COMMENT '에피소드 가격',

  `ce_service_state` VARCHAR(2) NOT NULL DEFAULT 'y' COMMENT '서비스상태(y:서비스, n:중지, r:서비스예약)',
  `ce_service_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '서비스일',
  `ce_service_stop_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '서비스 중지일',

  `ce_free_state` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '무료상태(y:무료, n:유료, r:무료예약)',
  `ce_free_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '무료일',
  `ce_login` VARCHAR(2) NOT NULL DEFAULT 'y' COMMENT '로그인후보기(y:로그인후, n:로그인전)',

  `ce_file` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '에피소드 파일 저장 경로',
  `ce_file_count` INT(7) NOT NULL DEFAULT '0' COMMENT '압축안에 파일 갯수',

  `ce_cover` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '에피소드 표지이미지',
  `ce_order` INT(11) NOT NULL DEFAULT '0' COMMENT '화 순서',
  `ce_reg_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '등록일',
  `ce_mod_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '수정일',

  PRIMARY KEY (`ce_no`),
  KEY `ce_chapter` (`ce_chapter`),
  KEY `ce_comics` (`ce_comics`),

  KEY `ce_pay` (`ce_pay`),

  KEY `ce_service_state` (`ce_service_state`),
  KEY `ce_free_state` (`ce_free_state`),
  KEY `ce_login` (`ce_login`),

  KEY `ce_order` (`ce_order`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('comics_episode_1','ce_no'));

/* 코믹스 제공사 */
$t_comics_provider = "
CREATE TABLE IF NOT EXISTS `comics_provider` (
  `cp_no` INT(11) NOT NULL AUTO_INCREMENT,
  `cp_name` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '제공사명',
  `cp_general` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '일반 요율',
  `cp_writer` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '글작가 요율',
  `cp_painter` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '그림작가 요율',
  `cp_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`cp_no`),
  UNIQUE KEY `cp_name` (`cp_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('comics_provider','cp_no'));

/* 코믹스 출판사 */
$t_comics_publisher = "
CREATE TABLE IF NOT EXISTS `comics_publisher` (
  `cp_no` INT(11) NOT NULL AUTO_INCREMENT,
  `cp_name` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '출판사명',
  `cp_ecn_pay` INT(7) NOT NULL DEFAULT '0' COMMENT '출판사 대여 가격',
  `cp_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`cp_no`),
  UNIQUE KEY `cp_name` (`cp_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('comics_publisher','cp_no'));

/* 코믹스 작가 */
$t_comics_professional = "
CREATE TABLE IF NOT EXISTS `comics_professional` (
  `cp_no` INT(11) NOT NULL AUTO_INCREMENT,
  `cp_name` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '작가명',
  `cp_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`cp_no`),
  UNIQUE KEY `cp_name` (`cp_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('comics_professional','cp_no'));

/* 코믹스 키워드 */
$t_comics_keyword = "
CREATE TABLE IF NOT EXISTS `comics_keyword` (
  `ck_no` INT(11) NOT NULL AUTO_INCREMENT,
  `ck_id` VARCHAR(10) NOT NULL DEFAULT '0' COMMENT '키워드고유ID[16진수]',
  `ck_name` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '키워드명',
  `ck_adult` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성인여부(y:성인, n:청소년)',
  `ck_order` INT(7) NOT NULL DEFAULT '100' COMMENT '키워드순서_숫자가 작을 수록 상위',
  `ck_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`ck_no`),
  KEY `ck_adult` (`ck_adult`), 
  KEY `ck_order` (`ck_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
array_push($table_create, array('comics_keyword','ck_no'));

/* 코믹스 메뉴 */
$t_comics_menu = "
CREATE TABLE IF NOT EXISTS `comics_menu` (
  `cn_no` INT(11) NOT NULL AUTO_INCREMENT,
  `cn_id` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '메뉴코드',
  `cn_name` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '메뉴명',
  `cn_link` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '링크',
  `cn_target` VARCHAR(10) NOT NULL DEFAULT '_self' COMMENT '링크속성',
  `cn_order` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '정렬순서',
  `cn_adult` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성인여부(y:성인, n:청소년)',
  `cn_use` VARCHAR(6) NOT NULL DEFAULT 'y' COMMENT '사용여부(y:사용, n:안함, web:웹, mob:모바일, app:앱)',
  `cn_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`cn_no`),
  KEY `cn_id` (`cn_id`), 
  KEY `cn_order` (`cn_order`), 
  KEY `cn_adult` (`cn_adult`), 
  KEY `cn_use` (`cn_use`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('comics_menu','cn_no'));

/* 코믹스 순위 & 코믹스 급상승 리스트 */
$t_comics_ranking = "
CREATE TABLE IF NOT EXISTS `comics_ranking` (
  `cr_no` INT(11) NOT NULL AUTO_INCREMENT,
  `cr_class` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '사용여부(r:실시간, d:일간, w:주간, en:완결, bk:단행본, wt:웹툰, al:전체)', 
  `cr_adult` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성인여부(y:성인, n:청소년)',
  `cr_lock` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '고정여부(y:고정, n:안함)',

  `cr_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호', 
  `cr_big` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '코믹스대분류',
  
  `cr_ranking` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '순위',
  `cr_ranking_gap` INT(11) NOT NULL DEFAULT '0' COMMENT '순위차이',
  `cr_ranking_mark` VARCHAR(4) NOT NULL DEFAULT '' COMMENT '순위표기(ff:급상승, f:상승, m:고정, p:강하 pp:급강하)',

  `cr_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`cr_no`),
  KEY `cr_class` (`cr_class`), 
  KEY `cr_adult` (`cr_adult`), 
  KEY `cr_lock` (`cr_lock`), 

  KEY `cr_comics` (`cr_comics`), 
  KEY `cr_big` (`cr_big`), 

  KEY `cr_ranking` (`cr_ranking`), 
  KEY `cr_ranking_gap` (`cr_ranking_gap`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8; 
";
array_push($table_create, array('comics_ranking','cr_no'));

/* 코믹스 단어추천 */
$t_comics_text = "
CREATE TABLE IF NOT EXISTS `comics_text` (
  `ct_no` INT(11) NOT NULL AUTO_INCREMENT,
  `ct_adult` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성인여부(y:성인, n:청소년)',
  `ct_recommend` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '단어추천',
  `ct_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`ct_no`),
  KEY `ct_adult` (`ct_adult`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('comics_text','ct_no'));

?>