<?
include_once '_common.php'; // 공통


/* 단행본 테이블를 단행본웹툰과 웹툰으로 복사

content테이블 content_bwt테이블와 content_wt테이블로 복사
comics_content
comics_content_bwt
comics_content_wt

*/

$table_copy = array(); /* array(대상 테이블명, 복사된 테이블명) */

/* 회원 -> 휴면회원 */
array_push($table_copy, array('member','member_human'));

/* 단행본 -> 단행본웹툰판, 웹툰 */
array_push($table_copy, array('comics_episode_1','comics_episode_2','comics_episode_3'));
array_push($table_copy, array('member_buy_episode_1','member_buy_episode_2','member_buy_episode_3'));
array_push($table_copy, array('member_point_expen_episode_1','member_point_expen_episode_2','member_point_expen_episode_3'));
array_push($table_copy, array('sales_episode_1','sales_episode_2','sales_episode_3'));
array_push($table_copy, array('sales_episode_1_age_sex','sales_episode_2_age_sex','sales_episode_3_age_sex'));

foreach($table_copy as $t_cp_key => $t_cp_val){
	foreach($t_cp_val as $t_cp_data_key => $t_cp_data_val){
		if($t_cp_data_key == 0){ continue; }
		$sql_t_cp_data ="CREATE TABLE ".$t_cp_data_val." SELECT * FROM ".$t_cp_val[0]." ; ";
		echo $sql_t_cp_data."<br/>";
		sql_query($sql_t_cp_data);

	}
}
/* 필드속성 및 인덱스 설정 */

$member_human_key = array('member_human');
foreach($member_human_key as $t_mh_key => $t_mh_val){
	$sql = "ALTER TABLE `".$t_mh_val."` ADD INDEX `mb_id` (`mb_id`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mh_val."` ADD INDEX `mb_email` (`mb_email`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mh_val."` ADD INDEX `mb_idx` (`mb_idx`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mh_val."` ADD INDEX `mb_level` (`mb_level`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mh_val."` ADD INDEX `mb_class` (`mb_class`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mh_val."` ADD INDEX `mb_state` (`mb_state`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mh_val."` ADD INDEX `mb_sex` (`mb_sex`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mh_val."` ADD INDEX `mb_post` (`mb_post`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mh_val."` ADD INDEX `mb_adult` (`mb_adult`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mh_val."` ADD INDEX `mb_overlap` (`mb_overlap`);";
	sql_query($sql);
}

$comics_episod_key = array('comics_episode_2','comics_episode_3');
foreach($comics_episod_key as $t_ce_key => $t_ce_val){
	$sql = "ALTER TABLE `".$t_ce_val."` ADD PRIMARY KEY `ce_no` (`ce_no`);";
	sql_query($sql);
	$sql = "ALTER TABLE  `".$t_ce_val."` CHANGE  `ce_no`  `ce_no` INT( 11 ) NOT NULL AUTO_INCREMENT ;";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_ce_val."` ADD INDEX `ce_chapter` (`ce_chapter`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_ce_val."` ADD INDEX `ce_comics` (`ce_comics`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_ce_val."` ADD INDEX `ce_pay` (`ce_pay`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_ce_val."` ADD INDEX `ce_service_state` (`ce_service_state`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_ce_val."` ADD INDEX `ce_free_state` (`ce_free_state`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_ce_val."` ADD INDEX `ce_login` (`ce_login`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_ce_val."` ADD INDEX `ce_order` (`ce_order`);";
	sql_query($sql);
}
$member_point_expen_episode_key = array('member_point_expen_episode_2','member_point_expen_episode_3');
foreach($member_point_expen_episode_key as $t_mpee_key => $t_mpee_val){
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD PRIMARY KEY `mpee_no` (`mpee_no`);";
	sql_query($sql);
	$sql = "ALTER TABLE  `".$t_mpee_val."` CHANGE  `mpee_no`  `mpee_no` INT( 11 ) NOT NULL AUTO_INCREMENT ;";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_member` (`mpee_member`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_member_idx` (`mpee_member_idx`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpec_no` (`mpec_no`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_comics` (`mpee_comics`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_big` (`mpee_big`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_small` (`mpee_small`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_episode` (`mpee_episode`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_chapter` (`mpee_chapter`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_cash_type` (`mpee_cash_type`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_state` (`mpee_state`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_sale` (`mpee_sale`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_free` (`mpee_free`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_way` (`mpee_way`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_age` (`mpee_age`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_sex` (`mpee_sex`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_kind` (`mpee_kind`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_year_month` (`mpee_year_month`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_year` (`mpee_year`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_month` (`mpee_month`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_day` (`mpee_day`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_hour` (`mpee_hour`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_mpee_val."` ADD INDEX `mpee_week` (mpee_week`);";
	sql_query($sql);
}
$sales_episode_key = array('sales_episode_2','sales_episode_3');
foreach($sales_episode_key as $t_se_key => $t_se_val){
	$sql = "ALTER TABLE `".$t_se_val."` ADD PRIMARY KEY `se_no` (`se_no`);";
	sql_query($sql);
	$sql = "ALTER TABLE  `".$t_se_val."` CHANGE  `se_no`  `se_no` INT( 11 ) NOT NULL AUTO_INCREMENT ;";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_se_val."` ADD INDEX `se_comics` (`se_comics`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_se_val."` ADD INDEX `se_big` (`se_big`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_se_val."` ADD INDEX `se_episode` (`se_episode`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_se_val."` ADD INDEX `se_chapter` (`se_chapter`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_se_val."` ADD INDEX `se_year_month` (`se_year_month`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_se_val."` ADD INDEX `se_year` (`se_year`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_se_val."` ADD INDEX `se_month` (`se_month`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_se_val."` ADD INDEX `se_day` (`se_day`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_se_val."` ADD INDEX `se_hour` (`se_hour`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_se_val."` ADD INDEX `se_week` (`se_week`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_se_val."` ADD INDEX `se_event` (`se_event`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_se_val."` ADD INDEX `se_cash_type` (`se_cash_type`);";
	sql_query($sql);
}
$sales_episode_age_sex_key = array('sales_episode_2_age_sex','sales_episode_3_age_sex');
foreach($sales_episode_age_sex_key as $t_seas_key => $t_seas_val){
	$sql = "ALTER TABLE `".$t_seas_val."` ADD PRIMARY KEY `seas_no` (`seas_no`);";
	sql_query($sql);
	$sql = "ALTER TABLE  `".$t_seas_val."` CHANGE  `seas_no`  `seas_no` INT( 11 ) NOT NULL AUTO_INCREMENT ;";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_seas_val."` ADD INDEX `seas_comics` (`seas_comics`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_seas_val."` ADD INDEX `seas_big` (`seas_big`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_seas_val."` ADD INDEX `seas_episode` (`seas_episode`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_seas_val."` ADD INDEX `seas_chapter` (`seas_chapter`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_seas_val."` ADD INDEX `seas_year_month` (`seas_year_month`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_seas_val."` ADD INDEX `seas_year` (`seas_year`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_seas_val."` ADD INDEX `seas_month` (`seas_month`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_seas_val."` ADD INDEX `seas_day` (`seas_day`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_seas_val."` ADD INDEX `seas_hour` (`seas_hour`);";
	sql_query($sql);
	$sql = "ALTER TABLE `".$t_seas_val."` ADD INDEX `seas_week` (`seas_week`);";
	sql_query($sql);
}

?>