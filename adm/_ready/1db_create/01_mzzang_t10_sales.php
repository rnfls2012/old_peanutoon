<?
include_once '_common.php'; // 공통


/* 매출 코믹스 통계 */
$t_sales = "
CREATE TABLE IF NOT EXISTS `sales` (
  `sl_no` INT(11) NOT NULL AUTO_INCREMENT,
  `sl_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호', 
  `sl_big` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스대분류',

  `sl_year_month` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
  `sl_year` VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
  `sl_month` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
  `sl_day` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
  `sl_hour` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
  `sl_week` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',

  `sl_open` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '오픈view횟수',
  `sl_free` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '무료view횟수',
  `sl_sale` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '세일view횟수',

  `sl_all_pay` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '전체구매횟수',
  `sl_pay` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '개별구매횟수',

  `sl_event` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '이벤트여부(0:아님, 1:이벤트)',
  `sl_point` INT(11) NOT NULL DEFAULT '0' COMMENT '매출 보너스코인',
  `sl_cash_point` INT(11) NOT NULL DEFAULT '0' COMMENT '매출 코인',

  `sl_cash_type` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '정산금액타입:config_unit_pay-연관',

  PRIMARY KEY (`sl_no`),
  KEY `sl_comics` (`sl_comics`),
  KEY `sl_big` (`sl_big`),

  KEY `sl_year_month` (`sl_year_month`),
  KEY `sl_year` (`sl_year`),
  KEY `sl_month` (`sl_month`),
  KEY `sl_day` (`sl_day`),
  KEY `sl_hour` (`sl_hour`),
  KEY `sl_week` (`sl_week`),

  KEY `sl_event` (`sl_event`),

  KEY `sl_cash_type` (`sl_cash_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('sales','sl_no'));

/* 매출 코믹스 나이, 성별 통계 */
$t_sales_age_sex = "
CREATE TABLE IF NOT EXISTS `sales_age_sex` (
  `sas_no` INT(11) NOT NULL AUTO_INCREMENT,
  `sas_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호', 
  `sas_big` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스대분류',

  `sas_year_month` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
  `sas_year` VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
  `sas_month` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
  `sas_day` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
  `sas_hour` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
  `sas_week` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',

  `sas_man` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자-구매횟수',
  `sas_woman` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자-구매횟수',
  `sas_nocertify` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '미인증-구매횟수',

  `sas_man10` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자10-구매횟수',
  `sas_man20` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자20-구매횟수',
  `sas_man30` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자30-구매횟수',
  `sas_man40` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자40-구매횟수',
  `sas_man50` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자50-구매횟수',
  `sas_man60` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자60-구매횟수',
  `sas_woman10` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자10-구매횟수',
  `sas_woman20` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자20-구매횟수',
  `sas_woman30` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자30-구매횟수',
  `sas_woman40` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자40-구매횟수',
  `sas_woman50` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자50-구매횟수',
  `sas_woman60` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자60-구매횟수',

  PRIMARY KEY (`sas_no`),
  KEY `sas_comics` (`sas_comics`),
  KEY `sas_big` (`sas_big`),

  KEY `sas_year_month` (`sas_year_month`),
  KEY `sas_year` (`sas_year`),
  KEY `sas_month` (`sas_month`),
  KEY `sas_day` (`sas_day`),
  KEY `sas_hour` (`sas_hour`),
  KEY `sas_week` (`sas_week`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('sales_age_sex','sas_no'));


/* 매출 코믹스 화-단행본별 통계 */
$t_sales_episode_1 = "
CREATE TABLE IF NOT EXISTS `sales_episode_1` (
  `se_no` INT(11) NOT NULL AUTO_INCREMENT,
  `se_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호', 
  `se_big` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스대분류',
  `se_episode` INT(11) NOT NULL DEFAULT '0' COMMENT '에피소드',
  `se_chapter` INT(11) NOT NULL DEFAULT '0' COMMENT '에피소드:화번호',

  `se_year_month` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
  `se_year` VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
  `se_month` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
  `se_day` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
  `se_hour` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
  `se_week` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',

  `se_open` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '오픈view횟수',
  `se_free` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '무료view횟수',
  `se_sale` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '세일view횟수',

  `se_all_pay` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '전체구매횟수',
  `se_pay` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '개별구매횟수',

  `se_event` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '이벤트여부(0:아님, 1:이벤트)',
  `se_point` INT(11) NOT NULL DEFAULT '0' COMMENT '매출 보너스코인',
  `se_cash_point` INT(11) NOT NULL DEFAULT '0' COMMENT '매출 코인',

  `se_cash_type` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '정산금액타입:config_unit_pay-연관',

  PRIMARY KEY (`se_no`),
  KEY `se_comics` (`se_comics`),
  KEY `se_big` (`se_big`),
  KEY `se_episode` (`se_episode`),
  KEY `se_chapter` (`se_chapter`),

  KEY `se_year_month` (`se_year_month`),
  KEY `se_year` (`se_year`),
  KEY `se_month` (`se_month`),
  KEY `se_day` (`se_day`),
  KEY `se_hour` (`se_hour`),
  KEY `se_week` (`se_week`),

  KEY `se_event` (`se_event`),

  KEY `se_cash_type` (`se_cash_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('sales_episode_1','se_no'));

/* 매출 코믹스 나이, 성별 통계 */
$t_sales_episode_1_age_sex = "
CREATE TABLE IF NOT EXISTS `sales_episode_1_age_sex` (
  `seas_no` INT(11) NOT NULL AUTO_INCREMENT,
  `seas_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호', 
  `seas_big` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스대분류',
  `seas_episode` INT(11) NOT NULL DEFAULT '0' COMMENT '에피소드',
  `seas_chapter` INT(11) NOT NULL DEFAULT '0' COMMENT '에피소드:화번호',

  `seas_year_month` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도&월',
  `seas_year` VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
  `seas_month` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
  `seas_day` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
  `seas_hour` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
  `seas_week` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',

  `seas_man` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자-구매횟수',
  `seas_woman` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자-구매횟수',
  `seas_nocertify` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '미인증-구매횟수',

  `seas_man10` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자10-구매횟수',
  `seas_man20` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자20-구매횟수',
  `seas_man30` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자30-구매횟수',
  `seas_man40` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자40-구매횟수',
  `seas_man50` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자50-구매횟수',
  `seas_man60` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '남자60-구매횟수',
  `seas_woman10` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자10-구매횟수',
  `seas_woman20` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자20-구매횟수',
  `seas_woman30` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자30-구매횟수',
  `seas_woman40` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자40-구매횟수',
  `seas_woman50` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자50-구매횟수',
  `seas_woman60` INT(7) unsigned NOT NULL DEFAULT '0' COMMENT '여자60-구매횟수',

  PRIMARY KEY (`seas_no`),
  KEY `seas_comics` (`seas_comics`),
  KEY `seas_big` (`seas_big`),
  KEY `seas_episode` (`seas_episode`),
  KEY `seas_chapter` (`seas_chapter`),

  KEY `seas_year_month` (`seas_year_month`),
  KEY `seas_year` (`seas_year`),
  KEY `seas_month` (`seas_month`),
  KEY `seas_day` (`seas_day`),
  KEY `seas_hour` (`seas_hour`),
  KEY `seas_week` (`seas_week`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('sales_episode_1_age_sex','seas_no'));

/* 매출 코믹스 나이, 성별 통계 */
$t_sales_recharge = "
CREATE TABLE IF NOT EXISTS `sales_recharge` (
  `sr_no` INT(11) NOT NULL AUTO_INCREMENT,

  `sr_year_month` VARCHAR(8) NOT NULL DEFAULT '' COMMENT '연도_월',
  `sr_year` VARCHAR(5) NOT NULL DEFAULT '' COMMENT '연도',
  `sr_month` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '월',
  `sr_day` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '일',
  `sr_hour` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '시',
  `sr_min` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '분',
  `sr_week` VARCHAR(3) NOT NULL DEFAULT '' COMMENT '요일(0:일, 1:월, 2:화, 3:수, 4:목, 5:금, 6:토)',

  `sr_product` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '결제상품종류',
  `sr_event` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '이벤트여부(0:아님, 1:이벤트)',
  `sr_way` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '결제수단(빈칸:에러, ph:폰, card:카드, virtual:가상계좌, toss:토스)',
  `sr_platform` INT(11) NOT NULL DEFAULT '1' COMMENT '이용환경[2진수](1:웹, 2:Android, 4:iOS, 8:Onestore)',

  `sr_cash_point` INT(7) NOT NULL DEFAULT '0' COMMENT '충전 코인', 
  `sr_point` INT(7) NOT NULL DEFAULT '0' COMMENT '충전 보너스 코인', 

  `sr_pay` INT(11) NOT NULL DEFAULT '0' COMMENT '결제금액',
  `sr_re_pay` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '재구매여부(0:아님, 1:재구매)',

  `sr_sex` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성별(m:남자, w:여자, n:미인증)',
  `sr_age` INT(5) NOT NULL DEFAULT '0' COMMENT '연령',

  `sr_adult` VARCHAR(2) NOT NULL DEFAULT 'n' COMMENT '성인여부(y:성인, n:청소년)',
  `sr_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`sr_no`),
  KEY `sr_year_month` (`sr_year_month`),
  KEY `sr_year` (`sr_year`),
  KEY `sr_month` (`sr_month`),
  KEY `sr_day` (`sr_day`),
  KEY `sr_hour` (`sr_hour`),
  KEY `sr_min` (`sr_min`),
  KEY `sr_week` (`sr_week`),
  KEY `sr_product` (`sr_product`),
  KEY `sr_event` (`sr_event`),
  KEY `sr_way` (`sr_way`),
  KEY `sr_platform` (`sr_platform`),
  KEY `sr_pay` (`sr_pay`),
  KEY `sr_re_pay` (`sr_re_pay`),
  KEY `sr_sex` (`sr_sex`),
  KEY `sr_age` (`sr_age`),
  KEY `sr_adult` (`sr_adult`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

";
array_push($table_create, array('sales_recharge','sr_no'));

?>