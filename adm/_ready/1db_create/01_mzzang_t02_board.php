<?
include_once '_common.php'; // 공통

/*
SELECT CHARACTER_LENGTH(faq_text) 
FROM  `faq` 
*/
/* 도움말 */
$t_board_help = "
CREATE TABLE IF NOT EXISTS `board_help` (
  `bh_no` INT(11) NOT NULL AUTO_INCREMENT,
  `bh_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `bh_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `bh_category` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '분류(1:회원, 2:결제, 3:이용)',
  `bh_title` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '제목',
  `bh_text` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '내용',
  `bh_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',

  PRIMARY KEY (`bh_no`), 
  KEY `bh_member` (`bh_member`), 
  KEY `bh_member_idx` (`bh_member_idx`), 

  KEY `bh_category` (`bh_category`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('board_help','bh_no'));

/* 문의/제안 */
$t_board_ask = "
CREATE TABLE IF NOT EXISTS `board_ask` (
  `ba_no` INT(11) NOT NULL AUTO_INCREMENT,
  `ba_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `ba_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `ba_title` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '문의제목',
  `ba_request` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '문의내용',
  `ba_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '문의일',

  `ba_mode` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '문의모드(0:답변대기, 1:답변완료, 4:관리자)',
  `ba_state` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '회원 쪽지 확인상태(0:읽음, 1:안봄)',

  `ba_admin_member` INT(11) NOT NULL DEFAULT '0' COMMENT '답변회원번호',
  `ba_admin_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '답변회원ID-index',
  `ba_admin_answer` VARCHAR(1000) NOT NULL DEFAULT '' COMMENT '답변내용',
  `ba_admin_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '답변일',

  PRIMARY KEY (`ba_no`),
  KEY `ba_member` (`ba_member`), 
  KEY `ba_member_idx` (`ba_member_idx`), 

  KEY `ba_mode` (`ba_mode`), 
  KEY `ba_state` (`ba_state`), 

  KEY `ba_admin_member` (`ba_admin_member`), 
  KEY `ba_admin_member_idx` (`ba_admin_member_idx`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('board_ask','ba_no'));

/* 오류제보 */
$t_board_report = "
CREATE TABLE IF NOT EXISTS `board_report` (
  `br_no` INT(11) NOT NULL AUTO_INCREMENT,
  `br_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `br_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `br_comics` INT(11) NOT NULL DEFAULT '0' COMMENT '코믹스번호',

  `br_title` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '제목',
  `br_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '제보일',

  PRIMARY KEY (`br_no`),
  KEY `br_member` (`br_member`), 
  KEY `br_member_idx` (`br_member_idx`), 

  KEY `br_comics` (`br_comics`) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('board_report','br_no'));

/* 알림 */
$t_board_notice= "
CREATE TABLE IF NOT EXISTS `board_notice` (
  `bn_no` INT(11) NOT NULL AUTO_INCREMENT,
  `bn_member` INT(11) NOT NULL DEFAULT '0' COMMENT '회원번호',
  `bn_member_idx` VARCHAR(2) NOT NULL DEFAULT '' COMMENT '회원ID-index',

  `bn_category` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '분류(1:공지, 2:안내, 3:사과, 4:공고)',
  `bn_title` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '알림제목',
  `bn_text` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '알림내용',
  `bn_date` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '알림일',

  PRIMARY KEY (`bn_no`),
  KEY `bn_member` (`bn_member`), 
  KEY `bn_member_idx` (`bn_member_idx`), 

  KEY `bn_category` (`bn_category`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
array_push($table_create, array('board_notice','bn_no'));

?>