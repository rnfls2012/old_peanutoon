<?
include_once '_common.php'; // 공통

/* DB필드추가  */

	/* ---------------------------config DROP------------------------------ */
	if(sql_query(" select cf_terms from `config` limit 0, 1 ")) {
		$sql = "ALTER TABLE `config` DROP `cf_terms`"; /* cf_terms */
		sql_query($sql);//-> 05 에서 처리됨
	}
	if(sql_query(" select cf_pinfo from `config` limit 0, 1 ")) {
		$sql = "ALTER TABLE `config` DROP `cf_pinfo`"; /* cf_pinfo */
		sql_query($sql);//-> 05 에서 처리됨
	}
	if(sql_query(" select cf_ipin from `config` limit 0, 1 ")) {
		$sql = "ALTER TABLE `config` DROP `cf_ipin`"; /* cf_ipin */
		sql_query($sql);//-> 05 에서 처리됨
	}
	if(sql_query(" select cf_mb_leave from `config` limit 0, 1 ")) {
		$sql = "ALTER TABLE `config` DROP `cf_mb_leave`"; /* cf_mb_leave */
		sql_query($sql);//-> 05 에서 처리됨
	}
	
	/* 관리자 계정 리스트 필요 없어서 삭제 */
	if(sql_query(" select cf_admin_sub_list from `config` limit 0, 1 ")) {
		$sql = "ALTER TABLE `config` DROP `cf_admin_sub_list`"; /* cf_admin_sub_list */
		sql_query($sql);//-> 05 에서 처리됨
	}
	
	/* 회원등급 재구성으로 삭제 */
	if(sql_query(" select cf_level_arr from `config` limit 0, 1 ")) {
		$sql = "ALTER TABLE `config` DROP `cf_level_arr`"; /* cf_admin_sub_list */
		sql_query($sql);//-> 05 에서 처리됨
	}

	/* ---------------------------config ADD------------------------------ */
	/* 대분류 */
	if(!sql_query(" select big from `config` limit 0, 1 ")) {
		$sql = "ALTER TABLE `config` ADD `big` varchar(100) NOT NULL COMMENT '대분류리스트'  AFTER cf_copy";
		sql_query($sql);
		$sql_insert = " update `config` set big = '".implode("|",$big_default)."';";
		sql_query($sql_insert);	
	}
	/* 장르 */
	if(!sql_query(" select small from `config` limit 0, 1 ")) {
		$sql = "ALTER TABLE `config` ADD `small` varchar(100) NOT NULL COMMENT '장르리스트'  AFTER big";
		sql_query($sql);
		$sql_insert = " update `config` set small = '".implode("|",$small_default)."';";
		sql_query($sql_insert);		
	}

	/* 회원등급리스트-고객 */
	if(!sql_query(" select mb_client_list from `config` limit 0, 1 ")) {
		$sql = "ALTER TABLE `config` ADD `mb_client_list` varchar(255) NOT NULL COMMENT '회원등급리스트-고객'  AFTER cf_copy";
		sql_query($sql);
		$sql_insert = " update `config` set mb_client_list = '".implode("|",$mb_client_list_default)."';";
		sql_query($sql_insert);		
	}

	/* 회원등급리스트-파트너 */
	if(!sql_query(" select mb_parter_level_list from `config` limit 0, 1 ")) {
		$sql = "ALTER TABLE `config` ADD `mb_parter_level_list` varchar(255) NOT NULL COMMENT '회원등급리스트-파트너'  AFTER cf_copy";
		sql_query($sql);
		$sql_insert = " update `config` set mb_parter_level_list = '".implode("|",$mb_parter_level_list_default)."';";
		sql_query($sql_insert);		
	}

	/* 회원등급리스트-관리자 */
	if(!sql_query(" select mb_admin_level_list from `config` limit 0, 1 ")) {
		$sql = "ALTER TABLE `config` ADD `mb_admin_level_list` varchar(255) NOT NULL COMMENT '회원등급리스트-관리자'  AFTER cf_copy";
		sql_query($sql);
		$sql_insert = " update `config` set mb_admin_level_list = '".implode("|",$mb_admin_level_list_default)."';";
		sql_query($sql_insert);		
	}

?>