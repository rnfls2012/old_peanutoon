<?
include_once '_common.php'; // 공통

/* in_free_date가 빈값만 검사 */
$sql_point_income = "SELECT *, INSTR(in_from, '결재')as infrom_chk FROM  `point_income` WHERE in_free_date = '' AND  in_date2 = '' ORDER BY in_date ASC, in_free_date ASC ";
$result_point_income = sql_query($sql_point_income);
while ($row_point_income = sql_fetch_array($result_point_income)) {
	if($row_point_income['infrom_chk'] > 0){
		$sql_update_point_income = "update `point_income` set in_date = '".$row_point_income['in_date']." 00:00:00',  in_date2 = '".$row_point_income['in_date']." 00:00:00' WHERE in_free_date = '' AND in_from = '".$row_point_income['in_from']."' AND  in_date2 = '' ";
		echo $sql_update_point_income."<br/>";
		sql_query($sql_update_point_income);

	}else{
		$sql_update_point_income = "update `point_income` set in_free_date = '".$row_point_income['in_date']." 00:00:00', in_date2 = '".$row_point_income['in_date']." 00:00:00', in_free='1' WHERE in_free_date = '' AND in_from = '".$row_point_income['in_from']."' AND  in_date2 = '' AND in_free='0' ";
		echo $sql_update_point_income."<br/>";
		sql_query($sql_update_point_income);
		
	}
}
/* in_free_date가 NULL 이고, 결재가 아닌 경우만 검사 -> 없음
SELECT *, INSTR(in_from, '결재')as infrom_chk FROM  `point_income` WHERE in_free_date is NULL AND  in_date2 = '' AND INSTR(in_from, '결재') = 0 ORDER BY in_date ASC, in_free_date ASC
*/


/* in_date 길이가 15 미만이고, 결재 경우만 검사 */
$sql_point_income = "SELECT *, INSTR(in_from, '결재')as infrom_chk FROM  `point_income` WHERE LENGTH(in_date) < 15 AND in_date2 = '' AND INSTR(in_from, '결재') > 0 ORDER BY in_date ASC, in_free_date ASC ";
$result_point_income = sql_query($sql_point_income);
while ($row_point_income = sql_fetch_array($result_point_income)) {
	if($row_point_income['infrom_chk'] > 0){
		$sql_update_point_income = "update `point_income` set in_date2 = '".$row_point_income['in_free_date']."', in_date = '".$row_point_income['in_free_date']."',  in_free_date = NULL WHERE in_no = '".$row_point_income['in_no']."' ";
		echo $sql_update_point_income."<br/>";
		sql_query($sql_update_point_income);
	}
}


/* in_date2 가 없는 데이터 검사 */
$sql_point_income = "SELECT *, INSTR(in_from, '결재')as infrom_chk FROM  `point_income` WHERE in_date2 = '' ORDER BY in_date ASC, in_free_date ASC ";
$result_point_income = sql_query($sql_point_income);
while ($row_point_income = sql_fetch_array($result_point_income)) {
	if($row_point_income['infrom_chk'] > 0){
		$sql_update_point_income = "update `point_income` set in_date2 = '".$row_point_income['in_date']."' WHERE in_from = '".$row_point_income['in_from']."' AND  in_date2 = '' AND in_no = '".$row_point_income['in_no']."' ";
		echo $sql_update_point_income."<br/>";
		sql_query($sql_update_point_income);

	}else{
		$sql_update_point_income = "update `point_income` set in_date2 = '".$row_point_income['in_free_date']."', in_free='1' WHERE in_from = '".$row_point_income['in_from']."' AND  in_date2 = '' AND in_free='0' AND in_no = '".$row_point_income['in_no']."' ";
		echo $sql_update_point_income."<br/>";
		sql_query($sql_update_point_income);		
	}
}

/* ---------------------------point_income DROP------------------------------ */
if(sql_query(" select in_free_date from `point_income` limit 0, 1 ")) {	
	sql_query("UPDATE `point_income` set `in_date` = left(in_date2,10);");/* in_date 업데이트 */

	$sql = "ALTER TABLE `point_income` DROP `in_free_date` "; /* in_date2로 대처 */
	sql_query($sql);//-> 05 에서 처리됨

	
	/* 구매,충전 시간 */
	sql_query("UPDATE `point_expen` set `ex_hour` = SUBSTRING(ex_date2, 12, 2 );");/* in_date 업데이트 */
	sql_query("UPDATE `point_income` set `in_hour` = SUBSTRING(in_date2, 12, 2 );");/* in_date 업데이트 */
	
	$sql = "ALTER TABLE `point_expen` ADD INDEX `ex_date` (`ex_date`);";
	sql_query($sql);	
	$sql = "ALTER TABLE `point_expen` ADD INDEX `ex_hour` (`ex_hour`);";
	sql_query($sql);	

	$sql = "ALTER TABLE `point_income` ADD INDEX `in_date` (`in_date`);";
	sql_query($sql);	
	$sql = "ALTER TABLE `point_income` ADD INDEX `in_hour` (`in_hour`);";
	sql_query($sql);

	/* 구매 갯수 */	
	sql_query("UPDATE `point_expen` set `ex_count` = 1 WHERE `ex_count` = 0 ");

	/* member에서 point_expen테이블에 sex와 age 넣기 */
	$sql = "
			UPDATE `point_expen` pe
			LEFT JOIN member m ON pe.ex_id = m.member_id 
			set pe.ex_age =
			(
			(FLOOR
			(
			(YEAR( NOW( ) ) - LEFT( m.member_birth, 4 ) +0)
			/10)
			)*10
			),
			pe.ex_sex = m.member_sex
			";
	sql_query($sql);
}
?>