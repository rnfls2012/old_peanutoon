<?
include_once '_common.php'; // 공통

/* /////////////////////////////// sales /////////////////////////////// */
sql_query("TRUNCATE sales");// 데이터 있음 비우기

/* 컨텐츠번호&분류 */
$field_point_expen = "ex_filenum, ex_big, "; 
/* 날짜 */
$field_point_expen.= "ex_date, left(ex_date,7)as ex_year_month, left(ex_date,4)as ex_year, SUBSTRING(ex_date, 6, 2 )as ex_month, SUBSTRING(ex_date, 9, 2 )as ex_day, ex_hour, ";
/* 값 */
$field_point_expen.= "SUM(ex_count)as ex_count_sum, SUM(ex_point)as ex_point_sum, SUM(ex_cash)as ex_cash_sum, ";
/* 값-캐쉬타입 */
$field_point_expen.= "cash_type";


$sql_point_expen = "SELECT $field_point_expen FROM  `point_expen` GROUP BY ex_filenum, ex_date, ex_hour ORDER BY ex_no";
// echo $sql_point_expen."<br/>";
$result_point_expen = sql_query($sql_point_expen);

$sales_arr = array('sp_filenum', 'sp_big', 'sp_year_month', 'sp_year', 'sp_month', 'sp_day', 'sp_hour', 'sp_week' );
array_push($sales_arr, 'sp_pay','sp_point','sp_cash_point','sp_cash_type');


while ($row_point_expen = sql_fetch_array($result_point_expen)) {
	/* 값 변수 및 값 수정 */
	
	/* 컨텐츠번호&분류 */
	$sp_filenum_val = $row_point_expen['ex_filenum'];
	$sp_big_val = $row_point_expen['ex_big'];

	/* 날짜 */
	$sp_year_month_val = $row_point_expen['ex_year_month'];
	$sp_year_val = $row_point_expen['ex_year'];
	$sp_month_val = $row_point_expen['ex_month'];
	$sp_day_val = $row_point_expen['ex_day'];
	$sp_hour_val = $row_point_expen['ex_hour'];
	$sp_week_val = date("w",strtotime($row_point_expen['ex_date']));
	

	/* 값 */
	$sp_pay_val = $row_point_expen['ex_count_sum'];
	$sp_point_val = $row_point_expen['ex_point_sum'];
	$sp_cash_point_val = $row_point_expen['ex_cash_sum'];
	$sp_cash_type_val = $row_point_expen['cash_type'];


	/* field명 */
	$sql_sales_insert = 'INSERT INTO sales(';
	foreach($sales_arr as $sales_key => $sales_val){
		$sql_sales_insert.= $sales_val.', ';
	}
	$sql_sales_insert = substr($sql_sales_insert,0,strrpos($sql_sales_insert, ","));
	
	/* VALUES 시작 */
	$sql_sales_insert.= ' )VALUES( ';
	foreach($sales_arr as $sales_key => $sales_val){
		$sql_sales_insert.= '"'.${$sales_val.'_val'}.'", ';
	}
	
	$sql_sales_insert = substr($sql_sales_insert,0,strrpos($sql_sales_insert, ","));

	/* SQL문 마무리 */
	$sql_sales_insert.= ' ) ';
	sql_query($sql_sales_insert);
	echo $sql_sales_insert."<br/>";	
}


/* /////////////////////////////// sales_sex_age /////////////////////////////// */
sql_query("TRUNCATE sales_sex_age");// 데이터 있음 비우기

/* 컨텐츠번호&분류 */
$field_point_expen = "ex_filenum, ex_big, "; 
/* 날짜 */
$field_point_expen.= "ex_date, left(ex_date,7)as ex_year_month, left(ex_date,4)as ex_year, SUBSTRING(ex_date, 6, 2 )as ex_month, SUBSTRING(ex_date, 9, 2 )as ex_day, ex_hour, ";
/* 값-남/여/미인증:성별없거나 나이없는 회원도 포함 */
$field_point_expen.= "	count(if(ex_sex='M' and ex_age>0,1,null))as m_cnt, 
						count(if(ex_sex='Y' and ex_age>0,1,null))as y_cnt, 
						count(if(ex_sex is NULL  OR ex_sex=''  OR  ex_age=0,1,null))as n_cnt, ";
/* 값-남 10, 20, 30, 40, 50, 60 연령 */
$field_point_expen.= "  count(if(ex_sex='M' and ex_age=10 ,1,null))as m10_cnt, 
						count(if(ex_sex='M' and ex_age=20 ,1,null))as m20_cnt, 
						count(if(ex_sex='M' and ex_age=30 ,1,null))as m30_cnt, 
						count(if(ex_sex='M' and ex_age=40 ,1,null))as m40_cnt, 
						count(if(ex_sex='M' and ex_age=50 ,1,null))as m50_cnt, 
						count(if(ex_sex='M' and ex_age=60 ,1,null))as m60_cnt, ";
/* 값-여 10, 20, 30, 40, 50, 60  연령 */
$field_point_expen.= "  count(if(ex_sex='Y' and ex_age=10 ,1,null))as y10_cnt, 
						count(if(ex_sex='Y' and ex_age=20 ,1,null))as y20_cnt, 
						count(if(ex_sex='Y' and ex_age=30 ,1,null))as y30_cnt, 
						count(if(ex_sex='Y' and ex_age=40 ,1,null))as y40_cnt, 
						count(if(ex_sex='Y' and ex_age=50 ,1,null))as y50_cnt, 
						count(if(ex_sex='Y' and ex_age=60 ,1,null))as y60_cnt, ";
/* 값-캐쉬타입 */
$field_point_expen.= "cash_type";


$sql_point_expen = "SELECT $field_point_expen FROM  `point_expen` GROUP BY ex_filenum, ex_date, ex_hour ORDER BY ex_no";
echo $sql_point_expen."<br/>";
$result_point_expen = sql_query($sql_point_expen);

$sales_sex_age_arr = array('sp_filenum', 'sp_big', 'sp_year_month', 'sp_year', 'sp_month', 'sp_day', 'sp_hour', 'sp_week' );
array_push($sales_sex_age_arr, 'sp_man','sp_woman','sp_no_certify');
array_push($sales_sex_age_arr, 'sp_man10','sp_man20','sp_man30','sp_man40','sp_man50','sp_man60');
array_push($sales_sex_age_arr, 'sp_woman10','sp_woman20','sp_woman30','sp_woman40','sp_woman50','sp_woman60');


while ($row_point_expen = sql_fetch_array($result_point_expen)) {
	/* 값 변수 및 값 수정 */
	
	/* 컨텐츠번호&분류 */
	$sp_filenum_val = $row_point_expen['ex_filenum'];
	$sp_big_val = $row_point_expen['ex_big'];

	/* 날짜 */
	$sp_year_month_val = $row_point_expen['ex_year_month'];
	$sp_year_val = $row_point_expen['ex_year'];
	$sp_month_val = $row_point_expen['ex_month'];
	$sp_day_val = $row_point_expen['ex_day'];
	$sp_hour_val = $row_point_expen['ex_hour'];
	$sp_week_val = date("w",strtotime($row_point_expen['ex_date']));
	

	/* 값-남/여/미인증:성별없거나 나이없는 회원도 포함 */
	$sp_man_val = $row_point_expen['m_cnt'];
	$sp_woman_val = $row_point_expen['y_cnt'];
	$sp_no_certify_val = $row_point_expen['n_cnt'];

	/* 값-남 10, 20, 30, 40, 50, 60 연령 */
	$sp_man10_val = $row_point_expen['m10_cnt'];
	$sp_man20_val = $row_point_expen['m20_cnt'];
	$sp_man30_val = $row_point_expen['m30_cnt'];
	$sp_man40_val = $row_point_expen['m40_cnt'];
	$sp_man50_val = $row_point_expen['m50_cnt'];
	$sp_man60_val = $row_point_expen['m60_cnt'];

	/* 값-여 10, 20, 30, 40, 50, 60 연령 */
	$sp_woman10_val = $row_point_expen['y10_cnt'];
	$sp_woman20_val = $row_point_expen['y20_cnt'];
	$sp_woman30_val = $row_point_expen['y30_cnt'];
	$sp_woman40_val = $row_point_expen['y40_cnt'];
	$sp_woman50_val = $row_point_expen['y50_cnt'];
	$sp_woman60_val = $row_point_expen['y60_cnt'];


	/* field명 */
	$sql_sales_sex_age_insert = 'INSERT INTO sales_sex_age(';
	foreach($sales_sex_age_arr as $sales_sex_age_key => $sales_sex_age_val){
		$sql_sales_sex_age_insert.= $sales_sex_age_val.', ';
	}
	$sql_sales_sex_age_insert = substr($sql_sales_sex_age_insert,0,strrpos($sql_sales_sex_age_insert, ","));
	
	/* VALUES 시작 */
	$sql_sales_sex_age_insert.= ' )VALUES( ';
	foreach($sales_sex_age_arr as $sales_sex_age_key => $sales_sex_age_val){
		$sql_sales_sex_age_insert.= '"'.${$sales_sex_age_val.'_val'}.'", ';
	}
	
	$sql_sales_sex_age_insert = substr($sql_sales_sex_age_insert,0,strrpos($sql_sales_sex_age_insert, ","));

	/* SQL문 마무리 */
	$sql_sales_sex_age_insert.= ' ) ';
	sql_query($sql_sales_sex_age_insert);
	echo $sql_sales_sex_age_insert."<br/>";	
}


?>