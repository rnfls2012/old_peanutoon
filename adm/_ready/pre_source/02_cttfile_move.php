<?
include_once '_common.php'; // 공통

/* 커버이미지 이동 및 커퍼DB 이미지 경로 수정 */
$cttfile_search_sql = "SELECT 
				f.filenum, f.big, f.small, 
				f.image, f.image_small, 
				c.content_no, c.content_file, c.content_num
				FROM `fileupload` f 
				INNER JOIN `content` c
				ON f.filenum = c.content_title
				WHERE  f.big = '1'

				order by f.filenum, c.content_num ";

$cttfile_search_result = sql_query($cttfile_search_sql);

/*
폴더 체크
폴더 없다면 폴더 생성
파일 이동 후 DB update
*/
while ($cttfile_search_row = sql_fetch_array($cttfile_search_result)) {
	// echo $cttfile_search_row['filenum']."<br/>";
	$content_file_mod = str_replace("./", "/", $cttfile_search_row['content_file']);
	$content_file_path = NM_PATH.$content_file_mod;
	echo $cttfile_search_row['filenum'].":".$cttfile_search_row['content_num']." - ".$content_file_path."<br/>";

	// 폴더 생성
	$content_dir_create_big_path = NM_PATH."/data/".$cttfile_search_row['big'];
	if(!is_dir($content_dir_create_big_path)){
		exec('mkdir '.$content_dir_create_big_path);
	}
	$content_dir_create_filenum_path = $content_dir_create_big_path."/".$cttfile_search_row['filenum'];
	if(!is_dir($content_dir_create_filenum_path)){
		exec('mkdir '.$content_dir_create_filenum_path);
	}

	//파일 이동
	exec('mv '.$content_file_path.' '.$content_dir_create_filenum_path);
	echo 'mv '.$content_file_path.' '.$content_dir_create_filenum_path."<br/>";

}
// 폴더 삭제
for($i=1; $i<=7; $i++){
	exec('rm -rf '.NM_PATH.'/data/1/'.$i);
}
exec('rm -rf '.NM_PATH.'/data/0');
exec('rm -rf '.NM_PATH.'/data/2');
exec('rm -rf '.NM_PATH.'/data/3');
exec('rm -rf '.NM_PATH.'/data/6');
exec('rm -rf '.NM_PATH.'/data/wt_data');

// DB 값 일괄 수정
for($i=1; $i<=7; $i++){
	$cttfile_field_sql = "UPDATE content SET content_file = REPLACE(content_file, './data/0/".$i."/', './data/1/')";
	sql_query($cttfile_field_sql);
}
for($i=1; $i<=7; $i++){
	$cttfile_field_sql = "UPDATE content SET content_file = REPLACE(content_file, './data/1/".$i."/', './data/1/')";
	sql_query($cttfile_field_sql);
}
for($i=1; $i<=7; $i++){
	$cttfile_field_sql = "UPDATE content SET content_file = REPLACE(content_file, './data/2/".$i."/', './data/1/')";
	sql_query($cttfile_field_sql);
}
for($i=1; $i<=7; $i++){
	$cttfile_field_sql = "UPDATE content SET content_file = REPLACE(content_file, './data/3/".$i."/', './data/1/')";
	sql_query($cttfile_field_sql);
}
for($i=1; $i<=7; $i++){
	$cttfile_field_sql = "UPDATE content SET content_file = REPLACE(content_file, './data/6/".$i."/', './data/1/')";
	sql_query($cttfile_field_sql);
}
?>