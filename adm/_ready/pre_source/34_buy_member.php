<?
include_once '_common.php'; // 공통


/* buy_member 에 content_no 추가 */

if(!sql_query(" select buy_content_no from `buy_member` limit 0, 1 ")) {
	$sql = "ALTER TABLE `buy_member` ADD `buy_content_no` int(11) NOT NULL COMMENT '컨텐츠 DB 번호'  AFTER `buy_id_idx` ";
	sql_query($sql);
	$sql = "update `buy_member` set `buy_content_no` = buy_content_num "; /* 기존 코인 데이터 필드 복사 */
	sql_query($sql);
	$sql = "ALTER TABLE `buy_member` ADD INDEX `buy_content_num` (`buy_content_num`);";
	sql_query($sql);
}


/* buy_member 에 content_no, content_num 넣기 */
if(!sql_query(" select buy_save_date from `buy_member` limit 0, 1 ")) {
	/* ---------------------------buy_member DROP------------------------------ */
	$sql = "ALTER TABLE `buy_member` DROP `buy_sex` "; /* 통계에 넣을거라 필요없음 */
	sql_query($sql);//-> 05 에서 처리됨
	$sql = "ALTER TABLE `buy_member` ADD `buy_save_date` varchar(20) NOT NULL COMMENT '구매 저장 날짜'  AFTER `buy_end_date` ";
	sql_query($sql);
	$sql = "ALTER TABLE `buy_member` ADD `buy_mod_date` varchar(20) NOT NULL COMMENT '구매 수정 날짜'  AFTER `buy_save_date` ";
	sql_query($sql);

	/* filenum 속성이동 */
	$sql = "ALTER TABLE  `buy_member` CHANGE  `buy_filenum`  `buy_filenum` INT( 11 ) NOT NULL DEFAULT  '0' COMMENT  '구매 컨텐츠번호' AFTER  `buy_content_num` ; ";
	sql_query($sql);

	/* content 속성이동 */
	$sql = "ALTER TABLE  `content` CHANGE  `content_num`  `content_num` INT( 11 ) NOT NULL COMMENT  '컨텐츠 신 번호' AFTER  `content_no` ;";
	sql_query($sql);

	/* content 속성명 변경 */
	$sql = "ALTER TABLE  `content` CHANGE  `content_title`  `content_filenum` INT( 11 ) NOT NULL COMMENT  '컨텐츠번호';";
	sql_query($sql);

	/* 구매 저장 날짜 */
	$sql = "update `buy_member` set `buy_save_date` = CONCAT(buy_date ,' 00:00:00') "; /* 기존 코인 데이터 필드 복사 */
	sql_query($sql);
}

/* countent와 buy_member 신번호 맞추기 */
$sql_countent = "SELECT max(content_num) as cn_max FROM  `content` ";
$row_countent = sql_fetch($sql_countent);

$sql_buy_member = "SELECT max(buy_content_num) as bcn_max FROM  `buy_member`  ";
$row_buy_member = sql_fetch($sql_buy_member);

if($row_countent['cn_max'] < $row_buy_member['bcn_max'] && 1000 < $row_buy_member['bcn_max'] && $row_buy_member['buy_mod_date'] == ''){
	/* 컨텐츠 번호와 매칭되어서 넣기
	b.buy_content_no = c.content_no 매칭되고 c.content_no 값 있는것만 처리  
	
	SELECT b.buy_no, b.buy_id, b.buy_date, b.buy_since, b.buy_filenum, b.buy_content_no, b.buy_content_num, 
	c.content_filenum, c.content_no, c.content_num
	FROM  `buy_member` b
	LEFT JOIN content c ON b.buy_content_no = c.content_no
	WHERE c.content_no !=  ''
	*/
	
	/* 컨텐츠 번호와 매칭되어서 넣기 */
	$sql_buy_member_update = "
			UPDATE `buy_member` b 
			LEFT JOIN content c ON b.buy_content_no = c.content_no 
			set b.buy_content_num = c.content_num, b.buy_filenum = c.content_filenum , b.buy_mod_date = '".NM_TIME_YMDHIS."'
			WHERE c.content_no !=  '' ";
	echo $sql_buy_member_update;
	sql_query($sql_buy_member_update);
	
	/* 위에서 처리 안된 데이터 삭제
	SELECT  distinct  b.buy_content_no, b.buy_content_num
	FROM  `buy_member` b
	WHERE b.buy_mod_date = ''
	*/
	$sql_buy_member_delete = "DELETE FROM `buy_member` WHERE buy_mod_date = '' ";
	echo $sql_buy_member_delete;
	sql_query($sql_buy_member_delete);


	

}


/* 
SELECT b.buy_no, b.buy_id, b.buy_date, b.buy_since, b.buy_filenum, b.buy_content_no, b.buy_content_num, 
c.content_title, c.content_no, c.content_num
  
FROM  `buy_member` b 
left JOIN content c 
ON b.buy_content_no = c.content_no
WHERE c.content_no !=''


SELECT b.buy_no, b.buy_id, b.buy_date, b.buy_since, b.buy_filenum, 
c.content_no, c.content_title, c.content_num
  
FROM  `buy_member` b 
left JOIN content c 
ON b.buy_content_num = c.content_no

WHERE content_no is null
AND buy_end_date = '3000-01-01'


////////////////////////////////////////

SELECT b.buy_date, b.buy_since, 
c.content_title, c.content_num
  
FROM  `buy_member` b 
left JOIN content c 
ON b.buy_content_num = c.content_no 

left JOIN point_expen pe 
ON b.buy_id = pe.ex_id
*/


?>