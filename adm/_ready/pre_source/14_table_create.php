<?
include_once '_common.php'; // 공통
/* 유일한 키 만드는 테이블
    사용하는 곳 :
    1. 글쓰기시 미리 유일키를 얻어 파일 업로드 필드에 넣는다.
    2. 주문번호 생성시에 사용한다.
    3. 기타 유일키가 필요한 곳에서 사용한다.
	4. 어느기간 지날시 자동삭제 기능을 넣어준다.
*/
	$table_ct = "
	CREATE TABLE IF NOT EXISTS `config_textarea` (
  `cf_no` int(11) NOT NULL AUTO_INCREMENT,
  `cf_terms` text NOT NULL COMMENT '이용약관',
  `cf_pinfo` text NOT NULL COMMENT '개인정보취급방침',
  `cf_ipin` varchar(300) NOT NULL COMMENT '본인인증',
  `cf_mb_leave` varchar(300) NOT NULL COMMENT '회원탈퇴시', 
  PRIMARY KEY (`cf_no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
	";
	if(!sql_query(" select cf_no from `config_textarea` limit 0, 1 ")) {
		/* 혹시 안되면 mysql_query으로 수정해서 해보기 */
		sql_query($table_ct);

		$cf_terms = addslashes($nm_config['cf_terms']);
		$cf_pinfo = addslashes($nm_config['cf_pinfo']);
		$cf_ipin = addslashes($nm_config['cf_ipin']);
		$cf_mb_leave = addslashes($nm_config['cf_mb_leave']);

		$sql_insert = " INSERT INTO `config_textarea`(cf_terms, cf_pinfo, cf_ipin, cf_mb_leave 
						)VALUES('{$cf_terms}', '{$cf_pinfo}', '{$cf_ipin}', '{$cf_mb_leave}' );";
		sql_query($sql_insert);
	}else{
		sql_query("DELETE FROM `config_textarea` ");
		sql_query("ALTER TABLE  `config_textarea` AUTO_INCREMENT =1");
	}
?>