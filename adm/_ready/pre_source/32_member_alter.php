<?
include_once '_common.php'; // 공통

/* 회원 중지일 추가 */
if(!sql_query(" select member_stop_date from `member` limit 0, 1 ")) {
	$sql = "ALTER TABLE `member` ADD `member_stop_date` varchar(30) DEFAULT NULL COMMENT '회원 중지일'  AFTER `member_out_date` ";
	sql_query($sql);

	/* 휴면 회원에게도 동일하게 적용 */
	$sql = "ALTER TABLE `member_human` ADD `member_stop_date` varchar(30) DEFAULT NULL COMMENT '회원 중지일'  AFTER `member_out_date` ";
	sql_query($sql);
}

/* 회원 상태 추가 */
if(!sql_query(" select member_state from `member` limit 0, 1 ")) {
	$sql = "ALTER TABLE `member` ADD `member_state` varchar(2) DEFAULT 'y' NULL COMMENT '회원상태_y_정상_s_정지_n_탈퇴'  AFTER `member_phone3` ";
	sql_query($sql);
	$sql = "ALTER TABLE `member` ADD INDEX `member_state` (`member_state`);";
	sql_query($sql);
	$sql = "update `member` set `member_state` = 'n' where member_out_date != '' ";
	sql_query($sql);

	/* 휴면 회원에게도 동일하게 적용 */
	$sql = "ALTER TABLE `member_human` ADD `member_state` varchar(2) DEFAULT 'y' NULL COMMENT '회원상태_y_정상_s_정지_n_탈퇴_h_휴면'  AFTER `member_phone3` ";
	sql_query($sql);
	$sql = "ALTER TABLE `member_human` ADD INDEX `member_state` (`member_state`);";
	sql_query($sql);
	$sql = "update `member_human` set `member_state` = 'h' ";
	sql_query($sql);
	$sql = "update `member_human` set `member_state` = 'n' where member_out_date != '' ";
	sql_query($sql);
}

/* 회원 중복키 추가 */
if(!sql_query(" select member_overlap_key from `member` limit 0, 1 ")) {
	$sql = "ALTER TABLE `member` ADD `member_overlap_key` int(11) NOT NULL COMMENT '회원 중복 체크 및 원본 넘버_0_중복아님'  AFTER `member_info` ";
	sql_query($sql);
	$sql = "ALTER TABLE `member` ADD INDEX `member_overlap_key` (`member_overlap_key`);";
	sql_query($sql);

	/* 휴면 회원에게도 동일하게 적용 */
	$sql = "ALTER TABLE `member_human` ADD `member_overlap_key` int(11) NOT NULL COMMENT '회원 중복 체크 및 원본 넘버_0_중복아님'  AFTER `member_info` ";
	sql_query($sql);
	$sql = "ALTER TABLE `member_human` ADD INDEX `member_overlap_key` (`member_overlap_key`);";
	sql_query($sql);
}
?>