<?
include_once '_common.php'; // 공통
/* 출석이벤트-월 month */
$table_ct = "
CREATE TABLE IF NOT EXISTS `event_att_month` (
  `eam_id` int(3) NOT NULL COMMENT '월',
  `eam_use` varchar(2) NOT NULL DEFAULT '' COMMENT '사용여부_y사용',
  `eam_end_day` int(2) NOT NULL DEFAULT '0' COMMENT '기간_몇일까지로 사용',
  `eam_title` varchar(50) NOT NULL DEFAULT '' COMMENT '이벤트명',
  PRIMARY KEY (`eam_id`),
  KEY `eam_use` (`eam_use`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
if(!sql_query(" select eam_id from `event_att_month` limit 0, 1 ")) {
	/* 혹시 안되면 mysql_query으로 수정해서 해보기 */
	sql_query($table_ct);
}else{
	sql_query('DELETE FROM `event_att_month` ');
}
for($m=1; $m<=12; $m++){
	sql_query("INSERT INTO `event_att_month` (`eam_id`, `eam_use`, `eam_end_day`, `eam_title`) VALUES ('$m', '', '0', ''); ");
}

/* 출석이벤트-일 day */
$table_ct = "
CREATE TABLE IF NOT EXISTS `event_att_day` (
  `ead_id` int(3) NOT NULL COMMENT '일',
  `ead_cash_point` int(6) NOT NULL DEFAULT '0' COMMENT '지급 코인',
  `ead_point` int(6) NOT NULL DEFAULT '0' COMMENT '지급 보너스 코인',
  PRIMARY KEY (`ead_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
if(!sql_query(" select ead_id from `event_att_month` limit 0, 1 ")) {
	/* 혹시 안되면 mysql_query으로 수정해서 해보기 */
	sql_query($table_ct);
}else{
	sql_query('DELETE FROM `event_att_month` ');
}
for($d=1; $d<=31; $d++){
	sql_query("INSERT INTO `event_att_day` (`ead_id`, `ead_cash_point`, `ead_point`) VALUES ('$d', '0', '0'); ");
}
?>