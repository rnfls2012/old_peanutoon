<?
include_once '_common.php'; // 공통

/* 작업 순서 */
$ready = array();

/* 기존 테이블로 테이블 생성시킨 작업입니다.
array_push($ready, array('db_structure','01_table_rename.php','테이블 이름수정',1));
array_push($ready, array('db_structure','02_table_drop.php','테이블 삭제',1));
array_push($ready, array('db_structure','03_table_create.php','테이블 생성',1));
array_push($ready, array('db_structure','04_table_alter_drop.php','테이블 속성삭제',1));
array_push($ready, array('db_structure','05_table_alter_add.php','테이블 속성추가',1));
array_push($ready, array('db_structure','07_table_alter_add_idx.php','테이블 속성수정',1));
*/

array_push($ready, array('1db_create','01_mzzang_create.php','테이블 재생성',1));
array_push($ready, array('1db_create','02_mzzang_content_copy.php','테이블 복사',1));
// array_push($ready, array('db_structure','03_table_create.php','테이블 생성',1));
/* ///////////////////////// 체크라인 ///////////////////////// */


array_push($ready, array('db_structure','08_table_copy.php','테이블 속성복사'));
array_push($ready, array('db_structure','09_table_delete_data.php','전체 데이터 삭제'));
array_push($ready, array('db_structure','10_table_insert_data.php','테이블중 무조건 데이터'));

array_push($ready, array('01_data_ya_delete.php','야톡삭제'));
array_push($ready, array('02_cttfile_move.php','컨텐츠파일이동'));
array_push($ready, array('03_cover_move.php','커버이동'));
array_push($ready, array('04_table_create.php','추가테이블생성'));
array_push($ready, array('05_table_alter.php','DB필드추가'));
array_push($ready, array('06_fileupload_fromto.php','컨텐츠데이터 제공사부분수정'));
array_push($ready, array('07_table_alter.php','DB필드추가(제공사이름삭제)'));
array_push($ready, array('08_content_total.php','fileupload에 신 토탈, 색, 방향, 이용환경, 연재주기, 컨텐츠유형 넣기'));
array_push($ready, array('09_table_alter.php','content의 content_service 필드 삭제'));
array_push($ready, array('10_content.php','content 필드 데이터정리(content_pay, content_login, service_state, free_state, order_num )'));
array_push($ready, array('11_data_del.php','data폴더에 필요없는 파일 삭제'));
array_push($ready, array('12_table_create.php','웹툰, 단행본웹툰 화DB생성'));
array_push($ready, array('13_table_alter.php','제공사 필드 변경 및 추가(일반요율, 글작가 요율, 그림작가 요율)'));
array_push($ready, array('14_table_create.php','config_textarea 테이블 생성(긴 글내용 저장공간) 및 데이터 넣기'));
array_push($ready, array('15_table_alter.php','config설정 변경'));
array_push($ready, array('16_table_create.php','키워드 테이블 생성'));
array_push($ready, array('17_fileupload_content.php','컨텐츠 키워드 초기화, 컨텐츠정보와 화정보 일치 , 화 파일 갯수 업데이트'));
array_push($ready, array('18_table_alter.php','회원정보에 출석일, 출석날짜 적용'));
array_push($ready, array('19_table_create.php','출석이벤트 월/일테이블 생성 및 기본 데이터 넣기'));
array_push($ready, array('20_table_create.php','충전/지급이벤트 테이블 생성'));
array_push($ready, array('21_table_create_alter.php','event 테이블 속성 수정 / 무료&코인할인 테이블 생성'));
array_push($ready, array('22_member_level_mod.php','회원레벨수정'));
array_push($ready, array('23_event_alist_insert.php','현재 이벤트 정보 넣기'));
array_push($ready, array('24_table_alter.php','cash테이블 속성 추가 및 member정보로 통해 재구매값넣기'));
array_push($ready, array('25_table_create.php','충전통계DB생성 및 cash&member정보 넣기'));

array_push($ready, array('26_table_alter.php','캐쉬포인트, 포인트, 캐쉬원 수정 및 필요없는 테이블 삭제'));
array_push($ready, array('27_table_create.php','unit_pay 단위 테이블 추가 및 기존 데이터 수정(// 이건 돌리기 전에 체크 해야함)'));
array_push($ready, array('28_table_create.php','sales, sales_month db이름수정, 코인통계DB생성, 충전DB추가'));
array_push($ready, array('29_point_expen.php','point_expen 데이터 정리'));
array_push($ready, array('30_point_income.php','point_income 데이터 정리'));

array_push($ready, array('31_sales.php','sales 데이터 분산'));
array_push($ready, array('32_member_alter.php','중지회원 / 중복회원 관련 속성 추가'));
array_push($ready, array('33_member_overlap_key.php','중복회원 데이터 넣기'));
array_push($ready, array('34_buy_member.php','buy_member content 데이터 정리'));
array_push($ready, array('35_sales_content.php','sales_content 데이터 분산'));
array_push($ready, array('36_buy_member_own.php','buy_member ->buy_own에 복사 및 데이터 분산'));


$pwd_dir = '_ready';
?>
<ul>
	<? foreach($ready as $ready_key => $ready_val){ 
		$no = $ready_key+1;
		if($ready_val[3] != 1){continue;}
	?>
		<li><a href='<?=NM_ADM_URL?>/<?=$pwd_dir?>/<?=$ready_val[0];?>/<?=$ready_val[1];?>' target='_blank'><?=$no?>.<?=$ready_val[2];?></a></li>
	<?}?>
</ul>