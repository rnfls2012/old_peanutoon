<?
include_once '_common.php'; // 공통

/* ---------------------------cash ADD------------------------------ */
if(!sql_query(" select cash_won from `cash` limit 0, 1 ")) {
	$sql = "ALTER TABLE `cash` ADD `cash_won` int(11) NOT NULL COMMENT '결제금액'  AFTER `cash_mode` ";
	sql_query($sql);
	$sql = "update `cash` set `cash_won` = `cash_point` "; /* 기존 코인 데이터 필드 복사 */
	sql_query($sql);

	/* ---------------------------ags_do_result CHANGE & MODIFY------------------------------ */
	$sql = "ALTER TABLE  `cash` CHANGE  `cash_point`  `cash_point` INT( 11 ) NOT NULL COMMENT  '충전한 코인' ";
	sql_query($sql);
	$sql = "ALTER TABLE  `cash` CHANGE  `cash_point_add`  `point` INT( 11 ) NOT NULL COMMENT  '충전한 보너스코인' AFTER  `cash_point`  ";
	sql_query($sql);
}

/* 충전통계 */
$table_ct = "
CREATE TABLE IF NOT EXISTS `sales_recharge` (
  `sr_no` int(11) NOT NULL AUTO_INCREMENT,
  `sr_id` varchar(30) NOT NULL COMMENT '회원ID',
  `sr_id_idx` varchar(2) NOT NULL COMMENT '회원ID 인덱스',
  `sr_date` varchar(20) NOT NULL COMMENT '원본 날짜',
  `sr_year_month` varchar(8) NOT NULL COMMENT '연도_월',
  `sr_year` varchar(5) NOT NULL COMMENT '연도',
  `sr_month` varchar(3) NOT NULL COMMENT '월',
  `sr_day` varchar(3) NOT NULL COMMENT '일',
  `sr_hour` varchar(3) NOT NULL COMMENT '시',
  `sr_min` varchar(3) NOT NULL COMMENT '분',
  `sr_week` varchar(3) NOT NULL COMMENT '요일(0_일_1_월_2_화_3_수_4_목_5_금_6_토)',

  `sr_product_class` varchar(10) NOT NULL COMMENT '결제상품종류',
  `sr_event` int(2) NOT NULL DEFAULT '0' COMMENT '이벤트여부(0_아님_1_이벤트)',
  `sr_way` varchar(10) NOT NULL COMMENT '결제수단',
  `sr_platform` varchar(10) NOT NULL COMMENT '결제환경',
  `sr_point` int(11) NOT NULL COMMENT '충전 보너스코인',
  `sr_cash_point` int(11) NOT NULL COMMENT '충전 코인',
  `sr_pay` int(11) NOT NULL COMMENT '결제금액',
  `sr_re_pay` int(2) NOT NULL DEFAULT '0' COMMENT '재구매여부(0_아님_1_재구매)',
  `sr_sex` varchar(3) NOT NULL COMMENT '성별(M_남자_Y_여자_빈칸_미인증)',
  `sr_age` int(5) NOT NULL DEFAULT '0' COMMENT '나이',
  `sr_adult` varchar(3) NOT NULL COMMENT '성인여부(n_청소년_y_성인_빈칸_미인증)',
  `sr_save_date` datetime DEFAULT NULL COMMENT '저장일',

  PRIMARY KEY (`sr_no`),
  KEY `sr_id_idx` (`sr_id_idx`),
  KEY `sr_year_month` (`sr_year_month`),
  KEY `sr_year` (`sr_year`),
  KEY `sr_month` (`sr_month`),
  KEY `sr_day` (`sr_day`),
  KEY `sr_hour` (`sr_hour`),
  KEY `sr_min` (`sr_min`),
  KEY `sr_week` (`sr_week`),

  KEY `sr_product_class` (`sr_product_class`),
  KEY `sr_event` (`sr_event`),
  KEY `sr_way` (`sr_way`),
  KEY `sr_platform` (`sr_platform`),
  KEY `sr_pay` (`sr_pay`),
  KEY `sr_re_pay` (`sr_re_pay`),
  KEY `sr_sex` (`sr_sex`),
  KEY `sr_age` (`sr_age`),
  KEY `sr_adult` (`sr_adult`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";//-> 04 에서 처리됨
if(!sql_query(" select sr_no from `sales_recharge` limit 0, 1 ")) {
	/* 혹시 안되면 mysql_query으로 수정해서 해보기 */
	sql_query($table_ct);
}else{
	sql_query('DELETE FROM `sales_recharge` ');
	sql_query('ALTER TABLE  `sales_recharge` AUTO_INCREMENT =1');
}


/* 데이터 넣기 - cash, member 데이터 */
$sql_cash_mb = "SELECT *, (year(now())-left(member_birth,4))as age FROM  `cash` c  left JOIN member m ON c.cash_id = m.member_id order by c.cash_id, c.cash_date, c.cash_date2, c.cash_date3";
$result_cash_mb = sql_query($sql_cash_mb);
while ($row_cash_mb = sql_fetch_array($result_cash_mb)) {
	$sr_sql = "INSERT INTO sales_recharge( sr_id, sr_id_idx, sr_date, sr_year_month, sr_year, sr_month, sr_day, sr_hour, sr_min, sr_week, sr_product_class, sr_event, sr_way, sr_platform, sr_point, sr_cash_point, sr_pay, sr_re_pay, sr_sex, sr_age, sr_adult, sr_save_date";
	$sr_sql.= " )VALUES( ";
	
	/* 데이터 가공 */
	$cash_date2 = $row_cash_mb['cash_date2'];
	if($row_cash_mb['cash_date2'] < 10){ $cash_date2 = "0".$row_cash_mb['cash_date2']; }
	$cash_date3 = $row_cash_mb['cash_date3'];
	if($row_cash_mb['cash_date3'] < 10){ $cash_date3 = "0".$row_cash_mb['cash_date3']; }
	
	/* 회원ID */	
	$sr_id = $row_cash_mb['cash_id'];
	$sr_id_idx = $row_cash_mb['cash_id_idx'];

	/* 시간 */
	$sr_date = $row_cash_mb['cash_date']." ".$cash_date2.":".$cash_date3.":00";
	$sr_year_month = substr($row_cash_mb['cash_date'],0,7);
	$sr_year = substr($row_cash_mb['cash_date'],0,4);
	$sr_month = substr($row_cash_mb['cash_date'],5,2);
	$sr_day = substr($row_cash_mb['cash_date'],8,2);
	$sr_hour = $cash_date2;
	$sr_min = $cash_date3;
	$sr_week = date('w', strtotime($row_cash_mb['cash_date']));

	/* 정보 */
	$sr_product_class = $row_cash_mb['cash_point']."Cash";
	$sr_event = "0";
	$sr_way = $row_cash_mb['cash_mode'];

	$sr_platform = "web";
	if(preg_match('/'.NM_MOBILE_AGENT.'/i', $row_cash_mb['cash_useragent']) || $row_cash_mb['cash_useragent'] == ''){ $sr_platform = "mob"; }
	
	$sr_point = $row_cash_mb['point'];
	$sr_cash_point = $row_cash_mb['cash_point'];
	$sr_pay = $row_cash_mb['cash_point'];

	$sr_re_pay = $row_cash_mb['cash_re'];
	$sr_sex = $row_cash_mb['member_sex'];
	$sr_age = substr($row_cash_mb['age'],0,1)."0";
	$sr_adult = 'n';
	if($row_cash_mb['member_adult'] == 1){ $sr_adult = 'y'; }


	$sr_sql.= "'".$sr_id."', "."'".$sr_id_idx."', ";
	$sr_sql.= "'".$sr_date."', "."'".$sr_year_month."', "."'".$sr_year."', "."'".$sr_month."', "."'".$sr_day."', "."'".$sr_hour."', "."'".$sr_min."', "."'".$sr_week."', ";
	$sr_sql.= "'".$sr_product_class."', "."'".$sr_event."', "."'".$sr_way."', "."'".$sr_platform."', "."'".$sr_point."', "."'".$sr_cash_point."', "."'".$sr_pay."', "."'".$sr_re_pay."', "."'".$sr_sex."', "."'".$sr_age."', "."'".$sr_adult."', ";
	$sr_sql.= "'".NM_TIME_YMDHIS."' ";
	$sr_sql.= " ) ";
	sql_query($sr_sql);
	echo $sr_sql."<br/>";
}
?>