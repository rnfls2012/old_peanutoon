<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동

sleep(10);

$php_self_file = pathinfo($_SERVER['PHP_SELF']);
$php_self_file_name  = strtolower($php_self_file['filename']);

$limit_start = $_GET['limit_start'];

if($limit_start == '' || $limit_start == 0){
	$limit_start = 0;
}

// 전체 수
$sql_total_episode = "SELECT count(*) as total_episode FROM comics_episode_3 WHERE 1 AND ce_service_state='y' AND ce_cover!='' ";
$row_total_episode = sql_count($sql_total_episode, 'total_episode');

// 검색 수
$limit_episode = " LIMIT $limit_start , 100 ";
$sql_episode = " SELECT * FROM  `comics_episode_3` WHERE 1 AND ce_service_state='y' AND ce_cover!='' ORDER BY ce_no DESC $limit_episode ";
$result_episode = sql_query($sql_episode);

$limit_start += 100;

// 끝
if($limit_start > (intval($row_total_episode) + 100)){
	echo $sql_episode."<br/>";
	echo $limit_start."<br/>";
	echo (intval($row_total_episode) + 100)."<br/>";
	die;
}

// 디렉토리 생성
$storage_down = NM_THUMB_PATH;
while ($row_episode = sql_fetch_array($result_episode)) {

	$ce_cover_info			= pathinfo($row_episode['ce_cover']);
	$ce_cover_name			= strtolower($ce_cover_info['filename']);
	$ce_cover_dir			= strtolower($ce_cover_info['dirname']);
	$ce_cover_extension		= strtolower($ce_cover_info['extension']);

	$ce_cover_file			= $ce_cover_name.".".$ce_cover_extension;
	$ce_cover_dirroot		= $ce_cover_dir."/";

	$ce_cover_path = str_replace("/".$ce_cover_file, "", $row_episode['ce_cover']);
	$ce_cover_storage_save_path = $ce_cover_storage_down_path = $storage_down.'/'.$ce_cover_path;

	$ce_cover_storage_down_path = str_replace(NM_PATH, '', $ce_cover_storage_down_path);

	$ce_cover_save_to_filename = $ce_cover_storage_save_path.'/'.$ce_cover_file;

	tn_auto_upload_thumbnail($ce_cover_save_to_filename, $ce_cover_dirroot, $ce_cover_file, 'ce_cover', 'all', '');
	
	echo $ce_cover_save_to_filename."<br/>";
	echo $ce_cover_dirroot."<br/>";
	echo $ce_cover_file."<br/>";
	die;
}

// 다음
if($limit_start < (intval($row_total_episode) + 100)){
	goto_url(NM_ADM_URL.'/_ready/18img_resizing/'.$php_self_file_name.'.php?limit_start='.$limit_start);
}

?>