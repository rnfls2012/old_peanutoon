<? include_once '_common.php'; // 공통 ?>
<?

$t_db_name = 'peanutoon';	// $t_db_name -> aws DB
$s_db_name = 'peanutoonDB';	// $s_db_name -> kt  DB

// db명 -> /error/adm/ready.php 에서 수정해야함
// $t_db_name -> aws DB
// $s_db_name -> kt  DB

// 테이블명
$t_table		= "t_content_info";
$t_order_field	= "content_info_num";

$s_table	= "comics";

// 전체 수	
$t_sql_total = " SELECT count(*) as t_total FROM $t_db_name.$t_table ";
$t_row_total = sql_count($t_sql_total, 't_total'); // 710648

// 검색 수
$t_sql_limit = " LIMIT $limit_start , $limit_end ";
if($limit_end == 0 || $limit_end == ''){ $t_sql_limit =''; }

// 정렬 필드
$t_sql_order = " ORDER BY $t_order_field ASC ";

// DB QUERY
$t_sql = " SELECT * FROM $t_db_name.$t_table $t_sql_order $t_sql_limit ";
$t_result = sql_query($t_sql);

$limit_start += $limit_end;

// 끝
if($limit_start > (intval($t_row_total) + $limit_end)){
	echo $t_sql."<br/>";
	echo $limit_start."<br/>";
	echo (intval($t_row_total) + $limit_end)."<br/>";
	die;
}

$cm_small_1 = $cm_small_2 = $cm_small_3 = $cm_small_4 = $cm_small_5 = $cm_small_6 = array();
array_push($cm_small_5, 1,27,32,52,55,56,60,61,62,63,64,65,66,69,71,73,76,77,78,79,80,81,82,112,125,128);
array_push($cm_small_6, 9,14,23,25,29,46,57,58,59,67,83,84,85,89,90,91,94,95,100,102,113,114,120);
array_push($cm_small_4, 2,3,10,11,13,15,19,35,36,38,39,87,97,101,122,126);
array_push($cm_small_3, 20);
array_push($cm_small_2, 4,6,7,18,22,26,40,41,42,43,44,45,47,70,72,127);

while ($t_row = sql_fetch_array($t_result)) {

	$cm_no = $t_row['content_info_num'];
	
	$cm_big = $cm_page_way = '';
	if($t_row['view_direction'] == 'bsreader'){
		$cm_big = 4;
		$cm_page_way = 'b';
	}else{
		if($t_row['view_direction'] == 'down'){
			if(mb_substr($t_row['title'], 0, 5, 'utf8') == '[웹툰판]'){
				$cm_big = 3;
			}else{
				$cm_big = 2;
			}
			$cm_page_way = 's';
		}else{
			if($t_row['view_direction'] == 'right'){
				$cm_big = 1;
				$cm_page_way = 'r';
			}else{
				$cm_big = 1;
				$cm_page_way = 'l';
			}
		}
	}

	if($t_row['content_classify_code'] == 'novel'){
		$cm_small = 7;
	}else{
		$category_code_arr = explode(',',$t_row['category_code']);
		foreach($category_code_arr  as $category_code_key  => $category_code_val){
			if(in_array($category_code_val,$cm_small_2)){ $cm_small = 2; }
			else if(in_array($category_code_val,$cm_small_3)){ $cm_small = 3; }
			else if(in_array($category_code_val,$cm_small_4)){ $cm_small = 4; }
			else if(in_array($category_code_val,$cm_small_5)){ $cm_small = 5; }
			else if(in_array($category_code_val,$cm_small_6)){ $cm_small = 6; }
			else { $cm_small = 1; }
		}
	}

	$cm_provider		= $t_row['provider_num'];
	$cm_provider_sub	= $t_row['provider_num_2'];
	$cm_publisher		= $t_row['publisher_num'];
	$cm_professional	= $t_row['writer_num'];
	
	$cm_service = $t_row['public_yn'];
	if($t_row['public_yn'] == 'c'){ $cm_service = 'y'; }

	$cm_adult = $t_row['adult'];
	
	$cm_platform = 0;
	$service_platform_arr = explode(',',$t_row['service_platform']);
	if(in_array('web',		$service_platform_arr)){ $cm_platform+=1; }
	if(in_array('android',	$service_platform_arr)){ $cm_platform+=2; }
	if(in_array('ios',		$service_platform_arr)){ $cm_platform+=4; }
	if(in_array('naver',	$service_platform_arr)){ $cm_platform+=8; }
	if($cm_platform == 0){ $cm_platform = 1; }

	$cm_public_cycle = 0;
	$publication_cycle_code_arr = explode(',',$t_row['publication_cycle_code']);
	if(in_array('1', $publication_cycle_code_arr)){ $cm_public_cycle+=1; }		// 월
	if(in_array('2', $publication_cycle_code_arr)){ $cm_public_cycle+=2; }		// 화
	if(in_array('3', $publication_cycle_code_arr)){ $cm_public_cycle+=4; }		// 수
	if(in_array('4', $publication_cycle_code_arr)){ $cm_public_cycle+=8; }		// 목
	if(in_array('5', $publication_cycle_code_arr)){ $cm_public_cycle+=16; }		// 금
	if(in_array('6', $publication_cycle_code_arr)){ $cm_public_cycle+=32; }		// 토
	if(in_array('0', $publication_cycle_code_arr)){ $cm_public_cycle+=64; }		// 일
	if(in_array('7', $publication_cycle_code_arr)){ $cm_public_cycle+=128; }	// 보름
	if(in_array('8', $publication_cycle_code_arr)){ $cm_public_cycle+=256; }	// 월간
	
	$cm_end =			$t_row['finished_yn'];
	$cm_color =			$t_row['color'];

	$cm_series =		addslashes($t_row['title']);
	$cm_somaery =		addslashes($t_row['content_story']);
	$cm_comment =		addslashes($t_row['content_comment']);
	$cm_episode_total = $t_row['content_length'];

	$cm_pay	= $t_row['sale_price'];

	$cm_cover_path = 'aws/img/'.intval($t_row['content_info_num']/1000).'/'.$t_row['content_info_num'];

	$cm_cover = $cm_cover_path.'/cover/'.$t_row['cover_image'];
	if($t_row['cover_image'] == ''){ $cm_cover = ''; }

	$cm_cover_sub = $cm_cover_path.'/thumb/'.$t_row['list_image'];
	if($t_row['list_image'] == ''){ $cm_cover_sub = ''; }

	$cm_service_stop_date = '';
	if($cm_service == 'n'){ $cm_service_stop_date = NM_TIME_YMDHIS; }

	$cm_reg_date		= $t_row['regist_date'];

	$cm_episode_date	= $t_row['update_date'];

	$content_info_num	= $t_row['content_info_num'];

	// $cp_date = NM_TIME_YMDHIS; 

	$sql_insert = " 
	INSERT INTO $s_table ( cm_no, cm_big, cm_small, 
	cm_provider, cm_provider_sub, cm_publisher, cm_professional, 
	cm_service, cm_adult, 
	cm_platform, cm_public_cycle, cm_page_way, 
	cm_end, cm_color, 
	cm_series, cm_somaery, cm_comment, cm_episode_total, cm_pay,  
	cm_cover, cm_cover_sub, 
	cm_service_stop_date, cm_reg_date, cm_episode_date, 
	content_info_num 
	) VALUES ( 	
	'".$cm_no."', '".$cm_big."', '".$cm_small."', 
	'".$cm_provider."', '".$cm_provider_sub."', '".$cm_publisher."' , '".$cm_professional."' , 
	'".$cm_service."' , '".$cm_adult."' , 
	'".$cm_platform."' , '".$cm_public_cycle."' , '".$cm_page_way."' , 
	'".$cm_end."' , '".$cm_color."' , 
	'".$cm_series."' , '".$cm_somaery."' , '".$cm_comment."' , '".$cm_episode_total."' , '".$cm_pay."' , 
	'".$cm_cover."' , '".$cm_cover_sub."' , 
	'".$cm_service_stop_date."' , '".$cm_reg_date."' , '".$cm_episode_date."' , 
	'".$content_info_num."' ) ";

	// echo $sql_insert.'<br/>';
	if(sql_query($sql_insert)){
	}else{
		echo $sql_insert;
		die;
	}
}

$t_sql_total = "select count(*) as total from $t_db_name.$t_table";
$t_row_total = sql_count($t_sql_total, 'total');
echo $t_db_name.$t_table.' : '.$t_row_total.'<br/>';

$s_sql_total = "select count(*) as total from $s_table";
$s_row_total = sql_count($s_sql_total, 'total');
echo $s_table.' : '.$s_row_total.'<br/>';

// 자라자 피너툰(aws) 에서 패스워드 기법
// $password = trim(strip_tags(stripslashes("password")));
// $mhash_sha1_results = bin2hex(mhash(MHASH_SHA1, $password));
// echo $mhash_sha1_results.'<br/>';

// cacao ID 
// select * from t_customer where customer_email = '451225927' // aws -> 김선규

// select * from t_content_info where title like '%브레인%' AND delete_yn='n'
// published_yn='y'

?>