<? include_once '_common.php'; // 공통 ?>
<?

// sleep(10);

// 파일 정보
$php_self_file = pathinfo($_SERVER['PHP_SELF']);
$php_self_dirname  = strtolower($php_self_file['dirname']);
$php_self_file_name  = strtolower($php_self_file['filename']);
$php_self_extension  = strtolower($php_self_file['extension']);

// 한계 수
$limit_start = $_GET['limit_start'];

if($limit_start == '' || $limit_start == 0){ $limit_start = 0; }
$limit_end = 0;

// db명 -> /error/adm/ready.php 에서 수정해야함
// $t_db_name -> aws DB
// $s_db_name -> kt  DB

// 테이블명
$t_table		= "t_customer";
$t_order_field	= "customer_num";

$s_table	= "member";
$s_table2	= "member_sns";

// 전체 수	
$t_sql_total = " SELECT count(*) as t_total FROM $t_db_name.$t_table ";
$t_row_total = sql_count($t_sql_total, 't_total'); // 710648

// 검색 수
$t_sql_limit = " LIMIT $limit_start , $limit_end ";
if($limit_end == 0 || $limit_end == ''){ $t_sql_limit =''; }

// 정렬 필드
$t_sql_order = " ORDER BY $t_order_field ASC ";

// DB QUERY
$t_sql = " SELECT * FROM $t_db_name.$t_table $t_sql_order $t_sql_limit ";
$t_result = sql_query($t_sql);

while ($t_row = sql_fetch_array($t_result)) {
	
	$mb_facebook_id = $mb_kakao_id = $mb_naver_id = $mb_google_id = $mb_twitter_id = $mb_sns_type = '';
	$mb_id_sns = '';
	
	$mb_no = $t_row['customer_num'];

	$mb_email = addslashes($t_row['customer_email']);

	switch($t_row['third_party_company']){
		case 'facebook' : 
			$mb_sns_type = 'fcb'; 
			$mb_facebook_id = $t_row['facebook_id'];
		break;
		case 'kakao' : 
			$mb_sns_type = 'kko'; 
			$mb_kakao_id = $t_row['facebook_id'];
			$mb_email = '';
		break;
		case 'naver' : 
			$mb_sns_type = 'nid'; 
			$mb_naver_id = $t_row['facebook_id'];
		break;
		case 'twitter' : 
			$mb_sns_type = 'twt'; 
			$mb_twitter_id = $t_row['facebook_id'];
			$mb_email = '';
		break;
	}
	if($mb_sns_type != ''){	
		$mb_id_sns = $t_row['facebook_id'].'_'.$mb_sns_type; 
	}

	$mb_id = addslashes($t_row['customer_email']);


	$mb_idx = addslashes(mb_get_idx($mb_id));

	$mb_pass = $t_row['password'];
	if($t_row['password'] == ''){
		$mb_pass = get_encrypt_string($mb_no.pack('V*', rand(), rand(), rand(), rand()));
	}

	$mb_name = addslashes($t_row['customer_name']);

	$mb_nick = addslashes($t_row['nickname']);

	$mb_state = 'y';
	if($t_row['customer_state'] == 'expel'){ $mb_state = 'n'; }		// 관리자 이용중지
	if($t_row['customer_state'] == 'signout'){ $mb_state = 'n'; }	// 관리자 탈퇴

	$mb_cash_point = $t_row['cash'];
	$mb_point = $t_row['point'];

	$mb_sex = 'n';
	if($t_row['gender'] == 'f'){ $mb_sex = 'w'; }
	if($t_row['gender'] == 'm'){ $mb_sex = 'm'; }

	$mb_adult = $t_row['adult'];
	
	$mb_birth = $t_row['birthday'];
	if($t_row['birthday'] == '0000-00-00'){ $mb_birth = ''; }

	$mb_join_date = $mb_out_date = $mb_login_date = $mb_login_today = '';

	
	if($t_row['join_date'] != ''){ $mb_join_date = $t_row['join_date']; }
	if($t_row['login_date'] != ''){ $mb_login_date = $t_row['login_date']; }
	$mb_login_today = substr($mb_login_date, 0, 10);

	if($mb_state == 'n'){ $mb_out_date = NM_TIME_YMDHIS; }

	if($t_row['join_date'] != ''){ $mb_join_date = $t_row['join_date']; }

	$customer_num = $t_row['customer_num'];
	$customer_password = 'y';
	
	// 중복체크
	$member_overlap = false;
	if(in_array($t_row['customer_email'], $customer_email_overlap)){
		$member_overlap = true;
	}
	if(in_array($t_row['facebook_id'], $facebook_id_overlap)){
		$member_overlap = true;
	}

	$mb_aws_overlap = 'n';

	if($member_overlap == true){
		$mb_aws_overlap = 'y';

		$sql_insert_member_aws_overlap = " 
		INSERT INTO member_aws_overlap ( mb_no, mb_id, mb_idx, 
		mb_pass, 
		mb_name, mb_nick, 
		mb_facebook_id, mb_kakao_id, mb_naver_id, mb_google_id, mb_twitter_id, 
		mb_sns_type, 
		mb_email, 
		mb_state, 
		mb_cash_point, mb_point, 
		mb_sex, mb_adult, mb_birth, 
		mb_join_date, mb_out_date, 
		mb_login_date, mb_login_today, 
		customer_num, customer_password 
		) VALUES ( '".$mb_no."', '".$mb_id."', '".$mb_idx."', 
		'".$mb_pass."', 
		'".$mb_name."', '".$mb_nick."', 
		'".$mb_facebook_id."', '".$mb_kakao_id."', '".$mb_naver_id."', '".$mb_google_id."', '".$mb_twitter_id."', 
		'".$mb_sns_type."', 
		'".$mb_email."', 
		'".$mb_state."', 
		'".$mb_cash_point."', '".$mb_point."', 
		'".$mb_sex."', '".$mb_adult."', '".$mb_birth."', 
		'".$mb_join_date."', '".$mb_out_date."', 
		'".$mb_join_date."', '".$mb_login_today."', 
		'".$customer_num."', '".$customer_password."' )	";

		if(sql_query($sql_insert_member_aws_overlap)){
		}else{
			echo $sql_insert_member_aws_overlap.'<br/>';
			die;
		}

		$mb_id = 'member_aws_overlap_'.$mb_no;
	}

	$sql_insert = " 
	INSERT INTO $s_table ( mb_no, mb_id, mb_idx, 
	mb_pass, 
	mb_name, mb_nick, 
	mb_facebook_id, mb_kakao_id, mb_naver_id, mb_google_id, mb_twitter_id, 
	mb_sns_type, 
	mb_email, 
	mb_state, 
	mb_cash_point, mb_point, 
	mb_sex, mb_adult, mb_birth, 
	mb_join_date, mb_out_date, 
	mb_login_date, mb_login_today, 
	customer_num, customer_password, mb_aws_overlap 
	) VALUES ( '".$mb_no."', '".$mb_id."', '".$mb_idx."', 
	'".$mb_pass."', 
	'".$mb_name."', '".$mb_nick."', 
	'".$mb_facebook_id."', '".$mb_kakao_id."', '".$mb_naver_id."', '".$mb_google_id."', '".$mb_twitter_id."', 
	'".$mb_sns_type."', 
	'".$mb_email."', 
	'".$mb_state."', 
	'".$mb_cash_point."', '".$mb_point."', 
	'".$mb_sex."', '".$mb_adult."', '".$mb_birth."', 
	'".$mb_join_date."', '".$mb_out_date."', 
	'".$mb_join_date."', '".$mb_login_today."', 
	'".$customer_num."', '".$customer_password."', '".$mb_aws_overlap."' )	";
	
	if(sql_query($sql_insert)){
	}else{
		echo $sql_insert.'<br/>';
		die;
	}
	
	
	if($mb_sns_type != ''){	
		$sql_mb_sns_insert = "	INSERT INTO $s_table2( mbs_id, mbs_idx, mbs_service, mbs_sns_type, mbs_date ) VALUES (
														'".$t_row['facebook_id']."', '".mb_get_idx($t_row['facebook_id'])."', 
														'".$t_row['third_party_company']."', '".$mb_sns_type."', '".$mb_join_date."' 
														)";
		if(sql_query($sql_mb_sns_insert)){
		}
	}
	


	// sns일때 



	/*
	echo 'customer_num : '.$t_row['customer_num'].'<br/>';					// mb_no
	echo 'customer_email : '.$t_row['customer_email'].'<br/>';				// mb_id

	echo 'password : '.$t_row['password'].'<br/>';							// mb_pass
	echo 'facebook_id : '.$t_row['facebook_id'].'<br/>';					// mb_facebook_id
	echo 'customer_state : '.$t_row['customer_state'].'<br/>';				// mb_state

	echo 'customer_name : '.$t_row['customer_name'].'<br/>';				// mb_name

	echo 'nickname : '.$t_row['nickname'].'<br/>';							// mb_nick
	echo 'birthday : '.$t_row['birthday'].'<br/>';							// mb_birth
	echo 'gender : '.$t_row['gender'].'<br/>';								// mb_sex
	echo 'adult : '.$t_row['adult'].'<br/>';								// mb_adult

	echo 'third_party_company : '.$t_row['third_party_company'].'<br/>';	// mb_sns_type

	echo 'join_date : '.$t_row['join_date'].'<br/>';						// mb_join_date
	echo 'login_date : '.$t_row['login_date'].'<br/>';						// mb_login_date / mb_login_today
	echo 'cash : '.$t_row['cash'].'<br/>';									// mb_cash_point
	echo 'point : '.$t_row['point'].'<br/>';								// mb_point
		
	echo '////////////////////////////////////////////////////'.'<br/>';
	*/
}

$t_sql_total = "select count(*) as total from $t_db_name.$t_table";
$t_row_total = sql_count($t_sql_total, 'total');
echo $t_db_name.$t_table.' : '.$t_row_total.'<br/>';

$s_sql_total = "select count(*) as total from member";
$s_row_total = sql_count($s_sql_total, 'total');
echo 'member : '.$s_row_total.'<br/>';

$s_sql_total = "select count(*) as total from member_aws_overlap";
$s_row_total = sql_count($s_sql_total, 'total');
echo 'member_aws_overlap : '.$s_row_total.'<br/>';

// 자라자 피너툰(aws) 에서 패스워드 기법
// $password = trim(strip_tags(stripslashes("password")));
// $mhash_sha1_results = bin2hex(mhash(MHASH_SHA1, $password));
// echo $mhash_sha1_results.'<br/>';

// cacao ID 
// select * from t_customer where customer_email = '451225927' // aws -> 김선규



// 다음
$t_goto_url = NM_URL.$php_self_dirname.'/'.$php_self_file_name.'.'.$php_self_extension.'?limit_start='.$limit_start;
if($limit_end > 0){
	if($limit_start < (intval($row_total_comics) + 100)){
		// goto_url($t_goto_ur);
	}
}

?>