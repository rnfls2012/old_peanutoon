<? include_once '_common.php'; // 공통 ?>
<?


$t_db_name = 'peanutoon';	// $t_db_name -> aws DB
$s_db_name = 'peanutoonDB';	// $s_db_name -> kt  DB

// db명 -> /error/adm/ready.php 에서 수정해야함
// $t_db_name -> aws DB
// $s_db_name -> kt  DB

// 테이블명
$t_table		= "t_content";
$t_order_field	= "content_num";

$s_table1	= "comics_episode_1";
$s_table2	= "comics_episode_2";
$s_table3	= "comics_episode_3";
$s_table4	= "comics_episode_4";

// 전체 수	
$t_sql_total = " SELECT count(*) as t_total FROM $t_db_name.$t_table ";
$t_row_total = sql_count($t_sql_total, 't_total'); // 710648

// 검색 수
$t_sql_limit = " LIMIT $limit_start , $limit_end ";
if($limit_end == 0 || $limit_end == ''){ $t_sql_limit =''; }

// 정렬 필드
$t_sql_order = " ORDER BY $t_order_field ASC ";

// DB QUERY
$t_sql = " SELECT * FROM $t_db_name.$t_table $t_sql_order $t_sql_limit ";
$t_result = sql_query($t_sql);

$limit_start += $limit_end;

// 끝
if($limit_start > (intval($t_row_total) + $limit_end)){
	echo $t_sql."<br/>";
	echo $limit_start."<br/>";
	echo (intval($t_row_total) + $limit_end)."<br/>";
	die;
}


while ($t_row = sql_fetch_array($t_result)) {

	
	// if($t_row['delete_yn'] == 'y'){ continue; }

	
	// cm_big 구하기
	$sql_big = " select * from $t_db_name.t_content_info where content_info_num = '".$t_row['content_info_num']."'";
	$row_big = sql_fetch($sql_big);

	$s_table = '';
	
	if($row_big['view_direction'] == 'bsreader'){
		$s_table = $s_table4;
	}else{
		if($row_big['view_direction'] == 'down'){
			if(mb_substr($row_big['title'], 0, 5, 'utf8') == '[웹툰판]'){
				$s_table = $s_table3;
			}else{
				$s_table = $s_table2;
			}
		}else{
			if($row_big['view_direction'] == 'right'){
				$s_table = $s_table1;
			}else{
				$s_table = $s_table1;
			}
		}
	}

	$ce_no					= $t_row['content_num'];
	$ce_chapter				= $t_row['sequence_num'];
	$ce_comics				= $t_row['content_info_num'];
	$ce_title				= addslashes($t_row['content_title']);
	$ce_notice				= addslashes($t_row['sequence_name']);

	if($t_row['sequence_num'] == 0 && $t_row['sale_price'] > 0){ $ce_outer	= 'y'; }
	else{ $ce_outer	= 'n'; }

	$ce_pay					= $t_row['sale_price'];
	$ce_service_state		= $t_row['publish_yn'];
	$ce_service_date		= $t_row['publish_date'];
	if($t_row['publish_yn'] == 'n'){ $ce_service_date = ''; }

	$ce_free_state			= $t_row['reservation_yn'];
	$ce_free_date			= $t_row['publish_date'];
	if($t_row['reservation_yn'] == 'n'){ $ce_free_date = ''; }

	$ce_login				= $t_row['login_yn'];

	if($t_row['sale_price'] > 0 || $t_row['event_yn'] == 'y'){ $toon = 'toons'; }
	else{ $toon = 'toon'; }

	$ce_file = 'aws/'.$toon.'/'.intval($t_row['content_info_num']/1000).'/'.$t_row['content_info_num'].'/'.$t_row['content_num'];

	/*
	[
		{"name":"000.jpg","width":740,"height":7495,"quality":98},
		{"name":"001.jpg","width":740,"height":7965,"quality":98},
		{"name":"002.jpg","width":740,"height":8513,"quality":98},
		{"name":"003.jpg","width":740,"height":10733,"quality":98},
		{"name":"004.jpg","width":740,"height":1110,"quality":98}
	]
	*/

	$ce_file_list_arr = array();
	$ce_file_list = '';
	
	if($t_row['page_size'] != ''){
		$page_size1 = str_replace("[", "", $t_row['page_size']);
		$page_size2 = str_replace("]", "", $page_size1);
		$page_size3 = str_replace("\"", "", $page_size2);
		$page_size4 = str_replace("},", "}", $page_size3);
		
		$page_size_arr1 = explode('}',$page_size4);
		foreach($page_size_arr1  as $page_size_key1  => $page_size_val1){
			$page_size_arr2 = explode(',',$page_size_val1);
			foreach($page_size_arr2  as $page_size_key2  => $page_size_val2){
				$page_size_arr3 = explode(':',$page_size_val2);				
					$page_size_name1 = str_replace("}", "", $page_size_arr3[0]);
					$page_size_name2 = str_replace("{", "", $page_size_name1);
					if($page_size_name2 == 'name'){	array_push($ce_file_list_arr, $page_size_arr3[1]); }
			}
		}


		$ce_file_list			=  implode(", ",$ce_file_list_arr);
	}
	
	$ce_cover_path = 'aws/img/'.intval($t_row['content_info_num']/1000).'/'.$t_row['content_info_num'].'/'.$t_row['content_num'];
	$ce_file_count			= $t_row['page_length'];
	$ce_cover				= $ce_cover_path.'/thumb/'.$t_row['sequence_image'];
	$ce_order				= $t_row['order_num'];
	$ce_reg_date			= $t_row['regist_date'];
	$content_num			= $t_row['content_num'];

	$sql_insert = " 
	INSERT INTO $s_table ( ce_no, ce_chapter, ce_comics, ce_title, ce_notice, ce_pay, 
	ce_service_state, ce_service_date, ce_free_state, ce_free_date, 
	ce_login, ce_file, ce_file_list, ce_file_count, ce_cover, ce_order, ce_reg_date, content_num 
	) VALUES ( 	
	'".$ce_no."', '".$ce_chapter."', '".$ce_comics."', '".$ce_title."', '".$ce_notice."', '".$ce_pay."' , 
	'".$ce_service_state."' , '".$ce_service_date."' , '".$ce_free_state."' , '".$ce_free_date."' , 

	'".$ce_login."' , '".$ce_file."' , '".$ce_file_list."' , 
	'".$ce_file_count."' , '".$ce_cover."' , '".$ce_order."' , '".$ce_reg_date."' , '".$content_num."' ) ";
	
	if(sql_query($sql_insert)){
	}else{
		echo $sql_insert;
		die;
	}
}

$t_sql_total = "select count(*) as total from $t_db_name.$t_table";
$t_row_total = sql_count($t_sql_total, 'total');
echo $t_db_name.$t_table.' : '.$t_row_total.'<br/>';

$s_sql_total = "select count(*) as total from $s_table1";
$s_row_total = sql_count($s_sql_total, 'total');
echo $s_table1.' : '.$s_row_total.'<br/>';

$s_sql_total = "select count(*) as total from $s_table2";
$s_row_total = sql_count($s_sql_total, 'total');
echo $s_table2.' : '.$s_row_total.'<br/>';

$s_sql_total = "select count(*) as total from $s_table3";
$s_row_total = sql_count($s_sql_total, 'total');
echo $s_table3.' : '.$s_row_total.'<br/>';

$s_sql_total = "select count(*) as total from $s_table4";
$s_row_total = sql_count($s_sql_total, 'total');
echo $s_table4.' : '.$s_row_total.'<br/>';



?>