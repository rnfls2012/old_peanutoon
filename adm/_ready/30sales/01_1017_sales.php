<? include_once '_common.php'; // 공통 

die;

for($i=1; $i<=3; $i++){
	$t_sql = " SELECT *	FROM member_point_expen_episode_".$i." WHERE mpee_date >= '2017-10-17 14:25:00' AND  mpee_date < '2017-10-17 19:55:00'
				ORDER BY mpee_date ";
	$t_result = sql_query($t_sql);
	while ($t_row= sql_fetch_array($t_result)) {
		$comics = array();
		$episode = array();
		$comics_buy = array();
		$nm_member = array();
		$save_date = array();

		$comics['cm_no'] = $t_row['mpee_comics'];
		$comics['cm_big'] = $i;

		$episode['ce_no'] = $t_row['mpee_episode'];
		$episode['ce_chapter'] = $t_row['mpee_chapter'];

		$nm_member['mb_sex'] = $t_row['mpee_sex'];
		$nm_member['mb_birth'] = 2017 - intval($t_row['mpee_age']);
		if($t_row['mpee_sex'] == 'n'){ $nm_member['mb_birth'] = ""; }

		$comics_buy['sl_free'] = 0;
		$comics_buy['sl_sale'] = 0;
		if($t_row['mpee_sale'] == 'y'){ $comics_buy['sl_sale'] = 1; }
		$comics_buy['sl_event'] = 0;
		$comics_buy['mb_cash_point'] = $t_row['mpee_cash_point'];
		$comics_buy['mb_point'] = $t_row['mpee_point'];

		$comics_free = 'n';
		$pay_type = $t_row['mpee_way'];

		$save_date['year_month'] = $t_row['mpee_year_month'];
		$save_date['year'] = $t_row['mpee_year'];
		$save_date['month'] = $t_row['mpee_month'];
		$save_date['day'] = $t_row['mpee_day'];
		$save_date['hour'] = $t_row['mpee_hour'];
		$save_date['week'] = $t_row['mpee_week'];

		cs_set_sales_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date); // CP용 통계
		cs_set_sales_age_sex_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date); // CP용 통계
	}
}


/* ///////////////////////////////// CP용 통계 ///////////////////////////////// */


function cs_set_sales_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $set_date)
{
	global $nm_config;

	$dbarray = array('sales', 'vsales', 'sales_episode', 'vsales_episode');
	
	foreach($dbarray as $dbkey => $dbtable){
		if( ($dbkey+1) % 2 == 0  && intval($comics_buy['mb_point']) > 0 ){ continue; } // CP용 통계일 경우 포인트는 0

		$sql_pay_type = $sql_insert = $sql_update = "";

		switch($dbkey){
			case 0 :
			case 1 :// unique -> `sl_comics`, `sl_year_month`, `sl_year`, `sl_month`, `sl_day`, `sl_hour`, `sl_week`
					if($comics_free == 'n'){
						if($pay_type == 7){ $sql_pay_type = " sl_all_pay= (sl_all_pay+1), ";
						}else{ $sql_pay_type = " sl_pay= (sl_pay+1), "; }
					}

					$sql_insert = "
						INSERT INTO ".$dbtable." (sl_comics, sl_big, sl_year_month, sl_year, sl_month, sl_day, sl_hour, sl_week 
						) VALUES ('".$comics['cm_no']."', '".$comics['cm_big']."', 
						'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
						'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."' 
					)";
					
					$sql_update = "
						UPDATE ".$dbtable." SET 
						sl_open= (sl_open+1), 
						sl_free= (sl_free+".intval($comics_buy['sl_free'])."), 
						sl_sale= (sl_sale+".intval($comics_buy['sl_sale'])."), 
						".$sql_pay_type." 
						sl_event= (sl_event+".intval($comics_buy['sl_event'])."), 
						sl_point= (sl_point+".intval($comics_buy['mb_point'])."), 
						sl_cash_point= (sl_cash_point+".intval($comics_buy['mb_cash_point'])."), 
						sl_cash_type = '".intval($nm_config['cf_cup_type'])."' 
						WHERE 1 AND sl_comics = '".$comics['cm_no']."' AND sl_big = '".$comics['cm_big']."' 
						AND sl_year_month = '".$set_date['year_month']."' AND sl_year = '".$set_date['year']."' 
						AND sl_month = '".$set_date['month']."' AND sl_day = '".$set_date['day']."' 
						AND sl_hour = '".$set_date['hour']."' AND sl_week = '".$set_date['week']."' 
					";

					// 연도+월 총합통계
					$sql_insert_w = "
						INSERT INTO w_".$dbtable." (sl_comics, sl_big, sl_year_month, sl_cash_type 
						) VALUES ('".$comics['cm_no']."', '".$comics['cm_big']."', 
						'".$set_date['year_month']."', '".intval($nm_config['cf_cup_type'])."' 
					)";
					
					$sql_update_w = "
						UPDATE w_".$dbtable." SET 
						sl_open= (sl_open+1), 
						sl_open= (sl_open+".intval($comics_buy['sl_open'])."), 
						sl_free= (sl_free+".intval($comics_buy['sl_free'])."), 
						sl_sale= (sl_sale+".intval($comics_buy['sl_sale'])."), 
						".$sql_pay_type." 
						sl_event= (sl_event+".intval($comics_buy['sl_event'])."), 
						sl_point= (sl_point+".intval($comics_buy['mb_point'])."), 
						sl_cash_point= (sl_cash_point+".intval($comics_buy['mb_cash_point'])."), 
						sl_cash_type = '".intval($nm_config['cf_cup_type'])."' 
						WHERE 1 AND sl_comics = '".$comics['cm_no']."' AND sl_big = '".$comics['cm_big']."' 
						AND sl_year_month = '".$set_date['year_month']."' 
					";

			break;

			case 2 :
			case 3 :// unique -> `sas_comics`, `sas_year_month`, `sas_year`, `sas_month`, `sas_day`, `sas_hour`, `sas_week`
					if($comics_free == 'n'){
						if($pay_type == 7){ $sql_pay_type = " se_all_pay= (se_all_pay+1), ";
						}else{ $sql_pay_type = " se_pay= (se_pay+1), "; }
					}
					$sql_insert = "
						INSERT INTO ".$dbtable."_".$comics['cm_big']." ( 
						se_comics, se_episode, se_year_month, se_year, se_month, se_day, se_hour, se_week 
						) VALUES ( '".$comics['cm_no']."', '".$episode['ce_no']."', 
						'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
						'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."' 
					)";
					
					$sql_update = "
						UPDATE ".$dbtable."_".$comics['cm_big']." SET 
						se_open= (se_open+1), 
						se_free= (se_free+".intval($comics_buy['sl_free'])."), 
						se_sale= (se_sale+".intval($comics_buy['sl_sale'])."), 
						".$sql_pay_type." 
						se_event= (se_event+".intval($comics_buy['sl_event'])."), 
						se_point= (se_point+".intval($comics_buy['mb_point'])."), 
						se_cash_point= (se_cash_point+".intval($comics_buy['mb_cash_point'])."), 
						se_cash_type = '".intval($nm_config['cf_cup_type'])."', 
						se_chapter = '".$episode['ce_chapter']."' 
						WHERE 1 AND se_comics = '".$comics['cm_no']."' AND se_episode = '".$episode['ce_no']."' 
						AND se_year_month = '".$set_date['year_month']."' AND se_year = '".$set_date['year']."' 
						AND se_month = '".$set_date['month']."' AND se_day = '".$set_date['day']."' 
						AND se_hour = '".$set_date['hour']."' AND se_week = '".$set_date['week']."' 
					";
					
					// 연도+월 총합통계
					$sql_insert_w = "
						INSERT INTO w_".$dbtable."_".$comics['cm_big']." ( 
						se_comics, se_episode, se_year_month 
						) VALUES ( '".$comics['cm_no']."', '".$episode['ce_no']."', 
						'".$set_date['year_month']."' 
					)";
					
					$sql_update_w = "
						UPDATE w_".$dbtable."_".$comics['cm_big']." SET 
						se_open= (se_open+1), 
						se_free= (se_free+".intval($comics_buy['sl_free'])."), 
						se_sale= (se_sale+".intval($comics_buy['sl_sale'])."), 
						".$sql_pay_type." 
						se_event= (se_event+".intval($comics_buy['sl_event'])."), 
						se_point= (se_point+".intval($comics_buy['mb_point'])."), 
						se_cash_point= (se_cash_point+".intval($comics_buy['mb_cash_point'])."), 
						se_cash_type = '".intval($nm_config['cf_cup_type'])."', 
						se_chapter = '".$episode['ce_chapter']."' 
						WHERE 1 AND se_comics = '".$comics['cm_no']."' AND se_episode = '".$episode['ce_no']."' 
						AND se_year_month = '".$set_date['year_month']."' 
					";
			break;
		}
		// 저장
		@mysql_query($sql_insert); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
		sql_query($sql_update);

		// 연도+월 총합통계
		@mysql_query($sql_insert_w); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
		sql_query($sql_update_w);

		// echo $sql_insert.";<br/>";
		// echo $sql_update.";<br/>";
		// echo $sql_insert_w.";<br/>";
		// echo $sql_update_w.";<br/>";
		// die;
	}
}

function cs_set_sales_age_sex_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $set_date)
{
	global $nm_config;

	$dbarray = array('sales_age_sex', 'vsales_age_sex', 'sales_episode', 'vsales_episode');
	
	foreach($dbarray as $dbkey => $dbtable){
		if( ($dbkey+1) % 2 == 0  && intval($comics_buy['mb_point']) > 0 ){ continue; } // CP용 통계일 경우 포인트는 0

		$sql_sex = $sex_field = $sql_insert = $sql_update = "";

		switch($dbkey){
			case 0 :
			case 1 :// unique -> `sas_comics`, `sas_year_month`, `sas_year`, `sas_month`, `sas_day`, `sas_hour`, `sas_week`
					// 성별
					$sex_field = cs_set_sales_sex($nm_member, 'sas');
					$sql_sex = " ".$sex_field."= (".$sex_field."+1) ";

					// 나이별
					if($nm_member['mb_birth'] != ''){
						$sql_sex.= ", ".$sex_field.mb_get_age_group($nm_member['mb_birth'])."= (".$sex_field.mb_get_age_group($nm_member['mb_birth'])."+1) ";
					}

					$sql_insert = "
						INSERT INTO ".$dbtable." ( sas_comics, sas_big, 
						sas_year_month, sas_year, sas_month, sas_day, sas_hour, sas_week 
						) VALUES ('".$comics['cm_no']."', '".$comics['cm_big']."', 
						'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
						'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."' 
					)";
					
					$sql_update = "
					UPDATE ".$dbtable." SET ".$sql_sex." WHERE 1 
					AND sas_comics = '".$comics['cm_no']."' AND sas_big = '".$comics['cm_big']."' 
					AND sas_year_month = '".$set_date['year_month']."' AND sas_year = '".$set_date['year']."' 
					AND sas_month = '".$set_date['month']."' AND sas_day = '".$set_date['day']."' 
					AND sas_hour = '".$set_date['hour']."' AND sas_week = '".$set_date['week']."' 
					";

					// 연도+월 총합통계
					$sql_insert_w = "
						INSERT INTO w_".$dbtable." ( sas_comics, sas_big, 
						sas_year_month 
						) VALUES ('".$comics['cm_no']."', '".$comics['cm_big']."', 
						'".$set_date['year_month']."' 
					)";
					
					$sql_update_w = "
					UPDATE w_".$dbtable." SET ".$sql_sex." WHERE 1 
					AND sas_comics = '".$comics['cm_no']."' AND sas_big = '".$comics['cm_big']."' 
					AND sas_year_month = '".$set_date['year_month']."' 
					";
			break;

			case 2 :
			case 3 :// unique -> `seas_comics`, `seas_episode`, `seas_year_month`, `seas_year`, `seas_month`, `seas_day`, `seas_hour`, `seas_week`
					// 성별
					$sex_field = cs_set_sales_sex($nm_member, 'seas');
					$sql_sex = " ".$sex_field."= (".$sex_field."+1) ";

					// 나이별
					if($nm_member['mb_birth'] != ''){
						$sql_sex.= ", ".$sex_field.mb_get_age_group($nm_member['mb_birth'])."= (".$sex_field.mb_get_age_group($nm_member['mb_birth'])."+1) ";
					}

					$sql_insert = "
						INSERT INTO ".$dbtable."_".$comics['cm_big']."_age_sex ( seas_comics, seas_episode, 
						seas_year_month, seas_year, seas_month, seas_day, seas_hour, seas_week 
						) VALUES ( '".$comics['cm_no']."', '".$episode['ce_no']."', 
						'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
						'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."' 
					)";
					
					$sql_update = "
						UPDATE ".$dbtable."_".$comics['cm_big']."_age_sex SET ".$sql_sex." , seas_chapter='".$episode['ce_chapter']."'  WHERE 1 
						AND seas_comics = '".$comics['cm_no']."' AND seas_episode = '".$episode['ce_no']."' 
						AND seas_year_month = '".$set_date['year_month']."' AND seas_year = '".$set_date['year']."' 
						AND seas_month = '".$set_date['month']."' AND seas_day = '".$set_date['day']."' 
						AND seas_hour = '".$set_date['hour']."' AND seas_week = '".$set_date['week']."' 
					";

					// 연도+월 총합통계
					$sql_insert_w = "
						INSERT INTO w_".$dbtable."_".$comics['cm_big']."_age_sex ( seas_comics, seas_episode, 
						seas_year_month 
						) VALUES ( '".$comics['cm_no']."', '".$episode['ce_no']."', 
						'".$set_date['year_month']."' 
					)";
					
					$sql_update_w = "
						UPDATE w_".$dbtable."_".$comics['cm_big']."_age_sex SET ".$sql_sex." , seas_chapter='".$episode['ce_chapter']."'  WHERE 1 
						AND seas_comics = '".$comics['cm_no']."' AND seas_episode = '".$episode['ce_no']."' 
						AND seas_year_month = '".$set_date['year_month']."' 
					";
			break;
		}
		// 저장
		@mysql_query($sql_insert); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
		sql_query($sql_update);

		// 연도+월 총합통계
		@mysql_query($sql_insert_w); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
		sql_query($sql_update_w);
		

		// echo $sql_insert.";<br/>";
		// echo $sql_update.";<br/>";
		// echo $sql_insert_w.";<br/>";
		// echo $sql_update_w.";<br/>";
	}
		// die;
}






?>