<?
include_once '_common.php'; // 공통
$cms_head_title = "준비";
$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더

/* 작업 순서 */
$ready = array();

array_push($ready, array('index.php','','리스트',1,'0'));

array_push($ready, array('_auto',"01_everyhour.php",'매시간 오토',1,'a-1'));
array_push($ready, array('_auto',"02_everyday.php",'매일정각 오토',1,'a-2'));
array_push($ready, array('_auto',"03_everyday_four_ten.php",'매일 4시,10시,16시,22시 오토',1,'a-3'));
array_push($ready, array('_auto',"04_everyday_six.php",'매일 6시 오토',1,'a-4'));

array_push($ready, array('index.php','','리스트',1,'0'));

// array_push($ready, array('8event','06_thanksgiving.php','추석이벤트 초기화 및 셋팅',1,'8-6-1'));
// array_push($ready, array('8event','07_thanksgiving_win.php','추석이벤트담청자미니땅콩주기',1,'8-6-2'));



// Array_push($ready, array('8event','05_finalexam_coupon.php','모의고사교시할인권일괄주기',1,'8-5'));

// 통합 땅콩매출
/*
array_push($ready, array('33auto','01_top_auto.php','top_auto_저장',1,'33-1'));

array_push($ready, array('33auto','02_cmnew_auto.php','cmnew_auto_저장',1,'33-2'));
array_push($ready, array('33auto','03_cmtop_auto.php','cmtop_auto_저장',1,'33-3'));
array_push($ready, array('33auto','04_cmfree_auto.php','cmfree_auto_저장',1,'33-4'));
array_push($ready, array('33auto','05_cmgenre_auto.php','cmgenre_auto_저장',1,'33-5'));
array_push($ready, array('33auto','06_main_slide_moscroll.php','main_slide_moscroll_저장',1,'33-6'));

*/

// array_push($ready, array('33auto','01_top_auto.php','top_auto_저장',1,'33-1'));
// array_push($ready, array('4comics','06_comics_public_period.php','옛날웹툰 요일주기넣기',1,'4-1'));
// array_push($ready, array('22buycomics','06_member_buy_cancel.php','3억의 가장 작은 존재:프롤로그 & 사귀는 건 아냐 외전',1,'22-6'));
// array_push($ready, array('32eventattend','03_eventattend_m02.php','출석이벤트누적처리18-0202~0203',1,'32-2'));
// array_push($ready, array('32eventattend','01_eventattend.php','출석이벤트누적처리18-0101~0102',1,'32-1'));
// array_push($ready, array('30sales','01_1017_sales.php','통계',1,'30-1'));

// array_push($ready, array('31marketerlink','01_marketerlink_join.php','마케터링크 가입수처리',1,'31-1'));



/*
array_push($ready, array('29wsales','01_w_sales.php','코믹스별 매출-연도&월통계',1,'29-1'));
array_push($ready, array('29wsales','05_w_vsales.php','코믹스별 정산-연도&월통계',1,'29-5'));

array_push($ready, array('29wsales','02_w_sales_age_sex.php','코믹스별 성별&연령 매출-연도&월통계',1,'29-2'));
array_push($ready, array('29wsales','06_w_vsales_age_sex.php','코믹스별 성별&연령 정산-연도&월통계',1,'29-6'));

array_push($ready, array('29wsales','03_w_sales_episode.php','에피소드별 매출-연도&월통계',1,'29-3'));
array_push($ready, array('29wsales','07_w_vsales_episode.php','에피소드별 정산-연도&월통계',1,'29-3'));

array_push($ready, array('29wsales','04_w_sales_episode_age_sex.php','에피소드별 성별&연령 매출-연도&월통계',1,'29-4'));
array_push($ready, array('29wsales','08_w_vsales_episode_age_sex.php','에피소드별 성별&연령 정산-연도&월통계',1,'29-8'));
*/

// array_push($ready, array('10stats','02_stats_member_attend.php','출석체크통계',1,'10-2'));

// ICS통계
// array_push($ready, array('28tksk','01_stats_coll_ticketsocket.php','ICS통계',1,'27-1'));

// cs_x_error_log('x_pg_certify_kcp', 'kcp', NM_KCP_CR_PATH, "sql_certify_insert", "cs_set_member_point_income_event", "INSERT INTO pg_certify_kcp (", 3, "관리자에게 문의하시기 바랍니다");

// 앱 푸시
// array_push($ready, array('27app_push','01_app_push.php','PUSH 보내기',1,'27-1'));

// v통계
// array_push($ready, array('26vsales','01_vsales.php','땅콩결제만 통계넣기',1,'26-1'));
// array_push($ready, array('26vsales','02_aws_sales_recharge.php','AWS-충전데이터 가져오기',1,'26-2'));
// array_push($ready, array('26vsales','03_aws_sales_buy.php','AWS-코믹스구매데이터 가져오기',1,'26-3'));

// array_push($ready, array('26vsales','06_sales_agust_45.php','통계:8월4일~5일',1,'26-6'));
// array_push($ready, array('26vsales','06_sales_agust_45_2.php','통계요약데이터수정',1,'26-6'));
// array_push($ready, array('26vsales','07_vsales2.php','땅콩결제2만 통계넣기',1,'26-7'));
// array_push($ready, array('26vsales','08_sales_agust_45_3.php','통계요약데이터수정-열람수',1,'26-8'));

// array_push($ready, array('26vsales','16_sales_agust_45_2.php','통계요약데이터수정',1,'26-16'));
// array_push($ready, array('26vsales','17_vsales2.php','땅콩결제2만 통계넣기',1,'26-17'));
// array_push($ready, array('26vsales','18_sales_agust_45_3.php','통계요약데이터수정-열람수',1,'26-18'));

// array_push($ready, array('15check_update','01_comics_check.php','코믹스 정리 출력(메인이미지누락체크)',1,'12-1'));
// array_push($ready, array('15check_update','01_comics_check.php?mode=sub','코믹스 정리 출력(서브이미지누락체크)',1,'12-2'));
// array_push($ready, array('15check_update','02_comics_epicode_total.php','코믹스 에피소드 토탈 누락',1,'12-4'));
// array_push($ready, array('15check_update',"02_everyhour.php",'매시간 오토',1,'12-3'));
// array_push($ready, array('15check_update',"03_everyday.php",'매일정각 오토',1,'12-4'));
// array_push($ready, array('15check_update',"04_everyday_four_ten.php",'매일 4시,10시,16시,22시 오토',1,'12-5'));
// array_push($ready, array('15check_update',"05_everyday_six.php",'매일 6시 오토',1,'12-6'));

// 내일하기
/*
member_point_expen_comics 에서 mpec_cash_point OR mpec_point 정보누락 제대로 넣기
xsales 넣을때 혹시 모르니 member_point_expen_episode_1,2,3 기준으로 넣고 해당 소스 개발하기
*/




// array_push($ready, array('23member_overlap',"01_member_2_overlap.php",'회원중복 저장',1,'23-1'));
// array_push($ready, array('23member_overlap',"02_member_2_overlap_statey.php",'회원중복 저장(상태y중복)',1,'23-2'));
// array_push($ready, array('23member_overlap',"03_member_statey.php",'회원중복상태수정',1,'23-3'));
// array_push($ready, array('23member_overlap',"03_member_statey_overlap.php",'회원중복상태수정2-(member_statey_overlap상태적용)',1,'23-3'));
// array_push($ready, array('23member_overlap',"04_member_buy_comics_mode.php",'회원중복 상태수정-(member_2_overlap상태적용)',1,'23-4'));
// array_push($ready, array('23member_overlap',"05_member_buy_comics_mode.php",'회원중복 소장정보이동',1,'23-5'));
// array_push($ready, array('23member_overlap',"06_member_sns.php",'sns회원데이터 재정리',1,'23-6'));
// array_push($ready, array('23member_overlap',"07_xsales.php",'cash_point데이터 넣기',1,'23-7'));
// array_push($ready, array('23member_overlap',"08_point_reward.php",'150미니땅콩보상',1,'23-8'));
// array_push($ready, array('23member_overlap',"09mpec_update.php",'cash_point_point데이터 update',1,'23-7'));



array_push($ready, array('23member_overlap',"10_member_buy_comics_move.php",'회원중복 소장정보이동',1,'23-10'));
// array_push($ready, array('23member_overlap',"11_member_attend_move.php",'회원 출석체크 정보 이동',1,'23-11'));

array_push($ready, array('21member',"07_member_180530.php",'회원중복리스트',1,'21-7'));
/*
array_push($ready, array('21member',"01_member.php",'회원중복리스트',1,'21-1'));
array_push($ready, array('21member',"02_member_aws_overlap.php",'회원중복 따로 데이터넣기',1,'21-2'));
array_push($ready, array('21member',"03_member_aws_overlap_july.php",'회원중복 7월 6~7일 처리 데이터넣기',1,'21-3'));
*/

// array_push($ready, array('21member',"05_member_invite_code.php",'회원 친구초대 코드 넣기',1,'21-5'));
// array_push($ready, array('21member',"04_member_aws_overlap_july_list.php",'회원중복 7월 6~7일 처리 데이터 SQL 보기',1,'21-1'));
// array_push($ready, array('21member',"06_member_point_income.php",'회원 땅콩/미니땅콩 지급',1,'21-6'));


/*
array_push($ready, array('22buycomics',"02_member_buy_episode.php",'회원소장정보_에피소드정보',1,'22-2'));
array_push($ready, array('22buycomics',"03_member_buy_comics.php",'회원소장정보_코믹스정보',1,'22-3'));
array_push($ready, array('22buycomics',"04_member_buy_edisode_idx.php",'회원소장-에피소드 idx넣기&중복제거 ',1,'22-4'));
array_push($ready, array('22buycomics',"05_member_buy_edisode_idx_view.php",'회원소장-에피소드 결제 중복 리스트 ',1,'22-5'));

array_push($ready, array('19bak',"01_member.php",'회원정보',1,'19-1'));
array_push($ready, array('19bak',"01_member2.php",'회원정보2',1,'19-1-1'));
array_push($ready, array('19bak',"02_comics.php",'코믹스정보',1,'19-2'));
array_push($ready, array('19bak',"03_episode.php",'코믹스화정보',1,'19-3'));
array_push($ready, array('19bak',"04_member_book.php",'회원책정보',1,'19-2'));
array_push($ready, array('19bak',"04_member_book_1.php",'회원책정보2',1,'19-3'));



array_push($ready, array('18img_resizing',"01_cover.php"					,'코믹스커버_다운로드(end)'							,1,'18-1'));
array_push($ready, array('18img_resizing',"01_cover_thumbnail.php"			,'코믹스커버_썸네일(end)'							,1,'18-2'));

array_push($ready, array('18img_resizing',"02_cover_sub.php"				,'코믹스커버서브_다운로드(end)'						,1,'18-3'));
array_push($ready, array('18img_resizing',"02_cover_sub_thumbnail.php"		,'코믹스커버서브_썸네일(die)'						,1,'18-4'));

array_push($ready, array('18img_resizing',"03_cover_episode.php"			,'코믹스커버에피소드_다운로드(end)'					,1,'18-5'));
array_push($ready, array('18img_resizing',"03_cover_episode_thumbnail.php"	,'코믹스커버에피소드_썸네일(end)'					,1,'18-6'));


array_push($ready, array('18img_resizing',"04_1_cover_upload.php"			,'코믹스커버_업로드(end)'							,1,'18-7'));
array_push($ready, array('18img_resizing',"04_2_cover_sub_upload.php"		,'코믹스커버서브버_업로드(end)'						,1,'18-8'));
array_push($ready, array('18img_resizing',"04_3_cover_episode_upload.php"	,'코믹스커버에피소드_업로드(end)'					,1,'18-9'));

array_push($ready, array('18img_resizing',"05_ce1_cover.php"				,'에피소드1_커버_다운로드(end)'						,1,'18-10'));
array_push($ready, array('18img_resizing',"05_ce2_cover.php"				,'에피소드2_커버_다운로드(end)'						,1,'18-11'));
array_push($ready, array('18img_resizing',"05_ce3_cover.php"				,'에피소드3_커버_다운로드(end)'						,1,'18-12'));

array_push($ready, array('18img_resizing',"06_ce1_cover_thumbnail.php"		,'에피소드1_커버_썸네일(end)'						,1,'18-13'));
array_push($ready, array('18img_resizing',"06_ce2_cover_thumbnail.php"		,'에피소드2_커버_썸네일(end)'						,1,'18-14'));
array_push($ready, array('18img_resizing',"06_ce3_cover_thumbnail.php"		,'에피소드3_커버_썸네일(end)'						,1,'18-15'));

array_push($ready, array('18img_resizing',"07_ce1_cover_upload.php"			,'에피소드1_커버_업로드(end)'						,1,'18-16'));
array_push($ready, array('18img_resizing',"07_ce2_cover_upload.php"			,'에피소드2_커버_업로드(end)'						,1,'18-17'));
array_push($ready, array('18img_resizing',"07_ce3_cover_upload.php"			,'에피소드3_커버_업로드(end)'						,1,'18-18'));


array_push($ready, array('18img_resizing',"08_ce1_file_list.php"			,'에피소드1_파일리스트(end)'						,1,'18-19'));
array_push($ready, array('18img_resizing',"08_ce2_file_list.php"			,'에피소드2_파일리스트(end)'						,1,'18-20'));
array_push($ready, array('18img_resizing',"08_ce3_file_list.php"			,'에피소드3_파일리스트(end)'						,1,'18-21'));


array_push($ready, array('18img_resizing',"09_ce1_file_list_thumbnail.php"	,'에피소드1_파일리스트_썸네일(end)'					,1,'18-22'));
array_push($ready, array('18img_resizing',"09_ce2_file_list_thumbnail.php"	,'에피소드2_파일리스트_썸네일(end)'					,1,'18-23'));
array_push($ready, array('18img_resizing',"09_ce3_file_list_thumbnail.php"	,'에피소드3_파일리스트_썸네일(end)'					,1,'18-24'));

array_push($ready, array('18img_resizing',"10_ce1_file_list_upload.php"	,	'에피소드1_파일리스트_업로드(end)'					,1,'18-25'));
array_push($ready, array('18img_resizing',"10_ce2_file_list_upload.php"	,	'에피소드2_파일리스트_업로드(end)'					,1,'18-26'));
array_push($ready, array('18img_resizing',"10_ce3_file_list_upload.php"	,	'에피소드3_파일리스트_업로드(end)'					,1,'18-27'));

*/




// array_push($ready, array('17db_add',"01_episode_file_info.php",'에피소드 파일 정보 넣기',1,'17-1'));
// array_push($ready, array('17db_add',"02_sales.php",'sales_sex_age 초기화',1,'17-1'));

// array_push($ready, array('15check_update',"06_member_point_use_creat.php",'사용내역생성',1,'12-7'));
// array_push($ready, array('15check_update',"07_admin_member_del_data.php",'결제부분 관리자 계정 데이터 제거',1,'12-8'));

// array_push($ready, array('16data_transfer',"01_member.php",'데이터리셋 및 이관',1,'13-1'));
// array_push($ready, array('16data_transfer',"02_member_add.php",'데이터 이관',1,'13-1'));
// array_push($ready, array('16data_transfer',"03_human.php",'휴면회원데이터 이관',1,'13-1'));

// array_push($ready, array('1db_create','01_mzzang_create.php','테이블 재생성',1,'1-1'));
// array_push($ready, array('1db_create','02_mzzang_content_copy.php','테이블 복사',1,'1-2'));
/*
array_push($ready, array('2file','01_dir_del.php','야톡, 기존웹툰 디렉토리, 필요없는 폴더와 파일 삭제',1,'2-1'));
array_push($ready, array('2file','02_comics_move.php','단행본파일 -> data폴더/1/해당단행본번호로 이동',1,'2-2'));
array_push($ready, array('2file','03_comics_cover_move.php','단행본커버파일 -> data폴더/1/해당단행본번호로 이동 & data폴더/필요없는 확장자 삭제',1,'2-3'));

array_push($ready, array('3config','01_config.php','데이터 기본설정',1,'3-1'));
array_push($ready, array('3config','02_config_long_text.php','데이터 설정시 긴글들',1,'3-2'));
array_push($ready, array('3config','03_config_unit_pay.php','정산 단위',1,'3-3'));
array_push($ready, array('3config','04_config_recharge_price.php','충전시 가격',1,'3-4'));

array_push($ready, array('4comics','01_comics.php','기존 uploadfile가 변환된 코믹스정보',1,'4-1'));
array_push($ready, array('4comics','02_comics_episode_1.php','기존 content가 변환된 에피소드 - 단행본 화정보',1,'4-2'));
array_push($ready, array('4comics','03_comics_provider.php','에피소드 제공사',1,'4-3'));
array_push($ready, array('4comics','04_comics_publisher.php','에피소드 출판사',1,'4-4'));
array_push($ready, array('4comics','05_comics_professional.php','에피소드 작가',1,'4-5'));

array_push($ready, array('5member','01_member.php','회원정보',1,'5-1'));
array_push($ready, array('5member','02_member_comics_mark.php','회원 코믹스 즐겨찾기',1,'5-2'));
array_push($ready, array('5member','03_member_buy_comics.php','회원 소장 코믹스 리스트',1,'5-3'));
array_push($ready, array('5member','04_member_buy_episode_1.php','회원 소장 코믹스 화 단행본리스트',1,'5-4'));
array_push($ready, array('5member','05_member_cash.php','회원 충전',1,'5-5'));
array_push($ready, array('5member','06_member_human.php','휴면 회원',1,'5-6'));
*/
array_push($ready, array('5member','07_member_mailing_FebMar.php','18년 2월 3월 회원 reward 테이블 이동',1,'5-7'));
/*
array_push($ready, array('6point','01_member_point_expen_comics.php','회원 코믹스 구매 정보',1,'6-1'));
array_push($ready, array('6point','02_member_point_expen_episode_1.php','회원 코믹스 구매 코믹스 화 단행본리스트',1,'6-2'));
array_push($ready, array('6point','03_member_point_income.php','회원 코믹스 화폐 충전 내역',1,'6-3'));
array_push($ready, array('6point','04_member_point_apk.php','회원 APK 다운로드 보상',1,'6-4'));

array_push($ready, array('7sales','01_sales.php','매출 코믹스 통계',1,'7-1'));
array_push($ready, array('7sales','02_sales_age_sex.php','매출 코믹스 나이, 성별 통계',1,'7-2'));
array_push($ready, array('7sales','03_sales_episode_1.php','매출 코믹스 화-단행본별 통계',1,'7-3'));
array_push($ready, array('7sales','04_sales_episode_1_age_sex.php','매출 코믹스 화-단행본별 나이, 성별 통계',1,'7-4'));
array_push($ready, array('7sales','05_sales_recharge.php','매출 충전 통계',1,'7-5'));

array_push($ready, array('8event','01_event_pay.php','무료&코인 설정',1,'8-1'));
array_push($ready, array('8event','02_event_pay_comics.php','무료&코인 설정- 코믹스리스트',1,'8-2'));
array_push($ready, array('8event','03_event_attend.php','출석체크',1,'8-3'));

array_push($ready, array('9board','01_board_help.php','도움말',1,'9-1'));
array_push($ready, array('9board','02_board_ask.php','문의/제안',1,'9-2'));
array_push($ready, array('9board','03_board_report.php','오류제보',1,'9-3'));
array_push($ready, array('9board','04_board_notice.php','알림',1,'9-4'));

array_push($ready, array('10stats','01_stats_log_member.php','회원 로그',1,'10-1'));

// array_push($ready, array('11epromotion','01_banner.php','배너',1,'11-1')); // 코딩 안함;;; 머 없음;;

array_push($ready, array('12channel','01_pg_channel_allthegate.php','올더게이트',1,'12-1'));
array_push($ready, array('12channel','02_pg_channel_allthegate_virtual_get.php','올더게이트_가상get',1,'12-2'));
array_push($ready, array('12channel','03_pg_channel_allthegate_virtual_put.php','올더게이트_가상put',1,'12-3'));
array_push($ready, array('12channel','04_pg_channel_toss.php','TOSS',1,'12-4'));
*/

array_push($ready, array('34aws_img_resizing',"01_cover.php",'AWS 이미지 다운로드',1,'18-1'));
array_push($ready, array('34aws_img_resizing',"01_cover_thumbnail.php",'AWS 이미지 Thumneil 생성',1,'18-2'));
array_push($ready, array('34aws_img_resizing',"02_cover_sub.php",'AWS 이미지 다운로드',1,'18-3'));
array_push($ready, array('34aws_img_resizing',"02_cover_sub_thumbnail.php",'AWS 이미지 Thumneil 생성',1,'18-4'));

/* ///////////////////////// 체크라인 ///////////////////////// */

/*
나중에 서비스 시작할때 남겨둘것들
admin_cms_access

board_help
board_ask
board_report
board_notice

comics
comics_episode_1
comics_episode_2
comics_episode_3
comics_provider
comics_publisher
comics_professional
comics_keyword
comics_menu
comics_ranking
comics_text
config

config_only_id
config_long_text
config_unit_pay
admin_cms_access

config_recharge_price
member
member_comics_mark
member_comics_open
member_buy_comics
member_buy_episode_1
member_buy_episode_2
member_buy_episode_3
member_cash
member_human
member_join_fail

pg_channel
pg_channel_allthegate
pg_channel_allthegate_virtual_get
pg_channel_allthegate_virtual_put
pg_channel_toss

pf_channel
pf_channel_comtiz
pf_channel_april7
pf_channel_lovewar

pi_channel
pi_channel_niceipin

이 외엔 전체 데이터만 삭제



*/
$pwd_dir = '_ready';

$login_pw_sha1 ='';
// $password_sha1 = trim(strip_tags(stripslashes('123')));
// $login_pw_sha1 = bin2hex(mhash(MHASH_SHA1, $password_sha1));

?>
<style type="text/css">
	.li_top{margin-top:10px;}
</style>
<div>
	<ul>
		<? foreach($ready as $ready_key => $ready_val){ 
			$no = $ready_key+1;
			$li_class = $li_class_chk = "";
			$li_class_chk = substr($ready_val[4],-1);
			if($ready_val[3] != 1){continue;}
			if($li_class_chk == '0' || $li_class_chk == '1'){$li_class ="li_top";}
		?>
			<li class="<?=$li_class?>"><a href='<?=NM_ADM_URL?>/<?=$pwd_dir?>/<?=$ready_val[0];?>/<?=$ready_val[1];?>' target='_self'><?=$ready_val[4]?>.<?=$ready_val[2];?></a></li>
		<?}?>
	</ul>
</div>

<div><?=$login_pw_sha1?></div>