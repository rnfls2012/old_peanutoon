<?
include_once '_common.php'; // 공통

$dbtable = "stats_log_member";
$sql2_cnt = "SELECT count(*) as cnt FROM `$dbtable` ";
$row2_cnt = sql_count($sql2_cnt, 'cnt');

$sql_chk_cnt = "SELECT count(*) as cnt FROM `login_log_back` ";
$row_chk_cnt = sql_count($sql_chk_cnt, 'cnt');

echo $sql2_cnt."<br/>";

echo $sql_chk_cnt."<br/>";

if($row_chk_cnt == 0 && $row2_cnt == 0){
	/* 테이블 카피 */
	$sql_cp_stats_back = "CREATE TABLE `login_log_back` 
						SELECT * FROM `m_mangazzang_cp`.`login_log` lg 
						LEFT JOIN `member` m ON lg.log_id = m.mb_id 
						WHERE m.mb_id IS NOT NULL
						ORDER BY lg.`log_no` ASC ";
	sql_query($sql_cp_stats_back);
}else{
	echo $dbtable."_back 테이블이 복사 되였으며, 파일경로도 수정업데이트 되였습니다."."<br/>";
}

$sql_cp_stats = "SELECT * FROM  `login_log_back` WHERE 1 ORDER BY log_no ASC  ";
$result_cp_stats = sql_query($sql_cp_stats);
echo $sql_cp_stats."<br/>";

$sql_cp_list = array('slm_member', 'slm_member_idx');
array_push($sql_cp_list, 'slm_date', 'slm_type', 'slm_kind', 'slm_user_agent', 'slm_version');

$sql2_insert = "";
if($row2_cnt == 0){
	// 데이터 복사
	while ($row_cp = sql_fetch_array($result_cp_stats)) {
		$row_cp['slm_member'] = $row_cp['mb_no'];
		$row_cp['slm_member_idx'] = $row_cp['mb_idx'];

		$row_cp['slm_date'] = $row_cp['log_date'];
		$row_cp['slm_type'] = get_os($row_cp['log_type']);
		$row_cp['slm_kind'] = get_brow($row_cp['log_type']);
		$row_cp['slm_user_agent'] = $row_cp['log_type'];

		$row_cp['slm_version'] = $row_cp['log_access_version'];
		if($row_cp['log_access_version'] == ''){
			$row_cp['slm_version'] = NM_VERSION;
		}
		



		$sql2_insert = "INSERT INTO `$dbtable` ( ";
		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 필드문구 */
			$sql2_insert.= $sql_cp_val.', ';
		}
		
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

		/* SQL문 VALUES 시작 */
		$sql2_insert.= ' )VALUES( ';

		foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
			/* SQL문 값문구 */
			if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
				$sql2_insert.= 'NULL, ';
			}else{
				$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
			}
		}
		$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
		/* SQL문 마무리 */
		$sql2_insert.= ' ); ';
		sql_query($sql2_insert);
		echo $sql2_insert."<br/>";		
	}
}else{
	echo $dbtable." 데이터 복사되였습니다."."<br/>";
}

?>