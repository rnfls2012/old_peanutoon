<?
include_once '_common.php'; // 공통

$image_path = "";
$image_small_path = "";

/* 커버이미지 이동 및 커퍼DB 이미지 경로 수정 */
$cover_search_sql = "SELECT * FROM `m_mangazzang_cp`.`fileupload` 
				WHERE  big = '1' group by filenum order by filenum ";

$cover_search_result = sql_query($cover_search_sql);
while ($cover_search_row = sql_fetch_array($cover_search_result)) {
	$image_path = str_replace("./", "/", NM_PATH.$cover_search_row['image']);
	$image_small_path = str_replace("./", "/", NM_PATH.$cover_search_row['image_small']);

	// 확장자 체크
	$image_extension = substr($cover_search_row['image'], -4);
	$image_small_extension = substr($cover_search_row['image_small'], -4);

	$image_data_path = "/data/".$cover_search_row['big']."/".$cover_search_row['filenum']."/cover".$image_extension;
	$image_small_data_path = "/data/".$cover_search_row['big']."/".$cover_search_row['filenum']."/cover_sub".$image_small_extension;

	$image_cp_path = NM_PATH.$image_data_path;
	$image_small_cp_path = NM_PATH.$image_small_data_path;

	echo 'cp -rp '.$image_path.' '.$image_cp_path."<br/>";
	echo 'cp -rp '.$image_small_path.' '.$image_small_cp_path."<br/>";
	exec('cp -rp '.$image_path.' '.$image_cp_path);
	exec('cp -rp '.$image_small_path.' '.$image_small_cp_path);
}

$uploads = is_dir(NM_PATH.'/uploads');
if($uploads == true){
	echo 'rm -rf '.NM_PATH.'/uploads'."<br/>";
	exec('rm -rf '.NM_PATH.'/uploads');
}else{
	echo "uploads 폴더가 삭제되어 없음"."<br/>";
}


/* /data폴더에 .db 확장자 삭제 */
$del_extension = array('db','ini','zip','doc');
foreach($del_extension as $del_ex_key => $del_ex_val){ 
	exec('find '.NM_PATH.'/data -name "*.'.$del_ex_val.'" -exec rm -rf {} \;',$outArray);
	echo "NM_PATH.'/data 에서 확장자 ".$del_ex_val." 파일삭제완료"."<br/>";
}

?>