<?
include_once '_common.php'; // 공통

$sql_cp_member = "SELECT * FROM  `m_mangazzang_cp`.`member_human` WHERE 1 ";
$result_cp_member = // sql_query($sql_cp_member);

$sql_cp_list = array('mb_no','mb_id', 'mb_idx', 'mb_pass', 'mb_name', 'mb_level', 'mb_class');
array_push($sql_cp_list, 'mb_email','mb_phone');
array_push($sql_cp_list, 'mb_state');
array_push($sql_cp_list, 'mb_cash_point','mb_point');
array_push($sql_cp_list, 'mb_sex', 'mb_post', 'mb_adult', 'mb_ipin', 'mb_overlap');
array_push($sql_cp_list, 'mb_won', 'mb_won_count', 'mb_birth');
array_push($sql_cp_list, 'mb_join_date', 'mb_out_date', 'mb_stop_date', 'mb_login_date');
array_push($sql_cp_list, 'mb_human_date', 'mb_human_email_date', 'mb_human_note');

while ($row_cp = sql_fetch_array($result_cp_member)) {
	$row_cp['mb_no'] = $row_cp['member_num'];
	$row_cp['mb_id'] = $row_cp['member_id'];
	$row_cp['mb_idx'] = $row_cp['member_id_idx'];
	$row_cp['mb_pass'] = $row_cp['member_pass'];
	$row_cp['mb_name'] = $row_cp['member_name'];

	$db_mb_level = 2;
	if($row_cp['member_level'] == "7"){ 
		$db_mb_level = 21; 
	}else if($row_cp['member_level'] == "9"){ 
		$db_mb_level = 30;
	}
	$row_cp['mb_level'] = $db_mb_level;

	$db_mb_class = 'c';
	if($row_cp['member_level'] == "7"){ 
		$db_mb_class = 'a'; 
	}else if($row_cp['member_level'] == "9"){ 
		$db_mb_class = 'a'; 
	}
	$row_cp['mb_class'] = $db_mb_class;

	$row_cp['mb_email'] = $row_cp['member_email'];
	$row_cp['mb_phone'] = $row_cp['member_phone'].$row_cp['member_phone2'].$row_cp['member_phone3'];

	$db_mb_state = 'y';
	if($row_cp['member_out_date'] != ""){
		$db_mb_state = 'n';
	}
	$row_cp['mb_state'] = "s";

	$row_cp['mb_cash_point'] = ceil($row_cp['member_cash_point']/100);
	$row_cp['mb_point'] = ceil($row_cp['member_point']/100);

	$db_mb_sex = 'n';
	if($row_cp['member_sex'] == "M" && $row_cp['member_birth'] != ""){ 
		$db_mb_sex = 'm'; 
	}else if($row_cp['member_sex'] == "Y" && $row_cp['member_birth'] != ""){ 
		$db_mb_sex = 'w';
	}
	$row_cp['mb_sex'] = $db_mb_sex;

	$db_mb_post = 'n';
	if($row_cp['member_out_date'] == "Y"){
		$db_mb_post = 'y';
	}
	$row_cp['mb_post'] = $db_mb_post;

	$db_mb_adult = 'n';
	if($row_cp['member_adult'] == "1" && $row_cp['mb_sex'] != "n" || $row_cp['mb_level'] > 9){
		$db_mb_adult = 'y';
	}
	$row_cp['mb_adult'] = $db_mb_adult;

	$row_cp['mb_ipin'] = $row_cp['member_info'];
	$row_cp['mb_overlap'] = intval($row_cp['member_overlap_key']);

	$row_cp['mb_won'] = $row_cp['member_cash'];
	$row_cp['mb_won_count'] = $row_cp['member_cash_num'];
	$row_cp['mb_birth'] = $row_cp['member_birth'];

	$db_mb_join_date = '';
	if($row_cp['member_join_date'] != ""){
		$db_mb_join_date = $row_cp['member_join_date']." 00:00:00";
	}
	$row_cp['mb_join_date'] = $db_mb_join_date ;
	

	$db_mb_out_date = '';
	if($row_cp['member_out_date'] != ""){
		$db_mb_out_date = $row_cp['member_out_date']." 00:00:00";
	}
	$row_cp['mb_out_date'] = $db_mb_out_date ;

	$row_cp['mb_stop_date'] = "";
	$row_cp['mb_login_date'] = $row_cp['member_login_date'];

	$row_cp['mb_human_date'] = $row_cp['human_date'];
	$row_cp['mb_human_email_date'] = $row_cp['human_email_date'];
	$row_cp['mb_human_note'] = $row_cp['route'];


	$sql2_insert = "INSERT INTO `member_human` ( ";
	foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
		/* SQL문 필드문구 */
		$sql2_insert.= $sql_cp_val.', ';
	}
	
	$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

	/* SQL문 VALUES 시작 */
	$sql2_insert.= ' )VALUES( ';

	foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
		/* SQL문 값문구 */
		$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
	}
	$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
	/* SQL문 마무리 */
	$sql2_insert.= ' ); ';
	// sql_query($sql2_insert);
	echo $sql2_insert."<br/>";		
}

?>