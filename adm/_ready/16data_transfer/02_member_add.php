<?
include_once '_common.php'; // 공통

/*

임시 DB로 테스트 5월1일 m_mangazzang_cp
임시 DB로 테스트 5월1일 megacomicsDB

1.충전회원 뺴고 관리자 ID 삭제
2.데이터 리셋 - 즐겨찾기, 대여정보 삭제
3.member 회원정보(엠짱)
4.comics 정보(엠짱)
5.episode 정보(엠짱)
6.buy_member(엠짱)
7.휴면회원데이터 가져오기(덮어쓰기)
*/


// ///////////////////////////////// 1.충전회원 뺴고 관리자 ID 삭제

// ///////////////////////////////// 2.데이터 리셋

// ///////////////////////////////// 3.member 회원정보(엠짱)

// ///////////////////////////////// 4.comics 정보(엠짱)

// ///////////////////////////////// 5.episode 정보(엠짱)

// ///////////////////////////////// 위 1~5번은 수동으로 처리하고 누락된 6데이터 넣기

// content_no 0인 경우 가져오기 신작제외(4207, 4208, 4209)
// select * from comics where cm_no in (3450,3888,4146) -> 뒤 화가 추가됨
/*
$comics_arr = array();
$sql_ce  = "select * from comics_episode_1 where content_no = 0 order by ce_comics asc, ce_chapter asc";
$result_ce = // sql_query($sql_ce);
while($row_ce = sql_fetch_array($result_ce)) {
	$sql_content = "select * from m_mangazzang_cp.content where content_title = '".$row_ce['ce_comics']."' AND content_num = '".$row_ce['ce_chapter']."'";
	$row_content = sql_fetch($sql_content);
	if($row_content['content_no'] == ''){
		echo $sql_content."<br/>";
		array_push($comics_arr, $row_ce['ce_comics']);
		// cs_error_log(NM_ADM_PATH."/_ready/log/".NM_TIME_YMDHIS."._transfer_log", "5.episode 정보(엠짱) : ".$sql_content, $_SERVER['PHP_SELF']);
	}else{
		echo $row_content['content_no']."<br/>";
		$sql_ce_update_content_no = "UPDATE comics_episode_1 set content_no = ".$row_content['content_no']."  where ce_no = '".$row_ce['ce_no']."'";
		// sql_query($sql_ce_update_content_no);
	}
}
echo "select * from comics where cm_no in (".implode($comics_arr, ',').") <br/>";
*/

// ///////////////////////////////// 6.buy_member(엠짱)

$er_mb = $er_cm = $er_ce = array();
// 비 매칭되는 회원 체크 // 없음
echo " ///////////////////////////////// 회원 확인 "."<br/>";
$sql_buy_member = "select * from m_mangazzang_cp.buy_member where buy_date >= '2017-03-30' group by buy_id order by buy_date, buy_id, buy_filenum, buy_content_num";
$result_buy_member = // sql_query($sql_buy_member);
while($row_buy_member = sql_fetch_array($result_buy_member)) {
	$sql_mb = "select * from member where mb_id = '".$row_buy_member['buy_id']."'";
	$row_mb = sql_fetch($sql_mb);
	if($row_mb['mb_id'] == ''){
		// echo $sql_mb."<br/>";
		array_push($er_mb, "'".$row_buy_member['buy_id']."'");
		// cs_error_log(NM_ADM_PATH."/_ready/log/".NM_TIME_YMDHIS."._transfer_log", "6.buy_member(엠짱) 비 매칭되는 회원 체크 : ".$sql_mb, $_SERVER['PHP_SELF']);
	}else{
		// echo $row_mb['mb_id']."<br/>";
	}
}

// 비 매칭되는 코믹스 체크
echo " ///////////////////////////////// 코믹스 확인 "."<br/>";
$sql_buy_member = "select * from m_mangazzang_cp.buy_member where buy_date >= '2017-03-30' group by buy_filenum order by buy_date, buy_id, buy_filenum, buy_content_num";
$result_buy_member = // sql_query($sql_buy_member);
while($row_buy_member = sql_fetch_array($result_buy_member)) {
	$sql_cm = "select * from comics where filenum = '".$row_buy_member['buy_filenum']."'";
	$row_cm = sql_fetch($sql_cm);
	if($row_cm['cm_no'] == ''){
		// echo $sql_cm."<br/>";
		array_push($er_cm, $row_buy_member['buy_filenum']);
		// cs_error_log(NM_ADM_PATH."/_ready/log/".NM_TIME_YMDHIS."._transfer_log", "6.buy_member(엠짱) 비 매칭되는 코믹스 체크 : ".$sql_cm, $_SERVER['PHP_SELF']);
	}else{
		// echo $row_cm['cm_no']."<br/>";
	}
}

// 비 매칭되는 에피소드 체크
echo " ///////////////////////////////// 에피소드 확인 "."<br/>";
$sql_buy_member = "select * from m_mangazzang_cp.buy_member where buy_date >= '2017-03-30' group by buy_content_num order by buy_date, buy_id, buy_filenum, buy_content_num";
$result_buy_member = // sql_query($sql_buy_member);
while($row_buy_member = sql_fetch_array($result_buy_member)) {
	$sql_ce = "select * from comics_episode_1 where content_no = '".$row_buy_member['buy_content_num']."'";
	$row_ce = sql_fetch($sql_ce);
	if($row_ce['ce_no'] == ''){
		// echo $sql_ce."<br/>";
		array_push($er_ce, $row_buy_member['buy_content_num']);
		// cs_error_log(NM_ADM_PATH."/_ready/log/".NM_TIME_YMDHIS."._transfer_log", "6.buy_member(엠짱) 비 매칭되는 에피소드 체크 : ".$sql_ce, $_SERVER['PHP_SELF']);
	}else{
		// echo $row_ce['ce_no']."<br/>";
	}
}

// 비 매칭 되는 데이터 제외
$sql_buy_id = $sql_buy_filenum = $sql_buy_content_num = "";
if(count($er_mb) > 0){ $sql_buy_id			= " AND buy_id not in (".implode($er_mb, ',').") "; }
if(count($er_cm) > 0){ $sql_buy_filenum		= " AND buy_filenum not in (".implode($er_cm, ',').") "; }
if(count($er_ce) > 0){ $sql_buy_content_num = " AND buy_content_num not in (".implode($er_ce, ',').") "; }

$sql_buy_member =	"select * from m_mangazzang_cp.buy_member where 1
					".$sql_buy_id.$sql_buy_filenum.$sql_buy_content_num;

if($sql_buy_id != '' || $sql_buy_filenum != '' || $sql_buy_content_num != ''){
	// cs_error_log(NM_ADM_PATH."/_ready/log/".NM_TIME_YMDHIS."._transfer_log", "비 매칭 되는 데이터 제외 : ".$sql_buy_member, $_SERVER['PHP_SELF']);
}

echo $sql_buy_member."<br/>";

// member_buy_comics 데이터 넣기
$sql_mbc_list = array('mbc_member', 'mbc_member_idx');
array_push($sql_mbc_list, 'mbc_comics', 'mbc_big');
array_push($sql_mbc_list, 'mbc_view', 'mbc_recent_chapter', 'mbc_own_type');
array_push($sql_mbc_list, 'mbc_date'); // 저장일 
array_push($sql_mbc_list, 'ready_date'); // 저장일 

$sql_buy_member_group_buy_filenum_buy_id = $sql_buy_member." GROUP BY buy_filenum, buy_id";
$result_buy_member_gbfbi = // sql_query($sql_buy_member_group_buy_filenum_buy_id);
while($row_cp = sql_fetch_array($result_buy_member_gbfbi)) {



	// 회원 정보
	$mb_get = mb_get($row_cp['buy_id']);

	// 코믹스 정보
	$sql_cm = "select * from comics where filenum = '".$row_cp['buy_filenum']."'";
	$row_cm = sql_fetch($sql_cm);

	$mb_get = mb_get($row_cp['buy_id']);
	$row_cp['mbc_member'] = $mb_get['mb_no'];
	$row_cp['mbc_member_idx'] = $mb_get['mb_idx'];

	$row_cp['mbc_comics'] = $row_cm['cm_no'];
	$row_cp['mbc_big'] = 1;

	$row_cp['mbc_view'] = 'y';
	$row_cp['mbc_recent_chapter'] = 1;
	$row_cp['mbc_own_type'] = 2;

	$row_cp['mbc_date'] = NM_TIME_YMDHIS; // 저장일
	$row_cp['ready_date'] = NM_TIME_YMDHIS; // 저장일
	
	$sql_member_buy_comics_check = "select count(*) as buy_comics_total from member_buy_comics where 
									mbc_member = '".$row_cp['mbc_member']."' 
									AND mbc_member_idx = '".$row_cp['mbc_member_idx']."' 
									AND mbc_comics = '".$row_cp['mbc_comics']."' 
									";
	$buy_comics_total = sql_count($sql_member_buy_comics_check, 'buy_comics_total');
	if($buy_comics_total > 0){ continue; }

	$sql2_insert = "INSERT INTO `member_buy_comics` ( ";
	foreach($sql_mbc_list as $sql_cp_key => $sql_cp_val){
		// SQL문 필드문구
		$sql2_insert.= $sql_cp_val.', ';
	}
	
	$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

	// SQL문 VALUES 시작
	$sql2_insert.= ' )VALUES( ';

	foreach($sql_mbc_list as $sql_cp_key => $sql_cp_val){
		// SQL문 값문구
		if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
			$sql2_insert.= 'NULL, ';
		}else{
			$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
		}
	}
	$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
	// SQL문 마무리
	$sql2_insert.= ' ); ';
	// sql_query($sql2_insert);
	echo $sql2_insert."<br/>";
}

// delete from member_buy_comics where ready_date !=''
/*
select * from member_buy_comics 
where mbc_member ='20202' 
AND mbc_member_idx ='i'  
AND mbc_comics ='3754'  
*/


// member_buy_episode_1 데이터 넣기
$sql_cp_list = array('mbe_member', 'mbe_member_idx');
array_push($sql_cp_list, 'mbe_comics', 'mbe_episode', 'mbe_order');
array_push($sql_cp_list, 'mbe_chapter', 'mbe_way', 'mbe_own_type');
array_push($sql_cp_list, 'mbe_end_date');
array_push($sql_cp_list, 'mbe_date'); // 저장일
array_push($sql_cp_list, 'ready_date'); // 저장일 

$result_buy_member = // sql_query($sql_buy_member);
while($row_cp = sql_fetch_array($result_buy_member)) {
	// 회원 정보
	$mb_get = mb_get($row_cp['buy_id']);

	// 코믹스 정보
	$sql_cm = "select * from comics where filenum = '".$row_cp['buy_filenum']."'";
	$row_cm = sql_fetch($sql_cm);
	
	// 에피소드 정보	
	$sql_ce = "select * from comics_episode_1 where content_no = '".$row_cp['buy_content_num']."'";
	$row_ce = sql_fetch($sql_ce);

	$mb_get = mb_get($row_cp['buy_id']);
	$row_cp['mbe_member'] = $mb_get['mb_no'];
	$row_cp['mbe_member_idx'] = $mb_get['mb_idx'];

	$row_cp['mbe_comics'] = $row_cm['cm_no'];
	$row_cp['mbe_episode'] = $row_ce['ce_no'];
	$row_cp['mbe_order'] = $row_ce['ce_order'];
	
	$row_cp['mbe_chapter'] = $row_ce['ce_chapter'];
	$row_cp['mbe_way'] = $row_cp['buy_since'];
	$row_cp['mbe_own_type'] = 2;
	$row_cp['mbe_end_date'] = $row_cp['buy_end_date'];
		
	$db_mbe_date = '';
	if($row_cp['buy_date'] != ""){ 
		$db_mbe_date = $row_cp['buy_date']." 00:00:00";
	}
	$row_cp['mbe_date'] = $db_mbe_date;
	$row_cp['ready_date'] = NM_TIME_YMDHIS; // 저장일

	
	
	$sql_member_buy_episode_check = "select count(*) as buy_episode_total from member_buy_episode_1 where 
									mbe_member = '".$row_cp['mbe_member']."' 
									AND mbe_member_idx = '".$row_cp['mbe_member_idx']."' 
									AND mbe_comics = '".$row_cp['mbe_comics']."' 
									AND mbe_episode = '".$row_cp['mbe_episode']."' 									
									";
	$buy_episode_total = sql_count($sql_member_buy_episode_check, 'buy_episode_total');
	if($buy_episode_total > 0){ continue; }


	$sql2_insert = "INSERT INTO `member_buy_episode_1` ( ";
	foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
		// SQL문 필드문구
		$sql2_insert.= $sql_cp_val.', ';
	}
	
	$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

	// SQL문 VALUES 시작
	$sql2_insert.= ' )VALUES( ';

	foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
		// SQL문 값문구
		if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
			$sql2_insert.= 'NULL, ';
		}else{
			$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
		}
	}
	$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
	// SQL문 마무리
	$sql2_insert.= ' ); ';
	// sql_query($sql2_insert);
	echo $sql2_insert."<br/>";		
}
// delete from member_buy_episode_1 where ready_date !=''
/*
select * from member_buy_episode_1 
where mbe_member ='9351' 
AND mbe_member_idx ='j'  
AND mbe_comics ='4078'  
AND mbe_episode ='17503' 
*/

die;
die;
// 7.휴면회원데이터 가져오기(덮어쓰기) 다른 파일에서...

?>