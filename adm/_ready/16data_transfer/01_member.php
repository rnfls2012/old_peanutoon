<?
include_once '_common.php'; // 공통

/*

임시 DB로 테스트 5월1일 m_mangazzang_cp
임시 DB로 테스트 5월1일 megacomicsDB

1.충전회원 뺴고 관리자 ID 삭제
2.데이터 리셋 - 즐겨찾기, 대여정보 삭제
3.member 회원정보(엠짱)
4.comics 정보(엠짱)
5.episode 정보(엠짱)
6.buy_member(엠짱)
7.휴면회원데이터 가져오기(덮어쓰기)

select * from member where member_join_date >= '2017-03-31' OR member_login_date >= '2017-03-31'

35879 까지 번호 동일
*/

// ///////////////////////////////// 1.충전회원 뺴고 관리자 ID 삭제
include_once NM_ADM_PATH.'/_ready/15check_update/07_admin_member_del_data.php'; // 관리자 및 관리자 관련 ID 삭제

// ///////////////////////////////// 2.데이터 리셋
$sql_del = "DELETE FROM `member_point_income` WHERE  `mpi_no`=67;"; // sql_query($sql_del); // 테스트 충전
$sql_del = "DELETE FROM `member_point_income` WHERE  `mpi_no`=66;"; // sql_query($sql_del); // 테스트 충전
$sql_del = "DELETE FROM `member_point_income` WHERE  `mpi_no`=65;"; // sql_query($sql_del); // 테스트 충전
$sql_del = "DELETE FROM `member_point_income` WHERE  `mpi_no`=64;"; // sql_query($sql_del); // 테스트 충전
$sql_del = "DELETE FROM `member_point_income` WHERE  `mpi_no`=63;"; // sql_query($sql_del); // 테스트 충전
$sql_del = "DELETE FROM `member_point_income` WHERE  `mpi_no`=44;"; // sql_query($sql_del); // 테스트 충전

$sql_del = "DELETE FROM pg_certify_kcp WHERE kcp_member = '0'"; // sql_query($sql_del); // 인증


$reset_tb = array();
array_push($reset_tb, 'board_report'); // 제보게시판(어떻게 쓸지 보류중...)
array_push($reset_tb, 'event_coupon', 'event_couponment'); // 쿠폰
array_push($reset_tb, 'member_comics_mark'); // 회원 즐겨찾기
array_push($reset_tb, 'member_human'); // 회원 휴면
array_push($reset_tb, 'pg_channel_payco_nonbank'); // payco 무통장
array_push($reset_tb, 'sales', 'sales_age_sex'); // 판매통계
array_push($reset_tb, 'sales_episode_1', 'sales_episode_1_age_sex'); // 판매통계
array_push($reset_tb, 'sales_episode_2', 'sales_episode_2_age_sex'); // 판매통계
array_push($reset_tb, 'sales_episode_3', 'sales_episode_3_age_sex'); // 판매통계
array_push($reset_tb, 'sales_recharge'); // 충전통계
array_push($reset_tb, 'stats_log_member'); // 로그
foreach($reset_tb as $table_key => $table_val) {
	$sql_reset = "TRUNCATE ".$table_val;
	// sql_query($sql_reset);
}

// 대여정보 삭제
$sql_del = "DELETE FROM member_buy_comics WHERE mbc_own_type = '2' "; // sql_query($sql_del); // 인증
$sql_del = "DELETE FROM member_buy_episode_1 WHERE mbe_own_type = '2' "; // sql_query($sql_del); // 인증
$sql_del = "DELETE FROM member_buy_episode_2 WHERE mbe_own_type = '2' "; // sql_query($sql_del); // 인증
$sql_del = "DELETE FROM member_buy_episode_3 WHERE mbe_own_type = '2' "; // sql_query($sql_del); // 인증

// ///////////////////////////////// 3.member 회원정보(엠짱)
// 엠짱 번호 넣는 필드 추가(임시)
$sql_mb_alter = "ALTER TABLE `member` ADD COLUMN `member_num` INT(11) NOT NULL DEFAULT '0' COMMENT '엠짱번호(이관용)' AFTER `mb_push`; ";
// sql_query($sql_mb_alter); // 엠짱번호(이관용)

// 35879 까지 번호 동일까지는 동일하지만 위 필드번호로 이용하여 업데이트 0번호 코믹스 유일 회원
$mb_new = array();
$mb_base = array();

$sql_mb_update_member_num = "UPDATE member set member_num = mb_no  where mb_no <= 35879 ";
// sql_query($sql_mb_update_member_num);

$sql_mb_m_mangazzang = "select * from m_mangazzang_cp.member where member_num > 35879"; // 이상 회원만 체크
$result_mb_m_mangazzang = // sql_query($sql_mb_m_mangazzang);
while($row_mb_m_mangazzang = sql_fetch_array($result_mb_m_mangazzang)) {
	$sql_mb_check = "select * from member where mb_id ='".$row_mb_m_mangazzang['member_id']."' 
	                 and mb_idx = '".mb_get_idx($row_mb_m_mangazzang['member_id'])."' ";
	$row_mb_check = sql_fetch($sql_mb_check);
	if($row_mb_check['mb_id'] == ""){
		array_push($mb_new, $row_mb_m_mangazzang);
	}else{
		array_push($mb_base, $row_mb_m_mangazzang);
		echo $row_mb_check['mb_id']."<br/>";
		cs_error_log(NM_ADM_PATH."/_ready/log/".NM_TIME_YMDHIS."._transfer_log", "기존회원 : ".$row_mb_check['mb_id'], $_SERVER['PHP_SELF']);
	}
} // end while

if(count($mb_base)> 0){echo "기존회원이 존재 합니다."; die;}
// 엠짱 새로운 회원 추가
$sql_insert_mb_new = array('mb_no', 'mb_id', 'mb_idx', 'mb_pass', 'mb_name', 'mb_level', 'mb_class');
array_push($sql_insert_mb_new, 'mb_email', 'mb_phone');
array_push($sql_insert_mb_new, 'mb_state');
array_push($sql_insert_mb_new, 'mb_cash_point', 'mb_point');
array_push($sql_insert_mb_new, 'mb_sex', 'mb_post', 'mb_adult', 'mb_ipin', 'mb_overlap');
array_push($sql_insert_mb_new, 'mb_won', 'mb_won_count', 'mb_birth');
array_push($sql_insert_mb_new, 'mb_join_date', 'mb_out_date', 'mb_login_date', 'mb_human_date', 'mb_human_email_date');
array_push($sql_insert_mb_new, 'member_num');
foreach($mb_new as $mb_new_key => $mb_new_val){	
	// $mb_new_val['mb_no'] = $mb_new_val['member_num'];
	$mb_new_val['mb_id'] = $mb_new_val['member_id'];
	$mb_new_val['mb_idx'] = $mb_new_val['member_id_idx'];
	$mb_new_val['mb_pass'] = $mb_new_val['member_pass'];
	$mb_new_val['mb_name'] = $mb_new_val['member_name'];
	$mb_new_val['mb_level'] = 2;
	$mb_new_val['mb_class'] = 'c';
	$mb_new_val['mb_email'] = $mb_new_val['member_email'];
	$mb_new_val['mb_phone'] = $mb_new_val['member_phone'].$mb_new_val['member_phone2'].$mb_new_val['member_phone3'];$db_mb_state = 
	$db_mb_state = 'y';
	if($mb_new_val['member_out_date'] != ""){
		$db_mb_state = 'n';
	}
	$mb_new_val['mb_state'] = $db_mb_state;

	$mb_new_val['mb_cash_point'] = ceil($mb_new_val['member_cash_point']/100);
	$mb_new_val['mb_point'] = ceil($mb_new_val['member_point']/100);

	$db_mb_sex = 'n';
	if($mb_new_val['member_sex'] == "M" && $mb_new_val['member_birth'] != ""){ 
		$db_mb_sex = 'm'; 
	}else if($mb_new_val['member_sex'] == "Y" && $mb_new_val['member_birth'] != ""){ 
		$db_mb_sex = 'w';
	}
	$mb_new_val['mb_sex'] = $db_mb_sex;

	$db_mb_post = 'n';
	if($mb_new_val['member_out_date'] == "Y"){
		$db_mb_post = 'y';
	}
	$mb_new_val['mb_post'] = $db_mb_post;

	$db_mb_adult = 'n';
	if($mb_new_val['member_adult'] == "1" && $mb_new_val['mb_sex'] != "n" || $mb_new_val['mb_level'] > 9){
		$db_mb_adult = 'y';
	}
	$mb_new_val['mb_adult'] = $db_mb_adult;

	$mb_new_val['mb_ipin'] = $mb_new_val['member_info'];
	$mb_new_val['mb_overlap'] = $mb_new_val['member_overlap_key'];

	$mb_new_val['mb_won'] = $mb_new_val['member_cash'];
	$mb_new_val['mb_won_count'] = $mb_new_val['member_cash_num'];
	$mb_new_val['mb_birth'] = $mb_new_val['member_birth'];

	$db_mb_join_date = '';
	if($mb_new_val['member_join_date'] != ""){
		$db_mb_join_date = $mb_new_val['member_join_date']." 00:00:00";
	}
	$mb_new_val['mb_join_date'] = $db_mb_join_date ;
	

	$db_mb_out_date = '';
	if($mb_new_val['member_out_date'] != ""){
		$db_mb_out_date = $mb_new_val['member_out_date']." 00:00:00";
	}
	$mb_new_val['mb_out_date'] = $db_mb_out_date ;

	$mb_new_val['mb_login_date'] = $mb_new_val['member_login_date'];
	$mb_new_val['mb_human_date'] = $mb_new_val['human_date'];
	$mb_new_val['mb_human_email_date'] = $mb_new_val['human_email_date'];

	
	$sql_insert_mb = "INSERT INTO `member` ( ";
	foreach($sql_insert_mb_new as $sql_cp_key => $sql_cp_val){
		// SQL문 필드문구 
		$sql_insert_mb.= $sql_cp_val.', ';
	}
	
	$sql_insert_mb = substr($sql_insert_mb,0,strrpos($sql_insert_mb, ","));

	// SQL문 VALUES 시작 
	$sql_insert_mb.= ' )VALUES( ';

	foreach($sql_insert_mb_new as $sql_cp_key => $sql_cp_val){
		// SQL문 값문구
		if($sql_cp_val == "mb_email" && $mb_new_val['mb_email'] == NULL){
			$sql_insert_mb.= 'NULL, ';
		}else{
			$sql_insert_mb.= '"'.$mb_new_val[$sql_cp_val].'", ';
		}
	}
	$sql_insert_mb = substr($sql_insert_mb,0,strrpos($sql_insert_mb, ","));
	// SQL문 마무리
	$sql_insert_mb.= ' ); ';
	// sql_query($sql_insert_mb);
	echo $sql_insert_mb."<br/>";
}

// $mb_new 넣었음 이따가 할땐 주석 풀어야 함

//캐시포인트, 포인트 업데이트

$sql_mb_m_mangazzang = "select * from m_mangazzang_cp.member where 1"; // 이상 회원만 체크
$result_mb_m_mangazzang = // sql_query($sql_mb_m_mangazzang);
while($row_mb_m_mangazzang = sql_fetch_array($result_mb_m_mangazzang)) {
	$mb_cash_point	= ceil($row_mb_m_mangazzang['member_cash_point']/100);
	$mb_point		= ceil($row_mb_m_mangazzang['member_point']/100);
	
	$sql_mb_update = "  UPDATE member set 
						mb_cash_point = '".$mb_cash_point."', 
						mb_point = '".$mb_point."' 
						where member_num = '".$row_mb_m_mangazzang['member_num']."' 
						  AND member_num!='0' ";
	// sql_query($sql_mb_update);
} // end while

// 3명에 대해서는 cash TB로 확인후 관리자가 직접 주는 걸로...

// ///////////////////////////////// 4.comics 정보(엠짱)
// comics 4162 이후 틀림
$sql_cm_alter = "ALTER TABLE `comics` ADD COLUMN `filenum` INT(11) NOT NULL DEFAULT '0' COMMENT '엠짱번호(이관용)' AFTER `cm_episode_date`;";
// sql_query($sql_cm_alter); // 엠짱번호(이관용)

$sql_cm_update_filenum = "UPDATE comics set filenum = cm_no  where cm_no <= 4162 ";
// sql_query($sql_cm_update_filenum);

// 체크한바 4개의 코믹스만 있음 바로 커리문으로 적용
// 4166 -누락 [페어리노블]이세계에서 온 늑대 2  -->> music2690 고객이 산거 체크
// 4165 -4180 아야야♥ 너무해요 주인님 (메이드 아야 완전판)
// 4164 -4178 벗으면 뿅가리 69번지 (불끈남 완전판)
// 4163 -4179 사쿠라 세 자매, 맛있게 드세요 (맛보러 와요 완전판)
$sql_cm_update_filenum = "UPDATE comics set filenum = '4165'  where cm_no = 4180 "; // sql_query($sql_cm_update_filenum);
$sql_cm_update_filenum = "UPDATE comics set filenum = '4164'  where cm_no = 4178 "; // sql_query($sql_cm_update_filenum);
$sql_cm_update_filenum = "UPDATE comics set filenum = '4163'  where cm_no = 4179 "; // sql_query($sql_cm_update_filenum);

// ///////////////////////////////// 5.episode 정보(엠짱)
// episode 18183 이후 틀림
$sql_ce_alter = "ALTER TABLE `comics_episode_1` ADD COLUMN `content_no` INT(11) NOT NULL DEFAULT '0' COMMENT '엠짱번호(이관용)' AFTER `ce_mod_date`;";
// sql_query($sql_ce_alter); // 엠짱번호(이관용)

$sql_ce_update_content_no = "UPDATE comics_episode_1 set content_no = ce_no  where ce_no <= 18183 ";
// sql_query($sql_ce_update_content_no);

// content_no 0인 경우 가져오기
$sql_ce  = "select * from comics_episode_1 where content_no = 0 order by ce_comics asc, ce_chapter asc";
$result_ce = // sql_query($sql_ce);
while($row_ce = sql_fetch_array($result_ce)) {
	$sql_content = "select * from m_mangazzang_cp.content where content_title = '".$row_ce['ce_comics']."' AND content_num = '".$row_ce['ce_chapter']."'";
	$row_content = sql_fetch($sql_content);
	if($row_content['content_no'] == ''){
		// echo $sql_content."<br/>";
		cs_error_log(NM_ADM_PATH."/_ready/log/".NM_TIME_YMDHIS."._transfer_log", "5.episode 정보(엠짱) : ".$sql_content, $_SERVER['PHP_SELF']);
	}else{
		// echo $row_content['content_no']."<br/>";
		$sql_ce_update_content_no = "UPDATE comics_episode_1 set content_no = ".$row_content['content_no']."  where ce_no = '".$row_ce['ce_no']."'";
		// sql_query($sql_ce_update_content_no);
	}
}

// ///////////////////////////////// 6.buy_member(엠짱)
// 대여 삭제
$sql_del_buy_memeber = "DELETE FROM m_mangazzang_cp.buy_member where buy_end_date != '3000-01-01' OR  buy_id = '' ";
// sql_query($sql_del_buy_memeber);

$er_mb = $er_cm = $er_ce = array();
// 비 매칭되는 회원 체크
$sql_buy_member = "select * from m_mangazzang_cp.buy_member where buy_date >= '2017-03-30' group by buy_id order by buy_date, buy_id, buy_filenum, buy_content_num";
$result_buy_member = // sql_query($sql_buy_member);
while($row_buy_member = sql_fetch_array($result_buy_member)) {
	$sql_mb = "select * from member where mb_id = '".$row_buy_member['buy_id']."'";
	$row_mb = sql_fetch($sql_mb);
	if($row_mb['mb_id'] == ''){
		// echo $sql_mb."<br/>";
		array_push($er_mb, $row_buy_member['buy_id']);
		cs_error_log(NM_ADM_PATH."/_ready/log/".NM_TIME_YMDHIS."._transfer_log", "6.buy_member(엠짱) 비 매칭되는 회원 체크 : ".$sql_mb, $_SERVER['PHP_SELF']);
	}else{
		// echo $row_mb['mb_id']."<br/>";
	}
}

// 비 매칭되는 코믹스 체크
$sql_buy_member = "select * from m_mangazzang_cp.buy_member where buy_date >= '2017-03-30' group by buy_filenum order by buy_date, buy_id, buy_filenum, buy_content_num";
$result_buy_member = // sql_query($sql_buy_member);
while($row_buy_member = sql_fetch_array($result_buy_member)) {
	$sql_cm = "select * from comics where filenum = '".$row_buy_member['buy_filenum']."'";
	$row_cm = sql_fetch($sql_cm);
	if($row_cm['cm_no'] == ''){
		// echo $sql_cm."<br/>";
		array_push($er_cm, $row_buy_member['buy_filenum']);
		cs_error_log(NM_ADM_PATH."/_ready/log/".NM_TIME_YMDHIS."._transfer_log", "6.buy_member(엠짱) 비 매칭되는 코믹스 체크 : ".$sql_cm, $_SERVER['PHP_SELF']);
	}else{
		// echo $row_cm['cm_no']."<br/>";
	}
}

// 비 매칭되는 에피소드 체크
$sql_buy_member = "select * from m_mangazzang_cp.buy_member where buy_date >= '2017-03-30' group by buy_content_num order by buy_date, buy_id, buy_filenum, buy_content_num";
$result_buy_member = // sql_query($sql_buy_member);
while($row_buy_member = sql_fetch_array($result_buy_member)) {
	$sql_ce = "select * from comics_episode_1 where content_no = '".$row_buy_member['buy_content_num']."'";
	$row_ce = sql_fetch($sql_ce);
	if($row_ce['ce_no'] == ''){
		//echo $sql_ce."<br/>";
		array_push($er_ce, $row_buy_member['buy_content_num']);
		cs_error_log(NM_ADM_PATH."/_ready/log/".NM_TIME_YMDHIS."._transfer_log", "6.buy_member(엠짱) 비 매칭되는 에피소드 체크 : ".$sql_ce, $_SERVER['PHP_SELF']);
	}else{
		// echo $row_ce['ce_no']."<br/>";
	}
}

// 비 매칭 되는 데이터 제외
$sql_buy_id = $sql_buy_filenum = $sql_buy_content_num = "";
if(count($er_mb) > 0){ $sql_buy_id			= " AND buy_id not in (".implode($er_mb, ',').") "; }
if(count($er_cm) > 0){ $sql_buy_filenum		= " AND buy_filenum not in (".implode($er_cm, ',').") "; }
if(count($er_ce) > 0){ $sql_buy_content_num = " AND buy_content_num not in (".implode($er_ce, ',').") "; }

$sql_buy_member =	"select * from m_mangazzang_cp.buy_member where buy_date >= '2017-03-30'
					".$sql_buy_id.$sql_buy_filenum.$sql_buy_content_num;

if($sql_buy_id != '' || $sql_buy_filenum != '' || $sql_buy_content_num != ''){
	cs_error_log(NM_ADM_PATH."/_ready/log/".NM_TIME_YMDHIS."._transfer_log", "비 매칭 되는 데이터 제외 : ".$sql_buy_member, $_SERVER['PHP_SELF']);
}

// member_buy_comics 데이터 넣기
$sql_mbc_list = array('mbc_member', 'mbc_member_idx');
array_push($sql_mbc_list, 'mbc_comics', 'mbc_big');
array_push($sql_mbc_list, 'mbc_view', 'mbc_recent_chapter', 'mbc_own_type');
array_push($sql_mbc_list, 'mbc_date'); /* 저장일 */

$sql_buy_member_group_buy_filenum_buy_id = $sql_buy_member." GROUP BY buy_filenum, buy_id";
$result_buy_member_gbfbi = // sql_query($sql_buy_member_group_buy_filenum_buy_id);
while($row_cp = sql_fetch_array($result_buy_member_gbfbi)) {
	// 회원 정보
	$mb_get = mb_get($row_cp['buy_id']);

	// 코믹스 정보
	$sql_cm = "select * from comics where filenum = '".$row_cp['buy_filenum']."'";
	$row_cm = sql_fetch($sql_cm);

	$mb_get = mb_get($row_cp['buy_id']);
	$row_cp['mbc_member'] = $mb_get['mb_no'];
	$row_cp['mbc_member_idx'] = $mb_get['mb_idx'];

	$row_cp['mbc_comics'] = $row_cm['cm_no'];
	$row_cp['mbc_big'] = 1;

	$row_cp['mbc_view'] = 'y';
	$row_cp['mbc_recent_chapter'] = 1;
	$row_cp['mbc_own_type'] = 2;

	$row_cp['mbc_date'] = NM_TIME_YMDHIS; // 저장일

	$sql2_insert = "INSERT INTO `member_buy_comics` ( ";
	foreach($sql_mbc_list as $sql_cp_key => $sql_cp_val){
		/* SQL문 필드문구 */
		$sql2_insert.= $sql_cp_val.', ';
	}
	
	$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

	/* SQL문 VALUES 시작 */
	$sql2_insert.= ' )VALUES( ';

	foreach($sql_mbc_list as $sql_cp_key => $sql_cp_val){
		/* SQL문 값문구 */
		if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
			$sql2_insert.= 'NULL, ';
		}else{
			$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
		}
	}
	$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
	/* SQL문 마무리 */
	$sql2_insert.= ' ); ';
	// sql_query($sql2_insert);
	echo $sql2_insert."<br/>";
}


// member_buy_episode_1 데이터 넣기
$sql_cp_list = array('mbe_member', 'mbe_member_idx');
array_push($sql_cp_list, 'mbe_comics', 'mbe_episode', 'mbe_order');
array_push($sql_cp_list, 'mbe_chapter', 'mbe_way', 'mbe_own_type');
array_push($sql_cp_list, 'mbe_end_date');
array_push($sql_cp_list, 'mbe_date'); /* 저장일 */

$result_buy_member = // sql_query($sql_buy_member);
while($row_cp = sql_fetch_array($result_buy_member)) {
	// 회원 정보
	$mb_get = mb_get($row_cp['buy_id']);

	// 코믹스 정보
	$sql_cm = "select * from comics where filenum = '".$row_cp['buy_filenum']."'";
	$row_cm = sql_fetch($sql_cm);
	
	// 에피소드 정보	
	$sql_ce = "select * from comics_episode_1 where content_no = '".$row_cp['buy_content_num']."'";
	$row_ce = sql_fetch($sql_ce);

	$mb_get = mb_get($row_cp['buy_id']);
	$row_cp['mbe_member'] = $mb_get['mb_no'];
	$row_cp['mbe_member_idx'] = $mb_get['mb_idx'];

	$row_cp['mbe_comics'] = $row_cm['cm_no'];
	$row_cp['mbe_episode'] = $row_ce['ce_no'];
	$row_cp['mbe_order'] = $row_ce['ce_order'];
	
	$row_cp['mbe_chapter'] = $row_ce['ce_chapter'];
	$row_cp['mbe_way'] = $row_cp['buy_since'];
	$row_cp['mbe_own_type'] = 2;
	$row_cp['mbe_end_date'] = $row_cp['buy_end_date'];
		
	$db_mbe_date = '';
	if($row_cp['buy_date'] != ""){ 
		$db_mbe_date = $row_cp['buy_date']." 00:00:00";
	}
	$row_cp['mbe_date'] = $db_mbe_date;

	$sql2_insert = "INSERT INTO `member_buy_episode_1` ( ";
	foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
		/* SQL문 필드문구 */
		$sql2_insert.= $sql_cp_val.', ';
	}
	
	$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));

	/* SQL문 VALUES 시작 */
	$sql2_insert.= ' )VALUES( ';

	foreach($sql_cp_list as $sql_cp_key => $sql_cp_val){
		/* SQL문 값문구 */
		if($sql_cp_val == "mb_email" && $row_cp['mb_email'] == NULL){
			$sql2_insert.= 'NULL, ';
		}else{
			$sql2_insert.= '"'.$row_cp[$sql_cp_val].'", ';
		}
	}
	$sql2_insert = substr($sql2_insert,0,strrpos($sql2_insert, ","));
	/* SQL문 마무리 */
	$sql2_insert.= ' ); ';
	// sql_query($sql2_insert);
	echo $sql2_insert."<br/>";		
}

// 7.휴면회원데이터 가져오기(덮어쓰기) 다른 파일에서...

?>