<? include_once '_common.php'; // 공통 

die;

/* AWS 연결 */
$connect = mysql_connect("peanutoon-db.cv8kfy8vdqde.ap-northeast-1.rds.amazonaws.com", "peanutoon", "vlsjxns6619") or die('MySQL Connect Error!!!');
mysql_select_db("peanutoon", $connect) or die('MySQL DB Error!!!');
mysql_query("set session character_set_connection=utf8;");
mysql_query("set session character_set_results=utf8;");
mysql_query("set session character_set_client=utf8;");
mysql_query("set names utf8");

// 땅콩 충전 우선 저장
// $t_pt_sql = " select * from t_payment pt left JOIN t_cash_product cpt ON pt.product_num = cpt.product_num where payment_success='y' order by pt.payment_num asc ";

$t_pt_sql = "	select *, cpt.cash as cpt_cash, cpt.point as cpt_point, cpt.price as cpt_price from 
				t_payment pt 
				left JOIN t_cash_product cpt ON pt.product_num = cpt.product_num 
				left JOIN t_customer ct ON pt.customer_num = ct.customer_num 
				where payment_success='y' order by pt.payment_num asc
";
$t_pt_result = sql_query($t_pt_sql);

// AWS table Collaboration
include NM_PATH.'/config/dbconnect.php'; // DB연결

$db_name = "peanutoonDB";

// 데이터 삭제
$year_arr = array(2015, 2016, 2017);
foreach($year_arr as $year_key => $year_val){
	$sql_sr_year_db_del		= "delete from ".$db_name."_".$year_val.".sales_recharge "; 
	$sql_mpi_year_db_del	= "delete from ".$db_name."_".$year_val.".member_point_income "; 
	$sql_mpu_year_db_del	= "delete from ".$db_name."_".$year_val.".member_point_used "; 
	sql_query($sql_sr_year_db_del);
	sql_query($sql_mpi_year_db_del);
	sql_query($sql_mpu_year_db_del);
}


$count = 0;
while ($t_pt_row = sql_fetch_array($t_pt_result)) {
	// 연도별로 다른 DB에 저장
	$sr_year_month		= substr($t_pt_row['payment_date'], 0, 7);
	$sr_year			= substr($t_pt_row['payment_date'], 0, 4);
	$sr_month			= substr($t_pt_row['payment_date'], 5, 2);
	$sr_day				= substr($t_pt_row['payment_date'], 8, 2);
	$sr_hour			= substr($t_pt_row['payment_date'], 11, 2);
	$sr_min				= substr($t_pt_row['payment_date'], 14, 2);
	$sr_week			= date('w', strtotime($t_pt_row['payment_date']));

	$sr_product			= $t_pt_row['product_name'];
	$sr_crp_no			= $t_pt_row['product_num'];

	$db_t_name = $db_name.'_'.$sr_year;
	
	$sr_way = "";
	// 결제수단(빈칸:에러, mobx:폰, card:카드, acnt:계좌이체, scbl:도서문화상품권, schm:해피머니, sccl:문화상품권, toss:토스, payco:페이코)
	switch($t_pt_row['payment_type_code']){
		case 'hp' :			$sr_way = "mobx";		break;	// mobx:폰
		case 'card' :		$sr_way = "card";		break;	// card:카드
		case 'iche' :		$sr_way = "acnt";		break;	// acnt:계좌이체
		case 'culture' :	$sr_way = "sccl";		break;	// sccl:문화상품권
		case 'happy' :		$sr_way = "schm";		break;	// schm:해피머니
		case 'payco' :		$sr_way = "payco";		break;	// payco:페이코
		case 'toss' :		$sr_way = "toss";		break;	// toss:토스
		case 'book' :		$sr_way = "scbl";		break;	// scbl:도서문화상품권
		case 'direct' :		$sr_way = "direct";		break;	// direct:직접
		default :			$sr_way = "etc";		break;	// etc:기타
	}

	$sr_platform = "";
	// 1:웹, 2:Android, 4:iOS, 8:AppMarket, 16:AppSetApk
	switch($t_pt_row['payment_container']){
		case 'android' :	$sr_platform = "2";		break;	// 2:폰
		case 'mobile' :		$sr_platform = "2";		break;	// 2:폰
		case 'naver' :		$sr_platform = "2";		break;	// 2:폰
		case 'web' :		$sr_platform = "1";		break;	// 1:웹
		default :			$sr_platform = "2";		break;	// 2:폰
	}

	$sr_cash_point		= $t_pt_row['cpt_cash'];
	$sr_point			= $t_pt_row['cpt_point'];
	$sr_pay				= $t_pt_row['cpt_price'];

	$sr_re_pay = '0';
	if($t_pt_row['re_payment'] == "n"){ $sr_re_pay = '1'; }

	$sr_sex = "n";
	// 성별(m:남자, w:여자, n:미인증)
	switch($t_pt_row['gender']){
		case 'm' :		$sr_sex = "m";		break;	// m:남자
		case 'f' :		$sr_sex = "w";		break;	// w:여자
		default :		$sr_sex = "n";		break;	// n:미인증
	}

	$sr_age = "0";
	if($t_pt_row['birthday'] != "0000-00-00"){
		$sr_age = mb_get_age_group ($t_pt_row['birthday']);
	}

	$sr_adult		= $t_pt_row['adult'];
	$sr_date		= $t_pt_row['payment_date'];


	$sql_sr = "INSERT INTO ".$db_t_name.".sales_recharge (

	`sr_year_month`, `sr_year`, `sr_month`, `sr_day`, 
	`sr_hour`, `sr_min`, `sr_week`, 

	`sr_product`, `sr_crp_no`, 
	`sr_way`, `sr_platform`, 

	`sr_cash_point`, `sr_point`, 
	`sr_pay`, `sr_re_pay`, 

	`sr_age`, `sr_sex`, `sr_adult`, `sr_date` 

	) VALUES (
	
	'".$sr_year_month."', '".$sr_year."', '".$sr_month."', '".$sr_day."', 
	'".$sr_hour."', '".$sr_min."', '".$sr_week."', 

	'".$sr_product."', '".$sr_crp_no."',
	'".$sr_way."', '".$sr_platform."', 

	'".$sr_cash_point."', '".$sr_point."', 
	'".$sr_pay."', '".$sr_re_pay."',

	'".$sr_age."', '".$sr_sex."', '".$sr_adult."', '".$sr_date."' 
	)";

	if(sql_query($sql_sr)){
	}else{ die($sql_sr); }	

	$mpi_member			= $t_pt_row['customer_num'];
	$mpi_member_idx		= mb_get_idx($t_pt_row['customer_email']);

	$mpi_won			= $sr_pay;
	$mpi_cash_point		= $sr_cash_point;
	$mpi_point			= $sr_point;

	$mpi_state			= "1";

	$mpi_age			= $sr_age;
	$mpi_sex			= $sr_sex;

	$mpi_kind			= "aws";
	$mpi_user_agent		= "aws";

	$mpi_version		= "aws";

	$mpi_from			= $sr_product;
	$mpi_order			= $t_pt_row['payment_oder_id'];

	$mpi_year_month		= $sr_year_month;
	$mpi_year			= $sr_year;
	$mpi_month			= $sr_month;
	$mpi_day			= $sr_day;
	$mpi_hour			= $sr_hour;
	$mpi_week			= $sr_week;
	$mpi_date			= $sr_date;

	$sql_mpi = "INSERT INTO ".$db_t_name.".member_point_income (
	`mpi_member`, `mpi_member_idx`, 
	`mpi_won`, `mpi_cash_point`, `mpi_point`, 
	`mpi_state`, 

	`mpi_age`, `mpi_sex`, 
	`mpi_kind`, `mpi_user_agent`, `mpi_version`, `mpi_from`, `mpi_order`, 

	`mpi_year_month`, `mpi_year`, `mpi_month`, `mpi_day`, 
	`mpi_hour`, `mpi_week`, `mpi_date` 
	) VALUES (
	'".$mpi_member."', '".$mpi_member_idx."', 
	'".$mpi_won."', '".$mpi_cash_point."', '".$mpi_point."', 
	'".$mpi_state."', 

	'".$mpi_age."', '".$mpi_sex."', 
	'".$mpi_kind."', '".$mpi_user_agent."', '".$mpi_version."', '".$mpi_from."', '".$mpi_order."', 
	
	'".$mpi_year_month."', '".$mpi_year."', '".$mpi_month."', '".$mpi_day."', 
	'".$mpi_hour."', '".$mpi_week."', '".$mpi_date."'
	)";
	
	if(sql_query($sql_mpi)){
	}else{ die($sql_mpi); }	
	
	$mpu_member					= $mpi_member;
	$mpu_member_idx				= $mpi_member_idx;
	$mpu_class					= "r";
	$mpu_recharge_won			= $mpi_won;
	$mpu_recharge_cash_point	= $mpi_cash_point;
	$mpu_recharge_point			= $mpi_point;
	
	$mpu_cash_type				= "2";
	$mpu_age					= $mpi_age;
	$mpu_sex					= $mpi_sex;
	
	$mpu_os						= "aws";
	$mpu_brow					= "aws";
	$mpu_user_agent				= "aws";
	$mpu_version				= "aws";
	$mpu_from					= $mpi_from;
	$mpu_order					= $mpi_order;

	$mpu_year_month				= $mpi_year_month;
	$mpu_year					= $mpi_year;
	$mpu_month					= $mpi_month;
	$mpu_day					= $mpi_day;
	$mpu_hour					= $mpi_hour;
	$mpu_week					= $mpi_week;
	$mpu_date					= $mpi_date;

	$sql_mpu = "INSERT INTO ".$db_t_name.".member_point_used (
	`mpu_member`, `mpu_member_idx`, `mpu_class`,
	`mpu_recharge_won`, `mpu_recharge_cash_point`, `mpu_recharge_point`, 

	`mpu_cash_type`, `mpu_age`, `mpu_sex`, 
	`mpu_os`, `mpu_brow`, `mpu_user_agent`, `mpu_version`, `mpu_from`, `mpu_order`, 

	`mpu_year_month`, `mpu_year`, `mpu_month`, `mpu_day`, 
	`mpu_hour`, `mpu_week`, `mpu_date` 
	) VALUES (
	'".$mpu_member."', '".$mpu_member_idx."', '".$mpu_class."', 
	'".$mpu_recharge_won."', '".$mpu_recharge_cash_point."', '".$mpu_recharge_point."',  

	'".$mpu_cash_type."', '".$mpu_age."', '".$mpu_sex."', 
	'".$mpu_os."','".$mpu_brow."', '".$mpu_user_agent."', '".$mpu_version."', '".$mpu_from."', '".$mpu_order."', 
	
	'".$mpu_year_month."', '".$mpu_year."', '".$mpu_month."', '".$mpu_day."', 
	'".$mpu_hour."', '".$mpu_week."', '".$mpu_date."'
	)";
	
	if(sql_query($sql_mpu)){
	}else{ die($sql_mpu); }	

	// $count++;
	
	// if($count > 10){ die; }
}

?>