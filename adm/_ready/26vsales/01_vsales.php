<? include_once '_common.php'; // 공통 

die;

// 데이터 초기화
$sql_vsales_del					= "delete from vsales"; 
/// sql_query($sql_vsales_del);
$sql_vsales_age_sex_del			= "delete from vsales_age_sex"; 
/// sql_query($sql_vsales_age_sex_del);

for($i=1; $i<=3; $i++){
	$sql_vsales_episode_del			= "delete from vsales_episode_".$i.""; 
	/// sql_query($sql_vsales_episode_del);
	$sql_vsales_episode_age_sex_del = "delete from vsales_episode_".$i."_age_sex"; 
	// sql_query($sql_vsales_episode_age_sex_del);

	$t_sql = " SELECT *	FROM member_point_expen_episode_".$i." WHERE mpee_point = '0'
				ORDER BY mpee_date ";
	$t_result = sql_query($t_sql);
	while ($t_row= sql_fetch_array($t_result)) {
		$comics = array();
		$episode = array();
		$comics_buy = array();
		$nm_member = array();
		$save_date = array();

		$comics['cm_no'] = $t_row['mpee_comics'];
		$comics['cm_big'] = $i;

		$episode['ce_no'] = $t_row['mpee_episode'];
		$episode['ce_chapter'] = $t_row['mpee_chapter'];

		$nm_member['mb_sex'] = $t_row['mpee_sex'];
		$nm_member['mb_birth'] = 2017 - intval($t_row['mpee_age']);
		if($t_row['mpee_sex'] == 'n'){ $nm_member['mb_birth'] = ""; }

		$comics_buy['sl_free'] = 0;
		$comics_buy['sl_sale'] = 0;
		if($t_row['mpee_sale'] == 'y'){ $comics_buy['sl_sale'] = 1; }
		$comics_buy['sl_event'] = 0;
		$comics_buy['mb_cash_point'] = $t_row['mpee_cash_point'];
		$comics_buy['mb_point'] = $t_row['mpee_point'];

		$comics_free = 'n';
		$pay_type = $t_row['mpee_way'];

		$save_date['year_month'] = $t_row['mpee_year_month'];
		$save_date['year'] = $t_row['mpee_year'];
		$save_date['month'] = $t_row['mpee_month'];
		$save_date['day'] = $t_row['mpee_day'];
		$save_date['hour'] = $t_row['mpee_hour'];
		$save_date['week'] = $t_row['mpee_week'];

		/// cs_set_vsales_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date); // CP용 통계
		/// cs_set_vsales_age_sex_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date); // CP용 통계
		/// cs_set_vsales_episode_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date); // CP용 통계
		/// cs_set_vsales_episode_age_sex_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date); // CP용 통계
	}
}

// open 수는 맞춰주기(무료 코믹스 또는 무료 화에 대해서도 처리 해야 함)
/* sales, sales_episode 1,2,3 */

/*
$t2_sql = " SELECT * FROM sales order by sl_no ";
$t2_result = sql_query($t2_sql);
while ($t2_row= sql_fetch_array($t2_result)) {
	// 검색	
	$sql_sales = "
		SELECT  count(*) as total_sales FROM vsales 
		WHERE 1 
		AND sl_comics = '".$t2_row['sl_comics']."' AND sl_big  = '".$t2_row['sl_big']."' 
		AND sl_year_month = '".$t2_row['sl_year_month']."' AND sl_year = '".$t2_row['sl_year']."' 
		AND sl_month = '".$t2_row['sl_month']."' AND sl_day = '".$t2_row['sl_day']."' 
		AND sl_hour = '".$t2_row['sl_hour']."' AND sl_week = '".$t2_row['sl_week']."' ";
	$total_sales = sql_count($sql_sales, 'total_sales');

	if($total_sales!=0){
		// 업데이트
		$sql_sales_update = " UPDATE vsales SET sl_open='".$t2_row['sl_open']."'
			WHERE 1 
			AND sl_comics = '".$t2_row['sl_comics']."' AND sl_big  = '".$t2_row['sl_big']."' 
			AND sl_year_month = '".$t2_row['sl_year_month']."' AND sl_year = '".$t2_row['sl_year']."' 
			AND sl_month = '".$t2_row['sl_month']."' AND sl_day = '".$t2_row['sl_day']."' 
			AND sl_hour = '".$t2_row['sl_hour']."' AND sl_week = '".$t2_row['sl_week']."' ";
		sql_query($sql_sales_update);
	}else{
		// 등록
		if( intval($t2_row['sl_point']) == 0 && intval($t2_row['sl_cash_point']) == 0 ){
			$sql_sales_insert = "
				INSERT INTO vsales (
				sl_open, sl_point, sl_cash_point, sl_cash_type, 
				sl_comics, sl_big, 
				sl_year_month, sl_year, sl_month, sl_day, 
				sl_hour, sl_week  
				) VALUES (
				'".$t2_row['sl_open']."','".$t2_row['sl_point']."','".$t2_row['sl_cash_point']."', '".$t2_row['sl_cash_type']."', 
				'".$t2_row['sl_comics']."', '".$t2_row['sl_big']."', 
				'".$t2_row['sl_year_month']."', '".$t2_row['sl_year']."', '".$t2_row['sl_month']."', '".$t2_row['sl_day']."', 
				'".$t2_row['sl_hour']."', '".$t2_row['sl_week']."' 
			)";
			sql_query($sql_sales_insert);	
		}
		
		if( intval($t2_row['sl_point']) == 0 && intval($t2_row['sl_cash_point']) > 0 ){
			$sql_sales_insert_chk = "
				'".$t2_row['sl_open']."','".$t2_row['sl_point']."','".$t2_row['sl_cash_point']."', '".$t2_row['sl_cash_type']."', 
				'".$t2_row['sl_comics']."', '".$t2_row['sl_big']."', 
				'".$t2_row['sl_year_month']."', '".$t2_row['sl_year']."', '".$t2_row['sl_month']."', '".$t2_row['sl_day']."', 
				'".$t2_row['sl_hour']."', '".$t2_row['sl_week']."' 
			)";
			echo $sql_sales_insert_chk.";<br/>";
		}
	}
}
for($i=1; $i<=3; $i++){	
	$t3_sql = " SELECT * FROM sales_episode_".$i." order by se_no ";
	$t3_result = sql_query($t3_sql);
	while ($t3_row= sql_fetch_array($t3_result)) {
		// 검색	
		$sql_sales_episode = "
			SELECT  count(*) as total_sales_episode FROM vsales_episode_".$i." 
			WHERE 1 
			AND se_comics = '".$t3_row['se_comics']."' 
			AND se_episode = '".$t3_row['se_episode']."' 
			AND se_chapter = '".$t3_row['se_chapter']."' 
			AND se_year_month = '".$t3_row['se_year_month']."' AND se_year = '".$t3_row['se_year']."' 
			AND se_month = '".$t3_row['se_month']."' AND se_day = '".$t3_row['se_day']."' 
			AND se_hour = '".$t3_row['se_hour']."' AND se_week = '".$t3_row['se_week']."' ";
		$total_sales_episode = sql_count($sql_sales_episode, 'total_sales_episode');

		if($total_sales_episode!=0){
			// 업데이트
			$sql_sales_episode_update = " UPDATE  vsales_episode_".$i." SET se_open='".$t3_row['se_open']."'
				WHERE 1 
				AND se_comics = '".$t3_row['se_comics']."' 
				AND se_episode = '".$t3_row['se_episode']."' 
				AND se_chapter = '".$t3_row['se_chapter']."' 
				AND se_year_month = '".$t3_row['se_year_month']."' AND se_year = '".$t3_row['se_year']."' 
				AND se_month = '".$t3_row['se_month']."' AND se_day = '".$t3_row['se_day']."' 
				AND se_hour = '".$t3_row['se_hour']."' AND se_week = '".$t3_row['se_week']."' ";
			sql_query($sql_sales_episode_update);
		}else{
			// 등록
			if( intval($t3_row['se_point']) == 0 && intval($t3_row['se_cash_point']) == 0 ){
				$sql_sales_episode_insert = "
					INSERT INTO  vsales_episode_".$i." (
					se_open, se_point, se_cash_point, se_cash_type, 
					se_comics, se_episode, se_chapter, 
					se_year_month, se_year, se_month, se_day, 
					se_hour, se_week  
					) VALUES (
					'".$t3_row['se_open']."','".$t3_row['se_point']."','".$t3_row['se_cash_point']."', '".$t3_row['se_cash_type']."', 
					'".$t3_row['se_comics']."', '".$t3_row['se_episode']."', '".$t3_row['se_chapter']."', 
					'".$t3_row['se_year_month']."', '".$t3_row['se_year']."', '".$t3_row['se_month']."', '".$t3_row['se_day']."', 
					'".$t3_row['se_hour']."', '".$t3_row['se_week']."' 
				)";
				sql_query($sql_sales_episode_insert);	
			}
			
			if( intval($t3_row['se_point']) == 0 && intval($t3_row['se_cash_point']) > 0 ){
				$sql_sales_episode_insert_chk = "
					'".$t3_row['se_open']."','".$t3_row['se_point']."','".$t3_row['se_cash_point']."', '".$t3_row['se_cash_type']."', 
					'".$t3_row['se_comics']."', '".$t3_row['se_big']."', 
					'".$t3_row['se_year_month']."', '".$t3_row['se_year']."', '".$t3_row['se_month']."', '".$t3_row['se_day']."', 
					'".$t3_row['se_hour']."', '".$t3_row['se_week']."' 
				)";
				echo $sql_sales_episode_insert_chk.";<br/>";
			}
		}
	}
}
*/



/* ///////////////////////////////// CP용 통계 ///////////////////////////////// */
function cs_set_vsales_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date)
{
	global $nm_config;
	
	// echo "cs_set_vsales_ready"."<br/>";
	// cs_set_ready_check($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date);


	if( intval($comics_buy['mb_point']) == 0 && intval($comics_buy['mb_cash_point']) > 0 ){

		// 검색
		$sql_sales = "
			SELECT  count(*) as total_sales FROM vsales WHERE 1 
			AND sl_comics = '".$comics['cm_no']."' AND sl_big  = '".$comics['cm_big']."' 
			AND `sl_year_month` = '".$save_date['year_month']."' AND `sl_year` = '".$save_date['year']."' 
			AND `sl_month` = '".$save_date['month']."' AND `sl_day` = '".$save_date['day']."' 
			AND `sl_hour` = '".$save_date['hour']."' AND `sl_week` = '".$save_date['week']."' ";
		$total_sales = sql_count($sql_sales, 'total_sales');

		if($total_sales!=0){
			$sql_pay_type = "";
			if($comics_free == 'n'){
				if($pay_type == 7){
					$sql_pay_type = " `sl_all_pay`= (sl_all_pay+1), ";
				}else{
					$sql_pay_type = " `sl_pay`= (sl_pay+1), ";
				}
			}

			$sql_sales_update = "
				UPDATE `vsales` SET 
				`sl_open`= (sl_open+1), 
				`sl_free`= (sl_free+".intval($comics_buy['sl_free'])."), 
				`sl_sale`= (sl_sale+".intval($comics_buy['sl_sale'])."), 
				$sql_pay_type
				`sl_event`= (sl_event+".intval($comics_buy['sl_event'])."), 
				`sl_point`= (sl_point+".intval($comics_buy['mb_point'])."), 
				`sl_cash_point`= (sl_cash_point+".intval($comics_buy['mb_cash_point'])."), 
				`sl_cash_type` = '".intval($nm_config['cf_cup_type'])."' 

				WHERE 1 AND sl_comics = '".$comics['cm_no']."' AND sl_big = '".$comics['cm_big']."' 
				AND `sl_year_month` = '".$save_date['year_month']."' AND `sl_year` = '".$save_date['year']."' 
				AND `sl_month` = '".$save_date['month']."' AND `sl_day` = '".$save_date['day']."'
				AND `sl_hour` = '".$save_date['hour']."' AND `sl_week` = '".$save_date['week']."' 
			";
			sql_query($sql_sales_update);
		}else{		
			$sql_pay_type_field = $sql_pay_type_value = "";
			if($comics_free == 'n'){
				if($pay_type == 7){
					$sql_pay_type_field = " `sl_all_pay`, ";
					$sql_pay_type_value = " '1',  ";
				}else{
					$sql_pay_type_field = " `sl_pay`, ";
					$sql_pay_type_value = " '1',  ";
				}
			}

			$sql_sales_insert = "
				INSERT INTO `vsales` (
				`sl_open`, `sl_free`, `sl_sale`, 		
				$sql_pay_type_field
				`sl_event`, 
				`sl_point`, `sl_cash_point`, `sl_cash_type`, 
				`sl_comics`, `sl_big`, 
				`sl_year_month`, `sl_year`, `sl_month`, `sl_day`, `sl_hour`, `sl_week` 
				) VALUES (
				'1', '".intval($comics_buy['sl_free'])."', '".intval($comics_buy['sl_sale'])."', 		
				$sql_pay_type_value
				'".intval($comics_buy['sl_event'])."', 
				'".intval($comics_buy['mb_point'])."', '".intval($comics_buy['mb_cash_point'])."', '".intval($nm_config['cf_cup_type'])."', 
				'".$comics['cm_no']."', '".$comics['cm_big']."', 
				'".$save_date['year_month']."', '".$save_date['year']."', '".$save_date['month']."', '".$save_date['day']."', '".$save_date['hour']."', '".$save_date['week']."' 
			)";
			sql_query($sql_sales_insert);
		}
	}
}


function cs_set_vsales_age_sex_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date)
{
	global $nm_config;

	// echo "cs_set_vsales_age_sex_ready"."<br/>";
	// cs_set_ready_check($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date);

	if( intval($comics_buy['mb_point']) == 0 && intval($comics_buy['mb_cash_point']) > 0 ){
		// 성별
		$sas_sex = cs_set_sales_sex($nm_member, 'sas');
		
		// 검색
		$sql_sales_age_sex = "
			SELECT  count(*) as total_sales_age_sex FROM vsales_age_sex WHERE 1 
			AND sas_comics = '".$comics['cm_no']."' AND sas_big  = '".$comics['cm_big']."' 
			AND `sas_year_month` = '".$save_date['year_month']."' AND `sas_year` = '".$save_date['year']."' 
			AND `sas_month` = '".$save_date['month']."' AND `sas_day` = '".$save_date['day']."' 
			AND `sas_hour` = '".$save_date['hour']."' AND `sas_week` = '".$save_date['week']."' ";
		$total_sales_age_sex = sql_count($sql_sales_age_sex, 'total_sales_age_sex');
		
		if($total_sales_age_sex!=0){		
			// 나이별
			$sql_sas_sex = " `".$sas_sex."`= (".$sas_sex."+1) ";
			if($nm_member['mb_birth'] != ''){
				$sql_sas_sex.= ", `".$sas_sex.mb_get_age_group($nm_member['mb_birth'])."`= (".$sas_sex.mb_get_age_group($nm_member['mb_birth'])."+1) ";
			}
			$sql_sales_age_sex_update = "
				UPDATE `vsales_age_sex` SET ".$sql_sas_sex." WHERE 1 
				AND sas_comics = '".$comics['cm_no']."' AND sas_big = '".$comics['cm_big']."' 
				AND `sas_year_month` = '".$save_date['year_month']."' AND `sas_year` = '".$save_date['year']."' 
				AND `sas_month` = '".$save_date['month']."' AND `sas_day` = '".$save_date['day']."' 
				AND `sas_hour` = '".$save_date['hour']."' AND `sas_week` = '".$save_date['week']."' ";
			sql_query($sql_sales_age_sex_update);
		}else{
			// 나이별
			$sql_sas_field = " `".$sas_sex."`, ";
			$sql_sas_value = "'1', ";
			if($nm_member['mb_birth'] != ''){
				$sql_sas_field.= " `".$sas_sex.mb_get_age_group($nm_member['mb_birth'])."`, ";
				$sql_sas_value.= "'1', ";
			}
			$sql_sales_age_sex_insert = "
				INSERT INTO `vsales_age_sex` ( ".$sql_sas_field." 
				`sas_comics`, `sas_big`, 
				`sas_year_month`, `sas_year`, `sas_month`, `sas_day`, `sas_hour`, `sas_week` 
				) VALUES ( ".$sql_sas_value." 
				'".$comics['cm_no']."', '".$comics['cm_big']."', 
				'".$save_date['year_month']."', '".$save_date['year']."', '".$save_date['month']."', '".$save_date['day']."', '".$save_date['hour']."', '".$save_date['week']."' 
			)";
			sql_query($sql_sales_age_sex_insert);
		}
	}
}

function cs_set_vsales_episode_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date)
{
	global $nm_config;

	// echo "cs_set_vsales_episode_ready"."<br/>";
	// cs_set_ready_check($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date);

	if( intval($comics_buy['mb_point']) == 0 && intval($comics_buy['mb_cash_point']) > 0 ){

		// 검색
		$sql_sales_episode = "
			SELECT  count(*) as total_sales_episode 
			FROM vsales_episode_".$comics['cm_big']." WHERE 1 
			AND se_comics = '".$comics['cm_no']."' 
			AND se_episode = '".$episode['ce_no']."' 
			AND se_chapter = '".$episode['ce_chapter']."' 
			AND	`se_year_month` = '".$save_date['year_month']."' AND `se_year` = '".$save_date['year']."' 
			AND `se_month` = '".$save_date['month']."' AND `se_day` = '".$save_date['day']."' 
			AND `se_hour` = '".$save_date['hour']."' AND `se_week` = '".$save_date['week']."' ";
		$total_sales_episode = sql_count($sql_sales_episode, 'total_sales_episode');

		if($total_sales_episode!=0){
			$sql_pay_type = "";
			if($comics_free == 'n'){
				if($pay_type == 7){
					$sql_pay_type = " `se_all_pay`= (se_all_pay+1), ";
				}else{
					$sql_pay_type = " `se_pay`= (se_pay+1), ";
				}
			}

			$sql_sales_episode_update = "
				UPDATE `vsales_episode_".$comics['cm_big']."` SET 
				`se_open`= (se_open+1), 
				`se_free`= (se_free+".intval($comics_buy['sl_free'])."), 
				`se_sale`= (se_sale+".intval($comics_buy['sl_sale'])."), 
				$sql_pay_type
				`se_event`= (se_event+".intval($comics_buy['sl_event'])."), 
				`se_point`= (se_point+".intval($comics_buy['mb_point'])."), 
				`se_cash_point`= (se_cash_point+".intval($comics_buy['mb_cash_point'])."), 
				`se_cash_type` = '".intval($nm_config['cf_cup_type'])."' 

				WHERE 1 AND se_comics = '".$comics['cm_no']."' 
				AND se_episode = '".$episode['ce_no']."' 
				AND se_chapter = '".$episode['ce_chapter']."' 
				AND `se_year_month` = '".$save_date['year_month']."' AND `se_year` = '".$save_date['year']."' 
				AND `se_month` = '".$save_date['month']."' AND `se_day` = '".$save_date['day']."' 
				AND `se_hour` = '".$save_date['hour']."' AND `se_week` = '".$save_date['week']."' ";
			sql_query($sql_sales_episode_update);
		}else{
			$sql_pay_type_field = $sql_pay_type_value = "";
			if($comics_free == 'n'){
				if($pay_type == 7){
					$sql_pay_type_field = " `se_all_pay`, ";
					$sql_pay_type_value = " '1',  ";
				}else{
					$sql_pay_type_field = " `se_pay`, ";
					$sql_pay_type_value = " '1',  ";
				}
			}

			$sql_sales_episode_insert = "
				INSERT INTO `vsales_episode_".$comics['cm_big']."` ( 
				`se_open`, `se_free`, `se_sale`, 		
				$sql_pay_type_field
				`se_event`, 
				`se_point`, `se_cash_point`, `se_cash_type`, 
				`se_comics`, `se_episode`, `se_chapter`, 
				`se_year_month`, `se_year`, `se_month`, `se_day`, `se_hour`, `se_week` 
				) VALUES (
				'1', '".intval($comics_buy['sl_free'])."', '".intval($comics_buy['sl_sale'])."', 		
				$sql_pay_type_value
				'".intval($comics_buy['sl_event'])."', 
				'".intval($comics_buy['mb_point'])."', '".intval($comics_buy['mb_cash_point'])."', '".intval($nm_config['cf_cup_type'])."', 
				'".$comics['cm_no']."', '".$episode['ce_no']."', '".$episode['ce_chapter']."', 
				'".$save_date['year_month']."', '".$save_date['year']."', '".$save_date['month']."', '".$save_date['day']."', '".$save_date['hour']."', '".$save_date['week']."' 
				)";
			sql_query($sql_sales_episode_insert);
		}
	}
}

function cs_set_vsales_episode_age_sex_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date)
{
	global $nm_config;

	// echo "cs_set_vsales_episode_age_sex_ready"."<br/>";
	// cs_set_ready_check($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date);

	if( intval($comics_buy['mb_point']) == 0 && intval($comics_buy['mb_cash_point']) > 0 ){
	
		// 검색
		$seas_sex = cs_set_sales_sex($nm_member, 'seas');
		
		// 검색
		$sql_sales_episode_age_sex = "
			SELECT  count(*) as total_sales_episode_age_sex FROM vsales_episode_".$comics['cm_big']."_age_sex WHERE 1 
			AND seas_comics = '".$comics['cm_no']."'
			AND seas_episode = '".$episode['ce_no']."' 
			AND seas_chapter = '".$episode['ce_chapter']."' 
			AND `seas_year_month` = '".$save_date['year_month']."' AND `seas_year` = '".$save_date['year']."' 
			AND `seas_month` = '".$save_date['month']."' AND `seas_day` = '".$save_date['day']."' 
			AND `seas_hour` = '".$save_date['hour']."' AND `seas_week` = '".$save_date['week']."' ";
		$total_sales_episode_age_sex = sql_count($sql_sales_episode_age_sex, 'total_sales_episode_age_sex');

		if($total_sales_episode_age_sex!=0){
			// 나이별
			$sql_seas_sex = " `".$seas_sex."`= (".$seas_sex."+1) ";
			if($nm_member['mb_birth'] != ''){
				$sql_seas_sex.= ", `".$seas_sex.mb_get_age_group($nm_member['mb_birth'])."`= (".$seas_sex.mb_get_age_group($nm_member['mb_birth'])."+1) ";
			}

			$sql_sales_episode_age_sex_update = "
				UPDATE `vsales_episode_".$comics['cm_big']."_age_sex` SET ".$sql_seas_sex." WHERE 1 
				AND seas_comics = '".$comics['cm_no']."' 
				AND seas_episode = '".$episode['ce_no']."' 
				AND seas_chapter = '".$episode['ce_chapter']."' 
				AND `seas_year_month` = '".$save_date['year_month']."' AND `seas_year` = '".$save_date['year']."' 
				AND `seas_month` = '".$save_date['month']."' AND `seas_day` = '".$save_date['day']."' 
				AND `seas_hour` = '".$save_date['hour']."' AND `seas_week` = '".$save_date['week']."' 
			";
			sql_query($sql_sales_episode_age_sex_update);
		}else{
			// 나이별
			$sql_seas_field = " `".$seas_sex."`, ";
			$sql_seas_value = "'1', ";
			if($nm_member['mb_birth'] != ''){
				$sql_seas_field.= " `".$seas_sex.mb_get_age_group($nm_member['mb_birth'])."`, ";
				$sql_seas_value.= "'1', ";
			}

			$sql_sales_episode_age_sex_insert = "
				INSERT INTO `vsales_episode_".$comics['cm_big']."_age_sex` ( ".$sql_seas_field." 
				`seas_comics`, `seas_episode`, `seas_chapter`, 
				`seas_year_month`, `seas_year`, `seas_month`, `seas_day`, `seas_hour`, `seas_week` 
				) VALUES ( ".$sql_seas_value."
				'".$comics['cm_no']."', '".$episode['ce_no']."', '".$episode['ce_chapter']."', 
				'".$save_date['year_month']."', '".$save_date['year']."', '".$save_date['month']."', '".$save_date['day']."', '".$save_date['hour']."', '".$save_date['week']."' 
			)";
			sql_query($sql_sales_episode_age_sex_insert);

		}
	}
}


function cs_set_ready_check($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date)
{
	echo "<br/>comics<br/>";
	print_r($comics);
	echo "<br/><br/>";

	echo "<br/>episode<br/>";
	print_r($episode);
	echo "<br/><br/>";

	echo "<br/>nm_member<br/>";
	print_r($nm_member);
	echo "<br/><br/>";

	echo "<br/>comics_buy<br/>";
	print_r($comics_buy);
	echo "<br/><br/>";

	echo "<br/>comics_free<br/>";
	echo $comics_free;
	echo "<br/><br/>";

	echo "<br/>pay_type<br/>";
	echo $pay_type;
	echo "<br/><br/>";

	echo "<br/>save_date<br/>";
	print_r($save_date);
	echo "<br/><br/>";
}
?>