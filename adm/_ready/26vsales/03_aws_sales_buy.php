<? include_once '_common.php'; // 공통 

die;

/* AWS 연결 */
$connect = mysql_connect("peanutoon-db.cv8kfy8vdqde.ap-northeast-1.rds.amazonaws.com", "peanutoon", "vlsjxns6619") or die('MySQL Connect Error!!!');
mysql_select_db("peanutoon", $connect) or die('MySQL DB Error!!!');
mysql_query("set session character_set_connection=utf8;");
mysql_query("set session character_set_results=utf8;");
mysql_query("set session character_set_client=utf8;");
mysql_query("set names utf8");

// 코믹스 매출 저장
$t_cbh_sql = "	select * from t_content_buy_history cbh 
				left JOIN t_customer ct ON cbh.customer_num = ct.customer_num 
				order by cbh.buy_num asc ";
$t_cbh_result = sql_query($t_cbh_sql);

// AWS table Collaboration
include NM_PATH.'/config/dbconnect.php'; // DB연결

$db_name = "peanutoonDB";

// 데이터 삭제
$year_arr = array(2015, 2016, 2017);
$big_arr = array(1, 2, 3);
foreach($year_arr as $year_key => $year_val){
	$sql_sl_year_db_del		= "delete from ".$db_name."_".$year_val.".sales "; 
	sql_query($sql_sl_year_db_del);

	$sql_sas_year_db_del	= "delete from ".$db_name."_".$year_val.".sales_age_sex "; 
	sql_query($sql_sas_year_db_del);

	$sql_mpec_year_db_del	= "delete from ".$db_name."_".$year_val.".member_point_expen_comics "; 
	sql_query($sql_mpec_year_db_del);

	$sql_mpu_year_db_del	= "delete from ".$db_name."_".$year_val.".member_point_used where mpu_class='b' "; 
	sql_query($sql_mpu_year_db_del);

	foreach($big_arr as $big_val){
		$sql_se_year_db_del	= "delete from ".$db_name."_".$year_val.".sales_episode_".$big_val." "; 
		sql_query($sql_se_year_db_del);

		$sql_seas_year_db_del	= "delete from ".$db_name."_".$year_val.".sales_episode_".$big_val."_age_sex "; 
		sql_query($sql_seas_year_db_del);
		
		$sql_mpee_year_db_del	= "delete from ".$db_name."_".$year_val.".member_point_expen_episode_".$big_val." "; 
		sql_query($sql_mpee_year_db_del);
	}
}

while ($t_cbh_row = sql_fetch_array($t_cbh_result)) {
	
	$comics = array();
	$episode = array();
	$comics_buy = array();
	$nm_member = array();
	$save_date = array();

	// 연도별로 다른 DB에 저장
	$save_date['year_month']	= substr($t_cbh_row['buy_date'], 0, 7);
	$save_date['year']			= substr($t_cbh_row['buy_date'], 0, 4);
	$save_date['month']			= substr($t_cbh_row['buy_date'], 5, 2);
	$save_date['day']			= substr($t_cbh_row['buy_date'], 8, 2);
	$save_date['hour']			= substr($t_cbh_row['buy_date'], 11, 2);
	$save_date['min']			= substr($t_cbh_row['buy_date'], 14, 2);
	$save_date['week']			= date('w', strtotime($t_cbh_row['buy_date']));
	$save_date['date']			= $t_cbh_row['buy_date'];

	$db_t_name = $db_name.'_'.$save_date['year'];

	// 에피소드 정보
	$sql_episode = " SELECT * FROM comics_episode_aws WHERE 1 AND content_num='".$t_cbh_row['content_num']."' ";
	$get_episode = sql_fetch($sql_episode);
	if($get_episode['ce_comics'] == ''){ die($sql_episode); }

	// 코믹스 정보
	$get_comics = get_comics($get_episode['ce_comics']);
	if($get_comics['cm_no'] == ''){ die($sql_episode); }

	$comics['cm_no']		= $get_comics['cm_no'];
	$comics['cm_big']		= $get_comics['cm_big'];
	$comics['cm_small']		= $get_comics['cm_small'];

	// cm_big=4는 서비스 안하니깐 넘어가기
	if($comics['cm_big'] == '4'){ continue; }

	$episode['ce_no']		= $get_episode['ce_no'];
	$episode['ce_chapter']	= $get_episode['ce_chapter'];
	$episode['ce_chapter']	= $get_episode['ce_chapter'];

	$nm_member['mb_no']		= $t_cbh_row['customer_num'];
	$nm_member['mb_idx']	= mb_get_idx($t_cbh_row['customer_email']);

	$mb_sex = "n";
	// 성별(m:남자, w:여자, n:미인증)
	switch($t_cbh_row['gender']){
		case 'm' :		$mb_sex = "m";		break;	// m:남자
		case 'f' :		$mb_sex = "w";		break;	// w:여자
		default :		$mb_sex = "n";		break;	// n:미인증
	}
	$nm_member['mb_sex']		= $mb_sex;

	$nm_member['mb_birth']		= "";
	if($t_cbh_row['birthday'] != "0000-00-00"){
		$nm_member['mb_birth']	= $t_cbh_row['birthday'];
	}

	$comics_buy['sl_free']			= 0;
	$comics_buy['sl_sale']			= 0;
	$comics_buy['sl_event']			= 0;
	$comics_buy['mb_cash_point']	= $t_cbh_row['sale_cash'];
	$comics_buy['mb_point']			= $t_cbh_row['sale_point'];

	$comics_buy['from']				= "";
	$ce_up_kind = '화';
	if($get_comics['cm_up_kind'] == '1'){ $ce_up_kind = '권'; }

	if($get_episode['ce_chapter'] > 0){
		$comics_buy['from'] = $get_episode['ce_chapter'].$ce_up_kind;
		if($get_episode['ce_title'] != ''){ $comics_buy['from'] = $get_episode['ce_chapter'].$ce_up_kind." - ".$get_episode['ce_title']; }
	}else{
		$comics_buy['from'] = $get_episode['ce_notice'];
	}

	$comics_buy['from'] = addslashes($comics_buy['from'])." 구매";


	$comics_free					= 'n';
	$pay_type						= "1";

	$dbtable_sl			= $db_t_name.".sales";
	$dbtable_sas		= $db_t_name.".sales_age_sex";
	$dbtable_se			= $db_t_name.".sales_episode";
	$dbtable_seas		= $db_t_name.".sales_episode";

	$dbtable_vsl		= $db_t_name.".vsales";
	$dbtable_vsas		= $db_t_name.".vsales_age_sex";
	$dbtable_vse		= $db_t_name.".vsales_episode";
	$dbtable_vseas		= $db_t_name.".vsales_episode";	

	cs_set_sales_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date, $dbtable_sl, ''); // CP용 통계
	cs_set_sales_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date, $dbtable_vsl, 'v'); // CP용 통계

	cs_set_sales_age_sex_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date, $dbtable_sas, ''); // CP용 통계
	cs_set_sales_age_sex_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date, $dbtable_vsas, 'v'); // CP용 통계

	cs_set_sales_episode_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date, $dbtable_se, ''); // CP용 통계
	cs_set_sales_episode_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date, $dbtable_vse, 'v'); // CP용 통계

	cs_set_sales_episode_age_sex_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date, $dbtable_seas, ''); // CP용 통계
	cs_set_sales_episode_age_sex_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date, $dbtable_vseas, 'v'); // CP용 통계
	
	$dbtable_mpec	= $db_t_name.".member_point_expen_comics";
	cs_set_expen_comics_ready($comics, $episode, $nm_member, $comics_buy, $save_date, $dbtable_mpec, '');

	$dbtable_mpee	= $db_t_name.".member_point_expen_episode_".$get_comics['cm_big'];
	cs_set_expen_episode_ready($comics, $episode, $nm_member, $comics_buy, $save_date, $dbtable_mpec, $dbtable_mpee, '');
	
	$dbtable_mpu	= $db_t_name.".member_point_used";
	cs_set_member_point_used($comics, $episode, $nm_member, $comics_buy, $save_date, $dbtable_mpu, '');


	/// die;
}

function cs_set_member_point_used($comics, $episode, $nm_member, $comics_buy, $save_date, $dbtable, $v_mode)
{
	global $nm_config;	

	/* 연령 */
	// $mb_age_group = mb_get_age_group($nm_member['mb_birth']);	
	$mb_age_group = mb_get_age_group($nm_member['mb_birth']);
	if(intval($mb_age_group) >=60){ $mb_age_group = 60; }

	$sql_mpu_insert = "
	INSERT INTO ".$dbtable." (
	mpu_member, mpu_member_idx, mpu_class, 
	mpu_cash_point, mpu_point, mpu_count, 

	mpu_comics, mpu_big, mpu_small, mpu_cash_type, 

	mpu_way, mpu_from, mpu_age, mpu_sex, 

	mpu_os, mpu_brow, mpu_user_agent, mpu_version, 

	mpu_year_month, mpu_year, mpu_month, mpu_day, mpu_hour, mpu_week, mpu_date 
	) VALUES (
	'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 'b', 
	'".$comics_buy['mb_cash_point']."', '".$comics_buy['mb_point']."', '1', 

	'".$comics['cm_no']."', '".$comics['cm_big']."', '".$comics['cm_small']."', '".$nm_config['cf_cup_type']."',

	'1', '".$comics_buy['from']."', '".$mb_age_group."', '".$nm_member['mb_sex']."', 

	'aws', 'aws', 'aws', 'aws',

	'".$save_date['year_month']."', '".$save_date['year']."', '".$save_date['month']."', '".$save_date['day']."', '".$save_date['hour']."', '".$save_date['week']."', '".$save_date['date']."' 
	)";
	if(sql_query($sql_mpu_insert)){
	}else{ die($sql_mpu_insert); }	

}

function cs_set_expen_episode_ready($comics, $episode, $nm_member, $comics_buy, $save_date, $dbtable_mpec, $dbtable_mpee, $v_mode)
{
	global $nm_config;

	/* 연령 */
	// $mb_age_group = mb_get_age_group($nm_member['mb_birth']);
	$mb_age_group = mb_get_age_group($nm_member['mb_birth']);
	if(intval($mb_age_group) >=60){ $mb_age_group = 60; }
	
	// 코믹스 구매 번호 구하기 - member_point_expen
	$sql_mpec_chk = "SELECT * FROM ".$dbtable_mpec." WHERE 1 AND mpec_comics = '".$comics['cm_no']."' AND mpec_member = '".$nm_member['mb_no']."' AND mpec_member_idx = '".$nm_member['mb_idx']."' ";	
	$row_mpec_chk = sql_fetch($sql_mpec_chk);

	// 코믹스 에피소드 구매 입력
	$sql_mpee_insert = "
	INSERT INTO ".$dbtable_mpee." (
	mpee_member, mpee_member_idx, 
	mpec_no, mpee_comics, mpee_small, mpee_episode, mpee_chapter, 

	mpee_cash_type, mpee_cash_point, mpee_point, 
	mpee_state, mpee_age, mpee_way, mpee_sex, 
	mpee_os, mpee_brow, mpee_user_agent, mpee_version, 

	mpee_year_month, mpee_year, mpee_month, mpee_day, mpee_hour, mpee_week, mpee_date
	) VALUES (
	'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 
	'".$row_mpec_chk['mpec_no']."', '".$comics['cm_no']."', '".$comics['cm_small']."', '".$episode['ce_no']."', '".$episode['ce_chapter']."', 

	'".$nm_config['cf_cup_type']."', '".$comics_buy['mb_cash_point']."', '".$comics_buy['mb_point']."', 
	'1', '".$mb_age_group."', '1', '".$nm_member['mb_sex']."', 
	'aws', 'aws', 'aws', 'aws',

	'".$save_date['year_month']."', '".$save_date['year']."', '".$save_date['month']."', '".$save_date['day']."', '".$save_date['hour']."', '".$save_date['week']."', '".$save_date['date']."' 
	)";
	if(sql_query($sql_mpee_insert)){
	}else{ die($sql_mpee_insert); }	
}

function cs_set_expen_comics_ready($comics, $episode, $nm_member, $comics_buy, $save_date, $dbtable, $v_mode)
{
	global $nm_config;

	/* 연령 */
	// $mb_age_group = mb_get_age_group($nm_member['mb_birth']);
	$mb_age_group = mb_get_age_group($nm_member['mb_birth']);
	if(intval($mb_age_group) >=60){ $mb_age_group = 60; }
	
	// 코믹스 구매 입력 - member_point_expen
	$sql_mpec = "SELECT  count(*) as total_mpec FROM ".$dbtable." WHERE 1 AND mpec_comics = '".$comics['cm_no']."' AND mpec_big = '".$comics['cm_big']."' AND mpec_member = '".$nm_member['mb_no']."' AND mpec_member_idx = '".$nm_member['mb_idx']."' ";	
	$total_mpec = sql_count($sql_mpec, 'total_mpec');
	
	if($total_mpec!=0){
		$sql_mpec_update = "
		UPDATE ".$dbtable." SET 
		mpec_big = '".$comics['cm_big']."', 
		mpec_small = '".$comics['cm_small']."', 

		mpec_cash_type = '".$nm_config['cf_cup_type']."', 
		mpec_cash_point= (mpec_cash_point+".intval($comics_buy['mb_cash_point'])."), 
		mpec_point= (mpec_point+".intval($comics_buy['mb_point'])."), 
		mpec_count= (mpec_count+1), 

		mpec_state = '1', mpec_way = '1', 
		mpec_age = '".$mb_age_group."', 
		mpec_sex = '".$nm_member['mb_sex']."', 

		mpec_os = 'aws', mpec_brow = 'aws', mpec_user_agent = 'aws', mpec_version = 'aws', 

		mpec_year_month = '".$save_date['year_month']."', 
		mpec_year = '".$save_date['year']."', 
		mpec_month = '".$save_date['month']."', 
		mpec_day = '".$save_date['day']."', 
		mpec_hour = '".$save_date['hour']."', 
		mpec_week = '".$save_date['week']."', 
		mpec_date = '".$save_date['date']."' 

		WHERE 1 AND mpec_comics = '".$comics['cm_no']."' AND mpec_member = '".$nm_member['mb_no']."' AND mpec_member_idx = '".$nm_member['mb_idx']."' 
				AND mpec_big='".$comics['cm_big']."' 
				AND mpec_year_month='".$save_date['year_month']."' 
				AND mpec_year='".$save_date['year']."' 
				AND mpec_month='".$save_date['month']."' 
				AND mpec_day='".$save_date['day']."' 
				AND mpec_hour='".$save_date['hour']."' 
				AND mpec_week='".$save_date['week']."' 		
		";
		if(sql_query($sql_mpec_update)){
		}else{ die($sql_mpec_update); }	
	}else{
		$sql_mpec_insert = "
		INSERT INTO ".$dbtable." (
		mpec_member, mpec_member_idx, 
		mpec_comics, mpec_big, mpec_small, 

		mpec_cash_type, mpec_cash_point, mpec_point, mpec_count, 
		mpec_state, mpec_way, mpec_age, mpec_sex, 
		mpec_os, mpec_brow, mpec_user_agent, mpec_version, 

		mpec_year_month, mpec_year, mpec_month, mpec_day, mpec_hour, mpec_week, mpec_date 
		) VALUES (
		'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 

		'".$comics['cm_no']."', '".$comics['cm_big']."', '".$comics['cm_small']."', 

		'".$nm_config['cf_cup_type']."', '".$comics_buy['mb_cash_point']."', '".$comics_buy['mb_point']."', '1', 
		'1', '1', '".$mb_age_group."', '".$nm_member['mb_sex']."', 
		'aws', 'aws', 'aws', 'aws', 

		'".$save_date['year_month']."', '".$save_date['year']."', '".$save_date['month']."', '".$save_date['day']."', '".$save_date['hour']."', '".$save_date['week']."', '".$save_date['date']."' 
		)";
		if(sql_query($sql_mpec_insert)){
		}else{ die($sql_mpec_insert); }	
	}	
}


function cs_set_sales_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date, $dbtable, $v_mode)
{
	global $nm_config;

	if($v_mode == "v" && intval($comics_buy['mb_point']) > 0){ return false; } // CP용 통계일 경우 포인트는 0

	// 검색
	$sql_sales = "
		SELECT  count(*) as total_sales FROM ".$dbtable." WHERE 1 
		AND sl_comics = '".$comics['cm_no']."' AND sl_big  = '".$comics['cm_big']."' 
		AND sl_year_month = '".$save_date['year_month']."' AND sl_year = '".$save_date['year']."' 
		AND sl_month = '".$save_date['month']."' AND sl_day = '".$save_date['day']."' 
		AND sl_hour = '".$save_date['hour']."' AND sl_week = '".$save_date['week']."' ";
	$total_sales = sql_count($sql_sales, 'total_sales');

	if($total_sales!=0){
		$sql_pay_type = "";
		if($comics_free == 'n'){
			if($pay_type == 7){
				$sql_pay_type = " sl_all_pay= (sl_all_pay+1), ";
			}else{
				$sql_pay_type = " sl_pay= (sl_pay+1), ";
			}
		}

		$sql_sales_update = "
			UPDATE ".$dbtable." SET 
			sl_open= (sl_open+1), 
			sl_free= (sl_free+".intval($comics_buy['sl_free'])."), 
			sl_sale= (sl_sale+".intval($comics_buy['sl_sale'])."), 
			$sql_pay_type
			sl_event= (sl_event+".intval($comics_buy['sl_event'])."), 
			sl_point= (sl_point+".intval($comics_buy['mb_point'])."), 
			sl_cash_point= (sl_cash_point+".intval($comics_buy['mb_cash_point'])."), 
			sl_cash_type = '".intval($nm_config['cf_cup_type'])."' 

			WHERE 1 AND sl_comics = '".$comics['cm_no']."' AND sl_big = '".$comics['cm_big']."' 
			AND sl_year_month = '".$save_date['year_month']."' AND sl_year = '".$save_date['year']."' 
			AND sl_month = '".$save_date['month']."' AND sl_day = '".$save_date['day']."'
			AND sl_hour = '".$save_date['hour']."' AND sl_week = '".$save_date['week']."' 
		";
		if(sql_query($sql_sales_update)){
		}else{ die($sql_sales_update); }	
	}else{		
		$sql_pay_type_field = $sql_pay_type_value = "";
		if($comics_free == 'n'){
			if($pay_type == 7){
				$sql_pay_type_field = " sl_all_pay, ";
				$sql_pay_type_value = " '1',  ";
			}else{
				$sql_pay_type_field = " sl_pay, ";
				$sql_pay_type_value = " '1',  ";
			}
		}

		$sql_sales_insert = "
			INSERT INTO ".$dbtable." (
			sl_open, sl_free, sl_sale, 		
			$sql_pay_type_field
			sl_event, 
			sl_point, sl_cash_point, sl_cash_type, 
			sl_comics, sl_big, 
			sl_year_month, sl_year, sl_month, sl_day, sl_hour, sl_week 
			) VALUES (
			'1', '".intval($comics_buy['sl_free'])."', '".intval($comics_buy['sl_sale'])."', 		
			$sql_pay_type_value
			'".intval($comics_buy['sl_event'])."', 
			'".intval($comics_buy['mb_point'])."', '".intval($comics_buy['mb_cash_point'])."', '".intval($nm_config['cf_cup_type'])."', 
			'".$comics['cm_no']."', '".$comics['cm_big']."', 
			'".$save_date['year_month']."', '".$save_date['year']."', '".$save_date['month']."', '".$save_date['day']."', '".$save_date['hour']."', '".$save_date['week']."' 
		)";
		if(sql_query($sql_sales_insert)){
		}else{ die($sql_sales_insert); }	
	}
}

function cs_set_sales_age_sex_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date, $dbtable, $v_mode)
{
	global $nm_config;

	if($v_mode == "v" && intval($comics_buy['mb_point']) > 0){ return false; } // CP용 통계일 경우 포인트는 0

	// 성별
	$sas_sex = cs_set_sales_sex($nm_member, 'sas');
	$age_group = mb_get_age_group($nm_member['mb_birth']);
	if(intval($age_group) >=60){ $age_group = 60; }
	
	// 검색
	$sql_sales_age_sex = "
		SELECT  count(*) as total_sales_age_sex FROM ".$dbtable." WHERE 1 
		AND sas_comics = '".$comics['cm_no']."' AND sas_big  = '".$comics['cm_big']."' 
		AND sas_year_month = '".$save_date['year_month']."' AND sas_year = '".$save_date['year']."' 
		AND sas_month = '".$save_date['month']."' AND sas_day = '".$save_date['day']."' 
		AND sas_hour = '".$save_date['hour']."' AND sas_week = '".$save_date['week']."' ";
	$total_sales_age_sex = sql_count($sql_sales_age_sex, 'total_sales_age_sex');
	
	if($total_sales_age_sex!=0){		
		// 나이별
		$sql_sas_sex = " ".$sas_sex."= (".$sas_sex."+1) ";
		if($nm_member['mb_birth'] != ''){
			$sql_sas_sex.= ", ".$sas_sex.$age_group."= (".$sas_sex.$age_group."+1) ";
		}
		$sql_sales_age_sex_update = "
			UPDATE ".$dbtable." SET ".$sql_sas_sex." WHERE 1 
			AND sas_comics = '".$comics['cm_no']."' AND sas_big = '".$comics['cm_big']."' 
			AND sas_year_month = '".$save_date['year_month']."' AND sas_year = '".$save_date['year']."' 
			AND sas_month = '".$save_date['month']."' AND sas_day = '".$save_date['day']."' 
			AND sas_hour = '".$save_date['hour']."' AND sas_week = '".$save_date['week']."' ";
		if(sql_query($sql_sales_age_sex_update)){
		}else{ die($sql_sales_age_sex_update); }	
	}else{
		// 나이별
		$sql_sas_field = " ".$sas_sex.", ";
		$sql_sas_value = "'1', ";
		if($nm_member['mb_birth'] != ''){
			$sql_sas_field.= " ".$sas_sex.$age_group.", ";
			$sql_sas_value.= "'1', ";
		}
		$sql_sales_age_sex_insert = "
			INSERT INTO ".$dbtable." ( ".$sql_sas_field." 
			sas_comics, sas_big, 
			sas_year_month, sas_year, sas_month, sas_day, sas_hour, sas_week 
			) VALUES ( ".$sql_sas_value." 
				'".$comics['cm_no']."', '".$comics['cm_big']."', 
				'".$save_date['year_month']."', '".$save_date['year']."', '".$save_date['month']."', '".$save_date['day']."', '".$save_date['hour']."', '".$save_date['week']."' 
		)";
		if(sql_query($sql_sales_age_sex_insert)){
		}else{ die($sql_sales_age_sex_insert); }	
	}
}

function cs_set_sales_episode_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date, $dbtable, $v_mode)
{
	global $nm_config;

	if($v_mode == "v" && intval($comics_buy['mb_point']) > 0){ return false; } // CP용 통계일 경우 포인트는 0
	
	// 검색
	$sql_sales_episode = "
		SELECT  count(*) as total_sales_episode 
		FROM ".$dbtable."_".$comics['cm_big']." WHERE 1 
		AND se_comics = '".$comics['cm_no']."' 
		AND se_episode = '".$episode['ce_no']."' 
		AND se_chapter = '".$episode['ce_chapter']."' 
		AND	se_year_month = '".$save_date['year_month']."' AND se_year = '".$save_date['year']."' 
		AND se_month = '".$save_date['month']."' AND se_day = '".$save_date['day']."' 
		AND se_hour = '".$save_date['hour']."' AND se_week = '".$save_date['week']."' ";
	$total_sales_episode = sql_count($sql_sales_episode, 'total_sales_episode');

	if($total_sales_episode!=0){
		$sql_pay_type = "";
		if($comics_free == 'n'){
			if($pay_type == 7){
				$sql_pay_type = " se_all_pay= (se_all_pay+1), ";
			}else{
				$sql_pay_type = " se_pay= (se_pay+1), ";
			}
		}

		$sql_sales_episode_update = "
			UPDATE ".$dbtable."_".$comics['cm_big']." SET 
			se_open= (se_open+1), 
			se_free= (se_free+".intval($comics_buy['sl_free'])."), 
			se_sale= (se_sale+".intval($comics_buy['sl_sale'])."), 
			$sql_pay_type
			se_event= (se_event+".intval($comics_buy['sl_event'])."), 
			se_point= (se_point+".intval($comics_buy['mb_point'])."), 
			se_cash_point= (se_cash_point+".intval($comics_buy['mb_cash_point'])."), 
			se_cash_type = '".intval($nm_config['cf_cup_type'])."' 

			WHERE 1 AND se_comics = '".$comics['cm_no']."' 
			AND se_episode = '".$episode['ce_no']."' 
			AND se_chapter = '".$episode['ce_chapter']."' 
			AND se_year_month = '".$save_date['year_month']."' AND se_year = '".$save_date['year']."' 
			AND se_month = '".$save_date['month']."' AND se_day = '".$save_date['day']."' 
			AND se_hour = '".$save_date['hour']."' AND se_week = '".$save_date['week']."' ";
		if(sql_query($sql_sales_episode_update)){
		}else{ die($sql_sales_episode_update); }	
	}else{
		$sql_pay_type_field = $sql_pay_type_value = "";
		if($comics_free == 'n'){
			if($pay_type == 7){
				$sql_pay_type_field = " se_all_pay, ";
				$sql_pay_type_value = " '1',  ";
			}else{
				$sql_pay_type_field = " se_pay, ";
				$sql_pay_type_value = " '1',  ";
			}
		}

		$sql_sales_episode_insert = "
			INSERT INTO ".$dbtable."_".$comics['cm_big']." ( 
			se_open, se_free, se_sale, 		
			$sql_pay_type_field
			se_event, 
			se_point, se_cash_point, se_cash_type, 
			se_comics, se_episode, se_chapter, 
			se_year_month, se_year, se_month, se_day, se_hour, se_week 
			) VALUES (
			'1', '".intval($comics_buy['sl_free'])."', '".intval($comics_buy['sl_sale'])."', 		
			$sql_pay_type_value
			'".intval($comics_buy['sl_event'])."', 
			'".intval($comics_buy['mb_point'])."', '".intval($comics_buy['mb_cash_point'])."', '".intval($nm_config['cf_cup_type'])."', 
			'".$comics['cm_no']."', '".$episode['ce_no']."', '".$episode['ce_chapter']."', 
			'".$save_date['year_month']."', '".$save_date['year']."', '".$save_date['month']."', '".$save_date['day']."', '".$save_date['hour']."', '".$save_date['week']."' 
			)";
		if(sql_query($sql_sales_episode_insert)){
		}else{ die($sql_sales_episode_insert); }	
	}
}

function cs_set_sales_episode_age_sex_ready($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $save_date, $dbtable, $v_mode)
{
	global $nm_config;

	if($v_mode == "v" && intval($comics_buy['mb_point']) > 0){ return false; } // CP용 통계일 경우 포인트는 0

	// 검색
	$seas_sex = cs_set_sales_sex($nm_member, 'seas');
	$age_group = mb_get_age_group($nm_member['mb_birth']);
	if(intval($age_group) >=60){ $age_group = 60; }
	
	// 검색
	$sql_sales_episode_age_sex = "
		SELECT  count(*) as total_sales_episode_age_sex FROM ".$dbtable."_".$comics['cm_big']."_age_sex WHERE 1 
		AND seas_comics = '".$comics['cm_no']."'
		AND seas_episode = '".$episode['ce_no']."' 
		AND seas_chapter = '".$episode['ce_chapter']."' 
			AND seas_year_month = '".$save_date['year_month']."' AND seas_year = '".$save_date['year']."' 
			AND seas_month = '".$save_date['month']."' AND seas_day = '".$save_date['day']."' 
			AND seas_hour = '".$save_date['hour']."' AND seas_week = '".$save_date['week']."' ";
	$total_sales_episode_age_sex = sql_count($sql_sales_episode_age_sex, 'total_sales_episode_age_sex');

	if($total_sales_episode_age_sex!=0){
		// 나이별
		$sql_seas_sex = " ".$seas_sex."= (".$seas_sex."+1) ";
		if($nm_member['mb_birth'] != ''){
			$sql_seas_sex.= ", ".$seas_sex.$age_group."= (".$seas_sex.$age_group."+1) ";
		}

		$sql_sales_episode_age_sex_update = "
			UPDATE ".$dbtable."_".$comics['cm_big']."_age_sex SET ".$sql_seas_sex." WHERE 1 
			AND seas_comics = '".$comics['cm_no']."' 
			AND seas_episode = '".$episode['ce_no']."' 
			AND seas_chapter = '".$episode['ce_chapter']."' 
			AND seas_year_month = '".$save_date['year_month']."' AND seas_year = '".$save_date['year']."' 
			AND seas_month = '".$save_date['month']."' AND seas_day = '".$save_date['day']."' 
			AND seas_hour = '".$save_date['hour']."' AND seas_week = '".$save_date['week']."' 
		";
		if(sql_query($sql_sales_episode_age_sex_update)){
		}else{ die($sql_sales_episode_age_sex_update); }	
	}else{
		// 나이별
		$sql_seas_field = " ".$seas_sex.", ";
		$sql_seas_value = "'1', ";
		if($nm_member['mb_birth'] != ''){
			$sql_seas_field.= " ".$seas_sex.$age_group.", ";
			$sql_seas_value.= "'1', ";
		}

		$sql_sales_episode_age_sex_insert = "
			INSERT INTO ".$dbtable."_".$comics['cm_big']."_age_sex ( ".$sql_seas_field." 
			seas_comics, seas_episode, seas_chapter, 
			seas_year_month, seas_year, seas_month, seas_day, seas_hour, seas_week 
			) VALUES ( ".$sql_seas_value."
			'".$comics['cm_no']."', '".$episode['ce_no']."', '".$episode['ce_chapter']."', 
				'".$save_date['year_month']."', '".$save_date['year']."', '".$save_date['month']."', '".$save_date['day']."', '".$save_date['hour']."', '".$save_date['week']."' 
		)";
		if(sql_query($sql_sales_episode_age_sex_insert)){
		}else{ die($sql_sales_episode_age_sex_insert); }	
	}
}


?>