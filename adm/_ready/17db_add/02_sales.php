<?
include_once '_common.php'; // 공통

$sql_sales						= " TRUNCATE `sales`; ";
$sql_sales_episode_1			= " TRUNCATE `sales_episode_1`; ";
$sql_sales_episode_2			= " TRUNCATE `sales_episode_2`; ";
$sql_sales_episode_3			= " TRUNCATE `sales_episode_3`; ";

$sql_sales_age_sex				= " TRUNCATE `sales_age_sex`; ";
$sql_sales_episode_1_age_sex	= " TRUNCATE `sales_episode_1_age_sex`; ";
$sql_sales_episode_2_age_sex	= " TRUNCATE `sales_episode_2_age_sex`; ";
$sql_sales_episode_3_age_sex	= " TRUNCATE `sales_episode_3_age_sex`; ";

/*
sql_query($sql_sales);
sql_query($sql_sales_episode_1);
sql_query($sql_sales_episode_2);
sql_query($sql_sales_episode_3);
*/


sql_query($sql_sales_age_sex);
sql_query($sql_sales_episode_1_age_sex);
sql_query($sql_sales_episode_2_age_sex);
sql_query($sql_sales_episode_3_age_sex);


// 코믹스 통계

$sql_mpec = "	select * from member_point_expen_comics 
				order by mpec_year_month, mpec_day, mpec_hour, mpec_no";
$result_mpec = sql_query($sql_mpec);
for ($i=0; $row_mpec=sql_fetch_array($result_mpec); $i++){
	
	// 나이별
	switch($row_mpec['mpec_sex']){
		case 'm': $sas_sex = "sas_man";
		break;
		case 'w': $sas_sex = "sas_woman";
		break;
		default: $sas_sex = "sas_nocertify";
		break;
	}	

	// 검색
	$sql_sales_age_sex = "
		SELECT  count(*) as total_sales_age_sex FROM sales_age_sex WHERE 1 
		AND sas_comics = '".$row_mpec['mpec_comics']."' AND sas_big  = '".$row_mpec['mpec_big']."' 
		AND `sas_year_month` = '".$row_mpec['mpec_year_month']."' AND `sas_year` = '".$row_mpec['mpec_year']."' 
		AND `sas_month` = '".$row_mpec['mpec_month']."' AND `sas_day` = '".$row_mpec['mpec_day']."' 
		AND `sas_hour` = '".$row_mpec['mpec_hour']."' AND `sas_week` = '".$row_mpec['mpec_week']."' ";
	$total_sales_age_sex = sql_count($sql_sales_age_sex, 'total_sales_age_sex');

	if($total_sales_age_sex!=0){		
		// 나이별
		$sql_sas_sex = " `".$sas_sex."`= (".$sas_sex."+".$row_mpec['mpec_count'].") ";
		if($row_mpec['mpec_sex'] == 'm' || $row_mpec['mpec_sex'] == 'w'){			
			$sql_sas_sex.= ", `".$sas_sex.$row_mpec['mpec_age']."`= (".$sas_sex.$row_mpec['mpec_age']."+".$row_mpec['mpec_count'].") ";
		}
		$sql_sales_age_sex_update = "
			UPDATE `sales_age_sex` SET ".$sql_sas_sex." WHERE 1 
			AND sas_comics = '".$row_mpec['mpec_comics']."' AND sas_big  = '".$row_mpec['mpec_big']."' 
			AND `sas_year_month` = '".$row_mpec['mpec_year_month']."' AND `sas_year` = '".$row_mpec['mpec_year']."' 
			AND `sas_month` = '".$row_mpec['mpec_month']."' AND `sas_day` = '".$row_mpec['mpec_day']."' 
			AND `sas_hour` = '".$row_mpec['mpec_hour']."' AND `sas_week` = '".$row_mpec['mpec_week']."' ";
		sql_query($sql_sales_age_sex_update);
		// echo $sql_sales_age_sex_update.";<br/>";
	}else{
		// 나이별
		$sql_sas_field = " `".$sas_sex."`, ";
		$sql_sas_value = "'".$row_mpec['mpec_count']."', ";
		if($row_mpec['mpec_sex'] == 'm' || $row_mpec['mpec_sex'] == 'w'){
			$sql_sas_field.= " `".$sas_sex.$row_mpec['mpec_age']."`, ";
			$sql_sas_value.= "'".$row_mpec['mpec_count']."', ";
		}
		$sql_sales_age_sex_insert = "
			INSERT INTO `sales_age_sex` ( ".$sql_sas_field." 
			`sas_comics`, `sas_big`, 
			`sas_year_month`, `sas_year`, `sas_month`, `sas_day`, `sas_hour`, `sas_week` 
			) VALUES ( ".$sql_sas_value." 
			'".$row_mpec['mpec_comics']."', '".$row_mpec['mpec_big']."', 
			'".$row_mpec['mpec_year_month']."', '".$row_mpec['mpec_year']."', 
			'".$row_mpec['mpec_month']."', '".$row_mpec['mpec_day']."', 
			'".$row_mpec['mpec_hour']."', '".$row_mpec['mpec_week']."' 
		)";
		sql_query($sql_sales_age_sex_insert);
		// echo $sql_sales_age_sex_insert.";<br/>";
	}
}

// 에피소드 통계

$big_arr = array(1,2,3);
foreach($big_arr as $big){

	$sql_mpee = "	select * from member_point_expen_episode_".$big." 
					order by mpee_year_month, mpee_day, mpee_hour, mpee_no";
	$result_mpee = sql_query($sql_mpee);
	for ($i=0; $row_mpee=sql_fetch_array($result_mpee); $i++){
		
		// 나이별
		switch($row_mpee['mpee_sex']){
			case 'm': $seas_sex = "seas_man";
			break;
			case 'w': $seas_sex = "seas_woman";
			break;
			default: $seas_sex = "seas_nocertify";
			break;
		}	

		// 검색 sales_episode_1_age_sex
		$sql_sales_episode_age_sex = "
			SELECT  count(*) as total_sales_episode_age_sex FROM sales_episode_".$big."_age_sex WHERE 1 
			AND seas_comics = '".$row_mpee['mpee_comics']."' 
			AND seas_episode  = '".$row_mpee['mpee_episode']."' 
			AND seas_chapter  = '".$row_mpee['mpee_chapter']."' 
			AND `seas_year_month` = '".$row_mpee['mpee_year_month']."' AND `seas_year` = '".$row_mpee['mpee_year']."' 
			AND `seas_month` = '".$row_mpee['mpee_month']."' AND `seas_day` = '".$row_mpee['mpee_day']."' 
			AND `seas_hour` = '".$row_mpee['mpee_hour']."' AND `seas_week` = '".$row_mpee['mpee_week']."' ";
		$total_sales_episode_age_sex = sql_count($sql_sales_episode_age_sex, 'total_sales_episode_age_sex');

		if($total_sales_episode_age_sex!=0){		
			// 나이별
			$sql_seas_sex = " `".$seas_sex."`= (".$seas_sex."+1) ";
			if($row_mpee['mpee_sex'] == 'm' || $row_mpee['mpee_sex'] == 'w'){			
				$sql_seas_sex.= ", `".$seas_sex.$row_mpee['mpee_age']."`= (".$seas_sex.$row_mpee['mpee_age']."+1) ";
			}
			$sql_sales_episode_age_sex_update = "
				UPDATE `sales_episode_".$big."_age_sex` SET ".$sql_seas_sex." WHERE 1 
				AND seas_comics = '".$row_mpee['mpee_comics']."' 
				AND seas_episode  = '".$row_mpee['mpee_episode']."' 
				AND seas_chapter  = '".$row_mpee['mpee_chapter']."' 
				AND `seas_year_month` = '".$row_mpee['mpee_year_month']."' AND `seas_year` = '".$row_mpee['mpee_year']."' 
				AND `seas_month` = '".$row_mpee['mpee_month']."' AND `seas_day` = '".$row_mpee['mpee_day']."' 
				AND `seas_hour` = '".$row_mpee['mpee_hour']."' AND `seas_week` = '".$row_mpee['mpee_week']."' ";
			sql_query($sql_sales_episode_age_sex_update);
			// echo $sql_sales_episode_age_sex_update.";<br/>";
		}else{
			// 나이별
			$sql_seas_field = " `".$seas_sex."`, ";
			$sql_seas_value = "'1', ";
			if($row_mpee['mpee_sex'] == 'm' || $row_mpee['mpee_sex'] == 'w'){
				$sql_seas_field.= " `".$seas_sex.$row_mpee['mpee_age']."`, ";
				$sql_seas_value.= "'1', ";
			}
			$sql_sales_episode_age_sex_insert = "
				INSERT INTO `sales_episode_".$big."_age_sex` ( ".$sql_seas_field." 
				`seas_comics`, `seas_episode`, `seas_chapter`, 
				`seas_year_month`, `seas_year`, `seas_month`, `seas_day`, `seas_hour`, `seas_week` 
				) VALUES ( ".$sql_seas_value." 
				'".$row_mpee['mpee_comics']."', '".$row_mpee['mpee_episode']."', '".$row_mpee['mpee_chapter']."', 
				'".$row_mpee['mpee_year_month']."', '".$row_mpee['mpee_year']."', 
				'".$row_mpee['mpee_month']."', '".$row_mpee['mpee_day']."', 
				'".$row_mpee['mpee_hour']."', '".$row_mpee['mpee_week']."' 
			)";
			sql_query($sql_sales_episode_age_sex_insert);
			// echo $sql_sales_episode_age_sex_insert.";<br/>";
		}
	}
}



?>