<?
include_once '_common.php'; // 공통

/* 데이터에서 해당 레벨(등급) 설정했는지 체크 */
$level_db_list = array();
$sql_fl_level = "SELECT * FROM member group by mb_level order by mb_level";
$result_fl_level = sql_query($sql_fl_level);
while ($row_fl_level = sql_fetch_array($result_fl_level)) {
	array_push($level_db_list,  $row_fl_level['mb_level']);
} // end while
$level_list = array();
array_push($level_list, mb_only(explode("|", $nm_config['cf_client_list']))); // 일반 회원
array_push($level_list, explode("|", $nm_config['cf_partner_list'])); //  파트너 회원
array_push($level_list, explode("|", $nm_config['cf_admin_list'])); // 관리자 회원

/* 등급갯수 */
$total_level = count(array_filter(array_map('trim',$nm_config['cf_level_list'])))-1;

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','mb_level_no',0));
array_push($fetch_row, array('등급','mb_level_name',10));

$grade = array();
array_push($grade, '회원');
array_push($grade, '파트너');
array_push($grade, '관리자');

$page_title = "회원 등급";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?>"/>
<script type="text/javascript" src="<?=$_cms_js;?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>총 <?=$page_title?> : <?=number_format($total_level);?> 건</strong>
	</div>
</section><!-- cms_title -->

<section id="level_result">
	<div id="cs_bg">
		<form name="level_list_form" id="level_list_form" method="post" action="<?=$_cms_update;?>" onsubmit="return level_list_submit();">
		<input type="hidden" name="level_val[]" value="<?=$nm_config['cf_level_list'][0]?>">
		<input type="hidden" name="level_val[]" value="<?=$nm_config['cf_level_list'][1]?>">
		
		<div class="align_div">
		<? foreach($level_list as $level_key => $level_val) { ?>
			<div class="float_div">
				<table>
					<thead id="cs_thead">
						<tr>
						<?
						//정렬표기
						$order_giho = "▼";
						$th_order = "desc";
						if($_order == 'desc'){ 
							$order_giho = "▲"; 
							$th_order = "asc";
						}
						foreach($fetch_row as $fetch_key => $fetch_val){
							$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
							if($fetch_val[2] == 1){
								$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
								$th_ahref_e = '</a>';
							}
							if($fetch_val[1] == $_order_field){
								$th_title_giho = $order_giho;
							}
							$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
							$th_class = $fetch_val[1];
							if($fetch_val[2] == 10){								
								$th_title = $grade[$level_key].' '.$th_title; 
							}
						?>
							<th class="<?=$th_class?>"><?=($fecth_key == 0)?$th_title:$grade[$i]." ".$th_title?></th>
						<?}?>
						</tr>
					</thead>
					<tbody>
						<?
						$i = 1;
						foreach($level_list[$level_key] as $level_key => $level_val){ ?>
						<tr class="result_hover">
							<td class="<?=$fetch_row[0][1]?> text_center">
								<?=$i;?>
								<input class="input_level_no" type="hidden" name="level_no[]" value="<?=$level_key;?>" />
							</td>
							<td class="<?=$fetch_row[1][1]?> text_center">
								<input class="input_level_val" type="text" name="level_val[]" value="<?=$level_val;?>" /> 
							</td>
						</tr>
						<? $i++; } // end foreach?>
					</tbody>
				</table>
			</div>
			<?} // end foreach ?>
				<div id="cm_level_list_submit">
					<input type="submit" value="회원 등급 내용 저장" />
				</div>
			</div>
		</form>
	</div>
</section><!-- level_result -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>