<?
include_once '_common.php'; // 공통

/* PARAMETER CHECK */
$para_list = array('mode','cup_no','cup_type','cup_won','cup_member','cup_id','cup_date');

/* 숫자 PARAMETER 체크 */
$para_num_list = array('cup_no', 'cup_won', 'cup_member', 'cup_type');

/* 빈칸 PARAMETER 허용 */
$null_list = array(); // 사용 안 함

/* notice_write 숫자검사하면서 $_notice_write로 값 대입  */
foreach($para_list as $para_key => $para_val){
	if(in_array($para_val,$para_num_list)){
		if(num_check($_POST[$para_val]) == false && $_POST[$para_val] != '' && $_POST[$para_val] != '0'){
			alert($para_val."값에 숫자가 아닌 값이 들어 있습니다.", $_SERVER['HTTP_REFERER']);
			die;
		} // end if
	} // end if
	${'_'.$para_val} = base_filter($_POST[$para_val]); /* 변수담기 */
} // end foreach

$dbtable = "config_unit_pay";
$dbt_primary = "cup_no";
$para_primary = "cup_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

/* DB field 아닌 목록 */
$db_field_exception = array('mode'); 

if($_mode == 'reg'){
	/* 고정값 */
	$_cup_date = NM_TIME_YMDHIS; /* 최초등록일 */
	$cup_type_sql = "SELECT MAX( cup_type ) cup_type FROM config_unit_pay"; /* cup_type 최대값 */
	$cup_type_result = sql_query($cup_type_sql); 
	$cup_type_row = sql_fetch_array($cup_type_result);
	$_cup_type = $cup_type_row['cup_type'] + 1;
	
	/* field명 */
	$sql_reg = 'INSERT INTO '.$dbtable.'(';
	foreach($para_list as $para_key => $para_val){
		if(in_array($para_val, $db_field_exception)){ continue;} /* DB field 아닌 목록 제외 */
		if(${"_".$para_val} == "" && in_array($para_val, $null_list) == false) { continue; } // DB field에 값 없는거 빼기

		/* sql문구 */
		$sql_reg.= $para_val.', ';
	}
	$sql_reg = substr($sql_reg,0,strrpos($sql_reg, ","));

	/* VALUES 시작 */
	$sql_reg.= ' )VALUES( ';

	/* field값 */
	foreach($para_list as $para_key => $para_val){
		if(in_array($para_val, $db_field_exception)){continue;} /* DB field 아닌 목록 제외 */
		if(${"_".$para_val} == "" && in_array($para_val, $null_list) == false) { continue; } // DB field에 값 없는거 빼기

		/* sql문구 */
		$sql_reg.= '"'.${'_'.$para_val}.'", ';
	}
	$sql_reg = substr($sql_reg,0,strrpos($sql_reg, ","));

	/* SQL문 마무리 */
	$sql_reg.= ' ) ';

	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = '정산 금액이 등록되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

/* 수정 */
}else if($_mode == 'mod'){
	$_cup_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$dbt_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$dbt_primary.'값이 없습니다.';
	}else{
		
		$sql_mod = ' UPDATE '.$dbtable.' SET ';
		foreach($para_list as $para_key => $para_val){
			if(in_array($para_val, $db_field_exception)){continue;} /* DB field 아닌 목록 제외 */
		if(${"_".$para_val} == "" && in_array($para_val, $null_list) == false) { continue; } // DB field에 값 없는거 빼기
			if($para_val == $para_primary || $para_val == $dbt_primary){ continue; } /* where문 빼기 */

			/* sql문구 */
			$sql_mod.= $para_val." = '".${'_'.$para_val}."', ";
		}

		$sql_mod = substr($sql_mod,0,strrpos($sql_mod, ","));
		$sql_mod.= ' WHERE '.$dbt_primary.'="'.${'_'.$dbt_primary}.'"';
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = '정산 금액이 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$dbt_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$dbt_primary.'값이 없습니다.';
	}else{
		/* 데이터 삭제 */
		$sql_del = 'delete from '.$dbtable.' where '.$dbt_primary.'="'.${'_'.$dbt_primary}.'"';
		sql_query($sql_del);		
		$db_result['msg'] = '정산 금액이 삭제되였습니다.';
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
pop_close($db_result['msg'], '', $db_result['error']);
die;

?>