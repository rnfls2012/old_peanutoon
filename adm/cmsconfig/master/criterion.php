<?
include_once '_common.php'; // 공통

/* PARAMITER 

*/

// 기본적으로 몇개 있는 지 체크
$sql_cup_total = "select count(*) as total_cup from config_unit_pay where 1";
$total_ask = sql_count($sql_cup_total, 'total_cup');

/* 사용중인 정산 타입 체크 */
// 1. config
$config_sql = "select * from config";
$config_result = sql_query($config_sql);
$select_cup = sql_fetch_array($config_result);

// 2. sales에서 사용중인 정산 타입 체크
$sales_cup = array();
$sales_sql = "select * from sales group by sl_cash_type";
$sales_result = sql_query($sales_sql);
while($row = sql_fetch_array($sales_result)) {
	array_push($sales_cup, $row['sl_cash_type']);
} // end while

/* 데이터 가져오기 */
$cup_where = "";

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "cup_date"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$cup_order = "order by ".$_order_field." ".$_order;

$sql_cup = "";
$field_cup = " * "; // 가져올 필드 정하기

$sql_cup = "select $field_cup FROM config_unit_pay where 1 $cup_where $cup_order";
$result = sql_query($sql_cup);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','cup_no',0));
array_push($fetch_row, array('정산 타입','cup_type',0));
array_push($fetch_row, array('정산 금액','cup_won',0));
array_push($fetch_row, array('저장 회원','cup_id',0));
array_push($fetch_row, array('저장일','cup_date',0));
array_push($fetch_row, array('관리','cup_management',0));

// if($_s_limit == ''){ $_s_limit = 30; }

$page_title = "정산 금액";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title." 설정";
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?>"/>
<script type="text/javascript" src="<?=$_cms_js;?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>총 <?=$page_title?> : <?=number_format($total_ask);?> 개</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','cup_write', 600, 250);"><?=$page_title?> 추가</button>
	</div>
</section><!-- cms_title -->

<section id="cup_result">
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $cp_css.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<input type="hidden" class="admin_id" name="admin_id" id="admin_id" value="<?=$admin_id;?>">
				<?foreach($row_data as $dvalue_key => $dvalue_val){	
					
					/* 수정/삭제 */
					$popup_url = $_cms_write."?cup_no=".$dvalue_val['cup_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					/* 삭제버튼 */
					$del_btn_use = "ok";
					if(($select_cup['cf_cup_type'] == $dvalue_val['cup_type']) || in_array($dvalue_val['cup_type'], $sales_cup)){
						$del_btn_use = "";
					} // end if

				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['cup_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$dvalue_val['cup_type'];?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$dvalue_val['cup_won'];?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$dvalue_val['cup_id'];?></td>
					<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['cup_date'];?></td>
					<td class="<?=$fetch_row[5][1]?> text_center">
						<?if($del_btn_use == "ok"){?>
							<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','cup_mod_write', 600, 250);">수정</button>
							<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','cup_del_write', 600, 250);">삭제</button>
						<?}else{?>삭제/수정 불가<?}?>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- cup_result -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>