<?
include_once '_common.php'; // 공통

// 로그인 회원 정보 저장
/* 세션에 저장된 값이 없으므로 테스트.. */
if($nm_member['mb_id']) {
	$admin_id = $nm_member['mb_id'];
} else {
	$admin_id = "leeih";
} // end else

$admin_no_sql = "select mb_no from member where mb_id = '$admin_id'";
$admin_no_result = sql_query($admin_no_sql);
$admin_no_row = mysql_fetch_array($admin_no_result);
$admin_no = $admin_no_row['mb_no'];

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_cup_no = tag_filter($_REQUEST['cup_no']);

$sql = "select * from config_unit_pay where cup_no = '$_cup_no'";

/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */} // end if
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
	break;
} // end switch

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}

/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "정산 금액";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>

<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- cms_notice_title -->

<section id="cup_write">
	<form name="cup_write_form" id="cup_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_folder;?>/criterion.update.php" onsubmit="return cup_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/> <!-- 입력모드 -->
		<input type="hidden" id="cup_no" name="cup_no" value="<?=$_cup_no;?>"/><!-- 수정/삭제시 글 번호 -->
		<input type="hidden" id="cup_member" name="cup_member" value="<?=$admin_no;?>"/><!-- 접속자 회원 번호 -->
		<input type="hidden" id="cup_id" name="cup_id" value="<?=$admin_id;?>"/><!-- 접속자 ID -->

		<table>
			<tbody>
				<tr>
					<th><label for="cup_won"><?=$required_arr[0]?>정산 금액</label></th>
					<td>
						<input type="text" placeholder="금액을 입력해주세요<?=$required_arr[2]?>" <?=$required_arr[1]?> 
							name="cup_won" id="cup_won" value="<?=$row['cup_won'];?>" autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="submit_btn">
						<input type="submit" value="<?=$mode_text?>">
						<input type="reset" value="취소" onclick="self.close()">
					</td>
				</tr>
			</tbody>
		</table>
	 </form>
</section><!-- cup_write -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>