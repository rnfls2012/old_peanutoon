<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode','cf_no');
array_push($para_list, 'cf_title','cf_admin_email','cf_admin_email_name');
array_push($para_list, 'cf_company','cf_ceo','cf_business_no', 'cf_mail_order_sales','cf_address','cf_tel','cf_copy');
array_push($para_list, 'cf_appsetapk','cf_appmarket');
array_push($para_list, 's_limit','s_limit_min','s_limit_max');

array_push($para_list, 'cf_pay_basic');
array_push($para_list, 'cf_cash_point_unit','cf_point_unit','cf_cash_point_unit_ko','cf_point_unit_ko');
array_push($para_list, 'cf_cup_type');
array_push($para_list, 'cf_certify_kcp_mode','cf_certify_kcp_test_id');
array_push($para_list, 'cf_pg_kcp_mode','cf_pg_kcp_test_id');
array_push($para_list, 'cf_pg_toss_mode','cf_pg_toss_test_id');
array_push($para_list, 'cf_pg_payco_mode','cf_pg_payco_test_id');

array_push($para_list, 'cf_pay');
array_push($para_list, 'cf_comics_type');
array_push($para_list, 'cf_er_type');
array_push($para_list, 'cf_menu');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'cf_certify_kcp_test_id', 'cf_pg_kcp_test_id', 'cf_pg_toss_test_id', 'cf_pg_payco_test_id');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'cf_no','cf_cup_type', 's_limit','s_limit_min','s_limit_max','cf_pay_basic');
array_push($para_num_list, 'cf_menu');

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

$cf_pay_arr = array();
foreach($nm_config['cf_pay'] as $cf_pay_key => $cf_pay_val){	
	if(num_check($_POST['cf_pay'.$cf_pay_key]) == false && $_POST['cf_pay'.$cf_pay_key] != '' && $_POST['cf_pay'.$cf_pay_key] != '0'){
		alert($cf_pay_val."값에 숫자가 아닌 값이 들어 있습니다.", $_SERVER['HTTP_REFERER']);
		die;
	}
	$cf_pay_arr[$cf_pay_key] = base_filter(get_int($_POST['cf_pay'.$cf_pay_key]));
}
$_cf_pay = implode("|",$cf_pay_arr);

$cf_comics_type_arr = array();
foreach($nm_config['cf_comics_type'] as $comics_type_key => $comics_type_val){
	$cf_comics_type_arr[$comics_type_key] = base_filter($_POST['cf_comics_type'.$comics_type_key]);
	if($comics_type_key == 0){ $cf_comics_type_arr[0] = $nm_config['cf_comics_type'][0][1];  }
}
$_cf_comics_type = implode("|",$cf_comics_type_arr);

$cf_er_type_arr = array();
foreach($nm_config['cf_er_type'] as $er_type_key => $er_type_val){
	$cf_er_type_arr[$er_type_key] = base_filter($_POST['cf_er_type'.$er_type_key]);
}
$_cf_er_type = implode("|",$cf_er_type_arr);

$dbtable = "config";
$dbt_primary = "cf_no";
$para_primary = "cf_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

/* 수정 */
if($_mode == 'mod'){
	$_er_mod_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = '기본설정 데이터가 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}


/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
goto_url($_SERVER['HTTP_REFERER']);
die;

?>