<?
include_once '_common.php'; // 공통
include_once NM_ADM_PATH.'/_adm.lib.php'; // 공통
/* _array_update.php 처리 불가능 */

/* PARAMITER CHECK */
$para_list = array('mode');
array_push($para_list, 'cfmb_no1', 'cfmb_no2', 'cfmb_no3');

/* 숫자 PARAMITER 체크 */
$para_num_list = array();

// 고객 회원 혜택
foreach(mb_only($nm_config['cf_level_list']) as $cf_mbllevel_key => $cf_mbllevel_val){
	if($cf_mbllevel_val == ''){ continue; }
	if($cf_mbllevel_key >= $nm_config['cf_partner_level']){ continue; }
	array_push($para_list, 'cfmb_level_premium'.$cf_mbllevel_key);
	array_push($para_num_list, 'cfmb_level_premium'.$cf_mbllevel_key); // 숫자 PARAMITER 체크

}

// 파트너 회원 메뉴권한
foreach(mb_only($nm_config['cf_level_list']) as $cf_mbllevel_key => $cf_mbllevel_val){
	${'cfmb_class_permission'.$cf_mbllevel_key} = '';
	if($cf_mbllevel_val == ''){ continue; }
	if($cf_mbllevel_key < $nm_config['cf_partner_level']){ continue; }
	if($cf_mbllevel_key >= $nm_config['cf_admin_level']){ continue; }
	foreach($menu as $menu_key => $menu_val) {
		$para_value = $menu_val[0][2];
		array_push($para_list, 'cfmb_level_permission'.$cf_mbllevel_key.'_'.$para_value);
		foreach($menu_val as $menu2_key => $menu2_val){
			if(!is_array($menu2_val[3])){continue;}
			if($menu2_key == 0){continue;}
			$mbv_total = count($menu2_val);
			for($mb_i=3; $mb_i<= $mbv_total; $mb_i++){
				unset($menu2_val[$mb_i]);
			}
			$para2_value = $menu2_val[2];
			array_push($para_list, 'cfmb_level_permission'.$cf_mbllevel_key.'_'.$para2_value);
		}	
	}
}

// 관리자 회원 메뉴권한
foreach(mb_only($nm_config['cf_level_list']) as $cf_mbllevel_key => $cf_mbllevel_val){
if($cf_mbllevel_val == ''){ continue; }
if($cf_mbllevel_key < $nm_config['cf_admin_level']){ continue; }
if($cf_mbllevel_key >= $nm_config['cf_admin_level']+8){ continue; }

	foreach($menu as $menu_key => $menu_val) {
		$para_value = $menu_val[0][2];
		array_push($para_list, 'cfmb_level_permission'.$cf_mbllevel_key.'_'.$para_value);
		foreach($menu_val as $menu2_key => $menu2_val){
			if(!is_array($menu2_val[3])){continue;}
			if($menu2_key == 0){continue;}
			$mbv_total = count($menu2_val);
			for($mb_i=3; $mb_i<= $mbv_total; $mb_i++){
				unset($menu2_val[$mb_i]);
			}
			$para2_value = $menu2_val[2];
			array_push($para_list, 'cfmb_level_permission'.$cf_mbllevel_key.'_'.$para2_value);
		}	
	}
}

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
foreach($para_list as $para_key => $para_val){
	if(in_array($para_val,$para_num_list)){
		if(num_check($_POST[$para_val]) == false && $_POST[$para_val] != '' && $_POST[$para_val] != '0'){
			alert($para_val."값에 숫자가 아닌 값이 들어 있습니다.", $_SERVER['HTTP_REFERER']);
			die;
		}
	}
	${'_'.$para_val} = base_filter($_POST[$para_val]); /* 변수담기 */
}

// echo $_cfmb_level_permission13_channel;
// cfmb_class_permission 재정리

// 파트너 회원 메뉴권한
foreach(mb_only($nm_config['cf_level_list']) as $cf_mbllevel_key => $cf_mbllevel_val){
	$loop_count = 0;
	if($cf_mbllevel_val == ''){ continue; }
	if($cf_mbllevel_key < $nm_config['cf_partner_level']){ continue; }
	if($cf_mbllevel_key >= $nm_config['cf_admin_level']){ continue; }
	${'_cfmb_level_permission'.$cf_mbllevel_key} = '';
	foreach($menu as $menu_key => $menu_val) {
		$para_value = $menu_val[0][2];
		${'_cfmb_level_permission'.$cf_mbllevel_key} .= ${'_cfmb_level_permission'.$cf_mbllevel_key.'_'.$para_value}."|";
		$loop_count++;
		foreach($menu_val as $menu2_key => $menu2_val){
			if(!is_array($menu2_val[3])){continue;}
			if($menu2_key == 0){continue;}
			$mbv_total = count($menu2_val);
			for($mb_i=3; $mb_i<= $mbv_total; $mb_i++){
				unset($menu2_val[$mb_i]);
			}
			$para2_value = $menu2_val[2];
			${'_cfmb_level_permission'.$cf_mbllevel_key} .= ${'_cfmb_level_permission'.$cf_mbllevel_key.'_'.$para2_value}."|";
			$loop_count++;
		}	
	}
	${'_cfmb_level_permission'.$cf_mbllevel_key} = substr(${'_cfmb_level_permission'.$cf_mbllevel_key},0,strrpos(${'_cfmb_level_permission'.$cf_mbllevel_key}, "|"));
	$loop_count--;
	// echo $cf_mbllevel_key.' : '.${'_cfmb_level_permission'.$cf_mbllevel_key}."<br/>";

	if(${'_cfmb_level_permission'.$cf_mbllevel_key} == str_repeat("|", $loop_count)){
		// echo $cf_mbllevel_key."<br/>";
		${'_cfmb_level_permission'.$cf_mbllevel_key} = '';
	}
}

// 관리자 회원 메뉴권한
foreach(mb_only($nm_config['cf_level_list']) as $cf_mbllevel_key => $cf_mbllevel_val){
	$loop_count = 0;
	if($cf_mbllevel_val == ''){ continue; }
	if($cf_mbllevel_key < $nm_config['cf_admin_level']){ continue; }
	if($cf_mbllevel_key >= $nm_config['cf_admin_level']+8){ continue; }
	${'_cfmb_level_permission'.$cf_mbllevel_key} = '';
	foreach($menu as $menu_key => $menu_val) {
		$para_value = $menu_val[0][2];
		${'_cfmb_level_permission'.$cf_mbllevel_key} .= ${'_cfmb_level_permission'.$cf_mbllevel_key.'_'.$para_value}."|";
		$loop_count++;
		foreach($menu_val as $menu2_key => $menu2_val){
			if(!is_array($menu2_val[3])){continue;}
			if($menu2_key == 0){continue;}
			$mbv_total = count($menu2_val);
			for($mb_i=3; $mb_i<= $mbv_total; $mb_i++){
				unset($menu2_val[$mb_i]);
			}
			$para2_value = $menu2_val[2];
			${'_cfmb_level_permission'.$cf_mbllevel_key} .= ${'_cfmb_level_permission'.$cf_mbllevel_key.'_'.$para2_value}."|";
			$loop_count++;
		}	
	}
	${'_cfmb_level_permission'.$cf_mbllevel_key} = substr(${'_cfmb_level_permission'.$cf_mbllevel_key},0,strrpos(${'_cfmb_level_permission'.$cf_mbllevel_key}, "|"));
	$loop_count--;
	// echo $cf_mbllevel_key.' : '.${'_cfmb_level_permission'.$cf_mbllevel_key}."<br/>";

	if(${'_cfmb_level_permission'.$cf_mbllevel_key} == str_repeat("|", $loop_count)){
		// echo $cf_mbllevel_key."<br/>";
		${'_cfmb_level_permission'.$cf_mbllevel_key} = '';
	}
}

$dbtable = "config_member_buff";

// UPDATE
// class 회원분류만
$no = 0;
foreach($d_mb_class as $mb_class_key => $mb_class_val){
	$no++;

	$sql_mod = ' UPDATE '.$dbtable.' SET ';
	if(${'_cfmb_no'.$no} == ''){
		$sql_mod.= "cfmb_class_permission = NULL, ";
	}else{
		$sql_mod.= "cfmb_class_permission = '".${'_cfmb_no'.$no}."', ";
	}
	$sql_mod = substr($sql_mod,0,strrpos($sql_mod, ","));
	$sql_mod.= " WHERE cfmb_member_class = '".$mb_class_key."'";

	/* DB 저장 */
	// echo $sql_mod."<br/>";
	if(sql_query($sql_mod)){
		$db_result['msg'] = '데이터가 수정되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_mod;
	}
}

// 고객 회원 혜택
foreach(mb_only($nm_config['cf_level_list']) as $cf_mbllevel_key => $cf_mbllevel_val){
	if($cf_mbllevel_val == ''){ continue; }
	if($cf_mbllevel_key >= $nm_config['cf_partner_level']){ continue; }

	$sql_mod = ' UPDATE '.$dbtable.' SET ';
	if(${'_cfmb_level_premium'.$cf_mbllevel_key} == ''){
		$sql_mod.= "cfmb_level_premium = NULL, ";
	}else{
		$sql_mod.= "cfmb_level_premium = '".${'_cfmb_level_premium'.$cf_mbllevel_key}."', ";
	}
	$sql_mod = substr($sql_mod,0,strrpos($sql_mod, ","));
	$sql_mod.= " WHERE cfmb_member_level = '".$cf_mbllevel_key."'";

	/* DB 저장 */
	// echo $sql_mod."<br/>";
	if(sql_query($sql_mod)){
		$db_result['msg'] = '데이터가 수정되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_mod;
	}
}

// 파트너 회원 메뉴권한
foreach(mb_only($nm_config['cf_level_list']) as $cf_mbllevel_key => $cf_mbllevel_val){
	if($cf_mbllevel_val == ''){ continue; }
	if($cf_mbllevel_key < $nm_config['cf_partner_level']){ continue; }
	if($cf_mbllevel_key >= $nm_config['cf_admin_level']){ continue; }

	// echo $cf_mbllevel_key.' : '.${'_cfmb_level_permission'.$cf_mbllevel_key}."<br/>";

	$sql_mod = ' UPDATE '.$dbtable.' SET ';
	if(${'_cfmb_level_permission'.$cf_mbllevel_key} == ''){
		$sql_mod.= "cfmb_level_permission = NULL, ";
	}else{
		$sql_mod.= "cfmb_level_permission = '".${'_cfmb_level_permission'.$cf_mbllevel_key}."', ";
	}
	$sql_mod = substr($sql_mod,0,strrpos($sql_mod, ","));
	$sql_mod.= " WHERE cfmb_member_level = '".$cf_mbllevel_key."'";

	/* DB 저장 */
	// echo $sql_mod."<br/>";
	if(sql_query($sql_mod)){
		$db_result['msg'] = "데이터가 수정되였습니다.";
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_mod;
	}
}

// 관리자 회원 메뉴권한
foreach(mb_only($nm_config['cf_level_list']) as $cf_mbllevel_key => $cf_mbllevel_val){
	if($cf_mbllevel_val == ''){ continue; }
	if($cf_mbllevel_key < $nm_config['cf_admin_level']){ continue; }
	if($cf_mbllevel_key >= $nm_config['cf_admin_level']+8){ continue; }

	// echo $cf_mbllevel_key.' : '.${'_cfmb_level_permission'.$cf_mbllevel_key}."<br/>";

	$sql_mod = ' UPDATE '.$dbtable.' SET ';
	if(${'_cfmb_level_permission'.$cf_mbllevel_key} == ''){
		$sql_mod.= "cfmb_level_permission = NULL, ";
	}else{
		$sql_mod.= "cfmb_level_permission = '".${'_cfmb_level_permission'.$cf_mbllevel_key}."', ";
	}
	$sql_mod = substr($sql_mod,0,strrpos($sql_mod, ","));
	$sql_mod.= " WHERE cfmb_member_level = '".$cf_mbllevel_key."'";

	/* DB 저장 */
	// echo $sql_mod."<br/>";
	if(sql_query($sql_mod)){
		$db_result['msg'] = '데이터가 수정되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_mod;
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
//goto_url($_SERVER['HTTP_REFERER']);
alert($db_result['msg'], $_SERVER['HTTP_REFERER']);
die;

?>