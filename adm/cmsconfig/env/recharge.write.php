<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_crp_no = tag_filter($_REQUEST['crp_no']);

$sql = "SELECT * FROM `config_recharge_price` WHERE  `crp_no` = '$_crp_no' ";
	
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}
/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "충전 금액";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- event_recharge_head -->

<section id="config_recharge_price_write">
	<form name="config_recharge_price_write_form" id="config_recharge_price_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return config_recharge_price_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="crp_no" name="crp_no" value="<?=$_crp_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="crp_no">충전금액 번호</label></th>
					<td><?=$_crp_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="crp_won">충전 가격</label></th>
					<td>
						<input type="text" class="onlynumber text_rt_130" placeholder="숫자만 입력해주세요" name="crp_won" id="crp_won" value="<?=$row['crp_won'];?>" autocomplete="off" maxlength="10" /> 
						<label for="crp_won">원</label>
					</td>
				</tr>
				<tr>
					<th><label for="crp_cash_point">충전 <?=$nm_config['cf_cash_point_unit_ko'];?>(<?=$nm_config['cf_cash_point_unit'];?>)</label></th>
					<td>
						<input type="text" class="onlynumber text_rt_130" placeholder="숫자만 입력해주세요" name="crp_cash_point" id="crp_cash_point" value="<?=$row['crp_cash_point'];?>" autocomplete="off" maxlength="7" onchange="crp_point_percent_calc();" /> 
						<label for="crp_cash_point"><?=$nm_config['cf_cash_point_unit'];?></label>
					</td>
				</tr>
				<tr>
					<th>
						<label for="crp_point_percent">보너스 퍼센트</label>
						<br/><br/>						
						<label for="crp_point">충전 <?=$nm_config['cf_point_unit_ko'];?>(<?=$nm_config['cf_point_unit'];?>)</label>
						<p class="calc_text">&nbsp;</p>
					</th>
					<td>
						<input type="text" class="onlynumber text_rt_130" placeholder="숫자만 입력해주세요" name="crp_point_percent" id="crp_point_percent" value="<?=$row['crp_point_percent'];?>" autocomplete="off" maxlength="7" onchange="crp_point_percent_calc();" /> 
						<label for="crp_cash_point">%</label>
						<br/><br/>
						<input type="text" class="onlynumber text_rt_130" placeholder="위 %에 자동계산" name="crp_point" id="crp_point" value="<?=$row['crp_point'];?>" autocomplete="off" maxlength="5" /> 
						<label for="crp_point">
							<?=$nm_config['cf_point_unit'];?>
						</label>
						<p class="calc_text">※ 위 계산: [{(충전<?=$nm_config['cf_cash_point_unit_ko'];?> / 100) * 보너스 퍼센트} => 정수 반환 )]  * 10[<?=$nm_config['cf_cash_point_unit_ko'];?>값의 10분의1이므로...]</p>
					</td>
				</tr>
				<tr>
					<th><label for="crp_payway_limit">결제수단제약</label></th>
					<td>
						<div id="payway_limit" class="payway_limit input_btn">
						<?  foreach($nm_config['cf_payway'] as $payway_key => $payway_val){ 
								$d_crp_payway_limit[$payway_val[1]] = $payway_val[2];
							}
							tag_checkbox($d_crp_payway_limit, "crp_payway_limit[]", $row['crp_payway_limit'], 'y', '제약없음', '', 5); ?>
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>충전금액 기간</label></th>
					<td>
						<div id="service_color" class="input_btn">
							<input type="radio" name="crp_date_type" id="crp_date_type0" value="y" <?=get_checked('', $row['crp_date_type']);?> <?=get_checked('y', $row['crp_date_type']);?>  onclick="crp_date_type_chk(this.value);" />
							<label for="crp_date_type0">기간만적용</label>
							<input class="still" type="radio" name="crp_date_type" id="crp_date_type1" value="m" <?=get_checked('m', $row['crp_date_type']);?> onclick="crp_date_type_chk(this.value);" />
							<label class="still" for="crp_date_type1">매월(날짜빈칸시 무기간)</label>
							<input class="still" type="radio" name="crp_date_type" id="crp_date_type2" value="w" <?=get_checked('w', $row['crp_date_type']);?> onclick="crp_date_type_chk(this.value);" />
							<label class="still" for="crp_date_type2">매주(날짜빈칸시 무기간)</label>
							<input type="radio" name="crp_date_type" id="crp_date_type3" value="n" <?=get_checked('n', $row['crp_date_type']);?> onclick="crp_date_type_chk(this.value);" />
							<label for="crp_date_type3">무기간</label>
						</div>
						<div class="<? echo ($row['crp_date_type'] != 'n')?"":"still";?> crp_date_type_view margin_top10">
							<span for="s_date">시작일 :</span>
							<input type="text" class="s_date readonly" placeholder="클릭하세요" name="crp_date_start" id="s_date" value="<?=$row['crp_date_start'];?>" autocomplete="off" readonly /> ~ 
							<span for="e_date">종료일 :</span>
							<input type="text" class="e_date readonly" placeholder="클릭하세요" name="crp_date_end" id="e_date" value="<?=$row['crp_date_end'];?>" autocomplete="off" readonly />
						</div>
						<div class="crp_holiday_view still margin_top10">
							<span for="crp_holiweek">매월</span>
							<?	tag_selects($day_list, "crp_holiday", $row['crp_holiday'], 'y', '사용안함', ''); ?>
						</div>
						<div class="crp_holiweek_view still margin_top10">
							<span for="crp_holiweek">매주</span>
							<?	tag_selects($week_list, "crp_holiweek", $row['crp_holiweek'], 'y', '사용안함', ''); ?>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="crp_cash_count_event">회원 결제횟수 이벤트</label></th>
					<td>
						<input type="text" class="onlynumber text_rt_130" placeholder="숫자만 입력해주세요" name="crp_cash_count_event" id="crp_cash_count_event" value="<?=$row['crp_cash_count_event'];?>" autocomplete="off" maxlength="5" /> 
						<label for="crp_cash_count_event">횟수</label>
					</td>
				</tr>
				<tr>
					<th><label for="crp_cash_count_event_text">회원 결제횟수 <br/>이벤트 멘트</label></th>
					<td>
						<input type="text" class="" placeholder="짧게 입력해주세요" name="crp_cash_count_event_text" id="crp_cash_count_event_text" value="<?=$row['crp_cash_count_event_text'];?>" autocomplete="off" /> 
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="crp_state">충전금액 상태</label></th>
					<td><?=$d_crp_state[$row['crp_state']];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="crp_date">충전금액 저장일</label></th>
					<td><?=$row['crp_date'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- config_recharge_price_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>