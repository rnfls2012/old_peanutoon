<?
include_once '_common.php'; // 공통
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode','csa_no');
array_push($para_list, 'csa_buy_count','csa_buy_than','csa_point');
array_push($para_list, 'csa_date_type','csa_state','csa_date_start','csa_date_end');
array_push($para_list, 'csa_holiday','csa_holiweek');
array_push($para_list, 'csa_date');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'csa_no','csa_buy_count','csa_point');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'csa_cash_point','csa_point','csa_point_percent','csa_payway_limit');
array_push($blank_list, 'csa_date_type','csa_state','csa_date_start','csa_date_end');
array_push($blank_list, 'csa_holiday','csa_holiweek');
array_push($blank_list, 'csa_cash_count_event', 'csa_cash_count_event_text');

/* 빈칸 PARAMITER 시 NULL 처리 */
array_push($null_list, 'csa_holiday','csa_holiweek');

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

// 상태와 날짜
$get_date_type = get_date_type($_csa_date_type, $_csa_date_start, $_csa_date_end);
$_csa_state = $get_date_type['state'];
$_csa_date_start = $get_date_type['date_start'];
$_csa_date_end = $get_date_type['date_end'];

$dbtable = "config_sin_allpay";
$dbt_primary = "csa_no";
$para_primary = "csa_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

if($_mode == 'reg'){	
	/* 고정값 */
	$_csa_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);
	
	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = '충전/지급 데이터가 등록되였습니다.';
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '등록 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

/* 수정 */
}else if($_mode == 'mod'){
	$_csa_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $_csa_no.'의 데이터가 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 데이터 삭제 */
		sql_query("delete from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
		
		$db_result['msg'] = $_csa_no.'의 데이터가 삭제되였습니다.';
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
pop_close($db_result['msg'], '', $db_result['error']);
die;

?>