<?
include_once '_common.php'; // 공통


$sql_config = "SELECT * FROM  `config`";
$row = sql_fetch($sql_config);


$admin_arr = $partner_arr = array();
$admin_level_arr_tmp = $admin_level_arr = $partner_level_arr_tmp = $partner_level_arr = array();
$sql_mb_class = "	SELECT * FROM  member 
					WHERE  mb_level >= '".$nm_config['cf_admin_leve']."' 
					AND  (mb_class =  'a' OR mb_class =  'p')
					ORDER BY  member.mb_level DESC 
";
$result_mb_class = sql_query($sql_mb_class);
for($c=1; $row_mb_class = sql_fetch_array($result_mb_class); $c++){
	if($row_mb_class['mb_class'] == 'a'){
		array_push($admin_level_arr_tmp, $row_mb_class['mb_level']);
		array_push($admin_arr, $row_mb_class);
	}else{
		array_push($partner_level_arr_tmp, $row_mb_class['mb_level']);
		array_push($partner_arr, $row_mb_class);
	}
}

// 레벨 중복 제거
$admin_level_arr = array_unique($admin_level_arr_tmp);
$partner_level_arr = array_unique($partner_level_arr_tmp);


/* 정산 리스트 가져오기 */
$cup_arr = array();
$sql_cup= "	SELECT * FROM  `config_unit_pay` ORDER BY  `cup_type`  ";
$result_cup = sql_query($sql_cup);
while ($row_cup = mysql_fetch_array($result_cup)) {
	// array_push($cup_arr,$row_cup);
	$temp[$row_cup['cup_type']] = $row_cup['cup_won'];
	$cup_arr = $temp;
}

/* 대분류 리스트 */
$cf_comics_big_arr = explode("|", $row['cf_big']);

/* 컨텐츠유형 */
$cf_comics_type_arr = explode("|", $row['cf_comics_type']);

/* 충전이벤트타입 */
$cf_er_type_arr = explode("|", $row['cf_er_type']);

$cf_submit_btn = '<div class="cf_submit_btn">
    <input type="submit" value="전체 저장" class="btn_submit" accesskey="s">
</div>';

$page_title = "기본설정";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
</section><!-- cms_title -->


<section id="cmsconfig_result">
	<div id="cr_bg">
		<form name="env_basic_write_form" id="env_basic_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return env_basic_write_submit(this);" >
		<input type="hidden" id="mode" name="mode" value="mod"/>	<!-- 수정모드 -->
		<input type="hidden" id="cf_no" name="cf_no" value="1"/>		<!-- cf_no모드 -->
			
			<table>
				<tr>
					<th>사이트 정보</th>
					<td class="cf_info">
						<p>
							<label for="cf_title">사이트명 : </label>
							<input type="text" name="cf_title" value="<?=$row['cf_title'];?>" id="cf_title" />

							<label for="cf_tel" class="mg_left">기업 전화번호 : </label>
							<input type="text" name="cf_tel" value="<?=$row['cf_tel'];?>" id="cf_tel" />
						</p>
						<p>
							<label for="cf_admin_email">관리자 이메일 : </label>
							<input type="text" name="cf_admin_email" value="<?=$row['cf_admin_email'];?>" id="cf_admin_email" />

							<label for="cf_admin_email_name" class="mg_left">이메일명 : </label>
							<input type="text" name="cf_admin_email_name" value="<?=$row['cf_admin_email_name'];?>" id="cf_admin_email_name" />
						</p>
						<p>
							<label for="cf_company">기업명 : </label>
							<input type="text" name="cf_company" value="<?=$row['cf_company'];?>" id="cf_company" />

							<label for="cf_ceo" class="mg_left">사업자명 : </label>
							<input type="text" name="cf_ceo" value="<?=$row['cf_ceo'];?>" id="cf_ceo" />
						</p>
						<p>
							<label for="cf_business_no">사업자등록번호 : </label>
							<input type="text" name="cf_business_no" value="<?=$row['cf_business_no'];?>" id="cf_business_no" />

							<label for="cf_mail_order_sales" class="mg_left">통신판매업신고번호 : </label>
							<input type="text" name="cf_mail_order_sales" value="<?=$row['cf_mail_order_sales'];?>" id="cf_mail_order_sales" />
						</p>
						<p>
							<label for="cf_address">기업주소 : </label>
							<input class="long_text" type="text" name="cf_address" value="<?=$row['cf_address'];?>" id="cf_address" />
						</p>
						<p>
							<label for="cf_copy">COPYRIGHT : </label>
							<input class="long_text" type="text" name="cf_copy" value="<?=$row['cf_copy'];?>" id="cf_copy" />
						</p>
					</td>
				</tr>
				<tr>
					<th>App 버젼</th>
					<td>
						<p>
							<label for="cf_appsetapk">App Apk 버젼 : </label>
							<input type="text" name="cf_appsetapk" value="<?=$row['cf_appsetapk'];?>" id="cf_appsetapk" />
						</p>
						<p>
							<label for="cf_appmarket">App Market 버젼 : </label>
							<input type="text" name="cf_appmarket" value="<?=$row['cf_appmarket'];?>" id="cf_appmarket" />
						</p>
					</td>
				</tr>
				<tr>
					<th>관리자 리스트</th>
					<td>
						<p class="blue">관리자 분류 권한 : 코믹스결제시-통계제외, CMS접근가능, 코믹스무료제공</p>
						<?if(count($admin_arr) > 0){?>
						<table>
							<? foreach($admin_level_arr as $admin_level_key => $admin_level_val){ ?>
							<tr>
								<th><?=$nm_config['cf_level_list'][$admin_level_val];?>(<?=$admin_level_val;?>)</th>
								<td>
								<? $admin_no = 0;
								foreach($admin_arr as $admin_key => $admin_val){ 
									if($admin_level_val != $admin_val['mb_level']){ continue; }
									$admin_no++;
									$admin_name = "";
									if($admin_val['mb_name'] != ''){
										$admin_name = "[".$admin_val['mb_name']."]";
									}

									?>
										<strong><?=$admin_val['mb_id']?></strong><?=$admin_name;?>&nbsp;&nbsp;
										<? echo $admin_no % 3 == 0 ? "<br/>":""; ?>
									<? } ?>
								</td>
							</tr>
							<? } ?>
						</table>
						<? }else{ ?>
						<p>관리자 회원은 없습니다.</p>
						<? } ?>
					</td>
				</tr>
				<tr>
					<th>파트너 리스트</th>
					<td>
						<p class="blue">파트너 분류 권한 : 코믹스결제시-통계제외, CMS접근가능</p>
						<?if(count($partner_arr) > 0){?>
						<table>
							<? foreach($partner_level_arr as $partner_level_key => $partner_level_val){ ?>
							<tr>
								<th><?=$nm_config['cf_level_list'][$partner_level_val]?>(<?=$partner_level_val;?>)</th>
								<td>
								<? $partner_no = 0;
								foreach($partner_arr as $partner_key => $partner_val){ 
									if($partner_level_val != $partner_val['mb_level']){ continue; }
									$partner_no++;
									$partner_name = "";
									if($partner_val['mb_name'] != ''){
										$partner_name = "[".$partner_val['mb_name']."]";
									}

									?>
										<strong><?=$partner_val['mb_id']?></strong><?=$partner_name;?>&nbsp;&nbsp;
										<? echo $partner_no % 3 == 0 ? "<br/>":""; ?>
									<? } ?>
								</td>
							</tr>
							<? } ?>
						</table>
						<? }else{ ?>
						<p>파트너 회원은 없습니다.</p>
						<? } ?>
					</td>
				</tr>
				<tr>
					<th>코믹스 대분류</th>
					<td>
						<? foreach($cf_comics_big_arr as $comics_big_key => $comics_big_val){ 
								if($comics_big_key == 0){ 
									continue; 
								} else if($comics_big_key == count($cf_comics_big_arr)-1) {
									echo $comics_big_val;
								} else {
									echo $comics_big_val.", ";
								} // end else							
							} // end foreach
						?>	
					</td>
				</tr>
				<tr>
					<th>업데이트 시간</th>
					<td>
						매 시간 정각 AUTO Shell : <p class="blue pgl10"><?=$row['cf_sh_everyhour_time'];?></p>
						매일 정각 AUTO Shell : <p class="blue pgl10"><?=$row['cf_sh_everyday_time'];?></p>
						매일 4시, 10시, 16시, 22시 AUTO Shell : <p class="blue pgl10"><?=$row['cf_sh_everyday_four_ten_time'];?></p>
						매일 6시 AUTO Shell :  <p class="blue pgl10"><?=$row['cf_sh_everyhour_six_time'];?></p>
					</td>
				</tr>
			</table>
			<?php echo $cf_submit_btn; ?>
			<table>
				<tr>
					<th>CMS 출력 설정</th>
					<td>
						<p>
							<label for="s_limit">기본출력 설정수 : </label>
							<input type="text" class="onlynumber wt_50 ta_right" name="s_limit" value="<?=$row['s_limit'];?>" id="s_limit" />줄

							<label for="s_limit_min" class="mg_left">최소출력수 : </label>
							<input type="text" class="onlynumber wt_50 ta_right" name="s_limit_min" value="<?=$row['s_limit_min'];?>" id="s_limit_min" />줄

							<label for="s_limit_max" class="mg_left">최대출력수 : </label>
							<input type="text" class="onlynumber wt_50 ta_right" name="s_limit_max" value="<?=$row['s_limit_max'];?>" id="s_limit_max" />줄
						</p>
					</td>
				</tr>
				<tr>
					<th>메뉴 출력수</th>
					<td>
						<p>
							<label for="cf_menu">기본 메뉴 출력 수 : </label>
							<input type="text" class="onlynumber wt_50 ta_right" name="cf_menu" value="<?=$row['cf_menu'];?>" id="cf_menu" />개
						</p>
					</td>
				</tr>
				<!-- 여기부터 --->
				<tr>
					<th><?=$nm_config['cf_cash_point_unit_ko']?> 화폐 설정<br/>리스트</th>
					<td>
						<p>사용안할시 삭제하시면 됩니다.(빈칸)</p>
						<div class="clear">
							<ul class="float">
							<?	$cf_pay_arr = explode("|", $row['cf_pay']);
								foreach($cf_pay_arr as $cf_pay_key => $cf_pay_val){ ?>
								<li>
									<label for="cf_pay<?=$cf_pay_key?>" class="mg_left"><?=$nm_config['cf_cash_point_unit_ko']?> 화폐<?=$cf_pay_key+1?> : </label>
									<input type="text" class="onlynumber wt_50 ta_right" name="cf_pay<?=$cf_pay_key?>" value="<?=$cf_pay_val?>" id="cf_pay<?=$cf_pay_key?>" /><?=$nm_config['cf_cash_point_unit']?>
								</li>
								<? if(($cf_pay_key+1) %10 == 0) echo"</ul><ul>"; ?>
							<? } ?>
							</ul>
						</div>
					</td>
				<tr>
					<th><?=$nm_config['cf_cash_point_unit_ko']?> 기본 화폐 설정</th>
					<td>
						<p>
							<label for="cf_pay_basic"><?=$nm_config['cf_cash_point_unit_ko']?> 기본 화폐 : </label>
							<input type="text" class="onlynumber wt_50 ta_right" name="cf_pay_basic" value="<?=$row['cf_pay_basic'];?>" id="cf_pay_basic" /><?=$nm_config['cf_cash_point_unit']?>
						</p>
					</td>
				</tr>
				<!-- 여기까지 --->
			</table>
			<?php echo $cf_submit_btn; ?>
			<table>
				<tr>
					<th>화폐 단위</th>
					<td>
						<p>
							<label for="s_limit"><?=$nm_config['cf_cash_point_unit_ko']?> 기본 화폐 : </label>
							<input type="text" class="onlynumber wt_50" name="cf_cash_point_unit" value="<?=$row['cf_cash_point_unit'];?>" id="cf_cash_point_unit" />
						</p>
						<p>
							<label for="s_limit"><?=$nm_config['cf_point_unit_ko']?> 기본 화폐 : </label>
							<input type="text" class="onlynumber wt_50" name="cf_point_unit" value="<?=$row['cf_point_unit'];?>" id="cf_point_unit" />
						</p>
					</td>
				</tr>
				<tr>
					<th>화폐 한국어 단위</th>
					<td>
						<p>
							<label for="s_limit"><?=$nm_config['cf_cash_point_unit_ko']?> 기본 화폐 : </label>
							<input type="text" class="onlynumber wt_100" name="cf_cash_point_unit_ko" value="<?=$row['cf_cash_point_unit_ko'];?>" id="cf_cash_point_unit_ko" />
						</p>
						<p>
							<label for="s_limit"><?=$nm_config['cf_point_unit_ko']?> 기본 화폐 : </label>
							<input type="text" class="onlynumber wt_100" name="cf_point_unit_ko" value="<?=$row['cf_point_unit_ko'];?>" id="cf_point_unit_ko" />
						</p>
					</td>
				</tr>
			</table>
			<?php echo $cf_submit_btn; ?>
			<table>
				<tr>
					<th>정산 설정</th>
					<td>
						<p>
							<label for="s_limit">1<?=$nm_config['cf_cash_point_unit_ko']?> 당 정산 금액 : </label>
							<?	tag_selects($cup_arr, "cf_cup_type", $row['cf_cup_type'], 'n'); ?>원
						</p>
					</td>
				</tr>
				<tr>
					<th>KCP 인증 설정</th>
					<td>
						<p>
							<? tag_radios($d_pg, "cf_certify_kcp_mode",  $row['cf_certify_kcp_mode'], 'n'); ?>
						</p>
						<p>
							<input class="long_text" type="text" placeholder="실결제모드에서 KCP 인증 테스트모드를 이용할 회원ID를 입력해주세요." name="cf_certify_kcp_test_id" value="<?=$row['cf_certify_kcp_test_id']?>" id="cf_certify_kcp_test_id" />
						</p>
					</td>
				</tr>
				<tr>
					<th>KCP 결제 설정</th>
					<td>
						<p>
							<? tag_radios($d_pg, "cf_pg_kcp_mode",  $row['cf_pg_kcp_mode'], 'n'); ?>
						</p>
						<p>
							<input class="long_text" type="text" placeholder="실결제모드에서 KCP 결제 테스트모드를 이용할 회원ID를 입력해주세요." name="cf_pg_kcp_test_id" value="<?=$row['cf_pg_kcp_test_id']?>" id="cf_pg_kcp_test_id" />
						</p>
					</td>
				</tr>
				<tr>
					<th>TOSS 결제 설정</th>
					<td>
						<p>
							<? tag_radios($d_pg, "cf_pg_toss_mode",  $row['cf_pg_toss_mode'], 'n'); ?>
						</p>
						<p>
							<input class="long_text" type="text" placeholder="실결제모드에서 TOSS 결제 테스트모드를 이용할 회원ID를 입력해주세요." name="cf_pg_toss_test_id" value="<?=$row['cf_pg_toss_test_id']?>" id="cf_pg_toss_test_id" />
						</p>
					</td>
				</tr>
				<tr>
					<th>PAYCO 결제 설정</th>
					<td>
						<p>
							<? tag_radios($d_pg, "cf_pg_payco_mode",  $row['cf_pg_payco_mode'], 'n'); ?>
						</p>
						<p>
							<input class="long_text" type="text" placeholder="실결제모드에서 PAYCO 결제 테스트모드를 이용할 회원ID를 입력해주세요." name="cf_pg_payco_test_id" value="<?=$row['cf_pg_payco_test_id']?>" id="cf_pg_payco_test_id" />
						</p>
						<p>
							테스트 모드일시 <a href="http://devcenter.payco.com">http://devcenter.payco.com</a> 에서 개발앱을 다운로드 받으셔서 설치 후에 개발앱 결제해야 합니다.<br/>
							앱을 설치하시면 <br/>
							앱 이름이 리얼앱일경우 'PAYCO' (실결제모드에서 사용되는 APP)<br/>
							알파앱일 경우 안드로이드는 'PAYCO alpha', (테스트모드에서 사용되는 APP)<br/>
							IOS 의 경우 D-PAYCO 로 표시되니 앱이름을 확인해 보시기 바랍니다.<br/>
							개발앱을 설치하시고도 시간이 지나면 리얼앱으로 업데이트 될 수 있으니 <br/>
							그 때는 개발앱을 재 설치하셔야 합니다.
						</p>
					</td>
				</tr>
			</table>
			<?php echo $cf_submit_btn; ?>
			<table>
				<tr>
					<th>컨텐츠유형 설정</th>
					<td>
						<div class="clear">
							<ul class="float">
							<? foreach($cf_comics_type_arr as $comics_type_key => $comics_type_val){ 
								if($comics_type_key == 0){ continue; }
							?>
							<li>
								<label for="cf_comics_type<?=$comics_type_key?>"> 유형 텍스트 <?=$comics_type_key?> : </label>
								<input type="text" class="wt_100" name="cf_comics_type<?=$comics_type_key?>" value="<?=$comics_type_val;?>" id="cf_comics_type<?=$comics_type_key?>" />
							</li>
							<? } ?>
							</ul>
						</div>
					</td>
				</tr>
				<tr>
					<th>이벤트 충전/지급 타입 설정</th>
					<td>
						<p class="red">텍스트 추가시 개발자에게 말씀하셔야 합니다.</p>
						<div class="clear">
							<ul class="float">
							<? foreach($cf_er_type_arr as $er_type_key => $er_type_val){ 
								if($er_type_key == 0){ continue; }
							?>
							<li>
								<label for="cf_er_type<?=$er_type_val?>"> 타입 텍스트 <?=$er_type_key?> : </label>
								<input type="text" class="" name="cf_er_type<?=$er_type_key?>" value="<?=$er_type_val;?>" id="cf_er_type<?=$er_type_val?>" />
							</li>
							<? } ?>
							</ul>
						</div>
					</td>
				</tr>
			</table>
			<?php echo $cf_submit_btn; ?>

		</form>
	</div>
</section><!-- cmsconfig_result -->




<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>