<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'cf_no');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'cf_no');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

$dbtable = "config_img";
$dbt_primary = "cf_no";
$para_primary = "cf_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 이미지 처리 */
	
/* 로그 배열 */
// 반복된 걸 배열로 처리 (경로/파일명/컬럼명)
$cf_logo_arr = array();
array_push($cf_logo_arr, array('_mobile/common/', 'confirmjoin_title', 'cf_mobile_confirmjoin_title'));
array_push($cf_logo_arr, array('_mobile/common/', 'confirmjoin_txt', 'cf_mobile_confirmjoin_txt'));

array_push($cf_logo_arr, array('_pc/common/', 'confirmjoin_title', 'cf_web_confirmjoin_title'));
array_push($cf_logo_arr, array('_pc/common/', 'confirmjoin_txt', 'cf_web_confirmjoin_txt'));

/* 이미지 경로 */
foreach($cf_logo_arr as $cf_logo_key => $cf_logo_val){

	/* 대분류 경로설정 - 폴더 체크 및 생성 */
	$path_cover = $cf_logo_val[0];
	
	/* 업로드한 이미지 명 */
	$image_cover_tmp_name = $_FILES[$cf_logo_val[2]]['tmp_name'];
	$image_cover_name = trim($_FILES[$cf_logo_val[2]]['name']);

	/* 확장자 얻기 .png .jpg .gif */
	$image_cover_extension = substr($image_cover_name, strrpos($image_cover_name, "."), strlen($image_cover_name));

	/* 고정된 파일 명 */
	$image_cover_name_define = $cf_logo_val[1].get_int(NM_TIME_YMDHIS).strtolower($image_cover_extension);

	/* 저장 경로 및 DB 파일명 및 파일경로 저장 */
	$image_cover_src = $path_cover.$image_cover_name_define;
	
	/* 파일 업로드 */ 
	$image_cover_result = false;
	if ($image_cover_name == '') {  continue; }
	else { 
		$image_cover_result = kt_storage_upload($image_cover_tmp_name, $path_cover, $image_cover_name_define); 
	}
	
	/* 임시파일이 존재하는 경우 삭제 */
	if (file_exists($image_cover_tmp_name) && is_file($image_cover_tmp_name)) {
		unlink($image_cover_tmp_name);
	}
	
	$sql_image = ' UPDATE '.$dbtable.' SET ';
	/* 이미지 field값 추가 */
	if ($image_cover_name != '' && $image_cover_result == true){ $sql_image.= ' '.$cf_logo_val[2].' = "'.$image_cover_src.'" '; }
	$sql_image.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
	
	if ($image_cover_name != '' && $image_cover_result == true){
		/* DB 저장 */
		if(sql_query($sql_image)){
			$db_result['msg'].= '\n'.$cf_logo_val[2].'의 이미지가 저장되었습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'].= '\n'.'이미지 에러가 발생하여 저장되지 않았습니다.';
			$db_result['error'].= $sql_image;
		}
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
alert($db_result['msg'], $_SERVER['HTTP_REFERER']);
die;
?>