<?
include_once '_common.php'; // 공통

/* DB */
$sql_img = "select * from config_img";
$row = sql_fetch($sql_img);

$page_title = "이미지 설정";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
</section><!-- cms_title -->

<section id="cmsconfig_result">
	<div id="cr_bg">
		<form name="env_basic_write_form" id="env_img_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return env_img_write_submit(this);" >
		<input type="hidden" id="cf_no" name="cf_no" value="1"/>  <!-- cf_no모드 -->
		<table>
			<tr>
				<th>공통 이미지</th>
				<td>
					<p>
						<label for="cf_favicon">Favicon : </label>
						<? if($row['cf_favicon']!=''){ $cf_favicon = img_url_para($row['cf_favicon'], $row['cf_date']); } ?>
						<input type="file" name="cf_favicon" id="cf_favicon" data-value="<?=$cf_favicon?>" />
						
						<?if($row['cf_favicon']!=''){?>
						<div class="logo_view">
							<p><a href="#logo_view" onclick="a_click_false();">Favicon 보기</a></p>
							<p class="logo_view_hide"><img src="<?=$cf_favicon?>" alt="Favicon" /></p>
						</div>
						<?}?>
						<p class="logo_explain">이미지 업로드 정보 : Width: 16px, Height: 16px - 확장자:con</p>
					</p>
					<p>
						<label for="cf_cash"><?=$nm_config['cf_cash_point_unit_ko']?> : </label>
						<? if($row['cf_cash']!=''){ $cf_cash = img_url_para($row['cf_cash'], $row['cf_date']); } ?>
						<input type="file" name="cf_cash" id="cf_cash" data-value="<?=$cf_cash?>" />
						
						<?if($row['cf_cash']!=''){?>
						<div class="logo_view">
							<p><a href="#logo_view" onclick="a_click_false();"><?=$nm_config['cf_cash_point_unit_ko']?> 이미지 보기</a></p>
							<p class="logo_view_hide"><img src="<?=$cf_cash?>" alt="<?=$nm_config['cf_cash_point_unit_ko'];?>" /></p>
						</div>
						<?}?>
						<p class="logo_explain">이미지 업로드 정보 : Width: 16px, Height: 16px - 확장자:jpg, gif, png</p>
					</p>

					<p>
						<label for="cf_point"><?=$nm_config['cf_point_unit_ko']?> : </label>
						<? if($row['cf_point']!=''){ $cf_point = img_url_para($row['cf_point'], $row['cf_date']); } ?>
						<input type="file" name="cf_point" id="cf_point" data-value="<?=$cf_point;?>" />
						
						<?if($row['cf_point']!=''){?>
						<div class="logo_view">
							<p><a href="#logo_view" onclick="a_click_false();"><?=$nm_config['cf_point_unit_ko'];?> 보기</a></p>
							<p class="logo_view_hide"><img src="<?=$cf_point?>" alt="<?=$nm_config['cf_point_unit_ko'];?>" /></p>
						</div>
						<?}?>
						<p class="logo_explain">이미지 업로드 정보 : Width: 16px, Height: 16px - 확장자:jpg, gif, png</p>
					</p>
					<p>
						<label for="cf_meta">meta : </label>
						<? if($row['cf_meta']!=''){ $cf_meta = img_url_para($row['cf_meta'], $row['cf_date']); } ?>
						<input type="file" name="cf_meta" id="cf_meta" data-value="<?=$cf_meta?>" />
						
						<?if($row['cf_meta']!=''){?>
						<div class="logo_view">
							<p><a href="#logo_view" onclick="a_click_false();">Meta 보기</a></p>
							<p class="logo_view_hide"><img src="<?=$cf_meta?>" alt="meta" /></p>
						</div>
						<?}?>
						<p class="logo_explain">이미지 업로드 정보 : Width: 400px, Height: 210px - 확장자:jpg, gif, png</p>
						<p class="logo_explain">https://developers.kakao.com/docs/cache 에서 캐쉬된 이미지 삭제 할수 있습니다.</p>
					</p>
				</td>
			</tr>
			<tr>
				<th>Mobile 이미지</th>
				<td>
					<p>
						<label for="cf_mobile_logo">Mobile 상단 로고 : </label>
						<? if($row['cf_mobile_logo']!=''){ $cf_mobile_logo = img_url_para($row['cf_mobile_logo'], $row['cf_date']); } ?>
						<input type="file" name="cf_mobile_logo" id="cf_mobile_logo" data-value="<?=$cf_mobile_logo?>" />
						
						<?if($row['cf_mobile_logo']!=''){?>
						<div class="logo_view">
							<p><a href="#logo_view" onclick="a_click_false();">Mobile 상단 로고 보기</a></p>
							<p class="logo_view_hide"><img src="<?=$cf_mobile_logo?>" alt="Mobile 상단 로고" /></p>
						</div>
						<?}?>
						<p class="logo_explain">이미지 업로드 정보 : Width: 110px, Height: 26px - 확장자:jpg, gif, png</p>
					</p>
					<p>
						<label for="cf_mobile_bookmark">Bookmark : </label>
						<? if($row['cf_mobile_bookmark']!=''){ $cf_mobile_bookmark = img_url_para($row['cf_mobile_bookmark'], $row['cf_date']); } ?>
						<input type="file" name="cf_mobile_bookmark" id="cf_mobile_bookmark" data-value="<?=$cf_mobile_bookmark?>" />
						
						<?if($row['cf_mobile_bookmark']!=''){?>
						<div class="logo_view">
							<p><a href="#logo_view" onclick="a_click_false();">Bookmark 보기</a></p>
							<p class="logo_view_hide"><img src="<?=$cf_mobile_bookmark?>" alt="Bookmark" /></p>
						</div>
						<?}?>
						<p class="logo_explain">이미지 업로드 정보 : Width: 16px, Height: 16px - 확장자:jpg, gif, png</p>
					</p>
				</td>
			</tr>
			<tr>
				<th>Web 이미지</th>
				<td>
					<p>
						<label for="cf_web_logo">Web 메인 상단 로고 : </label>
						<? if($row['cf_web_logo']!=''){ $cf_web_logo = img_url_para($row['cf_web_logo'], $row['cf_date']); } ?>
						<input type="file" name="cf_web_logo" id="cf_web_logo" data-value="<?=$cf_web_logo?>" />
						
						<?if($row['cf_web_logo']!=''){?>
						<div class="logo_view">
							<p><a href="#logo_view" onclick="a_click_false();">Web 메인 상단 로고 보기</a></p>
							<p class="logo_view_hide"><img src="<?=$cf_web_logo?>" alt="Web 메인 상단 로고" /></p>
						</div>
						<?}?>
						<p class="logo_explain">이미지 업로드 정보 : Width: 144px, Height: 57px - 확장자:jpg, gif, png</p>
					</p>
					<p>
						<label for="cf_web_logo_footer">Web 하단 로고 : </label>
						<? if($row['cf_web_logo_footer']!=''){ $cf_web_logo_footer = img_url_para($row['cf_web_logo_footer'], $row['cf_date']); } ?>
						<input type="file" name="cf_web_logo_footer" id="cf_web_logo_footer" data-value="<?=$cf_web_logo_footer?>" />
						
						<?if($row['cf_web_logo_footer']!=''){?>
						<div class="logo_view">
							<p><a href="#logo_view" onclick="a_click_false();">Web 하단 로고 보기</a></p>
							<p class="logo_view_hide"><img src="<?=$cf_web_logo_footer?>" alt="Web 하단 로고" /></p>
						</div>
						<?}?>
						<p class="logo_explain">이미지 업로드 정보 : Width: 134px, Height: 52px - 확장자:jpg, gif, png</p>
					</p>
					<p>
						<label for="cf_web_bookmark">Bookmark : </label>
						<? if($row['cf_web_bookmark']!=''){ $cf_web_bookmark = img_url_para($row['cf_web_bookmark'], $row['cf_date']); } ?>
						<input type="file" name="cf_web_bookmark" id="cf_web_bookmark" data-value="<?=$cf_web_bookmark?>" />
						
						<?if($row['cf_web_bookmark']!=''){?>
						<div class="logo_view">
							<p><a href="#logo_view" onclick="a_click_false();">Bookmark 보기</a></p>
							<p class="logo_view_hide"><img src="<?=$cf_web_bookmark?>" alt="Bookmark" /></p>
						</div>
						<?}?>
						<p class="logo_explain">이미지 업로드 정보 : Width: 16px, Height: 16px - 확장자:jpg, gif, png</p>
					</p>
				</td>
			</tr>
		</table>
			<div class="cf_submit_btn">
				<input type="submit" value="저장" class="btn_submit" accesskey="s">
			</div>
		</form>		
	</div>
</section><!-- cmsconfig_result -->
<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>