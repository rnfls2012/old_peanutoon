<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_csa_no = tag_filter($_REQUEST['csa_no']);

$sql = "SELECT * FROM `config_sin_allpay` WHERE  `csa_no` = '$_csa_no' ";
	
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}
/* 필수 속성 */
$required_arr = array('*', 'required', '(필수)');

/* 대분류로 제목 */
$page_title = "전체구매시 추가지급";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- event_recharge_head -->

<section id="config_sin_allpay_write">
	<form name="config_sin_allpay_write_form" id="config_sin_allpay_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return config_sin_allpay_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="csa_no" name="csa_no" value="<?=$_csa_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="csa_no">전체구매시 화수<br/>추가지급 번호</label></th>
					<td><?=$_csa_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="csa_buy_count">전체구매시 화수</label></th>
					<td>
						<input type="text" class="onlynumber text_rt_130" placeholder="숫자만 입력해주세요" name="csa_buy_count" id="csa_buy_count" value="<?=$row['csa_buy_count'];?>" autocomplete="off" maxlength="10" /> 
						<label for="csa_buy_count">화</label>
					</td>
				</tr>
				<tr>
					<th><label for="csa_won">전체구매시 화수타입</label></th>
					<td>
						<? tag_selects($d_csa_buy_than, "csa_buy_than", $row['csa_buy_than'], 'n'); ?>
					</td>
				</tr>
				<tr>
					<th><label for="csa_cash_point">전체구매시 화수 <br/>지급 보너스 <br/><?=$nm_config['cf_point_unit_ko'];?>(<?=$nm_config['cf_point_unit'];?>)</label></th>
					<td>
						<input type="text" class="onlynumber text_rt_130" placeholder="숫자만 입력해주세요" name="csa_point" id="csa_point" value="<?=$row['csa_point'];?>" autocomplete="off" maxlength="7" /> 
						<label for="csa_point"><?=$nm_config['cf_point_unit'];?></label>
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>전체구매시 화수 기간</label></th>
					<td>
						<div id="service_color" class="input_btn">
							<input type="radio" name="csa_date_type" id="csa_date_type0" value="y" <?=get_checked('', $row['csa_date_type']);?> <?=get_checked('y', $row['csa_date_type']);?>  onclick="csa_date_type_chk(this.value);" />
							<label for="csa_date_type0">기간만적용</label>
							<input class="still" type="radio" name="csa_date_type" id="csa_date_type1" value="m" <?=get_checked('m', $row['csa_date_type']);?> onclick="csa_date_type_chk(this.value);" />
							<label class="still" for="csa_date_type1">매월(날짜빈칸시 무기간)</label>
							<input class="still" type="radio" name="csa_date_type" id="csa_date_type2" value="w" <?=get_checked('w', $row['csa_date_type']);?> onclick="csa_date_type_chk(this.value);" />
							<label class="still" for="csa_date_type2">매주(날짜빈칸시 무기간)</label>
							<input type="radio" name="csa_date_type" id="csa_date_type3" value="n" <?=get_checked('n', $row['csa_date_type']);?> onclick="csa_date_type_chk(this.value);" />
							<label for="csa_date_type3">무기간</label>
						</div>
						<div class="<? echo ($row['csa_date_type'] != 'n')?"":"still";?> csa_date_type_view margin_top10">
							<span for="s_date">시작일 :</span>
							<input type="text" class="s_date readonly" placeholder="클릭하세요" name="csa_date_start" id="s_date" value="<?=$row['csa_date_start'];?>" autocomplete="off" readonly /> ~ 
							<span for="e_date">종료일 :</span>
							<input type="text" class="e_date readonly" placeholder="클릭하세요" name="csa_date_end" id="e_date" value="<?=$row['csa_date_end'];?>" autocomplete="off" readonly />
						</div>
						<div class="csa_holiday_view still margin_top10">
							<span for="csa_holiweek">매월</span>
							<?	tag_selects($day_list, "csa_holiday", $row['csa_holiday'], 'y', '사용안함', ''); ?>
						</div>
						<div class="csa_holiweek_view still margin_top10">
							<span for="csa_holiweek">매주</span>
							<?	tag_selects($week_list, "csa_holiweek", $row['csa_holiweek'], 'y', '사용안함', ''); ?>
						</div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="csa_state">전체구매시 화수 상태</label></th>
					<td><?=$d_csa_state[$row['csa_state']];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="csa_date">전체구매시 화수 저장일</label></th>
					<td><?=$row['csa_date'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- config_recharge_price_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>