<?
include_once '_common.php'; // 공통

/* PARAMITER */

// 기본적으로 몇개 있는 지 체크;
$sql_csa_total = "select count(*) as total_csa from config_sin_allpay where 1  ";
$total_csa = sql_count($sql_csa_total, 'total_csa');

/* 데이터 가져오기 */
$config_csa_where = '';

// 선택박스 검색 데이터 커리문 생성
$nm_paras_check = array('csa_state');
foreach($nm_paras_check as $nm_paras_key => $nm_paras_val){
	if(${"_".$nm_paras_val} != ''){
		$config_csa_where.= "and ".$nm_paras_val." = '".${"_".$nm_paras_val}."' ";
	}
}

// 시작일/종료일 날짜
$start_date = $end_date = "";
if($_date_type){ //all, upload_date, new_date
	if($_s_date && $_e_date){
		$start_date = $_s_date;
		$end_date = $_e_date;
	}else if($_s_date){
		$start_date = $_s_date;
		$end_date = NM_TIME_YMD;
	}
	switch($_date_type){
		case 'all' : $config_csa_where.= "and ( csa_date_start >= '$start_date' and csa_date_end <= '$end_date') ";
		break;

		case 'csa_date_start' : $config_csa_where.= "and csa_date_start >= '$start_date' ";
		break;

		case 'csa_date_end' : $config_csa_where.= "and csa_date_end <= '$end_date' ";
		break;

		default: $config_csa_where.= " ";
		break;
	}
}else{
	$_s_date = "";
	$_e_date = "";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "( CASE csa_state WHEN 'r' THEN 1 WHEN 'y' THEN 2 WHEN 'n' THEN 3 ELSE 4 END ), csa_point, csa_date "; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$config_csa_order = "order by ".$_order_field." ".$_order;

$config_csa_sql = "";
$config_csa_field = " * "; // 가져올 필드 정하기
$config_csa_limit = "";

$config_csa_sql = "SELECT $config_csa_field FROM config_sin_allpay where 1 $config_csa_where $config_csa_order $config_csa_limit";
$result = sql_query($config_csa_sql);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','crp_no',0));
array_push($fetch_row, array('구매화수','csa_buy_count',1));
array_push($fetch_row, array('구매화수타입','csa_buy_than',1));
array_push($fetch_row, array('구매시 지급 보너스 <br/>'.$nm_config['cf_point_unit_ko'].'('.$nm_config['cf_point_unit'].')','csa_point',1));
array_push($fetch_row, array('시작일','csa_date_start',1));
array_push($fetch_row, array('마감일','csa_date_end',1));
array_push($fetch_row, array('상태','csa_state',1));
array_push($fetch_row, array('관리','csa_management',0));

$cs_calendar_on = $cs_submit_h = '';
if($_date_type){
	$cs_calendar_on = 'cs_calendar_on';
	$cs_submit_h = 'cs_submit_on';
}

$page_title = "전체구매시 추가지급";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> 이벤트 : 총 <?=number_format($total_csa);?> 건</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','csa_write', <?=$popup_cms_width;?>, 580);"><?=$page_title?> 등록</button>
	</div>
</section><!-- cms_title -->

<section id="cms_explain">
	<ul>
		<li>미만으로 하시면 해당 화수의 미만일시 적용 됩니다.</li>
		<li>이상일 경우 MAX일 경우에만 즉 1회만 가능합니다.</li>
	</ul>
</section><!-- cms_explain -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="config_sin_allpay_search_form" id="config_sin_allpay_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return config_sin_allpay_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_er_type_btn">
					<?	tag_selects($d_state, "crp_state", $_csa_state, 'y', '상태 전체', ''); ?> 
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
				</div>
				<div class="cs_date_btn">
					<?	tag_selects($d_er_date_type, "date_type", $_date_type, 'y', '이벤트날짜 검색안함', ''); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit config_sin_allpay_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="52" data-on_h="107"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- event_search -->

<section id="cmsconfig_sin_allpay_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){

					// 구매화수
					$db_csa_buy_count = number_format($dvalue_val['csa_buy_count'])."화";

					// 구매화수타입
					$db_csa_buy_than = $d_csa_buy_than[$dvalue_val['csa_buy_than']];

					// 지급 보너스캐쉬
					$db_csa_point = point_view($dvalue_val['csa_point'], 'y');

					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?csa_no=".$dvalue_val['csa_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					$db_csa_no_date_text = "무기간";
					if($dvalue_val['csa_date_type'] != 'n' && $dvalue_val['csa_date_start'] == '' && $dvalue_val['csa_date_end'] == ''){
						$db_csa_no_date_text = "시작일&마감일 없어서 종료됩니다.";
					}
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['csa_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$db_csa_buy_count;?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$db_csa_buy_than;?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$db_csa_point;?></td>
					<?if($dvalue_val['csa_date_type'] != 'n' && $dvalue_val['csa_date_start'] != '' && $dvalue_val['csa_date_end'] != ''){?>
						<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['csa_date_start'];?></td>
						<td class="<?=$fetch_row[5][1]?> text_center"><?=$dvalue_val['csa_date_end'];?></td>
					<?}else{?>
						<td colspan='2' class="csa_date_no text_center"><?=$db_csa_no_date_text?></td>
					<?}?>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$d_state[$dvalue_val['csa_state']];?></td>

					<td class="<?=$fetch_row[7][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','csa_mod_writer', <?=$popup_cms_width;?>, 600);">수정</button>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','csa_del_writer', <?=$popup_cms_width;?>, 600);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- event_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>