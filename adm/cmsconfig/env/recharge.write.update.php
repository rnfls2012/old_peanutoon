<?
include_once '_common.php'; // 공통
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode','crp_no');
array_push($para_list, 'crp_won','crp_cash_point','crp_point','crp_point_percent','crp_payway_limit');
array_push($para_list, 'crp_date_type','crp_state','crp_date_start','crp_date_end');
array_push($para_list, 'crp_holiday','crp_holiweek');
array_push($para_list, 'crp_cash_count_event','crp_cash_count_event_text');
array_push($para_list, 'crp_date');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'crp_no','crp_won','crp_cash_point','crp_point','crp_point_percent');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'crp_cash_point','crp_point','crp_point_percent','crp_payway_limit');
array_push($blank_list, 'crp_date_type','crp_state','crp_date_start','crp_date_end');
array_push($blank_list, 'crp_holiday','crp_holiweek');
array_push($blank_list, 'crp_cash_count_event', 'crp_cash_count_event_text');

/* 빈칸 PARAMITER 시 NULL 처리 */
array_push($null_list, 'crp_won','crp_holiday','crp_holiweek');

/* 체크박스배열 PARAMITER 체크 */
array_push($checkbox_list, 'crp_payway_limit');

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

// 상태와 날짜
$get_date_type = get_date_type($_crp_date_type, $_crp_date_start, $_crp_date_end);
$_crp_state = $get_date_type['state'];
$_crp_date_start = $get_date_type['date_start'];
$_crp_date_end = $get_date_type['date_end'];

$dbtable = "config_recharge_price";
$dbt_primary = "crp_no";
$para_primary = "crp_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';
if($_mode == 'reg'){	
	/* 고정값 */
	$_crp_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);

	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = '충전/지급 데이터가 등록되였습니다.';
		crp_cash_count_event($_crp_cash_count_event, $_crp_cash_count_event_text);
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '등록 에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}

/* 수정 */
}else if($_mode == 'mod'){
	$_crp_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $_crp_no.'의 데이터가 수정되였습니다.';
			crp_cash_count_event($_crp_cash_count_event, $_crp_cash_count_event_text);
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}

	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 데이터 삭제 */
		sql_query("delete from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
		
		$db_result['msg'] = $_crp_no.'의 데이터가 삭제되였습니다.';
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/
pop_close($db_result['msg'], '', $db_result['error']);
die;


function crp_cash_count_event($_crp_cash_count_event, $_crp_cash_count_event_text) {
	if($_crp_cash_count_event != ''){
		$count_event = get_int(num_check($_crp_cash_count_event));
		$event_text = $_crp_cash_count_event_text;

		$update_query = "update config_recharge_price set crp_cash_count_event_text='".$event_text."' where crp_cash_count_event='".$count_event."'";
		$result = sql_query($update_query);
	}else{
		return false;
	}

	if(!$result) {
		cs_alert("결제횟수가 같은 이벤트끼리 동기화되지 않았습니다.", $_SERVER['HTTP_REFERER']);
	} // end if
} // crp_cash_count_event
?>