<?
include_once '_common.php'; // 공통

/* class 체크 */
foreach($d_mb_class as $mb_class_key => $mb_class_val){
	$sql_mb_class = "SELECT  count(*) as total_mb_class FROM  `config_member_buff` WHERE  `cfmb_member_class` =  '".$mb_class_key."' ";
	$total_mb_class = sql_count($sql_mb_class, 'total_mb_class');
	if($total_mb_class == 0){
		$sql_insert_mb_class = "INSERT INTO  `m_mangazzang2`.`config_member_buff` (`cfmb_member_class`)VALUES ( '".$mb_class_key."')";
		sql_query($sql_insert_mb_class);
	}
}
/* level 체크 */
foreach(mb_only($nm_config['cf_level_list']) as $mb_level_key => $mb_level_val){
	$sql_mb_level = "SELECT  count(*) as total_mb_level FROM  `config_member_buff` WHERE  `cfmb_member_level` =  '".$mb_level_key."' ";
	$total_mb_level = sql_count($sql_mb_level, 'total_mb_level');
	if($total_mb_level == 0){
		$sql_insert_mb_level = "INSERT INTO  `m_mangazzang2`.`config_member_buff` (`cfmb_member_level`)VALUES ( '".$mb_level_key."')";
		sql_query($sql_insert_mb_level);
	}
}

/* 이상한 데이터 삭제 */
$sql_mb_level_delete = "DELETE FROM `config_member_buff` WHERE cfmb_member_level = 0";
sql_query($sql_mb_level_delete);

// DB 데이터 가져오기
$cfmb_arr = array();
$sql_config = "SELECT * FROM  `config_member_buff` ORDER BY `cfmb_no` ASC ";;
$result_config = sql_query($sql_config);
while ($row_config = mysql_fetch_array($result_config)) {
	array_push($cfmb_arr,$row_config);
}

$cf_submit_btn = '<div class="cf_submit_btn">
    <input type="submit" value="전체 저장" class="btn_submit" accesskey="s">
</div>';

$page_title = "회원 분류·등급별 혜택";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더

?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
</section><!-- cms_title -->


<section id="cmsconfig_result">
	<div id="cr_bg">
		<form name="env_member_buff_write_form" id="env_member_buff_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return env_member_buff_write_submit(this);" >
		<input type="hidden" id="mode" name="mode" value="mod"/>	<!-- 수정모드 -->
		<input type="hidden" id="cfmb_no" name="cfmb_no1" value=""/>		<!-- cf_no모드 -->
		<input type="hidden" id="cfmb_no2" name="cfmb_no2" value="state_except|cms_access"/>		<!-- cf_no모드 -->
		<input type="hidden" id="cfmb_no3" name="cfmb_no3" value="state_except|cms_access|comics_free"/>		<!-- cf_no모드 -->
			
			<table>
				<tr>
					<th>회원 분류 혜택</th>
					<td class="cf_member_class_buff">
						<p>
							<label for="member_buff_c">고객(c&등급:2~<?=$nm_config['cf_partner_level']-1?>) : </label>
							<span id="member_buff_c">없음</span>
						</p>
						<p>
							<label for="member_buff_p">파트너(p&등급:<?=$nm_config['cf_partner_level']?>~<?=$nm_config['cf_admin_level']-1?>) : </label>
							<span id="member_buff_p">결제시-통계제외(state_except), CMS접근가능(cms_access)</span>
						</p>
						<p>
							<label for="member_buff_a">관리자(a&등급:<?=$nm_config['cf_admin_level']?>~30) : </label>
							<span id="member_buff_a">결제시-통계제외(state_except), CMS접근가능(cms_access), 코믹스무료제공(comics_free)</span>
						</p>
					</td>
				</tr>
				<tr>
					<th>고객 회원 혜택</th>
					<td class="cf_member_level_buff">
						<p></p>
						<div>
							<table>
								<tr>
									<th>회원등급</th>
									<th>회원결제혜택</th>
								</tr>
							<? foreach(mb_only($nm_config['cf_level_list']) as $cf_mbllevel_key => $cf_mbllevel_val){
								if($cf_mbllevel_val == ''){ continue; }
								if($cf_mbllevel_key >= $nm_config['cf_partner_level']){ continue; }
							?>
								<tr>
									<th>
										<? echo $cf_mbllevel_val;?>	등급(<?=$cf_mbllevel_key?>)								
									</th>
									<td>
										결제시
										<input type="text" class="onlynumber wt_50 ta_right" name="cfmb_level_premium<?=$cf_mbllevel_key?>" value="<?=$cfmb_arr[$cf_mbllevel_key+1]['cfmb_level_premium'];?>" id="cfmb_level_premium<?=$cf_mbllevel_key?>" />	% 보너스코인 추가							
									</td>
								</tr>
							<? } ?>
							</table>
						</div>
					</td>
				</tr>
			</table>
			<?php echo $cf_submit_btn; ?>
			<table class="buff_checkbox">
				<tr>
					<th>파트너 회원 메뉴권한</th>
					<td class="cf_member_level_buff">
						<p></p>
						<div>
							<table>
								<tr>
									<th>회원등급</th>
									<th>CMS 메뉴 사용</th>
								</tr>
								<? foreach(mb_only($nm_config['cf_level_list']) as $cf_mbllevel_key => $cf_mbllevel_val){
									if($cf_mbllevel_val == ''){ continue; }
									if($cf_mbllevel_key < $nm_config['cf_partner_level']){ continue; }
									if($cf_mbllevel_key >= $nm_config['cf_admin_level']){ continue; }
								?>
									<tr>
										<th>
											<? echo $cf_mbllevel_val;?> 등급(<?=$cf_mbllevel_key?>)									
										</th>
										<td>
											<!-- 메뉴 -->
											<? mb_cms_checkbox($menu, "cfmb_level_permission".$cf_mbllevel_key, $cfmb_arr[$cf_mbllevel_key+1]['cfmb_level_permission'], 'n'); ?>
											
										</td>
									</tr>
								<? } ?>
							</table>
						</div>
					</td>
				</tr>
			</table>
			<?php echo $cf_submit_btn; ?>
			<table class="buff_checkbox">
				<tr>
					<th>관리자 회원 메뉴권한</th>
					<td class="cf_member_level_buff">
						<p></p>
						<div>
							<table>
								<tr>
									<th>회원등급</th>
									<th>CMS 메뉴 사용</th>
								</tr>
								<? foreach(mb_only($nm_config['cf_level_list']) as $cf_mbllevel_key => $cf_mbllevel_val){
									if($cf_mbllevel_val == ''){ continue; }
									if($cf_mbllevel_key < $nm_config['cf_admin_level']){ continue; }
									if($cf_mbllevel_key >= $nm_config['cf_admin_level']+$nm_config['cf_admin_level_plus']){ continue; }
								?>
									<tr>
										<th>
											<? echo $cf_mbllevel_val;?> 등급(<?=$cf_mbllevel_key?>)									
										</th>
										<td>
											<!-- 메뉴 -->
											<? mb_cms_checkbox($menu, "cfmb_level_permission".$cf_mbllevel_key, $cfmb_arr[$cf_mbllevel_key+1]['cfmb_level_permission'], 'n'); ?>
											
										</td>
									</tr>
								<? } ?>
							</table>
						</div>
					</td>
				</tr>
			</table>
			<?php echo $cf_submit_btn; ?>
		</form>
	</div>
</section><!-- cmsconfig_result -->

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>