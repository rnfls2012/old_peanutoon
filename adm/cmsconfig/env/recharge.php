<?
include_once '_common.php'; // 공통

/* PARAMITER */

// 기본적으로 몇개 있는 지 체크;
$sql_crp_total = "select count(*) as total_crp from config_recharge_price where 1  ";
$total_crp = sql_count($sql_crp_total, 'total_crp');

/* 데이터 가져오기 */
$config_crp_where = '';

// 선택박스 검색 데이터 커리문 생성
$nm_paras_check = array('crp_advise', 'crp_state', 'crp_payway_limit');
foreach($nm_paras_check as $nm_paras_key => $nm_paras_val){
	if(${"_".$nm_paras_val} != ''){
		$config_crp_where.= "and ".$nm_paras_val." = '".${"_".$nm_paras_val}."' ";
	}
}

// 시작일/종료일 날짜
$start_date = $end_date = "";
if($_date_type){ //all, upload_date, new_date
	if($_s_date && $_e_date){
		$start_date = $_s_date;
		$end_date = $_e_date;
	}else if($_s_date){
		$start_date = $_s_date;
		$end_date = NM_TIME_YMD;
	}
	switch($_date_type){
		case 'all' : $config_crp_where.= "and ( crp_date_start >= '$start_date' and crp_date_end <= '$end_date') ";
		break;

		case 'crp_date_start' : $config_crp_where.= "and crp_date_start >= '$start_date' ";
		break;

		case 'crp_date_end' : $config_crp_where.= "and crp_date_end <= '$end_date' ";
		break;

		default: $config_crp_where.= " ";
		break;
	}
}else{
	$_s_date = "";
	$_e_date = "";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "( CASE crp_state WHEN 'r' THEN 1 WHEN 'y' THEN 2 WHEN 'n' THEN 3 ELSE 4 END ), crp_cash_point, crp_date "; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$config_crp_order = "order by ".$_order_field." ".$_order;

$config_crp_sql = "";
$config_crp_field = " * "; // 가져올 필드 정하기
$config_crp_limit = "";

$config_crp_sql = "SELECT $config_crp_field FROM config_recharge_price where 1 $config_crp_where $config_crp_order $config_crp_limit";
$result = sql_query($config_crp_sql);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','crp_no',0));
array_push($fetch_row, array('충전 가격','crp_won',1));
array_push($fetch_row, array('충전 '.$nm_config['cf_cash_point_unit_ko'].'('.$nm_config['cf_cash_point_unit'].')','crp_cash_point',1));
array_push($fetch_row, array('충전 '.$nm_config['cf_point_unit_ko'].'('.$nm_config['cf_point_unit'].')<br/>(보너스 퍼센트)','crp_point',1));
array_push($fetch_row, array('시작일','crp_date_start',1));
array_push($fetch_row, array('마감일','crp_date_end',1));
array_push($fetch_row, array('상태','crp_state',1));
array_push($fetch_row, array('결제수단제약','crp_payway_limit',0));
array_push($fetch_row, array('회원 결제횟수 이벤트','crp_cash_count_event',0));
array_push($fetch_row, array('관리','crp_management',0));

$cs_calendar_on = $cs_submit_h = '';
if($_date_type){
	$cs_calendar_on = 'cs_calendar_on';
	$cs_submit_h = 'cs_submit_on';
}

$page_title = "충전 금액";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong><?=$page_title?> 이벤트 : 총 <?=number_format($total_crp);?> 건</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','crp_write', <?=$popup_cms_width;?>, 580);"><?=$page_title?> 등록</button>
	</div>
</section><!-- cms_title -->

<section id="cms_explain">
	<ul>
		<li>회원 결제횟수 이벤트는 회원이 결제 횟수에 따라 적용되는 이벤트 입니다.</li>
		<li>특정 금액은 정산사정에 따라 제약할수 있습니다.(복수로 해야 될시 요청주세요.)</li>
	</ul>
</section><!-- cms_explain -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="config_recharge_price_search_form" id="config_recharge_price_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return config_recharge_price_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_er_type_btn">
					<?	tag_selects($d_state, "crp_state", $_crp_state, 'y', '상태 전체', ''); ?> 
					<?	tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
				</div>
				<div class="cs_date_btn">
					<?	tag_selects($d_er_date_type, "date_type", $_date_type, 'y', '이벤트날짜 검색안함', ''); ?>
				</div>
				<? calendar($_s_date, $_e_date, $cs_calendar_on); ?>
			</div>
			<div class="cs_submit config_recharge_price_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="52" data-on_h="107"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- event_search -->

<section id="cmsconfig_recharge_result">
	<h3>검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$order_giho = "▼";
				$th_order = "desc";
				if($_order == 'desc'){ 
					$order_giho = "▲"; 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}
					if($fetch_val[1] == $_order_field){
						$th_title_giho = $order_giho;
					}
					$th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
					$th_class = $fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){

					// 충전 가격
					$db_crp_won = number_format($dvalue_val['crp_won'])."원";

					// 지급 캐쉬
					$db_crp_cash_point = cash_point_view($dvalue_val['crp_cash_point'], 'y');

					// 지급 보너스캐쉬
					$db_crp_point = point_view($dvalue_val['crp_point'], 'y');
					$db_crp_point.= " (".$dvalue_val['crp_point_percent']."%)";

					/* 컨텐츠 수정/삭제 */
					$popup_url = $_cms_write."?crp_no=".$dvalue_val['crp_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					// 결제수단제약
					$db_crp_payway_limit_arr = explode("|", $dvalue_val['crp_payway_limit']);
					if(in_array($checkbox_value,$db_crp_payway_limit_arr)){
						$checkbox_checked = 'checked="checked"';
					}

					// 회원 결제횟수 이벤트
					$db_crp_cash_count_event = $dvalue_val['crp_cash_count_event'];
					if($dvalue_val['crp_cash_count_event'] == NULL){ $db_crp_cash_count_event = "사용안함"; }


					$db_crp_no_date_text = "무기간";
					if($dvalue_val['crp_date_type'] != 'n' && $dvalue_val['crp_date_start'] == '' && $dvalue_val['crp_date_end'] == ''){
						$db_crp_no_date_text = "시작일&마감일 없어서 종료됩니다.";
					}

					// 결제 수단
					foreach($nm_config['cf_payway'] as $payway_key => $payway_val){ 
						$d_crp_payway_limit[$payway_val[1]] = $payway_val[2];
					}
					$d_crp_payway_limit_text = "";
					$crp_payway_limit_arr = explode("|",$dvalue_val['crp_payway_limit']);
					
					foreach($crp_payway_limit_arr as $crp_payway_limity_val){ 
						$d_crp_payway_limit_text.= $d_crp_payway_limit[$crp_payway_limity_val]."<br/>";
					}
					$d_crp_payway_limit_text = substr($d_crp_payway_limit_text,0,strrpos($d_crp_payway_limit_text, "<br/>"));
				?>
				<tr class="result_hover">
					<td class="<?=$fetch_row[0][1]?> text_center"><?=$dvalue_val['crp_no'];?></td>
					<td class="<?=$fetch_row[1][1]?> text_center"><?=$db_crp_won;?></td>
					<td class="<?=$fetch_row[2][1]?> text_center"><?=$db_crp_cash_point;?></td>
					<td class="<?=$fetch_row[3][1]?> text_center"><?=$db_crp_point;?></td>
					<?if($dvalue_val['crp_date_type'] != 'n' && $dvalue_val['crp_date_start'] != '' && $dvalue_val['crp_date_end'] != ''){?>
						<td class="<?=$fetch_row[4][1]?> text_center"><?=$dvalue_val['crp_date_start'];?></td>
						<td class="<?=$fetch_row[5][1]?> text_center"><?=$dvalue_val['crp_date_end'];?></td>
					<?}else{?>
						<td colspan='2' class="crp_date_no text_center"><?=$db_crp_no_date_text?></td>
					<?}?>
					<td class="<?=$fetch_row[6][1]?> text_center"><?=$d_state[$dvalue_val['crp_state']];?></td>

					<td class="<?=$fetch_row[7][1]?> text_center">
						<?=$d_crp_payway_limit_text;?>
					</td>
					<td class="<?=$fetch_row[8][1]?> text_center"><?=$db_crp_cash_count_event;?></td>

					<td class="<?=$fetch_row[9][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','crp_mod_writer', <?=$popup_cms_width;?>, 600);">수정</button>
						<button class="del_btn" onclick="popup('<?=$popup_del_url;?>','crp_del_writer', <?=$popup_cms_width;?>, 600);">삭제</button>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
</section><!-- event_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>