<?
include_once '_common.php'; // 공통

/* DB */
$sql_img = "select * from config_img";
$row = sql_fetch($sql_img);

$page_title = "이미지 설정";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
</section><!-- cms_title -->

<section id="cmsconfig_result">
	<div id="cr_bg">
		<form name="env_basic_write_form" id="env_img_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return env_img_write_submit(this);" >
		<input type="hidden" id="cf_no" name="cf_no" value="1"/>  <!-- cf_no모드 -->
		<table>
			<tr>
				<th>Mobile 이미지</th>
				<td>					
					<p>
						<label for="cf_mobile_confirmjoin_title">가입팝업_타이틀 : </label>
						<? if($row['cf_mobile_confirmjoin_title']!=''){ $cf_mobile_confirmjoin_title = img_url_para($row['cf_mobile_confirmjoin_title'], $row['cf_date']); } ?>
						<input type="file" name="cf_mobile_confirmjoin_title" id="cf_mobile_confirmjoin_title" data-value="<?=$cf_mobile_confirmjoin_title?>" />
						
						<?if($row['cf_mobile_confirmjoin_title']!=''){?>
						<div class="logo_view">
							<p><a href="#logo_view" onclick="a_click_false();">가입팝업_타이틀 보기</a></p>
							<p class="logo_view_hide"><img src="<?=$cf_mobile_confirmjoin_title?>" alt="confirmjoin_title" /></p>
						</div>
						<?}?>
						<p class="logo_explain">이미지 업로드 정보 : Width: 미정px, Height: 미정px - 확장자:jpg, gif, png</p>
					</p>
					<p>
						<label for="cf_mobile_confirmjoin_txt">가입팝업_텍스트 : </label>
						<? if($row['cf_mobile_confirmjoin_txt']!=''){ $cf_mobile_confirmjoin_txt = img_url_para($row['cf_mobile_confirmjoin_txt'], $row['cf_date']); } ?>
						<input type="file" name="cf_mobile_confirmjoin_txt" id="cf_mobile_confirmjoin_txt" data-value="<?=$cf_mobile_confirmjoin_txt?>" />
						
						<?if($row['cf_mobile_confirmjoin_txt']!=''){?>
						<div class="logo_view">
							<p><a href="#logo_view" onclick="a_click_false();">가입팝업_텍스트 보기</a></p>
							<p class="logo_view_hide"><img src="<?=$cf_mobile_confirmjoin_txt?>" alt="confirmjoin_txt" /></p>
						</div>
						<?}?>
						<p class="logo_explain">이미지 업로드 정보 : Width: 미정px, Height: 미정px - 확장자:jpg, gif, png</p>
					</p>
				</td>
			</tr>
			<tr>
				<th>Web 이미지</th>
				<td>
					<p>
						<label for="cf_web_confirmjoin_title">가입팝업_타이틀 : </label>
						<? if($row['cf_web_confirmjoin_title']!=''){ $cf_web_confirmjoin_title = img_url_para($row['cf_web_confirmjoin_title'], $row['cf_date']); } ?>
						<input type="file" name="cf_web_confirmjoin_title" id="cf_web_confirmjoin_title" data-value="<?=$cf_web_confirmjoin_title?>" />
						
						<?if($row['cf_web_confirmjoin_title']!=''){?>
						<div class="logo_view">
							<p><a href="#logo_view" onclick="a_click_false();">가입팝업_타이틀 보기</a></p>
							<p class="logo_view_hide"><img src="<?=$cf_web_confirmjoin_title?>" alt="confirmjoin_title" /></p>
						</div>
						<?}?>
						<p class="logo_explain">이미지 업로드 정보 : Width: 미정px, Height: 미정px - 확장자:jpg, gif, png</p>
					</p>
					<p>
						<label for="cf_web_confirmjoin_txt">가입팝업_텍스트 : </label>
						<? if($row['cf_web_confirmjoin_txt']!=''){ $cf_web_confirmjoin_txt = img_url_para($row['cf_web_confirmjoin_txt'], $row['cf_date']); } ?>
						<input type="file" name="cf_web_confirmjoin_txt" id="cf_web_confirmjoin_txt" data-value="<?=$cf_web_confirmjoin_txt?>" />
						
						<?if($row['cf_web_confirmjoin_txt']!=''){?>
						<div class="logo_view">
							<p><a href="#logo_view" onclick="a_click_false();">가입팝업_텍스트 보기</a></p>
							<p class="logo_view_hide"><img src="<?=$cf_web_confirmjoin_txt?>" alt="confirmjoin_txt" /></p>
						</div>
						<?}?>
						<p class="logo_explain">이미지 업로드 정보 : Width: 미정px, Height: 미정px - 확장자:jpg, gif, png</p>
					</p>
				</td>
			</tr>
		</table>
			<div class="cf_submit_btn">
				<input type="submit" value="저장" class="btn_submit" accesskey="s">
			</div>
		</form>		
	</div>
</section><!-- cmsconfig_result -->
<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>