<?
include_once '_common.php'; // 공통


$sql_config = "SELECT * FROM  config_long_text";
$row = sql_fetch($sql_config);

$cf_submit_btn = '<div class="cf_submit_btn">
    <input type="submit" value="전체 저장" class="btn_submit" accesskey="s">
</div>';

$page_title = "안내문 기본설정";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
</section><!-- cms_title -->


<section id="cmsconfig_result">
	<div id="cr_bg">
		<form name="env_text_write_form" id="env_text_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return env_text_write_submit;" >
			<input type="hidden" id="mode" name="mode" value="mod"/> <!-- 수정모드 -->
			<input type="hidden" id="clt_no" name="clt_no" value="1"/> <!-- clt_no -->
			<table>
				<tr>
					<th>이용 약관</th>
					<td>
						<textarea name="clt_terms"><?=$row['clt_terms'];?></textarea>
					</td>
				</tr>
				<tr>
					<th>개인정보 취급방침</th>
					<td>
						<textarea name="clt_pinfo"><?=$row['clt_pinfo'];?></textarea>
					</td>
				</tr>
				<tr>
					<th>본인 인증</th>
					<td>
						<textarea name="clt_ipin"><?=$row['clt_ipin'];?></textarea>
					</td>
				</tr>
				<tr>
					<th>회원 탈퇴시 안내문</th>
					<td>
						<textarea name="clt_mb_leave"><?=$row['clt_mb_leave'];?></textarea>
					</td>
				</tr>
				<tr>
					<th>ID/패스워드 찾기</th>
					<td>
						<div class="clt_mb_find">
						<textarea placeholder="ID/패스워드 찾기 문구1" name="clt_mb_find1"><?=$row['clt_mb_find1'];?></textarea>
						<textarea placeholder="ID/패스워드 찾기 문구2" name="clt_mb_find2"><?=$row['clt_mb_find2'];?></textarea>
						<textarea placeholder="ID/패스워드 찾기 문구3" name="clt_mb_find3"><?=$row['clt_mb_find3'];?></textarea>
						<textarea placeholder="ID/패스워드 찾기 문구4" name="clt_mb_find4"><?=$row['clt_mb_find4'];?></textarea>
						<textarea placeholder="ID/패스워드 찾기 문구5" name="clt_mb_find5"><?=$row['clt_mb_find5'];?></textarea>
						</div>
					</td>
				</tr>
				<tr>
					<th>결제 안내</th>
					<td>
						<div class="clt_payment">
							<? foreach($nm_config['cf_payway'] as $payway_key => $payway_val) { ?>
							<p><?=$payway_val[2];?> 결제 안내</p>
							<textarea placeholder="<?=$payway_val[2];?> 결제 안내 문구" name="clt_<?=$payway_val[1];?>"><?=$row['clt_'.$payway_val[1]];?></textarea>
							<? } // end foreach ?>
						</div>
					</td>
				</tr>
				<tr>
					<th>회원 패스워드 변경 안내문</th>
					<td>
						<textarea name="clt_mb_pw_mod"><?=$row['clt_mb_pw_mod'];?></textarea>
					</td>
				</tr>
				<tr>
					<th>충전소 안내문</th>
					<td>
						<div class="clt_recharge">
							<textarea placeholder="충전소 안내 문구1" name="clt_recharge1"><?=$row['clt_recharge1'];?></textarea>
							<textarea placeholder="충전소 안내 문구2" name="clt_recharge2"><?=$row['clt_recharge2'];?></textarea>
							<textarea placeholder="충전소 안내 문구3" name="clt_recharge3"><?=$row['clt_recharge3'];?></textarea>
							<textarea placeholder="충전소 안내 문구4" name="clt_recharge4"><?=$row['clt_recharge4'];?></textarea>
							<textarea placeholder="충전소 안내 문구5" name="clt_recharge5"><?=$row['clt_recharge5'];?></textarea>
							<textarea placeholder="충전소 안내 문구6" name="clt_recharge6"><?=$row['clt_recharge6'];?></textarea>
						</div>
					</td>
				</tr>
			</table>
			<?php echo $cf_submit_btn; ?>
		</form>
	</div>
</section><!-- cmsconfig_result -->




<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>