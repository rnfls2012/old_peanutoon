<?
include_once '_common.php'; // 공통
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'clt_no','clt_terms','clt_pinfo','clt_ipin','clt_mb_leave');
array_push($para_list, 'clt_mb_find1','clt_mb_find2','clt_mb_find3','clt_mb_find4','clt_mb_find5');
array_push($para_list, 'clt_mobx', 'clt_card', 'clt_payco', 'clt_toss');
array_push($para_list, 'clt_sccl', 'clt_schm', 'clt_scbl', 'clt_acnt');
array_push($para_list, 'clt_mb_pw_mod');
array_push($para_list, 'clt_recharge1', 'clt_recharge2', 'clt_recharge3', 'clt_recharge4', 'clt_recharge5', 'clt_recharge6');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'clt_mb_find1','clt_mb_find2','clt_mb_find3','clt_mb_find4','clt_mb_find5');
array_push($blank_list, 'clt_mobx', 'clt_card', 'clt_payco', 'clt_toss');
array_push($blank_list, 'clt_sccl', 'clt_schm', 'clt_scbl', 'clt_acnt');
array_push($blank_list, 'clt_mb_pw_mod');
array_push($blank_list, 'clt_recharge1', 'clt_recharge2', 'clt_recharge3', 'clt_recharge4', 'clt_recharge5', 'clt_recharge6');

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

$dbtable = "config_long_text";
$dbt_primary = "clt_no";
$para_primary = "clt_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

/* 수정 */
if($_mode == 'mod'){
	
	$_er_mod_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
		/* DB 저장 */
		if(sql_query(stripslashes($sql_mod))){
			$db_result['msg'] = '기본설정 데이터가 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '수정 에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

/*
echo $db_result['state']."<br/>";
echo $db_result['msg']."<br/>";
*/

goto_url($_SERVER['HTTP_REFERER']);
die;

?>