<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode','tksk_no','tksk_name','tksk_campaign','tksk_campaign_complete','tksk_bgcolor');
array_push($para_list, 'tksk_adult', 'tksk_pop');
array_push($para_list, 'tksk_date_type','tksk_state', 'tksk_date_start','tksk_date_end');
array_push($para_list, 'state', 'event_reg_date','event_mod_date','tksk_holiday','tksk_holiweek');
array_push($para_list, 'tksk_reg_date', 'tksk_mod_date');
array_push($para_list, 'tksk_link','tksk_link_complete');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'tksk_no');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'tksk_date_start', 'tksk_date_end', 'tksk_holiday', 'tksk_holiweek');
array_push($blank_list, 'tksk_campaign_complete');
array_push($blank_list, 'tksk_link','tksk_link_complete');

/* 빈칸 PARAMITER 시 NULL 처리 */
array_push($null_list, 'tksk_holiday','tksk_holiweek');

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

/* 이벤트리스트DB처리 */
$para_event_list = array('epc_comics','epc_sale_pay','epc_sale_e','epc_free_e');
/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
foreach($para_event_list as $para_event_key => $para_event_val){
	for($i=0; $i<count($_POST[$para_event_val]); $i++){
		if(num_check($_POST[$para_event_val][$i]) == false && $_POST[$para_event_val][$i] != '' && $_POST[$para_event_val][$i] != '0'){
			alert($_POST[$para_event_val][$i]."값에 숫자가 아닌 값이 들어 있습니다.", $_SERVER['HTTP_REFERER']);
			die;
		}
		$_POST[$para_event_val][$i] = intval($_POST[$para_event_val][$i]);
	}
	${'_'.$para_event_val} = $_POST[$para_event_val]; /* 변수담기 */
}

// 상태와 날짜
$get_date_type = get_date_type($_tksk_date_type, $_tksk_date_start, $_tksk_date_end);
$_tksk_state = $get_date_type['state'];
$_tksk_date_start = $get_date_type['date_start'];
$_tksk_date_end = $get_date_type['date_end'];

$dbtable = "ticketsocket";
$dbt_primary = "tksk_no";
$para_primary = "tksk_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};


/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

if($_mode == 'reg'){
	/* 고정값 */
	$_tksk_reg_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);
	
	/* DB 저장 */
	if(sql_query($sql_reg)){
		$db_result['msg'] = $tksk_name.'의 데이터가 등록되였습니다.';
		
		// 등록된 티켓소켓 정보 가져오기
		$sql_tksk_no = " select * from ticketsocket order by tksk_no desc limit 0,1 ";
		$row_tksk_no = sql_fetch($sql_tksk_no);
		$folder_tksk_no = $row_tksk_no['tksk_no'];		
		
		// 해당 경로 디렉토리 및 파일생성
		mkdirAll('/tksk/ics/'.$folder_tksk_no); // 디렉토리 생성

		if($row_tksk_no['tksk_link'] == ''){
			// 페이지일 경우
			cpdirAll('/tksk/ics/create_page', '/tksk/ics/'.$folder_tksk_no);
		}else{
			// 링크일 경우
			cpdirAll('/tksk/ics/create_link', '/tksk/ics/'.$folder_tksk_no);
		}
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}
	
/* 수정 */
}else if($_mode == 'mod'){
	$_tksk_mod_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{		
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $tksk_name.'의 데이터가 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}
	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 배너 이미지 삭제 */
		$row_ep = sql_fetch("select * from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");

		/* 데이터 삭제 */
		sql_query("delete from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
		
		$db_result['msg'] = $tksk_name.'의 데이터가 삭제되였습니다.';
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

pop_close($db_result['msg'], '', $db_result['error']);
die;
?>