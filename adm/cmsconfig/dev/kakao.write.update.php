<?php

include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode', 'kt_no', 'kt_temp_no', 'kt_temp_usage', 'kt_temp_reg_member','kt_temp_use');
array_push($para_list, 'kt_temp_reg_date','kt_temp_mod_date');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'kt_temp_no');

/* 빈칸 PARAMITER 허용 */

/* 빈칸 PARAMITER 시 NULL 처리 */

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

$dbtable = "kakao_template";
$dbt_primary = "kt_no";
$para_primary = "kt_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

if($_mode == 'reg'){
    /* 고정값 */
    $_kt_temp_reg_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
    $_kt_temp_mod_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */

    /* 파라미터 sql-insert문 생성 */
    $sql_reg = para_sql_insert($dbtable);

    /* DB 저장 */
    if(sql_query($sql_reg)){
        $db_result['msg'] = $_kt_temp_no.'의 데이터가 등록되였습니다.';
    }else{
        $db_result['state'] = 1;
        $db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
        $db_result['error'] = $sql_reg;
    }

    /* 수정 */
}else if($_mode == 'mod'){
    $_kt_temp_mod_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
    if(${'_'.$para_primary} == '') {
        $db_result['state'] = 1;
        $db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
    }else{
        /* 파라미터 sql-update문 생성 */
        $sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
        $sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
        /* DB 저장 */
        if(sql_query($sql_mod)){
            $db_result['msg'] = $_kt_temp_no.'의 데이터가 수정되였습니다.';
        }else{
            $db_result['state'] = 1;
            $db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
            $db_result['error'] = $sql_mod;
        }
    }

    /* 삭제 */
}else if($_mode == 'del'){
    if(${'_'.$para_primary} == '') {
        $db_result['state'] = 1;
        $db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
    }else{
        /* 데이터 삭제 */
        sql_query("delete from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
        $db_result['msg'] = $tksk_name.'의 데이터가 삭제되였습니다.';
    }

    /* 예외 */
}else{
    echo "mode를 다시 확인해주시기 바람니다.";
    die;
    /* 넘어온 값 검사 */
    foreach($para_list as $para_key => $para_val){
        echo $para_val.":".${'_'.$para_val}."<br/><br/>";
    }
}

pop_close($db_result['msg'], '', $db_result['error']);
die;
