<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_emf_no = tag_filter($_REQUEST['emf_no']);

$sql = "SELECT * from epromotion_full WHERE emf_no = '$_emf_no' ";
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
			 $sql_list = "";
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}

/* 대분류로 제목 */
$page_title = "풀 페이지 프로모션";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section>

<section id="emf_write">
	<form name="emf_write_form" id="emf_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return emf_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="emf_no" name="emf_no" value="<?=$_emf_no;?>"/><!-- 수정/삭제시 번호 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="emf_no">번호</label></th>
					<td><?=$_emf_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="emf_name"><?=$required_arr[0];?>프로모션 명</label></th>
					<td>
						<input type="text" placeholder="프로모션 명 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="emf_name" id="emf_name" value="<?=$row['emf_name'];?>" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<th><label><?=$required_arr[0];?>프로모션 기간</label></th>
					<td>
						<div id="service_color" class="input_btn">
							<input type="radio" name="emf_date_type" id="emf_date_type0" value="y" <?=get_checked('', $row['emf_date_type']);?> <?=get_checked('y', $row['emf_date_type']);?>  onclick="emf_date_type_chk(this.value);" />
							<label for="emf_date_type0">기간만적용</label>
							<input type="radio" name="emf_date_type" id="emf_date_type1" value="n" <?=get_checked('n', $row['emf_date_type']);?> onclick="emf_date_type_chk(this.value);" />
							<label for="emf_date_type1">무기간</label>
						</div>
						<div class="emf_date_type_view">
							<span for="s_date">시작일 :</span>
							<input type="text" class="s_date readonly" placeholder="클릭하세요" name="emf_start_date" id="s_date" value="<?=$row['emf_start_date'];?>" autocomplete="off" readonly /> ~ 
							<span for="e_date">종료일 :</span>
							<input type="text" class="e_date readonly" placeholder="클릭하세요" name="emf_end_date" id="e_date" value="<?=$row['emf_end_date'];?>" autocomplete="off" readonly />
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="emf_adult"><?=$required_arr[0];?>ICS 등급</label></th>
					<td>
						<div id="emf_adult" class="input_btn">
							<input type="radio" name="emf_adult" id="emf_adultn" value="n" <?=get_checked('', $row['emf_adult']);?> <?=get_checked('n', $row['emf_adult']);?> />
							<label for="emf_adultn">청소년</label>
							<input type="radio" name="emf_adult" id="emf_adulty" value="y" <?=get_checked('y', $row['emf_adult']);?> />
							<label for="emf_adulty">성인</label>
						</div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="state">프로모션 상태</label></th>
					<td><?=$d_state[$row['emf_state']];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="emf_reg_date">프로모션 등록일</label></th>
					<td><?=$row['emf_reg_date'];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="emf_mod_date">프로모션 수정일</label></th>
					<td><?=$row['emf_mod_date'];?></td>
				</tr>
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- event_free_dc_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>