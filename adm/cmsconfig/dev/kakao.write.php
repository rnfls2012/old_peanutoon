<?php

include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_kt_no = tag_filter($_REQUEST['kt_no']);

$sql = "SELECT * FROM kakao_template WHERE kt_no = '".$_kt_no."'";

/* 모드설정 */
if( $_mode=='' ){
    $_mode='reg'; /* 등록 */
}

switch($_mode){
    case "mod": $mode_text = "수정";
        break;
    default: $mode_text = "등록";
        break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
    $row = sql_fetch($sql);
}

if ( !isset($row['kt_temp_reg_member']) ) {
    $member_no = $nm_member['mb_no'];
} else {
    $member_no = $row['kt_temp_reg_member'];
}
/* 대분류로 제목 */
$page_title = "알림톡 템플릿";
$cms_head_title = $page_title;
$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
    <link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
    <script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>

    <section id="cms_page_title">
        <h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
    </section><!-- event_free_dc_head -->

    <section id="cup_write">
        <form name="kt_write_form" id="kt_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return kt_write_write_submit();">
            <input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
            <input type="hidden" id="kt_no" name="kt_no" value="<?=$_kt_no;?>"/><!-- 수정/삭제 시 번호 -->
            <input type="hidden" name="kt_temp_reg_member" value="<?=$member_no?>" autocomplete="off"/>

            <table>
                <tbody>
                    <tr>
                        <th><label for="kt_temp_no"><?=$required_arr[0];?>템플릿 번호</label></th>
                        <td>
                            <input type="text" placeholder="승인된 템플릿 번호를 입력하세요. <?=$required_arr[2];?>" <?=$required_arr[1];?> name="kt_temp_no" id="kt_temp_no" value="<?=$row['kt_temp_no'];?>" autocomplete="off" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="kt_temp_usage"><?=$required_arr[0];?>템플릿 용도</label></th>
                        <td>
                            <input type="text" placeholder="템플릿 용도를 입력해주세요. <?=$required_arr[2];?>" <?=$required_arr[1];?> name="kt_temp_usage" id="kt_temp_usage" value="<?=$row['kt_temp_usage'];?>" autocomplete="off" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="kt_temp_use"><?=$required_arr[0];?>템플릿 사용여부</label></th>
                        <td>
                            <div id="kt_temp_use" class="input_btn">
                                <input type="radio" name="kt_temp_use" id="kakao_usey" value="y" <?=get_checked('y', $row['kt_temp_use']);?> <?=get_checked('', $row['kt_temp_use']);?> />
                                <label for="kakao_usey">사용</label>
                                <input type="radio" name="kt_temp_use" id="kakao_usen" value="n" <?=get_checked('n', $row['kt_temp_use']);?> />
                                <label for="kakao_usen">미사용</label>
                            </div>
                        </td>
                    </tr>
                    <?if($_mode != 'reg'){?>
                    <tr>
                        <th><label for="kakao_link">등록날짜</label></th>
                        <td><?=$row['kt_temp_reg_date']?></td>
                    </tr>
                    <?}?>
                    <?if($_mode != 'reg'){?>
                    <tr>
                        <th><label>수정날짜</label></th>
                        <td><?=$row['kt_temp_mod_date']?></td>
                    </tr>
                    <?}?>
                    <tr>
                        <td colspan='2' class="submit_btn">
                            <input type="submit" value="<?=$mode_text?>" />
                            <input type="reset" value="취소" onclick="self.close()" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </section><!-- event_free_dc_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>