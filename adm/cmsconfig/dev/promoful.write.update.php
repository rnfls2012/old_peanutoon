<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER

/* PARAMITER CHECK */
array_push($para_list, 'mode', 'emf_no', 'emf_name', 'emf_adult');
array_push($para_list, 'emf_date_type', 'emf_state', 'emf_start_date', 'emf_end_date');
array_push($para_list, 'emf_reg_date','emf_mod_date', 'emf_link');

/* 숫자 PARAMITER 체크 */
array_push($para_num_list, 'emf_no');

/* 빈칸 PARAMITER 허용 */
array_push($blank_list, 'emf_date_start', 'emf_date_end');

/* 빈칸 PARAMITER 시 NULL 처리 */

/* DB field 아닌 목록 */
array_push($db_field_exception, 'mode');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

// 상태와 날짜
$get_date_type = get_date_type($_emf_date_type, $_emf_start_date, $_emf_end_date);
$_emf_state = $get_date_type['state'];
$_tksk_start_date = $get_date_type['date_start'];
$_emf_end_date = $get_date_type['date_end'];

$dbtable = "epromotion_full";
$dbt_primary = "emf_no";
$para_primary = "emf_no";
${'_'.$dbt_primary} = ${'_'.$para_primary};

/* 등록 */
$db_result['state'] = 0;
$db_result['msg'] = '';
$db_result['error'] = '';

if($_mode == 'reg'){
	/* 고정값 */
	$_emf_reg_date = substr(NM_TIME_YMDHIS, 0, 16); /* 최초등록일 */
	
	/* 파라미터 sql-insert문 생성 */
	$sql_reg = para_sql_insert($dbtable);

	/* DB 저장 */
	if(sql_query($sql_reg)){
		// 등록된 프로모션 정보 가져오기
		$sql_emf_no = "select * from epromotion_full order by emf_no desc limit 0,1";
		$row_emf_no = sql_fetch($sql_emf_no);
		$_emf_no = $row_emf_no['emf_no'];

		// 프로모션 링크 업데이트
		$_emf_link = NM_URL."/promoful/index.php?promo=".$_emf_no;
		$sql_upd = "update epromotion_full set emf_link = '".$_emf_link."' where emf_no = '".$_emf_no."'";
		
		if(sql_query($sql_upd)) {
			$db_result['msg'] = $_emf_name.'의 데이터가 등록되였습니다.';
		} else {
			$db_result['state'] = 1;
			$db_result['msg'] = '에러가 발생하여 링크가 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_upd;
		} // end else
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
		$db_result['error'] = $sql_reg;
	}
	
/* 수정 */
}else if($_mode == 'mod'){
	$_emf_mod_date = substr(NM_TIME_YMDHIS, 0, 16); /* 수정일 */
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{		
		/* 파라미터 sql-update문 생성 */
		$sql_mod = para_sql_update($dbtable, $para_primary, $dbt_primary);
		$sql_mod.= " WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'";
		
		/* DB 저장 */
		if(sql_query($sql_mod)){
			$db_result['msg'] = $tksk_name.'의 데이터가 수정되였습니다.';
		}else{
			$db_result['state'] = 1;
			$db_result['msg'] = '에러가 발생하여 저장되지 않았습니다.\n';
			$db_result['error'] = $sql_mod;
		}
	}

/* 삭제 */
}else if($_mode == 'del'){
	if(${'_'.$para_primary} == '') { 
		$db_result['state'] = 1;
		$db_result['msg'] = '필수 변수인 '.$para_primary.'값이 없습니다.';
	}else{
		/* 데이터 삭제 */
		sql_query("delete from ".$dbtable." WHERE ".$dbt_primary."='".${'_'.$dbt_primary}."'");
		$db_result['msg'] = $tksk_name.'의 데이터가 삭제되였습니다.';
	}

/* 예외 */
}else{
	echo "mode를 다시 확인해주시기 바람니다.";
	die;
	/* 넘어온 값 검사 */
	foreach($para_list as $para_key => $para_val){
		echo $para_val.":".${'_'.$para_val}."<br/><br/>";
	}
}

pop_close($db_result['msg'], '', $db_result['error']);
die;
?>