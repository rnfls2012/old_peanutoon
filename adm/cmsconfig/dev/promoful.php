<?
include_once '_common.php'; // 공통

/* PARAMITER */

// 기본적으로 몇개 있는 지 체크;
$sql_emf_total = "select count(*) as emf_count from epromotion_full where 1 ";
$total_emf = sql_count($sql_emf_total, 'emf_count');

/* 데이터 가져오기 */
$emf_where = "";

// 검색단어+ 검색타입
if($_s_text){
	$emf_where .= "and emf_name like '%$_s_text%' ";
}

// 정렬
if($_order_field == null || $_order_field == ""){ $_order_field = "emf_no"; }
if($_order == null || $_order == ""){ $_order = "desc"; }
$emf_order = "order by ".$_order_field." ".$_order;

$sql_emf = "";
$field_emf = " * "; // 가져올 필드 정하기
$limit_emf = "";

$sql_emf = "select $field_emf from epromotion_full where 1 $emf_where $emf_order $limit_emf";
$result = sql_query($sql_emf);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('번호','emf_no',0));
array_push($fetch_row, array('프로모션 명','emf_name',0));
array_push($fetch_row, array('프로모션 링크','emf_link',0));
array_push($fetch_row, array('시작일','emf_start_date',0));
array_push($fetch_row, array('마감일','emf_end_date',0));
array_push($fetch_row, array('프로모션 등급','emf_adult',0));
array_push($fetch_row, array('프로모션 상태','emf_state',0));
array_push($fetch_row, array('등록/수정일','emf_date',0));
array_push($fetch_row, array('관리','emf_management',0));

$page_title = "풀 페이지 프로모션";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

<section id="cms_title">
	<h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
	<div>
		<strong>총 <?=$page_title?> : <?=number_format($total_emf);?> 건</strong>
	</div>
	<div class="write">
		<button onclick="popup('<?=$_cms_write;?>','emf_write', <?=$popup_cms_width;?>, 700);"><?=$page_title?> 등록</button>
	</div>
</section><!-- cms_title -->

<section id="cms_search">
	<h2 class="hidden"><?=$cms_head_title?> 검색</h2>
	<form name="emf_search_form" id="emf_search_form" method="post" action="<?=$_cms_self;?>" onsubmit="return emf_search_submit();">
		<div class="cs_bg">
			<div class="cs_form">
				<div class="cs_er_type_btn">
					<? tag_selects($d_state, "emf_state", $_emf_state, 'y', '상태 전체', ''); ?>
					<? tag_selects($d_adult, "emf_adult", $_emf_adult, 'y', '등급 전체', ''); ?>
				</div>
				<div class="cs_date_btn">
					<? tag_selects($s_limit_list, "s_limit", $_s_limit, 'n'); ?>
					<input type="text" id="s_text" name="s_text" value="<?=$_s_text?>">
				</div>
			</div>
			<div class="cs_submit push_submit">
				<input type="submit" class="<?=$cs_submit_h;?>" data-base_h="30" data-on_h="130"  value="검색" id="cms_submit">
			</div>
		</div><!-- cs_bg -->
	</form>
</section><!-- cms_search -->

<section id="tksk_result">
	<h3 id="cr_thead_mg_add">검색 리스트 
		<strong>검색 결과 수: <?=number_format($row_size);?>건</strong>
	</h3>
	<div id="cr_bg">
		<table>
			<thead id="cr_thead">
				<tr>
				<?
				//정렬표기
				$th_order = "desc";
				if($_order == 'desc'){ 
					$th_order = "asc";
				}
				foreach($fetch_row as $fetch_key => $fetch_val){

					$th_title = $th_ahref_s = $th_ahref_e = "";
					if($fetch_val[2] == 1){
						$th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
						$th_ahref_e = '</a>';
					}

					$th_title = $th_ahref_s.$fetch_val[0].$th_ahref_e;
					$th_class = $cp_css.$fetch_val[1];
				?>
					<th class="<?=$th_class?>"><?=$th_title?></th>
				<?}?>
				</tr>
			</thead>
			<tbody>
				<?foreach($row_data as $dvalue_key => $dvalue_val){	
					// 컨텐츠 수정/삭제
					$popup_url = $_cms_write."?emf_no=".$dvalue_val['emf_no'];
					$popup_mod_url = $popup_url."&mode=mod";
					$popup_del_url = $popup_url."&mode=del";

					// 등록일
					$db_emf_date_ymd = get_ymd($dvalue_val['emf_reg_date']);
					$db_emf_date_his = get_his($dvalue_val['emf_reg_date']);

					// 수정일
					if($dvalue_val['emf_mod_date'] != ''){
						$db_emf_date_ymd = get_ymd($dvalue_val['emf_mod_date']);
						$db_emf_date_his = get_his($dvalue_val['emf_mod_date']);
					}
				?>
				<tr class="result_hover">
					<td class="<?=$cp_css.$fetch_row[0][1]?> text_center"><?=$dvalue_val['emf_no'];?></td>
					<td class="<?=$cp_css.$fetch_row[1][1]?> text_center"><?=$dvalue_val['emf_name'];?></td>
					<td class="<?=$cp_css.$fetch_row[2][1]?> text_center"><?=$dvalue_val['emf_link'];?></td>
					<td class="<?=$cp_css.$fetch_row[3][1]?> text_center"><?=$dvalue_val['emf_start_date'];?></td>
					<td class="<?=$cp_css.$fetch_row[4][1]?> text_center"><?=$dvalue_val['emf_end_date'];?></td>
					<td class="<?=$cp_css.$fetch_row[5][1]?> text_center"><?=$d_adult[$dvalue_val['emf_adult']];?></td>
					<td class="<?=$cp_css.$fetch_row[6][1]?> text_center"><?=$d_state[$dvalue_val['emf_state']];?></td>
					<td class="<?=$cp_css.$fetch_row[7][1]?> text_center"><?=$db_emf_date_ymd;?><br/><?=$db_emf_date_his;?></td>
					<td class="<?=$cp_css.$fetch_row[8][1]?> text_center">
						<button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','emf_mod_write', <?=$popup_cms_width;?>, 500);">수정</button>
					</td>
				</tr>
				<?} // end foreach?>
			</tbody>
		</table>
	</div>
</section><!-- epromotion_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail.sub.php'; // 해더 ?>