<?
include_once '_common.php'; // 공통

/* PARAMITER */
$_mode = tag_filter($_REQUEST['mode']);
$_tksk_no = tag_filter($_REQUEST['tksk_no']);

$sql = "SELECT * from ticketsocket WHERE tksk_no = '$_tksk_no' ";
/* 모드설정 */
if($_mode==''){$_mode='reg'; /* 등록 */}
switch($_mode){
	case "mod": $mode_text = "수정";
	break;
	case "del": $mode_text = "삭제";
	break;
	default: $mode_text = "등록";
			 $sql = "";
			 $sql_list = "";
	break;
}

/* 수정 또는 삭제라면...*/
if($sql != ''){
	$row = sql_fetch($sql);
}

/* 대분류로 제목 */
$page_title = "티켓소켓";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_win.sub.php'; // 해더
?>
<link rel="stylesheet" type="text/css" href="<?=$_cms_pop_css;?><?=vs_para();?>"/>
<script type="text/javascript" src="<?=$_cms_pop_js;?><?=vs_para();?>"></script>
<script type="text/javascript" src="<?=NM_URL."/js/jscolor.js";?><?=vs_para();?>"></script>

<section id="cms_page_title">
	<h1><?=$cms_head_title;?> <?=$mode_text;?></h1>
</section><!-- event_free_dc_head -->

<section id="cup_write">
	<form name="tksk_write_form" id="tksk_write_form" enctype="multipart/form-data" method="post" action="<?=$_cms_update;?>" onsubmit="return tksk_write_write_submit();">
		<input type="hidden" id="mode" name="mode" value="<?=$_mode;?>"/><!-- 입력모드 -->
		<input type="hidden" id="tksk_no" name="tksk_no" value="<?=$_tksk_no;?>"/><!-- 수정/삭제시 충전/지급 번호 -->

		<table>
			<tbody>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="tksk_no">번호</label></th>
					<td><?=$_tksk_no;?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="tksk_name"><?=$required_arr[0];?>ICS명</label></th>
					<td>
						<input type="text" placeholder="ICS명 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="tksk_name" id="tksk_name" value="<?=$row['tksk_name'];?>" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<th><label for="tksk_campaign"><?=$required_arr[0];?>캠페인ID</label></th>
					<td>
						<input type="text" placeholder="캠페인ID 번호 입력해주세요<?=$required_arr[2];?>" <?=$required_arr[1];?> name="tksk_campaign" id="tksk_campaign" value="<?=$row['tksk_campaign'];?>" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<th><label for="tksk_link">캠페인ID 연결링크</label></th>
					<td>
						<input type="text" placeholder="캠페인ID 연결링크 입력해주세요" name="tksk_link" id="tksk_link" value="<?=$row['tksk_link'];?>" autocomplete="off" />
					</td>
				</tr>
				<? /* 사용 안함
				<tr>
					<th><label for="tksk_campaign_complete">캠페인ID_complete</label></th>
					<td>
						<input type="text" placeholder="캠페인ID_complete 번호 입력해주세요" name="tksk_campaign_complete" id="tksk_campaign_complete" value="<?=$row['tksk_campaign_complete'];?>" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<th><label for="tksk_link_complete">캠페인ID_complete 연결링크</label></th>
					<td>
						<input type="text" placeholder="캠페인ID_complete 연결링크 입력해주세요" name="tksk_link_complete" id="tksk_link_complete" value="<?=$row['tksk_link_complete'];?>" autocomplete="off" />
					</td>
				</tr>
				<? */ ?>
				<tr>
					<th><label><?=$required_arr[0];?>ICS 기간</label></th>
					<td>
						<div id="service_color" class="input_btn">
							<input type="radio" name="tksk_date_type" id="tksk_date_type0" value="y" <?=get_checked('', $row['tksk_date_type']);?> <?=get_checked('y', $row['tksk_date_type']);?>  onclick="tksk_date_type_chk(this.value);" />
							<label for="tksk_date_type0">기간만적용</label>
							<input class="still" type="radio" name="tksk_date_type" id="tksk_date_type1" value="m" <?=get_checked('m', $row['tksk_date_type']);?> onclick="tksk_date_type_chk(this.value);" />
							<label class="still" for="tksk_date_type1">매월(날짜빈칸시 무기간)</label>
							<input class="still" type="radio" name="tksk_date_type" id="tksk_date_type2" value="w" <?=get_checked('w', $row['tksk_date_type']);?> onclick="tksk_date_type_chk(this.value);" />
							<label class="still" for="tksk_date_type2">매주(날짜빈칸시 무기간)</label>
							<input type="radio" name="tksk_date_type" id="tksk_date_type3" value="n" <?=get_checked('n', $row['tksk_date_type']);?> onclick="tksk_date_type_chk(this.value);" />
							<label for="tksk_date_type3">무기간</label>
						</div>
						<div class="<? echo ($row['tksk_date_type'] != 'n')?"":"still";?> tksk_date_type_view margin_top10">
							<span for="s_date">시작일 :</span>
							<input type="text" class="s_date readonly" placeholder="클릭하세요" name="tksk_date_start" id="s_date" value="<?=$row['tksk_date_start'];?>" autocomplete="off" readonly /> ~ 
							<span for="e_date">종료일 :</span>
							<input type="text" class="e_date readonly" placeholder="클릭하세요" name="tksk_date_end" id="e_date" value="<?=$row['tksk_date_end'];?>" autocomplete="off" readonly />
						</div>
						<div class="tksk_holiday_view still margin_top10">
							<span for="tksk_holiweek">매월</span>
							<?	tag_selects($day_list, "tksk_holiday", $row['tksk_holiday'], 'y', '사용안함', ''); ?>
						</div>
						<div class="tksk_holiweek_view still margin_top10">
							<span for="tksk_holiweek">매주</span>
							<?	tag_selects($week_list, "tksk_holiweek", $row['tksk_holiweek'], 'y', '사용안함', ''); ?>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="tksk_adult"><?=$required_arr[0];?>ICS 등급</label></th>
					<td>
						<div id="tksk_adult" class="input_btn">
							<input type="radio" name="tksk_adult" id="tksk_adultn" value="n" <?=get_checked('', $row['tksk_adult']);?> <?=get_checked('n', $row['tksk_adult']);?> />
							<label for="tksk_adultn">청소년</label>
							<input type="radio" name="tksk_adult" id="tksk_adulty" value="y" <?=get_checked('y', $row['tksk_adult']);?> />
							<label for="tksk_adulty">성인</label>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="tksk_pop"><?=$required_arr[0];?>ICS 팝업만 입장여부</label></th>
					<td>
						<div id="tksk_pop" class="input_btn">
							<input type="radio" name="tksk_pop" id="tksk_popn" value="n" <?=get_checked('', $row['tksk_pop']);?> <?=get_checked('n', $row['tksk_pop']);?> />
							<label for="tksk_adultn">미사용</label>
							<input type="radio" name="tksk_pop" id="tksk_popy" value="y" <?=get_checked('y', $row['tksk_pop']);?> />
							<label for="tksk_pop">사용</label>
						</div>
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="state">ICS 상태</label></th>
					<td><?=$d_state[$row['tksk_state']];?></td>
				</tr>
				<?}?>
				<tr>
					<th><label for="tksk_bgcolor"><?=$required_arr[0];?>배경색</label></th>
					<td>
						<input type="text" <?=$required_arr[1];?> name="tksk_bgcolor" id="tksk_bgcolor" class="color" value="<?=$row['tksk_bgcolor'];?>" autocomplete="off" />
					</td>
				</tr>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="tksk_reg_date">티켓소켓 등록일</label></th>
					<td><?=$row['tksk_reg_date'];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<tr>
					<th><label for="tksk_mod_date">티켓소켓 수정일</label></th>
					<td><?=$row['tksk_mod_date'];?></td>
				</tr>
				<?}?>
				<?if($_mode != 'reg'){?>
				<!--
				<tr>
					<th><label for="tksk_click">티켓소켓 클릭수</label></th>
					<td><?=$row['tksk_click'];?></td>
				</tr>
				-->
				<?}?>
				<tr>
					<td colspan='2' class="submit_btn">
						<input type="submit" value="<?=$mode_text?>" />
						<input type="reset" value="취소" onclick="self.close()" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</section><!-- event_free_dc_write -->

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>