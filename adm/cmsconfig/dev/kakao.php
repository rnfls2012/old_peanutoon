<? include_once '_common.php'; // 공통

// 기본적으로 몇개 있는 지 체크;
$sql_total_kakao = "SELECT COUNT(*) as kakao_cnt FROM kakao_template";
$total_kakao = sql_count($sql_total_kakao, 'kakao_cnt');

/* 데이터 가져오기 */
$kakao_where = '';

$kakao_order = "ORDER BY kt_no";

$kakao_field = " * ";

$kakao_sql = "SELECT $kakao_field FROM kakao_template WHERE 1 $kakao_where $kakao_order";
$result = sql_query($kakao_sql);
$row_size = sql_num_rows($result);

/* 출력필드리스트 - array('표제목','정렬필드명') */
$fetch_row = array();
array_push($fetch_row, array('템플릿 번호','kt_temp_no',0));
array_push($fetch_row, array('템플릿 용도','kt_temp_usage',0));
array_push($fetch_row, array('템플릿 사용여부', 'kt_temp_use',0));
array_push($fetch_row, array('템플릿 등록날짜','kt_temp_reg_date',0));
array_push($fetch_row, array('템플릿 수정날짜','kt_temp_mod_date',0));

array_push($fetch_row, array('관리','kakao_menage',0));

$page_title = "알림톡 템플릿";
$cms_head_title = $page_title;

$head_title = $nm_config['cf_title']."-CMS-".$cms_head_title;
include_once NM_ADM_PATH.'/_head_menu.sub.php'; // 해더
include_once NM_ADM_PATH.'/_page_v1.php'; // 데이터 및 페이지 처리 -> $row_data / page_view()
?>
    <link rel="stylesheet" type="text/css" href="<?=$_cms_css;?><?=vs_para();?>"/>
    <script type="text/javascript" src="<?=$_cms_js;?><?=vs_para();?>"></script>

    <section id="cms_title">
        <h1><?=$cms_head_title?> <?=$cms_page_title?></h1>
        <div>
            <strong><?=$page_title?> 총 : <?=number_format($total_kakao);?> 건</strong>
        </div>
        <div class="write">
            <button onclick="popup('<?=$_cms_write;?>','free_dc_wirte', <?=$popup_cms_width;?>, 700);"><?=$page_title?> 등록</button>
        </div>
    </section>

    <section id="kakao_result">
        <h3></h3>
        <div id="cr_bg">
            <table>
                <thead id="">
                <tr>
                    <?
                    foreach($fetch_row as $fetch_key => $fetch_val){

                        $th_title = $th_ahref_s = $th_ahref_e = $th_title_giho = "";
                        if($fetch_val[2] == 1){
                            $th_ahref_s = '<a href="'.$_cms_self.'?v=2&page=1&order_field='.$fetch_val[1].'&order='.$th_order.'">';
                            $th_ahref_e = '</a>';
                        }
                        if($fetch_val[1] == $_order_field){
                            $th_title_giho = $order_giho;
                        }
                        $th_title = $th_ahref_s.$fetch_val[0].$th_title_giho.$th_ahref_e;
                        $th_class = $fetch_val[1];
                        ?>
                        <th class="<?=$th_class?>"><?=$th_title?></th>
                    <?}?>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($row_data as $key => $val) {
                    $popup_url = $_cms_write."?kt_no=".$val['kt_no'];
                    $popup_mod_url = $popup_url."&mode=mod";
                ?>
                    <tr class="result_hover">
                        <td class="<?=$fetch_row[0][1]?> text_center"><?=$val[$fetch_row[0][1]];?></td>
                        <td class="<?=$fetch_row[1][1]?> text_center"><?=$val[$fetch_row[1][1]];?></td>
                        <td class="<?=$fetch_row[2][1]?> text_center"><?=$val[$fetch_row[2][1]];?></td>
                        <td class="<?=$fetch_row[3][1]?> text_center"><?=$val[$fetch_row[3][1]];?></td>
                        <td class="<?=$fetch_row[4][1]?> text_center"><?=$val[$fetch_row[4][1]];?></td>
                        <td>
                            <button class="mod_btn" onclick="popup('<?=$popup_mod_url;?>','kakao_template_mod', 600, 400);">수정</button>
                        </td>
                    </tr>
                <?
                }
                ?>

                </tbody>
            </table>
        </div>
    </section><!-- event_recharge_result -->

<? page_view(); /* 페이지처리 */ ?>

<? include_once NM_ADM_PATH.'/_tail_date.sub.php'; // 해더 ?>