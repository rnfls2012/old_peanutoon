<?
include_once '_common.php';
// 이전 페이지 저장
$_SESSION['pre_page'] = NM_URL.$_SERVER['REQUEST_URI'];

//cms h1 텍스트
$cms_page_title = "관리 페이지";

/* 메뉴 */
$menu_url[] = NM_ADM_URL.'/comics/'; /* 0 */
$menu[] = array (
	/* 1뎁스 */
	array('코믹스', $menu_url[count($menu_url)-1], 'comics'), 
	/* 2뎁스 */
	array('코믹스 정보', $menu_url[count($menu_url)-1].'list/', 'list'), 
	array('코믹스 파트너', $menu_url[count($menu_url)-1].'partner/', 'partner'), 
	array('코믹스 분류', $menu_url[count($menu_url)-1].'class/', 'class')
);

	/* 3뎁스 */
	array_push($menu[count($menu_url)-1][1], array('전체', $menu[count($menu_url)-1][1][1].'index.php', 'list'));
	array_push($menu[count($menu_url)-1][1], array('단행본', $menu[count($menu_url)-1][1][1].'book.php', 'list'));
	array_push($menu[count($menu_url)-1][1], array('웹툰', $menu[count($menu_url)-1][1][1].'webtoon.php', 'list'));
	array_push($menu[count($menu_url)-1][1], array('단행본웹툰', $menu[count($menu_url)-1][1][1].'book_webtoon.php', 'list'));

	array_push($menu[count($menu_url)-1][2], array('제공사(CP)', $menu[count($menu_url)-1][2][1].'cp.php', 'partner'));
	array_push($menu[count($menu_url)-1][2], array('출판사', $menu[count($menu_url)-1][2][1].'publisher.php', 'partner'));
	array_push($menu[count($menu_url)-1][2], array('작가', $menu[count($menu_url)-1][2][1].'author.php', 'partner'));

	array_push($menu[count($menu_url)-1][3], array('장르', $menu[count($menu_url)-1][3][1].'small.php', 'class'));
	array_push($menu[count($menu_url)-1][3], array('키워드', $menu[count($menu_url)-1][3][1].'keyword.php', 'class'));
	array_push($menu[count($menu_url)-1][3], array('메뉴', $menu[count($menu_url)-1][3][1].'menu.php', 'class'));
	

/* 메뉴 */
$menu_url[] = NM_ADM_URL.'/comicsorder/'; /* 1 */
$menu[] = array (
	/* 1뎁스 */
	array('코믹스 정렬순서', $menu_url[count($menu_url)-1], 'comicsorder'), 
	/* 2뎁스 */
	array('코믹스 요일', $menu_url[count($menu_url)-1].'cmweek/', 'cmweek'),
	array('코믹스 장르', $menu_url[count($menu_url)-1].'ranking_small/', 'ranking_small'), 
	array('코믹스 신작', $menu_url[count($menu_url)-1].'cmnew/', 'cmnew'),
	array('코믹스 베스트', $menu_url[count($menu_url)-1].'cmtop/', 'cmtop'),
	array('코믹스 무료', $menu_url[count($menu_url)-1].'cmfree/', 'cmfree'),
	array('코믹스 업데이트작품(main)', $menu_url[count($menu_url)-1].'main_slide_moscroll/', 'main_slide_moscroll'),
	array('코믹스 대분류', $menu_url[count($menu_url)-1].'ranking/', 'ranking') 
);

	/* 3뎁스 */

	/*
	array_push($menu[count($menu_url)-1][2], array('실시간 순위',  $menu[count($menu_url)-1][2][1].'hour.php',   'ranking'));
	array_push($menu[count($menu_url)-1][2], array('일간 순위', $menu[count($menu_url)-1][2][1].'day.php', 'ranking'));
	array_push($menu[count($menu_url)-1][2], array('주간 순위', $menu[count($menu_url)-1][2][1].'week.php', 'ranking'));
	array_push($menu[count($menu_url)-1][2], array('완결 순위', $menu[count($menu_url)-1][2][1].'complete.php', 'ranking'));
	array_push($menu[count($menu_url)-1][2], array('검색창 추천문구', $menu[count($menu_url)-1][2][1].'search_text.php', 'ranking'));
	*/


	foreach($week_list as $week_list_key  => $week_list_val){
		
		$menu_title = $week_list_val.'';

		array_push($menu[count($menu_url)-1][1], array($menu_title, $menu[count($menu_url)-1][1][1].'cmweek_'.$week_list_key.'.php', 'cmweek'));
	}

	foreach($week_list as $week_list_key  => $week_list_val){
		
		$menu_title = $week_list_val.'-청소년';

		array_push($menu[count($menu_url)-1][1], array($menu_title, $menu[count($menu_url)-1][1][1].'cmweek_'.$week_list_key.'_t.php', 'cmweek'));
	}


	foreach($nm_config['cf_small']as $cf_small_key  => $cf_small_val){
		$menu_title = $cf_small_val;
		if($cf_small_key == 0){ $menu_title = '전체'; }
		$menu_title = $menu_title.' 순위';

		array_push($menu[count($menu_url)-1][2], array($menu_title, $menu[count($menu_url)-1][2][1].'small_'.$cf_small_key.'.php', 'ranking_small'));
	}
	

	foreach($nm_config['cf_small']as $cf_small_key  => $cf_small_val){
		$menu_title = $cf_small_val.'-청소년';
		if($cf_small_key == 0){ $menu_title = '전체-청소년'; }
		$menu_title = $menu_title.' 순위';

		array_push($menu[count($menu_url)-1][2], array($menu_title, $menu[count($menu_url)-1][2][1].'small_'.$cf_small_key.'_t.php', 'ranking_small'));
	}
	

	array_push($menu[count($menu_url)-1][3], array('신작', $menu[count($menu_url)-1][3][1].'cmnew_0.php', 'cmnew'));
	array_push($menu[count($menu_url)-1][3], array('신작-청소년', $menu[count($menu_url)-1][3][1].'cmnew_0_t.php', 'cmnew'));

	array_push($menu[count($menu_url)-1][4], array('베스트', $menu[count($menu_url)-1][4][1].'cmtop_0.php', 'cmtop'));
	array_push($menu[count($menu_url)-1][4], array('베스트-청소년', $menu[count($menu_url)-1][4][1].'cmtop_0_t.php', 'cmtop'));

	array_push($menu[count($menu_url)-1][5], array('무료', $menu[count($menu_url)-1][5][1].'cmfree_0.php', 'cmfree'));
	array_push($menu[count($menu_url)-1][5], array('무료-청소년', $menu[count($menu_url)-1][5][1].'cmfree_0_t.php', 'cmfree'));
	
	
	foreach($week_list as $week_list_key  => $week_list_val){
		
		$menu_title = $week_list_val.'';

		array_push($menu[count($menu_url)-1][6], array($menu_title, $menu[count($menu_url)-1][6][1].'cmweek_'.$week_list_key.'.php', 'main_slide_moscroll'));
	}

	foreach($week_list as $week_list_key  => $week_list_val){
		
		$menu_title = $week_list_val.'-청소년';

		array_push($menu[count($menu_url)-1][6], array($menu_title, $menu[count($menu_url)-1][6][1].'cmweek_'.$week_list_key.'_t.php', 'main_slide_moscroll'));
	}




	array_push($menu[count($menu_url)-1][7], array('전체 순위', $menu[count($menu_url)-1][7][1].'all.php', 'ranking'));
	array_push($menu[count($menu_url)-1][7], array('단행본 순위', $menu[count($menu_url)-1][7][1].'book.php', 'ranking'));
	array_push($menu[count($menu_url)-1][7], array('웹툰 순위', $menu[count($menu_url)-1][7][1].'webtoon.php', 'ranking'));
	array_push($menu[count($menu_url)-1][7], array('급상승 순위', $menu[count($menu_url)-1][7][1].'issue.php', 'ranking'));

	array_push($menu[count($menu_url)-1][7], array('전체-청소년 순위', $menu[count($menu_url)-1][7][1].'all_t.php', 'ranking'));
	array_push($menu[count($menu_url)-1][7], array('단행본-청소년 순위', $menu[count($menu_url)-1][7][1].'book_t.php', 'ranking'));
	array_push($menu[count($menu_url)-1][7], array('웹툰-청소년 순위', $menu[count($menu_url)-1][7][1].'webtoon_t.php', 'ranking'));
	array_push($menu[count($menu_url)-1][7], array('급상승-청소년 순위', $menu[count($menu_url)-1][7][1].'issue_t.php', 'ranking'));



$menu_url[] = NM_ADM_URL.'/members/'; /* 2 */
$menu[] = array (
	/* 1뎁스 */
	array('회원', $menu_url[count($menu_url)-1], 'members'),
	/* 2뎁스 */
	array('전체 회원', $menu_url[count($menu_url)-1].'everyone.php', 'members'),
	array('마케터', $menu_url[count($menu_url)-1].'market.php', 'members'),
	array('티켓소켓', $menu_url[count($menu_url)-1].'ticket.php', 'members'),
	array('최근접속 1년 지난 회원', $menu_url[count($menu_url)-1].'oneyear.php', 'members'),
	array('휴면 회원', $menu_url[count($menu_url)-1].'human.php', 'members'),
	// array('중복 회원', $menu_url[count($menu_url)-1].'overlap.php', 'members'),
	// array('탈퇴 회원', $menu_url[count($menu_url)-1].'leave.php', 'members'),
	// array('이용 중지 회원', $menu_url[count($menu_url)-1].'use_stop.php', 'members'),
    array('APK 이용 회원', $menu_url[count($menu_url)-1].'apk_down.php', 'members'),
    array('APK2.0 이용 회원', $menu_url[count($menu_url)-1].'apk_is_down.php', 'members')
	// array('회원 소장정보', $menu_url[count($menu_url)-1].'buycomics.php', 'members')
);

$menu_url[] = NM_ADM_URL.'/board/'; /* 3 */
$menu[] = array (
	/* 1뎁스 */
	array('게시판', $menu_url[count($menu_url)-1], 'board'),
	/* 2뎁스 */
	array('공지사항', $menu_url[count($menu_url)-1].'notice.php', 'board'),
	array('FAQ', $menu_url[count($menu_url)-1].'help.php', 'board'),
	array('1:1 문의', $menu_url[count($menu_url)-1].'ask.php', 'board')
	// array('연재 문의', $menu_url[count($menu_url)-1].'publish.php', 'board')
	// array('오류제보', $menu_url[count($menu_url)-1].'contents_error.php', 'board')
	/* --------------------------------------------------------------------------  */
	/* 
	array('설문조사', $menu_url[count($menu_url)-1].'survey.php', 'event'),
	array('댓글', $menu_url[count($menu_url)-1].'reply.php', 'event')
	// array('완료', $menu_url[count($menu_url)-1].'end.php', 'event')
	*/
);

$menu_url[] = NM_ADM_URL.'/event/'; /* 4 */
$menu[] = array (
	/* 1뎁스 */
	array('이벤트', $menu_url[count($menu_url)-1], 'attend'),
	/* 2뎁스 */
	array('출석', $menu_url[count($menu_url)-1].'attend/', 'event'),
	array('충전/지급', $menu_url[count($menu_url)-1].'recharge.php', 'event'),
	array('무료&코인할인', $menu_url[count($menu_url)-1].'free_dc.php', 'event'),
	array('쿠폰',  $menu_url[count($menu_url)-1].'coupon.php', 'event'),
	// array('네이밍 이벤트',  $menu_url[count($menu_url)-1].'naming.php', 'event')
	array('랜덤박스 이벤트',  $menu_url[count($menu_url)-1].'randombox/', 'randombox'),
	array('작품투표', $menu_url[count($menu_url)-1].'vote/', 'vote'),
	array('응원이벤트', $menu_url[count($menu_url)-1].'comments.php', 'event'),
	array('모의고사', $menu_url[count($menu_url)-1].'finalexam/', 'finalexam'),
	array('추석', $menu_url[count($menu_url)-1].'thanksgiving/', 'thanksgiving'),
	array('크리스마스', $menu_url[count($menu_url)-1].'christmas/', 'christmas')
	/* --------------------------------------------------------------------------  */
	/* 
	array('전체구매시 추가지급', $menu_url[count($menu_url)-1].'allbuy_point.php', 'event'),
	array('쿠폰 관리', $menu_url[count($menu_url)-1].'coupon.php', 'event')
	*/
);
	/* 3뎁스 */
	array_push($menu[count($menu_url)-1][1], array('캘린더', $menu[count($menu_url)-1][1][1].'attend.php', 'attend'));
	array_push($menu[count($menu_url)-1][1], array('통계', $menu[count($menu_url)-1][1][1].'attend_stats.php', 'attend'));

	array_push($menu[count($menu_url)-1][5], array('이벤트 설정', $menu[count($menu_url)-1][5][1].'randombox.php', 'randombox'));
	array_push($menu[count($menu_url)-1][5], array('할인권 설정', $menu[count($menu_url)-1][5][1].'randombox_couponment.php', 'randombox'));
	array_push($menu[count($menu_url)-1][5], array('할인권 보기', $menu[count($menu_url)-1][5][1].'randombox_coupon.php', 'randombox'));

	array_push($menu[count($menu_url)-1][6], array('투표구분', $menu[count($menu_url)-1][6][1].'vote_category.php', 'vote'));
	array_push($menu[count($menu_url)-1][6], array('투표작품 설정', $menu[count($menu_url)-1][6][1].'vote.php', 'vote'));
	array_push($menu[count($menu_url)-1][6], array('투표 회원', $menu[count($menu_url)-1][6][1].'vote_member.php', 'vote'));
	array_push($menu[count($menu_url)-1][6], array('투표 통계', $menu[count($menu_url)-1][6][1].'vote_stats.php', 'vote'));
	
	array_push($menu[count($menu_url)-1][8], array('시험설정', $menu[count($menu_url)-1][8][1].'finalexam_period.php', 'finalexam'));
	array_push($menu[count($menu_url)-1][8], array('시험지관리', $menu[count($menu_url)-1][8][1].'finalexam.php', 'finalexam'));
	array_push($menu[count($menu_url)-1][8], array('시험지결과-회원', $menu[count($menu_url)-1][8][1].'finalexam_member.php', 'finalexam'));
	
	array_push($menu[count($menu_url)-1][9], array('행운의 꿀송편', $menu[count($menu_url)-1][9][1].'coupon.php', 'thanksgiving'));
	array_push($menu[count($menu_url)-1][9], array('피넛타임 편성표', $menu[count($menu_url)-1][9][1].'time.php', 'thanksgiving'));
	array_push($menu[count($menu_url)-1][9], array('통sale 기획전', $menu[count($menu_url)-1][9][1].'sale.php', 'thanksgiving'));
	array_push($menu[count($menu_url)-1][9], array('신작 기대평', $menu[count($menu_url)-1][9][1].'reply.php', 'thanksgiving'));
	
	array_push($menu[count($menu_url)-1][10], array('선물상자 설정', $menu[count($menu_url)-1][10][1].'christmas.php', 'christmas'));
	array_push($menu[count($menu_url)-1][10], array('선물상자 당첨정보', $menu[count($menu_url)-1][10][1].'christmas_result.php', 'christmas'));
	// array_push($menu[count($menu_url)-1][10], array('선물상자 실경품 당첨자', $menu[count($menu_url)-1][10][1].'christmas_real_gift.php', 'christmas'));
	array_push($menu[count($menu_url)-1][10], array('통sale 기획전', $menu[count($menu_url)-1][10][1].'sale.php', 'christmas'));

$menu_url[] = NM_ADM_URL.'/epromotion/'; /* 5 */
$menu[] = array (
	/* 1뎁스 */
	array('프로모션', $menu_url[count($menu_url)-1], 'epromotion'),
	/* 2뎁스 */
	array('배너', $menu_url[count($menu_url)-1].'banner.php', 'epromotion'),
	array('팝업', $menu_url[count($menu_url)-1].'popup.php', 'epromotion'),
	array('이벤트', $menu_url[count($menu_url)-1].'event.php', 'epromotion'), 
	array('알림톡', $menu_url[count($menu_url)-1].'kakao.php', 'epromotion'),
	array('APP 푸시', $menu_url[count($menu_url)-1].'push.php', 'epromotion'),
	array('친구 초대', $menu_url[count($menu_url)-1].'invite/', 'invite')
);
	/* 3뎁스 */
	array_push($menu[count($menu_url)-1][6], array('가입 리스트', $menu[count($menu_url)-1][6][1].'invite_join.php', 'invite'));
	array_push($menu[count($menu_url)-1][6], array('SNS 공유 리스트', $menu[count($menu_url)-1][6][1].'invite_share_sns.php', 'invite'));
	array_push($menu[count($menu_url)-1][6], array('공유 링크', $menu[count($menu_url)-1][6][1].'invite_link.php', 'invite'));

$menu_url[] = NM_ADM_URL.'/marketing/'; /* 6 */
$menu[] = array (
	/* 1뎁스 */
	array('마케팅', $menu_url[count($menu_url)-1], 'marketing'),
	/* 2뎁스 */
	array('마케터', $menu_url[count($menu_url)-1].'marketer.php', 'marketing'),
	array('마케터링크', $menu_url[count($menu_url)-1].'marketerlink.php', 'marketing'),
	array('마케터링크통계', $menu_url[count($menu_url)-1].'marketerlink_stats.php', 'marketing'),
	array('마케터가입통계', $menu_url[count($menu_url)-1].'marketerjoin_stats.php', 'marketing'),
	array('마케터가입리스트', $menu_url[count($menu_url)-1].'marketerjoin.php', 'marketing'),
	array('애드티브API결과', $menu_url[count($menu_url)-1].'marketer_adpx.php', 'marketing'),
	array('티켓소켓', $menu_url[count($menu_url)-1].'ticketsocket/', 'ticketsocket')
);

	array_push($menu[count($menu_url)-1][7], array('캠페인', $menu[count($menu_url)-1][7][1].'ticketsocket.php', 'ticketsocket'));
	array_push($menu[count($menu_url)-1][7], array('공유회원', $menu[count($menu_url)-1][7][1].'ticketsocket_ics.php', 'ticketsocket'));
	array_push($menu[count($menu_url)-1][7], array('통계', $menu[count($menu_url)-1][7][1].'ticketsocket_ics_stats.php', 'ticketsocket'));
	array_push($menu[count($menu_url)-1][7], array('가입리스트', $menu[count($menu_url)-1][7][1].'ticketsocket_join.php', 'ticketsocket'));
	array_push($menu[count($menu_url)-1][7], array('가입통계', $menu[count($menu_url)-1][7][1].'ticketsocket_join_stats.php', 'ticketsocket'));




/*	협력사는 나중에 추가되면 그때서 사용할 예정입니다
	협력사 관련된 DB&소스 삭제 
	나중에 만든다면 마케팅 관련 소스&DB 참조해서 만들면 될것 같음
	2017-11-07

$menu_url[] = NM_ADM_URL.'/collaboration/'; // 6
$menu[] = array (
	// 1뎁스
	array('협력사', $menu_url[count($menu_url)-1], 'collaboration'),
	// 2뎁스
	array('협력사', $menu_url[count($menu_url)-1].'collective/', 'collective'),
	array('티켓소켓', $menu_url[count($menu_url)-1].'coll_ticketsocket/', 'coll_ticketsocket')
);
	
	// 3뎁스 
	array_push($menu[count($menu_url)-1][1], array('협력사', $menu[count($menu_url)-1][1][1].'collective.php', 'collective'));

	array_push($menu[count($menu_url)-1][2], array('ICS 캠페인',  $menu[count($menu_url)-1][2][1].'coll_ticketsocket.php',   'coll_ticketsocket'));
	array_push($menu[count($menu_url)-1][2], array('ICS 공유회원',  $menu[count($menu_url)-1][2][1].'coll_ticketsocket_ics.php',   'coll_ticketsocket'));
	array_push($menu[count($menu_url)-1][2], array('ICS 통계',  $menu[count($menu_url)-1][2][1].'coll_ticketsocket_ics_stats.php',   'coll_ticketsocket'));
*/

$menu_url[] = NM_ADM_URL.'/sales/'; /* 7 */
$menu[] = array (
	/* 1뎁스 */
	array('매출', $menu_url[count($menu_url)-1], 'sales'),
	/* 2뎁스 */
	array('충전 매출 통계', $menu_url[count($menu_url)-1].'recharge/', 'recharge'),
	array($nm_config['cf_cash_point_unit_ko'].' 사용 통계', $menu_url[count($menu_url)-1].'coin/', 'coin'),
	array('채널링', $menu_url[count($menu_url)-1].'channel/', 'channel'),
	array('회원 충전/결제 내역', $menu_url[count($menu_url)-1].'used/', 'used')
);

	/* 3뎁스 */
	// array_push($menu[count($menu_url)-1][1], array('전체 결제/매출', $menu[count($menu_url)-1][1][1].'all_buy.php', 'recharge'));
	array_push($menu[count($menu_url)-1][1], array('일별 결제', $menu[count($menu_url)-1][1][1].'day_buy.php', 'recharge'));
	array_push($menu[count($menu_url)-1][1], array('일별 결제 종류별', $menu[count($menu_url)-1][1][1].'day_payway.php', 'recharge'));
	array_push($menu[count($menu_url)-1][1], array('일별 결제 종류별(PAYCO)', $menu[count($menu_url)-1][1][1].'day_payway_payco.php', 'recharge'));
	array_push($menu[count($menu_url)-1][1], array('일별 결제 상품별', $menu[count($menu_url)-1][1][1].'day_product.php', 'recharge'));
	array_push($menu[count($menu_url)-1][1], array('일별 결제 성별&연령별', $menu[count($menu_url)-1][1][1].'day_sex_age.php', 'recharge'));
	array_push($menu[count($menu_url)-1][1], array('일별 결제 플랫폼별', $menu[count($menu_url)-1][1][1].'day_platform.php', 'recharge'));
	array_push($menu[count($menu_url)-1][1], array('결제 회원 리스트', $menu[count($menu_url)-1][1][1].'mb_buy_ranking.php', 'recharge'));
	array_push($menu[count($menu_url)-1][1], array('일&시간별 충전건수', $menu[count($menu_url)-1][1][1].'day_time_buy.php', 'recharge'));
	array_push($menu[count($menu_url)-1][1], array('일별 결제(POINTPARK)', $menu[count($menu_url)-1][1][1].'day_buy_pointpark.php', 'recharge'));

	array_push($menu[count($menu_url)-1][2], array('전체 사용',  $menu[count($menu_url)-1][2][1].'all.php',   'coin'));
	array_push($menu[count($menu_url)-1][2], array('일별 사용',  $menu[count($menu_url)-1][2][1].'day.php',   'coin'));
	array_push($menu[count($menu_url)-1][2], array('코믹스별 전체사용', $menu[count($menu_url)-1][2][1].'comics_all.php', 'coin'));
	array_push($menu[count($menu_url)-1][2], array('코믹스별 이달사용', $menu[count($menu_url)-1][2][1].'comics.php', 'coin'));
	// array_push($menu[count($menu_url)-1][2], array('코믹스 분류별 매출', $menu[count($menu_url)-1][2][1].'big.php', 'coin'));
	array_push($menu[count($menu_url)-1][2], array('코믹스별 성별&연령 사용건수', $menu[count($menu_url)-1][2][1].'comics_sex_age.php', 'coin'));
	array_push($menu[count($menu_url)-1][2], array('코믹스별 시간 사용건수', $menu[count($menu_url)-1][2][1].'comics_time.php', 'coin'));
	// array_push($menu[count($menu_url)-1][2], array('코믹스 키워드별 매출', $menu[count($menu_url)-1][2][1].'keyword.php', 'coin'));
	// array_push($menu[count($menu_url)-1][2], array('코믹스 메뉴별 매출', $menu[count($menu_url)-1][2][1].'menu.php', 'coin'));
	
	// array_push($menu[count($menu_url)-1][3], array('일별 정산', $menu[count($menu_url)-1][3][1].'day.php', 'calc'));
	// array_push($menu[count($menu_url)-1][3], array('코믹스별 정산', $menu[count($menu_url)-1][3][1].'comics.php', 'calc'));
	// array_push($menu[count($menu_url)-1][3], array('CP별 정산',  $menu[count($menu_url)-1][3][1].'cp.php',   'calc'));
	
	array_push($menu[count($menu_url)-1][3], array('종합 결제 정보', $menu[count($menu_url)-1][3][1].'synthesis.php', 'channel'));
	array_push($menu[count($menu_url)-1][3], array('KCP', $menu[count($menu_url)-1][3][1].'kcp.php', 'channel'));
	// array_push($menu[count($menu_url)-1][3], array('올더게이트-가상계좌입금', $menu[count($menu_url)-1][3][1].'allthegate_virtual.php', 'channel'));
	array_push($menu[count($menu_url)-1][3], array('토스', $menu[count($menu_url)-1][3][1].'toss.php', 'channel'));
	array_push($menu[count($menu_url)-1][3], array('PAYCO', $menu[count($menu_url)-1][3][1].'payco.php', 'channel'));
	array_push($menu[count($menu_url)-1][3], array('포인트 파크', $menu[count($menu_url)-1][3][1].'pointpark.php', 'channel'));
	
	array_push($menu[count($menu_url)-1][4], array('회원 충전/결제 내역', $menu[count($menu_url)-1][4][1].'payment.php', 'channel'));


$menu_url[] = NM_ADM_URL.'/salescalc/'; /* 8 */
$menu[] = array (
	/* 1뎁스 */
	array('정산', $menu_url[count($menu_url)-1], 'salescalc'),
	/* 2뎁스 */
	array('전체 정산', $menu_url[count($menu_url)-1].'all.php', 'salescalc'),
	array('일별 정산', $menu_url[count($menu_url)-1].'day.php', 'salescalc'),
	array('코믹스별 전체정산', $menu_url[count($menu_url)-1].'comics_all.php', 'salescalc'),
	array('코믹스별 기간정산', $menu_url[count($menu_url)-1].'comics.php', 'salescalc') 
	// array('코믹스별 RS', $menu_url[count($menu_url)-1].'percentage.php', 'salescalc')
	// array('CP별 정산', $menu_url[count($menu_url)-1].'cp.php', 'salescalc')
);

	/* 3뎁스 */
	
	/*
	array_push($menu[count($menu_url)-1][1], array('메인화면', $menu[count($menu_url)-1][1][1].'hour.php', 'stats_ui'));
	array_push($menu[count($menu_url)-1][1], array('코믹스리스트', $menu[count($menu_url)-1][1][1].'day.php', 'stats_ui'));
	array_push($menu[count($menu_url)-1][1], array('화리스트', $menu[count($menu_url)-1][1][1].'week.php', 'stats_ui'));
	array_push($menu[count($menu_url)-1][1], array('게시판', $menu[count($menu_url)-1][1][1].'isuee.php', 'stats_ui'));
	array_push($menu[count($menu_url)-1][1], array('이벤트', $menu[count($menu_url)-1][1][1].'event.php', 'stats_ui'));
	array_push($menu[count($menu_url)-1][1], array('배너', $menu[count($menu_url)-1][1][1].'banner.php', 'stats_ui'));

	array_push($menu[count($menu_url)-1][2], array('회원로그인', $menu[count($menu_url)-1][2][1].'member.php', 'stats_log'));
	array_push($menu[count($menu_url)-1][2], array('IP별',  $menu[count($menu_url)-1][2][1].'ip.php',   'stats_log'));
	array_push($menu[count($menu_url)-1][2], array('도메인별',  $menu[count($menu_url)-1][2][1].'domain.php',   'stats_log'));
	array_push($menu[count($menu_url)-1][2], array('브라우저별',  $menu[count($menu_url)-1][2][1].'browser.php',   'stats_log'));
	array_push($menu[count($menu_url)-1][2], array('운영체제별',  $menu[count($menu_url)-1][2][1].'os.php',   'stats_log'));
	array_push($menu[count($menu_url)-1][2], array('날짜별',  $menu[count($menu_url)-1][2][1].'time.php',   'stats_log'));
	*/


$menu_url[] = NM_ADM_URL.'/cmsconfig/'; /* 10 */
$menu[] = array (
	/* 1뎁스 */
	array('관리', $menu_url[count($menu_url)-1], 'cmsconfig'),
	/* 2뎁스 */
	array('기본&환경 설정', $menu_url[count($menu_url)-1].'env/', 'env'),
	array('운영 설정', $menu_url[count($menu_url)-1].'master/', 'master'),
	array('개발 설정', $menu_url[count($menu_url)-1].'dev/', 'dev')
);
	/* 3뎁스 */
	array_push($menu[count($menu_url)-1][1], array('기본 설정', $menu[count($menu_url)-1][1][1].'basic.php', 'env'));
	array_push($menu[count($menu_url)-1][1], array('회원 분류·등급별 혜택', $menu[count($menu_url)-1][1][1].'member_buff.php', 'env'));
	array_push($menu[count($menu_url)-1][1], array('충전 금액', $menu[count($menu_url)-1][1][1].'recharge.php', 'env'));
	array_push($menu[count($menu_url)-1][1], array('전체구매시 추가지급', $menu[count($menu_url)-1][1][1].'sin_allpay.php', 'env'));
	array_push($menu[count($menu_url)-1][1], array('가입, 탈퇴 등 텍스트 설정 ', $menu[count($menu_url)-1][1][1].'text.php', 'env'));
	array_push($menu[count($menu_url)-1][1], array('기본 이미지 설정', $menu[count($menu_url)-1][1][1].'img.php', 'env'));
	array_push($menu[count($menu_url)-1][1], array('팝업 이미지 설정', $menu[count($menu_url)-1][1][1].'imgadd.php', 'env'));

	array_push($menu[count($menu_url)-1][2], array('정산 금액 설정', $menu[count($menu_url)-1][2][1].'criterion.php', 'master'));
	array_push($menu[count($menu_url)-1][2], array('회원 등급 리스트', $menu[count($menu_url)-1][2][1].'member_level_list.php', 'master'));
	// array_push($menu[count($menu_url)-1][2], array('CMS로그', $menu[count($menu_url)-1][2][1].'log.php', 'master'));
	// array_push($menu[count($menu_url)-1][2], array('기타등등(나중에 생각나면…)', $menu[count($menu_url)-1][2][1].'etc.php', 'master'));
	
	// array_push($menu[count($menu_url)-1][3], array('이벤트풀',  $menu[count($menu_url)-1][3][1].'eventful.php',   'dev'));
	array_push($menu[count($menu_url)-1][3], array('티켓소켓',  $menu[count($menu_url)-1][3][1].'ticketsocket.php',   'dev'));
	array_push($menu[count($menu_url)-1][3], array('친구초대',  $menu[count($menu_url)-1][3][1].'promoful.php',   'dev'));
	array_push($menu[count($menu_url)-1][3], array('알림톡',  $menu[count($menu_url)-1][3][1].'kakao.php',   'dev'));
	
	/*
	// 통계 넣으려고 한것들...
	array_push($menu[count($menu_url)-1][1], array('전체', $menu[count($menu_url)-1][1][1].'comics_ranking.php', 'cm_ranking'));
	array_push($menu[count($menu_url)-1][1], array('웹툰별', $menu[count($menu_url)-1][1][1].'comics_webtoon.php', 'cm_ranking'));
	array_push($menu[count($menu_url)-1][1], array('단행본별', $menu[count($menu_url)-1][1][1].'comics_book.php', 'cm_ranking'));
	// array_push($menu[count($menu_url)-1][1], array('비정기별', $menu[count($menu_url)-1][1][1].'cm_ranking.php', 'cm_ranking'));
	array_push($menu[count($menu_url)-1][1], array('급상승코믹스별', $menu[count($menu_url)-1][1][1].'comics_isuee.php', 'cm_ranking'));
	// array_push($menu[count($menu_url)-1][1], array('유형별', $menu[count($menu_url)-1][1][1].'type.php', 'cm_ranking'));
	// array_push($menu[count($menu_url)-1][1], array('키워드별', $menu[count($menu_url)-1][1][1].'keyword.php', 'cm_ranking'));
	*/


$menu_url[] = NM_ADM_URL.'/nexcube/'; /* 11 */
$menu[] = array (
	/* 1뎁스 */
	array('CMS부서별', $menu_url[count($menu_url)-1], 'nexcube'),
	/* 2뎁스 */
	array('콘텐츠사업팀', $menu_url[count($menu_url)-1].'team1/', 'team1'), 
	array('플랫폼사업팀', $menu_url[count($menu_url)-1].'team2/', 'team2')
	// array('컨텐츠제작팀', $menu_url[count($menu_url)-1].'team3/', 'team3')
);
	/* 3뎁스 */
	array_push($menu[count($menu_url)-1][1], array('서지 정보', $menu[count($menu_url)-1][1][1].'bibliography.php', 'team1'));
	array_push($menu[count($menu_url)-1][1], array('ECN', $menu[count($menu_url)-1][1][1].'ecn.php', 'team1'));
	array_push($menu[count($menu_url)-1][1], array('ECN 코인 설정', $menu[count($menu_url)-1][1][1].'ecn_coin.php', 'team1'));
	
	array_push($menu[count($menu_url)-1][2], array('연재 문의', $menu[count($menu_url)-1][2][1].'publish.php', 'team2'));
	array_push($menu[count($menu_url)-1][2], array('회원 가입/결제 통계', $menu[count($menu_url)-1][2][1].'member_stats.php', 'team2'));
	array_push($menu[count($menu_url)-1][2], array('APK 다운/결제 통계', $menu[count($menu_url)-1][2][1].'apk_stats.php', 'team2'));
	array_push($menu[count($menu_url)-1][2], array('회원 요약', $menu[count($menu_url)-1][2][1].'member_summery.php', 'team2'));
	
	// array_push($menu[count($menu_url)-1][3], array('임시', $menu[count($menu_url)-1][3][1].'index.php', 'team3'));

$menu_url[] = NM_ADM_URL.'/support/'; /* 12 */
$menu[] = array (
	/* 1뎁스 */
	array('CMS지원', $menu_url[count($menu_url)-1], 'support'),
	/* 2뎁스 */
	array('마케터', $menu_url[count($menu_url)-1].'marketer_cms/', 'marketer_cms'),
	array('제공사(CP)', $menu_url[count($menu_url)-1].'provider_cms/', 'provider_cms'),
	array('작가', $menu_url[count($menu_url)-1].'professional_cms/', 'professional_cms')
	// array('티켓소켓', $menu_url[count($menu_url)-1].'ticketsocket_cms/', 'ticketsocket_cms')
);
	/* 3뎁스 */	
	// array_push($menu[count($menu_url)-1][1], array('일별 결제', $menu[count($menu_url)-1][1][1].'day_buy.php', 'marketer_cms'));	
	// array_push($menu[count($menu_url)-1][1], array('코믹스별 결제', $menu[count($menu_url)-1][1][1].'comics.php', 'marketer_cms'));
	array_push($menu[count($menu_url)-1][1], array('마케터 회원정보', $menu[count($menu_url)-1][1][1].'market.php', 'marketer_cms'));
	
	array_push($menu[count($menu_url)-1][2], array('일별 결제', $menu[count($menu_url)-1][2][1].'day.php', 'provider_cms'));	
	array_push($menu[count($menu_url)-1][2], array('코믹스별 결제', $menu[count($menu_url)-1][2][1].'comics.php', 'provider_cms'));
	array_push($menu[count($menu_url)-1][2], array('전체 결제', $menu[count($menu_url)-1][2][1].'all.php', 'provider_cms'));	
	array_push($menu[count($menu_url)-1][2], array('전체 코믹스별 결제', $menu[count($menu_url)-1][2][1].'comics_all.php', 'provider_cms'));
	
	array_push($menu[count($menu_url)-1][3], array('일별 결제', $menu[count($menu_url)-1][3][1].'day.php', 'professional_cms'));	
	array_push($menu[count($menu_url)-1][3], array('코믹스별 결제', $menu[count($menu_url)-1][3][1].'comics.php', 'professional_cms'));
	array_push($menu[count($menu_url)-1][3], array('전체 결제', $menu[count($menu_url)-1][3][1].'all.php', 'professional_cms'));	
	array_push($menu[count($menu_url)-1][3], array('전체 코믹스별 결제', $menu[count($menu_url)-1][3][1].'comics_all.php', 'professional_cms'));
	
	// array_push($menu[count($menu_url)-1][4], array('ICS 통계', $menu[count($menu_url)-1][4][1].'day.php', 'ics_cms'));
	// array_push($menu[count($menu_url)-1][4], array('ICS 공유통계', $menu[count($menu_url)-1][4][1].'day.php', 'ics_share_cms'));
	// array_push($menu[count($menu_url)-1][4], array('ICS 리워드', $menu[count($menu_url)-1][4][1].'day.php', 'ics_reword_cms'));

?>