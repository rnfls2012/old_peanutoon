<?
include_once '_common.php'; // 공통

error_page();

/* DB */
$sql_eventview = "select * from epromotion_event where eme_no=".$_event;
$row_eventview = sql_fetch($sql_eventview);

/* 없는 이벤트 일시 171212 추가 */
if($row_eventview['eme_no'] == "") {
	cs_alert("종료되거나, 없는 이벤트 입니다!", NM_URL."/event.php");
	die;
}

// SNS 썸네일
if(strpos($row_eventview['eme_text'], "img src")) {
	$img_src_start = strpos($row_eventview['eme_text'], "img src") + 9;
	$img_src_end = strpos(substr($row_eventview['eme_text'], $img_src_start), '"');
	$head_thumbnail = substr($row_eventview['eme_text'], $img_src_start, $img_src_end);
} // end if

/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */
/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/eventview.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>