<?
if (!defined('_NMPAGE_')) exit;
// kt storage server 에서 objects 리스트 가져오기
function kt_storage_list($file_path)
{
    global $container;

	$list_objects = $container->list_objects(0,NULL,NULL,$file_path);

	return $list_objects;
}

// kt storage server 업로드
function kt_storage_upload($tmp_upload, $path, $filename)
{
    global $container;

	if(kt_server_real_check() == false){return false;} // 실서버인지 확인

	$upload_result = true;
	
	$upload_info = getimagesize($tmp_upload);
	
	if(!preg_match("/\.(gif|jpg|bmp|png|ico|zip)$/i", $filename)){ 
		$upload_result = false; 
	}else{
		$content_type = $upload_info['mime'];
		$container->create_paths($path);
		$object = $container->create_object($path.$filename);
		$object->content_type = $content_type;
		$object->load_from_filename($tmp_upload);
	}
	
	return $upload_result;
}

// kt storage server 삭제
function kt_storage_delete($object)
{
	if(kt_server_real_check() == false){return false;} // 실서버인지 확인

	if($object == ''){return false;}
    global $container;
	
	// 파일존재 확인
	$object_confirm = true;
	try{ $object_check = $container->get_object($object); }
	catch(Exception $e) { /* echo $e->getMessage(); */ $object_confirm = false; }

	// 파일이 존재 한다면 삭제
	if($object_confirm == true){
		$delete_result = $container->delete_object($object); // boolean True if successfully removed
	}else{		
		$delete_result = false;
	}

	return $delete_result;
}

// kt storage server zip 업로드
function kt_storage_zip_upload($tmp_uploaded, $path, $filename)
{
	if(kt_server_real_check() == false){return false;} // 실서버인지 확인

	// 삭제 먼저 가기
	kt_storage_zip_delete($path); // kt storage
	
    global $container;

	mkdirAll($path);

	// kt server
	$move_uploaded = NM_PATH.'/'.$path.'/'.$filename;
	if (move_uploaded_file($tmp_uploaded, $move_uploaded)) {
		$zipfile = new PclZip($move_uploaded);
		$extract = $zipfile -> extract(PCLZIP_OPT_PATH, NM_PATH.'/'.$path);
		@unlink($tmp_uploaded);
		@unlink($move_uploaded);
	}

	foreach($extract as $extract_key => $extract_val){
		if(!preg_match("/\.(gif|jpg|bmp|png)$/i", $extract_val['stored_filename'])){
			if (is_file($extract_val['filename'])) {
				@unlink($extract_val['filename']); // 해당 파일 삭제 
				unSET($extract[$extract_key]);  // 해당 배열 삭제 
			}
		}
	}
	// print_r($extract);
	$zipfile_count = count($extract); /* 파일 갯수 */
	
	// kt storage
	$container->create_paths($path);
	foreach($extract as $extract_key => $extract_val){
		$upload_info = getimagesize($extract_val['filename']);
		$content_type = $upload_info['mime'];

		$container->create_paths($path);
		$object = $container->create_object($path.'/'.$extract_val['stored_filename']);
		$object->content_type = $content_type;
		$object->load_from_filename($extract_val['filename']);
	}
	rmdirAll(NM_PATH.'/'.$path,1);

	return $zipfile_count;
}

// kt storage server zip 삭제
function kt_storage_zip_delete($path)
{
	if(kt_server_real_check() == false){return false;} // 실서버인지 확인

	if($path == ''){return false;}

	$object_list = kt_storage_list($path);
	foreach($object_list as $object_val){
		kt_storage_delete($object_val);
	}
}


function kt_purge($purgeurl)
{
	### KEY ###
	$api_key="cKDnmSXtXaVpFLX79TiwhTpocdSTV99lri8dMaNphRYjr4lMfu5lNkQFXbaKU1UDyjt0cgBCs94OQMzxpCEigg";
	$secret_key="qP9Bzy5DeUNSWURoZGy-diupqrx9XkwEera3q4uUjupCp1FQlwfU0eJ5lMO7FoyZqF0qm1qXdaHoDasbbOL64w";
	$url = "https://api.ucloudbiz.olleh.com/cdn/v1/client/api?";

	### Command ###
	$svcname="zd5565";
	$svctype="cache";
	$response="xml";
	$command="purgeCmd";
	// $purgeurl="http://un01-zd5565.ktics.co.kr/data/1/3963/cover.jpg?dtp=20170228182339&cmd=R_192x277";
	$signature="";
	$uri = "";
	$uri2 = "";
	$sig_list = "";

	$list = array(
			"apikey"=>$api_key,
			"svcname"=>$svcname,
			"svctype"=>$svctype,
			"response"=>$response,
			"command"=>$command,
			"purgeurl"=>$purgeurl,
	);

	### url encoding ###
	foreach($list as $key=>$value){
			$list[$key] = str_replace('+', "%20", urlencode($value));
	}

	### 소문자 변환 및 소팅 ###
	foreach($list as $key=>$value){
			$uri=$uri.$key."=".$value."&";
			$sig_list = array_map('strtolower',$list);
			ksort($sig_list);
	}

	### request 생성 ###
	foreach($sig_list as $key=>$value){
			$uri2=$uri2.$key."=".$value."&";
	}

	### 마지막 '&' 제거 ###
	$uri = substr($uri,0,-1);
	$uri2 = substr($uri2,0,-1);

	$signature = urlencode(base64_encode(hash_hmac('sha1',$uri2, $secret_key,true)));

	$request_url = $request = $url.$uri."&"."signature=".$signature;

	if (ini_get('allow_url_fopen') == '1') {
		// fopen() 이나 file_get_contents() 사용
		$response = file_get_contents($request_url);
	} else {
		// curl 이나 함수 직접 작성
		if (function_exists('curl_init')) { // crul 직접 사용
		   // curl 리소스를 초기화
		   $ch = curl_init(); 

		   // url을 설정
		   curl_setopt($ch, CURLOPT_URL, $request_url); 

		   // 헤더는 제외하고 content 만 받음
		   curl_setopt($ch, CURLOPT_HEADER, 0); 

		   // 응답 값을 브라우저에 표시하지 말고 값을 리턴
		   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

		   // 브라우저처럼 보이기 위해 user agent 사용
		   curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041107 Firefox/1.0'); 

		   $response = curl_exec($ch);

		   $curl_errno = curl_errno($ch);

		   $curl_error = curl_error($ch);

		   // 리소스 해제를 위해 세션 연결 닫음
		   curl_close($ch);
		}else{
			// curl 라이브러리가 설치 되지 않음. 다른 방법 알아볼 것
			function getRemoteFile($url){ // 함수 사용
			   // 1. host name과 url path 값을 획득

			   // 2. 원격 서버에 접속

			   // 3. 파일을 얻기위해 필요한 헤더들을 전송

			   // 4. 원격 서버로부터 응답 받음

			   // 5. header 부분 걷어냄

			   // 6. 파일 content 리턴
			}
		}
	}
	//xml로 되어 있는 데이터 가져오기
	$object = simplexml_load_string($response);
	
	// print_r($object);
	// die;

	if ($object) {
		$products = $object->info;
		foreach($products->info as $value) {
			$data[code] = $value->code;
			$data[msg] = $value->msg;
			array_push($books, $data);
		}
	}else{
		$state = 1;
		$msg = 'error';
	}
}


function kt_server_real_check()
{
	$kt_server_real_check = true;

	if(NM_REAL_URL != "https://".$_SERVER['HTTP_HOST']){
		$kt_server_real_check = false;
	}
	return $kt_server_real_check;
}

function kt_cdn_purge($data='')
{ 
// 중단;;; purge api 가 제대로 작동 안하는 것 같음 - 이미지가 이전 이미지와 그전 이미지와 현재 이미지 등 이미지들이 왔다 갔다 함

/* 181030 
Syntax POST /v1/management/service/<svc_name>/volume/<vol_name>/purge?async=1 HTTP/1.1
- svc_name: 서비스 명
- vol_name : 볼륨 명
*/
	$svc_name='peanutoon';
	$vol_name='img';	
	$cdn_api_url = NM_CDN.'v1/management/service/'.$svc_name.'/volume/'.$vol_name.'/purge?async=1';

	// if(NM_HTTP_SERVER == 'https://' && $file !=''){
		$save_data = $get_data = array();
		if(is_array($data)){
			foreach($data as $data_val){
				array_push($get_data, '/'.$data_val);
			}
			$save_data = $get_data;
		}else{
			array_push($save_data, '/'.$data);
		}
		$ch_data = array();
		$ch_data["filelist"] = $save_data;
		$ch_data["comments"] = 'filemod';
		$jsonBody = json_encode($ch_data);
		$ch = curl_init($cdn_api_url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		// curl_setopt($kt_cdn_purge, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'X-Auth-Token: 132f2e63-0796-4cbb-b272-a735a2b5e310')
		);

		$result = curl_exec($ch);
		curl_close($ch);
	// }
}

?>