<?
if (!defined('_NMPAGE_')) exit;

function pf_row($emf_no) {
	$sql_emf = "SELECT * FROM epromotion_full WHERE emf_no=".$emf_no." ";
	$row_emf = sql_fetch($sql_emf);
	return $row_emf;
}

function pf_invite_join($mb) {
	$invite_bool = false;

	$invite_code = get_session('ss_invite_code');
	
	if($invite_code == "" || $invite_code == NULL){
			
	} else {
		// 초대한 회원 정보
		$invite_mb_sql = "SELECT * FROM member WHERE mb_invite_code = '".$invite_code."'";
		$invite_mb_row = sql_fetch($invite_mb_sql);
		
		if($invite_mb_row['mb_no'] == "") {

		} else {
			// 회원 정보 업데이트
			$update_sql = "UPDATE member SET mb_invite = '1' WHERE mb_no = '".$mb['mb_no']."' AND mb_idx = '".$mb['mb_idx']."'";
			sql_query($update_sql);

			// 로그 및 통계
			$insert_stats_invite_sql = "
				INSERT INTO invite_join (ij_to_member, ij_to_member_idx, ij_be_member, ij_be_member_idx, ij_be_member_sns_type, ij_invite_code, ij_date) 
				VALUES ('".$invite_mb_row['mb_no']."', '".$invite_mb_row['mb_idx']."', '".$mb['mb_no']."', '".$mb['mb_idx']."', '".$mb['mb_sns_type']."', '".$invite_code."', '".NM_TIME_YMDHIS."')"; // 로그
			sql_query($insert_stats_invite_sql);
			st_stats_invite_join(); // 통계 함수

			pf_invite_join_type($mb); // 가입 타입에 따라서 지급 방식 다르게

    		// 세션변수값 삭제
			del_session('ss_invite_code');

			$invite_bool = true;
		} // end else
	} // end else

	return $invite_bool;
}

function pf_recharge_reward($be_row) {
	/* 초대 로그 정보 가져오기 */
	$invite_sql = "SELECT * FROM invite_join WHERE ij_be_member = '".$be_row['mb_no']."' AND ij_be_member_idx = '".$be_row['mb_idx']."'";
	$invite_row = sql_fetch($invite_sql);

	if($invite_row['ij_recharge'] == '0') {
		/* 초대한 회원 정보 */
		$invite_mb_sql = "SELECT * FROM member WHERE mb_no = '".$invite_row['ij_to_member']."' AND mb_idx = '".$invite_row['ij_to_member_idx']."'";
		$invite_mb_row = sql_fetch($invite_mb_sql);

		/* 리워드 지급 */
		$reward_msg = "친구 초대 가입 이벤트 [".$be_row['mb_id']."님 첫 결제]";
		if(cs_set_member_point_income_event($invite_mb_row, "0", "100", "recharge_invite", $reward_msg, "y")) {
			$be_update_sql = "UPDATE invite_join SET ij_recharge = '1' WHERE ij_be_member = '".$be_row['mb_no']."' AND ij_be_member_idx = '".$be_row['mb_idx']."'";
			sql_query($be_update_sql);
		} // end if
	} // end if
}

function pf_invite_click_update($invite_code) {
	// 초대한 회원 정보
	$invite_mb_sql = "SELECT * FROM member WHERE mb_invite_code = '".$invite_code."'";
	$invite_mb_row = sql_fetch($invite_mb_sql);

	$invite_click_sql = "SELECT * FROM invite_click WHERE ic_member = '".$invite_mb_row['mb_no']."' AND ic_member_idx = '".$invite_mb_row['mb_idx']."'";
	$invite_click_row = sql_fetch($invite_click_sql);

	if($invite_click_row['ic_no'] == "") {
		$invite_click_cnt = "INSERT INTO invite_click(ic_member, ic_member_idx, ic_member_invite_code, ic_first_date, ic_last_date) VALUES('".$invite_mb_row['mb_no']."', '".$invite_mb_row['mb_idx']."', '".$invite_mb_row['mb_invite_code']."', '".NM_TIME_YMDHIS."', '".NM_TIME_YMDHIS."')";
	} else {
		$invite_click_cnt = "UPDATE invite_click SET ic_click = (ic_click + 1), ic_last_date = '".NM_TIME_YMDHIS."' WHERE ic_member = '".$invite_mb_row['mb_no']."' AND ic_member_idx = '".$invite_mb_row['mb_idx']."'";
	} // end else

	sql_query($invite_click_cnt);
}

function pf_invite_reward($mb) {
	$invite_reward_bool = false;

	/* 초대 로그 정보 가져오기 */
	$invite_sql = "SELECT * FROM invite_join WHERE ij_be_member = '".$mb['mb_no']."' AND ij_be_member_idx = '".$mb['mb_idx']."'";
	$invite_row = sql_fetch($invite_sql);

	// 초대한 회원 정보
	$invite_mb_row = mb_get_no($invite_row['ij_to_member']);
		
	if($invite_mb_row['mb_no'] == "") {
		
	} else {
		// 금액 조정 171229
		$invite_point = "100";
		if(NM_TIME_YMD >= '2018-01-01') {
			$invite_point = "50";
		} // end if
		
		// 가입 리워드 지급
		$invite_msg = "친구 초대 가입 이벤트 [".$mb['mb_id']."님 가입]";
		if(cs_set_member_point_income_event($invite_mb_row, "0", $invite_point, "to_invite", $invite_msg, "y")) {
			$invite_msg = "친구 초대 가입 이벤트 [".$invite_mb_row['mb_id']."님 초대]";
			if(cs_set_member_point_income_event($mb, "0", $invite_point, "be_invite", $invite_msg, "y")) {
				$invite_reward_update = "UPDATE invite_join SET ij_invite_reward = '1' WHERE ij_be_member = '".$mb['mb_no']."' AND ij_be_member_idx = '".$mb['mb_idx']."'";
				sql_query($invite_reward_update);
			} // end if
		} // end if

		$invite_reward_bool = true;
	} // end else

	return $invite_reward_bool;
}

function pf_invite_join_type($mb) {
	include_once(NM_LIB_PATH.'/PHPMailer/class.phpmailer.php'); //메일
	include_once(NM_LIB_PATH.'/PHPMailer/class.smtp.php');

	global $nm_config;

	if($mb['mb_sns_type'] != NULL || $mb['mb_sns_type'] != "") {
		pf_invite_reward($mb);
	} else {
		$email_to = $mb['mb_email']; 
		$email_from = $nm_config['cf_admin_email'];
		$email_subject = "[".$nm_config['cf_title']."] 친구초대 이벤트 이메일 확인";
		$content = "아래의 글자를 눌러 내정보 페이지로 이동하시면 미니땅콩이 지급됩니다.<br/>";
		$content .= "※내정보 페이지로 이동을 완료하셔야 미니땅콩이 지급되오니 유의해주시기 바랍니다.<br/>";
		$content .= "<br/>";
		$content .= "<br/>";
		$content .= "<a href='".NM_PROC_URL."/invite_email_reward.php?member=".$mb['mb_no']."'>미니땅콩 지급 받기</a>";
		mailer('', $email_from, '', $email_to, $email_subject, $content, 0);
	} // end else
}
?>