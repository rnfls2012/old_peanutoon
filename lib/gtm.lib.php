<? if (!defined('_NMPAGE_')) exit;

function gtm_head_code(){
	if (!NM_GTM) return;

	echo "
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','".NM_GTM_GODE."');</script>
	<!-- End Google Tag Manager -->
	";
}

function gtm_body_code(){
	if (!NM_GTM) return;

	echo '
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id='.NM_GTM_GODE.'"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	';
}

function gtm_utm($utm_source, $utm_medium, $utm_campaign){
	if (!NM_GTM) return;

	$gtm_utm = 'utm_source='.$utm_source.'&utm_medium='.$utm_medium.'&utm_campaign='.$utm_campaign;
	return $gtm_utm;
}

function gtm_utm_url($data){
	if (!NM_GTM) return;

	$utm_url = '';
	if(is_array($data)){	
		$utm_arr = array('mktl_link_utm_source', 'mktl_link_utm_medium', 'mktl_link_utm_campaign', 'mktl_link_utm_term', 'mktl_link_utm_content');
		foreach($utm_arr as $utm_key => $utm_val){
			if($data[$utm_val] != ''){
				$utm_para = str_replace("mktl_link_", "", $utm_val);
				$utm_para_mark = '&';
				if($utm_key == 0){ $utm_para_mark = ''; }
				$utm_url.=$utm_para_mark.$utm_para.'='.$data[$utm_val];
			}
		}
	}
	return $utm_url;
}

function gtm_amutus_user_id($nm_member){
	if (!NM_GTM_AMUTUS) return;

	echo '
		<!-- amutus_adclr_variable -->
		<script type="text/javascript">
		<!--
			var user_idx = "'.$nm_member['mb_no'].'";
			$.cookie("user_idx", user_idx);
		//-->
		</script>
		<!-- amutus_adclr_variable -->
	';
}

function gtm_amutus($nm_member=''){
	if (!NM_GTM_AMUTUS) return;

	echo '
	<!-- amutus_adclr -->
		<script type="text/javascript">
		void 0===window.adcstq&&function(t,c){t.adcstq=function(){t.adcstq.Q=t.adcstq.Q||[],t.adcstq.Q.push(arguments)};		var s=c.getElementsByTagName("script")[0],e=c.createElement("script");e.async=!0,e.src="//t.adclr.jp/st/adcst.js",s.parentNode.insertBefore(e,s)}(window,document);window.adcstq({site: 691});
		</script>
	<!-- amutus_adclr -->
	';
}

function gtm_amutus_entry($nm_member, $id_bool = false){
	if (!NM_GTM_AMUTUS) return;

	if($id_bool == false){
		gtm_amutus_user_id($nm_member);
	}
	echo '
	<!-- amutus_adclr_entry -->
	<script type="text/javascript" src="https://adclr.jp/sa?site_id=691&u='.$nm_member["mb_no"].'&c=entry"></script>
	<!-- amutus_adclr_entry -->
	';
}

function gtm_amutus_charge($nm_member, $id_bool = false){
	if (!NM_GTM_AMUTUS) return;

	if($id_bool == false){
		gtm_amutus_user_id($nm_member);
	}
	echo '
	<!-- amutus_adclr_charge -->
	<script type="text/javascript" src="https://adclr.jp/sa?site_id=691&u='.$nm_member["mb_no"].'&c=charge"></script>
	<!-- amutus_adclr_charge -->
	';
}

function gtm_amutus_utm($utm_source, $utm_medium, $utm_campaign){
	if (!NM_GTM_AMUTUS) return;

	$gtm_utm = 'utm_source='.$utm_source.'&utm_medium='.$utm_medium.'&utm_campaign='.$utm_campaign;
	return $gtm_utm;
}

// 18-12-03
function gtm_amutus_adult($nm_member){
	if (!NM_GTM_AMUTUS) return;

	if($nm_member['mb_adult'] == 'y'){ // 성인회원일 경우

	}else if($nm_member['mb_adult'] == 'n'){ // 회원인데 미성년자일 경우
		if($nm_member['mb_sex'] == 'n'){
			goto_url(NM_URL."/ctcertify.php?".set_add_para_redirect());
			die;
		}else{		
			alert('19세 이하는 보실 수 없습니다.', NM_URL);
			die;
		}
	}else{// 비회원일 경우
		goto_url(NM_URL."/ctlogin.php?".set_add_para_redirect());
		die;
	}
}