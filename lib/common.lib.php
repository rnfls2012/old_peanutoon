<?
if (!defined('_NMPAGE_')) exit;

function error_page(){
	if(NM_DOMAIN_ERROR != ''){
		if(is_nexcube()){
			if(REMOTE_ADDR == '119.42.175.158'){			// 개발자 IP
				// include_once (NM_DOMAIN_ERROR_PATH_ADM);	// Admin page
				// if($_SERVER['PHP_SELF'] == '/index.php' ){ die; }
			}else{
				// include_once (NM_DOMAIN_ERROR_PATH); die;	// Client page	open		
			}
		}else{
			goto_url(NM_DOMAIN_ERROR_URL); 
		}
	}
}

// 날짜
function get_ymd($date){
	
	$ymd = substr($date, 0, 10);
	return $ymd;
}
// 시간
function get_his($date){
	
	$his = substr($date, 11, 8);
	return $his;
}


// 한글 요일
function get_yoil($date, $full=0, $week=0)
{
    $arr_yoil = array ('일', '월', '화', '수', '목', '금', '토');

    if ($week) {
		$yoil = $date;
	}else{
		$yoil = date("w", strtotime($date));
	}
    $str = $arr_yoil[$yoil];
    if ($full) {
        $str .= '요일';
    }
    return $str;
}

// 요일 타입
function week_type($type=0)
{
	$week_type = NM_TIME_W;
	if($type==7 && $week_type==0){ $week_type=7; }
    return $week_type;
}

function cut_hangul_last($hangul)
{
    // 한글이 반쪽나면 ?로 표시되는 현상을 막음
    $cnt = 0;
    for($i=0;$i<strlen($hangul);$i++) {
        // 한글만 센다
        if (ord($hangul[$i]) >= 0xA0) {
            $cnt++;
        }
    }

    return $hangul;
}

// UTF-8 문자열 자르기
// 출처 : https://www.google.co.kr/search?q=utf8_strcut&aq=f&oq=utf8_strcut&aqs=chrome.0.57j0l3.826j0&sourceid=chrome&ie=UTF-8
function utf8_strcut( $str, $size, $suffix='...' )
{
        $substr = substr( $str, 0, $size * 2 );
        $multi_size = preg_match_all( '/[\x80-\xff]/', $substr, $multi_chars );

        if ( $multi_size > 0 )
            $size = $size + intval( $multi_size / 3 ) - 1;

        if ( strlen( $str ) > $size ) {
            $str = substr( $str, 0, $size );
            $str = preg_replace( '/(([\x80-\xff]{3})*?)([\x80-\xff]{0,2})$/', '$1', $str );
            $str .= $suffix;
        }

        return $str;
}

/*
-----------------------------------------------------------
    Charset 을 변환하는 함수
-----------------------------------------------------------
iconv 함수가 있으면 iconv 로 변환하고
없으면 mb_convert_encoding 함수를 사용한다.
둘다 없으면 사용할 수 없다.
*/
function convert_charset($from_charset, $to_charset, $str)
{

    if( function_exists('iconv') )
        return iconv($from_charset, $to_charset, $str);
    elseif( function_exists('mb_convert_encoding') )
        return mb_convert_encoding($str, $to_charset, $from_charset);
    else
        die("Not found 'iconv' or 'mbstring' library in server.");
}

// $_POST 형식에서 checkbox 엘리먼트의 checked 속성에서 checked 가 되어 넘어 왔는지를 검사
function is_checked($field)
{
    return !empty($_POST[$field]);
}

// ip2long 인터넷 프로토콜의 점이 찍혀 있는 주소를 포함한 문자열을 고유의 주소로 바꾸어줍니다
function abs_ip2long($ip='')
{ 
    $ip = $ip ? $ip : $_SERVER['REMOTE_ADDR'];
    return abs(ip2long($ip));
}


function get_selected($field, $value)
{
    return ($field==$value) ? ' selected="selected"' : '';
}


function get_checked($field, $value)
{
    return ($field==$value) ? ' checked="checked"' : '';
}


function is_mobile()
{
    return preg_match('/'.NM_MOBILE_AGENT.'/i', $_SERVER['HTTP_USER_AGENT']);
}

function is_nexcube()
{
	global $nm_member, $_SESSION;

	$is_nexcube = false;
	// if(($nm_member['mb_class'] == 'a' || $_SESSION['ss_adm_id'] != '' || NM_NEXCUBE_ACCESS == true)){
	if(NM_NEXCUBE_ACCESS == true){
		$is_nexcube = true;
	}
	return $is_nexcube;
}

function is_dev()
{
	$is_dev = false;
	if(REMOTE_ADDR == '119.42.175.158' || REMOTE_ADDR == '119.42.175.148' || REMOTE_ADDR == '119.42.175.147' || REMOTE_ADDR == '119.42.175.146'){
		$is_dev = true;
	}
	
	return $is_dev;
}

function is_app($setapk=0)
{
	$is_app = false;

	// 절대 매칭 안되는 값으로 초기화
	$agent = $app_setapk = $app_market = NM_DOMAIN_DIR.NM_VERSION.time();

    $agent = strtolower(HTTP_USER_AGENT);
    $app_setapk = strtolower(NM_APP_SETAPK);

	if($setapk==0){ // apk만 조회하고 싶을때
		$app_market = strtolower(NM_APP_MARKET);
	}

    if (preg_match("/".$app_setapk."/", $agent))			{ $is_app = true; }
    else if(preg_match("/".$app_market."/", $agent))		{ $is_app = true; }
    else { $is_app = false; }

	/*
	if($is_app == false){
		echo "false";
	}else{
		echo "true";
	}
	*/
	
	return $is_app;
}

// 메일 보내기 (파일 여러개 첨부 가능)
// type : text=0, html=1, text+html=2
function mailer($fname, $fmail, $_id, $to, $subject, $content, $type=0, $file="", $cc="", $bcc="")
{
    if ($type != 1)
        $content = nl2br($content);

    $mail = new PHPMailer(); // defaults to using php "mail()"
    if (defined('NM_SMTP') && NM_SMTP) {
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host = NM_SMTP; // SMTP server
        if(defined('NM_SMTP_PORT') && NM_SMTP_PORT)
            $mail->Port = NM_SMTP_PORT;
    }
    $mail->CharSet = 'UTF-8';
    $mail->From = $fmail;
    $mail->FromName = $fname;
    $mail->Subject = $subject;
    $mail->AltBody = ""; // optional, comment out and test
    $mail->msgHTML($content);
    $mail->addAddress($to, $_id);

    return $mail->send();
}



// CHARSET 변경 : euc-kr -> utf-8
function iconv_utf8($str)
{
    return iconv('euc-kr', 'utf-8', $str);
}


// CHARSET 변경 : utf-8 -> euc-kr
function iconv_euckr($str)
{
    return iconv('utf-8', 'euc-kr', $str);
}


// CHARSET 변경 : utf-8 -> cp949 (euc-kr 확장)
function iconv_cp949($str)
{
    return iconv('utf-8', 'cp949', $str);
}


// 세션변수 생성
function set_session($session_name, $value)
{
    if (PHP_VERSION < '5.3.0')
        session_register($session_name);
    // PHP 버전별 차이를 없애기 위한 방법
    $$session_name = $_SESSION[$session_name] = $value;
}


// 세션변수값 얻음
function get_session($session_name)
{
    return isset($_SESSION[$session_name]) ? $_SESSION[$session_name] : '';
}


// 쿠키변수 생성 // 기본 한달 60*60*24*30
function set_cookie($cookie_name, $value, $expire=259200)
{
    setcookie(md5($cookie_name), base64_encode($value), NM_SERVER_TIME + $expire, '/', NM_COOKIE_DOMAIN);
}

// 쿠키변수값 얻음
function get_cookie($cookie_name)
{
    $cookie = md5($cookie_name);
    if (array_key_exists($cookie, $_COOKIE))
        return base64_decode($_COOKIE[$cookie]);
    else
        return "";
}

// 세션변수값 삭제
function del_session($session_name)
{
    if (PHP_VERSION < '5.3.0')
        session_register($session_name);
    // PHP 버전별 차이를 없애기 위한 방법
    $$session_name = $_SESSION[$session_name] = '';
}

// 쿠키변수 삭제 // 기본 한달 60*60*24*30
function del_cookie($cookie_name, $expire=259200)
{
    setcookie(md5($cookie_name), '', NM_SERVER_TIME - $expire, '/', NM_COOKIE_DOMAIN);
}

// 쿠키변수 생성 // 기본 한달 60*60*24*30
function set_js_cookie($cookie_name, $value, $expire=259200)
{
    setcookie($cookie_name, $value, NM_SERVER_TIME + $expire, '/', NM_COOKIE_DOMAIN);
}

// 쿠키변수값 얻음
function get_js_cookie($cookie_name)
{
    return strip_tags(addslashes(trim($_COOKIE[$cookie_name])));
}

// 쿠키변수 삭제 // 기본 한달 60*60*24*30
function del_js_cookie($cookie_name, $expire=259200)
{
    setcookie($cookie_name, '', NM_SERVER_TIME - $expire, '/', NM_COOKIE_DOMAIN);
}

// 파일의 용량을 구한다.
//function get_filesize($file)
function get_filesize($size)
{
    //$size = @filesize(addslashes($file));
    if ($size >= 1048576) {
        $size = number_format($size/1048576, 1) . "M";
    } else if ($size >= 1024) {
        $size = number_format($size/1024, 1) . "K";
    } else {
        $size = number_format($size, 0) . "byte";
    }
    return $size;
}

// 폴더의 용량 ($dir는 / 없이 넘기세요)
function get_dirsize($dir)
{
    $size = 0;
    $d = dir($dir);
    while ($entry = $d->read()) {
        if ($entry != '.' && $entry != '..') {
            $size += filesize($dir.'/'.$entry);
        }
    }
    $d->close();
    return $size;
}


// 숫자+, 만 체크
function num_check($value)
{
	if(!is_array($value)) {
		if(preg_match('/[^0-9\,]/', $value) && $value){
			return false;
		}else{
			//return (int)$value;
			return $value;
		}
	}else{
		return false;
	}
}

// 영문+, 만 체크
function eng_check($value)
{
	if(!is_array($value)) {
		if(preg_match('/[^a-zA-Z\,]/', $value) && $value){
			return false;
		}else{
			return $value;
		}
	}else{
		return false;
	}
	
}

// 숫자+영문+, 만 체크
function num_eng_check($value)
{
	if(!is_array($value)) {
		if(preg_match('/[^0-9a-zA-Z\,]/', $value) && $value){
			return false;
		}else{
			return $value;
		}
	}else{
		return false;
	}
}

// 숫자+영문+,_ 만 체크
function num_eng_ext_check($value)
{
	if(!is_array($value)) {
		if(preg_match('/[^0-9a-zA-Z\,_]/', $value) && $value){
			return false;
		}else{
			return $value;
		}
	}else{
		return false;
	}
}

// ID 양식 체크
function id_check($value)
{	// 영문, 숫자, 특수문자( @ . _ - )
	if(preg_match('/(^[a-zA-Z0-9\.@_-]+$)/', $value) && $value){
		return $value;
	}else{
		return false;
	}
}

// 숫자+-만 체크
function point_check($value)
{
	if(preg_match('/[^0-9\-]+$/', $value) && $value){
		return false;
	}else{
		//return (int)$value;
		return $value;
	}
}


// PW 양식 체크
function pw_check($value)
{	// 영문, 숫자, 특수문자( @ . _ = + ! # )
	if(preg_match('/(^[a-zA-Z0-9\.@_=+!#]+$)/', $value) && $value){
		return $value;
	}else{
		return false;
	}
}

// EMALL 양식 체크
function email_check($value)
{
	/* 아래 함수는 php 5.2 부터 가능함 */
	if (PHP_VERSION < '5.2.0'){
		if(preg_match('/([0-9a-zA-Z_-]+)@([0-9a-zA-Z_-]+)\.([0-9a-zA-Z_-]+)/', $value) && $value){
			return $value;
		}else{
			return false;
		}
	}else{
		if (filter_var($value, FILTER_VALIDATE_EMAIL)) { 
			return $value;
		}else{
			return false;	
		}
	}
}

// 기본 필터
function base_filter($value)
{
	if(!is_array($value)) {
		return urldecode(addslashes(trim($value)));
	}else{
		return $value;
	}
}

// 기본 GET, REQUEST 필터
function base_get_filter($value)
{
	if(!is_array($value)) {
		return addslashes(trim($value));
	}else{
		return $value;
	}
}

// 태그 필터
function tag_filter($value)
{	
	if(!is_array($value)) {
		return urldecode(strip_tags(addslashes(trim($value))));
	}else{
		return $value;
	}
}

// 기본 GET, REQUEST 필터
function tag_get_filter($value)
{	
	if(!is_array($value)) {
		return strip_tags(addslashes(trim($value)));
	}else{
		return $value;
	}
}

// 특수기호삭제
function etc_filter($value)
{
	if(!is_array($value)) {
		$value = preg_replace("/[\<\>\'\"\\\'\\\"\%\=\(\)\/\^\*\s]/", "", $value);
		return $value;
	}else{
		return $value;
	}
}

// 특수기호삭제
function etc_filter2($value)
{
	if(!is_array($value)) {
		$str_pattern = array();
		$str_pattern[] = '#\.*/+#';
		$str_pattern[] = '#\\\*#';
		$str_pattern[] = '#\.{2,}#';
		$str_pattern[] = '#[/\'\"%=*\#\(\)\|\+\&\!\$~\{\}\[\]`;:\?\^\,]+#';

		$str_replace = array();
		$str_replace[] = '';
		$str_replace[] = '';
		$str_replace[] = '.';
		$str_replace[] = '';

		$value = preg_replace($str_pattern, $str_replace, $value);
		return $value;
	}else{
		return $value;
	}
}

/******************************************************************************* 끝에다가;;;
    유일한 키를 얻는다.

    결과 :

        년월일시분초00 ~ 년월일시분초99
        년(4) 월(2) 일(2) 시(2) 분(2) 초(2) 100분의1초(2)
        총 16자리이며 년도는 2자리로 끊어서 사용해도 됩니다.
        예) 2008062611570199 또는 08062611570199 (2100년까지만 유일키)

    사용하는 곳 :
    1. 글쓰기시 미리 유일키를 얻어 파일 업로드 필드에 넣는다.
    2. 주문번호 생성시에 사용한다.
    3. 기타 유일키가 필요한 곳에서 사용한다.
	4. 어느기간 지날시 자동삭제 기능을 넣어준다.
*******************************************************************************/
// 기존의 get_unique_id() 함수를 사용하지 않고 get_uniqid() 를 사용한다.
function get_uniqid()
{
    /* <input type="hidden" name="uid" value="<?php echo get_uniqid(); ?>"> */

    sql_query(" LOCK TABLE `config_only_id` WRITE ");
    while (1) {
        // 년월일시분초에 100분의 1초 두자리를 추가함 (1/100 초 앞에 자리가 모자르면 0으로 채움)
        $key = date('YmdHis', time()) . str_pad((int)(microtime()*100), 2, "0", STR_PAD_LEFT);
		$key_year = date('Y',time());
		$key_month = date('m',time());
		
		/* 음...날짜까지 넣을 생각임 날짜를 index로 하고 주기적으로 자동 삭제 기능 넣을 생각임 
		   필요하다고 생각하는 이유는 자동으로 글넣는 프로그램 방지(광고방지)
		*/
        $result = sql_query(" insert into `config_only_id` set coi_id = '$key', coi_ip = '{$_SERVER['REMOTE_ADDR']}', coi_year='$key_year', coi_month='$key_month', coi_date='".NM_TIME_YMDHIS."' ", false);

        if ($result) break; // 쿼리가 정상이면 빠진다.

        // insert 하지 못했으면 일정시간 쉰다음 다시 유일키를 만든다.
        usleep(10000); // 100분의 1초를 쉰다
    }
    sql_query(" UNLOCK TABLES ");

    return $key;
}



/*******************************************************************************
창이동, alert 기능, 부모창이동, 팝업닫기
*******************************************************************************/
// 메타태그를 이용한 URL 이동
// header("location:URL") 을 대체
function goto_url($url)
{
    $url = str_replace("&amp;", "&", $url);
    //echo "<script> location.replace('$url'); </script>";

    if (!headers_sent())
        header('Location: '.$url);
    else {
        echo '<script>';
        echo 'location.replace("'.$url.'");';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>';
    }
    exit;
}
function alert($msg, $url='', $error='')
{
	$goto_url = "";
	if($error!=''){ $msg.= '\n'.addslashes($error);}
	if($url){
		$goto_url = "document.location.href = '".$url."';";
	}
	echo' 
		<script type="text/javascript">
		<!--
			alert("'.$msg.'");
			'.$goto_url.'
		//-->
		</script>
		';
}
function alert_close($msg, $url='', $error='')
{
	$goto_url = "";
	if($error!=''){ $msg.= '\n'.addslashes($error);}
	if($url){
		$goto_url = "document.location.href = '".$url."';";
	}
	echo' 
		<script type="text/javascript">
		<!--
			alert("'.$msg.'");
			'.$goto_url.'
		//-->
		</script>
		';
}

function alert_para($para_check, $para_name)
{	/* alert_para($_mb_class, '회원 분류'); 이런식으로 사용하면 됨 */
	if($para_check == false){
		$msg = tag_filter($para_name).' 변수 확인해보세요.';		
		$goto_url = "document.location.href = '".$_SERVER['HTTP_REFERER']."';";

		echo' 
			<script type="text/javascript">
			<!--
				alert("'.$msg.'");
				'.$goto_url.'
			//-->
			</script>
			';
		die;
	}
}

function pop_close($msg='', $url='', $error='')
{	
	/* 메세지 */
	$alert = "";
	if($error!=''){ $msg.= '\n'.addslashes($error);}
	if($msg!=''){ $alert ='	alert("'.$msg.'");';}
	
	/* 부모창 */
	$location = "window.opener.document.location.reload();";
	if($url!=''){ $location =' opener.parent.location.href="'.$url.'";';}
	
	echo' 
		<script type="text/javascript">
		<!--
			'.$alert.'
			'.$location.'
			window.close();
			self.close();
			window.open("", "_self").close();
		//-->
		</script>
		';
}


function pop_url($msg='', $url='', $error='')
{	
	/* 메세지 */
	$alert = "";
	if($error!=''){ $msg.= '\n'.addslashes($error);}
	if($msg!=''){ $alert ='	alert("'.$msg.'");';}
	
	/* 부모창 */
	$location = "window.opener.document.location.reload();";
	if($url!=''){ $location =' location.href="'.$url.'";';}
	
	echo' 
		<script type="text/javascript">
		<!--
			'.$alert.'
			'.$location.'
		//-->
		</script>
		';
}


function alert_iframe($msg='', $url='', $error='')
{	
	/* 메세지 */
	$alert = "";
	if($error!=''){ $msg.= '\n'.addslashes($error);}
	if($msg!=''){ $alert ='	alert("'.$msg.'");';}
	
	/* 부모창 */
	$location = "parent.document.location.reload();";
	if($url!=''){ $location ='parent.document.location.href="'.$url.'";';}
	
	echo' 
		<script type="text/javascript">
		<!--
			'.$alert.'
			'.$location.'
		//-->
		</script>
		';
}


/*******************************************************************************
셀랙트박스 생성
*******************************************************************************/
function tag_selects($data, $selectid, $selected_no, $selected_all='y', $selected_all_text='전체', $selected_all_val=0){
	/*	tag_selects(데이터배열, ID, 선택한값=키값과일치해야함, 전체선택, 전체텍스트, 전체값
		tag_selects($data, 'id', 0, 'y', '전체', 0)
	*/
	echo '<select name="'.$selectid.'" id="'.$selectid.'" class="'.$selectid.'">';
		$select_selected = "";
		$select_no = $selected_all_val;
		if($selected_no == ""){ $select_selected = 'selected="selected"'; }
		if($selected_all == 'y'){ echo '<option value="'.$select_no.'" '.$select_selected.'>'.$selected_all_text.'</option>'; }
		if(is_array($data)){
			foreach($data as $data_key => $data_val) {
				if($data_val == ""){ continue; }

				$select_no = $data_key;
				$select_selected = "";
				if($select_no == $selected_no && $selected_no != ""){ $select_selected = 'selected="selected"'; }
				echo '<option value="'.$select_no.'" '.$select_selected.'>'.$data_val.'</option>'	;
			}
		}
	echo '</select>';
}

/*******************************************************************************
라디오박스 생성
*******************************************************************************/
function tag_radios($data, $radioid, $checked_no, $checked_all='y', $checked_all_text='전체', $checked_all_val=0, $checked_br=0){
	/*	tag_radios(데이터배열, ID, 선택한값=키값과일치해야함, 전체선택, 전체텍스트, 전체값, 줄바꿈
		tag_radios($data, 'id', 0, 'y', '전체', 0, 0)
	*/
		$radioid_name = $radioid; // 배열시 name만 적용
		if(strpos($radioid, '[')){
			$radioid = substr($radioid, 0, strpos($radioid, '['));// 이름이 배열시 수정
		}
		$checked_br_cnt = 0; // 줄바꿈카운터
		$checked_all_label = $checked_all_text.'('.implode("/",$data).')';
		if($checked_all_text != '전체'){ $checked_all_label = $checked_all_text; }
		$radio_checked = '';
		$check_no = $checked_all_val;
		if($checked_no == ""){ $radio_checked = 'checked="checked"'; }
		if($checked_all == 'y'){
			echo '<input type="radio" class="'.$radioid.'" name="'.$radioid_name.'" id="'.$radioid.'all" value="'.$check_no.'" '.$radio_checked.'><label for="'.$radioid.'all">'.$checked_all_label.'</label>';
			$checked_br_cnt++;
		}
		// 값 파이프로 들어올시 배열처리
		$checked_no_arr = explode("|",$checked_no); 

		foreach($data as $data_key => $data_val) {
			if($data_val == ""){ continue; }
			$check_no = $data_key;
			$radio_checked = '';
			if($check_no == $checked_no && $checked_no != ""){
				$radio_checked = 'checked="checked"';
			}
			if(in_array($check_no,$checked_no_arr) && $checked_no != ""){
				$radio_checked = 'checked="checked"';
			}
			if($checked_all == 'n'){ $check_no = $data_key; }
			echo '<input type="radio" name="'.$radioid_name.'" id="'.$radioid.$check_no.'" value="'.$check_no.'" '.$radio_checked.'><label for="'.$radioid.$check_no.'">'.$data_val.'</label>';	
			$checked_br_cnt++;
			if($checked_br > 0 &&$checked_br_cnt % $checked_br == 0){echo '<br/>';}
		}
}

/*******************************************************************************
체크박스 생성
*******************************************************************************/
function tag_checkbox($data, $checkboxid, $checked_no, $checked_all='y', $checked_all_text='전체', $checked_all_val=0, $checked_br=0){
	/*	tag_checkboxs(데이터배열, ID, 선택한값=키값과일치해야함, 전체선택, 전체텍스트, 전체값, 줄바꿈
		tag_checkboxs($data, 'id', 0, 'y', '전체', 0, 0)
	*/	
		$checkboxid_name = $checkboxid; // 배열시 name만 적용
		if(strpos($checkboxid, '[')){ // 배열시 name만 적용
			$checkboxid = substr($checkboxid, 0, strpos($checkboxid, '['));// 이름이 배열시 수정
		}
		$checked_br_cnt = 0; // 줄바꿈카운터
		$checked_all_label = $checked_all_text.'('.implode("/",$data).')';
		if($checked_all_text != '전체'){ $checked_all_label = $checked_all_text; }
		$checkbox_checked = '';
		$check_no = $checked_all_val;
		if($checked_no == ""){ $checkbox_checked = 'checked="checked"'; }
		if($checked_all == 'y'){
			echo '<input type="checkbox" class="'.$checkboxid.'" name="'.$checkboxid_name.'" id="'.$checkboxid.'all" value="'.$check_no.'" '.$checkbox_checked.'><label for="'.$checkboxid.'all">'.$checked_all_label.'</label>';
			$checked_br_cnt++;
		}

		// 값 파이프로 들어올시 배열처리
		$checked_no_arr = explode("|",$checked_no); 

		foreach($data as $data_key => $data_val) {
			if($data_val == ""){ continue; }
			$check_no = $data_key;
			$checkbox_checked = '';
			if($check_no == $checked_no && $checked_no != ""){
				$checkbox_checked = 'checked="checked"';
			}
			if(in_array($check_no,$checked_no_arr) && $checked_no != ""){
				$checkbox_checked = 'checked="checked"';
			}
			if($checked_all == 'n'){ $check_no = $data_key; }
			echo '<input type="checkbox" name="'.$checkboxid_name.'" id="'.$checkboxid.$check_no.'" value="'.$check_no.'" '.$checkbox_checked.'><label for="'.$checkboxid.$check_no.'">'.$data_val.'</label>';	
			$checked_br_cnt++;
			if($checked_br > 0 &&$checked_br_cnt % $checked_br == 0){echo '<br/>';}
		}
}

/* ///////////////////////// 가격 관련 함수 ////////////////////////////////// */
function cash_point_view($pay, $zero='n', $unit='y', $won='n') /* cash -> 코인 */
{	
    global $nm_config;
	$unit_kr = '';
	if($won=='y'){ $unit_kr = '_ko'; }
	$cash_point_unit = $nm_config['cf_cash_point_unit'.$unit_kr];
	if($pay == ''){ $pay = 0; }
	$pay = number_format($pay);

	if($unit == 'n'){ $cash_point_unit = ""; }

	if($pay == 0){
		if($zero == 'y'){ $pay.=$cash_point_unit;
		}else{ $pay ='무료'; }
	}else{
		$pay.=$cash_point_unit;
	}
	
    return $pay;
	
}

function point_view($point, $zero='n', $unit='y', $won='n') /* point -> 미니 코인 */
{
    global $nm_config;
	$unit_kr = '';
	if($won=='y'){ $unit_kr = '_ko'; }
	$point_unit = $nm_config['cf_point_unit'.$unit_kr];
	if($point == ''){ $point = 0; }
	$point = number_format($point);

	if($unit == 'n'){ $point_unit = ""; }

	if($point == 0){ 
		if($zero == 'y'){ $point.=$point_unit;
		}else{ $point ='무료'; }
	}else{
		$point.=$point_unit;
	}
    return $point;
}

/* 폴더 및 파일 삭제 */
function rmdirAll($dir, $mode=0) {
	$dir_check = is_dir($dir);
	if($dir_check == true){
		$dirs = dir($dir);
		while(false !== ($entry = $dirs->read())) {
			if(($entry != '.') && ($entry != '..')) {
				if(is_dir($dir.'/'.$entry)) {
					rmdirAll($dir.'/'.$entry);
				} else {
					@unlink($dir.'/'.$entry);
				}
			}
		}
		$dirs->close();
		if($mode == 0){
			@rmdir($dir);
			if(substr($dir, -1) == "/"){
				$dirm = substr($dir,0,strrpos($dir, "/"));
				@rmdir($dirm);
			}
		}
	}
}

/* 폴더 생성 */
function mkdirAll($dir)
{	
	// 상단 디렉토리가 777이여야 함
	$dirs = explode("/", $dir);
	$dir_path = NM_PATH;

	foreach($dirs as $dir_val){
		$dir_path.= "/";
		$dir_path.= $dir_val;
		
		$dir_check = is_dir($dir_path);
		if($dir_check == FALSE){
			@mkdir($dir_path, 0777, true);
			@chmod($dir_path, 0777);
		}	 
	}
}

function cpdirAll($ordir, $cpdir)
{
	if($ordir == '' || $ordir == NULL){ return false; }
	if($cpdir == '' || $cpdir == NULL){ return false; }
	
	// 경로 중 하나라도 빈칸으로 온다면...
	if(strpos($ordir, '//') > 0){ return false; }
	if(strpos($cpdir, '//') > 0){ return false; }
	
	// 아래 재귀함수 처리
	$ordir = str_replace(NM_PATH, "", $ordir);
	$cpdir = str_replace(NM_PATH, "", $cpdir);

	$ordir = NM_PATH.$ordir;
	$cpdir = NM_PATH.$cpdir;

	// echo $ordir."<br />\n";  
	// echo $cpdir."<br />\n";  

	if(filetype($ordir) === 'dir') { 
	   clearstatcache(); 

	   if($fp = @opendir($ordir)) { 
			  while(false !== ($ftmp = readdir($fp))){ 
					if(($ftmp !== ".") && ($ftmp !== "..") && ($ftmp !== "")) { 
						  if(filetype($ordir.'/'.$ftmp) === 'dir') { 
							   clearstatcache(); 

							   @mkdir($cpdir.'/'.$ftmp); 
							   // echo ($cpdir.'/'.$ftmp."<br />\n"); 
							   set_time_limit(0); 
							   cpdirAll($ordir.'/'.$ftmp,$cpdir.'/'.$ftmp); 
						  } else { 
							   copy($ordir.'/'.$ftmp,$cpdir.'/'.$ftmp); 
						  } 
					} 
			  } 
	   } 
	   if(is_resource($fp)){ 
			 closedir($fp); 
	   } 
	} else { 
		// echo $cpdir."<br />\n";      
		copy($ordir,$cpdir); 
	} 

}


// get_browser() 함수는 이미 있음
function get_brow($agent)
{
    $agent = strtolower($agent);

    //echo $agent; echo "<br/>";

    if (preg_match("/msie ([1-9][0-9]\.[0-9]+)/", $agent, $m))	{ $s = 'MSIE '.$m[1]; }
    else if(preg_match("/msie ([1-9]\.[0-9]+)/", $agent, $m))	{ $s = 'MSIE '.$m[1]; }
	else if(preg_match("/trident/", $agent) && (preg_match("/rv:11.0/", $agent))) { $s = "MSIE 11"; }
    else if(preg_match("/naver/", $agent))						{ $s = "Naver"; }
    else if(preg_match("/firefox/", $agent))					{ $s = "FireFox"; }
    else if(preg_match("/chrome/", $agent))						{ $s = "Chrome"; }
    else if(preg_match("/safari/", $agent))						{ $s = "Safari"; }
    else if(preg_match("/opera/", $agent))						{ $s = "Opera"; }
    else if(preg_match("/internet explorer/", $agent))			{ $s = "IE"; }

    else if(preg_match("/x11/", $agent))						{ $s = "Netscape"; }
    else if(preg_match("/gec/", $agent))						{ $s = "Gecko"; }
    else if(preg_match("/bot|slurp/", $agent))					{ $s = "Robot"; }
    else if(preg_match("/mozilla/", $agent))					{ $s = "Mozilla"; }
    else { $s = "기타"; }

    return $s;
}

function get_os($agent)
{
    $agent = strtolower($agent);

    //echo $agent; echo "<br/>";

    if (preg_match("/windows 98/", $agent))                 { $s = "98"; }
    else if(preg_match("/windows 95/", $agent))             { $s = "95"; }
    else if(preg_match("/windows nt 4\.[0-9]*/", $agent))   { $s = "NT"; }
    else if(preg_match("/windows nt 5\.0/", $agent))        { $s = "2000"; }
    else if(preg_match("/windows nt 5\.1/", $agent))        { $s = "XP"; }
    else if(preg_match("/windows nt 5\.2/", $agent))        { $s = "2003"; }
    else if(preg_match("/windows nt 6\.0/", $agent))        { $s = "Vista"; }
    else if(preg_match("/windows nt 6\.1/", $agent))        { $s = "Windows7"; }
    else if(preg_match("/windows nt 6\.2/", $agent))        { $s = "Windows8"; }
    else if(preg_match("/windows 9x/", $agent))             { $s = "ME"; }
    else if(preg_match("/windows ce/", $agent))             { $s = "CE"; }
    else if(preg_match("/sunos/", $agent))                  { $s = "sunOS"; }
    else if(preg_match("/irix/", $agent))                   { $s = "IRIX"; }
    else if(preg_match("/internet explorer/", $agent))      { $s = "IE"; }
    else if(preg_match("/iphone os/", $agent))              { $s = "iPhone OS"; }
    else if(preg_match("/ipad/", $agent))              			{ $s = "iPad MAC"; }
    else if(preg_match("/android/", $agent))								{ $s = "Android"; }
    else if(preg_match("/mac/", $agent))                    { $s = "MAC"; }

    else if(preg_match("/bot|slurp/", $agent))              { $s = "Robot"; }
    else if(preg_match("/linux/", $agent))                  { $s = "Linux"; }
    else if(preg_match("/phone/", $agent))                  { $s = "Phone"; }
    else if(preg_match("/mozilla/", $agent))                { $s = "Mozilla"; }
    else { $s = "기타"; }

    return $s;
}

///////////////////////////////////// 정렬알고리즘 ////////////////////////////////////////////////
/* 버블정렬 오름차순 */
function bubble_up_sort($data_arr, $data_field){
	$data_arr_count = count($data_arr);
	
	for($i=0; $i<$data_arr_count; $i++){
		for($j=0; $j<($data_arr_count-1)-$i; $j++){
		    if (strtotime($data_arr[$j][$data_field]) > strtotime($data_arr[$j+1][$data_field])){ // 뒤의 원소가 앞의 원소보다 작으면
				// 뒤의 원소가 앞의 원소보다 작으면
				$tmp = $data_arr[$j];
				$data_arr[$j] = $data_arr[$j+1];
				$data_arr[$j+1] = $tmp;
			} // end if
		} // end for
	} // end for
	return $data_arr;
}

/* 버블정렬 내림차순 */
function bubble_down_sort($data_arr, $data_field){
    $data_arr_count = count($data_arr);

    for($i=0; $i<$data_arr_count; $i++){
        for($j=0; $j<($data_arr_count-1)-$i; $j++){
            if (strtotime($data_arr[$j][$data_field]) < strtotime($data_arr[$j+1][$data_field])){ // 뒤의 원소가 앞의 원소보다 작으면
                // 뒤의 원소가 앞의 원소보다 작으면
                $tmp = $data_arr[$j];
                $data_arr[$j] = $data_arr[$j+1];
                $data_arr[$j+1] = $tmp;
            } // end if
        } // end for
    } // end for
    return $data_arr;
}

// 퀵 정렬(Quick Sort)
function Swap(&$arr, $idx1, $idx2) {
	$temp = $arr[$idx1];
	$arr[$idx1] = $arr[$idx2];
	$arr[$idx2] = $temp; 
}
 
// 중간값 찾기
function MedianOfThree($arr, $field, $left, $right) {
	$samples = array($left, floor(($left+$right)/2), $right);
	if ($arr[$samples[0][$field]] > $arr[$samples[1][$field]])
		Swap($samples, 0, 1);
	if ($arr[$samples[1][$field]] > $arr[$samples[2][$field]])
		Swap($samples, 1, 2);
	if ($arr[$samples[0][$field]] > $arr[$samples[1][$field]])
		Swap($samples, 0, 1);
 
	return $samples[1];
}
 
function Partition(&$arr, $field, $left, $right) {
	$pIdx = MedianOfThree($arr, $field, $left, $right);		// 중간값으로 피벗 선택
	$pivot = $arr[$pIdx][$field];
 
	$low = $left + 1;
	$high = $right;
	Swap($arr, $left, $pIdx);	 // 피벗을 가장 왼쪽으로 이동
 
	while ($low <= $high) {	// 교차되지 않을 때까지 반복
 
		// 피벗보다 큰 값을 찾는 과정
		while ($low <= $right  && $pivot >= $arr[$low][$field])
			$low++;
		
		// 피벗보다 작은 값을 찾는 과정
		while ($high >= ($left+1) && $pivot <= $arr[$high][$field])  
			$high--;
 
		// 교차되지 않는 상태라면 Swap 실행
		if ($low <= $high)
			Swap($arr, $low, $high);
	}
 
	Swap($arr, $left, $high);	// 피벗과 high 가 가리키는 대상 교환
	return $high;					// 옮겨진 피벗의 위치정보 교환
}
 
function quick_sort(&$arr, $field, $left, $right) {
	// http://sir.kr/pg_tip/14894
	if ($left < $right) {
		$pivot = Partition($arr, $field, $left, $right); 
		quick_sort($arr, $field, $left, $pivot-1);
		quick_sort($arr, $field, $pivot+1, $right);
	}
	return $arr;
}

// 이미지 캐시 때문에 뒤에 날짜파라미터 넣기
// function img_url_para($url, $date1, $date2='', $width='',  $height='', $ratio='', $x_axis='', $y_axis='')
function img_url_para($url, $date1, $date2='', $key='', $wxh='', $temp1='', $temp2='', $temp3='')
{
	global $thumbnail, $nm_member, $extension;

	if($url == ''){ return false; }
	
	/* 이미지 리사이징 */
	$thumbnail_url = img_tn_url_para($url, $date1, $date2, $key, $wxh, $temp1, $temp2, $temp3);
	
	if($thumbnail_url != false){
		$url = $thumbnail_url;
	} // end if

	/* 날짜 PARAMETER */
	$img_url_date = NM_IMG.$url;
	$date_para = $date1;

	if($date2 != '' && $date1 < $date2){
		$date_para = $date2;
	}

	$date_para = preg_replace('/[^0-9a-zA-Z]/', "", $date_para);
	
	$img_url_date.= "?dtp=".$date_para; // dtp = datetime paramiter

	return $img_url_date;
}

function img_tn_url_para($url, $date1, $date2='', $key='', $wxh='', $temp1='', $temp2='', $temp3='')
{
	global $thumbnail, $extension;

	$url_file_info = pathinfo($url);
	$url_file_name = strtolower($url_file_info['filename']);
	$url_file_dir = strtolower($url_file_info['dirname']);
	$url_file_extension = strtolower($url_file_info['extension']);

	/* peanutoon일때는 기존 aws-resizing 적용 되게..[임시] */
	$nm_path_arr = explode("/", NM_PATH);
	$url_file_dir_arr = explode("/", $url_file_dir);

	if($nm_path_arr[count($nm_path_arr)-1] == 'peanutoon' && $url_file_dir_arr[0] == 'aws') {
		$peanutoon_thumbnail_name = '';
		if($key == 'cm_cover' || $key == 'cm_cover_sub'){ 
			$peanutoon_key = 'peanutoon_'.$key; 

			$peanutoon_key_exist = array_key_exists($peanutoon_key, $thumbnail);
			if($peanutoon_key_exist) {	
				foreach($thumbnail[$peanutoon_key] as $thumbnail_peanutoon_key => $thumbnail_peanutoon_val) {
					if(array_key_exists($wxh, $thumbnail_peanutoon_val)) {
						$wxh_exist = array_key_exists($wxh, $thumbnail_peanutoon_val);
					} // end if	
				} // end foreach
			} else {
				return false;
			}
			if($peanutoon_key_exist && $wxh_exist) {
				if(($wxh == 'tn' && $key == 'cm_cover') || ($key == 'embc_cover' && $wxh != 'all' && $wxh != '')) {
					return false;
				} else {
					// print_r($thumbnail[$peanutoon_key]);
					// echo $thumbnail[$peanutoon_key][0][$wxh]['width'];
					// die;
					foreach($thumbnail[$peanutoon_key] as $thumbnail_peanutoon_key => $thumbnail_peanutoon_val) {
						if($thumbnail_peanutoon_val[$wxh]['size'] != ''){
							$peanutoon_thumbnail_name = '_'.$thumbnail_peanutoon_val[$wxh]['size'];
						}
					}
					$peanutoon_img_url = $url_file_dir.'/'.$url_file_name.$peanutoon_thumbnail_name.'.'.$url_file_extension;
					return $peanutoon_img_url;
				} // end else	
			} else {
				return false;
			} // end else
		
		}else{
			return false;
		}
		
		// return false;
	} // end if
	/* peanutoon일때는 resizing 적용 안되게..[임시] end */


	$thumb_list = array();
	
	/* key, wxh 검사 */
	$key_exist = array_key_exists($key, $thumbnail);
	if($key_exist) {	
		foreach($thumbnail[$key] as $thumbnail_key => $thumbnail_val) {
			if(array_key_exists($wxh, $thumbnail_val)) {
				$wxh_exist = array_key_exists($wxh, $thumbnail_val);
			} // end if	
		} // end foreach
	} else {
		return false;
	} // end else

	/* key, wxh 값이 유효할 때만 */
	if($key_exist && $wxh_exist) {
		if(($wxh == 'tn') || ($key == 'embc_cover' && $wxh != 'all' && $wxh != '')) {
			return false;
		} else {
			$img_url = 'thumbnail/'.$url_file_dir.'/'.$url_file_name.'_'.$wxh.'.'.$url_file_extension;
			return $img_url;
		} // end else	
	} else {
		return false;
	} // end else
} // img_tn_url_para

// 캐시 때문에 뒤에 파라미터 넣기
function vs_para($val='')
{
	$nm_version = md5(preg_replace('/[^0-9a-zA-Z]/', "", NM_VERSION.$val));
	$vs_para = "?vs=".$nm_version;
	return $vs_para;
}

function vs_config($val='')
{
	$nm_version = md5(preg_replace('/[^0-9a-zA-Z]/', "", substr(NM_VERSION, 0, 4).$val));
	$vs_config = "?vs=".$nm_version;
	return $vs_config;
}

// 숫자만 반환
function get_int($val)
{
	$val_replace = preg_replace('/[^0-9]/', "", $val);
	return $val_replace;
}

// 영문만 반환
function get_eng($val, $strto=1)
{	
	// 대소문자 반환
	if($strto == 1){ $val = strtolower($val); }
	else if($strto == 2){ $val = strtoupper($val); }

	$val_replace = preg_replace('/[^a-zA-Z]/', "", $val);
	return $val_replace;
}

// 숫자&영문만 반환
function get_int_eng($val, $strto=1)
{	
	// 대소문자 반환
	if($strto == 1){ $val = strtolower($val); }
	else if($strto == 2){ $val = strtoupper($val); }

	$val_replace = preg_replace('/[^0-9a-zA-Z]/', "", $val);
	return $val_replace;
}

// 숫자&영문&한글,특정특수만 반환
function get_int_eng_kor_etc($val, $strto=1)
{	
	// 대소문자 반환
	if($strto == 1){ $val = strtolower($val); }
	else if($strto == 2){ $val = strtoupper($val); }
	
	// $pattern = '/([\xEA-\xED][\x80-\xBF]{2}|[a-zA-Z0-9\,_-])+/';
	$pattern = '/([\xEA-\xED][\x80-\xBF]{2}|[a-zA-Z0-9]|[-_@!#$%^*()+=])+/';
	preg_match_all($pattern, $val, $val_replace_arr);
	$val_replace = implode('', $val_replace_arr[0]);
	return $val_replace;
}

function get_token($val){
	$token = false;
	if($val != ''){ $token = get_int_eng(md5(session_id()."_".$val)); }
	return $token;
}

// 문자열 암호화
function get_encrypt_string($str)
{
    if(defined('NM_STRING_ENCRYPT_FUNCTION') && NM_STRING_ENCRYPT_FUNCTION) {
        $encrypt = call_user_func(NM_STRING_ENCRYPT_FUNCTION, $str);
    } else {
        $encrypt = sql_password($str);
    }

    return $encrypt;
}

// 비밀번호 비교
function check_password($pass, $hash)
{
    $password = get_encrypt_string($pass);

    return ($password === $hash);
}

// 동일한 host url 인지
function check_url_host($url, $msg='', $return_url=NM_URL)
{
    if(!$msg)
        $msg = 'url에 타 도메인을 지정할 수 없습니다.';

    $p = @parse_url($url);
    $host = preg_replace('/:[0-9]+$/', '', $_SERVER['HTTP_HOST']);

    if(stripos($url, 'http:') !== false) {
        if(!isset($p['scheme']) || !$p['scheme'] || !isset($p['host']) || !$p['host'])
            alert('url 정보가 올바르지 않습니다.', $return_url);
    }

    if ((isset($p['scheme']) && $p['scheme']) || (isset($p['host']) && $p['host'])) {
        //if ($p['host'].(isset($p['port']) ? ':'.$p['port'] : '') != $_SERVER['HTTP_HOST']) {
        if ($p['host'] != $host) {
            echo '<script>'.PHP_EOL;
            echo 'alert("url에 타 도메인을 지정할 수 없습니다.");'.PHP_EOL;
            echo 'document.location.href = "'.$return_url.'";'.PHP_EOL;
            echo '</script>'.PHP_EOL;
            echo '<noscript>'.PHP_EOL;
            echo '<p>'.$msg.'</p>'.PHP_EOL;
            echo '<p><a href="'.$return_url.'">돌아가기</a></p>'.PHP_EOL;
            echo '</noscript>'.PHP_EOL;
            exit;
        }
    }
}

function set_hour($id, $value, $text, $hour_disuse='y')
{
	echo '
	<label for="'.$id.'">'.$text.'</label>
	<select name="'.$id.'" id="'.$id.'" class="'.$id.'">';
	if($hour_disuse == 'y'){
		echo '<option value="">사용안함</option>';
	}
	for($rs=0; $rs<24; $rs++){ 
		$hour = $rs; 
		if($rs <10){$hour = "0".$rs;}
		$hour_select = "";
		if($value == $hour){ $hour_select = "selected"; }
		echo '<option value="'.$hour.'" '.get_selected($hour, $value).' '.$hour_check.'>'.$hour.'시</option>';
	}/* for($rs=0; $rs<24; $rs++){ */
	echo '</select>';
}

// 코믹스 정보 가져오기
function get_comics($comics)
{
	$comics_field = "	c.*, 
						c_publ.cp_name as publ_name, 
						c_prof.cp_name as prof_name, c_prof_sub.cp_name as prof_name_sub, c_prof_thi.cp_name as prof_name_thi,
						c_prov.cp_name as prov_name, c_prov_sub.cp_name as prov_name_sub ";

	$sql = "SELECT $comics_field FROM comics c 
			LEFT JOIN comics_publisher c_publ ON c_publ.cp_no = c.cm_publisher 
			LEFT JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional
			LEFT JOIN comics_professional c_prof_sub ON c_prof_sub.cp_no = c.cm_professional_sub 
			LEFT JOIN comics_professional c_prof_thi ON c_prof_thi.cp_no = c.cm_professional_thi 
			LEFT JOIN comics_provider c_prov ON c_prov.cp_no = c.cm_provider 
			LEFT JOIN comics_provider c_prov_sub ON c_prov_sub.cp_no = c.cm_provider_sub 	
			WHERE  c.`cm_no` = '$comics' ";
	$row = sql_fetch($sql);
	return $row;
}

// 코믹스 정보 가져오기(텍스트 검색 & CP 번호)
function get_comics_series($comics_text)
{
	$cm_no_arr = array();
	$sql = "SELECT * FROM comics  WHERE  `cm_series` like '%$comics_text%'";
	
	$result = sql_query($sql);
	$num_rows = sql_num_rows($result);
	if($num_rows > 0){
		while($row = sql_fetch_array($result)) {
			array_push($cm_no_arr, $row['cm_no']);
		} // end while
	}
	return $cm_no_arr;
}

function get_cp_comics_sql($cp_name, $cp_partner='')
{
	$sql_return = $sql_cm_no = $sql_cm = $sql_cp = $sql = "";
	$cm_no = ""; 
	$cm_no_arr = $cp_no = $partner_arr = array();
	$cp_cnts = 0; // 전체에서만 사용

	if($cp_name == ''){ return $sql_cp; }

	if($cp_partner == ""){
		array_push($partner_arr, 'provider', 'professional', 'publisher');
		$provider_arr		= array('cm_provider', 'cm_provider_sub');
		$professional_arr	= array('cm_professional', 'cm_professional_sub', 'cm_professional_thi');
		$publisher_arr		= array('cm_publisher');

		foreach($partner_arr as $partner_key => $partner_val){
			$sql = "SELECT * FROM comics_".$partner_val." WHERE cp_name like '%$cp_name%' ";
			$sql_result = sql_query($sql);
			$sql_num_rows = sql_num_rows($sql_result);
			
			$cp_no = array(); // 초기화
			if($sql_num_rows > 0){
				$cp_cnts++;
				while ($sql_rows = sql_fetch_array($sql_result)) {
					array_push($cp_no, $sql_rows['cp_no']);
				}
				// Comics cp SQL문
				if($cp_cnts > 1){ $sql_cp.= " OR "; }
				foreach(${$partner_val.'_arr'} as $partner_name){
					if( strpos($partner_name, '_sub') > 0 || strpos($partner_name, '_thi') > 0 ){ $sql_cp.= " OR "; }
					$sql_cp.= $partner_name." in (".implode(",",$cp_no).") ";
				}
			}
		}
	}else{
		switch($cp_partner){
			case 'provider':
				$sql = "SELECT * FROM comics_".$cp_partner." WHERE cp_name like '%$cp_name%' ";
				array_push($partner_arr, 'cm_provider', 'cm_provider_sub'); // 제공사(CP)
			break;
			case 'professional':
				$sql = "SELECT * FROM comics_".$cp_partner." WHERE cp_name like '%$cp_name%' ";
				array_push($partner_arr, 'cm_professional', 'cm_professional_sub', 'cm_professional_thi'); // 작가
			break;
			case 'publisher':
				$sql = "SELECT * FROM comics_".$cp_partner." WHERE cp_name like '%$cp_name%' ";
				array_push($partner_arr, 'cm_publisher'); // 출판사
			break;
		}
		$sql_result = sql_query($sql);
		$sql_num_rows = sql_num_rows($sql_result);
		
		$cp_no = array(); // 초기화
		if($sql_num_rows > 0){
			while ($sql_rows = sql_fetch_array($sql_result)) {
				array_push($cp_no, $sql_rows['cp_no']);
			}
			// Comics cp SQL문
			$sql_cp = " ( ";
			foreach($partner_arr as $partner_name){
				if( strpos($partner_name, '_sub') > 0 || strpos($partner_name, '_thi') > 0 ){ $sql_cp.= " OR "; }
				$sql_cp.= $partner_name." in (".implode(",",$cp_no).") ";
			}
			$sql_cp.= " ) ";
		}		
	}

	return $sql_cp;
}

function get_cm_no_arr($sql_where)
{ 
	$cm_no_arr = array();
	if($sql_where !='' ){
		$sql = "SELECT * FROM comics where 1 AND ".$sql_where;
		$result = sql_query($sql);
		$num_rows = sql_num_rows($result);
		if($num_rows > 0){
			while ($rows = sql_fetch_array($result)) {
				array_push($cm_no_arr, $rows['cm_no']);
			}
		}
	}
	return $cm_no_arr;
}

/*
// 전체+요일선택
//메모리때문에... 주석처리 나중에 요일 계산할때 사용 하면 됩니다.
function get_comics_cycle($_menu)
{	// 전체 월1 화2 수3 목4 금5 토6 일7
    global $nm_config;
	$cmserial_lnb = $nm_config['cf_public_cycle'];

	// 일주일만 적용
	foreach($cmserial_lnb as $cmserial_lnb_key => $cmserial_lnb_val){
		if($cmserial_lnb_val[0] < 1 || $cmserial_lnb_val[0] > 64){
			unset($cmserial_lnb[$cmserial_lnb_key]);
		}
	}
	// 요일 확인
	// print_r($cmserial_lnb);

	$cmserial_lnb_arr = $cmserial_lnb; // 일주일만 적용 복사

	$cmserial_lnb_sort = array();
	$cmserial_cycle_val = $cmserial_lnb[$_menu][0];
	$cmserial_cycle_arr = array($cmserial_cycle_val);
	unset($cmserial_lnb_arr[$_menu]); // 일주일에서 해당요일 제외
	unset($cmserial_lnb_arr[0]); // 전체 제외

	// 키번호때문에 배열 재배열
	foreach($cmserial_lnb_arr as $cmserial_lnb_key => $cmserial_lnb_val){ 
		array_push($cmserial_lnb_sort, $cmserial_lnb_val[0]);
	}

	// print_r($cmserial_lnb_sort);
	for($i=0; $i<count($cmserial_lnb_sort); $i++){
		// 해당요일+각요일
						//die;
		array_push($cmserial_cycle_arr, $cmserial_cycle_val+$cmserial_lnb_sort[$i]);
		for($j=0; $j<count($cmserial_lnb_sort); $j++){
			// 해당요일+각요일+각요일
			if($cmserial_lnb_sort[$i] == $cmserial_lnb_sort[$j]){ continue; }
			if($cmserial_lnb_sort[$i] == $cmserial_cycle_val){ continue; }
			if($cmserial_lnb_sort[$j] == $cmserial_cycle_val){ continue; }
			array_push($cmserial_cycle_arr, $cmserial_cycle_val+$cmserial_lnb_sort[$i]+$cmserial_lnb_sort[$j]);
			
			for($x=0; $x<count($cmserial_lnb_sort); $x++){
				// 해당요일+각요일+각요일+각요일
				if($cmserial_lnb_sort[$i] == $cmserial_lnb_sort[$j]){ continue; }
				if($cmserial_lnb_sort[$i] == $cmserial_lnb_sort[$x]){ continue; }

				if($cmserial_lnb_sort[$j] == $cmserial_lnb_sort[$x]){ continue; }

				if($cmserial_lnb_sort[$i] == $cmserial_cycle_val){ continue; }
				if($cmserial_lnb_sort[$j] == $cmserial_cycle_val){ continue; }
				if($cmserial_lnb_sort[$x] == $cmserial_cycle_val){ continue; }
				//array_push($cmserial_cycle_arr, $cmserial_cycle_val+$cmserial_lnb_sort[$i]+$cmserial_lnb_sort[$j]+$cmserial_lnb_sort[$x]);
				
				for($v=0; $v<count($cmserial_lnb_sort); $v++){
					// 해당요일+각요일+각요일+각요일+각요일
					if($cmserial_lnb_sort[$i] == $cmserial_lnb_sort[$j]){ continue; }
					if($cmserial_lnb_sort[$i] == $cmserial_lnb_sort[$x]){ continue; }
					if($cmserial_lnb_sort[$i] == $cmserial_lnb_sort[$v]){ continue; }

					if($cmserial_lnb_sort[$j] == $cmserial_lnb_sort[$x]){ continue; }
					if($cmserial_lnb_sort[$j] == $cmserial_lnb_sort[$v]){ continue; }

					if($cmserial_lnb_sort[$x] == $cmserial_lnb_sort[$v]){ continue; }

					if($cmserial_lnb_sort[$i] == $cmserial_cycle_val){ continue; }
					if($cmserial_lnb_sort[$j] == $cmserial_cycle_val){ continue; }
					if($cmserial_lnb_sort[$x] == $cmserial_cycle_val){ continue; }
					if($cmserial_lnb_sort[$v] == $cmserial_cycle_val){ continue; }
					 array_push($cmserial_cycle_arr, $cmserial_cycle_val+$cmserial_lnb_sort[$i]+$cmserial_lnb_sort[$j]+$cmserial_lnb_sort[$x]+$cmserial_lnb_sort[$v]);

					for($z=0; $z<count($cmserial_lnb_sort); $z++){
						// 해당요일+각요일+각요일+각요일+각요일+각요일
						if($cmserial_lnb_sort[$i] == $cmserial_lnb_sort[$j]){ continue; }
						if($cmserial_lnb_sort[$i] == $cmserial_lnb_sort[$x]){ continue; }
						if($cmserial_lnb_sort[$i] == $cmserial_lnb_sort[$v]){ continue; }
						if($cmserial_lnb_sort[$i] == $cmserial_lnb_sort[$z]){ continue; }

						if($cmserial_lnb_sort[$j] == $cmserial_lnb_sort[$x]){ continue; }
						if($cmserial_lnb_sort[$j] == $cmserial_lnb_sort[$v]){ continue; }
						if($cmserial_lnb_sort[$j] == $cmserial_lnb_sort[$z]){ continue; }

						if($cmserial_lnb_sort[$x] == $cmserial_lnb_sort[$v]){ continue; }
						if($cmserial_lnb_sort[$x] == $cmserial_lnb_sort[$z]){ continue; }
						
						if($cmserial_lnb_sort[$v] == $cmserial_lnb_sort[$z]){ continue; }

						if($cmserial_lnb_sort[$i] == $cmserial_cycle_val){ continue; }
						if($cmserial_lnb_sort[$j] == $cmserial_cycle_val){ continue; }
						if($cmserial_lnb_sort[$x] == $cmserial_cycle_val){ continue; }
						if($cmserial_lnb_sort[$v] == $cmserial_cycle_val){ continue; }
						if($cmserial_lnb_sort[$z] == $cmserial_cycle_val){ continue; }
						array_push($cmserial_cycle_arr, $cmserial_cycle_val+$cmserial_lnb_sort[$i]+$cmserial_lnb_sort[$j]+$cmserial_lnb_sort[$x]+$cmserial_lnb_sort[$v]+$cmserial_lnb_sort[$z]);
					}
				}
				
			}
		}		
	}

	// 해당요일+전체요일
	array_push($cmserial_cycle_arr, array_sum($cmserial_lnb_arr)+$cmserial_cycle_val);
	$cmserial_cycle_in_arr = array_unique($cmserial_cycle_arr);
	
	natcasesort($cmserial_cycle_in_arr);

	return implode(", ",$cmserial_cycle_in_arr);
}

*/
// 전체+요일선택
function get_comics_cycle_fix($_menu)
{	// 전체 월1 화2 수3 목4 금5 토6 일7 
	// function get_comics_cycle($_menu) 함수 결과값 저장
	
	$cmserial_cycle_arr = "";

	switch($_menu){
		case '1': $cmserial_cycle_arr = "1, 3, 5, 7, 9, 11, 13, 17, 19, 21, 25, 31, 33, 35, 37, 41, 47, 49, 55, 59, 61, 63, 65, 67, 69, 73, 79, 81, 87, 91, 93, 95, 97, 103, 107, 109, 111, 115, 117, 119, 121, 123, 125";
		break;
		case '2': $cmserial_cycle_arr = "2, 3, 6, 7, 10, 11, 14, 18, 19, 22, 26, 31, 34, 35, 38, 42, 47, 50, 55, 59, 62, 63, 66, 67, 70, 74, 79, 82, 87, 91, 94, 95, 98, 103, 107, 110, 111, 115, 118, 119, 122, 123, 126";
		break;
		case '3': $cmserial_cycle_arr = "4, 5, 6, 7, 12, 13, 14, 20, 21, 22, 28, 31, 36, 37, 38, 44, 47, 52, 55, 61, 62, 63, 68, 69, 70, 76, 79, 84, 87, 93, 94, 95, 100, 103, 109, 110, 111, 117, 118, 119, 124, 125, 126";
		break;
		case '4': $cmserial_cycle_arr = "8, 9, 10, 11, 12, 13, 14, 24, 25, 26, 28, 31, 40, 41, 42, 44, 47, 56, 59, 61, 62, 63, 72, 73, 74, 76, 79, 88, 91, 93, 94, 95, 104, 107, 109, 110, 111, 121, 122, 123, 124, 125, 126";
		break;
		case '5': $cmserial_cycle_arr = "16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 28, 31, 48, 49, 50, 52, 55, 56, 59, 61, 62, 63, 80, 81, 82, 84, 87, 88, 91, 93, 94, 95, 112, 115, 117, 118, 119, 121, 122, 123, 124, 125, 126";
		break;
		case '6': $cmserial_cycle_arr = "32, 33, 34, 35, 36, 37, 38, 40, 41, 42, 44, 47, 48, 49, 50, 52, 55, 56, 59, 61, 62, 63, 96, 97, 98, 100, 103, 104, 107, 109, 110, 111, 112, 115, 117, 118, 119, 121, 122, 123, 124, 125, 126";
		break;
		case '7': $cmserial_cycle_arr = "64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 76, 79, 80, 81, 82, 84, 87, 88, 91, 93, 94, 95, 96, 97, 98, 100, 103, 104, 107, 109, 110, 111, 112, 115, 117, 118, 119, 121, 122, 123, 124, 125, 126";
		break;
	}
	return $cmserial_cycle_arr;
}

/* 요일값 요일로 배열 */
function get_comics_cycle_val($cm_public_cycle, $txt='n')
{
	global $nm_config;

	$public_cycle_check_val = $cm_public_cycle;
	$public_cycle_arr = array();
	$public_cycle_krsort = $nm_config['cf_public_cycle']; /* 키역순 임시 배열 */
	krsort($public_cycle_krsort);
	foreach($public_cycle_krsort as $public_cycle_krsort_key => $public_cycle_krsort_val){ 
		if($public_cycle_check_val-$public_cycle_krsort_val[0] >= 0 && $public_cycle_check_val >= 0){
			if($txt == 'y'){
				array_push($public_cycle_arr, $public_cycle_krsort_val[1]); // 한글
			}else{
				array_push($public_cycle_arr, $public_cycle_krsort_val[0]); // 해당값
			}
			$public_cycle_check_val-=$public_cycle_krsort_val[0];
			if($public_cycle_check_val == 0){break;}
		}
	}
	return $public_cycle_arr;
}

function get_comics_url($comics)
{
	$get_comics_url = NM_URL."/comics.php?comics=".$comics;
	return $get_comics_url;
}

function get_episode_url($comics, $episode)
{
	$get_episode_url = NM_URL."/comicsview.php?comics=".$comics."&episode=".$episode;
	return $get_episode_url;
}

function get_date_type($date_type, $date_start, $date_end, $hour_start='00', $hour_end='23')
{
	$date_arr['state'] = '';
	$date_arr['date_start'] = $date_start;
	$date_arr['date_end'] = $date_end;

	$date_arr['date_time_start'] = $date_arr['date_start']." ".$hour_start.":00:00";
	$date_arr['date_time_end'] = $date_arr['date_end']." ".$hour_end.":59:59";

	if($date_type == 'n'){ 
		// 무기간시 날짜 삭제
		$date_arr['state'] = 'y'; /* 진행중 */
		$date_arr['date_start'] = '';
		$date_arr['date_end']= '';
	}else if(($date_type == 'm' || $date_type == 'w') && $date_start = '' && $date_end = ''){
		// 매월 OR 매주 => 무기간
		$date_arr['state'] = 'y'; /* 진행중 */
		$date_arr['date_start'] = '';
		$date_arr['date_end']= '';
	}else if(($date_type == 'm' || $date_type == 'w') && $date_start != '' && $date_end != ''){
		// 매월 OR 매주 => 기간시
		if(NM_TIME_YMDHIS < $date_arr['date_time_start']){
			$date_arr['state'] = 'r'; /* 예약중 */
		}else if(NM_TIME_YMDHIS > 	$date_arr['date_time_end']){
			$date_arr['state'] = 'n'; /* 종료 */
		}else{
			$date_arr['state'] = 'y'; /* 진행중 */
		}
	}else{
		if(NM_TIME_YMDHIS < $date_arr['date_time_start']){
			$date_arr['state'] = 'r'; /* 예약중 */
		}else if(NM_TIME_YMDHIS > 	$date_arr['date_time_end']){
			$date_arr['state'] = 'n'; /* 종료 */
		}else{
			$date_arr['state'] = 'y'; /* 진행중 */
		}
	}
	return $date_arr;
}

function text_change($text, $char)
{   // 한글
	$text_change = $text;
	if(mb_strrpos($text, $char, 0, 'utf-8') > 0){
		$text_strlen = mb_strlen($text, 'utf-8'); // 총길이	
		$text_strrpos = mb_strrpos($text, $char, 0, 'utf-8')+1;	 // 찾고자 하는 문자열 위치
		$text_change = mb_substr($text, $text_strrpos, $text_strlen, 'utf-8'); // 문자열 자르기

		if($text_strlen == $text_strrpos && $char == ']'){
			$char_s = '[';
			$text_strlen = mb_strlen($text, 'utf-8'); // 총길이	
			$text_strrpos = mb_strrpos($text, $char_s, 0, 'utf-8')-1;	 // 찾고자 하는 문자열 위치
			$text_change = mb_substr($text, 0, $text_strrpos, 'utf-8'); // 문자열 자르기
		}
	}
	return $text_change;
}

function date_year_month($_s_date, $_e_date, $field, $_sr_hour='', $_er_hour='')
{
	$_s_date_y_m = substr($_s_date,0,7);
	$_e_date_y_m = substr($_e_date,0,7);
	$_s_date_d = substr($_s_date,8,2);
	$_e_date_d = substr($_e_date,8,2);

	$date_and_or = "OR";
	if($_s_date_y_m == $_e_date_y_m){ $date_and_or = "AND"; }
	
	// 시간
	$sql_sr_hour = $sql_er_hour = "";
	if($_sr_hour != ''){
		$sql_sr_hour = " AND sl_hour >= '$_sr_hour' ";
	}
	if($_er_hour != ''){
		$sql_er_hour = " AND sl_hour <= '$_er_hour' ";
	}

	$where_date = "AND 
					(
						(".$field."_year_month >  '$_s_date_y_m' AND ".$field."_year_month <  '$_e_date_y_m' ) 
						OR  ( 
								(".$field."_year_month =  '$_s_date_y_m' AND ".$field."_day >=  '$_s_date_d' $sql_sr_hour ) 
								$date_and_or (".$field."_year_month =  '$_e_date_y_m' AND ".$field."_day <=  '$_e_date_d' $sql_er_hour ) 
							)
					) ";

	return $where_date;
}

function date_year($_s_month, $_e_month, $field)
{
	if ($_s_month && $_e_month) {
		$where_date = "AND ( ".$field."_year_month >=  '$_s_month' AND ".$field."_year_month <=  '$_e_month' ) ";
	} else if ($_s_month) {
		$where_date = "AND ( ".$field."_year_month >= '".$_s_month."' ) ";
	} else if ($_e_month) {
		$where_date = "AND ( ".$field."_year_month <= '".$_e_month."' ) ";
	}  
	
	return $where_date;
}

function calendar($_s_date, $_e_date, $cs_calendar_on){
	echo '
		<div class="cs_calendar '.$cs_calendar_on.'">
			<label for="s_date">시작일</label>
			<input type="text" name="s_date" value="'.$_s_date.'" id="s_date" class="d_date readonly" readonly /> 
			<label for="s_date_view">일</label>
			<label for="date_"> / </label>
			<label for="e_date">종료일</label>
			<input type="text" name="e_date" value="'.$_e_date.'" id="e_date" class="d_date readonly" readonly />
			<label for="e_date_view">일</label>
			<div class="cs_calendar_btn">
				<a href="#이주전" onclick="a_click_false();" data-fr_date="'.NM_TIME_M14.'" data-to_date="'.NM_TIME_YMD.'">이주전</a>
				<a href="#지난달" onclick="a_click_false();" data-fr_date="'.NM_TIME_MON_M1_S.'" data-to_date="'.NM_TIME_MON_M1_E.'">지난달</a>
				<a href="#이번달" onclick="a_click_false();" data-fr_date="'.NM_TIME_MON_01.'" data-to_date="'.NM_TIME_YMD.'">이번달</a>
				<a href="#최근2개월" onclick="a_click_false();" data-fr_date="'.NM_TIME_MON_M2_S.'" data-to_date="'.NM_TIME_YMD.'">최근2개월</a>
				<a href="#최근3개월" onclick="a_click_false();" data-fr_date="'.NM_TIME_MON_M3_S.'" data-to_date="'.NM_TIME_YMD.'">최근3개월</a>
			</div>
		</div>
	';
}

function calendar_month($_s_month, $_e_month, $cs_calendar_on){
		 // /_pc_js/jquery.autocomplete-ui.min.js 같은 소스입니다.
	echo '
		<link rel="stylesheet" type="text/css" href="'.NM_URL.'/css/monthpicker.min.css'.vs_config().'"/>
		<style type="text/css">.ui-widget .ui-widget { font-size: 0.7em!important; }</style>
		<script type="text/javascript" src="'.NM_URL.'/js/jquery-ui.min.js'.vs_config().'"></script>
		<script type="text/javascript" src="'.NM_URL.'/js/jquery.maskedinput.min.js'.vs_config().'"></script>
		<script type="text/javascript" src="'.NM_URL.'/js/monthpicker.min.js'.vs_config().'"></script>
		<script type="text/javascript">
		<!--
			$(document).ready(function() {
				$("#s_month").MonthPicker({ Button: false,  });
				$("#e_month").MonthPicker({ Button: false,  });
				

				/* 날짜버튼 */
				$(".cs_calendar_month .cs_calendar_btn a").click(function(e){
					var fr_date = $(this).attr("data-fr_date");
					var to_date = $(this).attr("data-to_date");
					$("#s_month").val(fr_date);
					$("#e_month").val(to_date);
				});
			});
		//-->
		</script>
		<div class="cs_calendar_month '.$cs_calendar_on.'">
			<label for="s_month">시작월</label>
			<input type="text" name="s_month" value="'.$_s_month.'" id="s_month" class="d_date readonly" readonly /> 
			<label for="s_month_view">월</label>
			<label for="date_"> / </label>
			<label for="e_month">종료월</label>
			<input type="text" name="e_month" value="'.$_e_month.'" id="e_month" class="d_date readonly" readonly />
			<label for="e_month_view">월</label>
			<div class="cs_calendar_btn">
				<a href="#재작년" onclick="a_click_false();" data-fr_date="'.NM_TIME_YEAR_M2_01.'" data-to_date="'.NM_TIME_YEAR_M2_12.'">재작년</a>
				<a href="#작년" onclick="a_click_false();" data-fr_date="'.NM_TIME_YEAR_M1_01.'" data-to_date="'.NM_TIME_YEAR_M1_12.'">작년</a>
				<a href="#올해" onclick="a_click_false();" data-fr_date="'.NM_TIME_YEAR_01.'" data-to_date="'.NM_TIME_YM.'">올해</a>
				<a href="#최근2년" onclick="a_click_false();" data-fr_date="'.NM_TIME_YEAR_M1_01.'" data-to_date="'.NM_TIME_YM.'">최근2년</a>
				<a href="#최근3년" onclick="a_click_false();" data-fr_date="'.NM_TIME_YEAR_M2_01.'" data-to_date="'.NM_TIME_YM.'">최근3년</a>
			</div>
		</div>
	';
}

function random_string($type = 'alnum', $len = 8)
{
	switch($type)
	{
		case 'basic'	: return mt_rand();
			break;
		case 'alnum'	:
		case 'numeric'	:
		case 'nozero'	:
		case 'alpha'	:

		switch ($type)
		{
			case 'alpha'	:	$pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				break;
			case 'alnum'	:	$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				break;
			case 'numeric'	:	$pool = '0123456789';
				break;
			case 'nozero'	:	$pool = '123456789';
				break;
		}

		$str = '';
		for ($i=0; $i < $len; $i++)
		{
			$str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
		}
		return $str;
	break;
	}
} // end random_string

/* 181001 */
function get_redirect($url){
	$url_bool = true;
	$url_mod = $val_replace = $val_match = '';
	if($url != ''){
		// 다른 사이트 일 경우
		if(strpos($url, '://') > 0){
			if(strpos($url, NM_DOMAIN_ONLY) < 1){
				$url_mod = '';
				$url_bool = false;
			}
		}
		
		// 같은 사이트 및 경로일 경우
		if($url_bool == true){
			$url_tmp = strtolower($url);
			$val_replace = str_replace(NM_URL, "", $url_tmp);
			
			$pattern = '/([a-zA-Z0-9]|[-_@!#$%^*()+=&.:\/?])+/';
			preg_match_all($pattern, $val_replace, $val_replace_arr);
			$val_match = implode('', $val_replace_arr[0]);
			$url_mod = $val_match;		
		}
	}

	// 앞에 url + 도메인까지 삭제
	return $url_mod;
}

/* 181001 */
function goto_redirect($url, $go=0){
	$goto_redirect = $get_urldecode_redirect = '';

	$get_redirect = get_redirect($url);
	if($get_redirect == '' || $url == ''){
		$goto_redirect = NM_URL;		
	}else{
		$get_urldecode_redirect = urldecode($get_redirect);
		if(substr($get_urldecode_redirect, 0, 1) != '/'){
			$get_urldecode_redirect = '/'.$get_urldecode_redirect;
		}
		$goto_redirect = NM_URL.$get_urldecode_redirect;
	}
	//&대신해서 __nn__ 사용변형
	$goto_redirect_v1 = mod_redirect_paramiter($goto_redirect);

	if($go==0){
		return($goto_redirect_v1);
	}else{
		goto_url($goto_redirect_v1);
	}
}

function goto_redirect_cookie($go=0){
	$goto_redirect_cookie = "";
	if($go==0){
		$goto_redirect_cookie = goto_redirect(get_js_cookie("redirect"), $go);
	}else{
		$goto_redirect_cookie = NM_URL;
	}
	return($goto_redirect_cookie);
}

function mod_redirect_paramiter($url){
	$url_mod = str_replace("__nn__", "&", $url);
	return($url_mod);
}

/* 181022 */
function set_redirect_cookie(){
	global $_redirect;

	del_js_cookie("redirect");
	if($_redirect !=''){
		set_js_cookie("redirect", urldecode($_redirect));
	}
	get_js_cookie("redirect");
}

/* 181203 */
function get_add_para_redirect(){
	global $_redirect;
	return("redirect=".$_redirect);
}

/* 181203 */
function set_add_para_redirect($page='now'){
	$set_add_para_redirect = "";

	if($page == 'now'){
		$set_add_para_redirect = get_redirect(urlencode($_SERVER['REQUEST_URI']));
	}else if($page == 'before'){
		$set_add_para_redirect = get_redirect(urlencode($_SERVER['HTTP_REFERER']));
	}else{
		$set_add_para_redirect = get_redirect(urlencode($page));
	}
	return("redirect=".$set_add_para_redirect);
}

?>