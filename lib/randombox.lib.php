<?
if (!defined('_NMPAGE_')) exit;

function randombox_member_get($mb, $er_no) {
	$sql_select = "SELECT * FROM event_randombox_member WHERE erm_member=".$mb['mb_no']." AND erm_member_idx='".$mb['mb_idx']."' AND erm_er_no='".$er_no."'";
	$erm_member_row = sql_fetch($sql_select);
	return $erm_member_row;
}

function randombox_member_set($mb) {
	// 진행중인 이벤트 검색
	$sql_select = "SELECT * FROM event_randombox WHERE er_state='y' AND er_event_type='r'"; // 180227 이벤트 타입 추가
	$select_result = sql_query($sql_select);
	$select_size = sql_num_rows($select_result);
	
	// 진행중인 이벤트가 존재할 때
	if($select_size > 0) {
		while($er_row = sql_fetch_array($select_result)) {
			$sql_insert = "INSERT INTO event_randombox_member(erm_er_no, erm_member, erm_member_ndx, erm_member_idx, erm_reg_date) VALUES('".$er_row['er_no']."', '".$mb['mb_no']."', '".$mb['mb_ndx']."', '".$mb['mb_idx']."', '".NM_TIME_YMD."')";

			$sql_update = "UPDATE event_randombox_member SET erm_pop_count=20, erm_mod_date='".NM_TIME_YMD."' WHERE erm_member=".$mb['mb_no']." AND erm_er_no=".$er_row['er_no'];

			// 저장
			@sql_query($sql_insert); // UNIQUE INDEX로 동일한 데이터는 에러로 저장이 안됨
			sql_query($sql_update);
		} // end while
	} // end if
} 

function randombox_member_push($mb, $comics_buy, $er_no) {
	$erm_member_row = randombox_member_get($mb, $er_no);
	
	// event_randombox_member에 등록이 안 되어 있으면 등록
	if($erm_member_row['erm_member'] == "") {
		randombox_member_set($mb);
		$erm_member_row = randombox_member_get($mb, $er_no);
	} // end if

	$sql_update = "UPDATE event_randombox_member SET erm_cash_point_push=erm_cash_point_push+".$comics_buy['mb_cash_point'].", erm_point_push=erm_point_push+".$comics_buy['mb_point']." WHERE erm_member=".$mb['mb_no']." AND erm_member_idx='".$mb['mb_idx']."' AND erm_er_no='".$erm_member_row['erm_er_no']."'";

	sql_query($sql_update);
} 

function randombox_member_pop($random_mb) {
	$sql_update = "UPDATE event_randombox_member SET erm_cash_point_pop = erm_cash_point_pop + 10, erm_pop_count = erm_pop_count - 1 WHERE erm_member = '".$random_mb['erm_member']."' AND erm_member_idx = '".$random_mb['erm_member_idx']."' AND erm_er_no = '".$random_mb['erm_er_no']."'";
	$sql_result = sql_query($sql_update);
	return $sql_result;
} 

function randombox_member_coupon_get($random_mb, $coupon_arr) {
	$sql_insert = "INSERT INTO event_randombox_coupon(erc_er_no, erc_ercm_no, erc_member, erc_member_ndx, erc_member_idx, erc_discount_rate, erc_er_type, erc_state, erc_date) VALUES('".$coupon_arr['er_no']."', '".$coupon_arr['ercm_no']."', '".$random_mb['erm_member']."', '".$random_mb['erm_member_ndx']."', '".$random_mb['erm_member_idx']."', '".$coupon_arr['ercm_discount_rate']."', '".$coupon_arr['er_type']."', '".$coupon_arr['er_available_state']."', '".NM_TIME_YMDHIS."')";
	if(sql_query($sql_insert)) {
		$sql_select = "SELECT MAX(erc_no) coupon_no FROM event_randombox_coupon WHERE erc_er_no='".$coupon_arr['er_no']."' AND erc_member='".$random_mb['erm_member']."' AND erc_date>='".NM_TIME_YMD."' AND erc_discount_rate='".$coupon_arr['ercm_discount_rate']."'";

		$sql_select_result = sql_fetch($sql_select);
		$sql_result = $sql_select_result['coupon_no'];
	} // end if
	return $sql_result;
}

function randombox_member_coupon_get_first($random_mb, $coupon_arr) {
	
	// 가장 먼저 테이블을 잠근다
	$sql_result = false;

	/* 회원이 뽑았는지 확인 */
	$pop_sql = "SELECT COUNT(*) AS pop_cnt FROM event_randombox_coupon WHERE erc_er_no='".$coupon_arr['er_no']."' AND erc_member='".$nm_member['mb_no']."'";
	$member_pop = sql_count($pop_sql, "pop_cnt");

	/* 총 몇장 뽑았는지 확인 */
	$total_sql = "SELECT COUNT(*) AS total_cnt FROM event_randombox_coupon WHERE erc_er_no='".$coupon_arr['er_no']."'";
	$pop_count = sql_count($total_sql, "total_cnt");

	/* 이벤트 정보 */
	$first_event_sql = "SELECT * FROM event_randombox WHERE er_no='".$coupon_arr['er_no']."'";
	$first_row = sql_fetch($first_event_sql);

	if($member_pop == 0 && $pop_count < $first_row['er_first_count']) {
		/* /////////////////////////////////////////////////////////////
			LOCK은 조금 더 참조후에 적용 http://blog.saltfactory.net/introduce-mysql-lock/
		*/ /////////////////////////////////////////////////////////////
		// sql_query(" LOCK TABLE event_randombox_coupon WRITE "); 
		$sql_result = randombox_member_coupon_get($random_mb, $coupon_arr);
		// sql_query(" UNLOCK TABLES "); 나중에 테이블을 해제
	} // end if

	return $sql_result;
}

function randombox_coupon_get() {
	$coupon_arr = array();
	$sql_select = "SELECT * FROM event_randombox_couponment WHERE ercm_state='y'";
	$coupon_result = sql_query($sql_select);
	
	while($coupon_row = sql_fetch_array($coupon_result)) {
		array_push($coupon_arr, $coupon_row);
	} // end while

	return $coupon_arr;
}

function randombox_coupon_list_get($er_no) {
	$sql_select = "SELECT er_discount_rate_list FROM event_randombox WHERE er_no=".$er_no;
	$coupon_list_row = sql_fetch($sql_select);
	$coupon_list = explode("||", $coupon_list_row['er_discount_rate_list']);
	return $coupon_list;
}

function randombox_discount_list_get($coupon_list) {
	$discount_list = array();
	
	foreach($coupon_list as $coupon_key => $coupon_val) {
		$sql_select = "SELECT ercm_discount_rate FROM event_randombox_couponment WHERE ercm_no=".$coupon_val;
		$discount_rate = sql_fetch($sql_select);
		array_push($discount_list, $discount_rate['ercm_discount_rate']);
	} // end foreach
	return $discount_list;
}

function randombox_pop_log($random_mb, $coupon_arr) {
	$mb = mb_get_no($random_mb['erm_member']);
	$mb_age_group = mb_get_age_group($mb['mb_birth']);
	$sql_insert = "INSERT INTO z_randombox_pop(z_rp_er_no, z_rp_er_type, z_rp_ercm_no, 
												z_rp_member, z_rp_id, z_rp_idx, z_rp_ndx, z_rp_sex, z_rp_age, 
												z_rp_year, z_rp_month, z_rp_day, 
												z_rp_year_month, z_rp_date)
												VALUES(
												'".$coupon_arr['er_no']."', '".$coupon_arr['er_type']."', '".$coupon_arr['ercm_no']."', '"
												.$mb['mb_no']."', '".$mb['mb_id']."', '".$mb['mb_idx']."', '".$mb['mb_ndx']."', '".$mb['mb_sex']."', '".$mb_age_group."', '"
												.NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', '".NM_TIME_YM."', '".NM_TIME_YMDHIS."')";
	$sql_result = sql_query($sql_insert);
	return $sql_result;
}

function randombox_cs_recharge_list_coupondc_get($nm_member, $mb_coupondc){
	
	// 기초값
	$coupondc_no = intval($mb_coupondc);
	$coupondc_percent = 0;

	// 결과 리턴
	$result_coupondc_arr = array();
	$result_coupondc_arr['coupondc_ck'] =  false;
	$result_coupondc_arr['coupondc_no'] =  $coupondc_no;
	$result_coupondc_arr['coupondc_percent'] =  0;
	$result_coupondc_arr['txt_coupondc_name'] = "";
	$result_coupondc_arr['txt_coupondc_rate'] = "";
	$result_coupondc_arr['txt_coupondc_type'] = "";
	$result_coupondc_arr['txt_coupondc_available_date_start'] = "";
	$result_coupondc_arr['txt_coupondc_available_date_end'] = "";

	// 할인권 정보
	$coupondc_er_arr = array();
	$sql_erc_er_coupondc = "";
	if($coupondc_no > 0){
		// 랜덤박스 이벤트 -> 이벤트 설정
		$sql_er_coupondc = "SELECT * FROM event_randombox WHERE er_available_state ='y' AND er_type='d' ";
		$result_er_coupondc = sql_query($sql_er_coupondc);
		for ($c=0; $row_er_coupondc = sql_fetch_array($result_er_coupondc); $c++) {
			if(intval($row_er_coupondc['er_no']) > 0){
				$row_er_coupondc['er_discount_rate_lists'] = str_replace("||", ",", $row_er_coupondc['er_discount_rate_list']);
				$coupondc_er_arr[$c] = $row_er_coupondc;
			}
		}

		// 랜덤박스 이벤트 -> 이벤트 설정 할인권 유효가 1개라도 있다면...
		if(count($coupondc_er_arr) > 0){
			$sql_erc_er_coupondc = " AND ( ";
			foreach($coupondc_er_arr as $coupondc_er_val){
				// 이벤트 번호 & 할인권 번호 조합은 AND
				$sql_erc_er_coupondc.= " ( ";
				$sql_erc_er_coupondc.= " erc_er_no = '".$coupondc_er_val['er_no']."' ";
				$sql_erc_er_coupondc.= " AND erc_ercm_no in (".$coupondc_er_val['er_discount_rate_lists'].") ";
				$sql_erc_er_coupondc.= ") OR ";
			}
			
			$sql_erc_er_coupondc = substr($sql_erc_er_coupondc,0,strrpos($sql_erc_er_coupondc, "OR"));
			$sql_erc_er_coupondc.= ") ";

			$sql_erc_coupondc = "SELECT * FROM event_randombox_coupon WHERE erc_state ='y' AND erc_use='n' 
																	AND erc_member_ndx='".$nm_member['mb_ndx']."' 
																	AND erc_member_idx='".$nm_member['mb_idx']."' 
																	AND erc_member='".$nm_member['mb_no']."' 
																	AND erc_no='".$coupondc_no."' 
																	".$sql_erc_er_coupondc." ";
		}

		// 랜덤박스 이벤트 -> 회원이 소유하고 있는 할인권
		$row_erc_coupondc = sql_fetch($sql_erc_coupondc);
		if(intval($row_erc_coupondc['erc_no']) > 0 && intval($row_erc_coupondc['erc_ercm_no']) > 0 && intval($row_erc_coupondc['erc_discount_rate']) > 0 ){ 
			if(intval($row_erc_coupondc['erc_discount_rate']) > 0){ // 다시 한번 확인...				
				$result_coupondc_arr['coupondc_ck'] =  true;
				$result_coupondc_arr['coupondc_no'] =  $coupondc_no;
				$result_coupondc_arr['coupondc_percent'] = intval($row_erc_coupondc['erc_discount_rate']);

				// 정보 추가
				$sql_er_coupondc_get = $sql_er_coupondc." AND er_no='".$row_erc_coupondc['erc_er_no']."'";
				$row_er_coupondc_get = sql_fetch($sql_er_coupondc_get);

				$result_coupondc_arr['txt_coupondc_name'] = $row_er_coupondc_get['er_name'];
				$result_coupondc_arr['txt_coupondc_rate'] = $result_coupondc_arr['coupondc_percent']."% 결제 할인쿠폰";
				$result_coupondc_arr['txt_coupondc_type'] = "이벤트";
				$result_coupondc_arr['txt_coupondc_date'] = $row_erc_coupondc['erc_date'];

				$result_coupondc_arr['txt_coupondc_available_date_start'] = $row_er_coupondc_get['er_available_date_start']." ".$row_er_coupondc_get['er_date_start_hour'].":00:00";
				$result_coupondc_arr['txt_coupondc_available_date_end'] = $row_er_coupondc_get['er_available_date_end']." ".$row_er_coupondc_get['er_available_date_end_hour'].":59:59";
			}
		}
	}
	return $result_coupondc_arr;
}

function randombox_state_get($er_event_type = "r") { // 파라미터 추가 180227
	$er_no = "";
	$select_sql = "SELECT MAX(er_no) er_no FROM event_randombox WHERE er_state = 'y' AND er_event_type = '".$er_event_type."'";
	$sql_result = sql_fetch($select_sql);
	if(!is_null($sql_result['er_no'])) {
		$er_no = $sql_result['er_no'];
	} // end if

	return $er_no;
}

function randombox_coupon_use($nm_member, $coupondc, $erc_crp_no='', $erc_payway_no=''){
	
	$sql_erc_crp_no = " NULL ";
	$sql_erc_payway_no = " NULL ";
	
	if($erc_crp_no !=''){ $sql_erc_crp_no = " ".$erc_crp_no." ";	}
	if($erc_payway_no !=''){ $sql_erc_payway_no = " ".$erc_payway_no." ";	}
	
	$sql_erc_coupondc = "UPDATE event_randombox_coupon 
												SET erc_use='y', erc_use_date='".NM_TIME_YMDHIS."', 
														erc_crp_no=".$sql_erc_crp_no.", erc_payway_no=".$sql_erc_payway_no." 
												WHERE erc_state ='y' AND erc_use='n' 
												AND erc_member_idx='".$nm_member['mb_idx']."' 
												AND erc_member='".$nm_member['mb_no']."' 
												AND erc_no='".$coupondc."' ";
	sql_query($sql_erc_coupondc);
}

function randombox_coupon_use_cancel($nm_member, $coupondc, $erc_crp_no='', $erc_payway_no=''){
	
	$sql_erc_crp_no = " NULL ";
	$sql_erc_payway_no = " NULL ";
	
	if($erc_crp_no !=''){ $sql_erc_crp_no = " ".$erc_crp_no." ";	}
	if($erc_payway_no !=''){ $sql_erc_payway_no = " ".$erc_payway_no." ";	}
	
	$sql_erc_coupondc = "UPDATE event_randombox_coupon 
												SET erc_use='n', erc_use_date='', 
														erc_crp_no=NULL, erc_payway_no=NULL 
												WHERE erc_state ='y' AND erc_use='y' 
												AND erc_member_idx='".$nm_member['mb_idx']."' 
												AND erc_member='".$nm_member['mb_no']."' 
												AND erc_no='".$coupondc."' ";
	sql_query($sql_erc_coupondc);
}



function randombox_get_reservation($er_event_type = "r", $er_state = "y") { // 파라미터 추가 180320
	$er_result = array();
	$select_sql = "SELECT *, MAX(er_no) as er_no_max FROM event_randombox WHERE er_state = '".$er_state."' AND er_event_type = '".$er_event_type."'";
	$sql_result = sql_fetch($select_sql);
	if(!is_null($sql_result['er_no'])) {
		$er_result = $sql_result;
	} // end if

	return $er_result;
}

/************** 랜덤 함수 **************/
function randombox_weighted_random($weights) { // 확률 배열을 파라미터로
  $r = mt_rand(1, array_sum($weights)); // 1~확률의 전체 합에서 수 하나를 뽑는다

  for($i=0; $i<count($weights); $i++) { // 0부터 i가 배열의 개수보다 작을때까지 실행
    $r -= $weights[$i]; // 하나 뽑은수
    if($r < 1) {
		return $i;
	} // end if
  }
  return false;
}

/************** 할인권 반환 함수 **************/
function randombox_discount_rate($er_row) {
	$ercm_no = implode(", ", randombox_coupon_list_get($er_row['er_no']));

	$coupon_arr = array(); // 쿠폰 정보 배열
	$weight_arr = array(); // 확률(비율) 배열만 따로
	$random_sql = "SELECT * FROM event_randombox_couponment WHERE ercm_state='y' AND ercm_no IN(".$ercm_no.")";
	$random_result = sql_query($random_sql);

	while($random_row = sql_fetch_array($random_result)) {
		array_push($coupon_arr, $random_row);
		array_push($weight_arr, $random_row['ercm_weight']);
	} // end while

	$random_pick = randombox_weighted_random($weight_arr); // 하나를 뽑는다
	$coupon_arr[$random_pick]['er_no'] = $er_row['er_no']; // 이벤트 번호 추가
	$coupon_arr[$random_pick]['er_available_date_end'] = $er_row['er_available_date_end']; // 유효기간 추가
	$coupon_arr[$random_pick]['er_type'] = $er_row['er_type']; // 이벤트 타입 추가
	$coupon_arr[$random_pick]['er_available_state'] = $er_row['er_available_state']; // 유효상태 추가
	$pick_coupon_arr = $coupon_arr[$random_pick]; // 뽑은 쿠폰 정보를 배열로 저장

	return $pick_coupon_arr;
}
?>