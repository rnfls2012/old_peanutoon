<?
if (!defined('_NMPAGE_')) exit;

// 2018 크리스마트 이벤트

// Admin
function event_christmas_category(){
	$category_list = array('');
	// 경품명(CMS), 현물유무(YN), 미니땅콩, 경품명풀네임, 인당 당첨개수(0:무한)
	array_push($category_list, array('<멜드문> 담요','y',0,'pre_blanket.png','<멜로드라마의문법> 담요',3));
	array_push($category_list, array('<너바7> 담요','y',0,'pre_blanket.png','<너에게 바치는 7일> 담요',3));
	array_push($category_list, array('<럽트> 담요','y',0,'pre_blanket.png','<러브 트레인> 담요',3));
	array_push($category_list, array('<천악> 담요','y',0,'pre_blanket.png','<천사씨와 악마님> 담요',3));
	array_push($category_list, array('<냥호원> 담요','y',0,'pre_blanket.png','<고양이 아가씨와 경호원들> 담요',3)); //5
	array_push($category_list, array('우산','y',0,'pre_umbrella.png','라미 우산',1));
	array_push($category_list, array('무드등','y',0,'pre_lantern.png','라미 무드등',1));
	array_push($category_list, array('머그컵','y',0,'pre_cup.png','땅이콩이 머그컵',1));

	array_push($category_list, array('카페','m',0,'pre_coffee.png','카페 기프티콘 – 스타벅스 아메리카노',4));
	array_push($category_list, array('베이커리','m',0,'pre_parisbagette.png','베이커리 기프티콘 – 파리바게트 딸기듬뿍티라미스',1)); //10
	array_push($category_list, array('영화','m',0,'pre_movie.png','영화 기프티콘 – 메가박스 2인 패키지',1));
	array_push($category_list, array('문화상품권','m',0,'pre_giftcard.png','문화상품권 – 해피머니 1만원권',3));

	array_push($category_list, array('미니땅콩 2' ,'n',2 ,'pre_mpeanut.png','미니땅콩 2개',0));
	array_push($category_list, array('미니땅콩 3','n',3,'pre_mpeanut.png','미니땅콩 3개',0));
	array_push($category_list, array('미니땅콩 10','n',10,'pre_mpeanut.png','미니땅콩 10개',50)); //15
	array_push($category_list, array('미니땅콩 50','n',50,'pre_mpeanut.png','미니땅콩 50개',10));
	array_push($category_list, array('미니땅콩 100','n',100,'pre_mpeanut.png','미니땅콩 100개',5));
	array_push($category_list, array('미니땅콩 300','n',300,'pre_mpeanut.png','미니땅콩 300개',1));

	unset($category_list[0]);

	return $category_list;
}

// 삭제, 수정 버튼 제어
function event_christmas_ctrk_bool($ec_no){
	$bool_txt = "error";
	if(intval($ec_no) > 0){
		$sql = "select count(ec_no) as counts from event_christmas_gift where ec_no=".$ec_no.";";
		$row = sql_fetch($sql);
		if(intval($row['counts']) > 0) {
			$bool_txt = "unable";
			// $counts = intval($row['counts']);
		}else{
			$bool_txt = "enable";
		}
	}

	return $bool_txt;
}

// Client
function event_christmas_state_get(){
	$ec_no = "";
	$sql = "SELECT MAX(ec_no) as ec_no FROM event_christmas WHERE ec_state = 'y' AND ec_date = '".NM_TIME_YMD."'";
	$row = sql_fetch($sql);
	if(intval($row['ec_no']) > 0) {
		$ec_no = $row['ec_no'];
	} // end if

	return $ec_no;
}

function event_christmas_rows_get(){
	$rows = array();
	$sql = "SELECT * FROM event_christmas WHERE ec_state = 'y' AND ec_date = '".NM_TIME_YMD."'";
	$row = sql_fetch($sql);
	if(intval($row['ec_no']) > 0) {
		$rows = $row;
	} // end if

	return $rows;
}


function event_christmas_point_used_set($mb) {
	$ec_no = event_christmas_state_get();
	
	// 진행중인 이벤트가 존재할 때
	if(intval($ec_no) > 0 && intval($mb['mb_no']) > 0) {
		$sql_insert = "INSERT INTO event_christmas_point_used(ecpu_year_month, ecpu_member, ecpu_member_idx, ecpu_reg_date) VALUES('".NM_TIME_YM."', '".$mb['mb_no']."', '".$mb['mb_idx']."', '".NM_TIME_YMDHIS."')";		
		// 저장
		@sql_query($sql_insert); // UNIQUE INDEX로 동일한 데이터는 에러로 저장이 안됨
	} // end if
} 

function event_christmas_point_used_get($mb) {
	$sql = "SELECT * FROM event_christmas_point_used WHERE ecpu_member=".$mb['mb_no']." AND ecpu_member_idx='".$mb['mb_idx']."' AND ecpu_year_month='".NM_TIME_YM."'";
	$row = sql_fetch($sql);
	return $row;
}

function event_christmas_get($ec_no){
	$sql = "SELECT ec_no FROM event_christmas WHERE ec_state = 'y' AND ec_no = '".$ec_no."'";
	$row = sql_fetch($sql);
	return intval($row['ec_no']);
}

function event_christmas_point_used_push($mb, $comics_buy, $ec_no=0) {

	if(intval($ec_no) > 0){
		// 이벤트 번호 확인
		$ec_no_check = event_christmas_get($ec_no);
		if(intval($ec_no_check) > 0){
			// 포인트 테이블 확인
			$rows_ecpu = event_christmas_point_used_get($mb);
			
			// event_randombox_member에 등록이 안 되어 있으면 등록
			if($rows_ecpu['ecpu_member'] == "") {
				event_christmas_point_used_set($mb);
				$rows_ecpu = event_christmas_point_used_get($mb);
			} // end if

			$ecpu_cash_point_push = intval($comics_buy['mb_cash_point']);
			$ecpu_point_push = intval($comics_buy['mb_point']);
			$ecpu_total_point_push = $ecpu_cash_point_push + ($ecpu_point_push / 10);

			$sql_update = "UPDATE event_christmas_point_used 
			SET 
			ecpu_cash_point_push=ecpu_cash_point_push+".$ecpu_cash_point_push.", 
			ecpu_point_push=ecpu_point_push+".$ecpu_point_push.", 
			ecpu_total_point_push=ecpu_total_point_push+".$ecpu_total_point_push.", 
			ecpu_save_day='".NM_TIME_YMD."',
			ecpu_mod_date='".NM_TIME_YMDHIS."' 
			WHERE ecpu_member=".$mb['mb_no']." AND ecpu_member_idx='".$mb['mb_idx']."' 
			AND ecpu_year_month='".NM_TIME_YM."'";

			sql_query($sql_update);
		}else{
			return false; 
		}
	}else{
		return false; 
	}
} 


/////////////////////////////////////////////////////////////////////////////////////////////////////

function event_christmas_point_used_decision($row){
	$data = array();
	$chance = $calc = 0;
	$data['calc'] = $data['chance'] = 0;
	$data['fill'] = 10;
	$data['action'] = 'login';	
	$data['percent'] = 0;

	if(is_array($row)){	
		$data['action'] = 'enable';

		$data['calc'] = intval($row['ecpu_total_point_push']) - (intval($row['ecpu_chance_used']) * 10); // 뽑을 수 있는 땅콩수

		if($data['calc'] >= 10){
			$data['fill'] = 10 - ($data['calc'] % 10); // 뽑을 수 있는 땅콩의 나머지 수
			$data['chance'] = floor($data['calc'] / 10); // 뽑을 수 있는 횟수
			$data['percent'] = 100;
		}else{
			$data['fill'] = 10 - $data['calc'];  // 뽑을 수 있는 땅콩의 나머지 수
			$data['action'] = 'unable';
			$data['percent'] = 100 - ($data['fill'] * 10);
		}
	}
	return $data;
}




////// ajax - event_christmas_catagory_rand //////////////////////////////////////////////////////

function event_christmas_catagory_list_get($ec_no){
	$rows = array();
	$sql = "SELECT * FROM event_christmas_category WHERE ecc_ec_no='".$ec_no."' ORDER BY ecc_no ASC ";
	$result = sql_query($sql);
	$size = sql_num_rows($result);

	if($size > 0) {
		while($row = sql_fetch_array($result)) {
			array_push($rows, $row);
		} // end if
	}

	return $rows;
}


function event_christmas_item_list_catagory_get($ec_no){
	$rows = array();
	$sql = "SELECT eci_ecc_no FROM event_christmas_item WHERE eci_used=0 AND eci_ec_no=".$ec_no." GROUP BY eci_ecc_no ORDER BY eci_ecc_no ASC ";
	$result = sql_query($sql);
	$size = sql_num_rows($result);

	if($size > 0) {
		while($row = sql_fetch_array($result)) {
			array_push($rows, $row['eci_ecc_no']);
		} // end if
	}

	return $rows;

}

function event_christmas_item_get($ec_no, $ecc_no=0){
	$rows = array();
	$sql = "SELECT * FROM event_christmas_item WHERE eci_used=0 AND eci_ec_no='".$ec_no."' AND eci_ecc_no='".$ecc_no."' LIMIT 0,1 ";
	$row = sql_fetch($sql);
	return intval($row['eci_no']);
}

function event_christmas_item_set($ec_no, $ecc_no, $eci_no){
	$sql = "UPDATE event_christmas_item SET eci_used=1, eci_used_date='".NM_TIME_YMDHIS."' 
	WHERE eci_ec_no='".$ec_no."' AND eci_ecc_no='".$ecc_no."' AND eci_no='".$eci_no."' ";
	$row = sql_fetch($sql);
	return intval($row['eci_no']);
}

function event_christmas_item_del($ec_no, $ecc_no, $eci_no){
	$sql = "UPDATE event_christmas_item SET eci_used=0, eci_used_date='' 
	WHERE eci_ec_no='".$ec_no."' AND eci_ecc_no='".$ecc_no."' AND eci_no='".$eci_no."' ";
	$row = sql_fetch($sql);
	return intval($row['eci_no']);
}



function event_weighted_rand($weights) { // 확률 배열을 파라미터로
  $r = mt_rand(1, array_sum($weights)); // 1~확률의 전체 합에서 수 하나를 뽑는다

  for($i=0; $i<count($weights); $i++) { // 0부터 i가 배열의 개수보다 작을때까지 실행
    $r -= $weights[$i]; // 하나 뽑은수
    if($r < 1) {
		return $i;
	} // end if
  }
  return false;
}


function event_christmas_catagory_rand($nm_member){
	$state = 0;

	$row_ec = event_christmas_rows_get();
	$row_ecc_data = event_christmas_catagory_list_get($row_ec['ec_no']);
	$row_eci_ecc_no = event_christmas_item_list_catagory_get($row_ec['ec_no']); // 실제등록된 선물만
	// $row_ecg_member_ecc_select = event_christmas_gift_member_ecc_select_get($nm_member); // 회원이 갖고 있는 현물리스트
	// $row_ecg_member_limit = event_christmas_gift_member_limit_get($nm_member); // 회원이 하루에 가질수 있는 수 제약
	$row_ecg_member_have_bool = event_christmas_gift_member_have_bool($nm_member); // 회원당 가질 상품의 카테고리 판별

	// print_r($row_ecg_member_have_bool);

	// print_r($row_ec);
	// print_r($row_ecc_data);
	// $item_array
	$item_arr = array();   // item
	$weight_arr = array(); // 확률(비율) 배열만 따로
	foreach($row_ecc_data as $row_ecc_val){
		/*
		// 하루에 현물 받는 수 제약
		if($row_ecc_val['ecc_is_goods'] == 'y'){ // 현물
			if($row_ecg_member_limit['is_goods_y'] == true){
				continue;
			}
		}
		if($row_ecc_val['ecc_is_goods'] == 'm'){// 모바일기프트콘
			if($row_ecg_member_limit['is_goods_m'] == true){
				continue;
			}
		}
		// 이중 현물 중복 선물 제외
		if(in_array($row_ecc_val['ecc_select_no'], $row_ecg_member_ecc_select)){
			continue;
		}
		*/
		// echo $row_ecc_val['ecc_select_no']."<br/>";
		// 회원당 가질 상품의 카테고리 판별
		if($row_ecg_member_have_bool[$row_ecc_val['ecc_select_no']] == 1){
			continue;
		}

		// 실제등록된 선물 + 하루당첨개수
		if(in_array($row_ecc_val['ecc_no'], $row_eci_ecc_no)){
			array_push($item_arr,   $row_ecc_val);
			array_push($weight_arr, $row_ecc_val['ecc_weight']);
		}
	}

	// 꽝 가중치값 계산
	$ecc_weight_total = $ec_loss_weight = 0;	
	if(count($weight_arr) > 0){
		$ecc_weight_total = array_sum($weight_arr);	
		$ec_loss_weight = round($ecc_weight_total / 100 * intval($row_ec['ec_loss_percent'])) * 2;
	}else{
		$ec_loss_weight = 100;
	}

	// 꽝 부분 앞에 추가
	array_unshift($item_arr, $row_ec);
	array_unshift($weight_arr, $ec_loss_weight);

	$rand_ecc_no = intval(event_weighted_rand($weight_arr)); // 하나를 뽑는다

	if($rand_ecc_no == 0){ // => 꽝 미니땅콩
		event_christmas_gift_set_loss($nm_member, $row_ec['ec_no'], $row_ec['ec_loss']);
	}else{ // => 선물
		$rand_ecc_data = $item_arr[$rand_ecc_no];
		$ecc_ec_no     = $rand_ecc_data['ecc_ec_no'];
		$ecc_no        = $rand_ecc_data['ecc_no'];
		$eci_no = event_christmas_item_get($ecc_ec_no, $ecc_no);

		event_christmas_item_set($ecc_ec_no, $ecc_no, $eci_no);
		$member_bool = event_christmas_gift_set($nm_member, $eci_no, $rand_ecc_data);

		$state = $eci_no;

		// 저장 실패시...
		if($member_bool < 1){
			event_christmas_item_del($ecc_ec_no, $ecc_no, $eci_no);
			event_christmas_gift_del($nm_member, $ec_no, $ecc_no, $eci_no);
			$state = -1;
		}
		// 검색 없을시
		$member_no = event_christmas_gift_get($nm_member, $ecc_ec_no, $ecc_no, $eci_no);
		if($member_no < 1){
			event_christmas_item_del($ecc_ec_no, $ecc_no, $eci_no);
			event_christmas_gift_del($nm_member, $ec_no, $ecc_no, $eci_no);
			$state = -2;
		}
	}

	return $state;
}

function event_christmas_gift_member_ecc_select_get($nm_member){
	$rows = array();
	if(intval($nm_member['mb_no']) > 0) {
		$sql = "SELECT * FROM event_christmas_gift WHERE 
		ecg_member='".$nm_member['mb_no']."' AND ecg_member_idx='".$nm_member['mb_idx']."' AND 
		ecc_select_no>0 AND ecc_is_goods!='n' GROUP BY ecc_select_no ORDER BY ecc_no ASC;";
		$result = sql_query($sql);
		$size = sql_num_rows($result);

		if($size > 0) {
			while($row = sql_fetch_array($result)) {
				array_push($rows, $row['ecc_select_no']);
			} // end if
		}
	}

	return $rows;
}

function event_christmas_gift_member_limit_get($nm_member){
	$limit_y = 2; // 3개 이상
	$limit_m = 1; // 2개 이상
	$rows_limit = array();
	$rows_limit['is_goods_y'] = false;
	$rows_limit['is_goods_m'] = false;
	if(intval($nm_member['mb_no']) > 0) {
		$sql = "
		SELECT ecg_member, ecg_member_idx, ecg_reg_day, 
		SUM(if(ecc_is_goods='y',1,0))as is_goods_y, 
		SUM(if(ecc_is_goods='m',1,0))as is_goods_m FROM event_christmas_gift 
		WHERE ecg_member='".$nm_member['mb_no']."' AND ecg_member_idx='".$nm_member['mb_idx']."' AND 
		ecg_reg_day='".NM_TIME_YMD."' 
		AND ecc_select_no>0 AND ecc_is_goods!='n' 
		GROUP BY ecg_member ORDER BY ecc_no ASC;";
		$row = sql_fetch($sql);
		if(intval($row['is_goods_y']) > $limit_y){
			$rows_limit['is_goods_y'] = true;
		}
		if(intval($row['is_goods_m']) > $limit_m){
			$rows_limit['is_goods_m'] = true;
		}
	}

	return $rows_limit;

}

function event_christmas_gift_member_have_bool($nm_member){
	
	$ecc_bool = array();
	$ecc_have = array();
	$ecc_pre_blanket = array();
	$event_christmas_category = event_christmas_category();

	$ecc_select_no_arr = array();
	foreach($event_christmas_category as $ecc_key => $ecc_val){
		array_push($ecc_select_no_arr, "SUM(if(ecc_select_no='".$ecc_key."',1,0))as ecc_select_no_".$ecc_key);
		if($ecc_val[3] == 'pre_blanket.png'){
			array_push($ecc_pre_blanket, $ecc_key); // 담요들은 회원의 담요받은수-공유
		}
	}
	$sql = " SELECT ".implode(",",$ecc_select_no_arr)." FROM event_christmas_gift 
			 WHERE ecg_member='".$nm_member['mb_no']."' AND ecg_member_idx='".$nm_member['mb_idx']."'";
	$row = sql_fetch($sql);

	// echo $sql."<br/>";

	foreach($event_christmas_category as $ecc_key => $ecc_val){
		if(in_array($ecc_key,$ecc_pre_blanket)){ // 담요들은 회원의 담요받은수-공유
			foreach($ecc_pre_blanket as $ecc_pre_blanket_val){
				$ecc_have[$ecc_pre_blanket_val]+= $row['ecc_select_no_'.$ecc_key];
			}
		}else{
			$ecc_have[$ecc_key] = $row['ecc_select_no_'.$ecc_key];
		}
	}

	// 각 카테고리마다 판별 => 제약 개수다 = true
	foreach($ecc_have as $ecc_have_key => $ecc_have_val){
		// echo $ecc_have_val."=".$ecc_have_key."->".$event_christmas_category[$ecc_have_key][5]."<br/>";
		
		if(intval($event_christmas_category[$ecc_have_key][5]) == 0){ // 무제한
			$ecc_bool[$ecc_have_key] = 0;
		}else{
			if(intval($ecc_have_val) >= intval($event_christmas_category[$ecc_have_key][5])){
				$ecc_bool[$ecc_have_key] = 1;
			}else{
				$ecc_bool[$ecc_have_key] = 0;
			}
		}
	}
	/*
	print_r($row);
	print_r($event_christmas_category);
	print_r($ecc_have);
	print_r($ecc_bool);	
	echo $sql;
	//die;
	*/

	return $ecc_bool;

}


function event_christmas_gift_get($nm_member, $ec_no, $ecc_no, $eci_no) {	
	$ecg_no = 0;
	if(intval($nm_member['mb_no']) > 0 && intval($ec_no) > 0 && intval($ecc_no) > 0 && intval($eci_no) > 0) {
		$sql = "SELECT * FROM event_christmas_gift WHERE 
		ecg_member='".$nm_member['mb_no']."' AND ecg_member_idx='".$nm_member['mb_idx']."' AND 
		ec_no='".$ec_no."' AND ecc_no='".$ecc_no."' AND eci_no='".$eci_no."'";
		$row = sql_fetch($sql);
		$ecg_no = intval($row['ecg_no']);
	}

	return $ecg_no;
} 
function event_christmas_gift_del($nm_member, $ec_no, $ecc_no, $eci_no) {	
	$sql_bool = 0;
	if(intval($nm_member['mb_no']) > 0 && intval($ec_no) > 0 && intval($ecc_no) > 0 && intval($eci_no) > 0) {
		$sql = "DELETE FROM event_christmas_gift WHERE 
		ecg_member='".$nm_member['mb_no']."' AND ecg_member_idx='".$nm_member['mb_idx']."' AND 
		ec_no='".$ec_no."' AND ecc_no='".$ecc_no."' AND eci_no='".$eci_no."'";
		
		// 저장
		$result = sql_query($sql); // UNIQUE INDEX로 동일한 데이터는 에러로 저장이 안됨
		if ($result){ $sql_bool = 1; } // 쿼리가 정상이면 true.
	}

	return $sql_bool;
} 


function event_christmas_gift_set($nm_member, $eci_no, $rand_ecc_data) {	
	$sql_bool = 0;
	$ec_no         = $rand_ecc_data['ecc_ec_no'];
	$ecc_no        = $rand_ecc_data['ecc_no'];
	$ecc_select_no = $rand_ecc_data['ecc_select_no'];
	$ecc_point     = $rand_ecc_data['ecc_point'];
	$ecc_is_goods  = $rand_ecc_data['ecc_is_goods'];
	$ecg_reg_day   = NM_TIME_YMD;
	$ecg_reg_date  = NM_TIME_YMDHIS;

	if(intval($nm_member['mb_no']) > 0 && intval($ec_no) > 0 && intval($ecc_no) > 0 && intval($eci_no) > 0) {
		$sql = "INSERT INTO event_christmas_gift(
		ecg_member, ecg_member_idx, eci_no,
		ec_no, ecc_no, ecc_select_no, eci_point, ecc_is_goods, 
		ecg_reg_day, ecg_reg_date 
		) VALUES ( 
		'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', '".$eci_no."',  
		'".$ec_no."', '".$ecc_no."', '".$ecc_select_no."', '".$ecc_point."', '".$ecc_is_goods."', 
		'".$ecg_reg_day."', '".$ecg_reg_date."')";
		
		// 저장
		$result = sql_query($sql); // UNIQUE INDEX로 동일한 데이터는 에러로 저장이 안됨
		if ($result){ $sql_bool = 1; } // 쿼리가 정상이면 true.
	}

	return $sql_bool;
} 

function event_christmas_gift_set_loss($nm_member, $ec_no, $ec_loss) {
	if(intval($nm_member['mb_no']) > 0 && intval($ec_no) > 0) {
		$sql = "INSERT INTO event_christmas_gift(
		ecg_member, ecg_member_idx, 
		ec_no, eci_point, ecg_reg_day, ecg_reg_date 
		) VALUES ( 
		'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 
		'".$ec_no."','".$ec_loss."', '".NM_TIME_YMD."', '".NM_TIME_YMDHIS."')";
		
		// 저장
		$result = sql_query($sql); // UNIQUE INDEX로 동일한 데이터는 에러로 저장이 안됨
		if ($result){ $sql_bool = true; } // 쿼리가 정상이면 true.
	}

	return $sql_bool;
} 

////// ajax - ecpu_chance_used 처리 //////////////////////////////////////////////////////

function event_christmas_gift_chance_used($nm_member) {
	$sql_bool = 0;

	$sql = "UPDATE event_christmas_point_used SET 
	ecpu_point_chance_used = ecpu_point_chance_used + 10, 
	ecpu_chance_used = ecpu_chance_used + 1 
	WHERE ecpu_member=".$nm_member['mb_no']." AND ecpu_member_idx='".$nm_member['mb_idx']."' 
	AND ecpu_year_month='".NM_TIME_YM."'";

	// 저장
	$result = sql_query($sql); // UNIQUE INDEX로 동일한 데이터는 에러로 저장이 안됨
	if ($result){ $sql_bool = 1; } // 쿼리가 정상이면 true.

	return $sql_bool;
} 


function event_christmas_item_get_no($eci_no=0){
	$sql = "SELECT * FROM event_christmas_item eci 
	left JOIN event_christmas_category ecc ON eci.eci_ecc_no = ecc.ecc_no 
	WHERE eci.eci_no='".$eci_no."' ";
	$row = sql_fetch($sql);
	return $row;
}


////// delivery 처리 //////////////////////////////////////////////////////


function event_christmas_gift_row_get($nm_member, $ec_no, $ecc_no, $eci_no) {	
	$rows = array();
	if(intval($nm_member['mb_no']) > 0 && intval($ec_no) > 0 && intval($ecc_no) > 0 && intval($eci_no) > 0) {
		$sql = "SELECT * FROM event_christmas_gift WHERE 
		ecg_member='".$nm_member['mb_no']."' AND ecg_member_idx='".$nm_member['mb_idx']."' AND 
		ec_no='".$ec_no."' AND ecc_no='".$ecc_no."' AND eci_no='".$eci_no."'";
		$row = sql_fetch($sql);
		$rows = $row;
	}

	return $rows;
} 


function event_christmas_gift_row_get_is_goods($nm_member) {	
	$rows = array();
	if(intval($nm_member['mb_no']) > 0) {
		$sql = "SELECT * FROM event_christmas_gift WHERE 
		ecg_member='".$nm_member['mb_no']."' AND ecg_member_idx='".$nm_member['mb_idx']."' AND ecc_select_no > 0 and ecc_is_goods!='n'  ORDER BY ecc_select_no ASC ";
		$result = sql_query($sql);
		$size = sql_num_rows($result);

		if($size > 0) {
			while($row = sql_fetch_array($result)) {
				array_push($rows, $row);
			} // end if
		}
	}

	return $rows;
} 


function event_christmas_member_set($nm_member) {	
	$bool = 0;
	if(intval($nm_member['mb_no']) > 0) {
		$sql = "INSERT INTO event_christmas_member(ecm_member, ecm_member_idx, ecm_reg_date) 
		VALUES('".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', '".NM_TIME_YMDHIS."')";	
		@sql_query($sql); // UNIQUE INDEX로 동일한 데이터는 에러로 저장이 안됨
		$bool = 1;
	}
} 


function event_christmas_member_get($nm_member) {	
	$rows = array();
	if(intval($nm_member['mb_no']) > 0) {
		$sql = "SELECT * FROM event_christmas_member WHERE 
		ecm_member='".$nm_member['mb_no']."' AND ecm_member_idx='".$nm_member['mb_idx']."'";
		$row = sql_fetch($sql);
		$rows = $row;
	}

	return $rows;
} 

/* 크리스마스 테이블 비우기
TRUNCATE `event_christmas`;
TRUNCATE `event_christmas_all_sale`;
TRUNCATE `event_christmas_category`;
TRUNCATE `event_christmas_gift`;
TRUNCATE `event_christmas_item`;
TRUNCATE `event_christmas_member`;
TRUNCATE `event_christmas_point_used`;
TRUNCATE `z_event_christmas_point_used`;
TRUNCATE `z_event_christmas_pop`;
*/

?>