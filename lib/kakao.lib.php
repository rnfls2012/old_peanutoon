<?php
if (!defined('_NMPAGE_')) exit;
/**
 * PRIMARY FUNCTION
 * @param string $receiver_name 메세지 받을 고객 이름
 * @param $receiver_numb
 * @param string $sender_numb 업체 페이지 내에 저장된 발신번호
 * @return array
 */
function set_kakao_conf($receiver_name, $receiver_numb, $sender_numb = "01085256619") {

    $result=array();

    // 핸드폰 번호 작업
    if ( gettype($receiver_numb) === "string" ) {
        $receiver_numb = str_replace('-','',filter_var($receiver_numb, FILTER_SANITIZE_NUMBER_INT));
    } else {
        $receiver_numb = strval($receiver_numb);
        $receiver_numb = str_replace('-','',filter_var($receiver_numb, FILTER_SANITIZE_NUMBER_INT));
    }

    $result['kakao_sender']   = $sender_numb;
    $result['kakao_name']     = $receiver_name;
    $result['kakao_phone']    = $receiver_numb;

    return $result;
}

/**
 * SUB FUNCTION
 * @param string $is_res 예약발송인 경우 Y
 * @param string $res_date 예약인 경우에만 필요, 예) 2017-12-24 07:08:09
 * @param string $TRAN_REPLACE_TYPE 알림톡 실패시 대체문자 발송 ( 공백:미발송, S : SMS로 발송, L : LMS로 발송 )
 * @param string $is_080  대체문자발송시 080 무료수신거부를 사용하는 경우 Y
 * @return array
 */
function set_sub_conf($is_res = "", $res_date = "", $TRAN_REPLACE_TYPE = "", $is_080 = "N") {

    $result=array();

    if ( $is_res == "Y" && $res_date == "" ) {
        return NULL;
    } else if ($is_res == "Y") {
        $res_date = date_create($res_date) -> format('Y-m-d H:i:00');
    }

    $result['kakao_res']         = $is_res;
    $result['kakao_res_date']    = $res_date;
    $result['TRAN_REPLACE_TYPE'] = $TRAN_REPLACE_TYPE;
    $result['kakao_080']         = $is_080;

    return $result;
}

/**
 * TEMPLATE & CONTENTS
 * REQUIRE PRIMARY FUNCTION
 * @param int $temple_numb 승인받은 템플릿 고유번호
 * @param array $message_arr 키 작업을 할 배열
 * @return array 키와 값을 담은 배열
 */
function set_message($temple_numb, $message_arr = array()) {

    $message_info = array("");

    try {
        // parameter 크기 비교
        if ( count($message_arr) > 10 ) {
            throw new RuntimeException("Message array overflow ", 452); // 배열크기 10 넘으면 강제 예외 발생.
        } else {
            array_unshift($message_arr, ""); // 배열 전체를 뒤로 shift 후 빈 값 대입

            foreach ($message_arr as $msg_key => $msg_val) {
                ${kakao_add.$msg_key} = $msg_val;
            }

            unset($message_arr[0]); // shift 한 다음 대입한 값 제거.

            $message_info['tmp_number'] = $temple_numb;
            for($i=1; $i<=count($message_arr); $i++) {
                $message_info[kakao_add.$i] = ${kakao_add.$i};
            }
        }
    } catch (RuntimeException $exception) {
        $message_info['user_error'] = $exception -> getCode();
        $message_info['user_error_msg'] = $exception ->getMessage();
    }

    if ( empty($message_info[0]) ) {
        unset($message_info[0]);
    }

    $result = $message_info;

    return $result;
}

/**
 * SEND MESSAGE
 * @param $auth_key -> 개발사 문의 결과 헤더에 직접적인 키 설정해야함 (param삭제)
 * @param array $merged_arr
 * @return mixed
 */
function send_talk ($merged_arr = array()) {

    $headers = array(
    "Content-Type: application/json; charset=utf-8",
    "Authorization: 4+0RXZYD9Aft9cwY9ch2+BFJHHsU64KFOxidkpY0PR4="
    );

    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, "http://www.apiorange.com/api/send/notice.do");
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($merged_arr));

    curl_setopt($curl, CURLOPT_POST, true); // POST 방식으로 정보 보냄
    curl_setopt($curl, CURLOPT_NOSIGNAL, true); // PHP 에서 보내는 신호 무시
    curl_setopt($curl, CURLOPT_FORBID_REUSE, true); // 처리 끝났을 시 프로세스 끝냄, 재사용 X
    curl_setopt($curl, CURLOPT_VERBOSE, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER , true); // 외부 사이트 curl_exec() 값 string 으로 반환
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $response['info'] = curl_exec($curl);
    $response['detail'] = curl_getinfo($curl);
    $response['error'] = curl_error($curl);
    curl_close($curl);

    $obj = json_decode($response['info']);
    $detail = $response['detail'];

    function z_send_talk_insert($obj, $detail) {

        global $nm_member;

        $time       = NM_TIME_YMDHIS;
        $year_month = NM_TIME_YM;
        $day        = NM_TIME_D;
        $hour       = NM_TIME_H;

        $mb_idx     	 = $nm_member['mb_idx'];
        $temple_no  	 = $obj -> {"tmp_number"};
        $error_msg  	 = $obj -> {"response_message"};
        $response_code = $obj -> {"response_code"};

        if ( $response_code !== 200 ) {
            $response_code = 0;
            if ( isset($obj -> {"user_error"}) ) {
                $error_code = $obj->{"user_error"};
                $error_msg  = $obj->{"user_error_msg"};
            } else {
                $error_code = $obj->{"response_code"};
            }
        } else {
            $error_code = 0;
            $error_msg = "";
        }

        $sql_insert_log = "
            INSERT INTO z_send_kakaotalk (
            z_sk_mb_no, z_sk_mb_idx,  
            z_sk_temple_no, z_sk_state_code , z_sk_error_code, z_sk_error_msg, 
            z_sk_date, z_sk_year_month, z_sk_day, z_sk_hour) VALUES (
            '".$nm_member['mb_no']."', '".$mb_idx."', 
            '".$temple_no."', ".$response_code.", '".$error_code."', '".$error_msg."', 
            '".$time."', '".$year_month."', '".$day."', '".$hour."'
            )
            ";
            
        sql_query($sql_insert_log);

        $sql_insert_detail = "
            INSERT INTO kakao_response_detail (
            krd_mb_no, krd_url, krd_content_type, krd_http_code, 
            krd_header_size, krd_request_size, 
            krd_redirect_count, krd_redirect_time, 
            krd_primary_ip, krd_primary_port, krd_local_ip, krd_local_port,
            krd_date
            ) VALUES (
            '".$nm_member['mb_no']."', '".$detail['url']."', '".$detail['content_type']."', '".$detail['http_code']."',
             ".$detail['header_size'].", '".$detail['request_size']."',
            '".$detail['redirect_count']."', '".$detail['redirect_time']."', 
            '".$detail['primary_ip']."', '".$detail['primary_port']."', '".$detail['local_ip']."', '".$detail['local_port']."',
            '".NM_TIME_YMDHIS."'
            )        
            ";

        sql_query($sql_insert_detail);
    }

    z_send_talk_insert($obj, $detail);

    return $response;
}

function select_template($temple_numb){
    $sql_select = "
    SELECT * FROM kakao_template WHERE kt_temp_no = '".$temple_numb."'
    ";

    $result = sql_query($sql_select);

    $row = sql_fetch_array($result);

    return $row;
}