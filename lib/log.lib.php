<?
if (!defined('_NMPAGE_')) exit;

// 쿠폰 사용기록 로그
function log_coupon_use($member, $ecm_row, $ec_row) {
	$member['mb_ndx'] = mb_get_ndx($member['mb_no']);
	$member['mb_age'] = mb_get_age_group($member['mb_birth']);

	$coupon_use_log_sql = "INSERT INTO z_coupon_use (
	z_cu_ecm_no, z_cu_ec_no, z_cu_code, z_cu_member, 
	z_cu_id, z_cu_idx, z_cu_ndx, z_cu_age, 
	z_cu_cash_point, z_cu_point, 
	z_cu_year, z_cu_month, z_cu_day, 
	z_cu_year_month, z_cu_date, z_cu_user_agent) 
	VALUES ('".$ecm_row['ecm_no']."', '".$ec_row['ec_no']."', '".$ec_row['ec_code']."', '".$member['mb_no']."', '".
		$member['mb_id']."', '".$member['mb_idx']."', '".$member['mb_ndx']."', '".$member['mb_age']."', '".
		$ecm_row['ecm_cash_point']."', '".$ecm_row['ecm_point']."', '".
		NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', '".
		NM_TIME_YM."', '".NM_TIME_YMDHIS."', '".HTTP_USER_AGENT."')";

	sql_query($coupon_use_log_sql);
}

// 결제 취소 로그
function log_pg_channel_cancel($nm_member, $pg, $pg_order, $pg_amt, $pg_log, $pg_msg, $pg_state){
	$log_pg_cc_sql = "INSERT INTO z_pg_channel_cancel (
	z_pg_cc_member, z_pg_cc_member_idx,
	z_pg_cc_name, z_pg_cc_order,  z_pg_cc_amt, 
	z_pg_cc_log, z_pg_cc_msg, z_pg_cc_state, 
	z_pg_cc_agent, z_pg_cc_date) VALUES (
	'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 
	'".$pg."', '".$pg_order."',  '".$pg_amt."',  
	'".$pg_log."', '".$pg_msg."', '".$pg_state."',
	'".HTTP_USER_AGENT."', '".NM_TIME_YMDHIS."')";
	sql_query($log_pg_cc_sql);
}

?>