<? if (!defined('_NMPAGE_')) exit;

function vote_check($year)
{
    global $nm_member;
	
	$boolean = false;

	/* 회원인지 확인 */
	if($nm_member['mb_id'] != ''){ 
		/* 회원인지 확인 */
		if(intval($nm_member['mb_no']) > 0){ 
			/* 연도 틀리면 안하기 */
			if(num_check($year) || $year != NM_TIME_Y){ $boolean = true;  }
		}
	}
	return $boolean;
}

function vote_member($year)
{
    global $nm_member;

	/* 투표 확인사항 */
	if(vote_check($year) == false){ return false; }
	
	$sql = "SELECT * FROM event_vote_member WHERE evm_member='".$nm_member['mb_no']."' 
											AND evm_member_idx='".$nm_member['mb_idx']."' 
											AND evm_member_ndx='".$nm_member['mb_ndx']."'";
	$row = sql_fetch($sql);
	return $row;
}

function vote_member_insert($year)
{
    global $nm_member;

	/* 투표 확인사항 */
	if(vote_check($year) == false){ return false; }

	$sql_insert = "	INSERT INTO event_vote_member(evm_member, evm_member_idx, evm_member_ndx) 
					VALUES('".$nm_member['mb_no']."','".$nm_member['mb_idx']."','".$nm_member['mb_ndx']."')";
	@mysql_query($sql_insert); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
}

function vote_member_update($year, $comics, $ev_no)
{
    global $nm_member;

	/* 투표 확인사항 */
	if(vote_check($year) == false){ return false; }

	/* 투표번호 있는지 확인 */
	if($ev_no ==''){ return false; }

	/* 투표정보 가져오기 */
	$row_ev = vote_get($year, $ev_no);

	/* 투표회원정보 가져오기 */
	$row_evm = vote_member($year);

	$sql_update = "	UPDATE event_vote_member SET evm_".$row_ev['ev_evc_no']."_comics = '".$comics."', 
												 evm_".$row_ev['ev_evc_no']."_date = '".NM_TIME_YMD."'  
					WHERE evm_member='".$nm_member['mb_no']."' 
					AND evm_member_idx='".$nm_member['mb_idx']."'
					AND evm_member_ndx='".$nm_member['mb_ndx']."'";

	// 저장한 날짜가 오늘이 아니라면...
	if($row_evm['evm_'.$row_ev['ev_evc_no'].'_date'] != NM_TIME_YMD){
		// echo $sql_update;
		sql_query($sql_update);
		vote_stats_event_vote($year, $comics, $ev_no, 'vote');	// 통계
	}
}

function vote_member_ckeck($year, $evc_no)
{
    global $nm_member;
	
	$boolean = false;

	/* 투표회원정보 가져오기 */
	$row_evm = vote_member($year);

	/* 투표 확인사항 */ /* 투표번호 있는지 확인 */ /* 저장한 날짜가 오늘이 아니라면... */
	if(vote_check($year) && $evc_no !='' && $row_evm['evm_'.$evc_no.'_date'] != NM_TIME_YMD){
		$boolean = true;
	}

	return $boolean;
}


function vote_comics_view_update($year, $comics, $ev_no)
{
    global $nm_member;

	/* 투표 확인사항 */
	if(vote_check($year) == false){ return false; }

	/* 투표번호 있는지 확인 */
	if($ev_no ==''){ return false; }

	/* 투표정보 가져오기 */
	$row_ev = vote_get($year, $ev_no);

	/* 투표회원정보 가져오기 */
	$row_evm = vote_member($year);

	$sql_update = "	UPDATE event_vote_member SET evm_".$row_ev['ev_evc_no']."_comics_view = '".$comics."'  
					WHERE evm_member='".$nm_member['mb_no']."' 
					AND evm_member_idx='".$nm_member['mb_idx']."'
					AND evm_member_ndx='".$nm_member['mb_ndx']."'";

	// echo $sql_update;
	sql_query($sql_update);
}

function vote_get($year, $ev_no)
{
	/* 투표번호 있는지 확인 */
	if($ev_no ==''){ return false; }
	
	$sql = "SELECT * FROM event_vote WHERE ev_no='".$ev_no."'";
	$row = sql_fetch($sql);
	return $row;
}

function vote_z_event_vote_member($year, $comics, $ev_no)
{
    global $nm_member;

	$boolean = false;

	/* 투표 확인사항 */
	if(vote_check($year) == false){ return false; }

	/* 투표번호 있는지 확인 */
	if($ev_no ==''){ return false; }

	$sql_insert = "	INSERT INTO z_event_vote_member(z_evm_ev_no, z_evm_comics, 
													z_evm_member, z_evm_member_idx, 
													z_evm_year_month, z_evm_year, z_evm_month, 
													z_evm_day, z_evm_hour, z_evm_week, z_evm_date) 
					VALUES( '".$ev_no."', '".$comics."', 
							'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 
							'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."',  
							'".NM_TIME_D."', '".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."'
							)";
	if(sql_query($sql_insert)){ 
		$boolean = true; 
	}

	return $boolean;
}

function vote_z_event_vote_view($year, $comics, $ev_no)
{
    global $nm_member;

	$boolean = false;

	/* 투표 확인사항 */
	if(vote_check($year) == false){ return false; }

	/* 투표번호 있는지 확인 */
	if($ev_no ==''){ return false; }

	$sql_insert = "	INSERT INTO z_event_vote_view(z_evv_cm_no, z_evv_comics, 
												  z_evv_member, z_evv_member_idx, 
												  z_evv_year_month, z_evv_year, z_evv_month, 
												  z_evv_day, z_evv_hour, z_evv_week, z_evv_date) 
					VALUES( '".$ev_no."', '".$comics."', 
							'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 
							'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."',  
							'".NM_TIME_D."', '".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."'
							)";
	if(sql_query($sql_insert)){ 
		$boolean = true; 
		vote_stats_event_vote($year, $comics, $ev_no, 'view');	// 통계
	}

	return $boolean;
}

function vote_z_event_vote_pay($year, $comics, $episode, $comics_buy)
{
    global $nm_member;

	$boolean = false;

	/* 투표 확인사항 */
	if(vote_check($year) == false){ return false; }

	/* 투표 회원 정보 가져오고 확인 */
	$vote_member = vote_member($year);
	if($vote_member == false || intval($vote_member['evm_member']) == 0){ return false; }

	/* 투표 정보에 보러가기 코믹스번호 검색 */
	$evm_comics_view_arrs = array();
	array_push($evm_comics_view_arrs, $vote_member['evm_1_comics_view']);
	array_push($evm_comics_view_arrs, $vote_member['evm_2_comics_view']);
	array_push($evm_comics_view_arrs, $vote_member['evm_3_comics_view']);
	array_push($evm_comics_view_arrs, $vote_member['evm_4_comics_view']);

	if(!in_array($comics,$evm_comics_view_arrs)){ return false; }
	
	/* 해당 코믹스의 투표번호 구하기 */
	$sql_ev_no = " SELECT * FROM event_vote WHERE ev_comics='".$comics."'";
	$row_ev_no = sql_fetch($sql_ev_no);
	$ev_no = $row_ev_no['ev_no'];

	$sql_insert = "	INSERT INTO z_event_vote_pay(z_evp_ev_no, z_evp_comics, z_evp_episode, 
													z_evp_cash_point, z_evp_point, 
													z_evp_member, z_evp_member_idx, 
													z_evp_year_month, z_evp_year, z_evp_month, 
													z_evp_day, z_evp_hour, z_evp_week, z_evp_date) 
					VALUES( '".$ev_no."', '".$comics."', '".$episode."', 
							'".$comics_buy['mb_cash_point']."', '".$comics_buy['mb_point']."', 
							'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 
							'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."',  
							'".NM_TIME_D."', '".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."'
							)";
	if(sql_query($sql_insert)){ 
		$boolean = true; 
		vote_stats_event_vote($year, $comics, $ev_no, 'pay', $comics_buy);	// 통계
	}

	return $boolean;
}

function vote_z_event_vote_comicsview($year, $comics, $episode)
{
    global $nm_member;

	$boolean = false;

	/* 투표 확인사항 */
	if(vote_check($year) == false){ return false; }

	/* 투표 회원 정보 가져오고 확인 */
	$vote_member = vote_member($year);
	if($vote_member == false || intval($vote_member['evm_member']) == 0){ return false; }

	/* 투표 정보에 보러가기 코믹스번호 검색 */
	$evm_comics_view_arrs = array();
	array_push($evm_comics_view_arrs, $vote_member['evm_1_comics_view']);
	array_push($evm_comics_view_arrs, $vote_member['evm_2_comics_view']);
	array_push($evm_comics_view_arrs, $vote_member['evm_3_comics_view']);
	array_push($evm_comics_view_arrs, $vote_member['evm_4_comics_view']);

	if(!in_array($comics, $evm_comics_view_arrs)){ return false; }
	/* 해당 코믹스의 투표번호 구하기 */
	$sql_ev_no = " SELECT * FROM event_vote WHERE ev_comics='".$comics."'";
	$row_ev_no = sql_fetch($sql_ev_no);
	$ev_no = $row_ev_no['ev_no'];

	$sql_insert = "	INSERT INTO z_event_vote_comicsview(z_evc_ev_no, z_evc_comics, z_evc_episode, 
													z_evc_member, z_evc_member_idx, 
													z_evc_year_month, z_evc_year, z_evc_month, 
													z_evc_day, z_evc_hour, z_evc_week, z_evc_date) 
					VALUES( '".$ev_no."', '".$comics."', '".$episode."', 
							'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 
							'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."',  
							'".NM_TIME_D."', '".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."'
							)";	
	if(sql_query($sql_insert)){ 
		$boolean = true; 
		vote_stats_event_vote($year, $comics, $ev_no, 'comicsview');	// 통계
	}

	return $boolean;
}

function vote_stats_event_vote($year, $comics, $ev_no, $type, $comics_buy='')
{
    global $nm_member;

	$boolean = false;

	/* 투표 확인사항 */
	if(vote_check($year) == false){ return false; }

	/* 투표번호 있는지 확인 */
	if($ev_no ==''){ return false; }

	/* 통계타입 있는지 확인 */
	if($type ==''){ return false; }
	
	$set_date['year_month']	= NM_TIME_YM;
	$set_date['year']		= NM_TIME_Y;
	$set_date['month']		= NM_TIME_M;
	$set_date['day']		= NM_TIME_D;
	$set_date['hour']		= NM_TIME_H;
	$set_date['week']		= NM_TIME_W;

	$sql_insert = "	
		INSERT INTO stats_event_vote(
		sev_ev_no, sev_comics, 
		sev_year_month, sev_year, sev_month, 
		sev_day, sev_hour, sev_week
		)VALUES( 
		'".$ev_no."', '".$comics."', 
		'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."',  
		'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."'
		)";
	// 저장
	@mysql_query($sql_insert); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨

	$sql_update_set = "";
	$sql_ev_update_set = "";
	switch($type){
		case 'vote': /* 투표 */
			$sql_update_set = " sev_vote_cnt = (sev_vote_cnt+1) ";
			$sql_ev_update_set = " ev_vote_cnt = (ev_vote_cnt+1) ";
		break;

		case 'view': /* 보러가기 */
			$sql_update_set = " sev_view_cnt = (sev_view_cnt+1) ";
			$sql_ev_update_set = " ev_view_cnt = (ev_view_cnt+1) ";
		break;

		case 'pay': /* 결제 */
			if($comics_buy ==''){ return false; }
			$sql_update_set = " sev_pay_cnt = (sev_pay_cnt+1), 
								sev_cash_point = (sev_cash_point+".$comics_buy['mb_cash_point']."), 
								sev_point = (sev_point+".$comics_buy['mb_point']."), ";
								
			$sql_ev_update_set = "	ev_pay_cnt = (ev_pay_cnt+1), 
									ev_cash_point = (ev_cash_point+".$comics_buy['mb_cash_point']."), 
									ev_point = (ev_point+".$comics_buy['mb_point'].") ";

			if(intval($comics_buy['mb_cash_point']) > 0){
				$sql_update_set.=" sev_cash_point_cnt = (sev_cash_point_cnt+1), ";
			}
			if(intval($comics_buy['mb_point']) > 0){
				$sql_update_set.=" sev_point_cnt = (sev_point_cnt+1), ";
			}
			$sql_update_set = substr($sql_update_set,0,strrpos($sql_update_set, ","));
		break;

		case 'comicsview': /* comicsview */
			$sql_update_set = " sev_comicsview_cnt = (sev_comicsview_cnt+1) ";
			$sql_ev_update_set = " ev_comicsview_cnt = (ev_comicsview_cnt+1) ";
		break;

		default: return false;
		break;
	}

	$sql_update = "
		UPDATE stats_event_vote SET ".$sql_update_set." 
		WHERE 1 
		AND sev_ev_no = '".$ev_no."' AND sev_comics = '".$comics."' 
		AND sev_year_month = '".$set_date['year_month']."' AND sev_year = '".$set_date['year']."' 
		AND sev_month = '".$set_date['month']."' AND sev_day = '".$set_date['day']."' 
		AND sev_hour = '".$set_date['hour']."' AND sev_week = '".$set_date['week']."' 
		";
	sql_query($sql_update);

	// event_vote 에 업데이트

	$sql_ev_update = "
		UPDATE event_vote SET ".$sql_ev_update_set." 
		WHERE 1 
		AND ev_no = '".$ev_no."' AND ev_comics = '".$comics."' AND ev_year = '".$set_date['year']."' 
		";
	sql_query($sql_ev_update);
}


