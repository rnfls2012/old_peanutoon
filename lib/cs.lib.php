<?
if (!defined('_NMPAGE_')) exit;
// 성인 URL
function cs_url_adult($mb_adult_cookie = '', $web_adult='')
{
	global $adult_reverse, $nm_member;

	$para_adult = $adult_reverse[$mb_adult_cookie];
	if($web_adult != ''){
		if($web_adult == 'y' || $web_adult == 'n'){
			$para_adult = $web_adult;
		}
	}
	
	$url_adult = "http://";
	if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') { $url_adult = "https://"; }
	$url_adult.= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	//url에서 adult 빼기
	foreach($adult_reverse as $adult_reverse_key => $adult_reverse_val){
		$url_adult = str_replace("&adult=".$adult_reverse_val, "", $url_adult);
		$url_adult = str_replace("?adult=".$adult_reverse_val, "", $url_adult);
	}
	
	$url_adult = cs_para_attach($url_adult, 'adult='.$para_adult);
	
	// 회원체크	
	if($nm_member['mb_id'] == ''){ 
		$url_adult = cs_para_attach(NM_URL.'/ctlogin.php', 'adult='.$para_adult);
	} // 비로그인 경우...
	else if($nm_member['mb_adult'] != 'y'){ 
		$url_adult = cs_para_attach(NM_URL.'/ctcertify.php', 'adult='.$para_adult);
	} // 성인이 아닐 경우...

	return $url_adult;
}

function cs_para_attach($url, $para)
{
	$attach_url = $url;
	if(strrpos($url, '&')){
		$attach_url.= '&'.$para;
	}else if(strrpos($url, '&amp;')){
		$attach_url.= '&amp;'.$para;
	}else if(strrpos($url, '?')){
		$attach_url.= '&'.$para;
	}else{
		$attach_url.= '?'.$para;
	}

	return $attach_url;
}

function cs_url_adult_del($url)
{
	
	$url = str_replace("?adult=y", "", $url);
	$url = str_replace("?adult=n", "", $url);
	$url = str_replace("&adult=y", "", $url);
	$url = str_replace("&adult=n", "", $url);
	$url = str_replace("adult=y", "", $url);
	$url = str_replace("adult=n", "", $url);

	return $url;
}

function cs_is_admin()
{
	global $nm_config, $nm_member, $_SESSION;
	
	$cs_is_admin = false;
	$cf_admin_level = intval($nm_config['cf_admin_level']+$nm_config['cf_admin_level_plus']);
	if($nm_member['mb_level'] >= $cf_admin_level && $nm_member['mb_id'] != $row['mb_id'] && $_SESSION['ss_adm_id'] == ''){
		$cs_is_admin = true;
	}

	return $cs_is_admin;
}

function cs_get_episode_list($comics, $nm_member, $js='', $order='', $device='')
{
	global $nm_config;

	$episode_arr = array();
	$get_comics = get_comics($comics);

	if($get_comics['cm_no'] == ''){ cs_alert('작품이 없습니다.', NM_URL); die; }

	// 완전 중지 리스트
	$ce_no_x_arr = array();
	$sql_x = "SELECT ce_no FROM comics_episode_".$get_comics['cm_big']." WHERE 1 AND ce_comics = '$comics' AND ce_service_state = 'x' ORDER BY ce_order $sql_order ";
	$result_x = sql_query($sql_x);
	while ($row_x = sql_fetch_array($result_x)) {
			array_push($ce_no_x_arr, $row_x['ce_no']);
	}

	// 소유한 작품
	$mb_get_buy_episode = $mb_buy_episode_list = array();
	$mb_get_buy_episode = mb_get_buy_episode($nm_member, $comics, 'y');

	// 소장 정보
	$sql_in_ce_no = "";
	if(intval($nm_member['mb_no']) > 0){
		if(count($mb_get_buy_episode) > 0){
			if(count($ce_no_x_arr) > 0){
				foreach($mb_get_buy_episode as $mbe_episode_val){
					// 완전 중지 리스트는 제외
					if(in_array($mbe_episode_val, $ce_no_x_arr)){
					}else{
						array_push($mb_buy_episode_list, $mbe_episode_val);
					}
				}
			}else{
				$mb_buy_episode_list = $mb_get_buy_episode;
			}
		}
		if(count($mb_buy_episode_list) > 0){
			$sql_in_ce_no = " OR ce_no in (".implode(",",$mb_buy_episode_list).")";
		}
	}
	
	// DB 에피소드 리스트
	$sql_order = 'DESC';
	if($order == 'ASC'){ $sql_order = 'ASC'; }

	$sql = "SELECT * FROM comics_episode_".$get_comics['cm_big']." WHERE 1 AND ce_comics = '$comics' AND ce_service_state = 'y' ".$sql_in_ce_no." ORDER BY ce_order $sql_order ";
	$result = sql_query($sql);

	// 이벤트
	$event_arr = array();
	$sql_event = "SELECT * FROM event_pay WHERE 1 AND ep_state = 'y' ORDER BY ( CASE ep_state WHEN 'r' THEN 1 WHEN 'y' THEN 2 WHEN 'n' THEN 3 ELSE 4 END ), ep_reg_date DESC";
	$result_event = sql_query($sql_event);
	if(sql_num_rows($result_event) != 0){
		while ($row_event = sql_fetch_array($result_event)) {
			array_push($event_arr, $row_event['ep_no']);
		}
	}

	// 이벤트 번호 큰수 하나만
	$ce_epc_free = $ce_epc_sale = '';
	if(count($event_arr) > 0){
		$sql_event_comics_in = implode(", ",$event_arr);
		$sql_event_comics = "SELECT * FROM event_pay_comics WHERE 1 AND epc_ep_no in(".$sql_event_comics_in.") AND epc_comics = '".$get_comics['cm_no']."' ORDER BY epc_ep_no DESC LIMIT 0, 1";
		$row_event_comics = sql_fetch($sql_event_comics);
		$ce_epc_free = $row_event_comics['epc_free'];
		$ce_epc_sale = $row_event_comics['epc_sale'];
	}

	// 북마크
	// $sql_buy_comics = "SELECT * FROM member_buy_comics WHERE 1 AND mbc_member ='".$nm_member['mb_no']."' AND mbc_member_idx ='".$nm_member['mb_idx']."' AND mbc_comics = '$comics'";
	$sql_buy_comics = "SELECT * FROM member_buy_comics WHERE 1 AND mbc_member ='".$nm_member['mb_no']."' AND mbc_comics = '$comics'";
	$row_buy_comics = sql_fetch($sql_buy_comics);
	$mbc_recent_chapter = $row_buy_comics['mbc_recent_chapter'];

	/* 에피소드 가격 > 코믹스 무료화수 > 이벤트 무료 > 이벤트 코인할인 순위임 
	   그리고 보유&북마크는 당연히 최상입니다. */
	if(sql_num_rows($result) != 0){
		while ($row = sql_fetch_array($result)) {
			// 기본 초기화
			$row['ce_check'] = 'check_on';
			$row['ce_disabled'] = $row['ce_bookmark'] = $row['ce_purchased'] = $row['ce_event_free'] = $row['ce_event_sale'] = $row['ce_btn'] = $row['ce_icon_up'] = '';

			// 에피소드 가격
			if($row['ce_pay'] == 0){
				$row['ce_check'] = 'check_off';
				$row['ce_disabled'] = 'disabled'; 
			}

			// 코믹스 무료 화수
			if( 0 < $row['ce_chapter'] && $row['ce_chapter'] <= $get_comics['cm_free'] && $row['ce_pay'] != 0){
				$row['ce_pay'] = 0;
				$row['ce_check'] = 'check_off';
				$row['ce_disabled'] = 'disabled'; 
			}

			// 이벤트 무료
			if($ce_epc_free == 'y'){
				if( 0 < $row['ce_chapter'] && $row['ce_chapter'] <= $row_event_comics['epc_free_e'] && $row['ce_chapter'] >= $row_event_comics['epc_free_s'] && $row['ce_pay'] != 0){
					$row['ce_pay'] = 0;
					$row['ce_check'] = 'check_off';
					$row['ce_disabled'] = 'disabled'; 
					$row['ce_event_free'] = 'event_free';
					$row['ce_epc_class'] = 'event';
				} 
			}

			// 이벤트 코인할인
			if($ce_epc_sale == 'y'){
				if( 0 < $row['ce_chapter'] && $row['ce_chapter'] <= $row_event_comics['epc_sale_e'] && $row['ce_chapter'] >= $row_event_comics['epc_sale_s'] && $row['ce_pay'] != 0){
					$row['ce_pay'] = $row_event_comics['epc_sale_pay'];
					$row['ce_event_sale'] = 'event_sale';
					$row['ce_epc_class'] = 'event';
				}			
			}

			// 소유한 작품
			if(in_array($row['ce_no'], $mb_get_buy_episode)){
				$row['ce_check'] = 'check_off';
				$row['ce_disabled'] = 'disabled'; 
				$row['ce_purchased'] = "purchased";
			}else{
				// 서비스 중지일때
				if($get_comics['cm_service'] == 'n' && $nm_member['mb_class'] != 'a'){ continue; }
			}

			// 북마크
			if($row['ce_chapter'] == $mbc_recent_chapter){ $row['ce_bookmark'] = 'bookmark'; }

			/*
			if($js == 'y' && $row['ce_check'] == 'check_on'){ 
				$row['ce_a_href'] = "#".$comics."_".$row['ce_no'];
				$row['ce_onclick'] = "onclick='get_episode_url(\"".$comics."\",\"".$row['ce_no']."\");'"; 
			}else{ 
				$row['ce_a_href'] = $row['ce_url'];
				$row['ce_onclick'] = ""; 
			}
			*/

			// 작가, 출판사, 제공사
			if(cs_partner_return_y($nm_member, $get_comics) == "y"){
				$row['ce_check'] = 'check_off';
				$row['ce_disabled'] = 'disabled'; 
				$row['ce_purchased'] = "cs_partner";
			}

			// IMG
			if($device == '') {
				$row['ce_img_url'] = img_url_para($row['ce_cover'], $row['ce_reg_date'], $row['ce_mod_date'], 700, 350, 'RK');	
			} else if($device == 'c') { // PC 일때
				$row['ce_img_url'] = img_url_para($row['ce_cover'], $row['ce_reg_date'], $row['ce_mod_date'], 'ce_cover', 'tn173x86');
			} else {
				$row['ce_img_url'] = img_url_para($row['ce_cover'], $row['ce_reg_date'], $row['ce_mod_date'], 'ce_cover', 'tn640x320');
			} // end else
			
			// mobile-제목
			$ce_up_kind = '화';
			if($row['cm_up_kind'] == '1'){ $ce_up_kind = '권'; }
			if($row['ce_chapter'] > 0){
				$ce_txt = $row['ce_chapter'].$ce_up_kind;
				if($row['ce_title'] != ''){ $ce_txt = $row['ce_chapter'].$ce_up_kind." - ".$row['ce_title']; }
			}else{
				$ce_txt = $row['ce_notice'];
				if($row['ce_notice'] == '' && $row['ce_outer'] == 'y'){
					$ce_txt = "외전".$row['ce_outer_chapter'].$ce_up_kind;
				}
			}
			$row['ce_txt'] = stripslashes($ce_txt);
			
			// web-번호
			$row['ce_outer_txt'] = "";
			$ce_chapter = $row['ce_chapter'];
			if($row['ce_chapter'] < 0){
				if($row['ce_outer'] == 'y'){
					$ce_chapter = $row['ce_outer_chapter'];
					$row['ce_outer_txt'] = "외전";
				}else{
					$ce_chapter = '<i class="fa fa-star" aria-hidden="true"></i>';
					$row['ce_outer_txt'] = $row['ce_notice'];
				}
			}
			$row['ce_chapter_view'] = $ce_chapter;
			
			// web-제목
			$row['ce_title_txt'] = $row['ce_title'];
			if($row['ce_title_txt'] == ""){
				$row['ce_title_txt'] = $get_comics['cm_series'];
			}
			$row['ce_title_txt'] = stripslashes($row['ce_title_txt']);

			// 날짜
			$row['ce_service_date_10'] = substr($row['ce_service_date'], 0, 10);
			// UP ICON
			$week_after = date("Y-m-d", strtotime($row['ce_service_date_10']."+1day"));
			if($week_after >=NM_TIME_YMD){ $row['ce_icon_up'] = "up"; }			
			// 화폐 
			$row['ce_pay_unit'] = cash_point_view($row['ce_pay'],'n','y','y');
			// 버튼
			
			if($row['ce_purchased'] == "purchased"){ 
				$row['ce_btn'] = '<span class="'.$row['ce_purchased'].'"><i class="fa fa-check-square-o" aria-hidden="true"></i> 소장중</span>'; 
			}else if($row['ce_purchased'] == "cs_partner"){
				$cs_partner_txt = "";				
				switch($nm_member['mb_level']) {
					case '14' : $cs_partner_txt = "제공사 무료";
					break;
					case '15' : $cs_partner_txt = "작가 무료";
					break;
					// case '16' : $cs_partner_txt = "출판사 무료";
					// break;
				}
				$row['ce_btn'] = '<span class="'.$row['ce_purchased'].'"><i class="fa fa-check-square-o" aria-hidden="true"></i> '.$cs_partner_txt.'</span>'; 
			}else if($row['ce_pay'] > 0){ 
				$row['ce_btn'] = '<button class="'.$row['ce_epc_class'].'">'.$row['ce_pay_unit'].'</button>';
			}else{ 
				$row['ce_btn'] = '<span class="free '.$row['ce_epc_class'].'">'.$row['ce_pay_unit'].'</span>'; 
			}
			
			// URL
			if($row['ce_check'] == 'check_off' ){
				$row['ce_url'] = '<a href="'.get_episode_url($comics, $row['ce_no']).'" target="_self">'; 
			}else{
				$row['ce_url'] = '<a href="#comics_episode_goto_url'.$row['ce_no'].'" onclick=\'comics_episode_goto_url("'.$get_comics['cm_no'].'","'.$row['ce_no'].'","'.$row['ce_pay'].'","'.$row['ce_txt'].'");\' target="_self">';
			}

			array_push($episode_arr, $row);
		}
	}
	return $episode_arr;
}

/* 내 서재 화 리스트 */
function cs_get_myepisode_list($comics, $nm_member, $js='', $order='', $device='')
{
	global $nm_config;

	$episode_arr = array();
	$get_comics = get_comics($comics);

	if($get_comics['cm_no'] == ''){ cs_alert('작품이 없습니다.', NM_URL); die; }

	// 완전 중지 리스트
	$ce_no_x_arr = array();
	$sql_x = "SELECT ce_no FROM comics_episode_".$get_comics['cm_big']." WHERE 1 AND ce_comics = '$comics' AND ce_service_state = 'x' ORDER BY ce_order $sql_order ";
	$result_x = sql_query($sql_x);
	while ($row_x = sql_fetch_array($result_x)) {
		array_push($ce_no_x_arr, $row_x['ce_no']);
	}

	// 소유한 작품
	$mb_get_buy_episode = $mb_buy_episode_list = array();
	$mb_get_buy_episode = mb_get_buy_episode($nm_member, $comics, 'y');

	// 소장 정보
	$sql_in_ce_no = "";
	if(intval($nm_member['mb_no']) > 0){
		if(count($mb_get_buy_episode) > 0){
			if(count($ce_no_x_arr) > 0){
				foreach($mb_get_buy_episode as $mbe_episode_val){
					// 완전 중지 리스트는 제외
					if(in_array($mbe_episode_val, $ce_no_x_arr)){
					}else{
						array_push($mb_buy_episode_list, $mbe_episode_val);
					}
				}
			}else{
				$mb_buy_episode_list = $mb_get_buy_episode;
			}
		}
		if(count($mb_buy_episode_list) > 0){
			$sql_in_ce_no = " OR ce_no in (".implode(",",$mb_buy_episode_list).")";
		}
	}
	
	$sql_order = 'DESC';
	if($order == 'ASC'){ $sql_order = 'ASC'; }

	$sql = "SELECT * FROM comics_episode_".$get_comics['cm_big']." WHERE 1 AND ce_comics = '$comics' AND ce_service_state = 'y' ".$sql_in_ce_no." ORDER BY ce_order $sql_order ";
	$result = sql_query($sql);

	// 이벤트
	$event_arr = array();
	$sql_event = "SELECT * FROM event_pay WHERE 1 AND ep_state = 'y' ORDER BY ( CASE ep_state WHEN 'r' THEN 1 WHEN 'y' THEN 2 WHEN 'n' THEN 3 ELSE 4 END ), ep_reg_date DESC";
	$result_event = sql_query($sql_event);
	if(sql_num_rows($result_event) != 0){
		while ($row_event = sql_fetch_array($result_event)) {
			array_push($event_arr, $row_event['ep_no']);
		}
	}

	// 이벤트 번호 큰수 하나만
	$ce_epc_free = $ce_epc_sale = '';
	if(count($event_arr) > 0){
		$sql_event_comics_in = implode(", ",$event_arr);
		$sql_event_comics = "SELECT * FROM event_pay_comics WHERE 1 AND epc_ep_no in(".$sql_event_comics_in.") AND epc_comics = '".$get_comics['cm_no']."' ORDER BY epc_ep_no DESC LIMIT 0, 1";
		$row_event_comics = sql_fetch($sql_event_comics);
		$ce_epc_free = $row_event_comics['epc_free'];
		$ce_epc_sale = $row_event_comics['epc_sale'];
	}

	// 북마크
	// $sql_buy_comics = "SELECT * FROM member_buy_comics WHERE 1 AND mbc_member ='".$nm_member['mb_no']."' AND mbc_member_idx ='".$nm_member['mb_idx']."' AND mbc_comics = '$comics'";
	$sql_buy_comics = "SELECT * FROM member_buy_comics WHERE 1 AND mbc_member ='".$nm_member['mb_no']."' AND mbc_comics = '$comics'";
	$row_buy_comics = sql_fetch($sql_buy_comics);
	$mbc_recent_chapter = $row_buy_comics['mbc_recent_chapter'];

	/* 에피소드 가격 > 코믹스 무료화수 > 이벤트 무료 > 이벤트 코인할인 순위임 
	   그리고 보유&북마크는 당연히 최상입니다. */
	if(sql_num_rows($result) != 0){
		while ($row = sql_fetch_array($result)) {
			// 기본 초기화
			$row['ce_check'] = 'check_on';
			$row['ce_disabled'] = $row['ce_bookmark'] = $row['ce_purchased'] = $row['ce_event_free'] = $row['ce_event_sale'] = $row['ce_btn'] = $row['ce_icon_up'] = '';
			$row['ce_free_pay'] = 0; // 무료화 소장 금액 추가 0802

			// 에피소드 가격
			if($row['ce_pay'] == 0){
				if($row['ce_outer'] == 'n' && $row['ce_chapter'] > 0) { // 1. 코믹스 자체가 유료인 경우 0802
					$row['ce_free_pay'] = $get_comics['cm_free_pay']; 
				} else if($row['ce_outer'] == 'y' && $row['ce_chapter'] < 0) { // 2. 외전인데 무료 소장 금액 추가 0802
					$row['ce_free_pay'] = $get_comics['cm_free_pay']; 
				} else { // 3. 중간 공지사항, 휴재 공지 등 소장 금액 미추가 0802

				} // end else
				
				/* 주석 처리 0802
				$row['ce_check'] = 'check_off';
				$row['ce_disabled'] = 'disabled'; 
				*/
			}

			// 코믹스 무료 화수
			if( 0 < $row['ce_chapter'] && $row['ce_chapter'] <= $get_comics['cm_free'] && $row['ce_pay'] != 0){
				$row['ce_pay'] = 0;
				$row['ce_free_pay'] = $get_comics['cm_free_pay']; // 무료화 소장 금액 추가 0802
				/* 주석 처리 0802
				$row['ce_check'] = 'check_off';
				$row['ce_disabled'] = 'disabled'; 
				*/
			}

			// 이벤트 무료
			/* 이벤트 무료는 완전 제외 0802
			if($ce_epc_free == 'y'){
				if( 0 < $row['ce_chapter'] && $row['ce_chapter'] <= $row_event_comics['epc_free_e'] && $row['ce_chapter'] >= $row_event_comics['epc_free_s'] && $row['ce_pay'] != 0){
					$row['ce_pay'] = 0;
					$row['ce_check'] = 'check_off';
					$row['ce_disabled'] = 'disabled'; 
					$row['ce_event_free'] = 'event_free';
				} 
			}
			*/

			// 이벤트 코인할인
			// 할인된 가격으로 소장 가능 0802 (소스 그대로)
			if($ce_epc_sale == 'y'){
				if( 0 < $row['ce_chapter'] && $row['ce_chapter'] <= $row_event_comics['epc_sale_e'] && $row['ce_chapter'] >= $row_event_comics['epc_sale_s'] && $row['ce_pay'] != 0){
					$row['ce_pay'] = $row_event_comics['epc_sale_pay'];
					$row['ce_event_sale'] = 'event_sale';
				}			
			}

			// 소유한 작품
			if(in_array($row['ce_no'], $mb_get_buy_episode)){
				$row['ce_check'] = 'check_off';
				$row['ce_disabled'] = 'disabled'; 
				$row['ce_purchased'] = "purchased";
			}else{
				// 서비스 중지일때
				if($get_comics['cm_service'] == 'n' && $nm_member['mb_class'] != 'a'){ continue; }
			}

			// 북마크
			if($row['ce_chapter'] == $mbc_recent_chapter){ $row['ce_bookmark'] = 'bookmark'; }

			/*
			if($js == 'y' && $row['ce_check'] == 'check_on'){ 
				$row['ce_a_href'] = "#".$comics."_".$row['ce_no'];
				$row['ce_onclick'] = "onclick='get_episode_url(\"".$comics."\",\"".$row['ce_no']."\");'"; 
			}else{ 
				$row['ce_a_href'] = $row['ce_url'];
				$row['ce_onclick'] = ""; 
			}
			*/

			// IMG
			if($device == '') {
				$row['ce_img_url'] = img_url_para($row['ce_cover'], $row['ce_reg_date'], $row['ce_mod_date'], 700, 350, 'RK');	
			} else if($device == 'c') { // PC 일때
				$row['ce_img_url'] = img_url_para($row['ce_cover'], $row['ce_reg_date'], $row['ce_mod_date'], 'ce_cover', 'tn173x86');
			} else {
				$row['ce_img_url'] = img_url_para($row['ce_cover'], $row['ce_reg_date'], $row['ce_mod_date'], 'ce_cover', 'tn640x320');
			} // end else
			
			// mobile-제목
			$ce_up_kind = '화';
			if($row['cm_up_kind'] == '1'){ $ce_up_kind = '권'; }
			if($row['ce_chapter'] > 0){
				$ce_txt = $row['ce_chapter'].$ce_up_kind;
				if($row['ce_title'] != ''){ $ce_txt = $row['ce_chapter'].$ce_up_kind." - ".$row['ce_title']; }
			}else{
				$ce_txt = $row['ce_notice'];
				if($row['ce_notice'] == '' && $row['ce_outer'] == 'y'){
					$ce_txt = "외전".$row['ce_outer_chapter'].$ce_up_kind;
				}
			}
			$row['ce_txt'] = stripslashes($ce_txt);

			// web-번호
			$row['ce_outer_txt'] = "";
			$ce_chapter = $row['ce_chapter'];
			if($row['ce_chapter'] < 0){
				if($row['ce_outer'] == 'y'){
					$ce_chapter = $row['ce_outer_chapter'];
					$row['ce_outer_txt'] = "외전";
				}else{
					$ce_chapter = '<i class="fa fa-star" aria-hidden="true"></i>';
					$row['ce_outer_txt'] = $row['ce_notice'];
				}
			}
			$row['ce_chapter_view'] = $ce_chapter;
			
			// web-제목
			$row['ce_title_txt'] = $row['ce_title'];
			if($row['ce_title_txt'] == ""){
				$row['ce_title_txt'] = $get_comics['cm_series'];
			}
			$row['ce_title_txt'] = stripslashes($row['ce_title_txt']);

			// 날짜
			$row['ce_service_date_10'] = substr($row['ce_service_date'], 0, 10);
			// UP ICON
			$week_after = date("Y-m-d", strtotime($row['ce_service_date_10']."+1day"));
			if($week_after >=NM_TIME_YMD){ $row['ce_icon_up'] = "up"; }			
			// 화폐 
			$pay = 0; // 무료화 구매와, 일반 구매 비교해서 추가 0802
			if($row['ce_pay'] > 0 && $row['ce_free_pay'] == 0) {
				$pay = $row['ce_pay'];
			} else if($row['ce_free_pay'] > 0 && $row['ce_pay'] == 0) {
				$pay = $row['ce_free_pay'];
			} 

			$row['ce_pay_unit'] = cash_point_view($pay,'y','y','y'); // '무료'라는 말 제거 0802
			
			/* 필요없으므로 주석처리 0802
			// 버튼
			if($row['ce_purchased'] == "purchased"){ 
				$row['ce_btn'] = '<span class="'.$row['ce_purchased'].'"><i class="fa fa-check-square-o" aria-hidden="true"></i> 소장중</span>'; 
			}else if($pay > 0){ 
				$row['ce_btn'] = '<button>'.$row['ce_pay_unit'].'</button>'; 
			}else{ 
				$row['ce_btn'] = '<span class="free">'.$row['ce_pay_unit'].'</span>'; 
			}
			*/

			// mysecret parameter
			$mysecret = get_token($nm_member['mb_no']."_".$get_comics['cm_no']."_".$row['ce_no']);
			
			// URL
			if($row['ce_check'] == 'check_off' ){
				$row['ce_url'] = '<a href="'.get_episode_url($comics, $row['ce_no']).'" target="_self">'; 
			}else{
				$row['ce_url'] = '<a href="#comics_myepisode_goto_url'.$row['ce_no'].'" onclick=\'comics_myepisode_goto_url("'.$get_comics['cm_no'].'","'.$row['ce_no'].'", "'.$row['ce_pay'].'","'.$row['ce_free_pay'].'","'.$row['ce_txt'].'","'.$mysecret.'");\' target="_self">'; // my.js에 comics_myepisode_goto_url추가
			} // end else

			array_push($episode_arr, $row);
		}
	}
	return $episode_arr;
}

function cs_get_view_episode_list($comics, $nm_member, $episode_no)
{
	global $nm_config;

	$episode_no_current = "";
	// 이전화 = 다음화 = 화리스트
	$ce_no_prev = $ce_no_next = $episode_arr = array();
	$get_comics = get_comics($comics);

	if($get_comics['cm_no'] == ''){ cs_alert('작품이 없습니다.', NM_URL); die; }
	
	$sql_order = 'DESC';
	if($order == 'ASC'){ $sql_order = 'ASC'; }

	$sql = "SELECT * FROM comics_episode_".$get_comics['cm_big']." WHERE 1 AND ce_comics = '$comics' AND ce_service_state = 'y' ORDER BY ce_order $sql_order ";
	$result = sql_query($sql);

	// 이벤트
	$event_arr = array();
	$sql_event = "SELECT * FROM event_pay WHERE 1 AND ep_state = 'y' ORDER BY ( CASE ep_state WHEN 'r' THEN 1 WHEN 'y' THEN 2 WHEN 'n' THEN 3 ELSE 4 END ), ep_reg_date DESC";
	$result_event = sql_query($sql_event);
	if(sql_num_rows($result_event) != 0){
		while ($row_event = sql_fetch_array($result_event)) {
			array_push($event_arr, $row_event['ep_no']);
		}
	}

	// 이벤트 번호 큰수 하나만
	$ce_epc_free = $ce_epc_sale = '';
	if(count($event_arr) > 0){
		$sql_event_comics_in = implode(", ",$event_arr);
		$sql_event_comics = "SELECT * FROM event_pay_comics WHERE 1 AND epc_ep_no in(".$sql_event_comics_in.") AND epc_comics = '".$get_comics['cm_no']."' ORDER BY epc_ep_no DESC LIMIT 0, 1";
		$row_event_comics = sql_fetch($sql_event_comics);
		$ce_epc_free = $row_event_comics['epc_free'];
		$ce_epc_sale = $row_event_comics['epc_sale'];
	}

	// 소유한 작품
	$mb_buy_episode_list = array();
	$mb_get_buy_episode = mb_get_buy_episode($nm_member, $comics, 'y');

	/* 에피소드 가격 > 코믹스 무료화수 > 이벤트 무료 > 이벤트 코인할인 순위임 
	   그리고 보유&북마크는 당연히 최상입니다. */
	if(sql_num_rows($result) != 0){
		$ce_count=0;
		for ($i=0; $row=sql_fetch_array($result); $i++) {
			// 기본 초기화
			$row['ce_check'] = 'check_on';
			$row['ce_disabled'] = $row['ce_bookmark'] = $row['ce_purchased'] = $row['ce_event_free'] = $row['ce_event_sale'] = $row['ce_btn'] = $row['ce_icon_up'] = '';

			// 에피소드 가격
			if($row['ce_pay'] == 0){
				$row['ce_check'] = 'check_off';
				$row['ce_disabled'] = 'disabled'; 
			}

			// 코믹스 무료 화수
			if( 0 < $row['ce_chapter'] && $row['ce_chapter'] <= $get_comics['cm_free'] && $row['ce_pay'] != 0){
				$row['ce_pay'] = 0;
				$row['ce_check'] = 'check_off';
				$row['ce_disabled'] = 'disabled'; 
			}

			// 이벤트 무료
			if($ce_epc_free == 'y'){
				if( 0 < $row['ce_chapter'] && $row['ce_chapter'] <= $row_event_comics['epc_free_e'] && $row['ce_chapter'] >= $row_event_comics['epc_free_s'] && $row['ce_pay'] != 0){
					$row['ce_pay'] = 0;
					$row['ce_check'] = 'check_off';
					$row['ce_disabled'] = 'disabled'; 
					$row['ce_event_free'] = 'event_free';
				} 
			}

			// 이벤트 코인할인
			if($ce_epc_sale == 'y'){
				if( 0 < $row['ce_chapter'] && $row['ce_chapter'] <= $row_event_comics['epc_sale_e'] && $row['ce_chapter'] >= $row_event_comics['epc_sale_s'] && $row['ce_pay'] != 0){
					$row['ce_pay'] = $row_event_comics['epc_sale_pay'];
					$row['ce_event_sale'] = 'event_sale';
				}			
			}

			// 소유한 작품
			if(in_array($row['ce_no'], $mb_get_buy_episode)){
				$row['ce_check'] = 'check_off';
				$row['ce_disabled'] = 'disabled'; 
				$row['ce_purchased'] = "purchased";
				$row['ce_pay'] = 0;
			}else{
				// 서비스 중지일때
				if($get_comics['cm_service'] == 'n' && $nm_member['mb_class'] != 'a'){ continue; }
			}
			
			// 작가, 출판사, 제공사
			if(cs_partner_return_y($nm_member, $get_comics) == "y"){
				$row['ce_check'] = 'check_off';
				$row['ce_disabled'] = 'disabled'; 
				$row['ce_purchased'] = "cs_partner";
				$row['ce_pay'] = 0;
			}

			// URL
			$row['ce_url'] = get_episode_url($comics, $row['ce_no']); 

			// IMG
			$row['ce_img_url'] = img_url_para($row['ce_cover'], $row['ce_reg_date'], $row['ce_mod_date'], 700, 350, 'RK');			
			// 제목
			$ce_up_kind = '화';
			if($row['cm_up_kind'] == '1'){ $ce_up_kind = '권'; }
			if($row['ce_chapter'] > 0){
				$ce_txt = $row['ce_chapter'].$ce_up_kind;
				// if($row['ce_title'] != ''){ $ce_txt = $row['ce_chapter'].$ce_up_kind." - ".$row['ce_title']; }
			}else{
				$ce_txt = $row['ce_notice'];
				if($row['ce_notice'] == '' && $row['ce_outer'] == 'y'){
					$ce_txt = "외전".$row['ce_outer_chapter'].$ce_up_kind;
				}
			}
			$row['ce_txt'] = stripslashes($ce_txt);
			
						
			if($row['ce_purchased'] == "purchased"){  // 소장
				$row['ce_btn'] = '<span><i class="fa fa-check-square-o" aria-hidden="true"></i></span> 소장중'; 
			}else if($row['ce_pay'] > 0){  // 유료
				$row['ce_btn'] = '<button>'.$row['ce_pay_unit'].'</button>'; 
			}else{ // 무료
				$row['ce_btn'] = $row['ce_pay_unit']; 
			}

			array_push($episode_arr, $row);
			
			if($episode_no == $row['ce_no']){
				$episode_no_current = $ce_count;
			}
			$ce_count++;
		}
		
		$ce_no_prev_no = $episode_no_current-1;
		$ce_no_next_no = $episode_no_current+1;

		if($ce_no_prev_no >= 0){
			$ce_no_prev = $episode_arr[$ce_no_prev_no]; // 이전화 
		}
		if($ce_no_next_no < $i){
			$ce_no_next = $episode_arr[$ce_no_next_no]; // 다음화 
		}

		array_push($episode_arr, $ce_no_prev);
		array_push($episode_arr, $ce_no_next);
	}
	return $episode_arr;
}

function cs_alert($msg, $url='', $error='')
{
	global $nm_config;
	
	include_once($nm_config['nm_path']."/_head.sub.php");

	$goto_url = "";
	if($error!=''){ $msg.= '\n'.addslashes($error);}
	if($url){
		$goto_url = "document.location.href = '".$url."';";
	}
	echo' 
		<script type="text/javascript">
		<!--
			alertBox("'.$msg.'", goto_url);
			function goto_url(){
				'.$goto_url.'
			}
		//-->
		</script>
		';
}

function cs_confirm($msg, $url='', $error='')
{
	global $nm_config;
	
	include_once($nm_config['nm_path']."/_head.sub.php");

	$goto_url = "";
	if($error!=''){ $msg.= '\n'.addslashes($error);}
	if($url){
		$confirm_url = $url;
	}
	echo' 
		<script type="text/javascript">
		<!--
			confirm_url = "'.$confirm_url.'";
			confirmBox("'.$msg.'", goto_url, {url:confirm_url});
		//-->
		</script>
		';
}


function cs_confirm_url($msg, $url_true='', $url_false='', $error='')
{
	global $nm_config;
	
	include_once($nm_config['nm_path']."/_head.sub.php");

	$goto_url = "";
	if($error!=''){ $msg.= '\n'.addslashes($error);}

	if($url_true){	$confirm_url_true = $url_true; }
	if($url_false){	$confirm_url_false = $url_false; }

	echo' 
		<script type="text/javascript">
		<!--
			confirm_url_true = "'.$confirm_url_true.'";
			confirm_url_false = "'.$confirm_url_false.'";
			confirmBoxUrl("'.$msg.'", goto_url, {url:confirm_url_true}, {url:confirm_url_false});
		//-->
		</script>
		';
}

function cs_alert_close($msg, $error='')
{
	global $nm_config;

	if($nm_config['nm_url'] == NM_PC_URL){
		alert($msg, $url);
	}else{
		include_once($nm_config['nm_path']."/_head.sub.php");

		$goto_url = "";
		if($error!=''){ $msg.= '\n'.addslashes($error);}
		if($url){
			$goto_url = "document.location.href = '".$url."';";
		}
		echo' 
			<script type="text/javascript">
			<!--
				alertBox("'.$msg.'", alert_close);
				function alert_close(){
					window.close();
					self.close();
					window.open("", "_self").close();
				}
			//-->
			</script>
			';
	}
}

function cs_sin_allpay($js='n')
{
	$sin_allpay_arr = array();
	$sql_sin_allpay = "SELECT * FROM config_sin_allpay where 1 AND csa_state = 'y' ORDER BY csa_buy_count, csa_point ";
	$result_sin_allpay = sql_query($sql_sin_allpay);

	if(sql_num_rows($result_sin_allpay) != 0){
		$sin_allpay_arr_js = " [ ";
		while ($row_sin_allpay = sql_fetch_array($result_sin_allpay)) {
			$sin_allpay_arr_js.= "['".$row_sin_allpay['csa_buy_count']."',";
			$sin_allpay_arr_js.= " '".$row_sin_allpay['csa_buy_than']."',";
			$sin_allpay_arr_js.= " '".$row_sin_allpay['csa_point']."'";
			$sin_allpay_arr_js.= "], ";
			array_push($sin_allpay_arr, $row_sin_allpay);
		}
		$sin_allpay_arr_js = substr($sin_allpay_arr_js,0,strrpos($sin_allpay_arr_js, ","));
		$sin_allpay_arr_js.= " ] ";
	}

	if($js == 'y'){
		return $sin_allpay_arr_js;
	}else{
		return $sin_allpay_arr;
	}
}

function cs_comic_first_url($comics)
{
	$get_episode_first_url = "";
	$get_comics = get_comics($comics);

	$sql = "SELECT * FROM comics_episode_".$get_comics['cm_big']." WHERE 1 AND ce_comics = '$comics' AND ce_service_state = 'y' ORDER BY ce_order LIMIT 0, 1 ";
	$row = sql_fetch($sql);
	if($row['ce_no'] != ''){
		$get_episode_first_url = get_episode_url($comics, $row['ce_no']);
	}
	return $get_episode_first_url;
}

function cs_comics_adult($comics, $nm_member)
{
	$comics_adult_check = false;

	$get_comics = get_comics($comics);

	if($get_comics['cm_adult'] == 'y'){
		if($nm_member['mb_adult'] == 'y'){
			$comics_adult_check = true;
		}
	}else{
		$comics_adult_check = true;
	}
	
	if($comics_adult_check == false){
		if($nm_member['mb_no'] == ''){
			cs_alert('로그인해주세요.', NM_URL."/ctlogin.php");
			die;
		}else{
			cs_alert('성인인증해주세요.', NM_URL."/ctcertify.php");
			die;
		}
	}
}

function cs_set_expen_episode($comics, $episode, $nm_member, $comics_buy, $comics_way=1, $used='y')
{	
	// 코믹스Object 에피소드Object 회원Object 코믹스구매Object

	global $nm_config;

	// 회원정보 포인트 처리 후 재로드을 위해서...
	$nm_member = mb_get($nm_member['mb_id']);

	// 소장타입(0:무료, 1:소장, 2:대여, 3:에러)
	$mbe_end_date = "3000-01-01";
	if($comics['cm_own_type'] == '2'){
		$mbe_end_date = date("Y-m-d", strtotime(NM_TIME_YMD."+".$comics['cm_s_time']."day"));			
	}
	$comics_buy['mbe_end_date'] = $mbe_end_date;
	
	// 코믹스 가격
	// $cm_pay  = intval($comics['cm_pay']);
	$cm_pay  = intval($episode['ce_pay']);

	// 회원 포인트 계산
	$mb_point = intval($nm_member['mb_point']);
	$mb_cash_point = intval($nm_member['mb_cash_point']);

	// 포인트 10 자리 계산 처리
	$mb_point_decimal_remainder = intval($mb_point % NM_POINT);
	$mb_point_decimal = intval($mb_point / NM_POINT) * NM_POINT;

	// 캐쉬 결제
	$mb_cash_point_balance =  $mb_cash_point - $cm_pay;
	$mb_cash_point_use = $cm_pay;
	
	$mb_point_balance = $mb_point;
	$mb_point_use = 0;
	
	// 캐쉬포인트가 없으면... 포인트사용
	if($mb_cash_point_balance < 0){
		$mb_point_use  = abs(intval($mb_cash_point_balance * NM_POINT));
		$mb_point_balance = $mb_point_decimal - $mb_point_use + $mb_point_decimal_remainder;
		
		// 캐쉬포인트 처리
		$mb_cash_point_balance = 0;
		$mb_cash_point_use = $cm_pay - intval($mb_point_use / NM_POINT) ;
	}
	$comics_buy['mb_cash_point'] = $mb_cash_point_use;
	$comics_buy['mb_point'] = $mb_point_use;

	$comics_buy['sl_all_pay'] =  0;
	$comics_buy['sl_pay'] =  1;
	if($comics_way != 1){
		$comics_buy['sl_all_pay'] =  1;
		$comics_buy['sl_pay'] =  0;
	}
	
	if($mb_point_balance < 0){
		$msg = "결제 비용이 부족합니다. 결제페이지로 넘어갑니다.";
		cs_alert($msg, NM_URL."/recharge.php"."?redirect=comicsview.php?comics=".$comics['cm_no']."__nn__episode=".$episode['ce_no']);
		die;
	}else{ }	

	/* 연령 */
	$mb_age_group = mb_get_age_group($nm_member['mb_birth']);
	$comics_buy['mb_age_group'] = $mb_age_group;

	/* 결제환경 */
	$cs_brow = get_brow(HTTP_USER_AGENT);
	$cs_os = get_os(HTTP_USER_AGENT);

	/* 해당 코믹스 구매 체크 */
	$sql_mpee = "SELECT  count(*) as total_mpee FROM member_point_expen_episode_".$comics['cm_big']."  WHERE 1 AND mpee_comics = '".$comics['cm_no']."' AND mpee_episode = '".$episode['ce_no']."' AND mpee_member = '".$nm_member['mb_no']."' AND mpee_member_idx = '".$nm_member['mb_idx']."' ";	
	$total_mpee = sql_count($sql_mpee, 'total_mpee');

	/*
	if($total_mpee!=0){
		$msg = "이미 구매한 코믹스 화를 선택되어있습니다. 이중결재를 막기위해 다시 구매절차를 진행해주세요. code:200";
		cs_alert($msg, NM_URL."/comics.php?comics=".$comics['cm_no']);
	}
	*/

	// 코믹스 구매 입력 - member_point_expen
	$sql_mpec = "SELECT  count(*) as total_mpec FROM member_point_expen_comics WHERE 1 AND mpec_comics = '".$comics['cm_no']."' AND mpec_big = '".$comics['cm_big']."' AND mpec_member = '".$nm_member['mb_no']."' AND mpec_member_idx = '".$nm_member['mb_idx']."' ";	
	$total_mpec = sql_count($sql_mpec, 'total_mpec');
	
	if($total_mpec!=0){
		$sql_mpec_update = "
		UPDATE member_point_expen_comics SET 
		mpec_big = '".$comics['cm_big']."', 
		mpec_small = '".$comics['cm_small']."', 

		mpec_cash_type = '".$nm_config['cf_cup_type']."', 
		mpec_cash_point= (mpec_cash_point+".intval($mb_cash_point_use)."), 
		mpec_point= (mpec_point+".intval($mb_point_use)."), 
		mpec_count= (mpec_count+1), 
		mpec_cash_point_balance = '".$mb_cash_point_balance."', 
		mpec_point_balance = '".$mb_point_balance."', 

		mpec_state = '1', 
		mpec_way = '".$comics_way."', 
		mpec_age = '".$mb_age_group."', 
		mpec_sex = '".$nm_member['mb_sex']."', 

		mpec_os = '".$cs_os."', 
		mpec_brow = '".$cs_brow."', 
		mpec_user_agent = '".HTTP_USER_AGENT."', 
		mpec_version = '".NM_VERSION."', 

		mpec_year_month = '".NM_TIME_YM."', 
		mpec_year = '".NM_TIME_Y."', 
		mpec_month = '".NM_TIME_M."', 
		mpec_day = '".NM_TIME_D."', 
		mpec_hour = '".NM_TIME_H."', 
		mpec_week = '".NM_TIME_W."', 
		mpec_date = '".NM_TIME_YMDHIS."' 

		WHERE 1 AND mpec_comics = '".$comics['cm_no']."' AND mpec_member = '".$nm_member['mb_no']."' AND mpec_member_idx = '".$nm_member['mb_idx']."' 
				AND mpec_big='".$comics['cm_big']."' 
				AND mpec_year_month='".NM_TIME_YM."' 
				AND mpec_year='".NM_TIME_Y."' 
				AND mpec_month='".NM_TIME_M."' 
				AND mpec_day='".NM_TIME_D."' 
				AND mpec_hour='".NM_TIME_H."' 
				AND mpec_week='".NM_TIME_W."' 		
		";
		sql_query($sql_mpec_update);
	}else{
		$sql_mpec_insert = "
		INSERT INTO member_point_expen_comics (
		mpec_member, mpec_member_idx, 

		mpec_comics, mpec_big, mpec_small, 

		mpec_cash_type, mpec_cash_point, mpec_point, mpec_count, mpec_cash_point_balance, 	mpec_point_balance, 

		mpec_state, mpec_way, mpec_age, mpec_sex, 

		mpec_os, mpec_brow, mpec_user_agent, mpec_version, 

		mpec_year_month, mpec_year, mpec_month, mpec_day, mpec_hour, mpec_week, mpec_date 
		) VALUES (
		'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 

		'".$comics['cm_no']."', '".$comics['cm_big']."', '".$comics['cm_small']."', 

		'".$nm_config['cf_cup_type']."', '".$mb_cash_point_use."', '".$mb_point_use."', '1', '".$mb_cash_point_balance."', '".$mb_point_balance."', 

		'1', '".$comics_way."', '".$mb_age_group."', '".$nm_member['mb_sex']."', 

		'".$cs_os."', '".$cs_brow."', '".HTTP_USER_AGENT."', '".NM_VERSION."', 

		'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', '".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."' 
		)";
		sql_query($sql_mpec_insert);
	}	
	
	// 코믹스 구매 번호 구하기 - member_point_expen
	$sql_mpec_chk = "SELECT * FROM member_point_expen_comics WHERE 1 AND mpec_comics = '".$comics['cm_no']."' AND mpec_member = '".$nm_member['mb_no']."' AND mpec_member_idx = '".$nm_member['mb_idx']."' ";	
	$row_mpec_chk = sql_fetch($sql_mpec_chk);

	// 코믹스 에피소드 구매 입력
	$sql_mpee_insert = "
	INSERT INTO member_point_expen_episode_".$comics['cm_big']." (
	mpee_member, mpee_member_idx, 

	mpec_no, mpee_comics, mpee_small, mpee_episode, mpee_chapter, 

	mpee_cash_type, mpee_cash_point, mpee_point, mpee_cash_point_balance, mpee_point_balance, 

	mpee_state, mpee_sale, mpee_age, mpee_way, mpee_sex, 

	mpee_os, mpee_brow, mpee_user_agent, mpee_version, 

	mpee_year_month, mpee_year, mpee_month, mpee_day, mpee_hour, mpee_week, mpee_date
	) VALUES (
	'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 

	'".$row_mpec_chk['mpec_no']."', '".$comics['cm_no']."', '".$comics['cm_small']."', '".$episode['ce_no']."', '".$episode['ce_chapter']."', 

	'".$nm_config['cf_cup_type']."', '".$mb_cash_point_use."', '".$mb_point_use."', '".$mb_cash_point_balance."', '".$mb_point_balance."', 

	'1', '".$episode['ce_event_sale']."', '".$mb_age_group."', '".$comics_way."', '".$nm_member['mb_sex']."', 

	'".$cs_os."', '".$cs_brow."', '".HTTP_USER_AGENT."', '".NM_VERSION."', 

	'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', '".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."' 
	)";
	sql_query($sql_mpee_insert);
	
	// 소장 정보 코믹스만 등록 -> 등록 실패시 false 반환
	$cs_mb_comics_bookmark = cs_mb_comics_bookmark($comics, $episode, $nm_member);

	// 소장 정보 화 등록
	$sql_mbe = "SELECT  count(*) as total_mbe FROM member_buy_episode_".$comics['cm_big']."  
				WHERE 1 AND mbe_comics = '".$comics['cm_no']."' 
					AND mbe_episode = '".$episode['ce_no']."' 
					AND mbe_member = '".$nm_member['mb_no']."' 
					AND mbe_member_idx = '".$nm_member['mb_idx']."' ";					
	$total_mbe = sql_count($sql_mbe, 'total_mbe');

	if($total_mbe!=0){	
		$sql_mbe_update = "
		UPDATE member_buy_episode_".$comics['cm_big']." SET 
		mbe_chapter = '".$episode['ce_chapter']."', 
		mbe_order = '".$episode['ce_order']."', 
		mbe_way = '".$comics_way."', 
		mbe_own_type = '".$comics['cm_own_type']."', 
		mbe_end_date = '".$mbe_end_date."', 
		mbe_date = '".NM_TIME_YMDHIS."' 

		WHERE 1 AND mbe_comics = '".$comics['cm_no']."' 
					AND mbe_episode = '".$episode['ce_no']."' 
					AND mbe_member = '".$nm_member['mb_no']."' 
					AND mbe_member_idx = '".$nm_member['mb_idx']."' ";	
		sql_query($sql_mbe_update);

	}else{
		$sql_mbe_insert = "
		INSERT INTO member_buy_episode_".$comics['cm_big']." (
		mbe_chapter, mbe_order, mbe_way, mbe_own_type, mbe_end_date, mbe_date, 
		mbe_comics, mbe_episode, mbe_member, mbe_member_idx
		) VALUES (
		'".$episode['ce_chapter']."', '".$episode['ce_order']."', '".$comics_way."', '".$comics['cm_own_type']."',  '".$mbe_end_date."',  '".NM_TIME_YMDHIS."', 
		'".$comics['cm_no']."', '".$episode['ce_no']."', '".$nm_member['mb_no']."',  '".$nm_member['mb_idx']."' 
		)";
		sql_query($sql_mbe_insert);
	}

	// 멤버 포인트 차감
	$sql_mb_update = "
	UPDATE member SET 
	mb_cash_point = '".$mb_cash_point_balance."', 
	mb_point = '".$mb_point_balance."' 
	WHERE 1 AND mb_id = '".$nm_member['mb_id']."' AND mb_idx = '".$nm_member['mb_idx']."' ";
	sql_query($sql_mb_update);

	// 사용내역 저장
	if($used=='y'){
		// 코믹스 화/권 타입
		$comics_up_kind = '화';
		if($comics['cm_up_kind'] == '1'){ $comics_up_kind = '권'; }
		$mpu_from = $comics['cm_series']."<br/>".$episode['ce_chapter'].$comics_up_kind."구매";
		
		// 외전 or 공지사항이라면...
		if(intval($episode['ce_chapter']) < 0){
			$mpu_from = $comics['cm_series']."<br/>".$episode['ce_notice'].$comics_up_kind."구매";
			if($episode['ce_outer'] == 'y'){ // 외전이라면...
				$mpu_from = $comics['cm_series']."<br/>"."외전 ".$episode['ce_outer_chapter'].$comics_up_kind."구매";
			}
		}
		cs_set_member_point_used_expen($comics, $nm_member, $mb_cash_point_use, $mb_point_use, $mb_cash_point_balance, $mb_point_balance, 1, $mpu_from, $comics_way);
		// 코믹스, 회원, 사용캐쉬포인트, 사용포인트, 회원캐쉬포인트, 회원포인트, 에피소드갯수, 내역, 구매방법
	}

	return $comics_buy;
}

function cs_set_expen_myepisode($comics, $episode, $nm_member, $comics_buy, $comics_way=1, $used='y', $episode_pay, $free_pay)
{	
	// 코믹스Object 에피소드Object 회원Object 코믹스구매Object,코믹스소장타입, member_point_used 사용 여부, 실제 화 가격, 무료화 소장 가격

	global $nm_config;

	// 회원정보 포인트 처리 후 재로드을 위해서...
	$nm_member = mb_get($nm_member['mb_id']);

	// 소장타입(0:무료, 1:소장, 2:대여, 3:에러)
	$mbe_end_date = "3000-01-01";
	if($comics['cm_own_type'] == '2'){
		$mbe_end_date = date("Y-m-d", strtotime(NM_TIME_YMD."+".$comics['cm_s_time']."day"));			
	}
	$comics_buy['mbe_end_date'] = $mbe_end_date;
	
	// 코믹스 가격
	// $cm_pay  = intval($comics['cm_pay']);
	$cm_pay = 0;
	if($episode_pay > 0 && $free_pay == 0) {
		$cm_pay = intval($episode_pay);
	} else if($free_pay > 0 && $episode_pay == 0) {
		$cm_pay = intval($free_pay);
	} // end else if

	// 회원 포인트 계산
	$mb_point = intval($nm_member['mb_point']);
	$mb_cash_point = intval($nm_member['mb_cash_point']);

	// 포인트 10 자리 계산 처리
	$mb_point_decimal_remainder = intval($mb_point % NM_POINT);
	$mb_point_decimal = intval($mb_point / NM_POINT) * NM_POINT;

	// 캐쉬 결제
	$mb_cash_point_balance =  $mb_cash_point - $cm_pay;
	$mb_cash_point_use = $cm_pay;
	
	$mb_point_balance = $mb_point;
	$mb_point_use = 0;
	
	// 캐쉬포인트가 없으면... 포인트사용
	if($mb_cash_point_balance < 0){
		$mb_point_use  = abs(intval($mb_cash_point_balance * NM_POINT));
		$mb_point_balance = $mb_point_decimal - $mb_point_use + $mb_point_decimal_remainder;
		
		// 캐쉬포인트 처리
		$mb_cash_point_balance = 0;
		$mb_cash_point_use = $cm_pay - intval($mb_point_use / NM_POINT) ;
	}
	$comics_buy['mb_cash_point'] = $mb_cash_point_use;
	$comics_buy['mb_point'] = $mb_point_use;

	$comics_buy['sl_all_pay'] =  0;
	$comics_buy['sl_pay'] =  1;
	if($comics_way != 1){
		$comics_buy['sl_all_pay'] =  1;
		$comics_buy['sl_pay'] =  0;
	}
	
	if($mb_point_balance < 0){
		$msg = "결제 비용이 부족합니다. 결제페이지로 넘어갑니다.";
		cs_alert($msg, NM_URL."/recharge.php"."?redirect=comicsview.php?comics=".$comics['cm_no']."__nn__episode=".$episode['ce_no']);
		die;
	}else{ }	

	/* 연령 */
	$mb_age_group = mb_get_age_group($nm_member['mb_birth']);
	$comics_buy['mb_age_group'] = $mb_age_group;

	/* 결제환경 */
	$cs_brow = get_brow(HTTP_USER_AGENT);
	$cs_os = get_os(HTTP_USER_AGENT);

	/* 해당 코믹스 구매 체크 */
	$sql_mpee = "SELECT  count(*) as total_mpee FROM member_point_expen_episode_".$comics['cm_big']."  WHERE 1 AND mpee_comics = '".$comics['cm_no']."' AND mpee_episode = '".$episode['ce_no']."' AND mpee_member = '".$nm_member['mb_no']."' AND mpee_member_idx = '".$nm_member['mb_idx']."' ";	
	$total_mpee = sql_count($sql_mpee, 'total_mpee');

	/*
	if($total_mpee!=0){
		$msg = "이미 구매한 코믹스 화를 선택되어있습니다. 이중결재를 막기위해 다시 구매절차를 진행해주세요. code:200";
		cs_alert($msg, NM_URL."/comics.php?comics=".$comics['cm_no']);
	}
	*/

	// 코믹스 구매 입력 - member_point_expen
	$sql_mpec = "SELECT  count(*) as total_mpec FROM member_point_expen_comics WHERE 1 AND mpec_comics = '".$comics['cm_no']."' AND mpec_big = '".$comics['cm_big']."' AND mpec_member = '".$nm_member['mb_no']."' AND mpec_member_idx = '".$nm_member['mb_idx']."' ";	
	$total_mpec = sql_count($sql_mpec, 'total_mpec');
	
	if($total_mpec!=0){
		$sql_mpec_update = "
		UPDATE member_point_expen_comics SET 
		mpec_big = '".$comics['cm_big']."', 
		mpec_small = '".$comics['cm_small']."', 

		mpec_cash_type = '".$nm_config['cf_cup_type']."', 
		mpec_cash_point= (mpec_cash_point+".intval($mb_cash_point_use)."), 
		mpec_point= (mpec_point+".intval($mb_point_use)."), 
		mpec_count= (mpec_count+1), 
		mpec_cash_point_balance = '".$mb_cash_point_balance."', 
		mpec_point_balance = '".$mb_point_balance."', 

		mpec_state = '1', 
		mpec_way = '".$comics_way."', 
		mpec_age = '".$mb_age_group."', 
		mpec_sex = '".$nm_member['mb_sex']."', 

		mpec_os = '".$cs_os."', 
		mpec_brow = '".$cs_brow."', 
		mpec_user_agent = '".HTTP_USER_AGENT."', 
		mpec_version = '".NM_VERSION."', 

		mpec_year_month = '".NM_TIME_YM."', 
		mpec_year = '".NM_TIME_Y."', 
		mpec_month = '".NM_TIME_M."', 
		mpec_day = '".NM_TIME_D."', 
		mpec_hour = '".NM_TIME_H."', 
		mpec_week = '".NM_TIME_W."', 
		mpec_date = '".NM_TIME_YMDHIS."' 

		WHERE 1 AND mpec_comics = '".$comics['cm_no']."' AND mpec_member = '".$nm_member['mb_no']."' AND mpec_member_idx = '".$nm_member['mb_idx']."' 
				AND mpec_big='".$comics['cm_big']."' 
				AND mpec_year_month='".NM_TIME_YM."' 
				AND mpec_year='".NM_TIME_Y."' 
				AND mpec_month='".NM_TIME_M."' 
				AND mpec_day='".NM_TIME_D."' 
				AND mpec_hour='".NM_TIME_H."' 
				AND mpec_week='".NM_TIME_W."' 		
		";
		sql_query($sql_mpec_update);
	}else{
		$sql_mpec_insert = "
		INSERT INTO member_point_expen_comics (
		mpec_member, mpec_member_idx, 

		mpec_comics, mpec_big, mpec_small, 

		mpec_cash_type, mpec_cash_point, mpec_point, mpec_count, mpec_cash_point_balance, 	mpec_point_balance, 

		mpec_state, mpec_way, mpec_age, mpec_sex, 

		mpec_os, mpec_brow, mpec_user_agent, mpec_version, 

		mpec_year_month, mpec_year, mpec_month, mpec_day, mpec_hour, mpec_week, mpec_date 
		) VALUES (
		'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 

		'".$comics['cm_no']."', '".$comics['cm_big']."', '".$comics['cm_small']."', 

		'".$nm_config['cf_cup_type']."', '".$mb_cash_point_use."', '".$mb_point_use."', '1', '".$mb_cash_point_balance."', '".$mb_point_balance."', 

		'1', '".$comics_way."', '".$mb_age_group."', '".$nm_member['mb_sex']."', 

		'".$cs_os."', '".$cs_brow."', '".HTTP_USER_AGENT."', '".NM_VERSION."', 

		'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', '".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."' 
		)";
		sql_query($sql_mpec_insert);
	}	
	
	// 코믹스 구매 번호 구하기 - member_point_expen
	$sql_mpec_chk = "SELECT * FROM member_point_expen_comics WHERE 1 AND mpec_comics = '".$comics['cm_no']."' AND mpec_member = '".$nm_member['mb_no']."' AND mpec_member_idx = '".$nm_member['mb_idx']."' ";	
	$row_mpec_chk = sql_fetch($sql_mpec_chk);

	// 코믹스 에피소드 구매 입력
	$sql_mpee_insert = "
	INSERT INTO member_point_expen_episode_".$comics['cm_big']." (
	mpee_member, mpee_member_idx, 

	mpec_no, mpee_comics, mpee_small, mpee_episode, mpee_chapter, 

	mpee_cash_type, mpee_cash_point, mpee_point, mpee_cash_point_balance, mpee_point_balance, 

	mpee_state, mpee_sale, mpee_age, mpee_way, mpee_sex, 

	mpee_os, mpee_brow, mpee_user_agent, mpee_version, 

	mpee_year_month, mpee_year, mpee_month, mpee_day, mpee_hour, mpee_week, mpee_date
	) VALUES (
	'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 

	'".$row_mpec_chk['mpec_no']."', '".$comics['cm_no']."', '".$comics['cm_small']."', '".$episode['ce_no']."', '".$episode['ce_chapter']."', 

	'".$nm_config['cf_cup_type']."', '".$mb_cash_point_use."', '".$mb_point_use."', '".$mb_cash_point_balance."', '".$mb_point_balance."', 

	'1', '".$episode['ce_event_sale']."', '".$mb_age_group."', '".$comics_way."', '".$nm_member['mb_sex']."', 

	'".$cs_os."', '".$cs_brow."', '".HTTP_USER_AGENT."', '".NM_VERSION."', 

	'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', '".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."' 
	)";
	sql_query($sql_mpee_insert);
	
	// 소장 정보 코믹스만 등록 -> 등록 실패시 false 반환
	$cs_mb_comics_bookmark = cs_mb_comics_bookmark($comics, $episode, $nm_member);

	// 소장 정보 화 등록
	$sql_mbe = "SELECT  count(*) as total_mbe FROM member_buy_episode_".$comics['cm_big']."  
				WHERE 1 AND mbe_comics = '".$comics['cm_no']."' 
					AND mbe_episode = '".$episode['ce_no']."' 
					AND mbe_member = '".$nm_member['mb_no']."' 
					AND mbe_member_idx = '".$nm_member['mb_idx']."' ";					
	$total_mbe = sql_count($sql_mbe, 'total_mbe');

	if($total_mbe!=0){	
		$sql_mbe_update = "
		UPDATE member_buy_episode_".$comics['cm_big']." SET 
		mbe_chapter = '".$episode['ce_chapter']."', 
		mbe_order = '".$episode['ce_order']."', 
		mbe_way = '".$comics_way."', 
		mbe_own_type = '".$comics['cm_own_type']."', 
		mbe_end_date = '".$mbe_end_date."', 
		mbe_date = '".NM_TIME_YMDHIS."' 

		WHERE 1 AND mbe_comics = '".$comics['cm_no']."' 
					AND mbe_episode = '".$episode['ce_no']."' 
					AND mbe_member = '".$nm_member['mb_no']."' 
					AND mbe_member_idx = '".$nm_member['mb_idx']."' ";	
		sql_query($sql_mbe_update);

	}else{
		$sql_mbe_insert = "
		INSERT INTO member_buy_episode_".$comics['cm_big']." (
		mbe_chapter, mbe_order, mbe_way, mbe_own_type, mbe_end_date, mbe_date, 
		mbe_comics, mbe_episode, mbe_member, mbe_member_idx
		) VALUES (
		'".$episode['ce_chapter']."', '".$episode['ce_order']."', '".$comics_way."', '".$comics['cm_own_type']."',  '".$mbe_end_date."',  '".NM_TIME_YMDHIS."', 
		'".$comics['cm_no']."', '".$episode['ce_no']."', '".$nm_member['mb_no']."',  '".$nm_member['mb_idx']."' 
		)";
		sql_query($sql_mbe_insert);
	}

	// 멤버 포인트 차감
	$sql_mb_update = "
	UPDATE member SET 
	mb_cash_point = '".$mb_cash_point_balance."', 
	mb_point = '".$mb_point_balance."' 
	WHERE 1 AND mb_id = '".$nm_member['mb_id']."' AND mb_idx = '".$nm_member['mb_idx']."' ";
	sql_query($sql_mb_update);

	// 사용내역 저장
	if($used=='y'){
		/* 무료화 소장일때 구분 문자 추가 0802 */
		$check_free = "";
		if($episode_pay == 0) {
			$check_free = "_F";
		} // end if
		// 코믹스 화/권 타입
		$comics_up_kind = '화';
		if($comics['cm_up_kind'] == '1'){ $comics_up_kind = '권'; }
		$mpu_from = $comics['cm_series']."<br/>".$episode['ce_chapter'].$comics_up_kind."구매".$check_free;
		
		// 외전 or 공지사항이라면...
		if(intval($episode['ce_chapter']) < 0){
			$mpu_from = $comics['cm_series']."<br/>".$episode['ce_notice'].$comics_up_kind."구매".$check_free;
			if($episode['ce_outer'] == 'y'){ // 외전이라면...
				$mpu_from = $comics['cm_series']."<br/>"."외전 ".$episode['ce_outer_chapter'].$comics_up_kind."구매".$check_free;
			}
		}
		cs_set_member_point_used_expen($comics, $nm_member, $mb_cash_point_use, $mb_point_use, $mb_cash_point_balance, $mb_point_balance, 1, $mpu_from, $comics_way);
		// 코믹스, 회원, 사용캐쉬포인트, 사용포인트, 회원캐쉬포인트, 회원포인트, 에피소드갯수, 내역, 구매방법
	}

	return $comics_buy;
}

function cs_set_sales_sex($nm_member, $filed)
{
	// 나이별
	switch($nm_member['mb_sex']){
		case 'm': $filed_type = $filed."_man";
		break;
		case 'w': $filed_type = $filed."_woman";
		break;
		default: $filed_type = $filed."_nocertify";
		break;
	}
	return $filed_type;
}


function cs_set_sales_data($comics, $episode, $nm_member, $comics_buy, $comics_free='n', $pay_type=1)
{
	cs_set_sales($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type);
	if($comics_free == 'n'){ cs_set_sales_age_sex($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type);	}
	return $comics_buy;
}


function cs_set_sales($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type)
{
	global $nm_config;

	$dbarray = array('sales', 'vsales', 'sales_episode', 'vsales_episode');

	$set_date['year_month']	= NM_TIME_YM;
	$set_date['year']		= NM_TIME_Y;
	$set_date['month']		= NM_TIME_M;
	$set_date['day']		= NM_TIME_D;
	$set_date['hour']		= NM_TIME_H;
	$set_date['week']		= NM_TIME_W;
	
	foreach($dbarray as $dbkey => $dbtable){
		if( ($dbkey+1) % 2 == 0  && intval($comics_buy['mb_point']) > 0 ){ continue; } // CP용 통계일 경우 포인트는 0

		$sql_pay_type = $sql_insert = $sql_update = "";

		switch($dbkey){
			case 0 :
			case 1 :// unique -> `sl_comics`, `sl_year_month`, `sl_year`, `sl_month`, `sl_day`, `sl_hour`, `sl_week`
					if($comics_free == 'n'){
						/*
						if($pay_type == 7){ $sql_pay_type = " sl_all_pay= (sl_all_pay+1), ";
						}else{ $sql_pay_type = " sl_pay= (sl_pay+1), "; }
						*/
						// 통세일 추가로 인해 변경180918
						if($pay_type == 1){ $sql_pay_type = " sl_pay= (sl_pay+1), ";
						}else{ $sql_pay_type = " sl_all_pay= (sl_all_pay+1), "; }
					}

					$sql_insert = "
						INSERT INTO ".$dbtable." (sl_comics, sl_big, sl_year_month, sl_year, sl_month, sl_day, sl_hour, sl_week 
						) VALUES ('".$comics['cm_no']."', '".$comics['cm_big']."', 
						'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
						'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."' 
					)";
					
					$sql_update = "
						UPDATE ".$dbtable." SET 
						sl_open= (sl_open+1), 
						sl_free= (sl_free+".intval($comics_buy['sl_free'])."), 
						sl_sale= (sl_sale+".intval($comics_buy['sl_sale'])."), 
						".$sql_pay_type." 
						sl_event= (sl_event+".intval($comics_buy['sl_event'])."), 
						sl_point= (sl_point+".intval($comics_buy['mb_point'])."), 
						sl_cash_point= (sl_cash_point+".intval($comics_buy['mb_cash_point'])."), 
						sl_cash_type = '".intval($nm_config['cf_cup_type'])."' 
						WHERE 1 AND sl_comics = '".$comics['cm_no']."' AND sl_big = '".$comics['cm_big']."' 
						AND sl_year_month = '".$set_date['year_month']."' AND sl_year = '".$set_date['year']."' 
						AND sl_month = '".$set_date['month']."' AND sl_day = '".$set_date['day']."' 
						AND sl_hour = '".$set_date['hour']."' AND sl_week = '".$set_date['week']."' 
					";

					// 연도+월 총합통계
					$sql_insert_w = "
						INSERT INTO w_".$dbtable." (sl_comics, sl_big, sl_year_month, sl_cash_type 
						) VALUES ('".$comics['cm_no']."', '".$comics['cm_big']."', 
						'".$set_date['year_month']."', '".intval($nm_config['cf_cup_type'])."' 
					)";
					
					$sql_update_w = "
						UPDATE w_".$dbtable." SET 
						sl_open= (sl_open+1), 
						sl_open= (sl_open+".intval($comics_buy['sl_open'])."), 
						sl_free= (sl_free+".intval($comics_buy['sl_free'])."), 
						sl_sale= (sl_sale+".intval($comics_buy['sl_sale'])."), 
						".$sql_pay_type." 
						sl_event= (sl_event+".intval($comics_buy['sl_event'])."), 
						sl_point= (sl_point+".intval($comics_buy['mb_point'])."), 
						sl_cash_point= (sl_cash_point+".intval($comics_buy['mb_cash_point'])."), 
						sl_cash_type = '".intval($nm_config['cf_cup_type'])."' 
						WHERE 1 AND sl_comics = '".$comics['cm_no']."' AND sl_big = '".$comics['cm_big']."' 
						AND sl_year_month = '".$set_date['year_month']."' 
					";

			break;

			case 2 :
			case 3 :// unique -> `sas_comics`, `sas_year_month`, `sas_year`, `sas_month`, `sas_day`, `sas_hour`, `sas_week`
					if($comics_free == 'n'){
						if($pay_type == 7){ $sql_pay_type = " se_all_pay= (se_all_pay+1), ";
						}else{ $sql_pay_type = " se_pay= (se_pay+1), "; }
					}
					$sql_insert = "
						INSERT INTO ".$dbtable."_".$comics['cm_big']." ( 
						se_comics, se_episode, se_year_month, se_year, se_month, se_day, se_hour, se_week 
						) VALUES ( '".$comics['cm_no']."', '".$episode['ce_no']."', 
						'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
						'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."' 
					)";
					
					$sql_update = "
						UPDATE ".$dbtable."_".$comics['cm_big']." SET 
						se_open= (se_open+1), 
						se_free= (se_free+".intval($comics_buy['sl_free'])."), 
						se_sale= (se_sale+".intval($comics_buy['sl_sale'])."), 
						".$sql_pay_type." 
						se_event= (se_event+".intval($comics_buy['sl_event'])."), 
						se_point= (se_point+".intval($comics_buy['mb_point'])."), 
						se_cash_point= (se_cash_point+".intval($comics_buy['mb_cash_point'])."), 
						se_cash_type = '".intval($nm_config['cf_cup_type'])."', 
						se_chapter = '".$episode['ce_chapter']."' 
						WHERE 1 AND se_comics = '".$comics['cm_no']."' AND se_episode = '".$episode['ce_no']."' 
						AND se_year_month = '".$set_date['year_month']."' AND se_year = '".$set_date['year']."' 
						AND se_month = '".$set_date['month']."' AND se_day = '".$set_date['day']."' 
						AND se_hour = '".$set_date['hour']."' AND se_week = '".$set_date['week']."' 
					";
					
					// 연도+월 총합통계
					$sql_insert_w = "
						INSERT INTO w_".$dbtable."_".$comics['cm_big']." ( 
						se_comics, se_episode, se_year_month 
						) VALUES ( '".$comics['cm_no']."', '".$episode['ce_no']."', 
						'".$set_date['year_month']."' 
					)";
					
					$sql_update_w = "
						UPDATE w_".$dbtable."_".$comics['cm_big']." SET 
						se_open= (se_open+1), 
						se_free= (se_free+".intval($comics_buy['sl_free'])."), 
						se_sale= (se_sale+".intval($comics_buy['sl_sale'])."), 
						".$sql_pay_type." 
						se_event= (se_event+".intval($comics_buy['sl_event'])."), 
						se_point= (se_point+".intval($comics_buy['mb_point'])."), 
						se_cash_point= (se_cash_point+".intval($comics_buy['mb_cash_point'])."), 
						se_cash_type = '".intval($nm_config['cf_cup_type'])."', 
						se_chapter = '".$episode['ce_chapter']."' 
						WHERE 1 AND se_comics = '".$comics['cm_no']."' AND se_episode = '".$episode['ce_no']."' 
						AND se_year_month = '".$set_date['year_month']."' 
					";
			break;
		}
		// 저장
		@mysql_query($sql_insert); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
		sql_query($sql_update);

		// 연도+월 총합통계
		@mysql_query($sql_insert_w); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
		sql_query($sql_update_w);
	}
}

function cs_set_sales_age_sex($comics, $episode, $nm_member, $comics_buy, $comics_free, $pay_type, $dbtable='sales_age_sex')
{
	global $nm_config;

	$dbarray = array('sales_age_sex', 'vsales_age_sex', 'sales_episode', 'vsales_episode');

	$set_date['year_month']	= NM_TIME_YM;
	$set_date['year']		= NM_TIME_Y;
	$set_date['month']		= NM_TIME_M;
	$set_date['day']		= NM_TIME_D;
	$set_date['hour']		= NM_TIME_H;
	$set_date['week']		= NM_TIME_W;
	
	foreach($dbarray as $dbkey => $dbtable){
		if( ($dbkey+1) % 2 == 0  && intval($comics_buy['mb_point']) > 0 ){ continue; } // CP용 통계일 경우 포인트는 0

		$sql_sex = $sex_field = $sql_insert = $sql_update = "";

		switch($dbkey){
			case 0 :
			case 1 :// unique -> `sas_comics`, `sas_year_month`, `sas_year`, `sas_month`, `sas_day`, `sas_hour`, `sas_week`
					// 성별
					$sex_field = cs_set_sales_sex($nm_member, 'sas');
					$sql_sex = " ".$sex_field."= (".$sex_field."+1) ";

					// 나이별
					if($nm_member['mb_birth'] != ''){
						$sql_sex.= ", ".$sex_field.mb_get_age_group($nm_member['mb_birth'])."= (".$sex_field.mb_get_age_group($nm_member['mb_birth'])."+1) ";
					}

					$sql_insert = "
						INSERT INTO ".$dbtable." ( sas_comics, sas_big, 
						sas_year_month, sas_year, sas_month, sas_day, sas_hour, sas_week 
						) VALUES ('".$comics['cm_no']."', '".$comics['cm_big']."', 
						'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
						'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."' 
					)";
					
					$sql_update = "
					UPDATE ".$dbtable." SET ".$sql_sex." WHERE 1 
					AND sas_comics = '".$comics['cm_no']."' AND sas_big = '".$comics['cm_big']."' 
					AND sas_year_month = '".$set_date['year_month']."' AND sas_year = '".$set_date['year']."' 
					AND sas_month = '".$set_date['month']."' AND sas_day = '".$set_date['day']."' 
					AND sas_hour = '".$set_date['hour']."' AND sas_week = '".$set_date['week']."' 
					";

					// 연도+월 총합통계
					$sql_insert_w = "
						INSERT INTO w_".$dbtable." ( sas_comics, sas_big, 
						sas_year_month 
						) VALUES ('".$comics['cm_no']."', '".$comics['cm_big']."', 
						'".$set_date['year_month']."' 
					)";
					
					$sql_update_w = "
					UPDATE w_".$dbtable." SET ".$sql_sex." WHERE 1 
					AND sas_comics = '".$comics['cm_no']."' AND sas_big = '".$comics['cm_big']."' 
					AND sas_year_month = '".$set_date['year_month']."' 
					";
			break;

			case 2 :
			case 3 :// unique -> `seas_comics`, `seas_episode`, `seas_year_month`, `seas_year`, `seas_month`, `seas_day`, `seas_hour`, `seas_week`
					// 성별
					$sex_field = cs_set_sales_sex($nm_member, 'seas');
					$sql_sex = " ".$sex_field."= (".$sex_field."+1) ";

					// 나이별
					if($nm_member['mb_birth'] != ''){
						$sql_sex.= ", ".$sex_field.mb_get_age_group($nm_member['mb_birth'])."= (".$sex_field.mb_get_age_group($nm_member['mb_birth'])."+1) ";
					}

					$sql_insert = "
						INSERT INTO ".$dbtable."_".$comics['cm_big']."_age_sex ( seas_comics, seas_episode, 
						seas_year_month, seas_year, seas_month, seas_day, seas_hour, seas_week 
						) VALUES ( '".$comics['cm_no']."', '".$episode['ce_no']."', 
						'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
						'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."' 
					)";
					
					$sql_update = "
						UPDATE ".$dbtable."_".$comics['cm_big']."_age_sex SET ".$sql_sex." , seas_chapter='".$episode['ce_chapter']."'  WHERE 1 
						AND seas_comics = '".$comics['cm_no']."' AND seas_episode = '".$episode['ce_no']."' 
						AND seas_year_month = '".$set_date['year_month']."' AND seas_year = '".$set_date['year']."' 
						AND seas_month = '".$set_date['month']."' AND seas_day = '".$set_date['day']."' 
						AND seas_hour = '".$set_date['hour']."' AND seas_week = '".$set_date['week']."' 
					";

					// 연도+월 총합통계
					$sql_insert_w = "
						INSERT INTO w_".$dbtable."_".$comics['cm_big']."_age_sex ( seas_comics, seas_episode, 
						seas_year_month 
						) VALUES ( '".$comics['cm_no']."', '".$episode['ce_no']."', 
						'".$set_date['year_month']."' 
					)";
					
					$sql_update_w = "
						UPDATE w_".$dbtable."_".$comics['cm_big']."_age_sex SET ".$sql_sex." , seas_chapter='".$episode['ce_chapter']."'  WHERE 1 
						AND seas_comics = '".$comics['cm_no']."' AND seas_episode = '".$episode['ce_no']."' 
						AND seas_year_month = '".$set_date['year_month']."' 
					";
			break;
		}
		// 저장
		@mysql_query($sql_insert); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
		sql_query($sql_update);

		// 연도+월 총합통계
		@mysql_query($sql_insert_w); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
		sql_query($sql_update_w);
	}
}


function cs_episode_list($episode)
{
	// 에피소드Object
	$episode_list = explode(", ",$episode['ce_file_list']);

	foreach($episode_list as $episode_key => &$episode_val){ 
		$episode_val = img_url_para($episode['ce_file'].'/'.$episode_val, $episode['ce_reg_date'], $episode['ce_mod_date']);
	}
	return $episode_list;
}

function cs_mb_comics_bookmark($comics, $episode, $nm_member, $free='n')
{
	// 코믹스Object 에피소드Object 회원Object

	global $nm_config;
	
	$state = true;
	// 소장 정보 등록
	$sql_mbc = "SELECT  count(*) as total_mbc FROM member_buy_comics 
				WHERE 1 AND mbc_comics = '".$comics['cm_no']."' 
					AND mbc_big = '".$comics['cm_big']."' 
					AND mbc_member = '".$nm_member['mb_no']."' 
					AND mbc_member_idx = '".$nm_member['mb_idx']."' ";
					
	$total_mbc = sql_count($sql_mbc, 'total_mbc');
	
	// 무료 체크
	$cm_own_type = $comics['cm_own_type'];
	if($free == 'y'){ $cm_own_type = 0; }

	if($total_mbc!=0){	
		$sql_mbc_update = "
		UPDATE member_buy_comics SET 
		mbc_view = 'n', 
		mbc_recent = 'n', 
		mbc_recent_chapter = '".$episode['ce_chapter']."', 
		mbc_own_type = '".$cm_own_type."', 
		mbc_date = '".NM_TIME_YMDHIS."' 

		WHERE 1 AND mbc_comics = '".$comics['cm_no']."' 
					AND mbc_big = '".$comics['cm_big']."' 
					AND mbc_member = '".$nm_member['mb_no']."' 
					AND mbc_member_idx = '".$nm_member['mb_idx']."' ";
		if(sql_query($sql_mbc_update)){

		}else{
			$state = false;
		}
	}else{
		$sql_mbc_insert = "
		INSERT INTO member_buy_comics (
		mbc_view, mbc_recent_chapter, mbc_own_type, mbc_date, 
		mbc_comics, mbc_big, mbc_member, mbc_member_idx
		) VALUES (
		'n', '".$episode['ce_chapter']."', '".$comics['cm_own_type']."',  '".NM_TIME_YMDHIS."', 

		'".$comics['cm_no']."', '".$comics['cm_big']."', '".$nm_member['mb_no']."',  '".$nm_member['mb_idx']."' 
		)";
		if(sql_query($sql_mbc_insert)){

		}else{
			$state = false;
		}
	}
	
	return $state;
}

function cs_comic_bookmark_url($comics)
{
	$get_comics_bookmark_url = NM_URL."/mycomics.php?mbc=2&comics=".$comics;
	return $get_comics_bookmark_url;
}

function cs_mb_auto_key($member)
{	
	global $nm_config;

	// $key = md5($_SERVER['SERVER_ADDR'] . $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'] . $mb['mb_pass']);
	/* 위 키에서는 브라우저 업데이트시 자동로그인 풀림 */

	// $key = md5($_SERVER['SERVER_ADDR'] . REMOTE_ADDR . get_os(HTTP_USER_AGENT) . get_brow(HTTP_USER_AGENT) .$nm_config['nm_path'] . $member['mb_pass']);
	/* 위 키에서는 로드밸런스 서버라서 자동로그인 풀림 */

	// $key = md5(REMOTE_ADDR . get_os(HTTP_USER_AGENT) . get_brow(HTTP_USER_AGENT) .$nm_config['nm_path'] . $member['mb_pass']);
	/* 자동로그인이 풀린다는 항의에 의해 수정함 */

	$key = md5( $member['mb_no'] . $member['mb_id'] . $member['mb_pass']);


	return $key;
}



function cs_recharge_list($event='n', $select='', $mb_crp_no=0, $coupondc=0)
{
	global $nm_member; // 관리자일 경우 테스트 결제하려고
	
	// 회원 쿠폰번호 18-01-31 추가
	$coupondc_ck = false;
	$coupondc_no = intval($coupondc);
	$coupondc_percent = 0;
	
	$randombox_member_coupondc_get = array();
	if($coupondc_no > 0){ // 쿠폰정보 가져오기
		// 랜덤박스 lib 사용
		$randombox_member_coupondc_get = randombox_cs_recharge_list_coupondc_get($nm_member, $coupondc);

		if($randombox_member_coupondc_get['coupondc_ck'] == true && 
			intval($randombox_member_coupondc_get['coupondc_no']) > 0 && 
			intval($randombox_member_coupondc_get['coupondc_percent']) > 0){

			$coupondc_ck = $randombox_member_coupondc_get['coupondc_ck'];
			$coupondc_no = intval($randombox_member_coupondc_get['coupondc_no']);
			$coupondc_percent = intval($randombox_member_coupondc_get['coupondc_percent']);
		}
	}

	// 1.완전판, 2.이벤트결제상품(첫결제), 3.50땅콩할인제외, 4.할인권 쿠폰적용, 5.CMS적용-> 이벤트 충전/지급 이벤트

	// 결제상품 선택
	$sql_select = "";
	if($select != ''){ $sql_select= " AND crp_no = '".$select."' ";	}

	$return_arr = array();
	$er_arr = array();
	$sql_er = "SELECT * FROM event_recharge WHERE er_state = 'y' AND er_type = '1' ORDER BY er_reg_date desc LIMIT 0 , 1";
	$result_er = sql_query($sql_er);
	while ($row_er = sql_fetch_array($result_er)) { $er_arr = $row_er; }

	/* DB */
	$crp_arr = $crp_event_arr = array();
	$sql_crp = "SELECT * FROM config_recharge_price WHERE crp_state = 'y' $sql_select ORDER BY crp_cash_point, crp_date desc";
	$result_crp = sql_query($sql_crp);
	for ($r=0; $row_crp = sql_fetch_array($result_crp); $r++) {
		// 텍스트로 보여줄 가격 표기
		$row_crp['crp_won_txt'] = $row_crp['crp_won'];
		$row_crp['crp_coupondc_txt'] = 0;

		// view에서 select 표기
		$row_crp['crp_no_select'] = "";
		if($row_crp['crp_no'] == intval($mb_crp_no) && intval($mb_crp_no) > 0){ $row_crp['crp_no_select'] = "checked"; }

		// 1.완전판, 2.이벤트결제상품(첫결제), 3.50땅콩할인제외, 4.할인권 쿠폰적용, 5.CMS적용

		if ($event == 'n' && $coupondc_ck == false && is_app(1) && is_mobile() && floatval($_SESSION['appver']) >= floatval(NM_APP_SALE_VER)){
			/* 1. 이벤트가 아니고 쿠폰이 아닐 경우 & APP sale 이며 버전이 1.4일 경우 */
			$row_crp['crp_won'] = intval($row_crp['crp_won']) - ( intval($row_crp['crp_won']) / 100 * NM_APP_SALE );
				// 0.01 곱으로 십자리까지 소수점 만들어서 floor함수로 소수점 버리고 십자리까지 00 으로 처리
			$row_crp['crp_won'] = floor($row_crp['crp_won'] * 0.01) * 100;
			$row_crp['crp_coupondc_txt'] = NM_APP_SALE;
		}else if($row_crp['crp_cash_count_event'] != '' && $event == 'y'){ 
			/* 2. 이벤트 상품 제외 - 예시 첫결제 */
		}else if(intval($row_crp['crp_cash_point']) < 50 ) {
			/* 3. 50땅콩 미만인 결제 상품에는 보너스 지급 안되도록 2017-08-31 */	
		}else if($coupondc_ck == true){
			/* 4. 할인권 쿠폰일 경우 */
			$row_crp['crp_won'] = intval($row_crp['crp_won']) - ( intval($row_crp['crp_won']) / 100 * intval($coupondc_percent));
				// 0.01 곱으로 십자리까지 소수점 만들어서 floor함수로 소수점 버리고 십자리까지 00 으로 처리
			$row_crp['crp_won'] = floor($row_crp['crp_won'] * 0.01) * 100;
			$row_crp['crp_coupondc_txt'] = intval($coupondc_percent);
		}else{
				// 혹시 몰라서 한번 조건문 처리 => 이벤트가 아니고 쿠폰이 아닐 경우
			if($event == 'n' && $coupondc_ck == false){ // 이벤트 충전/지급 이벤트
				$row_crp['er_no'] = intval($er_arr['er_no']);
				if($er_arr['er_calc_way'] == 'a'){
					$row_crp['er_cash_point'] = $er_arr['er_cash_point'];
					$row_crp['er_point'] = $er_arr['er_point'];
				}else if($er_arr['er_calc_way'] == 'p'){	
					$row_crp['er_cash_point'] = intval($er_arr['er_cash_point']) / 100 * intval($row_crp['crp_cash_point']);
					$row_crp['er_cash_point'] = floor($row_crp['er_cash_point']);
					$row_crp['er_point'] = intval($er_arr['er_point']) / 100 * intval($row_crp['crp_point']);
					$row_crp['er_point'] = floor($row_crp['er_point']);
				}else if($er_arr['er_calc_way'] == 'm'){ 
					$row_crp['er_cash_point'] = intval($row_crp['crp_cash_point'] * $er_arr['er_cash_point']) - intval($row_crp['crp_cash_point']);
					$row_crp['er_point'] = intval($row_crp['crp_point'] * $er_arr['er_point']) - intval($row_crp['crp_point']);
				}else if($er_arr['er_calc_way'] == 'd'){ 
					$row_crp['crp_won'] = intval($row_crp['crp_won']) - ( intval($row_crp['crp_won']) / 100 * intval($er_arr['er_dc_percent']));
					// 0.01 곱으로 십자리까지 소수점 만들어서 floor함수로 소수점 버리고 십자리까지 00 으로 처리
					$row_crp['crp_won'] = floor($row_crp['crp_won'] * 0.01) * 100;
					$row_crp['crp_coupondc_txt'] = intval($er_arr['er_dc_percent']);
				}
			}
		}

		
		// 텍스트로 보여줄 가격 표기
		$row_crp['crp_coupondc_won_txt'] = intval($row_crp['crp_won_txt']) - intval($row_crp['crp_won']);

		$row_crp['er_cash_point'] = intval($row_crp['er_cash_point']);
		$row_crp['er_point'] = intval($row_crp['er_point']);

		$row_crp['sum_cash_point'] = intval($row_crp['crp_cash_point'] + $row_crp['er_cash_point']);  
		$row_crp['sum_point'] = intval($row_crp['crp_point'] + $row_crp['er_point']);  
		
		// 수단제약
		$row_crp['crp_payway_limit_js'] = str_replace("|", ",", $row_crp['crp_payway_limit']);

		// 저장
		if($row_crp['crp_cash_count_event'] == ''){
			array_push($crp_arr,  $row_crp); // 기본 
		}else{
			array_push($crp_event_arr,  $row_crp); // 결제 횟수 이벤트
		}
	}
	
	$return_arr = $crp_arr;
	if($event == 'y'){ $return_arr = $crp_event_arr; }

	return $return_arr;
}

function cs_goto_referer()
{
	$goto_url = HTTP_REFERER;
	if(stripos(HTTP_REFERER, $_SERVER['PHP_SELF']) == true){
		$goto_url = NM_URL;
	}
	if(HTTP_REFERER == ''){
		$goto_url = NM_URL;
	}
	return $goto_url;
}

function cs_login_check($ss_cs_login_check_http_referer="")
{ // 로그인 체크
	global $nm_member;
	if($nm_member['mb_no'] == '' || $nm_member['mb_no'] == NULL || intval($nm_member['mb_no']) <= 0){
		$goto_url = NM_URL."/ctlogin.php";
		// depth 많다고 하셔서 수정됨 조차장님 수정사항 171206
		// cs_alert('로그인을 확인해주시기 바랍니다.', $goto_url);
		if($ss_cs_login_check_http_referer){
			set_session('ss_cs_login_check_http_referer', urlencode($ss_cs_login_check_http_referer));			
		}else{
			set_session('ss_cs_login_check_http_referer', urlencode(NM_DOMAIN.$_SERVER['REQUEST_URI']));
		}
		goto_url($goto_url);
		die;
	}
}


/* 181001 */
function cs_login_redirect($http_referer="")
{ // 로그인 체크 /* 181001 */	
	
	global $nm_member, $_redirect;	
	
	if($http_referer != ''){
		$redirect_referer = get_redirect($http_referer);
	}else{
		if($_redirect != ''){
			$redirect_referer = get_redirect($_redirect);
		}else{
			$redirect_referer = get_redirect(HTTP_REFERER);
		}
	}

	if($nm_member['mb_no'] == '' || $nm_member['mb_no'] == NULL || intval($nm_member['mb_no']) <= 0){
		$goto_url = NM_URL."/ctlogin.php?redirect=".urlencode($redirect_referer);
		goto_url($goto_url);
		die;
	}
}

function cs_cash_name($recharge)
{ // 상품명 => 상품캐쉬명_이벤트캐쉬명
	global $nm_config;
	$crp_cash_name = $recharge['crp_cash_point'].$nm_config['cf_cash_point_unit'];
	if($recharge['er_cash_point'] > 0){
		$crp_cash_name = $recharge['crp_cash_point'].$nm_config['cf_cash_point_unit']."_".$recharge['er_cash_point'].$nm_config['cf_cash_point_unit']."_event";
	}
	return $crp_cash_name;
}

function cs_payway($payway, $device='mobile')
{ // pc사 파일 경로
	global $nm_config;

	$pg = array();

	switch($payway){
		case 'kcp':
			if($device == 'pc'){
				$pg['url'] = NM_KCP_PC_URL;
				$pg['path'] = NM_KCP_PC_PATH;
			}else{
				$pg['url'] = NM_KCP_MO_URL;
				$pg['path'] = NM_KCP_MO_PATH;
			}
		break;
		case 'toss':
				$pg['url'] = NM_TOSS_URL;
				$pg['path'] = NM_TOSS_PATH;
		break;
		case 'payco':
				$pg['url'] = NM_PAYCO_URL;
				$pg['path'] = NM_PAYCO_PATH;
		break;
	}
	return $pg;
}


function cs_kcp_method($kcp_method)
{ // kcp 결제 수단 번호
	$kcp_method_no = 0;
	switch($kcp_method){
		// mobx:폰, card:카드, acnt:계좌이체, scbl:도서문화상품권, schm:해피머니, sccl:문화상품권
		case 'mobx': $kcp_method_no = "000010000000";
		break;
		case 'card': $kcp_method_no = "100000000000";
		break;
		case 'acnt': $kcp_method_no = "010000000000";
		break;
		case 'scbl': $kcp_method_no = "000000001000";
		break;
		case 'schm': $kcp_method_no = "000000001000";
		break;
		case 'sccl': $kcp_method_no = "000000001000";
		break;
	}
	return $kcp_method_no;
}

function cs_pg_channel_kcp($nm_member, $kcp_order, $kcp_amt=0)
{
	$sql_kcp_amt = "";
	if($kcp_amt > 0){ $sql_kcp_amt = " AND kcp_amt = '".$kcp_amt."' "; }

	$sql_kcp = "SELECT * FROM pg_channel_kcp 
	WHERE kcp_member = '".$nm_member['mb_no']."' AND kcp_member_id = '".$nm_member['mb_id']."' AND kcp_member_idx = '".$nm_member['mb_idx']."' AND 
	kcp_order = '".$kcp_order."' ".$sql_kcp_amt." ";
	$row = sql_fetch($sql_kcp);
	return $row;
}

function cs_pg_channel_toss($nm_member, $toss_order)
{
	$sql_toss = "SELECT * FROM pg_channel_toss 
	WHERE toss_member = '".$nm_member['mb_no']."' AND toss_member_id = '".$nm_member['mb_id']."' AND toss_member_idx = '".$nm_member['mb_idx']."' AND 
	toss_order = '".$toss_order."' ";
	$row = sql_fetch($sql_toss);
	return $row;
}

function cs_pg_channel_payco($nm_member, $payco_order)
{
	$sql_payco = "SELECT * FROM pg_channel_payco 
	WHERE payco_member = '".$nm_member['mb_no']."' AND payco_member_id = '".$nm_member['mb_id']."' AND payco_member_idx = '".$nm_member['mb_idx']."' AND 
	payco_order = '".$payco_order."' ";
	$row = sql_fetch($sql_payco);
	return $row;
}


function cs_member_point_income($nm_member, $order=0)
{
	$sql_order = "";
	if($order > 0){ $sql_order = " AND mpi_order = '".$order."' "; }

	$sql_mpi = "SELECT * FROM member_point_income 
	WHERE mpi_member = '".$nm_member['mb_no']."' AND mpi_member_idx = '".$nm_member['mb_idx']."' ".$sql_order." ";
	$row = sql_fetch($sql_mpi);
	return $row;
}


function cs_set_member_point_income($nm_member, $cash_point, $point, $won, $from='', $order='', $state='1', $free='n', $used='y')
{
	$boolean = false;
	$cancel = 'n';
	// 2:취소, 3:환불, 6:에러
	switch($state){
		case 2:
		case '2':
		case 3:
		case '3':
		case 6:
		case '6':
		$cancel = 'y';
	}

	/* 181001 */
	$mb_won_count = 1;
	if($cancel == 'y'){ $mb_won_count = -1; }
	
	$mpi_cash_point_balance = intval($nm_member['mb_cash_point'] + $cash_point);
	$mpi_point_balance = intval($nm_member['mb_point'] + $point);

	$mpi_age = intval(mb_get_age_group($nm_member['mb_birth']));
	$mpi_kind = "bw:".get_brow(HTTP_USER_AGENT)." & os:".get_brow(HTTP_USER_AGENT);

	$sql_mpi = "INSERT INTO member_point_income (
	mpi_member, mpi_member_idx, 
	mpi_won, mpi_cash_point, mpi_point, 
	mpi_cash_point_balance, mpi_point_balance, 
	mpi_state, mpi_free, 

	mpi_age, mpi_sex, 
	mpi_kind, mpi_user_agent, mpi_version, mpi_from, mpi_order, 

	mpi_year_month, mpi_year, mpi_month, mpi_day, 
	mpi_hour, mpi_week, mpi_date 
	) VALUES (
	'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 
	'".$won."', '".$cash_point."', '".$point."', 
	'".$mpi_cash_point_balance."', '".$mpi_point_balance."', 
	'".$state."', '".$free."', 

	'".$mpi_age."', '".$nm_member['mb_sex']."', 
	'".$mpi_kind."', '".HTTP_USER_AGENT."', '".NM_VERSION."', '".$from."', '".$order."', 
	
	'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', 
	'".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."'
	)";

	if(sql_query($sql_mpi)){ 
		$boolean = true; 
		$boolean = mb_update_point($nm_member, $cash_point, $point, $won, $mb_won_count); // member update

		// 사용내역 저장
		if($used == 'y'){
			$boolean = cs_set_member_point_used_income($nm_member, $cash_point, $point, $won, $from, '', $order, 'r', $free, $cancel);
			// 회원정보, 캐쉬포인트, 포인트, 원, 내역, 이벤트타입, 충전주문번호, 내역구분, 무료여부, 결제취소여부
		}
	}

	return $boolean;
}


function cs_set_member_point_income_refund($nm_member, $order='', $date='', $mpi_refund='1')
{
	$boolean = false;

	$set_date['year_month']	= substr($date, 0, 7);
	$set_date['year']		= substr($date, 0, 4);
	$set_date['month']		= substr($date, 5, 2);
	$set_date['day']		= substr($date, 8, 2);
	$set_date['hour']		= substr($date, 11, 2);

	$sql_mpi = "UPDATE member_point_income SET mpi_refund = ".$mpi_refund." WHERE 
	mpi_member = '".$nm_member['mb_no']."' AND mpi_member_idx = '".$nm_member['mb_idx']."' AND mpi_order = '".$order."' 
	AND mpi_year_month = '".$set_date['year_month']."' AND mpi_year = '".$set_date['year']."' 
	AND mpi_month = '".$set_date['month']."' AND mpi_day = '".$set_date['day']."' 
	AND mpi_hour = '".$set_date['hour']."'
	";

	if(sql_query($sql_mpi)){ 
		$boolean = true; 
	}

	return $boolean;
}

function cs_set_member_point_income_event($nm_member, $cash_point, $point, $type, $from='', $used='y')
{
	$boolean = false;
	
	$mpie_cash_point_balance = intval($nm_member['mb_cash_point'] + $cash_point);
	$mpie_point_balance = intval($nm_member['mb_point'] + $point);

	$sql_mpie = "INSERT INTO member_point_income_event (
	mpie_member, mpie_member_idx, 
	mpie_cash_point, mpie_point, 
	mpie_cash_point_balance, mpie_point_balance, 

	mpie_from, mpie_type, 

	mpie_year_month, mpie_year, mpie_month, mpie_day, 
	mpie_hour, mpie_week, mpie_date 
	) VALUES (
	'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 
	'".$cash_point."', '".$point."', 
	'".$mpie_cash_point_balance."', '".$mpie_point_balance."', 

	'".$from."', '".$type."', 
	
	'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', 
	'".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."'
	)";
	if(sql_query($sql_mpie)){ 
		$boolean = true; 
		$boolean = mb_update_point($nm_member, $cash_point, $point); // member update

		// 사용내역 저장
		if($used == 'y'){
			$boolean = cs_set_member_point_used_income($nm_member, $cash_point, $point, 0, $from, $type, '', 'e', 'y');
			// 회원정보, 캐쉬포인트, 포인트, 원, 내역, 이벤트타입, 충전주문번호, 내역구분, 무료여부
		}
	}

	return $boolean;
}

function cs_set_sales_recharge($nm_member, $payway, $payway_name)
{
	// 결제정보 배열로 받기 => $payway
	// pc사 영문이름 => $payway_name

	$boolean = false;
	
	$sr_way = $payway_name;
	if($sr_way == "kcp"){ $sr_way = $payway['kcp_payway']; }

	$sr_way_payco = '';
	if($sr_way == "payco"){ $sr_way_payco = $payway['payco_payment_method_code']; }

	$sr_platform = cs_platform(HTTP_USER_AGENT);

	$sr_product = $payway[$payway_name.'_product'];
	$sr_crp_no = intval($payway[$payway_name.'_crp_no']);
	$sr_er_no = intval($payway[$payway_name.'_er_no']);

	$sr_cash_point = intval($payway[$payway_name.'_cash_point']); 
	$sr_point = intval($payway[$payway_name.'_point']);

	$sr_event_cash_point = intval($payway[$payway_name.'_event_cash_point']); 
	$sr_event_point = intval($payway[$payway_name.'_event_point']);
	
	$sr_event = 0;
	if($sr_er_no > 0 ){ $sr_event = 1; }

	$sr_pay = intval($payway[$payway_name.'_amt']);
	$sr_re_pay = 0;
	if(intval($nm_member['mb_won_count']) > 0 ){ $sr_re_pay = 1; }	
	
	$sr_age = mb_get_age_group($nm_member['mb_birth']);

	$sql_sr = "INSERT INTO sales_recharge (

	sr_year_month, sr_year, sr_month, sr_day, 
	sr_hour, sr_min, sr_week, 

	sr_event, sr_product, sr_crp_no, sr_er_no, 
	sr_way, sr_way_payco, sr_platform, 

	sr_cash_point, sr_point, 
	sr_event_cash_point, sr_event_point, 
	sr_pay, sr_re_pay, 

	sr_age, sr_sex, sr_adult, sr_date 

	) VALUES (
	
	'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', 
	'".NM_TIME_H."', '".NM_TIME_I."', '".NM_TIME_W."', 

	'".$sr_event."', '".$sr_product."', '".$sr_crp_no."', '".$sr_er_no."', 
	'".$sr_way."', '".$sr_way_payco."', '".$sr_platform."', 

	'".$sr_cash_point."', '".$sr_point."', 
	'".$sr_event_cash_point."', '".$sr_event_point."', 
	'".$sr_pay."', '".$sr_re_pay."',

	'".$sr_age."', '".$nm_member['mb_sex']."', '".$nm_member['mb_adult']."', '".NM_TIME_YMDHIS."' 
	)";
	
	if(sql_query($sql_sr)){ $boolean = true; }
	return $boolean;
}

function cs_platform($agent)
{
	// $nm_config['cf_platform'] 참조해야함
	// 이용환경(1:웹, 2:Android, 4:iOS, 8:AppMarket, 16:AppSetApk)
    $agent = strtolower($agent);
    $setapk= strtolower(NM_APP_SETAPK);
    $market = strtolower(NM_APP_MARKET);

    if (preg_match("/".$setapk."/", $agent))				{ $s = 16; }
    else if(preg_match("/".$market."/", $agent))			{ $s = 8; }
    else if(preg_match("/iphone/", $agent))					{ $s = 4; }
    else if(preg_match("/ipod/", $agent))					{ $s = 4; }
    else if(preg_match("/ipad/", $agent))					{ $s = 4; }
    else if(preg_match("/android/", $agent))				{ $s = 2; }
    else { $s = 1; }
	
	return $s;
}

function cs_get_long_text()
{
	$long_text_query = "select * from config_long_text";
	return sql_fetch($long_text_query);
} // cs_get_long_text

function cs_error_log($path, $sql, $file)
{
	$log_write = chr(13).chr(10).chr(13).chr(10).str_repeat("/", 100);
	$log_write.= chr(13).chr(10)."\n data : ".NM_TIME_YMDHIS." / SERVER IP : ".$_SERVER['SERVER_ADDR'];
	$log_write.= chr(13).chr(10)."\n REMOTE_ADDR IP : ".REMOTE_ADDR;
	$log_write.= chr(13).chr(10)."\n HTTP_USER_AGENT : ".HTTP_USER_AGENT;
	$log_write.= chr(13).chr(10)."\n HTTP_REFERER : ".HTTP_REFERER;
	$log_write.= chr(13).chr(10)."\n SQL or FUNCTION or SNS : ".$sql;
	$log_write.= chr(13).chr(10)."\n FILE : ".$file;
	// log write
	$fp		= fopen($path, "a+");
	$fp_a	= fputs($fp, $log_write);
	fclose($fp);
}

function cs_auto_time($path, $field)
{
	$sql_update_config = "UPDATE config set ".$field." ='".NM_TIME_YMDHIS."'";
	if(sql_query($sql_update_config)){ // 에피소드 업데이트
	}else{
		// 업데이트 실패시 로그
		cs_error_log($path."/log/".NM_TIME_YMD.".log", $sql_update_config, $_SERVER['PHP_SELF']);
	}
}

function cs_x_error_log($table, $field, $path, $var, $function, $sql, $state, $msg, $nm_member=array())
{
	$mb_no = 0;
	$mb_idx = "";
	if($nm_member['mb_no'] != ''){
		$mb_no = intval($nm_member['mb_no']);
		$mb_idx = $nm_member['mb_idx'];
	}
	
	$sql_insert = " INSERT INTO ".$table." ( 
									x_".$field."_member, x_".$field."_member_idx, 
									x_".$field."_path, x_".$field."_var, x_".$field."_function, x_".$field."_sql, 
									x_".$field."_state, x_".$field."_msg, 
									x_".$field."_php_self, x_".$field."_serverip, x_".$field."_remote_addr, 
									x_".$field."_http_user_agent, x_".$field."_http_referer, x_".$field."_date 
									)VALUES( 
									'".$mb_no."', '".$mb_idx."',
									'".$path."', '".addslashes($var)."', '".addslashes($function)."', '".addslashes($sql)."', 
									'".$state."', '".addslashes($msg)."', 
									'".$_SERVER['PHP_SELF']."', '".$_SERVER['SERVER_ADDR']."', '".addslashes(REMOTE_ADDR)."', 
									'".addslashes(HTTP_USER_AGENT)."', '".addslashes(HTTP_REFERER)."', '".NM_TIME_YMDHIS."');
								";
	sql_query($sql_insert);
}

function cs_set_member_point_used_income($nm_member, $cash_point=0, $point=0, $won=0, $from='', $type='', $order='', $mpu_class='r', $free='n', $cancel='n')
{	// 이용내역-충전
	global $nm_config;

	$boolean = false;
	
	// 이벤트충전시 무료
	if($mpu_class == 'e'){ $free='y'; }

	$cash_point = intval($cash_point);
	$point = intval($point);
	$won = intval($won);
	
	$mpu_cash_point_balance = intval($nm_member['mb_cash_point'] + $cash_point);
	$mpu_point_balance = intval($nm_member['mb_point'] + $point);

	$mpu_age = intval(mb_get_age_group($nm_member['mb_birth']));
	$mpu_os = get_os(HTTP_USER_AGENT);
	$mpu_brow = get_brow(HTTP_USER_AGENT);

	$sql_mpu = "INSERT INTO member_point_used (
	mpu_member, mpu_member_idx, 
	mpu_class,
	mpu_recharge_won, mpu_recharge_cash_point, mpu_recharge_point, mpu_recharge_free, 

	mpu_cash_type,
	mpu_cash_point_balance, mpu_point_balance, 
	mpu_cancel, 

	mpu_age, mpu_sex, 
	mpu_os, mpu_brow, mpu_user_agent, mpu_version, mpu_from, mpu_order, mpu_type, 

	mpu_year_month, mpu_year, mpu_month, mpu_day, 
	mpu_hour, mpu_week, mpu_date 
	) VALUES (
	'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 
	'".$mpu_class."', 
	'".$won."', '".$cash_point."', '".$point."', '".$free."', 

	'".$nm_config['cf_cup_type']."', 
	'".$mpu_cash_point_balance."', '".$mpu_point_balance."', 
	'".$cancel."',

	'".$mpu_age."', '".$nm_member['mb_sex']."', 
	'".$mpu_os."','".$mpu_brow."', '".HTTP_USER_AGENT."', '".NM_VERSION."', '".$from."', '".$order."', '".$type."', 
	
	'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', 
	'".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."'
	)";
	if(sql_query($sql_mpu)){ $boolean = true; }

	return $boolean;
}

function cs_set_member_point_used_income_refund($nm_member, $order='', $date='', $from='', $mpu_refund='1')
{
	$boolean = false;

	$set_date['year_month']	= substr($date, 0, 7);
	$set_date['year']		= substr($date, 0, 4);
	$set_date['month']		= substr($date, 5, 2);
	$set_date['day']		= substr($date, 8, 2);
	$set_date['hour']		= substr($date, 11, 2);
	
	$sql_from = '';
	if($from != ''){
		$sql_from = ', mpu_from=CONCAT(mpu_from, "'.$from.'")';
	}

	$sql_mpu = "UPDATE member_point_used SET mpu_refund = ".$mpu_refund." ".$sql_from." WHERE 
	mpu_member = '".$nm_member['mb_no']."' AND mpu_member_idx = '".$nm_member['mb_idx']."' AND mpu_order = '".$order."' 
	AND mpu_year_month = '".$set_date['year_month']."' AND mpu_year = '".$set_date['year']."' 
	AND mpu_month = '".$set_date['month']."' AND mpu_day = '".$set_date['day']."' 
	AND mpu_hour = '".$set_date['hour']."' 
	";
	if(sql_query($sql_mpu)){ 
		$boolean = true; 
	}

	return $boolean;
}

function cs_set_member_point_used_expen($comics, $nm_member, $mb_cash_point_use, $mb_point_use,  $mb_cash_point_balance, $mb_point_balance, $episode_count=1, $from='', $comics_way=1)
{	// 이용내역-사용
	global $nm_config;
	
	$mb_cash_point_use		= intval($mb_cash_point_use);
	$mb_point_use			= intval($mb_point_use);
	$mb_cash_point_balance	= intval($mb_cash_point_balance);
	$mb_point_balance		= intval($mb_point_balance);

	/* 연령 */
	$mb_age_group = mb_get_age_group($nm_member['mb_birth']);

	$mpu_os = get_os(HTTP_USER_AGENT);
	$mpu_brow = get_brow(HTTP_USER_AGENT);

	$sql_mpu_insert = "
	INSERT INTO member_point_used (
	mpu_member, mpu_member_idx, 
	mpu_class,

	mpu_cash_point, mpu_point, mpu_count, 

	mpu_comics, mpu_big, mpu_small, 
	mpu_cash_type, 
	mpu_cash_point_balance, 	mpu_point_balance, 

	mpu_way, mpu_from, 
	mpu_age, mpu_sex, 

	mpu_os, mpu_brow, mpu_user_agent, mpu_version, 

	mpu_year_month, mpu_year, mpu_month, mpu_day, mpu_hour, mpu_week, mpu_date 
	) VALUES (
	'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 	
	'b', 

	'".$mb_cash_point_use."', '".$mb_point_use."', '".$episode_count."', 

	'".$comics['cm_no']."', '".$comics['cm_big']."', '".$comics['cm_small']."', 
	'".$nm_config['cf_cup_type']."',
	'".$mb_cash_point_balance."', '".$mb_point_balance."', 

	'".$comics_way."', '".$from."', 
	'".$mb_age_group."', '".$nm_member['mb_sex']."', 

	'".$mpu_os."', '".$mpu_brow."', '".HTTP_USER_AGENT."', '".NM_VERSION."', 

	'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', '".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."' 
	)";
	sql_query($sql_mpu_insert);

}

/* SQL문, PC/Mobile, 이미지커버명, 이미지커버사이즈 */
function cs_comics_content_list($sql, $device='', $thumb_key='', $thumb_wxh=''){

	global $nm_config;

	$comics_content_list_arr = array();

	// 무료&세일 이벤트 리스트 가져오기
	$event_cm_no_arr = $event_comics_arr = $event_arr = array();
	$sql_event = "SELECT * FROM event_pay WHERE 1 AND ep_state = 'y' ORDER BY ep_reg_date DESC";
	$result_event = sql_query($sql_event);
	if(sql_num_rows($result_event) != 0){
		while ($row_event = sql_fetch_array($result_event)) {
			array_push($event_arr, $row_event['ep_no']);
		}
	}
	if(count($event_arr) > 0){
		$sql_event_comics_in = implode(", ",$event_arr);
		$sql_event_comics = "SELECT * FROM event_pay_comics WHERE 1 AND epc_ep_no in(".$sql_event_comics_in.") group by epc_comics ORDER BY epc_ep_no DESC ";
		$result_event_comics = sql_query($sql_event_comics);
		if(sql_num_rows($result_event_comics) != 0){
			while ($row_event_comics = sql_fetch_array($result_event_comics)) {
				array_push($event_comics_arr, $row_event_comics);
				array_push($event_cm_no_arr, $row_event_comics['epc_comics']);
			}
		}
	}

	$result = sql_query($sql);
	for ($i=0; $row = sql_fetch_array($result); $i++) {
		
		if($device != '' && $thumb_key !='' && $thumb_wxh !=''){ // 특정 커버이미지와 사이즈 지정할때
			$row[$thumb_key] = img_url_para($row[$thumb_key], $row['cm_reg_date'], $row['cm_mod_date'], $thumb_key, $thumb_wxh);
		}else{	
			// SQL문에 이미지 사이즈 넣었을때( cm_cover, cm_cover_sub 사이즈 둘다 있어야 함 ) 
			if($row['cm_cover_thumb_wxh']!='' && $row['cm_cover_sub_thumb_wxh']!=''){
					$row['cm_cover'] = img_url_para($row['cm_cover'], $row['cm_reg_date'], $row['cm_mod_date'], 'cm_cover', $row['cm_cover_thumb_wxh']);
					$row['cm_cover_sub'] = img_url_para($row['cm_cover_sub'], $row['cm_reg_date'], $row['cm_mod_date'], 'cm_cover_sub', $row['cm_cover_sub_thumb_wxh']);
			}else{
				if($device == '') { // DEFAULT
					$row['cm_cover'] = img_url_para($row['cm_cover'], $row['cm_reg_date'], $row['cm_mod_date'], 500, 300);
					$row['cm_cover_sub'] = img_url_para($row['cm_cover_sub'], $row['cm_reg_date'], $row['cm_mod_date'], 200, 200);
				} else if($device == 'c') { // PC 일때
					$row['cm_cover'] = img_url_para($row['cm_cover'], $row['cm_reg_date'], $row['cm_mod_date'], 'cm_cover', 'tn286x147');
					$row['cm_cover_sub'] = img_url_para($row['cm_cover_sub'], $row['cm_reg_date'], $row['cm_mod_date'], 'cm_cover_sub', 'tn136x136');
				} else { // MOBILE 일때
					$row['cm_cover_sub'] = img_url_para($row['cm_cover_sub'], $row['cm_reg_date'], $row['cm_mod_date'], 'cm_cover_sub', 'tn249x249');
				} // end else
			}
		}

		$row['cm_serial_url'] = get_comics_url($row['cm_no']);
		$row['small_txt'] = $nm_config['cf_small'][$row['cm_small']];
		
		// 무료&세일 이벤트
		$row['cm_event'] = $row['cm_event_free'] = $row['cm_event_sale'] = "n";
		$row['cm_event_free_cnt'] = $row['cm_event_sale_cnt'] = "";
		$cm_event_free_cnt_tmp = $cm_event_sale_cnt = 0;

		$cm_event_free_cnt_tmp = $cm_event_sale_cnt_tmp = 0;
		if(in_array($row['cm_no'],$event_cm_no_arr)){
			foreach($event_comics_arr as $event_comics_key =>  $event_comics_val){ 				
				$row['cm_event_free_cnt'] = $row['cm_event_sale_cnt'] = "";
				if($row['cm_no'] == $event_comics_val['epc_comics']){
					$row['cm_event'] = "y";
					$row['cm_event_free'] = $event_comics_val['epc_free'];
					$row['cm_event_sale'] = $event_comics_val['epc_sale'];
					// 갯수
					$cm_event_free_cnt_tmp = $event_comics_val['epc_free_e'] - ($event_comics_val['epc_free_s']-1);
					$cm_event_sale_cnt_tmp = $event_comics_val['epc_sale_e'] - ($event_comics_val['epc_sale_s']-1);
					if(intval($cm_event_free_cnt_tmp) > 0){ $row['cm_event_free_cnt'] = intval($cm_event_free_cnt_tmp); }
					if(intval($cm_event_sale_cnt_tmp) > 0){ $row['cm_event_sale_cnt'] = intval($cm_event_sale_cnt_tmp); }
					break;
				}
			}
		}	
		/* icon */
		$row['cm_type_icon'] = $row['cm_free_icon'] = $row['cm_sale_icon'] = ""; 
		$row['cm_up_icon'] = $row['cm_new_icon'] = $row['cm_adult_icon'] = "";

		/* mark tag - free,sale */
		$row['cm_free_mark'] = $row['cm_sale_mark'] = $row['cm_adult_mark'] = "";
		if(intval($row['cm_free_cnt']) > 0 && intval($row['cm_event_free_cnt']) == 0){
			$row['cm_free_mark'] = '<mark class="mark_free">'.$row['cm_free_cnt'].'화 무료</mark>';
		}
		
		if($row['cm_type'] > 0){ $row['cm_type_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/cm_type'.$row['cm_type'].'.png?v='.NM_FIX_VER.'" alt="TYPE" />'; }
		if($row['cm_event'] == 'y'){
			$cm_icon_style="background-color: #ef5350; display: inline-block; padding: 1px 7px 2px 7px; color: #FFF; font-size: 0.3em; border-radius: 1em; letter-spacing: -1px; font-size: 11px; z-index: 20; margin-left:2px;";
			$cm_free_mark_style="background-color: #8bc34a;";
			$cm_sale_mark_style="background-color: #ef5350;";
			if($row['cm_event_free'] == 'y'){ 
				$row['cm_free_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_free.png?v='.NM_FIX_VER.'" class="icon_free_img" alt="FREE" />'; 
				/* 181024 운영요청 */
				// $row['cm_free_icon'] = '<mark class="mark_free" style="'.$cm_icon_style.'">FREE</mark>'; //181024 pc는 원래꺼로 운영요청
				$row['cm_free_mark'] = '<mark class="mark_free" style="'.$cm_free_mark_style.'">FREE</mark>';
			}
			if($row['cm_event_sale'] == 'y'){ 
				$row['cm_sale_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_sale.png?v='.NM_FIX_VER.'" class="icon_sale_img" alt="SALE" />'; 
				/* 181024 운영요청 */
				// $row['cm_sale_icon'] = '<mark class="mark_sale" style="'.$cm_icon_style.'">SALE</mark>'; //181024 pc는 원래꺼로 운영요청
				$row['cm_sale_mark'] = '<mark class="mark_sale" style="'.$cm_sale_mark_style.'">SALE</mark>';
			}
				
			if(intval($row['cm_event_free_cnt']) > 0){
				/* 181024 운영요청-사용안함 */
				// $row['cm_free_mark'] = '<mark class="mark_free">'.$row['cm_event_free_cnt'].'화 무료</mark>';
			}
			if(intval($row['cm_event_sale_cnt']) > 0){
				/* 181024 운영요청-사용안함 */
				// $row['cm_sale_mark'] = '<mark class="mark_sale">'.$row['cm_event_sale_cnt'].'화 할인</mark>';
			}
		}
		
		$cm_episode_date_up = date("Y-m-d", strtotime($row['cm_episode_date']."+1day"));
		if($cm_episode_date_up >=NM_TIME_YMD){ $row['cm_up_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_up.png?v='.NM_FIX_VER.'" class="icon_up_img" alt="UPDATE" />'; }
		
		$cm_reg_date_new = date("Y-m-d", strtotime($row['cm_reg_date']."+1day"));
		if($cm_reg_date_new >=NM_TIME_M21){ $row['cm_new_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_new.png?v='.NM_FIX_VER.'" class="icon_new_img" alt="NEW" />'; }	

		if($row['cm_adult'] =='y'){ 
			$row['cm_adult_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_adult2.png?v='.NM_FIX_VER.'" class="icon_adult_img" alt="ADULT" />'; 
			$row['cm_adult_mark'] = '<mark class="mark_adult">19</mark>';
		}
		
		// 요일 텍스트
		$row['cm_week_txt'] = cs_get_comics_week($row);

		// span
		$row['cm_small_span'] = $row['cm_end_span'] = "";
		$row['cm_small_span'] = '<span class="cm_small">'.$row['small_txt'].'</span>';// 장르
		if($row['cm_end'] == 'y'){ $row['cm_end_span'] = '<span class="cm_end">완결</span>'; } // 완결

		/* 테스트 */
		// $row['cm_type_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/cm_type'.$row['cm_type'].'.png?v='.NM_FIX_VER.'" alt="TYPE" />';
		// $row['cm_free_mark'] = '<mark class="mark_free">1화 무료</mark>';
		// $row['cm_sale_mark'] = '<mark class="mark_sale">2화 할인</mark>';
		// $row['cm_adult_mark'] = '<mark class="mark_adult">19</mark>';
		// $row['cm_free_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_free.png?v='.NM_FIX_VER.'" class="icon_free_img" alt="FREE" />';
		// $row['cm_sale_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_sale.png?v='.NM_FIX_VER.'" class="icon_sale_img" alt="SALE" />';
		// $row['cm_up_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_up.png?v='.NM_FIX_VER.'" class="icon_up_img" alt="UPDATE" />';
		// $row['cm_new_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_new.png?v='.NM_FIX_VER.'" class="icon_new_img" alt="NEW" />';
		// $row['cm_adult_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_adult2.png?v='.NM_FIX_VER.'" class="icon_adult_img" alt="ADULT" />';
		// $row['cm_end_span'] = '<span class="cm_end">완결</span>';
		/* 테스트 끝 */

		array_push($comics_content_list_arr,  $row);
	}
	return $comics_content_list_arr;
}

function cs_comics_ranking_small_tap($crs_class_arr){

	global $nm_config;

	$crs_class_group_arr = array();
	$sql_crs_class_group = "select count(crs_class) as crs_class_ctn, crs_class from comics_ranking_small group by crs_class order by crs_class";
	$result_crs_class_group = sql_query($sql_crs_class_group);
	while ($row_crs_class_group = sql_fetch_array($result_crs_class_group)) {
		if($row_crs_class_group['crs_class_ctn'] > 0){
			array_push($crs_class_group_arr, $row_crs_class_group['crs_class']);
		}
	}

	$crs_class_tap_arr = array();
	foreach($crs_class_arr as $crs_class_key => $crs_class_val){
		if(in_array($crs_class_key,$crs_class_group_arr)){
			$crs_class_tap_arr[$crs_class_key] = $crs_class_val;
		}
	}

	return $crs_class_tap_arr;
}

function cs_comics_ranking_small_list($crs_class, $device=''){

	global $nm_config;

	$crs_arr = array();	

	// 무료&세일 이벤트 리스트 가져오기
	$event_cm_no_arr = $event_comics_arr = $event_arr = array();
	$sql_event = "SELECT * FROM event_pay WHERE 1 AND ep_state = 'y' ORDER BY ep_reg_date DESC";
	$result_event = sql_query($sql_event);
	if(sql_num_rows($result_event) != 0){
		while ($row_event = sql_fetch_array($result_event)) {
			array_push($event_arr, $row_event['ep_no']);
		}
	}
	if(count($event_arr) > 0){
		$sql_event_comics_in = implode(", ",$event_arr);
		$sql_event_comics = "SELECT * FROM event_pay_comics WHERE 1 AND epc_ep_no in(".$sql_event_comics_in.") group by epc_comics ORDER BY epc_ep_no DESC ";
		$result_event_comics = sql_query($sql_event_comics);
		if(sql_num_rows($result_event_comics) != 0){
			while ($row_event_comics = sql_fetch_array($result_event_comics)) {
				array_push($event_comics_arr, $row_event_comics);
				array_push($event_cm_no_arr, $row_event_comics['epc_comics']);
			}
		}
	}

	$sql_crs = " SELECT * FROM comics_ranking_small WHERE crs_class='".$crs_class."' ORDER BY crs_ranking ASC LIMIT 16;";
	$result_crs = sql_query($sql_crs);
	for ($i=0; $row = sql_fetch_array($result_crs); $i++) {
		$sql_comics = " SELECT c.*, c_prof.cp_name as prof_name  FROM comics c 
						left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
						WHERE 1 and c.cm_service = 'y' and cm_no='".$row['crs_comics']."' ";
		$row_comics = sql_fetch($sql_comics);
		
		if($row_comics['cm_no'] == '' || $row_comics['cm_no'] == 0){ $i--; continue; } // 중지인 경우
		
		if($device == 'c') {
			if($i < 3) {
				$row_comics['cm_cover'] = img_url_para($row_comics['cm_cover'], $row_comics['cm_reg_date'], $row_comics['cm_mod_date'], 'cm_cover', 'tn388x198');
			} else {
				$row_comics['cm_cover'] = img_url_para($row_comics['cm_cover'], $row_comics['cm_reg_date'], $row_comics['cm_mod_date'], 'cm_cover', 'tn226x115');
			} // end else
			$row_comics['cm_cover_sub'] = img_url_para($row_comics['cm_cover_sub'], $row_comics['cm_reg_date'], $row_comics['cm_mod_date'], 'cm_cover_sub', 'tn133x133');
		} else { 
			// PC 외에는 안 쓰지만 일단 남겨둠..
			$row_comics['cm_cover'] = img_url_para($row_comics['cm_cover'], $row_comics['cm_reg_date'], $row_comics['cm_mod_date'], 500, 300);
			$row_comics['cm_cover_sub'] = img_url_para($row_comics['cm_cover_sub'], $row_comics['cm_reg_date'], $row_comics['cm_mod_date'], 200, 200);
		} // end else

		$row_comics['cm_serial_url'] = get_comics_url($row_comics['cm_no']);
		$row_comics['small_txt'] = $nm_config['cf_small'][$row_comics['cm_small']];
		$row_comics['cm_somaery'] = stripslashes($row_comics['cm_somaery']);
		
		// 무료&세일 이벤트
		$row_comics['cm_event'] = $row_comics['cm_event_free'] = $row_comics['cm_event_sale'] = "n";
		if(in_array($row_comics['cm_no'],$event_cm_no_arr)){
			foreach($event_comics_arr as $event_comics_key =>  $event_comics_val){ 
				if($row_comics['cm_no'] == $event_comics_val['epc_comics']){
					$row_comics['cm_event'] = "y";
					$row_comics['cm_event_free'] = $event_comics_val['epc_free'];
					$row_comics['cm_event_sale'] = $event_comics_val['epc_sale'];
				}
			}
		}	
		/* icon */
		$row_comics['cm_lank_txt'] = $row_comics['cm_lank'] = $lank_no = $i + 1; 
		if($lank_no < 10){ $row_comics['cm_lank_txt'] = "0".$lank_no; }
		$row_comics['cm_lank_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_lank'.$row_comics['cm_lank_txt'].'.png" class="icon_lank_img" alt="'.$row_comics['cm_lank'].'위" />';

		$row_comics['cm_type_icon'] = $row_comics['cm_free_icon'] = $row_comics['cm_sale_icon'] = ""; 
		$row_comics['cm_up_icon'] = $row_comics['cm_new_icon'] = $row_comics['cm_adult_icon'] = "";
		
		if($row_comics['cm_type'] > 0){ $row_comics['cm_type_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/cm_type'.$row_comics['cm_type'].'.png" alt="TYPE" />'; }
		if($row_comics['cm_event'] == 'y'){
			if($row_comics['cm_event_free'] == 'y'){ $row_comics['cm_free_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_free.png" class="icon_free_img" alt="FREE" />'; }
			if($row_comics['cm_event_sale'] == 'y'){ $row_comics['cm_sale_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_sale.png" class="icon_sale_img" alt="SALE" />'; }
		}
		
		$cm_episode_date_up = date("Y-m-d", strtotime($row_comics['cm_episode_date']."+1day"));
		if($cm_episode_date_up >=NM_TIME_YMD){ $row_comics['cm_up_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_up.png" class="icon_up_img" alt="UPDATE" />'; }
		
		$cm_reg_date_new = date("Y-m-d", strtotime($row_comics['cm_reg_date']."+1day"));
		if($cm_reg_date_new >=NM_TIME_YMD){ $row_comics['cm_new_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_new.png" class="icon_new_img" alt="NEW" />'; }	

		if($row_comics['cm_adult'] =='y'){ $row_comics['cm_adult_icon'] ='<img src="'.$nm_config['nm_url_img'].'icon/icon_adult2.png" class="icon_adult_img" alt="ADULT" />'; }

		array_push($crs_arr, $row_comics);
	}

	return $crs_arr;
}

function cs_partner_return_y($member, $comics) {
	$return_val = "n";
	
	switch($member['mb_level']) {
		case '14' :
			if($member['mb_class_no'] == $comics['cm_provider'] || $member['mb_class_no'] == $comics['cm_provider_sub']) {
				$return_val = "y";
			} // end if 제공사(CP)일 때
			break;
		case '15' :
			if($member['mb_class_no'] == $comics['cm_professional'] || $member['mb_class_no'] == $comics['cm_professional_sub'] || $member['mb_class_no'] == $comics['cm_professional_thi']) {
				$return_val = "y";
			} // end if 작가일 때
			break;
		/*
		case '16' :
			if($member['mb_class_no'] == $comics['cm_publisher']) {
				$return_val = "y";
			} // end if 출판사 일때
			break;
		*/
	} // end switch

	return $return_val;
} 

// 포인트 파크 암호화/복호화
class Security {
	/*
	// 사용법
	$encrypt = Security::encrypt('2', NM_POINTPARK_KEY);
	echo $encrypt."<br/>";
	echo Security::decrypt($encrypt, NM_POINTPARK_KEY);
	*/
	public static function encrypt($input, $key) {
		$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB); 
		$input = Security::pkcs5_pad($input, $size); 
		$td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_ECB, ''); 
		$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND); 
		mcrypt_generic_init($td, $key, $iv); 
		$data = mcrypt_generic($td, $input); 
		mcrypt_generic_deinit($td); 
		mcrypt_module_close($td); 
		$data = base64_encode($data); 
		return $data; 
	} 

	private static function pkcs5_pad ($text, $blocksize) { 
		$pad = $blocksize - (strlen($text) % $blocksize); 
		return $text . str_repeat(chr($pad), $pad); 
	} 

	public static function decrypt($sStr, $sKey) {
		$decrypted= mcrypt_decrypt(
			MCRYPT_RIJNDAEL_128,
			$sKey, 
			base64_decode($sStr), 
			MCRYPT_MODE_ECB
		);
		$dec_s = strlen($decrypted); 
		$padding = ord($decrypted[$dec_s-1]); 
		$decrypted = substr($decrypted, 0, -$padding);
		return $decrypted;
	}
	
	/*
	// 사용법
	$encrypt_aes256 = Security::encrypt_aes256(rawurlencode('2017091813550906&170900005328&10&20170918135430592'), NM_POINTPARK_ENC_KEY);
	echo $encrypt_aes256."<br/>";
	echo Security::decrypt_aes256(rawurldecode($encrypt_aes256), NM_POINTPARK_ENC_KEY);
	*/
	
	public static function decrypt_aes256($sStr, $sKey) {
		$decrypted= mcrypt_decrypt(
			MCRYPT_RIJNDAEL_128,
			$sKey, 
			base64_decode(str_replace(" ", "+", $sStr)), 
			MCRYPT_MODE_CBC, 
			str_repeat(chr(0), 16)
		);

		$dec_s = strlen($decrypted); 
		if ($dec_s % 16 > 0) $decrypted = "";        
        $padSize = ord($decrypted{$dec_s - 1});

        if (($padSize < 1) or ($padSize > 16)) $decrypted = ""; // Check padding.

        for ($i = 0; $i < $padSize; $i++)
        {
            if (ord($decrypted{$dec_s - $i - 1}) != $padSize) $decrypted = "";
        }
        $decrypted = substr($decrypted, 0, $dec_s - $padSize);
		return $decrypted;
	}

	public static function encrypt_aes256($sStr, $sKey) {
        $padSize = 16 - (strlen($sStr) % 16);
        $sStr = $sStr . str_repeat(chr($padSize), $padSize);
        $output = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $sKey, $sStr, MCRYPT_MODE_CBC, str_repeat(chr(0), 16));
        return base64_encode($output);
    }	
}

function cs_pg_channel_pointpark($nm_member, $pointpark_order)
{
	$sql_pointpark = "SELECT * FROM pg_channel_pointpark 
	WHERE pointpark_member = '".$nm_member['mb_no']."' AND pointpark_member_id = '".$nm_member['mb_id']."' AND pointpark_member_idx = '".$nm_member['mb_idx']."' AND 
	pointpark_order = '".$pointpark_order."' ";
	$row = sql_fetch($sql_pointpark);
	return $row;
}


function cs_member_point_income_pointpark($nm_member, $order=0)
{
	$sql_order = "";
	if($order > 0){ $sql_order = " AND mpip_order = '".$order."' "; }

	$sql_mpip = "SELECT * FROM member_point_income_pointpark 
	WHERE mpip_member = '".$nm_member['mb_no']."' AND mpip_member_idx = '".$nm_member['mb_idx']."' ".$sql_order." ";
	$row = sql_fetch($sql_mpip);
	return $row;
}


function cs_set_member_point_income_pointpark($nm_member, $cash_point, $point, $pay_point, $from='', $order='', $state='1', $used='y')
{
	$boolean = false;
	
	$mpip_cash_point_balance = intval($nm_member['mb_cash_point'] + $cash_point);
	$mpip_point_balance = intval($nm_member['mb_point'] + $point);

	$mpip_age = intval(mb_get_age_group($nm_member['mb_birth']));
	$mpip_kind = "bw:".get_brow(HTTP_USER_AGENT)." & os:".get_brow(HTTP_USER_AGENT);

	$sql_mpip = "INSERT INTO member_point_income_pointpark (
	mpip_member, mpip_member_idx, 
	mpip_pay_point, mpip_cash_point, mpip_point, 
	mpip_cash_point_balance, mpip_point_balance, 
	mpip_state, 

	mpip_age, mpip_sex, 
	mpip_kind, mpip_user_agent, mpip_version, mpip_from, mpip_order, 

	mpip_year_month, mpip_year, mpip_month, mpip_day, 
	mpip_hour, mpip_week, mpip_date 
	) VALUES (
	'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 
	'".$pay_point."', '".$cash_point."', '".$point."', 
	'".$mpip_cash_point_balance."', '".$mpip_point_balance."', 
	'".$state."',

	'".$mpip_age."', '".$nm_member['mb_sex']."', 
	'".$mpip_kind."', '".HTTP_USER_AGENT."', '".NM_VERSION."', '".$from."', '".$order."', 
	
	'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', 
	'".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."'
	)";
	
	if(sql_query($sql_mpip)){ 
		$boolean = true; 
		$boolean = mb_update_point($nm_member, $cash_point, $point); // member update

		// 사용내역 저장
		if($used == 'y'){
			$boolean = cs_set_member_point_used_income($nm_member, $cash_point, $point, $pay_point, $from, '', $order, 'r');
			// 회원정보, 캐쉬포인트, 포인트, 원, 내역, 이벤트타입, 충전주문번호, 내역구분
		}
	}

	return $boolean;
}


function cs_set_sales_recharge_pointpark($nm_member, $payway, $payway_name)
{
	// 결제정보 배열로 받기 => $payway
	// pc사 영문이름 => $payway_name

	$boolean = false;
	
	$srp_way = $payway_name;

	$srp_platform = cs_platform(HTTP_USER_AGENT);

	$srp_cash_point = intval($payway[$payway_name.'_cash_point']); 
	$srp_point = intval($payway[$payway_name.'_point']);

	$srp_pay = intval($payway[$payway_name.'_amt']);
	$srp_re_pay = 0;
	if(intval($nm_member['mb_won_count']) > 0 ){ $srp_re_pay = 1; }	
	
	$srp_age = mb_get_age_group($nm_member['mb_birth']);

	$sql_sr = "INSERT INTO sales_recharge_pointpark (

	srp_year_month, srp_year, srp_month, srp_day, 
	srp_hour, srp_min, srp_week, 

	srp_platform, 

	srp_cash_point, srp_point, 
	srp_pay, srp_re_pay, 

	srp_age, srp_sex, srp_adult, srp_date 

	) VALUES (
	
	'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', 
	'".NM_TIME_H."', '".NM_TIME_I."', '".NM_TIME_W."', 

	'".$srp_platform."', 

	'".$srp_cash_point."', '".$srp_point."', 
	'".$srp_pay."', '".$srp_re_pay."',

	'".$srp_age."', '".$nm_member['mb_sex']."', '".$nm_member['mb_adult']."', '".NM_TIME_YMDHIS."' 
	)";
	
	if(sql_query($sql_sr)){ $boolean = true; }
	return $boolean;
}

function cs_crp_won_txt($crp_val){
	$mar_pay_arr = array();

	if($crp_val['crp_won'] == $crp_val['crp_won_txt']){
		$mar_pay_arr[0] = "";
		$mar_pay_arr[1] = number_format($crp_val['crp_won'])."원";
	}else{
		$mar_pay_arr[0] = "mar_pay_con_app_sale";
		$mar_pay_arr[1] = "<s>".number_format($crp_val['crp_won_txt'])."원</s><br/>";
		$mar_pay_arr[1].= "( <strong>".number_format($crp_val['crp_won'])."원</strong> )";
	}
	return $mar_pay_arr;
}

/* 180420 신규회원가입팝업  */
function cs_confirmjoin($onload='y')
{
	global $nm_config;

	$cf_confirmjoin_mode = 'mobile';
	$cf_confirmjoin_url = NM_IMG;
	if($nm_config['nm_mode'] == NM_PC){
		$cf_confirmjoin_mode = 'web';
	}
	$cf_confirmjointitle = $cf_confirmjoin_url.$nm_config['cf_'.$cf_confirmjoin_mode.'_confirmjoin_title'];
	$cf_confirmjointxt = $cf_confirmjoin_url.$nm_config['cf_'.$cf_confirmjoin_mode.'_confirmjoin_txt'];

	echo' 
		<script type="text/javascript">
		<!--
			cf_confirmjointitle_img_url = "'.$cf_confirmjointitle.'";
			cf_confirmjointxt_img_url = "'.$cf_confirmjointxt.'";
			confirmJoin_imgload(cf_confirmjointitle_img_url,cf_confirmjointxt_img_url);
		';
	if($onload == 'y'){
	echo' 
			window.onload = function(){
				confirmJoin(cf_confirmjointitle_img_url,cf_confirmjointxt_img_url);
			}
		';
	}
	echo' 
		//-->
		</script>';
		/********************************************************************
		$(function(){
			$('#push_set').click(function(){

			});
		});
		<div class="hide"><img src="'.$img_url.'" alt="회원가입/로그인팝업"></div>
		*******************************************************************/
}

/* 180420 신규회원가입팝업 */
function cs_confirmjoin_ask($count=3)
{
	global $nm_member;
	// 기본이 3번

	$cookie_value = $get_cookie_value = 1;
	$cookie_name='confirmjoin_count';
	$ask_exe = false;
	$confirmjoin_ask_config = 'n';

	// 임시 설정
	$mode_arr = array('y','a','n');
	$confirmjoin_ask_config = $mode_arr[0]; 

	$count_tmp = 0;
	if($count_tmp > 0){ $count = $count_tmp; }
	// 임시 설정 끝

	if($confirmjoin_ask_config == 'y'){ // 모두 적용
		if($nm_member['mb_no'] == '' || $nm_member['mb_no'] == NULL || intval($nm_member['mb_no']) <= 0){ // 비회원만 적용			
			$ask_exe = true;
		}
	}else if($confirmjoin_ask_config == 'a'){ // 관리자만 적용
		if($nm_member['mb_class'] == 'a'){ // 관리자 회원만 적용	
			$ask_exe = true;
		}
	}

	if($ask_exe == true){
		$get_cookie_value = get_js_cookie($cookie_name);
		if($get_cookie_value == '' || $get_cookie_value == NULL){
			$get_cookie_value = $cookie_value;
		}

		// $count 보다 많은지 확인
		if($get_cookie_value >= $count){
			$get_cookie_value = $cookie_value;
			cs_confirmjoin();
		}else{
			$get_cookie_value++;
		}
		echo'
			<script type="text/javascript">
			<!--
				$.cookie("'.$cookie_name.'", "'.$get_cookie_value.'");
			//-->
			</script>
		';
	}else{
		echo'
			<script type="text/javascript">
			<!--
				$.cookie("'.$cookie_name.'", "0");
			//-->
			</script>
		';		
	}

	/* 적용 리스트 *******************************
	/_pc/comicsviewpage.php 하단
	/_pc/comicsviewscrolle.php 하단
	/_moblie/comicsviewpage.php 하단
	/_moblie/comicsviewscrolle.php 하단
	******************************************/
}

function cs_get_comics_week($comics){
	global $nm_config;

	$week_txt = "없음";
	$week_arr = array();
	$public_cycle_check_val = $comics['cm_public_cycle'];
	$public_cycle_arr = get_comics_cycle_val($public_cycle_check_val);
	
	foreach($nm_config['cf_public_cycle'] as $cycle_key => $cycle_val){ 
		if($cycle_key == 0 || $cycle_key == 8) { continue; } // 주기 없음, 보름 무시
		$this_checked = "";
		if(in_array($cycle_val[0], $public_cycle_arr)){ 
			array_push($week_arr, $cycle_val[1]);
		}
	}

	if(count($week_arr) > 0){ $week_txt = implode(",",$week_arr); }

	return $week_txt;
}

function cs_recharge_save_filename_ctrl(){
	global $nm_config, $nm_member;

	$cs_recharge_save_filename = 'recharge_save';

	if($nm_member['mb_won_count'] == 0){
		$cs_recharge_save_filename = 'recharge_save_first';
	}
	return $cs_recharge_save_filename;
}

function cs_recharge_save_cancel($nm_member, $pg_cancel_name, $recharge_order_no_cancel, $transaction, $coupondc=0, $boolean_mpi=false, $boolean_cash=false, $boolean_sr=false, $randombox_coupon_use=false, $from_cancel='-취소(네트워크에러)', $pg_refund=2){
	global $nm_config;
	
	$cs_cancel['state']= 0;
	$cs_cancel['msg']  = '취소완료';
	$row_cancel = "";
	$nm_proc_path = "";

	if($nm_member['mb_id'] == ''){ 
		$cs_cancel['state']= 1;
		$cs_cancel['msg']  = 'mb_id 정보없음';
	}

	if($recharge_order_no_cancel == ''){ 
		$cs_cancel['state']= 1;
		$cs_cancel['msg']  = 'recharge_order_no_cancel 정보없음';
	}

	$nm_member_cancel = mb_get($nm_member['mb_id']);
	switch($pg_cancel_name){
		case 'kcp' :
			$row_cancel = cs_pg_channel_kcp($nm_member_cancel, $recharge_order_no_cancel);
			$nm_proc_path = NM_KCP_PATH.'/cfg/cancel.php';
			$tno = $transaction['tno'];
			$mod_desc = '통신에러-환불처리';
			if($from_cancel != '-취소(네트워크에러)'){ $mod_desc=$from_cancel; }
		break;
		case 'payco' :
			$row_cancel = cs_pg_channel_payco($nm_member_cancel, $recharge_order_no_cancel);
			$nm_proc_path = NM_PAYCO_PATH.'/cancel.php';
			$reserveOrderNo = $transaction['reserveOrderNo'];
			$sellerOrderReferenceKey = $transaction['sellerOrderReferenceKey'];
			$sellerKey = $transaction['sellerKey'];
			/* 181017-취소필수값저장 */
			$orderCertifyKey = $transaction['orderCertifyKey'];
			$totalPaymentAmt = $transaction['totalPaymentAmt'];
			/* 181017-취소필수값저장 end */
		break;
		case 'toss' :
			$row_cancel = cs_pg_channel_toss($nm_member_cancel, $recharge_order_no_cancel);
			$nm_proc_path = NM_TOSS_PATH.'/cancel.php';
			$apikey = $transaction['apikey'];
			$payToken = $transaction['payToken'];
			$amount = $transaction['amount'];
			$amountTaxFree = $transaction['amountTaxFree'];
		break;
		default: 
			$row_cancel = "cs_recharge_save_cancel error";	
			$nm_proc_path = "";	
			$cs_cancel['state']= 2;
			$cs_cancel['msg']  = 'pg_cancel_name 정보없음';
		break;
	}

	if($row_cancel[$pg_cancel_name.'_no'] == '' || intval($row_cancel[$pg_cancel_name.'_no']) == 0){
			$cs_cancel['state']= 3;
			$cs_cancel['msg']  = 'row_cancel 정보없음';
	}

	/*
	print_r($nm_member);
	echo "<br/><br/>";
	echo $nm_proc_path."<br/><br/>";
	echo $recharge_order_no_cancel."<br/><br/>";
	print_r($row_cancel);
	die;
	*/

	if($state == 0){	
	    $mpi_won_cancel = intval($row_cancel[$pg_cancel_name.'_amt']);
	    $mpi_cash_point_cancel = intval($row_cancel[$pg_cancel_name.'_cash_point'] + $row_cancel[$pg_cancel_name.'_event_cash_point']);
	    $mpi_point_cancel = intval($row_cancel[$pg_cancel_name.'_point'] + $row_cancel[$pg_cancel_name.'_event_point']);
		$mpi_won_cancel *=-1;
		$mpi_cash_point_cancel *=-1;
		$mpi_point_cancel *=-1;
		
	    $mpi_payway_cancel = "";
	    foreach($nm_config['cf_payway'] as $mpi_payway_cancel_key => $mpi_payway_cancel_val){
	  	    if($mpi_payway_cancel_val[1] == $row_cancel[$pg_cancel_name.'_payway']){ 
				$mpi_payway_cancel = $mpi_payway_cancel_val[2]; 
				$mpi_payway_no_cancel = $mpi_payway_cancel_key; 
			}
	    }

		if($pg_cancel_name == 'payco'){ $mpi_payway_cancel = $row_cancel['payco_payment_method_name']; } // 페이코일 경우
		$mpi_from_cancel = $pg_cancel_name."(".$mpi_payway_cancel.")[".$row_cancel[$pg_cancel_name.'_product']."]";
		$mpi_crp_no_cancel = $row_cancel[$pg_cancel_name.'_crp_no'];

		if($boolean_mpi == true){
			cs_set_member_point_income($nm_member_cancel, $mpi_cash_point_cancel, $mpi_point_cancel, $mpi_won_cancel, $mpi_from_cancel.$from_cancel, $recharge_order_no_cancel, '6','n','y');
			$pg_refund = 1;
		}
		cs_set_member_point_income_refund($nm_member_cancel, $recharge_order_no_cancel, $row_cancel[$pg_cancel_name.'_date'],$pg_refund);
		cs_set_member_point_used_income_refund($nm_member_cancel, $recharge_order_no_cancel, $row_cancel[$pg_cancel_name.'_date'],$from_cancel,$pg_refund);
		
		
		if($boolean_cash == true || $boolean_sr == true){
			$row_cancel[$pg_cancel_name.'_amt'] *=-1;
			$row_cancel[$pg_cancel_name.'_cash_point'] *=-1;
			$row_cancel[$pg_cancel_name.'_event_cash_point'] *=-1;
			$row_cancel[$pg_cancel_name.'_point'] *=-1;
			$row_cancel[$pg_cancel_name.'_event_event_point'] *=-1;
			if($boolean_cash == true){ mb_set_cash($nm_member_cancel, $row_cancel, $pg_cancel_name); }
			if($boolean_sr == true){ cs_set_sales_recharge($nm_member_cancel, $row_cancel, $pg_cancel_name); }
		}
		if($randombox_coupon_use == true){
			randombox_coupon_use_cancel($nm_member_cancel, $coupondc, $mpi_crp_no_cancel, $mpi_payway_no_cancel);
		}
		
		// 결제 취소 모듈 추가
		include $nm_proc_path;

		$cs_cancel['state']= $db_result['state'];
		$cs_cancel['msg']  = $db_result['msg'];
	}

	return $cs_cancel;
}

/* 181024 // 주문번호-캔슬용 삭제 : 세션처리했었는데 뒤로가기할때 문제가 발생되어 DB 저장하기로 함 */
function cs_del_pg_member_order($nm_member){
	$sql = " DELETE FROM pg_member_order 
	WHERE pmo_member='".$nm_member['mb_no']."' 
	AND pmo_member_idx='".mb_get_idx($nm_member['mb_id'])."' 
	; ";
	sql_query($sql);
}

function cs_set_pg_member_order($nm_member, $order=0){
	if(intval($order) < 1){ return false; }

	cs_del_pg_member_order($nm_member);

	$sql = " INSERT INTO pg_member_order ( 
	         pmo_member, pmo_member_id, pmo_member_idx, 
			 pmo_order, pmo_date 
			 ) VALUES ( 
			 '".$nm_member['mb_no']."', '".$nm_member['mb_id']."', '".mb_get_idx($nm_member['mb_id'])."', 
			 '".$order."', '".NM_TIME_YMDHIS."' 
			 ); ";
	sql_query($sql);
}

function cs_get_pg_member_order($nm_member){
	$order = 0;

	$sql = " SELECT * FROM pg_member_order 
	WHERE pmo_member='".$nm_member['mb_no']."' 
	AND pmo_member_idx='".mb_get_idx($nm_member['mb_id'])."' 
	; ";
	$row = sql_fetch($sql);

	$order = intval($row['pmo_order']);
	
	return $order;
}

?>