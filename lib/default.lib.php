<?
if (!defined('_NMPAGE_')) exit;
/* 공통 변수 디폴트 값 설정(값 없을시...) */

include_once(NM_LIB_PATH.'/thumbnail.lib.php');	 // 썸네일 라이브러리($nm_config변수 포함이라 선언 아래)

/* 회원등급 */
$mb_client_list_default =  $mb_partner_level_list_default =  $mb_admin_level_list_default =  array();
array_push($mb_client_list_default, '', '비회원', '일반회원', 'SILVER회원', 'GOLD회원', 'VIP회원', '', '', '', '', ''); /* 고객 1~10 => 10개 */
array_push($mb_partner_level_list_default, '파트너(채널/광고/PG사)', '컨텐츠파트너(CP/작가/출판사)', '', '', '', '', '', '', '', ''); /* 관계자 11~20 => 10개 */
array_push($mb_admin_level_list_default, '넥스큐브사원', '콘텐츠관리자', '', '', '', '', '', '', '운영관리자','마스터관리자'); /* 관리자 21~30 => 10개 */
/* 위 회원리스트 병합 */
$mb_level_list_default =  array_merge($mb_client_list_default, $mb_partner_level_list_default, $mb_admin_level_list_default);
// array_unshift($mb_level_list_default, ''); /* 전체 0 => 1개 -> 삭제*/

if(($nm_config['cf_client_list'] == "" || $nm_config['cf_client_list'] == NULL) && ($nm_config['cf_partner_list'] == "" || $nm_config['cf_partner_list'] == NULL) && ($nm_config['cf_admin_list'] == "" || $nm_config['cf_admin_list'] == NULL)){// 회원등급 - 디폴트
	$nm_config['cf_level_list'] = $mb_level_list_default;
}else{
	$mb_client_list = explode("|", $nm_config['cf_client_list']); /* 고객 1~10 => 10개 */
	$mb_partner_level_list = explode("|", $nm_config['cf_partner_list']); /* 관계자 11~20 => 10개 */
	$mb_admin_level_list = explode("|", $nm_config['cf_admin_list']); /* 관리자 21~30 => 10개 */
	$mb_level_list =  array_merge($mb_client_list, $mb_partner_level_list, $mb_admin_level_list); /* 위 회원리스트 병합 */
	// array_unshift($mb_level_list, ''); /* 전체 0 => 1개 -> 삭제 */
	$nm_config['cf_level_list'] = $mb_level_list;
}


$member_partner_level_default = 11;
if($nm_config['cf_partner_level'] == "" || $nm_config['cf_partner_level'] == NULL){// 파트너등급레벨 - 디폴트
	$nm_config['cf_partner_level'] = $member_partner_level_default;
}
$member_admin_level_default = 21;
if($nm_config['cf_admin_level'] == "" || $nm_config['cf_admin_level'] == NULL){// 관리자등급레벨 - 디폴트
	$nm_config['cf_admin_level'] = $member_admin_level_default;
}


$member_partner_class_default = 'p';
if($nm_config['cf_partner_class'] == "" || $nm_config['cf_partner_class'] == NULL){// 파트너분류 - 디폴트
	$nm_config['cf_partner_class'] = $member_partner_class_default;
}
$member_admin_class_default = 'a';
if($nm_config['cf_admin_class'] == "" || $nm_config['cf_admin_class'] == NULL){// 관리자분류 - 디폴트
	$nm_config['cf_admin_class'] = $member_admin_class_default;
}

$big_default = array('', '단행본', '웹툰', '단행본웹툰');
if($nm_config['cf_big'] == "" || $nm_config['cf_big'] == NULL){// 대분류 - 디폴트
	$nm_config['cf_big'] = $big_default;
}else{ 
	$nm_config['cf_big'] = explode("|", $nm_config['cf_big']);
	if(count($nm_config['cf_big']) < count($big_default)){ $nm_config['cf_big'] = $big_default; }
}

$small_default = array('', '성인', 'BL/GL', 'TL', '순정', '드라마', '코믹', '소설');
if($nm_config['cf_small'] == "" || $nm_config['cf_small'] == NULL){// 장르 - 디폴트
	$nm_config['cf_small'] = $small_default;
}else{ 
	$nm_config['cf_small'] = explode("|", $nm_config['cf_small']);
	if(count($nm_config['cf_small']) < count($small_default)){ $nm_config['cf_small'] = $small_default; }
}

$s_limit_default = 30;
if($nm_config['s_limit'] == "" || $nm_config['s_limit'] == NULL){// 출력 - 디폴트
	$nm_config['s_limit'] = $s_limit_default;
}

$s_limit_min_default = 10;
if($nm_config['s_limit_min'] == "" || $nm_config['s_limit_min'] == NULL){// 출력 - 디폴트
	$nm_config['s_limit_min'] = $s_limit_min_default;
}

$s_limit_max_default = 50;
if($nm_config['s_limit_max'] == "" || $nm_config['s_limit_max'] == NULL){// 출력 - 디폴트
	$nm_config['s_limit_max'] = $s_limit_max_default;
}


$banner_position_default = array('');// 프로모션 배너 배너
foreach($thumbnail['embc_cover'] as $thumbnail_banner_position_key => $thumbnail_banner_position_cover){ // include_once(NM_LIB_PATH.'/thumbnail.lib.php');
	array_push($banner_position_default, $thumbnail_banner_position_cover[$thumbnail_banner_position_key+1]['text']);
}

if($nm_config['cf_banner_position'] == "" || $nm_config['cf_banner_position'] == NULL){// 배너 위치
	$nm_config['cf_banner_position'] = $banner_position_default;
}else{ 
	$nm_config['cf_banner_position'] = explode("|", $nm_config['cf_banner_position']);
	if(count($nm_config['cf_banner_position']) < count($banner_position_default)){	
		$nm_config['cf_banner_position'] = $banner_position_default; 
	}
}



$pay_default = array(0, 1, 2, 3, 4, 5, 8, 9, 12, 20);
if($nm_config['cf_pay'] == "" || $nm_config['cf_pay'] == NULL){// 코인리스트 - 디폴트
	$nm_config['cf_pay'] = $pay_default;
}else{ $nm_config['cf_pay'] = explode("|", $nm_config['cf_pay']); }

$pay_basic_default = 3;
if($nm_config['cf_pay_basic'] == "" || $nm_config['cf_pay_basic'] == NULL){// 코인 디폴트
	$nm_config['cf_pay_basic'] = $pay_basic_default;
}

/* 상수가 있어서 삭제
if($nm_config['cf_point_rate'] == "" || $nm_config['cf_point_rate'] == NULL){// 보너스코인 비율 config.php NM_POINT 상수선언
	$nm_config['cf_point_rate'] = NM_POINT; // 10 config.php 파일 -> 상수 
}
*/

$cash_point_unit_default = "C";
if($nm_config['cf_cash_point_unit'] == "" || $nm_config['cf_cash_point_unit'] == NULL){// 코인단위
	$nm_config['cf_cash_point_unit'] = $cash_point_unit_default;
}

$cash_point_unit_ko_default  = "코인";
if($nm_config['cf_cash_point_unit_ko'] == "" || $nm_config['cf_cash_point_unit_ko'] == NULL){// 가격 - 한국어 단위
	$nm_config['cf_cash_point_unit_ko'] = $cash_point_unit_ko_default;
}

$point_unit_default = "M";;
if($nm_config['cf_point_unit'] == "" || $nm_config['cf_point_unit'] == NULL){// 포인트 - 단위
	$nm_config['cf_point_unit'] = $point_unit_default;
}

$point_unit_ko_default = "미니코인";
if($nm_config['cf_point_unit_ko'] == "" || $nm_config['cf_point_unit_ko'] == NULL){// 포인트 - 한국어 단위
	$nm_config['cf_point_unit_ko'] = $point_unit_ko_default;
}

/* cfbig으로 대처할 예정
$content_db_default = array('','_1','_2','_3');
if($nm_config['content_db'] == "" || $nm_config['content_db'] == NULL){// 가격 리스트 - 디폴트
	$nm_config['content_db'] = $content_db_default;
}
*/

/* 정산금액타입 */
$cup_type_default = 1;
if($nm_config['cf_cup_type'] == "" || $nm_config['cf_cup_type'] == NULL){// 정산금액타입
	$nm_config['cf_cup_type'] = $cup_type_default;
}

/* 정산금액설정 */
$nm_unit_pay_sql = "SELECT * FROM  `config_unit_pay` WHERE  `cup_type` = ".$nm_config['cf_cup_type'];
$nm_unit_pay = sql_fetch($nm_unit_pay_sql);
$nm_config['cf_cup_won'] = $nm_unit_pay['cup_won'];

$cup_won_default = 100;
if($nm_config['cf_cup_won'] == "" || $nm_config['cf_cup_won'] == NULL){// 정산금액
	$nm_config['cf_cup_won'] = $cup_won_default;
}

// 연재주기 - 메인
$public_cycle_main = array('주기없음', '매주', '격주', '월간');
if($nm_config['cf_public_cycle_main'] == "" || $nm_config['cf_public_cycle_main'] == NULL){
	$nm_config['cf_public_cycle_main'] = $public_cycle_main;
}

// 연재주기 - 디폴트
$public_cycle_default = array();
array_push($public_cycle_default, array(0, '주기없음'));
array_push($public_cycle_default, array(1, '월'));
array_push($public_cycle_default, array(2, '화'));
array_push($public_cycle_default, array(4, '수'));
array_push($public_cycle_default, array(8, '목'));
array_push($public_cycle_default, array(16, '금'));
array_push($public_cycle_default, array(32, '토'));
array_push($public_cycle_default, array(64, '일'));
array_push($public_cycle_default, array(128, '보름'));
array_push($public_cycle_default, array(256, '월간'));
$nm_config['cf_public_cycle'] = $public_cycle_default;

// 이용환경 - 디폴트
$platform_default = array();
array_push($platform_default, array(0, '모두'));
array_push($platform_default, array(1, '웹'));
array_push($platform_default, array(2, 'Android'));
array_push($platform_default, array(4, 'iOS'));
array_push($platform_default, array(8, 'AppMarket'));
array_push($platform_default, array(16, 'AppSetApk'));
$nm_config['cf_platform'] = $platform_default;

// 컨텐츠유형 - 디폴트
$comics_type_default = $cf_comics_type_list = $cf_comics_type_arr = array();
array_push($comics_type_default, array(0, '사용안함'));
array_push($comics_type_default, array(1, 'PD추천만화'));
array_push($comics_type_default, array(2, 'hot만화'));
array_push($comics_type_default, array(3, 'best만화'));
if($nm_config['cf_comics_type'] == "" || $nm_config['cf_comics_type'] == NULL){
	$nm_config['cf_comics_type'] = $comics_type_default;
}else{
	if($nm_config['cf_comics_type']){ /* 데이터 있을시 따로 처리 한다 */
		$cf_comics_type_arr = explode("|", $nm_config['cf_comics_type']);
		foreach($cf_comics_type_arr as $cf_comics_type_arr_key => $cf_comics_type_arr_val){
			if($cf_comics_type_arr_key == 0){
				array_push($cf_comics_type_list, array(0, $cf_comics_type_arr_val));
			}else{
				// array_push($cf_comics_type_list, array(pow(2, $cf_comics_type_arr_key-1), $cf_comics_type_arr_val));
				array_push($cf_comics_type_list, array($cf_comics_type_arr_key, $cf_comics_type_arr_val));
			}
		}
		$nm_config['cf_comics_type'] = $cf_comics_type_list;
	}
}

// 이벤트-충전/지급 - 디폴트
// 데이터베이스 따로 생성됨 event_recharge 최소 성인인증시 지급은 ....나중에....;;
$er_type_default = array('', '충전시 추가 지급 이벤트', '최초 성인인증시 지급');
if($nm_config['cf_er_type'] == "" || $nm_config['cf_er_type'] == NULL){
	$nm_config['cf_er_type'] = $er_type_default;
}else{ 
	$nm_config['cf_er_type'] = explode("|", $nm_config['cf_er_type']);
	if(count($nm_config['cf_er_type']) < count($er_type_default)){ $nm_config['cf_er_type'] = $er_type_default; }
}

// 결제방법 - 디폴트
$payway_default = array();
array_push($payway_default, array('kcp', 'mobx', '휴대폰', ''));
array_push($payway_default, array('kcp', 'card', '신용카드', ''));
array_push($payway_default, array('payco', 'payco', 'PAYCO 간편결제', NM_IMG.'common/payco.png?v=2'));
array_push($payway_default, array('payco', 'payco60', 'PAYCO 휴대폰결제', NM_IMG.'common/payco.png?v=2'));
array_push($payway_default, array('toss', 'toss', 'TOSS(간편송금)', ''));
array_push($payway_default, array('kcp', 'sccl', '문화상품권', ''));
array_push($payway_default, array('kcp', 'schm', '해피머니', ''));
array_push($payway_default, array('kcp', 'scbl', '도서문화상품권', ''));
array_push($payway_default, array('kcp', 'acnt' ,'계좌이체',  ''));
$nm_config['cf_payway'] = $payway_default;

// 운영+마스터+개발 관리자 $nm_config['cf_admin_level'] ->20 에서 더하기
$nm_config['cf_admin_level_plus'] = 8;

// 메뉴갯수 디폴트
if($nm_config['cf_menu'] == "" || $nm_config['cf_menu'] == NULL || intval($nm_config['cf_menu']) == 0){
	$nm_config['cf_menu'] = 5;
}

/********************
    고객 디바이스 환경에 따른 링크연결
********************/
$nm_config['nm_url'] =$nm_config['nm_path'] = '';
if(is_mobile()){
	$nm_config['nm_url'] = NM_MO_URL;
	$nm_config['nm_url_img'] = NM_MO_IMG;
	$nm_config['nm_path'] = NM_MO_PATH;
	$nm_config['nm_mode'] = NM_MO;
}else{
	$nm_config['nm_url'] = NM_PC_URL;
	$nm_config['nm_url_img'] = NM_PC_IMG;
	$nm_config['nm_path'] = NM_PC_PATH;
	$nm_config['nm_mode'] = NM_PC;
}

/* 위 고객 디바이스 환경 쿠키로 조절하기 */
$ck_default_mode = get_js_cookie('ck_default_mode');

if($ck_default_mode == 'pc'){
	$nm_config['nm_url'] = NM_PC_URL;
	$nm_config['nm_url_img'] = NM_PC_IMG;
	$nm_config['nm_path'] = NM_PC_PATH;
	$nm_config['nm_mode'] = NM_PC;
}

if($ck_default_mode == 'mobile'){
	$nm_config['nm_url'] = NM_MO_URL;
	$nm_config['nm_url_img'] = NM_MO_IMG;
	$nm_config['nm_path'] = NM_MO_PATH;
	$nm_config['nm_mode'] = NM_MO;
}

/* cms 팝업 넓이 */
$popup_cms_width = 900;

/* 상단 서브 메뉴 */
$nm_config['lnb_sub'] = false;

/* 하단 footer */
$nm_config['footer'] = true;
?>