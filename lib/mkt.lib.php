<?
if (!defined('_NMPAGE_')) exit;

function mkt_row($mkt_no)
{
	$sql_mkt = "select * from marketer where mkt_no=".$mkt_no." ";
	$row_mkt = sql_fetch($sql_mkt);
	return $row_mkt;
}

function mkt_mktl_row($mkt_no, $mktl_no)
{
	$sql_mktl = "select * from marketer_link where mkt_no=".$mkt_no." AND mktl_no=".$mktl_no." ";
	$row_mktl= sql_fetch($sql_mktl);
	return $row_mktl;
}

function mkt_short_row($mkt_no, $mktl_short_link)
{
	$sql_mktl = "select * from marketer_link where mkt_no=".$mkt_no." AND mktl_short_link='".$mktl_short_link."' ";
	$row_mktl= sql_fetch($sql_mktl);
	return $row_mktl;
}

function mkt_click_update($row_mktl)
{
	global $nm_member;

	$mkt_bool = false;

	$sql_update_mktl = "update marketer_link set mktl_click= (mktl_click+1) where mktl_no=".$row_mktl['mktl_no']." ";
	/*
	if($nm_member['mb_class'] != "a"){
		if(sql_query($sql_update_mktl)){ $mkt_bool = true; }
	}else{
		$mkt_bool = true;
	}
	*/
	// 황의대리님 요청 180917
	if(sql_query($sql_update_mktl)){ $mkt_bool = true; }

	return $mkt_bool;
}

function mkt_join_update($row_mktl)
{
	global $nm_member;

	$mkt_bool = false;

	$sql_update_mktl = "update marketer_link set mktl_join= (mktl_join+1) where mktl_no=".$row_mktl['mktl_no']." ";
	if($nm_member['mb_class'] != "a"){
		if(sql_query($sql_update_mktl)){ $mkt_bool = true; }
	}else{
		$mkt_bool = true;
	}

	return $mkt_bool;
}

function mkt_click_stats_update($row_mktl)
{
	global $nm_member;

	$mktl_bool = false;

	$set_date['year_month']	= NM_TIME_YM;
	$set_date['year']		= NM_TIME_Y;
	$set_date['month']		= NM_TIME_M;
	$set_date['day']		= NM_TIME_D;
	$set_date['hour']		= NM_TIME_H;
	$set_date['week']		= NM_TIME_W;

	// 통계
	$sql_insert_stats_mktl = "
		INSERT INTO stats_marketer_link (mkt_no, mktl_no, slmktl_year_month, slmktl_year, slmktl_month, slmktl_day, slmktl_hour, slmktl_week 
		) VALUES ('".$row_mktl['mkt_no']."', '".$row_mktl['mktl_no']."', 
		'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
		'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."' 
	)";
	$sql_update_stats_mktl = "
		UPDATE stats_marketer_link SET slmktl_click= (slmktl_click+1) 
		WHERE 1 AND mkt_no = '".$row_mktl['mkt_no']."' AND mktl_no = '".$row_mktl['mktl_no']."' 
		AND slmktl_year_month = '".$set_date['year_month']."' AND slmktl_year = '".$set_date['year']."' 
		AND slmktl_month = '".$set_date['month']."' AND slmktl_day = '".$set_date['day']."' 
			AND slmktl_hour = '".$set_date['hour']."' AND slmktl_week = '".$set_date['week']."' ";
	
	
	/* 181001 */
	/*
	if($nm_member['mb_class'] != "a"){
		@mysql_query($sql_insert_stats_mktl); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
		if(sql_query($sql_update_stats_mktl)){ $mktl_bool = true; }
	}else{
		$mktl_bool = true;
	}
	*/
	@mysql_query($sql_insert_stats_mktl); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
	if(sql_query($sql_update_stats_mktl)){ $mktl_bool = true; }

	return $mktl_bool;
}



function mkt_join_stats_update($row_mktl)
{
	global $nm_member;

	$mktl_bool = false;

	$set_date['year_month']	= NM_TIME_YM;
	$set_date['year']		= NM_TIME_Y;
	$set_date['month']		= NM_TIME_M;
	$set_date['day']		= NM_TIME_D;
	$set_date['hour']		= NM_TIME_H;
	$set_date['week']		= NM_TIME_W;

	// 통계
	$sql_insert_stats_mktj = "
		INSERT INTO stats_marketer_join (mkt_no, mktl_no, slmktj_year_month, slmktj_year, slmktj_month, slmktj_day, slmktj_hour, slmktj_week 
		) VALUES ('".$row_mktl['mkt_no']."', '".$row_mktl['mktl_no']."', 
		'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
		'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."' 
	)";
	$sql_update_stats_mktj = "
		UPDATE stats_marketer_join SET slmktj_join= (slmktj_join+1) 
		WHERE 1 AND mkt_no = '".$row_mktl['mkt_no']."'  AND mktl_no = '".$row_mktl['mktl_no']."' 
		AND slmktj_year_month = '".$set_date['year_month']."' AND slmktj_year = '".$set_date['year']."' 
		AND slmktj_month = '".$set_date['month']."' AND slmktj_day = '".$set_date['day']."' 
			AND slmktj_hour = '".$set_date['hour']."' AND slmktj_week = '".$set_date['week']."' ";
	if($nm_member['mb_class'] != "a"){
		@mysql_query($sql_insert_stats_mktj); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
		if(sql_query($sql_update_stats_mktj)){ $mktl_bool = true; }
	}else{
		$mktl_bool = true;
	}
	return $mktl_bool;
}


function mkt_marketer_join($mb)
{
	global $nm_member;

	$mktl_bool = false;

	$mkt_no = get_session('ss_mkt_no');
	$mktl_no = get_session('ss_mktl_no');

	// 세션이 없다면 쿠키로
	if($mktl_no == "" || $mktl_no == NULL || $mktl_no == 0){
		$mkt_no = get_cookie('ss_mkt_no');
		$mktl_no = get_cookie('ss_mktl_no');
	}
	
	if($mktl_no == "" || $mktl_no == NULL || $mktl_no == 0){

	}else{

		$row_mkt = mkt_row($mkt_no);
		$row_mktl = mkt_mktl_row($mkt_no, $mktl_no);

		$mktj_para_name = $row_mkt['mkt_para_join'];
		$mkt_para_join = get_session('ss_'.$row_mkt['mkt_para_join']);

		$sql_insert_stats_mktj = "
			INSERT INTO marketer_join (mkt_no, mktl_no, mktj_member, mktj_member_idx, mktj_para_name, mktj_para_val, mktj_date 
			) VALUES ('".$mkt_no."', '".$mktl_no."', '".$mb['mb_no']."', 
			'".$mb['mb_idx']."', '".$mktj_para_name."', '".$mkt_para_join."', 
			'".NM_TIME_YMDHIS."'
		)";

		// 마케팅업체 링크가입 정보 업데이트
		$sql_mb_update = "update member set mb_mkt='".$mkt_no."' where mb_no='".$mb['mb_no']."' AND mb_idx='".$mb['mb_idx']."' ";
		
		if($nm_member['mb_class'] != "a"){
			mkt_join_update($row_mktl);

			mkt_join_stats_update($row_mktl);

			sql_query($sql_insert_stats_mktj);
			sql_query($sql_mb_update);

			switch($mkt_no){
				case '1':	mkt_adplex($row_mkt, $row_mktl, $mb);
				break;

			}

			// 세션변수값 삭제
			del_session('ss_mkt_no');
			del_session('ss_mktl_no');
			del_session('ss_'.$row_mkt['mkt_para_join']);

			$mktl_bool = true;
		}else{
			$mktl_bool = true;
		}
	}
	return $mktl_bool;
}


function mkt_adplex($row_mkt, $row_mktl, $mb){
	/* 
	애드티브이노베이션
	개발팀 김정은 피디
	Direct Phone : 070-4334-5521
	HP : 010-2727-5720
	email: jjung830@adtive.co.kr
	
	회원가입(전환)시 아래와 같은 curl로 전송해 주시면 됩니다.
	curl "https://www.adplex.co.kr/api_v1/cpa_data" -H "WWW-Authenticate: G1ROemLPr35aMCoTPvuPWMQA6TCXTyIN"  -X "POST"  -d "adpx_be_cd=226_2683_4124_74012&is_mobile=1&unique_key=1111" --ipv4 

	정상적으로 crul이 전송되면 아래와 같은 형식의 json 데이타가 출력됩니다.
	{"result_code":200,"result_msg":"Success :  CPA Data insert  ","result_data":{"idx":"24320","campaign_code":"4068","campaign_name":"\ubbf8\uc18c\uc124CPA(\uacfc\uae08)","is_mobile":1,"money":"1650","reg_user_key":"1111"}}


	curl로 전송되는 데이타에 대해 설명 드리면,
	curl "https://www.adplex.co.kr/api_v1/cpa_data" 
	-H "WWW-Authenticate: <<api 인증키>>"  -X "POST"  
	-d "adpx_be_cd=<<유입시 adx_be_cd 파라미터 값>>&is_mobile=<<모바일이면 1, 피씨면 0>>&unique_key=<<회원 아이디나 회원번호>>" --ipv4 

	<<api 인증키>>  <--- "G1ROemLPr35aMCoTPvuPWMQA6TCXTyIN" 넥스큐브의 api 인증키 입니다.
	<<유입시 adx_be_cd 파라미터 값>> <---- "goo.gl/7e1AbG?comics=805&adpx_be_cd=226_2683_4124_74012" 와 같은 URL로 넥스큐브 사이트 유입하면, adpx_be_cd 파라미터의 값("226_2683_4124_74012")을 쿠키나 세션에 저장하셨다가 전달해주세요. 해당 값은 광고 유입을 추적하기 위한 데이타로 숫자_숫자_숫자_숫자 형식입니다. 이것을 이용해서 광고 효율을 올리는 작업을 진행합니다.
	<<모바일이면 1, 피씨면 0>> <--- 사용자 환경에 따라서 1/0 으로 해주세요.
	<<회원 아이디나 회원번호>> <--- 회원 아이디나 회원번호 입니다. 데이타를 비교하기 위한 회원고유키 값을 보내주세요.
		
	URL: https://www.adplex.co.kr/cpa/cpa_action_list
	ID: nexcube1
	PW: nexcube123

	*/
	

	$is_mobile = "0";
	if(is_mobile()){ $is_mobile = "1"; }
	
	$adpx_be_cd = get_session('ss_'.$row_mkt['mkt_para_join']);

	if (function_exists('curl_init')) { // crul 직접 사용
		$url = "https://www.adplex.co.kr/api_v1/cpa_data";
		$post_data  = array(
			"adpx_be_cd"	=>$adpx_be_cd, 
			"is_mobile"		=>$is_mobile, 
			"unique_key"	=>$mb['mb_no']
		);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('WWW-Authenticate: G1ROemLPr35aMCoTPvuPWMQA6TCXTyIN'));
		$adplex = curl_exec($ch);
		curl_close($ch);
		$adplex_result = implode('||',json_decode(stripslashes($adplex),true));
		// print_r($adplex);
		// echo $adplex_result;

		// 결과 저장
		$sql_adplex_result = "INSERT INTO x_marketer_adplex (x_adpx_be_cd, x_is_mobile, x_unique_key, x_result, x_adpx_date 
		) VALUES ('".$adpx_be_cd."', '".$is_mobile."', '".$mb['mb_no']."', 
		'".$adplex_result."', '".NM_TIME_YMDHIS."' ) ";
		sql_query($sql_adplex_result);
	}else{
		echo "crul 안됨";
	}
}

// ddt 파일 연결 데이터 트래킹 툴 (Data Traking Tools)
function mkt_ddt_connect(){
	$ddt_list = scandir(NM_DTT_PATH);
	foreach($ddt_list as $ddt_key => $ddt_name){
		if($ddt_name == ".." || $ddt_name == "."){ continue; }
		
		$ddt_file = NM_DTT_PATH.'/'.$ddt_name;
		if(is_dir($ddt_file)){ continue; }
		
		if(file_exists($ddt_file)){ 
			include_once($ddt_file);
		}		
	} 	
}

// rdt 파일 연결 데이터 트래킹 툴 (Retargeting Display Network)
function mkt_rdt_connect(){
	$rdt_list = scandir(NM_RDT_PATH);
	foreach($rdt_list as $rdt_key => $rdt_name){
		if($rdt_name == ".." || $rdt_name == "."){ continue; }
		
		$rdt_file = NM_RDT_PATH.'/'.$rdt_name;
		if(is_dir($rdt_file)){ continue; }
		
		if(file_exists($rdt_file)){ 
			include_once($rdt_file);
		}		
	} 	
}

/* rdt_acecounter_file start */

//rdt-acecounter-login
function rdt_acecounter_ss_login($value=''){
	if($value=='y'){
		set_session('rdt_acecounter_ss_login', $value);
	}else{
		del_session('rdt_acecounter_ss_login');
	}
}

//rdt-acecounter-join
function rdt_acecounter_ss_join($value=''){
	if($value=='y'){
		set_session('rdt_acecounter_ss_join', $value);
	}else{
		del_session('rdt_acecounter_ss_join');
	}
}

//rdt-acecounter-leave
function rdt_acecounter_ss_leave($value=''){
	if($value=='y'){
		set_session('rdt_acecounter_ss_leave', $value);
	}else{
		del_session('rdt_acecounter_ss_leave');
	}
}

//rdt-acecount-login
function mkt_rdt_acecounter_login($nm_member){
	$rdt_acecounter_file = NM_MKT_PATH.'/rdt_acecounter/acecounter_login.php';
	if(file_exists($rdt_acecounter_file)){ 
		include_once($rdt_acecounter_file);
	}	
}

//rdt-acecount-join
function mkt_rdt_acecounter_join($nm_member){
	$rdt_acecounter_file = NM_MKT_PATH.'/rdt_acecounter/acecounter_join.php';
	if(file_exists($rdt_acecounter_file)){ 
		include_once($rdt_acecounter_file);
	}	
}

//rdt-acecount-leave
function mkt_rdt_acecounter_leave($nm_member){
	$rdt_acecounter_file = NM_MKT_PATH.'/rdt_acecounter/acecounter_leave.php';
	if(file_exists($rdt_acecounter_file)){ 
		include_once($rdt_acecounter_file);
	}	
}

//rdt-acecounter-search
function mkt_rdt_acecounter_search($search_txt){
	$rdt_acecounter_file = NM_MKT_PATH.'/rdt_acecounter/acecounter_search.php';
	if(file_exists($rdt_acecounter_file)){ 
		include_once($rdt_acecounter_file);
	}	
}

//rdt-acecount-comicsview
function mkt_rdt_acecounter_comicsview($comics, $episode){
	$rdt_acecounter_file = NM_MKT_PATH.'/rdt_acecounter/acecounter_comicsview.php';
	if(file_exists($rdt_acecounter_file)){ 
		include_once($rdt_acecounter_file);
	}	
}

//rdt-acecount-recharge
function mkt_rdt_acecounter_recharge($nm_member, $acecounter_order ){
	$rdt_acecounter_file = NM_MKT_PATH.'/rdt_acecounter/acecounter_recharge.php';
	if(file_exists($rdt_acecounter_file)){ 
		include_once($rdt_acecounter_file);
	}	
}

/* rdt_acecounter_file end */


?>