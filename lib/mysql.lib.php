<?
if (!defined('_NMPAGE_')) exit;
// mysql_query 와 mysql_error 를 한꺼번에 처리
// mysql connect resource 지정 - 명랑폐인님 제안
function sql_query($sql, $error=NM_DISPLAY_SQL_ERROR)
{
    global $connect;

    // Blind SQL Injection 취약점 해결
    $sql = trim($sql);
    // union의 사용을 허락하지 않습니다.
    //$sql = preg_replace("#^select.*from.*union.*#i", "select 1", $sql);
    $sql = preg_replace("#^select.*from.*[\s\(]+union[\s\)]+.*#i ", "select 1", $sql);
    // `information_schema` DB로의 접근을 허락하지 않습니다.
    $sql = preg_replace("#^select.*from.*where.*`?information_schema`?.*#i", "select 1", $sql);

    if ($error)
        $result = @mysql_query($sql, $connect) or die("<p>$sql<p>" . mysql_errno() . " : " .  mysql_error() . "<p>error file : {$_SERVER['SCRIPT_NAME']}");
    else
        $result = @mysql_query($sql, $connect);

    return $result;
}


// 쿼리를 실행한 후 결과값에서 한행을 얻는다.
function sql_fetch($sql, $error=NM_DISPLAY_SQL_ERROR)
{
    global $connect;

    $result = sql_query($sql, $error);
    //$row = @sql_fetch_array($result) or die("<p>$sql<p>" . mysql_errno() . " : " .  mysql_error() . "<p>error file : $_SERVER['SCRIPT_NAME']");
    $row = sql_fetch_array($result);
    return $row;
}

// 쿼리를 실행한 후 결과값에서 총 갯수 구하기
function sql_count($sql, $data_field, $error=NM_DISPLAY_SQL_ERROR)
{
    global $connect;

    $result = sql_query($sql, $error);
    //$row = @sql_fetch_array($result) or die("<p>$sql<p>" . mysql_errno() . " : " .  mysql_error() . "<p>error file : $_SERVER['SCRIPT_NAME']");
    $row = sql_fetch_array($result);
	$row_data_total = $row[$data_field];
    return $row_data_total;
}

// 결과값에서 한행 연관배열(이름으로)로 얻는다.
function sql_fetch_array($result)
{
    $row = @mysql_fetch_assoc($result);

    return $row;
}

function sql_num_rows($result)
{
    return mysql_num_rows($result);
}

function sql_free_result($result)
{
    return mysql_free_result($result);
}

function sql_data_seek($result, $row_number)
{
    return mysql_data_seek($result, $row_number);
}

function sql_password($value)
{
    // mysql 4.0x 이하 버전에서는 password() 함수의 결과가 16bytes
    // mysql 4.1x 이상 버전에서는 password() 함수의 결과가 41bytes
    $row = sql_fetch(" select password('$value') as pass ");

    return $row['pass'];
}

// INSERT INTO 한 후에 쓰면 되는데 마지막 커리의 테이블 AUTO_INCREMENT 값은 가져온다
function sql_insert_id()
{
    global $connect;

    return mysql_insert_id($connect);
}

function sql_field_names($table)
{
    global $connect;

    $columns = array();

    $sql = " select * from `$table` limit 1 ";
    $result = sql_query($sql, $connect);

	$i = 0;
	$cnt = mysql_num_fields($result);
	while($i < $cnt) {
		$field = mysql_fetch_field($result, $i);
		$columns[] = $field->name;
		$i++;
	}

    return $columns;
}


function sql_error_info()
{
    global $connect;

    return mysql_errno($connect).' : '.mysql_error($connect);
}


function sql_adult($adult='n', $field='')
{
	$sql_field = "";
    if( $adult == '' || $adult == 'n' ||  $field == ''){
		$sql_field = "AND $field = 'n' ";
	}
    return $sql_field;
}

function sql_array_merge($array1, $array2=array(), $array3=array(), $combine='AND')
{
	$sql = "";
	$sql_array_unique = $sql_array_merge = array();
	for($i=1;$i<=3;$i++){
		if(is_array(${'array'.$i})){
		}else{ return $sql; }
	}
	// 병합
	$sql_array_merge = array_merge($array1, $array2, $array3);

	// 중복제거
	$sql_array_unique = array_unique($sql_array_merge);

	if(count($sql_array_unique) > 0){
		$sql = implode(",",$sql_array_unique);
	}
	return $sql;
}


?>