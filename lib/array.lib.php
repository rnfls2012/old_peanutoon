<?
	/* common */
	// 30줄
	$s_limit_text = "줄";
	for($sll=$nm_config['s_limit_min']; $sll<=$nm_config['s_limit_max']; $sll+=10){
		$s_limit_list[$sll] = $sll.$s_limit_text; ; 
	}

	/* 필수 속성 */
	$required_arr = array('*', 'required', '(필수)');

	$a_target['_self'] = '현재창';
	$a_target['_blank'] = '새창';
	
	for($day=1; $day<=31; $day++){
		$day_list[$day] = $day."일";
	}

	for($week=0; $week<=6; $week++){
		$week_list[$week] = get_yoil($week,1,1); 
	}
	
	/* CMS common */
	$d_state['r'] = '예약중';
	$d_state['y'] = '진행중';
	$d_state['n'] = '종료';

	$d_adult['n'] = '청소년';
	$d_adult['y'] = '성인';

	$d_order['asc'] = '오름차순';
	$d_order['desc'] = '내림차순';


		/* /////////////////////// CMS /////////////////////// */

	/* comics */
	$d_cm_service['n'] = '중지';
	$d_cm_service['y'] = '제공';

	$d_cm_end['n'] = '연속';
	$d_cm_end['y'] = '완결';

	$d_cm_public_period[0] = '주기없음';
	$d_cm_public_period[1] = '매주';
	$d_cm_public_period[2] = '격주';
	$d_cm_public_period[3] = '월간';

	$d_cm_date_type['all'] = '최초/최신등록일';
	$d_cm_date_type['cm_reg_date'] = '최초등록일';
	$d_cm_date_type['cm_episode_date'] = '최신등록일';
	
	$d_cm_up_kind[0] = '화';
	$d_cm_up_kind[1] = '권';

	$d_sl_order_field['sl_pay_sum'] = '구매수';
	$d_sl_order_field['sl_open'] = '열람수';

	$d_cr_class['al']	= '전체 순위';
	$d_cr_class['wt']	= '웹툰 순위';
	$d_cr_class['bk']	= '단행본 순위';
	$d_cr_class['is']	= '급상승 순위';

	$d_cr_t_class['al_t']	= '전체-청소년 순위';
	$d_cr_t_class['wt_t']	= '웹툰-청소년 순위';
	$d_cr_t_class['bk_t']	= '단행본-청소년 순위';
	$d_cr_t_class['is_t']	= '급상승-청소년 순위';

	/*
	$d_crs_class['al']	= '전체 순위';
	$d_crs_class['1']	= '드라마 순위';
	$d_crs_class['2']	= 'TL 순위';
	$d_crs_class['3']	= '코믹 순위';
	$d_crs_class['4']	= '순정 순위';
	$d_crs_class['5']	= 'BL/GL 순위';
	$d_crs_class['6']	= '성인 순위';
	$d_crs_class['7']	= '소설 순위';

	$d_crs_t_class['al_t']		= '전체-청소년 순위';
	$d_crs_t_class['1_t']		= '드라마-청소년 순위';
	$d_crs_t_class['2_t']		= 'TL-청소년 순위';
	$d_crs_t_class['3_t']		= '코믹-청소년 순위';
	$d_crs_t_class['4_t']		= '순정-청소년 순위';
	$d_crs_t_class['5_t']		= 'BL/GL-청소년 순위';
	$d_crs_t_class['6_t']		= '성인-청소년 순위';
	$d_crs_t_class['7_t']		= '소설-청소년 순위';
	*/

	$d_ranking_mark['n']	= '진입';
	$d_ranking_mark['ff']	= '급상승';
	$d_ranking_mark['f']	= '상승';
	$d_ranking_mark['m']	= '고정';
	$d_ranking_mark['p']	= '강하';
	$d_ranking_mark['pp']	= '급강하';

	$d_ranking_mark_icon['n']	= 'sign-in';
	$d_ranking_mark_icon['ff']	= 'long-arrow-up';
	$d_ranking_mark_icon['f']	= 'chevron-up';
	$d_ranking_mark_icon['m']	= 'minus';
	$d_ranking_mark_icon['p']	= 'chevron-down';
	$d_ranking_mark_icon['pp']	= 'long-arrow-down';
	
	
	$d_class_display['y'] = '모두보임';
	$d_class_display['n'] = '숨김';
	$d_class_display['web'] = 'Web';
	$d_class_display['mob'] = 'Mobile';
	$d_class_display['app'] = 'App';

	/* member */
	$d_mb_class['c'] = '고객';
	$d_mb_class['p'] = '파트너';
	$d_mb_class['a'] = '관리자';

	$d_mb_date_type['mb_login_date'] = '최근 로그인';
	$d_mb_date_type['mb_join_date'] = '가입날짜';
	$d_mb_date_type['mb_out_date'] = '탈퇴일';
	
	$d_mb_sex['m'] = '남성';
	$d_mb_sex['w'] = '여성';
	$d_mb_sex['n'] = '미인증';

	$d_mb_post['y'] = '동의';
	$d_mb_post['n'] = '거부';

	$d_point_mode['mpi'] = '획득내역';
	$d_point_mode['mpec'] = '사용내역';

	$d_own_type['0'] = '무료';
	$d_own_type['1'] = '소장';
	$d_own_type['2'] = '대여';
	$d_own_type['3'] = '에러';

	$d_mbe_way['1'] = '개별';
	$d_mbe_way['7'] = '전체';

	$d_mb_state['y'] = '진행중';
	$d_mb_state['n'] = '탈퇴';
	$d_mb_state['s'] = '정지';

	$d_mb_sns_type['']		= '일반';
	$d_mb_sns_type['fcb']	= '페이스북';
	$d_mb_sns_type['twt']	= '트위터';
	$d_mb_sns_type['nid']	= '네이버';
	$d_mb_sns_type['ggl']	= '구글';
	$d_mb_sns_type['kko']	= '카카오';

	
	$d_mb_won['1'] = '결제';
	$d_mb_won['2'] = '미결제';

	$d_is_error['e'] = '에러';
	$d_is_error['s'] = '정상';
	
	/* board */
		/* 도움말 */
	$d_bh_category['1'] = '회원';
	$d_bh_category['2'] = '결제';
	$d_bh_category['3'] = '이용';
		// $bh_category = array("" ,"회원", "결제", "이용"); // 카테고리 용 배열

		/* 문의/제안 */
		$ba_mode = array("답변 대기", "답변 완료", "", "", "공지"); // 상태 용 배열


	/* event */
		/* 충전 */
		$d_er_calc_way['a'] = $nm_config['cf_cash_point_unit_ko'].' 지급값 추가로 더하기(+)';
		$d_er_calc_way['p'] = $nm_config['cf_cash_point_unit_ko'].' 지급값 퍼센트 추가로 더하기(%)';
		$d_er_calc_way['m'] = $nm_config['cf_cash_point_unit_ko'].' 지급값 곱하기(*)';
		$d_er_calc_way['d'] = '충전 가격 할인(%)';

		$d_er_date_type['all'] = '시작일/종료일';
		$d_er_date_type['er_date_start'] = '시작일';
		$d_er_date_type['er_date_end'] = '종료일';

		/* 무료&코인할인 */
		$d_ep_date_type['all'] = '시작일/종료일';
		$d_ep_date_type['ep_date_start'] = '시작일';
		$d_ep_date_type['ep_date_end'] = '종료일';

	/* epromotion */
		/* 팝업 */
		$d_emp_device['pc_mob'] = 'PC&모바일';
		$d_emp_device['pc'] = 'PC';
		$d_emp_device['mob'] = '모바일';

		$d_emb_script['y'] = '사용';
		$d_emb_script['n'] = '안함';

		$d_emb_target['_self'] = '현재창';
		$d_emb_target['_blank'] = '새창';

		$d_eme_event_type['t'] = '기본게시판';
		$d_eme_event_type['c'] = '단일코믹스번호';
		$d_eme_event_type['e'] = '이벤트번호';
		$d_eme_event_type['u'] = 'URL';

		/* 배너 위치 리스트 */
		foreach($nm_config['cf_banner_position'] as $cf_bp_key => $cf_bp_val){
			if($cf_bp_val == ''){ continue; }
			$cf_bp_arr = explode(": ", $cf_bp_val);
			$cf_bp_name = $cf_bp_arr[0];
			$cf_bp_explan = $cf_bp_arr[1];
			$d_banner_position[$cf_bp_key] = $cf_bp_name;
		}


	/* sales */
	$week_en = array('sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat');
	
	$d_authty['card'] = '카드';
	$d_authty['hp'] = '핸드폰';
	$d_authty['virtual'] = '가상계좌';

	$d_rsuccyn['y'] = '성공';
	$d_rsuccyn['n'] = '실패';
	
	/* cmsconfig */
	$d_pg['t'] = '테스트모드';
	$d_pg['r'] = '실결제모드';

	$d_crp_advise['y'] = '추천함';
	$d_crp_advise['n'] = '추천안함';
	
	$d_toss_status['PAY_CANCEL'] = "취소";
	$d_toss_status['PAY_SUCCESS'] = "완료";
	$d_toss_status['PAY_COMPLETE'] = "완료";
	
	$d_csa_buy_than['n'] = '미만';
	$d_csa_buy_than['y'] = '이상';

	$d_sr_way['mobx'] = "핸드폰";
	$d_sr_way['card'] = "신용카드";
	$d_sr_way['acnt'] = "계좌이체";
	$d_sr_way['scbl'] = "도서 문화상품권";
	$d_sr_way['schm'] = "해피머니";
	$d_sr_way['sccl'] = "문화상품권";
	$d_sr_way['toss'] = "토스";
	$d_sr_way['payco'] = "페이코";

	$d_sr_way_payco['01'] = "신용카드(일반)";
	$d_sr_way_payco['02'] = "무통장입금";
	$d_sr_way_payco['04'] = "계좌이체";
	$d_sr_way_payco['05'] = "핸드폰(일반)";
	$d_sr_way_payco['31'] = "신용카드";
	$d_sr_way_payco['35'] = "바로이체";
	$d_sr_way_payco['60'] = "핸드폰";
	$d_sr_way_payco['98'] = "PAYCO 포인트";
	$d_sr_way_payco['75'] = "PAYCO 쿠폰";
	$d_sr_way_payco['76'] = "카드 쿠폰";
	$d_sr_way_payco['77'] = "가맹점 쿠폰";
	$d_sr_way_payco['96'] = "충전금 환불";

	$d_mpu_class['r'] = '충전';
	$d_mpu_class['e'] = '이벤트충전';
	$d_mpu_class['b'] = '구매';
	
	/* marketer */
	$d_mkt_type['i'] = '내부 마케팅';
	$d_mkt_type['o'] = '외부 마케팅';
	$d_mkt_type['s'] = 'SNS';

	$d_mkt_medium['page'] = 'page(페이지/무료)';
	$d_mkt_medium['cpc'] = 'cpc(클릭당단가)';


		/* //////////////////////// Service  //////////////////////// */
	/* 상단 */
	$adult_reverse[''] = 'y';
	$adult_reverse['y'] = 'n';
	$adult_reverse['n'] = 'y';

	/* 상단 로그인메뉴 */
	$d_logmenu['mycomics'] = '내 서재';
	$d_logmenu['myinfo'] = '내 정보';
	$d_logmenu['recharge'] = $nm_config['cf_cash_point_unit_ko'].' 충전소';
	$d_logmenu['ctcertify'] = '본인인증';
	$d_logmenu['coupon'] = '쿠폰함';

	/* comicsview */
	$d_cm_page_way['s'] = '아래';
	$d_cm_page_way['r'] = '오른쪽';
	$d_cm_page_way['l'] = '왼쪽';

?>