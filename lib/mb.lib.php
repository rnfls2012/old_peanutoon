<?
if (!defined('_NMPAGE_')) exit;

// 회원 정보를 얻는다.
function mb_get($mb_id, $table='member', $fields='*', $mb_state='y')
{	// get_member
	$mb_id = base_filter($mb_id);
	$mb_idx = mb_get_idx($mb_id);
	
    return sql_fetch("select $fields from $table where mb_id = '$mb_id' and mb_idx = '$mb_idx' and mb_state = '$mb_state'");
}

function mb_get_idx($mb_id)
{	// get_mb_idx
	$mb_idx = substr($mb_id, 0, 1);
    return $mb_idx;
}

function mb_get_no($mb_no, $table='member', $fields='*')
{	// get_member_no
	$mb_no = base_filter($mb_no);
    return sql_fetch(" select $fields from $table where mb_no = '$mb_no' ");
}

function mb_get_ndx($mb_no)
{	// get_mb_ndx
	$mb_ndx = substr($mb_no,-1);
    return $mb_ndx;
}

// 회원ID체크
function mb_is($mb_id, $table='member')
{	// is_member
	$is_member = false;
	$mb_id = base_filter($mb_id);
	$mb_idx =mb_get_idx($mb_id);
	$row_is_member = sql_fetch(" select count(*)as cnt from $table where mb_id = '$mb_id' and mb_idx = '$mb_idx' ");
	if($row_is_member['cnt'] > 0){
		$is_member = true;
	}
    return $is_member;
}

// 회원 Email체크
function mb_is_email($mb_email, $table='member')
{	// is_email
	$is_email = false;
	$mb_email = base_filter($mb_email);
	$row_is_email= sql_fetch(" select count(*)as cnt from $table where mb_email = '$mb_email'");
	if($row_is_email['cnt'] > 0){
		$is_email = true;
	}
    return $is_email;
}

// 회원 목록만
function mb_only($mb_level_list){
	// only_member
	unset($mb_level_list[0]);  /* 해당 배열 삭제 */
	unset($mb_level_list[1]);  /* 해당 배열 삭제 */
	return $mb_level_list;
}

// 관리자&운영자
function mb_is_admin(){
    global $nm_config, $nm_member;

	$mb_is_admin = false;

	$cf_admin_level = intval($nm_config['cf_admin_level']+$nm_config['cf_admin_level_plus']); // 마스터&운영자 -> 현재 29
	
	if( intval($nm_member['mb_level']) >= intval($cf_admin_level) || intval($nm_member['mb_level']) == 25 ){ $mb_is_admin = true; }

    return $mb_is_admin;
}

/*******************************************************************************
셀랙트박스 생성
*******************************************************************************/
function mb_selects($data, $selectid, $selected_no, $selected_all='y', $selected_all_text='전체', $selected_all_val=0, $key_s='', $key_e=''){
	/*	tag_selects(데이터배열, ID, 선택한값=키값과일치해야함, 전체선택, 전체텍스트, 전체값
		tag_selects($data, 'id', 0, 'y', '전체', 0)
	*/
	
    global $_mb_class, $nm_config;
	$mb_leval_limit_s = 1;
	$mb_leval_limit_e = 30;
	if($_mb_class == 'p'){ 
		$mb_leval_limit_s = $nm_config['cf_partner_level']; 
		$mb_leval_limit_e = $nm_config['cf_admin_level']-1; 
	}
	else if($_mb_class == 'a'){ 
		$mb_leval_limit_s = $nm_config['cf_admin_level']; 
		$mb_leval_limit_e = $nm_config['cf_admin_level']+9; 
	}
	else if($_mb_class == 'c'){ 
		$mb_leval_limit_s = $nm_config['cf_partner_level']-9; 
		$mb_leval_limit_e = $nm_config['cf_partner_level']-1; 
	}
	echo '<select name="'.$selectid.'" id="'.$selectid.'" class="'.$selectid.'">';
		$select_selected = "";
		$select_no = $selected_all_val;
		if($selected_no == ""){ $select_selected = 'selected="selected"'; }
		if($selected_all == 'y'){ echo '<option value="'.$select_no.'" '.$select_selected.'>'.$selected_all_text.'</option>'; }
		foreach($data as $data_key => $data_val) {
			if($data_val == ""){ continue; }
			if($key_s !='' && $key_s > $data_key){ continue; }
			if($key_e !='' && $key_e <= $data_key){ continue; }
			
			if(($_mb_class !='' && !($data_key >= $mb_leval_limit_s && $data_key <= $mb_leval_limit_e))){ continue; }

			$select_no = $data_key;
			$select_selected = "";
			if($select_no == $selected_no && $selected_no != ""){ $select_selected = 'selected="selected"'; }
			echo '<option value="'.$select_no.'" '.$select_selected.'>'.$data_val.'</option>'	;

		}						
	echo '</select>';
}
/*******************************************************************************
cms 체크박스 생성
*******************************************************************************/
function mb_cms_checkbox($menu_list, $checkboxid, $checked_lst_no){
	// cms_checkbox
	/*	cms_checkbox(데이터배열, ID, 선택한값=키값과일치해야함, 전체선택, 전체텍스트, 전체값
		cms_checkbox($data, 'id', 0)
	*/
	$checked_no = explode("|", $checked_lst_no);
	foreach($menu_list as $menu_key => $menu_val) {

		$check_no = $data_key;
		$checkbox_checked = '';
		$label_name = $menu_val[0][0];
		$checkbox_value = $menu_val[0][2];
		if(in_array($checkbox_value,$checked_no)){
			$checkbox_checked = 'checked="checked"';
		}
		echo '<input type="checkbox" name="'.$checkboxid.$check_no.'_'.$checkbox_value.'" id="'.$checkboxid.$check_no.'_'.$checkbox_value.'" value="'.$checkbox_value.'" '.$checkbox_checked.'><label for="'.$checkboxid.$check_no.'_'.$checkbox_value.'"><strong>'.$label_name.'</strong></label>';	
		
		foreach($menu_val as $menu2_key => $menu2_val){
			if(!is_array($menu2_val[3])){continue;}
			if($menu2_key == 0){continue;}
			$mbv_total = count($menu2_val);
			for($mb_i=3; $mb_i<= $mbv_total; $mb_i++){
				unset($menu2_val[$mb_i]);
			}
			$label2_name = $menu2_val[0];
			$checkbox2_value = $menu2_val[2];
			$checkbox_checked = '';
			if(in_array($checkbox2_value,$checked_no)){
				$checkbox_checked = 'checked="checked"';
			}
			echo '<input type="checkbox" class="'.$checkboxid.$check_no.'_'.$checkbox_value.'" name="'.$checkboxid.$check_no.'_'.$checkbox2_value.'" id="'.$checkboxid.$check_no.'_'.$checkbox2_value.'" value="'.$checkbox2_value.'" '.$checkbox_checked.'><label for="'.$checkboxid.$check_no.'_'.$checkbox2_value.'">'.$label2_name.'</label>';	

		}
		echo '<br/>';
	}
}

// 핸드폰자리
function mb_get_phone($phone){
	// get_phone
	$mb_phone = "";
	if(strlen($phone > 9)){
		$mb_phone = substr($phone,0,3)."-".substr($phone,3,4)."-".substr($phone,7,4);
		if(strlen($phone) < 11){
			$mb_phone = substr($phone,0,3)."-".substr($phone,3,3)."-".substr($phone,6,4);
		}
	}else{
		$mb_phone = "-";
	}
	return $mb_phone;
}

// 회원 연령대
function mb_get_age_group ($birth)
{	// get_age
	if($birth == ""){ $age = 0; }
	else{
		$birth_year = substr($birth, 0, 4);
		$age = intval(intval((NM_TIME_Y - $birth_year) / 10) * 10);
	}
    return $age;
}

// 회원 나이
function mb_get_age($birth)
{	// get_mb_age
	if($birth == ""){ $age = ""; }
	else{
		$birth_year = substr($birth, 0, 4);
		$age = intval(NM_TIME_Y - $birth_year);
	}
    return $age;
}

/* 회원 충전 포인트 내용  */
function mb_set_point_income($nm_member, $point_class, $cash_point=0, $point=0, $from='', $mpi_state='1', $mpi_free='n', $used='y')
{	// set_point_income

	$sql_insert = "INSERT INTO member_point_income ("; 
	$sql_insert.= "mpi_member,mpi_member_idx,"; /* 회원번호, 회원ID-index */
	$sql_insert.= "mpi_cash_point, mpi_cash_point_balance,";
	$sql_insert.= "mpi_point, mpi_point_balance,";
	$sql_insert.= "mpi_state, mpi_free,";
	$sql_insert.= "mpi_age, mpi_sex,";
	$sql_insert.= "mpi_kind, mpi_user_agent,";
	$sql_insert.= "mpi_version, mpi_from,";
	$sql_insert.= "mpi_year_month, mpi_year, mpi_month, mpi_day, mpi_hour, mpi_week, mpi_date ";
	$sql_insert.= ") VALUES ( "; 

	$sql_insert.= '"'.$nm_member['mb_no'].'", '; 
	$sql_insert.= '"'.$nm_member['mb_idx'].'", '; 
	if($point_class == 'mb_cash_point'){
		$sql_insert.= '"'.$cash_point.'", '; 
		$sql_insert.= '"'.intval(intval($nm_member['mb_cash_point'])+$cash_point).'", '; 
		$sql_insert.= '"0", '; 
		$sql_insert.= '"'.$nm_member['mb_point'].'", ';
	}else if($point_class == 'mb_point'){
		$sql_insert.= '"0", '; 
		$sql_insert.= '"'.$nm_member['mb_cash_point'].'", '; 
		$sql_insert.= '"'.$point.'", '; 
		$sql_insert.= '"'.intval(intval($nm_member['mb_point'])+$point).'", '; 
	}else if($point_class == 'mb_cash_point_mb_point'){
		$sql_insert.= '"'.$cash_point.'", '; 
		$sql_insert.= '"'.$nm_member['mb_cash_point']+$cash_point.'", '; 
		$sql_insert.= '"'.$point.'", '; 
		$sql_insert.= '"'.$nm_member['mb_point']+$point.'", '; 
	}else{
		alert("set_point_income point_class 에러",$_SERVER['HTTP_USER_AGENT']);
		die;
	}
	$sql_insert.= '"'.$mpi_state.'", '; 
	$sql_insert.= '"'.$mpi_free.'", '; 
	$sql_insert.= '"'.mb_get_age_group($nm_member['mb_birth']).'", '; 
	$sql_insert.= '"'.$nm_member['mb_sex'].'", '; 
	$sql_insert.= '"'."bw:".get_brow($_SERVER['HTTP_USER_AGENT'])." & os:".get_os($_SERVER['HTTP_USER_AGENT']).'", '; 
	$sql_insert.= '"'.addslashes($_SERVER['HTTP_USER_AGENT']).'", '; 
	$sql_insert.= '"'.NM_VERSION.'", '; 
	$sql_insert.= '"'.$from.'", '; 
	$sql_insert.= '"'.NM_TIME_YM.'", '; 
	$sql_insert.= '"'.NM_TIME_Y.'", '; 
	$sql_insert.= '"'.NM_TIME_M.'", '; 
	$sql_insert.= '"'.NM_TIME_D.'", '; 
	$sql_insert.= '"'.NM_TIME_H.'", '; 
	$sql_insert.= '"'.NM_TIME_W.'", '; 
	$sql_insert.= '"'.NM_TIME_YMDHIS.'" '; 
	$sql_insert.= "); "; 
	sql_query($sql_insert);

	// 사용내역 저장
	if($used == 'y'){
		$boolean = cs_set_member_point_used_income($nm_member, $cash_point, $point, 0, $from, 'cms-admin', '', 'r', $mpi_free);
		// 회원정보, 캐쉬포인트, 포인트, 원, 내역, 이벤트타입, 충전주문번호, 내역구분, 무료여부
	}

}

// 성인 쿠키&설정 확인
function mb_adult_cookie($adult='', $nm_member='')
{	
	global $is_member;

	$adult_arr = array();
	$adult_permission = $get_adult_cookie = 'n';

	if($adult == '' && $is_member == true){ $adult = get_cookie('ck_mb_adult'); }

	// cookie_adult	
	if($nm_member['mb_id'] != ""){
		set_cookie('ck_mb_adult', $adult);
	}
	if($adult != ''){ $get_adult_cookie = $adult; }
	array_push($adult_arr, $get_adult_cookie);

	// db_adult
	if($nm_member['mb_adult'] == 'y'){
		$get_mb_adult = 'y';
	}
	
	if($adult == 'y' && $get_mb_adult == 'y'){
		$adult_permission = 'y';
	}
	array_push($adult_arr, $adult_permission);
	return $adult_arr;
}


// 회원 정보를 얻는다.
function mb_sns_get($mb_id, $table='member_sns', $fields='*')
{	// get_member
	$mbs_id = base_filter($mb_id);
	$mbs_idx = mb_get_idx($mb_id);
    return sql_fetch(" select $fields from $table where mbs_id = '$mbs_id' and mbs_idx = '$mbs_idx' ");
}

// 회원 충전 내역을 배열로 얻는다
function mb_get_income($mb_arr, $order_field="", $order="", $limit=0) {
	$sql = "select * from member_point_income where mpi_member = ".$mb_arr['mb_no']." and mpi_member_idx = '".$mb_arr['mb_idx']."' ";

	if($order_field != "") {
		$sql .= "order by ".$order_field." ".$order;
	} // end if 정렬 입력시
	
	if($limit != 0) {
		$sql .= " LIMIT 0, $limit";
	} // end if 갯수 제한 입력시
	$arr = array();
	$result = sql_query($sql);

	while($row = sql_fetch_array($result)) {
		array_push($arr, $row);
	} // end while

	return $arr;
} // mb_get_income

// 회원 이벤트 충전내역을 배열로 얻는다
function mb_get_income_event($mb_arr, $order_field="", $order="", $limit=0) {
	$sql = "select * from member_point_income_event where mpie_member = ".$mb_arr['mb_no']." and mpie_member_idx = '".$mb_arr['mb_idx']."' ";

	if($order_field != "") {
		$sql .= "order by ".$order_field." ".$order;
	} // end if 정렬 입력시
	
	if($limit != 0) {
		$sql .= " LIMIT 0, $limit";
	} // end if 갯수 제한 입력시
	$arr = array();
	$result = sql_query($sql);

	while($row = sql_fetch_array($result)) {
		array_push($arr, $row);
	} // end while

	return $arr;
} // mb_get_income_event

// 사용내역을 배열로 얻는다
function mb_get_expen($mb_arr) {
	global $d_cm_up_kind;
	global $nm_config;
	// $mb_arr['mb_no'] = 10606;
	// $mb_arr['mb_idx'] = 't';
	$result_point_expen = "select * from member_point_expen_comics mpec
							   left JOIN comics c ON mpec.mpec_comics = c.cm_no
							   where mpec.mpec_member=".$mb_arr['mb_no']." and mpec.mpec_member_idx='".$mb_arr['mb_idx']."' order by mpec.mpec_date desc LIMIT 0, 50";
	
		$result_point_expen= sql_query($result_point_expen);

		$pe_arr = array();

		while($row=sql_fetch_array($result_point_expen)) {
			array_push($pe_arr, $row); // sql문의 결과값을 저장
		} // end while (expen)

		$arr = array();

		for($i=0; $i<count($pe_arr); $i++) {
					/* 날짜 */
					$db_point_date_ymd = get_ymd($pe_arr[$i]['mpec_date']);

					/* 내역 상세 */
					$db_from = "";
					
					// 아래 db_from 은 나중에 위한 소스 몇화 몇화 몇화 구매했다는 소스임
					$sql_mpee = "SELECT mpee_member, mpee_comics, mpee_episode, mpee_chapter FROM  member_point_expen_episode_".$pe_arr[$i]['mpec_big']." 
												WHERE  mpee_member = '".$mb_arr['mb_no']."' 
												AND  mpee_member_idx =  '".$mb_arr['mb_idx']."' 
												AND  mpee_comics = '".$pe_arr[$i]['mpec_comics']."' 
												";
					$result_mpee = sql_query($sql_mpee);
					$row_size_mpee = sql_num_rows($result_mpee);

					/*
					if($row_size_mpee > 0){
						while ($row_mpee = sql_fetch_array($result_mpee)) {
							$chapter_arr.= $row_mpee['mpee_chapter'].$d_cm_up_kind[$pe_arr[$i]['cm_up_kind']].", ";
						} // end while
						$chapter_arr = substr($chapter_arr,0,strrpos($chapter_arr, ","));
					} // end if
					
					$db_from = $pe_arr[$i]['cm_series']."<br/>";
					$db_from.= $chapter_arr." 구매";
					*/
					// 임시적으로 사용 안함 - 아래 db_from 은 나중에 위한 소스 몇화 몇화 몇화 구매했다는 소스임

					// 카운트 해서 몇화구매로 우선 보이게 함
					$db_from = $pe_arr[$i]['cm_series']."<br/>".$pe_arr[$i]['mpec_count']." ".$d_cm_up_kind[$pe_arr[$i]['cm_up_kind']]." 구매";

					/* 획득 / 사용 내역 */
					$db_cash_point = $pe_arr[$i]['mpec_cash_point']." ".$nm_config['cf_cash_point_unit_ko'];
					$db_point =  $pe_arr[$i]['mpec_point']." ".$nm_config['cf_point_unit_ko'];

					$tmp_arr = array('date' => $db_point_date_ymd, 'from' => $db_from, 'expen' => $db_cash_point." / ".$db_point);

					array_push($arr, $tmp_arr);				
		} // end for	

	return $arr;
} // mb_get_expen

// 회원 1:1문의내역을 배열로 얻는다
function mb_get_ask($mb_arr, $order_field="", $order="", $limit=0) {
	$sql = "select * from board_ask where ba_member = ".$mb_arr['mb_no']." and ba_member_idx = '".$mb_arr['mb_idx']."' ";

	if($order_field != "") {
		$sql .= "order by ".$order_field." ".$order;
	} // end if 정렬 입력시
	
	if($limit != 0) {
		$sql .= " LIMIT 0, $limit";
	} // end if 갯수 제한 입력시
	$arr = array();
	$result = sql_query($sql);

	while($row = sql_fetch_array($result)) {
		array_push($arr, $row);
	} // end while
	
	return $arr;
} // mb_get_ask

/*******************************************************************************
service 
*******************************************************************************/
// 회원정보 업데이트
function mb_login_date_update($nm_member, $mb_token='')
{
	$sql_mb_update = "	UPDATE member SET mb_login_date='".NM_TIME_YMDHIS."',
										  mb_human_date = '', mb_human_email_date = '', mb_human_note = '' ";
	
	/*
	// db에 토큰, 아이폰/안드로이드 정보 추가
	// /lib/app.lib.php로 옮김
	// globalhu 여기부터
	if($_SESSION['app']){
		$device=($_SESSION['device'])?1:0;
		$sql_mb_update .=	" ,mb_token = '".$_SESSION['token']."' , mb_device={$device} ";
	}
	// globalhu 여기까지
	*/

	/* APK 로그인시 카운트 업 0619 추가 */
	if(preg_match('/'.NM_APP_SETAPK.'/i', HTTP_USER_AGENT)) {
		// if(app_ver()>=NM_APP_IS_VER){ //180605
		if(app_is_chk()){ //180610 -> NM_APP_IS_VER 버전 무조건
			$mb_apk_count = $nm_member['mb_apk_is_count'];
			$mb_apk_count += 1;
			$sql_mb_update .= ", mb_apk_is_count = '".$mb_apk_count."' ";
		}else{
			$mb_apk_count = $nm_member['mb_apk_count'];
			$mb_apk_count += 1;
			$sql_mb_update .= ", mb_apk_count = '".$mb_apk_count."' ";
		}

		// APP 경우 토큰값 저장
		if($mb_token != ''){
			if($nm_member['mb_token'] == '' || $nm_member['mb_token'] != $mb_token){
				$sql_mb_update .= ", mb_token = '".$mb_token."' ";
			}
		}
	} // end if

	$sql_mb_update.= app_mb_sql(); // 위 db에 토큰, 아이폰/안드로이드 정보 추가 수정
	$sql_mb_update.= " WHERE mb_id = '".$nm_member['mb_id']."' AND mb_idx = '".$nm_member['mb_idx']."' AND mb_state = 'y' ";

	sql_query($sql_mb_update);
}

// 회원로그
function mb_stats_log_member_update($nm_member)
{	
	$login_device = 'pc';
	$app_col = "";
	$app_val = "";
	$slm_kind = get_brow(HTTP_USER_AGENT);

	if(is_mobile()) { 
		$login_device = 'mobile';
	} // end if 
	
	if(preg_match('/'.NM_APP_MARKET.'/i', HTTP_USER_AGENT)) {	
		$app_col = ', slm_app_apk';
		$app_val = ", '".NM_APP_MARKET."'";
		$slm_kind = 'App'.$_SESSION['appver'];
	} else if(preg_match('/'.NM_APP_SETAPK.'/i', HTTP_USER_AGENT)){	
		$app_col = ', slm_app_apk';
		$app_val = ", '".NM_APP_SETAPK."'";
		$slm_kind = 'App'.$_SESSION['appver'];
	} // end else if

	$sql_log_update = "INSERT INTO stats_log_member (slm_member, slm_member_idx, slm_date, 
											 slm_type, slm_ip, slm_kind, 
											 slm_user_agent, slm_version".$app_col.") 
									 VALUES ('".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', '".NM_TIME_YMDHIS."', 
											 '".get_os(HTTP_USER_AGENT)."', '".REMOTE_ADDR."', '".$slm_kind."', 
											 '".HTTP_USER_AGENT."', '".$login_device.".".NM_VERSION."'".$app_val.")";
	sql_query($sql_log_update);
}

// cms 접근 권한
function mb_cms_access()
{
    global $is_partner, $is_admin, $nm_member;

	$alert_url = NM_URL;
	if(count($nm_member) < 10){
		$alert_url = NM_URL."/ctlogin.php";
	}
	if(!($is_partner == true || $is_admin == true)){
		alert('접근 권한이 없습니다.', $alert_url);
	}
}

// cms 접근 회원별 권한
function mb_cms_level_access($nm_member, $_cms_current_folder, $menu='n')
{
    global $nm_config;

	$menu_access = true;

	$sql_level = "SELECT * FROM config_member_buff WHERE cfmb_member_level = '".$nm_member['mb_level']."'";

	$row_level = sql_fetch($sql_level);
	$cfmb_level_permission_arr = explode("|", $row_level['cfmb_level_permission']);
	
	$cf_admin_level = intval($nm_config['cf_admin_level']+$nm_config['cf_admin_level_plus']); // 마스터&운영자

	if($nm_member['mb_level'] < $cf_admin_level){
		if(!in_array($_cms_current_folder, $cfmb_level_permission_arr)){
			if($menu == 'n'){
				$alert_url = NM_URL;
				if(count($nm_member) < 10){
					alert(1);
					$alert_url = NM_URL."/ctlogin.php";
				}
				alert('접근 권한이 없습니다.', $alert_url);
			}else{
				$menu_access = false;
			}
		}
	}
	return $menu_access;
}

function mb_cms_level_access_firstlink($nm_member, $menu_val)
{
	foreach($menu_val as $menu_key2 => $menu_val2){ if($menu_key2 == 0){continue;} 
		/* 하위폴더 링크로 연동 */
		$steps2_link = $menu_val2[1];
		if(is_array($menu_val2[3])){
			$steps2_link = $menu_val2[3][1];
		}
		

		/* 권한이 있는 메뉴만 보기 */
		$mb_cms_level_access2 = mb_cms_level_access($nm_member, $menu_val2[2], 'y');
		if($mb_cms_level_access2 == false){ continue; }		
		if($firstlink == ""){ $firstlink = $steps2_link; }	
		// echo $firstlink."<br/>";	
	}
	return $firstlink;
}


// 회원정보 업데이트(금일 로그 날짜)
function mb_login_today_update($nm_member)
{	
	$sql_mb_update = "	UPDATE member SET mb_login_today='".NM_TIME_YMD."'  
						WHERE mb_id = '".$nm_member['mb_id']."' AND mb_idx = '".$nm_member['mb_idx']."' ";
	sql_query($sql_mb_update);
}

// 회원정보 출석 업데이트
function mb_attend($nm_member)
{	
	// 출석 이벤트 아직 안됨
	/*
	$mb_attend_result = false;
	if($nm_member['mb_attend_day'] == NM_TIME_YMD){

	}else{

	}

	$sql_mb_update = "  UPDATE member SET mb_attend_date='".NM_TIME_YMD."', 
										  mb_attend_day=(mb_attend_day+1) 
						WHERE mb_id = '".$nm_member['mb_id']."' AND mb_idx = '".$nm_member['mb_idx']."' ";
	sql_query($sql_mb_update);

	return $mb_attend_result;
	*/
}

function mb_get_buy_episode($nm_member, $comics, $episode='n')
{
	$mb_buy_episode_arr = array();
	$get_comics = get_comics($comics);

	if($get_comics['cm_no'] == ''){ cs_alert('작품이 없습니다.', NM_URL); }

	$sql = "SELECT * FROM member_buy_episode_".$get_comics['cm_big']." 
			WHERE 1 AND mbe_comics = '".$get_comics['cm_no']."' 
					AND mbe_member = '".$nm_member['mb_no']."' 
					AND (mbe_own_type = '1' 
						OR (mbe_own_type = '2' AND mbe_end_date >= '".NM_TIME_YMD."' )
					)
			ORDER BY mbe_order DESC, mbe_chapter DESC ";
	/*
	$sql = "SELECT * FROM member_buy_episode_".$get_comics['cm_big']." 
			WHERE 1 AND mbe_comics = '".$get_comics['cm_no']."' 
					AND mbe_member = '".$nm_member['mb_no']."' 
					AND mbe_member_idx = '".$nm_member['mb_idx']."' 
					AND (mbe_own_type = '1' 
						OR (mbe_own_type = '2' AND mbe_end_date >= '".NM_TIME_YMD."' )
					)
			ORDER BY mbe_order DESC, mbe_chapter DESC ";
	*/
	$result = sql_query($sql);
	if(sql_num_rows($result) != 0){
		while ($row = sql_fetch_array($result)) {
			if($episode == 'y'){
				array_push($mb_buy_episode_arr, $row['mbe_episode']);
			}else{
				array_push($mb_buy_episode_arr, $row);
			}
		}
	}
	return $mb_buy_episode_arr;
}

function mb_update_point($nm_member, $cash_point, $point, $mb_won=0, $mb_won_count=0)
{	
	$boolean = false;
	$sql_mb_won = $sql_mb_won_count = "";

	if($mb_won != 0){
		$sql_mb_won = ", mb_won = ( mb_won + '".intval($mb_won)."' ) ";
	}

	if($mb_won_count != 0){
		$sql_mb_won_count = ", mb_won_count = ( mb_won_count + '".intval($mb_won_count)."' ) ";
	}

	/* 181001 */
	// if($mb_won > 0 && $mb_won_count > 0){
	/*
	if($mb_won == 0 && $mb_won_count == 0){

	}else{
		*/
		$sql_mb_won = ", mb_won = ( mb_won + '".intval($mb_won)."' ) ";
		$sql_mb_won_count = ", mb_won_count = ( mb_won_count + '".intval($mb_won_count)."' ) ";

		$mb_cash_point = intval($nm_member['mb_cash_point']) + $cash_point;
		$mb_point = intval($nm_member['mb_point']) + $point;

		$sql_update_member = "update member set mb_cash_point='".$mb_cash_point."', mb_point='".$mb_point."' 
		".$sql_mb_won."".$sql_mb_won_count." 
		where mb_no='".$nm_member['mb_no']."' and mb_id='".$nm_member['mb_id']."'";

		if(sql_query($sql_update_member)){ $boolean = true; }
	// }

	return $boolean;
}

function mb_set_cash($nm_member, $payway, $payway_name)
{
	// 결제정보 배열로 받기 => $payway
	// pc사 영문이름 => $payway_name

	$boolean = false;
	
	$mc_won = intval($payway[$payway_name.'_amt']);
	$mc_cash_point = intval($payway[$payway_name.'_cash_point']) + intval($payway[$payway_name.'_event_cash_point']); 
	$mc_point = intval($payway[$payway_name.'_point']) + intval($payway[$payway_name.'_event_point']);

	$mc_product = $payway[$payway_name.'_product'];
	$mc_way = $payway_name;
	if($mc_way == "kcp"){ $mc_way = $payway['kcp_payway']; }
	
	$mc_way_payco = '';
	if($mc_way == "payco"){ $mc_way_payco = $payway['payco_payment_method_code']; }

	$mc_order = $payway[$payway_name.'_order'];
	$mc_platform = cs_platform(HTTP_USER_AGENT);

	$mc_pg_no = $payway[$payway_name.'_no'];

	$mc_crp_no = $payway[$payway_name.'_crp_no'];
	$mc_er_no = $payway[$payway_name.'_er_no'];
	$mc_re_pay = 0;
	if(intval($nm_member['mb_won_count']) > 0 ){ $mc_re_pay = 1; }	

	$sql_mc = "INSERT INTO member_cash (
	mc_member, mc_member_idx, 
	mc_won, mc_cash_point, mc_point, 
	mc_product, mc_way, mc_way_payco, 

	mc_order, mc_platform, 
	mc_pg, mc_pg_no, 

	mc_crp_no, mc_er_no, mc_re_pay, 
	mc_useragent, mc_version, mc_date 
	) VALUES (
	'".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 
	'".$mc_won."', '".$mc_cash_point."', '".$mc_point."', 
	'".$mc_product."', '".$mc_way."', '".$mc_way_payco."', 

	'".$mc_order."', '".$mc_platform."', 
	'".$payway_name."', '".$mc_pg_no."', 
	
	'".$mc_crp_no."', '".$mc_er_no."', '".$mc_re_pay."', 
	'".HTTP_USER_AGENT."', '".NM_VERSION."', '".NM_TIME_YMDHIS."' 
	)";
	if(sql_query($sql_mc)){ $boolean = true; }
	return $boolean;
}

function mb_set_attend($nm_member, $mb_attend_day)
{	// mb_attend_day : 출석이벤트 날짜
	
	$boolean = false;

	$sql_update_mb_attend = "update member set mb_attend_day='".$mb_attend_day."', mb_attend_date='".NM_TIME_YMDHIS."' 
	where mb_no='".$nm_member['mb_no']."' and mb_id='".$nm_member['mb_id']."'";
	
	if(sql_query($sql_update_mb_attend)){ $boolean = true; }
	return $boolean;
}

function mb_partner_acceess($nm_member)
{
	if(is_nexcube()){

	}else{
		if(NM_PARTNER_LIMIT == true){
			if($nm_member['mb_class'] == 'p'){
				alert('CMS 관련해서 수정중입니다. 빠른 시일내로 수정해서 오픈하겠습니다. 사용하시는 데 불편을 드려서 죄송합니다.', NM_URL);
				exit();
			}
		}
	}
}

function mb_cm_cp_sql($nm_member)
{
	$sql_mb_cm_cp = "";
	$error_msg = "이용하실수 없습니다. 등급 또는 등급번호를 확인을 관리자에게 문의 바랍나다.[error code : class&class_no ]";

	if($nm_member['mb_class'] == 'a'){
		$sql_mb_cm_cp = "";
	}else if($nm_member['mb_class'] == 'p' && $nm_member['mb_level'] == '13'){ // 마켓터
		$sql_mb_cm_cp = $nm_member['mb_class_no'];
	}else{
		if($nm_member['mb_class'] == 'p' && intval($nm_member['mb_class_no']) > 0){
			$partner_arr = array();
			switch($nm_member['mb_level']){
				case '14': array_push($partner_arr, 'cm_provider', 'cm_provider_sub'); // 제공사(CP)
				break;
				case '15': array_push($partner_arr, 'cm_professional', 'cm_professional_sub', 'cm_professional_thi'); // 작가
				break;
				case '16': array_push($partner_arr, 'cm_publisher'); // 출판사
				break;
			}
			if(count($partner_arr) > 0){
				$sql_mb_cm_cp = " AND ( ";
				foreach($partner_arr as $mb_cm_cp_val){
					if( strpos($mb_cm_cp_val, '_sub') > 0 || strpos($mb_cm_cp_val, '_thi') > 0 ){ $sql_mb_cm_cp.= " OR "; }
					$sql_mb_cm_cp.= $mb_cm_cp_val."='".$nm_member['mb_class_no']."' ";
				}
				$sql_mb_cm_cp.= " ) ";
			}else{
				alert($error_msg, NM_URL);
				exit();
			}
		}else{
			alert($error_msg, NM_URL);
			exit();
		}
	}
	return $sql_mb_cm_cp;
}

function mb_partner_e_date($nm_member, $_e_date)
{
	$e_date = $_e_date;
	if($nm_member['mb_class'] == 'p'){
		if($_e_date > NM_TIME_M1){ $e_date = NM_TIME_M1; }
	}
	return $e_date;
}

function mb_partner_e_month($nm_member, $_e_month)
{
	$e_month = $_e_month;
	if($nm_member['mb_class'] == 'p'){
		if($_e_month > NM_TIME_MON_M1){ $e_month = NM_TIME_MON_M1; }
	}
	return $e_month;
}

function mb_partner_cp($nm_member)
{
	$mb_partner_cp = false;
	switch($nm_member['mb_level']){
		case '14': $mb_partner_cp = true;
		case '15': $mb_partner_cp = true;
		case '16': $mb_partner_cp = true;
		break;
		default: $mb_partner_cp = false;
		break;
	}
	return $mb_partner_cp;
}

function mb_class_permission($mb_class='c')
{
	global $nm_member;

	$mb_class_permission = false;
	$mb_class_arr = explode(",", $mb_class);
	
	// 회원 등급 확인
	$nm_member_class = 'c';
	if($nm_member['mb_class'] != ''){
		$nm_member_class = $nm_member['mb_class'];
	}
	
	// 지정된 등급	
	foreach($mb_class_arr as $mb_class_val){
		if($nm_member_class == $mb_class_val){
			$mb_class_permission = true;
		}
	}
	return $mb_class_permission;
}

function mb_set_invite_code($nm_member) {
	$mb_code_origin = $nm_member['mb_no'].get_ymd($nm_member['mb_join_date']);
	$enc_code = Security::encrypt($mb_code_origin, 'invite'); // KEY는 invite로 설정
	// $dec_code = Security::decrypt($enc_code, 'invite'); 디코딩 코드
	$code_update_sql = "UPDATE member SET mb_invite_code = '".$enc_code."' WHERE mb_no = '".$nm_member['mb_no']."'";
	return sql_query($code_update_sql);
}

function mb_get_reward_state($mb_no, $emrm_no) {
	$reward_state = "n";
	$select_sql = "SELECT * FROM event_member_reward WHERE emrm_no = '".$emrm_no."' AND emr_member = '".$mb_no."'";
	$select_row = sql_fetch($select_sql);

	if($select_row['emr_state'] == "n") {
		$reward_state = "y";
	} // end if

	return $reward_state;
}

function mb_state_stop_logout($nm_member)
{
	$msg = '사용하신 계정은 정지된 계정입니다. 관리자에게 문의 바랍니다. ';
	
	$link = '/proc/ctlogout.php';
	
	if($_SERVER['PHP_SELF'] != $link){
		if($nm_member['mb_state_sub'] == 's'){ // 상태가 중지인 회원
			alert($msg, NM_URL.$link);
			die;
		}
	}
}

?>