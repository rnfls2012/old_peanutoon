<?
if (!defined('_NMPAGE_')) exit;

// APK 파일 다운로드 통계
function st_stats_log_apk_update($nm_member, $download_type="")
{	
    global $nm_config;

	$login_device = 'pc';
	$sla_kind = get_brow(HTTP_USER_AGENT);
	$sla_member = 0;

	if(is_mobile()) { 
		$login_device = 'mobile';
	} // end if 

	if($nm_member['mb_no'] != '') {
		$sla_member = $nm_member['mb_no'];
	} // end if
	
	$sla_download_type = "";
	if($download_type == "update"){ $sla_download_type = $download_type; }
	
	$sql_log_update = "INSERT INTO stats_log_app (sla_member, sla_member_idx, sla_date, 
											 sla_type, sla_ip, sla_kind, 
											 sla_user_agent, sla_version, sla_download_version, sla_download_type) 
									 VALUES ('".$sla_member."', '".$nm_member['mb_idx']."', '".NM_TIME_YMDHIS."', 
											 '".get_os(HTTP_USER_AGENT)."', '".REMOTE_ADDR."', '".$sla_kind."', 
											 '".HTTP_USER_AGENT."', '".$login_device.".".NM_VERSION."', 
											 '".floatval($nm_config['cf_appsetapk'])."', '".$sla_download_type."')";

	sql_query($sql_log_update);
}

// APK 파일 다운로드 - 다음에 하기 통계
function st_stats_log_apk_update_later($nm_member, $user_appver=0)
{	
	$login_device = 'pc';
	$slal_kind = get_brow(HTTP_USER_AGENT);
	$slal_member = 0;

	if(is_mobile()) { 
		$login_device = 'mobile';
	} // end if 

	if($nm_member['mb_no'] != '') {
		$slal_member = $nm_member['mb_no'];
	} // end if
	
	$slal_user_version = 0;
	if(floatval($user_appver) > 0){ $slal_user_version = floatval($user_appver); }
	
	$sql_log_update = "INSERT INTO stats_log_app_later (slal_member, slal_member_idx, slal_date, 
											 slal_type, slal_ip, slal_kind, 
											 slal_user_agent, slal_version, slal_user_version) 
									 VALUES ('".$slal_member."', '".$nm_member['mb_idx']."', '".NM_TIME_YMDHIS."', 
											 '".get_os(HTTP_USER_AGENT)."', '".REMOTE_ADDR."', '".$slal_kind."', 
											 '".HTTP_USER_AGENT."', '".$login_device.".".NM_VERSION."', '".$slal_user_version."')";

	sql_query($sql_log_update);
}

function st_stats_event_attend_cms($ea_arr, $set_date, $z_ma_point)
{
		
		$ea_arr_original = $ea_arr;
		
		$ea_oneday_point = 'NULL';
		$ea_cont_date_1 = 'NULL';
		$ea_cont_date_2 = 'NULL';
		$ea_cont_date_3 = 'NULL';
		$ea_cont_point_1 = 'NULL';
		$ea_cont_point_2 = 'NULL';
		$ea_cont_point_3 = 'NULL';
		if($ea_arr['ea_oneday_point'] != ''){ $ea_oneday_point = "'".$ea_arr['ea_oneday_point']."'"; }
		if($ea_arr['ea_cont_date_1'] != ''){ $ea_cont_date_1 = "'".$ea_arr['ea_cont_date_1']."'"; }
		if($ea_arr['ea_cont_date_2'] != ''){ $ea_cont_date_2 = "'".$ea_arr['ea_cont_date_2']."'"; }
		if($ea_arr['ea_cont_date_3'] != ''){ $ea_cont_date_3 = "'".$ea_arr['ea_cont_date_3']."'"; }
		if($ea_arr['ea_cont_point_1'] != ''){ $ea_cont_point_1 = "'".$ea_arr['ea_cont_point_1']."'"; }
		if($ea_arr['ea_cont_point_2'] != ''){ $ea_cont_point_2 = "'".$ea_arr['ea_cont_point_2']."'"; }
		if($ea_arr['ea_cont_point_3'] != ''){ $ea_cont_point_3 = "'".$ea_arr['ea_cont_point_3']."'"; }

		$sql_insert = "
		INSERT INTO stats_event_attend_cms (
		seac_year, seac_month,  
		ea_date, ea_no, 
		seac_start_date, seac_end_date, 
		seac_oneday_point, 
		seac_cont_date_1, seac_cont_point_1, 
		seac_cont_date_2, seac_cont_point_2, 
		seac_cont_date_3, seac_cont_point_3, 
		seac_cont_use, seac_regu_date, seac_regu_point, 
		seac_regu_use, seac_use, seac_date ) VALUES ( 
		'".$set_date['year']."', '".$set_date['month']."', 
		'".$ea_arr['ea_date']."', '".$ea_arr['ea_no']."', 
		'".$ea_arr['ea_start_date']."', '".$ea_arr['ea_end_date']."', 
		".$ea_oneday_point.", 
		".$ea_cont_date_1.", ".$ea_cont_point_1.", 
		".$ea_cont_date_2.", ".$ea_cont_point_2.", 
		".$ea_cont_date_3.", ".$ea_cont_point_3.", 
		'".$ea_arr['ea_cont_use']."', '".$ea_arr['ea_regu_date']."', '".$ea_arr['ea_regu_point']."', 
		'".$ea_arr['ea_regu_use']."', '".$ea_arr['ea_use']."', '".NM_TIME_YMDHIS."'
		) ";

	@mysql_query($sql_insert); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
	st_stats_event_attend($ea_arr_original, $set_date, $z_ma_point);
}

function st_stats_event_attend_cms_row($ea_arr, $set_date)
{
	$sql = "SELECT * FROM stats_event_attend_cms WHERE 
			seac_year='".$set_date['year']."' AND seac_month='".$set_date['month']."' 
			AND ea_date='".$ea_arr['ea_date']."'  AND ea_no='".$ea_arr['ea_no']."' 
			";
	return sql_fetch($sql);
}

function st_stats_event_attend($ea_arr, $set_date, $z_ma_point)
{
	$row_seac_cms = st_stats_event_attend_cms_row($ea_arr, $set_date);

	$sql_insert = "
		INSERT INTO stats_event_attend ( seac_no, 
		sea_year_month, 
		sea_year, sea_month, sea_day, sea_hour, sea_week 
		) VALUES ( '".$row_seac_cms['seac_no']."', 
		'".$set_date['year_month']."', 
		'".$set_date['year']."', '".$set_date['month']."', '".$set_date['day']."', 
		'".$set_date['hour']."', '".$set_date['week']."' 
	)";

	$sea_oneday_point = $sea_cont_point_1 = $sea_cont_point_2 = $sea_cont_point_3 = $sea_regu_point = 0;
	if($ea_arr['ea_oneday_point'] == $z_ma_point){ $sea_oneday_point++; }
	if($ea_arr['ea_cont_point_1'] == $z_ma_point){ $sea_cont_point_1++; }
	if($ea_arr['ea_cont_point_2'] == $z_ma_point){ $sea_cont_point_2++; }
	if($ea_arr['ea_cont_point_3'] == $z_ma_point){ $sea_cont_point_3++; }
	if($ea_arr['ea_regu_point'] == $z_ma_point){ $sea_regu_point++; }

	$sql_set = "";
	
	$sql_update = "
		UPDATE stats_event_attend SET 
		sea_oneday_point= (sea_oneday_point+".intval($sea_oneday_point)."), 
		sea_cont_point_1= (sea_cont_point_1+".intval($sea_cont_point_1)."), 
		sea_cont_point_2= (sea_cont_point_2+".intval($sea_cont_point_2)."), 
		sea_cont_point_3= (sea_cont_point_3+".intval($sea_cont_point_3)."), 
		sea_regu_point= (sea_regu_point+".intval($sea_regu_point).") 		
		WHERE 1 
		AND seac_no = '".$row_seac_cms['seac_no']."' 
		AND sea_year_month = '".$set_date['year_month']."' AND sea_year = '".$set_date['year']."' 
		AND sea_month = '".$set_date['month']."' AND sea_day = '".$set_date['day']."' 
		AND sea_hour = '".$set_date['hour']."' AND sea_week = '".$set_date['week']."' 
	";
	// 저장
	@mysql_query($sql_insert); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
	sql_query($sql_update);
}

function st_stats_log_share_sns($name, $type='')
{
	global $nm_member;

	$slss_member = intval($nm_member['mb_no']);
	$slss_member_idx = mb_get_idx($nm_member['mb_id']);
	$slss_member_ndx = mb_get_ndx($slss_member);
	$slss_sns = $name;
	$slss_type = $type;
	$slss_ip = REMOTE_ADDR;
	$slss_user_agent = HTTP_USER_AGENT;
	$slss_version = NM_VERSION;
	$slss_date = NM_TIME_YMDHIS;
	
	$sql = " INSERT INTO stats_log_share_sns ( 
			 slss_member, slss_member_idx, slss_member_ndx, 
			 slss_sns, slss_type, slss_ip, slss_user_agent, 
			 slss_version, slss_date
			 )VALUES (
			 '".$slss_member."', '".$slss_member_idx."', '".$slss_member_ndx."', 
			 '".$slss_sns."', '".$slss_type."', '".$slss_ip."', '".$slss_user_agent."', 
			 '".$slss_version."', '".$slss_date."');";
	sql_query($sql);

	// 통계
	st_stats_share_sns($name, $type);
}

function st_stats_share_sns($name, $type='')
{
	$set_date['year_month']	= NM_TIME_YM;
	$set_date['year']		= NM_TIME_Y;
	$set_date['month']		= NM_TIME_M;
	$set_date['day']		= NM_TIME_D;
	$set_date['hour']		= NM_TIME_H;
	$set_date['week']		= NM_TIME_W;

	$sql_insert = "
		INSERT INTO stats_share_sns ( sss_sns, sss_type,
		sss_year_month, sss_year, sss_month, sss_day, sss_hour, sss_week
		) VALUES ('".$name."', '".$type."', 
		'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
		'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."')";
	
					
	$sql_update = "
		UPDATE stats_share_sns SET sss_click = (sss_click + 1) WHERE 1 
		AND sss_sns = '".$name."' AND sss_type = '".$type."' 
		AND sss_year_month = '".$set_date['year_month']."' AND sss_year = '".$set_date['year']."' 
		AND sss_month = '".$set_date['month']."' AND sss_day = '".$set_date['day']."' 
		AND sss_hour = '".$set_date['hour']."' AND sss_week = '".$set_date['week']."' 
	";
	// 저장
	@mysql_query($sql_insert); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
	sql_query($sql_update);
}

function st_stats_invite_join() {
	$set_date['year_month']	= NM_TIME_YM;
	$set_date['year']		= NM_TIME_Y;
	$set_date['month']		= NM_TIME_M;
	$set_date['day']		= NM_TIME_D;
	$set_date['hour']		= NM_TIME_H;
	$set_date['week']		= NM_TIME_W;

	$sql_insert = "
		INSERT INTO stats_invite_join 
		(sij_year_month, sij_year, sij_month, sij_day, sij_hour, sij_week) 
		VALUES 
		('".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', '".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."')";
	
					
	$sql_update = "
		UPDATE stats_invite_join SET sij_join = (sij_join + 1) WHERE 1 
		AND sij_year_month = '".$set_date['year_month']."' AND sij_year = '".$set_date['year']."' 
		AND sij_month = '".$set_date['month']."' AND sij_day = '".$set_date['day']."' 
		AND sij_hour = '".$set_date['hour']."' AND sij_week = '".$set_date['week']."'";

	// 저장
	@mysql_query($sql_insert); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
	sql_query($sql_update);
} 

function st_stats_randombox_pop($result_arr) {
	$set_date['year_month']	= NM_TIME_YM;
	$set_date['year']		= NM_TIME_Y;
	$set_date['month']		= NM_TIME_M;
	$set_date['day']		= NM_TIME_D;
	$set_date['hour']		= NM_TIME_H;
	$set_date['week']		= NM_TIME_W;	
	
	$sql_insert = "
		INSERT INTO stats_event_randombox 
		(ser_er_no, ser_discount_rate, ser_year_month, ser_year, ser_month, ser_day, ser_hour, ser_week) 
		VALUES 
		('".$result_arr['er_no']."', '".$result_arr['ercm_discount_rate']."', '".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', '".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."')";
					
	$sql_update = "
		UPDATE stats_event_randombox SET ser_pop_count = (ser_pop_count + 1) WHERE 1 
		AND ser_year_month = '".$set_date['year_month']."' AND ser_year = '".$set_date['year']."' 
		AND ser_month = '".$set_date['month']."' AND ser_day = '".$set_date['day']."' 
		AND ser_hour = '".$set_date['hour']."' AND ser_week = '".$set_date['week']."'
		AND ser_er_no = '".$result_arr['er_no']."' AND ser_discount_rate = '".$result_arr['ercm_discount_rate']."'";

	// 저장
	@mysql_query($sql_insert); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
	sql_query($sql_update);		
}

// 2018-09-20 추석이벤트 조회 통계
function st_stats_event_thanksgiving_data($comics, $episode, $event_mode) {
	
	$event_date_arr = array();
	$config_time_name = "2018년 추석 타입세일 이벤트";
	$sql = "select ep_date_start from event_pay where ep_text='".$config_time_name."' 
			group by ep_date_start order by ep_date_start asc;";
	$result = sql_query($sql);
	for($i=0; $row = sql_fetch_array($result); $i++){
		array_push($event_date_arr, $row['ep_date_start']);
	}

	// 날짜
	$set_date['year_month']	= NM_TIME_YM;
	$set_date['year']		= NM_TIME_Y;
	$set_date['month']		= NM_TIME_M;
	$set_date['day']		= NM_TIME_D;
	$set_date['hour']		= NM_TIME_H;
	$set_date['week']		= NM_TIME_W;
	$set_date['date']		= NM_TIME_YMD;

	// 이벤트 날짜라면 실행
	if(in_array($set_date['date'],$event_date_arr)){
		st_stats_event_thanksgiving_all_time_open($comics, $episode, $event_mode, $set_date);
	}

}
function st_stats_event_thanksgiving_all_time_open($comics, $episode, $event_mode, $set_date) {

	global $nm_config;

	$dbarray = array('event_thanksgiving_all_time_open', 'event_thanksgiving_all_time_open_episode');

	foreach($dbarray as $dbkey => $dbtable){

		$sql_pay_type = $sql_insert = $sql_update = "";

		switch($dbkey){
			case 0 :				
					$sql_insert = "
						INSERT INTO ".$dbtable." (
						sl_comics, sl_big, 
						sl_year_month, sl_year, 
						sl_month, sl_day, 
						sl_hour, sl_week, 
						sl_event_mode 
						) VALUES ('".$comics['cm_no']."', '".$comics['cm_big']."', 
						'".$set_date['year_month']."', '".$set_date['year']."', 
						'".$set_date['month']."', '".$set_date['day']."', 
						'".$set_date['hour']."', '".$set_date['week']."' , 
						'".$event_mode."' 
					)";
					
					$sql_update = "
						UPDATE ".$dbtable." SET 
						sl_event_open= (sl_event_open+1) 
						WHERE 1 AND sl_comics = '".$comics['cm_no']."' AND sl_big = '".$comics['cm_big']."' 
						AND sl_year_month = '".$set_date['year_month']."' AND sl_year = '".$set_date['year']."' 
						AND sl_month = '".$set_date['month']."' AND sl_day = '".$set_date['day']."' 
						AND sl_hour = '".$set_date['hour']."' AND sl_week = '".$set_date['week']."' 
						AND sl_event_mode = '".$event_mode."'
					";
			break;
			case 1 :			
					$sql_insert = "
						INSERT INTO ".$dbtable."_".$comics['cm_big']." ( 
						se_comics, se_episode, 
						se_year_month, se_year, 
						se_month, se_day, 
						se_hour, se_week, 
						se_event_mode 
						) VALUES ( '".$comics['cm_no']."', '".$episode['ce_no']."', 
						'".$set_date['year_month']."', '".$set_date['year']."', 
						'".$set_date['month']."', '".$set_date['day']."', 
						'".$set_date['hour']."', '".$set_date['week']."' , 
						'".$event_mode."' 
					)";
					
					$sql_update = "
						UPDATE ".$dbtable."_".$comics['cm_big']." SET 
						se_event_open= (se_event_open+1) 
						WHERE 1 AND se_comics = '".$comics['cm_no']."' AND se_episode = '".$episode['ce_no']."' 
						AND se_year_month = '".$set_date['year_month']."' AND se_year = '".$set_date['year']."' 
						AND se_month = '".$set_date['month']."' AND se_day = '".$set_date['day']."' 
						AND se_hour = '".$set_date['hour']."' AND se_week = '".$set_date['week']."' 
						AND se_event_mode = '".$event_mode."'
					";
			break;
		}
		// 저장
		@mysql_query($sql_insert); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
		sql_query($sql_update);
	}
}
// 2018-09-20 추석이벤트 조회 통계 end

?>