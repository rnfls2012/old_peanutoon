<? if (!defined('_NMPAGE_')) exit;


function tksk_ics($tksk_no)
{
	$tksk_ics = '';
	if(intval($tksk_no) > 0){
		$tksk_ics = 'ics'.intval($tksk_no);		
	}
  return $tksk_ics;
}

function tksk_ics_row($tksk_ics)
{
	$tksk_ics_row = false;
	$tksk_no = get_int($tksk_ics);
	
	$tksk_str = get_eng($tksk_ics);
	if($tksk_str == 'ics'){	
		$sql = "SELECT * FROM ticketsocket WHERE tksk_no='".$tksk_no."' ";
		$row = sql_fetch($sql);
		if(intval($row['tksk_no']) > 0){ 
			$tksk_ics_row = $row; 
		}
	}	
  return $tksk_ics_row;
}

function tksk_ics_route($tksk_ics)
{
	$tksk_ics_route = '';
	$tksk_no = get_int($tksk_ics);
	
	$tksk_str = get_eng($tksk_ics);

	$tksk_ics_route = $tksk_str.'/'.$tksk_no;

  return $tksk_ics_route;
}

function tksk_alias($ticketsocket)
{
	$tksk_alias_bool = false;	
	$tksk_str = get_eng($ticketsocket);
	if($tksk_str == 'ics'){	$tksk_alias_bool = true; }
	
  return $tksk_alias_bool;
}

function tksk_ics_log($tksk_order, $tksk_ics, $tksk_row=''){
	
	global $nm_member;

	$device = 'pc';
	if(is_mobile()) { 
		$device = 'mobile';
	} // end if 
	
	// DB값이 없다면;;;
	if($tksk_row == ''){ $tksk_row = tksk_ics_row($tksk_ics); }

	
	$set_date['year_month']		= NM_TIME_YM;
	$set_date['year']			= NM_TIME_Y;
	$set_date['month']			= NM_TIME_M;
	$set_date['day']			= NM_TIME_D;
	$set_date['hour']			= NM_TIME_H;
	$set_date['week']			= NM_TIME_W;
	$set_date['date']			= NM_TIME_YMDHIS;
	
	$tksk_campaign = $tksk_row['tksk_campaign'];
	if($tksk_order['page'] == 'complete'){
		$tksk_campaign = $tksk_row['tksk_campaign_complete'];	
	}
	
	$sql_tksk_ics_log = "
		INSERT INTO z_ticketsocket ( 
		z_tksk_ics, z_tksk_number, z_tksk_email, z_tksk_revenue, z_tksk_name, 
		z_tksk_productname, z_tksk_campaign, z_tksk_page, 
		z_tksk_mb_no, z_tksk_mb_facebook_id, 
		z_tksk_year_month, z_tksk_year, z_tksk_month, 
		z_tksk_day, z_tksk_hour, z_tksk_week, z_tksk_date, 
		z_tksk_version, z_tksk_user_agent )
		VALUES (
		'".$tksk_row['tksk_no']."','".$tksk_order['number']."','".$tksk_order['email']."','".$tksk_order['revenue']."','".$tksk_order['name']."',
		'".$tksk_order['productname']."', '".$tksk_campaign."', '".$tksk_order['page']."',
		'".$nm_member['mb_no']."','".$nm_member['mb_facebook_id']."',
		'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
		'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."', '".$set_date['date']."', 
		'".$device.".".NM_VERSION."', '".HTTP_USER_AGENT."');";
	sql_query($sql_tksk_ics_log);
}

function tksk_ics_nonmember_log($tksk_order, $tksk_ics, $tksk_row=''){
	
	global $nm_member;

	$device = 'pc';
	if(is_mobile()) { 
		$device = 'mobile';
	} // end if 
	
	// DB값이 없다면;;;
	if($tksk_row == ''){ $tksk_row = tksk_ics_row($tksk_ics); }

	
	$set_date['year_month']		= NM_TIME_YM;
	$set_date['year']			= NM_TIME_Y;
	$set_date['month']			= NM_TIME_M;
	$set_date['day']			= NM_TIME_D;
	$set_date['hour']			= NM_TIME_H;
	$set_date['week']			= NM_TIME_W;
	$set_date['date']			= NM_TIME_YMDHIS;
	
	$tksk_campaign = $tksk_row['tksk_campaign'];
	if($tksk_order['page'] == 'complete'){
		$tksk_campaign = $tksk_row['tksk_campaign_complete'];	
	}
	
	$sql_tksk_ics_log = "
		INSERT INTO z_ticketsocket_nonmember ( 
		z_tkskn_ics, z_tkskn_number, 
		z_tkskn_name, z_tkskn_email, z_tkskn_tel, z_tkskn_page, 
		z_tkskn_revenue, z_tkskn_campaign,  
		z_tkskn_year_month, z_tkskn_year, z_tkskn_month, 
		z_tkskn_day, z_tkskn_hour, z_tkskn_week, z_tkskn_date, 
		z_tkskn_version, z_tkskn_user_agent )
		VALUES (
		'".$tksk_row['tksk_no']."','".$tksk_order['number']."',
		'".$tksk_order['name']."','".$tksk_order['email']."','".$tksk_order['tel']."','".$tksk_order['page']."',
		'".intval($tksk_order['revenue'])."', '".$tksk_campaign."', 
		'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
		'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."', '".$set_date['date']."', 
		'".$device.".".NM_VERSION."', '".HTTP_USER_AGENT."');";
	sql_query($sql_tksk_ics_log);
}


class tksk_js
{		
	public static function tksk_ics_peanutoon_js($tksk_order, $tksk_ics, $tksk_pop='y', $mb='y')
	{
		global $nm_member;

		$tksk_no = get_int($tksk_ics);
		$sql = "SELECT * FROM ticketsocket WHERE tksk_no='".$tksk_no."'";
		$row = sql_fetch($sql);
		
		// 로그 & 공유 통계
		/* ajax 처리하기
		if(mb_class_permission()){
			tksk_ics_log($tksk_order, $tksk_ics, $row); // 로그		
			tksk_ics_stats($tksk_order['page'], $tksk_ics, 'y'); // 공유 통계
		}
		*/

		$tksk_campaign = $row['tksk_campaign'];
		if($tksk_order['page'] == 'complete'){
			$tksk_campaign = $row['tksk_campaign_complete'];	
		}
		
		// 비회원이 공유 클릭한 경우
		if(intval($nm_member['mb_no']) == 0 && $nm_member['mb_id'] == '' && $mb == 'y'){
			
			echo "
			<script>
				function tksk_ics_peanutoon_js()
				{
					alertBox('본서비스는 로그인이 필요합니다.', goto_url);
					function goto_url(){
						document.location.href = '".NM_URL."/ctlogin.php';
					}
				}				
			</script>
			";

		}else{
			
			echo "
			<script>
				function tksk_ics_peanutoon_js()
				{
					var order_number	 	= 	'".$tksk_order['number']."';
					var order_revenue 		= 	'".$tksk_order['revenue']."';
					var order_page			= 	'".$tksk_order['page']."';
					var order_name 			= 	'".$tksk_order['name']."';
					var order_productname 	= 	'".$tksk_order['productname']."';
					var tksk_ics_email		=	$('#tksk_ics_email').val();
					var order_email 		= 	tksk_ics_email;

					";
					if($mb != 'y'){
					echo "
						var tksk_ics_name		=	encodeURI($('#tksk_ics_name').val());
						var tksk_ics_tel		=	$('#tksk_ics_tel').val();
						order_name = tksk_ics_name;
						order_productname = tksk_ics_tel;
						";
					}
					echo "
					
					/* 이메일이 없다면 입력 받기 */
					if(order_email == ''){
						alertBoxFocus('이메일을 입력해주세요.', $('#tksk_ics_email'));	
						return false;
					}else{
						if(email_chk(order_email) == false){
							alertBoxFocus('이메일 형식에 맞게 입력해주세요.', $('#tksk_ics_email'));	
							return false;					
						}else{
							tksk_icecreamsocial_stats();
							tksk_icecreamsocial_js();
						}
					}
					
					function tksk_icecreamsocial_stats(){

						var tksk_icecreamsocial_stats_access = $.ajax({
							url: '".NM_TKSK_URL."/stats.php',
							dataType: 'json',
							type: 'POST',
							data: { ics: ".$tksk_no.", order_number: order_number, order_email: order_email, order_revenue: order_revenue, order_page: order_page, order_name:order_name, order_productname:order_productname},
							beforeSend: function( data ) {
								//console.log('로딩');
							}
						})
						tksk_icecreamsocial_stats_access.done(function( data ) {
							//console.log('성공');
						});
						tksk_icecreamsocial_stats_access.fail(function( data ) {
							//console.log('실패');
						});
						tksk_icecreamsocial_stats_access.always(function( data ) {
							//console.log('로딩삭제');
						});

					}
					
					function tksk_icecreamsocial_js(){					    
						(function(i,s,r,publicKey,campaignId,a,m,frame,bodyChild){
							i['IceCreamSocialObject'] = r;
							i[r] = i[r] || function(){( i[r].q = i[r].q||[]).push(arguments)}, i[r].l = +new Date();
							a = s.createElement('script'), m = s.scripts[0];
							a.async = a.src = 'https://app.icecreamsocial.io/js/ics.js';
							m.parentNode.insertBefore(a,m);
							frame = s.createElement('iframe'), bodyChild = s.body.firstChild;
							frame.src = 'https://app.icecreamsocial.io/?campaignId='+campaignId+'&publicKey='+publicKey;
							frame.id='SocialIframe', frame.style.cssText = 'position:fixed;height:100%;width:100%;z-index:9999;border: 0';
							bodyChild.parentNode.insertBefore(frame,bodyChild);
						})(window ,document ,'ics', '".NM_TKSK_KEY."', '".$tksk_campaign."');

						ics('addTransaction', {    
							locale: 'ko-KR',
							orderId: order_number,
							email: order_email,
							revenue: order_revenue,
							name: order_name,
							productName: order_productname
						});
					}
				}

				";
				if($tksk_pop == 'y'){
				echo "			
				$(function(){
					// 1초 티켓소켓 스크립트 실행
					setTimeout(tksk_ics_peanutoon_js, 1000);
				});
				";
				}?>
			<? echo "
				
			</script>
			";
		}
	}
}

function tksk_ics_stats($tksk_file_name, $tksk_ics, $share='n')
{
	$tksk_no = get_int($tksk_ics);
	$sql = "SELECT * FROM ticketsocket WHERE tksk_no='".$tksk_no."'";
	$row = sql_fetch($sql);
	
	$tksk_campaign = $row['tksk_campaign'];
	if($tksk_file_name == 'complete'){
		$tksk_campaign = $row['tksk_campaign_complete'];	
	}

	$stksk_ics = $tksk_no;
	$stksk_ics_page = $tksk_file_name;
	$stksk_campaign = $tksk_campaign;

	$set_date['year_month']	= NM_TIME_YM;
	$set_date['year']		= NM_TIME_Y;
	$set_date['month']		= NM_TIME_M;
	$set_date['day']		= NM_TIME_D;
	$set_date['hour']		= NM_TIME_H;
	$set_date['week']		= NM_TIME_W;
	
	$sql_insert = "
		INSERT INTO stats_ticketsocket (stksk_ics, stksk_ics_page, stksk_campaign, stksk_year_month, stksk_year, stksk_month, stksk_day, stksk_hour, stksk_week 
		) VALUES ('".$stksk_ics."', '".$stksk_ics_page."', '".$stksk_campaign."', 
		'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
		'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."' 
	)";	

	$sql_update_set = " stksk_open= (stksk_open+1) ";
	if($share == 'y'){ $sql_update_set = " stksk_share= (stksk_share+1) "; }
	
	$sql_update = "
		UPDATE stats_ticketsocket SET 
		".$sql_update_set."
		WHERE 1 AND stksk_ics = '".$stksk_ics."' AND stksk_ics_page = '".$stksk_ics_page."' AND stksk_campaign = '".$stksk_campaign."' 
		AND stksk_year_month = '".$set_date['year_month']."' AND stksk_year = '".$set_date['year']."' 
		AND stksk_month = '".$set_date['month']."' AND stksk_day = '".$set_date['day']."' 
		AND stksk_hour = '".$set_date['hour']."' AND stksk_week = '".$set_date['week']."' 
	";

	// 저장
	@mysql_query($sql_insert); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
	sql_query($sql_update);
}



// 가입 관련 소스


function tksk_ics_join($mb)
{
	global $nm_member;

	$tksk_bool = false;

	$tksk_no = get_session('ss_tksk_no');
	$tksk_campaign = get_session('ss_tksk_campaign');

	if($tksk_no == "" || $tksk_no == NULL || $tksk_no == 0){
		// echo "tksk_no error";
	}else{
		// echo "tksk_no enter";

		$sql_insert_stats_tkskj = "
			INSERT INTO ticketsocket_join (tksk_no, tksk_campaign, tkskj_member, tkskj_member_ndx, tkskj_member_idx, tkskj_date 
			) VALUES ('".$tksk_no."', '".$tksk_campaign."', '".$mb['mb_no']."', 
			 '".mb_get_ndx($mb['mb_no'])."', '".$mb['mb_idx']."',
			'".NM_TIME_YMDHIS."'
		)";

		// 마케팅업체 링크가입 정보 업데이트
		$sql_mb_update = "update member set mb_tksk='".$tksk_campaign."' where mb_no='".$mb['mb_no']."' AND mb_idx='".$mb['mb_idx']."' ";
		
		if(mb_class_permission()){
			tksk_ics_join_stats_update($tksk_no, $tksk_campaign);

			sql_query($sql_insert_stats_tkskj);
			sql_query($sql_mb_update);

			// 세션변수값 삭제
			del_session('ss_tksk_no');
			del_session('ss_tksk_campaign');

			$tksk_bool = true;
		}else{
			$tksk_bool = true;
		}
	}
	return $tksk_bool;
}



function tksk_ics_join_stats_update($tksk_no, $tksk_campaign)
{
	global $nm_member;

	$tksk_bool = false;

	$set_date['year_month']	= NM_TIME_YM;
	$set_date['year']		= NM_TIME_Y;
	$set_date['month']		= NM_TIME_M;
	$set_date['day']		= NM_TIME_D;
	$set_date['hour']		= NM_TIME_H;
	$set_date['week']		= NM_TIME_W;

	// 통계
	$sql_insert_stats_tkskj = "
		INSERT INTO stats_ticketsocket_join (stksk_no, stksk_campaign, stkskj_year_month, stkskj_year, stkskj_month, stkskj_day, stkskj_hour, stkskj_week 
		) VALUES ('".$tksk_no."', '".$tksk_campaign."', 
		'".$set_date['year_month']."', '".$set_date['year']."', '".$set_date['month']."', 
		'".$set_date['day']."', '".$set_date['hour']."', '".$set_date['week']."' 
	)";
	$sql_update_stats_tkskj = "
		UPDATE stats_ticketsocket_join SET stkskj_join= (stkskj_join+1) 
		WHERE 1 AND stksk_no = '".$tksk_no."'  AND stksk_campaign = '".$tksk_campaign."' 
		AND stkskj_year_month = '".$set_date['year_month']."' AND stkskj_year = '".$set_date['year']."' 
		AND stkskj_month = '".$set_date['month']."' AND stkskj_day = '".$set_date['day']."' 
		AND stkskj_hour = '".$set_date['hour']."' AND stkskj_week = '".$set_date['week']."' ";
	if($nm_member['mb_class'] != "a"){
		@mysql_query($sql_insert_stats_tkskj); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨
		if(sql_query($sql_update_stats_tkskj)){ $coll_bool = true; }
	}else{
		$tksk_bool = true;
	}
	return $tksk_bool;
}

?>