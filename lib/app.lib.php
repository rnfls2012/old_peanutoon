<?
if (!defined('_NMPAGE_')) exit;

// db에 토큰, 아이폰/안드로이드 정보 추가
function app_mb_sql(){
	$sql_mb_update = "";
	if($_SESSION['app'] != '' && $_SESSION['token'] != ''){
		$device=($_SESSION['device'])?1:0;
		$sql_mb_update = " , mb_token = '".$_SESSION['token']."' , mb_device={$device} ";
	}
	return $sql_mb_update;
}

// 즐겨찾기 업데이트시에 푸시 보내는 함수
function push($message,$idx)
{
	$idxs = array();
	$token = array();
	$token2 = array();

	$sql="select mcm_member from member_comics_mark where mcm_comics = '$idx'";
	$query=sql_query($sql);
	while($result=sql_fetch_array($query)){
		array_push($idxs,$result['mcm_member']);
	}
	$idx=implode(',',$idxs);
	$sql="select mb_token from member where mb_no in ($idx) and mb_device=0 and mb_push=1 and mb_state='y'";
	$query=sql_query($sql);

	while($result=sql_fetch_array($query)){
		array_push($token,$result['mb_token']);
	}

	$sql="select mcm_member from member_comics_mark where mcm_comics = '$idx'";
	$query=sql_query($sql);
	while($result=sql_fetch_array($query)){
		array_push($idxs,$result['mcm_member']);
	}
	$idx=implode(',',$idxs);
	$sql="select mb_token from member where mb_no in ($idx) and mb_device=1 and mb_push=1 and mb_state='y'";
	$query=sql_query($sql);

	while($result=sql_fetch_array($query)){
		array_push($token2,$result['mb_token']);
	}

	$content=$message;

	$push_i = array("body" => $content);
	$push_a = array("message" => $content, "imgurllink" => "", 'link' => "");

	fcm_send($token, $push_a);
	fcm_send($token2, $push_a,$push_i);
	fcm_send_store($token, $push_a);
	fcm_send_store($token2, $push_a,$push_i);
}

// 푸시 APK
function fcm_send ($tokens, $message, $message2 = null)
{
	$url = 'https://fcm.googleapis.com/fcm/send';
	$fields = array(
			'registration_ids' => $tokens,
			'data' => $message,
			'notification' => $message2
		);

	$headers = array(
		'Authorization:key =' . 'AAAAhz0E7kA:APA91bFs7YHxWoCfc6vUy7PL30leqGO2--HJcyOuWsr6QENve2DArxtWx8CXEZnOgv1W7fh88XAdo7-it0_XKJXpWAKojlkVIVpKK4Kk1tXiXi0yQMaZXzmRPcpYAKgF4imDgzdy24Ql',
		'Content-Type: application/json'
		);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	$result = curl_exec($ch);
	if ($result === FALSE) {
		die('Curl failed: ' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}

// 푸시 STORE
function fcm_send_store ($tokens, $message, $message2 = null)
{
	$url = 'https://fcm.googleapis.com/fcm/send';
	$fields = array(
			'registration_ids' => $tokens,
			'data' => $message,
			'notification' => $message2
		);

	$headers = array(
		'Authorization:key =' . 'AAAAAI-W27k:APA91bEMeHhUVWcxJy4PqWjuUzm5-LMM6_E8G4_cBQ6oiBocAsxgY1KvuZTOe-NQMsMYytNoIKnTvG-L-mZ_YMa4weirmXgcxpQIiPQ5TyoQbPy9cG0J-Cdb2lF_Z7ctTHrFwJM9zuxM',
		'Content-Type: application/json'
		);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	$result = curl_exec($ch);
	if ($result === FALSE) {
		die('Curl failed: ' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}

/* 토큰 배열로 푸시 알람 */
function push_msg($token_arr, $message, $link = "") {
	// link는 http://www.peanutoon.com/comics.php?comics=2024 형태로
	$push_i = array("body" => $message); // 아이폰 용 추후 나왔을 시 사용
	$push_a = array("message" => $message, "imgurllink" => "", 'link' => $link);
	
	/* PUSH는 1000개씩 보내도록 되어있으므로 1000개 이상일 때 나눠줘야 한다. */
	if(count($token_arr) > 1000) {
		$token_arr = array_chunk($token_arr, 1000); // 1000개씩 나눔
		
		// PUSH 시작
		for($i=0; $i<count($token_arr); $i++) {
			fcm_send($token_arr[$i], $push_a);
			fcm_send_store($token_arr[$i], $push_a);
		} // end for
	} else {
		fcm_send($token_arr, $push_a);
		fcm_send_store($token_arr, $push_a);
	} // end else
} // end push_msg


/* NM_APP_IS_VER -> 자체 개발 APP */

function app_ver(){
	$app_var = 0;
	$app_var = floatval(substr(HTTP_USER_AGENT,strrpos(HTTP_USER_AGENT, 'APP_VER:')+strlen('APP_VER:'),3));
	return $app_var;
}

function app_device(){
	$app_device = 0;
	$app_device = preg_match('/(iPhone|iPod|iPad)/i',HTTP_USER_AGENT,$matches);

	return $app_device;
}

function app_android(){
	$app_android = '';
	$app_android = floatval(trim(substr(HTTP_USER_AGENT,strrpos(HTTP_USER_AGENT, 'Linux; Android')+strlen('Linux; Android'),4)));

	return $app_android;
}

function app_is_chk(){

	$bool = false;	
	if(strrpos(HTTP_USER_AGENT, 'APP_VER:') > 0){
		$bool = true;	
	}
	return $bool;
}

function app_android_chk(){

	$bool = false;	
	if(strrpos(HTTP_USER_AGENT, 'Linux; Android') > 0){
		$bool = true;	
	}
	return $bool;
}

?>