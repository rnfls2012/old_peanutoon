<?
if (!defined('_NMPAGE_')) exit;

/* 이미지 사이즈 규격 */


// 코믹스 표지이미지 - comics
$thumbnail['cm_cover'] = array(); 
array_push($thumbnail['cm_cover'], array('tn'					=> array('width' => 838,	'height' => 430,	
																		 'size'	 => 0,		'name' => 'cover')));		// [Web]
array_push($thumbnail['cm_cover'], array('tn229x117'			=> array('width' => 229,	'height' => 117,	
																		 'size'	 => 0,		'name' => 'cover')));		// [Web]메인 급상승 작품
array_push($thumbnail['cm_cover'], array('tn388x198'			=> array('width' => 388,	'height' => 198,	
																		 'size'	 => 0,		'name' => 'cover')));		// [Web]장르별 추천 big
array_push($thumbnail['cm_cover'], array('tn226x115'			=> array('width' => 226,	'height' => 115,	
																		 'size'	 => 0,		'name' => 'cover')));		// [Web]장르별 추천 middle

array_push($thumbnail['cm_cover'], array('tn286x147'			=> array('width' => 286,	'height' => 147,	
																		 'size'	 => 0,		'name' => 'cover')));		// [Web]서브 코믹스 리스트
array_push($thumbnail['cm_cover'], array('tn551x282'			=> array('width' => 551,	'height' => 282,	
																		 'size'	 => 0,		'name' => 'cover')));		// [Web]서브 이벤트 리스트

array_push($thumbnail['cm_cover'], array('tn518x265'			=> array('width' => 518,	'height' => 265,	
																		 'size'	 => 0,		'name' => 'cover')));		// [mobile]BL 스크롤
array_push($thumbnail['cm_cover'], array('tn750x384'			=> array('width' => 750,	'height' => 384,	
																		 'size'	 => 0,		'name' => 'cover')));		// [mobile]코믹스 화 리스트

array_push($thumbnail['cm_cover'], array('tn655x335'			=> array('width' => 655,	'height' => 335,	
																		 'size'	 => 0,		'name' => 'cover')));		// [mobile]이벤트 화 리스트


// 코믹스 작은표지이미지 - comics
$thumbnail['cm_cover_sub'] = array(); 
array_push($thumbnail['cm_cover_sub'], array('tn'				=> array('width' => 356,	'height' => 356,	
																		 'size'	 => 0,		'name' => 'cover_sub')));		// [Web]
array_push($thumbnail['cm_cover_sub'], array('tn335x335'		=> array('width' => 335,	'height' => 335,	
																		 'size'	 => 0,		'name' => 'cover_sub')));		// [Web]급상승 big
array_push($thumbnail['cm_cover_sub'], array('tn133x133'		=> array('width' => 133,	'height' => 133,	
																		 'size'	 => 0,		'name' => 'cover_sub')));		// [Web]메인 인기종합순위
array_push($thumbnail['cm_cover_sub'], array('tn50x50'			=> array('width' => 50,		'height' => 50,	
																		 'size'	 => 0,		'name' => 'cover_sub')));		// [Web]메인 인기종합순위
array_push($thumbnail['cm_cover_sub'], array('tn136x136'		=> array('width' => 136,	'height' => 136,	
																		 'size'	 => 0,		'name' => 'cover_sub')));		// [Web]장르별 추천 small

array_push($thumbnail['cm_cover_sub'], array('tn102x102'		=> array('width' => 102,	'height' => 102,	
																		 'size'	 => 0,		'name' => 'cover_sub')));		// [Web]코믹스 화리스트 추천작품

array_push($thumbnail['cm_cover_sub'], array('tn222x222'		=> array('width' => 222,	'height' => 222,	
																		 'size'	 => 0,		'name' => 'cover_sub')));		// [mobile]업데이트 스크롤
array_push($thumbnail['cm_cover_sub'], array('tn75x75'			=> array('width' => 75,		'height' => 75,	
																		 'size'	 => 0,		'name' => 'cover_sub')));		// [mobile]인기작품 슬라이드리스트
array_push($thumbnail['cm_cover_sub'], array('tn249x249'		=> array('width' => 249,	'height' => 249,	
																		 'size'	 => 0,		'name' => 'cover_sub')));		// [mobile]서브 코믹스 리스트


// 코믹스 화이미지 - comics
$thumbnail['cm_cover_episode'] = array(); 
array_push($thumbnail['cm_cover_episode'], array('tn'			=> array('width' => 720,	'height' => 360,	
																		 'size'	 => 0,		'name' => 'cover_episode')));		// [Web]
array_push($thumbnail['cm_cover_episode'], array('tn173x86'		=> array('width' => 173,	'height' => 86,	
																		 'size'	 => 0,		'name' => 'cover_episode')));		// [Web]코믹스 화리스트

array_push($thumbnail['cm_cover_episode'], array('tn668x334'	=> array('width' => 668,	'height' => 334,	
																		 'size'	 => 0,		'name' => 'cover_episode')));		// [mobile]인기작품 슬라이드리스트
array_push($thumbnail['cm_cover_episode'], array('tn640x320'	=> array('width' => 640,	'height' => 320,	
																		 'size'	 => 0,		'name' => 'cover_episode')));		// [mobile]코믹스 화리스트


// 코믹스-에피소드 화이미지 - comics_episode
$thumbnail['ce_cover'] = array(); 
array_push($thumbnail['ce_cover'], array('tn'			=> array('width' => 720,	'height' => 360,	
																 'size'	 => 0,		'name' => 'chapter')));		// [Web]
array_push($thumbnail['ce_cover'], array('tn173x86'		=> array('width' => 173,	'height' => 86,	
																 'size'	 => 0,		'name' => 'chapter')));		// [Web]코믹스 화리스트

array_push($thumbnail['ce_cover'], array('tn668x334'	=> array('width' => 668,	'height' => 334,	
																 'size'	 => 0,		'name' => 'chapter')));		// [mobile]인기작품 슬라이드리스트
array_push($thumbnail['ce_cover'], array('tn640x320'	=> array('width' => 640,	'height' => 320,	
																 'size'	 => 0,		'name' => 'chapter')));		// [mobile]코믹스 화리스트


// 코믹스-에피소드 뷰어 - comics_episode
$thumbnail['ce_file'] = array();
array_push($thumbnail['ce_file'], array('tn'			=> array('width' => 740,		'height' => 0,	
																 'pagesize'	 => 0,		'scrollsize'	 => 0,		
																 'name' => '')));


// 프로모션 배너 배너 - epromotion_banner_cover
$thumbnail['embc_cover'] = array();
array_push($thumbnail['embc_cover'], array('1' => array('width' => 1000,	'height' => 470,	'text' => '[Web]메인슬라이드: width:1000px,height:470px',	
														'size'	 => 0,		'name' => 'banner')));		// [Web]메인슬라이드
array_push($thumbnail['embc_cover'], array('2' => array('width' => 750,		'height' => 384,	'text' => '[Mobile]메인슬라이드: width:750px,height:384px',	
														'size'	 => 0,		'name' => 'banner')));		// [Mobile]메인슬라이드
array_push($thumbnail['embc_cover'], array('3' => array('width' => 390,		'height' => 200,	'text' => '[Web]메인이벤트배너: width:390px,height:200px',	
														'size'	 => 0,		'name' => 'banner')));		// [Web]메인이벤트배너
array_push($thumbnail['embc_cover'], array('4' => array('width' => 592,		'height' => 207,	'text' => '[Mobile]메인이벤트배너: width:592px,height:207px',	
														'size'	 => 0,		'name' => 'banner')));	// [Mobile]메인이벤트배너

array_push($thumbnail['embc_cover'], array('5' => array('width' => 750,		'height' => 105,	'text' => '[Mobile]메인띠배너: width:750px,height:105px',	
														'size'	 => 0,		'name' => 'banner')));		// [Mobile]메인띠배너 보기

array_push($thumbnail['embc_cover'], array('6' => array('width' => 592,		'height' => 207,	'text' => '[Web&Mobile]뷰어띠배너: width:740px,height:116px',	
														'size'	 => 0,		'name' => 'banner')));	// [Web&Mobile]뷰어띠배너

array_push($thumbnail['embc_cover'], array('7' => array('width' => 1200,	'height' => 100,	'text' => '[Web]충전소띠배너: width:1200px,height:100px',	
														'size'	 => 0,		'name' => 'banner')));		// [Web]충전소띠배너
array_push($thumbnail['embc_cover'], array('8' => array('width' => 640,		'height' => 250,	'text' => '[Mobile]충전소띠배너: width:640px,height:250px',	
														'size'	 => 0,		'name' => 'banner')));		// [Mobile]충전소띠배너

array_push($thumbnail['embc_cover'], array('9' => array('width' => 340,		'height' => 119,	'text' => '[Web]화리스트배너: width:340px,height:119px',	
														'size'	 => 0,		'name' => 'banner')));			// [Web]화리스트배너

array_push($thumbnail['embc_cover'], array('10' => array('width' => 640,	'height' => 90,	'text' => '[Mobile]메인슬라이드띠배너: width:750px,height:105px',	
														'size'	 => 0,		'name' => 'banner')));		// [Mobile]메인슬라이드띠배너 보기 // 181030추가


// 이벤트 메인 이미지 - epromotion_event
$thumbnail['eme_cover'] = array();
array_push($thumbnail['eme_cover'], array('tn'					=> array('width' => 1000,	'height' => 417,	
																		 'size'	 => 0,		'name' => 'event')));		// [Web]
array_push($thumbnail['eme_cover'], array('tn250x104'			=> array('width' => 250,	'height' => 104,	
																		 'size'	 => 0,		'name' => 'event')));		// [Web]이벤트 리스트
array_push($thumbnail['eme_cover'], array('tn730x304'			=> array('width' => 730,	'height' => 304,	
																		 'size'	 => 0,		'name' => 'event')));		// [mobile] 이벤트 리스트


// 이벤트 메인 이미지 - event_pay
$thumbnail['ep_cover'] = array();
array_push($thumbnail['ep_cover'], array('tn'					=> array('width' => 1000,	'height' => 417,	
																		 'size'	 => 0,		'name' => 'event')));		// [Web]
array_push($thumbnail['ep_cover'], array('tn250x104'			=> array('width' => 250,	'height' => 104,	
																		 'size'	 => 0,		'name' => 'event')));		// [Web]이벤트 리스트
array_push($thumbnail['ep_cover'], array('tn730x304'			=> array('width' => 730,	'height' => 304,	
																		 'size'	 => 0,		'name' => 'event')));		// [mobile] 이벤트 리스트


// 이벤트 콜렉션 상단 이미지 - event_pay
$thumbnail['ep_collection_top'] = array();
array_push($thumbnail['ep_collection_top'], array('tn'			=> array('width' => 1000,	'height' => 348,	
																		 'size'	 => 0,		'name' => 'event_collection')));		// [Web]
array_push($thumbnail['ep_collection_top'], array('tn750x261'	=> array('width' => 750,	'height' => 261,	
																		 'size'	 => 0,		'name' => 'event_collection')));		// [mobile]

// 확장자
$extension = array(1 => 'gif', 2 => 'jpg', 3 => 'png');

// 피너툰 전용 // 임시
// 코믹스 표지이미지 - comics
$thumbnail['peanutoon_cm_cover'] = array(); 
array_push($thumbnail['peanutoon_cm_cover'], array('tn'					=> array('width' => 838,	'height' => 430,	
																		 'size'	 => '',		'name' => 'cover')));		// [Web]
array_push($thumbnail['peanutoon_cm_cover'], array('tn229x117'			=> array('width' => 229,	'height' => 117,	
																		 'size'	 => '384x200',		'name' => 'cover')));		// [Web]메인 급상승 작품
array_push($thumbnail['peanutoon_cm_cover'], array('tn388x198'			=> array('width' => 388,	'height' => 198,	
																		 'size'	 => '384x200',		'name' => 'cover')));		// [Web]장르별 추천 big
array_push($thumbnail['peanutoon_cm_cover'], array('tn226x115'			=> array('width' => 226,	'height' => 115,	
																		 'size'	 => '214x109',		'name' => 'cover')));		// [Web]장르별 추천 middle

array_push($thumbnail['peanutoon_cm_cover'], array('tn286x147'			=> array('width' => 286,	'height' => 147,	
																		 'size'	 => '214x109',		'name' => 'cover')));		// [Web]서브 코믹스 리스트
array_push($thumbnail['peanutoon_cm_cover'], array('tn551x282'			=> array('width' => 551,	'height' => 282,	
																		 'size'	 => '384x200',		'name' => 'cover')));		// [Web]서브 이벤트 리스트

array_push($thumbnail['peanutoon_cm_cover'], array('tn518x265'			=> array('width' => 518,	'height' => 265,	
																		 'size'	 => '384x200',		'name' => 'cover')));		// [mobile]BL 스크롤
array_push($thumbnail['peanutoon_cm_cover'], array('tn750x384'			=> array('width' => 750,	'height' => 384,	
																		 'size'	 => '718x368',		'name' => 'cover')));		// [mobile]코믹스 화 리스트

array_push($thumbnail['peanutoon_cm_cover'], array('tn655x335'			=> array('width' => 655,	'height' => 335,	
																		 'size'	 => '718x368',		'name' => 'cover')));		// [mobile]이벤트 화 리스트


// 코믹스 작은표지이미지 - comics
$thumbnail['peanutoon_cm_cover_sub'] = array(); 
array_push($thumbnail['peanutoon_cm_cover_sub'], array('tn'				=> array('width' => 356,	'height' => 356,	
																		 'size'	 => '500x500',		'name' => 'cover_sub')));		// [Web]
array_push($thumbnail['peanutoon_cm_cover_sub'], array('tn335x335'		=> array('width' => 335,	'height' => 335,	
																		 'size'	 => '250x250',		'name' => 'cover_sub')));		// [Web]급상승 big
array_push($thumbnail['peanutoon_cm_cover_sub'], array('tn133x133'		=> array('width' => 133,	'height' => 133,	
																		 'size'	 => '178x178',		'name' => 'cover_sub')));		// [Web]메인 인기종합순위
array_push($thumbnail['peanutoon_cm_cover_sub'], array('tn50x50'			=> array('width' => 50,		'height' => 50,	
																		 'size'	 => '178x178',		'name' => 'cover_sub')));		// [Web]메인 인기종합순위
array_push($thumbnail['peanutoon_cm_cover_sub'], array('tn136x136'		=> array('width' => 136,	'height' => 136,	
																		 'size'	 => '178x178',		'name' => 'cover_sub')));		// [Web]장르별 추천 small

array_push($thumbnail['peanutoon_cm_cover_sub'], array('tn102x102'		=> array('width' => 102,	'height' => 102,	
																		 'size'	 => '178x178',		'name' => 'cover_sub')));		// [Web]코믹스 화리스트 추천작품

array_push($thumbnail['peanutoon_cm_cover_sub'], array('tn222x222'		=> array('width' => 222,	'height' => 222,	
																		 'size'	 => '178x178',		'name' => 'cover_sub')));		// [mobile]업데이트 스크롤
array_push($thumbnail['peanutoon_cm_cover_sub'], array('tn75x75'			=> array('width' => 75,		'height' => 75,	
																		 'size'	 => '178x178',		'name' => 'cover_sub')));		// [mobile]인기작품 슬라이드리스트
array_push($thumbnail['peanutoon_cm_cover_sub'], array('tn249x249'		=> array('width' => 249,	'height' => 249,	
																		 'size'	 => '250x250',		'name' => 'cover_sub')));		// [mobile]서브 코믹스 리스트

// 피너툰 전용 끝



// 썸네일
function tn_thumbnail($get_path_file, $set_path, $set_file, $thumb_width=0, $thumb_height=0, $thumb_quality=100)
{
	global $thumbnail, $extension;

	$tn_thumbnail_bool = false;

	$tmp_file = pathinfo($get_path_file);
	$tmp_file_name  = strtolower($tmp_file['filename']);
	$tmp_dirname = strtolower($tmp_file['dirname']);
	$tmp_extension = strtolower($tmp_file['extension']);
	
	
	$set_file_info = pathinfo($set_file);
	$set_file_name = strtolower($set_file_info['filename']);
	$set_file_extension = strtolower($set_file_info['extension']);

	$size = @getimagesize($get_path_file);
	if($size[2] < 1 || $size[2] > 3){ /* gif, jpg, png 에 대해서만 적용 */ return false; }
	$get_path_file_extension = $extension[$size[2]];	
	$set_path_file = $set_path.$set_file_name.'.'.$get_path_file_extension; // 저장경로파일

	$get_width = $size[0];
	$get_height = $size[1];	

	$set_width = $thumb_width;
	$set_height = $thumb_height;

	if($thumb_width == 0){ $set_width = $get_width; }
	if($thumb_height == 0){ $set_height = $get_height; }
	
	$tmp_set_path = str_replace(NM_PATH, '', $set_path);
	mkdirAll($tmp_set_path);

    // 디렉토리가 존재하지 않거나 쓰기 권한이 없으면 썸네일 생성하지 않음
    if(!(is_dir($set_path) && is_writable($set_path))){ return false; }

	if (in_array(strtolower($get_path_file_extension), array("jpg","gif","bmp","png"))){

		// Animated GIF는 썸네일 생성하지 않음
		if($size[2] == 1) { if(tn_is_animated_gif($get_path_file)){ return false; } }

		$ext = array(1 => 'gif', 2 => 'jpg', 3 => 'png');
		
		if (file_exists($set_path_file)) { return true; } // 파일이 있다면....

		// 원본파일의 GD 이미지 생성
		$src = null;
		$degree = 0;

		if ($size[2] == 1) {
			$src = @imagecreatefromgif($get_path_file);
			$src_transparency = @imagecolortransparent($src);
		} else if ($size[2] == 2) {
			$src = @imagecreatefromjpeg($get_path_file);

			if(function_exists('exif_read_data')) {
				// exif 정보를 기준으로 회전각도 구함
				$exif = @exif_read_data($source_file);
				if(!empty($exif['Orientation'])) {
					switch($exif['Orientation']) {
						case 8:
							$degree = 90;
							break;
						case 3:
							$degree = 180;
							break;
						case 6:
							$degree = -90;
							break;
					}

					// 회전각도 있으면 이미지 회전
					if($degree) {
						$src = imagerotate($src, $degree, 0);

						// 세로사진의 경우 가로, 세로 값 바꿈
						if($degree == 90 || $degree == -90) {
							$tmp = $size;
							$size[0] = $tmp[1];
							$size[1] = $tmp[0];
						}
					}
				}
			}
		} else if ($size[2] == 3) {
			$src = @imagecreatefrompng($get_path_file);
			@imagealphablending($src, true);
		} else {
			return;
		}

		if(!$src){ return; }

		$is_large = true;
		// width, height 설정
		if($set_width) {
			if(!$set_height) {
				$set_height = round(($set_width * $size[1]) / $size[0]);
			} else {
				if($size[0] < $set_width || $size[1] < $set_height)
					$is_large = false;
			}
		} else {
			if($set_height) {
				$set_width = round(($set_height * $size[0]) / $size[1]);
			}
		}

		$dst_x = 0;
		$dst_y = 0;
		$src_x = 0;
		$src_y = 0;
		$dst_w = $set_width;
		$dst_h = $set_height;
		$src_w = $size[0];
		$src_h = $size[1];

		$ratio = $dst_h / $dst_w;

		if($is_large) {
			// 크롭처리
			if($is_crop) {
				switch($crop_mode)
				{
					case 'center':
						if($size[1] / $size[0] >= $ratio) {
							$src_h = round($src_w * $ratio);
							$src_y = round(($size[1] - $src_h) / 2);
						} else {
							$src_w = round($size[1] / $ratio);
							$src_x = round(($size[0] - $src_w) / 2);
						}
						break;
					default:
						if($size[1] / $size[0] >= $ratio) {
							$src_h = round($src_w * $ratio);
						} else {
							$src_w = round($size[1] / $ratio);
						}
						break;
				}
			}

			$dst = @imagecreatetruecolor($dst_w, $dst_h);

			if($size[2] == 3) {
				@imagealphablending($dst, false);
				@imagesavealpha($dst, true);
			} else if($size[2] == 1) {
				$palletsize = @imagecolorstotal($src);
				if($src_transparency >= 0 && $src_transparency < $palletsize) {
					$transparent_color   = @imagecolorsforindex($src, $src_transparency);
					$current_transparent = @imagecolorallocate($dst, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
					@imagefill($dst, 0, 0, $current_transparent);
					@imagecolortransparent($dst, $current_transparent);
				}
			}
		} else {
			$dst = @imagecreatetruecolor($dst_w, $dst_h);
			$bgcolor = @imagecolorallocate($dst, 255, 255, 255); // 배경색

			if($src_w < $dst_w) {
				if($src_h >= $dst_h) {
					$dst_x = round(($dst_w - $src_w) / 2);
					$src_h = $dst_h;
				} else {
					$dst_x = round(($dst_w - $src_w) / 2);
					$dst_y = round(($dst_h - $src_h) / 2);
					$dst_w = $src_w;
					$dst_h = $src_h;
				}
			} else {
				if($src_h < $dst_h) {
					$dst_y = round(($dst_h - $src_h) / 2);
					$dst_h = $src_h;
					$src_w = $dst_w;
				}
			}

			if($size[2] == 3) {
				$bgcolor = @imagecolorallocatealpha($dst, 0, 0, 0, 127);
				@imagefill($dst, 0, 0, $bgcolor);
				@imagealphablending($dst, false);
				@imagesavealpha($dst, true);
			} else if($size[2] == 1) {
				$palletsize = @imagecolorstotal($src);
				if($src_transparency >= 0 && $src_transparency < $palletsize) {
					$transparent_color   = @imagecolorsforindex($src, $src_transparency);
					$current_transparent = @imagecolorallocate($dst, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
					@imagefill($dst, 0, 0, $current_transparent);
					@imagecolortransparent($dst, $current_transparent);
				} else {
					@imagefill($dst, 0, 0, $bgcolor);
				}
			} else {
				@imagefill($dst, 0, 0, $bgcolor);
			}
		}

		@imagecopyresampled($dst, $src, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

		// sharpen 적용
		if($is_sharpen && $is_large) {
			$val = explode('/', $um_value);
			UnsharpMask($dst, $val[0], $val[1], $val[2]);
		}

		if($size[2] == 1) {
			if($thumb_quality < 85){ $thumb_quality = 85; } // 최소품질
			imagegif($dst, $set_path_file, $thumb_quality);
		} else if($size[2] == 3) {
			$png_compress = round(10 - ($thumb_quality / 10));
			if($png_compress < 3){ $png_compress = 2; } // 최소품질

			imagepng($dst, $set_path_file, $png_compress);
		} else { 
			if($thumb_quality < 85){ $thumb_quality = 85; } // 최소품질
			imagejpeg($dst, $set_path_file, $thumb_quality); 
		}

		chmod($set_path_file, 0777); // 추후 삭제를 위하여 파일모드 변경

		imagedestroy($src);
		imagedestroy($dst);

		$tn_thumbnail_bool = true;

		return $tn_thumbnail_bool;
	}
}


function tn_is_animated_gif($filename)
{
    if(!($fh = @fopen($filename, 'rb')))
        return false;
    $count = 0;
    // 출처 : http://www.php.net/manual/en/function.imagecreatefromgif.php#104473
    // an animated gif contains multiple "frames", with each frame having a
    // header made up of:
    // * a static 4-byte sequence (\x00\x21\xF9\x04)
    // * 4 variable bytes
    // * a static 2-byte sequence (\x00\x2C) (some variants may use \x00\x21 ?)

    // We read through the file til we reach the end of the file, or we've found
    // at least 2 frame headers
    while(!feof($fh) && $count < 2) {
        $chunk = fread($fh, 1024 * 100); //read 100kb at a time
        $count += preg_match_all('#\x00\x21\xF9\x04.{4}\x00(\x2C|\x21)#s', $chunk, $matches);
   }

    fclose($fh);
    return $count > 1;
}

function tn_set_thumbnail($get_path_file, $set_path, $set_file, $thumb_width=0, $thumb_height=0, $thumb_quality=100)
{
	global $thumbnail, $extension;

	$tn_set_thumbnail_bool = false;
	// 이미지 보다 큰 크기 사이즈면 이미지 사이즈로...
	$size = @getimagesize($get_path_file);
	if($size[2] < 1 || $size[2] > 3){ /* gif, jpg, png 에 대해서만 적용 */ return false; }
	$get_path_file_extension = $extension[$size[2]];

	$get_width = $size[0];
	$get_height = $size[1];	

	$set_width = $thumb_width;
	$set_height = $thumb_height;

	if($thumb_width == 0 || $get_width < $thumb_width){ $set_width = $get_width; }
	if($thumb_height == 0 || $get_height < $thumb_height){ $set_height = $get_height; }
	
	// echo $get_path_file."<br/>";
	// echo $set_path."<br/>";
	// echo $set_file."<br/>";
	// echo $set_width."<br/>";
	// echo $set_height."<br/>";
	// die;

	
	$tn_set_thumbnail_bool = tn_thumbnail($get_path_file, $set_path, $set_file, $set_width, $set_height, $thumb_quality);

	return $tn_set_thumbnail_bool;
}

function tn_set_key_thumbnail($get_path_file, $set_path, $key, $wxh='all', $set_nick='', $mod_file='')
{
	global $thumbnail, $extension;
	
	$tn_set_key_thumbnail_bool = false;
	
	$size = @getimagesize($get_path_file);
	if($size[2] < 1 || $size[2] > 3){ /* gif, jpg, png 에 대해서만 적용 */ return false; }
	$get_path_file_extension = $extension[$size[2]];

	$mod_file_info				= pathinfo($mod_file);
	$mod_file_name				= strtolower($mod_file_info['filename']);
	$mod_file_dir				= strtolower($mod_file_info['dirname']);
	$mod_file_extension			= strtolower($mod_file_info['extension']);

	$set_file = $mod_file_name.'.'.$mod_file_extension;

	$thumb_list = array();

	foreach($thumbnail[$key] as $thumbnail_key => $thumbnail_val){
		foreach($thumbnail_val as $thumb_key => $thumb_val){
			if($wxh == 'all'){
					array_push($thumb_list, array($thumb_key => array('width' => intval($thumb_val['width']),	
																	  'height' => intval($thumb_val['height']))));
			}else if($wxh == 'not'){
				if('tn' != $thumb_key){
					array_push($thumb_list, array($thumb_key => array('width' => intval($thumb_val['width']),	
																	  'height' => intval($thumb_val['height']))));
				}
			}else{
				if($wxh == $thumb_key){
					array_push($thumb_list, array($thumb_key => array('width' => intval($thumb_val['width']),	
																	  'height' => intval($thumb_val['height']))));
				}
			}
		}
	}

	foreach($thumb_list as $thumb_list_key => $thumb_list_val){
		foreach($thumb_list_val as $nail_key => $nail_val){
			$set_file_name = '';
			if($nail_key == 'tn'){
				$set_path_name = NM_PATH.'/'.$set_path;
				$set_file_name = $key.'.'.$get_path_file_extension;
				if($mod_file != ''){ $set_file_name = $mod_file_name.'.'.$mod_file_extension; }
			}else{
				$set_path_name = NM_THUMB_PATH.'/'.$set_path;
				$set_file_name = $key.'_'.$nail_key.'.'.$get_path_file_extension;
				if($mod_file != ''){ $set_file_name = $mod_file_name.'_'.$nail_key.'.'.$mod_file_extension; }
			}
			
			// 배너는 단일로 쓰임
			if($key == 'embc_cover' && $wxh != 'all'&& $wxh != ''){
				$set_path_name = NM_PATH.'/'.$set_path;
				$set_file_name = $key.'.'.$get_path_file_extension;
				if($mod_file != ''){ $set_file_name = $mod_file_name.'.'.$mod_file_extension; }
			}
			
			// echo $get_path_file."<br/>";
			// echo $set_path_name."<br/>";
			// echo $set_file_name."<br/>";
			// echo $nail_val['width']."<br/>";
			// echo $nail_val['height']."<br/>";
			// die;
			
			$tn_set_key_thumbnail_bool = tn_set_thumbnail($get_path_file, $set_path_name, $set_file_name, $nail_val['width'], $nail_val['height']);
		}
	}
	return $tn_set_key_thumbnail_bool;
}

function tn_upload_thumbnail($get_path_file, $set_path, $set_file, $key, $wxh='all', $set_nick='', $mod_file='')
{
	global $thumbnail, $extension;

	$tn_upload_thumbnail_bool = false;
	
	$size = @getimagesize($get_path_file);
	if($size[2] < 1 || $size[2] > 3){ /* gif, jpg, png 에 대해서만 적용 */ return false; }
	$get_path_file_extension = $extension[$size[2]];			

	$set_file_info				= pathinfo($set_file);
	$set_file_name				= strtolower($set_file_info['filename']);
	$set_file_dir				= strtolower($set_file_info['dirname']);
	$set_file_extension			= strtolower($set_file_info['extension']);

	$mod_file_info				= pathinfo($mod_file);
	$mod_file_name				= strtolower($mod_file_info['filename']);
	$mod_file_dir				= strtolower($mod_file_info['dirname']);
	$mod_file_extension			= strtolower($mod_file_info['extension']);

	$thumb_list = array();

	foreach($thumbnail[$key] as $thumbnail_key => $thumbnail_val){
		foreach($thumbnail_val as $thumb_key => $thumb_val){
			if($wxh == 'all'){
					array_push($thumb_list, array($thumb_key => array('width' => intval($thumb_val['width']),	
																	  'height' => intval($thumb_val['height']),	
																	  'name' => $thumb_val['name'])));
			}else if($wxh == 'not'){
				if('tn' != $thumb_key){
					array_push($thumb_list, array($thumb_key => array('width' => intval($thumb_val['width']),	
																	  'height' => intval($thumb_val['height']),	
																	  'name' => $thumb_val['name'])));
				}
			}else{
				if($wxh == $thumb_key){
					array_push($thumb_list, array($thumb_key => array('width' => intval($thumb_val['width']),	
																	  'height' => intval($thumb_val['height']),	
																	  'name' => $thumb_val['name'])));
				}
			}
		}
	}

	foreach($thumb_list as $thumb_list_key => $thumb_list_val){
		foreach($thumb_list_val as $nail_key => $nail_val){
			$get_file_name = '';
			if($nail_key == 'tn'){
				$get_path = NM_PATH.'/'.$set_path;
				$get_db_path = $set_path;
				$get_file_name = $nail_val['name'].'.'.$get_path_file_extension;
				$get_db_name = $nail_val['name'].$set_nick.'.'.$get_path_file_extension;
				if($mod_file != ''){ 
					$get_file_name = $mod_file_name.'.'.$get_path_file_extension; 
					$get_db_name = $mod_file_name.'.'.$get_path_file_extension; 
				}
			}else{
				$get_path = NM_THUMB_PATH.'/'.$set_path;
				$get_db_path = 'thumbnail/'.$set_path;
				$get_file_name = $nail_val['name'].'_'.$nail_key.'.'.$get_path_file_extension;
				$get_db_name = $nail_val['name'].$set_nick.'_'.$nail_key.'.'.$get_path_file_extension;
				if($mod_file != ''){ 
					$get_file_name = $mod_file_name.'_'.$nail_key.'.'.$get_path_file_extension; 
					$get_db_name = $mod_file_name.'_'.$nail_key.'.'.$get_path_file_extension; 
				}
			}
			
			// 배너는 단일로 쓰임
			if($key == 'embc_cover' && $wxh != 'all'&& $wxh != ''){
				$get_path = NM_PATH.'/'.$set_path;
				$get_db_path = $set_path;
				$get_file_name = $nail_val['name'].'.'.$get_path_file_extension;
				$get_db_name = $nail_val['name'].$set_nick.'.'.$get_path_file_extension;
				if($mod_file != ''){ 
					$get_file_name = $mod_file_name.'.'.$get_path_file_extension; 
					$get_db_name = $mod_file_name.'.'.$get_path_file_extension; 
				}
			}

			$tmp_upload = $get_path.$get_file_name;
			// echo $tmp_upload.'<br/>';
			// echo $get_db_path.'<br/>';
			// echo $get_file_name.'<br/>';
			// echo $get_db_name.'<br/>';
			// echo "<br/><br/><br/><br/>";
			$tn_upload_thumbnail_bool = kt_storage_upload($tmp_upload, $get_db_path, $get_db_name);
		}
	}
	return $tn_upload_thumbnail_bool;
}

// 썸네일 코믹스뷰어
function tn_set_viewer_thumbnail($get_path_file, $set_path, $set_nick='', $mod_file='')
{	
	global $thumbnail, $extension;
	
	// echo "<br/><br/><br/><br/>";
	// echo "/***** s *****/";
	// echo $get_path_file.'<br/>';
	// echo $set_path.'<br/>';
	// echo $set_nick.'<br/>';
	// echo $mod_file.'<br/>';
	// echo "/***** e *****/";
	// echo "<br/><br/><br/><br/>";



	$tn_set_viewer_thumbnail_bool = false;	

	$config_viewer_width = $thumbnail['ce_file'][0]['tn']['width'];
	
	$get_path_file_info			= pathinfo($get_path_file);
	$get_path_file_name			= strtolower($get_path_file_info['filename']);
	$get_path_file_dir			= strtolower($get_path_file_info['dirname']);
	$get_path_file_extension	= strtolower($get_path_file_info['extension']);

	$mod_file_info				= pathinfo($mod_file);
	$mod_file_name				= strtolower($mod_file_info['filename']);
	$mod_file_dir				= strtolower($mod_file_info['dirname']);
	$mod_file_extension			= strtolower($mod_file_info['extension']);
	
	if($mod_file == ''){
		$set_file = $get_path_file_name.'.'.$get_path_file_extension;
	}else{
		$set_file = $mod_file_name.'.'.$mod_file_extension;
	}

	$size = @getimagesize($get_path_file);

	if($size[2] < 1 || $size[2] > 3){ /* gif, jpg, png 에 대해서만 적용 */ return false; }

	$set_width	= $size[0];
	$set_height = $size[1];

	if($config_viewer_width < $set_width){
		$set_width	= $config_viewer_width;
		$set_height = floor($config_viewer_width * ($size[1] / $size[0]));
	}

	$tn_set_viewer_thumbnail_bool = tn_thumbnail($get_path_file, NM_THUMB_PATH.'/'.$set_path, $set_file, $set_width, $set_height);
	
	if($tn_set_viewer_thumbnail_bool == false){ $set_path_file = $get_path_file; } // false시 복사할 이미지 경로&파일명

	return $tn_set_viewer_thumbnail_bool;
}


// 썸네일 코믹스뷰어
function tn_upload_viewer_thumbnail($get_path_file, $set_path, $set_file, $set_nick='', $mod_file='')
{
	global $thumbnail;

	$tn_upload_viewer_thumbnail = false;

	$get_path_file			= strtolower($get_path_file);
	$set_path				= strtolower($set_path);
	$set_file				= strtolower($set_file);
	$set_nick				= strtolower($set_nick);
	$mod_file				= strtolower($mod_file);

	$tn_upload_viewer_thumbnail = kt_storage_upload($get_path_file, $set_path, $set_file);

	return $tn_upload_viewer_thumbnail;
}


function tn_thumbnail_storage_zip_upload($tmp_uploaded, $path, $filename)
{
// 삭제 먼저 가기
	kt_storage_zip_delete($path); // kt storage
	
    global $container, $thumbnail, $extension;
	$tn_thumbnail_storage_zip_upload = false;

	mkdirAll($path);

	// kt server
	$move_uploaded = NM_PATH.'/'.$path.'/'.$filename;
	if (move_uploaded_file($tmp_uploaded, $move_uploaded)) {
		$zipfile = new PclZip($move_uploaded);
		$extract = $zipfile -> extract(PCLZIP_OPT_PATH, NM_PATH.'/'.$path);
		@unlink($tmp_uploaded);
		@unlink($move_uploaded);
	}

	foreach($extract as $extract_key => $extract_val){
		if(!preg_match("/\.(gif|jpg|bmp|png)$/i", $extract_val['stored_filename'])){
			if (is_file($extract_val['filename'])) {
				@unlink($extract_val['filename']); // 해당 파일 삭제 
				unSET($extract[$extract_key]);  // 해당 배열 삭제 
			}
		}
	}
	// print_r($extract);
	$zipfile_count = count($extract); /* 파일 갯수 */

	// thumbnail생성
	foreach($extract as $extract_key => $extract_val){
		$tn_thumbnail_storage_zip_upload = tn_set_viewer_thumbnail($extract_val['filename'], $path.'/');
	}
	
	// kt storage
	$container->create_paths($path);
	foreach($extract as $extract_key => $extract_val){
		$upload_info = getimagesize($extract_val['filename']);
		$content_type = $upload_info['mime'];

		$container->create_paths($path);
		$object = $container->create_object($path.'/'.$extract_val['stored_filename']);
		$object->content_type = $content_type;
		// $object->load_from_filename($extract_val['filename']);
		$object->load_from_filename(NM_THUMB_PATH.'/'.$path.'/'.$extract_val['stored_filename']);
	}
	rmdirAll(NM_THUMB_PATH.'/'.$path,1);

	return $zipfile_count;
}




/* ///////////////////////// tn_auto auto ///////////////////////// */

// 썸네일
function tn_auto_thumbnail($get_path_file, $set_path, $set_file, $thumb_width=0, $thumb_height=0, $thumb_quality=100)
{
	$tmp_file = pathinfo($get_path_file);
	$tmp_file_name  = strtolower($tmp_file['filename']);
	$tmp_dirname = strtolower($tmp_file['dirname']);
	$tmp_extension = strtolower($tmp_file['extension']);
	
	$set_file_info = pathinfo($set_file);
	$set_file_name = strtolower($set_file_info['filename']);
	$set_file_extension = strtolower($set_file_info['extension']);

	$set_path_file = $set_path.'/'.$set_file_name.'.'.$tmp_extension; // 저장경로파일

	$size = @getimagesize($get_path_file);
	if($size[2] < 1 || $size[2] > 3){ /* gif, jpg, png 에 대해서만 적용 */ return false; }

	$get_width = $size[0];
	$get_height = $size[1];	

	$set_width = $thumb_width;
	$set_height = $thumb_height;

	if($thumb_width == 0){ $set_width = $get_width; }
	if($thumb_height == 0){ $set_height = $get_height; }
	
	$tmp_set_path = str_replace(NM_PATH, '', $set_path);
	mkdirAll($tmp_set_path);

    // 디렉토리가 존재하지 않거나 쓰기 권한이 없으면 썸네일 생성하지 않음
    if(!(is_dir($set_path) && is_writable($set_path))){ return false; }

	if (in_array(strtolower($tmp_file['extension']), array("jpg","gif","bmp","png"))){

		// Animated GIF는 썸네일 생성하지 않음
		if($size[2] == 1) {
			if(tn_auto_is_animated_gif($get_path_file))
				return basename($get_path_file);
		}

		$ext = array(1 => 'gif', 2 => 'jpg', 3 => 'png');
		
		if (file_exists($set_path_file)) { return false; } // 파일이 있다면....

		// 원본파일의 GD 이미지 생성
		$src = null;
		$degree = 0;

		if ($size[2] == 1) {
			$src = @imagecreatefromgif($get_path_file);
			$src_transparency = @imagecolortransparent($src);
		} else if ($size[2] == 2) {
			$src = @imagecreatefromjpeg($get_path_file);

			if(function_exists('exif_read_data')) {
				// exif 정보를 기준으로 회전각도 구함
				$exif = @exif_read_data($source_file);
				if(!empty($exif['Orientation'])) {
					switch($exif['Orientation']) {
						case 8:
							$degree = 90;
							break;
						case 3:
							$degree = 180;
							break;
						case 6:
							$degree = -90;
							break;
					}

					// 회전각도 있으면 이미지 회전
					if($degree) {
						$src = imagerotate($src, $degree, 0);

						// 세로사진의 경우 가로, 세로 값 바꿈
						if($degree == 90 || $degree == -90) {
							$tmp = $size;
							$size[0] = $tmp[1];
							$size[1] = $tmp[0];
						}
					}
				}
			}
		} else if ($size[2] == 3) {
			$src = @imagecreatefrompng($get_path_file);
			@imagealphablending($src, true);
		} else {
			return;
		}

		if(!$src){ return; }

		$is_large = true;
		// width, height 설정
		if($set_width) {
			if(!$set_height) {
				$set_height = round(($set_width * $size[1]) / $size[0]);
			} else {
				if($size[0] < $set_width || $size[1] < $set_height)
					$is_large = false;
			}
		} else {
			if($set_height) {
				$set_width = round(($set_height * $size[0]) / $size[1]);
			}
		}

		$dst_x = 0;
		$dst_y = 0;
		$src_x = 0;
		$src_y = 0;
		$dst_w = $set_width;
		$dst_h = $set_height;
		$src_w = $size[0];
		$src_h = $size[1];

		$ratio = $dst_h / $dst_w;

		if($is_large) {
			// 크롭처리
			if($is_crop) {
				switch($crop_mode)
				{
					case 'center':
						if($size[1] / $size[0] >= $ratio) {
							$src_h = round($src_w * $ratio);
							$src_y = round(($size[1] - $src_h) / 2);
						} else {
							$src_w = round($size[1] / $ratio);
							$src_x = round(($size[0] - $src_w) / 2);
						}
						break;
					default:
						if($size[1] / $size[0] >= $ratio) {
							$src_h = round($src_w * $ratio);
						} else {
							$src_w = round($size[1] / $ratio);
						}
						break;
				}
			}

			$dst = @imagecreatetruecolor($dst_w, $dst_h);

			if($size[2] == 3) {
				@imagealphablending($dst, false);
				@imagesavealpha($dst, true);
			} else if($size[2] == 1) {
				$palletsize = @imagecolorstotal($src);
				if($src_transparency >= 0 && $src_transparency < $palletsize) {
					$transparent_color   = @imagecolorsforindex($src, $src_transparency);
					$current_transparent = @imagecolorallocate($dst, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
					@imagefill($dst, 0, 0, $current_transparent);
					@imagecolortransparent($dst, $current_transparent);
				}
			}
		} else {
			$dst = @imagecreatetruecolor($dst_w, $dst_h);
			$bgcolor = @imagecolorallocate($dst, 255, 255, 255); // 배경색

			if($src_w < $dst_w) {
				if($src_h >= $dst_h) {
					$dst_x = round(($dst_w - $src_w) / 2);
					$src_h = $dst_h;
				} else {
					$dst_x = round(($dst_w - $src_w) / 2);
					$dst_y = round(($dst_h - $src_h) / 2);
					$dst_w = $src_w;
					$dst_h = $src_h;
				}
			} else {
				if($src_h < $dst_h) {
					$dst_y = round(($dst_h - $src_h) / 2);
					$dst_h = $src_h;
					$src_w = $dst_w;
				}
			}

			if($size[2] == 3) {
				$bgcolor = @imagecolorallocatealpha($dst, 0, 0, 0, 127);
				@imagefill($dst, 0, 0, $bgcolor);
				@imagealphablending($dst, false);
				@imagesavealpha($dst, true);
			} else if($size[2] == 1) {
				$palletsize = @imagecolorstotal($src);
				if($src_transparency >= 0 && $src_transparency < $palletsize) {
					$transparent_color   = @imagecolorsforindex($src, $src_transparency);
					$current_transparent = @imagecolorallocate($dst, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
					@imagefill($dst, 0, 0, $current_transparent);
					@imagecolortransparent($dst, $current_transparent);
				} else {
					@imagefill($dst, 0, 0, $bgcolor);
				}
			} else {
				@imagefill($dst, 0, 0, $bgcolor);
			}
		}

		@imagecopyresampled($dst, $src, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

		// sharpen 적용
		if($is_sharpen && $is_large) {
			$val = explode('/', $um_value);
			UnsharpMask($dst, $val[0], $val[1], $val[2]);
		}

		if($size[2] == 1) {
			if($thumb_quality < 85){ $thumb_quality = 85; } // 최소품질
			imagegif($dst, $set_path_file, $thumb_quality);
		} else if($size[2] == 3) {
			$png_compress = round(10 - ($thumb_quality / 10));
			if($png_compress < 3){ $png_compress = 2; } // 최소품질

			imagepng($dst, $set_path_file, $png_compress);
		} else { 
			if($thumb_quality < 85){ $thumb_quality = 85; } // 최소품질
			imagejpeg($dst, $set_path_file, $thumb_quality); 
		}

		chmod($set_path_file, 0777); // 추후 삭제를 위하여 파일모드 변경

		imagedestroy($src);
		imagedestroy($dst);

		return basename($set_path_file);
	}
}


function tn_auto_is_animated_gif($filename)
{
    if(!($fh = @fopen($filename, 'rb')))
        return false;
    $count = 0;
    // 출처 : http://www.php.net/manual/en/function.imagecreatefromgif.php#104473
    // an animated gif contains multiple "frames", with each frame having a
    // header made up of:
    // * a static 4-byte sequence (\x00\x21\xF9\x04)
    // * 4 variable bytes
    // * a static 2-byte sequence (\x00\x2C) (some variants may use \x00\x21 ?)

    // We read through the file til we reach the end of the file, or we've found
    // at least 2 frame headers
    while(!feof($fh) && $count < 2) {
        $chunk = fread($fh, 1024 * 100); //read 100kb at a time
        $count += preg_match_all('#\x00\x21\xF9\x04.{4}\x00(\x2C|\x21)#s', $chunk, $matches);
   }

    fclose($fh);
    return $count > 1;
}

function tn_auto_set_thumbnail($get_path_file, $set_path, $set_file, $thumb_width=0, $thumb_height=0, $thumb_quality=100)
{
	// 이미지 보다 큰 크기 사이즈면 이미지 사이즈로...
	$size = @getimagesize($get_path_file);
	if($size[2] < 1 || $size[2] > 3){ /* gif, jpg, png 에 대해서만 적용 */ return false; }

	$get_width = $size[0];
	$get_height = $size[1];	

	$set_width = $thumb_width;
	$set_height = $thumb_height;

	if($thumb_width == 0 || $get_width < $thumb_width){ $set_width = $get_width; }
	if($thumb_height == 0 || $get_height < $thumb_height){ $set_height = $get_height; }

	$set_path_file = tn_auto_thumbnail($get_path_file, $set_path, $set_file, $set_width, $set_height, $thumb_quality);
	
	if($set_path_file == false){ $set_path_file = $get_path_file; } // false시 복사할 이미지 경로&파일명

	return basename($set_path_file);
}

function tn_auto_set_key_thumbnail($get_path_file, $set_path, $key, $wxh='all', $set_nick='', $mod_file='')
{
	global $thumbnail;

	$mod_file_info				= pathinfo($mod_file);
	$mod_file_name				= strtolower($mod_file_info['filename']);
	$mod_file_dir				= strtolower($mod_file_info['dirname']);
	$mod_file_extension			= strtolower($mod_file_info['extension']);

	$thumb_list = array();
	
	foreach($thumbnail[$key] as $thumbnail_key => $thumbnail_val){
		foreach($thumbnail_val as $thumb_key => $thumb_val){
			if($wxh == 'all'){
					array_push($thumb_list, array($thumb_key => array('width' => intval($thumb_val['width']),	
																	  'height' => intval($thumb_val['height']))));
			}else{
				if($wxh == $thumb_key){
					array_push($thumb_list, array($thumb_key => array('width' => intval($thumb_val['width']),	
																	  'height' => intval($thumb_val['height']))));
				}
			}
		}
	}

	foreach($thumb_list as $thumb_list_key => $thumb_list_val){
		foreach($thumb_list_val as $nail_key => $nail_val){
			$set_file_name = '';
			if($nail_key == 'tn'){
				$set_file_name = $key;
				if($mod_file != ''){ $set_file_name = $mod_file_name; }
			}else{
				$set_file_name = $key.'_'.$nail_key;
				if($mod_file != ''){ $set_file_name = $mod_file_name.'_'.$nail_key; }
			}
			tn_auto_set_thumbnail($get_path_file, NM_THUMB_PATH.'/'.$set_path, $set_file_name, $nail_val['width'], $nail_val['height']);
		}
	}
}

function tn_auto_upload_thumbnail($get_path_file, $set_path, $set_file, $key, $wxh='all', $set_nick='', $mod_file='')
{
	global $thumbnail;
	
	$get_path_file_info			= pathinfo($get_path_file);
	$get_path_file_name			= strtolower($get_path_file_info['filename']);
	$get_path_file_dir			= strtolower($get_path_file_info['dirname']);
	$get_path_file_extension	= strtolower($get_path_file_info['extension']);

	$set_thumb_path				= 'thumbnail/'.$set_path;
	$set_file_info				= pathinfo($set_file);
	$set_file_name				= strtolower($set_file_info['filename']);
	$set_file_dir				= strtolower($set_file_info['dirname']);
	$set_file_extension			= strtolower($set_file_info['extension']);

	$mod_file_info				= pathinfo($mod_file);
	$mod_file_name				= strtolower($mod_file_info['filename']);
	$mod_file_dir				= strtolower($mod_file_info['dirname']);
	$mod_file_extension			= strtolower($mod_file_info['extension']);

	$thumb_list = array();
	
	foreach($thumbnail[$key] as $thumbnail_key => $thumbnail_val){
		foreach($thumbnail_val as $thumb_key => $thumb_val){
			if($wxh == 'all'){
					array_push($thumb_list, array($thumb_key => array('width' => intval($thumb_val['width']),	
																	  'height' => intval($thumb_val['height']),	
																	  'name' => $thumb_val['name'])));
			}else{
				if($wxh == $thumb_key){
					array_push($thumb_list, array($thumb_key => array('width' => intval($thumb_val['width']),	
																	  'height' => intval($thumb_val['height']),	
																	  'name' => $thumb_val['name'])));
				}
			}
		}
	}

	foreach($thumb_list as $thumb_list_key => $thumb_list_val){
		foreach($thumb_list_val as $nail_key => $nail_val){
			$get_file_name = '';
			if($nail_key == 'tn'){
				$get_path = $set_path;
				$get_file_name = $key.'.'.$set_file_extension;
				$get_db_name = $nail_val['name'].$set_nick.'.'.$set_file_extension;
				if($mod_file != ''){ 

					$get_file_name = $mod_file_name.'.'.$get_path_file_extension; 
					$get_db_name = $mod_file_name.'.'.$get_path_file_extension; 
				}
			}else{
				$get_path = $set_thumb_path;
				$get_file_name = $key.'_'.$nail_key.'.'.$set_file_extension;
				$get_db_name = $nail_val['name'].$set_nick.'_'.$nail_key.'.'.$set_file_extension;
				if($mod_file != ''){ 
					$get_file_name = $mod_file_name.'_'.$nail_key.'.'.$get_path_file_extension; 
					$get_db_name = $mod_file_name.'_'.$nail_key.'.'.$get_path_file_extension; 
				}
			}
			$tmp_upload = $get_path_file_dir.'/'.$get_file_name;
			// echo $tmp_upload.'<br/>';
			// echo $get_path.'<br/>';
			// echo $get_file_name.'<br/>';
			// echo $get_db_name.'<br/>';
			kt_storage_upload($tmp_upload, $get_path, $get_db_name);
		}
	}
}

// 썸네일 코믹스뷰어
function tn_auto_set_viewer_thumbnail($get_path_file, $set_path, $set_nick='', $mod_file='')
{	
	global $thumbnail;
	
	$config_viewer_width = $thumbnail['ce_file'][0]['tn']['width'];
	
	$get_path_file_info			= pathinfo($get_path_file);
	$get_path_file_name			= strtolower($get_path_file_info['filename']);
	$get_path_file_dir			= strtolower($get_path_file_info['dirname']);
	$get_path_file_extension	= strtolower($get_path_file_info['extension']);

	$mod_file_info				= pathinfo($mod_file);
	$mod_file_name				= strtolower($mod_file_info['filename']);
	$mod_file_dir				= strtolower($mod_file_info['dirname']);
	$mod_file_extension			= strtolower($mod_file_info['extension']);
	
	if($mod_file == ''){
		$set_file = $get_path_file_name.'.'.$get_path_file_extension;
	}else{
		$set_file = $mod_file_name.'.'.$mod_file_extension;
	}

	$size = @getimagesize($get_path_file);

	if($size[2] < 1 || $size[2] > 3){ /* gif, jpg, png 에 대해서만 적용 */ return false; }

	$set_width	= $size[0];
	$set_height = $size[1];

	if($config_viewer_width < $set_width){
		$set_width	= $config_viewer_width;
		$set_height = floor($config_viewer_width * ($size[1] / $size[0]));
	}

	$set_path_file = tn_auto_thumbnail($get_path_file, NM_THUMB_PATH.'/'.$set_path, $set_file, $set_width, $set_height);
	
	if($set_path_file == false){ $set_path_file = $get_path_file; } // false시 복사할 이미지 경로&파일명

	return basename($set_path_file);
}


// 썸네일 코믹스뷰어
function tn_auto_upload_viewer_thumbnail($get_path_file, $set_path, $set_file, $set_nick='', $mod_file='')
{

	global $thumbnail;

	$get_path_file			= strtolower($get_path_file);
	$set_path				= strtolower($set_path);
	$set_file				= strtolower($set_file);
	$set_nick				= strtolower($set_nick);
	$mod_file				= strtolower($mod_file);

	kt_storage_upload($get_path_file, $set_path, $set_file);
}

// 썸네일정보-181106
function tn_get_info($get_path_file, $key, $giho=false, $print=true)
{
	global $thumbnail, $extension;

	if($get_path_file == ''){ return ''; }
	
	$tn_get_img = array();
	$giho_text='';
	if($giho==true){ $giho_text='||'; }
	
	$get_path_file_info			= pathinfo($get_path_file);
	$get_path_file_name			= strtolower($get_path_file_info['filename']);
	$get_path_file_dir			= strtolower($get_path_file_info['dirname']);
	$get_path_file_extension	= strtolower($get_path_file_info['extension']);

	foreach($thumbnail[$key] as $thumbnail_key => $thumbnail_val){
		foreach($thumbnail_val as $thumb_key => $thumb_val){
			if($thumb_key == 'tn'){
				array_push($tn_get_img, '/'.$get_path_file.$giho_text); 
			}else{
				array_push($tn_get_img, '/thumbnail/'.$get_path_file_dir.'/'.$get_path_file_name.'_'.$thumb_key.'.'.$get_path_file_extension.$giho_text); 
			}
		}
	}

	if($print==true){
		foreach($tn_get_img as $img_key => $img_val){
			echo $img_val."\n";
		}		
	}else{
		return $tn_get_info;
	}
}



?>