<?
include_once '_common.php'; // 공통

error_page();

/* DB */
// PARAMETER

$search_txt = base_filter($_GET['search_txt']);
$sql_adult = "";

if (preg_match('/'.NM_APP_MARKET.'/i', HTTP_USER_AGENT)) {
	$sql_adult = sql_adult($mb_adult_permission, 'cm_adult');
} else {
	// 전체검색 가능하게 재수정170901	
	$sql_adult = sql_adult($nm_member['mb_adult'], 'cm_adult'); // 방송통신위원회시정사항: 비회원또는19세미만회원일 경우 성인작품제외-180416
	// 회원이 성인일 경우 전체검색기능
	if($nm_member['mb_adult'] == "y"){ $sql_adult = "";	}
} // end else

if($search_txt == "") {	
	$goto_url = cs_goto_referer();
	cs_alert("검색어를 입력하세요!", $goto_url);
	die;
} // end if

$search_arr = array();
$sql_search =	"SELECT c.*, c_prof.cp_name as prof_name  FROM comics c 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 AND ( cm_series like '%$search_txt%' OR cm_professional_info like '%$search_txt%' ) AND cm_service='y' $sql_adult  
				ORDER BY  `cm_episode_date` DESC";

$result_search= sql_query($sql_search);

while ($row_search = sql_fetch_array($result_search)) {
	if(substr($nm_config['nm_path'], -1) == 'c') { // PC 일때
		$row_search['cm_cover'] = img_url_para($row_search['cm_cover'], $row_search['cm_reg_date'], $row_search['cm_mod_date'], 'cm_cover', 'tn286x147');
		$row_search['cm_cover_sub'] = img_url_para($row_search['cm_cover_sub'], $row_search['cm_reg_date'], $row_search['cm_mod_date'], 'cm_cover_sub', 'tn136x136');
	} else { // MOBILE 일때
		$row_search['cm_cover_sub'] = img_url_para($row_search['cm_cover_sub'], $row_search['cm_reg_date'], $row_search['cm_mod_date'], 'cm_cover_sub', 'tn249x249');
	} // end else
	
	// 요일 텍스트
	$row_search['cm_week_txt'] = cs_get_comics_week($row_search);
	
	array_push($search_arr,  $row_search);
}

/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */

/* view */
include_once (NM_PATH.'/_head.php'); // 공통

mkt_rdt_acecounter_search($search_txt); // AceCounter 검색

include_once($nm_config['nm_path']."/search.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>