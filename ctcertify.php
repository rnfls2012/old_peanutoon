<? include_once '_common.php'; // 공통
define('_CERTIFY_', true); // 결제페이지 개별로 접근 금지하기 위한 상수

error_page();

cs_login_check(); // 로그인 체크

// 인증이 되어 있는 지 체크
if($nm_member['mb_ipin'] != ''){ cs_alert('인증이 되어 있습니다.', NM_URL); }

/* DB */
$sql = "select * from config_long_text";
$row = sql_fetch($sql);

// 주문번호생성
$order_no = get_uniqid();

/* view */
include_once (NM_PATH.'/_head.php'); // 공통
// 181001 아무타스 리타겟팅 태그
gtm_amutus($nm_member); 

include_once($nm_config['nm_path']."/ctcertify.php");

include_once (NM_PATH.'/_tail.php'); // 공통

?>