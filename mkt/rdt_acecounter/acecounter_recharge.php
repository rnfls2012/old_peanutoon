<?
if($nm_member["mb_no"] == "" || $nm_member["mb_no"] == null || $nm_member["mb_no"] == 0){ return false; }
if(intval($acecounter_order['state'] != 0)){ return false; }
if(intval($acecounter_order['count'] > 1)){ return false; }
if($acecounter_order['order'] == "" || $acecounter_order['amt'] == "" || $acecounter_order['cash_point'] == ""){ return false; }

$rdt_acecounter_bOrderNo		= $acecounter_order['order'];
$rdt_acecounter_bTotalPrice		= $acecounter_order['amt'];
$rdt_acecounter_bPay			= $acecounter_order['payway'];
$rdt_acecounter_bItem			= ''; // 주문리스트 -> 물어보기

$rdt_acecounter_pCode			= $acecounter_order['cash_point'].$nm_config['cf_cash_point_unit'];
$rdt_acecounter_pName			= $acecounter_order['cash_point'].$nm_config['cf_cash_point_unit_ko'];
$rdt_acecounter_pPrice			= $acecounter_order['amt'];
?>

<!--AceCounter-Plus eCommerce Buy Start -->
<script language='javascript'>
var _AceTM=(_AceTM||{});
_AceTM.Buy={
	bOrderNo:'<?=$rdt_acecounter_bOrderNo;?>',			//주문번호(필수)
	bTotalPrice:'<?=$rdt_acecounter_bTotalPrice;?>',	//주문 총가격(필수)
	bPay:'<?=$rdt_acecounter_bPay;?>',					//지불방법(ex : 무통장,신용카드 ,~~~)
	bItem:[],											// 주문리스트(필수)
	bDeliveryPrice:''									//배송비
};
</script>
<!--AceCounter-Plus eCommerce Buy End -->

<!--AceCounter-Plus eCommerce Buy item Start -->
<script language='javascript'>
_AceTM.Buy.bItem.push({
	pCode:'<?=$rdt_acecounter_pCode;?>',		//제품아이디
	pName:'<?=$rdt_acecounter_pName;?>',  		//제품이름
	pQuantity:1, 								//제품수량 or 옵션수량
	pPrice:'<?=$rdt_acecounter_pPrice;?>', 	 	//판매가 
	oCode:'',    								//옵션아이디
	oName:''   									//옵션이름
});
</script>
<!--AceCounter-Plus eCommerce Buy item End -->