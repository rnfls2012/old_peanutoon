<? include_once '_common.php'; // 공통 
	// error_page();

	$mkt_para = $_SERVER['PHP_SELF'];
	// echo $_SERVER['PHP_SELF'];

	$mkt_para_arr = explode("/", $mkt_para);

	$mkt_no = $mkt_para_arr[count($mkt_para_arr)-2];
	$mktl_short_link = $mkt_para_arr[count($mkt_para_arr)-1];

	$row_mkt = mkt_row($mkt_no);
	$row_mktl= mkt_short_row($mkt_no, $mktl_short_link);
	
	// 18-04-24 추가
	// $mktl_ga_utm = gtm_utm($row_mkt['mkt_title_en'], $row_mkt['mkt_medium'], $row_mktl['mktl_no']);
	/* 181001 */
	$mktl_ga_utm = gtm_utm_url($row_mktl);

	// 18-09-11 관리자도 되게끔 수정
	$mktl_link_ga_utm  = cs_para_attach($row_mktl['mktl_link'], $mktl_ga_utm);
	/*
	if($nm_member['mb_class'] != "a"){
		$mktl_link_ga_utm  = cs_para_attach($row_mktl['mktl_link'], $mktl_ga_utm);
	}else{
		$mktl_link_ga_utm  = $row_mktl['mktl_link'];		
	}
	*/

	if($row_mkt['mkt_state'] == "y"){
		if($row_mktl['mktl_link'] != ''){
			// 넘어가기전에 클릭수 업데이트
			if($row_mkt['mkt_para_join'] == ""){
				
			}else{
				if($_REQUEST[$row_mkt['mkt_para_join']] == "" && $nm_member['mb_class'] != "a"){				
					cs_alert('mkt필수코드:'.$row_mkt['mkt_para_join'].'_error 관리자에게 문의 바랍니다.', $row_mktl['mktl_link']);
					die;
				}
			}
			/* 181001 */
			/*
			$mkt_click_update = mkt_click_update($row_mktl);
			if($mkt_click_update == true){ $mkt_click_stats_update = mkt_click_stats_update($row_mktl); }
			else{
				cs_alert('에러코드:mkt_mktl_click_update_error 없음 관리자에게 문의 바랍니다.', $row_mktl['mktl_link']);
				die;
			}
			*/
			mkt_click_update($row_mktl);
			mkt_click_stats_update($row_mktl);

			/* 181001 */
			// if($mkt_click_stats_update == true){ 
				$mkt_para_join = base_filter($_REQUEST[$row_mkt['mkt_para_join']]);

				set_session('ss_'.$row_mkt['mkt_para_join'], $mkt_para_join); // 마케팅파라미터값 세션저장
				set_session('ss_mkt_no', $mkt_no); // 마케팅 업체번호 세션저장
				set_session('ss_mktl_no', $row_mktl['mktl_no']); // 마케팅 업체 링크번호 세션저장

				// 쿠키 72시간 유지
				set_cookie('ss_'.$row_mkt['mkt_para_join'], $mkt_para_join, 60*60*24*3); // 마케팅파라미터값 쿠키저장
				set_cookie('ss_mkt_no', $mkt_no, 60*60*24*3); // 마케팅 업체번호 쿠키저장
				set_cookie('ss_mktl_no', $row_mktl, 60*60*24*3); // 마케팅 업체 링크번호 쿠키저장

				// goto_url($row_mktl['mktl_link']); // 해당 링크 이동
				goto_url($mktl_link_ga_utm); // 해당 링크 이동 18-04-24 추가

			/* 181001 */
			/*
			}else{
				cs_alert('에러코드:mkt_mktl_click_stats_update_error 관리자에게 문의 바랍니다.', $row_mktl['mktl_link']);
				die;
			}
			*/
		}else{
			cs_alert('마켓팅 링크를 확인해주시기 바랍니다.',NM_URL);
			die;
		}
	}else{
		cs_alert('마켓팅 업체확인 및 업체상태 확인해주시기 바랍니다.',NM_URL);
		die;
	}
?>