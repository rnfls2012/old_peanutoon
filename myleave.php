<?
include_once '_common.php'; // 공통

error_page();

/* DB */
/* 회원정보 가져오기 */
if($nm_member['mb_no'] == ''){ cs_alert('로그인이 되어 있지 않습니다.',NM_URL."/ctlogin.php"); }

/* 탈퇴 문구 가져오기 */
$long_text = cs_get_long_text();

/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/myleave.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>