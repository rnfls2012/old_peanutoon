<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-07-11
 * Time: 오후 5:40
 */

include_once "./_common.php";

/**
 * block    : 총 페이지 수를 단락으로 구분짓는 변수
 * page     : 테이블에 있는 데이터 수를 묶음으로 보여주는 페이지 변수
 * list     : page 에 몇 개의 데이터를 보여주는 변수
 */

$record_per_page = 5; // 페이지 당 보여줄 데이터 수
$page_per_block = 5; // 페이지 당 보여줄 페이지단락 수
$current_page = '';
$output = '';

$rami_img_path = NM_IMG.$nm_config['nm_mode'].'/event/rami'; // 이미지 경로

if ( isset($_POST['page']) ) {
    $current_page = base_filter($_POST["page"]);
} else {
    $current_page = 1;
}

$page_query = "
  SELECT COUNT(*) AS total 
  FROM event_cheerup 
  WHERE blind_YN = 'n'
";

$page_result        = sql_count($page_query, 'total');            // 총 데이터 수

$total_pages        = ceil($page_result / $record_per_page);      // 총 페이지 수
$total_block        = ceil($total_pages / $page_per_block);       // 총 블럭 수
$current_block      = ceil($current_page / $page_per_block);      // 현재 블럭 수

$start_from         = ($current_page - 1) * $record_per_page;     // 시작 페이지

$start_page        = ($current_block - 1) * $page_per_block + 1;  // 시작 페이지
$end_page          = $start_page + $page_per_block - 1;           // 마지막 페이지

$next_block = $current_block + 1;
$prev_block = $current_block - 1;

$next_block_page = $next_block * $page_per_block - $page_per_block +1;
$prev_block_page = $prev_block * $page_per_block - $page_per_block +1;

if ($end_page > $total_pages)
    $end_page = $total_pages;

$query = "
    SELECT * 
    FROM event_cheerup AS ec 
    LEFT JOIN member ON ec.mb_no = member.mb_no 
    WHERE ec.blind_YN = 'n'
    ORDER BY ec.reg_date DESC
    LIMIT $start_from, $record_per_page;
";

$result = sql_query($query);

$output .= "
    <ol>
";

while ( $row = sql_fetch_array($result) ) {

    switch ( $row['mb_sns_type'] )  {
        case 'nid':
            // TODO : implement html code (naver)
            if ( strpos($row['mb_id'], '@') ) {
                $mb_id = strstr($row['mb_id'], '@', TRUE);
            } else {
                $mb_id = strstr($row['mb_id'], '_', TRUE);
            }
            $img_src = "<img src='$rami_img_path/sns_naver.png' alt='네이버'>";
            break;
        case 'kko':
            // TODO : implement html code (kakao)
            $mb_id = $row['mb_id'];
            $img_src = "<img src='$rami_img_path/sns_kakaotalk.png' alt='카카오'>";
            break;
        case 'fcb':
            // TODO : implement html code (facebook)
            if ( strpos($row['mb_id'], '@') ) {
                $mb_id = strstr($row['mb_id'], '@', TRUE);
            } else {
                $mb_id = strstr($row['mb_id'], '_', TRUE);
            }
            $img_src = "<img src='$rami_img_path/sns_facebook.png' alt='페북'>";
            break;
        case 'twt':
            // TODO : implement html code (tweet)
            $full_mb_id = strrchr($row['mb_id'], '/');
            $mb_id = substr($full_mb_id, 1, strlen($full_mb_id));
            $img_src = "<img src='$rami_img_path/sns_tweet.png' alt='트위터'>";
            break;
        default :
            $mb_id = strstr($row['mb_id'], '@', TRUE);
            $img_src = "<img src='$rami_img_path/peanut.png' alt='땅콩'>";
            break;
    }

    if ( strlen($mb_id) > 4 ) {
        $mb_id = substr_replace($mb_id, '****', -4);
    } else {
        $mb_id = substr_replace($mb_id, '***', -3);
    }


    if ( $row['blind_YN'] !== 'y' ) {
        $output .= "
        <li>
            <ul>
                <li>        
                    <div class='reply_name'>".$img_src."<span>&nbsp;".$mb_id."</span></div>
                    <div class='reply_date'>".substr($row['reg_date'],5, 11)."</div>                   
                </li>
                <li>
                    ".$row['comments']."
                </li>
            </ul>            
        </li>
    ";
    }
}

$output .= "
    </ol>
";

$output .= "
    <div class='reply_paging' id='pagination_data'>
        <ul>
";

if ($current_block > 1) {
    $output .= "
        <li class='prev_block' id='prev_block'><i class='fa fa-angle-left' aria-hidden='true'></i></li>
        ";
}

for ($i=$start_page; $i<=$end_page; $i++) {

    if ($current_page == $i) {
        $output .= "<li class='pagination_link' id='".$i."'><strong>".$i."</strong></li>";
    } else {
        $output .= "<li class='pagination_link' id='".$i."'>".$i."</span>";
    }
}

if ($current_block !== $total_block && $total_block > 1) {
    $output .= "<li class='next_block' id='next_block'><i class='fa fa-angle-right' aria-hidden='true'></i></li>";
}

$output .= "</ul>";

$output .="
    <input type='hidden' name='start_page' id='start_page' value='$prev_block_page'/>
    <input type='hidden' name='last_page' id='last_page' value='$next_block_page'/>
";

echo $output;