<?
include_once '_common.php'; // 공통

error_page();

/* DB */
/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */
$faq_list = array();

$cate_all_class = "";
$cate_mb_class = "";
$cate_pay_class = "";
$cate_used_class = "";

switch($_cate) {
	case 1 :
		$cate_mb_class = "active";
		break;

	case 2 : 
		$cate_pay_class = "active";
		break;

	case 3 : 
		$cate_used_class = "active";
		break;

	default :
		$cate_all_class = "active";
		break;
}

/* 카테고리 Tabbed 부분 보류
$tabbed_query = "select bh_category from board_help group by bh_category order by bh_category asc";
$tabbed_count = sql_num_rows(sql_query($tabbed_query))+1;
$tabbed_arr = array('0'=>'전체', '1'=>'회원 관련', '2'=>'결제 관련', 

if($tabbed_count > 0){
	$lnb_width = intval(100 / $tabbed_count);
	$lnb_width_first = $lnb_width + (100 - intval($tabbed_count)  * intval(100 / intval($tabbed_count)));
}
*/

/* 본 데이터 */
$faq_where = "";
if($_cate) { 
	$faq_where = " AND bh_category='".$_cate."'";
}

$faq_query = "select * from board_help where 1 ".$faq_where." order by bh_date desc";

$faq_result = sql_query($faq_query);
while($row = sql_fetch_array($faq_result)) {
	switch($row['bh_category']) {
		case 1 :
			$row['bh_category_text'] = "회원 관련";
			break;

		case 2 : 
			$row['bh_category_text'] = "결제 관련";
			break;

		case 3 : 
			$row['bh_category_text'] = "이용 관련";
			break;
	} // end switch

	array_push($faq_list, $row);
} // end while

/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/cshelp.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>