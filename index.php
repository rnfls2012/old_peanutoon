<?
include_once '_common.php'; // 공통

/* DB */
/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */

/********************
    앱 접속 확인
********************/
// globalhu 여기부터
/*
if(!$_SESSION['app']){
  $_SESSION['app']=($_POST['app'])?true:false;
  $_SESSION['device']=($_POST['device'])?true:false;
}
*/
/*
$app=preg_match('/(AppSetApk|AppMarket)/i',$_SERVER['HTTP_USER_AGENT'],$matches);

if(!$_SESSION['app']){
  $_SESSION['app'] = $app;
} // end if

if(!$_SESSION['appver']) {
	$_SESSION['appver'] = $_REQUEST['appver'];
} // end if

//위 버전에서 업데이트라면...
if($_REQUEST['appver'] !="" && floatval($_REQUEST['appver']) == floatval($nm_config['cf_appsetapk']) && floatval($_SESSION['appver']) < floatval($_REQUEST['appver'])) {
	$_SESSION['appver'] = $_REQUEST['appver'];
}

// NM_APP_IS_VER 
if(is_app(1) && is_mobile() && app_is_chk()) {
  $_SESSION['app'] = $app;
  $_SESSION['device'] = app_device();
  $_SESSION['appver'] = app_ver();
} // end if

// NM_APP_IS_VER -> 적용 오류 관련 수정
if($_SESSION['appver']=='5.0'){
	$_SESSION['appver'] = '1.5';
}
*/

// globalhu 여기까지

error_page();

// APP일때 업데이트
if(!$_SESSION['appupdate_later']){
	$_SESSION['appupdate_later'] = false;
	$_SESSION['appupdate_later_date'] = "";
}

// APK이고 모바일경우
if(is_app(1) && is_mobile()) {
	// APP일때 업데이트 "다음에 하기" 쿠키가 있다면 true;
	$appupdate_later_date = get_cookie('appupdate_later_date');
	if($appupdate_later_date == NM_TIME_YMD){
		$_SESSION['appupdate_later'] = true;
		$_SESSION['appupdate_later_date'] = NM_TIME_YMD;
		unset($appupdate_later_date);
	}

	// APP일때 업데이트 "다음에 하기" 날짜가 오늘이라면...
	if($_SESSION['appupdate_later_date']  == NM_TIME_YMD){
		$_SESSION['appupdate_later'] = true;
	}else{
		$_SESSION['appupdate_later'] = false;
	}
}

if($_SESSION['appupdate_later'] == false){
	if (is_app(1) && is_mobile()){
		if(floatval($_SESSION['appver']) < floatval($nm_config['cf_appsetapk'])) {
			$user_appver_txt = $user_appver = floatval($_SESSION['appver']);
			// APP sale 버전인 1.4보다 낮을 경우 0으로 처리
			if(floatval($_SESSION['appver']) < floatval(NM_APP_SALE_VER)){ 
				$user_appver = 0; 
				$user_appver_txt = "초기버전"; 
			}

			include_once (NM_PATH.'/appupdate.php'); // appupdate
			die;	
		} // end if
	} // end if
}

/* view */
include_once (NM_PATH.'/_head.php'); // 공통
// 181001 아무타스 리타겟팅 태그
gtm_amutus($nm_member); 

include_once($nm_config['nm_path']."/index.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>
