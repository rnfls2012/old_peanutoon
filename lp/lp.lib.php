<? if (!defined("_LANDINGPAGE_")) exit; // 개별 페이지 접근 불가

/* 설정 */
$lp_para = $_SERVER['PHP_SELF'];
$lp_para_arr = explode("/", $lp_para);

$lp_bool = false;
// 경로
$lp_root_path = "/var/www/html/peanutoon/lp";
$lp_page_type_file = "";
$lp_page_view_file = "";
$lp_page_type_path = "";
$lp_page_view_path = "";

$lp_page_type_arr = dir_list($lp_root_path,1);

$type_key = 0;
foreach($lp_page_type_arr as $key => $val){
	if(intval(array_search($val, $lp_para_arr)) > 0){
		$type_key = array_search($val, $lp_para_arr)+1;
	}
}

/* 경로 수 따라서 */
if(intval($type_key) > 0){ /* 경로가 3개 이상일때 */
	$lp_page_type_file = $lp_para_arr[$type_key-1];
	$lp_page_view_file = $lp_para_arr[$type_key+1];
	echo $lp_page_type_file."<br/>";
	echo $lp_page_view_file;
}

if(in_array($lp_page_type_file, $lp_page_type_arr)){
	$lp_page_view_arr = dir_list($lp_root_path."/".$lp_page_type_file);

	if(in_array($lp_page_view_file, $lp_page_view_arr)){

		$lp_page_view_path = $lp_root_path."/".$lp_page_type_file."/".$lp_page_view_file;

		if(file_exists($lp_page_view_path)){
			dir_goto($lp_page_view_path);
			$lp_bool = true;
		}
	}
}

/* landing page 파일 실패할 경우 */

if($lp_bool == false){
	if(in_array($lp_page_type_file, $lp_page_type_arr)){ /* 상위 경로가 맞을 경우 */
		$lp_page_type_path = $lp_root_path."/".$lp_page_type_file;
		$lp_page_type_dir_arr = dir_list($lp_page_type_path,1);
		
		dir_goto($lp_page_view_path= $lp_page_type_path."/".$lp_page_type_dir_arr[0]);
	}else{
		dir_not();
	}
}

// 아래 체크하려고 만듬
// dir_check();

/* //////////////////// function //////////////////// */

function dir_list($dir_path, $dir_sort=0){		
	$dir_arr = array();
	$dir_path_list = scandir($dir_path);
	
	foreach($dir_path_list as $key => $val){ // 파일 리스트 체크
		switch($val){
			case '.':
			case '..':
			case '.htaccess':
			case '_common.php':
			case 'index.php':
			case 'lp.lib.php':
			break;
			default:
				array_push($dir_arr, $val);
			break;
		}			
	}

	if($dir_sort!=0){ // 최신순으로 
		krsort($dir_arr); 
		$dir_temp_arr = array();
		foreach($dir_arr as $val){
			array_push($dir_temp_arr, $val);
		}
		$dir_arr = $dir_temp_arr;
	}

	return $dir_arr;
}

function dir_include($dir_path){
	if(file_exists($dir_path)){
		$dir_path_include = $dir_path.'/'.'index.html';
		if(is_file($dir_path_include)){
			include_once $dir_path_include;
		}else{
			echo "관리자에게 문의 하시기 바랍니다. error code : file null";
		}
	}else{
			echo "관리자에게 문의 하시기 바랍니다. error code : directory null";
	}
}

function dir_goto($dir_path){
	if(file_exists($dir_path)){
		$dir_path_include = $dir_path.'/'.'index.html';
		if(is_file($dir_path_include)){	
			$goto_url = NM_URL.'/'.str_replace("/var/www/html/peanutoon/", '', $dir_path_include);
			goto_url($goto_url);
		}else{
			echo "관리자에게 문의 하시기 바랍니다. error code : file null";
		}
	}else{
			echo "관리자에게 문의 하시기 바랍니다. error code : directory null";
	}
}

function dir_not(){
	global $lp_page_type_arr, $lp_root_path;

	$lp_page_type_file = $lp_page_type_arr[0];
	$lp_page_type_path = $lp_root_path."/".$lp_page_type_file;
	$lp_page_type_dir_arr = dir_list($lp_page_type_path,1);
		
	// dir_include($lp_page_view_path= $lp_page_type_path."/".$lp_page_type_dir_arr[0]);
	dir_goto($lp_page_view_path= $lp_page_type_path."/".$lp_page_type_dir_arr[0]);
}

function dir_check(){
	global $lp_page_type_arr, $lp_root_path;

	$url_arr = array();
	$lp_page_type_arr = dir_list($lp_root_path,1);
	foreach($lp_page_type_arr as $type_val){
		array_push($url_arr, NM_URL.'/lp/'.$type_val);
		$lp_page_view_arr = dir_list($lp_root_path.'/'.$type_val,1);
		foreach($lp_page_view_arr as $view_val){
			array_push($url_arr, NM_URL.'/lp/'.$type_val.'/'.$view_val);
		}
	}

	echo "<h2>Landing Page List"."</h2>";
	foreach($url_arr as $url_val){
		echo "<a href='";
		echo $url_val;
		echo "' target='_blank'>";
		echo $url_val;
		echo "</a>";
		echo "<br/>";
	}
}

?>