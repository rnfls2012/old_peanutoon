<?
include_once '_common.php'; // 공통

// 회원정보 확인
$sale_mb_adult = 'n';
if($is_member == true){
	if($nm_member['mb_adult'] == 'y'){
		$sale_mb_adult = 'y';
	}
}else{
	$sale_mb_adult = 'log';
}


// 1. 고객 캐쉬포인트+포인트 합친 sum_point포인트
$sale_mb_sum_point = intval($nm_member['mb_cash_point']) + intval( $nm_member['mb_point'] / NM_POINT );

// sale comics 데이터
$evt_thanksgiving_sale_comic = array();
// array_push(변수, array(코믹스번호, 성인yn, 에피소드1화, a_tag, 구매yn, 이미지번호, 땅콩소비));
array_push($evt_thanksgiving_sale_comic, array(2630, 'n', 29064, '', 'n', '01', 44)); // 소담빌라
array_push($evt_thanksgiving_sale_comic, array(2660, 'n', 29253, '', 'n', '02', 44)); // 플레이 하우스
array_push($evt_thanksgiving_sale_comic, array(2151, 'n', 25852, '', 'n', '03', 78)); // 난봉꾼과 왕자님
array_push($evt_thanksgiving_sale_comic, array(602 , 'n', 6199 , '', 'n', '04', 52)); // 불가항력 그대 -상수와 하공-
array_push($evt_thanksgiving_sale_comic, array(1365, 'n', 15207, '', 'n', '05', 79)); // 머리 괜찮냐?!
array_push($evt_thanksgiving_sale_comic, array(596 , 'n', 6117 , '', 'n', '06', 56)); // 여고생과 편의점
array_push($evt_thanksgiving_sale_comic, array(2832, 'n', 29887, '', 'n', '07',  6)); // 천사씨와 악마님 -외전-
array_push($evt_thanksgiving_sale_comic, array(1964, 'n', 22597, '', 'n', '08',  9)); // [웹툰판] 교사가 AV 감독과 사랑해도 괜찮을까
array_push($evt_thanksgiving_sale_comic, array(1572, 'n', 17421, '', 'n', '09', 11)); // [웹툰판] 순결 증정식 -날 안도록 해-
array_push($evt_thanksgiving_sale_comic, array(2097, 'n', 24672, '', 'n', '10', 13)); // [웹툰판] 오빠는 달콤한 내 몸에 중독되었어

foreach($evt_thanksgiving_sale_comic as &$et_sale_comic_val){
	// 데이터 가져오기
	$evt_thanksgiving_sale_get_comics = get_comics($et_sale_comic_val[0]);
	// 성인 처리
	$et_sale_comic_val[1] = $evt_thanksgiving_sale_get_comics['cm_adult'];

	// 통sale구매 확인
	$sql_all_sale_check = " select count(*) as all_sale_pay_ctn from event_thanksgiving_all_sale 
	                        where mpec_member=".$nm_member['mb_no']." 
							and mpec_comics=".$et_sale_comic_val[0].";";
	$row_all_sale_check = sql_fetch($sql_all_sale_check);
	if(intval($row_all_sale_check['all_sale_pay_ctn']) > 0){
		$et_sale_comic_val[4] = 'y';
	}
	
	// 클릭 처리
	$sale_mb_adult_n_msg = "해당 작품은 성인만 보실수 있습니다.";
	$sale_mb_adult_log_msg = "통 SALE 기획전은 로그인 후 이용 하실수 있습니다.<br/>로그인으로 이동하시겠습니까?";
	$sale_mb_adult_log_msg = this_apple_msg_tag($sale_mb_adult_log_msg);
	$sale_pay_url_msg = "&lt;".trim(text_change($evt_thanksgiving_sale_get_comics['cm_series'],"]"))."&gt; 전체구매시 <br/>기존 구매 내역과는 상관없이 <br/> ‘".$et_sale_comic_val[6]."땅콩‘이 일괄 소비 됩니다. <br/> 구매한 작품은 내 서재에 보관 됩니다. <br/> <br/> 전체구매를 진행 하시겠습니까?";

	$sale_view_url_msg = "통 SALE 기획전에서 구매하셨습니다.<br/>“".$evt_thanksgiving_sale_get_comics['cm_series']."“ 작품 1화로 이동하시겠습니까?";
	$sale_view_url_msg = this_apple_msg_tag($sale_view_url_msg);

	// 구매 링크
	$evt_thanksgiving_sale_pay_url = "{url:'".NM_PROC_URL."/eventful/event_thanksgiving_sale.php?comics=".$et_sale_comic_val[0]."&episode=".$et_sale_comic_val[2]."'}";	
	// 구매 클릭
	$sale_pay_box = ' onclick="confirmBox(\''.$sale_pay_url_msg.'\',goto_url,'.$evt_thanksgiving_sale_pay_url.');" ';
	if($et_sale_comic_val[6] > $sale_mb_sum_point){
		$sale_pay_box = ' onclick="alertBox(\'보유하신 땅콩/미니땅콩이 부족하여 땅콩 충전이 필요합니다.\');" ';
	}
	
	// view 링크
	$evt_thanksgiving_view_url = 
	"{url:'".get_episode_url($et_sale_comic_val[0], $et_sale_comic_val[2])."'}";
	// view 클릭
	$sale_view_box = ' onclick="confirmBox(\''.$sale_view_url_msg.'\',goto_url,'.$evt_thanksgiving_view_url.');" ';

	// login 링크
	$evt_thanksgiving_login_url = 
	"{url:'".NM_URL."/ctlogin.php'}";
	// login 클릭
	$sale_login_box = ' onclick="confirmBox(\''.$sale_mb_adult_log_msg.'\',goto_url,'.$evt_thanksgiving_login_url.');" ';
	
	// echo $sale_mb_adult."<br/>";
	if($is_member == true){
		if($et_sale_comic_val[4] == 'y'){
			$et_sale_comic_val[3] = $sale_view_box; // 구매된작품 => alert
		}else if($sale_mb_adult == 'y'){
			$et_sale_comic_val[3] = $sale_pay_box; // 구매링크
		}else if($evt_thanksgiving_sale_get_comics['cm_adult'] == 'n'){
			$et_sale_comic_val[3] = $sale_pay_box; // 구매링크
		}else{
			$et_sale_comic_val[3] = ' onclick="alertBox(\''.$sale_mb_adult_n_msg.'\');" '; // 성인작품&청소년 => alert
		}
	}else{
		$et_sale_comic_val[3] = $sale_login_box; // alert
	}
}

// print_r($evt_thanksgiving_sale_comic);

/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */
/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once(NM_EVENTFUL_PATH.'/'.$nm_config['nm_mode']."/event_thanksgiving_sale.php");

include_once (NM_PATH.'/_tail.php'); // 공통

// ------------------------------------------------------------------------------

function this_apple_msg_tag($msg){

	if(stristr(HTTP_USER_AGENT, "iPhone") || stristr(HTTP_USER_AGENT, "iPod")){
			$msg = strip_tags($msg);
	}
	return $msg;
}

?>