<?
include_once '_common.php'; // 공통

// 회원정보 확인
$time_mb_adult = 'n';
if($is_member == true){
	if($nm_member['mb_adult'] == 'y'){
		$time_mb_adult = 'y';
	}
}else{
	$time_mb_adult = 'log';
}

// setting
$config_time_name = "2018년 추석 타입세일 이벤트";

$event_pay = array();

// 시간 제어
$nm_time_h = intval(NM_TIME_H);
$sql_order_apm = "desc";
$nm_time_h_am_s = 3;  // 03:00 부터 -> 기획서 참조
$nm_time_h_am_e = 17; // 17:59 까지 -> 기획서 참조

if($nm_time_h >= $nm_time_h_am_s && $nm_time_h <=$nm_time_h_am_e){
	$sql_order_apm = "asc";
}

$nm_time_today = NM_TIME_YMD;
$nm_time_today_h_s = 0;  // 00:00 부터 -> 기획서 참조
$nm_time_today_h_e = 2; // 02:59 까지 -> 기획서 참조
if($nm_time_h >= $nm_time_today_h_s && $nm_time_h <=$nm_time_today_h_e){
	$nm_time_today = NM_TIME_M1;
}

$time_today_key_no = 0;

$ep_date_arr = array();
$time_data_arr = array();
// search 0
$sql = "select ep_date_start from event_pay where ep_text='".$config_time_name."' 
        group by ep_date_start order by ep_date_start asc;";
$result = sql_query($sql);
for($i=0; $row = sql_fetch_array($result); $i++){
	if($nm_time_today == $row['ep_date_start']){ $time_today_key_no = $i; } // 오늘 편성표 보러가기
	array_push($ep_date_arr, $row['ep_date_start']);

	$time_data_arr[$i] = array();
	// search 1
	$sql1 = "select * from event_pay where ep_date_start='".$row['ep_date_start']."' and ep_name like '피넛타입%' order by ep_no ".$sql_order_apm.";";
	$result1 = sql_query($sql1);
	// if(is_dev()){ echo $sql1."<br/>"; }
	while($row1=sql_fetch_array($result1)){
		// search 2
		$sql2 = "select * from event_pay_comics where epc_ep_no=".$row1['ep_no']." order by epc_order;";
		$result2 = sql_query($sql2);
		while($row2=sql_fetch_array($result2)){
			if($row1['ep_hour_start'] == '09'){ 
				$row2['class_day_img'] = "tab02_slide_day.png"; 
				$row2['class_time']    = "daytime";
			}else{
				$row2['class_day_img'] = "tab02_slide_night.png"; 
				$row2['class_time']    = "nighttime";
			}

			$time_get_comics = get_comics($row2['epc_comics']);

			$time_mb_adult_n_msg = "해당 작품은 성인만 보실수 있습니다.";
			$time_mb_adult_log_msg = "해당 작품은 로그인 후 성인만 보실수 있습니다.<br/>로그인으로 이동하시겠습니까?";			
			$time_mb_adult_log_msg = this_apple_msg_tag($time_mb_adult_log_msg);

			// login 링크
			$time_mb_adult_log_url = 
			"{url:'".NM_URL."/ctlogin.php'}";

			// login 클릭
			$time_mb_adult_log_box = ' onclick="confirmBox(\''.$time_mb_adult_log_msg.'\',goto_url,'.$time_mb_adult_log_url.');" ';
			
			// comic 링크
			$time_comics_url = get_comics_url($row2['epc_comics']).'&order=ASC&event_mode=thanksgivingtime';

			if($time_mb_adult == 'y'){
				$row2['get_comics_a_tag'] = ' href="'.$time_comics_url.'" '; // 링크
			}else if($time_get_comics['cm_adult'] == 'n'){
				$row2['get_comics_a_tag'] = ' href="'.$time_comics_url.'" '; // 링크
			}else if($time_get_comics['cm_adult'] == 'y' && $time_mb_adult == 'n'){
				$row2['get_comics_a_tag'] = ' onclick="alertBox(\''.$time_mb_adult_n_msg.'\');" '; // 성인작품&청소년 => alert
			}else{
				$row2['get_comics_a_tag'] = $time_mb_adult_log_box; // alert
			}

			array_push($time_data_arr[$i], $row2);
		}
	}
}

/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */
/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once(NM_EVENTFUL_PATH.'/'.$nm_config['nm_mode']."/event_thanksgiving_time.php");

include_once (NM_PATH.'/_tail.php'); // 공통

// ------------------------------------------------------------------------------

function this_apple_msg_tag($msg){

	if(stristr(HTTP_USER_AGENT, "iPhone") || stristr(HTTP_USER_AGENT, "iPod")){
			$msg = strip_tags($msg);
	}
	return $msg;
}

?>