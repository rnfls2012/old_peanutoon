<?php
include_once '_common.php'; // 공통

$eci_no = num_check(tag_get_filter($_REQUEST['eci_no'])); 

$christmas_img = NM_IMG.NM_PC.'/eventful/evt_christmas'; // 이미지 경로


$ecm_win = false;
$is_member_false = array('올바른 방법으로 이용하시기 바랍니다.', NM_URL.'/event_christmas.php');
$ecm_win_false = array('당첨자이시면 1:1문의하시기 바랍니다.[error code:efm_win-false]', NM_URL.'/event_christmas.php');
if($is_member == false){
	if($nm_config['nm_mode'] == NM_PC){ // 팝업창
		pop_close($is_member_false[0], $is_member_false[1]);
		die;
	}else{
		alert($is_member_false[0], $is_member_false[1]);
		die;
	}
}else{
	// 당첨자 정보 가져오기
	$ecm_row = array();
	$ecm_no = 0;
	$eci_no_data = event_christmas_gift_row_get_is_goods($nm_member);

	if(count($eci_no_data) > 0){
		$ecm_win = true;
		$ecm_no = intval($ecm_row['ecm_no']);
		$row_ecm_data = event_christmas_member_get($nm_member);
		if(intval($row_ecm_data['ecm_member']) < 1){
			event_christmas_member_set($nm_member);
			$row_ecm_data = event_christmas_member_get($nm_member);
		}
		
		// 당첨 이미지 가져오기
		$event_christmas_category = event_christmas_category();

		$gift_mode = "m";

		foreach($eci_no_data as &$eci_no_data_mod){
			$gift_data = $event_christmas_category[$eci_no_data_mod['ecc_select_no']];
			$eci_no_data_mod['ecg_src'] = $christmas_img.'/'.str_replace("pre_", "preicon_", $gift_data[3]);
			$eci_no_data_mod['ecg_alt']  = $gift_data[0];
			$eci_no_data_mod['ecg_txt']  = $gift_data[4];
			if($gift_data[1] == "y"){
				$gift_mode = $gift_data[1];
			}
		}
	}
}

// 당첨자 아니면...
if($ecm_win == false){
	if($nm_config['nm_mode'] == NM_PC){ // 팝업창
		pop_close($ecm_win_false[0], $ecm_win_false[1]);
		die;
	}else{
		alert($ecm_win_false[0], $ecm_win_false[1]);
		die;
	}
}

$nm_config['footer'] = false;

include_once($nm_config['nm_path']."/_head.sub.php");

// include_once(NM_EVENTFUL_PATH.'/'.$nm_config['nm_mode']."/event_christmas_delivery.php");
include_once(NM_EVENTFUL_PATH.'/'.NM_PC.'/event_christmas_member.php');


include_once($nm_config['nm_path']."/_tail.sub.php");