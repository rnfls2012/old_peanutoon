<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/event_christmas.css<?=vs_para();?>" />
		<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
		<style type="text/css">
			.evt-header {
				background: url(<?=$christmas_img?>/bg02.png<?=vs_para();?>), url(<?=$christmas_img?>/bg01.png<?=vs_para();?>);
				background-position: center 47px, left top;
				background-repeat: no-repeat, repeat-x;
			}
			.evt-contents {
				background: url(<?=$christmas_img?>/bg03.png<?=vs_para();?>), url(<?=$christmas_img?>/bg04.png<?=vs_para();?>);
				background-position: left top, right top;
				background-repeat: no-repeat;
			}
			.evt-tablist ul li:first-child {
				background: url(<?=$christmas_img?>/tab01.png<?=vs_para();?>);
				background-position: center bottom;
			}
			.evt-tablist ul li:nth-child(2) {
				background: url(<?=$christmas_img?>/tab02.png<?=vs_para();?>);
				background-position: center bottom;
			}
			.evt-tablist ul li:last-child {
				background: url(<?=$christmas_img?>/tab03.png<?=vs_para();?>);
				background-position: center bottom;
			}
			.evt-tablist ul li:hover, .evt-tablist ul li.active {
				background-position: center top;
			}

			.evt-tabcontent {
				background: url(<?=$christmas_img?>/bg05.png<?=vs_para();?>), url(<?=$christmas_img?>/bg06.png<?=vs_para();?>);
				background-position: left 20px top 20px, bottom 20px right 20px;
				background-repeat: no-repeat;
				background-color: #c93333;
			}
			.evt-gauge {
				background: url(<?=$christmas_img?>/tab01_bg.png<?=vs_para();?>);
				background-position: center;
				background-repeat: no-repeat;
			}
			.c100 >.gauge_login, .c100 >.gauge_open {
			  background: url(<?=$christmas_img?>/gauge02.png<?=vs_para();?>);
			}
			.evt-present:before {
				background: url(<?=$christmas_img?>/tab01_ribbon.png<?=vs_para();?>) no-repeat;
			}
			.ModalPopup {
				background: url(<?=$christmas_img?>/tab01_popup_bg.png<?=vs_para();?>) no-repeat;
				background-color: #fff;
				background-position: center 50px;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio'] { display: none; }
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket1 + label::before {
				background: url(<?=$christmas_img?>/tab01_popup_box01.png<?=vs_para();?>);
				background-position: left top;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket1:checked +label::before {
				background:url(<?=$christmas_img?>/tab01_popup_box01.png<?=vs_para();?>);
				background-position: right top;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket2 + label::before {
				background: url(<?=$christmas_img?>/tab01_popup_box02.png<?=vs_para();?>);
				background-position: left top;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket2:checked +label::before {
				background:url(<?=$christmas_img?>/tab01_popup_box02.png<?=vs_para();?>);
				background-position: right top;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket3 + label::before {
				background: url(<?=$christmas_img?>/tab01_popup_box03.png<?=vs_para();?>);
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket3:checked +label::before {
				background:url(<?=$christmas_img?>/tab01_popup_box03.png<?=vs_para();?>);
				background-position: right top;
			}
		</style>
		<script type="text/javascript">
		<!--
			<?=$scrolltop_js;?>
		//-->
		</script>

		<div class="evt-container">
			<div class="evt-header">
				<img src="<?=$christmas_img?>/title.png<?=vs_para();?>" alt="피너툰 Winter Party" />
			</div>
			<div class="evt-contents tab02">
				<div class="evt-tablist">
					<ul class="evt-frame">
						<li><a href="<?=NM_URL?>/event_christmas.php?type=gift">이벤트 1탄</a></li>
						<li class="active"><a href="<?=NM_URL?>/event_christmas.php?type=sale">이벤트 2탄</a></li>
						<li>이벤트 3탄</li>
					</ul>
				</div>
				<div class="evt-tabcontent">
					<img src="<?=$christmas_img?>/tab02_exp.png<?=vs_para();?>" alt="통세일 기획전" />
					<div class="evt-link">
						<? foreach($evt_christmas_sale_comic as $et_sale_comic_val2){ ?>
						<a <?=$et_sale_comic_val2[3]?>>
							<img src="<?=$christmas_img;?>/tab02_link<?=$et_sale_comic_val2[5]?>.png<?=vs_para()?>" alt="<?=$et_sale_comic_val2[0]?>" />
						</a>
						<? } ?>
					</div>
					<div class="evt-warn">
						<img src="<?=$christmas_img?>/tab02_warn.png<?=vs_para();?>" alt="이벤트 주의사항" />
					</div>
				</div>
			</div>
		</div>