<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<script type="text/javascript">
		<!--
			var slide_angle_left = '<div class="slide_angle left"><img src="<?=$thanksgiving_img;?>/reply/tab04_slide_angleleft.png<?=vs_para();?>" alt="왼쪽화살표" /></div>';
			var slide_angle_right = '<div class="slide_angle right"><img src="<?=$thanksgiving_img;?>/reply/tab04_slide_angleright.png<?=vs_para();?>" alt="오른쪽화살표" /></div>';
		//-->
		</script>

		<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/event_thanksgiving.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_EVENTFUL_PC_URL;?>/js/event_thanksgiving.js<?=vs_para();?>"></script>
		<script type="text/javascript">
		<!--
			<?=$scrolltop_js;?>
		//-->
		</script>
		<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/event_thanksgiving_reply.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_EVENTFUL_PC_URL;?>/js/event_thanksgiving_reply.js<?=vs_para();?>"></script>
		<script type="text/javascript" src="<?=NM_EVENTFUL_PC_URL;?>/js/event_thanksgiving_reply2.js<?=vs_para();?>"></script>

		
		<style type="text/css">
			.evt-content {
				background: url(<?=$thanksgiving_img;?>/bg01.png<?=vs_para()?>);
				background-size: auto;
				background-position: middle center;
			}		
			
			.evt-content .evt-frame ul.evt-tab li a.tab01 {
				background:url(<?=$thanksgiving_img;?>/tab01.png<?=vs_para()?>);
				background-position: bottom center;
			}
			.evt-content .evt-frame ul.evt-tab li a.tab02 {
				background:url(<?=$thanksgiving_img;?>/tab02.png<?=vs_para()?>);
				background-position: bottom center;
			}
			.evt-content .evt-frame ul.evt-tab li a.tab03 {
				background:url(<?=$thanksgiving_img;?>/tab03.png<?=vs_para()?>);
				background-position: bottom center;
			}
			.evt-content .evt-frame ul.evt-tab li a.tab04 {
				background:url(<?=$thanksgiving_img;?>/tab04.png<?=vs_para()?>);
				background-position: bottom center;
			}
			.evt-content .evt-frame .evt-tabcontents {			
				background: url(<?=$thanksgiving_img;?>/bg02.png<?=vs_para()?>), url(<?=$thanksgiving_img;?>/bg03.png<?=vs_para()?>), url(<?=$thanksgiving_img;?>/bg04.png<?=vs_para()?>), url(<?=$thanksgiving_img;?>/bg05.png<?=vs_para()?>);
				background-position: left top, right top, left bottom, right bottom;
				background-size: auto;
				background-repeat: no-repeat;
				background-color: #f2c45c;
			}
			.ModalPopup .p_content {
				background-image: url(<?=$thanksgiving_img;?>/bg06.png<?=vs_para()?>);
				background-repeat: no-repeat;
			}
			
			.evt-reply .reply_list ol li ul>li:nth-child(2):before {
				background-image: url(<?=$thanksgiving_img;?>/reply/tab04_reply_deco01.png<?=vs_para()?>);
			}

		</style>

		<div class="evt-container">
			<div class="evt-title">
				<div class="evt-maincopy">
					<img src="<?=$thanksgiving_img;?>/title.png<?=vs_para()?>" alt="피너툰 추석 대잔치" />
				</div>
			</div>
			<div class="evt-content">
				<div class="evt-frame">
					<ul class="evt-tab">
						<li>
							<a href="<?=NM_URL?>/event_thanksgiving.php?type=coupon" class="tab01">행운의 꿀송편</a>
						</li>
						<li>
							<a href="<?=NM_URL?>/event_thanksgiving.php?type=time" class="tab02">피넛타임 편성표</a>
						</li>
						<li>
							<a href="<?=NM_URL?>/event_thanksgiving.php?type=sale" class="tab03">통 SALE 기획전</a>
						</li>
						<li class="on last">
							<a href="<?=NM_URL?>/event_thanksgiving.php?type=reply" class="tab04">신작 기대평</a>
						</li>
					</ul>
					<?/* 여기서 부터 틀림-그외 다 동일함 */?>
					<div class="evt-tabcontents tabcontents04">
						<img src="<?=$thanksgiving_img;?>/reply/tab04_exp.png<?=vs_para()?>" alt="이벤트 설명" class="exp" />
						
						
						<script type="text/javascript">
						<!--
							$(document).on('ready', function() {
								$("#pc_slide_reply .regular").slick({
									dots: true,
									infinite: true,
									centerMode: true,
									centerPadding: '0px',
									speed: 500,
									slidesToShow: 1,
									slidesToScroll: 1,
									autoplay: true,
									autoplaySpeed: 5000,
									arrows: true
								});
							});
						//-->
						</script>

						<div class="tab04_slide">
							<div class="slide02-title">
								<img src="<?=$thanksgiving_img;?>/reply/tab04_slide_title.png<?=vs_para()?>" alt="신작" />
							</div>
							<div class="slide_wrap day_link_bg" id="pc_slide_reply">
								<div class="pc_slide_bg">
									<div class="slide_contents regular slider">
									<? foreach($evt_thanksgiving_sale_comic as $et_reply_comic_val2){ ?>
										<div class="slide_image">
											<a <?=$et_reply_comic_val2[3]?>>
												<img src="<?=$thanksgiving_img;?>/reply/tab04_slide_link<?=$et_reply_comic_val2[5]?>.png<?=vs_para()?>" alt="<?=$et_reply_comic_val2[0]?>" />
											</a>
										</div>
									<? } ?>
									</div>
								</div>
							</div>
						</div><!-- tab04_slide -->
						

						<div class="evt-reply">
						<style type="text/css">
							.reply_btn {
								background: url(<?=$thanksgiving_img;?>/reply/tab04_reply_submit.png<?=vs_para()?>);
								margin-left: 0px;
								width: 143px;
								height: 122px;
								vertical-align: top;
							}
						</style>
						
						<?php if ( !$did_comment ) { ?>
							<div class="reply_input">
								<form>
									<div class="reply_byte"><span class="count_box_character">0</span>/200</div>
									<!--
									<textarea placeholder="신작 기대평을 작성해 주세요! (200Byte 이내)" cols="1" onKeyPress="javascript: if (event.keyCode==13) return false;" onKeyUp="javascript: limitMemo(this, 50);"></textarea>
									<input type="image" src="<?=$thanksgiving_img;?>/reply/tab04_reply_submit.png<?=vs_para()?>" name="submit" value="submit">
									-->
									<input type="hidden" name="mb_no" id="mb_no" value="<?=$nm_member['mb_no']?>" />
									<textarea placeholder="신작 기대평을 작성해 주세요! (200자 이내)" id="comments" name="comments" cols="1" onKeyPress="if (event.keyCode===13) return false;" onKeyUp="countChar(this)"></textarea>
									<button type="button" onclick="eventrami_submit()" class="reply_btn">
								</form>
							</div>
							<!-- 댓글쓰고나서 이미지 출력 -->
							<div class="reply_img" style="display: none">
								<img src="<?=$thanksgiving_img?>/reply/reply_ok.png<?=vs_para()?>" alt="참여해주셔서 감사합니다!">
							</div>
						<?php } else { ?>
							<!-- 댓글쓰고나서 이미지 출력 -->
							<div class="reply_img">
								<img src="<?=$thanksgiving_img?>/reply/reply_ok.png<?=vs_para()?>" alt="참여해주셔서 감사합니다!">
							</div>
						<?php } ?>
						<div class="reply_list" id="reply_list" data-value="1">

							<!-- Paging -->
						<!--

							<div class="reply_list">
								<ol>
									<li>
										<ul>+
											<li><img src="<?=$thanksgiving_img;?>/reply/sns_tweet.png<?=vs_para()?>" alt="트위터">&nbsp;<span>normalboy_****</span></li>
											<li>댓글댓글댓글댓글댓글</li>
											<li>07/03 10:16</li>
										</ul>
									</li>
									<li>
										<ul>
											<li><img src="<?=$thanksgiving_img;?>/reply/sns_kakaotalk.png<?=vs_para()?>" alt="카카오">&nbsp;<span>69491****</span></li>
											<li>댓글댓글댓글댓글댓글</li>
											<li>07/03 10:16</li>
										</ul>
									</li>
									<li>
										<ul>
											<li><img src="<?=$thanksgiving_img;?>/reply/peanut.png<?=vs_para()?>" alt="땅콩">&nbsp;<span>일반가입</span></li>
											<li>댓글댓글댓글댓글댓글</li>
											<li>07/03 10:16</li>
										</ul>
									</li>
									<li>
										<ul>
											<li><img src="<?=$thanksgiving_img;?>/reply/sns_naver.png<?=vs_para()?>" alt="네이버">&nbsp;<span>네이버간편</span></li>
											<li>댓글댓글댓글댓글댓글</li>
											<li>07/03 10:16</li>
										</ul>
									</li>
									<li>
										<ul>
											<li><img src="<?=$thanksgiving_img;?>/reply/sns_facebook.png<?=vs_para()?>" alt="땅콩">&nbsp;<span>페이스북간편</span></li>
											<li>댓글댓글댓글댓글댓글</li>
											<li>07/03 10:16</li>
										</ul>
									</li>
								</ol>

								<div class="reply_paging">
									<ul>
										<li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
										<li><a href="#">1</a></li>
										<li><a href="#">2</a></li>
										<li><a href="#">3</a></li>
										<li><a href="#">4</a></li>
										<li><a href="#">5</a></li>
										<li class="end"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
									</ul>
								</div>
							</div>
						</div> <!-- evt-reply -->
					</div>
					<div class="evt-warn">
						<img src="<?=$thanksgiving_img;?>/reply/tab04_warn.png<?=vs_para()?>" alt="이벤트 유의사항">
					</div>
					<?/* 여기서 부터 틀림-그외 다 동일함 end */?>
				</div>
			</div>
		</div>