<?php

$evt_img = NM_PC_IMG.'eventful/evt_perpectbl';
?>

<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/evt_perpectbl.css<?=vs_para();?>">

<div class="evt-container">
	<div class="evt-title">
		<img src="<?=$evt_img?>/title.png<?=vs_para()?>" alt="고수위&소프트 BL 2땅콩 파티" />
	</div>
	<div class="evt-subtitle">
		<ul>
			<li><img src="<?=$evt_img?>/subtitle01.png<?=vs_para()?>" alt="고수위 BL 라인업" /></li>
			<li><img src="<?=$evt_img?>/subtitle02.png<?=vs_para()?>" alt="소프트 BL 라인업"></li>
		</ul>
	</div>
	<div class="evt-link">
		<div class="con-left">
			<a href="<?=get_comics_url(2850)?>">
				<img src="<?=$evt_img?>/left01.png<?=vs_para()?>" alt="무엇에 쓰는 물건인고? 2850" />
			</a>
			<a href="<?=get_comics_url(3255)?>">
				<img src="<?=$evt_img?>/left02.png<?=vs_para()?>" alt="지주 : 구슬을 가지다 3255" />
			</a>
			<a href="<?=get_comics_url(3033)?>">
				<img src="<?=$evt_img?>/left03.png<?=vs_para()?>" alt="구배씨의 인생역전 3033" />
			</a>
		</div>
		<div class="con-right">
			<a href="<?=get_comics_url(2933)?>">
				<img src="<?=$evt_img?>/right01.png<?=vs_para()?>" alt="너에게까지 99% 2933" />
			</a>
			<a href="<?=get_comics_url(3265)?>">
				<img src="<?=$evt_img?>/right02.png<?=vs_para()?>" alt="남고생과 동거중! 3265" />
			</a>
			<a href="<?=get_comics_url(1559)?>">
				<img src="<?=$evt_img?>/right03.png<?=vs_para()?>" alt="남자끼리 친구가 어딨어! 1559" />
			</a>
		</div>
	</div>
	<div class="evt-warn">
		<img src="<?=$evt_img?>/warn.png<?=vs_para()?>" alt="이벤트 유의사항" />
	</div>
</div>