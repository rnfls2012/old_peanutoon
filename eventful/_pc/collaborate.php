<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-07-16
 * Time: 오전 10:53
 */

$evt_img_path = NM_IMG.$nm_config['nm_mode'].'/eventful/4'; // 이미지 경로
?>
<style type="text/css">
    .evt-title {
        position: relative;
        width: 100%;
        background:url(<?=$evt_img_path?>/bg02.png<?=vs_para()?>), url(<?=$evt_img_path?>/bg01.png<?=vs_para()?>);
        background-size: auto;
        background-position: left top, center top;
        background-repeat: repeat-x, no-repeat;
    }
</style>
    <link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/collaborate.css<?=vs_para();?>">

    <div class="evt-container">
        <div class="evt-title">
            <div class="evt-maincopy">
                <img src="<?=$evt_img_path?>/title.png<?=vs_para()?>" alt="몸에도 좋고 맛도 좋은 국산 해물 삼계탕 EVENT" />
            </div>
            <div class="evt-insp">
                <img src="<?=$evt_img_path?>/insp.png<?=vs_para()?>" alt="이벤트 내용">
            </div>
        </div>
        <div class="evt-content">
            <div class="evt-link">
                <a href="<?=get_comics_url(2678)?>"><img src="<?=$evt_img_path?>/link01.png<?=vs_para()?>" alt="3억의 가장 작은 존재" /></a>
                <a href="<?=get_comics_url(1067)?>"><img src="<?=$evt_img_path?>/link02.png<?=vs_para()?>" alt="청년의 음란함은 주방에서" /></a>
                <a href="<?=get_comics_url(1746)?>"><img src="<?=$evt_img_path?>/link03.png<?=vs_para()?>" alt="Kill your HONEY♥(킬유어허니)" /></a>
                <a href="<?=get_comics_url(596)?>"><img src="<?=$evt_img_path?>/link04.png<?=vs_para()?>" alt="여고생과 편의점" /></a>
                <a href="<?=get_comics_url(1235)?>"><img src="<?=$evt_img_path?>/link05.png<?=vs_para()?>" alt="신의 신부" /></a>
            </div>
        </div>

        <div class="evt-warn">
                <img src="<?=$evt_img_path?>/warn.png<?=vs_para()?>" alt="이벤트 유의사항" />
            </div>

    </div>