<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/event_thanksgiving.css<?=vs_para();?>" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<script type="text/javascript" src="<?=NM_EVENTFUL_PC_URL;?>/js/event_thanksgiving.js<?=vs_para();?>"></script>
		<script type="text/javascript">
		<!--
			<?=$scrolltop_js;?>
		//-->
		</script>

		
		<style type="text/css">
			.evt-content {
				background: url(<?=$thanksgiving_img;?>/bg01.png<?=vs_para()?>);
				background-size: auto;
				background-position: middle center;
			}		
			
			.evt-content .evt-frame ul.evt-tab li a.tab01 {
				background:url(<?=$thanksgiving_img;?>/tab01.png<?=vs_para()?>);
				background-position: bottom center;
			}
			.evt-content .evt-frame ul.evt-tab li a.tab02 {
				background:url(<?=$thanksgiving_img;?>/tab02.png<?=vs_para()?>);
				background-position: bottom center;
			}
			.evt-content .evt-frame ul.evt-tab li a.tab03 {
				background:url(<?=$thanksgiving_img;?>/tab03.png<?=vs_para()?>);
				background-position: bottom center;
			}
			.evt-content .evt-frame ul.evt-tab li a.tab04 {
				background:url(<?=$thanksgiving_img;?>/tab04.png<?=vs_para()?>);
				background-position: bottom center;
			}
			.evt-content .evt-frame .evt-tabcontents {			
				background: url(<?=$thanksgiving_img;?>/bg02.png<?=vs_para()?>), url(<?=$thanksgiving_img;?>/bg03.png<?=vs_para()?>), url(<?=$thanksgiving_img;?>/bg04.png<?=vs_para()?>), url(<?=$thanksgiving_img;?>/bg05.png<?=vs_para()?>);
				background-position: left top, right top, left bottom, right bottom;
				background-size: auto;
				background-repeat: no-repeat;
				background-color: #f2c45c;
			}
			.ModalPopup .p_content {
				background-image: url(<?=$thanksgiving_img;?>/bg06.png<?=vs_para()?>);
				background-repeat: no-repeat;
			}			
			.evt-reply .reply_list ol li ul>li:nth-child(2):before {
				background-image: url(<?=$thanksgiving_img;?>/reply/tab04_reply_deco01.png<?=vs_para()?>);
			}

		</style>

		<input type="hidden" id="member_no" class="member_no" value="<?=$nm_member['mb_no'];?>" />
		<input type="hidden" id="total_coupon" class="total_coupon" value="<?=$total_coupon;?>" />

		<!-- Modal Popup -->
		<div class="Modalalert">
			<div class="ModalBackground"></div>
			<div class="ModalPopup">
				<div class="ModalClose"><i class="material-icons">clear</i></div>
				<div class="p_content">
					<div class="p_coupon">
						<img src="<?=$thanksgiving_img;?>/p_title.png<?=vs_para()?>" alt="20% 결제 할인 쿠폰">
					</div>
					<div class="p_get">
						<img src="<?=$thanksgiving_img;?>/p_warn.png<?=vs_para()?>" alt="쿠폰 사용 유의사항">
					</div>
					<ul>
						<li><button id="recharge_link" class="p_buy" onclick="location.href='<?=NM_URL?>/recharge.php'">결제하기</button></li>
						<li><button class="p_couponpage" onclick="location.href='<?=NM_URL?>/coupon.php'">쿠폰 보기</button></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- /Modal Popup -->

		<div class="evt-container">
			<div class="evt-title">
				<div class="evt-maincopy">
					<img src="<?=$thanksgiving_img;?>/title.png<?=vs_para()?>" alt="피너툰 추석 대잔치" />
				</div>
			</div>
			<div class="evt-content">
				<div class="evt-frame">
					<ul class="evt-tab">
						<li class="on">
							<a href="<?=NM_URL?>/event_thanksgiving.php?type=coupon" class="tab01">행운의 꿀송편</a>
						</li>
						<li>
							<a href="<?=NM_URL?>/event_thanksgiving.php?type=time" class="tab02">피넛타임 편성표</a>
						</li>
						<li>
							<a href="<?=NM_URL?>/event_thanksgiving.php?type=sale" class="tab03">통 SALE 기획전</a>
						</li>
						<li class="last">
							<a href="<?=NM_URL?>/event_thanksgiving.php?type=reply" class="tab04">신작 기대평</a>
						</li>
					</ul>
					<?/* 여기서 부터 틀림-그외 다 동일함 */?>
					<div class="evt-tabcontents tabcontents01">
						<img src="<?=$thanksgiving_img;?>/tab01_exp.png<?=vs_para()?>" alt="이벤트 설명" />
						<img src="<?=$thanksgiving_img;?>/tab01_dduk_<?=$tab01_dduk_img_name;?>.png<?=vs_para()?>" class="tab01_dduk dduk <?=$nochk;?>" id="open_btn" alt="송편" />
						<img src="<?=$thanksgiving_img;?>/tab01_warn.png<?=vs_para()?>" class="tab01_dduk" alt="이벤트 주의사항" />
					</div>
					<?/* 여기서 부터 틀림-그외 다 동일함 end */?>
				</div>
			</div>
		</div>