<?php
/**
 * Created by PhpStorm.
 * User: Jiwan Ye
 * Date: 2018-10-24
 * Time: 오전 9:33
 */

$evt_img = NM_PC_IMG.'eventful/evt_noi';
?>

<style type="text/css">
    .evt-title {
        position: relative;
        background-image:url(<?=$evt_img?>/bg01.png<?=vs_para()?>),url(<?=$evt_img?>/bg02.png<?=vs_para()?>);
        background-size: auto;
        background-repeat: no-repeat;
        background-position: left top, right top;
        width: 100%;
        padding-top: 100px;
    }
</style>

<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/evt_noi.css<?=vs_para();?>">

<div class="evt-container">
    <div class="evt-title">
        <div class="evt-maincopy">
            <img src="<?=$evt_img?>/title01.png" alt="노이 완결 기념 이벤트" />
        </div>
    </div>
    <div class="evt-content">
        <div class="evt-titlelink">
            <a href="<?=get_comics_url(1979)?>">
                <img src="<?=$evt_img?>/title02.png" alt="노이" />
            </a>
        </div>
        <div class="evt-link">
            <a href="<?=get_comics_url(2344)?>"><img src="<?=$evt_img?>/link01.png" alt="[웹툰판] 사람 거로는 만족할 수 없어♡" /></a>
            <a href="<?=get_comics_url(1039)?>"><img src="<?=$evt_img?>/link02.png" alt="애완용 로봇 릴리" class="link02" /></a>
            <a href="<?=get_comics_url(1234)?>"><img src="<?=$evt_img?>/link03.png" alt="에로봇 –뒤를 털려버렸어-" class="link03" /></a>
            <a href="<?=get_comics_url(507)?>"><img src="<?=$evt_img?>/link04.png" alt="쾌감인형의 사랑" class="link03" /></a>
        </div>
        <div class="evt-warn">
            <img src="<?=$evt_img?>/warn.png" alt="이벤트 유의사항" />
        </div>

    </div>
</div>
