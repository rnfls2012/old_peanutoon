<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" href="<?=NM_EVENTFUL_PC_URL;?>/css/circle.css<?=vs_para();?>">
		<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/event_christmas.css<?=vs_para();?>">
		<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
		<style type="text/css">
			.evt-header {
				background: url(<?=$christmas_img?>/bg02.png<?=vs_para();?>), url(<?=$christmas_img?>/bg01.png<?=vs_para();?>);
				background-position: center 47px, left top;
				background-repeat: no-repeat, repeat-x;
			}
			.evt-contents {
				background: url(<?=$christmas_img?>/bg03.png<?=vs_para();?>), url(<?=$christmas_img?>/bg04.png<?=vs_para();?>);
				background-position: left top, right top;
				background-repeat: no-repeat;
			}
			.evt-tablist ul li:first-child {
				background: url(<?=$christmas_img?>/tab01.png<?=vs_para();?>);
				background-position: center bottom;
			}
			.evt-tablist ul li:nth-child(2) {
				background: url(<?=$christmas_img?>/tab02.png<?=vs_para();?>);
				background-position: center bottom;
			}
			.evt-tablist ul li:last-child {
				background: url(<?=$christmas_img?>/tab03.png<?=vs_para();?>);
				background-position: center bottom;
			}
			.evt-tablist ul li:hover, .evt-tablist ul li.active {
				background-position: center top;
			}

			.evt-tabcontent {
				background: url(<?=$christmas_img?>/bg05.png<?=vs_para();?>), url(<?=$christmas_img?>/bg06.png<?=vs_para();?>);
				background-position: left 20px top 20px, bottom 20px right 20px;
				background-repeat: no-repeat;
				background-color: #c93333;
			}
			.evt-gauge {
				background: url(<?=$christmas_img?>/tab01_bg.png<?=vs_para();?>);
				background-position: center;
				background-repeat: no-repeat;
			}
			.c100 >.gauge_login, .c100 >.gauge_open {
			  background: url(<?=$christmas_img?>/gauge02.png<?=vs_para();?>);
			}
			.evt-present:before {
				background: url(<?=$christmas_img?>/tab01_ribbon.png<?=vs_para();?>) no-repeat;
			}
			.ModalPopup {
				background: url(<?=$christmas_img?>/tab01_popup_bg.png<?=vs_para();?>) no-repeat;
				background-color: #fff;
				background-position: center 50px;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio'] { display: none; }
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket1 + label::before {
				background: url(<?=$christmas_img?>/tab01_popup_box01.png<?=vs_para();?>);
				background-position: left top;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket1:checked +label::before {
				background:url(<?=$christmas_img?>/tab01_popup_box01.png<?=vs_para();?>);
				background-position: right top;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket2 + label::before {
				background: url(<?=$christmas_img?>/tab01_popup_box02.png<?=vs_para();?>);
				background-position: left top;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket2:checked +label::before {
				background:url(<?=$christmas_img?>/tab01_popup_box02.png<?=vs_para();?>);
				background-position: right top;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket3 + label::before {
				background: url(<?=$christmas_img?>/tab01_popup_box03.png<?=vs_para();?>);
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket3:checked +label::before {
				background:url(<?=$christmas_img?>/tab01_popup_box03.png<?=vs_para();?>);
				background-position: right top;
			}
		</style>
		<script type="text/javascript" src="<?=NM_EVENTFUL_PC_URL;?>/js/event_christmas.js<?=vs_para();?>"></script>		
		<script type="text/javascript">
		<!--
			<?=$scrolltop_js;?>
		//-->
		</script>
		
		<input type="hidden" id="action" value="<?=$christmas_action;?>" />
		<input type="hidden" id="fill" value="<?=$christmas_fill;?>" />

		<!-- Modal Popup -->
		<div class="Modalalert">
			<div class="ModalBackground"></div>
			<div class="ModalPopup">
				<div class="ModalClose"><i class="material-icons">clear</i></div>
				<div class="p_content01">
					<div class="p_title">
						<img src="<?=$christmas_img?>/tab01_popup_exp.png<?=vs_para();?>" alt="행운의 선물상자" />
					</div>
					<ul class="p_choice">
						<li>
							<input type="radio" name="pocket" value="pocket1" id="pocket1">
							<label for="pocket1">선물상자1</label>
						</li>
						<li>
							<input type="radio" name="pocket" value="pocket2" id="pocket2">
							<label for="pocket2">선물상자2</label>
						</li>
						<li>
							<input type="radio" name="pocket" value="pocket3" id="pocket3">
							<label for="pocket3">선물상자3</label>
						</li>
					</ul>
					<button id="open_btn">바로 열기</button>
				</div>
					
				<!-- 당첨 부분 -->
				<div class="p_content02" style="display: none;">
					<img src="<?=$christmas_img?>/pre_mpeanut.png<?=vs_para();?>" alt="미니땅콩 당첨!">
					<p><span id="result_text">미니땅콩 00개</span>를 받았어요!</p>
					<button>확인</button>
				</div>
			</div>
		</div>
		<!-- /Modal Popup -->
		<div class="evt-container">
			<div class="evt-header">
				<img src="<?=$christmas_img?>/title.png<?=vs_para();?>" alt="피너툰 Winter Party" />
			</div>
			<div class="evt-contents">
				<div class="evt-tablist">
					<ul class="evt-frame">
						<li class="active"><a href="<?=NM_URL?>/event_christmas.php?type=gift">이벤트 1탄</a></li>
						<li><a href="<?=NM_URL?>/event_christmas.php?type=sale">이벤트 2탄</a></li>
						<li>이벤트 3탄</li>
					</ul>
				</div>
				<div class="evt-tabcontent">
					<img src="<?=$christmas_img?>/tab01_exp.png<?=vs_para();?>" alt="이벤트 설명" />
					<div class="evt-gauge">
						<div class="c100 p<?=$christmas_percent;?> christmas" id="christmas_percent">
							<div class="gauge_open">
								<a id="open_button">
									<p><span id="christmas_text1"><?=$christmas_text1;?></span></p>
									<p class="peanut" id="christmas_text2"><?=$christmas_text2;?></p>
								</a>
							</div>
							<div class="slice">
								<div class="bar"></div>
								<div class="fill"></div>
							</div>
						</div>
					</div>
					<div class="evt-present">
						<img src="<?=$christmas_img?>/tab01_present.png<?=vs_para();?>" alt="이벤트 경품" />
					</div>
					<? if($eci_is_goods_bool == true){ ?>
					<div class="evt-delivery">
						<a><img src="<?=$christmas_img?>/delivery_btn.png<?=vs_para();?>" alt="배송지 확인" /></a>
					</div>
					<? } ?>
					<div class="evt-warn">
						<img src="<?=$christmas_img?>/tab01_warn.png<?=vs_para();?>" alt="이벤트 주의사항" />
					</div>
				</div>
			</div>
		</div>
