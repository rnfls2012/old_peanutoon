<?php
?>

<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL?>/css/event_christmas_add.css<?=vs_para()?>" />
<script type="text/javascript" src="<?=NM_EVENTFUL_PC_URL?>/js/event_christmas.js<?=vs_para()?>"></script>
<script type="text/javascript">
<!--
	function prize_page_submit(){
		var ecm_form = document.forms["prize_form_page"];

		// 성명
		if(ecm_form.name.value == ''){ ecm_form.name.focus(); return false; }

		// 휴대폰번호
		if(ecm_form.phone.value == ''){ ecm_form.phone.focus(); return false; }
		
		<?if($gift_mode == 'y'){?>
			// 배송지정보
			if(ecm_form.address.value == ''){ ecm_form.address.focus(); return false; }

			// 우편번호
			if(ecm_form.postal.value == ''){ ecm_form.postal.focus(); return false; }
		<? } ?>

		if($('#term_ok').prop("checked") == false){ 
			alertBox("동의하셔야 합니다.");
			ecm_form.postal.focus(); return false; 
		}

		ecm_form.submit();
	}
//-->
</script>



<div class="evt-container">
	<div class="present">
		<div class="present-header">
			<h3 style="margin-top:0px; padding-top:20px;"><span>배송지 정보</span> 입니다!</h3>
		</div>
		<form name="prize_form_page" id="prize_form_page" method="post" action="<?=NM_PROC_URL;?>/eventful/event_christmas_member.php" onsubmit="return prize_page_submit();">
			<div class="prizeinfo">
					<div class="prize_name">
						<label for="name">당첨자 성명</label>
						<input type="text" id="name" name="name" placeholder="당첨자 성명을 입력해주세요" required autocomplete="off" value="<?=$row_ecm_data['ecm_name'];?>" />
					</div>
					<div class="prize_phone">
						<label for="phone">휴대폰 번호</label>
						<input type="text" id="phone" name="phone" placeholder="'-'를 포함하지 않고 입력해주세요" required autocomplete="off" value="<?=$row_ecm_data['ecm_tel'];?>" />
					</div>
					<?if($gift_mode == 'y'){?>
					<div class="prize_address">
						<label for="address">배송지 정보</label>
						<input type="text" id="address" name="address" placeholder="'도로명 주소'를 입력해주세요" required autocomplete="off" value="<?=$row_ecm_data['ecm_address'];?>" />
					</div>
					<div class="prize_zipcode">
						<label for="address">우편번호</label>
						<input type="text" id="postal" name="postal" placeholder="'우편번호 5자리'를 입력해주세요" required autocomplete="off" value="<?=$row_ecm_data['ecm_postal'];?>" />
					</div>
					<? } ?>
					<div class="pre-check">
						<p class="pre-delivery">※ 선물 상자는 2019년 1월 16일 일괄 배송 됩니다.</p>
						<ul>
							<? foreach($eci_no_data as $eci_no_data_val){?>
							<li>
								<div class="pre-img"><img src="<?=$eci_no_data_val['ecg_src'];?>" alt="<?=$eci_no_data_val['ecg_alt'];?>" /></div>
								<div class="pre-text">
									<p><?=$eci_no_data_val['ecg_txt'];?></p>
									<span><?=$eci_no_data_val['ecg_reg_day'];?></span>
								</div>
							</li>
							<? } ?>
						</ul>
					</div>
				</div>
			<div class="present-footer">
				<input type="submit" id="prize_ok" name="prize_ok" value="등록하기">
			</div>
		</form>
	</div>
</div>

