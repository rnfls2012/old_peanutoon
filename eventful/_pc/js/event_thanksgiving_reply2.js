$(document).ready(function() {
    load_data();
    function load_data(page) {
        $.ajax({
            url: nm_url+"/ajax/eventful/event_thanksgiving_reply.php",
            method: "POST",
            data: {page: page},
            success: function(data){
                $('#reply_list').html(data);
            }
        });
    }

    $(document).on('click', '.pagination_link', function() {
        var page = $(this).attr("id");
        load_data(page);
    });

    $(document).on('click', '.next_block', function() {
        var block = $('#last_page').val();
        load_data(Number(block));
    });

    $(document).on('click', '.prev_block', function() {
        var block = $('#start_page').val();
        load_data(Number(block));
    });
});


function countChar(val) {
    var len = val.value.length;
    if (len > 200) {
        val.value = val.value.substring(0, 200);
    } else {
        $('.count_box_character').text(len);
    }
}

function eventrami_submit() {

    var comments    = $("#comments").val();
    var loginPage   =  nm_url  + '/ctlogin.php';

    if($("#mb_no").val() == "" ) {
        confirmBox("로그인이 필요합니다!", goto_url, {url: loginPage});
        return false;
    } else if(comments === "" ) {
        alertBoxFocus("신작 기대평 남겨주세요!", $("#comments"));
        return false;
    } else {
        $.ajax({
            url: nm_url+"/proc/eventful/event_thanksgiving_reply.php",
            method: "POST",
            data: {comments: comments},
            success : function (data) {
                load_data();
                $(".reply_input").hide();
                $(".reply_img").show();
            }
        })
    }
}

function load_data(page) {
    $.ajax({
        url:  nm_url+"/ajax/eventful/event_thanksgiving_reply.php",
        method: "POST",
        data: {page: page},
        success: function(data){
            $('#reply_list').html(data);
        }
    });
}