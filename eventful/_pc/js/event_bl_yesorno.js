/* 원클릭 */
$(function() {
	$('.evt_contents_btn svg circle').click(function() {
		var header_height = Number($("#header").height());

		/* 	var evt_contents_top = Number($(".evt_contents").offset().top);	*/
		var id = $(this).attr('data-id');
		var id_top = Number($("#"+id).offset().top);
		var id_top_position = id_top - header_height ;

		$('html, body').animate({
			scrollTop:id_top_position
		}, 500);
	});
	$('.evt_contents_btn svg polygon').click(function() {
		var url = $(this).attr('data-url');
		link(url);
	});
	$('.evt_contents_btn svg rect').click(function() {
		var url = $(this).attr('data-url');
		link(url);
	});
});
