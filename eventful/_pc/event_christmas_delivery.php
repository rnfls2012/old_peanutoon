<?php
?>

<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL?>/css/event_christmas_add.css<?=vs_para()?>" />
<script type="text/javascript" src="<?=NM_EVENTFUL_PC_URL?>/js/event_christmas.js<?=vs_para()?>"></script>
<script type="text/javascript">
<!--
	function prize_page_submit(){
		var ecm_form = document.forms["prize_form_page"];

		// 성명
		if(ecm_form.name.value == ''){ ecm_form.name.focus(); return false; }

		// 휴대폰번호
		if(ecm_form.phone.value == ''){ ecm_form.phone.focus(); return false; }
		
		<?if($gift_mode == 'y'){?>
			// 배송지정보
			if(ecm_form.address.value == ''){ ecm_form.address.focus(); return false; }

			// 우편번호
			if(ecm_form.postal.value == ''){ ecm_form.postal.focus(); return false; }
		<? } ?>

		if($('#term_ok').prop("checked") == false){ 
			alertBox("동의하셔야 합니다.");
			ecm_form.postal.focus(); return false; 
		}

		ecm_form.submit();
	}
//-->
</script>



<div class="evt-container">
	<div class="present">
		<div class="present-header">
			<img src="<?=$gift_src?><?=vs_para()?>" alt="<?=$gift_alt?>" />
		<!-- 상품 리스트
			<img src="images/evt_christmas/pre_blanket.png" alt="담요" />
			<img src="images/evt_christmas/pre_coffee.png" alt="커피" />
			<img src="images/evt_christmas/pre_giftcard.png" alt="문화상품권" />
			<img src="images/evt_christmas/pre_lantern.png" alt="무드등" />
			<img src="images/evt_christmas/pre_movie.png" alt="영화" />
			<img src="images/evt_christmas/pre_parisbagette.png" alt="파리바게뜨" />
			<img src="images/evt_christmas/pre_umbrella.png" alt="우산" />
		-->
			<h3><span><?=$gift_txt;?></span>에 당첨되었어요!</h3>
		</div>
		<form name="prize_form_page" id="prize_form_page" method="post" action="<?=NM_PROC_URL;?>/eventful/event_christmas_delivery.php" onsubmit="return prize_page_submit();">
			<div class="prizeinfo">
					<input type="hidden" name="eci_no" id="eci_no" value="<?=$eci_no?>">
					<input type="hidden" name="ecg_no" id="ecg_no" value="<?=$ecg_no?>">
					<div class="prize_name">
						<label for="name">당첨자 성명</label>
						<input type="text" id="name" name="name" placeholder="당첨자 성명을 입력해주세요" required autocomplete="off" value="<?=$row_ecm_data['ecm_name'];?>" />
					</div>
					<div class="prize_phone">
						<label for="phone">휴대폰 번호</label>
						<input type="text" id="phone" name="phone" placeholder="'-'를 포함하지 않고 입력해주세요" required autocomplete="off" value="<?=$row_ecm_data['ecm_tel'];?>" />
					</div>
					<?if($gift_mode == 'y'){?>
					<div class="prize_address">
						<label for="address">배송지 정보</label>
						<input type="text" id="address" name="address" placeholder="'도로명 주소'를 입력해주세요" required autocomplete="off" value="<?=$row_ecm_data['ecm_address'];?>" />
					</div>
					<div class="prize_zipcode">
						<label for="address">우편번호</label>
						<input type="text" id="postal" name="postal" placeholder="'우편번호 5자리'를 입력해주세요" required autocomplete="off" value="<?=$row_ecm_data['ecm_postal'];?>" />
					</div>
					<? } ?>
				<div class="prize_check">
					<h4>개인정보 수집 및 이용에 대한 안내</h4>
					<div class="prize_term">㈜피너툰은 피너툰 윈터 파티 이벤트 경품 당첨자를 대상으로 아래와 같이 개인정보를 수집하고 있습니다.

	1. 수집 개인정보 항목 : [필수] 성명/연락처/주소/우편번호
	2. 개인정보의 수집 및 이용목적 : 경품 배송을 위한 본인확인 및 배송 주소, 우편번호, 연락 경로 확인
	3. 개인정보의 이용기간 : 이용목적 달성 후, 즉시 파기. 단, 관계법령의 규정에 의하여 보존할 필요가 있는 경우 일정기간 동안 개인정보를 보관 할 수 있습니다.

	그 밖의 사항은 ㈜피너툰 개인정보취급방침을 준수합니다.</div>
					<input type="checkbox"" name="term_ok" id="term_ok" value="y"><label for="term_ok"><span></span> 개인정보 수집 및 이용안내에 동의합니다.</label>
				</div>
			</div>
			<div class="present-footer">
				<input type="submit" id="prize_ok" name="prize_ok" value="등록하기">
			</div>
		</form>
	</div>
</div>

