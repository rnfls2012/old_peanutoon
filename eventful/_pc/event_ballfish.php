<?php
/**
 * Created by PhpStorm.
 * User: Jiwan Ye
 * Date: 2018-10-05
 * Time: 오후 6:01
 */

$evt_img = NM_PC_IMG.'eventful/evt_ballfish';
?>

<style type="text/css">
    .evt-title {
        position: relative;
        background-image:url(<?=$evt_img?>/bg01.png<?=vs_para()?>);
        background-size: 100%;
        background-repeat: no-repeat;
        background-position: center top;
        width: 100%;
        padding: 100px 0;
    }

    .evt-content {
        width:100%;
        text-align: center;
        padding-bottom: 100px;
        background:url(<?=$evt_img?>/bg02.png<?=vs_para()?>), url(<?=$evt_img?>/bg03.png<?=vs_para()?>);
        background-size: auto;
        background-position: left bottom, right bottom;
        background-repeat: no-repeat;
    }

</style>

<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/evt_ballfish.css<?=vs_para();?>">

<div class="evt-container">
    <div class="evt-title">
        <div class="evt-maincopy">
            <img src="<?=$evt_img?>/title.png<?=vs_para()?>" alt="초롱아귀 단독 이벤트" />
        </div>
    </div>
    <div class="evt-content">
        <div class="evt-titlelink">
            <a href="<?=get_comics_url(1866)?>">
                <img src="<?=$evt_img?>/title02.png<?=vs_para()?>" alt="초롱아귀" />
            </a>
        </div>
        <img src="<?=$evt_img?>/title03.png<?=vs_para()?>" alt="BES SCENE " />
        <div class="evt-link">
            <a href="<?=get_episode_url(1866, 21254)?>"><img src="<?=$evt_img?>/link01.png<?=vs_para()?>" alt="초롱아귀 프롤로그" /></a>
            <a href="<?=get_episode_url(1866, 28399)?>"><img src="<?=$evt_img?>/link02.png<?=vs_para()?>" alt="초롱아귀 36화" class="link02" /></a>
            <a href="<?=get_episode_url(1866, 31097)?>"><img src="<?=$evt_img?>/link03.png<?=vs_para()?>" alt="초롱아귀 외전 2화" class="link03" /></a>
        </div>
        <div class="evt-warn">
            <img src="<?=$evt_img?>/warn.png<?=vs_para()?>" alt="이벤트 유의사항" />
        </div>
    </div>
</div>
