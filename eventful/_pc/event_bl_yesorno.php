<?
include_once '_common.php'; // 공통
$event_bl_yesorno_img = NM_PC_IMG.'eventful/1';
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/event_bl_yesorno.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_EVENTFUL_PC_URL;?>/js/event_bl_yesorno.js<?=vs_para();?>"></script>


		<div id="event_bl_yesorno">
			<div class="evtcontainer">
				<div class="evt_contentswrap">
					<div class="evt_contents">
						<img src="<?=$event_bl_yesorno_img;?>/1200_1.png<?=vs_para();?>" alt="BL취향저녁1" />
						<div class="evt_contents_btn">
							<img src="<?=$event_bl_yesorno_img;?>/1200_2.png<?=vs_para();?>" alt="BL취향저녁2" />
							<svg class="evt_svg" xmlns="//www.w3.org/2000/svg" preserveAspectRatio="none" width="100%" height="100%">
								<circle cx="293" cy="1170" r="37" data-url="" data-id="bl_yesorno_a" />
								<circle cx="571" cy="1170" r="37" data-url="" data-id="bl_yesorno_b" />
								<circle cx="853" cy="1170" r="37" data-url="" data-id="bl_yesorno_c" />
								<circle cx="1035" cy="976" r="37" data-url="" data-id="bl_yesorno_d" />
								<circle cx="1035" cy="706" r="37" data-url="" data-id="bl_yesorno_e" />
							</svg>
						</div>
						<div class="evt_contents_btn">
							<img src="<?=$event_bl_yesorno_img;?>/1200_3.png<?=vs_para();?>" alt="BL취향저녁3" />
							<svg class="evt_svg" xmlns="//www.w3.org/2000/svg" preserveAspectRatio="none" width="100%" height="100%">
								<polygon points="22 513, 1178 513, 1178 813, 22 933" data-url="<?=NM_MKT_URL?>/4/7264h3runhhbfxny?v=2" id="bl_yesorno_a" />
								<polygon points="22 943, 1178 823, 1178 1243, 22 1145" data-url="<?=NM_MKT_URL?>/4/7264wnvvwcmixo1e?v=2" id="bl_yesorno_b" />
								<polygon points="22 1155, 1178 1253, 1178 1463, 22 1565" data-url="<?=NM_MKT_URL?>/4/7264rtnln9vphwcs?v=2" id="bl_yesorno_c" />
								<polygon points="22 1575, 1178 1473, 1178 1929, 22 1832" data-url="<?=NM_MKT_URL?>/4/72641dcrka1bmqca?v=2" id="bl_yesorno_d" />
								<polygon points="22 1842, 1178 1939, 1178 2220, 22 2220" data-url="<?=NM_MKT_URL?>/4/7264sdxtvdwfmgal?v=2" id="bl_yesorno_e" />
							</svg>
						</div>
						<div class="evt_contents_btn">
							<img src="<?=$event_bl_yesorno_img;?>/1200_4.png<?=vs_para();?>" alt="BL취향저녁4" />
							<svg class="evt_svg" xmlns="//www.w3.org/2000/svg" preserveAspectRatio="none" width="100%" height="100%">
								<rect x="22" y="544" width="1154" height="320" data-url="<?=NM_MKT_URL?>/4/7264xiav4eke5woy?v=2" />
								<rect x="22" y="866" width="1154" height="320" data-url="<?=NM_MKT_URL?>/4/7264vprqjoun1c6m?v=2" />
								<rect x="22" y="1186" width="1154" height="320" data-url="<?=NM_MKT_URL?>/4/7264heeaggahzchi?v=2" />
							</svg>
						</div>
						<img src="<?=$event_bl_yesorno_img;?>/1200_5.png<?=vs_para();?>" alt="BL취향저녁5" />
					</div>
				</div>
			</div>
		</div> <!-- /contents -->

