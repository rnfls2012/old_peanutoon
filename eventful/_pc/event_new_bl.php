<?php
/**
 * Created by PhpStorm.
 * User: Jiwan Ye
 * Date: 2018-08-20
 * Time: 오후 5:51
 */

$evt_img = NM_PC_IMG.'eventful/evt_new_bl';
?>

<style type="text/css">
.evt-title {
    position: relative;
    width: 100%;
    background:url(<?=$evt_img?>/bg02.png), url(<?=$evt_img?>/bg01.png);
    background-size: auto;
    background-position: right top, left top;
    background-repeat: no-repeat;
}
.evt-warn {
    width: 100%;
    padding: 130px 0;
    text-align: center;
    background:url(<?=$evt_img?>/bg04.png), url(<?=$evt_img?>/bg03.png);
    background-size: auto;
    background-position: right bottom, left bottom;
    background-repeat: no-repeat;
}
</style>

<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/evt_new_bl.css<?=vs_para();?>">

<div class="evt-container">
    <div class="evt-title">
        <div class="evt-maincopy">
            <img src="<?=$evt_img?>/title.png" alt="신작 BL 2땅콩 할인전" />
        </div>
    </div>
    <div class="evt-content">
        <div class="evt-link">
            <a href="<?=get_comics_url(3009)?>&utm_source=internal-event&utm_medium=event&utm_campaign=bl_discount_event&utm_content=동거인"><img src="<?=$evt_img?>/link01.png" alt="동거인" /></a>
            <a href="<?=get_comics_url(2941)?>&utm_source=internal-event&utm_medium=event&utm_campaign=bl_discount_event&utm_content=교수님의 은밀한 전생"><img src="<?=$evt_img?>/link02.png" alt="교수님의 은밀한 전생" /></a>
            <a href="<?=get_comics_url(2933)?>&utm_source=internal-event&utm_medium=event&utm_campaign=bl_discount_event&utm_content=너에게까지 99%"><img src="<?=$evt_img?>/link03.png" alt="너에게까지 99%" /></a>
            <a href="<?=get_comics_url(3044)?>&utm_source=internal-event&utm_medium=event&utm_campaign=bl_discount_event&utm_content=지안이의 말 못할 체질"><img src="<?=$evt_img?>/link04.png" alt="지안이의 말 못할 체질" /></a>
            <a href="<?=get_comics_url(3033)?>&utm_source=internal-event&utm_medium=event&utm_campaign=bl_discount_event&utm_content=구배씨의 인생역전"><img src="<?=$evt_img?>/link05.png" alt="구배씨의 인생역전" /></a>
        </div>
    </div>

    <div class="evt-warn">
        <img src="<?=$evt_img?>/warn.png" alt="이벤트 유의사항" />
    </div>

</div>
