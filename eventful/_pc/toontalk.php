<?php

include_once '_common.php'; // 공통
$toontalk_img = NM_PC_IMG.'eventful/3';
?>
<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/evt_toontalk.css<?=vs_para();?>">

<div class="clear"></div>
    <div class="evtcontainer">
        <div class="evt_header">
            <img src="<?=$toontalk_img?>/header_title.png<?=vs_para();?>" alt="피너툰 카카오톡 플러스친구 추가 이벤트!" />
            <img src="<?=$toontalk_img?>/header_exp.png<?=vs_para();?>" alt="피너툰 카카오톡 플러스친구 추가 이벤트!" />
        </div>
        <div class="evt_con01">
            <img src="<?=$toontalk_img?>/contents01.png<?=vs_para();?>" alt="이벤트 내용" />
        </div>
        <div class="evt_con02">
            <div class="evt_con02_tit">
                <img src="<?=$toontalk_img?>/contents02_tit.png<?=vs_para();?>" alt="피너툰 신작" />
            </div>
            <div class="evt_con02_link">
                <a href="<?=get_comics_url(2659)?>"><img src="<?=$toontalk_img?>/contents02_link01.png<?=vs_para();?>" alt="유수씨 그건 먹으면 안됩니다" /></a>
                <a href="<?=get_comics_url(2630)?>"><img src="<?=$toontalk_img?>/contents02_link02.png<?=vs_para();?>" alt="소담빌라" /></a>
                <a href="<?=get_comics_url(2660)?>"><img src="<?=$toontalk_img?>/contents02_link03.png<?=vs_para();?>" alt="플레이 하우스" /></a>
                <a href="<?=get_comics_url(2645)?>"><img src="<?=$toontalk_img?>/contents02_link04.png<?=vs_para();?>" alt="프라이빗 스캔들" /></a>
                <a href="<?=get_comics_url(2678)?>"><img src="<?=$toontalk_img?>/contents02_link05.png<?=vs_para();?>" alt="3억의 가장 작은 존재" /></a>
            </div>
        </div>
    </div>