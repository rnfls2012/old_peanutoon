<? 
include_once '_common.php'; // 공통 
$awards_img = NM_PC_IMG.'eventful/2';
?>
	<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/evt_awards.css<?=vs_para();?>">

	<div class="evtcontainer">
		<div class="evt_header">
			<div class="blink">
				<img src="<?=$awards_img?>/blink.png<?=vs_para();?>" alt="blink" />
			</div>
			<img src="<?=$awards_img?>/evt_header.png<?=vs_para();?>" alt="2017 피너툰 awards" />
		</div>
		<div class="evt_con01">
			<img src="<?=$awards_img?>/evt_con01_tit.png<?=vs_para();?>" alt="2017년 최고 응원상" />
			<a href="<?=get_comics_url(978)?>"><img src="<?=$awards_img?>/evt_con01_link.png<?=vs_para();?>" alt="고양이 아가씨와 경호원들" /></a>
		</div>

		<div class="evt_con02">
			<img src="<?=$awards_img?>/evt_con02_tit.png<?=vs_para();?>" alt="엄마미소상" />
			<a href="<?=get_comics_url(2024)?>"><img src="<?=$awards_img?>/evt_con02_link.png<?=vs_para();?>" alt="호식이 이야기" /></a>
		</div>

		<div class="evt_con03">
			<img src="<?=$awards_img?>/evt_con03_tit.png<?=vs_para();?>" alt="베스트 케미상" />
			<a href="<?=get_comics_url(2333)?>"><img src="<?=$awards_img?>/evt_con03_link.png<?=vs_para();?>" alt="작은 집에 사는 커다란 개" /></a>
		</div>

		<div class="evt_con04">
			<img src="<?=$awards_img?>/evt_con04_tit.png<?=vs_para();?>" alt="고구마상" />
			<a href="<?=get_comics_url(2151)?>"><img src="<?=$awards_img?>/evt_con04_link.png<?=vs_para();?>" alt="난봉꾼과 왕자님" /></a>
		</div>

		<div class="evt_con05">
			<img src="<?=$awards_img?>/evt_con05_tit.png<?=vs_para();?>" alt="기럭지상" />
			<a href="<?=get_comics_url(1187)?>"><img src="<?=$awards_img?>/evt_con05_link.png<?=vs_para();?>" alt="순종적인 유산" /></a>
		</div>

		<div class="evt_con06">
			<img src="<?=$awards_img?>/evt_con06_tit.png<?=vs_para();?>" alt="낮져밤이상" />
			<a href="<?=get_comics_url(2322)?>"><img src="<?=$awards_img?>/evt_con06_link.png<?=vs_para();?>" alt="둘만의 퀘스트" /></a>
		</div>

		<div class="evt_con07">
			<img src="<?=$awards_img?>/evt_con07_tit.png<?=vs_para();?>" alt="오른팔상" />
			<a href="<?=get_comics_url(2460)?>"><img src="<?=$awards_img?>/evt_con07_link.png<?=vs_para();?>" alt="헤이라이프" /></a>
		</div>

		<div class="evt_con08">
			<img src="<?=$awards_img?>/evt_con08_tit.png<?=vs_para();?>" alt="종신계약상" />
			<a href="<?=get_comics_url(843)?>"><img src="<?=$awards_img?>/evt_con08_link.png<?=vs_para();?>" alt="멜로드라마의문법" /></a>
		</div>

		<div class="evt_con09">
			<img src="<?=$awards_img?>/evt_con09_tit.png<?=vs_para();?>" alt="2017년 그만해상" />
			<a href="<?=get_comics_url(2207)?>"><img src="<?=$awards_img?>/evt_con09_link.png<?=vs_para();?>" alt="원조교제 아닌데요?" /></a>
		</div>

		<div class="evt_con10">
			<img src="<?=$awards_img?>/evt_con10_tit.png<?=vs_para();?>" alt="2018년 기대해상" />
			<a href="<?=get_comics_url(1674)?>"><img src="<?=$awards_img?>/evt_con10_link.png<?=vs_para();?>" alt="각자의 식사 시즌 2" /></a>
		</div>

		<div class="evt_footer">
			<img src="<?=$awards_img?>/evt_footer.png<?=vs_para();?>" alt="감사합니다" />
		</div>
	</div>
