<?php
/**
 * Created by PhpStorm.
 * User: Jiwan Ye
 * Date: 2018-09-05
 * Time: 오후 6:10
 */

$evt_img = NM_PC_IMG.'eventful/evt_adios';
?>
<style type="text/css">
    .evt-container {
        position: relative;
        width: 100%;
        background: #e9f8ff;
        background-image:url(<?=$evt_img?>/bg01.png<?=vs_para()?>);
        background-size: cover;
        background-repeat: no-repeat;
        overflow:hidden;
    }
    .evt-title {
        position: relative;
        width: 100%;
        background:url(<?=$evt_img?>/bg02.png<?=vs_para()?>), url(<?=$evt_img?>/bg03.png<?=vs_para()?>);
        background-size: auto;
        background-position: left top, right top;
        background-repeat: no-repeat;
        padding-bottom: 160px;
    }
    .evt-content {
        width:100%;
        text-align: center;
        padding-bottom: 100px;
        background:url(<?=$evt_img?>/bg04.png<?=vs_para()?>), url(<?=$evt_img?>/bg03.png<?=vs_para()?>);
        background-size: auto;
        background-position: left bottom, right bottom;
        background-repeat: no-repeat;
    }
</style>

<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_PC_URL;?>/css/evt_adios.css<?=vs_para();?>">

<div class="evt-container">
    <div class="evt-title">
        <div class="evt-maincopy">
            <img src="<?=$evt_img?>/title.png<?=vs_para()?>" alt="Good bye, 호식이 이야기!" />
        </div>
    </div>
    <div class="evt-content">
        <div class="evt-link">
            <a href="<?=get_comics_url(2024)?>&utm_source=internal-event&utm_medium=page&utm_campaign=comic_complete_event&utm_content=호식이 이야기">
                <img src="<?=$evt_img?>/link01.png<?=vs_para()?>" alt="호식이 이야기" />
            </a>
            <a href="<?=get_comics_url(2080)?>&utm_source=internal-event&utm_medium=page&utm_campaign=comic_free_event&utm_content=가족이 되자">
                <img src="<?=$evt_img?>/link02.png<?=vs_para()?>" alt="가족이 되자" />
            </a>
            <a href="<?=get_comics_url(2430)?>&utm_source=internal-event&utm_medium=page&utm_campaign=comic_free_event&utm_content=아빠도 하고 싶어">
                <img src="<?=$evt_img?>/link03.png<?=vs_para()?>" alt="아빠도 하고 싶어" />
            </a>
            <a href="<?=get_comics_url(2872)?>&utm_source=internal-event&utm_medium=page&utm_campaign=comic_free_event&utm_content=굿나잇 키스의 다음은">
                <img src="<?=$evt_img?>/link04.png<?=vs_para()?>" alt="굿나잇 키스의 다음은" />
            </a>
            <a href="<?=get_comics_url(2491)?>&utm_source=internal-event&utm_medium=page&utm_campaign=comic_free_event&utm_content=다녀왔어, 어서 와">
                <img src="<?=$evt_img?>/link05.png<?=vs_para()?>" alt="다녀왔어, 어서 와" />
            </a>
            <a href="<?=get_comics_url(2999)?>&utm_source=internal-event&utm_medium=page&utm_campaign=comic_free_event&utm_content=다녀왔어, 어서 와 - 빛나는 나날 -">
                <img src="<?=$evt_img?>/link06.png<?=vs_para()?>" alt="다녀왔어, 어서 와 - 빛나는 나날 -" />
            </a>
        </div>
        <div class="evt-warn">
            <img src="<?=$evt_img?>/warn.png<?=vs_para()?>" alt="이벤트 유의사항" />
        </div>
    </div>
</div>
