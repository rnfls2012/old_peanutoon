<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<script type="text/javascript">
		<!--
			var slide_angle_left = '<div class="slide_angle left"><img src="<?=$thanksgiving_img;?>/tab02_slide_angleleft.png<?=vs_para();?>" alt="왼쪽화살표" /></div>';
			var slide_angle_right = '<div class="slide_angle right"><img src="<?=$thanksgiving_img;?>/tab02_slide_angleright.png<?=vs_para();?>" alt="오른쪽화살표" /></div>';
		//-->
		</script>

		<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_MO_URL;?>/css/event_thanksgiving.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_EVENTFUL_MO_URL;?>/js/event_thanksgiving.js<?=vs_para();?>"></script>
		<script type="text/javascript">
		<!--
			<?=$scrolltop_js;?>
		//-->
		</script>
		<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_MO_URL;?>/css/event_thanksgiving_time.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_EVENTFUL_MO_URL;?>/js/event_thanksgiving_time.js<?=vs_para();?>"></script>
		
		<script type="text/javascript">
		<!--
			var time_data_no = "<?=$time_today_key_no?>";
		//-->
		</script>

		
		<style type="text/css">


			.ModalPopup .p_content {
				background-image: url(<?=$thanksgiving_img;?>/bg06.png<?=vs_para()?>);
				background-repeat: no-repeat;
			}			
			.evt-reply .reply_list ol li ul>li:nth-child(2):before {
				background-image: url(<?=$thanksgiving_img;?>/reply/tab04_reply_deco01.png<?=vs_para()?>);
			}

		</style>

		<div class="evt-container">
			<div class="evt-title">
				<div class="evt-maincopy">
					<img src="<?=$thanksgiving_img;?>/title.png<?=vs_para()?>" alt="피너툰 추석 대잔치" />
				</div>
			</div>
			<div class="evt-content">
				<div class="evt-frame">
					<ul class="evt-tab">
						
						<li>
							<a href="<?=NM_URL?>/event_thanksgiving.php?type=coupon"><img src="<?=$thanksgiving_img;?>/tab01_off.png<?=vs_para()?>" alt="행운의 꿀송편" /></a>
						</li>
						<li class="on">
							<a href="<?=NM_URL?>/event_thanksgiving.php?type=time"><img src="<?=$thanksgiving_img;?>/tab02_on.png<?=vs_para()?>" alt="피넛타임 편성표" /></a>
						</li>
						<li>
							<a href="<?=NM_URL?>/event_thanksgiving.php?type=sale"><img src="<?=$thanksgiving_img;?>/tab03_off.png<?=vs_para()?>" alt="통 SALE 기획전" /></a>
						</li>
						<li class="last">
							<a href="<?=NM_URL?>/event_thanksgiving.php?type=reply"><img src="<?=$thanksgiving_img;?>/tab04_off.png<?=vs_para()?>" alt="신작 기대평" /></a>
						</li>
					</ul>
					<?/* 여기서 부터 틀림-그외 다 동일함 */?>
					<div class="evt-tabcontents tabcontents02">
						<img src="<?=$thanksgiving_img;?>/tab02_exp.png<?=vs_para();?>" alt="이벤트 설명" class="exp" />
						<br />
						<a class="tab02_btn"><img src="<?=$thanksgiving_img;?>/tab02_btn.png<?=vs_para();?>" alt="오늘 편성표 보러가기" /></a>

						<div class="tab02_slide">
						<? foreach($time_data_arr as $time_data_key => $time_data_val){ ?>
						
						<script type="text/javascript">
						<!--
							$(document).on('ready', function() {
								$("#pc_slide<?=$time_data_key?> .regular").slick({
									dots: false,
									infinite: false,
									centerMode: false,
									centerPadding: '0px',
									speed: 500,
									slidesToShow: 2.3,
									slidesToScroll: 2,
									autoplay: false,
									autoplaySpeed: 5000,
									arrows: false
								});
							});
						//-->
						</script>
							<div class="slide_title">
								<img src="<?=$thanksgiving_img;?>/time/<?=$time_data_key?>/<?=$time_data_key?>.png<?=vs_para();?>" alt="<?=$time_data_key?>">
								<!-- 테스트 확인하려고 넣음 -->
								<!-- <label>test-<?=$ep_date_arr[$time_data_key]?> (본서비스일때 삭제 예정)</label> -->
								<!-- 테스트 확인하려고 넣음 end -->
							</div>
							<div class="slide_wrap" id="pc_slide<?=$time_data_key?>">
								<div class="pc_slide_bg">
									<!--
									<div class="slide_angle left">
										<img src="<?=$thanksgiving_img;?>/tab02_slide_angleleft.png" alt="왼쪽화살표" />
									</div>
									<div class="slide_angle right">
										<img src="<?=$thanksgiving_img;?>/tab02_slide_angleright.png" alt="오른쪽화살표" />
									</div>
									-->
									<div class="slide_contents">
										<div class="regular slider day_link_bg">
											<? foreach($time_data_val as $time_data_key2 => $time_data_val2){ ?>
												<div>
													<p class="day <?=$time_data_val2['class_time']?>">
														<img src="<?=$thanksgiving_img;?>/<?=$time_data_val2['class_day_img']?><?=vs_para();?>" alt="<?=$time_data_val2['class_day_img']?>" />
													</p>
													<p class="link">
														<a <?=$time_data_val2['get_comics_a_tag']?>>
															<img src="<?=$thanksgiving_img;?>/time/<?=$time_data_key?>/<?=$time_data_val2['epc_comics']?>.png<?=vs_para();?>" alt="<?=$time_data_val2['epc_comics']?>" />
														</a>
													</p>
												</div>
											<? } /* foreach($time_data_val as $time_data_key2 => $time_data_val2){ end */ ?>
										</div>
									</div><!-- slide_contents -->
								</div><!-- pc_slide_bg -->
							</div><!-- slide_wrap -->
						<? } /* foreach($time_data_arr as $time_data_key => $time_data_val){ end */ ?>
						</div><!-- tab02_slide -->

						<div class="evt-warn">
							<img src="<?=$thanksgiving_img;?>/tab02_warn.png<?=vs_para();?>" alt="이벤트 유의사항">
						</div>
					</div>
					<?/* 여기서 부터 틀림-그외 다 동일함 end */?>
				</div>
			</div>
		</div>