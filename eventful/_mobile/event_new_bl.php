<?php
/**
 * Created by PhpStorm.
 * User: Jiwan Ye
 * Date: 2018-08-20
 * Time: 오후 7:28
 */

$evt_img = NM_MO_IMG.'eventful/evt_new_bl';
?>
<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_MO_URL;?>/css/evt_new_bl.css<?=vs_para();?>">

<div class="evt-container">
    <div class="evt-header">
        <img src="<?=$evt_img?>/title.png" alt="신작 BL 2땅콩 할인전" />
    </div>
    <div class="evt-content">
        <div class="evt-link">
            <a href="<?=get_comics_url(3009)?>&utm_source=internal-event&utm_medium=event&utm_campaign=bl_discount_event&utm_content=동거인">
                <img src="<?=$evt_img?>/link01.png" alt="동거인 3009" />
            </a>
            <a href="<?=get_comics_url(2941)?>&utm_source=internal-event&utm_medium=event&utm_campaign=bl_discount_event&utm_content=교수님의 은밀한 전생">
                <img src="<?=$evt_img?>/link02.png" alt="교수님의 은밀한 전생 2941" />
            </a>
            <a href="<?=get_comics_url(2933)?>&utm_source=internal-event&utm_medium=event&utm_campaign=bl_discount_event&utm_content=너에게까지 99%">
                <img src="<?=$evt_img?>/link03.png" alt="너에게까지 99% 2933" />
            </a>
            <a href="<?=get_comics_url(3044)?>&utm_source=internal-event&utm_medium=event&utm_campaign=bl_discount_event&utm_content=지안이의 말 못할 체질">
                <img src="<?=$evt_img?>/link04.png" alt="구배씨의 인생역전 3033" />
            </a>
            <a href="<?=get_comics_url(3033)?>&utm_source=internal-event&utm_medium=event&utm_campaign=bl_discount_event&utm_content=구배씨의 인생역전">
                <img src="<?=$evt_img?>/link05.png" alt="지안이의 말 못할 체질 3044" />
            </a>
        </div>
    </div>

    <div class="evt-warn"><img src="<?=$evt_img?>/warn.png" alt="이벤트 유의사항" /></div>
</div>
