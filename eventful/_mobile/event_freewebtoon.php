<?php

$evt_img = NM_MO_IMG.'eventful/evt_freewebtoon';
?>

<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_MO_URL;?>/css/evt_freewebtoon.css<?=vs_para();?>">

<div class="evt-container">
	<div class="evt-title">
		<img src="<?=$evt_img?>/title.png<?=vs_para()?>" alt="무료 웹툰 기획전" />
	</div>
	<div class="evt-link">
		<!-- 기생툰 -->
		<div class="con01">
			<a href="<?=get_comics_url(557)?>"><img src="<?=$evt_img?>/con01_link01.png<?=vs_para()?>" alt="기생툰 557" /></a>
			<a href="<?=get_episode_url(557,8409)?>">"><img src="<?=$evt_img?>/con01_link02.png<?=vs_para()?>" alt="기생툰 557 ep24" /></a>
			<a href="<?=get_episode_url(557,30436)?>"><img src="<?=$evt_img?>/con01_link03.png<?=vs_para()?>" alt="기생툰 557 ep224"/></a>
			<a href="<?=get_episode_url(557,31005)?>"><img src="<?=$evt_img?>/con01_link04.png<?=vs_para()?>" alt="기생툰 557 ep233" /></a>
		</div>
		<!-- PT툰 -->
		<div class="con02">
			<a href="<?=get_comics_url(2720)?>"><img src="<?=$evt_img?>/con02_link01.png<?=vs_para()?>" alt="PT툰 2720" /></a>
			<a href="<?=get_episode_url(2720,29474)?>"><img src="<?=$evt_img?>/con02_link02.png<?=vs_para()?>" alt="PT툰 2720 ep02" /></a>
			<a href="<?=get_episode_url(2720,30658)?>"><img src="<?=$evt_img?>/con02_link03.png<?=vs_para()?>" alt="PT툰 2720 ep18" /></a>
			<a href="<?=get_episode_url(2720,31391)?>"><img src="<?=$evt_img?>/con02_link04.png<?=vs_para()?>" alt="PT툰 2720 ep최종화" /></a>
		</div>
		<!-- 잡종툰 -->
		<div class="con03">
			<a href="<?=get_comics_url(2061)?>"><img src="<?=$evt_img?>/con03_link01.png<?=vs_para()?>" alt="잡종툰 2061" /></a>
			<a href="<?=get_episode_url(2061,24202)?>"><img src="<?=$evt_img?>/con03_link02.png<?=vs_para()?>" alt="잡종툰 2061 ep11" /></a>
			<a href="<?=get_episode_url(2061,31322)?>"><img src="<?=$evt_img?>/con03_link03.png<?=vs_para()?>" alt="잡종툰 2061 ep120" /></a>
			<a href="<?=get_episode_url(2061,31102)?>"><img src="<?=$evt_img?>/con03_link04.png<?=vs_para()?>" alt="잡종툰 2061 ep115" /></a>
		</div>
		<!-- 일상로맨스 -->
		<div class="con04">
			<a href="<?=get_comics_url(855)?>"><img src="<?=$evt_img?>/con04_link01.png<?=vs_para()?>" alt="일상로맨스 855" /></a>
			<a href="<?=get_episode_url(855,12272)?>"><img src="<?=$evt_img?>/con04_link02.png<?=vs_para()?>" alt="일상로맨스 855 ep41" /></a>
			<a href="<?=get_episode_url(855,14309)?>"><img src="<?=$evt_img?>/con04_link03.png<?=vs_para()?>" alt="일상로맨스 855 ep59" /></a>
			<a href="<?=get_episode_url(855,21134)?>"><img src="<?=$evt_img?>/con04_link04.png<?=vs_para()?>" alt="일상로맨스 855 ep116" /></a>
		</div>
		<!-- 잡탕주의 -->
		<div class="con05">
			<a href="<?=get_comics_url(1682)?>"><img src="<?=$evt_img?>/con05_link01.png<?=vs_para()?>" alt="잡탕주의 1682" /></a>
			<a href="<?=get_episode_url(1682,19098)?>"><img src="<?=$evt_img?>/con05_link02.png<?=vs_para()?>" alt="잡탕주의 1682 ep06" /></a>
			<a href="<?=get_episode_url(1682,30446)?>"><img src="<?=$evt_img?>/con05_link03.png<?=vs_para()?>" alt="잡탕주의 1682 ep161" /></a>
			<a href="<?=get_episode_url(1682,31678)?>"><img src="<?=$evt_img?>/con05_link04.png<?=vs_para()?>" alt="잡탕주의 1682 ep183" /></a>
		</div>
	</div>

	<div class="evt-warn">
		<img src="<?=$evt_img?>/warn.png<?=vs_para()?>" alt="이벤트 유의사항" />
	</div>
</div>