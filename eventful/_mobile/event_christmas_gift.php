<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_MO_URL;?>/css/event_christmas.css<?=vs_para();?>">
		<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
		<style type="text/css">
			.evt-gauge {
				position: relative;
				width: 100%;
				background: url(<?=$christmas_img?>/pre_pocket_bg.png<?=vs_para();?>);
				background-size: 90%;
				background-position: center top;
				background-repeat: no-repeat;
				font-size: 16px;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio'] { display: none; }
			.ModalPopup .p_content01 ul.p_choice li input[type='radio'] + label::before {
				content: '';
				display: inline-block;
				width: 95%;
				padding-top: 105%;
				background-position: left top;
				cursor: pointer;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket1 + label::before {
				background: url(<?=$christmas_img?>/tab01_popup_box01.png<?=vs_para();?>);
				background-repeat: no-repeat;
				background-size: 200%;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket1:checked +label::before {
				background-position: right top;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket2 + label::before {
				background: url(<?=$christmas_img?>/tab01_popup_box02.png<?=vs_para();?>);
				background-repeat: no-repeat;
				background-size: 200%;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket2:checked +label::before {
				background-position: right top;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket3 + label::before {
				background: url(<?=$christmas_img?>/tab01_popup_box03.png<?=vs_para();?>);
				background-repeat: no-repeat;
				background-size: 200%;
			}
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket3:checked +label::before {
				background-position: right top;
			}
			.ModalPopup .p_content02 { padding: 20px; max-width: 500px; margin: 0 auto; }
			.ModalPopup .p_content02 .p_coupon {
				position: relative;
				max-width: 320px;
				height: 148px;
				background: url(<?=$christmas_img?>/p_coupon.png<?=vs_para();?>) no-repeat center top;
				margin: 0 auto;
				padding:45px 0 0 85px;
				background-size: 100%;
			}

		</style>
		<script type="text/javascript" src="<?=NM_EVENTFUL_MO_URL;?>/js/event_christmas.js<?=vs_para();?>"></script>		
		<script type="text/javascript">
		<!--
			<?=$scrolltop_js;?>
		//-->
		</script>
		
		<input type="hidden" id="action" value="<?=$christmas_action;?>" />
		<input type="hidden" id="fill" value="<?=$christmas_fill;?>" />

		
		<div class="evt-container">
			<!-- Modal Popup -->
			<div class="Modalalert">
				<div class="ModalBackground"></div>
				<div class="ModalPopup">
					<div class="ModalClose"><i class="material-icons">clear</i></div>
					<div class="p_content01">
						<div class="p_title">
							<img src="<?=$christmas_img?>/tab01_popup.png<?=vs_para();?>" alt="tab01_popup" />
						</div>
						<ul class="p_choice">
							<li>
								<input type="radio" name="pocket" value="pocket1" id="pocket1">
								<label for="pocket1">선물상자1</label>
							</li>
							<li>
								<input type="radio" name="pocket" value="pocket2" id="pocket2">
								<label for="pocket2">선물상자2</label>
							</li>
							<li>
								<input type="radio" name="pocket" value="pocket3" id="pocket3">
								<label for="pocket3">선물상자3</label>
							</li>
						</ul>
						<div class="p_btnbig">
							<button id="open_btn">바로 열기</button>
						</div>
					</div>

					<!-- 당첨 부분 -->
					<div class="p_content02" style="display: none;">
						<img src="<?=$christmas_img?>/pre_mpeanut.png<?=vs_para();?>" alt="미니땅콩 당첨!">
						<p><span id="result_text">미니땅콩 00개</span>를 받았어요!</p>
						<button>확인</button>
					</div>
				</div>
			</div>
			<!-- /Modal Popup -->
			<div class="evt-header">
				<img src="<?=$christmas_img?>/title.png<?=vs_para();?>" alt="피너툰 Winter Party" />
			</div>
			<div class="evt-contents">
				<div class="evt-tablist">
					<ul class="evt-frame">
						<li class="active"><a href="<?=NM_URL?>/event_christmas.php?type=gift"><img src="<?=$christmas_img?>/tab01_on.png<?=vs_para();?>" alt="1탄" /></a></li>
						<li><a href="<?=NM_URL?>/event_christmas.php?type=sale"><img src="<?=$christmas_img?>/tab02.png<?=vs_para();?>" alt="2탄" /></a></li>
						<li><img src="<?=$christmas_img?>/tab03.png<?=vs_para();?>" alt="3탄" /></li>
					</ul>
				</div>
				<div class="evt-tabcontent">
					<img src="<?=$christmas_img?>/tab01_exp.png<?=vs_para();?>" class="w-100" alt="이벤트 설명" />
					<div class="evt-gauge">
						<!-- <div class="gauge_logout"><img src="<?=$christmas_img?>/gauge01.png<?=vs_para();?>"></div> -->
						<!-- <div class="gauge_login">
							<img src="<?=$christmas_img?>/gauge02.png<?=vs_para();?>">
							<div class="gauge_count">
								<p><span>10</span>개</p>
								<p class="peanut">소진 땅콩 수</p>
							</div>
						</div> -->
						<div class="gauge_open christmas">
							<img src="<?=$christmas_img?>/gauge02.png<?=vs_para();?>" alt="모은 땅콩 수">
							<div class="gauge_count">
								<p><span id="christmas_text1"><?=$christmas_text1;?></span></p>
								<p class="peanut" id="christmas_text2"><?=$christmas_text2;?></p>
							</div>
						</div>
						<div class="gauge_bar">
							<div id="christmas_percent" class="bar" style="width: <?=$christmas_percent;?>%;";></div>
							<div class="fill"></div>
						</div>
					</div>
					<div class="evt-present w-100">
						<img src="<?=$christmas_img?>/tab01_pre.png<?=vs_para();?>" class="w-100" alt="이벤트 경품" />
					</div>
					<? if($eci_is_goods_bool == true){ ?>
					<div class="evt-delivery">
						<a>
							<img src="<?=$christmas_img?>/delivery_btn.png<?=vs_para();?>" class="w-100" alt="배송지 확인" />
						</a>
					</div>
					<? } ?>
					<div class="evt-warn w-100">
						<img src="<?=$christmas_img?>/tab01_warn.png<?=vs_para();?>" class="w-100" alt="이벤트 주의사항" />
					</div>
				</div>
			</div>
		</div>
