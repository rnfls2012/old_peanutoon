<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_MO_URL;?>/css/event_christmas.css<?=vs_para();?>" />
		<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
		<style type="text/css">
			.evt-gauge {
				position: relative;
				width: 100%;
				background: url(<?=$christmas_img?>/pre_pocket_bg.png<?=vs_para();?>);
				background-size: 90%;
				background-position: center top;
				background-repeat: no-repeat;
				font-size: 16px;
			}
		</style>
		<script type="text/javascript">
		<!--
			<?=$scrolltop_js;?>
		//-->
		</script>

		
		<div class="evt-container">
			<!-- Modal Popup -->
			<div class="Modalalert">
				<div class="ModalBackground"></div>
				<div class="ModalPopup">
					<div class="ModalClose"><i class="material-icons">clear</i></div>
					<div class="p_content01">
						<div class="p_title">
							<img src="<?=$christmas_img?>/tab01_popup.png<?=vs_para();?>" alt="tab01_popup" />
						</div>
						<ul class="p_choice">
							<li>
								<input type="radio" name="pocket" value="pocket1" id="pocket1">
								<label for="pocket1">선물상자1</label>
							</li>
							<li>
								<input type="radio" name="pocket" value="pocket2" id="pocket2">
								<label for="pocket2">선물상자2</label>
							</li>
							<li>
								<input type="radio" name="pocket" value="pocket3" id="pocket3">
								<label for="pocket3">선물상자3</label>
							</li>
						</ul>
						<div class="p_btnbig">
							<button>바로 열기</button> <!-- 비활성화는 nochk 클래스 붙이면 됨 -->
						</div>
					</div>
				</div>
			</div>
			<!-- /Modal Popup -->
			<div class="evt-header">
				<img src="<?=$christmas_img?>/title.png<?=vs_para();?>" alt="피너툰 Winter Party" />
			</div>
			<div class="evt-contents">
				<div class="evt-tablist">
					<ul class="evt-frame">
						<li><a href="<?=NM_URL?>/event_christmas.php?type=gift"><img src="<?=$christmas_img?>/tab01.png<?=vs_para();?>" alt="1탄" /></a></li>
						<li class="active"><a href="<?=NM_URL?>/event_christmas.php?type=sale"><img src="<?=$christmas_img?>/tab02_on.png<?=vs_para();?>" alt="2탄" /></a></li>
						<li><img src="<?=$christmas_img?>/tab03.png<?=vs_para();?>" alt="3탄" /></li>
					</ul>
				</div>
				<div class="evt-tabcontent">
					<img src="<?=$christmas_img?>/tab02_exp.png<?=vs_para();?>" class="w-100" alt="이벤트 설명" />
					<div class="evt-link">
						<? foreach($evt_christmas_sale_comic as $et_sale_comic_val2){ ?>
						<a <?=$et_sale_comic_val2[3]?>>
							<img src="<?=$christmas_img;?>/tab02_link<?=$et_sale_comic_val2[5]?>.png<?=vs_para();?><?=vs_para()?>" alt="<?=$et_sale_comic_val2[0]?>" />
						</a>
						<? } ?>
					</div>
					<div class="evt-warn w-100">
						<img src="<?=$christmas_img?>/tab02_warn.png<?=vs_para();?>" class="w-100" alt="이벤트 주의사항" />
					</div>
				</div>
			</div>
		</div>