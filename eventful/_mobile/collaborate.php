<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-07-16
 * Time: 오전 10:53
 */

$evt_img_path = NM_IMG.$nm_config['nm_mode'].'/eventful/4'; // 이미지 경로
?>

<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_MO_URL;?>/css/collaborate.css<?=vs_para();?>">

    <div class="evt-container">
        <div class="evt-header">
            <img src="<?=$evt_img_path?>/title01.png<?=vs_para()?>" alt="국산 해물 삼계탕 EVENT" />
        </div>
        <div class="evt-insp">
            <img src="<?=$evt_img_path?>/insp.png<?=vs_para()?>" alt="이벤트 안내" />
        </div>
        <div class="evt-content">
            <div class="evt-link">
                <a href="<?=get_comics_url(2678)?>">
                    <img src="<?=$evt_img_path?>/link01.png<?=vs_para()?>" alt="3억의 가장 작은 존재 2678" />
                </a>
                <a href="<?=get_comics_url(1067)?>">
                    <img src="<?=$evt_img_path?>/link02.png<?=vs_para()?>" alt="청년의 음란함은 주방에서 1067" />
                </a>
                <a href="<?=get_comics_url(1746)?>">
                    <img src="<?=$evt_img_path?>/link03.png<?=vs_para()?>" alt="Kill your HONEY♥(킬유어허니) 1746" />
                </a>
                <a href="<?=get_comics_url(596)?>">
                    <img src="<?=$evt_img_path?>/link04.png<?=vs_para()?>" alt="여고생과 편의점 596" />
                </a>
                <a href="<?=get_comics_url(1235)?>">
                    <img src="<?=$evt_img_path?>/link05.png<?=vs_para()?>" alt="신의 신부1235" />
                </a>
            </div>
        </div>

        <div class="evt-warn"><img src="<?=$evt_img_path?>/warn.png<?=vs_para()?>" alt="이벤트 유의사항" /></div>
    </div>