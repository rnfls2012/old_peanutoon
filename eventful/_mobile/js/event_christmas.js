$(function(){
    $('.Modalalert > .ModalBackground').click(function(){
		Modalalert_close();
    });	
    $('.Modalalert > .ModalPopup > .ModalClose').click(function(){
		Modalalert_close();
    });
    $('.ModalPopup .p_content02 button').click(function(){
		Modalalert_close();
    });

    $('.evt-delivery a').click(function(){
		link(nm_url+'/event_christmas.php?type=member','eventchristmas_delivery', 710, 770);
    });

	/* 바로열기 버튼 활성 유무에 따라 Alert */
	$("#open_btn").click(function() {
		if($('input:radio[name="pocket"]:checked').val()) {
			/* AJAX 처리 */
			$.ajax({
				url: nm_url+'/ajax/eventful/event_christmas.php',
				cache: false, 
				dataType : "json", 
				success: function(data) {
					if(data['result']) {
						if(data['code']=='0'){
							$("#christmas_text1").text(data['text1']);	
							$("#christmas_text2").text(data['text2']);		
							$("#christmas_percent").css('width',data['percent']+'%');
							$("#action").val(data['action']);	
							if(data['goods']!='n'){
								$(".p_content01").css({'display': 'none'});
								$(".p_content02").css({'display': 'none'});	
								Modalalert_close();
								link(nm_url+'/event_christmas.php?type=delivery&eci_no='+data['eci_no'],'eventchristmas_delivery', 710, 770);
							}else{
								$(".p_content01").css({'display': 'none'});
								$(".p_content02").css({'display': ''});	
								$("#result_text").html(data['text']);// 미니땅콩 00개
							}
						}else{
							alertBox(data['text']);
						}
					} else {
						alertBox(data['text']);
					} // end else
				} // end success
			}) // end ajax
		} else {
			alertBox("선물 상자 선택하세요!");
			return false;
		} // end else
	});
	
    $('.christmas').click(function(){
		event_christmas_action();
	});
});


function Modalalert_close(){
	$('.Modalalert > .ModalBackground').hide();
	$('.Modalalert > .ModalPopup').hide();
	$('html, body').css({'overflow': 'auto'});
	$('.Modalalert > .ModalBackground').off('scroll touchmove mousewheel');
	$('.Modalalert > .ModalBackground').removeAttr('style');
	$("input[name=pocket]").prop("checked", false);
	$('.Modalalert > .ModalPopup').removeAttr('style');

	// if($(".p_content01").css("display") == "none") {
	$(".p_content01").show();
	$(".p_content02").hide();
	// location.reload();
	// }
}

function event_christmas_action(){
	var action = $('#action').val();
	var fill = $('#fill').val();

	if(action == 'enable'){
		position_cm($('.Modalalert > .ModalPopup'));
	}else if(action == 'unable'){
		// confirmBox('선물 상자까지 '+fill+' 땅콩 남았습니다. ', goto_url, {url:nm_url});
		alertBox('선물 상자까지 '+fill+' 땅콩 남았습니다. ');
	}else{
		confirmBox('해당 이벤트는 로그인시 참여하실 수 있습니다.', goto_url, {url:nm_url+'/ctlogin.php'});
	}
}



function position_cm(obj) {
	var windowHeight = $(window).height();
	var topOfWindow = $(window).scrollTop()
	var $obj = $(obj);
	var objHeight = $obj.height();
	var headerHeigth = $('#header').height() / 2;
	$obj.css ({
		/* 'top':(windowHeight/2) - (objHeight/2) + headerHeigth + topOfWindow */
		'top':(windowHeight/2) - (objHeight/2) + headerHeigth + topOfWindow - $('#header').height()
	});		

	var maskHeight = $(window).height();
	var topOfWindow = $(window).scrollTop()

	$('.Modalalert > .ModalBackground').show();
	$('.Modalalert > .ModalBackground').css({'height':maskHeight, 'top':topOfWindow});
	$('html, body').css({'overflow': 'hidden'});
	$('.Modalalert > .ModalBackground').on('scroll touchmove mousewheel', function(event) { // 터치무브와 마우스휠 스크롤 방지
		event.preventDefault();
		event.stopPropagation();
		return false;
	});

	obj.show();

	return this;
};