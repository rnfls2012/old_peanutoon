<?php
/**
 * Created by PhpStorm.
 * User: Jiwan Ye
 * Date: 2018-10-05
 * Time: 오후 6:01
 */

$evt_img = NM_MO_IMG.'eventful/evt_ballfish';
?>

<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_MO_URL;?>/css/evt_ballfish.css<?=vs_para()?>">

<div class="evt-container">
    <div class="evt-header">
        <img src="<?=$evt_img?>/title.png<?=vs_para()?>" alt="초롱아귀 단독 이벤트" />
        <a href="<?=get_comics_url(1866)?>"><img src="<?=$evt_img?>/title02.png<?=vs_para()?>" alt="초롱아귀" /></a>
    </div>
    <div class="evt-content">
        <div class="evt-link">
            <img src="<?=$evt_img?>/title03.png<?=vs_para()?>" alt="BEST SCENE" />
            <a href="<?=get_episode_url(1866, 21254)?>">
                <img src="<?=$evt_img?>/link01.png<?=vs_para()?>" alt="초롱아귀 프롤로그" />
            </a>
            <a href="<?=get_episode_url(1866, 28399)?>">
                <img src="<?=$evt_img?>/link02.png<?=vs_para()?>" alt="초롱아귀 36화" />
            </a>
            <a href="<?=get_episode_url(1866, 31097)?>">
                <img src="<?=$evt_img?>/link03.png<?=vs_para()?>" alt="초롱아귀 외전 2화" />
            </a>
        </div>
    </div>
    <div class="evt-warn"><img src="<?=$evt_img?>/warn.png<?=vs_para()?>" alt="이벤트 유의사항" /></div>
</div>


