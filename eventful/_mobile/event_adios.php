<?php
/**
 * Created by PhpStorm.
 * User: Jiwan Ye
 * Date: 2018-09-05
 * Time: 오후 6:10
 */

$evt_img = NM_MO_IMG.'eventful/evt_adios';
?>

<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_MO_URL;?>/css/evt_adios.css<?=vs_para()?>">

<div class="evt-container">
    <div class="evt-header">
        <img src="<?=$evt_img?>/title01.png<?=vs_para()?>" alt="Good bye, 호식이 이야기!" />
        <img src="<?=$evt_img?>/title02.png<?=vs_para()?>" alt="Good bye, 호식이 이야기!" />
        <img src="<?=$evt_img?>/title03.png<?=vs_para()?>" alt="Good bye, 호식이 이야기!" />
    </div>
    <div class="evt-content">
        <div class="evt-link">
            <a href="<?=get_comics_url(2024)?>&utm_source=internal-event&utm_medium=page&utm_campaign=comic_complete_event&utm_content=호식이 이야기">
                <img src="<?=$evt_img?>/link01.png<?=vs_para()?>" alt="호식이 이야기" />
            </a>
            <a href="<?=get_comics_url(2080)?>&utm_source=internal-event&utm_medium=page&utm_campaign=comic_free_event&utm_content=가족이 되자">
                <img src="<?=$evt_img?>/link02.png<?=vs_para()?>" alt="가족이 되자" />
            </a>
            <a href="<?=get_comics_url(2430)?>&utm_source=internal-event&utm_medium=page&utm_campaign=comic_free_event&utm_content=아빠도 하고 싶어">
                <img src="<?=$evt_img?>/link03.png<?=vs_para()?>" alt="아빠도 하고 싶어" />
            </a>
            <a href="<?=get_comics_url(2872)?>&utm_source=internal-event&utm_medium=page&utm_campaign=comic_free_event&utm_content=굿나잇 키스의 다음은">
                <img src="<?=$evt_img?>/link04.png<?=vs_para()?>" alt="굿나잇 키스의 다음은" />
            </a>
            <a href="<?=get_comics_url(2491)?>&utm_source=internal-event&utm_medium=page&utm_campaign=comic_free_event&utm_content=다녀왔어, 어서 와">
                <img src="<?=$evt_img?>/link05.png<?=vs_para()?>" alt="다녀왔어, 어서 와" />
            </a>
            <a href="<?=get_comics_url(2999)?>&utm_source=internal-event&utm_medium=page&utm_campaign=comic_free_event&utm_content=다녀왔어, 어서 와 - 빛나는 나날 -">
                <img src="<?=$evt_img?>/link06.png<?=vs_para()?>" alt="다녀왔어, 어서 와 - 빛나는 나날 -" />
            </a>
        </div>
    </div>

    <div class="evt-warn"><img src="<?=$evt_img?>/warn.png<?=vs_para()?>" alt="이벤트 유의사항" /></div>
</div>