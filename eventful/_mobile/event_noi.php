<?php
/**
 * Created by PhpStorm.
 * User: Jiwan Ye
 * Date: 2018-10-24
 * Time: 오전 9:33
 */

$evt_img = NM_MO_IMG.'eventful/evt_noi';
?>

<link rel="stylesheet" type="text/css" href="<?=NM_EVENTFUL_MO_URL;?>/css/evt_noi.css<?=vs_para()?>">

<div class="evt-container">
    <div class="evt-header">
        <img src="<?=$evt_img?>/title01.png<?=vs_para()?>" alt="노이 완결 기념 이벤트" />
        <a href="<?=get_comics_url(1979)?>"><img src="<?=$evt_img?>/title02.png<?=vs_para()?>" alt="노이" /></a>
    </div>
    <div class="evt-content">
        <div class="evt-link">
            <a href="<?=get_comics_url(2344)?>">
                <img src="<?=$evt_img?>/link01.png<?=vs_para()?>" alt="[웹툰판] 사람 거로는 만족할 수 없어♡" />
            </a>
            <a href="<?=get_comics_url(1039)?>">
                <img src="<?=$evt_img?>/link02.png<?=vs_para()?>" alt="애완용 로봇 릴리" />
            </a>
            <a href="<?=get_comics_url(1234)?>">
                <img src="<?=$evt_img?>/link03.png<?=vs_para()?>" alt="에로봇 –뒤를 털려버렸어-" />
            </a>
            <a href="<?=get_comics_url(507)?>">
                <img src="<?=$evt_img?>/link04.png<?=vs_para()?>" alt="쾌감인형의 사랑" />
            </a>
        </div>
    </div>

    <div class="evt-warn"><img src="<?=$evt_img?>/warn.png<?=vs_para()?>" alt="이벤트 유의사항" /></div>
</div>
