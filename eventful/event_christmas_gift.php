<?
include_once '_common.php'; // 공통

/* DB */

// 권한 
$event_christmas_bool = true;
if(event_christmas_state_get() == "") {
	$event_christmas_bool = false;
}
if($nm_member['mb_class'] == "a"){
	$event_christmas_bool = true;
}

// 이벤트 시작
if($event_christmas_bool == false){
	cs_alert("진행중인 이벤트가 없습니다!", NM_URL);
	die;
}else{
	$christmas_mb = event_christmas_point_used_get($nm_member);	

	if($christmas_mb['ecpu_member'] == "") {
		event_christmas_point_used_set($nm_member);
		$christmas_mb = event_christmas_point_used_get($nm_member);
	} // end if

	$christmas_decision = event_christmas_point_used_decision($christmas_mb);

	$christmas_action  = $christmas_decision['action']; // 회원상태
	$christmas_calc    = intval($christmas_decision['calc']); // 소진 땅콩수
	$christmas_chance  = intval($christmas_decision['chance']); // 뽑기 수
	$christmas_fill    = intval($christmas_decision['fill']); // 땅콩 채울 수
	$christmas_percent = intval($christmas_decision['percent']); // 땅콩 %

	
	if($christmas_action == 'enable'){
		$christmas_text1 = $christmas_chance."번";
		$christmas_text2 = "OPEN!";
	}else if($christmas_action == 'unable'){
		$christmas_text1 = $christmas_calc."개";
		$christmas_text2 = "CLOSE!";
	}else{
		$christmas_text1 = "로그인시";
		$christmas_text2 = "참여 가능";
	}

	// 배송할 경품 확인
	$eci_is_goods_bool = false;
	$eci_no_data = event_christmas_gift_row_get_is_goods($nm_member);
	if(count($eci_no_data) > 0){
		$eci_is_goods_bool = true;
	}
}

/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */
/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once(NM_EVENTFUL_PATH.'/'.$nm_config['nm_mode']."/event_christmas_gift.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>