<?
include_once '_common.php'; // 공통

// 회원정보 확인
$reply_mb_adult = 'n';
if($is_member == true){
	if($nm_member['mb_adult'] == 'y'){
		$reply_mb_adult = 'y';
	}
}else{
	$reply_mb_adult = 'log';
}

// 신작 리스트
	// sale comics 데이터
	$evt_thanksgiving_sale_comic = array();
	// array_push(변수, array(코믹스번호, 성인yn, 에피소드1화/오픈예정, a_tag, 구매yn, 이미지번호, 땅콩소비));
	array_push($evt_thanksgiving_sale_comic, array(3211, 'n', 31954             , '', 'n', '01', 46)); // 편의점 빌런
	array_push($evt_thanksgiving_sale_comic, array(3255, 'n', 32376             , '', 'n', '02', 43)); // 지주 : 구슬을 가지다
	array_push($evt_thanksgiving_sale_comic, array(3265, 'n', 32403             , '', 'n', '03', 78)); // 남고생과 동거중!
	array_push($evt_thanksgiving_sale_comic, array(3269, 'n', 32412             , '', 'n', '04', 52)); // 사립 남자아이 고등학교
	array_push($evt_thanksgiving_sale_comic, array(0   , 'n', '10월 오픈 예정입니다.', '', 'n', '05', 79)); // 황금정원 -> 날짜 임시 넣기
	array_push($evt_thanksgiving_sale_comic, array(2562, 'n', 28642             , '', 'n', '06', 56)); // 로빈의 법칙 시즌 2

	foreach($evt_thanksgiving_sale_comic as &$et_reply_comic_val){
		// 데이터 가져오기
		$evt_thanksgiving_sale_get_comics = get_comics($et_reply_comic_val[0]);
		// 성인 처리
		$et_reply_comic_val[1] = $evt_thanksgiving_sale_get_comics['cm_adult'];
		
		// 클릭 처리
		$reply_mb_adult_n_msg = "해당 작품은 성인만 보실수 있습니다.";
		$reply_mb_adult_log_msg = "신작 기대평 은 로그인 후 이용 하실수 있습니다.";
		// 1화 링크
		$evt_thanksgiving_reply_first_url = get_episode_url($et_reply_comic_val[0], $et_reply_comic_val[2]);		
		
		if($et_reply_comic_val[0] == 0){
			$et_reply_comic_val[3] = ' onclick="alertBox(\''.$et_reply_comic_val[2].'\');" '; // 작품 미정 처리
		}else if($reply_mb_adult == 'y'){
			$et_reply_comic_val[3] = ' href="'.$evt_thanksgiving_reply_first_url.'" '; // 링크
		}else if($evt_thanksgiving_sale_get_comics['cm_adult'] == 'n'){
			$et_reply_comic_val[3] = ' href="'.$evt_thanksgiving_reply_first_url.'" '; // 링크
		}else if($evt_thanksgiving_sale_get_comics['cm_adult'] == 'y' && $reply_mb_adult == 'n'){
			$et_reply_comic_val[3] = ' onclick="alertBox(\''.$reply_mb_adult_n_msg.'\');" '; // 성인작품&청소년 => alert
		}else{
			$et_reply_comic_val[3] = ' onclick="alertBox(\''.$reply_mb_adult_log_msg.'\');" '; // alert
		}
	}
// 확인
// print_r($evt_thanksgiving_sale_comic);

// 댓글
	$did_comment = false;
	if ( $is_member == true ) {
		// 로그인 됨.
		$sql_event_thanksgiving_reply = "SELECT COUNT(*) AS cnt FROM event_thanksgiving_reply WHERE mb_no = ".$nm_member['mb_no'];
		$row_event_thanksgiving_reply = sql_fetch($sql_event_thanksgiving_reply);
		if ( $row_event_thanksgiving_reply['cnt'] > 0 ) {
			$did_comment = true;
		}
	}


// print_r($evt_thanksgiving_sale_comic);

/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */
/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once(NM_EVENTFUL_PATH.'/'.$nm_config['nm_mode']."/event_thanksgiving_reply.php");

include_once (NM_PATH.'/_tail.php'); // 공통

?>