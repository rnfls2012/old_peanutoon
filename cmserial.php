<? include_once '_common.php'; // 공통

error_page();

include_once (NM_PATH.'/_head.php'); // 공통

/* COOKIE */

$sub_list_url = get_js_cookie('ck_sub_list_url');
$sub_list_limit = intval(get_int(get_js_cookie('ck_load_list_limit')));
if($sub_list_limit < NM_SUB_LIST_LIMIT || $sub_list_limit == ''){ $sub_list_limit = NM_SUB_LIST_LIMIT; }

if($sub_list_url != $_SERVER['REQUEST_URI']){ $sub_list_limit = NM_SUB_LIST_LIMIT; }

/* DB */
// 전체 월1 화2 수3 목4 금5 토6 일7
$sql_cm_public_cycle_in = "";

if(!($_menu == 'all' || $_menu == '')){
	$cmserial_cycle_in_arr = get_comics_cycle_fix($_menu);
	$sql_cm_public_cycle_in = "AND c.cm_public_cycle IN(".$cmserial_cycle_in_arr.") ";
}else{
	$_menu = 'all';
}

// 오늘 요일 표기 하기 위한...
$cm_public_cycle_in = get_comics_cycle_fix(week_type(7));

$cmserial_arr = array();
$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');

// 총갯수
$sql_cmserial_total = " SELECT count(*) as total FROM comics c WHERE 1 $sql_cm_adult $sql_cm_public_cycle_in and c.cm_service = 'y'; ";
$row_total = sql_count($sql_cmserial_total, 'total');

// 익스플로러일때 전체 출력-임시
if(substr(get_brow(HTTP_USER_AGENT), 0, 4) == "MSIE"){ $sub_list_limit = $row_total; }

// cm_reg_date_new 아직 사용 안하지만 나중에 사용할수 있음

// 특정 만화 순서 앞으로...18-04-25
$week_comicno_arr = array();
$week_comicno_arr[1] = "2024,2323,1241";
$week_comicno_arr[2] = "1979,2376,2773";
$week_comicno_arr[3] = "1252,2382";
$week_comicno_arr[4] = "2659,2151";
$week_comicno_arr[5] = "2207,2718";
$week_comicno_arr[6] = "2390,2755,2599";
$week_comicno_arr[7] = "978,2660,2678";

if(!($_menu == 'all' || $_menu == '')){
	$week_comicno_order = $week_comicno_arr[$_menu];
}else{
	$week_comicno_order = $week_comicno_arr[week_type(7)];
}

$sql_week_comicno_field = "";
$sql_week_comicno_order = "";

if($week_comicno_order != ""){
	$week_comicno_order_arr = explode(",", $week_comicno_order);

	$sql_week_comicno_field = " CASE ";
	foreach($week_comicno_order_arr as $week_comicno_order_key => $week_comicno_order_val){
		$sql_then = 99 - intval($week_comicno_order_key);
		$sql_week_comicno_field.= " WHEN c.cm_no = '".$week_comicno_order_val."' THEN ".$sql_then." ";
	}
	$sql_week_comicno_field.= "ELSE 0 ";
	$sql_week_comicno_field.= "END AS cm_no_special, ";

	$sql_week_comicno_order = "cm_no_special DESC, ";
}


$sql_cmserial = " SELECT c.*, c_prof.cp_name as prof_name, 
					".$sql_week_comicno_field."
					if(c.cm_public_cycle IN(".$cm_public_cycle_in.")=1,
						if(left(c.cm_episode_date,10)>='".NM_TIME_M1."',1,0),0
					)as cm_public_cycle_ck, 
					if(left(c.cm_reg_date,10)>='".NM_TIME_M1."',c.cm_reg_date,0)as cm_reg_date_new, 
					if(left(c.cm_episode_date,10)>='".NM_TIME_M1."',1,0)as cm_episode_date_up 
				FROM comics c 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 $sql_cm_adult $sql_cm_public_cycle_in 
				and c.cm_service = 'y' and c.cm_end='n' 
				ORDER BY ".$sql_week_comicno_order."
				cm_public_cycle_ck DESC, cm_episode_date_up DESC, cm_reg_date DESC, cm_episode_date DESC 
				LIMIT 0, ".$sub_list_limit."; ";

// 리스트 가져오기
$cmserial_arr =cs_comics_content_list($sql_cmserial, substr($nm_config['nm_path'], -1));

$sub_list_bool = "false";
if(count($cmserial_arr) < NM_SUB_LIST_LIMIT){ $sub_list_bool = "true"; }

/* view */

include_once($nm_config['nm_path']."/cmserial.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>