$(function(){
	var ready = false;
	if(ready == false){ ready = empty_image_ready(ready); }

	$("#comicsviewpage .comic .bg").click(function(event){ // 흑백배경
		empty_image_ready_end();
	});
	$("#comicsviewpage .comic_viewer .toggle_menu").click(function(event){ // 메뉴영역
		event.stopPropagation();
		menuToggle();
	});
	$("#comicsviewpage .prev_page").click(function(event){ // 이전페이지
		event.stopPropagation();
		menu_hide();
		prev_image();
	});
	$("#comicsviewpage .next_page").click(function(event){ // 다음페이지
		event.stopPropagation();
		menu_hide();
		next_image();
	});

	$("#comicsviewpage .sliding_bar").change(function(){
		menu_hide();
		var page = $('#sliding_bar').val();
		var cm_page = cm_page_way_page(page);

		if(ck_view_page == ''){
			if(cm_page < 2){cm_page = 0;}
			else if(cm_page % 2 == 1){ cm_page+= 1;	}
		}
		movepage(cm_page)
	});
	$("#comicsviewpage .comic_viewer").on("swipeleft", function(){
		//왼쪽 방향으로 수평 드래그
		menu_hide();
		if(cm_page_way == 'l'){ prev_image();
		}else{ next_image(); }
	});
	$("#comicsviewpage .comic_viewer").on("swiperight", function(){
		//오른쪽 방향으로 수평 드래그
		menu_hide();
		if(cm_page_way == 'l'){ next_image();
		}else{ prev_image(); }
	});

	/* 즐겨찾기 들어갈 때 유무 처리 */
	if($("#mybookmark").data("mybookmark") != 0) {
		var bookmark_star = $("#mybookmark").find("i");

		bookmark_star.removeClass("fa-star-o");
		bookmark_star.addClass("fa-star");
	} // end if

	/* 키보드 */
	$(window).on( "keydown", function() {
		var keycode = event.keyCode;
		if(cm_page_way == 'r'){
			if(keycode == 37){ next_image(); }
			else if(keycode == 39){ prev_image(); }
		}else{
			if(keycode == 39){ next_image(); }
			else if(keycode == 37){ prev_image(); }
		}
	});
	var mousewheel_count = 0;
	$(window).on('mousewheel DOMMouseScroll', function(e){
		var delta = e.originalEvent.wheelDelta || e.originalEvent.detail * -1;
		if (delta >= 0) { mousewheel_count++; }
		else{ mousewheel_count--; }

		if(mousewheel_count % 3 == 0 && mousewheel_count != 0){
			if(mousewheel_count > 2){ prev_image(); }
			if(mousewheel_count < -2){ next_image(); }
			mousewheel_count = 0;
		}
	});

	/* 랜덤박스 하단 뷰어 */
	$("#viewer_footer .viewer_event_wrap .viewer_event_close").click(function(){
		$("#viewer_footer .viewer_event_wrap").hide();
		$("#viewer_footer").css("height","50px");
		$(".comic_page_controller").css("bottom","50px");
	});
});

function inviewer_load(){
	var list_on_height = 0;
	$(".inviewer_list ol li").each(function( idx, val ){
		if($(this).hasClass('list_on')== true){
		list_on_height = idx * 37;
		}
	});
	$(".inviewer_list").animate({scrollTop:list_on_height}, 0);
}

function direction_view(){
	$('#comicsviewpage .direction img').css({ "position" : "absolute", "left" : ($(window).width() - $('.direction img').width()) / 2, "top" : ($(window).height() - $('.direction img').height()) / 2 }).load(function(){
		$(this).css({ "left" : ($(window).width() - $(this).width()) / 2, "top" : ($(window).height() - $(this).height()) / 2 });
	});
	$('#comicsviewpage .direction').show();
}

function direction_hide(){
	$('#comicsviewpage .direction').fadeOut();
}

function menu_hide(){
	if ($("#comicsviewpage #viewer_header").is(":visible")){
		$("#comicsviewpage #viewer_header").slideUp();
		$("#comicsviewpage #viewer_footer").slideUp();
		$("#comicsviewpage #comic_page_controller").slideUp();
		$(".inviewer_list").hide('');
	}
}

function menu_show(){
	if ($("#comicsviewpage #viewer_header").is(":hidden")){
		$("#comicsviewpage #viewer_header").slideDown();
		$("#comicsviewpage #viewer_footer").slideDown();
		$("#comicsviewpage #comic_page_controller").slideDown();
	}
}

function menuToggle(){
	if ($("#comicsviewpage #viewer_header").is(":hidden")){
		menu_show();
	} else{
		menu_hide();
	}
}

function empty_image_ready(){
	direction_view();
	empty_image_ready_end();
	// 이미지 로드 완료시 호출 되는 콜백 함수 -> 사파리에서 에러
	/*
	var $episode_list_img = $('#comicsviewpage .empty_image ul li img');
	var episode_list_img_count = $episode_list_img.length;
	$episode_list_img.load(function(){
		episode_list_img_count--;
		if(episode_list_img_count <= 0){
			empty_image_ready_end();
		}
	});
	*/
	return true;
}

function empty_image_ready_end(){
	$('#comicsviewpage .direction').delay(1000).fadeOut(function(){
		menu_hide();
	});
}

function prev_image(){
	var $episode_list = $('.empty_image ul li');
	var active2 = active = 0;
	$episode_list.each(function(idx, val) {
		if($(this).hasClass('active')){
			active = idx;
		}
		if($(this).hasClass('active2')){
			active2 = idx;
		}
	});
	active--;
	active2--;
	if(ck_view_page == ''){ // 2장
		if(active == 0 && active2 == 1 ){ active = 0;} // 홀수로 바낄 경우
		else{
			if(active < active2){ active--; }
		}
	}

	if(active < 0){
		if(episode_prev == ''){
			toast('처음 화 입니다.');
		}else{
			toast('이전 화를 불러오는 중입니다.');
			episode_goto_url(episode_prev, episode_prev_pay, episode_prev_txt);
		}
	}else{
		movepage(active);
	}

}

function next_image(){
	var $episode_list = $('.empty_image ul li');
	var active2 = active = 0;
	$episode_list.each(function(idx, val) {
		if($(this).hasClass('active')){
			active = idx;
		}
		if($(this).hasClass('active2')){
			active2 = idx;
		}
	});
	active++;
	active2++;
	
	if(ck_view_page == ''){ 
		if(active < active2){ active = active2; } 
	} // 2장

	if(active > page_slider_count){
		if(episode_next == ''){
			toast('마지막 화 입니다.');
		}else{
			toast('다음 화를 불러오는 중입니다.');
			episode_goto_url(episode_next, episode_next_pay, episode_next_txt);
		}
	}else{
		movepage(active);
	}
}


function episode_goto_url(episode_no, episode_pay, episode_txt){
	episode_url = nm_comicsview+"?comics="+comics+"&episode="+episode_no;

	if(Number(episode_pay) > 0){
		txt = episode_txt+"를 보시려면 결제가 필요합니다.<br/>";
		txt+= episode_pay+nm_cash_point_unit_ko+"을 사용하여 결제하시겠습니까?<br/>";
		txt+= "구매한 작품은 내 서재에 보관 됩니다.";
		confirmBox(txt, goto_replace_url, {url:episode_url});
	}else{
		document.location.replace(episode_url);
	}
}

function cm_page_way_page(page){
	var cm_page = 0;
	if(cm_page_way == 'l'){
		cm_page = Number(page);
	}else{
		cm_page = Number(page_slider_count) - Number(page);
	}
	return cm_page;
}

function movepage(page){
	if(page == ''){page = 0;}
	var page_next_count = 0;
	var $episode_list = $('.empty_image ul li');
	var data_img_src_chk = $episode_list.eq(page).find('img').attr('data_img_src_chk');
	var data_img_src = $episode_list.eq(page).find('img').attr('data_img_src');
	var data_img_alt = $episode_list.eq(page).find('img').attr('alt');
	
	var data_img_src_chk2 = $episode_list.eq(page+1).find('img').attr('data_img_src_chk');
	var data_img_src2 = $episode_list.eq(page+1).find('img').attr('data_img_src');
	var data_img_alt2 = $episode_list.eq(page+1).find('img').attr('alt');


	var $comic_viewer_img_first = $('.comic_viewer .cut img.first');
	var $comic_viewer_img_second = $('.comic_viewer .cut img.second');

	if(ck_view_page == ''){
		if(typeof data_img_src_chk2 == "undefined"){
			$comic_viewer_img_second.css('visibility','hidden');
		}else{
			$comic_viewer_img_second.css('visibility','visible');
		}
	}
	// 다음꺼
	var data_img_src_chk_next =  data_img_src_next = "";
	var page_next = page;

	// 해당 페이지 active 수정
	$episode_list.removeClass('active');
	$episode_list.removeClass('active2');

	$episode_list.eq(page).addClass('active');
	$episode_list.eq(page+1).addClass('active2');

	// 해당 페이지 src 수정
	if(data_img_src_chk != 'success'){
		$episode_list.eq(page).find('img').attr('src',data_img_src);
		$episode_list.eq(page).find('img').attr('data_img_src_chk','success');
	}
	if(data_img_src_chk2 != 'success'){
		$episode_list.eq(page+1).find('img').attr('src',data_img_src2);
		$episode_list.eq(page+1).find('img').attr('data_img_src_chk','success');
	}

	// 해당 페이지 표기 & 바이동
	page_sliding_bar(page);

	//view 적용
	$comic_viewer_img_first.attr('src',data_img_src);
	$comic_viewer_img_first.attr('alt',data_img_alt);
	$comic_viewer_img_second.attr('src',data_img_src2);
	$comic_viewer_img_second.attr('alt',data_img_alt2);

	for(page_next_count ; page_next_count < 4 && page_next < page_slider_count; page_next_count++ ){
		page_next++; // 다음꺼
		data_img_src_chk_next = $episode_list.eq(page_next).find('img').attr('data_img_src_chk');
		data_img_src_chk_next2 = $episode_list.eq(page_next+1).find('img').attr('data_img_src_chk');
		if(data_img_src_chk_next != 'success'){
			data_img_src_next = $episode_list.eq(page_next).find('img').attr('data_img_src');
			$episode_list.eq(page_next).find('img').attr('src',data_img_src_next);
			$episode_list.eq(page_next).find('img').attr('data_img_src_chk','success');
		}
		if(data_img_src_chk_next2 != 'success' && ck_view_page == ''){
			data_img_src_next2 = $episode_list.eq(page_next+1).find('img').attr('data_img_src');
			$episode_list.eq(page_next+1).find('img').attr('src',data_img_src_next2);
			$episode_list.eq(page_next+1).find('img').attr('data_img_src_chk','success');
		}
	}
}

function page_sliding_bar(page){
	var page_plus = 1;
	if(ck_view_page == ''){ page_plus = 2;}

	var page_num_start = Number(page)+page_plus;
	var page_num_end = Number(page_slider_count)+page_plus;
	$('#page_num').text(page_num_start + ' / ' +page_num_end);
	// 바이동
	var cm_page = cm_page_way_page(page);
	$('#sliding_bar').val(cm_page);
}

/* set_bookmark */
function set_bookmark(comics){
	var bookmark_star = $("#mybookmark").find("i");

	$.ajax({
		url: nm_url+"/ajax/mybookmark.php",
		dataType: "json",
		type: "POST",
		data: {"cm_no": comics},
		success : function(data) {
			if(data.state == 0){
				if(data.mode == 'in') {
					bookmark_star.removeClass("fa-star-o");
					bookmark_star.addClass("fa-star");
				} else {
					bookmark_star.removeClass("fa-star");
					bookmark_star.addClass("fa-star-o");
				} // end else
				toast(data.msg);
			}else{
				alertBox(data.msg);
			}
		}
	});
} // set_bookmark

function bmk_list(){

	var viewerlist_stats = $(".inviewer_list").css("display");
	/* none, block */
	if(viewerlist_stats == "none"){
		$(".inviewer_list").slideDown('fast');
	}else{
		$(".inviewer_list").slideUp('fast');
	}
	inviewer_load();
}

function set_view_page(){
	var $episode_list = $('.empty_image ul li');
	var $episode_list_active = $('.empty_image ul li.active');
	var $comic_viewer_img_second = $('.comic_viewer .cut img.second');
	var data_img_src2 = "";

	if(ck_view_page == ''){
		ck_view_page = 1;
		$episode_list.removeClass('active2');
		$comic_viewer_img_second.hide();
		$('#view_page i').removeClass('fa-window-maximize').addClass('fa-columns');
	}else{
		ck_view_page = '';
		$episode_list.removeClass('active2');
		data_img_src2 = $episode_list_active.next().addClass('active2').attr('src');
		$comic_viewer_img_second.attr('src',data_img_src2);
		$comic_viewer_img_second.show();
		$('#view_page i').removeClass('fa-columns').addClass('fa-window-maximize');
	}
	$.cookie("ck_view_page", ck_view_page);
	page_sliding_bar($episode_list_active.index());
}

/* full screen */
function fullscreen() {
    var browser = document.documentElement;
    var isbrowser = (document.fullScreenElement && document.fullScreenElement !== null) ||  (document.mozFullScreen || document.webkitIsFullScreen);

    if (isbrowser) {
        fullscreen_cancel(document);
    } else {
        fullscreen_display(browser);
    }
    return false;
}

function fullscreen_cancel(browser) {
    var browser_method = browser.cancelFullScreen|| browser.webkitCancelFullScreen || browser.mozCancelFullScreen || browser.msCancelFullScreen || browser.exitFullscreen;
    if (browser_method) {
        browser_method.call(browser);
		$('#fullscreen i').removeClass('fa-compress').addClass('fa-expand');
    }
}

function fullscreen_display(browser) {
    // Supports most browsers and their versions.
    var browser_method = browser.requestFullScreen || browser.webkitRequestFullScreen || browser.mozRequestFullScreen || browser.msRequestFullScreen;

    if (browser_method) {
        browser_method.call(browser);
		$('#fullscreen i').removeClass('fa-expand').addClass('fa-compress');
    }
    return false;
}
// full screen
