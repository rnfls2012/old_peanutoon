/* CS_FAQ(Accordian menu) */
$(function() {

	var faqPanels = $('.cs_faq > dd');
	var qnaPanels = $('.cs_ask > dd');
	
	/* 자주하는 질문 */
	$('.cs_faq > dt').click(function() {
		a_click_false();
		var current_chk = $(this).hasClass('current')
		if(current_chk == false){
			faqPanels.slideUp('fast');
			$(".cs_faq > dt").removeClass('current');
			$(this).next().slideDown('fast');
			$(this).addClass('current');
		} else {
			$(this).removeClass('current');
			$(this).next().slideUp('fast');
		} // end else
	});

	/* 1:1 문의 */
	$('.cs_ask > dt').click(function() {
		a_click_false();
		var current_chk = $(this).hasClass('current')
		var	this_down = $(this).find('i');
		var dt_down = $(".cs_ask > dt").find('i');

		if(current_chk == false){
			qnaPanels.slideUp('fast');
			dt_down.removeClass('fa-chevron-circle-up');
			dt_down.addClass('fa-chevron-circle-down')
			$(".cs_ask > dt").removeClass('current');
			this_down.removeClass('fa-chevron-circle-down');
			this_down.addClass('fa-chevron-circle-up')
			$(this).next().slideDown('fast');
			$(this).addClass('current');

		}else if(current_chk == true){
			$(this).removeClass('current');
			$(this).next().slideUp('fast');
			dt_down.removeClass('fa-chevron-circle-up');
			dt_down.addClass('fa-chevron-circle-down');
		}
	});

	return false;
});

/* 1:1문의 작성 */
function csaskwrite_submit() {
	if($("#ba_category").val() == "") {
		alertBox("분류를 선택해주세요!");
	} // end if

	if($("#qnaw_title").val() == "") {
		alertBox("제목을 입력해주세요!");
		return false;
	} // end if

	if($("#qnaw_request").val() == "") {
		alertBox("내용을 입력해주세요!");
		return false;
	} // end if
}

/* 연재문의 */
function cspublishwrite_submit() {
	
	//이메일 형식 체크 함수
	function validateEmail(email_val) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@(([a-zA-Z]+\.)+[a-zA-Z]{2,})$/;
        return re.test(email_val);
  }
  
  var email = $("#bp_return").val();
  
	if($("#bp_title").val() == "") {
		alertBox("제목을 입력해주세요!");
		return false;
	} // end if

	if($("#bp_text").val() == "") {
		alertBox("내용을 입력해주세요!");
		return false;
	} // end if

	if ($("#bp_return").val() === "") {
      alertBox("메일주소를 입력해주세요!");
      return false;
  }

  if (!validateEmail(email)) {
      alertBoxFocus("메일형식을 확인해주세요!", $("#bp_return"));
      return false;
  }
  
	if($("#bp_file").val() == "") {
		alertBox("파일(.zip)을 첨부해주세요!");
		return false;
	} // end if
}