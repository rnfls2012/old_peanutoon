$(function(){	
	if($("#search_txt").length > 0){
		$( "#search_txt" ).autocomplete({
			source : function( request, response ) {
				 $.ajax({
						type: 'POST',
						url: nm_url+"/ajax/search.php",
						dataType: "json",
						data: { search_txt: request.term },
						success: function(data) {
							if(data == null){
								data = {}
							}
							response( 
								$.map(data, function(item) {
									return {
										value: item.value,
										editor: item.editor,
										link: item.link,
										img: item.img,
										label: item.label
									}
								})
							);
						}
				   });
				},
			minLength: 1,
			focus: function( event, ui ) {
				return false;
			},
			select: function( event, ui ) {
			},
			open: function(){
			},
			close: function(){
				disable=false; $(this).focus();
			}
		}).data("uiAutocomplete")._renderItem = function(ul, item){
			var search_html_arr = '<a href="'+item.link+'" >';
			search_html_arr+= '<img src="'+item.img+'" alt="'+item.label+'" />';
			search_html_arr+= '<span><strong>';
			search_html_arr+= item.label+'</strong><br/>'
			search_html_arr+= item.editor
			search_html_arr+= '</span>';
			search_html_arr+= '</a>';
			return $("<li>").append(search_html_arr).appendTo(ul);
		}

		$.fn.selectRange = function(start, end) {
			return this.each(function() {
				if(this.setSelectionRange) {
					 this.focus();
					 this.setSelectionRange(start, end);
				} else if(this.createTextRange) {
					 var range = this.createTextRange();
					 range.collapse(true);
					 range.moveEnd('character', end);
					 range.moveStart('character', start);
					 range.select();
				}
			});
		};
	}

	// app_push
	$('#push_set').click(function(){
		$push_set = $('#push_set');
		push=$(this).children('input').val();
		$.ajax({
			url:nm_url+'/ajax/app_push.php'
		}).success(function(data){
			if(data=='ON'){
				$('#push_set').removeClass('off').addClass('on');
				$('#push_set').find('i').removeClass('fa-bell-slash').addClass('fa-bell');
			}else{
				$('#push_set').removeClass('on').addClass('off');
				$('#push_set').find('i').removeClass('fa-bell').addClass('fa-bell-slash');
			}
		});
	});

	/* header */
	$("#header .usermenu .usermenu_tabs input[name=usermenutab]").click(function(){
		var tab_name = $(this).val();
		$('#header .usermenu_contents div').removeClass('on');
		$('#header .'+tab_name).addClass('on');
	});
	
	$("#container").click(function(){
		var usermenu_stats = $(".usermenu").css('display');
		/* none, block */
		if(usermenu_stats != "none"){
			$(".usermenu").slideUp('fast');
		}
	});
	

		/* Registration Check Form */
	$('#regi_agree').click(function(e){
		var agree_checked = '';
		agree_checked = $(this).is(":checked");
			$('#regi_terms').prop("checked", agree_checked);
			$('#regi_pinfo').prop("checked", agree_checked);
			$('#regi_event').prop("checked", agree_checked);
	});

	$('#regi_terms, #regi_pinfo, #regi_event').click(function(e){
		var terms_checked = '';
		terms_checked = $(this).is(":checked");
		if(terms_checked == false){
			$('#regi_agree').prop("checked", false);
		}

		if($('#regi_terms').is(":checked") && $('#regi_pinfo').is(":checked") && $('#regi_event').is(":checked")) {
			$('#regi_agree').prop("checked", true);
		} else {
			$('#regi_agree').prop("checked", false);
		} // end else
	});

});

/* common */
function app_device_token(){
	var token = '';
	if( /Android/i.test(navigator.userAgent)) {
		window.megacomics.Token('token');
	} else {
		window.webkit.messageHandlers.Token.postMessage('token');
	}
}

function token(text){
	$.ajax({
		url:nm_url+'/ajax/app_token.php',
		type:'post',
		data:{
			token:text
		}
	})
}

/*
// 새로고침했을때 맨 위로 이동
window.addEventListener('load',function(){
	setTimeout(scrollTo, 0, 1, 1);
}, false);

if (navigator.userAgent.indexOf('iPhone') != -1) {
	addEventListener("load", function() {
		setTimeout(hideURLbar, 0);
	}, false);
}
// 위에껀 아이폰
// 요 아래껀 다른폰
*/

function hideURLbar() {
	window.addEventListener('load', function() {
		setTimeout(scrollTo, 0, 0, 1);
	}, false);
}

function link(url) {
	window.document.url = url;
	location.href = window.document.url;
}

/* a태그 #책받임 기능 막기 */
function a_click_false(){
	event.preventDefault(); //FF
	event.returnValue = false; //IE
}

// 이메일형식 검사
function email_chk(email)
{
	var email_check = true;
    if (!email){ 
		email_check = false;
	}else{
		var pattern = /([0-9a-zA-Z_-]+)@([0-9a-zA-Z_-]+)\.([0-9a-zA-Z_-]+)/;
		if (!pattern.test(email)) {
			email_check = false;
		}
	}
	return email_check;
}

// 정수만 추출
function getNumberOnly(str)
{
    var res;
    res = str.replace(/[^0-9\-]/g,"");
    return res;
}

// alertBox 이용함수
function goto_url(obj){
	location.href = obj.url;
}
// alertBox 이용함수
function goto_replace_url(obj){
	document.location.replace(obj.url);
}

function mode_chage(mode){
	if(mode == 'pc'){
		$.cookie("ck_default_mode", 'pc');
	}else{
		$.cookie("ck_default_mode", 'mobile');
	}
	location.reload();
}
/* //////////////////////// 모달팝업 //////////////////////// */

function alertBox(txt, callbackMethod, jsonData){
	var ua = window.navigator.userAgent;
	ua_lower = ua.toLowerCase();
	if(ua_lower.search("safari")!= -1 && ua_lower.search("chrome")== -1){
		alert(txt);
		if(callbackMethod){ callbackMethod(jsonData); }
	}else{
		modal({
			type: 'alert',
			title: '알림',
			text: txt,
			callback: function(result){
				if(callbackMethod){ callbackMethod(jsonData); }
			}
		});
	}
}
 
function alertBoxFocus(txt, obj){
    modal({
        type: 'alert',
        title: '알림',
        text: txt,
        callback: function(result){
            obj.focus();
        }
    });
}


function confirmBox(txt, cb){
    modal({
        type: 'confirm',
        title: '알림',
        text: txt,
        callback: function(result) {
            if(result){
				cb;
            }
        }
    });
} 
    
function confirmBoxYN(txt, form){
    modal({
        type: 'confirm',
        title: '알림',
        text: txt,
        callback: function(result) {
			if(result == true){
				form.submit();
			}
        }
    });
}

function confirmBox(txt, callbackMethod, jsonData){
    modal({
        type: 'confirm',
        title: '알림',
        text: txt,
        callback: function(result) {
            if(result){
                callbackMethod(jsonData);
            }
        }
    });
}

function confirmBoxUrl(txt, callbackMethod, jsonData_true, jsonData_false){
    modal({
        type: 'confirm',
        title: '알림',
        text: txt,
        callback: function(result) {
            if(result){
                callbackMethod(jsonData_true);
            }else{
                callbackMethod(jsonData_false);
			}
        }
    });
}
 
 
function promptBox(txt, callbackMethod, jsonData){
    modal({
        type: 'prompt',
        title: 'Prompt',
        text: txt,
        callback: function(result) {
            if(result){
                callbackMethod(jsonData);
            }
        }
    });
}

/* confirmview: "View" add 17-09-26 */ 
function confirmView(txt, callbackMethod, jsonData, jsonView){
    modal({
        type: 'confirmview',
        title: '알림',
        text: txt,
        callback: function(result) {
            if(result=='view'){
				callbackMethod(jsonView);
            }else if(result){
                callbackMethod(jsonData);
			}
        }
    });
}

/* confirmjoin: "Join" add 18-04-18 */ 
function confirmJoin_imgload(title, txt){
	
	var title_imgload = txt_imgload = confirmJoin_imgload_bool = false;
	var width_img = '';
	var title_img = '<img id="confirmjoin_title_img" src="'+title+'" alt="회원가입/로그인팝업-title" '+width_img+'>';
	var txt_img = '<img id="confirmjoin_txt_img" src="'+txt+'" alt="회원가입/로그인팝업-txt" '+width_img+'>';
	
	$('body').append('<div class="hide" style="display:none;">'+title_img+txt_img+'</div>');
}
 
/* confirmjoin: "Join" add 18-04-18 */ 
function confirmJoin(title, txt){

	callbackMethod = goto_replace_url;

	var join_url = nm_url+'/ctjoin.php';
	var login_url = nm_url+'/ctlogin.php';
	jsonJoin = {url:join_url};
	jsonData = {url:login_url};

	var width_img = '';
	var title_img = '';
	var txt_img = '';

	// 무조건 이미지로...
	width_img = ' width="100%" ';
	title_img = '<img id="confirmjoin_title_img" src="'+title+'" alt="회원가입/로그인팝업-title" '+width_img+'>';
	txt_img = '<img id="confirmjoin_txt_img" src="'+txt+'" alt="회원가입/로그인팝업-txt" '+width_img+'>';
	
	modal({
		type: 'confirmjoin',
		title: title_img,
		text: txt_img,
		callback: function(result) {
			if(result=='join'){
				callbackMethod(jsonJoin);
			}else if(result){
				callbackMethod(jsonData);
			}
		}
	});
}

function successBox(txt){
    modal({
        type: 'success',
        title: 'Success',
        text: txt
    });
}
 
function warningBox(txt){
    modal({
        type: 'warning',
        title: 'Warning',
        text: txt,
        center: false
    });
}
 
function infoBox(txt){
    modal({
        type: 'info',
        title: 'Info',
        text: txt,
        autoclose: true
    });
}
 
function errorBox(txt){
    modal({
        type: 'error',
        title: 'Error',
        text: txt
    });
}
 
function invertedBox(txt){
    modal({
        type: 'inverted',
        title: 'Inverted',
        text: txt
    });
}
 
function primaryBox(txt){
    modal({
        type: 'primary',
        title: 'Primary',
        text: txt
    });
}

/* //////////////////////// 모달팝업 end //////////////////////// */

/* header */

	//alert
	function alert_win(){
		user_menu_hide();
		comice_search_hide();
		a_click_false();
	}

	// search
	function comice_search(){
		var search_state = $('.search img').hasClass('off');
		if(search_state == true){
			comice_search_hide();
		}else{
			comice_search_show();
		}
		a_click_false();
	}

	function comice_search_show(){
		$('#search_list').show();
		$('.search img').addClass('off');
		var data_img = $('.search img').attr('data_img_off');
		$('.search img').attr('src',data_img);
		$('#alert_win').addClass('mgt50');
		$('#alert_win').fadeIn(300);

		$('#search_txt').focus();
		if($('.ui-autocomplete li').length > 0){ $('.ui-autocomplete').show(); }
	}
	function comice_search_hide(){
		$('#search_list').hide();
		$('.search img').removeClass('off');
		var data_img = $('.search img').attr('data_img_on');
		$('.search img').attr('src',data_img);
		$('#alert_win').hide();
		$('#alert_win').removeClass('mgt50');
		$('.ui-autocomplete').hide();
	}

	// user_menu
	function user_menu(){
		var hide_check = Number(getNumberOnly($('#user_menu').css('right')));
		if(hide_check < 0){
			user_menu_show();
		}else{
			user_menu_hide();
		}
		a_click_false();
	}

	function user_menu_show(){
		comice_search_hide();
		$('#user_menu').animate({right: '0%'}, 300, 'easeInQuad');
		$('#alert_win').fadeIn(300);
	}

	function user_menu_hide(){
		$('#user_menu').animate({right: '-100%'}, 300, 'easeOutQuad');
		$('#alert_win').fadeOut(300);
	}

	var onToast;
	function toast(msg)
	{
		if (onToast)
		{
			onToast.remove();
			onToast = null;
		}
		onToast = $("<div class='pop_toast'>" + msg + "</div>").appendTo("body");
		onToast.css({ "left" : ($(window).width() - onToast.width()) / 2, "top" : $(window).height() / 2 });
		onToast.delay(600).fadeOut(200, function()
		{
			$(this).remove();
			onToast = null;
		});
	}

	function goto_coupon(url){
		link(url);
		alert_win();
	}

/* header */
function usermenu(){
	$(".usermenu").slideToggle('fast');
}

/* login */
function ctlogin_submit(){
	var login_id = $('#login_id').val();
	var login_pw = $('#login_pw').val();

	/* 아이디 & 비밀번호 */
	if(login_id == "" || login_pw == "") {
		var empty_var = "login_pw";
		var msg = "비밀번호를 ";
		if(login_id == "") {	
			empty_var = "login_id"; 
			msg = "아이디를 ";
		} // end if
		
		alertBoxFocus(msg+"입력해 주세요!", $('#'+empty_var));
		return false;
	} // end if
}

function ctjoin_submit() {
	var regi_terms = $('#regi_terms').is(":checked");
	var regi_pinfo = $('#regi_pinfo').is(":checked");
	var regi_event = $('#regi_event').is(":checked");
	var join_id = $('#join_id').val();
	var join_pw = $('#join_pw').val();
	var email_validity = /^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/;

	/* 아이디 & 비밀번호 */
	if(join_id == "" || join_pw == "") {
		var empty_var = "join_pw";
		var msg = "비밀번호를 ";
		if(join_id == "") {	
			empty_var = "join_id"; 
			msg = "아이디를 ";
		} // end if
		
		alertBoxFocus(msg+"입력해 주세요!", $('#'+empty_var));
		return false;
	} // end if

	/* 비밀번호 4자 이상 */
	if(join_pw.length < 4) {
		alertBoxFocus("비밀번호는 4자 이상 입력해 주세요!", $('#join_pw'));
		$('#join_pw').val("");
		return false;
	} // end if

	/* 아이디 Email 형식(유효성 검사) */
	if(email_validity.test(join_id) == false) {
		alertBoxFocus("이메일 형식의 아이디를 입력해 주세요!", $('#join_id'));
		$('#join_id').val("");
		return false;
	} // end if
	
	/* 약관 */
	if(regi_terms == false || regi_pinfo == false) {
		alertBox("약관에 동의하여 주세요!.");
		return false;
	} // end if
} // ctjoin_submit

/* number_format 구현 */
Number.prototype.number_format = function(round_decimal) {
     return this.toFixed(round_decimal).replace(/(\d)(?=(\d{3})+$)/g, "$1,");
};

/* 팝업 */
function popup(url,frame,w,h) {
	var bwidth = 700;
	var bheight =700;
	if(w == 0 || w == "" || typeof w == "undefined"){ w = bwidth; }
	if(h == 0 || h== ""  || typeof h == "undefined"){ h = bheight; }
	if(frame== "" || typeof frame == "undefined"){ frame = "newpopup"; }
	var popup_win = window.open(url, frame, "left=0, top=0, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width="+w+", height="+h);
	popup_win.resizeTo(w, h);
	/* popup_win.moveTo(0, 0); */
	a_click_false();
}