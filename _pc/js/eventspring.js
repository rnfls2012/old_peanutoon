(function ($) {
    /** Polyfills and prerequisites **/

    // requestAnimationFrame Polyfill
    var lastTime    = 0;
    var vendors     = ['webkit', 'o', 'ms', 'moz', ''];
    var vendorCount = vendors.length;

    for (var x = 0; x < vendorCount && ! window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame  = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if ( ! window.requestAnimationFrame) {
        window.requestAnimationFrame = function(callback) {
            var currTime   = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));

            var id   = window.setTimeout(function() { callback(currTime + timeToCall); }, timeToCall);
            lastTime = currTime + timeToCall;

            return id;
        };
    }

    if ( ! window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
    }

    // Prefixed event check
    $.fn.prefixedEvent = function(type, callback) {
        for (var x = 0; x < vendorCount; ++x) {
            if ( ! vendors[x]) {
                type = type.toLowerCase();
            }

            el = (this instanceof jQuery ? this[0] : this);
            el.addEventListener(vendors[x] + type, callback, false);
        }

        return this;
    };

    // Test if element is in viewport
    function elementInViewport(el) {

        if (el instanceof jQuery) {
            el = el[0];
        }

        var rect = el.getBoundingClientRect();

        return (
            rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
    }

    // Random array element
    function randomArrayElem(arr) {
        return arr[Math.floor(Math.random() * arr.length)];
    }

    // Random integer
    function randomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /** Actual plugin code **/
    $.fn.sakura = function (event, options) {

        // Target element
        var target = this.selector == "" ? $('body') : this;

        // Defaults for the option object, which gets extended below
        var defaults = {
            blowAnimations: ['blow-soft-left', 'blow-medium-left', 'blow-soft-right', 'blow-medium-right'],
            className: 'sakura',
            fallSpeed: 1,
            maxSize: 14,
            minSize: 10,
            newOn: 300,
            swayAnimations: ['sway-0', 'sway-1', 'sway-2', 'sway-3', 'sway-4', 'sway-5', 'sway-6', 'sway-7', 'sway-8']
        };

        var options = $.extend({}, defaults, options);

        // Default or start event
        if (typeof event === 'undefined' || event === 'start') {

            // Set the overflow-x CSS property on the target element to prevent horizontal scrollbars
            target.css({ 'overflow-x': 'hidden' });

            // Function that inserts new petals into the document
            var petalCreator = function () {
                if (target.data('sakura-anim-id')) {
                    setTimeout(function () {
                        requestAnimationFrame(petalCreator);
                    }, options.newOn);
                }

                // Get one random animation of each type and randomize fall time of the petals
                var blowAnimation = randomArrayElem(options.blowAnimations);
                var swayAnimation = randomArrayElem(options.swayAnimations);
                var fallTime = ((document.documentElement.clientHeight * 0.007) + Math.round(Math.random() * 5)) * options.fallSpeed;

                // Build animation
                var animations =
                    'fall ' + fallTime + 's linear 0s 1' + ', ' +
                        blowAnimation + ' ' + (((fallTime > 30 ? fallTime : 30) - 20) + randomInt(0, 20)) + 's linear 0s infinite' + ', ' +
                        swayAnimation + ' ' + randomInt(2, 4) + 's linear 0s infinite';

                // Create petal and randomize size
                var petal  = $('<div class="' + options.className + '" />');
                var height = randomInt(options.minSize, options.maxSize);
                var width  = height - Math.floor(randomInt(0, options.minSize) / 3);

                // Apply Event Listener to remove petals that reach the bottom of the page
                petal.prefixedEvent('AnimationEnd', function () {
                    if ( ! elementInViewport(this)) {
                        $(this).remove();
                    }
                })
                // Apply Event Listener to remove petals that finish their horizontal float animation
                .prefixedEvent('AnimationIteration', function (ev) {
                    if (
                        (
                            $.inArray(ev.animationName, options.blowAnimations) != -1 ||
                            $.inArray(ev.animationName, options.swayAnimations) != -1
                        ) &&
                        ! elementInViewport(this)
                    ) {
                        $(this).remove();
                    }
                })
                .css({
                    '-webkit-animation': animations,
                    animation: animations,
                    height: height + 'px',
                    left: (Math.random() * document.documentElement.clientWidth - 100) + 'px',
                    'margin-top': (-(Math.floor(Math.random() * 20) + 15)) + 'px',
                    width: width + 'px'
                });

                target.append(petal);
            };

            // Finally: Start adding petals
            target.data('sakura-anim-id', requestAnimationFrame(petalCreator));

        }
        // Stop event, which stops the animation loop and removes all current blossoms
        else if (event === 'stop') {

            // Cancel animation
            var animId = target.data('sakura-anim-id');

            if (animId) {
                cancelAnimationFrame(animId);
                target.data('sakura-anim-id', null);
            }

            // Remove all current blossoms
            setTimeout(function() {
                $('.' + options.className).remove();
            }, (options.newOn + 50));

        }
    };
}(jQuery));

$(function(){
	$('.evt-container').sakura('start', {
        fallSpeed : 3,
        maxSize : 30,
        minSize : 15,
        newOn: 300
    });

    $('.Modalalert > .ModalBackground').click(function(){
        $('.Modalalert > .ModalBackground').hide();
        $('.Modalalert > .ModalPopup').hide();
        $('html, body').css({'overflow': 'auto'});
        $('.Modalalert > .ModalBackground').off('scroll touchmove mousewheel');
        $(this).removeAttr('style');
        $('.Modalalert > .ModalPopup').removeAttr('style');
		location.reload();
    });

    $('.Modalalert > .ModalPopup > .ModalClose').click(function(){
        $('.Modalalert > .ModalBackground').hide();
        $('.Modalalert > .ModalPopup').hide();
        $('html, body').css({'overflow': 'auto'});
        $('.Modalalert > .ModalBackground').off('scroll touchmove mousewheel');
        $('.Modalalert > .ModalBackground').removeAttr('style');
        $(this).removeAttr('style');
		location.reload();
    });

    function position_cm(obj) {
        var windowHeight = $(window).height();
        var topOfWindow = $(window).scrollTop()
        var $obj = $(obj);
        var objHeight = $obj.height();
        $obj.css ({
            'top':(windowHeight/2) - (objHeight/2) + topOfWindow
        });
        return this;
    };

    $("#open_btn").click(function(){
		var btn_activation = $(this).attr("class");

		// 버튼 활성유무에 따라 프로세스 진행
		if(btn_activation == "nochk") {
			if($("#member_no").val() == "") {
				alertBox("로그인 후 이용이 가능합니다!");
			} else if($("#total_coupon").val() <= 0){
				alertBox("남은 할인권이 없습니다. 내일 다시 도전하세요!");
			} else {
				alertBox("오늘자 할인권을 이미 뽑으셨습니다!");	
			}// end else

			return false;
		} else {
			// AJAX 처리
			$.ajax({
				url: nm_url+'/ajax/eventfullmoon.php',
				cache: false, 
				dataType : "json", 
				success: function(data) {
					if(data['result']) {
						var maskHeight = $(window).height();
						var topOfWindow = $(window).scrollTop();

						$(".coupon_value").text(data['ercm_discount_rate']);
						$(".coupon_value02").text(data['er_available_date_end']);
						$("#recharge_link").attr('onclick','link(nm_url+\'/recharge.php?mb_coupondc='+data['erc_no']+'\')');
						$('.Modalalert > .ModalBackground').show();
						$('.Modalalert > .ModalBackground').css({'height':maskHeight, 'top':topOfWindow});
						$('html, body').css({'overflow': 'hidden'});
						$('.Modalalert > .ModalBackground').on('scroll touchmove mousewheel', function(event) { // 터치무브와 마우스휠 스크롤 방지
							event.preventDefault();
							event.stopPropagation();
							return false;
						});
						position_cm($('.Modalalert > .ModalPopup'));
						$('.Modalalert > .ModalPopup').show();
					} else {
						alertBox(data['text']);
					} // end else
				} // end success
			}) // end ajax
		} // end else
    });
});


