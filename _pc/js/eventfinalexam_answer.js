$(document).ready(function() {

	$(".exp-header .q_paging ul li").click(function(event){
		var q_paging_idx = $(this).index();
		var exp_img_len = $(".exp-con .exp_img").length;

		var now_idx = 0;
		var show_idx = 0;
		var show_paging = 0;
		var show_paging_css = 0;
		
		var paging = false;
		var paging_start = false;
		var paging_end = false;

		$(".exp-con .exp_img").each( function( idx, val ) {
			console.log($(this).css('display'));
			if($(this).css('display') == 'block'){
				now_idx = idx+1;
			}
		});

		if(q_paging_idx == 0){
			show_paging = now_idx - 1;
			paging = true;
		}else if(q_paging_idx == 2){
			show_paging = now_idx + 1;
			paging = true;
		}

		if(show_paging < 2){ show_paging = 1; paging_start = true; }
		if(show_paging > 4){ show_paging = 5; paging_end = true; }

		show_idx = show_paging -1;

		if(paging == true){
			$(".exp-con .exp_img").hide();
			$(".exp-con .exp_img:eq("+show_idx+")").show();

			$(".exp-header .q_paging ul li:eq(1)").text(show_paging+'/'+exp_img_len);
			$(".exp-header .q_paging ul li").removeClass('off');
			if(paging_start == true){
				$(".exp-header .q_paging ul li:eq(0)").addClass('off');		
			}
			if(paging_end == true){
				$(".exp-header .q_paging ul li:eq(2)").addClass('off');					
			}
			
			$("html, body").animate({scrollTop: 0}, 0);
		}
    });
});