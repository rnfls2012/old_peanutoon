$(function(){
	/* 랜덤박스 이벤트 */
	$(".evt_box button").click(function(event){
		if($('input:radio[name="randombox"]:checked').val()) {
			
			$.ajax({
				url: nm_url+'/ajax/eventrandombox.php?',
				cache: false, 
				dataType : "json", 
				success: function(data) {
					if(data['result']) {
						var img_src = nm_img+"_pc/event/2017/randombox/evt04_pop_"+data['discount_rate']+".png";
						$("#dialog_img").attr("src", img_src);
						$("#dialog").dialog({
							width:476,
							height:445,
							modal:true,
							buttons: {
								"확인": function() {
									$( this ).dialog( "close" );
									$("#dialog_img").attr("src", "");
								}
							}
						}); // end dialog
					} else {
						alertBox(data['text']);
					} // end else
				} // end success
			}) // end ajax
		} else {
			alertBox("상자를 선택하세요!");
		} // end else
	});
});

function eventnaming_submit() {
	if($("input[name=squirrel_name]").val() == "") {
		alertBoxFocus("다람쥐의 이름을 지어주세요!", $("input[name=squirrel_name]"));
		return false;
	} else if($("input[name=peanut_name]").val() == "") {
		alertBoxFocus("큰 땅콩의 이름을 지어주세요!", $("input[name=peanut_name]"));
		return false;
	} else if($("input[name=mini_peanut_name]").val() == "") {
		alertBoxFocus("작은 땅콩의 이름을 지어주세요!", $("input[name=mini_peanut_name]"));
		return false;
	} // end else
} // eventnaming_submit

function eventattend_submit() {

}