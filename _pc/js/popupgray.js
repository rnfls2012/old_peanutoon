$(function(){	
	/* blackpopup */
	$('#blackpopup #blackpopup_bg').click(function(e){
		if($('#blackpopup').hasClass("hide") == false){
			blackpopup_hide();
		}
	});
});

/* blackpopup */
function blackpopup_view(){
	$('#blackpopup').removeClass('hide');
	$('html').css('overflow','hidden');
}

function blackpopup_hide(){
	$('#blackpopup').addClass('hide');
	$('html').css('overflow','scroll');
}