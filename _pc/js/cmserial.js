$(function(){

	/* 탭 고정 */
	var $lnb_sub = $(".comics_content_list .lnb_sub_bg .lnb_sub");
	var sub_list_top = Math.round(Number($lnb_sub.offset().top));							// 시작 위치	
	var container_top = $("#container").css('padding-top');									// 컨테이너 상단 높이	
	var sub_list_fixed = sub_list_top - Math.round(container_top.replace("px",""));			// 탭 고정 위치
	var sub_list_fixed = 417; // 위처럼 계산해야 하는데 가끔 sub_list_top 값이 14로 나오는 오작동으로 인해 고정값

	$lnb_sub.wrap('<div class="nav-placeholder"></div>');
	$(".nav-placeholder").height($lnb_sub.outerHeight());
	var scrolltop = 0;
	$(window).scroll(function(){		
		var ua = window.navigator.userAgent;
		if(ua.indexOf('Chrome') != -1 || ua.indexOf('Safari') != -1){
			//scrolltop = event.clientY+document.body.scrollTop;
			scrolltop = document.body.scrollTop;
		}
		else{
			//scrolltop = event.clientY+document.documentElement.scrollTop;
			scrolltop = document.documentElement.scrollTop;
		}

		if (scrolltop >= sub_list_fixed) {
			$lnb_sub.addClass("fixed");
		} else {
			$lnb_sub.removeClass("fixed");
		}
	});

	var $sub_list = $(".comics_content_list .sub_list_bg .sub_list");						// 코믹스 리스트
	var sub_list_url = $sub_list.attr('data-url');											// URL체크
	$.cookie("ck_sub_list_url", sub_list_url);												// URL저장
	setTimeout(isLoading_check, 1200);														// 1.2초 후 실행

	function isLoading_check(){
		var isLoading = false;																// isLoading 상태
		var $sub_list_img = "";
		var $sub_list_thumb = $(".comics_content_list .sub_list_bg");						// 시작위치 태그
		var $sub_list = $(".comics_content_list .sub_list_bg .sub_list");					// 코믹스 리스트
		var sub_list_menu = $sub_list.attr('data-menu');									// 메뉴

		var sub_list_limit = Number($sub_list.attr('data-limit'));							// 갯수
		var sub_list_limit_define = Number($sub_list.attr('data-limit_define'));			// 정해진 갯수
		var sub_list_total = Number($sub_list.attr('data-total'));							// 총갯수
		var sub_list_url = $sub_list.attr('data-url');										// URL체크
		var sub_list_bool = $sub_list.attr('data-bool');									// 코믹스 더 있는지 체크
		
		var sub_list_thumb_big_height = $sub_list.find(".sub_list_thumb_big").height();
		var sub_list_thumb_big_pdb10 = $sub_list.find(".sub_list_thumb_big").css('padding-bottom');
		var sub_list_piece_big = sub_list_thumb_big_height + Math.round(sub_list_thumb_big_pdb10.replace("px",""));
		
		var sub_list_thumb_small_height = 0;
		var sub_list_thumb_small_pdb10 = "0px";
		
		if($sub_list.find(".sub_list_thumb_small").length > 0){
			sub_list_thumb_small_height = $sub_list.find(".sub_list_thumb_small").height();
			sub_list_thumb_small_pdb10 = $sub_list.find(".sub_list_thumb_small").css('padding-bottom');
		}
		
		var sub_list_piece_small = sub_list_thumb_small_height + Math.round(sub_list_thumb_small_pdb10.replace("px",""));
		// 코믹스 1개의 높이

		var sub_list_top = Math.round(Number($sub_list_thumb.offset().top));				// 시작위치
		var load_height = calc_load_height(sub_list_top, sub_list_piece_big, sub_list_piece_small, sub_list_limit, sub_list_limit_define);	
		// 2줄 전 높이
		

		if(isLoading == false){																// false 로딩 끝
			if(sub_list_bool == "false"){													// false 코믹스 더 있음( 더 있음 )
				$.cookie("load_list_limit", sub_list_limit);								// 갯수저장
				$(window).scroll(function(){					
					this_top = Number($(document).scrollTop()) + Number($(window).height());	// 현재 위치 + 창크기
					this_top = Math.round(this_top);

					if (this_top > load_height){
						if(isLoading == false){
							isLoading = true;
							sub_list_add(sub_list_menu, sub_list_limit, sub_list_bool);
							sub_list_limit+=sub_list_limit_define;
							$.cookie("ck_load_list_limit", sub_list_limit);

							if(sub_list_limit < sub_list_total){
								isLoading = false;
								load_height = calc_load_height(sub_list_top, sub_list_piece_big, sub_list_piece_small, sub_list_limit, sub_list_limit_define);
							}
						}
					}

				});
			}
		}
	}
});

function calc_load_height(sub_list_top, sub_list_piece_big, sub_list_piece_small, sub_list_limit, sub_list_limit_define){	// 2줄 전 높이 계산
	
	var sub_list_sum_big = Math.round(sub_list_piece_big * 12 / 4) + sub_list_top;		
	// 코믹스 큰 그림 1 * 총갯수 (나누기) 한줄갯수
	var sub_list_sum_small_first = Math.round(sub_list_piece_small * (Number(sub_list_limit_define-12) / 8));		
	// 코믹스 작은 그림 1행 (60 - 12) / 8	
	var sub_list_sum_small = 0;

	if(sub_list_limit_define < sub_list_limit){														// 60개 이상일때
		sub_list_sum_small = Math.round(sub_list_piece_small * (sub_list_limit - sub_list_limit_define) / 8);
	}

	sub_list_sum = sub_list_sum_big + sub_list_sum_small_first + sub_list_sum_small;				// 합계

	// 코믹스 1 * 총갯수 (나누기) 한줄갯수
	var load_height = sub_list_sum - (sub_list_piece_small * 2);									// 2줄 전 높이	
	return load_height;
}

function sub_list_add(sub_list_menu, sub_list_limit, sub_list_bool){
	var html_arr = new Array();
	var result = new Array();
	var bool = false;
	var $sub_list = $(".comics_content_list .sub_list_bg .sub_list");									// 코믹스 리스트
	
	var sub_list_access = $.ajax({
		url: nm_url +"/ajax/cmserial.php",
		dataType: "json",
		type: "POST",
		data: { menu: sub_list_menu, limit: sub_list_limit, bool: sub_list_bool},
		beforeSend: function( data ) {
			//console.log('로딩');
		}
	})
	sub_list_access.done(function( data ) {
		var html_tag = '';
		$(data).each(function(idx){
			html_arr.push('<a href="'+data[idx]['cm_serial_url']+'" target="_self">');
				html_arr.push('<div class="sub_list_thumb sub_list_thumb_small">');

				html_arr.push('<div class="icon_cm_type">'+data[idx]['cm_type_icon']+'</div>');
				html_arr.push('<div class="icon_event">'+data[idx]['cm_free_icon']+data[idx]['cm_sale_icon']+'</div>');
				html_arr.push('<div class="icon_date">'+data[idx]['cm_up_icon']+data[idx]['cm_new_icon']+'</div>');
				html_arr.push('<div class="icon_adult">'+data[idx]['cm_adult_icon']+'</div>');

					html_arr.push('<dl>');
					html_arr.push('<dt><img src="'+data[idx]['cm_cover_sub']+'" alt="'+data[idx]['cm_series']+'" /></dt>');
						html_arr.push('<dt class="ellipsis">'+data[idx]['cm_series']+'</dt>');
						html_arr.push('<dd class="ellipsis small">'+data[idx]['small_txt']+'</dd>');
						html_arr.push('<dd class="ellipsis">'+data[idx]['cm_professional_info']+'</dd>');
					html_arr.push('</dl>');
				html_arr.push('</div>');
			html_arr.push('</a>');
		});
		$sub_list.append(html_arr.join("\n"));
	});
	sub_list_access.fail(function( data ) {
		//console.log('실패');
	});
	sub_list_access.always(function( data ) {
		//console.log('로딩삭제');
	});
}
