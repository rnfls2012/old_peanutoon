var ef_timer = 15*60;
var ef_time_id;

$(document).ready(function() {
	$(".question .ef_list > ul li").click(function(event){ // 흑백배경
		var ef_len = $(".question .ef_list").length;

		var ef_now = Number($(this).parents('ul').attr('data-ef-key'));
		var ef_next = ef_now+1;

		if(ef_len > ef_next){
			$(".question .ef_list").removeClass('hide');
			$(".question .ef_list").addClass('hide');
			$(".question .ef_list").eq(ef_next).removeClass('hide');
		}else{
			ef_question_submit();
		}
    });

	$(".question .ef_list .question-footer > ul li").click(function(event){ // 흑백배경
		var ef_len = $(".question .ef_list").length;

		var ef_now = Number($(this).parents('ul').attr('data-ef-key'));
		var ef_pre = ef_now-1;

		if(0 <= ef_pre){
			$(".question .ef_list").removeClass('hide');
			$(".question .ef_list").addClass('hide');
			$(".question .ef_list").eq(ef_pre).removeClass('hide');
		}
    });

	ef_time_id = setInterval(ef_times, 1000);
});

function ef_times(){
	if(ef_timer > 0){ ef_timer--; 
		ef_time_txt(ef_timer);	
	}else{
		clearInterval(ef_time_id);
		ef_question_submit();
	}
}

function ef_time_txt(ef_timer){
	var ef_min = Math.floor(ef_timer / 60);
	var ef_sec = ef_timer - (ef_min * 60);

	var ef_min_txt = String(ef_min);
	var ef_sec_txt = String(ef_sec);

	if(ef_min < 10){ef_min_txt = '0'+ef_min}
	if(ef_min == 0){ef_min_txt = '00'}

	if(ef_sec < 10){ef_sec_txt = '0'+ef_sec}

	var ef_time_txt = ef_min_txt+":"+ef_sec_txt;
	$(".q_time").html('<span>'+ef_time_txt+'</span>');
	$("#ef_time").val(ef_time_txt);
}


function ef_question_submit() {
	var ef_form = document.forms["ef_question_form"];
	ef_form.submit();
}
