<? include_once '_common.php'; // 공통 ?>
			</div><!-- container -->
			<? if($nm_config['footer'] == true){ ?>
			<div id="footer">
				<div class="footer_wrap">
					<div class="footer_info">
						<ul class="service">
							<li><a href="<?=NM_URL;?>/siteterms.php">서비스 이용약관</a></li>
							<li><a href="<?=NM_URL;?>/siteprivacy.php"><span>개인정보 처리방침</span></a></li>
							<li><a href="<?=NM_URL."/cscenter.php";?>">고객센터</a></li>
							<li><a href="<?=NM_URL."/cscenter.php";?>?menu=4">연재문의</a></li>
						</ul>
						<div class="company">
						<? // if(substr($_SERVER['PHP_SELF'],1) == "cscenter.php") { ?>
							<p><i class="fa fa-map-marker" aria-hidden="true"></i><?=$nm_config['cf_address'];?></p>
							<p><?=$nm_config['cf_company'];?> / 대표이사 : <?=$nm_config['cf_ceo'];?> / 사업자등록번호 : <?=$nm_config['cf_business_no'];?> / 통신판매업신고번호 : <?=$nm_config['cf_mail_order_sales'];?> </p>
							<p>대표번호 : <?=$nm_config['cf_tel'];?></p>
						<? // } // end if ?>
							<span><?=$nm_config['cf_copy'];?></span>
						</div>
						<? // if(substr($_SERVER['PHP_SELF'],1) == "cscenter.php") { ?>
						<div class="okmark">
							<a href="http://www.copyrightok.kr" target="_blank">
								<img src="<?=NM_IMG?>common/okmark.png<?=vs_para();?>" alt="저작권 OK" />
							</a>
						</div>
						<? // } // end if ?>
					</div>
				</div>
			</div><!-- Footer -->
			<? } ?>
		</div><!-- wrapper -->
		<?php mkt_rdt_connect(); /*  */ ?> 
	</body>
</html>