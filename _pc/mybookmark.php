<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/my.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/my.js<?=vs_para();?>"></script>

			<div class="lib_con">
				<div class="fixed_snb_wrap">
					<div class="fixed_snb">
						<ul>
							<li>
								<a href="<?=NM_URL."/mycomics.php?mbc=1";?>">
									최근 본 작품
								</a>
							</li>

							<li>
								<a href="<?=NM_URL."/mycomics.php?mbc=2";?>">
									구매목록
								</a>
							</li>

							<li>
								<a href="<?=NM_URL."/mycomics.php?mbc=3";?>" class="cnt">
									즐겨찾기
								</a>
							</li>
						</ul>
						
						<?
						// 삭제(숨김)기능
						if(count($bookmark_arr) > 0) {
						?>
						<div class="lib_del_chk" id="fixed_del">
							<div class="lib_chk mt15">
								<input type="checkbox" class="lib_delete" id="lib_delete_fixed" name="m_radio" />
								<label for="lib_delete_fixed">
									<span>전체선택</span>
								</label>
							</div>
							
							<button class="lib_cancle_btn" id="fixed_cancle_btn">취소</button>
							<button class="lib_del_btn" id="fixed_delete_btn"><i class="fa fa-trash" aria-hidden="true"></i> 삭제</button>
						</div>
						<? } // end if ?>
					</div>
				</div>
				<div class="lib_header">
					<h4><span><?=$nm_member['mb_id'];?></span> 님의 서재
					<a href="<?=NM_URL."/myinfo.php";?>"><i class="fa fa-cog" aria-hidden="true"></i>내 정보</a></h4>

					<ul>
						<li>
							<a href="<?=NM_URL."/mycomics.php?mbc=1";?>">
								<div class="lib_h_btn">
									최근 본 작품
								</div>
							</a>
						</li>

						<li>
							<a href="<?=NM_URL."/mycomics.php?mbc=2";?>">
								<div class="lib_h_btn">
									구매목록
								</div>
							</a>
						</li>

						<li>
							<a href="<?=NM_URL."/mycomics.php?mbc=3";?>" class="currunt">
								<div class="lib_h_btn">
									즐겨찾기
								</div>
							</a>
						</li>
					</ul>

				<? 
				// 삭제기능
				if(count($bookmark_arr) > 0) {
				?>
					<div class="lib_del_chk" id="lib_h_del">
						<div class="lib_chk mt10">
							<input type="checkbox" class="lib_delete" id="lib_delete_rel" name="m_radio" />
							<label for="lib_delete_rel">
								<span>전체선택</span>
							</label>
						</div>
						
						<button class="lib_cancle_btn" id="lib_cancle_btn">취소</button>
						<button class="lib_del_btn" id="lib_delete_btn"><i class="fa fa-trash" aria-hidden="true"></i> 삭제 </button>
					</div>
				</div>
				<? } else {  ?>
				</div>
				<div class="lib_no_data" align="center">등록된 즐겨찾기가 없습니다.</div>
				<? } ?>			

				<form name="mycomics" id="mycomics" method="post" action="<?=NM_PROC_URL;?>/mycomics.php" onsubmit="return mycomics_submit();">

					<input type="hidden" id="mode" name="mode" value="bookmark">
					
					<!-- 내서재 썸네일리스트 -->
					<div class="lib_list">
						<? foreach($bookmark_arr as $key => $val) { 
								$img_url = img_url_para($val['cm_cover_sub'], $val['cm_reg_date'], $val['cm_mod_date'], 200, 200); ?>
							<a href="<?=get_comics_url($val['cm_no']);?>">
								<div class="lib_thumb">
									<dl>
										<dt>
											<div class="lib_chk">
												<input type="checkbox" id="lib_delete_thumb_<?=$key;?>" name="comics_check[]" value="<?=$val['cm_no'];?>" />
												<label for="lib_delete_thumb_<?=$key;?>"></label>
											</div>
											<img src="<?=$img_url;?>" alt="<?=$val['cm_series'];?>" />
										</dt>
										<dt><?=$val['cm_series'];?></dt>
										<dd><?=$val['cm_professional_info'];?></dd>
										<dd><?=substr($val['mbc_date'], 0, 10);?></dd>
									</dl>
								</div>
							</a>
						<? } // end foreach ?>
					</div><!-- /lib_list -->
				</form>
			</div>
		</div> <!-- /contents -->