<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		

		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/search.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/search.js<?=vs_para();?>"></script>

			
		<div class="comics_content_list">
			<!-- 작품리스트 -->
			<div id="cmserial" class="sub_list_bg">
        <? if(count($search_arr) > 0){ ?>
				<div class="sub_list clear" data-limit_define="<?=NM_SUB_LIST_LIMIT;?>" data-limit="<?=$sub_list_limit;?>" data-total="<?=$row_total;?>" data-url="<?=$_SERVER['REQUEST_URI'];?>" data-menu="<?=$_menu;?>" data-bool="<?=$sub_list_bool;?>">
					<? foreach($search_arr as $search_key => $search_val){
						$sub_thumb_class = "sub_list_thumb_small";
						$cm_cover = "cm_cover_sub";
						if($search_key < 12){
							$sub_thumb_class = "sub_list_thumb_big";
							$cm_cover = "cm_cover";
						}
						$search_url = get_comics_url($search_val['cm_no']);

						$small_txt = $nm_config['cf_small'][$search_val['cm_small']];

						$cm_series_txt = str_replace($search_txt, '<span class="highlight">'.$search_txt.'</span>', $search_val['cm_series']);

						$cm_professional_info_txt = str_replace($search_txt, '<span class="highlight">'.$search_txt.'</span>', $search_val['cm_professional_info']);
					?>
					<a href="<?=$search_url;?>" target="_self" data-week="<?=$search_val['cm_week_txt']?>">
						<div class="sub_list_thumb <?=$sub_thumb_class?>">
							<div class="icon_cm_type">
								<?=$search_val['cm_type_icon'];?>
							</div>
							<div class="icon_event">
								<?=$search_val['cm_free_icon'];?>
								<?=$search_val['cm_sale_icon'];?>
							</div>
							<div class="icon_date">
								<?=$search_val['cm_up_icon'];?>
								<?=$search_val['cm_new_icon'];?>
							</div>
							<div class="icon_adult">
								<?=$search_val['cm_adult_icon'];?>
							</div>
							<dl>
								<dt><img src="<?=$search_val[$cm_cover]?>" alt="<?=$search_val['cm_series'];?>" /></dt>
								<dt class="ellipsis"><span class="cm_title"><?=$cm_series_txt;?></span></dt>
								<dd class="ellipsis small"><span><?=$small_txt?></span></dd>
								<dd class="ellipsis"><?=$cm_professional_info_txt;?></dd>
							</dl>
						</div>
					</a>
					<? } /* foreach($search_arr as $search_key => $search_val){  */ ?>
				</div>
        <? } else { ?>
        <div class="search_no_data">검색어를 확인해주세요.</div>
        <? } ?>
			</div>
		</div>