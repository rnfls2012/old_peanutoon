<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/cs.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/cs.js<?=vs_para();?>"></script>

			<div class="container_wrap">
				<h4><span><i class="fa fa-comments-o" aria-hidden="true"></i> <?=$nm_config['cf_title'];?></span> 고객센터</h4>
				<div class="cs_left">
					<ul class="tabs">
						<li>
							<a href="<?=NM_URL."/cscenter.php?menu=1";?>"> <span><i class="fa fa-volume-up" aria-hidden="true"></i></span><br />공지사항</a>
						</li>
						<li>
							<a href="<?=NM_URL."/cscenter.php?menu=2";?>"><span><i class="fa fa-question" aria-hidden="true"></i></span><br />자주 하는 질문</a>
						</li>
						<li class="active">
							<a href="<?=NM_URL."/cscenter.php?menu=3";?>"><span><i class="fa fa-user-o" aria-hidden="true"></i></span><br />1:1 문의</a>
						</li>
						<li>
							<a href="<?=NM_URL."/cscenter.php?menu=4";?>"><span><i class="fa fa-book" aria-hidden="true"></i></span><br />연재문의</a>
						</li>
					</ul>
				</div>

				<div class="cs_contents">


					<div class="tab_container">
						<div id="tab3" class="tab_content">
							<fieldset>
								<form name="askwrite" id="askwrite" method="post" action="<?=NM_PROC_URL;?>/csaskwrite.php" onsubmit="return csaskwrite_submit();">
									<select name="ba_category" id="ba_category" class="ba_category">
										<option value="">문의 분류 선택</option>
										<option value="0">결제 관련</option>
										<option value="1">성인인증 관련</option>
										<option value="2">가입 / 탈퇴</option>
										<option value="3">제휴</option>
										<option value="4">기타</option>
									</select>
									<input type="text" id="qnaw_title" name="qnaw_title" placeholder="제목" />
									<textarea id="qnaw_request" name="qnaw_request" placeholder="내용을 입력하세요"></textarea>
									<input type="submit" value="문의하기">
								</form>
							</fieldset>
							<div class="clear"></div>
						</div>
						<!-- #tab3 -->
					</div>
					<!-- .tab_container -->
				</div>