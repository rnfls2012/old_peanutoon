<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<? include NM_PC_PART_PATH.'/event.php'; // Web 배너?>

		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/cmweek.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/cmweek.js<?=vs_para();?>"></script>
		<div class="comics_content_list">
			<? if($lnb_sub_count > 0){ ?>
				<style type="text/css">
					/* main_menu */
					.sub_list .lnb_sub ul li{width:<?=$lnb_sub_width;?>%;}
					.sub_list .lnb_sub ul li:first-child{width:<?=$lnb_sub_width_first;?>%;}
				</style>
				<div class="lnb_sub_bg" id="<?=$html_lnb_id;?>">
					<div class="lnb_sub">
						<ul>
							<? foreach($lnb_sub_arr as $lnb_sub_key =>  $lnb_sub){ 
								if($lnb_sub_key == 0){
									$cn_name = $lnb_sub['cn_name'];
								}else{
									$cn_name = $lnb_sub['cn_name']."요일";
								}
							?>
								<li class="<?=$lnb_sub['cn_current'];?>"><a href="<?=$lnb_sub['cn_lnb_link']?>" target="<?=$lnb_sub['cn_target']?>"><?=$cn_name?></a></li>
							<? } /* foreach($lnb_arr as $lnb){ */ ?>
						</ul>
					</div>
				</div><!-- /lnb_sub -->
			<?}else{ /* if($lnb_sub_count > 0) */?>
				<div class="lnb_sub_bg" id="<?=$html_lnb_id;?>">
					<div class="lnb_sub hide"></div>	
				</div>		
			<? } ?>
			<!-- 작품리스트 -->
			<div id="cmweek" class="sub_list_bg">
				<div class="sub_list clear" data-limit_define="<?=NM_SUB_LIST_LIMIT;?>" data-limit="<?=$sub_list_limit;?>" data-total="<?=$row_total;?>" data-url="<?=$_SERVER['REQUEST_URI'];?>" data-menu="<?=$_menu;?>" data-bool="<?=$sub_list_bool;?>"> 
					<? foreach($cmweek_arr as $cmweek_key => $cmweek_val){ 
						$sub_thumb_class = "sub_list_thumb_small";
						$cm_cover = "cm_cover_sub";
						if($cmweek_key < 12){ 
							$sub_thumb_class = "sub_list_thumb_big"; 
							$cm_cover = "cm_cover";
						}
						// 마오랑 요청 - 원조교제 아닌데요에 금요베스트 넣기
						$cmweek_best_txt = "";
						if($sql_cmweek_field_ck != "" & $cmweek_val['cmweek_field_ck'] == 0 && $cmweek_val['cm_public_cycle'] > 0 && $cmweek_val['cm_end'] == 'n'){
							$cmweek_best_txt = $cmweek_val['cm_week_txt']."요베스트";
						}
						// 마오랑 요청 - 원조교제 아닌데요에 금요베스트 넣기 end
					?>
					<a href="<?=$cmweek_val['cm_serial_url']?>" target="_self" data-week="<?=$cmweek_val['cm_week_txt']?>">
						<div class="sub_list_thumb <?=$sub_thumb_class?>">
							<div class="icon_cm_type">
								<?=$cmweek_val['cm_type_icon'];?>
							</div>
							<div class="icon_event">
								<?=$cmweek_val['cm_free_icon'];?>
								<?=$cmweek_val['cm_sale_icon'];?>
							</div>
							<div class="icon_date">
								<?=$cmweek_val['cm_up_icon'];?>
								<?=$cmweek_val['cm_new_icon'];?>
							</div>
							<div class="icon_adult">
								<?=$cmweek_val['cm_adult_icon'];?>
							</div>
							<dl>
								<dt><img src="<?=$cmweek_val[$cm_cover]?>" alt="<?=$cmweek_val['cm_series']?>" /></dt>
								<dt class="ellipsis"><span class="cm_title"><?=str_replace("[웹툰판]", "", $cmweek_val['cm_series']);?></span></dt>
								<dd class="ellipsis"><?=$cmweek_val['cm_small_span']?><?=$cmweek_val['cm_end_span']?><?=$cmweek_best_txt;?></dd>
								<dd class="ellipsis"><?=$cmweek_val['cm_professional_info']?></dd>
							</dl>
						</div>
					</a>
					<? } /* foreach($cmweek_arr as $cmweek_key => $cmweek_val){  */ ?>
				</div>
			</div>
		</div>

		<? // [mobile] webtoon 신작	
		if($_menu != 'all'){ include NM_PC_PART_PATH.'/cmweek_new.php'; } ?>