<? include_once '_common.php'; // 공통
include_once($nm_config['nm_path']."/_head.sub.php");
// mkt_rdt_acecounter_comicsview($comics, $episode); //에이스카운터 사용안함
// 181001 아무타스 리타겟팅 태그
gtm_amutus($nm_member); 
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/<?=$comicsview_css;?><?=vs_para();?>" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/comicsviewscroll.js<?=vs_para();?>"></script>
		<script type="text/javascript">
		<!--
			page_slider = "<?=$page_slider?>";
			page_slider_count = "<?=$episode_list_count?>";
			cm_page_way = "<?=$comics['cm_page_way']?>";

			comics = "<?=$comics['cm_no']?>";
			episode = "<?=$episode['ce_no']?>";

			episode_prev = "<?=$episode['ce_no_prev']?>";
			episode_prev_pay = "<?=$episode['ce_no_prev_pay']?>";
			episode_prev_txt = "<?=$episode['ce_no_prev_txt']?>";

			episode_next = "<?=$episode['ce_no_next']?>";
			episode_next_pay = "<?=$episode['ce_no_next_pay']?>";
			episode_next_txt = "<?=$episode['ce_no_next_txt']?>";
		//-->
		</script>

		<div id="comicsviewpage"> <!-- 상단 작품배너와 작품소개 -->
			<header id="viewer_header" class="viewer_header clear">
				<div class="header_left">
					<a href="<?=$comics_url;?>">
						<!-- <img src="<?=NM_PC_IMG?>common/viewer_icon_comic_list.png" alt="만화리스트"> --><span><?=$episode_chapter?></span>
					</a>
				</div>
				<div class="header_right">
					<div class="comicsviewpage_btn">
						<button id="mybookmark" class="d_faborite" onclick="set_bookmark('<?=$comics['cm_no'];?>');" data-mybookmark="<?=$count_bookmark;?>">
							<!-- <img src="<?=NM_PC_IMG?>common/viewer_icon_favorite_on.png" alt="" class="favorite"> -->
							<i class="fa fa-star-o" aria-hidden="true"></i>
						</button>
					</div>
					<div class="comicsviewpage_btn">
						<button id="fullscreen" class="d_faborite" onclick="fullscreen();" data-mybookmark="<?=$count_bookmark;?>">
							<!-- <img src="<?=NM_PC_IMG?>common/viewer_icon_favorite_on.png" alt="" class="favorite"> -->
							<i class="fa fa-expand" aria-hidden="true"></i>
						</button>
					</div>
					<a href="<?=$mycomics_url;?>">
						<!-- <img src="<?=NM_PC_IMG?>common/viewer_icon_my_library.png" alt="내서재" class="my_library"> -->
						<i class="fa fa-book" aria-hidden="true"></i>
					</a>

					<a href="<?=$cs_comic_bookmark_url;?>"> 
						<i class="fa fa-list" aria-hidden="true"></i>
					</a>

					<a href="#" id="bmk_list"> 
						<i class="fa fa-bookmark-o" aria-hidden="true"></i>
					</a>

				</div>
				<div class="inviewer_list">
					<ol>
					<? foreach($episode_inviewer_list_arr as $key => $val) { ?>
						<li class="<?=($val['ce_no'] == $_episode)?"list_on":"";?>">
							<button onclick="episode_goto_url('<?=$val['ce_no'];?>','<?=$val['ce_pay'];?>','<?=$val['ce_txt'];?>');">
								<?=$val['ce_txt'];?>
							</button>
						</li>
					<? } // end foreach ?>
					</ol>
				</div>
			</header>

			<div class="webtoon-viewer-list">
				<div class="webtoon-viewer-btn"></div>
				<section class="webtoon-viewer">
					<? foreach($episode_list as $episode_key => $episode_val){ // 파일 리스트
						$img_alt = $comics['cm_series'].$episode['ce_chapter'].$d_cm_up_kind[$comics['cm_up_kind']].$episode_key;
						$img_src = $episode_val."&cmd=RL_760x1";
					?>
							<img src="<?=$img_src;?>" alt="<?=$img_alt?>">
					<?}?>
					<img class="copyright" src="<?=img_url_para('common/copyright.png', NM_TIME_YMD);?>" alt="저작권 안내">
				</section>
			</div>
			
			<footer id="viewer_footer" class="viewer_footer">
			<? if($randombox_state_get_ck == true) { ?>
			<div class="viewer_event_wrap">
				<a href="<?=NM_URL;?>/eventrandombox.php">
					<div class="viewer_event_contents">
						<ul>
							<li><img src="<?=NM_PC_IMG;?>event/randombox/viewerbar.png<?=vs_para();?>" alt="땅콩 품은 복주머니" /></li>
							<li>참여 땅콩 수</li>
							<li><span class="pocket_value"><?=$randombox_mb['erm_cash_point_total'];?></span> / 10</li>
						</ul>
					</div>
				</a>
				<div class="viewer_event_close"><span class="bnr_close">다시보지 않기</span><i class="material-icons">clear</i></div>
			</div>
			<? } // end if ?>
			<?if($episode['ce_no_prev'] != '' && $comics['cm_page_way'] == 's'){?>
				<button onclick="episode_goto_url(episode_prev, episode_prev_pay, episode_prev_txt);">
					<!--<img src="<?=NM_PC_IMG?>common/viewer_tool_next_prev.png" alt="" class="cursor_pointer right prevContent"> -->
					<i class="fa fa-angle-left" aria-hidden="true"></i> <span>이전화</span>

				</button>
			<?}?>
			<?if($episode['ce_no_next'] != '' && $comics['cm_page_way'] == 's'){?>
				<button onclick="episode_goto_url(episode_next, episode_next_pay, episode_next_txt);">
					<!-- <img src="<?=NM_PC_IMG?>common/viewer_tool_prev_next.png" alt="" class="cursor_pointer left nextContent"> -->
					<span>다음화</span> <i class="fa fa-angle-right" aria-hidden="true"></i>

				</button>
			<?}?>
			</footer>
			<div class="direction">
				<div class="bg">&nbsp;</div>
				<img alt="<?=$d_cm_page_way[$comics['cm_page_way']]?>" src="<?=NM_IMG?>common/view_direction_<?=$comics['cm_page_way']?>.png" class="view_direction">
			</div>
		</div>

<? cs_confirmjoin_ask(); /* 180420 신규회원가입이벤트  */?>
<?include_once($nm_config['nm_path']."/_tail.sub.php");?>
