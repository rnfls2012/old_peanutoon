<?
    include_once '_common.php'; // 공통

		//설 이벤트 이미지 변경
    if (NM_TIME_YMD >= '2018-02-15' && NM_TIME_YMD <= '2018-02-18') {
        $event_img = "_e";
    } else {
        $event_img = "";
    }
?>
<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->

			<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/appdown.css<?=vs_para();?>" />
			<style type="text/css">
				#appdown .evt_header { position: relative; width: 100%; text-align: center; background:url(<?=NM_PC_IMG;?>appdown/v2/header_bg.png<?=vs_para();?>) repeat-x left top; background-color: #fbeac2; }
			</style>
			<script type="text/javascript" src="<?=NM_PC_URL;?>/js/appdown.js<?=vs_para();?>"></script>

			<div id="appdown">
				<div class="evtcontainer">
					<div class="evt_header">
						<img src="<?=NM_PC_IMG?>appdown/v2/header<?=$event_img?>.png<?=vs_para();?>" alt="피너툰 완전판 앱 다운로드" />
					</div>

					<div class="evt_con01">
						<img src="<?=NM_PC_IMG?>appdown/v2/con01.png<?=vs_para();?>" alt="피너툰 완전판 앱 다운로드 1" />
					</div>
					<div class="evt_con02">
						<img src="<?=NM_PC_IMG?>appdown/v2/con02.png<?=vs_para();?>" alt="피너툰 완전판 앱 다운로드 2" />
						<a href="<?=NM_URL;?>/app/app_setapk.php">
							<img src="<?=NM_PC_IMG?>appdown/v2/con02_btn.png<?=vs_para();?>" alt="피너툰 완전판 앱 설치하기" />
						</a>
					</div>
					<div class="evt_con03">
						<img src="<?=NM_PC_IMG?>appdown/v2/con03<?=$event_img?>.png<?=vs_para();?>" alt="피너툰 완전판 앱 다운로드 3" />
					</div>
					<div class="evt_con04">
						<img src="<?=NM_PC_IMG?>appdown/v2/con04<?=$event_img?>.png<?=vs_para();?>" alt="피너툰 완전판 앱 다운로드 4" />
					</div>

				</div>
			</div> <!-- /contents -->
