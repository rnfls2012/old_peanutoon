<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/eventageday.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/eventageday.js<?=vs_para();?>"></script>
		<style type="text/css">
			.evt-title {
				background-image:url(<?=$ageday_img;?>/top_bg02.png<?=vs_para();?>), url(<?=$ageday_img;?>/top_bg01.png<?=vs_para();?>);
				background-position:right bottom, left bottom;
				background-repeat:no-repeat;
			}
			.evt-content {
				background-image:url(<?=$ageday_img;?>/bottom_bg02.png<?=vs_para();?>), url(<?=$ageday_img;?>/bottom_bg01.png<?=vs_para();?>);
				background-position: right center, left center;
				background-repeat: no-repeat;
				background-size: auto;
			}
		</style>
		
		<div class="evt-container">
			<div class="evt-title">
				<div class="evt-maincopy">
					<img src="<?=$ageday_img;?>/top_title.png<?=vs_para();?>" alt="20살 축하해" />
				</div>
				<div class="evt-exp">
					<img src="<?=$ageday_img;?>/top_exp.png<?=vs_para();?>" alt="이벤트 안내" class="top_exp" />
				</div>
			</div>
			<div class="evt-content">
				<img src="<?=$ageday_img;?>/con_01.png<?=vs_para();?>" alt="Event 1. 3작품 2땅콩 할인" />
				<div class="evt-link">
					<a href="<?=get_comics_url(2773);?>"><img src="<?=$ageday_img;?>/con_02.png<?=vs_para();?>" alt="블라인드 데이트" /></a>
					<a href="<?=get_comics_url(2718);?>"><img src="<?=$ageday_img;?>/con_03.png<?=vs_para();?>" alt="내 사랑은 고단해" /></a>
					<a href="<?=get_comics_url(2660);?>"><img src="<?=$ageday_img;?>/con_04.png<?=vs_para();?>" alt="플레이 하우스" /></a>
					<img src="<?=$ageday_img;?>/con_05.png<?=vs_para();?>" alt="Event 2. 5작품 총 20화 무료" />
					<a href="<?=get_comics_url(2531);?>"><img src="<?=$ageday_img;?>/con_06.png<?=vs_para();?>" alt="키스까지 앞으로 1초" /></a>
					<a href="<?=get_comics_url(2580);?>"><img src="<?=$ageday_img;?>/con_07.png<?=vs_para();?>" alt="몰락 왕자의 달콤한 키스" /></a>
					<a href="<?=get_comics_url(2553);?>"><img src="<?=$ageday_img;?>/con_08.png<?=vs_para();?>" alt="매일 너랑 첫 키스" /></a>
					<a href="<?=get_comics_url(2931);?>"><img src="<?=$ageday_img;?>/con_09.png<?=vs_para();?>" alt="키스하는 짐승" /></a>
					<a href="<?=get_comics_url(1838);?>"><img src="<?=$ageday_img;?>/con_10.png<?=vs_para();?>" alt="너는 키스를 거부할 수 없어" /></a>
				</div>
				<img src="<?=$ageday_img;?>/con_11.png<?=vs_para();?>" alt="Event 1. 3작품 2땅콩 할인" />
				<div class="evt-warn">
					<img src="<?=$ageday_img;?>/bottom_warn.png<?=vs_para();?>" alt="이벤트 유의사항" />
				</div>
			</div>
		</div>