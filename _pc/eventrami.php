<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-07-09
 * Time: 오후 5:47
 */

$rami_img_path = NM_IMG.$nm_config['nm_mode'].'/event/rami'; // 이미지 경로

?>

<style type="text/css">
    .evt-header {
        position: relative;
        width: 100%;
        background:
                url(<?=$rami_img_path?>/bg01.png<?=vs_para()?>), url(<?=$rami_img_path?>/bg02.png<?=vs_para()?>), #b1c95d;
        background-size: auto;
        background-repeat: no-repeat, repeat-x;
        background-position: center top, left 850px, 0;
        padding-bottom: 50px;
    }

    .evt-insp {
        position: relative;
        background:
                url(<?=$rami_img_path?>/bg03.png<?=vs_para()?>), url(<?=$rami_img_path?>/bg04.png<?=vs_para()?>),
                url(<?=$rami_img_path?>/bg05.png<?=vs_para()?>), url(<?=$rami_img_path?>/bg06.png<?=vs_para()?>), #f8ecd4;
        background-size: auto;
        background-repeat: no-repeat;
        background-position: left top, right top, left bottom, right bottom;
        text-align: center;
        padding: 90px 0;
    }
    
    .reply_btn {
        background: url(<?=$rami_img_path?>/reply_confirm.png<?=vs_para()?>);
        margin-left: 15px;
        width: 208px;
        height: 147px;
    }
</style>

<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL?>/css/eventrami.css<?=vs_para()?>" />
<script type="text/javascript" src="<?=NM_PC_URL?>/js/eventrami.js<?=vs_para()?>"></script>

<div class="evt-container">
    <div class="evt-header">
        <div class="evt-title">
            <img src="<?=$rami_img_path?>/title.png<?=vs_para()?>" alt="라미, 넌 누구니?" />
        </div>
        <div class="evt-intro">
            <img src="<?=$rami_img_path?>/intro.png<?=vs_para()?>" alt="라미를 소개합니다" />
        </div>
    </div>
    <div class="evt-insp">
        <img src="<?=$rami_img_path?>/insp.png<?=vs_para()?>" alt="이벤트 설명" />
        <div class="foot_kong">
            <img src="<?=$rami_img_path?>/insp_foot01.png<?=vs_para()?>" alt="땅이콩이">
        </div>
        <div class="foot_rami">
            <img src="<?=$rami_img_path?>/insp_foot02.png<?=vs_para()?>" alt="라미">
        </div>
    </div>
    <div class="evt-reply">
        <?php
        if ( !$did_comment ) {
        ?>
        <div class="reply_input">
            <form>
                <div class="reply_byte">
                    <span class="count_box_character">0</span>
                    <span class="limit_box_character"> / 50</span>
                </div>
                <input type="hidden" name="mb_no" id="mb_no" value="<?=$nm_member['mb_no']?>" />
                <textarea placeholder="라미를 위한 응원의 한마디! (50자 이내)" id="comments" name="comments" cols="1" onKeyPress="if (event.keyCode===13) return false;" onKeyUp="countChar(this)"></textarea>
                <button type="button" onclick="eventrami_submit()" class="reply_btn">
            </form>
        </div>
        <!-- 댓글쓰고나서 이미지 출력 -->
        <div class="reply_img" style="display: none">
            <img src="<?=$rami_img_path?>/reply_ok.png<?=vs_para()?>" alt="참여해주셔서 감사합니다!">
        </div>

        <?php } else { ?>
            <!-- 댓글쓰고나서 이미지 출력 -->
            <div class="reply_img">
                <img src="<?=$rami_img_path?>/reply_ok.png<?=vs_para()?>" alt="참여해주셔서 감사합니다!">
            </div>
        <?php } ?>
        <div class="reply_list" id="reply_list" data-value="1">

            <!-- Paging -->

        </div>
    </div> <!-- /evt-reply -->
    <div class="evt-warn">
        <img src="<?=$rami_img_path?>/warn.png<?=vs_para()?>" alt="이벤트 유의사항">
    </div>
</div>
