<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/cs.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/cs.js<?=vs_para();?>"></script>

		<div class="container_wrap">
			<h4><span><i class="fa fa-comments-o" aria-hidden="true"></i> <?=$nm_config['cf_title'];?></span> 고객센터</h4>
			<div class="cs_left">
				<ul class="tabs">
					<li class="active">
						<a href="<?=NM_URL."/cscenter.php?menu=1";?>"> <span><i class="fa fa-volume-up" aria-hidden="true"></i></span><br />공지사항</a>
					</li>
					<li>
						<a href="<?=NM_URL."/cscenter.php?menu=2";?>"><span><i class="fa fa-question" aria-hidden="true"></i></span><br />자주 하는 질문</a>
					</li>
					<li>
						<a href="<?=NM_URL."/cscenter.php?menu=3";?>"><span><i class="fa fa-user-o" aria-hidden="true"></i></span><br />1:1 문의</a>
					</li>
					<li>
						<a href="<?=NM_URL."/cscenter.php?menu=4";?>"><span><i class="fa fa-book" aria-hidden="true"></i></span><br />연재문의</a>
					</li>
				</ul>
			</div>

			<div class="cs_contents">
				<div class="tab_container">
					<div id="tab1" class="tab_content">

						<!-- 공지사항 _ 게시판 형태 -->
						<table class="cs_notice" cellpadding="0" cellspacing="0">
							<tr class="t_index">
								<th>No</th>
								<th>제목</th>
								<th>작성일</th>
							</tr>
							<? for($start_num; $start_num<=$end_num; $start_num++) { ?>
							<tr>
								<td class="b_num"><?=$notice_list[$start_num-1]['bn_no'];?></td>
								<td class="b_title">
									<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
									<a href="<?=NM_URL."/cscenter.php?menu=1&bn=".$notice_list[$start_num-1]['bn_no'];?>">
										<?=$notice_list[$start_num-1]['bn_title']?>
									</a>

									<? if(substr($notice_list[$start_num-1]['bn_date'],0 ,10) === NM_TIME_YMD) { ?>
										<img src="<?=$nm_config['nm_url_img']?>icon/b_icon_new.png" />
									<? } ?>

								</td>
								<td class="b_date"><?=substr($notice_list[$start_num-1]['bn_date'],0 ,10);?></td>
							</tr>
							<? } // end for ?>
						</table>
						
						<!-- 페이징 처리 -->
						<div class="list_paging">
							<div class="list_paging_align">
								<ul>
									<? if($cur_block < $single_left_block) { ?>
									<li class="angle">
										<a href="<?=NM_URL."/csnotice.php?curpage=".$double_left_page."&curblock=".$double_left_block;?>">
											<i class="fa fa-angle-double-left" aria-hidden="true"></i>
										</a>
									</li>
									<li class="angle">
										<a href="<?=NM_URL."/csnotice.php?curpage=".$single_left_page."&curblock=".$single_left_block;?>">
											<i class="fa fa-angle-left" aria-hidden="true"></i>
										</a>
									</li>
									<? } // end if ?>
									
									<? for($block_start_num; $block_start_num <= $block_end_num; $block_start_num++) { ?>
									<li <? if($block_start_num == $curpage) { echo 'class="on"'; } // end if)?>>
										<a href="<?=NM_URL."/csnotice.php?curpage=".$block_start_num."&curblock=".$curblock;?>"><?=$block_start_num?></a>
									</li>
									<? } // end for ?>
									
									<? if($curblock < $total_block) { ?>
									<li class="angle">
										<a href="<?=NM_URL."/csnotice.php?curpage=".$single_right_page."&curblock=".$single_right_block;?>">
											<i class="fa fa-angle-right" aria-hidden="true"></i>
										</a>
									</li>
									<li class="angle">
										<a href="<?=NM_URL."/csnotice.php?curpage=".$double_right_page."&curblock=".$double_right_block;?>">
											<i class="fa fa-angle-double-right" aria-hidden="true"></i>
										</a>
									</li>
									<? } // end if ?>
								</ul>
							</div>
						</div>
						<!-- list_paging -->
					</div>
					<!-- #tab1 -->
				</div>
				<!-- .tab_container -->
			</div>
		</div>