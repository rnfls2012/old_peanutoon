<?php

?>

<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL?>/css/eventfinalexam_result.css<?=vs_para()?>" />
<script type="text/javascript" src="<?=NM_PC_URL?>/js/eventfinalexam_result.js<?=vs_para()?>"></script>
	<style type="text/css">		
		.evt-container {
			background: url(<?=$finalexam_img_url;?>/pc/bg01.jpg<?=vs_para();?>);
			background-color: #ecece1;
		}
		
		.evt-header {
			background-image:url(<?=$finalexam_img_url;?>/pc/bg04.png<?=vs_para();?>), url(<?=$finalexam_img_url;?>/pc/bg03.png<?=vs_para();?>), url(<?=$finalexam_img_url;?>/pc/bg02.png<?=vs_para();?>);
			background-position:center, right top, left bottom;
			background-size: auto;
			background-repeat:no-repeat;
		}
		.evt-present:before {
			background: url(<?=$finalexam_img_url;?>/pc/bg05.png<?=vs_para();?>);
		}
		.evt-input:before {
			background: url(<?=$finalexam_img_url;?>/pc/bg05.png<?=vs_para();?>);
		}
		.evt-answer:before {
			background: url(<?=$finalexam_img_url;?>/pc/bg05.png<?=vs_para();?>);
		}
	</style>

	<div class="evt-container">
		<div class="evt-container">
			<div class="evt-header">
				<img src="<?=$finalexam_img_url;?>/pc/title.png<?=vs_para();?>" alt="전국덕후학력평가 당첨자 발표&답안해설" />
				<div class="header_btn">
					<a onclick="a_click_false();"><img src="<?=$finalexam_img_url;?>/pc/header_btn.png<?=vs_para();?>" alt="답안해설 보러가기" /></a>
				</div>
			</div>
			<div class="evt-present"><img src="<?=$finalexam_img_url;?>/pc/present.png<?=vs_para();?>" alt="이벤트 상품구성" /></div>
			<div class="evt-input">
				<img src="<?=$finalexam_img_url;?>/pc/input_title.png<?=vs_para();?>" alt="당첨자 정보 입력" />
				<div class="input_btn">
					<a <?=$delivery_js;?>><img src="<?=$finalexam_img_url;?>/pc/input_btn.png<?=vs_para();?>" alt="당첨자 확인" /></a>
				</div>
				<img src="<?=$finalexam_img_url;?>/pc/input_warn.png<?=vs_para();?>" alt="8월 17일까지 상품을 수령할 당첨자의 정보를 입력해주세요." />
			</div>
			<div class="evt-answer">
				<img src="<?=$finalexam_img_url;?>/pc/answer.png<?=vs_para();?>" alt="이벤트 답안 해설" />
				<ul>
					<li>
						<a <?=$answer1_js;?>><img src="<?=$finalexam_img_url;?>/pc/answer_btn01.png<?=vs_para();?>" alt="1교시 답안 해설" /></a>
					</li>
					<li>
						<a <?=$answer2_js;?>><img src="<?=$finalexam_img_url;?>/pc/answer_btn02.png<?=vs_para();?>" alt="2교시 답안 해설" /></a>
					</li>
					<li>
						<a <?=$answer3_js;?>><img src="<?=$finalexam_img_url;?>/pc/answer_btn03.png<?=vs_para();?>" alt="3교시 답안 해설" /></a>
					</li>
				</ul>
			</div>
			<div class="evt-warn"><img src="<?=$finalexam_img_url;?>/pc/warn.png<?=vs_para();?>" alt="이벤트 유의사항"></div>
		</div>
	</div>