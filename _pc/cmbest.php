<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<? include NM_PC_PART_PATH.'/event.php'; // 모바일배너
		?>

		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/cmbest.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/cmbest.js<?=vs_para();?>"></script>
		<?if($lnb_sub_count > 0){ ?>
			<style type="text/css">
				/* main_menu */
				.comics_content_list .lnb_sub ul li{width:<?=$lnb_sub_width;?>%;}
				.comics_content_list .lnb_sub ul li:first-child{width:<?=$lnb_sub_width_first;?>%;}
			</style>
		<div class="comics_content_list">
			<div class="lnb_sub_bg" id="<?=$html_lnb_id;?>">
				<div class="lnb_sub">
					<ul>
						<? foreach($lnb_sub_arr as $lnb_sub_key =>  $lnb_sub){ 
							$cn_name = $lnb_sub['cn_name']; ?>
							<li class="<?=$lnb_sub['cn_current'];?>"><a href="<?=$lnb_sub['cn_lnb_link']?>" target="<?=$lnb_sub['cn_target']?>"><?=$cn_name?></a></li>
						<? } /* foreach($lnb_arr as $lnb){ */ ?>
					</ul>
				</div>
			</div><!-- /lnb_sub -->
			<!-- 작품리스트 -->
			<div id="cmbest" class="sub_list_bg">
				<div class="sub_list clear" data-limit_define="<?=NM_SUB_LIST_LIMIT;?>" data-limit="<?=$sub_list_limit;?>" data-total="<?=$row_total;?>" data-url="<?=$_SERVER['REQUEST_URI'];?>" data-small="<?=$_small;?>" data-bool="<?=$sub_list_bool;?>"> 
					<? foreach($cmend_arr as $cmend_key => $cmend_val){ 
						$sub_thumb_class = "sub_list_thumb_small";
						$cm_cover = "cm_cover_sub";
						if($cmend_key < 12){ 
							$sub_thumb_class = "sub_list_thumb_big"; 
							$cm_cover = "cm_cover";
						}
					?>
					<a href="<?=$cmend_val['cm_serial_url']?>" target="_self">
						<div class="sub_list_thumb <?=$sub_thumb_class?>">
							<div class="icon_cm_type">
								<?=$cmend_val['cm_type_icon'];?>
							</div>
							<div class="icon_event">
								<?=$cmend_val['cm_free_icon'];?>
								<?=$cmend_val['cm_sale_icon'];?>
							</div>
							<div class="icon_date">
								<?=$cmend_val['cm_up_icon'];?>
								<?=$cmend_val['cm_new_icon'];?>
							</div>
							<div class="icon_adult">
								<?=$cmend_val['cm_adult_icon'];?>
							</div>
							<dl>
								<dt><img src="<?=$cmend_val[$cm_cover]?>" alt="<?=$cmend_val['cm_series']?>" /></dt>
								<dt class="ellipsis"><?=$cmend_val['cm_series']?></dt>
								<dd class="ellipsis small"><span><?=$cmend_val['small_txt']?></span></dd>
								<dd class="ellipsis"><?=$cmend_val['cm_professional_info']?></dd>
							</dl>
						</div>
					</a>
					<? } /* foreach($cmend_arr as $cmend_key => $cmend_val){  */ ?>
				</div>
			</div>
		</div>
		<?}?>