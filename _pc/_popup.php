<? include_once '_common.php'; // 공통

// DB
$sql_popup = "SELECT * FROM `epromotion_popup` WHERE  `emp_state` = 'y' order by emp_no DESC limit 0, 1";
$row_popup = sql_fetch($sql_popup);

$popup_emp_css = "";
if($row_popup['emp_no'] == ""){
	$popup_emp_css = " display:none; ";
}
if($_COOKIE["popup_emp_{$row_popup['emp_no']}"] != ""){
	$popup_emp_css = " display:none; ";
}

$emp_height = $row_popup['emp_height']."px;";
if($row_popup['emp_height'] == '0'){$emp_height = "auto;";}

$emp_width_pc = $row_popup['emp_width_pc']."px;";

// 위치
$emp_top = $row_popup['emp_top']."px;";
$emp_left = $row_popup['emp_left']."px;";

// 중앙 정렬
$emp_width_pc_margin_left = "";
$emp_width_pc_helf = intval($row_popup['emp_width_pc']) / 2;
if($row_popup['emp_center'] == 'y'){
	$emp_left = "50%;";
	$emp_width_pc_margin_left = "margin-left:-".$emp_width_pc_helf."px;";
}

// 이전 페이지 조건 노출
$emp_exposure_mb_sns_type_bool = $emp_exposure_referer_bool = false;
if($row_popup['emp_exposure_referer'] == ""){
	$emp_exposure_referer_bool = true;
}else{
	$emp_exposure_referer = explode(",", $row_popup['emp_exposure_referer']);
	foreach($emp_exposure_referer as $emp_exposure_referer_val){
		if(strpos(HTTP_REFERER, $emp_exposure_referer_val)){ 
			$emp_exposure_referer_bool = true; 
		}
	}
}

// 회원 조건 노출
if($row_popup['emp_exposure_mb_sns_type_used'] == "y"){
	if(intval($nm_member['mb_no']) > 0){
		if($row_popup['emp_exposure_mb_sns_type'] == $nm_member['mb_sns_type']){
			$emp_exposure_mb_sns_type_bool = true; 
		}
	}
}else{
	$emp_exposure_mb_sns_type_bool = true; 
}

// 이전 페이지 조건 노출 OR 회원 조건 노출
if($emp_exposure_referer_bool == false && $emp_exposure_mb_sns_type_bool == false){
	$popup_emp_css = " display:none; ";
}


$popselect_l = $popselect_r = ""; 
$popselect_l_alt = $popselect_r_alt = "";

/* css */
$popup_layer_css = "";
$popup_layer_footer_bg_css = "";

// 임시로 만든 기능 팝업 6번일 경우
if($row_popup['emp_no'] == '6'){
	$popselect_l = NM_IMG."data/editor/2017-11/793e74f7a1428fd260ae1984d6e256df_1512028810_1974.png";
	$popselect_l_alt = "회원";
	$popselect_r = NM_IMG."data/editor/2017-11/793e74f7a1428fd260ae1984d6e256df_1512028812_2139.png";
	$popselect_r_alt = "비회원";
	
	/* css */
	$popup_layer_css = " border:0px solid #e9e9e9; ";
	$popup_layer_footer_bg_css = "background: #d2d2d2 url(".NM_PC_IMG."popup/popup_layer_footer_bg.png".vs_para().") no-repeat center center; text-align: center; ";
}


// 접기일때
$popup_layer_fold_bool = false;
$popup_layer_reject_txt = "<strong>".$row_popup['emp_disable_hours']."</strong>시간 동안 닫기";
if($row_popup['emp_fold'] == 'y'){ 
	$popup_layer_fold_bool = true;
	// 아래 나중에...
	/*
	$popup_layer_reject_txt = "잠시 접어두기";
	$popup_layer_reject_img = "잠시 접어두기";
	$popup_layer_reject_close_txt = "닫기";
	$popup_layer_reject_close_img = "닫기";
	*/

	$popup_layer_fold_class = "hide";
	if(get_js_cookie('popup_emp_'.$row_popup['emp_no']) != ''){
		$popup_layer_fold_class = "";
	}
	
	/* css */
	$popup_layer_css = " border:0px solid #e9e9e9; ";
	$popup_layer_footer_bg_css = "background: #d2d2d2 url(".NM_PC_IMG."popup/popup_layer_fold_bg.png".vs_para().") no-repeat center center; text-align: center; ";
}

?>

<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/popup.css<?=vs_para();?>" />

<script type="text/javascript" src="<?=NM_PC_URL;?>/js/popup.js<?=vs_para();?>"></script>

<div id="popup_emp_<?php echo $row_popup['emp_no']; ?>" class="popup_layer" style="top:<?php echo $emp_top;?> left:<?php echo $emp_left;?> <?=$popup_emp_css;?> <?=$emp_width_pc_margin_left;?> <?=$popup_layer_css;?>">
	<div class="popup_content" style="width:<?php echo $emp_width_pc;?> height:<?php echo $emp_height ?> position: relative;">
		<? echo stripslashes($row_popup['emp_text']);?>

		<? if($row_popup['emp_no'] == '6'){ 
		/* 임시로 만든 기능 팝업 6번일 경우 */ ?>
		<div style="position: absolute; height: 41px; width: 100%; bottom: 23px; text-align:center;";>
			<a data-href="<?=NM_TKSK_URL;?>/ics5/popselect"><img src="<?=$popselect_l;?>" alt="<?=$popselect_l_alt;?>" style="margin-right:4px;"></a><a data-href="<?=NM_TKSK_URL;?>/ics6/popselect"><img src="<?=$popselect_r;?>" alt="<?=$popselect_r_alt;?>" style="margin-left:4px;"></a>
		</div>
		<? } /* end 임시로 만든 기능 팝업 6번일 경우 */ ?>

	</div>
	<div class="popup_layer_footer" style="<?=$popup_layer_footer_bg_css;?>">		
		<? if($row_popup['emp_no'] == '6'){ ?>
		  <style type="text/css">
		  	.popup_layer .popup_layer_footer button {
			    margin-right: 140px;
			    padding: 0px 0px;
			    border: 0;
			    background: none;
			    color: none;
				}			
		  </style>
			<button class="popup_layer_reject popup_emp_<?php echo $row_popup['emp_no']; ?> <?php echo $row_popup['emp_disable_hours']; ?>">
				<img src="<?=NM_PC_IMG;?>popup/popup_layer_footer_checkbox.png<?=vs_para();?>" alt="오늘그만보기">
			</button>
		<? }else{ ?>
			<button class="popup_layer_reject popup_emp_<?php echo $row_popup['emp_no']; ?> <?php echo $row_popup['emp_disable_hours']; ?>">
				<? /* 접기 기능 사용시 */
				if($popup_layer_fold_bool == true){ ?>	
				<img src="<?=NM_PC_IMG;?>popup/popup_layer_fold.png<?=vs_para();?>" alt="잠시 접어두기">
				<? }else{ ?>
				<strong><?php echo $row_popup['emp_disable_hours']; ?></strong>시간 동안 닫기
				<? } ?>
			</button>
			<button class="popup_layer_close popup_emp_<?php echo $row_popup['emp_no']; ?>">
				<? /* 접기 기능 사용시 */
				if($popup_layer_fold_bool == true){ ?>
				<img src="<?=NM_PC_IMG;?>popup/popup_layer_fold_close.png<?=vs_para();?>" alt="닫기">
				<? }else{ ?>
					닫기
				<? } ?>
			</button>
		<? } ?>
	</div>
</div>

<? /* 접기 기능 사용시 */
if($popup_layer_fold_bool == true){ ?>
<div id="popup_emp_fold_<?php echo $row_popup['emp_no']; ?>"  class="popup_layer_fold <?=$popup_layer_fold_class;?>">
	<img src="<?=NM_PC_IMG;?>popup/popup_layer_fold_icon.png<?=vs_para();?>" alt="<?=$row_popup['emp_name'];?>접기">
</div>
<? } ?>