<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<style type="text/css">
			.evt-container {
				position: relative;
				width: 100%;
				background: #2ccfff; /* Old browsers */
				background: linear-gradient(to bottom, #2ccfff 0%,#16c0ff 50%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				overflow:hidden;
			}
			.evt-img {
				position: relative;
				width: 900px;
				margin: 0 auto;
			}
			.evt-btn {
				position: relative;
				width: 900px;
				margin: 0 auto;
				text-align: center;
				background-image: url(<?=$schedule_img;?>/cal_btn_bg.png<?=vs_para();?>);
			}
		</style>
		
		<div class="evt-container">
			<div class="evt-img">
				<img src="<?=$schedule_img;?>/cal01.png<?=vs_para();?>" alt="피너툰 스케줄러 6월의 신작" />
			</div>
			<div class="evt-btn">
				<a href="<?=NM_URL."/cmnew.php";?>">
					<img src="<?=$schedule_img;?>/cal_btn.png<?=vs_para();?>" alt="신작 보러 가기" />
				</a>
			</div>
			<div class="evt-img">
				<img src="<?=$schedule_img;?>/cal02.png<?=vs_para();?>" alt="알려드립니다" />
			</div>
		</div>