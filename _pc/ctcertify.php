<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/ct.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/ct.js<?=vs_para();?>"></script>

		<div class="certify_box">
			<div class="certify_box_bg">
				<div class="adult_alart">
					<div class="adult_mark"><img src="<?=NM_MO_IMG?>ct/teenager.png" alt="19세" /></div>
					<div class="adult_warn">
						<?=stripslashes(nl2br($row['clt_ipin']));?>
					</div>
					<div class="adult_confirm">
						<a href="#adult" onclick="return auth_type_check_web('<?=NM_KCP_CR_URL.'/'.NM_PC.'/start.php';?>?<?=get_add_para_redirect();?>');">본인인증하기</a>
					</div>
					<div class="adult_out">
						<a href="#teenager" onclick="return teenager_web();">취소</a>
					</div>
				</div>
			</div>
		</div>