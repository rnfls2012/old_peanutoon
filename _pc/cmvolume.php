<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<? include NM_PC_PART_PATH.'/event.php'; // 모바일배너?>

		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/cmvolume.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/cmvolume.js<?=vs_para();?>"></script>
		<?if($lnb_sub_count > 0){ ?>
			<style type="text/css">
				/* main_menu */
				.comics_content_list .lnb_sub ul li{width:<?=$lnb_sub_width;?>%;}
				.comics_content_list .lnb_sub ul li:first-child{width:<?=$lnb_sub_width_first;?>%;}
			</style>
		<div class="comics_content_list">
			<div class="lnb_sub_bg" id="<?=$html_lnb_id;?>">
				<div class="lnb_sub">
					<ul>
						<? foreach($lnb_sub_arr as $lnb_sub_key =>  $lnb_sub){ 
							$cn_name = $lnb_sub['cn_name']; ?>
							<li class="<?=$lnb_sub['cn_current'];?>"><a href="<?=$lnb_sub['cn_lnb_link']?>" target="<?=$lnb_sub['cn_target']?>"><?=$cn_name?></a></li>
						<? } /* foreach($lnb_arr as $lnb){ */ ?>
					</ul>
				</div>
			</div><!-- /lnb_sub -->
			<!-- 작품리스트 -->
			<div id="cmvolume" class="sub_list_bg">
				<div class="sub_list clear" data-limit_define="<?=NM_SUB_LIST_LIMIT;?>" data-limit="<?=$sub_list_limit;?>" data-total="<?=$row_total;?>" data-url="<?=$_SERVER['REQUEST_URI'];?>" data-small="<?=$_small;?>" data-bool="<?=$sub_list_bool;?>"> 
					<? foreach($cmvolume_arr as $cmvolume_key => $cmvolume_val){ 
						$sub_thumb_class = "sub_list_thumb_small";
						$cm_cover = "cm_cover_sub";
						if($cmvolume_key < 12){ 
							$sub_thumb_class = "sub_list_thumb_big"; 
							$cm_cover = "cm_cover";
						}
					?>
					<a href="<?=$cmvolume_val['cm_serial_url']?>" target="_self">
						<div class="sub_list_thumb <?=$sub_thumb_class?>">
							<div class="icon_cm_type">
								<?=$cmvolume_val['cm_type_icon'];?>
							</div>
							<div class="icon_event">
								<?=$cmvolume_val['cm_free_icon'];?>
								<?=$cmvolume_val['cm_sale_icon'];?>
							</div>
							<div class="icon_date">
								<?=$cmvolume_val['cm_up_icon'];?>
								<?=$cmvolume_val['cm_new_icon'];?>
							</div>
							<div class="icon_adult">
								<?=$cmvolume_val['cm_adult_icon'];?>
							</div>
							<dl>
								<dt><img src="<?=$cmvolume_val[$cm_cover]?>" alt="<?=$cmvolume_val['cm_series']?>" /></dt>
								<dt class="ellipsis"><?=$cmvolume_val['cm_series']?></dt>
								<dd class="ellipsis small"><span><?=$cmvolume_val['small_txt']?></span></dd>
								<dd class="ellipsis"><?=$cmvolume_val['cm_professional_info']?></dd>
							</dl>
						</div>
					</a>
					<? } /* foreach($cmvolume_arr as $cmvolume_key => $cmvolume_val){  */ ?>
				</div>
			</div>
		</div>
		<?}?>