<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/event.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/event.js<?=vs_para();?>"></script>
		<style type="text/css">
			.event_list_bg{ background-color: <?=$eme_bgcolor;?>; }
		</style>

			<div class="event_list_bg"> <!-- 배경색 사용자 정의 -->
				</div> <!-- /event_list_bg -->
				<div class="event_list_con">
					<div class="event_list_title">
						<img src="<?=$ep_collection_top_url;?>" alt="이벤트Top" />
					</div>
					<div class="event_detail_list" id="event_list">
					<? if (count($list_array) <= 0) { ?>
						<div class="only_adult" align="center">성인전용 이벤트 입니다.</div>
					<? } else {
						foreach($list_array as $key => $val) { ?>
						<dl>
							<dt><a href="<?=get_comics_url($val['cm_no']);?>"><img src="<?=$val['comics_img_url']?>" alt="<?=$val['cm_series'];?>"/></a></dt>
							<dt class="event_detail_series"><?=$val['cm_series'];?></dt>
							<dt><button onclick="location.href='<?=$val['comics_url'];?>'">첫화보기</button></dt>
							<dt><?=$val['cp_name'];?></dt>
							<dd><?=stripslashes($val['cm_somaery']);?></dd>
						</dl>
					<? 
						} // end foreach 
					}
					?>
					</div>
				</div>
			</div> <!-- /contents -->