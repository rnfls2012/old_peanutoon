<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/cs.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/cs.js<?=vs_para();?>"></script>
			<div class="container_wrap">
				<h4><span><i class="fa fa-comments-o" aria-hidden="true"></i> <?=$nm_config['cf_title'];?></span> 고객센터</h4>
				<div class="cs_left">
					<ul class="tabs">
						<li class="active">
							<a href="<?=NM_URL."/cscenter.php?menu=1";?>"> <span><i class="fa fa-volume-up" aria-hidden="true"></i></span><br />공지사항</a>
						</li>
						<li>
							<a href="<?=NM_URL."/cscenter.php?menu=2";?>"><span><i class="fa fa-question" aria-hidden="true"></i></span><br />자주 하는 질문</a>
						</li>
						<li>
							<a href="<?=NM_URL."/cscenter.php?menu=3";?>"><span><i class="fa fa-user-o" aria-hidden="true"></i></span><br />1:1 문의</a>
						</li>
						<li>
							<a href="<?=NM_URL."/cscenter.php?menu=4";?>"><span><i class="fa fa-book" aria-hidden="true"></i></span><br />연재문의</a>
						</li>
					</ul>
				</div>

				<div class="cs_contents">
					<div class="tab_container">
						<div id="tab1" class="tab_content">
							<!-- 공지사항 _ 게시판 형태 -->
							<table class="cs_view" cellpadding="0" cellspacing="0">
								<tr>
									<th>No</th>
									<td class="v_num"><?=$notice_view['bn_no'];?></td>
									<td class="v_title"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><?=$notice_view['bn_title'];?></td>
									<td class="v_date"><?=substr($notice_view['bn_date'],0 ,10);?></td>
								</tr>
								<tr>
									<td colspan="4" class="v_content">
										<?=$notice_view['bn_text'];?>
									</td>
								</tr>
							</table>
								<a href="javascript:history.back()"><button type="button" class="cs_notice_list">목록으로</button></a>
							</div>
							<!-- #tab1 -->
						</div>
						<!-- .tab_container -->
					</div>

				</div>
			</div> <!-- /contents -->