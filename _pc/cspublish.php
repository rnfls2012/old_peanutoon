<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/cs.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/cs.js<?=vs_para();?>"></script>
			<div class="container_wrap">
				<h4><span><i class="fa fa-comments-o" aria-hidden="true"></i> <?=$nm_config['cf_title'];?></span> 고객센터</h4>
					<div class="cs_left">
						<ul class="tabs">
							<li>
								<a href="<?=NM_URL."/cscenter.php?menu=1";?>"> <span><i class="fa fa-volume-up" aria-hidden="true"></i></span><br />공지사항</a>
							</li>
							<li>
								<a href="<?=NM_URL."/cscenter.php?menu=2";?>"><span><i class="fa fa-question" aria-hidden="true"></i></span><br />자주 하는 질문</a>
							</li>
							<li>
								<a href="<?=NM_URL."/cscenter.php?menu=3";?>"><span><i class="fa fa-user-o" aria-hidden="true"></i></span><br />1:1 문의</a>
							</li>
							<li class="active">
								<a href="<?=NM_URL."/cscenter.php?menu=4";?>"><span><i class="fa fa-book" aria-hidden="true"></i></span><br />연재문의</a>
							</li>
						</ul>
					</div>
				<div class="cs_contents">


					<div class="tab_container">
						<div id="tab3" class="cs_publish">
							<form name="cspublishwrite" id="cspublishwrite" enctype="multipart/form-data" method="post" action="<?=NM_PROC_URL;?>/cspublishwrite.php" onsubmit="return cspublishwrite_submit();">
								<div class="cs_p_notice">
									<span><i class="fa fa-paw" aria-hidden="true"></i> <?=$nm_config['cf_title'];?> 연재문의 안내</span><br />
									포트폴리오(선택)와 3화 이상의 원고(필수)와 약식 이력서(이름 / 연락처 / 나이 / 경력)를 .zip파일 형식으로 압축하여 보내주시면 검토 후 연락드리겠습니다.
									<br/>
									감사합니다.
								</div>
								<fieldset>
										<input type="hidden" id="bp_member" name="bp_member" value="<?=$nm_member['mb_no'];?>" />
										<input type="hidden" id="bp_member_idx" name="bp_member_idx" value="<?=$nm_member['mb_idx'];?>" />
										<input type="hidden" id="bp_date" name="bp_date" value="<?=substr(NM_TIME_YMDHIS, 0, 16);?>" />

										<input type="text" id="bp_title" name="bp_title" placeholder="제목을 입력하세요." />
										<textarea id="bp_text" name="bp_text" placeholder="문의 내용을 입력하세요."></textarea>
										<input type="text" id="bp_return" name="bp_return" placeholder="연락 받을 메일주소를 입력하세요." />
										<input type="file" name="bp_file" id="bp_file"/>
										<input type="submit" value="문의하기">
										<br>
										<span class="zip">zip형식으로 압축하여 첨부해주세요.</span>
								</fieldset>
							</form>
						</div>
						<!-- #tab3 -->
					</div>
					<!-- .tab_container -->
				</div>

			</div>