<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/eventsummer.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/eventsummer.js<?=vs_para();?>"></script>
		<style type="text/css">
			.evt-container {
				background-color: #48c4ff;
				background-image:  url(<?=$summer_img;?>/bg02.png<?=vs_para();?>), url(<?=$summer_img;?>/bg01.png<?=vs_para();?>);
				background-repeat: no-repeat;
				background-size: auto;
				background-position: right top, left top, center top, left top;
			}
			.evt-content .c03 {
				background-image: url(<?=$summer_img;?>/bg07.png<?=vs_para();?>);
				background-size: auto;
				background-position: center;
				background-repeat: no-repeat;
			}
			.evt-content .evt-coupon {
				background-image: url(<?=$summer_img;?>/bg03.png<?=vs_para();?>);
				background-size: auto;
				background-position: bottom;
				background-repeat: repeat-x;
			}
			.evt-content .coupon {
				background-image: url(<?=$summer_img;?>/c03_couponbg.png<?=vs_para();?>);
				background-repeat: no-repeat;
			}
			.evt-content .c04:before {
				background-image: url(<?=$summer_img;?>/bg08.png<?=vs_para();?>);
			}
			.ModalPopup {
				background-image: url(<?=$summer_img;?>/bg12.png<?=vs_para();?>);
				background-repeat: no-repeat;
				background-size: auto;
				background-color: #5ecbff;
			}
			.ModalPopup .p_content {
				background-image: url(<?=$summer_img;?>/bg13.png<?=vs_para();?>);
				background-repeat: no-repeat;
			}

		</style>
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<input type="hidden" id="member_no" class="member_no" value="<?=$nm_member['mb_no'];?>" />
		<input type="hidden" id="total_coupon" class="total_coupon" value="<?=$total_coupon;?>" />

		<!-- Modal Popup -->
		<div class="Modalalert">
			<div class="ModalBackground"></div>
			<div class="ModalPopup">
				<div class="ModalClose"><i class="material-icons">clear</i></div>
				<div class="p_content">
					<div class="p_coupon">
						<img src="<?=$summer_img;?>/p_title.png<?=vs_para();?>" alt="15% 결제 할인 쿠폰">
					</div>
					<div class="p_get">
						<img src="<?=$summer_img;?>/p_warn.png<?=vs_para();?>" alt="쿠폰 사용 유의사항">
					</div>
					<ul>
						<li><button id="recharge_link" class="p_buy" onclick="location.href='<?=NM_URL?>/recharge.php'">결제하기</button></li>
						<li><button class="p_couponpage" onclick="location.href='<?=NM_URL?>/coupon.php'">쿠폰 보기</button></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- /Modal Popup -->

		<div class="evt-container">
			<div class="evt-title">
				<img src="<?=$summer_img;?>/c01_title.png<?=vs_para();?>" alt="피너툰 SUMMER EVENT">
			</div>
			<div class="evt-content">
				<div class="evt-coupon">
					<div class="c02"><img src="<?=$summer_img;?>/c02_con.png<?=vs_para();?>" alt="이벤트 설명"></div>
					<div class="c03">
						<div class="coupon">
							<div class="coupon-badge"><img src="<?=$summer_img;?>/c03_couponbadge.png<?=vs_para();?>" alt="선착순 200명" /></div>
							<img src="<?=$summer_img;?>/c03_coupontext.png<?=vs_para();?>">
							<p>남은 수량 : <span class="value11"><?=$total_coupon;?></span>장</p>
						</div>
						<!-- 버튼 발급 비활성화 시 nochk 클래스 추가 -->
						<button id="open_btn" class="<?=$nochk;?>"><?=$coupon_off_text;?></button>
						<span class="couponwarn"><img src="<?=$summer_img;?>/c03_couponwarn.png<?=vs_para();?>" alt="1일 선착순 발급 완료 시 쿠폰 발급이 조기 마감됩니다."></span>
					</div>
				</div>
					<div class="c04">
						<img src="<?=$summer_img;?>/c04_warn.png<?=vs_para();?>" alt="이벤트 유의사항">
					</div>
				</div>
			</div>
		</div>