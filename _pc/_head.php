<? include_once '_common.php'; // 공통
	include_once(NM_PC_PATH."/_head.sub.php");
	if(NM_INDEX){ include_once(NM_PC_PATH."/_popup.php"); /* 모바일팝업 */ }
?>
<!-- head.html -->
<!-- body space -->
	<!-- wrapper space -->
		<?/* autocomplete 참조
		1. http://jqueryui.com/autocomplete/#custom-data
		2. http://sway.tistory.com/entry/jqueryui-autocomplete%EC%97%90%EC%84%9C-json-%EB%8D%B0%EC%9D%B4%ED%84%B0%EA%B0%80-null%EC%9D%BC-%EB%95%8C-%EC%B2%98%EB%A6%AC
		3. http://stackoverflow.com/questions/26636873/jquery-ui-autocomplete-widget-and-renderitem-in-chrome
		4. http://astrap.tistory.com/275
		5. http://aramk.tistory.com/35
		6. http://m.blog.naver.com/mazineta/220319244615

		1. http://m.todayhumor.co.kr/view.php?table=total&no=9808422

		1.커서이동 http://m.blog.daum.net/bang2001/110

		http://www.w3ii.com/ko/jqueryui/jqueryui_autocomplete.html

		파일 jquery.autocomplete-ui.js
		// this.close( event ); // 수정
		5916
		6222

		커서문제 나중에 처리하기
		ex 선생님 입력 도중 커서가 가운데 있고 down하면  오류결과

		*/

		/* SNS URL */		
		$social_oauth_url = NM_OAUTH_URL.'/login.php?service=';
		$twiter_oauth_url = NM_OAUTH_URL.'/twitterlogin.php';

		/* SNS URL- 18-12-03 수정 */	
		$social_oauth_url = NM_OAUTH_URL.'/login.php?'.get_add_para_redirect().'&service=';
		$twiter_oauth_url = NM_OAUTH_URL.'/twitterlogin.php?'.get_add_para_redirect();
		
		/* 코믹스 최근 본 작품 가져오기 - PC만 사용해서; */
		$mb_buyrecent_arr = array();
		$mb_buyrecent_comics_where = "AND mbc.mbc_member='".$nm_member['mb_no']."' AND mbc.mbc_member_idx='".$nm_member['mb_idx']."' AND mbc_recent = 'n' order by mbc_date desc LIMIT 0, 3";
		$sql_mb_buyrecent = "select * from member_buy_comics mbc 
							left JOIN comics c ON mbc.mbc_comics = c.cm_no
							left JOIN comics_professional cp ON cp.cp_no = c.cm_professional
							where 1 $mb_buyrecent_comics_where";
		$result_mb_buyrecent = sql_query($sql_mb_buyrecent);
		while($row_mb_buyrecent = sql_fetch_array($result_mb_buyrecent)) {
			array_push($mb_buyrecent_arr, $row_mb_buyrecent);
		} // end while

		
		
		/* 코믹스 소장작품중 업데이트된 작품 가져오기 - PC만 사용해서; */
		$mb_buyown_arr = $mb_buyown_cm_no_list_arr = array();
		$mb_buyown_comics_where = "AND mbc_member='".$nm_member['mb_no']."' AND mbc_member_idx='".$nm_member['mb_idx']."' AND mbc_view = 'n' AND mbc_own_type = '1' ";
		$sql_mb_buyown = "select * from member_buy_comics mbc where 1 $mb_buyown_comics_where";
		$result_mb_buyown = sql_query($sql_mb_buyown);
		while($row_mb_buyown = sql_fetch_array($result_mb_buyown)) {
			array_push($mb_buyown_cm_no_list_arr, $row_mb_buyown['mbc_comics']);
		} // end while

		$sql_mb_buyown_comic = "select * from comics c left JOIN comics_professional cp ON c.cm_professional = cp.cp_no 
								where cm_service = 'y' AND cm_no in (".implode(",",$mb_buyown_cm_no_list_arr).") 
								AND cm_episode_date >= '".NM_TIME_M1."' AND cm_episode_date <= '".NM_TIME_YMD."' 
								GROUP BY cm_no ORDER BY cm_episode_date DESC LIMIT 0, 3 ";
		$result_mb_buyown_comic = sql_query($sql_mb_buyown_comic);
		while($row_mb_buyown_comic = sql_fetch_array($result_mb_buyown_comic)) {
			array_push($mb_buyown_arr, $row_mb_buyown_comic);
		} // end while

		?>
		<link href="<?=NM_PC_URL?>/css/jquery.autocomplete.css" rel="stylesheet">
		<script type="text/javascript" src="<?=NM_PC_URL?>/js/jquery.autocomplete-ui.js"></script>

		<div id="header">
			<div id="gnb"> <!-- Global navigation bar -->
				<div class="gnbwrap">
					<div class="adult_tab">
						<a href="<?=$cs_url_adulty?>" class="<?=$adulty_on?>">성인</a>
						<a href="<?=$cs_url_adultn?>" class="<?=$adultn_on?>">비성인</a>
					</div>
					<div class="myinfo">
						<ul>
							<li class="ct_login"><?=$nm_member['cs_user_menu_top_text'];?></li>
							<? if($_SESSION['ss_mb_id'] == ""){ ?>
								<li><a href="<?=NM_URL."/ctjoin.php";?>?<?=get_add_para_redirect();?>" class="cs_center">회원가입</a></li>
							<? } ?>
							<? if($_SESSION['ss_adm_id']){ ?>
								<li><a href="<?=NM_PROC_URL."/ss_mb_id.php";?>" class="cs_center">관리자모드</a></li>
							<? } ?>
							<? if(is_nexcube()){?>
							<li><?=substr($_SERVER['SERVER_ADDR'],strlen($_SERVER['SERVER_ADDR'])-3,3);?>서버</li>
							<? } ?>
						</ul>
					</div>
					<div class="mymenu">
						<ul>
							<li><a href="<?=NM_URL."/recharge.php";?>"><span><?=$d_logmenu['recharge'];?></span></a></li>
							<li><a href="<?=NM_URL."/cscenter.php";?>">고객센터</a></li>
							<li><a href="<?=NM_URL."/myinfo.php";?>">마이페이지</a></li>
							<? if(is_nexcube()){ ?>
								<li><button onclick="mode_chage('Mobile');">Mobile Ver</button></li>
							<? } ?>
						</ul>
					</div>
				</div>
			</div> <!-- /gnb -->
			
			<div id="lnb">
				<div class="lnbwrap">
					<div class="logo"><a href="<?=NM_URL?>"><img src="<?=NM_IMG?><?=$nm_config['cf_web_logo'];?>" alt="<?=$nm_config['cf_title'];?>" /></a></div>

					<nav> <!-- Local navigation bar -->						
						<ul>
						<? foreach($lnb_arr as $lnb){ ?>
							<li class="<?=$lnb['cn_current'];?>"><a href="<?=$lnb['cn_lnb_link']?>" target="<?=$lnb['cn_target']?>"><?=$lnb['cn_name']?></a></li>
						<? } /* foreach($lnb_arr as $lnb){ */ ?>
						</ul>
					</nav>
					
					<div class="user">
						<div id="search_list"> <!-- Search -->
							<form id="search_result" name="search_result" method="GET" action="<?=NM_URL?>/search.php">
								<input type="text" id="search_txt" name="search_txt" placeholder="작품, 작가명 검색" autocomplete="off" value="<?=$search_txt;?>">
								<button type="submit" class="searchBtn"><i class="fa fa-search" aria-hidden="true"></i></button>
							</form>
						</div> <!-- /Search -->
						<div class="members"> <!-- Log-in/Member-info Icon -->
							<button onclick="usermenu();">
								<img src="<?=NM_PC_IMG?>common/usermenu.png" alt="회원메뉴">
							</button>
						</div> <!-- /Log-in/Member-info  Icon -->
					</div>

					<? if($is_member == false){ /* 로그인전 */?>
						<div class="usermenu"> <!-- 로그인버튼 클릭시 -->
							<div class="usermenu_tabs">
								<input id="tab01" type="radio" name="usermenutab" value="login" class="tabselector01 hidden" checked="checked" />
								<label for="tab01" class="tab-label-1">로그인</label>

								<input id="tab02" type="radio" name="usermenutab" value="registration" class="tabselector02 hidden" />
								<label for="tab02" class="tab-label-2">회원가입</label>
							</div>

							<div class="usermenu_contents">
								<div class="tabcontents login on">
									<form name="ctlogin_form" id="ctlogin_form" method="post" action="<?=NM_PROC_URL;?>/ctlogin.php" onsubmit="return ctlogin_submit();">
										<input type="hidden" name="http_referer" value="<?=NM_URL.$_SERVER['REQUEST_URI'];?>"/>
										<input type="hidden" name="redirect" value="<?=$_redirect;?>"/>
										<fieldset>
											<input type="text" id="login_id" name="login_id" placeholder="아이디" value="" />
											<input type="password" id="login_pw" name="login_pw" placeholder="비밀번호" value="" />
											<label class="login_rememberme">
												<input type="checkbox"  id="login_auto" name="login_auto_save" value="y"/>
												<span>로그인 상태 유지</span>
											</label>
											<span class="findidpw"><a href="./ctmyfind.php?<?=get_add_para_redirect();?>" id="myBtn">아이디 / 비밀번호 찾기</a></span>
											<input type="submit" id="log-in" name="log-in" value="로그인">
										</fieldset>

										<hr />

										<div class="login_or"> <!-- 다른곳 아이디로 로그인 -->
											<ul>
												<li class="facebook"><a href="<?=$social_oauth_url?>facebook"><i class="fa fa-facebook" aria-hidden="true"></i> 페이스북 아이디로 로그인</a></li>
												<li class="twitter"><a href="<?=$social_oauth_url?>twitter"><i class="fa fa-twitter" aria-hidden="true"></i> 트위터 아이디로 로그인</a></li>
												<li class="kakao"><a href="<?=$social_oauth_url?>kakao"><img src="<?=NM_PC_IMG?>ct/login_kakao.png" alt="카카오 로고" /> 카카오 아이디로 로그인</a></li>
												<li class="naver"><a href="<?=$social_oauth_url?>naver"><img src="<?=NM_PC_IMG?>ct/login_naver.png" alt="네이버 로고" /> 네이버 아이디로 로그인</a></li>
												<!--<li><a href="<?=$social_oauth_url?>google"><img src="<?=NM_MO_IMG?>ct/login_naver.png" alt="네이버 로고" /> 구글 아이디로 로그인</a></li>-->
											</ul>
										</div>
										<div class="login_help">
											문제가 있거나 궁금한 점이 있으시면<br />
											<a href="mailto:<?=$nm_config['cf_admin_email'];?>"><?=$nm_config['cf_admin_email'];?></a> 으로 문의주시기 바랍니다.
										</div>
									</form>
								</div>

								<div class="tabcontents registration">
									<fieldset>									
										<form name="ctjoin_form" id="ctjoin_form" method="post" action="<?=NM_PROC_URL;?>/ctjoin.php" onsubmit="return ctjoin_submit();">
											<input type="hidden" name="http_referer" value="<?=NM_URL.$_SERVER['REQUEST_URI'];?>"/>
											<input type="hidden" name="redirect" value="<?=$_redirect;?>"/>

											<input type="text" id="join_id" name="join_id" placeholder="아이디(E-Mail)" value="" />
											<input type="password" id="join_pw" name="join_pw" placeholder="비밀번호" value="" />
											<ol>
												<li>
													<label class="regi_agree" for="regi_agree"><input type="checkbox" id="regi_agree" /><span>전체동의</span></label>

												</li>
												<li><label class="regi_terms" for="regi_terms"><input type="checkbox" id="regi_terms" name="regi_terms" /><span>이용약관에 동의합니다.</span></label><a href="<?=NM_URL?>/siteterms.php" class="terms">상세보기</a></li>

												<li><label class="regi_pinfo" for="regi_pinfo"><input type="checkbox" id="regi_pinfo" name="regi_pinfo" /><span>개인정보취급방침에 동의합니다.</span></label><a href="<?=NM_URL?>/siteprivacy.php" class="terms">상세보기</a></li>

												<li><label class="regi_pinfo" for="regi_event"><input type="checkbox"id="regi_event" name="regi_event" /><span>정보/이벤트 메일 수신 동의합니다.(선택)</span></label></li>
											</ol>

											<a href="#" class="layer_pop_center_show"><input type="submit" id="regi_submit" name="regi_submit" value="회원가입"></a>
											<span class="regi_notice"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> 잘못된 이메일을 입력하시면 아이디/패스워드 찾기가<br />불가능합니다.</span>

										</form>
									</fieldset>
								</div>
							</div>
						</div> <!-- /usermenu -->

					<? // if($is_member == false){
					   }else{ /* 로그인후 */?>
						<div class="usermenu"> <!-- 로그인버튼 클릭시 -->
							<div class="usermenu_lists">
								<ul class="logmenu">
									<li><a href="<?=NM_URL."/mycomics.php";?>"><span><i class="fa fa-book" aria-hidden="true"></i></span><?=$d_logmenu['mycomics'];?></a></li>
									<li><a href="<?=NM_URL."/myinfo.php";?>"><span><i class="fa fa-user-circle-o" aria-hidden="true"></i></span><?=$d_logmenu['myinfo'];?></a></li>
									<li><a href="<?=NM_URL."/recharge.php";?>"><span><i class="fa fa-shopping-basket" aria-hidden="true"></i></span><?=$d_logmenu['recharge'];?></a></li>
									<?if($nm_member['mb_sex'] == 'n' || $nm_member['mb_class'] == 'a') {?>
									<li><a href="<?=NM_URL."/ctcertify.php";?>"><span><i class="fa fa-unlock" aria-hidden="true"></i></span><?=$d_logmenu['ctcertify'];?></a></li>
									<?}?>
									<li><a href="<?=NM_URL."/coupon.php";?>"><span><i class="fa fa-ticket" aria-hidden="true"></i></span><?=$d_logmenu['coupon'];?></a></li>
									<?if($is_member == true){?>
									<li><a href="<?=$nm_member['cs_login_url'];?>" class="login"><span><i class="fa fa-user-times" aria-hidden="true"></i></span><?=$nm_member['cs_login_text'];?></a></li>
									<?}?>
								</ul>

							

								<!-- 최근본 작품 -->
								<div class="comics_recent">
									<p class="recent">
										<span>
											<i class="fa fa-bookmark" aria-hidden="true"></i>
											<strong>최근 본 작품</strong>
										</span>
										<a class="more" href="<?=NM_URL?>/mycomics.php">
											<i class="fa fa-plus" aria-hidden="true"></i> 더보기 
										</a>
									</p>
	
									<? if(count($mb_buyrecent_arr) > 0){ ?>
										<ul>
											<? foreach($mb_buyrecent_arr as $mb_buyrecent_key => $mb_buyrecent_val) {
												$mb_buyrecent_img_url = img_url_para($mb_buyrecent_val['cm_cover_sub'], $mb_buyrecent_val['cm_reg_date'], $mb_buyrecent_val['cm_mod_date'], 100, 100); 
											?>
											<li>
												<a href="<?=get_comics_url($mb_buyrecent_val['cm_no']);?>"><img src="<?=$mb_buyrecent_img_url;?>" alt="<?=$mb_buyrecent_val['cm_series'];?>" width="100" height="100" /></a>
												<span class="tooltiptext"><?=$mb_buyrecent_val['cm_series'];?></span>
											</li>
											<? } ?>
										</ul>
									<? }else{ ?>
										<p class="recent_zero">최근 본 작품이 없습니다.</p>
									<? } // if(count($mb_buyrecent_arr) > 0){ ?>


								</div>

								<!-- 업데이트 된 작품 -->
								<div class="comics_recent">
									<p class="recent">
										<span>
											<i class="fa fa fa-bell" aria-hidden="true"></i>
											<strong>업데이트 된 소장 작품</strong>
										</span>
										<a class="more" href="<?=NM_URL?>/mycomics.php?mbc=2">
											<i class="fa fa-plus" aria-hidden="true"></i> 더보기 
										</a>
									</p>

									<? if(count($mb_buyown_arr) > 0){ ?>
										<ul>
											<? foreach($mb_buyown_arr as $mb_buyown_key => $mb_buyown_val) {
												$mb_buyown_img_url = img_url_para($mb_buyown_val['cm_cover_sub'], $mb_buyown_val['cm_reg_date'], $mb_buyown_val['cm_mod_date'], 100, 100); 
											?>
											<li>
												<a href="<?=get_comics_url($mb_buyown_val['cm_no']);?>"><img src="<?=$mb_buyown_img_url;?>" alt="<?=$mb_buyown_val['cm_series'];?>"  width="100" height="100" /></a>
												<span class="tooltiptext"><?=$mb_buyown_val['cm_series'];?></span>
											</li>
											<? } ?>
										</ul>
									<? }else{ ?>
										<p class="buyown_zero">업데이트 된 소장 작품이 없습니다.</p>
									<? } // if(count($mb_buyown_arr) > 0){ ?>
								</div>
							</div> <!-- /usermenu_contents -->
						</div> <!-- /user_menu -->
					<? } // if($is_member == false){ ?>
				</div> <!-- /lnbwrap -->
			</div> <!-- /lnb -->
		</div> <!-- /hearder -->

		<? if($lnb_sub_count > 0){ $container_lnb_sub = 'container_lnb_sub';}	?>
		<div id="container" class="<?=$container_lnb_sub;?>">

