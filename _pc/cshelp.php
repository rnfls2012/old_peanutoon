<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/cs.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/cs.js<?=vs_para();?>"></script>
		<!-- Tabbed 부분 보류
		<style type="text/css">
			/* tab */
			.cs_faq_tab ul li{width:<?=$lnb_width;?>%;}
			.cs_faq_tab ul li:first-child{width:<?=$lnb_width_first;?>%;}
		</style>
		-->

			<div class="container_wrap">
				<h4><span><i class="fa fa-comments-o" aria-hidden="true"></i> <?=$nm_config['cf_title'];?></span> 고객센터</h4>
				<div class="cs_left">
					<ul class="tabs">
						<li>
							<a href="<?=NM_URL."/cscenter.php?menu=1";?>"> <span><i class="fa fa-volume-up" aria-hidden="true"></i></span><br />공지사항</a>
						</li>
						<li class="active">
							<a href="<?=NM_URL."/cscenter.php?menu=2";?>"><span><i class="fa fa-question" aria-hidden="true"></i></span><br />자주 하는 질문</a>
						</li>
						<li>
							<a href="<?=NM_URL."/cscenter.php?menu=3";?>"><span><i class="fa fa-user-o" aria-hidden="true"></i></span><br />1:1 문의</a>
						</li>
						<li>
							<a href="<?=NM_URL."/cscenter.php?menu=4";?>"><span><i class="fa fa-book" aria-hidden="true"></i></span><br />연재문의</a>
						</li>
					</ul>
				</div>

				<div class="cs_contents">


					<div class="tab_container">
							<div id="tab2" class="tab_content">

							<div class="cs_faq_tab">
								<ul>
									<li class="<?=$cate_all_class;?>"><a href="<?=NM_URL."/cshelp.php";?>">전체</a></li>
									<li class="<?=$cate_mb_class;?>"><a href="<?=NM_URL."/cshelp.php?cate=1";?>">회원 관련</a></li>
									<li class="<?=$cate_pay_class;?>"><a href="<?=NM_URL."/cshelp.php?cate=2";?>">결제 관련</a></li>
									<li class="<?=$cate_used_class;?>"><a href="<?=NM_URL."/cshelp.php?cate=3";?>">서비스</a></li>
								</ul>
							</div>

							<!-- FAQ _ 아코디언 메뉴 형태 -->
							<dl class="cs_faq">
							<? foreach($faq_list as $faq_key => $faq_val) { ?>
								<dt>
									<ul>
										<li><?=$faq_val['bh_category_text'];?></li>
										<li><i class="fa fa-question-circle" aria-hidden="true"></i>
											<?=$faq_val['bh_title'];?>
										</li>
									</ul>
								</dt>

								<dd><?=stripslashes(nl2br($faq_val['bh_text']));?></dd>
							<? } // end foreach ?>
							</dl>
						</div>
						<!-- #tab2 -->
					</div>
					<!-- .tab_container -->
				</div>

			</div>



		</div> <!-- /contents -->