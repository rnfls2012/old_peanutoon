<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/eventspring.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/eventspring.js<?=vs_para();?>"></script>
		<style type="text/css">
			.evt-container {
				background-color: #fde7df;
				background-image: url(<?=$spring_img;?>/bg02.png<?=vs_para();?>), url(<?=$spring_img;?>/bg03.png<?=vs_para();?>), url(<?=$spring_img;?>/bg01.png<?=vs_para();?>);
				background-repeat: no-repeat;
				background-size: auto, auto, 100%;
				background-position: left top, right top, center top;
			}
			.evt-content-wrap {
				background-image:url(<?=$spring_img;?>/bg05.png<?=vs_para();?>), url(<?=$spring_img;?>/bg06.png<?=vs_para();?>), url(<?=$spring_img;?>/bg04.png<?=vs_para();?>);
				background-position: left, right, bottom;
				background-repeat: no-repeat;
				background-size: auto, auto, 100%;
			}
			.evt-content .coupon {
				background-image: url(<?=$spring_img;?>/con-coupon<?=$coupon_off;?>.png<?=vs_para();?>);
				background-repeat: no-repeat;
			}
			.evt-content .warn {
				background-image: url(<?=$spring_img;?>/con-line.png<?=vs_para();?>);
				background-position: center top;
				background-repeat: no-repeat;
			}
			.ModalPopup {
				background-image: url(<?=$spring_img;?>/p_bg.png<?=vs_para();?>);
				background-repeat: no-repeat;
				background-color: #f5d7d7;
			}
			.ModalPopup .p_content .p_coupon {
				background-image: url(<?=$spring_img;?>/p_coupon.png<?=vs_para();?>);
				background-repeat: no-repeat;
				background-position: center;
			}
			.sakura{
				background:url(<?=$spring_img;?>/cherryblossom.png<?=vs_para();?>);
				background:url(<?=$spring_img;?>/cherryblossom.png<?=vs_para();?>);
				pointer-events:none;
				position:absolute; 
				background-size: 100%;
				background-repeat: no-repeat;
			}
		</style>
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<input type="hidden" id="member_no" class="member_no" value="<?=$nm_member['mb_no'];?>" />
		<input type="hidden" id="total_coupon" class="total_coupon" value="<?=$total_coupon;?>" />

		<!-- Modal Popup -->
		<div class="Modalalert">
			<div class="ModalBackground"></div>
			<div class="ModalPopup">
				<div class="ModalClose"><i class="material-icons">clear</i></div>
				<div class="p_content">
					<div class="p_coupon">
						<p><span class="coupon_value"></span>%</p>
						<span class="coupon_time"><span class="coupon_value02"></span>일까지 사용가능</span>
					</div>
					<div class="p_get">
						<span class="coupon_value"></span><span>%</span> 결제 할인권 발급!
					</div>
					<div class="p_exp">
						<ol>
							<li>해당 쿠폰은 중복 사용이 불가합니다.</li>
							<li>해당 쿠폰은 9,900원 이상 상품부터 적용 가능합니다.</li>
							<li>해당 쿠폰은 중복 할인이 불가합니다.</li>
							<li>해당 쿠폰의 사용기간은 2018년 3월 31일까지 입니다.</li>
						</ol>
						<img src="<?=$spring_img;?>/p_info.png<?=vs_para();?>" alt="당첨된 쿠폰은 [쿠폰함] 에서 확인 가능합니다.">
					</div>
					<ul>
						<li><button id="recharge_link" class="p_buy" onclick="location.href='<?=NM_URL?>/recharge.php'">결제하기</button></li>
						<li><button class="p_couponpage" onclick="location.href='<?=NM_URL?>/coupon.php'">쿠폰 보기</button></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- /Modal Popup -->

		<div class="evt-container">
			<div class="evt-title">
				<img src="<?=$spring_img;?>/title_copy.png<?=vs_para();?>" alt="봄 이벤트">
			</div>
			<div class="evt-content-wrap">
				<div class="evt-content">
					<img src="<?=$spring_img;?>/con-info.png<?=vs_para();?>">
					<div class="coupon">
						<div class="coupon-badge"><img src="<?=$spring_img;?>/con-coupon-badge.png<?=vs_para();?>" alt="선착순 200명" /></div>
						<img src="<?=$spring_img;?>/con-coupon-text.png<?=vs_para();?>" alt="땅콩 결제 15% 할인 쿠폰">
						<p>남은 수량 : <span class="value11"><?=$total_coupon;?></span>장</p>
					</div>
					<!-- 버튼 발급 비활성화 시 nochk 클래스 추가 -->
					<button id="open_btn" class="<?=$nochk;?>"><?=$coupon_off_text;?></button>
					<span><img src="<?=$spring_img;?>/con-info02.png<?=vs_para();?>"></span>
					<div class="warn">
						<img src="<?=$spring_img;?>/warn.png<?=vs_para();?>">
					</div>
				</div>
			</div>
		</div>