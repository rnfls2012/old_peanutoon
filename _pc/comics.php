<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/comics.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/comics.js<?=vs_para();?>"></script>
		<script type="text/javascript"> 
		<!-- 
			<?/* 고객 포인트 정보 + 전체구매시 추가지급 정보 */?>
			var total_point = "<?=$mb_sum_point?>";
			var sin_allpay_arr = <?=$sin_allpay_arr_js;?>;
			var episode_btn_check = true;
		//-->
		</script>
		<div id="comics"> <!-- 상단 작품배너와 작품소개 -->
			<div class="comics_bg" style="background:url('<?=$comics['cm_cover_url'];?>') no-repeat center 50%; background-size: cover;"> <!-- 타이틀 영역 -->
			</div>
			<div class="comics_contents">
				<img src="<?=$comics['cm_cover_url'];?>" alt="<?=$comics['cm_series'];?>">				
				<div class="comics_info"> <!-- 타이틀&작품설명 -->
					<dl>
						<dt><h3><?=stripslashes($comics['cm_series']);?></h3></dt>
						<dd class="prof">
 		          <a href="<?=NM_DOMAIN?>/search.php?search_txt=<?=$comics['cm_professional_info'];?>">
	    	        <?=$comics['cm_professional_info'];?>
    	        </a>
            </dd>
						<dd class="small">
            	<a href="<?=NM_DOMAIN?>/cmbest.php?small=<?=$comics['cm_small'];?>"><?=$small_txt;?></a>
            </dd>
						<dd class="cm_somaery"><?=stripslashes($comics['cm_somaery']);?></dd>
					</dl>
					<ul> <!-- 하단 기능버튼 -->
						<li class="first"><button onclick="link('<?=$cs_comic_first_url;?>');"><i class="fa fa-map-marker" aria-hidden="true"></i> 처음부터</button></li>
						<li id="mybookmark" class="" data-mybookmark="<?=$count_bookmark;?>"><button onclick="set_bookmark('<?=$comics['cm_no'];?>');"><i class="fa fa-star-o" aria-hidden="true"></i></button></li>
						<li id="myshare"><button href="#"><i class="fa fa-share-alt" aria-hidden="true"></i></button></li>
						<!-- 공유기능은 나중에;; -->
					</ul>
				</div> <!-- /title_contents -->
			</div>
			<div class="comics_episode">
				<div class="episode_bg">
					<div class="episode_left">
						<div class="list_top"> <!-- 리스트 상단 -->
							<div class="list_day"><?=$cm_public_cycle_arr_txt;?></div>
							<ul> <!-- 리스트 정렬 -->
								<li class="<?=$comics_order_asc;?>">
									<a href="<? echo get_comics_url($comics['cm_no'])."&order=ASC";?>" target="_self" class="<?=$comics_order_asc;?>">
										<i class="fa fa-check" aria-hidden="true"></i> <span>정주행</span>
									</a>
								</li>
								<li class="<?=$comics_order_desc;?>">
									<a href="<? echo get_comics_url($comics['cm_no'])."&order=DESC";?>" target="_self" class="<?=$comics_order_desc;?>">
										<i class="fa fa-check" aria-hidden="true"></i> <span>최신순</span>
									</a>
								</li>
							</ul>
						</div> <!-- /list_top -->
						<? if($purchased_cnt > 0) { ?>
						<div class="all_buy"><button onclick="check_enalble();">유료화 전체구매하기</button></div>
						<? } // end if ?>
						<div class ="detail_allby clear">
							<input type="hidden" name="cm_no" id="cm_no" value="<?=$comics['cm_no']?>">
							<div class="d_a_chk">
								<div class="detail_chk">
									<input type="checkbox" id="sin_allpay" name="sin_allpay" onchange="sin_allpay()" />
									<label for="sin_allpay"></label>
								</div>
								<label class="sin_allpay_btn" for="sin_allpay">전체선택</label>
							</div>
							<div class="d_a_ok">
								<button onclick="check_buy();">구매하기</button>
							</div>
							<div class="d_a_cancel">
								<button onclick="check_cancel();" class="cancel">취소</button>
							</div>
							<div class="d_a_bonus">
								<span class="d_a_b_0"> 
									총 <span id="chapter_count">0</span>화 
								</span>
								<span class="d_a_b_1">
									<span id="cach_point_sum">0 </span><?=$nm_config['cf_cash_point_unit_ko'];?>
								</span><br />
								<span class="d_a_b_2">
									<span id="point_service">0</span> <?=$nm_config['cf_point_unit_ko'];?> 보너스!
								</span>
							</div>
						</div>

						<div class ="episode_list clear">
							<ol>
								<? foreach($episode_arr as $episode_key => $episode_val){ 
									$db_ce_title_txt = text_change($episode_val['ce_title_txt'], "]");
								?>
								<li class="<?=$episode_val['ce_check']?>">
									<?=$episode_val['ce_url'];?>
										<div class="detail_chk">
											<input type="checkbox" id="episode<?=$episode_val['ce_no'];?>" class=" <?=$episode_val['ce_disabled'];?>" <?=$episode_val['ce_disabled'];?> name="episode[]" value="<?=$episode_val['ce_no'];?>" data-cash_point="<?=$episode_val['ce_pay']?>" />
											<label class="<?=$episode_val['ce_disabled'];?>" for="episode<?=$episode_val['ce_no'];?>"></label>
										</div>
										<div class="detail_thumb <?=$episode_val['ce_icon_up']?> <?=$episode_val['ce_bookmark']?>">
											<img src="<?=$episode_val['ce_img_url']?>" alt="<?=$episode_val['cm_series']?>" />
											<div class="icon_update"><img src="<?=NM_PC_IMG?>common/icon_up.png" alt="update" /></div> <!-- 업데이트 아이콘 -->
											<div class="icon_bookmark"><img src="<?=NM_IMG?><?=$nm_config['cf_web_bookmark']?>" alt="bookmark" /></div> <!-- 북마크 아이콘 -->
										</div>
										<div class="detail_num"><?=$episode_val['ce_chapter_view'];?></div>
										<div class="list_contents">
											<span class="detail_etc"><?=$episode_val['ce_outer_txt'];?></span><br />
											<span class="detail_title"><?=$db_ce_title_txt?></span><br />
											<?=$episode_val['ce_service_date_10']?>
										</div>
										<div class="buy"><?=$episode_val['ce_btn']?></div>
									</a>
								</li>
								<? } ?>
							</ol>
						</div>
					</div>
					<div class="episode_banner">
						<? include NM_PC_PART_PATH.'/recommend.php'; // Web 추천작품?>
						<? include NM_PC_PART_PATH.'/comicsbanner.php'; // [Web]화리스트배너?>
					</div>
				</div>
			</div>
		</div>
