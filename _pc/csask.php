<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/cs.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/cs.js<?=vs_para();?>"></script>
		
		<div class="container_wrap">
			<h4><span><i class="fa fa-comments-o" aria-hidden="true"></i> <?=$nm_config['cf_title'];?></span> 고객센터</h4>
			<div class="cs_left">
				<ul class="tabs">
					<li>
						<a href="<?=NM_URL."/cscenter.php?menu=1";?>"> <span><i class="fa fa-volume-up" aria-hidden="true"></i></span><br />공지사항</a>
					</li>
					<li>
						<a href="<?=NM_URL."/cscenter.php?menu=2";?>"><span><i class="fa fa-question" aria-hidden="true"></i></span><br />자주 하는 질문</a>
					</li>
					<li class="active">
						<a href="<?=NM_URL."/cscenter.php?menu=3";?>"><span><i class="fa fa-user-o" aria-hidden="true"></i></span><br />1:1 문의</a>
					</li>
					<li>
						<a href="<?=NM_URL."/cscenter.php?menu=4";?>"><span><i class="fa fa-book" aria-hidden="true"></i></span><br />연재문의</a>
					</li>
				</ul>
			</div>

			<div class="cs_contents">
				<div class="tab_container">
					<div id="tab2" class="tab_content">
						<!-- FAQ _ 아코디언 메뉴 형태 -->
						<dl class="cs_ask">
						<? foreach($ask_list as $ask_key => $ask_val) { ?>
							<dt>
								<ul>
									<li><?=$ask_category[$ask_val['ba_category']];?></li>
									<li><?=$ask_val['ba_title'];?></li>
									<li class="<?=($ask_val['ba_mode']==0)?'waiting':'';?>">
										<?=$ask_state[$ask_val['ba_mode']];?>
									</li>
									<li><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> </li>
								</ul>
							</dt>

							<dd>
								<ol>
									<li>
										<span><i class="fa fa-question" aria-hidden="true"></i> 문의 내용</span>
										<?=nl2br($ask_val['ba_request']);?>
									</li>
									<? if($ask_val['ba_mode']!=0) { ?>
									<li>
										<span><i class="fa fa-quote-left" aria-hidden="true"></i> 답변 내용</span>
										<p><?=nl2br($ask_val['ba_admin_answer']);?></p>
									</li>
									<? } // end if ?>
								</ol>
							</dd>
						<? } // end foreach ?>
						</dl>
						<? if(count($ask_list) == 0) { ?>
							<div class="cs_none">
								<ul>
									<li></li>
									<li><span class="exclamation"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span><br/>
									<span>문의하신 내역이 없습니다.</span><br />
									문제가 있거나 궁금한 점이 있으시면 언제든지 문의해주세요. 신속하게 답변드리도록 하겠습니다<br/></li>
									<li></li>
								</ul>
								<button type="button" class="cs_write" onclick="location.href='cscenter.php?menu=3&mode=w'">문의하기</button>
							</div>
						<? } else { ?>
						<button type="button" class="cs_write" onclick="location.href='cscenter.php?menu=3&mode=w'">문의하기</button>
						<? } // end else ?>
					</div>
					<!-- #tab2 -->
				</div>
				<!-- .tab_container -->
			</div>
		</div>
	</div> <!-- /contents -->