<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<style type="text/css">
			.evt-container {
				position: relative;
				width: 100%;
				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#4066a6+0,a06a9c+44,edc984+70,afebdf+100 */
				background: #4066a6; /* Old browsers */
				background: -moz-linear-gradient(top, #4066a6 0%, #a06a9c 44%, #edc984 100%); /* FF3.6-15 */
				background: -webkit-linear-gradient(top, #4066a6 0%,#a06a9c 44%,#edc984 100%); /* Chrome10-25,Safari5.1-6 */
				background: linear-gradient(to bottom, #4066a6 0%,#a06a9c 44%,#edc984 100%,); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4066a6', endColorstr='#afebdf',GradientType=0 ); /* IE6-9 */
				overflow:hidden;
			}
			.evt-title {
				position: relative;
				width: 100%;
				background-image:url(<?=$drama_img?>/bg02.png<?=vs_para();?>), url(<?=$drama_img?>/bg01.png<?=vs_para();?>);
				background-position:right 115px, left 115px;
				background-size: auto;
				background-repeat:no-repeat;
			}
			.evt-maincopy {
				width: 100%;
				padding: 80px 0 100px 0;
				text-align: center;
				display: block;
			}
			.evt-exp {
				display: block;
				width: 100%;
				text-align: center;
				margin-top: 95px;
				display: block;
			}
			.evt-content {
				width:100%;
				text-align: center;
				padding-bottom: 100px;
				background-image:url(<?=$drama_img?>/bg03.png<?=vs_para();?>);
				background-size: auto;
				background-position: center bottom;
				background-repeat: no-repeat;
			}
			.evt-content .evt-link {
				width: 1245px;
				margin: 0 auto;
			}
			.evt-content .evt-warn {
				width: 100%;
				padding: 250px 0;
				text-align: center;
			}
		</style>
		
		<div class="evt-container">
			<div class="evt-title">
				<div class="evt-maincopy">
					<img src="<?=$drama_img?>/header.png<?=vs_para();?>" alt="6월 9일 단 하루! 6/9DAY를 위한 특별한 2땅콩 할인 이벤트" />
				</div>
			</div>
			<div class="evt-content">
				<div class="evt-link">
					<a href="<?=get_comics_url(3027);?>"><img src="<?=$drama_img?>/link01.png<?=vs_para();?>" alt="7의 잔재" /></a>
					<a href="<?=get_comics_url(2991);?>"><img src="<?=$drama_img?>/link02.png<?=vs_para();?>" alt="도시괴담" /></a>
					<a href="<?=get_comics_url(1827);?>"><img src="<?=$drama_img?>/link03.png<?=vs_para();?>" alt="뿔통-무간천하" /></a>
					<a href="<?=get_comics_url(1655);?>"><img src="<?=$drama_img?>/link04.png<?=vs_para();?>" alt="좀비플래너" /></a>
					<a href="<?=get_comics_url(2599);?>"><img src="<?=$drama_img?>/link05.png<?=vs_para();?>" alt="체어맨" /></a>
				</div>
				<div class="evt-warn">
					<img src="<?=$drama_img?>/warn.png<?=vs_para();?>" alt="이벤트 유의사항" />
				</div>
			</div>
		</div>