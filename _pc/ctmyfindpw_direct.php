<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/ct.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/ct.js<?=vs_para();?>"></script>

		<div class="ctmyfindpw_direct">
			<div class="ctmyfindpw_direct_bg">
				<div id="result_idpw" class="result_idpw">
					<h5>ID/패스워드 변경</h5>
					<form name="idpw_form" id="idpw_form" method="post" action="<?=NM_PROC_URL?>/ctmyfindpw_direct.php" onsubmit="return ctmyfindpw_direct_submit();">
						<input type="hidden" id="mb_email_token" name="mb_email_token" value="<?=$mb_email_token?>" />
						<input type="hidden" id="mb_email_idx" name="mb_email_idx" value="<?=$mb_email_idx?>" />
						<input type="hidden" id="mb_email_domain" name="mb_email_domain" value="<?=$mb_email_domain?>" />
						<table>
							<tr>
								<th>아이디</th>
								<td>
									<ul id="mb_id_list">
									<? foreach($mb_email_token_arr as $mb_email_token_key => $mb_email_token_val){ 
										$mb_sns_type = '직접'.' 가입';
										switch($mb_email_token_val['mb_sns_type']) {
											case 'nid'	: $mb_sns_type = 'naver'.	' 가입'; break;
											case 'kko'	: $mb_sns_type = 'kakao'.	' 가입'; break;
											case 'fcb'	: $mb_sns_type = 'facebook'.' 가입'; break;
											case 'ggl'	: $mb_sns_type = 'google'.	' 가입'; break;
											case 'twt'	: $mb_sns_type = 'twitter'.	' 가입'; break;
											default		: $mb_sns_type = '직접'.	' 가입'; break;
										}
									?>
										<li><?=$mb_email_token_val['mb_id']?> (<?=$mb_email_token_val['mb_no']?>) [<?=$mb_sns_type?>]</li>
									<? } ?>
									</ul>
								</td>
							</tr>
							<tr>
								<th>비밀번호</th>
								<td class="pw_input">
									<label for="mb_pw">변경 비밀번호</label> <input type="password" name="mb_pw" />
									<label for="mb_pw_check">변경 비밀번호 확인</label> <input type="password" name="mb_pw_check" />
									<input type="submit" value="확인">
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>