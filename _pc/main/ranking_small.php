<? include_once '_common.php'; // 공통 

/* DB */

$crs_class_tap_arr = $crs_class_arr = array();

foreach($nm_config['cf_small']as $cf_small_key  => $cf_small_val){
	if($cf_small_key == 7 ){ continue; } // 무료 패스

	if($cf_small_key == 0){ $menu_title = '전체'; }
	else{ $menu_title = $cf_small_val; }
	

	$page_adult = '';
	$cf_small_key_name = $cf_small_key;
	if($mb_adult_permission == 'n'){
		$page_adult = '-청소년'; 
		$cf_small_key_name = $cf_small_key.'_t';
	}
	$crs_class_arr[$cf_small_key_name] = $menu_title.$page_adult.' 순위';
}
// 장르별 상단 탭
$crs_class_tap_arr = cs_comics_ranking_small_tap($crs_class_arr);

$crs_tab_count = count($crs_class_tap_arr);
if($crs_tab_count > 0){
	$all_width = 100; // 탭 총 길이
	$crs_width = intval($all_width / $crs_tab_count);
	$crs_width_first = $crs_width + ($all_width - intval($crs_tab_count)  * intval($all_width / intval($crs_tab_count)));
}

// 장르별 코믹스
/*
cs_comics_ranking_small_list() 사용 안함 
cs_comics_content_list()함수안에서 할수 있게 해당 함수 수정
*/

/* /mgenre.php 소스 복사 */

// 특정 만화 순서 앞으로...18-04-25
for($i=0;$i<=8;$i++){ $week_comicno_arr[$i] = array(); } // 배열선언
$sql_cg_adult = sql_adult($mb_adult_permission , 'cg_adult');

$cg_class = 0;

if(!($_menu == 'all' || $_menu == '')){
	$cg_class = intval($_menu);
}
$_small = $cg_class;

if($sql_cg_adult !=''){ $cg_class = $cg_class.'_t'; }
$sql_cg_class = " AND cg_class = '". $cg_class."' ";


$sql_cg = " select * from comics_genre where 1 $sql_cg_adult $sql_cg_class order by cg_class, cg_ranking ";
$result_cg = sql_query($sql_cg);
while ($row_cg = sql_fetch_array($result_cg)) {
	$cg_class_key = intval(str_replace("_t", "", $row_cg['cg_class']));
	array_push($week_comicno_arr[$cg_class_key], $row_cg['cg_comics']);
}
$week_comicno_order = $week_comicno_arr[$_small];

$sql_week_comicno_field = "";
$sql_week_comicno_order = "";

$sql_week_comicno_where = ""; // 18-05-14 마오랑대표&부사장님지시사항

if(count($week_comicno_order) > 0){
	$week_comicno_order_arr = $week_comicno_order;

	$sql_week_comicno_field = " CASE ";
	foreach($week_comicno_order_arr as $week_comicno_order_key => $week_comicno_order_val){
		$sql_then = 99 - intval($week_comicno_order_key);
		$sql_week_comicno_field.= " WHEN c.cm_no = '".$week_comicno_order_val."' THEN ".$sql_then." ";
	}
	$sql_week_comicno_field.= "ELSE 0 ";
	$sql_week_comicno_field.= "END AS cm_no_special, ";

	$sql_week_comicno_order = "cm_no_special DESC, ";

	$sql_week_comicno_where = " OR c.cm_no IN (".implode(", ",$week_comicno_order).") "; // 18-05-14 마오랑대표&부사장님지시사항
}

$sql_crs_class = " AND crs.crs_class = '".$cg_class."' ";

/* 
	cs_comics_content_list() 사용시
	SQL문에 이미지 사이즈 넣었을때( cm_cover, cm_cover_sub 사이즈 둘다 있어야 함 ) 
	cm_cover_thumb_wxh, cm_cover_sub_thumb_wxh
*/

$sql_genre = " 

SELECT CG.*, 
@rownum:=@rownum+1 AS rownum,  
if(@rownum < 4,'tn388x198','tn226x115') as cm_cover_thumb_wxh, 
if(@rownum < 4,'tn133x133','tn133x133') as cm_cover_sub_thumb_wxh
FROM(
	SELECT c.*, crs.crs_ranking, 
		".$sql_week_comicno_field."
		c_prof.cp_name as prof_name 
	FROM comics_ranking_sales_cmgenre_auto crs 
	left JOIN comics c ON crs.crs_comics = c.cm_no 				 
	left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 

	,(SELECT @rownum :=0) AS R

	WHERE 1 and c.cm_service = 'y' $sql_crs_class
	$sql_week_comicno_where 	
	GROUP BY c.cm_no 
	ORDER BY ".$sql_week_comicno_order." 
	crs.crs_ranking ASC  
) AS CG LIMIT 0, 16
";

// 리스트 가져오기
$cmgenre_arr = cs_comics_content_list($sql_genre, substr($nm_config['nm_path'], -1));
/* /mgenre.php 소스 복사 끝 */


/*
$crs_arr = array();
$crs_class_tap_key_arr = array_keys($crs_class_tap_arr);
$crs_arr = cs_comics_ranking_small_list($crs_class_tap_key_arr[0], substr($nm_config['nm_path'], -1));
*/

?>
	<? if($crs_tab_count > 0 ){ ?>
	<!-- body space -->
		<!-- wrapper space -->
			<!-- container_bg -->	
				<link rel="stylesheet" type="text/css" href="<?=NM_PC_MAIN_URL;?>/css/ranking_small.css<?=vs_para();?>" />
				
				<style type="text/css">
					/* main_menu */
					#main_small .crs_class_tap ul li {width:<?=$crs_width;?>%;}
					#main_small .crs_class_tap ul li:first-child {width:<?=$crs_width_first;?>%;}
				</style>

				<script type="text/javascript" src="<?=NM_PC_MAIN_URL;?>/js/ranking_small.js<?=vs_para();?>"></script>

				<div id="main_small">
					<div class="main_small_bg">
						<h2><span><i class="fa fa-heart" aria-hidden="true"></i> 장르별</span> 추천</h2>

						<div class="crs_class_tap">
							<ul>
							<?	$crs_class_count = 0;
								foreach($crs_class_tap_arr as $crs_class_tap_key => $crs_class_tap_val){ 
									$crs_class_tap_text = str_replace("-청소년", "", $crs_class_tap_val);
									$crs_class_tap_text = str_replace(" 순위", "", $crs_class_tap_text);
									?>
								<li class="<?echo $crs_class_tap_key==$crs_class_tap_key_arr[0]?"currunt":"";?> ">
									<button href="#comics_ranking_small<?=$crs_class_tap_key?>" onclick="comics_ranking_small('<?=$crs_class_tap_key?>', '<?=$crs_class_count?>');"><?=$crs_class_tap_text?></button>
								</li>
							<?	$crs_class_count++;	} ?>
							</ul>
						</div> <!-- /crs_class_tap -->
						<div class="crs_class_content">
						<? foreach($cmgenre_arr as $crs_key => $crs_val){ 
							$sub_thumb_class = "sub_list_thumb_small";
							$cm_cover = "cm_cover_sub";
							$small_txt = $crs_val['small_txt'];
							$cm_lank = $crs_val['cm_lank']."위";
							if($crs_key < 8){ 
								$sub_thumb_class = "sub_list_thumb_medium";
								if($crs_key < 3){ 
									$sub_thumb_class = "sub_list_thumb_big"; 
									$small_txt = $crs_val['cm_somaery'];
									$cm_lank = $crs_val['cm_lank_icon'];
								}
								$cm_cover = "cm_cover";
							}
						?>						
							<a href="<?=$crs_val['cm_serial_url']?>" target="_self">
								<div class="sub_list_thumb <?=$sub_thumb_class?>">
									<div class="icon_cm_type">
										<?=$crs_val['cm_type_icon'];?>
									</div>
									<div class="icon_event">
										<?=$crs_val['cm_free_icon'];?>
										<?=$crs_val['cm_sale_icon'];?>
									</div>
									<div class="icon_date">
										<?=$crs_val['cm_up_icon'];?>
										<?=$crs_val['cm_new_icon'];?>
									</div>
									<div class="icon_adult">
										<?=$crs_val['cm_adult_icon'];?>
									</div>
									<dl>
										<dt><img src="<?=$crs_val[$cm_cover]?>" alt="<?=$crs_val['cm_series']?>" /></dt>
										<dt class="ellipsis"><?=stripslashes($crs_val['cm_series']);?></dt>
										<dd class="ellipsis small"><span><?=stripslashes($small_txt);?></span></dd>
										<dd class="ellipsis"><?=stripslashes($crs_val['cm_professional_info']);?></dd>
									</dl>
								</div>
							</a>
						<? } ?>
						</div> <!-- /crs_class_content -->
					</div> <!-- /main_small_bg -->
				</div> <!-- /main_small -->

	<? } ?>