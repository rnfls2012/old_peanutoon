<? include_once '_common.php'; // 공통 

/* DB */
$ranking_arr = array();
$d_cr_class_arr = array();
if($mb_adult_permission == 'y'){ $d_cr_class_arr = $d_cr_class; }
else{$d_cr_class_arr = $d_cr_t_class;}

$ranking_link_arr = array('cmbest.php?type=1','cmbest.php?type=2','cmbest.php?type=3');
$ranking_link_count = $ranking_arr_count = 0;
$cr_class_al_value = $cr_class_is_value = "";
foreach($d_cr_class_arr as $cr_class_key => $cr_class_val){	
	$ranking_limit = 0;
	${'sql_ranking_big_'.$cr_class_key} = "";
	${'sql_ranking_comics_'.$cr_class_key} = "";
	${'ranking_comics_'.$cr_class_key} = array();
	${'ranking_arr_'.$cr_class_key} = array();

	${'ranking_link'.$cr_class_key} = NM_URL."/".$ranking_link_arr[$ranking_link_count];
	$ranking_link_count++;

	${'sql_ranking_'.$cr_class_key} = " SELECT * FROM `comics_ranking` WHERE cr_class='$cr_class_key' ORDER BY cr_ranking ASC LIMIT 5;";
	${'result_ranking_'.$cr_class_key} = sql_query(${'sql_ranking_'.$cr_class_key});
	${'row_size_'.$cr_class_key} = sql_num_rows(${'result_ranking_'.$cr_class_key});

	if(${'row_size_'.$cr_class_key} == 0){ continue; }
	while (${'row_ranking_'.$cr_class_key} = sql_fetch_array(${'result_ranking_'.$cr_class_key})) {
		array_push(${'ranking_arr_'.$cr_class_key},  ${'row_ranking_'.$cr_class_key});
		array_push(${'ranking_comics_'.$cr_class_key},  ${'row_ranking_'.$cr_class_key}['cr_comics']);
		if($ranking_arr_count != 0){
			${'sql_ranking_big_'.$cr_class_key} = " AND cm_big = '".${'row_ranking_'.$cr_class_key}['cr_big']."' ";
		}
	}
	// 5개 미만이라면...
	$ranking_limit = 5 - intval(${'row_size_'.$cr_class_key});
	if($ranking_limit > 0){
		$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');
		${'sql_ranking_comics_'.$cr_class_key} = " SELECT * FROM comics  WHERE 1 AND cm_no not in (".implode(", ",${'ranking_comics_'.$cr_class_key}).") ".${'sql_ranking_big_'.$cr_class_key}." $sql_cm_adult ORDER BY cm_episode_date  LIMIT 5 ";
		${'result_ranking_comics'.$cr_class_key} = sql_query(${'sql_ranking_comics_'.$cr_class_key});
		while (${'row_ranking_comics'.$cr_class_key} = sql_fetch_array(${'result_ranking_comics'.$cr_class_key})) {
			${'row_ranking_comics'.$cr_class_key}['cr_comics'] = ${'row_ranking_comics'.$cr_class_key}['cm_no'];
			array_push(${'ranking_arr_'.$cr_class_key},  ${'row_ranking_comics'.$cr_class_key});
		}
	}

	if($cr_class_key == 'al_t' || $cr_class_key == 'al'){ $cr_class_al_value = $cr_class_key; }
	if($cr_class_key == 'is_t' || $cr_class_key == 'is'){ $cr_class_is_value = $cr_class_key; continue; }

	$ranking_arr_count++;
}

if($ranking_arr_count == 0){ return; }

if($ranking_arr_count > 0){
	$all_width = 339; // 탭 총 길이
	$ranking_width = intval($all_width / $ranking_arr_count);
	$ranking_width_first = $ranking_width + ($all_width - intval($ranking_arr_count)  * intval($all_width / intval($ranking_arr_count)));
}
?>


<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<link rel="stylesheet" type="text/css" href="<?=NM_PC_MAIN_URL;?>/css/ranking.css<?=vs_para();?>" />
			<style type="text/css">
				/* main_menu */
				#main_ranking .famous_tab button {width:<?=$ranking_width;?>px;}
				#main_ranking .famous_tab button:first-child {width:<?=$ranking_width_first;?>px;}
			</style>
			<script type="text/javascript" src="<?=NM_PC_MAIN_URL;?>/js/ranking.js<?=vs_para();?>"></script>
			<div id="main_ranking"> <!-- main_ranking -->
				<div class="famous"> <!-- 인기순위 -->
					<h2>
						<span><i class="fa fa-star" aria-hidden="true"></i> 인기</span> 종합 순위
						<a id="ranking_more" href="<?=${'ranking_link'.$cr_class_al_value}?>"><span><i class="fa fa-plus" aria-hidden="true"></i></span> 더보기</a>
					</h2>
					<div class="famous_tab_wrap"> <!-- 인기종합순위 탭 -->
						<ul>
						<?  $cr_class_no = 0;
							foreach($d_cr_class_arr as $cr_class_key => $cr_class_val){ 
								if($mb_adult_permission == 'y'){ $cr_class_val_text = str_replace(" 순위", "", $cr_class_val); }
								else{ $cr_class_val_text = str_replace("-청소년 순위", "", $cr_class_val); }
								if(${'row_size_'.$cr_class_key} == 0){ continue; }
								if($cr_class_is_value == $cr_class_key){ continue; } // 급상승작품 제외
							?>
								<li class="famous_tab <? echo $cr_class_no==0?"on":""; ?>"><button onclick="rankingview(<?=$cr_class_no?>, '<?=${'ranking_link'.($cr_class_key)};?>');"><?=$cr_class_val_text;?></button></li>
						<?		$cr_class_no++;
							} ?>
						</ul>
					</div> <!-- /인기종합순위 탭 -->
					<div class="famous_list"> <!-- 인기종합순위 리스트 -->
						<?	$cr_class_no = 0;
							foreach($d_cr_class_arr as $cr_class_key => $cr_class_val){ 							
							if(${'row_size_'.$cr_class_key} == 0){ continue; }
							if($cr_class_is_value == $cr_class_key){ continue; } // 급상승작품 제외
						?>
							<ol class="<? echo $cr_class_no==0?"on":""; ?>">
								<?	foreach(${'ranking_arr_'.$cr_class_key} as $ranking_key => $ranking_val){ 
									/* 컨텐츠 정보 */
									$get_comics = get_comics($ranking_val['cr_comics']);
									
									/* 코믹스 URL */
									$db_cr_comics_url = NM_URL."/comics.php?comics=".$get_comics['cm_no'];
					
									/* 컨텐츠 제목 */ 
									$db_cr_series = text_change($get_comics['cm_series'], "]");

									/* 컨텐츠 작가명 */
									$db_cr_professional = $get_comics['cm_professional_info'];
					
									/* 순위 */
									$db_cr_ranking = $ranking_val['cr_ranking'];						
									
									/* 이미지 표지 */
									if($db_cr_ranking == 1) {
										$db_cr_img_url = img_url_para($get_comics['cm_cover_sub'], $get_comics['cm_reg_date'], $get_comics['cm_mod_date'], 'cm_cover_sub', 'tn133x133');
									} else {
										$db_cr_img_url = img_url_para($get_comics['cm_cover_sub'], $get_comics['cm_reg_date'], $get_comics['cm_mod_date'], 'cm_cover_sub', 'tn50x50');
									}

									/* 줄거리 */
									$cr_str_limit = 60;
									$db_cr_somaery = strip_tags(stripslashes($get_comics['cm_somaery']));
									$db_cr_somaery_tag = mb_substr($db_cr_somaery,0,$cr_str_limit, "UTF-8");
									if(mb_strlen($db_cr_somaery, "UTF-8") > $cr_str_limit){$db_cr_somaery_tag = trim($db_cr_somaery_tag)."..."; }
					
								?>
									<? if($db_cr_ranking == 1){ ?>
										<li><!-- 1위 -->
											<a href="<?=$db_cr_comics_url?>">
												<div class="famous01 padding10">
													<span class="no no1"><?=$db_cr_ranking?></span>
													<img src="<?=$db_cr_img_url?>" alt="<?=$db_cr_series?> / <?=$cr_class_val?> <?=$db_cr_ranking?>위" />
													<dl>
														<dt class="ellipsis cm_title">
															<?=$db_cr_series?><br />
														</dt>
														<dt class="famous_author ellipsis">
															<?=$db_cr_professional?>
														</dt>
														<dd><?=$db_cr_somaery_tag;?></dd>
													</dl>
													<!-- 
													<div class="lank">
														<span class="up"><i class="fa fa-caret-up" aria-hidden="true"></i></span>
														<span class="lankno"><?=$ranking_val['cr_ranking_gap'];?></span>
													</div>
													-->
												</div>
											</a>
										</li><!-- /1위 -->
									<? } // if($db_cr_ranking == 1){ ?>

									<? if($db_cr_ranking == 2 || $db_cr_ranking == 3){ ?>
										<li><!-- 2~3위 -->
											<a href="<?=$db_cr_comics_url?>">
												<div class="famous02">
													<span class="no no2"><?=$db_cr_ranking?></span>
													<img src="<?=$db_cr_img_url?>" alt="<?=$db_cr_series?> / <?=$cr_class_val?> <?=$db_cr_ranking?>위" />
													<span class="series ellipsis cm_title"><?=$db_cr_series?></span>
													<!-- 
													<div class="lank margintop15">
														<span class="up"><i class="fa fa-caret-up" aria-hidden="true"></i></span>
														<span class="lankno"><?=$ranking_val['cr_ranking_gap'];?></span>
													</div>
													-->
												</div>
											</a>
										</li><!-- /2~3위 -->
									<? } // if($db_cr_ranking == 2 || $db_cr_ranking == 3){  ?>

									<? if($db_cr_ranking == 4 || $db_cr_ranking == 5){ ?>
										<li><!-- 4~5위 -->
											<a href="<?=$db_cr_comics_url?>">
												<div class="famous03 ellipsis cm_title">
													<span class="no3"><?=$db_cr_ranking?></span> <?=$db_cr_series?>
												</div>
												<!-- 
												<div class="lank">
													<span class="up"><i class="fa fa-caret-up" aria-hidden="true"></i></span>
													<span class="lankno">1</span>
												</div>
												-->
											</a>
										</li><!-- /4~5위 -->
									<? } // if($db_cr_ranking == 4 || $db_cr_ranking == 5){  ?>
								<? } //foreach(${'ranking_arr_'.$cr_class_key} as $ranking_key => $ranking_val){ ?>
							</ol>
						<?	$cr_class_no++;
							} /* foreach($d_cr_class_arr as $cr_class_key => $cr_class_val){  */ ?>
					</div>  <!-- /인기종합순위 리스트 -->
				</div> <!-- /인기순위 -->

			<? if(count(${'ranking_arr_'.$cr_class_is_value}) > 0){?>
				<div class="rise"> <!-- 급상승작품 -->
					<h2><span><i class="fa fa-level-up" aria-hidden="true"></i> 급상승</span> 작품</h2>
					<div class="rise_thumbnail_wrap"> <!-- 썸네일리스트 -->					
						<?	foreach(${'ranking_arr_'.$cr_class_is_value} as $ranking_is_key => $ranking_is_val){ 
								/* 컨텐츠 정보 */
								$get_comics = get_comics($ranking_is_val['cr_comics']);
								
								/* 코믹스 URL */
								$db_cr_comics_url = NM_URL."/comics.php?comics=".$get_comics['cm_no'];

								/* 이미지 표지 */
								if($ranking_is_key == 0) {
									$db_cr_img_url = img_url_para($get_comics['cm_cover_sub'], $get_comics['cm_reg_date'], $get_comics['cm_mod_date'], 'cm_cover_sub', 'tn335x335');
								} else {
									$db_cr_img_url = img_url_para($get_comics['cm_cover'], $get_comics['cm_reg_date'], $get_comics['cm_mod_date'], 'cm_cover', 'tn229x117');
								} // end else
				
								/* 컨텐츠 제목 */ 
								$db_cr_series = text_change($get_comics['cm_series'], "]");

								/* 컨텐츠 작가명 */
								$db_cr_professional = $get_comics['cm_professional_info'];
				
								/* 순위 */
								$db_cr_ranking = $ranking_is_val['cr_ranking'];		
				
								/* 장르 */
								$db_cr_small = $nm_config['cf_small'][$get_comics['cm_small']];

								/* 줄거리 */
								$cr_str_limit = 60;
								$db_cr_somaery = strip_tags(stripslashes($get_comics['cm_somaery']));
								$db_cr_somaery_tag = mb_substr($db_cr_somaery,0,$cr_str_limit, "UTF-8");
								if(mb_strlen($db_cr_somaery, "UTF-8") > $cr_str_limit){$db_cr_somaery_tag = trim($db_cr_somaery_tag)."..."; }
								if($ranking_is_key >= 5){ continue;}
						?>
								<a href="<?=$db_cr_comics_url?>"> <!-- 큰 썸네일 -->
									<div class="rise_thumbnail <? echo $db_cr_ranking==1?"big":""; ?>">

										<dl>
											<dt><img src="<?=$db_cr_img_url?>" alt="<?=$db_cr_series?>" /></dt>
											<dt class="cm_title"><?=$db_cr_series?></dt>
											<dd><?=$db_cr_small?></dd>
											<dd><?=$db_cr_professional?></dd>
										</dl>
									</div>
								</a> <!-- /큰 썸네일 -->
						<?  } ?>
					</div> <!-- /썸네일리스트 -->
				</div><!-- /급상승작품 -->
			<?}?>
			</div> <!-- /main_ranking -->