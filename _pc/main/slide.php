<? include_once '_common.php'; // 공통 
// 초기화 - slide_no
$slide_no = '1';

/* TEST 배너는 ADMIN만 나오도록 20180322 */
$emb_test = " AND emb_test = 'n' ";
if(mb_class_permission("a")) {
	$emb_test = "AND (emb_test = 'n' OR emb_test = 'y') ";
} // end if

/* DB */
$event_thanksgiving_embc_no = 2415; // 추석번호 처리

$slide_arr = array();
$sql_slide_adult = sql_adult($mb_adult_permission , 'emb_adult');
$sql_slide = " SELECT * FROM epromotion_banner_cover embc JOIN epromotion_banner emb ON embc.embc_emb_no = emb.emb_no 
				WHERE 1 AND embc_position = '$slide_no' AND emb_state = 'y' 
				AND embc_no!={$event_thanksgiving_embc_no} 
				$emb_test $sql_slide_adult 
				ORDER BY IF(embc_order=0,99999,embc_order) ASC , embc_no DESC";
/* 18-11-16 김성훈 차장님 요청에 의해 랜덤 삭제 -> 고정 적용 
  ORDER BY rand() -> ORDER BY IF(embc_order=0,99999,embc_order) ASC , embc_no DESC
*/
$result_slide = sql_query($sql_slide);

while ($row_slide = sql_fetch_array($result_slide)) {
	array_push($slide_arr,  $row_slide);
}


// 추석
$sql_event_thanksgiving = " 
 SELECT * FROM epromotion_banner_cover embc JOIN epromotion_banner emb ON embc.embc_emb_no = emb.emb_no 
 WHERE 1 AND embc_position = '$slide_no' AND emb_state = 'y' 
 AND embc_no={$event_thanksgiving_embc_no}";
$row_event_thanksgiving = sql_fetch($sql_event_thanksgiving);
if($row_event_thanksgiving['embc_no'] == $event_thanksgiving_embc_no){
	array_unshift($slide_arr, $row_event_thanksgiving);
}
?>
<!-- body space -->
	<!-- wrapper space -->
			<!-- container_bg -->
				<link rel="stylesheet" type="text/css" href="<?=NM_PC_MAIN_URL;?>/css/slide.css<?=vs_para();?>" />
				<script type="text/javascript" src="<?=NM_PC_MAIN_URL;?>/js/slide.js<?=vs_para();?>"></script>
				<script type="text/javascript">
				<!--
					$(document).on('ready', function() {
						$("#pc_slide<?=$slide_no?> .regular").slick({
							dots: true,
							infinite: true,
							fade: true, cssEase: 'linear', 
							speed: 500,
							slidesToShow: 1,
							slidesToScroll: 1,
							autoplay: true,
							autoplaySpeed: 5000,
							arrows: true
						});
					});
					<? /* fade: true, cssEase: 'linear', 
						  vertical: true, verticalSwiping: true,
					*/ ?>
				//-->
				</script>
				<div class="pc_slide_bgcolor" >
					<div id="pc_slide<?=$slide_no?>">
						<div class="pc_slide_bg">
							<section class="regular slider">
								<? foreach($slide_arr as $slide_key => $slide_val){ 
									$emb_url = NM_URL.'/'.$slide_val['emb_url'];
									if(strpos($slide_val['emb_url'], 'ttp://') > 1 || strpos($slide_val['emb_url'], 'ttps://') > 1){
										$emb_url = $slide_val['emb_url'];						
									}
									/* 이미지 표지 */
									$slide_img_url = img_url_para($slide_val['embc_cover'], $slide_val['embc_date'], '', 'embc_cover', '1');
								?>
								<div data-bgcolor="<?=$slide_val['emb_bgcolor']?>" data-name="<?=$slide_val['emb_name']?>">
									<a href="<?=$emb_url?>" target="<?=$slide_val['emb_target'];?>"><img src="<?=$slide_img_url;?>" alt="<?=$slide_val['emb_name'];?>"></a>
								</div>
								<? } ?>
							</section>
						</div>
					</div>
				</div>