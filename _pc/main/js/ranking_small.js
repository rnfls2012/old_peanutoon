function comics_ranking_small(crs_class, crs_idx){
	var html_arr = new Array();

	var $crs_tap = $("#main_small .crs_class_tap ul li");
	var $crs_list = $("#main_small .crs_class_content");

	var crs_list_access = $.ajax({
		url: nm_url +"/ajax/main/ranking_small.php",
		dataType: "json",
		type: "POST",
		data: { crs_class: crs_class},
		beforeSend: function( data ) {
			//console.log('로딩');
			// $crs_list.hide();
		}
	})
	crs_list_access.done(function( data ) {
		var html_tag = '';
		var sub_thumb_class = cm_cover = small_txt = '';

		$(data).each(function(idx){
			sub_thumb_class = 'sub_list_thumb_small';
			cm_cover = 'cm_cover_sub';
			small_txt = data[idx]['small_txt'];
			cm_lank = data[idx]['cm_lank']+'위';
			if(idx < 8){ 
				sub_thumb_class = 'sub_list_thumb_medium';
				if(idx < 3){ 
					sub_thumb_class = 'sub_list_thumb_big';
					small_txt = data[idx]['cm_somaery'];
					cm_lank = data[idx]['cm_lank_icon'];
				}
				cm_cover = 'cm_cover';
			}
			html_arr.push('<a href="'+data[idx]['cm_serial_url']+'" target="_self">');
				html_arr.push('<div class="sub_list_thumb '+sub_thumb_class+'">');
					html_arr.push('<div class="icon_cm_type">'+data[idx]['cm_type_icon']+'</div>');
					html_arr.push('<div class="icon_event">'+data[idx]['cm_free_icon']+data[idx]['cm_sale_icon']+'</div>');
					html_arr.push('<div class="icon_date">'+data[idx]['cm_up_icon']+data[idx]['cm_new_icon']+'</div>');
					html_arr.push('<div class="icon_adult">'+data[idx]['cm_adult_icon']+'</div>');

					html_arr.push('<dl>');
						html_arr.push('<dt><img src="'+data[idx][cm_cover]+'" alt="'+data[idx]['cm_series']+'" /></dt>');
						html_arr.push('<dt class="ellipsis">'+data[idx]['cm_series']+'</dt>');
						html_arr.push('<dd class="ellipsis small">'+small_txt+'</dd>');
						html_arr.push('<dd class="ellipsis">'+data[idx]['cm_professional_info']+'</dd>');
					html_arr.push('</dl>');
				html_arr.push('</div>');
			html_arr.push('</a>');
		});

		$crs_tap.removeClass('currunt');
		$crs_tap.eq(crs_idx).addClass('currunt');

		$crs_list.html(html_arr.join("\n"));
		var $crs_list_images = $crs_list.find('img');
		var images_count = $crs_list_images.length;
		$crs_list_images.load(function(){
			images_count--;
			if(!images_count){
				// $crs_list.show();
			}
		});
	});
	crs_list_access.fail(function( data ) {
		//console.log('실패');
	});
	crs_list_access.always(function( data ) {
		//console.log('로딩삭제');
	});
}