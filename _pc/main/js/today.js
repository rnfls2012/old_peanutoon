function comics_week_today(crs_class, crs_idx){
	var html_arr = new Array();
	var yoil_arr = {1 : "월", 2 : "화", 3 : "수", 4 : "목", 5 : "금", 6 : "토", 7 : "일"};
	var $crs_tap = $("#main_today .crs_class_tap ul li");
	var $crs_list = $("#main_today .crs_class_content");

	var crs_list_access = $.ajax({
		url: nm_url +"/ajax/main/today.php",
		dataType: "json",
		type: "POST",
		data: { crs_class: crs_class},
		beforeSend: function( data ) {
			//console.log('로딩');
			// $crs_list.hide();
		}
	})
	crs_list_access.done(function( data ) {
		var html_tag = '';
		var sub_thumb_class = cm_cover = small_txt = '';
		$(".today_yoil").text(yoil_arr[crs_class]);
		
		var $cmweek_best_txt = ''; // 마오랑 요청 - 원조교제 아닌데요에 금요베스트 넣기

		$(data).each(function(idx){
			sub_thumb_class = 'sub_list_thumb_small';
			cm_cover = 'cm_cover_sub';
			cm_somaery = '';
			if(idx < 8){ 
				sub_thumb_class = 'sub_list_thumb_medium';
				if(idx < 3){ 
					sub_thumb_class = 'sub_list_thumb_big';
					cm_somaery = data[idx]['cm_somaery'].replace("\\", "");
					cm_somaery = cm_somaery.replace("\\", "");
					cm_somaery = cm_somaery.replace("\\", "");
					cm_somaery = cm_somaery.replace("\\", "");
				}
				cm_cover = 'cm_cover';
				
				// 마오랑 요청 - 원조교제 아닌데요에 금요베스트 넣기
				cmweek_best_txt = "";
				if(data[idx]['cmweek_field_ck'] == 0 && data[idx]['cm_public_cycle'] > 0 && data[idx]['cm_end'] == 'n'){
					cmweek_best_txt = data[idx]['cm_week_txt']+"요베스트";
				}
				// 마오랑 요청 - 원조교제 아닌데요에 금요베스트 넣기 end
			}
			html_arr.push('<a href="'+data[idx]['cm_serial_url']+'" target="_self">');
				html_arr.push('<div class="sub_list_thumb '+sub_thumb_class+'">');
					html_arr.push('<div class="icon_cm_type">'+data[idx]['cm_type_icon']+'</div>');
					html_arr.push('<div class="icon_event">'+data[idx]['cm_free_icon']+data[idx]['cm_sale_icon']+'</div>');
					html_arr.push('<div class="icon_date">'+data[idx]['cm_up_icon']+data[idx]['cm_new_icon']+'</div>');
					html_arr.push('<div class="icon_adult">'+data[idx]['cm_adult_icon']+'</div>');

					html_arr.push('<dl>');
						html_arr.push('<dt><img src="'+data[idx][cm_cover]+'" alt="'+data[idx]['cm_series']+'" /></dt>');
						html_arr.push('<dt class="ellipsis">'+data[idx]['cm_series']+'</dt>');
						if(cm_somaery != ''){
							html_arr.push('<dd class="ellipsis small">'+cm_somaery+'</dd>');
						}
						html_arr.push('<dd class="ellipsis">'+data[idx]['cm_small_span']+data[idx]['cm_end_span']+cmweek_best_txt+'</dd>');
						html_arr.push('<dd class="ellipsis">'+data[idx]['cm_professional_info']+'</dd>');
					html_arr.push('</dl>');
				html_arr.push('</div>');
			html_arr.push('</a>');
		});

		$crs_tap.removeClass('currunt');
		$crs_tap.eq(crs_idx).addClass('currunt');

		$crs_list.html(html_arr.join("\n"));
		var $crs_list_images = $crs_list.find('img');
		var images_count = $crs_list_images.length;
		$crs_list_images.load(function(){
			images_count--;
			if(!images_count){
				// $crs_list.show();
			}
		});
	});
	crs_list_access.fail(function( data ) {
		//console.log('실패');
	});
	crs_list_access.always(function( data ) {
		//console.log('로딩삭제');
	});
}