<? include_once '_common.php'; // 공통 

/* DB */
// 전체 월1 화2 수3 목4 금5 토6 일7
$cmweek_cycle_in_arr = get_comics_cycle_fix(week_type(7));
$sql_cm_public_cycle_in = "AND c.cm_public_cycle IN(".$cmweek_cycle_in_arr.") ";

// 18-05-19 마오랑대표&부사장님지시사항
$sql_cmweek_field_ck = " if(c.cm_public_cycle IN(".$cmweek_cycle_in_arr.")=1,1,0)as cmweek_field_ck, ";

// 오늘 요일 표기 하기 위한...
$cm_public_cycle_in = get_comics_cycle_fix(week_type(7));

$cmweek_arr = array();
$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');

// 특정 만화 순서 앞으로...18-04-25
for($i=1;$i<=7;$i++){ $week_comicno_arr[$i] = array(); } // 배열선언
$sql_cw_adult = sql_adult($mb_adult_permission , 'cw_adult');
$sql_cw_class = "";
if($sql_cw_adult !=''){ $sql_cw_class = " AND cw_class LIKE '%_t' "; }

$sql_cmweek = " select * from comics_week where 1 $sql_cw_adult $sql_cw_class order by cw_class, cw_ranking ";

$result_cmweek = sql_query($sql_cmweek);

while ($row_cmweek = sql_fetch_array($result_cmweek)) {
	$cw_class_key = intval(str_replace("_t", "", $row_cmweek['cw_class']));
	if($cw_class_key == 0){
		array_push($week_comicno_arr[7],$row_cmweek['cw_comics']);
	}else{
		array_push($week_comicno_arr[$cw_class_key],$row_cmweek['cw_comics']);
	}
}

$week_comicno_order = $week_comicno_arr[week_type(7)];

$sql_week_comicno_where = ""; // 18-05-14 마오랑대표&부사장님지시사항

if(count($week_comicno_order) > 0){
	$week_comicno_order_arr = $week_comicno_order;

	$sql_week_comicno_field = " CASE ";
	foreach($week_comicno_order_arr as $week_comicno_order_key => $week_comicno_order_val){
		$sql_then = 99 - intval($week_comicno_order_key);
		$sql_week_comicno_field.= " WHEN c.cm_no = '".$week_comicno_order_val."' THEN ".$sql_then." ";
	}
	$sql_week_comicno_field.= "ELSE 0 ";
	$sql_week_comicno_field.= "END AS cm_no_special, ";

	$sql_week_comicno_order = "cm_no_special DESC, ";

	$sql_week_comicno_where = " OR c.cm_no IN (".implode(", ",$week_comicno_order).") "; // 18-05-14 마오랑대표&부사장님지시사항
} // end if


// 18-05-14 부사장님&마오랑 추가사항
$sql_limit = " LIMIT 0, 16 ";

$sql_cmweek = " SELECT c.*, c_prof.cp_name as prof_name, 
					".$sql_week_comicno_field."
					".$sql_cmweek_field_ck."
					if(c.cm_public_cycle IN(".$cm_public_cycle_in.")=1,
						if(left(c.cm_episode_date,10)>='".NM_TIME_M1."',1,0),0
					)as cm_public_cycle_ck, 
					if(left(c.cm_reg_date,10)>='".NM_TIME_M1."',c.cm_reg_date,0)as cm_reg_date_new, 
					if(left(c.cm_episode_date,10)>='".NM_TIME_M1."',1,0)as cm_episode_date_up 
				FROM comics c 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 $sql_cm_adult $sql_cm_public_cycle_in and c.cm_service = 'y' and c.cm_big = 2 
				$sql_week_comicno_where 
				ORDER BY ".$sql_week_comicno_order." 
				cm_end ASC, cm_public_cycle_ck DESC, cm_episode_date_up DESC, cm_reg_date DESC, cm_episode_date DESC 
				".$sql_limit." ";

// 리스트 가져오기
$cmweek_arr = cs_comics_content_list($sql_cmweek, substr($nm_config['nm_path'], -1));

// 전체 월1 화2 수3 목4 금5 토6 일7
$cmweek_menu_arr = array(1=>"월", 2=>"화", 3=>"수", 4=>"목", 5=>"금", 6=>"토", 7=>"일"); // 메뉴용 배열
$crs_tab_count = count($cmweek_menu_arr);
if($crs_tab_count > 0){
	$all_width = 100; // 탭 총 길이
	$crs_width = intval($all_width / $crs_tab_count);
	$crs_width_first = $crs_width + ($all_width - intval($crs_tab_count)  * intval($all_width / intval($crs_tab_count)));
}
?>
	<? if($crs_tab_count > 0 ){ ?>
	<!-- body space -->
		<!-- wrapper space -->
			<!-- container_bg -->	
				<link rel="stylesheet" type="text/css" href="<?=NM_PC_MAIN_URL;?>/css/today.css<?=vs_para();?>" />
				
				<style type="text/css">
					/* main_menu */
					#main_today .crs_class_tap ul li {width:<?=$crs_width;?>%;}
					#main_today .crs_class_tap ul li:first-child {width:<?=$crs_width_first;?>%;}
				</style>

				<script type="text/javascript" src="<?=NM_PC_MAIN_URL;?>/js/today.js<?=vs_para();?>"></script>

				<div id="main_today" class="main_today_week">
					<div class="main_small_bg">
						<h2><span><i class="fa fa-calendar" aria-hidden="true"></i> 요일별</span> 작품 - <span class="today_yoil"><?=get_yoil(NM_TIME_W,0,1);?></span></h2>
						
						<!-- 메뉴 -->
						<div class="crs_class_tap">
							<ul>
							<?	$crs_class_count = 0;
								foreach($cmweek_menu_arr as $cmweek_tap_key => $cmweek_tap_val){ ?>
								<li class="<?echo $cmweek_tap_key==NM_TIME_W?"currunt":"";?> "> <!-- 여기 수정 -->
									<button href="#comics_week_today<?=$cmweek_tap_key?>" onclick="comics_week_today('<?=$cmweek_tap_key?>', '<?=$crs_class_count?>');"><?=$cmweek_tap_val?></button>
								</li>
							<?	$crs_class_count++;	} ?>
							</ul>
						</div> <!-- /crs_class_tap -->
						<!-- 메뉴 끝 -->

						<div class="crs_class_content">
						<? foreach($cmweek_arr as $crs_key => $crs_val){ 
							$sub_thumb_class = "sub_list_thumb_small";
							$cm_cover = "cm_cover_sub";
							$cm_somaery = '';
							if($crs_key < 8){ 
								$sub_thumb_class = "sub_list_thumb_medium";
								if($crs_key < 3){ 
									$sub_thumb_class = "sub_list_thumb_big"; 
									$cm_somaery = $crs_val['cm_somaery'];
								} // end if
								$cm_cover = "cm_cover";
							} // end if
							// 마오랑 요청 - 원조교제 아닌데요에 금요베스트 넣기
							$cmweek_best_txt = "";
							if($crs_val['cmweek_field_ck'] == 0 && $crs_val['cm_public_cycle'] > 0 && $crs_val['cm_end'] == 'n'){
								$cmweek_best_txt = $crs_val['cm_week_txt']."요베스트";
							}
							// 마오랑 요청 - 원조교제 아닌데요에 금요베스트 넣기 end
						?>						
							<a href="<?=$crs_val['cm_serial_url']?>" target="_self">
								<div class="sub_list_thumb <?=$sub_thumb_class?>">
									<div class="icon_cm_type">
										<?=$crs_val['cm_type_icon'];?>
									</div>
									<div class="icon_event">
										<?=$crs_val['cm_free_icon'];?>
										<?=$crs_val['cm_sale_icon'];?>
									</div>
									<div class="icon_date">
										<?=$crs_val['cm_up_icon'];?>
										<?=$crs_val['cm_new_icon'];?>
									</div>
									<div class="icon_adult">
										<?=$crs_val['cm_adult_icon'];?>
									</div>
									<dl>
										<dt><img src="<?=$crs_val[$cm_cover]?>" alt="<?=$crs_val['cm_series']?>" /></dt>
										<dt class="ellipsis"><?=stripslashes($crs_val['cm_series']);?></dt>
										<? if($cm_somaery !=''){ ?>
										<dd class="ellipsis"><span class="cm_somaery"><?=stripslashes($cm_somaery);?></span></dd>
										<? } ?>
										<dd class="ellipsis"><?=$crs_val['cm_small_span']?><?=$crs_val['cm_end_span']?><?=$cmweek_best_txt;?></dd>
										<dd class="ellipsis"><?=stripslashes($crs_val['cm_professional_info']);?></dd>
									</dl>
								</div>
							</a>
						<? } // end foreach ?>
						</div> <!-- /crs_class_content -->
					</div> <!-- /main_small_bg -->
				</div> <!-- /main_small -->

	<? } ?>