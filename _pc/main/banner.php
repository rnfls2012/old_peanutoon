<? include_once '_common.php'; // 공통 
$banner_no = '3';

/* TEST 배너는 ADMIN만 나오도록 20180322 */
$emb_test = " AND emb_test = 'n' ";
if(mb_class_permission("a")) {
	$emb_test = "AND (emb_test = 'n' OR emb_test = 'y') ";
} // end if

/* DB */
$banner_arr = array();
$sql_banner_adult = sql_adult($mb_adult_permission , 'emb_adult');
$sql_banner = " SELECT * FROM epromotion_banner_cover embc JOIN epromotion_banner emb ON embc.embc_emb_no = emb.emb_no 
				WHERE 1 AND embc_position = '$banner_no' AND emb_state = 'y' $emb_test $sql_banner_adult  
				ORDER BY IF(embc_order=0,99999,embc_order) ASC , embc_no DESC LIMIT 0, 3";
$result_banner = sql_query($sql_banner);
while ($row_banner = sql_fetch_array($result_banner)) {
	array_push($banner_arr,  $row_banner);
}
?>
	<? if(count($banner_arr) > 0 ){?>
	<!-- body space -->
		<!-- wrapper space -->
			<!-- container_bg -->			
				<link rel="stylesheet" type="text/css" href="<?=NM_PC_MAIN_URL;?>/css/banner.css<?=vs_para();?>" />
				<script type="text/javascript" src="<?=NM_PC_MAIN_URL;?>/js/banner.js<?=vs_para();?>"></script>

				<div id="main_banner">
					<ul class="event_banner_wrap">
						<? foreach($banner_arr as $banner_key => $banner_val){ 
							$emb_url = NM_URL.'/'.$banner_val['emb_url'];
							if(strpos($banner_val['emb_url'], 'ttp://') > 1 || strpos($banner_val['emb_url'], 'ttps://') > 1){
								$emb_url = $banner_val['emb_url'];						
							}
							/* 이미지 표지 */
							$banner_img_url = img_url_para($banner_val['embc_cover'], $banner_val['embc_date'], '', 'embc_cover', '3');
						?>
						<li><a href="<?=$emb_url;?>"><img src="<?=$banner_img_url;?>" alt="<?=$banner_val['emb_name'];?>"/></a></li>
						<? } ?>
					</ul>
				</div> <!-- /main_banner -->

	<? } ?>