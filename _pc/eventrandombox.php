<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/eventrandombox.css<?=vs_para();?>" />
		<style type="text/css">
			.con01 { background:url(<?=$randombox_img;?>/bg02.png<?=vs_para();?>), url(<?=$randombox_img;?>/bg01.png<?=vs_para();?>); 
				background-repeat: no-repeat, repeat;
				background-size: auto;
				background-position: center top, left top; }
			.con01 .pocket { background:url(<?=$randombox_img;?>/con01_bg.png<?=vs_para();?>) no-repeat center top; }
			.con01 .pocket .gauge_blank::after { background:url(<?=$randombox_img;?>/gauge_full_bg.png<?=vs_para();?>), #ffec49; }
			.con01 .pocket .gauge_blank .gauge::before { background: url(<?=$randombox_img;?>/gauge_point.png<?=vs_para();?>) no-repeat; }
			.con01 .pocket_stat { background:url(<?=$randombox_img;?>/con01_btn.png<?=vs_para();?>) no-repeat; }
			.ModalPopup { background:url(<?=$randombox_img;?>/bg01.png<?=vs_para();?>); }
			.ModalPopup .p_content01 ul.p_choice li input[type='radio'] + label::before { background-position: left top; }
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket1 + label::before { background: url(<?=$randombox_img;?>/p_pocket01.png<?=vs_para();?>); }
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket1:checked +label::before { background:url(<?=$randombox_img;?>/p_pocket01.png<?=vs_para();?>); background-position: right top; }
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket2 + label::before { background: url(<?=$randombox_img;?>/p_pocket02.png<?=vs_para();?>); }
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket2:checked +label::before { background:url(<?=$randombox_img;?>/p_pocket02.png<?=vs_para();?>); background-position: right top; }
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket3 + label::before { background: url(<?=$randombox_img;?>/p_pocket03.png<?=vs_para();?>); }
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket3:checked +label::before { background:url(<?=$randombox_img;?>/p_pocket03.png<?=vs_para();?>); background-position: right top; }
			.ModalPopup .p_content02 .p_coupon { background: url(<?=$randombox_img;?>/p_coupon.png<?=vs_para();?>) no-repeat center top; }
			.con01 .pocket .gauge_blank::after { background-repeat: no-repeat; background-position:center; background-size: auto; }
		</style>
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/eventrandombox.js<?=vs_para();?>"></script>
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/jquery.autocomplete.css">
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/jquery.autocomplete-ui.min.js"></script>
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<input type="hidden" id="erm_member" class="erm_member" value="<?=$randombox_mb['erm_member'];?>" />
		<input type="hidden" id="erm_pop_count" class="erm_pop_count" value="<?=$randombox_mb['erm_pop_count'];?>" />
		<input type="hidden" id="erm_cash_point_total" class="erm_cash_point_total" value="<?=$randombox_mb['erm_cash_point_total'];?>" />

			<!-- Modal Popup -->
			<div class="Modalalert">
				<div class="ModalBackground"></div>
				<div class="ModalPopup">
					<div class="ModalClose"><i class="material-icons">clear</i></div>
					<!-- 복주머니 부분 -->
					<div class="p_content01">
						<div class="p_title">
							<img src="<?=$randombox_img;?>/p_title.png<?=vs_para();?>" alt="땅콩 품은 복주머니" />
						</div>
						<div class="p_exp">
							<img src="<?=$randombox_img;?>/p_exp.png<?=vs_para();?>" alt="마음에 드는 복주머니를 선택하세요">
						</div>
						<ul class="p_choice">
							<li>
								<input type="radio" name="pocket" value="pocket1" id="pocket1">
								<label for="pocket1">복주머니1</label>
							</li>
							<li>
								<input type="radio" name="pocket" value="pocket2" id="pocket2">
								<label for="pocket2">복주머니2</label>
							</li>
							<li>
								<input type="radio" name="pocket" value="pocket3" id="pocket3">
								<label for="pocket3">복주머니3</label>
							</li>
						</ul>
						<div class="p_btnbig">
							<button id="open_btn" class="nochk">바로 열기</button>
						</div>
					</div>
					
					<!-- 쿠폰 부분 -->
					<div class="p_content02" style="display: none;">
						<div class="p_coupon">
							<p><span class="coupon_value"></span>%</p>
							<span class="coupon_time"><span class="coupon_value02"></span>까지 사용가능</span>
						</div>
						<div class="p_get">
							<span class="coupon_value"></span><span>%</span>결제 할인권 획득!
						</div>
						<div class="p_exp">
							<img src="<?=$randombox_img;?>/con02_warn.png<?=vs_para();?>" alt="당첨된 쿠폰은 쿠폰함에서 확인 가능합니다.">
						</div>
						<ul>
							<li><button id="recharge_link" class="p_buy" onclick="location.href='<?=NM_URL?>/recharge.php'">결제하기</button></li>
							<li><button class="p_couponpage" onclick="location.href='<?=NM_URL?>/coupon.php'">쿠폰 보기</button></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /Modal Popup -->

			<div class="evtcontainer">
				<div class="goToTop">
					<img src="<?=$randombox_img;?>/scroll_top.png<?=vs_para();?>" alt="위로가기">
				</div>
				<div class="con01">
					<div class="title"><img src="<?=$randombox_img;?>/title.png<?=vs_para();?>" alt="땅콩 품은 복주머니" /></div>

					<div class="pocket">
						<div class="p_count">
							<p>소비 땅콩 수</p>
							<span class="count_col"><?=$randombox_mb['erm_cash_point_total'];?>&nbsp;</span> <span>/ 10</span>
						</div>
						<div class="p_gauge">
							<div class="gauge_blank">
								<div class="gauge" style="width:<?=$randombox_mb['erm_pop_gauge'];?>px;"></div>
							</div>
						</div>
						<div class="pocket_day"><img src="<?=$randombox_img;?>/con01_day.png<?=vs_para();?>" alt="이벤트 기간" /></div>
					</div>

					<div class="pocket_stat">
						오늘 남은 횟수 <strong><?=$randombox_mb['erm_pop_count'];?></strong>회
					</div>
					<div class="next">
						<img src="<?=$randombox_img;?>/next.png<?=vs_para();?>" alt="참여방법 확인하기" />
					</div>
				</div>

				<div class="con02">
					<div class="con02_tit"><img src="<?=$randombox_img;?>/con02_tit.png<?=vs_para();?>" alt="복주머니 이벤트 참여방법" /></div>
					<ul>
						<li><img src="<?=$randombox_img;?>/con02_step01.png<?=vs_para();?>" alt="웹툰을 보고 땅콩을 사용합니다" /></li>
						<li><img src="<?=$randombox_img;?>/con02_step02.png<?=vs_para();?>" alt="땅콩을 열 개 이상 사용하고 복주머니를 열어봅니다" /></li>
						<li><img src="<?=$randombox_img;?>/con02_step03.png<?=vs_para();?>" alt="원하는 복주머니를 고릅니다" /></li>
						<li><img src="<?=$randombox_img;?>/con02_step04.png<?=vs_para();?>" alt="결제 할인 쿠폰에 100% 당첨!" /></li>
					</ul>
					<div class="con02_warn">
						<img src="<?=$randombox_img;?>/con02_warn.png<?=vs_para();?>" alt="당첨된 쿠폰은 쿠폰함에서 확인 가능합니다" />
						<div class="gotocoupon">
							<button onclick="location.href='<?=NM_URL?>/coupon.php'">쿠폰함 바로가기</button>
						</div>
					</div>
				</div>

				<div class="con03">
					<div class="con03_warn"><img src="<?=$randombox_img;?>/con03_warn.png<?=vs_para();?>" alt="이벤트 유의사항" />
				</div>

			</div>