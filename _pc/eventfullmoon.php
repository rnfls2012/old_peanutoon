<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/eventfullmoon.css<?=vs_para();?>" />
		<style type="text/css">
		.evt-container {
			background-image: url(<?=$fullmoon_img;?>/bg01.png<?=vs_para();?>);
			background-repeat: no-repeat;
			background-size: auto;
			background-position: center top;
			background-color: #2e2e3c;
		}
		.evt-content-wrap {
			background-color: #2e2e3c;
			background-image:url(<?=$fullmoon_img;?>/bg02.png<?=vs_para();?>), url(<?=$fullmoon_img;?>/bg03.png<?=vs_para();?>), url(<?=$fullmoon_img;?>/bg04.png<?=vs_para();?>);
			background-position: left, right, bottom;
			background-repeat: no-repeat;
			background-size: auto, auto, 100%;
		}
		.evt-content .coupon {
			background-image: url(<?=$fullmoon_img;?>/con-coupon.png<?=vs_para();?>);
			background-repeat: no-repeat;
		}
		.evt-content .warn {
			background-image: url(<?=$fullmoon_img;?>/con-line.png<?=vs_para();?>);
			background-position: center top;
			background-repeat: no-repeat;
		}
		.ModalPopup { background-image: url(<?=$fullmoon_img;?>/p_bg.png<?=vs_para();?>); }
		.ModalPopup .p_content .p_coupon { 
			background-image: url(<?=$fullmoon_img;?>/p_coupon.png<?=vs_para();?>);
			background-repeat: no-repeat;
			background-position: center;
		}

		</style>
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/eventfullmoon.js<?=vs_para();?>"></script>
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/jquery.autocomplete.css">
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/jquery.autocomplete-ui.min.js"></script>
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<input type="hidden" id="member_no" class="member_no" value="<?=$nm_member['mb_no'];?>" />
		<input type="hidden" id="total_coupon" class="total_coupon" value="<?=$total_coupon;?>" />

		<!-- Modal Popup -->
			<div class="Modalalert">
				<div class="ModalBackground"></div>
				<div class="ModalPopup">
					<div class="ModalClose"><i class="material-icons">clear</i></div>
					<div class="p_content">
						<div class="p_coupon">
							<p><span class="coupon_value"></span>%</p>
							<span class="coupon_time"><span class="coupon_value02"></span>일까지 사용가능</span>
						</div>
						<div class="p_get">
							<span class="coupon_value"></span><span>%</span> 결제 할인권 발급!
						</div>
						<div class="p_exp">
							<ol>
								<li>해당 쿠폰은 중복 사용이 불가합니다.</li>
								<li>해당 쿠폰은 9,900원 이상 상품부터 적용 가능합니다.</li>
								<li>해당 쿠폰은 중복 할인이 불가합니다.</li>
								<li>해당 쿠폰의 사용기간은 2018년 3월 10일까지 입니다.</li>
							</ol>
							<img src="<?=$fullmoon_img;?>/p_info.png<?=vs_para();?>">
						</div>
						<ul>
							<li><button id="recharge_link" class="p_buy" onclick="location.href='<?=NM_URL?>/recharge.php'">결제하기</button></li>
							<li><button class="p_couponpage" onclick="location.href='<?=NM_URL?>/coupon.php'">쿠폰 보기</button></li>
						</ul>
					</div>
				</div>
			</div>
		<!-- /Modal Popup -->

		<div class="evt-container">
			<div class="evt-header-bg">
				<img src="<?=$fullmoon_img;?>/title_moon.png<?=vs_para();?>" alt="보름달" />
			</div>
			<div class="evt-title">
				<img src="<?=$fullmoon_img;?>/title_copy.png<?=vs_para();?>" alt="정월대보름 달맞이 축제">
			</div>
			<div class="evt-term">
				<img src="<?=$fullmoon_img;?>/title_term.png<?=vs_para();?>" alt="이벤트 기간 : 2018년 3월 2일부터 2018년 3월 6일까지(5일간)">
			</div>
			<div class="evt-content-wrap">
				<div class="evt-content">
					<img src="<?=$fullmoon_img;?>/con-info.png<?=vs_para();?>">
					<div class="coupon">
						<div class="coupon-badge"><img src="<?=$fullmoon_img;?>/con-coupon-badge.png<?=vs_para();?>" alt="선착순 200명" /></div>
						<img src="<?=$fullmoon_img;?>/con-coupon-text.png<?=vs_para();?>" alt="땅콩 결제 15% 할인 쿠폰">
						<p>남은 수량 : <span class="value11"><?=$total_coupon;?></span>장</p>
					</div>

					<button id="open_btn" class="<?=$nochk;?>">쿠폰 발급</button>
					<span><img src="<?=$fullmoon_img;?>/con-info02.png<?=vs_para();?>"></span>
					<div class="warn">
						<img src="<?=$fullmoon_img;?>/warn.png<?=vs_para();?>">
					</div>
				</div>
			</div>
		</div>