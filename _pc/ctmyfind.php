<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/ct.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/ct.js<?=vs_para();?>"></script>

		<div class="certify_box">
			<div class="certify_box_bg">
				<div id="find_idpw">
					<div class="find_idpw">
						<h5>ID/패스워드 찾기</h5>
						<div class="idpw_btnwrap idpw_head">
							<div class="idpw_kcp">
								<a href="#adult" onclick="return auth_type_check_web('<?=NM_KCP_CR_URL.'/'.NM_PC.'/start_idpw.php';?>');">
									<i class="fa fa-mobile" aria-hidden="true"></i><br />본인인증
								</a>
							</div>
							<div class="idpw_email">
								<a href="#" id="find_email_idpw" class="idpw_e">
									<i class="fa fa-envelope-o" aria-hidden="true"></i><br />이메일
								</a>
							</div>
						</div>
					</div>
					<div class="find_idpw">
						<h3 class="hide">이메일인증</h3>
						<div class="idpw_btnwrap">
							<div id="find_email_idpw_txt" class="idpw_emailtxt">
								<form name="idpw_mail_form" id="idpw_mail_form" method="post" action="<?=NM_PROC_URL?>/ctmyfindmail.php" onsubmit="return idpw_mail_submit();">
									<input type="text" id="idpw_mail" name="idpw_mail" placeholder="이메일 주소를 입력하세요" value="" autocomplete="off" />
									<input type="submit" id="idpw_mail_ok" value="인증메일 보내기" />
								</form>
							</div>
							<div class="idpw_exp">
								<ol>
									<?if($row['clt_mb_find1']!=''){?><li><?=stripslashes(nl2br($row['clt_mb_find1']));?></li><?}?>
									<?if($row['clt_mb_find2']!=''){?><li><?=stripslashes(nl2br($row['clt_mb_find2']));?></li><?}?>
									<?if($row['clt_mb_find3']!=''){?><li><?=stripslashes(nl2br($row['clt_mb_find3']));?></li><?}?>
									<?if($row['clt_mb_find4']!=''){?><li><?=stripslashes(nl2br($row['clt_mb_find4']));?></li><?}?>
									<?if($row['clt_mb_find5']!=''){?><li><?=stripslashes(nl2br($row['clt_mb_find5']));?></li><?}?>
								</ol>
							</div>
						</div>
					</div>
				</div>
				<div id="result_idpw" class="result_idpw">
					<h5>ID/패스워드 변경</h5>
					<form name="idpw_form" id="idpw_form" method="post" onsubmit="return idpw_submit();">
						<table>
							<tr>
								<th>아이디</th>
								<td>
									<ul id="mb_id_list">
										<li>없음</li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>비밀번호</th>
								<td class="pw_input">
									<label for="mb_pw">변경 비밀번호</label> <input type="password" name="mb_pw" />
									<label for="mb_pw_check">변경 비밀번호 확인</label> <input type="password" name="mb_pw_check" />
									<input type="submit" value="확인">
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>