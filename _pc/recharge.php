<? include_once '_common.php'; // 공통 

$device = "pc";
$row_mb = mb_get($nm_member['mb_id']);

?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/recharge.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_PC_URL;?>/js/recharge.js<?=vs_para();?>"></script>
				<div id="recharge">		
						<h4><?=$d_logmenu['recharge'];?></h4>
						<div class="mycoin">
							<span class="mybasket"><i class="fa fa-user-circle-o" aria-hidden="true"></i> <strong><?=$nm_member['mb_id'];?></strong> 님의 <?=$nm_config['cf_cash_point_unit_ko'];?>바구니</span>
							<ul>
								<li>
									<div class="mycoin_con">
										<span class="mctitle">보유 <?=$nm_config['cf_cash_point_unit_ko'];?></span>
										<span class="mcnum"><?=number_format($nm_member['mb_cash_point']);?></span> <img src="<?=NM_IMG.$nm_config['cf_cash'];?>">
									</div>
								</li>
								<li>
									<div class="mycoin_con">
										<span class="mctitle">보유 <?=$nm_config['cf_point_unit_ko'];?></span>
										<span class="mcnum"><?=number_format($nm_member['mb_point']);?></span> <img src="<?=NM_IMG.$nm_config['cf_point'];?>">
									</div>
								</li>
							</ul>
						</div>
						
						<? if($row_banner['embc_cover'] != ''){ ?>
						<div class="coin_event">
							<img src="<?=$banner_img_url;?>" alt="<?=$banner_val['emb_name'];?>"/>
						</div>
						<? } ?>
						
						<div class="pointpoak_event">
							<a href="<?=NM_URL?>/pointpark.php"><img src="<?=NM_PC_IMG."pointpark/pointpark_pc.png";?>" alt="포인트파크링크"></a>
						</div>

						<div class="recharge_info">
							<div class="recharge_left">
								<form name="recharge_form" id="recharge_form" method="post" action="<?=NM_PROC_URL;?>/recharge.php" onsubmit="return recharge_payway_submit();">

									<div class="mar_section02">
										<? if(count($crp_member_arr) > 0){ ?>
										<div class="first_charge">
											<span class="fc_title"><?=$crp_event_text;?></span>
											<ol>
											<? foreach($crp_member_arr as $crp_key => $crp_val){
												$er_cash_point = $crp_val['er_cash_point'];
												$er_point = $crp_val['er_point'];
												$data_er_point = $crp_val['er_point'];
												if($crp_val['crp_point'] ==  0){ $data_er_point = 0; }

												// APP sale 이며 버전이 1.4일 경우
												$mar_pay_arr = cs_crp_won_txt($crp_val);
												$mar_pay_con_app_sale = $mar_pay_arr[0];
												$mar_pay_price = $mar_pay_arr[1];
												
											?>
												<li class="config_recharge_price_<?=$crp_count_arr?>" id="crp_no_id<?=$crp_val['crp_no']?>" data-crp_cash_point="<?=$crp_val['crp_cash_point'];?>" data-er_cash_point="<?=$crp_val['er_cash_point'];?>" data-crp_point="<?=$crp_val['crp_point'];?>" data-er_point="<?=$data_er_point;?>" data-crp_won="<?=$crp_val['crp_won'];?>" data-crp_coupondc_won="<?=$crp_val['crp_coupondc_won_txt'];?>" data-crp_coupondc="<?=$crp_val['crp_coupondc_txt'];?>">
													<label class="mar_radio" for="crp<?=$crp_count_arr?>">
														<div class="pay_check"> <!-- 라디오버튼 -->
															<input type="radio" id="crp<?=$crp_count_arr?>" name="crp_no" value="<?=$crp_val['crp_no']?>" data-payway_limit="<?=$crp_val['crp_payway_limit_js']?>" <?=$crp_val['crp_no_select']?> />
															<span></span><!-- /라디오버튼 -->
														</div>
														<div class="mar_pay_con <?=$mar_pay_con_app_sale;?>">
															<span><?=cash_point_view($crp_val['crp_cash_point'], 'n', 'y', 'y');?>
															</span>&nbsp;
															<?if($crp_val['er_cash_point'] > 0){?>
															<span class="minibonus">+ <?=cash_point_view($crp_val['er_cash_point'], 'n', 'y', 'y');?></span>
															<?}?>
															<?if($crp_val['crp_point'] > 0){?>
															 <span>+ <?=point_view($crp_val['crp_point'], 'n', 'y', 'y');?></span> 
																<?if($crp_val['er_point'] > 0){?>
																	<span class="minibonus">+<?=point_view($crp_val['er_point'], 'n', 'y', 'y');?></span>
																<?}?>
															<?}?>
														</div>
														<div class="mar_pay_price">
															<?=$mar_pay_price;?>
														</div>
													</label>
												</li>
											<? $crp_count_arr++; }?>
											</ol>
										</div>
										<? } ?>

										<div class="charge">
											<span class="charge_title"><?=$nm_config['cf_cash_point_unit_ko'];?> 충전</span>
											<ol>
											<? foreach($crp_base_arr as $crp_key => $crp_val){
												$er_cash_point = $crp_val['er_cash_point'];
												$er_point = $crp_val['er_point'];
												$data_er_point = $crp_val['er_point'];
												if($crp_val['crp_point'] ==  0){ $data_er_point = 0; }

												// APP sale 이며 버전이 1.4일 경우
												$mar_pay_arr = cs_crp_won_txt($crp_val);
												$mar_pay_con_app_sale = $mar_pay_arr[0];
												$mar_pay_price = $mar_pay_arr[1];
											?>
												<li class="config_recharge_price_<?=$crp_count_arr?>" id="crp_no_id<?=$crp_val['crp_no']?>" data-crp_cash_point="<?=$crp_val['crp_cash_point'];?>" data-er_cash_point="<?=$crp_val['er_cash_point'];?>" data-crp_point="<?=$crp_val['crp_point'];?>" data-er_point="<?=$data_er_point;?>" data-crp_won="<?=$crp_val['crp_won'];?>" data-crp_coupondc_won="<?=$crp_val['crp_coupondc_won_txt'];?>"
												 data-crp_coupondc="<?=$crp_val['crp_coupondc_txt'];?>">
													<label class="mar_radio" for="crp<?=$crp_count_arr?>">
														<div class="pay_check"> <!-- 라디오버튼 -->
															<input type="radio" id="crp<?=$crp_count_arr?>" name="crp_no" value="<?=$crp_val['crp_no']?>" data-payway_limit="<?=$crp_val['crp_payway_limit_js']?>" <?=$crp_val['crp_no_select']?> />
															<span></span><!-- /라디오버튼 -->
														</div>
														<div class="mar_pay_con <?=$mar_pay_con_app_sale;?>">
															<span><?=cash_point_view($crp_val['crp_cash_point'], 'n', 'y', 'y');?>
															</span>&nbsp;
															<?if($crp_val['er_cash_point'] > 0){?>
															<span class="minibonus">+ <?=cash_point_view($crp_val['er_cash_point'], 'n', 'y', 'y');?></span>
															<?}?>
															<?if($crp_val['crp_point'] > 0){?>
											  				<span>+ <?=point_view($crp_val['crp_point'], 'n', 'y', 'y');?></span> 
																<?if($crp_val['er_point'] > 0){?>
																	<span class="minibonus">+<?=point_view($crp_val['er_point'], 'n', 'y', 'y');?></span>
																<?}?>
															<?}?>
														</div>
														<div class="mar_pay_price">
															<?=$mar_pay_price;?>
														</div>
													</label>
												</li>
											<? $crp_count_arr++; }?>
											</ol>
										</div>
									</div>

									<div class="payment mar_section03">
										<div class="clear">
											<h3 class="fleft">결제방법</h3>
											<a data-href="<?=NM_URL;?>/coupon.php" class="fright coupondc_btn">쿠폰함</a>
										</div>
										<ol>
											<?  $i=0;
												foreach($nm_config['cf_payway'] as $payway_key => $payway_val){
												$i++;

												$payway_view = $payway_val[2];
												$payway_img_name = "";
												if($payway_val[3] != ''){ 
													$payway_img_name = trim(str_ireplace($payway_val[0], "", $payway_val[2]));
													$payway_view = '<img src="'.$payway_val[3].'" alt="'.$payway_val[2].'" /><strong>'.$payway_img_name.'</strong>'; 
												}

												$payway_select = "";
												if($payway_key == $_mb_payway_no && $_mb_payway_no != ''){ $payway_select = "checked"; }
												?>
											<li>
												<label class="mar_pay_radio" for="payway<?=$payway_key?>">
													<div class="mar_pay_method"> <!-- 라디오버튼 -->
														<input type="radio" class="<?=$payway_val[1];?>" id="payway<?=$payway_key?>" name="payway_no" value="<?=$payway_key;?>" <?=$payway_select;?> />
														<span><?=$payway_view?></span><!-- /라디오버튼 -->
													</div>
												</label>
											</li>
											<? }?>
										</ol>
									</div>
									<? foreach($nm_config['cf_payway'] as $payway_key => $payway_val) { ?>
									<div id="mar_<?=$payway_val[1];?>_guide" class="mar_guide">
										<input type="hidden" id="clt_<?=$payway_val[1];?>" name="clt_<?=$payway_val[1];?>" value="<?=$clt_row['clt_'.$payway_val[1]];?>">
										<span><?=$payway_val[2];?> 결제 안내</span>
										<?=nl2br($clt_row['clt_'.$payway_val[1]]);?>
									</div>
									<? } ?>
									
									<? if($coupondc_list_ck == true){ ?>
									<!-- 쿠폰 -->
									<div class="coupondc_bg">
										<input type="hidden" name="coupondc" id="coupondc" value="<?=$_mb_coupondc?>">
										<ul>
											<li>
												<span>[<?=$coupondc_list['txt_coupondc_type'];?>] </span> 
												<?=$coupondc_list['txt_coupondc_rate'];?> <strong>적용</strong>
											</li>
											<li><?=$coupondc_list['txt_coupondc_name'];?></li>
											<li>
												발급일 : <?=$coupondc_list['txt_coupondc_date'];?>
											</li>
											<li>
												유효기간 : 
												<?=$coupondc_list['txt_coupondc_available_date_start'];?> ~				<?=$coupondc_list['txt_coupondc_available_date_end'];?>
											</li>
										</ul>
										<div class="coupon_cancel">
											<a data-href="<?=NM_URL;?>/recharge.php" class="coupondc_btn"><i class="fa fa-times"></i></a>
										</div>
									</div>
									<!-- 쿠폰 end -->
									<? } ?>
								</form>
							</div>
							<div class="recharge_right">
								<span class="cl_title"><?=$nm_config['cf_cash_point_unit_ko'];?>충전</span>
								<ol>
									<? for($i=1; $i<=6; $i++) { 
										  if($long_text_row['clt_recharge'.$i] != "") { ?>
											 <li><?=$long_text_row['clt_recharge'.$i];?></li>
									   <? } // end if ?>
									<? } // end for ?>
								</ol>
								<hr />
								<span class="cl_title">결제금액 확인</span>
								<ul>
									<li><span class="cr_article">선택한 항목</span><span id="crp_point_chk" class="cr_pay"><strong>&nbsp;</strong></span></li>
									<li><span class="cr_article">보너스</span><span id="er_point_chk" class="cr_pay"><strong>&nbsp;</strong></span></li>
									<li><span class="cr_article">합계</span><span id="sum_point_chk" class="cr_pay"><strong>&nbsp;</strong></span></li>
									<li class="cr_coupondc"><span class="cr_article">할인금액</span><span id="sum_coupondc" class="cr_pay"><strong>&nbsp;</strong></span></li>
									<li><span class="cr_article02">결제할 금액</span><span id="crp_won_chk" class="cr_pay02"><strong>&nbsp;</strong></span></li>
									<?php
                  if ( $row_mb['mb_pay_agree'] === 'n' ) {
                      ?>
                      <li>
                          <div class="pay_agree">
                              <input type="checkbox" value="y" id="pay_agree" name="mb_agree" />
                              <label for="pay_agree">개인정보 처리방침 동의 (최초 1회)</label>
                              <label><a href="<?=NM_DOMAIN?>/siteprivacy.php" id="see_policy" target="_blank">내용보기</a></label>
                          </div>
                      </li>
                      <?
                  }
					  
                  if(mb_class_permission('a') == true){ /* 181001 결제 테스트모드 */
                      ?>
                      <li>
                          <div class="pay_agree">
                              <input type="checkbox" value="y" id="cf_pg_test_ctrl" name="cf_pg_test_ctrl" />
                              <label for="cf_pg_test_ctrl">결제테스트 모드 (관리자)</label>
                          </div>
                      </li>
                      <?
                  }
					  
                  if(mb_class_permission('a') == true){ /* 181001 결제 패스모드 */
                      ?>
                      <li>
                          <div class="pay_agree">
                              <input type="checkbox" value="y" id="cf_pg_pass_ctrl" name="cf_pg_pass_ctrl" />
                              <label for="cf_pg_pass_ctrl">결제대행패스-테스트 모드 (관리자)</label>
                          </div>
                      </li>
                      <?
                  }
                  ?>
									<li><button onclick="recharge_payway_submit('<?=$device?>');">결제하기</button></li>
								</ul>
							</div>
						</div>
				</div>
