<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/event.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/event.js<?=vs_para();?>"></script>

			<div id="event" class="event_list_wrap">
				<div class="event_list">
					<ol>
						<? foreach($event_arr as $key => $val) {?>
						<li>
							<a href="<?=$val['eme_url'];?>">
								<div class="event_thumbnail">
									<img src="<?=$val['eme_img_url'];?>" class="thumb" alt="<?=$val['eme_name'];?>" />
								</div>
								<div class="list_contents">
									<span class="event_title"><?=$val['eme_name'];?></span>
										<?=stripslashes(nl2br($val['eme_summary']));?>
									<span class="event_date"><strong>이벤트 기간</strong><?=$val['eme_date_start']." ~ ".$val['eme_date_end'];?></span>
									</div>
								<button class="event_detail">이벤트 상세보기</button>
							</a>
						</li>
						<? } // end foreach ?>
					</ol>
				</div> <!-- /event_list -->
			</div> <!-- /event_list_wrap -->

			</div> <!-- /contents -->