<?
include_once '_common.php'; // 공통

/* 버튼 텍스트 조절(패키지 있으면 구매, 없으면 소장) 0802 */
$btn_txt = "소장";
if($comics['cm_package'] == 'y') {
	$btn_txt = "구매";
} // end if
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/my.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/my.js<?=vs_para();?>"></script>
			<div class="container_wrap">
				<div class="library_listtop_wrap">
					
					<!-- 표지 사진 -->
					<div class="library_listtop_img">
						<img src="<?=$comics['cm_cover_url'];?>" alt="<?=$comics['cm_series'];?>" />
					</div>

					<!-- 작품 이름, 작가, 장르 및 소장율 -->
					<div class="library_listtop_con">
						<div class="library_listtop_title">
							<h3><?=$comics['cm_series'];?></h3>
							<span class="library_listtop_author"><?=$comics['cm_professional_info'];?></span>
							<span class="library_listtop_genre"><?=$small_txt;?></span>
						</div>

						<span class="library_listtop_ptext">이 작품을 <strong><?=$buy_percent;?>%</strong> 소장중 입니다</span>
						<!-- 소장율 퍼센티지 바 -->
						<div class="library_listtop_percentbar">
							<div class="library_listtop_percentbar_inner" style="width: <?=$buy_percent;?>%;"><!-- 여기다가 width 값 -->
							</div>
						</div>
					</div>

					<div class="clear"></div>

				</div> <!-- /library_listtop_wrap -->

				<div class="library_listcon">
				<!-- 화 리스트 전체 배열 시작 -->
				<? foreach($episode_arr as $total_key => $total_val) { 
						// 이미지 url
						$img_url = img_url_para($total_val['ce_cover'], $total_val['ce_reg_date'], $total_val['ce_mod_date']);
						if($img_url == "") {
							$img_url = NM_MO_IMG."common/no_img.png";
						} // end if

						// 활성화, 비활성화 클래스 / 버튼 활성화, 비활성화
						$activate = "library_listcon_thumb";
						$activate_num = "library_listcon_thumb_";
						$button_txt = "소장중";
						if($total_val['ce_purchased'] == '') {
							$activate .= " nothave";
							$activate_num .= "buy";
							if($total_val['ce_pay_unit'] == "0땅콩") { $total_val['ce_pay_unit'] = "무료"; } // 소장구분 버튼으로 변경 0802
							$button_txt = $total_val['ce_pay_unit']." ".$btn_txt;
						} else {
							$activate_num .= "have";
						} // end else

						/* 업데이트 표시할 시 사용
						// 화 업데이트 유무
						$update = "icon_down";
						if($total_val['ce_icon_up'] != "") {
							$update = "icon_up";
						} // end if
						*/

						// 이벤트 구분
						$ce_txt = $total_val['ce_txt'];
						if($total_val['ce_event_free'] == "event_free") { // 이벤트 무료 일때
							$ce_txt .= " [무료 이벤트!]";
						} else if($total_val['ce_event_sale'] == "event_sale") { // 이벤트 할인 일때
							$ce_txt .= " [할인 이벤트!]";
						} // end else if

						// 버튼 텍스트 구분 / 소장구분 버튼으로 변경 0802
						if($comics['cm_package'] == 'y') {
							if(($total_val['ce_purchased'] == '' && $total_val['ce_notice'] != '') || 
								($total_val['ce_purchased'] == '' && $total_val['ce_pay_unit'] == '무료')) { // 공지사항일때, 무료일때
								$button_txt = $total_val['ce_pay_unit'];
							} // end if
						} // end if
						
				?>

					<?=$total_val['ce_url'];?>
						<div class="<?=$activate;?>">
							<span class="library_listcon_thumb_title"><?=$ce_txt;?></span>
							<span class="<?=$activate_num;?>"><?=$button_txt;?></span>
							<div class="clear"></div>
							<img src="<?=$img_url;?>" alt="<?=$total_val['ce_chapter'];?>">
						</div>
					</a>
			<? } // end foreach ?>
				</div>
			</div>
	</div> <!-- /contents -->