<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/eventfamily.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_URL;?>/js/eventfamily.js<?=vs_para();?>"></script>
		<style type="text/css">
			.evt-container {
				background-image: url(<?=$family_img?>/bg04.png<?=vs_para();?>), url(<?=$family_img?>/bg03.png<?=vs_para();?>), url(<?=$family_img?>/bg02.png<?=vs_para();?>), url(<?=$family_img?>/bg01.png<?=vs_para();?>);
				background-repeat: no-repeat, no-repeat, no-repeat, repeat-x;
				background-size: auto;
				background-position: right top, left top, center top, left top;
			}
			
			.evt-content .c03 {
				background-image: url(<?=$family_img?>/bg07.png<?=vs_para();?>);
				background-size: auto;
				background-position: center;
			}
			
			.evt-content .evt-coupon {
				background-image: url(<?=$family_img?>/bg05.png<?=vs_para();?>),url(<?=$family_img?>/bg06.png<?=vs_para();?>);
				background-repeat: no-repeat;
				background-size: auto;
				background-position: left bottom, right bottom;
			}
			
			.evt-content .coupon {
				background-image: url(<?=$family_img?>/c03_couponbg.png<?=vs_para();?>);
				background-repeat: no-repeat;
			}
			
			.evt-content .c04 {
				background-image: url(<?=$family_img?>/bg10.png<?=vs_para();?>), url(<?=$family_img?>/bg09.png<?=vs_para();?>);
				background-size: auto, auto;
				background-position: left 55%, 43% top;
				background-repeat: no-repeat;
			}
			
			.evt-content .c04:before {
				background-image: url(<?=$family_img?>/bg08.png<?=vs_para();?>);
			}

			.ModalPopup {
				background-image: url(<?=$family_img?>/bg12.png<?=vs_para();?>), url(<?=$family_img?>/bg11.png<?=vs_para();?>);
				background-repeat: no-repeat,repeat-x;
				background-size: auto;
			}

			.ModalPopup .p_content {
				background-image: url(<?=$family_img?>/bg13.png<?=vs_para();?>);
				background-repeat: no-repeat;
			}


		</style>
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<input type="hidden" id="member_no" class="member_no" value="<?=$nm_member['mb_no'];?>" />
		<input type="hidden" id="total_coupon" class="total_coupon" value="<?=$total_coupon;?>" />

		<!-- Modal Popup -->
		<div class="Modalalert">
			<div class="ModalBackground"></div>
			<div class="ModalPopup">
				<div class="ModalClose"><i class="material-icons">clear</i></div>
				<div class="p_content">
					<div class="p_coupon">
						<img src="<?=$family_img?>/p_title.png<?=vs_para();?>" alt="15% 결제 할인 쿠폰">
					</div>
					<div class="p_get">
						<img src="<?=$family_img?>/p_warn.png<?=vs_para();?>" alt="쿠폰 사용 유의사항">
					</div>
					<ul>
						<li><button id="recharge_link" class="p_buy" onclick="location.href='<?=NM_URL?>/recharge.php'">결제하기</button></li>
						<li><button class="p_couponpage" onclick="location.href='<?=NM_URL?>/coupon.php'">쿠폰 보기</button></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- /Modal Popup -->
		<div class="evt-container">
			<div class="evt-title">
				<img src="<?=$family_img?>/c01_title.png<?=vs_para();?>" alt="피너툰 패밀리">
			</div>
			<div class="evt-content">
				<div class="evt-coupon">
					<div class="c02"><img src="<?=$family_img?>/c02_con.png<?=vs_para();?>" alt="이벤트 설명"></div>
					<div class="c03">
						<div class="coupon">
							<div class="coupon-badge"><img src="<?=$family_img?>/c03_couponbadge.png<?=vs_para();?>" alt="선착순 200명" /></div>
							<img src="<?=$family_img?>/c03_coupontext.png<?=vs_para();?>">
							<p>남은 수량 : <span class="value11"><?=$total_coupon;?></span>장</p>
						</div>
						<!-- 버튼 발급 비활성화 시 nochk 클래스 추가 -->
						<button id="open_btn" class="<?=$nochk;?>"><?=$coupon_off_text;?></button>
						<span class="couponwarn"><img src="<?=$family_img?>/c03_couponwarn.png<?=vs_para();?>" alt="1일 선착순 발급 완료 시 쿠폰 발급이 조기 마감됩니다."></span>
					</div>
				</div>
					<div class="c04">
						<span class="c04_info"><img src="<?=$family_img?>/c04_info.png<?=vs_para();?>" alt="가정의 달에 어울리는 따스한 추천 작품"></span>
						<div class="c04_link">
							<a href="<?=get_comics_url(2024);?>">
								<img src="<?=$family_img?>/c04_link01.png<?=vs_para();?>" alt="호식이 이야기">
							</a>
							<a href="<?=get_comics_url(996);?>">
								<img src="<?=$family_img?>/c04_link02.png<?=vs_para();?>" alt="우리집 신령님">
							</a>
							<a href="<?=get_comics_url(2080);?>">
								<img src="<?=$family_img?>/c04_link03.png<?=vs_para();?>" alt="가족이 되자">
							</a>
							<a href="<?=get_comics_url(2491);?>">
								<img src="<?=$family_img?>/c04_link04.png<?=vs_para();?>" alt="다녀왔어 어서와">
							</a>
							<a href="<?=get_comics_url(1098);?>">
								<img src="<?=$family_img?>/c04_link05.png<?=vs_para();?>" alt="19살에 아빠랑 엄마">
							</a>
						</div>
					</div>
					<div class="c05">
						<img src="<?=$family_img?>/c05_warn.png<?=vs_para();?>" alt="이벤트 유의사항">
					</div>
				</div>
			</div>
		</div>