<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->

			<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL;?>/css/sleepcancel.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_PC_URL;?>/js/sleepcancel.js<?=vs_para();?>"></script>

				<div class="container_wrap">
					<div class="rest_warn">
						1년 이상 미 로그인으로 인해 휴면 처리된 계정입니다. 계정을 활성화 하시겠습니까?
					</div>
					<div class="btnwrap">
						<div class="rest_ok">
							<a href="<?=NM_PROC_URL;?>/sleepcancel.php?mode=cancel">휴면 해제</a>
						</div>
						<div class="rest_no">
							<a href="<?=NM_PROC_URL;?>/sleepcancel.php?mode=keep">다음에 하기</a>
						</div>
					</div>
				</div>
