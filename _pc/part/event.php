<? include_once '_common.php'; // 공통

/* 
	경로: /_pc/cmserial.php 
	경로: /_pc/cmend.php
	경로: /_pc/cmbest.php
	경로: /_pc/cmvolume.php
*/

// 초기화 - slide_no
if($slide_no == '' || $slide_no < 2){ $slide_no = 2; }

/* DB */
$slide_arr = array();
$sql_slide_adult = sql_adult($mb_adult_permission , 'eme_adult');
$sql_slide = " SELECT * FROM epromotion_event where eme_state = 'y' $sql_slide_adult order by eme_date desc ";
$result_slide = sql_query($sql_slide);
while ($row_slide = sql_fetch_array($result_slide)) {
	array_push($slide_arr,  $row_slide);
}
?>

<link rel="stylesheet" type="text/css" href="<?=NM_PC_PART_URL;?>/css/event.css<?=vs_para();?>" />
<script type="text/javascript" src="<?=NM_PC_PART_URL;?>/js/event.js<?=vs_para();?>"></script>
<script type="text/javascript">
<!--
    $(document).on('ready', function() {
		$("#pc_slide<?=$slide_no?> .regular").slick({
			dots: true,
			infinite: true,
			centerMode: true,
            centerPadding: '0px',
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 5000,
			arrows: true
		});
    });
	<? /* fade: true, cssEase: 'linear', 
		  vertical: true, verticalSwiping: true,
	*/ ?>
//-->
</script>
<!-- body space -->
	<!-- wrapper space -->
		<div class="pc_slide_bgcolor" >
			<div id="pc_slide<?=$slide_no?>">
				<div class="pc_slide_bg">
					<section class="regular slider">
						<? foreach($slide_arr as $slide_key => $slide_val){ 
							switch($slide_val['eme_event_type']) {
								case "t": $slide_val['eme_url'] = NM_URL."/eventview.php?event=".$slide_val['eme_no'];
								break;
								case "c": $slide_val['eme_url'] = NM_URL."/comics.php?comics=".$slide_val['eme_event_no'];
								break;
								case "e": $slide_val['eme_url'] = NM_URL."/eventlist.php?event=".$slide_val['eme_event_no'];
								break;
								case "u": $slide_val['eme_url'] = $slide_val['eme_direct_url'];
								break;
							} // end switch
							$eme_url = $slide_val['eme_url'];
							/* 이미지 표지 */
							$slide_img_url = img_url_para($slide_val['eme_cover'], $slide_val['eme_date'], '', 'eme_cover', 'tn');
						?>
						<div>
							<a href="<?=$slide_val['eme_url']?>" target="_self" onclick="a_click_false();"><img src="<?=$slide_img_url;?>" alt="<?=$slide_val['eme_name'];?>"></a>
						</div>
						<? } ?>
					</section>
				</div>
			</div>
		</div>


