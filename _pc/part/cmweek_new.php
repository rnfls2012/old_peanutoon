<? include_once '_common.php'; // 공통 

/* DB */

$cmweek_new_arr = array();

// 오늘 요일 표기 하기 위한...
$cm_public_cycle_in = get_comics_cycle_fix(week_type(7));

$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');

$sql_cmweek_new = " SELECT c.*, c_prof.cp_name as prof_name
					FROM comics c 
					left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
					WHERE 1 $sql_cm_adult 
					AND c.cm_service = 'y' and c.cm_big = 2 and c.cm_end = 'n' 
					AND c.cm_reg_date >= '".NM_TIME_MON_M3_S." ".NM_TIME_HI_start."'
					AND c.cm_reg_date <= '".NM_TIME_YMD." ".NM_TIME_HI_end."'
					ORDER BY cm_reg_date DESC, cm_episode_date DESC";				

// 리스트 가져오기
$cmweek_new_arr =cs_comics_content_list($sql_cmweek_new, substr($nm_config['nm_path'], -1));
?>

		<link rel="stylesheet" type="text/css" href="<?=NM_PC_PART_URL;?>/css/cmweek_new.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_PC_PART_URL;?>/js/cmweek_new.js<?=vs_para();?>"></script>

			<!-- 작품리스트 -->
			<div id="cmweek_new">
				<div class="cmweek_new_head_bg">
					<h2>웹툰 신작 소개</h2> 
				</div>
				<div class="sub_list_bg">
					<div class="sub_list clear" data-limit_define="<?=NM_SUB_LIST_LIMIT;?>" data-limit="<?=$sub_list_limit;?>" data-total="<?=$row_total;?>" data-url="<?=$_SERVER['REQUEST_URI'];?>" data-menu="<?=$_menu;?>" data-bool="<?=$sub_list_bool;?>">

						<
						
						<? foreach($cmweek_new_arr as $cmweek_new_key => $cmweek_new_val){ ?>
						<a href="<?=$cmweek_new_val['cm_serial_url']?>" target="_self" data-week="<?=$cmweek_new_val['cm_week_txt']?>">
							<div class="sub_list_thumb sub_list_thumb_small">
								<div class="icon_cm_type">
									<?=$cmweek_new_val['cm_type_icon'];?>
								</div>
								<div class="icon_event">
									<?=$cmweek_new_val['cm_free_icon'];?>
									<?=$cmweek_new_val['cm_sale_icon'];?>
								</div>
								<div class="icon_date">
									<?=$cmweek_new_val['cm_up_icon'];?>
									<?=$cmweek_new_val['cm_new_icon'];?>
								</div>
								<div class="icon_adult">
									<?=$cmweek_new_val['cm_adult_icon'];?>
								</div>
								<dl>
									<dt><img src="<?=$cmweek_new_val[ "cm_cover_sub"]?>" alt="<?=$cmweek_new_val['cm_series']?>" /></dt>
									<dt class="ellipsis"><span class="cm_title"><?=str_replace("[웹툰판]", "", $cmweek_new_val['cm_series']);?></span></dt>
									<dd class="ellipsis"><?=$cmweek_new_val['cm_small_span']?><?=$cmweek_new_val['cm_end_span']?></dd>
									<dd class="ellipsis"><?=$cmweek_new_val['cm_professional_info']?></dd>
								</dl>
							</div>
						</a>
						<? } /* foreach($cmweek_new_arr as $cmweek_new_key => $cmweek_new_val){  */ ?>
					</div>
				</div>
			</div>
		</div>