<? include_once '_common.php'; // 공통 

/* 경로: /_pc/comics.php */

// 보여주기 갯수
$recommend_count = 3;

/* DB */
$recommend_arr = array();
$recommend_comics = get_comics($_comics);

$recommend_merge = array();
$recommend_arr1 = $recommend_arr2 = $recommend_arr3 = $recommend_arr4 = $recommend_arr5 = array();

$sql_recommend = "select * from comics 
						where cm_no != '".$recommend_comics['cm_no']."' 
						and cm_no != '1763' 
						and cm_small = '".$recommend_comics['cm_small']."' 
						and cm_service = 'y' ";
$sql_recommend_ck = $sql_recommend;

// 키워드로 작품추천
$cm_ck_id_arr = $cm_ck_id_sub_arr = array();
if($recommend_comics['cm_ck_id_sub'] != ''){
	$cm_ck_id_sub_arr		= explode("|", $recommend_comics['cm_ck_id_sub']);
	$sql_recommend_ck.= "and ( ";
	foreach($cm_ck_id_sub_arr as $cm_ck_id_sub_val){
		$sql_recommend_ck.= "cm_ck_id_sub like '".$cm_ck_id_sub_val."' OR ";
	}
	$sql_recommend_ck = substr($sql_recommend_ck,0,strrpos($sql_recommend_ck, "OR"));
	$sql_recommend_ck.= ") ";
}

// 정렬
$sql_recommend_ck.= "order by cm_episode_date";
$sql_recommend.= "order by cm_episode_date";

$result_recommend_ck = sql_query($sql_recommend_ck);
while ($row_recommend_ck = sql_fetch_array($result_recommend_ck)) {
	$row_recommend_ck['cm_cover_sub_url'] = img_url_para($row_recommend_ck['cm_cover_sub'], $row_recommend_ck['cm_reg_date'], $row_recommend_ck['cm_mod_date'], 'cm_cover_sub', 'tn102x102'); // 이미지
	array_push($recommend_arr1,  $row_recommend_ck);
}

if(count($recommend_arr1) < $recommend_count){ // 키워드값이 $recommend_count개 미만이라면...
	$result_recommend = sql_query($sql_recommend);
	while ($row_recommend = sql_fetch_array($result_recommend)) {
		$row_recommend['cm_cover_sub_url'] = img_url_para($row_recommend['cm_cover_sub'], $row_recommend['cm_reg_date'], $row_recommend['cm_mod_date'], 'cm_cover_sub', 'tn102x102'); // 이미지

		if($row_recommend['cm_big'] == $recommend_comics['cm_big'] && $row_recommend['cm_adult'] == $recommend_comics['cm_adult'] ){
			array_push($recommend_arr2,  $row_recommend);
		}else if($row_recommend['cm_big'] == $recommend_comics['cm_big']){
			array_push($recommend_arr3,  $row_recommend);
		}else if($row_recommend['cm_adult'] == $recommend_comics['cm_adult']){
			array_push($recommend_arr4,  $row_recommend);
		}else{
			array_push($recommend_arr5,  $row_recommend);
		}
	}
	$recommend_merge2345 = array_merge($recommend_arr2, $recommend_arr3, $recommend_arr4, $recommend_arr5); /* 위 결과 병합 */
	shuffle($recommend_merge2345);/* 랜덤으로 섞기 */
	$recommend_merge = array_merge($recommend_arr1, $recommend_merge2345); // 앞에 키워드 무조건 보이기
}else{ // 키워드값이 3개 이상이라면...
	$recommend_merge = $recommend_arr1;	
	shuffle($recommend_merge);/* 랜덤으로 섞기 */
}

for($r=0; $r<$recommend_count; $r++){ /* $recommend_count개만 추출 */
	array_push($recommend_arr, $recommend_merge[$r]);
}

?>
<link rel="stylesheet" type="text/css" href="<?=NM_PC_PART_URL;?>/css/recommend.css<?=vs_para();?>" />
<script type="text/javascript" src="<?=NM_PC_PART_URL;?>/js/recommend.js<?=vs_para();?>"></script>

<? if(count($recommend_arr) > 0){ ?>
	<!-- body space -->
		<!-- wrapper space -->
			<!-- container_bg -->
				<div class="episode_recommend">
					<h2><span><i class="fa fa-commenting-o" aria-hidden="true"></i> 추천</span>작품</h2>
					<hr>
					<ul>
						<? foreach($recommend_arr as $recommend_key => $recommend_val){ 
							$db_cm_series_txt = text_change($recommend_val['cm_series'], "]");
						?>
						<li>
							<a href="<?=get_comics_url($recommend_val['cm_no']);?>">
								<img src="<?=$recommend_val['cm_cover_sub_url'];?>" alt="<?=stripslashes($recommend_val['cm_series']);?>" />
								<dl>
									<dt class="ellipsis"><?=stripslashes($db_cm_series_txt);?></dt>
									<dt class="ellipsis"><?=$recommend_val['cm_professional_info'];?></dt>
									<dd><?=stripslashes($recommend_val['cm_somaery']);?></dd>
								</dl>
							</a>
						</li>
						<? } ?>
					</ul>
				</div> <!-- /detail_recommend -->
<? } ?>