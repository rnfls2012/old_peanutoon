<?
include_once '_common.php'; // 공통

error_page();

/* DB */
/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */

$notice_list = array();
$notice_query = "select * from board_notice order by bn_date desc";
$notice_result = sql_query($notice_query);
while($row = sql_fetch_array($notice_result)) {
	array_push($notice_list, $row);
} // end while

$total_count = count($notice_list); // 게시물의 총 수

/* WEB 전용 */
/* PAGING */
$page_row = 15; // 한 페이지에 뿌려줄 게시물 개수
$total_page = ceil($total_count/$page_row); // 총 페이지 수
$curpage = ($_REQUEST['curpage']?$_REQUEST['curpage']:1); // 현재 페이지
$end_num = $curpage * $page_row; // 반복문 처음 기준 값
$start_num = ($end_num - $page_row) + 1;  // 반복문 마지막 기준 값

if($end_num > $total_count) {
	$end_num = $total_count;
} // end if

if($start_num < 0) {
	$start_num = 1;
} // end if

$block_row = 5; // 한 블럭에 보여줄 페이지 개수
$total_block = ceil($total_page/$block_row); // 총 블럭 수
$curblock = ($_REQUEST['curblock']?$_REQUEST['curblock']:1); // 현재 블럭
$block_end_num = $curblock * $block_row; // 반복문 마지막 기준 값
$block_start_num = ($block_end_num - $block_row) + 1; // 반복문 처음 기준 값

if($block_end_num > $total_page) {
	$block_end_num = $total_page;
} // end if

if($block_start_num < 0) {
	$block_start_num = 1;
} // end if

$double_left_page = $block_row; // 왼쪽 맨 끝으로 이동 커서 눌렀을 때 페이지
$double_left_block = 1; // 왼쪽 맨 끝으로 이동 커서 눌렀을 때 블록
$double_right_page = $total_page; // 오른쪽 맨 끝으로 이동 커서 눌렀을 때 페이지
$double_right_block = $total_block; // 오른쪽 맨 끝으로 이동 커서 눌렀을 때 블록

$single_left_page = ($curblock - 1) * $block_row; // 왼쪽 한 칸 이동 커서 눌렀을 때 페이지
$single_left_block = $curblock - 1; // 왼쪽 한 칸 이동 커서 눌렀을 때 블록
$single_right_page = ($curblock * $block_row) + 1; // 오른쪽 한 칸 이동 커서 눌렀을 때 페이지
$single_right_block = $curblock + 1; // 왼쪽 한 칸 이동 커서 눌렀을 때 블록

/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/csnotice.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>