<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->

			<link rel="stylesheet" type="text/css" href="<?=$tksk_ics_html_mode;?>/css/index.css<?=vs_para();?>" />
			<style type="text/css">
				#ics1 .evt_facebook_formgroup { background:url(<?=$tksk_ics_img?>/evt_form_bg.png<?=vs_para();?>) no-repeat center top; }
			</style>
			<script type="text/javascript" src="<?=$tksk_ics_html_mode;?>/js/index.js<?=vs_para();?>"></script>

			<? /* js 스크립트 출력 */
			tksk_js::tksk_ics_peanutoon_js($tksk_order, $tksk_ics);
			?>

			<div class="peanutoon_tkskics">
				<div class="evtcontainer">	
					<div class="evt_header">
						<img src="<?=$tksk_ics_img?>/evt01_header_img.png<?=vs_para();?>" alt="완전판 앱 다운로드" />
					</div>					

					<!-- 170929 추가 -->
					<div class="evt_facebook">
						<img src="<?=$tksk_ics_img?>/evt_facebook_con.png<?=vs_para();?>" alt="완전판 앱 공유 이벤트" />

						<div class="evt_facebook_formgroup">
							<div class="evt_facebook_form">
								<form>
									<input type="text" placeholder="E-메일을 입력하세요(공유하기)" autocomplete="off" id="tksk_ics_email" value="<?=$order['email'];?>" <?=$tksk_email_css;?>>
									<a class="btn" onclick="<?=$tksk_js?>();"><img src="<?=$tksk_ics_img?>/<?=$tksk_js;?>.png<?=vs_para();?>" alt="<?=$tksk_sns_name?>"></a>
								</form>
							</div>
						</div>
					</div>
					<!-- /170929 추가 -->

					<div class="evt_con01">
						<img src="<?=$tksk_ics_img?>/evt01_con01.png<?=vs_para();?>" alt="완전판 앱 다운로드 1" />
					</div>
					<div class="evt_con02">
						<a href="<?=NM_URL;?>/app/app_setapk.php">
							<img src="<?=$tksk_ics_img?>/evt01_con01_btn.png<?=vs_para();?>" alt="완전판 앱 다운로드 버튼" />
						</a>
					</div>
					<div class="evt_con03">
						<img src="<?=$tksk_ics_img?>/evt01_con02.png<?=vs_para();?>" alt="완전판 앱 다운로드 2" />
					</div>
					<div class="evt_con04">
						<img src="<?=$tksk_ics_img?>/evt01_con03.png<?=vs_para();?>" alt="완전판 앱 다운로드 3" />
					</div>
					<div class="evt_con05">
						<img src="<?=$tksk_ics_img?>/evt01_con04.png<?=vs_para();?>" alt="완전판 앱 다운로드 4" />
					</div>
					<div class="evt_con06">
						<img src="<?=$tksk_ics_img?>/evt01_con05.png<?=vs_para();?>" alt="완전판 앱 다운로드 5" />
					</div>
					<div class="evt_con07">
						<img src="<?=$tksk_ics_img?>/evt01_con06.png<?=vs_para();?>" alt="완전판 앱 다운로드 6" />
					</div>
					<div class="evt_con08">
						<img src="<?=$tksk_ics_img?>/evt01_con07.png<?=vs_para();?>" alt="완전판 앱 다운로드 7" />
					</div>
					<div class="evt_con02">
						<a href="<?=NM_URL;?>/app/app_setapk.php">
							<img src="<?=$tksk_ics_img?>/evt01_con01_btn.png<?=vs_para();?>" alt="완전판 앱 다운로드 버튼" />
						</a>
					</div>
					<div class="evt_con09">
						<img src="<?=$tksk_ics_img?>/evt01_con08.png<?=vs_para();?>" alt="완전판 앱 다운로드 8" />
					</div>
				</div>
			</div>