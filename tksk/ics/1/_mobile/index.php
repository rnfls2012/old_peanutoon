<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			
			<link rel="stylesheet" type="text/css" href="<?=$tksk_ics_html_mode;?>/css/index.css<?=vs_para();?>" />
			<style type="text/css">
				.peanutoon_tkskics .evt_facebook_formgroup { background:url(<?=$tksk_ics_img?>/evt_form_bg.png<?=vs_para();?>) no-repeat center top; background-size: 100%;  }
			</style>
			<script type="text/javascript" src="<?=$tksk_ics_html_mode;?>/js/index.js<?=vs_para();?>"></script>

			<? /* js 스크립트 출력 */
			tksk_js::tksk_ics_peanutoon_js($tksk_order, $tksk_ics);	
			?>

			<div class="peanutoon_tkskics">
				<div class="evtcontainer">
					<div class="evt_header">
						<img src="<?=$tksk_ics_img?>/evt01_header_mobile.png<?=vs_para();?>" alt="완전판 앱 다운로드" />
					</div>

					<!-- 170929 추가 -->
					<div class="evt_facebook">
						<img src="<?=$tksk_ics_img?>/evt_facebook_con.png<?=vs_para();?>" alt="완전판 앱 공유 이벤트" />

						<div class="evt_facebook_formgroup">
							<div class="evt_facebook_form">

									<div class="evt_facebook_formlabel"><img src="<?=$tksk_ics_img?>/evt_form_email.png<?=vs_para();?>" alt="<?=$tksk_sns_name?>"></div>
									<input type="text" placeholder="" autocomplete="off" id="tksk_ics_email" value="<?=$tksk_order['email'];?>" <?=$tksk_email_css;?>>
									<span class="evtform_va"><a class="btn" onclick="<?=$tksk_js?>();"><img src="<?=$tksk_ics_img?>/tksk_ics_js.png<?=vs_para();?>" alt="<?=$tksk_sns_name?>"></a></span>
							</div>
						</div>
					</div>
					<!-- /170929 추가 -->

					<div class="evt_complete">
						<a href="<?=$tksk_ics_url_complete;?>">
							<img src="<?=$tksk_ics_img?>/evt_complete_btn.png<?=vs_para();?>" alt="완전판 앱 다운로드 페이지 이동" />
						</a>
					</div>
					<div class="evt_caution">
						<img src="<?=$tksk_ics_img?>/evt_caution.png<?=vs_para();?>" alt="완전판 앱 다운로드 주의사항" />
					</div>
				</div>
			</div>