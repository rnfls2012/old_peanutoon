<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->

			<link rel="stylesheet" type="text/css" href="<?=$tksk_ics_html_mode;?>/css/index.css<?=vs_para();?>" />
			<style type="text/css">
				.peanutoon_tkskics .evt_header { background:url(<?=$tksk_ics_img?>/header_bg02.png<?=vs_para();?>), url(<?=$tksk_ics_img?>/header_bg01.png<?=vs_para();?>); background-repeat: no-repeat, repeat-x; background-size: auto; background-position: center top, left top;
				}
			</style>
			<script type="text/javascript" src="<?=$tksk_ics_html_mode;?>/js/index.js<?=vs_para();?>"></script>

			<? /* js 스크립트 출력 */
			tksk_js::tksk_ics_peanutoon_js($tksk_order, $tksk_ics,'n');
			?>

			<div class="peanutoon_tkskics">
				<div class="evtcontainer">		
					<div class="evt_header">
						<img src="<?=$tksk_ics_img?>/header_title.png<?=vs_para();?>" alt="피너툰과 함께하는 페이스북 이벤트" />
					</div>

					<div class="evt_con01">
						<img src="<?=$tksk_ics_img?>/con01.png<?=vs_para();?>" alt="이벤트 내용" />
					</div>

					<!-- 170929 추가 -->
					<div class="evt_facebook">
						<div class="evt_facebook_formgroup">
							<a class="btn" onclick="tksk_ics_peanutoon_js();">
								<img src="<?=$tksk_ics_img?>/con02_title.png<?=vs_para();?>" alt="이벤트 참여하기">
							</a>
							<div class="evt_facebook_form">
								<form>
									<input type="text" placeholder="E-메일을 입력하세요(공유하기)" autocomplete="off" id="tksk_ics_email" value="<?=$tksk_order['email'];?>" <?=$tksk_email_css;?>>
									<a class="btn" onclick="tksk_ics_peanutoon_js();"><img src="<?=$tksk_ics_img?>/con02_btn.png<?=vs_para();?>" alt="공유하기"></a>
								</form>
							</div>
						</div>
					</div>
					
					<!-- 추가 -->
					<div class="evt_con03">
						<div class="evt_con03_tit"><img src="<?=$tksk_ics_img?>/con03_title.png" alt="피너툰 가입하기" /></div>

						<a href="<?=NM_URL?>/ctjoin.php">
							<img src="<?=$tksk_ics_img?>/con03_btn.png<?=vs_para();?>" alt="피너툰 가입하기" />
						</a>
						<div class="warn"><img src="<?=$tksk_ics_img?>/con03_warn.png<?=vs_para();?>" alt="본 이벤트는 피너툰 회원가입 후 참여하실 수 있습니다."></div>
					</div>
					<div class="evt_con04">
						<img src="<?=$tksk_ics_img?>/con04_title.png<?=vs_para();?>" alt="피너툰 인기 작품 보러가기" />
						<div class="evt_con04_link">
							<a href="<?=get_comics_url(2024)?>">
								<img src="<?=$tksk_ics_img?>/con04_link01.png<?=vs_para();?>" alt="호식이 이야기" />
							</a>
							<a href="<?=get_comics_url(2073)?>">
								<img src="<?=$tksk_ics_img?>/con04_link02.png<?=vs_para();?>" alt="죽은 까마귀의 시선" />
							</a>
							<a href="<?=get_comics_url(2333)?>">
								<img src="<?=$tksk_ics_img?>/con04_link03.png<?=vs_para();?>" alt="작은 집에 사는 커다란 개" />
							</a>
							<a href="<?=get_comics_url(1252)?>">
								<img src="<?=$tksk_ics_img?>/con04_link04.png<?=vs_para();?>" alt="천사씨와 악마님" />
							</a>
							<a href="<?=get_comics_url(557)?>">
								<img src="<?=$tksk_ics_img?>/con04_link05.png<?=vs_para();?>" alt="기막힌 생활툰" />
							</a>
						</div>
					</div>
				</div>
			</div>