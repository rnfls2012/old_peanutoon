$(function(){
	$('#tksk_ics_agree_img').click(function(){
		var tksk_ics_agree = $('#tksk_ics_agree').is(':checked');

		if(tksk_ics_agree == true){
			$('#tksk_ics_agree').prop('checked',false);
		}else{
			$('#tksk_ics_agree').prop('checked',true);
		}
	});
});

function tksk_ics_nonmember(){
	var tksk_ics_nonmember_bool = true;
	var tksk_ics = $('#tksk_ics').val();
	var tksk_ics_number = $('#tksk_ics_number').val();
	// var tksk_ics_name = encodeURI($('#tksk_ics_name').val());
	var tksk_ics_email = $('#tksk_ics_email').val();
	// var tksk_ics_tel = $('#tksk_ics_tel').val();
	var tksk_ics_page = $('#tksk_ics_page').val();
	var tksk_ics_agree = $('#tksk_ics_agree').is(':checked');
	
	var tksk_ics_name = "";
	var tksk_ics_tel = "";
	
	/*
	if(tksk_ics_name == ""){
		alertBoxFocus('이름을 입력하세요.', $('#tksk_ics_name'));	
		tksk_ics_nonmember_bool = false;
		return false;
	}
	*/

	if(tksk_ics_email == ""){
		alertBoxFocus('E-Mail을 입력하세요', $('#tksk_ics_email'));	
		tksk_ics_nonmember_bool = false;
		return false;
	}

	if(email_chk(tksk_ics_email) == false){
		alertBoxFocus('이메일 형식에 맞게 입력해주세요.', $('#tksk_ics_email'));	
		tksk_ics_nonmember_bool = false;
		return false;
	}

	/*
	if(tksk_ics_tel == ""){
		alertBoxFocus('전화번호를 입력하세요.', $('#tksk_ics_tel'));
		tksk_ics_nonmember_bool = false;
		return false;
	}
	*/

	if(tksk_ics_agree == false){
		alertBoxFocus('이벤트 개인정보 동의해주세요.', $('#tksk_ics_agree_checkbox_bg'));
		tksk_ics_nonmember_bool = false;
		return false;
	}

	// 성공시 해당 정보 저장
	if(tksk_ics_nonmember_bool == true){
		var tksk_icecreamsocial_nonmember_access = $.ajax({
			url: nm_url+'/tksk/nonmember.php',
			dataType: 'json',
			type: 'POST',
			data: { tksk_ics: tksk_ics , tksk_ics_number: tksk_ics_number, tksk_ics_name: tksk_ics_name, tksk_ics_email: tksk_ics_email, tksk_ics_tel: tksk_ics_tel, tksk_ics_page:tksk_ics_page},
			beforeSend: function( data ) {
				//console.log('로딩');
			}
		})
		tksk_icecreamsocial_nonmember_access.done(function( data ) {
			tksk_ics_peanutoon_js();
			//console.log('성공');
		});
		tksk_icecreamsocial_nonmember_access.fail(function( data ) {
			//console.log('실패');
		});
		tksk_icecreamsocial_nonmember_access.always(function( data ) {
			//console.log('로딩삭제');
		});		
	}
}	