<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			
			<link rel="stylesheet" type="text/css" href="<?=$tksk_ics_html_mode;?>/css/index.css<?=vs_para();?>" />
			<style type="text/css">
				.peanutoon_tkskics .evt_con02{background:url(<?=$tksk_ics_img?>/tksk_con02_bg.png<?=vs_para();?>) repeat-y; background-size: 100%; }
				.peanutoon_tkskics .evt_con02 .form_agree input[type="checkbox"] + label span{background: url(<?=$tksk_ics_img?>/tksk_con02_chkoff.png<?=vs_para();?>) left top no-repeat; background-size: cover;}
				.peanutoon_tkskics .evt_con02 .form_agree input[type="checkbox"]:checked + label span{ background: url(<?=$tksk_ics_img?>/tksk_con02_chkon.png<?=vs_para();?>) left top no-repeat; background-size: cover; }
			</style>
			<script type="text/javascript" src="<?=$tksk_ics_html_mode;?>/js/index.js<?=vs_para();?>"></script>

			<? /* js 스크립트 출력 */
			tksk_js::tksk_ics_peanutoon_js($tksk_order, $tksk_ics, 'n', 'n');	
			?>

			<div class="peanutoon_tkskics">
				<div class="evtcontainer">
					<div class="evt_header">
						<img src="<?=$tksk_ics_img?>/tksk_tit.png<?=vs_para();?>" alt="BBQ치킨 총 360마리의 행운! 피너툰이 쏜다!" />
					</div>
					<div class="evt_con01">
						<img src="<?=$tksk_ics_img?>/tksk_con01.png<?=vs_para();?>" alt="이벤트 내용" />
					</div>
					<div class="evt_con02">
						<div class="evt_con02_tit">
							<img src="<?=$tksk_ics_img?>/tksk_con02_tit.png<?=vs_para();?>" alt="아래 정보 입력하고 치킨&미니땅콩 받으러 Go Go~!!!" />
						</div>						
						<form onsubmit="return false;">
							<input type="hidden" id="tksk_ics" name="tksk_ics" value="<?=$tksk_ics_row['tksk_no'];?>">
							<input type="hidden" id="tksk_ics_number" name="tksk_ics_number" value="<?=$tksk_order['number'];?>">
							<input type="hidden" id="tksk_ics_page" name="tksk_ics_page" value="<?=$tksk_order['page'];?>">
							
							<div class="formgroup">
								<label for="tksk_ics_email"><img src="<?=$tksk_ics_img?>/tksk_con02_email.png<?=vs_para();?>" alt="email" /></label>
								<input type="text" placeholder="E-Mail을 입력하세요" autocomplete="off" id="tksk_ics_email" required />
							</div>
							<? /*
							<div class="formgroup">
								<label for="tksk_ics_name"><img src="<?=$tksk_ics_img?>/tksk_con02_name.png<?=vs_para();?>" alt="이름" /></label>
								<input type="text" placeholder="이름을 입력하세요" autocomplete="off" id="tksk_ics_name" required />
							</div>
							<div class="formgroup">
								<label for="tksk_ics_tel"><img src="<?=$tksk_ics_img?>/tksk_con02_tel.png<?=vs_para();?>" alt="전화번호" /></label>
								<input type="text" placeholder="전화번호를 입력하세요" autocomplete="off" id="tksk_ics_tel" required />
							</div>
							*/ ?>
							<div class="form_agree">
								<input type="checkbox" id="tksk_ics_agree" name="y" required />
								<label for="tksk_ics_agree"><span id="tksk_ics_agree_checkbox_bg"></span></label>
								<img src="<?=$tksk_ics_img?>/tksk_con02_agree.png<?=vs_para();?>" alt="이벤트 개인정보 동의" id="tksk_ics_agree_img" />
							</div>

							<div class="form_ok">
								<a class="btn" onclick="tksk_ics_nonmember();"><img src="<?=$tksk_ics_img?>/tksk_con02_ok.png<?=vs_para();?>"></a>
							</div>
						</form>
					</div>
					<div class="evt_con03">
						<img src="<?=$tksk_ics_img?>/tksk_con03.png<?=vs_para();?>" alt="이벤트 유의사항" />
					</div>
					<div class="evt_con04">
						<img src="<?=$tksk_ics_img?>/tksk_con04_tit.png<?=vs_para();?>" alt="피너툰 무료 작품 보러가기" />
					</div>

					<div class="evt_con05">
						<a href="<?=NM_URL?>/comics.php?comics=557">
							<img src="<?=$tksk_ics_img?>/tksk_link01.png<?=vs_para();?>" alt="기막힌 생활툰" />
						</a>
					</div>

					<div class="evt_con06">
						<a href="<?=NM_URL?>/comics.php?comics=2061">
							<img src="<?=$tksk_ics_img?>/tksk_link02.png<?=vs_para();?>" alt="잡종툰" />
						</a>
					</div>
				</div>
			</div>