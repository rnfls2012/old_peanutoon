<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->

			<link rel="stylesheet" type="text/css" href="<?=$tksk_ics_html_mode;?>/css/index.css<?=vs_para();?>" />
			<style type="text/css">
				.peanutoon_tkskics .evt_facebook_formgroup { background:url(<?=$tksk_ics_img?>/tksk_con02.png<?=vs_para();?>) no-repeat center top; }
				.peanutoon_tkskics .evt_con01 { background:url(<?=$tksk_ics_img?>/tksk_con03.png<?=vs_para();?>) no-repeat center top; }
			</style>
			<script type="text/javascript" src="<?=$tksk_ics_html_mode;?>/js/index.js<?=vs_para();?>"></script>

			<? /* js 스크립트 출력 */
			tksk_js::tksk_ics_peanutoon_js($tksk_order, $tksk_ics,'n');
			?>

			<div class="peanutoon_tkskics">
				<div class="evtcontainer">		
					<div class="evt_header">
						<img src="<?=$tksk_ics_img?>/tksk_tit.png<?=vs_para();?>" alt="BBQ치킨 총 360마리의 행운! 피너툰이 쏜다!" />
					</div>

					<!-- 170929 추가 -->
					<div class="evt_facebook">
						<img src="<?=$tksk_ics_img?>/tksk_con01.png<?=vs_para();?>" alt="이벤트 내용" />

						<div class="evt_facebook_formgroup">
							<div class="evt_facebook_form">
								<form>
									<input type="text" placeholder="E-메일을 입력하세요(공유하기)" autocomplete="off" id="tksk_ics_email" value="<?=$tksk_order['email'];?>" <?=$tksk_email_css;?>>
									<a class="btn" onclick="tksk_ics_peanutoon_js();"><img src="<?=$tksk_ics_img?>/evt_form_btnshare.png<?=vs_para();?>" alt="공유하기"></a>
								</form>
							</div>
						</div>
					</div>
					<div class="evt_con01">
						<a href="<?=NM_URL?>/ctjoin.php">
							<img src="<?=$tksk_ics_img?>/tksk_joinbtn.png" alt="피너툰 가입하기" />
						</a>
					</div>
					<div class="evt_con02">
						<img src="<?=$tksk_ics_img?>/tksk_con04.png" alt="피너툰 작품 보러가기" />
					</div>
					<div class="evt_con03">
						<a href="<?=NM_URL?>/comics.php?comics=2491">
							<img src="<?=$tksk_ics_img?>/tksk_link01.png" alt="다녀왔어, 어서와" />
						</a>
					</div>
					<div class="evt_con04">
						<a href="<?=NM_URL?>/comics.php?comics=2561">
							<img src="<?=$tksk_ics_img?>/tksk_link02.png" alt="당신의 분부대로, 데스티니" />
						</a>
					</div>
				</div>
			</div>