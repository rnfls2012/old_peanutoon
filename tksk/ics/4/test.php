<?	include_once '_common.php'; // 공통 
	
	include_once (NM_PATH.'/_head.php'); // 공통

	cs_login_check(); // 로그인 체크
	
	if(mb_class_permission() == true){ /* 파트너&관리자만보기 */ 
		cs_alert('현재 관리자&파트너만 보입니다. 등급을 관리자에게 문의 바랍니다.',NM_URL);
		die;		
	}

	$temp_productname	= intval(date('i'));
	$temp_revenue		= $temp_productname * 10;

	/* 티켓소켓 이벤트 */ 
	$tksk_order = array();
	$tksk_order['number'] = get_uniqid();
	$tksk_order['email'] = $nm_member['mb_email'];
	$tksk_order['revenue'] = $temp_revenue;
	$tksk_order['page'] = 'ics'.$tksk_ics_row['tksk_no'].'_test';
	$tksk_order['name'] = $nm_member['mb_no'];
	$tksk_order['productname'] = 't'.$temp_productname.'P'; // 분

	tksk_js::tksk_ics_peanutoon_js($tksk_order, $tksk_ics);

	?>
	<link rel="stylesheet" type="text/css" href="<?=NM_URL;?>/css/receipt.css<?=vs_para();?>" />
	<script type="text/javascript" src="<?=NM_URL;?>/js/receipt.js<?=vs_para();?>"></script>

	<style type="text/css">
		.receipt ul li{ width: 100%; display: block;font-size: 16px; text-align:center; line-height:20px;}
		.receipt table > caption{ padding: 12px; font-size: 20px; background-color: #fad7e6; font-weight:bold; letter-spacing:5pt;  color:#fff;}
	</style>
	
	<div class="receipt">
		<h2>티켓소켓 스크립트 테스트 페이지</h2>
		<ul>
			<li>
				안내문 <br/>
				현재 보시는 페이지는 충전후 충전에 대해서 영수증 출력시 추가되는 TEST페이지 이며, <br/>
				아래 표 ( IceCreamSocialObject Option Value ) 는 <br/>
				충전 영수증 대신 티켓소켓의 Option값을 대신 넣은 것 입니다. <br/>
				실제 페이지는 결제 후 보시면 될 것 같습니다. <br/>
			</li>
			<li>
				실제 페이지 경로 <br/>
				상단 메뉴 중 땅콩 충전소 -> 실제 결제후 -> 땅콩 충전 영수증내역
			</li>
		</ul>
		<table>
			<caption>IceCreamSocialObject Option Value</caption>
			<tr>
				<th>campaign [캠페인 번호]</th>
				<td><?=get_session('ss_tksk_campaign');?></td>
			</tr>
			<tr>
				<th>orderId [충전 결제번호]</th>
				<td><?=$tksk_order['number'];?></td>
			</tr>
			<tr>
				<th>email [충전자 이메일]</th>
				<td><?=$tksk_order['email'];?></td>
			</tr>
			<tr>
				<th>revenue [충전 가격]</th>
				<td><?=$tksk_order['revenue'];?></td>
			</tr>
			<tr>
				<th>name [충전 회원ID]</th>
				<td><?=$tksk_order['name'];?></td>
			</tr>
			<tr>
				<th>productName [충전 상품명]</th>
				<td><?=$tksk_order['productname'];?></td>
			</tr>
			<tr>
				<th>page [페이지정보(피너툰전용)]</th>
				<td><?=$tksk_order['page'];?></td>
			</tr>
		</table>

		<div class="receipt_tksk">
			<img src="<?=$nm_config['nm_url_img'];?>tksk/<?=$tksk_ics;?>/tksk_tit.png<?=vs_para();?>" alt="티켓소켓이벤트내용" />
			<div class="tksk_valign">
				<input type="text" id="tksk_ics_email" placeholder="이메일 주소를 입력하세요" autocomplete="off" value="<?=$tksk_order['email'];?>">
				<span class="group_btn"><button class="tksk_ok" onclick="tksk_ics_peanutoon_js();">페이스북 공유하기</button></span>

			</div>
		</div>
	</div>

<?
	include_once (NM_PATH.'/_tail.php'); // 공통

?>