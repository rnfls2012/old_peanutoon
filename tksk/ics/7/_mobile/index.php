<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			
			<link rel="stylesheet" type="text/css" href="<?=$tksk_ics_html_mode;?>/css/index.css<?=vs_para();?>" />
			<style type="text/css">
				.peanutoon_tkskics .evt_facebook_formgroup { background:url(<?=$tksk_ics_img?>/evt_con02_bg.png<?=vs_para();?>) no-repeat center top; background-size: 100%;  }
				.peanutoon_tkskics .evt_con01 { background:url(<?=$tksk_ics_img?>/evt_con03_bg.png<?=vs_para();?>) no-repeat center top; background-size: 100%;  }
			</style>
			<script type="text/javascript" src="<?=$tksk_ics_html_mode;?>/js/index.js<?=vs_para();?>"></script>

			<? /* js 스크립트 출력 */
			tksk_js::tksk_ics_peanutoon_js($tksk_order, $tksk_ics, 'n');	
			?>

			<div class="peanutoon_tkskics">
				<div class="evtcontainer">
					<div class="evt_header">
						<img src="<?=$tksk_ics_img?>/evt_header.png<?=vs_para();?>" alt="BBQ치킨 총 360마리의 행운! 피너툰이 쏜다!" />
					</div>

					<!-- 170929 추가 -->
					<div class="evt_facebook">
						<img src="<?=$tksk_ics_img?>/evt_con01.png<?=vs_para();?>" alt="이벤트 내용" />

						<div class="evt_facebook_formgroup">
							<a class="btn" onclick="tksk_ics_peanutoon_js();">
								<img src="<?=$tksk_ics_img?>/evt_con02_title.png<?=vs_para();?>" alt="이벤트 참여하기">
							</a>							
							<div class="evt_facebook_form">

									<div class="evt_facebook_formlabel"><img src="<?=$tksk_ics_img?>/tksk_email.png<?=vs_para();?>" alt="<?=$tksk_sns_name?>"></div>
									<input type="text" placeholder="" autocomplete="off" id="tksk_ics_email" value="<?=$tksk_order['email'];?>" <?=$tksk_email_css;?>>
									<span class="evtform_va"><a class="btn" onclick="tksk_ics_peanutoon_js();"><img src="<?=$tksk_ics_img?>/tksk_form_btnshare.png<?=vs_para();?>" alt="공유하기"></a></span>
							</div>
						</div>
					</div>
					<!-- 추가 -->
					
					<!-- /170929 추가 -->
					<div class="evt_con01">
						<a href="<?=NM_URL?>/ctjoin.php">
							<img src="<?=$tksk_ics_img?>/tksk_joinbtn.png" alt="피너툰 가입하기">
						</a>
						<div class="warn"><img src="<?=$tksk_ics_img?>/evt_con03_warn.png" alt="본 이벤트는 피너툰 회원가입 후 참여하실 수 있습니다."></div>
					</div>
					<div class="evt_con02">
						<img src="<?=$tksk_ics_img?>/evt_con04_tit.png" alt="피너툰 작품 보러가기" />
					</div>
					<div class="evt_con03">
						<a href="<?=get_comics_url(978)?>">
							<img src="<?=$tksk_ics_img?>/tksk_link01.png" alt="피너툰 완전판 앱 다운로드 2" />
						</a>
					</div>
					<div class="evt_con04">
						<a href="<?=get_comics_url(1187)?>">
							<img src="<?=$tksk_ics_img?>/tksk_link02.png" alt="피너툰 완전판 앱 다운로드 3" />
						</a>
					</div>
				</div>
			</div>