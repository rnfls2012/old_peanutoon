<? include_once '_common.php'; // 공통 
	// error_page();

	$tksk_para = $_SERVER['PHP_SELF'];
	// echo $_SERVER['PHP_SELF'];

	$tksk_para_arr = explode("/", $tksk_para);
	$tksk_para_arr_cnt = count($tksk_para_arr);

	$tksk_ics_file	= "";
	$tksk_file_name = "index";
		
	if($tksk_para_arr_cnt == 4){
		$tksk_ics		= num_eng_check(tag_filter($tksk_para_arr[count($tksk_para_arr)-1]));	// ICS-이벤트번호		
	}else if($tksk_para_arr_cnt == 5){
		$tksk_ics		= num_eng_check(tag_filter($tksk_para_arr[count($tksk_para_arr)-2]));	// ICS-이벤트
		$tksk_ics_sub	= num_eng_check(tag_filter($tksk_para_arr[count($tksk_para_arr)-1]));	// ICS-이벤트-완료
		if($tksk_ics_sub == "complete"){ $tksk_file_name = "complete"; }						// ICS-이벤트-완료 확인
		if($tksk_ics_sub == "email"){ $tksk_file_name = "email"; }								// ICS-이벤트-이메일 확인
		if($tksk_ics_sub == "test"){ $tksk_file_name = "test"; }								// ICS-이벤트-스크립트 확인
		if($tksk_ics_sub == "popselect"){ $tksk_file_name = "popselect"; }						// ICS-이벤트-팝업선택

	}else if($tksk_para_arr_cnt == 6){
		$tksk_ics		= num_eng_check(tag_filter($tksk_para_arr[count($tksk_para_arr)-3]));	// ICS-이벤트
		$tksk_ics_sub	= num_eng_check(tag_filter($tksk_para_arr[count($tksk_para_arr)-2]));	// ICS-이벤트-완료
		$tksk_ics_thi	= num_eng_check(tag_filter($tksk_para_arr[count($tksk_para_arr)-1]));	// fcb-회원번호
		if($tksk_ics_sub == "complete"){ $tksk_file_name = "complete"; }						// ICS-이벤트번호-완료 확인
		if($tksk_ics_sub == "email"){ $tksk_file_name = "email"; }								// ICS-이벤트번호-이메일 확인
		if($tksk_ics_sub == "test"){ $tksk_file_name = "test"; }								// ICS-이벤트-스크립트 확인
		if($tksk_ics_sub == "popselect"){ $tksk_file_name = "popselect"; }						// ICS-이벤트-팝업선택

	}else{
		cs_alert('관리자에게 문의 바랍니다. 에러사항:필수값에러',NM_URL);
		die;
	}

	// 해당 CMS데이터 가져오기
	$tksk_ics_row = tksk_ics_row($tksk_ics);
	
	if($tksk_ics_row['tksk_state'] != 'y' || $tksk_ics_row == false){
		if(mb_class_permission('a') == false){
			cs_alert('해당 티켓포켓이벤트는 종료되였습니다.',NM_URL);
			die;		
		}
	}
	
	// 접속 통계
	tksk_ics_stats($tksk_file_name, $tksk_ics);

	// 세션저장	
	set_session('ss_tksk_no', $tksk_ics_row['tksk_no']); // 티켓소켓번호 세션저장
	set_session('ss_tksk_campaign', $tksk_ics_row['tksk_campaign']); // 티켓소켓번호 캠페인번호저장
	if($tksk_file_name == 'complete'){
		set_session('ss_tksk_campaign', $tksk_ics_row['tksk_campaign_complete']); // 티켓소켓번호 캠페인complete번호저장
	}

	/* 연결링크여부 */
	$tksk_link = "";
	$tksk_link_bool = false;
	if($tksk_ics_row['tksk_link'] != '' && $tksk_file_name == 'index'){
		$tksk_link = $tksk_ics_row['tksk_link'];
		$tksk_link_bool = true;

	}
	if($tksk_ics_row['tksk_link_complete'] != '' && $tksk_file_name == 'complete'){
		$tksk_link = $tksk_ics_row['tksk_link_complete'];
		$tksk_link_bool = true;
	}

	/* 연결링크가 있다면... */
	if($tksk_link_bool == true && !($tksk_ics_sub =='test' || $tksk_ics_sub =='popselect')){
		goto_url($tksk_link);	// 이동
	}else{
	/* 연결링크가 없다면... 해당페이지 보여주기 */
	
		/* 경로 및 파일 설정 */
		$tksk_ics_route = tksk_ics_route($tksk_ics);

		// echo $tksk_ics_route;
		// die;

		$tksk_ics_polder		= NM_TKSK_PATH."/".$tksk_ics_route;				// 폴더
		$tksk_ics_html			= NM_TKSK_URL."/".$tksk_ics_route;				// js / css
		$tksk_ics_url			= NM_TKSK_URL."/".$tksk_ics;					// url

		$tksk_ics_polder_mode	= $tksk_ics_polder.'/'.$nm_config['nm_mode'];	// 폴더 -pc/모바일
		$tksk_ics_html_mode		= $tksk_ics_html.'/'.$nm_config['nm_mode'];		// js / css -pc/모바일
		$tksk_ics_url_complete	= $tksk_ics_url.'/complete';					// url - ICS-이벤트-완료

		$tksk_ics_file			= $tksk_ics_route.'/'.$tksk_file_name.'.php';	// 파일
		$tksk_ics_root			= NM_TKSK_PATH."/".$tksk_ics_file;				// 전체

		/* img 경로 */
		$tksk_ics_img = NM_IMG.$nm_config['nm_mode'].'/tksk/'.$tksk_ics;
			
		error_page();
		
		// 페이스북 공유 링크 
		// http://www.peanutoon.com/tksk/ics1/피너툰회원번호/페이스북회원번호
		/* URL은 고정이라 위처럼 피너툰회원번호 넣기 불가능함;;; */
		
		/* 티켓소켓 이벤트 */ 
		$tksk_order = array();
		$tksk_order['number'] = get_uniqid();
		$tksk_order['email'] = $nm_member['mb_email'];
		$tksk_order['revenue'] = 0;
		$tksk_order['page'] = $tksk_file_name;
		$tksk_order['name'] = $nm_member['mb_no'];
		$tksk_order['productname'] = 'tksk_event_view'.$tksk_ics; // 상품명인데 이벤트일경우 이벤트라고 명시함
			
		/* 연결 */	
		include_once($tksk_ics_root);
	}
	
?>