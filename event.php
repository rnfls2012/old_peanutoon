<?
include_once '_common.php'; // 공통

error_page();

/* DB */

$event_arr = array();

$eme_adult = sql_adult($mb_adult_permission , 'eme_adult');
$event_sql_where = "eme_state='y' ".$eme_adult;
// $event_sql = "select * from epromotion_event where ".$event_sql_where."  order by eme_no desc ";
// 치킨이벤트 상위
$event_sql_order = "
    SELECT *
    FROM epromotion_event
    WHERE ".$event_sql_where." AND eme_order IS NOT NULL
";

$event_sql = "
    SELECT *
    FROM epromotion_event
    WHERE ".$event_sql_where." AND eme_order IS NULL ORDER BY eme_no DESC
";

$event_order_result = sql_query($event_sql_order);
$event_result = sql_query($event_sql);

while ($event_order_row = sql_fetch_array($event_order_result)) {
    switch($event_order_row['eme_event_type']) {
        case "t": $event_order_row['eme_url'] = NM_URL."/eventview.php?event=".$event_order_row['eme_no'];
            break;
        case "c": $event_order_row['eme_url'] = NM_URL."/comics.php?comics=".$event_order_row['eme_event_no'];
            break;
        case "e": $event_order_row['eme_url'] = NM_URL."/eventlist.php?event=".$event_order_row['eme_event_no'];
            break;
        case "u": $event_order_row['eme_url'] = $event_order_row['eme_direct_url'];
            break;
    } // end switch
    // 이미지
    $size = "tn730x304";
    if(substr($nm_config['nm_path'], -1) == 'c') { // PC 일때
        $size = "tn250x104";
    } // end if
    $event_order_row['eme_img_url'] = img_url_para($event_order_row['eme_cover'], $event_order_row['eme_date'], '', 'eme_cover', $size);
    array_push($event_arr,  $event_order_row);
}

while ($event_row = sql_fetch_array($event_result)) {
	switch($event_row['eme_event_type']) {
		case "t": $event_row['eme_url'] = NM_URL."/eventview.php?event=".$event_row['eme_no'];
		break;
		case "c": $event_row['eme_url'] = NM_URL."/comics.php?comics=".$event_row['eme_event_no'];
		break;
		case "e": $event_row['eme_url'] = NM_URL."/eventlist.php?event=".$event_row['eme_event_no'];
		break;
		case "u": $event_row['eme_url'] = $event_row['eme_direct_url'];
		break;
	} // end switch
	// 이미지
	$size = "tn730x304";
	if(substr($nm_config['nm_path'], -1) == 'c') { // PC 일때
		$size = "tn250x104";
	} // end if
	$event_row['eme_img_url'] = img_url_para($event_row['eme_cover'], $event_row['eme_date'], '', 'eme_cover', $size);
	array_push($event_arr,  $event_row);
} // end while

/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */

/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/event.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>