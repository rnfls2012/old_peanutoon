<?
include_once '_common.php'; // 공통

error_page();

/* DB */
$list_array = array();
$eme_img_url = "";

$slq_eme_state = " AND eme.eme_state='y' ";
if(mb_class_permission('a') == true){
	// $slq_eme_state = " AND (eme.eme_state='y' OR eme.eme_state='r') ";
	$slq_eme_state = "";
}
$sql_ep = "select * from event_pay ep left join epromotion_event eme on ep.ep_no = eme.eme_event_no
where ep.ep_no = '".$_event."' ".$slq_eme_state." ";

$row_ep = sql_fetch($sql_ep);

/* 없는 이벤트 일시 171212 추가 */
if($row_ep['ep_no'] == "") {
	cs_alert("종료되거나, 없는 이벤트 입니다!", NM_URL."/event.php");
	die;
}

if($row_ep['ep_adult'] == 'y') {
	cs_login_check();
} // end if

$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');

// 이벤트 클릭 수 올리기
$click = $row_ep['ep_click'] + 1; // 클릭 수 + 1
$sql_click = "UPDATE event_pay SET ep_click='".$click."' where ep_no='".$_event."'";;
sql_query($sql_click);

if($row_ep['ep_no'] !=''){
	$size = "tn750x261";
	if(substr($nm_config['nm_path'], -1) == 'c') { // PC 일때
		$size = "tn";
	} // end if
	$ep_collection_top_url = img_url_para($row_ep['ep_collection_top'], $row_ep['ep_reg_date'], '', 'ep_collection_top', $size);
	$eme_bgcolor = "#".$row_ep['ep_bgcolor'];

	$list_sql = 
	"select epc.*, c.*, cp.cp_name 
	from event_pay_comics epc 
	left join comics c on epc.epc_comics = c.cm_no 
	left join comics_professional cp on c.cm_professional = cp.cp_no 
	where epc_ep_no = ".$row_ep['ep_no']." ".$sql_cm_adult." 
	order by epc_order ASC";

	$list_result = sql_query($list_sql);

	while($list_row = sql_fetch_array($list_result)) {
		$list_row['comics_url'] = cs_comic_first_url($list_row['cm_no']);
		$list_row['comics_img_url'] = img_url_para($list_row['cm_cover'], $list_row['cm_reg_date'], $list_row['cm_mod_date'], 'cm_cover', 'tn551x282');
		array_push($list_array, $list_row);
	} // end while
}

// SNS 썸네일
$head_thumbnail = $list_array[0]['comics_img_url'];

/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */

/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/eventlist.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>