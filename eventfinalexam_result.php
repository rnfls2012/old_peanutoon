<?php
include_once '_common.php'; // 공통

$finalexam_img_url = NM_IMG.'/finalexam/result'; // 이미지 경로

// 당첨자 정보 가져오기
$efm_win = false;
// $efm_arr = array();
$sql_efm = " select efm_member from event_finalexam_member where 1 AND efm_win=1 ";
$result_efm = sql_query($sql_efm);
for($e=1; $row_efm = sql_fetch_array($result_efm); $e++){
	if($row_efm['efm_member'] == $nm_member['mb_no']){ $efm_win = true;	} // 접속한 사람 당첨자인지 확인
	// array_push($efm_arr, $row_efm);
}

$delivery_js = 'onclick=\'alertBox("피너툰 전국덕후학력평가의 <br/>이벤트 당첨자가 아닙니다.")\'';

if($efm_win == true){
	if($nm_config['nm_mode'] == NM_PC){ // 팝업창
		$delivery_js = 'onclick=\'popup("'.NM_URL.'/eventfinalexam_delivery.php","eventfinalexam_delivery", 710, 770);\'';
	}else{ // 새창
		$delivery_js = 'href=\''.NM_URL.'/eventfinalexam_delivery.php\'';
		if(is_app()==false && is_mobile()){ $delivery_js.= ' target=\'_blank\'';  }
	}
}

// 답안해설 팝업
$answer1_js = 'href=\''.NM_URL.'/eventfinalexam_answer.php?efp_no=1\'';
if(is_app()==false && is_mobile()){ $answer1_js.= ' target=\'_blank\''; }

$answer2_js = 'href=\''.NM_URL.'/eventfinalexam_answer.php?efp_no=2\'';
if(is_app()==false && is_mobile()){ $answer2_js.= ' target=\'_blank\''; }

$answer3_js = 'href=\''.NM_URL.'/eventfinalexam_answer.php?efp_no=3\'';
if(is_app()==false && is_mobile()){ $answer3_js.= ' target=\'_blank\''; }

include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/eventfinalexam_result.php");

include_once (NM_PATH.'/_tail.php'); // 공통