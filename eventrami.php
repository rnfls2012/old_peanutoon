<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-07-09
 * Time: 오후 5:46
 */

include_once '_common.php'; // 공통

cs_alert("종료된 이벤트입니다!", "./index.php");
die;

error_page();

$did_comment = false;

if ( isset($nm_member['mb_no']) ) {
    // 로그인 됨.
    $sql_select = "SELECT COUNT(*) AS cnt FROM event_cheerup WHERE mb_no = ".$nm_member['mb_no'];

    $result = sql_query($sql_select);

    $row_data = sql_fetch_array($result);
    if ( $row_data['cnt'] > 0 ) {
        $did_comment = true;
    }
}

include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/eventrami.php");

include_once (NM_PATH.'/_tail.php'); // 공통