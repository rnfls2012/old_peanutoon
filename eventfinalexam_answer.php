<?php
include_once '_common.php'; // 공통

$efp_no = num_check(tag_get_filter($_REQUEST['efp_no'])); 

$finalexam_img_url = NM_IMG.'/finalexam/result'; // 이미지 경로

if($nm_config['nm_mode'] == NM_PC){ // 팝업창
	$finalexam_answer_img_url = NM_IMG.'/finalexam/answer/pc';
}else{
	$finalexam_answer_img_url = NM_IMG.'/finalexam/answer'; // 이미지 경로
}

$finalexam_answer_img_url.='/'.$efp_no;

if($efp_no == '' || $efp_no > 3){
	pop_close('올바른 방법으로 이용하시기 바랍니다.', NM_URL.'/eventfinalexam_result.php');
	die;
}else{
	// 시험정보 가져오기
	$sql_efp = "SELECT * FROM event_finalexam_period WHERE efp_state = 'y' AND efp_no='".$efp_no."' ";
	$row_efp = sql_fetch($sql_efp);
	$efp_arr = $row_efp;
}

$nm_config['footer'] = false;

//->화면 필요함;;

include_once($nm_config['nm_path']."/_head.sub.php");

include_once($nm_config['nm_path']."/eventfinalexam_answer.php");

include_once($nm_config['nm_path']."/_tail.sub.php");