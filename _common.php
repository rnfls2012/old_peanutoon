<?
/*******************************************************************************
** 공통 변수, 상수, 코드
*******************************************************************************/
error_reporting( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING );

// 보안설정이나 프레임이 달라도 쿠키가 통하도록 설정
header('P3P: CP="ALL CURa ADMa DEVa TAIa OUR BUS IND PHY ONL UNI PUR FIN COM NAV INT DEM CNT STA POL HEA PRE LOC OTC"');

if (!defined('NM_SET_TIME_LIMIT')) define('NM_SET_TIME_LIMIT', 0);
@set_time_limit(NM_SET_TIME_LIMIT);


//==========================================================================================================================
// extract($_GET); 명령으로 인해 page.php?_POST[var1]=data1&_POST[var2]=data2 와 같은 코드가 _POST 변수로 사용되는 것을 막음
//--------------------------------------------------------------------------------------------------------------------------
$ext_arr = array ('PHP_SELF', '_ENV', '_GET', '_POST', '_FILES', '_SERVER', '_COOKIE', '_SESSION', '_REQUEST',
                  'HTTP_ENV_VARS', 'HTTP_GET_VARS', 'HTTP_POST_VARS', 'HTTP_POST_FILES', 'HTTP_SERVER_VARS',
                  'HTTP_COOKIE_VARS', 'HTTP_SESSION_VARS', 'GLOBALS');
$ext_cnt = count($ext_arr);
for ($i=0; $i<$ext_cnt; $i++) {
    // POST, GET 으로 선언된 전역변수가 있다면 unset() 시킴
    if (isset($_GET[$ext_arr[$i]]))  unset($_GET[$ext_arr[$i]]);
    if (isset($_POST[$ext_arr[$i]])) unset($_POST[$ext_arr[$i]]);
}
//==========================================================================================================================

/* 절대경로와 URL 구하기 */
function nm_path()
{
    $result['path'] = str_replace('\\', '/', dirname(__FILE__));
    $tilde_remove = preg_replace('/^\/\~[^\/]+(.*)$/', '$1', $_SERVER['SCRIPT_NAME']);
    $document_root = str_replace($tilde_remove, '', $_SERVER['SCRIPT_FILENAME']);
    $root = str_replace($document_root, '', $result['path']);
    $port = $_SERVER['SERVER_PORT'] != 80 ? ':'.$_SERVER['SERVER_PORT'] : '';
    $http = 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') ? 's' : '') . '://';
    $user = str_replace(str_replace($document_root, '', $_SERVER['SCRIPT_FILENAME']), '', $_SERVER['SCRIPT_NAME']);
    $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
    if(isset($_SERVER['HTTP_HOST']) && preg_match('/:[0-9]+$/', $host))
        $host = preg_replace('/:[0-9]+$/', '', $host);
    $result['url'] = $http.$host.$port.$user.$root;
    return $result;
}

$nm_path = nm_path();
include_once($nm_path['path'].'/config/config.php');   // 설정
/* 설정 - define값[대문자], 회원등급, $nm_config선언, $nm_member선언, $nm_para선언 및 대입 */
unset($nm_path);

include_once(NM_PATH.'/config/dbconnect.php');   // DB 연결

/********************
    공통 라이브러리
********************/
include_once(NM_LIB_PATH.'/common.lib.php');	 // 공통 라이브러리
include_once(NM_LIB_PATH.'/mysql.lib.php');		 // SQL  라이브러리
include_once(NM_LIB_PATH.'/mb.lib.php');		 // 회원 라이브러리

/********************
    KT 라이브러리
********************/
include_once(NM_LIB_PATH.'/cloudfiles_exceptions.php');	 // KT 라이브러리
include_once(NM_LIB_PATH.'/cloudfiles-kt.php');			 // KT 라이브러리
include_once(NM_LIB_PATH.'/cloudfiles_http-kt.php');	 // KT 라이브러리
// include_once(NM_PATH.'/config/kt_connect.php');		 // KT storage 연결 0.7~0.8초 걸려서 해당 파일만 include
include_once(NM_LIB_PATH.'/kt.lib.php');				 // KT 라이브러리


/********************
    SESSION 설정
********************/
@ini_set("session.use_trans_sid", 0);    // PHPSESSID를 자동으로 넘기지 않음
@ini_set("url_rewriter.tags",""); // 링크에 PHPSESSID가 따라다니는것을 무력화함 (해뜰녘님께서 알려주셨습니다.)

if(MEMCACHED == false){
	if($_SERVER['SERVER_NAME'] != NM_REAL_URL_IP){
		session_save_path(NM_SESSION_PATH); // 실서버는 memcached, 개발서버는 file로 session 처리
	}
}

if (isset($SESSION_CACHE_LIMITER))
    @session_cache_limiter($SESSION_CACHE_LIMITER);
else
    @session_cache_limiter("no-cache, must-revalidate");

// ini_set("session.cache_expire", 180); // 세션 캐쉬 보관시간 (분)
ini_set("session.cache_expire", 36000); // 세션 캐쉬 보관시간 (분)
// ini_set("session.gc_maxlifetime", 10800); // session data의 garbage collection 존재 기간을 지정 (초)
ini_set("session.gc_maxlifetime", 0); // session data의 garbage collection 존재 기간을 지정 (초) 0이면 무한 171220
ini_set("session.gc_probability", 1); // session.gc_probability는 session.gc_divisor와 연계하여 gc(쓰레기 수거) 루틴의 시작 확률을 관리합니다. 기본값은 1입니다. 자세한 내용은 session.gc_divisor를 참고하십시오.
ini_set("session.gc_divisor", 10000); // session.gc_divisor는 session.gc_probability와 결합하여 각 세션 초기화 시에 gc(쓰레기 수거) 프로세스를 시작할 확률을 정의합니다. 확률은 gc_probability/gc_divisor를 사용하여 계산합니다. 즉, 1/100은 각 요청시에 GC 프로세스를 시작할 확률이 1%입니다. session.gc_divisor의 기본값은 100입니다.

session_set_cookie_params(0, '/');
ini_set("session.cookie_domain", NM_COOKIE_DOMAIN);

@session_start();

/********************
    공용 변수
********************/
$nm_config_sql = "SELECT * FROM  `config` c LEFT JOIN `config_img` ci ON c.cf_no = ci.cf_no";
$nm_config = sql_fetch($nm_config_sql);

include_once(NM_LIB_PATH.'/thumbnail.lib.php');	 // 썸네일 라이브러리($nm_config변수 포함이라 선언 아래)
include_once(NM_LIB_PATH.'/default.lib.php');	 // 공통 변수 디폴트 값 설정(값 없을시...)
include_once(NM_LIB_PATH.'/array.lib.php');		 // 배열 라이브러리($nm_config변수 포함이라 선언 아래)
include_once(NM_LIB_PATH.'/cs.lib.php');		 // 서비스 라이브러리($nm_config변수 포함이라 선언 아래)
include_once(NM_LIB_PATH.'/app.lib.php');		 // APP 라이브러리($nm_config변수 포함이라 선언 아래)

include_once(NM_LIB_PATH.'/stats.lib.php');		 // 통계 라이브러리($nm_config변수 포함이라 선언 아래)
include_once(NM_LIB_PATH.'/log.lib.php');		 // 로그 라이브러리($nm_config변수 포함이라 선언 아래)

include_once(NM_LIB_PATH.'/mkt.lib.php');		 // 마케팅 라이브러리($nm_config변수 포함이라 선언 아래)
include_once(NM_LIB_PATH.'/tksk.lib.php');		 // 티켓소켓 라이브러리($nm_config변수 포함이라 선언 아래)
include_once(NM_LIB_PATH.'/promoful.lib.php');	 // 풀 페이지 프로모션 라이브러리($nm_config변수 포함이라 선언 아래)
include_once(NM_LIB_PATH.'/vote.lib.php');		 // 투표 라이브러리($nm_config변수 포함이라 선언 아래)
include_once(NM_LIB_PATH.'/randombox.lib.php');	 // 랜덤박스 라이브러리($nm_config변수 포함이라 선언 아래)
include_once(NM_LIB_PATH.'/kakao.lib.php');	 // 알림톡 라이브러리($nm_config변수 포함이라 선언 아래)

include_once(NM_LIB_PATH.'/gtm.lib.php');		 // Google Tag Manager 라이브러리($nm_config변수 포함이라 선언 아래)
include_once(NM_LIB_PATH.'/event.lib.php');		 // 이벤트 라이브러리($nm_config변수 포함이라 선언 아래)

// 4.00.03 : [보안관련] PHPSESSID 가 틀리면 로그아웃한다.
if (isset($_REQUEST['PHPSESSID']) && $_REQUEST['PHPSESSID'] != session_id()){
    goto_url(NM_URL.'/proc/ctlogout.php');
}

// 도메인 www.일때 처리
if(NM_DOMAIN != NM_HTTP_SERVER.$_SERVER['HTTP_HOST']){
	if($_SERVER['SERVER_NAME'] != NM_REAL_URL_IP){
		if(is_app() == false){
			goto_url(NM_DOMAIN.$_SERVER['REQUEST_URI']);
		}
	}
}

// URL-파라미터  -> 검색시 사용
$_nm_paras = "v=".substr(NM_VERSION,1,1);
// 파라미터 검사 - 숫자만
foreach($nm_para['num_list'] as $num_key => $num_val){
	if (isset($_REQUEST[$num_val]))  {
		${$num_val} = num_check(tag_get_filter($_REQUEST[$num_val]));
		if (${$num_val} !="") {
			${"_".$num_val} = etc_filter(${$num_val});
			$_nm_paras .= '&'.$num_val.'='.urlencode(${"_".$num_val});
		}
	} else {
		${$num_val} = '';
	}
}

// 파라미터 검사 - 숫자+영문만
foreach($nm_para['eng_list'] as $eng_key => $eng_val){
	if (isset($_REQUEST[$eng_val]))  {
		${$eng_val} = num_eng_check(tag_get_filter($_REQUEST[$eng_val]));
		if (${$eng_val} !="") {
			${"_".$eng_val} = etc_filter(${$eng_val});
			$_nm_paras .= '&'.$eng_val.'='.urlencode(${"_".$eng_val});
		}
	} else {
		${$eng_val} = '';
	}
}
// 파라미터 검사 - 숫자+영문+기타(한글+특수문자(etc_filter함수에서 제외됨))
foreach($nm_para['etc_list'] as $etc_key => $etc_val){
	if (isset($_REQUEST[$etc_val]))  {
		${$etc_val} = base_get_filter($_REQUEST[$etc_val]);
		if (${$etc_val} !="") {
			${"_".$etc_val} = ${$etc_val};
			if($etc_val == 'redirect'){ ${"_".$etc_val} = get_redirect(${$etc_val}); } /* 181001 */
			$_nm_paras .= '&'.$etc_val.'='.urlencode(${"_".$etc_val});
		}
	} else {
		${$etc_val} = '';
	}
}

/********************
    APP
********************/
// app 2.0 session 수정
/* NM_APP AppSetApk */
if(is_app(1) && is_mobile()) {
  $_SESSION['app'] = preg_match('/(AppSetApk|AppMarket)/i',HTTP_USER_AGENT,$matches);
  $is_app_ver = strtolower($matches[0]);
	$_SESSION['device'] = app_device();
  if(app_is_chk()){
	$_SESSION['appver'] = app_ver();
  }else{
	  if($is_app_ver == strtolower('AppSetApk')){
	    $_SESSION['appver'] = 1.5; // 완전판 최신 버전
	  }else{
	    $_SESSION['appver'] = 1.4; // 구글플레이어 최신 버전
	  }
  }
} // end if

/********************
    로그인
********************/
// 자동로그인 부분에서 첫로그인에 포인트 부여하던것을 로그인중일때로 변경하면서 코드도 대폭 수정하였습니다.
if ($_SESSION['ss_mb_id']) { // 로그인중이라면
    $nm_member = mb_get($_SESSION['ss_mb_id']);
	$_SESSION['ss_login_id'] = '';
	$_SESSION['ss_login_pw'] = '';

	$mb_attend_view = false;

	mb_state_stop_logout($nm_member); // 계정 정리 로그 아웃

	// 관리자 세션
	if($_SESSION['ss_adm_id']){
	
	}else{
		// 오늘 처음 로그인 이라면
		if (substr($nm_member['mb_login_today'], 0, 10) != NM_TIME_YMD) {

			// 회원 탈퇴일 경우
			if($nm_member['mb_state'] == 'n'){
				cs_alert('탈퇴된 아이디 입니다. 관리자에게 문의하세요!', NM_URL.'/proc/ctlogout.php');
				die;
			} // end if
			
			// 회원 중지일 경우
			if($nm_member['mb_state'] == 'y' && $nm_member['mb_state_sub'] == 's'){
				cs_alert('이용 중지된 아이디 입니다. 관리자에게 문의하세요!', NM_URL.'/proc/ctlogout.php');
				die;
			} // end if

			// 랜덤박스 회원 데이터 생성/업데이트 20180212
			randombox_member_set($nm_member);

			// 회원 휴면일 경우
			if($nm_member['mb_state'] == 'y' && $nm_member['mb_state_sub'] == 'h'){
				$link = NM_URL.'/sleepcancel.php';
				goto_url($link);
				die;
			} // end if
			
			if ($nm_member['mb_state'] == 'y' && $nm_member['mb_state_sub'] == 'y') {
				mb_login_today_update($nm_member); // 회원정보 업데이트(금일 로그 날짜)
				
				// 회원정보 업데이트 & 회원로그		
				mb_login_date_update($nm_member, get_js_cookie('mb_token'));
				mb_stats_log_member_update($nm_member);

				// mkt - ICE COUNTER LOGIN
				rdt_acecounter_ss_login('y');
				
				if($nm_member['mb_adult'] == 'y'){ $_adult = 'y'; } /* 배과장님 요청사항(조차장님도 승인함)171110 */

				// 성인이고 남자일 경우 (본인인증완료된 회원) 조차장님 요청사항 171206
				if($nm_member['mb_adult'] == 'y' && $nm_member['mb_sex'] == 'm'){ 	
					$link = NM_DOMAIN."/cmgenre.php?menu=6&adult=y"; 
					if($_redirect !=''){ // 리다이렉트 추가-18-12-03
						$link = goto_redirect($_redirect);
					}
					goto_url($link);
				} // end if
			} else {
				cs_alert('로그인에 실패하였습니다. 관리자에게 문의하세요!'.'<br>'.'ERROR CODE : common ERROR', NM_URL.'/proc/ctlogout.php');
				die;
			} // end else
		} // end if
	}
} else {
    // 자동로그인 ---------------------------------------
    // 회원아이디가 쿠키에 저장되어 있다면 (3.27)
    if (get_cookie('ck_mb_id')  != '' && get_cookie('ck_mb_auto') != '') {
		$tmp_mb_id = get_cookie('ck_mb_id');
		$tmp_mb_auto = get_cookie('ck_mb_auto');

		$tmp_nm_member = mb_get($tmp_mb_id);
		
		mb_state_stop_logout($tmp_nm_member); // 계정 정리 로그 아웃
		
		if($tmp_nm_member['mb_id'] == "") {
			set_cookie('ck_mb_id', '', 0);
			set_cookie('ck_mb_auto', '', 0);
			alert("자동 로그인에 실패하였습니다. 관리자에게 문의하세요.");
		} // end if

		$key = cs_mb_auto_key($tmp_nm_member);
		// 쿠키에 저장된 키와 같다면
		$tmp_key = get_cookie('ck_mb_auto');
		if ($tmp_key == $key && $tmp_key) {
			// 차단, 탈퇴가 아니라면...
			if ($tmp_nm_member['mb_state'] == 'y' && $tmp_nm_member['mb_state_sub'] == 'y') {
				// 세션에 회원아이디를 저장하여 로그인으로 간주
				set_session('ss_mb_id', $tmp_mb_id);

				// 회원정보 업데이트 & 회원로그
				mb_login_date_update($tmp_nm_member, get_js_cookie('mb_token'));
				mb_stats_log_member_update($tmp_nm_member);

				// mkt - ICE COUNTER LOGIN
				rdt_acecounter_ss_login('y');
			
				$_SESSION['ss_login_id'] = '';
				$_SESSION['ss_login_pw'] = '';

				// 페이지를 재실행
				echo "<script type='text/javascript'> window.location.reload(); </script>";
				exit;
			}
		}else{
			del_cookie('ck_mb_auto');
			cs_alert("마지막으로 로그인한곳과 로그인 환경(browser업데이트)이 달라서 자동로그인이 안됩니다.");
		}
		// 임시 변수 해제
		unset($tmp_mb_id);
		unset($tmp_mb_auto);
		unset($tmp_nm_member);

    }
    // 자동로그인 end ---------------------------------------
}

// 회원, 비회원 구분
$is_admin = $is_partner = $is_member = $is_guest = false;
if ($nm_member['mb_id'] != '') {
    $is_member = true;
    $nm_member['mb_idx'] = substr($nm_member['mb_id'],0,1);

	// 서비스 페이지
	if($nm_member['mb_nick'] != ''){ $nm_member['cs_user_menu_top_text'] = trim($nm_member['mb_nick'])."님"; }
	else{ $nm_member['cs_user_menu_top_text'] = trim($nm_member['mb_id'])."님"; }
    $nm_member['cs_login_url'] = NM_PROC_URL."/ctlogout.php";
    $nm_member['cs_login_text'] = "로그아웃";

	/* 파트너 확인 */
	if($nm_member['mb_class']  == $nm_config['cf_partner_class'] && $nm_member['mb_class'] >= $nm_config['cf_partner_level']){ $is_partner = true; }

	/* 관리자 확인 */
	if($nm_member['mb_class']  == $nm_config['cf_admin_class'] && $nm_member['mb_level'] >= $nm_config['cf_admin_level']){ $is_admin = true; }

	/* cms링크 */
	if(($is_partner == true || $is_admin == true) && !(is_app())){
		$nm_member['cs_user_menu_top_text'] = '<a href="'.NM_URL.'/adm/" target="_self" class="cms">'.$nm_member['cs_user_menu_top_text'].'</a>';
	};

} else {
    $is_guest = true;
    $nm_member['mb_id'] = '';
    $nm_member['mb_level'] = 1; // 비회원의 경우 회원레벨을 가장 낮게 설정

	// 서비스 페이지
    // $nm_member['cs_login_url'] = NM_URL."/ctlogin.php";
    $nm_member['cs_login_url'] = NM_URL."/ctlogin.php?".get_add_para_redirect().""; // 18-12-03 리다이렉트
    $nm_member['cs_login_text'] = "로그인";
	$nm_member['cs_user_menu_top_text'] = '<a href="'.$nm_member['cs_login_url'].'" class="login">로그인 해주세요.</a>';
}

// 성인 쿠키&설정 확인
$mb_adult_cookie_arr = mb_adult_cookie($_adult, $nm_member);
$mb_adult_cookie = $mb_adult_cookie_arr[0]; // 쿠키&파라미터
$mb_adult_permission = $mb_adult_cookie_arr[1]; // 쿠키&회원&파라미터

if (preg_match('/'.NM_APP_MARKET.'/i', HTTP_USER_AGENT)) {	
	$mb_adult_cookie = 'n';
	$mb_adult_permission = 'n';
} // end if

// 서브메뉴에서 서브메뉴의 파라마터 아무것도 없고 특정 메뉴 지정 하고 싶을때
$_current_lnb_sub_para_arr_define = '';

?>