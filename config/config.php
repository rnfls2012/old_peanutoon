<?php
/********************
    상수 선언
********************/

define('NM_VERSION', 'v2.3.1_181213.1');
define('NM_FIX_VER', 'v2.3'); // 고정 버전

// 이 상수가 정의되지 않으면 각각의 개별 페이지는 별도로 실행될 수 없음
define('_NMPAGE_', true);

if (PHP_VERSION >= '5.1.0') {
    //if (function_exists("date_default_timezone_set")) date_default_timezone_set("Asia/Seoul");
    date_default_timezone_set("Asia/Seoul");
}

/********************
    경로 상수
********************/

define('HTTP_USER_AGENT',   $_SERVER['HTTP_USER_AGENT']);
define('HTTP_REFERER',   		$_SERVER['HTTP_REFERER']);
define('HTTP_HOST',   			$_SERVER['HTTP_HOST']);

$apache_request_headers = array();
if(HTTP_HOST != ""){ $apache_request_headers = apache_request_headers(); }
/********************
apache_request_headers()는 아파치 관련 함수라서 crontab에서 shell 실행시 
PHP Fatal error:  Call to undefined function apache_request_headers()
에러 발생 이유는 shell 실행이라 apache 실행해서 얻을 정보가 없어 에러 안나게 하려면 
$_SERVER['HTTP_HOST'] 서버 정보 변수 이용해서 처리함
********************/

if($apache_request_headers['X-Forwarded-Proto'] == 'https'){
  define('NM_HTTP_SERVER', 'https://');
}else{
  define('NM_HTTP_SERVER', 'http://');	
}


/********************
    서버 설정
********************/
// 서버 리스트
/* 서버명, 서버URL, 서버SSH유무, MEMCACHED유무, 로드밸랜싱사유무 */
$SERVER_LIST_ARR = array(array('실서버', 			'www.peanutoon.com', 0, 1, 1)); /* 0 */
/* 1 */ array_push($SERVER_LIST_ARR, array('개발서버', 			'dev.peanutoon.com', 0, 0, 0));
/* 2 */ array_push($SERVER_LIST_ARR, array('실서버COPY',		'copy.peanutoon.com', 0, 1, 0));

/* 3 */ array_push($SERVER_LIST_ARR, array('김선규개발서버', 	'ingaha.peanutoon.com', 0, 0, 0));
/* 4 */ array_push($SERVER_LIST_ARR, array('이익희개발서버', 	'leeih.peanutoon.com', 0, 0, 0));
/* 5 */ array_push($SERVER_LIST_ARR, array('예지완개발서버', 	'rnfls2012.peanutoon.com', 0, 0, 0));
/* 6 */ array_push($SERVER_LIST_ARR, array('김수진개발서버', 	'merveilles.peanutoon.com', 0, 0, 0));

/* 7 */ array_push($SERVER_LIST_ARR, array('실서버TEST서버', 	'test.peanutoon.com', 1, 1, 1));

// 서버 번호 설정
define('NM_SERVER_NO', 5);			/* 사용 서버 번호 */
define('NM_DEV_SERVER_NO', 1);	/* 개발 서버 번호 */

/* ///////////////////// 사용 서버 설정 ///////////////////// */
$server_config = $SERVER_LIST_ARR[NM_SERVER_NO];
if($server_config[2] == 1){  
  define('NM_HTTP', 'https://');
}else{
	define('NM_HTTP', 'http://');	
}


define('NM_DOMAIN', NM_HTTP.$server_config[1]);
define('NM_DOMAIN_ONLY', substr(NM_DOMAIN,strpos(NM_DOMAIN,'://')+3,strlen(NM_DOMAIN)));
// MEMCACHED 설정
if($server_config[3] == 1){
	define('MEMCACHED',   true);
}else{
	define('MEMCACHED',   false);
}
// REMOTE_ADDR 설정
if($server_config[4] == 1){
	define('REMOTE_ADDR',   $_SERVER['HTTP_X_FORWARDED_FOR']);
}else{
	define('REMOTE_ADDR',   $_SERVER['REMOTE_ADDR']);
}

/* ///////////////////// 개발 서버 설정 ///////////////////// */
$server_dev_config = $SERVER_LIST_ARR[NM_DEV_SERVER_NO];
if($server_dev_config[2] == 1){
	define('NM_DEV_HTTP', 'https://');	
}else{
	define('NM_DEV_HTTP', 'http://');	
}
define('NM_DEV_URL', NM_DEV_HTTP.$server_dev_config[1]); // 개발 사이트

/* ///////////////////// 실제 서버 설정 ///////////////////// */
$server_real_config = $SERVER_LIST_ARR[0];
if($server_real_config[2] == 1){
	define('NM_REAL_HTTP', 'https://');	
}else{
	define('NM_REAL_HTTP', 'http://');	
}
define('NM_REAL_URL', NM_REAL_HTTP.$server_real_config[1]); // 실 사이트
define('NM_REAL_URL_IP', '14.63.219.143'); // 실 사이트 IP

/* ///////////////////// 실제 & 사용 서버 비교 ///////////////////// */
if(NM_DOMAIN == NM_REAL_URL){
	define('NM_ADM_CHANGE_URL', NM_DEV_URL);
	define('NM_ADM_CHANGE_TEXT', '개발전용서버이동');
}else{
	define('NM_ADM_CHANGE_URL', NM_REAL_URL);
	define('NM_ADM_CHANGE_TEXT', '실제운영서버이동');
}

/* 실서버 & 개발서버 설정시 end db_connect.php 덮어써주세요. */

/* 디렉토리명 */
define('NM_DOMAIN_DIR', 'peanutoon');

/* 점검시 보여주는 페이지 사용 여부 : 빈칸 사용 안함, 1준비, 2점검 */
define('NM_DOMAIN_ERROR', '');

/* CMS 파트너 접근 제어 */
define('NM_PARTNER_LIMIT', false);

/* ////////////////////////// 덮어쓰기시 체크 사항 끝 ////////////////////////// */

define('NM_HTTPS_DOMAIN', '');

define('NM_COOKIE_DOMAIN',  '');

define('NM_PC', '_pc');
define('NM_MO', '_mobile');
define('NM_ADM', 'adm');


// URL 은 브라우저상에서의 경로 (도메인으로 부터의)
if (NM_DOMAIN) {
    define('NM_URL', NM_DOMAIN);
} else {
    if (isset($nm_path['url']))
        define('NM_URL', $nm_path['url']);
    else
        define('NM_URL', '');
}

if (isset($nm_path['path'])) {
    define('NM_PATH', $nm_path['path']);
} else {
    define('NM_PATH', '');
}

// URL 는 인터넷주소 경로
define('NM_PC_URL', NM_URL.'/'.NM_PC);
define('NM_MO_URL', NM_URL.'/'.NM_MO);

// PATH 는 서버상에서의 절대경로
define('NM_PC_PATH', NM_PATH.'/'.NM_PC);
define('NM_MO_PATH', NM_PATH.'/'.NM_MO);

// admin 폴더 경로
define('NM_ADM_URL', NM_URL.'/'.NM_ADM);
define('NM_ADM_PATH', NM_PATH.'/'.NM_ADM);

// data 폴더 경로
define('NM_DATA_PATH', NM_PATH.'/data');
define('NM_DATA_URL', NM_URL.'/data');

// thumbnail 폴더 경로
define('NM_THUMB_PATH', NM_PATH.'/thumbnail');
define('NM_THUMB_URL', NM_URL.'/thumbnail');

// thumbnail-storage 폴더 경로
define('NM_THUMB_STORAGE_PATH', NM_THUMB_PATH.'/storage');
define('NM_THUMB_STORAGE_URL', NM_THUMB_URL.'/storage');

// sh 폴더 경로
define('NM_SH_PATH', NM_PATH.'/sh');
define('NM_SH_URL', NM_URL.'/sh');

// sns 폴더 경로
define('NM_SNS_PATH', NM_PATH.'/sns');
define('NM_SNS_URL', NM_URL.'/sns');

// main 폴더 경로-pc/mobile
define('NM_PC_MAIN_URL', NM_PC_URL.'/main');
define('NM_MO_MAIN_URL', NM_MO_URL.'/main');
define('NM_PC_MAIN_PATH', NM_PC_PATH.'/main');
define('NM_MO_MAIN_PATH', NM_MO_PATH.'/main');

// part 폴더 경로-pc/mobile
define('NM_PC_PART_URL', NM_PC_URL.'/part');
define('NM_MO_PART_URL', NM_MO_URL.'/part');
define('NM_PC_PART_PATH', NM_PC_PATH.'/part');
define('NM_MO_PART_PATH', NM_MO_PATH.'/part');

// oauth 폴더 경로
define('NM_OAUTH_PATH', NM_SNS_PATH.'/oauth');
define('NM_OAUTH_URL', NM_SNS_URL.'/oauth');
define('NM_OAUTH_CALLBACK_URL', NM_OAUTH_URL.'/callback.php');

// oauth 닉네임 Prefix
define('NM_OAUTH_NICK_PREFIX',  '');

// oauth 로그인 ID 구분자
define('NM_OAUTH_ID_DELIMITER', '_');

// define('NM_CDN', 'http://qi01-ie5524.ktics.co.kr/'); /* 추후 시디엔 사용시... */
define('NM_CDN', 'https://cdn.peanutoon.com/'); /* 추후 시디엔 사용시... */
define('NM_IMG', NM_CDN); /* 추후 시디엔 사용시... */


define('NM_PC_IMG', NM_CDN.NM_PC.'/'); /* 추후 시디엔 사용시... */
define('NM_MO_IMG', NM_CDN.NM_MO.'/'); /* 추후 시디엔 사용시... */

// session
define('NM_SESSION_PATH',   NM_PATH.'/'.'session');

/* ///////// 서비스 페이지에서 사용될 상수 경로 ///////// */
// cs에서 update 되는 폴더 경로
define('NM_PROC_URL', NM_URL.'/proc');
define('NM_PROC_PATH', NM_PATH.'/proc');

define('NM_PG_URL', NM_PROC_URL.'/pg');
define('NM_PG_PATH', NM_PROC_PATH.'/pg');

// pc사 폴더 경로
define('NM_KCP_URL', NM_PG_URL.'/kcp');
define('NM_KCP_PATH', NM_PG_PATH.'/kcp');
define('NM_KCP_PC_URL', NM_KCP_URL.'/'.NM_PC);
define('NM_KCP_PC_PATH', NM_KCP_PATH.'/'.NM_PC);
define('NM_KCP_MO_URL', NM_KCP_URL.'/'.NM_MO);
define('NM_KCP_MO_PATH', NM_KCP_PATH.'/'.NM_MO);

define('NM_TOSS_URL', NM_PG_URL.'/toss');
define('NM_TOSS_PATH', NM_PG_PATH.'/toss');

define('NM_PAYCO_URL', NM_PG_URL.'/payco');
define('NM_PAYCO_PATH', NM_PG_PATH.'/payco');

// certify사 폴더 경로
define('NM_CERTIFY_URL', NM_PROC_URL.'/certify');
define('NM_CERTIFY_PATH', NM_PROC_PATH.'/certify');
define('NM_KCP_CR_URL', NM_CERTIFY_URL.'/kcp');
define('NM_KCP_CR_PATH', NM_CERTIFY_PATH.'/kcp');

// marketer사 폴더 경로
define('NM_MKT_URL', NM_URL.'/mkt');
define('NM_MKT_PATH', NM_PATH.'/mkt');

// marketer사 폴더- data traking tools ex: Google 애널리틱스, Facebook 픽셀
define('NM_DTT_URL', NM_MKT_URL.'/dtt');
define('NM_DTT_PATH', NM_MKT_PATH.'/dtt');
// marketer사 폴더- 구글 GDN, 다음 DDN, 크로스타겟
define('NM_RDT_URL', NM_MKT_URL.'/rdt');
define('NM_RDT_PATH', NM_MKT_PATH.'/rdt');

// eventful 폴더 경로
define('NM_EVENTFUL_URL', NM_URL.'/eventful');
define('NM_EVENTFUL_PATH', NM_PATH.'/eventful');
define('NM_EVENTFUL_PC_URL', NM_EVENTFUL_URL.'/'.NM_PC);
define('NM_EVENTFUL_PC_PATH', NM_EVENTFUL_PATH.'/'.NM_PC);
define('NM_EVENTFUL_MO_URL', NM_EVENTFUL_URL.'/'.NM_MO);
define('NM_EVENTFUL_MO_PATH', NM_EVENTFUL_PATH.'/'.NM_MO);

// promoful 폴더 경로
define('NM_PROMOFUL_URL', NM_URL.'/promoful');
define('NM_PROMOFUL_PATH', NM_PATH.'/promoful');
define('NM_PROMOFUL_PC_URL', NM_PROMOFUL_URL.'/'.NM_PC);
define('NM_PROMOFUL_PC_PATH', NM_PROMOFUL_PATH.'/'.NM_PC);
define('NM_PROMOFUL_MO_URL', NM_PROMOFUL_URL.'/'.NM_MO);
define('NM_PROMOFUL_MO_PATH', NM_PROMOFUL_PATH.'/'.NM_MO);

// ticketsocket 폴더 경로
define('NM_TKSK_KEY', 'd49bcd55f6954e86ab91730a5ea87d04'); // ticketsocket-Ice Cream Social Key 
define('NM_TKSK_URL', NM_URL.'/tksk');
define('NM_TKSK_PATH', NM_PATH.'/tksk');

// vote 폴더 경로
define('NM_VOTE_BOOLEAN', true); /* 투표 시작-true / 끝-false */
define('NM_VOTE_YEAR', 2017); /* 투표연도 */
define('NM_VOTE_URL', NM_URL.'/vote');
define('NM_VOTE_PATH', NM_PATH.'/vote');
define('NM_VOTE_PC_URL', NM_VOTE_URL.'/'.NM_PC);
define('NM_VOTE_PC_PATH', NM_VOTE_PATH.'/'.NM_PC);
define('NM_VOTE_MO_URL', NM_VOTE_URL.'/'.NM_MO);
define('NM_VOTE_MO_PATH', NM_VOTE_PATH.'/'.NM_MO);

// remarketer사 폴더 경로
define('NM_REMKT_URL', NM_URL.'/remkt');
define('NM_REMKT_PATH', NM_PATH.'/remkt');


//==============================================================================
// 사용기기 설정
// pc 설정 시 모바일 기기에서도 PC화면 보여짐
// mobile 설정 시 PC에서도 모바일화면 보여짐
// both 설정 시 접속 기기에 따른 화면 보여짐
//------------------------------------------------------------------------------
define('NM_SET_DEVICE', 'both');

define('NM_USE_MOBILE', false); // 모바일 홈페이지를 사용하지 않을 경우 false 로 설정
define('NM_USE_CACHE',  true); // 최신글등에 cache 기능 사용 여부


/********************
    시간 상수
********************/
// 서버의 시간과 실제 사용하는 시간이 틀린 경우 수정하세요.
// 하루는 86400 초입니다. 1시간은 3600초
// 6시간이 빠른 경우 time() + (3600 * 6);
// 6시간이 느린 경우 time() - (3600 * 6);
define('NM_SERVER_TIME',    time());
define('NM_TIME_YMDHIS',    date('Y-m-d H:i:s', NM_SERVER_TIME));
define('NM_TIME_W',			date('w', NM_SERVER_TIME));

// 일
define('NM_TIME_M1',		date('Y-m-d', strtotime('-1 days', NM_SERVER_TIME)));
define('NM_TIME_M2',		date('Y-m-d', strtotime('-2 days', NM_SERVER_TIME)));
define('NM_TIME_M3',		date('Y-m-d', strtotime('-3 days', NM_SERVER_TIME)));
define('NM_TIME_M4',		date('Y-m-d', strtotime('-4 days', NM_SERVER_TIME)));
define('NM_TIME_M5',		date('Y-m-d', strtotime('-5 days', NM_SERVER_TIME)));
define('NM_TIME_M6',		date('Y-m-d', strtotime('-6 days', NM_SERVER_TIME)));
define('NM_TIME_M7',		date('Y-m-d', strtotime('-7 days', NM_SERVER_TIME)));

define('NM_TIME_M14',		date('Y-m-d', strtotime('-14 days', NM_SERVER_TIME)));
define('NM_TIME_M21',		date('Y-m-d', strtotime('-21 days', NM_SERVER_TIME)));

define('NM_TIME_P1',		date('Y-m-d', strtotime('+1 days', NM_SERVER_TIME)));
define('NM_TIME_P2',		date('Y-m-d', strtotime('+2 days', NM_SERVER_TIME)));
define('NM_TIME_P3',		date('Y-m-d', strtotime('+3 days', NM_SERVER_TIME)));
define('NM_TIME_P4',		date('Y-m-d', strtotime('+4 days', NM_SERVER_TIME)));
define('NM_TIME_P5',		date('Y-m-d', strtotime('+5 days', NM_SERVER_TIME)));
define('NM_TIME_P6',		date('Y-m-d', strtotime('+6 days', NM_SERVER_TIME)));
define('NM_TIME_P7',		date('Y-m-d', strtotime('+7 days', NM_SERVER_TIME)));

define('NM_TIME_P14',		date('Y-m-d', strtotime('+14 days', NM_SERVER_TIME)));
define('NM_TIME_P21',		date('Y-m-d', strtotime('+21 days', NM_SERVER_TIME)));

// 월
define('NM_TIME_MON_01',	date('Y-m-01', NM_SERVER_TIME));

define('NM_TIME_MON_M1',	date('Y-m', strtotime(NM_TIME_MON_01.'-1 month')));

define('NM_TIME_MON_M1_S',	date('Y-m-d', strtotime(NM_TIME_MON_01.'-1 month')));
define('NM_TIME_MON_M1_E',	date('Y-m-d', strtotime(NM_TIME_MON_01.'-1 days')));

define('NM_TIME_MON_M2_S',	date('Y-m-d', strtotime(NM_TIME_MON_01.'-2 month')));
define('NM_TIME_MON_M2_E',	date('Y-m-d', strtotime(NM_TIME_MON_M1_S.'-1 days')));

define('NM_TIME_MON_M3_S',	date('Y-m-d', strtotime(NM_TIME_MON_01.'-3 month')));
define('NM_TIME_MON_M3_E',	date('Y-m-d', strtotime(NM_TIME_MON_M2_S.'-1 days')));

define('NM_TIME_MON_M6_S',	date('Y-m-d', strtotime(NM_TIME_MON_01.'-6 month')));
define('NM_TIME_MON_M9_S',	date('Y-m-d', strtotime(NM_TIME_MON_01.'-9 month')));
define('NM_TIME_MON_M12_S',	date('Y-m-d', strtotime(NM_TIME_MON_01.'-12 month')));

// 월-date
define('NM_DATE_MON_M1',		date('Y-m-d', strtotime('-1 month', NM_SERVER_TIME)));
define('NM_DATE_MON_M2',		date('Y-m-d', strtotime('-2 month', NM_SERVER_TIME)));

// 연도
define('NM_TIME_YEAR_01',	date('Y-01', NM_SERVER_TIME));

define('NM_TIME_YEAR_M1_01',	date('Y-01', strtotime('-1 year', NM_SERVER_TIME)));
define('NM_TIME_YEAR_M1_12',	date('Y-12', strtotime('-1 year', NM_SERVER_TIME)));

define('NM_TIME_YEAR_M2_01',	date('Y-01', strtotime('-2 year', NM_SERVER_TIME)));
define('NM_TIME_YEAR_M2_12',	date('Y-12', strtotime('-2 year', NM_SERVER_TIME)));


define('NM_TIME_YMD',       substr(NM_TIME_YMDHIS, 0, 10));
define('NM_TIME_HIS',       substr(NM_TIME_YMDHIS, 11, 8));

define('NM_TIME_YM',	    substr(NM_TIME_YMDHIS, 0, 7));
define('NM_TIME_Y',		    substr(NM_TIME_YMDHIS, 0, 4));
define('NM_TIME_M',		    substr(NM_TIME_YMDHIS, 5, 2));
define('NM_TIME_D',		    substr(NM_TIME_YMDHIS, 8, 2));
define('NM_TIME_H',		    substr(NM_TIME_YMDHIS, 11, 2));
define('NM_TIME_I',		    substr(NM_TIME_YMDHIS, 14, 2));

define('NM_TIME_HI_start',	'00:00:00');
define('NM_TIME_HI_end',	'23:59:59');

// AWS에서 KT로 옮긴 시간
define('NM_AWS_KT_MOVE',	'2017-06-30 06:00:00');

// 퍼미션
define('NM_DIR_PERMISSION',  0755); // 디렉토리 생성시 퍼미션
define('NM_FILE_PERMISSION', 0644); // 파일 생성시 퍼미션

// 모바일 인지 결정 $_SERVER['HTTP_USER_AGENT']
define('NM_MOBILE_AGENT',   'phone|samsung|lgtel|mobile|[^A]skt|nokia|blackberry|android|sony');

// SMTP
// lib/mailer.lib.php 에서 사용
define('NM_SMTP', '127.0.0.1');
define('NM_SMTP_PORT', '25');

/********************
    에디터경로
********************/
define('NM_EDITOR_LIB', NM_PATH."/editor/smarteditor2/editor.lib.php");
define('NM_EDITOR_PATH', NM_PATH."/editor/smarteditor2");
define('NM_EDITOR_URL', NM_URL."/editor/smarteditor2");

/* 에디터 폴더명 + KT storage */
define('NM_EDITOR_FILE_DIR', NM_DATA_PATH.'/editor/'.NM_TIME_YM.'/');
define('NM_EDITOR_FILE_URL', NM_IMG.'data/editor/'.NM_TIME_YM.'/');
define('NM_EDITOR_FILE_KT', 'data/editor/'.NM_TIME_YM.'/');
/* 에디터 파일업로드 경로 /ediotr/smarteditor2/photo_uploader/popup/php/index.php */

/********************
    라이브러리 경로
********************/
define('NM_LIB_PATH', NM_PATH."/lib");
define('NM_LIB_URL', NM_URL."/lib");

/********************
    APP 경로&상수
********************/
define('NM_APP_PATH', NM_PATH."/app");
define('NM_APP_URL', NM_URL."/app");
define('NM_APP_MARKET', "AppMarket");
define('NM_APP_SETAPK', "AppSetApk");

/********************
    ERROR 상수
********************/
switch(NM_DOMAIN_ERROR){
	case 1:	define('NM_DOMAIN_ERROR_URL', NM_URL.'/error/ready.php');
			define('NM_DOMAIN_ERROR_URL_ADM', NM_URL.'/error/adm/ready.php');
	break;

	case 2:	define('NM_DOMAIN_ERROR_URL', NM_PATH.'/error/index.php');
			define('NM_DOMAIN_ERROR_URL_ADM', NM_URL.'/error/index.php');
	break;

	default: define('NM_DOMAIN_ERROR_URL', false);
			 define('NM_DOMAIN_ERROR_URL_ADM', false);
	break;
}

/********************
    SNS 상수
********************/
/* 이미희 주임님께서 관리하는 계정 */
/* FACEBOOK
https://developers.facebook.com/
peanutoon1@gmail.com
nexcube*2017
*/
define('NM_FACEBOOK_CLIENT_ID', '830243330364119');
define('NM_FACEBOOK_SECRET_KEY', '3da2a78e0532aa8e5a7ef4b655a5a006');

/* KAKAO
https://developers.kakao.com/
gutsjo@nexcube.co.kr
nexcube*2017
/////////////////////////////////////////////////////////////////////
네이티브 앱 키 
d363cc160a2914420143dca47c8324f3
REST API 키 
90199b553975596fc668a060731d1ee1
JavaScript 키 
1b7cfc5c84409b79038ab4e278e00e2c
Admin 키 
cea243c81df794c9f5de865deac4234a
*/
define('NM_KAKAO_OAUTH_REST_API_KEY', '90199b553975596fc668a060731d1ee1');
define('NM_KAKAO_OAUTH_NATIVE_APP_KEY', 'd363cc160a2914420143dca47c8324f3');

/* NAVER
https://developers.naver.com/main
peanutoon	
nexcube*2017

dev는 
L0sRxSglreX4PUOTlxDs
B23_6SJ4d2
*/
define('NM_NAVER_OAUTH_CLIENT_ID', 'LGSGnlMEa2fmWgYQ59mN');
define('NM_NAVER_OAUTH_SECRET_KEY', 'OvAhgDUjfZ');

/* TWITTER
https://apps.twitter.com/
peanutoon	
vlsjxns*3446
/////////////////////////////////////////////////////////////////////
Consumer Key (API Key)	gQVjipbOnoD9yDXr8bSlwygBC
Consumer Secret (API Secret)	PVDM69PbaSGAgb3tpbsyLfvt96bFAmgyVJXtPyU46nYgG5E6ow
Access Level	Read-only (modify app permissions)
Owner	peanutoon
Owner ID	3270086839
*/
define('NM_TWITTER_CLIENT_ID', 'gQVjipbOnoD9yDXr8bSlwygBC');
define('NM_TWITTER_SECRET_KEY', 'PVDM69PbaSGAgb3tpbsyLfvt96bFAmgyVJXtPyU46nYgG5E6ow');

/*
https://console.developers.google.com
https://console.developers.google.com/apis/credentials?project=peanutoon-158506
// nexcube123@gmail.com
// nexcube6619
// -> 애널리틱스 https://analytics.google.com/analytics/web/
peanutoon.dev@gmail.com
vlsjxns*3446
*/
define('NM_GOOGLE_CLIENT_ID', '580844318272-kr5c22ruqaimil7m044jqmavlnib0829.apps.googleusercontent.com');
define('NM_GOOGLE_SECRET_KEY', 'pauMScSA3ohfph6kKibJHPV8');

/********************
카카오 알림톡(오렌지메세지)
 ********************/
/*
 * http://orangemsg.com/
 * ala235@nexcube.co.kr
 * asdf042084
 */
define('NM_KAKAO_ORANGE', '4+0RXZYD9Aft9cwY9ch2+BFJHHsU64KFOxidkpY0PR4=');

/********************
    기타 상수
********************/

// 암호화 함수 지정
// 사이트 운영 중 설정을 변경하면 로그인이 안되는 등의 문제가 발생합니다.
define('NM_STRING_ENCRYPT_FUNCTION', 'sql_password');

// SQL 에러를 표시할 것인지 지정
// 에러를 표시하려면 TRUE 로 변경
define('NM_DISPLAY_SQL_ERROR', FALSE);

// escape string 처리 함수 지정
// addslashes 로 변경 가능
define('NM_ESCAPE_FUNCTION', 'sql_escape_string');

// 썸네일 jpg Quality 설정
define('NM_THUMB_JPG_QUALITY', 90);

// 썸네일 png Compress 설정
define('NM_THUMB_PNG_COMPRESS', 5);

// ip 숨김방법 설정
/* 123.456.789.012 ip의 숨김 방법을 변경하는 방법은
\\1 은 123, \\2는 456, \\3은 789, \\4는 012에 각각 대응되므로
표시되는 부분은 \\1 과 같이 사용하시면 되고 숨길 부분은 ♡등의
다른 문자를 적어주시면 됩니다.
*/
define('NM_IP_DISPLAY', '\\1.♡.\\3.\\4');

if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') {   //https 통신일때 daum 주소 js
    define('NM_POSTCODE_JS', '<script src="https://spi.maps.daum.net/imap/map_js_init/postcode.v2.js"></script>');
} else {  //http 통신일때 daum 주소 js
    define('NM_POSTCODE_JS', '<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>');
}

/********************
    서브 페이지 출력 갯수
********************/
define('NM_SUB_LIST_LIMIT', 60);

/********************
    포인트 비율
********************/
define('NM_POINT', 10);

/********************
    성인
********************/
define('NM_ADULT', 19);

/********************
    APP
********************/
define('NM_APP_SALE', 10); // 할인 %
define('NM_APP_SALE_VER', 2.0); // 할인 해줄 최소 버전
define('NM_APP_IS_VER', 2.0); // 자체 개발 버전
define('NM_APP_ANDROID_IS_VER', 5.0); // 자체 개발 안드로이드 환경 버전

/********************
    포인트파크 PW
********************/
define('NM_POINTPARK_KEY', 'qPa7vb43vhiY7u8g');
define('NM_POINTPARK_ENC_KEY', 'vhdlsxmvkzm0314sprtmzbqmvlsjxns7');

/********************
    SNS PW
********************/
define('NM_JOIN_SNS_KEY', 'e1k7f0k7a0w1nl');
define('NM_JOIN_SNS_ENC_KEY', 'v2l0s1j7x0n7s0e1k0f6k0a0wnl');

/********************
    Google Tag Manager 사용 유무
********************/
define('NM_GTM', true);
define('NM_GTM_GODE', 'GTM-P27JXPJ');
define('NM_GTM_AMUTUS', true);

/********************
    넥스큐브 IP대역
********************/
define('NM_NEXCUBE_IP', '119.42.175');
define('NM_NEXCUBE_IP_start', '129');
define('NM_NEXCUBE_IP_end', '254');

$remote_addr_arr = explode(".", REMOTE_ADDR);
$remote_addr_ips = $remote_addr_arr[0].'.'.$remote_addr_arr[1].'.'.$remote_addr_arr[2];
if($remote_addr_ips == NM_NEXCUBE_IP){
	if($remote_addr_arr[3] >= NM_NEXCUBE_IP_start && $remote_addr_arr[3] <= NM_NEXCUBE_IP_end){
		define('NM_NEXCUBE_ACCESS', true);
	}else{
		define('NM_NEXCUBE_ACCESS', false);
	}
}else{
	define('NM_NEXCUBE_ACCESS', false);
}

/********************
    파라미터
********************/
// multi-dimensional array에 사용자지정 함수적용
function array_map_deep($fn, $array)
{
    if(is_array($array)) {
        foreach($array as $key => $value) {
            if(is_array($value)) {
                $array[$key] = array_map_deep($fn, $value);
            } else {
                $array[$key] = call_user_func($fn, $value);
            }
        }
    } else {
        $array = call_user_func($fn, $array);
    }

    return $array;
}


// SQL Injection 대응 문자열 필터링
function sql_escape_string($str)
{
    if(defined('NM_ESCAPE_PATTERN') && defined('NM_ESCAPE_REPLACE')) {
        $pattern = NM_ESCAPE_PATTERN;
        $replace = NM_ESCAPE_REPLACE;

        if($pattern)
            $str = preg_replace($pattern, $replace, $str);
    }

    $str = call_user_func('addslashes', $str);

    return $str;
}


//==============================================================================
// SQL Injection 등으로 부터 보호를 위해 sql_escape_string() 적용
//------------------------------------------------------------------------------
// magic_quotes_gpc 에 의한 backslashes 제거
if (get_magic_quotes_gpc()) {
    $_POST    = array_map_deep('stripslashes',  $_POST);
    $_GET     = array_map_deep('stripslashes',  $_GET);
    $_COOKIE  = array_map_deep('stripslashes',  $_COOKIE);
    $_REQUEST = array_map_deep('stripslashes',  $_REQUEST);
}

// sql_escape_string 적용
$_POST    = array_map_deep(NM_ESCAPE_FUNCTION,  $_POST);
$_GET     = array_map_deep(NM_ESCAPE_FUNCTION,  $_GET);
$_COOKIE  = array_map_deep(NM_ESCAPE_FUNCTION,  $_COOKIE);
$_REQUEST = array_map_deep(NM_ESCAPE_FUNCTION,  $_REQUEST);
//==============================================================================


// PHP 4.1.0 부터 지원됨
// php.ini 의 register_globals=off 일 경우
@extract($_GET);
@extract($_POST);
@extract($_SERVER);


// 완두콩님이 알려주신 보안관련 오류 수정
// $member 에 값을 직접 넘길 수 있음
$nm_config = array();
$nm_member = array();
$nm_para = array();


/********************
    값 고정
********************/

// 파라미터 -> 검색시 사용
$nm_para['num_list'] = array(); // 숫자만
$nm_para['eng_list'] = array(); // 숫자+영문만
$nm_para['etc_list'] = array(); // 숫자+영문+기타

	/* /////////////////////// CMS /////////////////////// */
/* 숫자만 */
array_push($nm_para['num_list'], 's_type', 's_limit', 'page');				/* 공통 */

array_push($nm_para['num_list'], 'cm_no', 'cm_big', 'cm_small', 'cm_menu');	/* 코믹스 */
array_push($nm_para['num_list'], 'big', 'small', 'menu');					/* 코믹스 */
array_push($nm_para['num_list'], 'comics', 'episode', 'chapter');			/* 코믹스 */
array_push($nm_para['num_list'], 'provider', 'publisher', 'professional');	/* 코믹스, 매출  */
array_push($nm_para['num_list'], 'cm_public_period');						/* 코믹스 */

array_push($nm_para['num_list'], 'member', 'mb_no', 'mb_level');			/* 회원 */
array_push($nm_para['num_list'], 'mb_comics_page');									/* 회원 */
array_push($nm_para['num_list'], 'mb_won', 'mb_mkt', 'mb_tksk');	/* 회원 */

array_push($nm_para['num_list'], 'bh_category', 'ba_mode');					/* 게시판 */
array_push($nm_para['num_list'], 'er_type', 'ecm_no', 'evc_no');			/* 이벤트 */
array_push($nm_para['num_list'], 'ercm_no');								/* 이벤트 */

array_push($nm_para['num_list'], 'embp_no', 'embc_no', 'embc_position');	/* 이프로모션 */
array_push($nm_para['num_list'], 'slp_no');									/* 이프로모션 */

array_push($nm_para['num_list'], 'mpu_no', 'mpu_member', 'mpu_point_class');/* 회원결제내역 */
array_push($nm_para['num_list'], 'marketer', 'mktl_no');					/* 마케팅 */
array_push($nm_para['num_list'], 'tksk_campaign');							/* 티켓소켓 */
array_push($nm_para['num_list'], 'campaign');										/* 캠페인번호 */


/* 숫자+영문만 */
array_push($nm_para['eng_list'], 'mode', 'order', 'page_mode');				/* 공통-모드, 정렬, 페이지모드(Array|SQL) */
array_push($nm_para['eng_list'], 'kw_id', 'cm_end', 'cm_service');			/* 코믹스 */
array_push($nm_para['eng_list'], 'cm_adult');								/* 코믹스 */

array_push($nm_para['eng_list'], 'mb_adult', 'mb_class', 'mb_sex', 'mb_sns_type');			/* 회원 */
array_push($nm_para['eng_list'], 'er_calc_way', 'er_state');				/* 이벤트 */
array_push($nm_para['eng_list'], 'ep_state', 'ep_adult');					/* 이벤트 */
array_push($nm_para['eng_list'], 'erc_use_state');							/* 이벤트 */

array_push($nm_para['eng_list'], 'eme_event_type', 'emb_state');			/* 이프로모션 */
array_push($nm_para['eng_list'], 'emp_state', 'emb_state_mod');				/* 이프로모션 */
array_push($nm_para['eng_list'], 'authty');									/* 매출-정산-올더게이트 */
array_push($nm_para['eng_list'], 'sl_mode');								/* 매출 */
array_push($nm_para['eng_list'], 'crp_state');								/* 설정 */


/* 숫자+영문만+기타 */
array_push($nm_para['etc_list'], 's_text', 'order_field');					/* 공통-검색 */
array_push($nm_para['etc_list'], 'date_type', 's_date', 'e_date');			/* 공통-날짜 */
array_push($nm_para['etc_list'], 's_month', 'e_month');						/* 공통-월 */
array_push($nm_para['etc_list'], 's_hour', 'e_hour');						/* 공통-시간 */
array_push($nm_para['etc_list'], 'year_month_day');								/* 시간대별 정산(180116) */

array_push($nm_para['etc_list'], 'sr_date', 'er_date');						/* 순위-날짜 */
array_push($nm_para['etc_list'], 'sr_hour', 'er_hour');						/* 순위-시간 */

array_push($nm_para['etc_list'], 'mb_id', 'mb_post');						/* 회원 */
array_push($nm_para['etc_list'], 'is_error');										/* 에러/정상 */

array_push($nm_para['etc_list'], 'mpu_class', 'mpu_member_idx');			/* 회원결제내역 */

array_push($nm_para['etc_list'], 'cm_no_list');								/* CMS지원-유통파트:comics:컨텐츠번호들[체크해서 넣은 값 123,546,579 이런식으로 넘겨버리자] */



	/* /////////////////////// Service  /////////////////////// */
/* 숫자만 */
array_push($nm_para['num_list'], 'chapter', 'mbc');							/* 화리스트 */
array_push($nm_para['num_list'], 'ba', 'bn', 'bp');							/* 고객센터 (1:1문의, 공지사항, 연재문의) */
array_push($nm_para['num_list'], 'ba_category');							/* 1:1 문의사항 */

array_push($nm_para['num_list'], 'event');									/* 이벤트 번호 */
array_push($nm_para['num_list'], 'mb_coupondc');							/* 회원이 선택한 할인권 번호 */
array_push($nm_para['num_list'], 'mb_crp_no');								/* 회원이 선택한 결제상품 번호 */
array_push($nm_para['num_list'], 'mb_payway_no');							/* 회원이 선택한 결제 방법 번호 */

array_push($nm_para['num_list'], 'epay', 'fpay');							/* 코믹스 뷰어(내 서재-화 리스트) */
// array_push($nm_para['num_list'], 'select_value');						/* 1:1 문의사항 */


/* 숫자+영문만 */
array_push($nm_para['eng_list'], 'adult', 'type', 'cate');					/* 상단 - 코믹스도 같이 쓰임 */
array_push($nm_para['eng_list'], 'bp_member_idx', 'bp_no');					/* 연재 문의 */
array_push($nm_para['eng_list'], 'cm_ck_id', 'cm_ck_id_sub', 'cm_ck_id_thi');		/* 코믹스 키워드 */
array_push($nm_para['eng_list'], 'ce_dt');											/* 코믹스 에피소드 */
array_push($nm_para['eng_list'], 'mysecret');								/* 코믹스 뷰어(내 서재-화 리스트) */
array_push($nm_para['eng_list'], 'event_mode');								/* 코믹스 뷰어(이벤트조회통계)-180920 */

/* 숫자+영문만+기타 */
array_push($nm_para['etc_list'], 'qnaw_title', 'qnaw_request');				/* 1:1 문의사항 */
array_push($nm_para['etc_list'], 'bp_member', 'bp_title');	                /* 연재 문의 */
array_push($nm_para['etc_list'], 'bp_text', 'bp_file', 'bp_date');
array_push($nm_para['etc_list'], 'storage_file');

array_push($nm_para['etc_list'], 'tksk_page');								/* 티켓소켓 */
array_push($nm_para['etc_list'], 'redirect');								/* 다시보내기 url - 181001  */

?>