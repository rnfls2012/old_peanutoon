<?
include_once '_common.php'; // 공통

$er_no = randombox_state_get("f");
$er_r = randombox_get_reservation("f","r"); // 예약중

/* 총 쿠폰 수량 */
$event_total_sql = "SELECT * FROM event_randombox WHERE er_no='".$er_no."'";
$event_total_row = sql_fetch($event_total_sql);
$total_coupon = $event_total_row['er_first_count']; // 선착순 쿠폰 총 수량
$total_sql = "SELECT COUNT(*) AS total_cnt FROM event_randombox_coupon WHERE erc_er_no='".$er_no."'";
$pop_count = sql_count($total_sql, "total_cnt"); // 현재 뽑은 수량
$total_coupon -= $pop_count; // 남은 수량

if($total_coupon <= 0) {
	$total_coupon = 0;		
} // end if

/* 회원이 뽑았는지 확인 */
$member_pop = 0;
if($nm_member['mb_id'] != "") {
	$pop_sql = "SELECT COUNT(*) AS pop_cnt FROM event_randombox_coupon WHERE erc_er_no='".$er_no."' AND erc_member='".$nm_member['mb_no']."'";
	$member_pop = sql_count($pop_sql, "pop_cnt");
} // end if

/* 버튼 비활성화 */
$nochk = $coupon_off = "";
$coupon_off_text = "쿠폰 발급";

// 현재 날짜 비교 - 쿠폰 텍스트 coupon_off_text
$er_y_date_start = $event_total_row['er_date_start']." ".$event_total_row['er_date_start_hour'].":00:00";
$er_y_date_end = $event_total_row['er_date_end']." ".$event_total_row['er_date_end_hour'].":59:59";

if($member_pop > 0 || $total_coupon <= 0 || $nm_member['mb_id'] == "") {
	$nochk = "nochk";
	$coupon_off = "_off";

	// 날짜 비교 - 쿠폰 텍스트	
	if(intval($event_total_row['er_no']) == 0 ) {
		$coupon_off_text = "쿠폰 마감";
		
		// 예약 쿠폰이 있다면
		if($er_r['er_date_start'] >= NM_TIME_YMD) {
			$coupon_off_text = "쿠폰 준비중";
		} // end if
	} else {
		if($total_coupon <= 0) {
			$coupon_off_text = "쿠폰 마감";
		} // end if
	} // end else

} // end if

$family_img = NM_IMG.$nm_config['nm_mode'].'/event/family'; // 이미지 경로

/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/eventfamily.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>