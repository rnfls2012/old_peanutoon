<? include_once '_common.php'; // 공통

error_page();

include_once (NM_PATH.'/_head.php'); // 공통

/* COOKIE */

$sub_list_url = get_js_cookie('ck_sub_list_url');
$sub_list_limit = intval(get_int(get_js_cookie('ck_load_list_limit')));
if($sub_list_limit < NM_SUB_LIST_LIMIT || $sub_list_limit == ''){ $sub_list_limit = NM_SUB_LIST_LIMIT; }

if($sub_list_url != $_SERVER['REQUEST_URI']){ $sub_list_limit = NM_SUB_LIST_LIMIT; }

/* DB */

/* 성인6, BL/GL5, TL2, 순정4, 드라마1, 코믹3, 소설7 */
$small_val = get_int($lnb_sub_arr[0]['cn_link']); // 위 _head.php에서 가져옴

if($_small != ''){
	$small_val = $_small;
} else {
	$_small = $small_val;
} // end else

$sql_small == '';
if($_small != ''){
	$sql_small = "AND c.cm_small = ".$small_val." ";
	// $sql_small = "AND (c.cm_small = ".$small_val.")";
}
$sql_bl_keyword = "";
if($_mode) {
	$sql_bl_keyword = "AND (cm_ck_id = '40' AND cm_ck_id_sub != '40b0')";
} // end if

$cmvolume_arr = array();
$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');

// 총갯수
$sql_cmend_total = " SELECT count(*) as total FROM comics c WHERE 1 $sql_cm_adult $sql_small and c.cm_service = 'y' AND (c.cm_big = 1 OR c.cm_big = 3); ";
$row_total = sql_count($sql_cmend_total, 'total');

// 익스플로러일때 전체 출력-임시
if(substr(get_brow(HTTP_USER_AGENT), 0, 4) == "MSIE"){ $sub_list_limit = $row_total; }

$sql_cmvolume = " SELECT c.*, c_prof.cp_name as prof_name  FROM comics c 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE cm_service = 'y' AND (c.cm_big = 1 OR c.cm_big = 3) $sql_cm_adult $sql_small $sql_bl_keyword ORDER BY cm_episode_date DESC, cm_reg_date DESC LIMIT 0, ".$sub_list_limit."; ";

// 리스트 가져오기
$cmvolume_arr =cs_comics_content_list($sql_cmvolume, substr($nm_config['nm_path'], -1));

$sub_list_bool = "false";
if(count($cmvolume_arr) < NM_SUB_LIST_LIMIT){ $sub_list_bool = "true"; }

/* view */

include_once($nm_config['nm_path']."/cmvolume.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>