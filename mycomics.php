<?
include_once '_common.php'; // 공통

error_page();

/* DB */
cs_login_check(); // 로그인 체크

/* view */
// include_once (NM_PATH.'/_head.php'); // 공통 아래 include해서 필요없음

switch($_mbc) {
	case 1 : 
		include_once(NM_PATH."/myrecent.php");
		break; // 최근 본 작품

	case 2 :
		if($_comics == "") {
			include_once(NM_PATH."/mycomicslist.php");
		} else {
			include_once(NM_PATH."/myepisode.php");
		} // end else
		break; // 구매목록 & 소장 정보

	case 3 :
		include_once(NM_PATH."/mybookmark.php");
		break; // 즐겨찾기

	default :
		include_once(NM_PATH."/myrecent.php");
		break; // 최근 본 작품
} // end switch

// include_once (NM_PATH.'/_tail.php'); // 공통 아래 include해서 필요없음
?>