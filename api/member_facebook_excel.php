<?

/* *******************************************
마오랑-정의장대표 요청 18-04-26
******************************************* */
$nm_path['path'] = '/var/www/html/peanutoon';
$nm_path['url'] = '/var/www/html/peanutoon';
include_once('../config/config.php');   // 설정
include_once(NM_PATH.'/config/dbconnect.php');   // DB 연결
include_once(NM_LIB_PATH.'/common.lib.php');	 // 공통 라이브러리
include_once(NM_LIB_PATH.'/mysql.lib.php');		 // SQL  라이브러리
include_once(NM_LIB_PATH.'/mb.lib.php');		 // 회원 라이브러리


$api = num_eng_check(tag_get_filter($_REQUEST['api']));

if($api != "ak1dh8fkd0wjd4dml2rhks6"){ echo "api가 틀립니다. 확인 또는 관리자에게 문의하세요"; die; }

// 다운로드 정보 저장 및 제한
$api_download_limit = 10;

$php_self = $_SERVER['PHP_SELF'];
$php_self_arr = explode("/", $php_self);
$api_file = $php_self_arr[count($php_self_arr)-1]; // 현재 접속한 파일명

$sql_filed_api = " count(*) as api_total ";
$sql_api = " 
 select {$sql_filed_api} from z_api_excel_download where 
 z_aed_file = '".$api_file."' and  z_aed_day='".NM_TIME_YMD."'
 ";

$api_total = sql_count($sql_api, 'api_total');

if($api_download_limit <= intval($api_total)){
	echo $api_download_limit."번 이상 다운로드 불가 합니다.";
	die;
}

$sql_insert = " 
 INSERT INTO z_api_excel_download (z_aed_ip, z_aed_user_agent, z_aed_file, z_aed_day, z_aed_date) 
 VALUES ('".REMOTE_ADDR."', '".HTTP_USER_AGENT."', '".$api_file."', '".NM_TIME_YMD."', '".NM_TIME_YMDHIS."');
";
if(sql_query($sql_insert)){

}else{
	echo "기록 에러";
	die;
}

// 고정
$sql_query		= " select mb_id, mb_facebook_id from member where mb_state='y' AND mb_sns_type='fcb' ";
$sql_order		= " order by mb_no ";
$sql_querys		= " {$sql_query} {$sql_order} ";
$result			= sql_query($sql_querys);

$fetch_row = array();
array_push($fetch_row, array('회원ID', 'mb_id', 0));
array_push($fetch_row, array('페이스북ID', 'mb_facebook_id', 1));

// 고정 end
	
$today = date("Ymd");
// $today = NM_TIME_YMD;

// 엑셀 Library Import
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(NM_LIB_PATH.'/php_writeexcel/class.writeexcel_worksheet.inc.php');


/************ 엑셀작업 시작 ************/
// 서버에 임시파일 생성
$fname = tempnam("./library", "tmp-member.xls");

// workbook객체 생성
$workbook = new writeexcel_workbook($fname);

// worksheet 활성화
$worksheet = $workbook->addworksheet();

// 해당 시트 당 컬럼 크기 변경
$worksheet->set_column(0, 1, 15);
$worksheet->set_column(1, 1, 25);
$worksheet->set_column(2, 5, 20);

// workbook price format 추가(00,000,000)
$f_price = $workbook -> addformat();
$f_price -> set_align('right');
$f_price -> set_num_format('#,##0');
$f_price -> set_border('1');

// 셀(cell) 속성 지정
$right  = $workbook -> addformat(array(
								'align' => 'right',
								'border' => 1
								));
								
$center  = $workbook -> addformat(array(
								'align' => 'center',
								'border' => 1
								));

$heading = $workbook -> addformat(array(
								'align' => 'center', 
								'border' => 1,
								'bold' => 1,
								'fg_color' => 27
								));

$date_format = $workbook -> addformat(array(								
								'bold' => 1
								));

$cell_format = $workbook -> addformat(array(
								'size' => 15,
								'bold' => 1
								));
								
/**************  worksheet  *****************/

//문서 출력일자 출력
$worksheet->write(0, 0 , iconv_cp949($cms_head_title." 내역"), $cell_format);
$worksheet->write(1, 0 , iconv_cp949("출력일자"), $date_format);
$worksheet->write(1, 1 , iconv_cp949(date("Y-m-d")));

$row_cnt = 3; //합계 데이터 출력 행 cnt.

/***************  본문 출력   **************/

//worksheet 내에 검색 필터링 항목
$filter_type = $filter_cont = array();
array_push($filter_type, "플랫폼");
array_push($filter_type, "마케팅 업체");

//필터링에 들어갈 내용
array_push($filter_cont, $nm_config['cf_title']);
array_push($filter_cont, "facebook");

$filter_cnt = count($filter_type);

//필터링 배열 이용
for($i=0; $i < $filter_cnt; $i++){
	$worksheet->write($row_cnt, 0, iconv_cp949($filter_type[$i]), $heading);
	$worksheet->write($row_cnt, 1, iconv_cp949($filter_cont[$i]), $right);
	$row_cnt++;
}

$row_cnt++;

// EXCEL용 배열
$data_head = array(); // 분류값(컬럼) 지정 배열
foreach($fetch_row as $key => $val) {
	array_push($data_head, $val[0]);
}

// DB 컬럼용 배열
$data_key = array();
foreach($fetch_row as $key => $val) {
	array_push($data_key, $val[1]);
}

/* 컬럼 이름 지정 부분 */
$col = 0;
$data_head = array_map('iconv_cp949', $data_head);
foreach($data_head as $cell) {
	$worksheet->write($row_cnt, $col++, $cell, $heading);
}

$row_cnt++; //필터 지정 후 다음 행 부터 데이터 출력.

/* 데이터 전달 부분 */
for ($i=$row_cnt; $row=mysql_fetch_array($result); $i++) {
	foreach ($data_key as $cell_key => $cell_val) {

		//날짜 및 마케팅이름 표시 (가독성 위해 switch 문)
		switch($cell_val) {
				
			case "mb_id" :
				$mb_id = $row['mb_id']; // ID에 번호만 있을 경우 				
				$worksheet->write($i, $cell_key , iconv_cp949($mb_id));				
				break;

			case "mb_facebook_id" :
				$mb_facebook_id = '\''.$row['mb_facebook_id'];
				$worksheet->write($i, $cell_key , iconv_cp949($mb_facebook_id));
				break;
		}
	}
	$row_cnt++;
}

$row_cnt++;

$worksheet->write($row_cnt, 1, iconv_cp949('검색된 회원 수 : '.mysql_num_rows($result)));

$workbook->close();

header("Content-Disposition: attachment;filename="."member_mkt_".$today."_".$api_total.".xls");
header("Content-Type: application/x-msexcel; name=\"member_mkt_".$today."_".$api_total.".xls");
header("Content-Disposition: inline; filename=\"member_mkt_".$today."_".$api_total.".xls");

$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);
die;

?>