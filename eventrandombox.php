<?
include_once '_common.php'; // 공통

/* DB */
if(randombox_state_get() == "") {
	cs_alert("진행중인 이벤트가 없습니다!", NM_URL);
	die;
} else {
	$randombox_mb = randombox_member_get($nm_member, randombox_state_get()); // 이벤트 번호 제어 randombox_state_get() 사용 180213
	$mobile_logout_class = "";

	if($nm_member['mb_id'] != "" && $randombox_mb['erm_member'] == "") {
		randombox_member_set($nm_member);
		$randombox_mb = randombox_member_get($nm_member, randombox_state_get()); // 이벤트 번호 제어 randombox_state_get() 사용 180213
	} // end if

	if($randombox_mb['erm_member'] == "") {
		$randombox_mb['erm_pop_count'] = 0; // 남은 뽑기 횟수
		$randombox_mb['erm_pop_chance'] = 0; // 뽑을 수 있는 횟수
		$randombox_mb['erm_pop_gauge'] = 0; // 게이지 css 용
		$randombox_mb['erm_cash_point_total'] = 0; // 결제 땅콩, 뽑기에 쓴 땅콩 계산한 값
		$mobile_logout_class = "logout";
	} else { 
		$randombox_mb['erm_cash_point_total'] = ($randombox_mb['erm_cash_point_push'] + ($randombox_mb['erm_point_push'] / 10)) - $randombox_mb['erm_cash_point_pop']; // 결제 땅콩, 뽑기에 쓴 땅콩 계산한 값
		$randombox_mb['erm_pop_chance'] = floor($randombox_mb['erm_cash_point_total'] / 10); // 뽑을 수 있는 횟수
		if($randombox_mb['erm_cash_point_total'] >= 10) {
			$randombox_mb['erm_pop_gauge'] = 767; // PC 일때
			if($nm_config['nm_mode'] == "_mobile") {
				$randombox_mb['erm_pop_gauge'] = 100; // MOBILE 일때
			} // end if
		} else {
			$randombox_mb['erm_pop_gauge'] = $randombox_mb['erm_cash_point_total'] * 75; // PC 일때
			if($nm_config['nm_mode'] == "_mobile") {
				$randombox_mb['erm_pop_gauge'] = $randombox_mb['erm_cash_point_total'] * 10; // MOBILE 일때
			} // end if
		} // end else
	} // end else

	$randombox_img = NM_IMG.$nm_config['nm_mode'].'/event/randombox'; // 이미지 경로
} // end else

/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */
/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/eventrandombox.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>