<?
include_once '_common.php'; // 공통

error_page();

/* DB */
/* 코믹스 정보 가져오기 */
$member_buy_arr = array();
// $member_buy_comics_where = "AND mbc.mbc_member='".$nm_member['mb_no']."' AND mbc.mbc_member_idx='".$nm_member['mb_idx']."' and (mbc_own_type='1' or mbc_own_type='2') and mbc_view='n' order by cp_no desc";
$member_buy_comics_where = "AND mbc.mbc_member='".$nm_member['mb_no']."' AND (mbc_own_type='1' or mbc_own_type='2') and mbc_view='n' order by cp_no desc";
$sql_member_buy = "select * from member_buy_comics mbc 
					left JOIN comics c ON mbc.mbc_comics = c.cm_no
					left JOIN comics_professional cp ON cp.cp_no = c.cm_professional
					where 1 $member_buy_comics_where";

$member_buy_result = sql_query($sql_member_buy);
while($row = sql_fetch_array($member_buy_result)) {
	array_push($member_buy_arr, $row);
} // end while

/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/mycomicslist.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>