<? include_once '_common.php'; // 공통

error_page();

// all 인지 확인 -> 없다면 해당 요일로 지정
// 서브메뉴에서 서브메뉴의 파라마터 아무것도 없고 특정 메뉴 지정 하고 싶을때
$menuall = num_eng_check($_GET['menu']);
if($menuall == ''){ 
	$_menu = week_type(7); 
	$_current_lnb_sub_para_arr_define = "menu=".$_menu;
}

// _head.php 이전에 사용 해야 함

include_once (NM_PATH.'/_head.php'); // 공통

/* COOKIE */

$sub_list_url = get_js_cookie('ck_sub_list_url');
$sub_list_limit = intval(get_int(get_js_cookie('ck_load_list_limit')));
if($sub_list_limit < NM_SUB_LIST_LIMIT || $sub_list_limit == ''){ $sub_list_limit = NM_SUB_LIST_LIMIT; }

if($sub_list_url != $_SERVER['REQUEST_URI']){ $sub_list_limit = NM_SUB_LIST_LIMIT; }

/* DB */
// 전체 월1 화2 수3 목4 금5 토6 일7
$sql_cm_public_cycle_in = "";


$sql_cmweek_field_ck = ""; // 18-05-19 마오랑대표&부사장님지시사항
if(!($_menu == 'all' || $_menu == '')){
	$cmweek_cycle_in_arr = get_comics_cycle_fix($_menu);
	$sql_cm_public_cycle_in = "AND c.cm_public_cycle IN(".$cmweek_cycle_in_arr.") ";
	$sql_cmweek_field_ck = "
	if(c.cm_public_cycle IN(".$cmweek_cycle_in_arr.")=1,1,0)as cmweek_field_ck,
	";
	// 18-05-19 마오랑대표&부사장님지시사항
}else{
	$_menu = 'all';
}

// 오늘 요일 표기 하기 위한...
$cm_public_cycle_in = get_comics_cycle_fix(week_type(7));

$cmweek_arr = array();
$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');

// 총갯수
$sql_cmweek_total = " SELECT count(*) as total FROM comics c WHERE 1 $sql_cm_adult $sql_cm_public_cycle_in and c.cm_service = 'y'; ";
$row_total = sql_count($sql_cmweek_total, 'total');

// 익스플로러일때 전체 출력-임시
if(substr(get_brow(HTTP_USER_AGENT), 0, 4) == "MSIE"){ $sub_list_limit = $row_total; }

// cm_reg_date_new 아직 사용 안하지만 나중에 사용할수 있음

// 특정 만화 순서 앞으로...18-04-25
for($i=1;$i<=7;$i++){ $week_comicno_arr[$i] = array(); } // 배열선언
$sql_cw_adult = sql_adult($mb_adult_permission , 'cw_adult');
$sql_cw_class = "";
if($sql_cw_adult !=''){ $sql_cw_class = " AND cw_class LIKE '%_t' "; }

$sql_cw = " select * from comics_week where 1 $sql_cw_adult $sql_cw_class order by cw_class, cw_ranking ";
$result_cmweek = sql_query($sql_cw);
while ($row_cmweek = sql_fetch_array($result_cmweek)) {
	$cw_class_key = intval(str_replace("_t", "", $row_cmweek['cw_class']));
	if($cw_class_key == 0){
		array_push($week_comicno_arr[7],$row_cmweek['cw_comics']);
	}else{
		array_push($week_comicno_arr[$cw_class_key],$row_cmweek['cw_comics']);
	}
}

if(!($_menu == 'all' || $_menu == '')){
	$week_comicno_order = $week_comicno_arr[$_menu];
}else{
	$week_comicno_order = $week_comicno_arr[week_type(7)];
}

$sql_week_comicno_field = "";
$sql_week_comicno_order = "";

$sql_week_comicno_where = ""; // 18-05-14 마오랑대표&부사장님지시사항

if(count($week_comicno_order) > 0){
	$week_comicno_order_arr = $week_comicno_order;

	$sql_week_comicno_field = " CASE ";
	foreach($week_comicno_order_arr as $week_comicno_order_key => $week_comicno_order_val){
		$sql_then = 99 - intval($week_comicno_order_key);
		$sql_week_comicno_field.= " WHEN c.cm_no = '".$week_comicno_order_val."' THEN ".$sql_then." ";
	}
	$sql_week_comicno_field.= "ELSE 0 ";
	$sql_week_comicno_field.= "END AS cm_no_special, ";

	$sql_week_comicno_order = "cm_no_special DESC, ";

	$sql_week_comicno_where = " OR c.cm_no IN (".implode(", ",$week_comicno_order).") "; // 18-05-14 마오랑대표&부사장님지시사항
}


$sql_cmweek = " SELECT c.*, c_prof.cp_name as prof_name, 
					".$sql_week_comicno_field."
					".$sql_cmweek_field_ck."
					if(c.cm_public_cycle IN(".$cm_public_cycle_in.")=1,
						if(left(c.cm_episode_date,10)>='".NM_TIME_M1."',1,0),0
					)as cm_public_cycle_ck, 
					if(left(c.cm_reg_date,10)>='".NM_TIME_M1."',c.cm_reg_date,0)as cm_reg_date_new, 
					if(left(c.cm_episode_date,10)>='".NM_TIME_M1."',1,0)as cm_episode_date_up 
				FROM comics c 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 $sql_cm_adult $sql_cm_public_cycle_in and c.cm_service = 'y' 
				$sql_week_comicno_where 				   
				ORDER BY ".$sql_week_comicno_order." 
				cm_end ASC, cm_public_cycle_ck DESC, cm_episode_date_up DESC, cm_reg_date DESC, cm_episode_date DESC 
				LIMIT 0, ".$sub_list_limit."; ";

// 리스트 가져오기
$cmweek_arr =cs_comics_content_list($sql_cmweek, substr($nm_config['nm_path'], -1));
$sub_list_bool = "false";
if(count($cmweek_arr) < NM_SUB_LIST_LIMIT){ $sub_list_bool = "true"; }

/* view */

include_once($nm_config['nm_path']."/cmweek.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>