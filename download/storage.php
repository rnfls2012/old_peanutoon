<?php
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동

if(!($nm_member['mb_class'] == 'a' || $_SESSION['ss_adm_id'] != '')){
	echo "다운로드 할 권한이 없습니다.";
	die;
}else{
	// 보내준 파일은 urlencode() 함수 필수
	$storage_object = urldecode($_storage_file);
	$storage_filename = 'download/'.$storage_object;

	// 경로와 파일명 분리
	$storage_pos = strrpos($storage_filename, '/');

	$download_folder = substr($storage_filename, 0, $storage_pos-1);
	$download_filename = substr($storage_filename, $storage_pos+1, strlen($storage_filename));

	// 저장 디렉토리 생성 및 서버에 파일저장
	mkdirAll($download_folder);
	$object = $container->get_object($storage_object); // 파일박스에서 다운로드 받고자 하는 오브젝트
	$object->save_to_filename(NM_PATH.'/'.$download_folder.'/'.$download_filename); //실제 로컬에 저장 받을 시, 저장되는 파일 네임
	
	include_once NM_PATH.'/download/file.php'; // 파일 다운로드
}
?>