<?php
include_once './_common.php'; // 공통

$unlink_mode = false;

if($storage_object){ // storage 가 있다면...
	$unlink_mode = true;
}else{
	$download_folder = $_REQUEST['folder'];
	$download_filename = $_REQUEST['filename'];
	$download_file = $_REQUEST['file'];
	$unlink = $_REQUEST['unlink'];
	if($unlink == 'y'){ $unlink_mode = true; }
}

if($download_file){ // file(path와 filename 모두 포함된 문자열이 있다면...
	$download_path = $download_file;
}else{
	$download_path = NM_PATH.'/'.$download_folder.'/'.$download_filename;
}

// 나중에 빈 디렉토리 자동 삭제 기능 넣기

if (file_exists($download_path)) {
	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	//header('Content-Disposition: attachment; filename='.basename($download_path));
	header('Content-Disposition: attachment; filename='.$download_filename);
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($download_path));
	ob_clean();
	flush();
	readfile($download_path);
	
	// 디렉토리 파일 삭제
	if( $unlink_mode == true){ @unlink($download_path); }
	exit;
}
?>
<script type="text/javascript">
<!--
	sr_pop.self.close();
-->
</script>