<? include_once '_common.php'; // 공통 
$listing_img = NM_CDN.'/listing/';
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_URL;?>/listing/<?=NM_PC?>/css/amu_ad.css<?=vs_para();?>" />
		<? /* // 성인 체크 - 18-12-04 end */
		if($adultcheck != 'y'){  ?>
		<style type="text/css">
			.adultcheck{ display:none !important; }
		</style>
		<? } /* // 성인 체크 - 18-12-04 end */ ?>		

		<header><img src="<?=$listing_img?>title.png" alt="지금 광고중인 작품!" /></header>
		
		<section class="area-pickup adultcheck">
			<div>
				<a href="https://www.peanutoon.com/comics.php?comics=3473"><img src="<?=$listing_img?>link08.png" alt="죽어도 나만 사랑한다고 말해" /></a>
			</div>
			<h2>죽어도 나만 사랑한다고 말해</h2>
			<div class="genretag"><span class="genre romance">로맨스</span></div>
			<div class="comicsexp">나는, 그 남자에게…지배당하고 있다. 모두가 부러워하는 결혼 상대를 가진 유리코. 하지만 그녀의 결혼 생활은 애정이라는 이름의 집착으로 얼룩져 있었다. 자신을 지배하는 남편, 그리고 자신을 원하는 남편의 동생. 두 사람의 사이에서 헤메이는 그녀…. 유리코는 해방될 수 있을 것인가?</div>
			<ul class="btnset">
				<li><a href="https://www.peanutoon.com/comicsview.php?comics=3473&episode=38628"><img src="<?=$listing_img?>btn_free02.png" alt="무료화 보러가기" /></a></li>
			</ul>
		</section>
		
		<section class="area-pickup adultcheck">
			<div>
				<a href="https://www.peanutoon.com/comics.php?comics=3474"><img src="<?=$listing_img?>link07.png" alt="그녀는 하고 싶어" /></a>
			</div>
			<h2>그녀는 하고 싶어</h2>
			<div class="genretag"><span class="genre romance">로맨스</span></div>
			<div class="comicsexp">완벽한 여자를 만드는 마지막 한 조각의 비밀! 그녀는 오늘도 하고 싶어 합니다. 지위, 명예, 동경, 선망…. 완벽한 여자로서의 조건을 모두 갖춘 그녀, 아마키 린코. 하지만 그런 그녀에게도 단 하나 부족한 것이 있었으니…. 그것은 바로 '성 경험'?! 단 하나 모자란 조각을 채우기 위해 고군분투하는 린코 앞에 완벽한 스펙의 동정남 카미시카 겐이 나타난다! 서로의 비밀을 공유하게 된 두 사람은 "탈 미경험 파트너 협정"을 맺으며 기묘한 관계를 시작하게 되는데…?!</div>
			<ul class="btnset">
				<li><a href="https://www.peanutoon.com/comicsview.php?comics=3474&episode=38637"><img src="<?=$listing_img?>btn_free02.png" alt="무료화 보러가기" /></a></li>
			</ul>
		</section>
		
		<section class="area-pickup adultcheck">
			<div>
				<a href="https://www.peanutoon.com/comics.php?comics=1814"><img src="<?=$listing_img?>link06.png" alt="미남 간수의 절대명령 -이 교도소에 여죄수는 나 혼자?!-" /></a>
			</div>
			<h2>미남 간수의 절대명령 -이 교도소에 여죄수는 나 혼자?!-</h2>
			<div class="genretag"><span class="genre romance">로맨스</span></div>
			<div class="comicsexp">"단순한 신체검사라고 했을 텐데… 왜 젖은 건가?" 느끼고 싶지 않은데 그의 손길에 몇 번이나 절정을 맛본 거지…? 이대로 가다간 내 몸, 이상해질 것 같아! 억울하게 죄를 뒤집어쓰고 형무소에 수감된 히나. 그곳에서 히나는 잘생겼지만 냉혹한 간수, 묘진 아키에게 성적 지배를 당하기 시작한다. 깃털처럼 부드러운 감촉, 섬세한 손놀림, 외설적인 혀끝…. 무자비하게 끝없는 쾌감을 주는 그는 시간과 장소를 가리지 않고 히나를 희롱하는데. 점점 음란해지는 몸으로 개발되어가는 그녀의 몸. 도와주세요, 더는 가고 싶지 않아…!!</div>
			<ul class="btnset">
				<li><a href="https://www.peanutoon.com/comicsview.php?comics=1814&episode=20625"><img src="<?=$listing_img?>btn_free02.png" alt="무료화 보러가기" /></a></li>
			</ul>
		</section>
		
		<section class="area-pickup adultcheck">
			<div>
				<a href="https://www.peanutoon.com/comics.php?comics=1894"><img src="<?=$listing_img?>link05.png" alt="누구랑 할 거야? -두 여자와의 엉망진창 동거생활-" /></a>
			</div>
			<h2>누구랑 할 거야? -두 여자와의 엉망진창 동거생활-</h2>
			<div class="genretag"><span class="genre adult">성인</span></div>
			<div class="comicsexp">순간의 실수로 야릇한 분위기로 바뀌어버린 우리의 관계. 아무리 소꿉친구라지만, 여자 두 명과 함께 사는 건 힘들다구!! 대학 근처에서 자취를 하고 있던 켄이치. 어느 날, 소꿉친구인 레이미와 유카리가 찾아와 학교가 가깝다는 이유로 이 집에서 지내겠다고 한다. 아니나다를까, 동거 생활 첫날부터 세 남녀는 엉망진창 사건 연발! 유카리가 샤워하고 있는 욕실에서 미끄러져 덮쳐버리거나, 켄이치가 자고 있는 벽장에 레이미가 몰래 들어와 몸을 밀착하거나…. 켄이치, 레이미, 유카리가 함께 보내는 누구에게도 말 못할 매일 밤. 서로의 욕망은 점점 부풀어져만 가는데…!</div>
			<ul class="btnset">
				<li><a href="https://www.peanutoon.com/comicsview.php?comics=1894&episode=21690"><img src="<?=$listing_img?>btn_free02.png" alt="무료화 보러가기" /></a></li>
			</ul>
		</section>
		
		<section class="area-pickup adultcheck">
			<div>
				<a href="https://www.peanutoon.com/comics.php?comics=1892"><img src="<?=$listing_img?>link04.png" alt="만져 보니 여동생?! -잠든 여친 옆에서-" /></a>
			</div>
			<h2>만져 보니 여동생?! -잠든 여친 옆에서-</h2>
			<div class="genretag"><span class="genre adult">성인</span></div>
			<div class="comicsexp">'오빠가 이런 데를 만지면… 느껴버려…!' 여자 친구를 집으로 초대하고, 나도 드디어 동정 졸업! …이라고 생각한 순간, 가출해서 나를 찾아온 의붓동생 카오루의 집요한 초인종 소리. 게다가 매 순간 여자 친구와의 사이를 가로막고 방해받는다. 더는 참을 수가 없어진 욕구를 풀기 위해 화장실로 가지만 카오루가 문을 열어 버리는데?! 그런데 빨리 나가기는커녕 문을 닫고 들어와 내 것을 쥐며 하는 말이… "내가… 해줄까?"</div>
			<ul class="btnset">
				<li><a href="https://www.peanutoon.com/comicsview.php?comics=1892&episode=21668"><img src="<?=$listing_img?>btn_free02.png" alt="무료화 보러가기" /></a></li>
			</ul>
		</section>
		
		<section class="area-pickup">
			<div>
				<a href="https://www.peanutoon.com/comics.php?comics=3379"><img src="<?=$listing_img?>link01.png" alt="옆 집 남자와 함께한 7일간" /></a>
			</div>
			<h2>옆집 남자와 함께한 7일간</h2>
			<div class="genretag"><span class="genre romance">로맨스</span></div>
			<div class="comicsexp">시내에서 떨어진 어느 산장에서 일주일 전 실종되었던 한 유부녀가 발견되었다. 피투성이의 모습으로 나타난 그녀, 휴우가 사에코는 어째선지 범인으로 지목된 살인마를 두둔하기 시작하는데…? 불행했던 5년간의 결혼 생활, 운명적인 만남, 그리고 밝혀지기 시작하는 유괴 사건의 전말…. 과연 7일간 그녀가 겪었던 진실은 무엇인가?</div>
			<ul class="btnset">
				<li><a href="http://www.peanutoon.com/comicsview.php?comics=3379&episode=37569"><img src="<?=$listing_img?>btn_free.png" alt="무료화 보러가기" /></a></li>
				<li><a href="https://www.peanutoon.com/ctlogin.php"><img src="<?=$listing_img?>btn_join.png" alt="회원가입" /></a></li>
			</ul>
		</section>
		
		<section class="area-pickup">
			<div>
				<a href="https://www.peanutoon.com/comics.php?comics=3422"><img src="<?=$listing_img?>link02.png" alt="사귄 지 0일, 결혼" /></a>
			</div>
			<h2>사귄 지 0일, 결혼</h2>
			<div class="genretag"><span class="genre romance">로맨스</span></div>
			<div class="comicsexp">"기분이 좋을 땐 제대로 소리 내서 말해줘요. 알았죠?" 친구들도, 전 남친마저도 누군가와 행복한 시간을 보내고 있다! 보란 듯이 연애에 성공해서 행복한 모습을 보여줄 테다! 그렇게 결심하고 맞선 사이트에 가입한 쿠로키. 하지만 처음 만난 날, 방심한 사이 결혼까지 일사천리?! 진지하면서도 이상한 이 마츠다라는 남자, 믿어도 되는 걸까?</div>
			<ul class="btnset">
				<li><a href="https://www.peanutoon.com/comicsview.php?comics=3422&episode=37971"><img src="<?=$listing_img?>btn_free.png" alt="무료화 보러가기" /></a></li>
				<li><a href="https://www.peanutoon.com/ctlogin.php"><img src="<?=$listing_img?>btn_join.png" alt="회원가입" /></a></li>
			</ul>
		</section>
		
		<section class="area-pickup">
			<div>
				<a href="https://www.peanutoon.com/comics.php?comics=3423"><img src="<?=$listing_img?>link03.png" alt="어린 아내와 신혼생활" /></a>
			</div>
			<h2>어린 아내와 신혼생활</h2>
			<div class="genretag"><span class="genre romance">로맨스</span></div>
			<div class="comicsexp">맞선, 데이트 세 번, 그리고 바로 혼인신고?! 열 다섯 차이나는 커플의 갑작스럽고도 달콤한 신혼 생활! 하나사토 하루미, 44살 남성, 이혼 경력 있음. 회사 거래처 사장의 권유를 거절하지 못하고, 15살 차이 나는 사장의 딸, 무라카미 마이코과 맞선을 보게 된다. 처음에는 하루미 쪽에서 물러나려고 했지만, 어째선지 마이코가 적극적으로 그에게 다가오기 시작하는데…?</div>
			<ul class="btnset">
				<li><a href="https://www.peanutoon.com/comicsview.php?comics=3423&episode=37978"><img src="<?=$listing_img?>btn_free.png" alt="무료화 보러가기" /></a></li>
				<li><a href="https://www.peanutoon.com/ctlogin.php"><img src="<?=$listing_img?>btn_join.png" alt="회원가입" /></a></li>
			</ul>
		</section>