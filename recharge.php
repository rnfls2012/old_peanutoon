<?
include_once '_common.php'; // 공통

$_SESSION['recharge_order_no'] = ''; // 주문번호 초기화

error_page();

// 181024 주문번호-캔슬용 삭제 : 세션처리했었는데 뒤로가기할때 문제가 발생되어 DB 저장하기로 함
cs_del_pg_member_order($nm_member);


// 181022 -> 리다이렉트 쿠키 남기기
set_redirect_cookie();

// echo get_js_cookie("redirect");

// 181001
cs_login_check($_SERVER['REQUEST_URI']); // 로그인 체크

// 이벤트 충전/지급 이벤트
$er_arr = array();
$sql_er = "SELECT * FROM event_recharge WHERE er_state = 'y' AND er_type = '1' ORDER BY er_reg_date desc LIMIT 0 , 1";
$result_er = sql_query($sql_er);
while ($row_er = sql_fetch_array($result_er)) { $er_arr = $row_er; }

/* DB */
$crp_arr = $crp_member_arr = $crp_arr_base = $crp_event_arr = array();
$crp_base_arr = cs_recharge_list('n','',$_mb_crp_no,$_mb_coupondc);
$crp_event_arr = cs_recharge_list('y','',$_mb_crp_no,$_mb_coupondc);

$long_text_sql = "select clt_recharge1, clt_recharge2, clt_recharge3, clt_recharge4, clt_recharge5, clt_recharge6 from config_long_text";
$long_text_row = sql_fetch($long_text_sql);

// 이벤트 있을 시
$crp_event_text = "";
if(count($crp_event_arr) > 0){
	foreach($crp_event_arr as $crp_event_key => $crp_event_val){ 
		if($nm_member['mb_won_count'] == $crp_event_val['crp_cash_count_event']){
			$crp_event_text = $crp_event_val['crp_cash_count_event_text'];
			array_push($crp_member_arr,  $crp_event_val); // 기본 
		}
	}
}

/* 안내 문구 DB */
$clt_sql = "select * from config_long_text";
$clt_row = sql_fetch($clt_sql);

$recharge_path = $nm_config['nm_path']."/recharge.php";

if (preg_match('/'.NM_APP_MARKET.'/i', HTTP_USER_AGENT)){	
	if($nm_member['mb_won'] < 9900) {
		$recharge_path = $nm_config['nm_path']."/recharge_".strtolower(NM_APP_MARKET).".php";
	} // end if
} // end if

/* [Web]충전소띠배너 / [Mobile]충전소띠배너 */
if($nm_config['nm_mode'] == NM_PC){ $banner_no = 7; }
else{ $banner_no = 8;  }

/* TEST 배너는 ADMIN만 나오도록 20180322 */
$emb_test = " AND emb_test = 'n' ";
if(mb_class_permission("a")) {
	$emb_test = "AND (emb_test = 'n' OR emb_test = 'y') ";
} // end if

$banner_arr = array();
$sql_banner_adult = sql_adult($mb_adult_permission , 'emb_adult');
$sql_banner = " SELECT * FROM epromotion_banner_cover embc JOIN epromotion_banner emb ON embc.embc_emb_no = emb.emb_no 
				WHERE 1 AND embc_position = '$banner_no' AND emb_state = 'y' $emb_test $sql_banner_adult  
				ORDER BY IF(embc_order=0,99999,embc_order) ASC , embc_no DESC LIMIT 0, 1";
$row_banner = sql_fetch($sql_banner);
if($row_banner['emb_scripty'] == 'n'){
	$emb_url = NM_URL.'/'.$row_banner['emb_url'];
	if(strpos($row_banner['emb_url'], 'ttp://') > 1 || strpos($row_banner['emb_url'], 'ttps://') > 1){
		$emb_url = $row_banner['emb_url'];						
	}
}
// 이미지 표지
$banner_img_url = img_url_para($row_banner['embc_cover'], $row_banner['embc_date'], '', 'embc_cover', '3');

// 랜덤박스 할인권
$coupondc_list = randombox_cs_recharge_list_coupondc_get($nm_member, $_mb_coupondc);
$coupondc_list_ck = false;
if(count($coupondc_list) > 0 && $coupondc_list['coupondc_ck'] == true){ $coupondc_list_ck = $coupondc_list['coupondc_ck'];}

// 알림톡 보내기 위한 쿠키값 (curl의 redirect 방지 위함) - 180403 (지완)
set_cookie('send_talk', mt_rand(1,mt_getrandmax()), 3600);

/* view */
$crp_count_arr = 0; // 캐쉬충전 카운트
include_once (NM_PATH.'/_head.php'); // 공통
// 181001 아무타스 리타겟팅 태그
gtm_amutus($nm_member); 

include_once($recharge_path);
include_once (NM_PATH.'/_tail.php'); // 공통
?>