<?
include_once '_common.php'; // 공통

error_page();

if($is_guest == true){ // 비로그인중이면 메인화면으로...
	goto_url(NM_URL);
}

// 가입전 보던 페이지 가져오기
$ss_http_referer = base_filter(get_session('ss_http_referer'));

// 아무타스 리타겟팅 태그 - 가입시 출력 => 쿠키생성 18-11-27
$gtm_amutus_entry_cookie = get_cookie('gtm_amutus_entry');
if(intval($nm_member["mb_no"]) > 0){
	if($gtm_amutus_entry_cookie != $nm_member["mb_no"]){
		set_cookie('gtm_amutus_entry', $nm_member["mb_no"]);
	}
}
// 18-12-03
if(get_js_cookie("redirect") !=''){
	$ss_http_referer = goto_redirect(get_js_cookie("redirect"));
}

/* view */
include_once (NM_PATH.'/_head.php'); // 공통
// 181001 아무타스 리타겟팅 태그
gtm_amutus($nm_member); 
if(intval($nm_member["mb_no"]) > 0){
	if($gtm_amutus_entry_cookie != $nm_member["mb_no"]){ // 쿠키가 있다면 출력안함
		gtm_amutus_entry($nm_member); 
	}
}

include_once($nm_config['nm_path']."/ctjoinup.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>