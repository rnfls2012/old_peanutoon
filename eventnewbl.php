<?php
/**
 * Created by PhpStorm.
 * User: Jiwan Ye
 * Date: 2018-08-20
 * Time: 오후 5:48
 */

include_once '_common.php'; // 공통

/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */
/* view */
include_once (NM_PATH.'/_head.php'); // 공통

/* eventful 디렉토리로 이동 */
include_once (NM_EVENTFUL_PATH.'/'.$nm_config['nm_mode']."/event_new_bl.php");

include_once (NM_PATH.'/_tail.php'); // 공통