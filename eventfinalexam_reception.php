<?php
include_once '_common.php'; // 공통

$finalexam_img_url = NM_IMG.'/finalexam/reception'; // 이미지 경로
$efp_arr = $efm_arr = array();

$efp_no = num_check(tag_get_filter($_REQUEST['efp_no'])); // 교시값 파라미터
$efp_test = num_eng_check(tag_get_filter($_REQUEST['efp_test'])); // 탭 위치로 가기


$scrolltop_js = "";
if($efp_test == "y"){
	$scrolltop_js = '
	window.onload = function(){
		var scrolltest = $(".evt-test").offset().top;
		var header_h = $("#header").height();
		var test_top = scrolltest-header_h;		
		$("html, body").animate({scrollTop: test_top}, 500);
	}
	';
}

if($is_member == true){
	// 회원이면 무조건 넣기
	$sql_insert_efm = " INSERT INTO event_finalexam_member ( efm_member, efm_member_idx, efm_date 
					    ) VALUES ( '".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', '".NM_TIME_YMDHIS."')
					  ";
    @mysql_query($sql_insert_efm); // unique -> UNIQUE INDEX 으로 동일한 데이터는 에러로 저장이 안됨

	// 데이터 가져오기
	$sql_efm = " SELECT * FROM event_finalexam_member WHERE 
	            efm_member = '".$nm_member['mb_no']."' AND  efm_member_idx = '".$nm_member['mb_idx']."' 
			   ";
	$row_efm = sql_fetch($sql_efm);

	$efm_arr = $row_efm;
}

$sql_efp = "SELECT * FROM event_finalexam_period WHERE efp_state = 'y' ORDER BY efp_no ";
$result_efp = sql_query($sql_efp);
for($e=1; $row_efp = sql_fetch_array($result_efp); $e++){
	
	$row_efp['efp_date_start_txt'] = str_replace("-", ".", substr($row_efp['efp_date_start'],2,strlen($row_efp['efp_date_start'])));
	$row_efp['efp_date_end_txt'] = str_replace("-", ".", substr($row_efp['efp_date_end'],2,strlen($row_efp['efp_date_end'])));
	$row_efp['efp_date_start_week_txt'] = get_yoil($row_efp['efp_date_start']);
	$row_efp['efp_date_end_week_txt'] = get_yoil($row_efp['efp_date_end']);
	$row_efp['tab_class'] = '';
	$row_efp['con_class'] = 'hide';
	$row_efp['data_tab_use'] = 'n';
	$row_efp['data_tab_date'] = $row_efp['efp_mark']."교시 시작 시간은 <br/> ";
	$row_efp['data_tab_date'].= substr($row_efp['efp_date_start'], 2, 2)."년 ";
	$row_efp['data_tab_date'].= substr($row_efp['efp_date_start'], 5, 2)."월 ";
	$row_efp['data_tab_date'].= substr($row_efp['efp_date_start'], 8, 2)."일 ";
	$row_efp['data_tab_date'].= "00시 입니다.";

	// 응시링크
	$row_efp['efp_link_txt'] = '응시하기';
	$row_efp['efp_link_msg'] = '로그인해주시기 바랍니다.';
	$row_efp['efp_link'] = NM_URL.'/ctlogin.php';

	// 시험 기간
	$efp_time_start = $row_efp['efp_date_start'].' '.NM_TIME_HI_start;
	$efp_time_end = $row_efp['efp_date_end'].' '.NM_TIME_HI_end;

	// tab use
	if($efp_time_start <= NM_TIME_YMDHIS){ $row_efp['data_tab_use'] = 'y'; }

	if($efp_no == ''){
		if($efp_time_start <= NM_TIME_YMDHIS && $efp_time_end >= NM_TIME_YMDHIS){
			$row_efp['tab_class'] = 'test_on';
			$row_efp['con_class'] = '';
		}
	}else{
		if($row_efp['efp_mark'] == $efp_no){
			$row_efp['tab_class'] = 'test_on';
			$row_efp['con_class'] = '';
		}
	}

	// 로그인 중이라면...
	$row_efp['efm_count1'] = $row_efp['efm_count2'] = $row_efp['efm_count3'] = '0';
	$row_efp['efm_efp_no1'] = $row_efp['efm_efp_no2'] = $row_efp['efm_efp_no3'] = '0';
	$row_efp['efm_highscore1'] = $row_efp['efm_highscore2'] = $row_efp['efm_highscore3'] = '0';
	if($is_member == true){
		$row_efp['efm_count1'] = $efm_arr['efm_count1'];
		$row_efp['efm_count2'] = $efm_arr['efm_count2'];
		$row_efp['efm_count3'] = $efm_arr['efm_count3'];
		$row_efp['efm_efp_no1'] = $efm_arr['efm_efp_no1'];
		$row_efp['efm_efp_no2'] = $efm_arr['efm_efp_no2'];
		$row_efp['efm_efp_no3'] = $efm_arr['efm_efp_no3'];
		$row_efp['efm_highscore1'] = $efm_arr['efm_highscore1'];
		$row_efp['efm_highscore2'] = $efm_arr['efm_highscore2'];
		$row_efp['efm_highscore3'] = $efm_arr['efm_highscore3'];
		
		if($efp_time_start <= NM_TIME_YMDHIS && $efp_time_end >= NM_TIME_YMDHIS){
			// 응시링크
			$row_efp['efp_link_msg'] = $row_efp['efp_mark'].'교시 응시합니다.<br/><br/>※휴대폰 기종의 뒤로가기 버튼을 누르시면 시험이 종료됩니다.<br/>시험지 하단의 뒤로가기 버튼을 이용해주시기 바랍니다. ';
			$row_efp['efp_link'] = NM_URL.'/eventfinalexam.php?efp_no='.$row_efp['efp_no'];
			

			if($row_efp['efm_count1'] >= 3 && $efp_time_start <= NM_TIME_YMDHIS && $efp_time_end >= NM_TIME_YMDHIS && $e == 1 ){
				$row_efp['efp_link_msg'] = '해당 영역의 3번 응시완료 하셨습니다.';
				$row_efp['efp_link_txt'] = '응시완료';
				$row_efp['efp_link'] = "";
			}

			if($row_efp['efm_count2'] >= 3 && $efp_time_start <= NM_TIME_YMDHIS && $efp_time_end >= NM_TIME_YMDHIS && $e == 2 ){
				$row_efp['efp_link_msg'] = '해당 영역의 3번 응시완료 하셨습니다.';
				$row_efp['efp_link_txt'] = '응시완료';
				$row_efp['efp_link'] = "";
			}

			if($row_efp['efm_count3'] >= 3 && $efp_time_start <= NM_TIME_YMDHIS && $efp_time_end >= NM_TIME_YMDHIS && $e == 3  ){
				$row_efp['efp_link_msg'] = '해당 영역의 3번 응시완료 하셨습니다.';
				$row_efp['efp_link_txt'] = '응시완료';
				$row_efp['efp_link'] = "";
			}
		}else{
			$row_efp['efp_link_msg'] = '해당 영역의 응시 기간이 아닙니다.';
			$row_efp['efp_link'] = "";
			$row_efp['efp_link_txt'] = '응시완료';
		}
	}

	array_push($efp_arr, $row_efp);
}


include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/eventfinalexam_reception.php");

include_once (NM_PATH.'/_tail.php'); // 공통