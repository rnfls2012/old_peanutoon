<? include_once '_common.php'; // 공통

cs_login_check(); // 로그인 체크

$code = get_int_eng(base_filter($_REQUEST['coupon_num']), 2); // 쿠폰코드

if(!num_eng_check($code) || strlen($code) != 16) {
	cs_alert("쿠폰 값이 올바르지 않습니다! 쿠폰 값을 확인해주시기 바랍니다.", $_SERVER['HTTP_REFERER']);
	die;
} // end if

/* DB */
$coupon_row = sql_fetch("SELECT * FROM event_coupon WHERE ec_code='".$code."'");

if($coupon_row['ec_use_date'] != "" || $coupon_row['ec_member'] != 0 || $coupon_row['ec_member_idx'] != "") {
	cs_alert("유효하지 않거나, 이미 사용한 쿠폰입니다!",  $_SERVER['HTTP_REFERER']);
	die;
} // end if

$couponment_row = sql_fetch("SELECT * FROM event_couponment WHERE ecm_no = '".$coupon_row['ec_ecm_no']."'");

if($couponment_row['ecm_state'] == "n" || $couponment_row['ecm_end_date'] < NM_TIME_YMDHIS) {
	cs_alert("사용기간이 지났거나, 해당 이벤트가 종료되었습니다.",  $_SERVER['HTTP_REFERER']);
	die;
} // end if

if($couponment_row['ecm_overlap'] == "n") {
	$coupon_use_sql = "SELECT count(*) AS coupon_use FROM event_coupon WHERE ec_ecm_no='".$coupon_row['ec_ecm_no']."' AND ec_member='".$nm_member['mb_no']."'";
	$use_count = sql_count($coupon_use_sql, 'coupon_use');

	if($use_count > 0) {
		cs_alert($couponment_row['ecm_ticket']." 쿠폰은 중복 사용이 불가합니다.",  $_SERVER['HTTP_REFERER']);
		die;
	} // end if
} // end if

$cash_balance = $nm_member['mb_cash_point']+$couponment_row['ecm_cash_point'];
$point_balance = $nm_member['mb_point']+$couponment_row['ecm_point'];

/* 업데이트 순서
1. 쿠폰 사용
2. 회원 포인트 income 여부 테이블
3. 회원 테이블(쿠폰 보너스 지급) */

// 1. 쿠폰 사용 업데이트
$ec_count = $coupon_row['ec_count'] + 1;
$coupon_result = sql_query("UPDATE event_coupon SET ec_member = '".$nm_member['mb_no']."', ec_member_idx = '".$nm_member['mb_idx']."', ec_use_date = '".NM_TIME_YMDHIS."', ec_count = '".$ec_count."' WHERE ec_code = '".$code."' AND ec_ecm_no='".$couponment_row['ecm_no']."'");
if($coupon_result) {
	$mb_income_result = cs_set_member_point_income_event($nm_member, $couponment_row['ecm_cash_point'], $couponment_row['ecm_point'], 'coupon', $couponment_row['ecm_ticket']);
	// 2. 회원 테이블 업데이트
	if($mb_income_result) {
		log_coupon_use($nm_member, $couponment_row, $coupon_row);
		cs_alert("쿠폰이 적용되었습니다!", $_SERVER['HTTP_REFERER']);
		die;
	} else {
		cs_alert("포인트 지급에 실패하였습니다. 관리자에게 문의하세요.", $_SERVER['HTTP_REFERER']);
	} // end else
} else {
	cs_alert("쿠폰 사용에 실패하였습니다. 관리자에게 문의하세요.", $_SERVER['HTTP_REFERER']);
	die;
} // end else
/*
// 2. 회원 테이블 업데이트
	$member_result = sql_query("update member set mb_cash_point = '".$cash_balance."', mb_point = '".$point_balance."' where mb_no = '".$nm_member['mb_no']."' and mb_idx = '".$nm_member['mb_idx']."'");
	if($member_result) {
		
		// 3. 회원 포인트 income 여부 INSERT
		$income_result = sql_query("INSERT INTO member_point_income_event(
		mpie_member, mpie_member_idx, mpie_cash_point, mpie_point, mpie_cash_point_balance, 
		mpie_point_balance, mpie_from, mpie_type, mpie_year_month, mpie_year, mpie_month, 
		mpie_day, mpie_hour, mpie_week, mpie_date) VALUES
		('".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', '".$couponment_row['ecm_cash_point']."', '".$couponment_row['ecm_point']."', '".$cash_balance."', '".$point_balance."', '".
			$couponment_row['ecm_ticket']."', 'coupon', '".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', '".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."')");
		if($income_result) {
			cs_alert("쿠폰이 적용되었습니다!", $_SERVER['HTTP_REFERER']);
			die;
		} else {
			cs_alert("포인트 지급 기록에 실패하였습니다. 관리자에게 문의하세요.", $_SERVER['HTTP_REFERER']);
		} // end else

	} else {
		cs_alert("포인트 지급에 실패하였습니다. 관리자에게 문의하세요.", $_SERVER['HTTP_REFERER']);
		die;
	} // end else
	*/
?>