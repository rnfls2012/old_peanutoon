<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동

alert('보상 기간 종료 입니다.',NM_URL."/myinfo.php");
die;


// 로그인 체크
cs_login_check(HTTP_REFERER);

$_member = base_filter($_REQUEST['member']);
$_emrm_no = base_filter($_REQUEST['rewardment']);

if($_member != "" && $_emrm_no != "") {
	$mb_row = $rewardment = array();
	$mb_row = mb_get_no($_member); // 회원 정보
	$rewardment = sql_fetch("select * from event_member_rewardment where emrm_no = '".$_emrm_no."'"); // 리워드 정보
	$mb_rw_cnt = sql_count("select count(*) as cnt from event_member_reward where emr_member = '".$mb_row['mb_no']."' and emrm_no = '".$rewardment['emrm_no']."' and emr_state = 'n' and emr_use_date = ''", "cnt"); // 복구 대상 유무

	if($rewardment['emrm_state'] == "y") {
		if($mb_rw_cnt > 0) {
			$reward_update_sql = "UPDATE event_member_reward SET emr_state = 'y', emr_use_date = '".NM_TIME_YMDHIS."' WHERE emr_member = '".$mb_row['mb_no']."' and emrm_no = '".$rewardment['emrm_no']."'";
			$from = $rewardment['emrm_reward']." ".$rewardment['emrm_point']." 미니땅콩 지급";

			if(cs_set_member_point_income_event($mb_row, '0', $rewardment['emrm_point'], 'reward', $from, 'y')) {
				sql_query($reward_update_sql);
				cs_alert("보상이 지급되었습니다.", HTTP_REFERER);
				die;
			} else {
				cs_alert("보상 기록에 오류가 발생했습니다. 관리자에게 문의하세요!", HTTP_REFERER);
				die;
			} // end else

		} else {
			cs_alert("보상받을 대상이 아니거나, 이미 보상받으셨습니다.", HTTP_REFERER);
			die;
		} // end else
	} else {
		cs_alert("마감되었거나, 중지된 이벤트입니다.", HTTP_REFERER);
		die;
	} // end else
} else {
	cs_alert("유효하지 않은 접근방식입니다.", HTTP_REFERER);
	die;
} // end else
?>