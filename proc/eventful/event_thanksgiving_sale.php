<?
include_once '_common.php'; // 공통

cs_login_check(); // 로그인 체크

if($nm_member['mb_class'] == "a"){
	alert("관리자계정입니다.일반계정으로 테스트해주세요.", NM_URL."/event_thanksgiving.php?type=sale");
	die;
}

/* 통 sale 작품 리스트 */
	$all_sale_arr = array();
	// array_push($all_sale_arr, array(코믹번호, 할인 후 땅콩, 할인 화 범위, 할인 단위, 추가에피소드번호, 1화에피소드번호));
	array_push($all_sale_arr, array(2630, 44, 21,  2, 0    , 29064));
	array_push($all_sale_arr, array(2660, 44, 16,  2, 0    , 29253));
	array_push($all_sale_arr, array(2151, 78, 28,  2, 0    , 25852));
	array_push($all_sale_arr, array(602 , 52,  9,  2, 0    , 6199));
	array_push($all_sale_arr, array(1365, 79, 82,  2, 0    , 15207));

	array_push($all_sale_arr, array(596 , 56, 47,  2, 17103, 6117));
	array_push($all_sale_arr, array(2832,  6,  0,  2, 0    , 29887));
	array_push($all_sale_arr, array(1572, 11,  0,  1, 0    , 17421));
	array_push($all_sale_arr, array(1964,  9,  0,  1, 0    , 22597));
	array_push($all_sale_arr, array(2097, 13,  0,  1, 0    , 24672));

	// 리스트에 있는지 확인
	$all_sale_comics_arr = array();
	$all_sale_comics_key = 0;	
	$all_sale_comics_bool = false;
	foreach($all_sale_arr as $all_sale_key => $all_sale_val){
		if($all_sale_val[0] == $_comics){
			$all_sale_comics_bool = true;	
			$all_sale_comics_key = intval($all_sale_key);
		}
	}

	if($all_sale_comics_bool == false){
		alert("통 sale 작품이 아닙니다.", NM_URL."/event_thanksgiving.php?type=sale");
		die;
	}

	// 해당 작품값 배열값 저장
	$get_all_sale = array();
	$get_all_sale['cm_no']         = $all_sale_arr[$all_sale_comics_key][0];
	$get_all_sale['cm_cash_point'] = $all_sale_arr[$all_sale_comics_key][1];
	$get_all_sale['cm_field']      = $all_sale_arr[$all_sale_comics_key][2];
	$get_all_sale['cm_dc_point']   = $all_sale_arr[$all_sale_comics_key][3];
	$get_all_sale['cm_ce_no']      = $all_sale_arr[$all_sale_comics_key][4];
	$get_all_sale['cm_ce_first_ep']= $all_sale_arr[$all_sale_comics_key][5];
	
	// 코믹스 정보 가져오기
	$comics = get_comics($_comics);

	// 구매 했는지 확인
	$sql_all_sale_check = " select count(*) as all_sale_pay_ctn from event_thanksgiving_all_sale 
	                        where mpec_member=".$nm_member['mb_no']." 
							and mpec_comics=".$_comics.";";
	$row_all_sale_check = sql_fetch($sql_all_sale_check);
	if(intval($row_all_sale_check['all_sale_pay_ctn']) > 0){
		$sale_view_url_msg = "통 SALE 기획전에서 구매하셨습니다.<br/>“".$comics['cm_series']."“ 작품 1화로 이동합니다.";
		$sale_view_url_msg = this_apple_msg_tag($sale_view_url_msg);
		cs_alert($sale_view_url_msg, get_episode_url($get_all_sale['cm_no'], $get_all_sale['cm_ce_first_ep']));
		die;
	}
	
	// 구매 가능한지 확인
		// 1. 고객 캐쉬포인트+포인트 합친 sum_point포인트
		$mb_sum_point = intval($nm_member['mb_cash_point']) + intval( $nm_member['mb_point'] / NM_POINT );

		// 2. 작품 통 sale 가격 구하기- 1)필드
		$sql_all_sale_episode_from = " from comics_episode_".$comics['cm_big']." 
	                                   where ce_service_state='y' and ce_comics=".$_comics." and ce_pay > 0";
		if($get_all_sale['cm_dc_point'] == 2){
			$sql_all_sale_episode_sumpay_field = " if(ce_chapter>=1,if(ce_chapter<=".$get_all_sale['cm_field'].",1,2), "; // 1화 이상 범위 사이
			$sql_all_sale_episode_sumpay_field.= " if(ce_no=".$get_all_sale['cm_ce_no'].",1,2)) "; // 외전,특별중 1개의 화번호만
		}else if($get_all_sale['cm_dc_point'] == 1){
			$sql_all_sale_episode_sumpay_field = " if(ce_chapter>=1,if(ce_chapter<=".$get_all_sale['cm_field'].",1,1), "; // 1화 이상 범위 사이
			$sql_all_sale_episode_sumpay_field.= " if(ce_no=".$get_all_sale['cm_ce_no'].",1,1)) "; // 외전,특별중 1개의 화번호만
		}else{
			alert("통 sale error no : 1", NM_URL."/event_thanksgiving.php?type=sale");
			die;
		}

		// 2. 작품 통 sale 가격 구하기- 2)SQL
		$sql_all_sale_episode_sumpay = " select sum(ce_pay)as sum_ce_pay, 
	                                     sum(".$sql_all_sale_episode_sumpay_field.") as sum_all_sale,
										 count(ce_pay)as cnt_ce_pay 
									   ".$sql_all_sale_episode_from;
		$row_all_sale_episode_sumpay = sql_fetch($sql_all_sale_episode_sumpay);

		// 3. 비교
		$all_sale_sum_ce_pay   = $row_all_sale_episode_sumpay['sum_ce_pay'];   // 할인 전 값
		$all_sale_sum_all_sale = $row_all_sale_episode_sumpay['sum_all_sale']; // 할인 후 값
		$all_sale_cnt_all_sale = $row_all_sale_episode_sumpay['cnt_ce_pay'];   // 갯수
		if($mb_sum_point - $all_sale_sum_all_sale < 1){
			alert("보유 땅콩/미니땅콩이 부족합니다.", NM_URL."/recharge.php");
			die;
		}


// 성인체크
cs_comics_adult($comics['cm_no'], $nm_member);

/* 사용내역변수 */
$mpu_from = "";

/* ////////////////// 소유한 작품 체크 ////////////////// */
$mpu_from = $comics['cm_series']." >> ".$get_all_sale['cm_cash_point']."땅콩(미니땅콩) 통 sale 구매";
$goto_url = get_episode_url($_comics, $_episode); // 전체결제후 바로 이동할 URL

// 에피소드
$sql = " select *, ".$sql_all_sale_episode_sumpay_field." as all_sale ".$sql_all_sale_episode_from." ORDER BY ce_order";
$result = sql_query($sql);
for ($e=0; $row=sql_fetch_array($result); $e++) {
	$episode = $row;
	
	/* ////////////////// 체크 변수 초기화////////////////// */
	$episode['ce_event_free'] = $episode['ce_event_sale'] = $event_sale = $event_free = $comics_buy = $comics_free = $state_except = 'n'; 
	// 이벤트세일 = 이벤트무료 = 코믹스무료 = 통계제외 = n
	$comics_buy = array();
	$comics_buy['sl_event'] =  $comics_buy['sl_sale'] =  $comics_buy['sl_free'] = 0;
	$comics_buy['mb_cash_point'] = $comics_buy['mb_point'] = $comics_buy['sl_pay'] = 0;
	$comics_buy['mbc_own_type'] = $comics['cm_own_type'];

	/* ////////////////// 회원 체크 ////////////////// */
	/*
	if($nm_member['mb_class'] == "a" || $_SESSION['ss_adm_id'] != "" || $_SESSION['ss_adm_id'] != NULL){ $state_except = $comics_free = "y"; }
	if($nm_member['mb_class'] == "p"){ 
		$state_except = "y";
	} // end if
	*/

	/* ////////////////// 이벤트 체크 ////////////////// */

	// 이벤트 코인할인
	$episode['ce_pay'] = $episode['all_sale'];
	$episode['ce_event_sale'] = $event_sale = "y";
	$comics_buy['sl_sale'] = $comics_buy['sl_event'] = 1;

	/* ////////////////// 결제 ////////////////// */
		$comics_buy = cs_set_expen_episode($comics, $episode, $nm_member, $comics_buy, 8, 'n');  // 통 세일 번호
		$mb_all_pay_count++;

	/* ////////////////// 통계 ////////////////// */
	if($state_except == 'n'){ //sales, sales_age_sex, sales_episode_1, sales_episode_1_age_sex
		$comics_buy = cs_set_sales_data($comics, $episode, $nm_member, $comics_buy, 'n', 8);
	}
}

// 사용내역 저장
$prev_nm_member = $nm_member; // 이전 데이터 저장
$prev_mb_cash_point = intval($nm_member['mb_cash_point']);
$prev_mb_point = intval($nm_member['mb_point']);



// 회원정보 포인트 처리 후 재로드을 위해서...
$nm_member = mb_get($nm_member['mb_id']);
$next_mb_cash_point = intval($nm_member['mb_cash_point']);
$next_mb_point = intval($nm_member['mb_point']);
$mb_cash_point_use = $prev_mb_cash_point - $next_mb_cash_point;
$mb_point_use = $prev_mb_point - $next_mb_point;

cs_set_member_point_used_expen($comics, $nm_member, $mb_cash_point_use, $mb_point_use, $next_mb_cash_point, $next_mb_point, $mb_all_pay_count, $mpu_from, 7);

// 이벤트 테이블 저장
this_event_thanksgiving_all_sale($comics, $all_sale_sum_all_sale, $nm_member, $comics_buy, $prev_nm_member, $all_sale_cnt_all_sale);

// 코믹스 에피소드로 이동
goto_url($goto_url);

// ------------------------------------------------------------------------------

function this_apple_msg_tag($msg){

	if(stristr(HTTP_USER_AGENT, "iPhone") || stristr(HTTP_USER_AGENT, "iPod")){
			$msg = strip_tags($msg);
	}
	return $msg;
}

function this_event_thanksgiving_all_sale($comics, $all_sale_sum_all_sale, $nm_member, $comics_buy, $prev_nm_member, $all_sale_cnt_all_sale){

	global $nm_config;

	// 회원정보 포인트 처리 후 재로드을 위해서...
	$nm_member = mb_get($nm_member['mb_id']);

	// 코믹스 가격
	$cm_pay  = intval($all_sale_sum_all_sale);
	
	// 구매 가격
	$mb_point_use = intval($prev_nm_member['mb_point'])-intval($nm_member['mb_point']);
	$mb_cash_point_use = intval($prev_nm_member['mb_cash_point'])-intval($nm_member['mb_cash_point']);
	
	$comics_buy['mb_cash_point'] = $mb_cash_point_use;
	$comics_buy['mb_point'] = $mb_point_use;

	/* 연령 */
	$mb_age_group = mb_get_age_group($nm_member['mb_birth']);
	$comics_buy['mb_age_group'] = $mb_age_group;

	/* 결제환경 */
	$cs_brow = get_brow(HTTP_USER_AGENT);
	$cs_os = get_os(HTTP_USER_AGENT);

	// 저장
	$sql_mpec_insert = "
	INSERT INTO event_thanksgiving_all_sale (
	mpec_member,  mpec_comics, 

	mpec_cash_point, mpec_point, 

	mpec_state,  mpec_age, mpec_sex, 

	mpec_os, mpec_brow, mpec_user_agent, mpec_version, 

	mpec_year_month, mpec_year, mpec_month, mpec_day, mpec_hour, mpec_week, mpec_date 
	) VALUES (
	'".$nm_member['mb_no']."',  '".$comics['cm_no']."', 

	'".$mb_cash_point_use."', '".$mb_point_use."', 

	'1', '".$mb_age_group."', '".$nm_member['mb_sex']."', 

	'".$cs_os."', '".$cs_brow."', '".HTTP_USER_AGENT."', '".NM_VERSION."', 

	'".NM_TIME_YM."', '".NM_TIME_Y."', '".NM_TIME_M."', '".NM_TIME_D."', '".NM_TIME_H."', '".NM_TIME_W."', '".NM_TIME_YMDHIS."' 
	)";
	sql_query($sql_mpec_insert);
}

?>