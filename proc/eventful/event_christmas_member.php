<?php
include_once '_common.php'; // 공통


$ecm_name = tag_get_filter($_REQUEST['name']);
$ecm_tel = tag_get_filter($_REQUEST['phone']);
$ecm_address = tag_get_filter($_REQUEST['address']);
$ecm_postal = tag_get_filter($_REQUEST['postal']);

$ecm_win = false;
$is_member_false = array('올바른 방법으로 이용하시기 바랍니다.', NM_URL.'/event_christmas.php');
$ecm_win_false = array('당첨자이시면 1:1문의하시기 바랍니다.[error code:efm_win-false]', NM_URL.'/event_christmas.php');
if($is_member == false){
	if($nm_config['nm_mode'] == NM_PC){ // 팝업창
		pop_close($is_member_false[0], $is_member_false[1]);
		die;
	}else{
		alert($is_member_false[0], $is_member_false[1]);
		die;
	}
}else{
	// 당첨자 정보 가져오기
	$ecm_row = array();
	$eci_no_data = event_christmas_gift_row_get_is_goods($nm_member);

	if(count($eci_no_data) > 0){
		$ecm_win = true;
		$row_ecm_data = event_christmas_member_get($nm_member);
		if(intval($row_ecm_data['ecm_member']) < 1){
			event_christmas_member_set($nm_member);
			$row_ecm_data = event_christmas_member_get($nm_member);
		}		
	}
}

// 당첨자 아니면...
if($ecm_win == false){
	if($nm_config['nm_mode'] == NM_PC){ // 팝업창
		pop_close($is_member_false[0], $is_member_false[1]);
		die;
	}else{
		alert($is_member_false[0], $is_member_false[1]);
		die;
	}
}else{
	// if($row_ecm_data['ecm_tel'] != $ecm_tel || $row_ecm_data['ecm_address'] != $ecm_address || $row_ecm_data['ecm_postal'] != $ecm_postal || $row_ecm_data['ecm_name'] != $ecm_name){
		$sql_ecm_tel = $sql_ecm_address = $sql_ecm_postal = $sql_ecm_name = "";
		if($row_ecm_data['ecm_tel'] != $ecm_tel && $ecm_tel !=''){
			$sql_ecm_tel = "ecm_tel='".$ecm_tel."',";
		}
		if($row_ecm_data['ecm_address'] != $ecm_tel && $ecm_address !=''){
			$sql_ecm_address = "ecm_address='".$ecm_address."',";
		}
		if($row_ecm_data['ecm_postal'] != $ecm_postal && $ecm_postal !=''){
			$sql_ecm_postal = "ecm_postal='".$ecm_postal."',";
		}
		if($row_ecm_data['ecm_name'] != $ecm_name && $ecm_name !=''){
			$sql_ecm_name = "ecm_name='".$ecm_name."',";
		}
		$sql_ecm_set = $sql_ecm_tel.$sql_ecm_address.$sql_ecm_postal.$sql_ecm_name;
		if($sql_ecm_set !=''){
			$sql_update_ecm = "update event_christmas_member 
			set ".$sql_ecm_set." 
			ecm_mod_date = '".NM_TIME_YMDHIS."'
			where 1 AND ecm_member='".$nm_member['mb_no']."' AND ecm_member_idx='".$nm_member['mb_idx']."' ";
			if(sql_query($sql_update_ecm)){ 
			}else{
				alert('당첨자 정보가 저장되지 않았습니다.<br/>관리자에게 1:1문의바랍니다.(errorcode:delivery_update_error)', NM_URL.'/event_christmas.php?type=delivery&eci_no='.$eci_no);
				die;
			}
		}
	// }	
}

if(is_app()==true && is_mobile()){
	alert('배송지 정보가 저장되었습니다.', NM_URL.'/event_christmas.php');
}else{
	if($nm_config['nm_mode'] == NM_PC){
		pop_close('배송지 정보가 저장되었습니다.', NM_URL.'/event_christmas.php');
	}else{
		alert('배송지 정보가 저장되었습니다.', NM_URL.'/event_christmas.php');
	}
}
die;