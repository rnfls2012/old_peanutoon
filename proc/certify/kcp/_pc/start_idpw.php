<? include_once './_common.php'; // 공통
    /* ============================================================================== */
    /* =   PAGE : 인증 요청 PAGE                                                    = */
    /* = -------------------------------------------------------------------------- = */
    /* =   Copyright (c)  2012.01   KCP Inc.   All Rights Reserved.                 = */
    /* ============================================================================== */

    /* ============================================================================== */
    /* =   Hash 데이터 생성 필요 데이터                                             = */
    /* = -------------------------------------------------------------------------- = */
    /* = 사이트코드 ( up_hash 생성시 필요 )                                         = */
    /* = -------------------------------------------------------------------------- = */
    // $site_cd_real   = "A7IIB";
	  /* 18-07-18 피너툰 법인으로 교체 */
	  $site_cd_real   = "A8357";
	  
    $site_cd_test   = "S6186";
	$site_cd = $site_cd_real;
	if($nm_config['cf_certify_kcp_mode'] == 't'){
		$site_cd = $site_cd_test;
	}
	if($nm_config['cf_certify_kcp_test_id'] != '' && $nm_config['cf_certify_kcp_test_id'] == $nm_member['mb_id']){
		$site_cd = $site_cd_test;
	}

    // $site_cd   = "S6186"; // 테스트모드
    // $site_cd   = "A7IIB"; // 실제모드
	// J17032801596
    /* = -------------------------------------------------------------------------- = */

	
	$kcp_cert_bg_css = "kcp_cert_bg_px";

	// 주문번호생성
	$order_no = get_uniqid();
	
	$ct_certify_mode = 'find';
	$ct_param_opt_1 = NM_URL.'/ctlogin.php';
	$ct_ret_url = NM_PROC_URL."/ctmyfind.php";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
        <meta name="viewport" content="user-scalable=yes, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width, target-densitydpi=medium-dpi" >
        <title>*** KCP Online Certification System [PHP Version] ***</title>
		<script type="text/javascript" src="<?=NM_PC_URL?>/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?=NM_PC_URL?>/js/easing.1.3.js"></script>
		<script type="text/javascript" src="<?=NM_URL?>/js/jquery.cookie.js"></script>

		<script type="text/javascript" src="<?=NM_PC_URL?>/js/common.js<?=vs_para();?>"></script>
		
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL?>/css/jquery.modal.css">
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL?>/css/jquery.modal.theme-xenon.css">
		<link rel="stylesheet" type="text/css" href="<?=NM_PC_URL?>/css/jquery.modal.theme-atlant.css">
		<script type="text/javascript" src="<?=NM_PC_URL?>/js/jquery.modal.min.js"></script>
		<script type="text/javascript">

			// 결제창 종료후 인증데이터 리턴 함수
			function auth_data( frm )
			{
				var auth_form     = document.form_auth;
				var nField        = frm.elements.length;
				var response_data = "";
				var res_msg = "";
				var param_opt_1 = document.getElementById("param_opt_1").value;

				// up_hash 검증 
				if( frm.up_hash.value != auth_form.veri_up_hash.value )
				{					
					response_data = "up_hash 변조 위험있음\n";  
					// alertBox(response_data, pop_close);
				}                
				

				//스마트폰 처리
				for ( i = 0; i < nField; i++ )
				{
					if( frm.elements[i].value != "" )
					{
						// response_data += frm.elements[i].name + " : " + frm.elements[i].value + "\n";
						if(frm.elements[i].name == 'res_msg'){res_msg = frm.elements[i].value;}
					}
				}
				
				document.getElementById( "cert_info" ).style.display = "none";
				document.getElementById( "kcp_cert"  ).style.display = "none";
					
				// alertBox(res_msg, pop_close);
			}

			// 인증창 호출 함수
			function auth_type_check()
			{
				var auth_form = document.form_auth;

				
				auth_form.target = "kcp_cert";
				
				document.getElementById( "cert_info" ).style.display = "none";
				document.getElementById( "kcp_cert"  ).style.display = "";

				auth_form.action = "<?=NM_KCP_CR_URL.'/'.NM_PC?>/req_idpw.php"; // 인증창 호출 및 결과값 리턴 페이지 주소
				
				auth_form.submit();
				return true;                
			}

			// IP/PW찾기에서 취소
			function certify_cancel()
			{
				alertBox("취소하셨습니다.", pop_close);
			}

			function pop_close(){
				window.close();
				self.close();
				window.open("", "_self").close();
			}

			function result_idpw(mb_id_arr, action_url){
				var html_arr =new Array();
				var n_mb_id_arr        = mb_id_arr.length;
				for ( i = 0; i < n_mb_id_arr; i++ )
				{
					html_arr.push('<li><input type="radio" class="s_type" name="mb_id" id="mb_id'+i+'" value="'+mb_id_arr[i]+'"><label for="mb_id'+i+'">'+mb_id_arr[i]+'</label></li>');
				}
				if(n_mb_id_arr > 0){
					$("#find_idpw", opener.document).css('display','none');
					$("#result_idpw", opener.document).css('display','block');
					$("#result_idpw form", opener.document).attr('action', action_url);
					$("#mb_id_list", opener.document).html(html_arr.join("\n"));
					alertBox("인증코드가 "+n_mb_id_arr+"개가 있습니다. ", pop_close);
				}
				if(n_mb_id_arr == 0){ alertBox("인증코드가 존재하지 않습니다. 관리자에게 문의 바랍니다.", pop_close); }
			}

			// 로드시 인증창 호출 함수
			window.onload=function(){ auth_type_check(); }

		</script>
    </head>
    <body oncontextmenu="return false;" ondragstart="return false;" onselectstart="return false;">
		<div class="certify_box">
			<div align="center" id="cert_info">
				<form name="form_auth" method="post">
					<!-- 요청종류 -->
					<input type="hidden" name="ordr_idxx" value="<?=$order_no?>"/>

					<input type="hidden" name="req_tx"       value="cert"/>
					<!-- 요청구분 -->
					<input type="hidden" name="cert_method"  value="01"/>
					<!-- 웹사이트아이디 -->
					<input type="hidden" name="web_siteid"   value="J17032801596"/> 
					<!-- 노출 통신사 default 처리시 아래의 주석을 해제하고 사용하십시요 
						 SKT : SKT , KT : KTF , LGU+ : LGT
					<input type="hidden" name="fix_commid"      value="KTF"/>
					-->
					<!-- 사이트코드 -->
					<input type="hidden" name="site_cd"      value="<?= $site_cd ?>" />               
					<!-- Ret_URL : 인증결과 리턴 페이지 ( 가맹점 URL 로 설정해 주셔야 합니다. ) -->
					<input type="hidden" name="Ret_URL"      value="<?=$ct_ret_url?>" />
					<!-- cert_otp_use 필수 ( 메뉴얼 참고)
						 Y : 실명 확인 + OTP 점유 확인 , N : 실명 확인 only
					-->
					<input type="hidden" name="cert_otp_use" value="Y"/>
					<!-- cert_enc_use 필수 (고정값 : 메뉴얼 참고) -->
					<input type="hidden" name="cert_enc_use" value="Y"/>

					<!-- cert_able_yn input 비활성화 설정 -->
					<input type="hidden" name="cert_able_yn" value=""/>

					<input type="hidden" name="res_cd"       value=""/>
					<input type="hidden" name="res_msg"      value=""/>

					<!-- up_hash 검증 을 위한 필드 -->
					<input type="hidden" name="veri_up_hash" value=""/>

					<!-- web_siteid 을 위한 필드 -->
					<input type="hidden" name="web_siteid_hashYN" value="Y"/>

					<!-- 가맹점 사용 필드 (인증완료시 리턴)-->
					<input type="hidden" id="param_opt_1" name="param_opt_1"  value="<?=$ct_param_opt_1;?>"/> 
					<input type="hidden" id="param_opt_2"  name="param_opt_2"  value="opt2"/> 
					<input type="hidden" id="param_opt_3"  name="param_opt_3"  value="opt3"/> 
				</form>	
			</div>
			<div class="kcp_cert_bg <?=$kcp_cert_bg_css?>">
				<iframe id="kcp_cert" name="kcp_cert" width="100%" height="700" frameborder="0" scrolling="no" style="display:none"></iframe>
			</div>
		</div>
    </body>
</html>