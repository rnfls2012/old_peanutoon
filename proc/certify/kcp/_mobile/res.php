<? include_once './_common.php'; // 공통
    /* ============================================================================== */
    /* =   인증데이터 수신 및 복호화 페이지                                         = */
    /* = -------------------------------------------------------------------------- = */
    /* =   해당 페이지는 반드시 가맹점 서버에 업로드 되어야 하며                    = */ 
    /* =   가급적 수정없이 사용하시기 바랍니다.                                     = */
    /* ============================================================================== */

    /* ============================================================================== */
    /* =   라이브러리 파일 Include                                                  = */
    /* = -------------------------------------------------------------------------- = */

    require NM_KCP_CR_PATH."/lib/ct_cli_lib.php";

    /* = -------------------------------------------------------------------------- = */
    /* =   라이브러리 파일 Include END                                               = */
    /* ============================================================================== */

    /* ============================================================================== */
    /* =   null 값을 처리하는 메소드                                                = */
    /* = -------------------------------------------------------------------------- = */
    function f_get_parm_str( $val )
    {
        if ( $val == null ) $val = "";
        if ( $val == ""   ) $val = "";
        return  $val;
    }
    /* ============================================================================== */
    $home_dir	   = NM_KCP_CR_PATH; // ct_cll 절대경로 ( bin 전까지 )

    $site_cd       = "";
    $ordr_idxx     = "";
    
    $cert_no       = "";
    $cert_enc_use  = "";
    $enc_info      = "";
    $enc_data      = "";
    $req_tx        = "";
    
    $enc_cert_data = "";
    $cert_info     = "";

    $tran_cd       = "";
    $res_cd        = "";
    $res_msg       = "";

    $dn_hash       = "";
	/*------------------------------------------------------------------------*/
    /*  :: 전체 파라미터 남기기                                               */
    /*------------------------------------------------------------------------*/

    // request 로 넘어온 값 처리
    foreach($_POST as $nmParam => $valParam)
    {

        if ( $nmParam == "site_cd" )
        {
            $site_cd = f_get_parm_str ( $valParam );
        }

        if ( $nmParam == "ordr_idxx" )
        {
            $ordr_idxx = f_get_parm_str ( $valParam );
        }

        if ( $nmParam == "res_cd" )
        {
            $res_cd = f_get_parm_str ( $valParam );
        }

        if ( $nmParam == "cert_enc_use" )
        {
            $cert_enc_use = f_get_parm_str ( $valParam );
        }

        if ( $nmParam == "req_tx" )
        {
            $req_tx = f_get_parm_str ( $valParam );
        }

        if ( $nmParam == "cert_no" )
        {
            $cert_no = f_get_parm_str ( $valParam );
        }

        if ( $nmParam == "enc_cert_data" )
        {
            $enc_cert_data = f_get_parm_str ( $valParam );
        }

        if ( $nmParam == "dn_hash" )
        {
            $dn_hash = f_get_parm_str ( $valParam );
       }

        // 부모창으로 넘기는 form 데이터 생성 필드
		$f_get_parm_str = f_get_parm_str( $valParam );
		// 메세지 문자셋 UTF-8 반환
		if($nmParam == 'res_msg'){ $f_get_parm_str = iconv("euc-kr", "UTF-8", $f_get_parm_str); }
        $sbParam .= "<input type='hidden' name='" . $nmParam . "' value='" . $f_get_parm_str . "'/>";
    }
    

    $ct_cert = new C_CT_CLI;
    $ct_cert->mf_clear();

    // 결과 처리
    if( $cert_enc_use == "Y" )
    {
        if( $res_cd == "0000" )
        {

            // dn_hash 검증
            // KCP 가 리턴해 드리는 dn_hash 와 사이트 코드, 주문번호 , 인증번호를 검증하여
            // 해당 데이터의 위변조를 방지합니다
             $veri_str = $site_cd.$ordr_idxx.$cert_no; // 사이트 코드 + 주문번호 + 인증거래번호

            if ( $ct_cert->check_valid_hash ( $home_dir , $dn_hash , $veri_str ) != "1" )
            {
                echo "dn_hash 변조 위험있음";
                // 오류 처리 ( dn_hash 변조 위험있음)
            }

            // 가맹점 DB 처리 페이지 영역
            // echo "========================= 리턴 데이터 ======================="       ."<br>";
            // echo "사이트 코드            :" . $site_cd                                 ."<br>";
            // echo "인증 번호              :" . $cert_no                                 ."<br>";
            // echo "암호된 인증정보        :" . $enc_cert_data                           ."<br>";

            // 인증데이터 복호화 함수
            // 해당 함수는 암호화된 enc_cert_data 를
            // site_cd 와 cert_no 를 가지고 복화화 하는 함수 입니다.
            // 정상적으로 복호화 된경우에만 인증데이터를 가져올수 있습니다.                   
            $opt = "1" ; // 복호화 인코딩 옵션 ( UTF - 8 사용시 "1" ) 
            $ct_cert->decrypt_enc_cert( $home_dir , $site_cd , $cert_no , $enc_cert_data , $opt );
			/*
            
            echo "========================= 복호화 데이터 ====================="       ."<br>";
            echo "복호화 이동통신사 코드 :" . $ct_cert->mf_get_key_value("comm_id"    )."<br>"; // 이동통신사 코드   
            echo "복호화 전화번호        :" . $ct_cert->mf_get_key_value("phone_no"   )."<br>"; // 전화번호          
            echo "복호화 이름            :" . $ct_cert->mf_get_key_value("user_name"  )."<br>"; // 이름              
            echo "복호화 생년월일        :" . $ct_cert->mf_get_key_value("birth_day"  )."<br>"; // 생년월일          
            echo "복호화 성별코드        :" . $ct_cert->mf_get_key_value("sex_code"   )."<br>"; // 성별코드          
            echo "복호화 내/외국인 정보  :" . $ct_cert->mf_get_key_value("local_code" )."<br>"; // 내/외국인 정보    
            echo "복호화 CI              :" . $ct_cert->mf_get_key_value("ci_url"     )."<br>"; // CI                
            echo "복호화 DI              :" . $ct_cert->mf_get_key_value("di_url"     )."<br>"; // DI 중복가입 확인값
            echo "복호화 WEB_SITEID      :" . $ct_cert->mf_get_key_value("web_siteid" )."<br>"; // WEB_SITEID
            echo "복호화 결과코드        :" . $ct_cert->mf_get_key_value("res_cd"     )."<br>"; // 암호화된 결과코드
            echo "복호화 결과메시지      :" . $ct_cert->mf_get_key_value("res_msg"    )."<br>"; // 암호화된 결과메시지
	
            
            echo "========================= 복호화 데이터 변수저장 ====================="       ."<br>";
			*/
            $comm_id	= $ct_cert->mf_get_key_value("comm_id"		);			// 이동통신사 코드   
            $phone_no	= $ct_cert->mf_get_key_value("phone_no"		);			// 전화번호 
            $user_name	= $ct_cert->mf_get_key_value("user_name"    );			// 이름 
            $birth_day	= $ct_cert->mf_get_key_value("birth_day"    );			// 생년월일  
            $sex_code	= $ct_cert->mf_get_key_value("sex_code"		);			// 성별코드   
            $local_code = $ct_cert->mf_get_key_value("local_code"   );			// 내/외국인 정보  
            // $ci_url		= $ct_cert->mf_get_key_value("ci_url"		);		// CI   
            // $di_url		= $ct_cert->mf_get_key_value("di_url"		);		// DI 중복가입 확인값   
            $ci			= $ct_cert->mf_get_key_value("ci"		);				// CI 연계정보 값  
			$ci_idx		= mb_get_idx($ci);										// CI 연계정보 값 index
            $di			= $ct_cert->mf_get_key_value("di"		);				// DI 중복가입 확인값   
			$di_idx		= mb_get_idx($di);										// DI 중복가입 값 index
            $web_siteid = $ct_cert->mf_get_key_value("web_siteid"   );			// WEB_SITEID   

            $get_res_cd		= $ct_cert->mf_get_key_value("res_cd"		);		// 암호화된 결과코드   
            $get_res_msg	= $ct_cert->mf_get_key_value("res_msg"		);		//  암호화된 결과메시지  

			// echo "========================= 인증 이벤트 검색 ====================="."<br>";
			$sql_certify_event = "SELECT * FROM event_recharge WHERE er_type='2' AND er_state='y' ORDER BY er_no DESC LIMIT 0, 1 ";
			$row_certify_event = sql_fetch($sql_certify_event);

			// echo "========================= 복호화 데이터 DB 저장 ====================="."<br>";
			$sql_certify_insert = " INSERT INTO pg_certify_kcp ( 
			kcp_member, kcp_member_id, kcp_member_idx, 
			kcp_product, kcp_er_no, 
			kcp_event_cash_point, kcp_event_point, 
			kcp_payway, kcp_order, kcp_date, kcp_useragent, 

			res_cd, res_msg, cert_no,  
			phone_no, comm_id, user_name, birth_day, sex_code, local_code, 
			ci, ci_idx, di, di_idx, web_siteid )VALUES( 
			'".$nm_member['mb_no']."', '".$nm_member['mb_id']."', '".$nm_member['mb_idx']."',
			'".$nm_config['cf_er_type'][$row_certify_event['er_type']]."', '".intval($row_certify_event['er_no'])."',
			'".intval($row_certify_event['er_cash_point'])."', '".intval($row_certify_event['er_point'])."', 
			'mobx', '".$ordr_idxx."', '".NM_TIME_YMDHIS."', '".HTTP_USER_AGENT."', 

			'".$get_res_cd."', '".$get_res_msg."', '".$cert_no."', 
			'".$phone_no."', '".$comm_id."', '".$user_name."', '".$birth_day."', '".$sex_code."', '".$local_code."', 
			'".$ci."', '".$ci_idx."','".$di."', '".$di_idx."', '".$web_siteid."' ) ";
			if(sql_query($sql_certify_insert)){
			}else{
				// 실패 로그 남기기
				// cs_error_log(NM_KCP_CR_PATH."/log/".NM_MO."/".NM_TIME_YMD.".log", $sql_certify_insert, $_SERVER['PHP_SELF']);
				cs_x_error_log("x_pg_certify_kcp", "kcp", NM_KCP_CR_PATH."/".NM_MO, "sql_certify_insert", "", $sql_certify_insert, 1, "복호화 데이터 DB 저장 실패"); // state-1
				cs_alert("관리자에게 문의하시기 바랍니다: error code : insert into not - 0",NM_URL."/ctcertify.php");
				die;
			}
			// echo "========================= 회원 이벤트 캐쉬 업데이트 ====================="."<br>";
			if(intval($row_certify_event['er_no']) > 0){
				$boolean_mpie = cs_set_member_point_income_event($nm_member, $row_certify_event['er_cash_point'], $row_certify_event['er_point'], $row_certify_event['er_type'], $nm_config['cf_er_type'][$row_certify_event['er_type']]);
				if($boolean_mpie == false){ 
					//실패 로그 남기기 
					// cs_error_log(NM_KCP_CR_PATH."/log/".NM_MO."/".NM_TIME_YMD.".log", "function cs_set_member_point_income_event error", $_SERVER['PHP_SELF']);
					cs_x_error_log("x_pg_certify_kcp", "kcp", NM_KCP_CR_PATH."/".NM_MO, "", "cs_set_member_point_income_event", "", 2, "회원 이벤트 캐쉬 업데이트 실패"); // state-2
				}
			}
			
			// echo "========================= 회원정보 업데이트 ====================="."<br>";
			$mb_name = $user_name;
			$mb_birth = substr($birth_day, 0,4)."-".substr($birth_day, 4,2)."-".substr($birth_day, 6,2);
			$mb_sex = 'n';
			if($sex_code == '01'){ $mb_sex = 'm'; }
			if($sex_code == '02'){ $mb_sex = 'w'; }
			$mb_adult = 'n';
			$mb_birth_age = intval(NM_TIME_Y) - intval(substr($birth_day, 0,4));
			if($mb_birth_age >= NM_ADULT){
				$mb_adult = 'y';
			}
			$sbParam .= "<input type='hidden' name='adult' value='".$mb_adult."'/>"; // 인증시 성인이면 성인페이지로 가게 설정 171110
			
			// 실제모드
			$sql_update_member = "update member set 
								  mb_name		='".$mb_name."', 
								  mb_birth		='".$mb_birth."', 
								  mb_sex		='".$mb_sex."', 
								  mb_phone		='".$phone_no."', 
								  mb_adult		='".$mb_adult."', 
								  mb_ipin		='".$ci."', 
								  mb_ipin_idx	='".$ci_idx."' 
 
			where mb_no='".$nm_member['mb_no']."' and mb_id='".$nm_member['mb_id']."'";
			
			// 테스트모드
			$sql_update_member_test = "update member set 
									   mb_name		='".$mb_name."', 
									   mb_birth		='".$mb_birth."', 
									   mb_sex		='".$mb_sex."', 
									   mb_phone		='".$phone_no."', 
									   mb_adult		='".$mb_adult."' 
 
			where mb_no='".$nm_member['mb_no']."' and mb_id='".$nm_member['mb_id']."'";
			
			if($nm_config['cf_certify_kcp_mode'] == 't'){
				$sql_update_member = $sql_update_member_test;
			}
			if($nm_config['cf_certify_kcp_test_id'] == $nm_member['mb_id']){
				$sql_update_member = $sql_update_member_test;
			}

			if(sql_query($sql_update_member)){
			}else{
				// 실패 로그 남기기
				// cs_error_log(NM_KCP_CR_PATH."/log/".NM_MO."/".NM_TIME_YMD.".log", $sql_update_member, $_SERVER['PHP_SELF']);				
				cs_x_error_log("x_pg_certify_kcp", "kcp", NM_KCP_CR_PATH."/".NM_MO, "sql_update_member", "", $sql_update_member, 3, "회원정보 업데이트 실패"); // state-3
				cs_alert("관리자에게 문의하시기 바랍니다: error code : update not - 0",NM_URL."/ctcertify.php");
				die;
			}
			
					
        }
        else/*if( res_cd.equals( "0000" ) != true )*/
        {
           // 인증실패
        }
    }
    else/*if( cert_enc_use.equals( "Y" ) != true )*/
    {
        // 암호화 인증 안함
    }

    $ct_cert->mf_clear();
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>*** KCP Online Payment System [PHP Version] ***</title>
        <script type="text/javascript">
            window.onload=function()
            {
                try
                {
                    parent.auth_data( document.form_auth );// 부모창으로 값 전달
                }
                catch(e)
                {
                    alert(e); // 정상적인 부모창의 iframe 를 못찾은 경우임
                }
            }
        </script>
    </head>
    <body oncontextmenu="return false;" ondragstart="return false;" onselectstart="return false;">
        <form name="form_auth" method="post">
            <?= $sbParam ?>
        </form>
    </body>
</html>
