<? if (!defined("_CERTIFY_")) exit; // 개별 페이지 접근 불가
include_once './_common.php'; // 공통
    /* ============================================================================== */
    /* =   PAGE : 인증 요청 PAGE                                                    = */
    /* = -------------------------------------------------------------------------- = */
    /* =   Copyright (c)  2012.01   KCP Inc.   All Rights Reserved.                 = */
    /* ============================================================================== */

    /* ============================================================================== */
    /* =   Hash 데이터 생성 필요 데이터                                             = */
    /* = -------------------------------------------------------------------------- = */
    /* = 사이트코드 ( up_hash 생성시 필요 )                                         = */
    /* = -------------------------------------------------------------------------- = */
    // $site_cd_real   = "A7IIB";
	  /* 18-07-18 피너툰 법인으로 교체 */
	  $site_cd_real   = "A8357";
	  
    $site_cd_test   = "S6186";
	$site_cd = $site_cd_real;
	if($nm_config['cf_certify_kcp_mode'] == 't'){
		$site_cd = $site_cd_test;
	}
	if($nm_config['cf_certify_kcp_test_id'] != '' && $nm_config['cf_certify_kcp_test_id'] == $nm_member['mb_id']){
		$site_cd = $site_cd_test;
	}
    // $site_cd   = "S6186"; // 테스트모드
    // $site_cd   = "A7IIB"; // 실제모드
	// J17032801596
    /* = -------------------------------------------------------------------------- = */

	
	$kcp_cert_bg_css = "kcp_cert_bg_px";
	if(is_mobile()){
		$kcp_cert_bg_css = "kcp_cert_bg_mobile";
	}
?>

<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/ct.css<?=vs_para();?>" />
<script type="text/javascript" src="<?=NM_MO_URL;?>/js/ct.js<?=vs_para();?>"></script>
<!-- body space -->
	<!-- wrapper space -->
		<!-- container_bg -->
			<div class="certify_box">

				<script type="text/javascript">

					// 결제창 종료후 인증데이터 리턴 함수
					function auth_data( frm )
					{
						var auth_form     = document.form_auth;
						var nField        = frm.elements.length;
						var response_data = "";
						var res_msg = "";
						var param_opt_1 = document.getElementById("param_opt_1").value;
						var adult = ""; // 인증시 성인이면 성인페이지로 가게 설정 171110

						// up_hash 검증 
						if( frm.up_hash.value != auth_form.veri_up_hash.value )
						{					
							response_data = "up_hash 변조 위험있음\n";  
							alert(response_data);
						}                
						

						//스마트폰 처리
						for ( i = 0; i < nField; i++ )
						{
							if( frm.elements[i].value != "" )
							{
								// response_data += frm.elements[i].name + " : " + frm.elements[i].value + "\n";
								if(frm.elements[i].name == 'res_msg'){res_msg = frm.elements[i].value;}
								if(frm.elements[i].name == 'adult'){adult = frm.elements[i].value;} // 인증시 성인이면 성인페이지로 가게 설정 171110
							}
						}
						
						<?if($nm_config['nm_mode'] == NM_MO){ /* js의 navigator.userAgent로 제어 하는걸 php로 교체함 */?>
							// MOBILE
							document.getElementById( "cert_info" ).style.display = "none";
							document.getElementById( "kcp_cert"  ).style.display = "none";
							if(adult == 'y') { 
								param_opt_1 += '<?=$attach?>adult=y'; 
							} // 인증시 성인이면 성인페이지로 가게 설정 171110
						<?}?>
							
						alertBox(res_msg, goto_url, {url:param_opt_1});
					}

					// 인증창 호출 함수
					function auth_type_check()
					{
						var auth_form = document.form_auth;

						<?if($nm_config['nm_mode'] == NM_MO){ /* js의 navigator.userAgent로 제어 하는걸 php로 교체함 */?>
							// MOBILE
							auth_form.target = "kcp_cert";
							
							document.getElementById( "cert_info" ).style.display = "none";
							document.getElementById( "kcp_cert"  ).style.display = "";
						<?}else{?>
							// PC
							var return_gubun;
							var width  = 410;
							var height = 500;

							var leftpos = screen.width  / 2 - ( width  / 2 );
							var toppos  = screen.height / 2 - ( height / 2 );

							var winopts  = "width=" + width   + ", height=" + height + ", toolbar=no,status=no,statusbar=no,menubar=no,scrollbars=no,resizable=no";
							var position = ",left=" + leftpos + ", top="    + toppos;
							var AUTH_POP = window.open('','auth_popup', winopts + position);
							
							auth_form.target = "auth_popup";
						<?}?>

						auth_form.action = "<?=NM_KCP_CR_URL.'/'.$nm_config['nm_mode']?>/req.php"; // 인증창 호출 및 결과값 리턴 페이지 주소
						
						auth_form.submit();
						return true;                
					}
					// 19세 이하라 뒤로가기
					function teenager()
					{
						link('<?=NM_URL?>');
					}

					// IP/PW찾기에서 취소
					function certify_cancel()
					{
						alertBox("취소하셨습니다.", goto_url, {url:'<?=NM_URL?>/ctmyfind.php'});
					}

				</script>

				<div align="center" id="cert_info">
					<form name="form_auth" method="post">
						<?if($ct_certify_mode == '19'){?>
						<div class="adult_alart">
							<div class="adult_mark"><img src="<?=NM_MO_IMG?>ct/teenager.png" alt="19세" /></div>
							<div class="adult_warn">
								<?=stripslashes(nl2br($row['clt_ipin']));?>
							</div>
							<div class="adult_confirm">
								<a href="#adult" onclick="return auth_type_check();">본인인증하기</a>
							</div>
							<div class="adult_out">
								<a href="#teenager" onclick="return teenager();">취소</a>
							</div>
						</div>
						<?}?>
						<?if($ct_certify_mode == 'find'){?>
						<div class="find_idpw">
							<h2>ID/패스워드 찾기</h2>
							<div class="idpw_btnwrap idpw_head">
								<div class="idpw_kcp">
									<a href="#adult" onclick="return auth_type_check();">
										<i class="fa fa-mobile" aria-hidden="true"></i><br />본인인증
									</a>
								</div>
								<div class="idpw_email">
									<a href="#" id="find_email_idpw" class="idpw_e">
										<i class="fa fa-envelope-o" aria-hidden="true"></i><br />이메일
									</a>
								</div>
							</div>
						</div>
						<?}?>
						<!-- 요청종류 -->
						<input type="hidden" name="ordr_idxx" value="<?=$order_no?>"/>

						<input type="hidden" name="req_tx"       value="cert"/>
						<!-- 요청구분 -->
						<input type="hidden" name="cert_method"  value="01"/>
						<!-- 웹사이트아이디 -->
						<input type="hidden" name="web_siteid"   value="J17032801596"/> 
						<!-- 노출 통신사 default 처리시 아래의 주석을 해제하고 사용하십시요 
							 SKT : SKT , KT : KTF , LGU+ : LGT
						<input type="hidden" name="fix_commid"      value="KTF"/>
						-->
						<!-- 사이트코드 -->
						<input type="hidden" name="site_cd"      value="<?= $site_cd ?>" />               
						<!-- Ret_URL : 인증결과 리턴 페이지 ( 가맹점 URL 로 설정해 주셔야 합니다. ) -->
						<input type="hidden" name="Ret_URL"      value="<?=$ct_ret_url?>" />
						<!-- cert_otp_use 필수 ( 메뉴얼 참고)
							 Y : 실명 확인 + OTP 점유 확인 , N : 실명 확인 only
						-->
						<input type="hidden" name="cert_otp_use" value="Y"/>
						<!-- cert_enc_use 필수 (고정값 : 메뉴얼 참고) -->
						<input type="hidden" name="cert_enc_use" value="Y"/>

						<!-- cert_able_yn input 비활성화 설정 -->
						<input type="hidden" name="cert_able_yn" value=""/>

						<input type="hidden" name="res_cd"       value=""/>
						<input type="hidden" name="res_msg"      value=""/>

						<!-- up_hash 검증 을 위한 필드 -->
						<input type="hidden" name="veri_up_hash" value=""/>

						<!-- web_siteid 을 위한 필드 -->
						<input type="hidden" name="web_siteid_hashYN" value="Y"/>

						<!-- 가맹점 사용 필드 (인증완료시 리턴)-->
						<input type="hidden" id="param_opt_1" name="param_opt_1"  value="<?=$ct_param_opt_1;?>"/> 
						<input type="hidden" id="param_opt_2"  name="param_opt_2"  value="<?=$nm_member['mb_id']?>"/> 
						<input type="hidden" id="param_opt_3"  name="param_opt_3"  value="<?=$order_no?>"/> 
					</form>					
					<?if($ct_certify_mode == 'find'){?>
						<div class="find_idpw">
							<h3 class="hide">이메일인증</h3>
							<div class="idpw_btnwrap">
								<div id="find_email_idpw_txt" class="idpw_emailtxt">
									<form name="idpw_mail_form" id="idpw_mail_form" method="post" action="<?=NM_PROC_URL?>/ctmyfindmail.php" onsubmit="return idpw_mail_submit();">
										<input type="text" id="idpw_mail" name="idpw_mail" placeholder="이메일 주소를 입력하세요" value="" autocomplete="off" />
										<input type="submit" id="idpw_mail_ok" value="인증메일 보내기" />
									</form>
								</div>
								<div class="idpw_exp">
									<ol>
										<?if($row['clt_mb_find1']!=''){?><li><?=stripslashes(nl2br($row['clt_mb_find1']));?></li><?}?>
										<?if($row['clt_mb_find2']!=''){?><li><?=stripslashes(nl2br($row['clt_mb_find2']));?></li><?}?>
										<?if($row['clt_mb_find3']!=''){?><li><?=stripslashes(nl2br($row['clt_mb_find3']));?></li><?}?>
										<?if($row['clt_mb_find4']!=''){?><li><?=stripslashes(nl2br($row['clt_mb_find4']));?></li><?}?>
										<?if($row['clt_mb_find5']!=''){?><li><?=stripslashes(nl2br($row['clt_mb_find5']));?></li><?}?>
									</ol>
								</div>
							</div>
						</div>
					<?}?>
				</div>
				<div class="kcp_cert_bg <?=$kcp_cert_bg_css?>">
					<iframe id="kcp_cert" name="kcp_cert" width="100%" height="700" frameborder="0" scrolling="no" style="display:none"></iframe>
				</div>
				
				<div id="result_idpw" class="result_idpw">
					<h2>ID/패스워드 찾기</h2>
					<form name="idpw_form" id="idpw_form" method="post" onsubmit="return idpw_submit();">
						<table>
							<tr>
								<th>아이디</th>
								<td>
									<ul id="mb_id_list">
										<li>없음</li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>비밀번호</th>
								<td class="pw_input">
									<label for="mb_pw">변경 비밀번호</label> <input type="password" name="mb_pw" />
									<label for="mb_pw_check">변경 비밀번호 확인</label> <input type="password" name="mb_pw_check" />
									<input type="submit" value="확인">
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>