<?
include_once '_common.php'; // 공통

// header('Content-Type: application/json; charset=UTF-8');

// $_getinput = file_get_contents("php://input"); // 본문을 불러옴
// $__getData = array(json_decode($__rawBody)); // 데이터를 변수에 넣고

/*
처리부
*/

// echo json_encode(array('result_code' => '200', 'result'=>$__getData));


/*
userKey=2
&enc_key=zZohsqqTqqCddxk%2B0XlNkr6GLo2%2FmUCJJhQhBo6zLhjntUvEWjBl0wuYAKRK%2Br7MrwEvNVV4RPws81gCzCRrLQ%3D%3D
&code=0
&dealNo=2017091813550906
&totalPoint=1900
&tradeNo=170900005328
&timestamp=20170918135430592
&message=
&coin=10
*/
// 포인트파크 응답
$response_pointpark['code'] = 0;
$response_pointpark['massage'] = "";

$userkey = num_check(tag_filter($_REQUEST['userKey']));
$enc_key = Security::decrypt_aes256(rawurldecode($_REQUEST['enc_key']), NM_POINTPARK_ENC_KEY);
$code = num_check(tag_filter($_REQUEST['code']));
$dealno = num_check(tag_filter($_REQUEST['dealNo']));
$totalpoint = num_check(tag_filter($_REQUEST['totalPoint']));
$tradeno = num_check(tag_filter($_REQUEST['tradeNo']));
$timestamp = num_check(tag_filter($_REQUEST['timestamp']));
$message = tag_filter($_REQUEST['message']);
$coin = num_check(tag_filter($_REQUEST['coin']));
$pointpark_order = $dealno;
$pointpark_amt = $totalpoint;

// 결제승인저장
$pointpark_member = mb_get_no($userkey);
$sql_pointpark_update_complete = " UPDATE pg_channel_pointpark SET  
userkey = '$userkey', 
enc_key = '$enc_key', 
code = '$code', 
dealno = '$dealno', 
totalPoint = '$totalpoint', 
tradeno = '$tradeno', 
userkey = '$userkey', 
timestamp = '$timestamp', 
message = '$message', 
coin = '$coin', 
pointpark_cash_point = '$coin',
pointpark_amt= '$pointpark_amt' 
WHERE pointpark_member = '".$pointpark_member['mb_no']."' AND pointpark_member_id = '".$pointpark_member['mb_id']."' AND pointpark_member_idx = '".$pointpark_member['mb_idx']."' AND pointpark_order = '".$pointpark_order."' ";

if(sql_query($sql_pointpark_update_complete)){
	// payco 저장 정보 가져오기
	$row_pointpark = cs_pg_channel_pointpark($pointpark_member, $pointpark_order);
	$mpip_pay_point = intval($row_pointpark['totalpoint']);
	$mpip_cash_point = intval($row_pointpark['pointpark_cash_point']);
	$mpip_point = 0;
	$mpip_from = "pointpark[".number_format($mpip_pay_point)."포인트결제]";

	// 새로고침시 또 부여되어서 검사
	$row_mpip = cs_member_point_income_pointpark($pointpark_member, $pointpark_order);
	// if(count($row_mpi) > 1){
	//   $bSucc = true;
	// }else{
		// 회원 포인트 부여
		$bSucc = $boolean_mpip = cs_set_member_point_income_pointpark($pointpark_member, $mpip_cash_point, $mpip_point, $mpip_pay_point, $mpip_from, $orderNo);
		if($boolean_mpip == false){ 
			//실패 로그 남기기
			$response_pointpark['code'] = 1;
			$response_pointpark['massage'] = "member_point_income false";
			// cs_error_log(NM_TOSS_PATH."/log/".$nm_config['nm_mode']."/".NM_TIME_YMD.".log", "function cs_set_member_point_income error", $_SERVER['PHP_SELF']);
			cs_x_error_log("x_pg_channel_pointpark", "pointpark", NM_PROC_PATH, "", "cs_set_member_point_income_pointpark", "", $response_pointpark['code'], "회원 포인트 부여 실패", $pointpark_member); // state-3
			
		}

		// 회원 포인트 통계
		$bSucc = $boolean_sr = cs_set_sales_recharge_pointpark($pointpark_member, $row_pointpark, 'pointpark');
		if($boolean_sr == false){ 
			//실패 로그 남기기 
			$response_pointpark['code'] = 2;
			$response_pointpark['massage'] = "sales_recharge false";
			// cs_error_log(NM_TOSS_PATH."/log/".$nm_config['nm_mode']."/".NM_TIME_YMD.".log", "function cs_set_sales_recharge error", $_SERVER['PHP_SELF']);
			cs_x_error_log("x_pg_channel_pointpark", "pointpark", NM_PROC_PATH, "", "cs_set_sales_recharge_pointpark", "", $response_pointpark['code'], "회원 포인트 통계 실패", $pointpark_member); // state-5
		}

	// }
}else{
	$response_pointpark['code'] = 3;
	$response_pointpark['massage'] = "member_pointpark_parameter_save false";
	cs_x_error_log("x_pg_channel_pointpark", "pointpark", NM_PROC_PATH, "sql_pointpark_update_complete", "", $sql_pointpark_update_complete, $response_pointpark['code'], "결제승인요청저장 실패", $pointpark_member); // state-10
}

header("content-type: application/json; charset=utf-8");
echo json_encode($response_pointpark);

?>