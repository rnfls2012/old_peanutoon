<? if (!defined("_RECHARGE_")) exit; // 개별 페이지 접근 불가
include_once './_common.php'; // 공통
?>
<?
    include_once NM_TOSS_PATH.'/cfg/site_conf_inc.php';      // 환경설정 파일 include

$arrayBody = array();
$arrayBody["apiKey"] = $apikey; // toss key 환경설정 파일 include
$arrayBody["payToken"] = $payToken; // 결제 고유 번호 (필수)
$arrayBody["amount"] = $amount; // 결제 총금액 
$arrayBody["amountTaxFree"] = $amountTaxFree; // 결제 금액 중 비과세금액 (복합과세) 

$jsonBody = json_encode($arrayBody);

$ch = curl_init('https://pay.toss.im/tosspay/api/v1/refunds');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
'Content-Type: application/json',
'Content-Length: ' . strlen($jsonBody))
);

$cancel_result = curl_exec($ch);
curl_close($ch);


$toss_cancel_result = json_decode(stripslashes($cancel_result),true);

// 200 정상
/*
  Array ( [result] => 0 [msg] => 이미 환불 되었습니다. [code] => -1 [errorCode] => COMMON_REFUND_ERROR [status] => 200 )
*/
$db_result['state'] = 'cancel-'.$toss_cancel_result['status'];
$db_result['msg']   = '취소요청-'.$toss_cancel_result['msg'];


?>