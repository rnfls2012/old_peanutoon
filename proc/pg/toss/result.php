<? if (!defined("_RECHARGE_")) exit; // 개별 페이지 접근 불가
	include_once './_common.php'; // 공통
?>
<?
    /* ============================================================================== */
    /* =   환경 설정 파일 Include                                                   = */
    /* = -------------------------------------------------------------------------- = */
    /* =   ※ 필수                                                                  = */
    include NM_TOSS_PATH.'/cfg/site_conf_inc.php';      // 환경설정 파일 include
?>
<?
	/* 토즈
	http://tossdev.github.io/api.html#state
	payway=toss&status=PAY_COMPLETE&orderNo=2017032815322481

	PARAMETER
	status	: PAY_SUCCESS, PAY_CANCEL, REFUND_SUCCESS 중 하나
	orderNo : Toss 결제와 이어진 주문번호
	*/

	$orderNo = tag_filter($_REQUEST['orderNo']);
	$status = tag_filter($_REQUEST['status']);
		
	$row_toss = cs_pg_channel_toss($nm_member, $orderNo);	
			
	$arrayBody = array();
	$arrayBody["apiKey"] = $apikey; // toss key 환경설정 파일 include
	$arrayBody["payToken"] = $row_toss['toss_pay_token'];
	$jsonBody = json_encode($arrayBody);

	$ch = curl_init('https://pay.toss.im/tosspay/api/v1/execute');
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/json',
	'Content-Length: ' . strlen($jsonBody))
	);

	$result = curl_exec($ch);
	curl_close($ch);

	$toss_result = json_decode(stripslashes($result),true);
	$toss_msg = $toss_result['msg'];
	$toss_code = $toss_result['code'];
	$toss_errorcode = $toss_result['errorCode'];
	$toss_status = $toss_result['status'];

	
	// 상태체크 및 메세지
	$db_result['state'] = 's000';
	if($toss_code != ""){	$db_result['state'] = $toss_code; }
	$db_result['msg'] = 'TOSS 네트워크 오류';
	if($toss_msg != ""){	"TOSS 메세지는 ".$toss_msg."[".$toss_errorcode."] 입니다.(에러 메세지 메모 후 관리자에게 문의하시기 바랍니다)"; }

	$bSucc = false; // DB 정상처리 완료 여부
	$boolean_mpi = false;
	$boolean_cash = false;
	$boolean_sr = false;
	$randombox_coupon_use = false;

	// 상태체크 및 메세지
	$db_result['state'] = $toss_code;
	$db_result['msg'] = "TOSS 메세지는 ".$toss_msg."[".$toss_errorcode."] 입니다.(에러 메세지 메모 후 관리자에게 문의하시기 바랍니다)";
	
	if($toss_code == '0'){
		$db_result['state'] = $toss_code;
		$db_result['msg'] = "";
	}
	
	// 결제 에러
	if($db_result['state'] != '0'){
		cs_x_error_log("x_pg_channel_toss", "toss", NM_TOSS_PATH, "result", "", $result, 1, $toss_msg."- errorCode :".$toss_errorcode, $nm_member); // state-1
			$bSucc = false;
		if(is_mobile()){ 
			// cs_alert($db_result['msg'], NM_URL."/recharge.php");
		}else{ 
			// pop_close($db_result['msg']); 
		}
		// die;
	}
	
	/*
	if($status == "PAY_CANCEL"){
		$db_result['state'] = 1;
		$db_result['msg'] = 'Toss결제를 취소하셨습니다.';
	}else{
		if($status == "" || $status == NULL || $status != "PAY_COMPLETE"){	
			// cs_error_log(NM_TOSS_PATH."/log/".$nm_config['nm_mode']."/".NM_TIME_YMD.".log", $sql_toss_insert, $_SERVER['PHP_SELF']);
			cs_x_error_log("x_pg_channel_toss", "toss", NM_TOSS_PATH, "sql_toss_insert", "", $sql_toss_insert, 1, "error code : toss retUrl status NULL ".$status, $nm_member); // state-1
			$db_result['state'] = 2;
			$db_result['msg'] = '에러 메세지 메모 후 관리자에게 문의하시기 바랍니다: error code : toss retUrl status NULL '.$status;
		}
		if($orderNo == "" || $orderNo == NULL){	
			// cs_error_log(NM_TOSS_PATH."/log/".$nm_config['nm_mode']."/".NM_TIME_YMD.".log", $sql_toss_insert, $_SERVER['PHP_SELF']);
			cs_x_error_log("x_pg_channel_toss", "toss", NM_TOSS_PATH, "sql_toss_insert", "", $sql_toss_insert, 2, "error code : toss retUrl orderNo NULL ", $nm_member); // state-2
			$db_result['state'] = 3;
			$db_result['msg'] = '에러 메세지 메모 후 관리자에게 문의하시기 바랍니다: error code : toss retUrl orderNo NULL';
		}
	}
	
	print_r($db_result);
	die;
	// 에러메세지
	if($db_result['state'] != 0){
		if(is_mobile()){ 
			cs_alert($db_result['msg']." code(".$db_result['state'].")", NM_URL."/recharge.php");
		}else{ 
			cs_alert_close($db_result['msg']); 
		}
		die;
	}
	*/

	/* = -------------------------------------------------------------------------- = */
	/* =   위 결제 정보 저장		                                                  = */
	/* ============================================================================== */
	$sql_toss_update = " UPDATE pg_channel_toss SET 
	toss_status = '".$status."', toss_returl_date = '".NM_TIME_YMDHIS."'
	WHERE toss_member = '".$nm_member['mb_no']."' AND toss_member_id = '".$nm_member['mb_id']."' AND toss_member_idx = '".$nm_member['mb_idx']."' AND 
	toss_order = '".$orderNo."' 
	";

	if($db_result['state'] == '0'){ // toss 정보 저장	
		if(sql_query($sql_toss_update)){ 

		}else{
			//실패 로그 남기기
			// cs_error_log(NM_TOSS_PATH."/log/".$nm_config['nm_mode']."/".NM_TIME_YMD.".log", "function cs_set_member_point_income error", $_SERVER['PHP_SELF']);
			cs_x_error_log("x_pg_channel_toss", "toss", NM_TOSS_PATH, "sql_toss_update", "", $sql_toss_update, 4, "toss 정보 저장 실패", $nm_member); // state-3
		}
		// toss 저장 정보 가져오기
		$row_toss = cs_pg_channel_toss($nm_member, $orderNo);	  
		$mpi_won = intval($row_toss['toss_amt']);
		$mpi_cash_point = intval($row_toss['toss_cash_point'] + $row_toss['toss_event_cash_point']);
		$mpi_point = intval($row_toss['toss_point'] + $row_toss['toss_event_point']);
	    $mpi_from = "toss[".$row_toss['toss_product']."]";

		// 새로고침시 또 부여되어서 검사
		$row_mpi = cs_member_point_income($nm_member, $orderNo);
		if(count($row_mpi) > 1){
		  $bSucc = true;
		}else{
			// 회원 포인트 부여
			$bSucc = $boolean_mpi = cs_set_member_point_income($nm_member, $mpi_cash_point, $mpi_point, $mpi_won, $mpi_from, $orderNo);
			if($boolean_mpi == false){ 
				//실패 로그 남기기
				// cs_error_log(NM_TOSS_PATH."/log/".$nm_config['nm_mode']."/".NM_TIME_YMD.".log", "function cs_set_member_point_income error", $_SERVER['PHP_SELF']);
				cs_x_error_log("x_pg_channel_toss", "toss", NM_TOSS_PATH, "", "cs_set_member_point_income", "", 3, "회원 포인트 부여 실패", $nm_member); // state-3
			}

			// 회원 캐쉬
			$bSucc = $boolean_cash = mb_set_cash($nm_member, $row_toss, 'toss');
			if($boolean_cash == false){ 
				//실패 로그 남기기 
				// cs_error_log(NM_TOSS_PATH."/log/".$nm_config['nm_mode']."/".NM_TIME_YMD.".log", "function mb_set_cash error", $_SERVER['PHP_SELF']);
				cs_x_error_log("x_pg_channel_toss", "toss", NM_TOSS_PATH, "", "mb_set_cash", "", 4, "회원 캐쉬 실패", $nm_member); // state-4
			}

			// 회원 포인트 통계
			$bSucc = $boolean_sr = cs_set_sales_recharge($nm_member, $row_toss, 'toss');
			if($boolean_sr == false){ 
				//실패 로그 남기기 
				// cs_error_log(NM_TOSS_PATH."/log/".$nm_config['nm_mode']."/".NM_TIME_YMD.".log", "function cs_set_sales_recharge error", $_SERVER['PHP_SELF']);
				cs_x_error_log("x_pg_channel_toss", "toss", NM_TOSS_PATH, "", "cs_set_sales_recharge", "", 5, "회원 포인트 통계 실패", $nm_member); // state-5
			}
		  
		    // 회원 랜덤박스 할인권 사용시 사용처리
			if(intval($coupondc)>0 && $coupondc != '') {
			    $randombox_coupon_use = false;
	
				// 가격 가져오기
				$sql_crp = "SELECT * FROM config_recharge_price WHERE crp_state = 'y' AND crp_no = '".$row_toss['toss_crp_no']."'";
				$row_crp = sql_fetch($sql_crp);
	
				// 1.이벤트결제상품(첫결제), 2.50땅콩할인제외
				if($row_crp['crp_cash_count_event'] != '') {
					/* 2. 이벤트 상품 제외 - 예시 첫결제 */
				} else if(intval($row_crp['crp_cash_point']) < 50 ) {
					/* 3. 50땅콩 미만인 결제 상품에는 보너스 지급 안되도록 2017-08-31 */	
				} else { 
					$randombox_coupon_use = true; 
				} // end else
	
				if($randombox_coupon_use == true) {
					randombox_coupon_use($nm_member, $coupondc, $payway_no, $row_toss['toss_crp_no']);		  	
				} // end if
			} // end if
		} // end else
	}else{
		// 실패 로그 남기기	  
		// cs_error_log(NM_TOSS_PATH."/log/".$nm_config['nm_mode']."/".NM_TIME_YMD.".log", $sql_toss_insert, $_SERVER['PHP_SELF']);
		
		if($toss_code != '0'){
			cs_x_error_log("x_pg_channel_toss", "toss", NM_TOSS_PATH, "not0000", "", $toss_code, 6, "TOSS 코드 실패", $nm_member); // state-6
		}else{
			cs_x_error_log("x_pg_channel_toss", "toss", NM_TOSS_PATH, "sql_toss_insert", "", $sql_toss_update, 99, "TOSS 네트워크 오류", $nm_member); // state-6
		}
	}
	
	/* 181001 */
	// $bSucc = false;
	if($bSucc == false)
	{
		sleep(2); // 2초 지연
		$transaction['apikey'] = $apikey;
		$transaction['payToken'] = $arrayBody["payToken"];
		$transaction['amountTaxFree'] = $transaction['amount'] = $row_toss['toss_amt'];
		// $db_result = cs_recharge_save_cancel($nm_member, 'toss', $_SESSION['recharge_order_no'], $transaction, $coupondc, $boolean_mpi, $boolean_cash, $boolean_sr, $randombox_coupon_use);
		// 181024 주문번호-캔슬용 삭제 : 세션처리했었는데 뒤로가기할때 문제가 발생되어 DB 저장하기로 함
		$cs_get_pg_member_order = cs_get_pg_member_order($nm_member); // 주문번호 가져오기
		cs_del_pg_member_order($nm_member); // 주문번호 삭제
		// $db_result = cs_recharge_save_cancel($nm_member, 'toss', $cs_get_pg_member_order, $transaction, $coupondc, $boolean_mpi, $boolean_cash, $boolean_sr, $randombox_coupon_use);
		// $toss_code = $db_result['state'];
		// $toss_msg = $db_result['msg'];			

		// 취소 x-테스트 - 181120
		$nm_cancel_member = mb_get($nm_member['mb_id']);  // 취소전에 데이터 갱신
		$row_mpi_check = cs_member_point_income($nm_cancel_member, $cs_get_pg_member_order);

		// 기록 땅콩과 현재 땅콩이 같으면 취소 패스		
		if(intval($row_mpi_check['mpi_no']) > 0){

			$x_point = "mpi_cpb:".$row_mpi_check['mpi_cash_point_balance']."||";
			$x_point.= "mb_cp:".$nm_cancel_member['mb_cash_point']."||";
			$x_point.= "mpi_pb:".$row_mpi_check['mpi_point_balance']."||";
			$x_point.= "mb_p:".$nm_cancel_member['mb_point'];

			if($row_mpi_check['mpi_cash_point_balance'] == $nm_cancel_member['mb_cash_point'] && $row_mpi_check['mpi_point_balance'] == $nm_cancel_member['mb_point'] ){

				$sql_x_pg_cancel = " 
				INSERT INTO x_pg_cancel (x_cancel_member, x_cancel_from, x_cancel_order, 
				x_mpi_no, x_point, x_mpi_state, 
				x_cancel_mode, x_cancel_useragent, x_cancel_server_ip, x_cancel_date 
				)VALUES( '".$nm_member['mb_no']."', '".'toss'."', '".$cs_get_pg_member_order."', 
				'".$row_mpi_check['mpi_no']."', '".$x_point."', '1', 
				'".$nm_config['nm_mode']."', '".HTTP_USER_AGENT."', '".$_SERVER['SERVER_ADDR']."', '".NM_TIME_YMDHIS."' )
				";
				@mysql_query($sql_x_pg_cancel);
			}else{
				$sql_x_pg_cancel = " 
				INSERT INTO x_pg_cancel (x_cancel_member, x_cancel_from, x_cancel_order, 
				x_mpi_no, x_point, x_mpi_state, 
				x_cancel_mode, x_cancel_useragent, x_cancel_server_ip, x_cancel_date 
				)VALUES( '".$nm_member['mb_no']."', '".'toss'."', '".$cs_get_pg_member_order."', 
				'".$row_mpi_check['mpi_no']."', '".$x_point."', '2', 
				'".$nm_config['nm_mode']."', '".HTTP_USER_AGENT."', '".$_SERVER['SERVER_ADDR']."', '".NM_TIME_YMDHIS."' )
				";
				@mysql_query($sql_x_pg_cancel);
				// 위 테스트가 제대로 돌아간다면 주석 풀기
				// $db_result = cs_recharge_save_cancel($nm_cancel_member, 'toss', $cs_get_pg_member_order, $transaction, $coupondc, $boolean_mpi, $boolean_cash, $boolean_sr, $randombox_coupon_use);
				// $toss_code = $db_result['state'];
				// $toss_msg = $db_result['msg'];		
			}
		}
		// 취소 x-테스트 end - 181120

		$nm_member = mb_get($nm_member['mb_id']);
		
		
	}else{
		cs_del_pg_member_order($nm_member); // 주문번호 삭제
	}

	/* 결제 보여주기에서 사용할 변수 정의 */
	$row_view 						= cs_pg_channel_toss($nm_member, $orderNo);
	$row_view_mb 					= mb_get($nm_member['mb_id']);
	
	$view_order						= $row_view['toss_order'];			// 주문번호
	$view_product					= $row_view['toss_product'];		// 상품명
	$view_amt							= $row_view['toss_amt'];			// 결제금액
	$view_date						= $row_view['toss_returl_date'];	// 결제시간	
    $view_recharge_cash		= $row_view['toss_cash_point'];	// 충전땅콩
    $view_recharge_point	= $row_view['toss_point'];		// 충전미니땅콩
    
	$view_cash_point			= number_format($row_view_mb['mb_cash_point']);
	$view_point						= number_format($row_view_mb['mb_point']);
	
	$view_payway = $nm_config['cf_payway'][$payway_no][2];	// 결제방법

	$receipt_text_error = "";							// 결제 상단 메세지
	$receipt_state = 0;
	if($db_result['state'] != '0'){
		$receipt_text_error = "결제가 정상적으로 되지 않았습니다.";
		$receipt_text_error.= "<br/>".$toss_msg."<br/>"."(code=".$toss_code.")"; 
		$receipt_state = 1;
	}

	/* 결제 보여주기 */
	include_once NM_PROC_PATH.'/recharge_receipt.php';
?>