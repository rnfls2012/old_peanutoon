<? if (!defined("_RECHARGE_")) exit; // 개별 페이지 접근 불가
include_once './_common.php'; // 공통
?>
<?
    /* ============================================================================== */
    /* =   환경 설정 파일 Include                                                   = */
    /* = -------------------------------------------------------------------------- = */
    /* =   ※ 필수                                                                  = */
    include NM_TOSS_PATH.'/cfg/site_conf_inc.php';      // 환경설정 파일 include
?>
<?
$retUrl = NM_PROC_URL."/".cs_recharge_save_filename_ctrl().".php?payway=".$payway[0]."&payway_no=".$payway_no."&coupondc=".$coupondc;	 // 리턴 URL

$arrayBody = array();
$arrayBody["apiKey"] = $apikey; // toss key 환경설정 파일 include
$arrayBody["orderNo"] = $order_no; // 쇼핑몰 주문번호
$arrayBody["amount"] = $recharge['crp_won']; // 결제 총금액 
$arrayBody["productDesc"] = $crp_cash_name; // 상품명   
// $arrayBody["expiredTime"] = NM_TIME_YMDHIS; // 결제 만료 기한-17-09-13
$arrayBody["autoExecute"] = false; // 자동 승인 여부 설정 -17-09-14
$arrayBody["retUrl"] = $retUrl; /* 결제 완료 후 연결할 웹페이지의 URL (미지정 시 Toss 웹페이지로 연결) */
// 180630 법인변경으로 인해 필수 항목 추가
$arrayBody["amountTaxFree"] = $recharge['crp_won']; // 결제 금액 중 비과세금액 (복합과세) 
// $arrayBody["amountTaxFree"] = 0; 180906 유소진대리 요청-비과세
$arrayBody["cashreceipt"] = true; // 현금영수증 발급 가능 여부

$jsonBody = json_encode($arrayBody);

$ch = curl_init('https://pay.toss.im/tosspay/api/v1/payments');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
'Content-Type: application/json',
'Content-Length: ' . strlen($jsonBody))
);

$result = curl_exec($ch);
curl_close($ch);
/* 설명 -> https://chongmoa.com:45183/php/364 
stdClass 는 Json 을 사용할때도 사용 되기도 한다.
스크립트에서 ajax 사용시 넘기는 데이터 타입을 json 으로 지정하면
넘어가는 데이터가 stdClass로 넘어간다.
이럴경우 일반 배열로 다시 변환 하고 싶다면...
json_decode($aa,true); 로 선언하면 된다.
*/

$toss_result = json_decode(stripslashes($result),true);
$toss_status = $toss_result['status'];
$toss_paytoken = $toss_result['payToken'];
$toss_checkoutpage = $toss_result['checkoutPage'];
$toss_code = $toss_result['code'];
// toss url로 보내기 전에 저장

$sql_toss_insert = " INSERT INTO pg_channel_toss (
	toss_member, toss_member_id, toss_member_idx, 
	toss_product, toss_crp_no, toss_er_no, toss_amt, 
	toss_cash_point, toss_point, toss_event_cash_point, toss_event_point, 
	toss_order, toss_date, toss_useragent, 
	toss_code, toss_pay_token, toss_checkoutpage
	)VALUES( 
	'".$nm_member['mb_no']."', '".$nm_member['mb_id']."', '".$nm_member['mb_idx']."', 
	'".$crp_cash_name."', '".intval($crp_no)."', '".intval($recharge['er_no'])."', '".intval($recharge['crp_won'])."', 
	'".intval($recharge['crp_cash_point'])."', '".intval($recharge['crp_point'])."', 
	'".intval($recharge['er_cash_point'])."', '".intval($recharge['er_point'])."', 
	'".$order_no."', '".NM_TIME_YMDHIS."', '".HTTP_USER_AGENT."', 
	'".$toss_code."', '".$toss_paytoken."', '".$toss_checkoutpage."' 
	)";
if(sql_query($sql_toss_insert)){
	/* 181001 결제대행패스-테스트모드 */
	if($_SESSION['cf_pg_pass_ctrl'] == 'y' && mb_class_permission('a') == true){
		goto_url(NM_PROC_URL."/".cs_recharge_save_filename_ctrl().".php?crp_no=".$crp_no."&payway_no=".$payway_no."&coupondc=".$coupondc."&payway=toss&pg_pass=y");
		die;
	}else{
		goto_url($toss_checkoutpage);
	}
}else{
	// 실패 로그 남기기
	// cs_error_log(NM_TOSS_PATH."/log/".$nm_config['nm_mode']."/".NM_TIME_YMD.".log", $sql_toss_insert, $_SERVER['PHP_SELF']);
	cs_x_error_log("x_pg_channel_toss", "toss", NM_TOSS_PATH, "sql_toss_insert", "", $sql_toss_insert, 1, "toss url로 보내기 전에 저장 실패", $nm_member); // state-1
	cs_alert("관리자에게 문의하시기 바랍니다: error code : insert into not - 0",NM_URL."/recharge.php");
	die;
}
/*
code 응답코드 
0	성공
10	결제 에러
11	중복 주문
12	1회 결제 한도 초과 금액
*/
?>