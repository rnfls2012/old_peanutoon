<? if (!defined("_RECHARGE_")) exit; // 개별 페이지 접근 불가
	include_once './_common.php'; // 공통
?>

<?
    /* ============================================================================== */
    /* =   02. 지불 요청 정보 설정                                                  = */
    /* = -------------------------------------------------------------------------- = */
	if(is_mobile()){
		include NM_KCP_PATH.'/cfg/site_conf_inc.php';           // 환경설정 파일 include
		include_once NM_KCP_MO_PATH."/pp_cli_hub_lib.php";      // library [수정불가]
	}else{
		include_once NM_KCP_PATH.'/cfg/site_conf_inc.php';      // 환경설정 파일 include
		include_once NM_KCP_PC_PATH."/pp_cli_hub_lib.php";      // library [수정불가]
	}
    /* = -------------------------------------------------------------------------- = */
    $req_tx         = "mod"; 							                      // 요청 종류		
    $mod_type       = "STSC";                                                 // 변경TYPE(승인취소시 필요)
    $tno            = $tno;                                                   // KCP 거래 고유 번호
    // $mod_desc       = "통신에러-결제취소";                                         // 변경사유
    $mod_desc       = $mod_desc;                                              // 변경사유
    $cust_ip        = REMOTE_ADDR;                                            // 요청 IP
	/* = -------------------------------------------------------------------------- = */
	
    /* ============================================================================== */
    /* =   02. 지불 요청 정보 설정 END                                              = */
    /* ============================================================================== */


    /* ============================================================================== */
    /* =   03. 인스턴스 생성 및 초기화(변경 불가)                                   = */
    /* = -------------------------------------------------------------------------- = */
    /* =       결제에 필요한 인스턴스를 생성하고 초기화 합니다.                     = */
    /* = -------------------------------------------------------------------------- = */
    $c_PayPlus = new C_PP_CLI;

    $c_PayPlus->mf_clear();
    /* ============================================================================== */
    /* =   03. 인스턴스 생성 및 초기화 END                                          = */
    /* ============================================================================== */


    /* ============================================================================== */
    /* =   04. 처리 요청 정보 설정                                                  = */
    /* = -------------------------------------------------------------------------- = */

    /* = -------------------------------------------------------------------------- = */
    /* =   04-1. 취소/매입 요청                                                     = */
    /* = -------------------------------------------------------------------------- = */
        $tran_cd = "00200000";
        $c_PayPlus->mf_set_modx_data( "tno",        $tno        ); // KCP 원거래 거래번호
        $c_PayPlus->mf_set_modx_data( "mod_type",   $mod_type   ); // 전체취소 STSC / 부분취소 STPC 
        $c_PayPlus->mf_set_modx_data( "mod_ip",     $cust_ip    ); // 변경 요청자 IP
        $c_PayPlus->mf_set_modx_data( "mod_desc",   ""          ); // 변경 사유
    /* = -------------------------------------------------------------------------- = */
    /* =   04. 처리 요청 정보 설정 END                                              = */
    /* = ========================================================================== = */


    /* = ========================================================================== = */
    /* =   05. 실행                                                                 = */
    /* = -------------------------------------------------------------------------- = */
	if($tno == ""){
		$db_result['state'] = 'cancel-tno-null';
		$db_result['msg']   = '취소요청-거래번호빈값';
		$db_history = $db_result['state'];
	}else{
		if ( $tran_cd != "" )
		{
			//$c_PayPlus->mf_do_tx( "", $home_dir, $site_cd, $site_key, $tran_cd, "",
								  //$gw_url, $gw_port, "payplus_cli_slib", "",
								  //$cust_ip, "3", 0, 0, $log_path ); // 응답 전문 처리

			// windows 사용시
			$c_PayPlus->mf_do_tx( "", $g_conf_home_dir, $g_conf_site_cd, $g_conf_site_key, $tran_cd, "",
								 $g_conf_gw_url, $g_conf_gw_port, "payplus_cli_slib", "",
								 "", "3" , 0, 0, $g_conf_key_dir, $g_conf_log_dir); // 응답 전문 처리
			
		}
		else
		{
			$c_PayPlus->m_res_cd  = "9562";
			$c_PayPlus->m_res_msg = "연동 오류|Payplus Plugin이 설치되지 않았거나 tran_cd값이 설정되지 않았습니다.";
			$db_history = 'tran_cd 빈값';
		}

			$res_cd  = $c_PayPlus->m_res_cd;  // 결과 코드
			$res_msg = $c_PayPlus->m_res_msg; // 결과 메시지
		/* = -------------------------------------------------------------------------- = */
		/* =   05. 실행 END                                                             = */
		/* ============================================================================== */
		if ( $res_cd == "0000" ) // 정상결제 인 경우
		{
			$card_cd    = $c_PayPlus->mf_get_res_data( "card_cd"    ); 	// 카드사 코드
			$card_name  = $c_PayPlus->mf_get_res_data( "card_name"  ); 	// 카드 명
			$amount     = $c_PayPlus->mf_get_res_data( "amount"  	); 	// 결제 금액
			$coupon_mny = $c_PayPlus->mf_get_res_data( "coupon_mny" ); 	// 쿠폰금액 
			$canc_time  = $c_PayPlus->mf_get_res_data( "canc_time"   ); // 취소 시간
		}
		
		// utf-8로 변환
		$card_name = iconv("euc-kr", "UTF-8", $card_name);
		$res_msg = iconv("euc-kr", "UTF-8", $res_msg);

		if ( $res_cd== "0000" && $mod_type == "STSC"){
			/*
			echo "취소요청이 완료되었습니다.    <br>";
			echo "결과코드 : $res_cd     <br>";
			echo "카드코드 : $card_cd    <br>";
			echo "카드명  : $card_name  <br>";
			echo "결제금액 : $amount     <br>";
			echo "쿠폰금액 : $coupon_mny <br>";
			echo "취소요청시간:$canc_time  <br>";
			echo "결과메세지 :$res_msg    <p>";
			*/
			$db_history = 'ok - res_msg : '.$res_msg;
			
		}else{
			/*
			echo "취소요청이 처리 되지 못하였습니다.  <br>";
			echo "결과코드 : $res_cd   <br>";
			echo "결과메세지 : $res_msg <p>";
			*/				
			$db_history = 'error - res_cd : '.$res_cd.' / res_msg : '.$res_msg;
		}

	}   
	cs_x_error_log("x_pg_channel_kcp", "kcp", NM_KCP_PATH."/cfg/", "kcp_cancel", "", $db_history, 88, "kcp 취소", $nm_member); // state-88
	
	$db_result['state'] = 'cancel-'.$res_cd;
	$db_result['msg']   = '취소요청-'. $res_msg;
    /* = -------------------------------------------------------------------------- = */
    /* =   06. 취소 결과 처리 END                                                   = */
    /* ============================================================================== */

?>