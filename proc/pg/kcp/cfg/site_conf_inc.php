<? include_once './_common.php'; // 공통
    /* ============================================================================== */
    /* =   PAGE : 결제 정보 환경 설정 PAGE                                          = */
    /* =----------------------------------------------------------------------------= */
    /* =   연동시 오류가 발생하는 경우 아래의 주소로 접속하셔서 확인하시기 바랍니다.= */
    /* =   접속 주소 : http://kcp.co.kr/technique.requestcode.do                    = */
    /* =----------------------------------------------------------------------------= */
    /* =   Copyright (c)  2016   NHN KCP Inc.   All Rights Reserverd.               = */
    /* ============================================================================== */

    /* ============================================================================== */
    /* = ※ 주의 ※                                                                 = */
    /* = * g_conf_home_dir 변수 설정                                                = */
    /* =----------------------------------------------------------------------------= */
    /* =   BIN 절대 경로 입력 (bin전까지 설정                                       = */
    /* ============================================================================== */
    $g_conf_home_dir  = NM_KCP_PATH;					// BIN 절대경로 입력 (bin전까지)
	
	/* ============================================================================== */
    /* = ※ 주의 ※                                                                 = */
    /* = * g_conf_log_path 변수 설정                                                = */
    /* =----------------------------------------------------------------------------= */
    /* =   log 경로 지정                                                            = */
    /* ============================================================================== */
    $g_conf_log_path = "/../log";
    
    /* ============================================================================== */
    /* = ※ 주의 ※                                                                 = */
    /* = * g_conf_gw_url 설정                                                       = */
    /* =----------------------------------------------------------------------------= */
    /* = 테스트 시 : testpaygw.kcp.co.kr로 설정해 주십시오.                         = */
    /* = 실결제 시 : paygw.kcp.co.kr로 설정해 주십시오.                             = */
    /* ============================================================================== */

	/* 181001 */
	if($_SESSION['cf_pg_test_ctrl'] == 'y'){ $nm_config['cf_pg_kcp_mode'] = 't';}

	$g_conf_gw_url_real = "paygw.kcp.co.kr";
	$g_conf_gw_url_test = "testpaygw.kcp.co.kr";
	$g_conf_gw_url = $g_conf_gw_url_real;
	if($nm_config['cf_pg_kcp_mode'] == 't'){
		$g_conf_gw_url = $g_conf_gw_url_test;
	}
	if($nm_config['cf_pg_kcp_test_id'] == $nm_member['mb_id']){
		$g_conf_gw_url = $g_conf_gw_url_test;
	}

    /* ============================================================================== */
    /* = ※ 주의 ※                                                                 = */
    /* = * 표준웹 결제창 g_conf_js_url 설정                                         = */
    /* =----------------------------------------------------------------------------= */
    /* = 테스트 시 : src="https://testpay.kcp.co.kr/plugin/payplus_web.jsp"         = */
    /* = 실결제 시 : src="https://pay.kcp.co.kr/plugin/payplus_web.jsp"             = */
    /* =----------------------------------------------------------------------------= */
    /* = * 플러그인 결제창 g_conf_js_url 설정                                       = */
    /* =----------------------------------------------------------------------------= */
    /* = 테스트 시 : src="http://pay.kcp.co.kr/plugin/payplus_test.js"              = */
    /* =             src="https://pay.kcp.co.kr/plugin/payplus_test.js"             = */
    /* = 실결제 시 : src="http://pay.kcp.co.kr/plugin/payplus.js"                   = */
    /* =             src="https://pay.kcp.co.kr/plugin/payplus.js"                  = */
    /* =                                                                            = */
    /* = 테스트 시(UTF-8) : src="http://pay.kcp.co.kr/plugin/payplus_test_un.js"    = */
    /* =                    src="https://pay.kcp.co.kr/plugin/payplus_test_un.js"   = */
    /* = 실결제 시(UTF-8) : src="http://pay.kcp.co.kr/plugin/payplus_un.js"         = */
    /* =                    src="https://pay.kcp.co.kr/plugin/payplus_un.js"        = */
    /* ============================================================================== */
	$g_conf_js_url_real = "https://pay.kcp.co.kr/plugin/payplus_web.jsp";
	$g_conf_js_url_test = "https://testpay.kcp.co.kr/plugin/payplus_web.jsp";
	$g_conf_js_url = $g_conf_js_url_real;
	if($nm_config['cf_pg_kcp_mode'] == 't'){
		$g_conf_js_url = $g_conf_js_url_test;
	}
	if($nm_config['cf_pg_kcp_test_id'] == $nm_member['mb_id']){
		$g_conf_js_url = $g_conf_js_url_test;
	}
    /* ============================================================================== */
    /* = 스마트폰 SOAP 통신 설정                                                     = */
    /* =----------------------------------------------------------------------------= */
    /* = 테스트 시 : KCPPaymentService.wsdl                                         = */
    /* = 실결제 시 : real_KCPPaymentService.wsdl                                    = */
    /* ============================================================================== */
    $g_wsdl_real          = "real_KCPPaymentService.wsdl";
    $g_wsdl_test          = "KCPPaymentService.wsdl";
	$g_wsdl = $g_wsdl_real;
	if($nm_config['cf_pg_kcp_mode'] == 't'){
		$g_wsdl = $g_wsdl_test;
	}
	if($nm_config['cf_pg_kcp_test_id'] == $nm_member['mb_id']){
		$g_wsdl = $g_wsdl_test;
	}

    /* ============================================================================== */
    /* = g_conf_site_cd, g_conf_site_key 설정                                       = */
    /* = 실결제시 KCP에서 발급한 사이트코드(site_cd), 사이트키(site_key)를 반드시   = */
    /* = 변경해 주셔야 결제가 정상적으로 진행됩니다.                                = */
    /* =----------------------------------------------------------------------------= */
    /* = 테스트 시 : 사이트코드(T0000)와 사이트키(3grptw1.zW0GSo4PQdaGvsF__)로      = */
    /* =            설정해 주십시오.                                                = */
    /* = 실결제 시 : 반드시 KCP에서 발급한 사이트코드(site_cd)와 사이트키(site_key) = */
    /* =            로 설정해 주십시오.                                             = */
    /* ============================================================================== */
    // $g_conf_site_cd_real   = "A7H0B";
    // $g_conf_site_key_real  = "0o8-5ArkIsrwrjWdSQPZW3T__";

	  /* 18-07-18 피너툰 법인으로 교체 */
    $g_conf_site_cd_real   = "A832J";
    $g_conf_site_key_real  = "3WDrz5z5h-yzl479egu8bHl__";
    
    $g_conf_site_cd_test   = "T0000";
    $g_conf_site_key_test  = "3grptw1.zW0GSo4PQdaGvsF__";
	$g_conf_site_cd = $g_conf_site_cd_real;
	$g_conf_site_key = $g_conf_site_key_real;
	if($nm_config['cf_pg_kcp_mode'] == 't'){
		$g_conf_site_cd = $g_conf_site_cd_test;
		$g_conf_site_key = $g_conf_site_key_test;
	}
	if($nm_config['cf_pg_kcp_test_id'] == $nm_member['mb_id']){
		$g_conf_site_cd = $g_conf_site_cd_test;
		$g_conf_site_key = $g_conf_site_key_test;
	}
	/*
	// 피너툰
    $g_conf_site_cd   = "A7H0B";
    $g_conf_site_key  = "0o8-5ArkIsrwrjWdSQPZW3T__";

	// 메가코믹스
    $g_conf_site_cd   = "A7H0D";
    $g_conf_site_key  = "01WuxQWEZsqjinpCpvUo51u__";
	*/

    /* ============================================================================== */
    /* = g_conf_site_name 설정                                                      = */
    /* =----------------------------------------------------------------------------= */
    /* = 사이트명 설정(한글 불가) : 반드시 영문자로 설정하여 주시기 바랍니다.       = */
    /* ============================================================================== */
    $g_conf_site_name_real = "PEANUTOON";
    $g_conf_site_name_test = "KCP TEST SHOP";
	$g_conf_site_name = $g_conf_site_name_real;
	if($nm_config['cf_pg_kcp_mode'] == 't'){
		$g_conf_site_name = $g_conf_site_name_test;
	}
	if($nm_config['cf_pg_kcp_test_id'] == $nm_member['mb_id']){
		$g_conf_site_name = $g_conf_site_name_test;
	}
	/*
	// 피너툰
    $g_conf_site_name   = "PEANUTOON";

	// 메가코믹스
    $g_conf_site_name   = "MEGACOMICS";
	*/

    /* ============================================================================== */
    /* = 지불 데이터 셋업 (변경 불가)                                               = */
    /* ============================================================================== */
    $g_conf_log_level = "3";
    $g_conf_gw_port   = "8090";        // 포트번호(변경불가)
    $module_type      = "01";          // 변경불가
?>
