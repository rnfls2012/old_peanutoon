<? if (!defined("_RECHARGE_")) exit; // 개별 페이지 접근 불가
	include_once './_common.php'; // 공통
    /* ============================================================================== */
    /* =   PAGE : 결과 처리 PAGE                                                    = */
    /* = -------------------------------------------------------------------------- = */
    /* =   pp_cli_hub.php 파일에서 처리된 결과값을 출력하는 페이지입니다.           = */
    /* = -------------------------------------------------------------------------- = */
    /* =   연동시 오류가 발생하는 경우 아래의 주소로 접속하셔서 확인하시기 바랍니다.= */
    /* =   접속 주소 : http://kcp.co.kr/technique.requestcode.do                    = */
    /* = -------------------------------------------------------------------------- = */
    /* =   Copyright (c)  2016  NHN KCP Inc.   All Rights Reserverd.                = */
    /* ============================================================================== */
?>
<?

    /* ============================================================================== */
    /* =   지불 결과                                                                = */
    /* = -------------------------------------------------------------------------- = */
    $site_cd          = $_POST[ "site_cd"        ];      // 사이트코드
    $req_tx           = $_POST[ "req_tx"         ];      // 요청 구분(승인/취소)
    $use_pay_method   = $_POST[ "use_pay_method" ];      // 사용 결제 수단
    $bSucc            = $_POST[ "bSucc"          ];      // 업체 DB 정상처리 완료 여부
    /* = -------------------------------------------------------------------------- = */
    $res_cd           = $_POST[ "res_cd"         ];      // 결과코드
    $res_msg          = $_POST[ "res_msg"        ];      // 결과메시지
    $res_msg_bsucc    = "";
    /* = -------------------------------------------------------------------------- = */
    $amount           = $_POST[ "amount"         ];      // 금액
    $ordr_idxx        = $_POST[ "ordr_idxx"      ];      // 주문번호
    $tno              = $_POST[ "tno"            ];      // KCP 거래번호
    $good_mny         = $_POST[ "good_mny"       ];      // 결제금액
    $good_name        = $_POST[ "good_name"      ];      // 상품명
    $buyr_name        = $_POST[ "buyr_name"      ];      // 구매자명
    $buyr_tel1        = $_POST[ "buyr_tel1"      ];      // 구매자 전화번호
    $buyr_tel2        = $_POST[ "buyr_tel2"      ];      // 구매자 휴대폰번호
    $buyr_mail        = $_POST[ "buyr_mail"      ];      // 구매자 E-Mail
    /* = -------------------------------------------------------------------------- = */
    // 공통
    $pnt_issue        = $_POST[ "pnt_issue"      ];      // 포인트 서비스사
    $app_time         = $_POST[ "app_time"       ];      // 승인시간 (공통)
    /* = -------------------------------------------------------------------------- = */
    // 신용카드
    $card_cd          = $_POST[ "card_cd"        ];      // 카드코드
    $card_name        = $_POST[ "card_name"      ];      // 카드명
    $noinf            = $_POST[ "noinf"          ];      // 무이자 여부
    $quota            = $_POST[ "quota"          ];      // 할부개월
    $app_no           = $_POST[ "app_no"         ];      // 승인번호
    /* = -------------------------------------------------------------------------- = */
    // 계좌이체
    $bank_name        = $_POST[ "bank_name"      ];      // 은행명
    $bank_code        = $_POST[ "bank_code"      ];      // 은행코드
    /* = -------------------------------------------------------------------------- = */
    // 가상계좌
    $bankname         = $_POST[ "bankname"       ];      // 입금할 은행
    $depositor        = $_POST[ "depositor"      ];      // 입금할 계좌 예금주
    $account          = $_POST[ "account"        ];      // 입금할 계좌 번호
    $va_date          = $_POST[ "va_date"        ];      // 가상계좌 입금마감시간
    /* = -------------------------------------------------------------------------- = */
    // 포인트
    $add_pnt          = $_POST[ "add_pnt"        ];      // 발생 포인트
    $use_pnt          = $_POST[ "use_pnt"        ];      // 사용가능 포인트
    $rsv_pnt          = $_POST[ "rsv_pnt"        ];      // 총 누적 포인트
    $pnt_app_time     = $_POST[ "pnt_app_time"   ];      // 승인시간
    $pnt_app_no       = $_POST[ "pnt_app_no"     ];      // 승인번호
    $pnt_amount       = $_POST[ "pnt_amount"     ];      // 적립금액 or 사용금액
    /* = -------------------------------------------------------------------------- = */
    //상품권
    $tk_van_code      = $_POST[ "tk_van_code"    ];      // 발급사 코드
    $tk_app_no        = $_POST[ "tk_app_no"      ];      // 승인 번호
    /* = -------------------------------------------------------------------------- = */
    //휴대폰
    $commid           = $_POST[ "commid"         ];      // 통신사 코드
    $mobile_no        = $_POST[ "mobile_no"      ];      // 휴대폰 번호
    /* = -------------------------------------------------------------------------- = */
    // 현금영수증
    $cash_yn          = $_POST[ "cash_yn"        ];      //현금영수증 등록 여부
    $cash_authno      = $_POST[ "cash_authno"    ];      //현금영수증 승인 번호
    $cash_tr_code     = $_POST[ "cash_tr_code"   ];      //현금영수증 발행 구분
    $cash_id_info     = $_POST[ "cash_id_info"   ];      //현금영수증 등록 번호

	/* 기타 파라메터 추가 부분 - Start - */
	$param_opt_1     = $_POST[ "param_opt_1"     ];      // 기타 파라메터 추가 부분
	$param_opt_2     = $_POST[ "param_opt_2"     ];      // 기타 파라메터 추가 부분
	$param_opt_3     = $_POST[ "param_opt_3"     ];      // 기타 파라메터 추가 부분
	/* 기타 파라메터 추가 부분 - End -   */

    /* = -------------------------------------------------------------------------- = */


  
	
	/* = -------------------------------------------------------------------------- = */
	/* =   위 결제 정보 저장		                                                  = */
	/* ============================================================================== */
	$sql_kcp_update = " UPDATE pg_channel_kcp SET 
	amount = '".$amount."', tno = '".$tno."', 
	res_cd = '".$res_cd."', res_msg = '".$res_msg."', 
	app_time = '".$app_time."', pnt_issue = '".$pnt_issue."', 

	card_cd = '".$card_cd."', card_name = '".$card_name."', app_no = '".$app_no."', 
	noinf = '".$noinf."', quota = '".$quota."', partcanc_yn = '".$partcanc_yn."', 

	bank_name = '".$bank_name."', bank_code = '".$bank_code."', 

	bankname = '".$bankname."', depositor = '".$depositor."', 
	account = '".$account."', va_date = '".$va_date."', 

	add_pnt = '".$add_pnt."', use_pnt = '".$use_pnt."', rsv_pnt = '".$rsv_pnt."', 
	pnt_app_time = '".$pnt_app_time."', pnt_app_no = '".$pnt_app_no."', pnt_amount = '".$pnt_amount."', 

	commid = '".$commid."', mobile_no = '".$mobile_no."', 

	tk_van_code = '".$tk_van_code."', tk_app_no = '".$tk_app_no."',  

	cash_yn = '".$cash_yn."', cash_authno = '".$cash_authno."',  
	cash_tr_code = '".$cash_tr_code."', cash_id_info = '".$cash_id_info."',  

	param_opt_1 = '".$param_opt_1."', param_opt_2 = '".$param_opt_2."', param_opt_3 = '".$param_opt_3."'  

	WHERE kcp_member = '".$nm_member['mb_no']."' AND kcp_member_id = '".$nm_member['mb_id']."' AND kcp_member_idx = '".$nm_member['mb_idx']."' AND 
	kcp_order = '".$ordr_idxx."' AND kcp_amt = '".$good_mny."' 
	";
	// 상태체크 및 메세지
	$db_result['state'] = 's000';
	if($res_cd != ""){	$db_result['state'] = $res_cd; }
	$db_result['msg'] = 'kcp 네트워크 오류';
	if($res_msg != ""){	$db_result['msg'] = $res_msg; }

	$row_kcp_check = cs_pg_channel_kcp($nm_member, $ordr_idxx);
	$payway_name = "";
	foreach($nm_config['cf_payway'] as $payway_key => $payway_val){ 
		if($payway_val[1] == $row_kcp_check['kcp_payway']){
			$payway_name = $payway_val[2];
		}
	}

	$bSucc = false; // DB 정상처리 완료 여부
	$boolean_mpi = false;
	$boolean_cash = false;
	$boolean_sr = false;
	$randombox_coupon_use = false;
	
	// 참조 내용
	// 사용자 결제 취소 $res_cd == 3001
	// 새로고침시 $res_cd == "0000"
	
	if(!($res_cd != "0000" || $amount == ""  || $amount == "0" || $amount == 0 || $good_mny == ""  || $good_mny == "0" || $good_mny == 0 || $tno == "" || $ordr_idxx == "")){ // 상태값 , 주문금액, 결제금액, 거래번호, 주문번호
	  if(sql_query($sql_kcp_update)){ // kcp 정보 저장	
		  /*
		  $db_result['state'] = 1;
		  $db_result['msg'] = $payway_name.'결제를 취소하셨습니다.';	
		  cs_x_error_log("x_pg_channel_kcp", "kcp", NM_KCP_PATH."/".NM_PC, "not0000", "", $res_cd, 1, "kcp 코드 실패", $nm_member); // state-1  
		  if(is_mobile()){ 
			  // cs_alert($db_result['msg']." code(".$db_result['state']."[".$res_cd."])", NM_URL."/recharge.php");
		  }else{ 
			  // cs_alert_close($db_result['msg']); 
		  }
		  die;
		  */
	  }else{
		cs_x_error_log("x_pg_channel_kcp", "kcp", NM_KCP_PATH."/".NM_PC, "sql_kcp_update", "", $sql_kcp_update, 4, "kcp 정보 저장 실패", $nm_member); // state-4
	  }
	  // kcp 저장 정보 가져오기
	  $row_kcp = cs_pg_channel_kcp($nm_member, $ordr_idxx, $good_mny);
	  $mpi_won = intval($row_kcp['kcp_amt']);
	  $mpi_cash_point = intval($row_kcp['kcp_cash_point'] + $row_kcp['kcp_event_cash_point']);
	  $mpi_point = intval($row_kcp['kcp_point'] + $row_kcp['kcp_event_point']);
	  $mpi_payway = "";
	  foreach($nm_config['cf_payway'] as $mpi_payway_key => $mpi_payway_val){
		  if($mpi_payway_val[1] == $row_kcp['kcp_payway']){ $mpi_payway = $mpi_payway_val[2]; }
	  }
	  $mpi_from = "kcp(".$mpi_payway.")[".$row_kcp['kcp_product']."]";
	  
	  // 새로고침시 또 부여되어서 검사
	  $row_mpi = cs_member_point_income($nm_member, $ordr_idxx);
	  if(count($row_mpi) > 1){
		  $bSucc = true;
	  }else{
		  // 회원 포인트 부여
		  $bSucc = $boolean_mpi = cs_set_member_point_income($nm_member, $mpi_cash_point, $mpi_point, $mpi_won, $mpi_from, $ordr_idxx);
		  if($boolean_mpi == false){ 
				// 실패 로그 남기기 
				// cs_error_log(NM_KCP_PATH."/log/".NM_PC."/".NM_TIME_YMD.".log", "function cs_set_member_point_income error", $_SERVER['PHP_SELF']);
	  		cs_x_error_log("x_pg_channel_kcp", "kcp", NM_KCP_PATH."/".NM_PC, "", "cs_set_member_point_income", "", 1, "회원 포인트 부여 실패", $nm_member); // state-1
		  }

		  // 회원 캐쉬
		  $bSucc = $boolean_cash = mb_set_cash($nm_member, $row_kcp, 'kcp');
		  if($boolean_cash == false){ 
				// 실패 로그 남기기 
				// cs_error_log(NM_KCP_PATH."/log/".NM_PC."/".NM_TIME_YMD.".log", "function mb_set_cash error", $_SERVER['PHP_SELF']);
	  		cs_x_error_log("x_pg_channel_kcp", "kcp", NM_KCP_PATH."/".NM_PC, "", "mb_set_cash", "", 2, "회원 캐쉬 실패", $nm_member); // state-2
		  }

		  // 회원 포인트 통계
		  $bSucc = $boolean_sr = cs_set_sales_recharge($nm_member, $row_kcp, 'kcp');
		  if($boolean_sr == false){ 
				// 실패 로그 남기기 
				// cs_error_log(NM_KCP_PATH."/log/".NM_PC."/".NM_TIME_YMD.".log", "function cs_set_sales_recharge error", $_SERVER['PHP_SELF']);
	  		cs_x_error_log("x_pg_channel_kcp", "kcp", NM_KCP_PATH."/".NM_PC, "", "cs_set_sales_recharge", "", 3, "회원 포인트 통계 실패", $nm_member); // state-3
		  }
		  
		  // 회원 랜덤박스 할인권 사용시 사용처리
		  if(intval($coupondc)>0 && $coupondc != '') {
			  $randombox_coupon_use = false;

			  // 가격 가져오기
  			  $sql_crp = "SELECT * FROM config_recharge_price WHERE crp_state = 'y' AND crp_no = '".$row_kcp['kcp_crp_no']."'";
			  $row_crp = sql_fetch($sql_crp);

			  // 1.이벤트결제상품(첫결제), 3.50땅콩할인제외
			  if($row_crp['crp_cash_count_event'] != '') {
				  /* 1. 이벤트 상품 제외 - 예시 첫결제 */
			  } else if(intval($row_crp['crp_cash_point']) < 50 ) {
				  /* 2. 50땅콩 미만인 결제 상품에는 보너스 지급 안되도록 2017-08-31 */	
			  } else { 
				  $randombox_coupon_use = true; 
			  } // end else

			  if($randombox_coupon_use == true) {
	      	    randombox_coupon_use($nm_member, $coupondc, $payway_no, $row_kcp['kcp_crp_no']);		  	
			  } // end if
		  }	 // end if		  
	  }
	}else{
	  // 실패 로그 남기기	  
	  // cs_error_log(NM_KCP_PATH."/log/".NM_PC."/".NM_TIME_YMD.".log", $sql_kcp_update, $_SERVER['PHP_SELF']);
	  if($res_cd != "0000" && $res_cd != ""){
		cs_x_error_log("x_pg_channel_kcp", "kcp", NM_KCP_PATH."/".NM_PC, "not0000", "", $res_cd, 1, "kcp 코드 실패", $nm_member); // state-1  
	  }else{
		cs_x_error_log("x_pg_channel_kcp", "kcp", NM_KCP_PATH."/".NM_PC, "sql_kcp_null", "", $sql_kcp_update, 99, "kcp 네트워크 오류", $nm_member); // state-99
	  }
	}

  /* = -------------------------------------------------------------------------- = */
  /* =   위 결제 정보 저장 END		                                              = */
  /* ============================================================================== */

  $req_tx_name     = "";

  if ( $req_tx == "pay" )
  {
    $req_tx_name = "지불" ;
  }
  else if ( $req_tx == "mod" )
  {
    $req_tx_name = "취소/매입" ;
  }



    $req_tx_name = "";

    if( $req_tx == "pay" )
    {
        $req_tx_name = "지불";
    }
    else if( $req_tx == "mod" )
    {
        $req_tx_name = "매입/취소";
    }

    /* ============================================================================== */
    /* =   가맹점 측 DB 처리 실패시 상세 결과 메시지 설정                           = */
    /* = -------------------------------------------------------------------------- = */

    if($req_tx == "pay")
    {
        //업체 DB 처리 실패
        if($bSucc == false)
        {
            if ($res_cd == "0000")
            {
                $res_msg_bsucc = "결제는 정상적으로 이루어졌지만 업체에서 결제 결과를 처리하는 중 오류가 발생하여 시스템에서 자동으로 취소 요청을 하였습니다. <br> 업체로 문의하여 확인하시기 바랍니다.";
            }
            else
            {
                $res_msg_bsucc = "결제는 정상적으로 이루어졌지만 업체에서 결제 결과를 처리하는 중 오류가 발생하여 시스템에서 자동으로 취소 요청을 하였으나, <br> <b>취소가 실패 되었습니다.</b><br> 업체로 문의하여 확인하시기 바랍니다.";
				
				// $db_result['state'] = 2;
				// $db_result['msg'] = $res_msg_bsucc;
            }
        }
    }
	
	/* 181001 */
	// $bSucc = false;
	// 8094 에러는 인증데이터가 중복
	if($bSucc == false)
	{	
		sleep(2); // 2초 지연
		/* 181017-취소필수값저장 */
		if($tno != '' || intval($tno) > 0){
			$transaction['tno'] = $tno;
			cs_del_pg_member_order($nm_member); // 주문번호 삭제
		}else{
			// $row_kcp_cancel = cs_pg_channel_kcp($nm_member, $_SESSION['recharge_order_no']);
			// 181024 주문번호-캔슬용 삭제 : 세션처리했었는데 뒤로가기할때 문제가 발생되어 DB 저장하기로 함
			$cs_get_pg_member_order = cs_get_pg_member_order($nm_member); // 주문번호 가져오기
			cs_del_pg_member_order($nm_member); // 주문번호 삭제
			$row_kcp_cancel = cs_pg_channel_kcp($nm_member, $cs_get_pg_member_order);
			$transaction['tno'] = $row_kcp_cancel['tno'];
		}
		/* 181017-취소필수값저장 end */
		// $db_result = cs_recharge_save_cancel($nm_member, 'kcp', $_SESSION['recharge_order_no'], $transaction, $coupondc, $boolean_mpi, $boolean_cash, $boolean_sr, $randombox_coupon_use);			
		// 181024 주문번호-캔슬용 삭제 : 세션처리했었는데 뒤로가기할때 문제가 발생되어 DB 저장하기로 함
		// $db_result = cs_recharge_save_cancel($nm_member, 'kcp', $cs_get_pg_member_order, $transaction, $coupondc, $boolean_mpi, $boolean_cash, $boolean_sr, $randombox_coupon_use);

		// 취소 x-테스트 - 181120
		$nm_cancel_member = mb_get($nm_member['mb_id']);  // 취소전에 데이터 갱신
		$row_mpi_check = cs_member_point_income($nm_cancel_member, $cs_get_pg_member_order);

		// 기록 땅콩과 현재 땅콩이 같으면 취소 패스
		if(intval($row_mpi_check['mpi_no']) > 0){

			$x_point = "mpi_cpb:".$row_mpi_check['mpi_cash_point_balance']."||";
			$x_point.= "mb_cp:".$nm_cancel_member['mb_cash_point']."||";
			$x_point.= "mpi_pb:".$row_mpi_check['mpi_point_balance']."||";
			$x_point.= "mb_p:".$nm_cancel_member['mb_point'];

			if($row_mpi_check['mpi_cash_point_balance'] == $nm_cancel_member['mb_cash_point'] && $row_mpi_check['mpi_point_balance'] == $nm_cancel_member['mb_point'] ){

				$sql_x_pg_cancel = " 
				INSERT INTO x_pg_cancel (x_cancel_member, x_cancel_from, x_cancel_order, 
				x_mpi_no, x_point, x_mpi_state, 
				x_cancel_mode, x_cancel_useragent, x_cancel_server_ip, x_cancel_date 
				)VALUES( '".$nm_member['mb_no']."', '".'kcp-pc'."', '".$cs_get_pg_member_order."', 
				'".$row_mpi_check['mpi_no']."', '".$x_point."', '1', 
				'".$nm_config['nm_mode']."', '".HTTP_USER_AGENT."', '".$_SERVER['SERVER_ADDR']."', '".NM_TIME_YMDHIS."' )
				";
				@mysql_query($sql_x_pg_cancel);
			}else{
				$sql_x_pg_cancel = " 
				INSERT INTO x_pg_cancel (x_cancel_member, x_cancel_from, x_cancel_order, 
				x_mpi_no, x_point, x_mpi_state, 
				x_cancel_mode, x_cancel_useragent, x_cancel_server_ip, x_cancel_date 
				)VALUES( '".$nm_member['mb_no']."', '".'kcp-pc'."', '".$cs_get_pg_member_order."', 
				'".$row_mpi_check['mpi_no']."', '".$x_point."', '2', 
				'".$nm_config['nm_mode']."', '".HTTP_USER_AGENT."', '".$_SERVER['SERVER_ADDR']."', '".NM_TIME_YMDHIS."' )
				";
				@mysql_query($sql_x_pg_cancel);
				// 위 테스트가 제대로 돌아간다면 주석 풀기
				// $db_result = cs_recharge_save_cancel($nm_cancel_member, 'kcp', $cs_get_pg_member_order, $transaction, $coupondc, $boolean_mpi, $boolean_cash, $boolean_sr, $randombox_coupon_use);
			}
		}
		// 취소 x-테스트 end - 181120

		$nm_member = mb_get($nm_member['mb_id']);
		
		
	}else{
		cs_del_pg_member_order($nm_member); // 주문번호 삭제
	}

    /* = -------------------------------------------------------------------------- = */
    /* =   가맹점 측 DB 처리 실패시 상세 결과 메시지 설정 끝                        = */
    /* ============================================================================== */

    /* = -------------------------------------------------------------------------- = */
    /* =   가맹점 측 DB 처리 실패시 상세 결과 메시지 설정 끝                        = */
    /* ============================================================================== */

	/* 결제 보여주기에서 사용할 변수 정의 */
	$row_view 							= cs_pg_channel_kcp($nm_member, $ordr_idxx);
	$row_view_mb 						= mb_get($nm_member['mb_id']);

	$view_order							= $row_view['kcp_order'];		// 주문번호
	$view_product						= $row_view['kcp_product'];		// 상품명
	$view_amt								= $row_view['kcp_amt'];			// 결제금액
	$view_date							= $row_view['app_time'];		// 결제시간
	$view_recharge_cash			= $row_view['kcp_cash_point'];	// 충전땅콩
	$view_recharge_point		= $row_view['kcp_point'];		// 충전미니땅콩

	$view_cash_point				= number_format($row_view_mb['mb_cash_point']);
	$view_point							= number_format($row_view_mb['mb_point']);
	
	$view_payway	= "";	// 결제방법
	foreach($nm_config['cf_payway'] as $view_payway_key => $view_payway_val){
		if($view_payway_val[1] == $row_view['kcp_payway']){ $view_payway = $view_payway_val[2]; }
	}

	$receipt_text_error = "";								// 결제 상단 메세지
	$receipt_state = 0;
	if($db_result['state'] != '0000'){
		$receipt_text_error = "결제가 정상적으로 되지 않았습니다.";
		$receipt_text_error.= "<br/>".$db_result['msg']."(state=".$db_result['state'].")";
		$receipt_state = 1;
	}
	
	/* 결제 보여주기 */
	include_once NM_PROC_PATH.'/recharge_receipt.php';
?>