<? if (!defined("_RECHARGE_")) exit; // 개별 페이지 접근 불가
	include_once './_common.php'; // 공통
    /* ============================================================================== */
    /* =   PAGE : 결제 요청 PAGE                                                    = */
    /* = -------------------------------------------------------------------------- = */
    /* =   이 페이지는 Payplus Plug-in을 통해서 결제자가 결제 요청을 하는 페이지    = */
    /* =   입니다. 아래의 ※ 필수, ※ 옵션 부분과 매뉴얼을 참조하셔서 연동을        = */
    /* =   진행하여 주시기 바랍니다.                                                = */
    /* = -------------------------------------------------------------------------- = */
    /* =   연동시 오류가 발생하는 경우 아래의 주소로 접속하셔서 확인하시기 바랍니다.= */
    /* =   접속 주소 : http://kcp.co.kr/technique.requestcode.do			        = */
    /* = -------------------------------------------------------------------------- = */
    /* =   Copyright (c)  2016  NHN KCP Inc.   All Rights Reserverd.                = */
    /* ============================================================================== */
?>
<?
    /* ============================================================================== */
    /* =   환경 설정 파일 Include                                                   = */
    /* = -------------------------------------------------------------------------- = */
    /* =   ※ 필수                                                                  = */
    /* =   테스트 및 실결제 연동시 site_conf_inc.jsp 파일을 수정하시기 바랍니다.    = */
    /* = -------------------------------------------------------------------------- = */

    // include "./cfg/site_conf_inc.php";       // 환경설정 파일 include
    include NM_KCP_PATH.'/cfg/site_conf_inc.php';      // 환경설정 파일 include

?>
<?
    /* = -------------------------------------------------------------------------- = */
    /* =   환경 설정 파일 Include END                                               = */
    /* ============================================================================== */
?>
<?
    /* = -------------------------------------------------------------------------- = */
    /* =   위 결제 정보 저장		                                                = */
    /* ============================================================================== */
	$request_tmp = HTTP_REFERER;
	/*
	foreach($_REQUEST as $key => $value){ 
		$request_tmp.= "key=".$key.":".$value; 
	} 
	*/
	$sql_kcp_insert = " INSERT INTO pg_channel_kcp (
	kcp_member, kcp_member_id, kcp_member_idx, 
	kcp_product, kcp_crp_no, kcp_er_no, kcp_amt, 
	kcp_cash_point, kcp_point, kcp_event_cash_point, kcp_event_point, 
	kcp_payway, kcp_order, kcp_date, kcp_useragent 
	)VALUES( 
	'".$nm_member['mb_no']."', '".$nm_member['mb_id']."', '".$nm_member['mb_idx']."', 
	'".$crp_cash_name."', '".intval($crp_no)."', '".intval($recharge['er_no'])."', '".intval($recharge['crp_won'])."', 
	'".intval($recharge['crp_cash_point'])."', '".intval($recharge['crp_point'])."', 
	'".intval($recharge['er_cash_point'])."', '".intval($recharge['er_point'])."', 
	'".$payway[1]."', '".$order_no."', '".NM_TIME_YMDHIS."', '".HTTP_USER_AGENT." //// ".$_SERVER['REQUEST_URI']." //// ".$request_tmp."' 
	)";

	/* 181001 */
	if($res_cd == '3001' ||  $tran_cd_para == "00100000"){ // 사용자 취소시, kcp확인용
		  if($res_cd == '3001'){
			cs_x_error_log("x_pg_channel_kcp", "kcp", NM_KCP_PATH."/".NM_MO, "sql_kcp_cancel", "", $sql_kcp_insert, 3001, "사용자 결제 취소", $nm_member); // state-3001
		  }else{
			cs_x_error_log("x_pg_channel_kcp", "kcp", NM_KCP_PATH."/".NM_MO, "sql_kcp_cancel", "", $sql_kcp_insert, 100000, "enc_data", $nm_member); // state-00100000
		  }
	}else{
		if(sql_query($sql_kcp_insert)){ // kcp 정보 저장 
		}else{
		  // 실패 로그 남기기
		  // cs_error_log(NM_KCP_PATH."/log/".NM_MO."/".NM_TIME_YMD.".log", $sql_kcp_insert, $_SERVER['PHP_SELF']);
		  cs_x_error_log("x_pg_channel_kcp", "kcp", NM_KCP_PATH."/".NM_MO, "sql_kcp_insert", "", $sql_kcp_insert, 1, "결제 정보 저장 실패", $nm_member); // state-1
		}
	}

	/* 181001 결제대행패스-테스트모드 */
	if($_SESSION['cf_pg_pass_ctrl'] == 'y' && mb_class_permission('a') == true){
		goto_url(NM_PROC_URL."/".cs_recharge_save_filename_ctrl().".php?crp_no=".$crp_no."&payway_no=".$payway_no."&coupondc=".$coupondc."&payway=kcp&pg_pass=y");
		die;
	}else{
    /* = -------------------------------------------------------------------------- = */
    /* =   위 결제 정보 저장 END		                                            = */
    /* ============================================================================== */
		?>
		<?
			/* kcp와 통신후 kcp 서버에서 전송되는 결제 요청 정보 */
			$req_tx          = $_POST[ "req_tx"         ]; // 요청 종류         
			$res_cd          = $_POST[ "res_cd"         ]; // 응답 코드         
			$tran_cd         = $_POST[ "tran_cd"        ]; // 트랜잭션 코드     
			$ordr_idxx       = $_POST[ "ordr_idxx"      ]; // 쇼핑몰 주문번호   
			$good_name       = $_POST[ "good_name"      ]; // 상품명            
			$good_mny        = $_POST[ "good_mny"       ]; // 결제 총금액       
			$buyr_name       = $_POST[ "buyr_name"      ]; // 주문자명          
			$buyr_tel1       = $_POST[ "buyr_tel1"      ]; // 주문자 전화번호   
			$buyr_tel2       = $_POST[ "buyr_tel2"      ]; // 주문자 핸드폰 번호
			$buyr_mail       = $_POST[ "buyr_mail"      ]; // 주문자 E-mail 주소
			$use_pay_method  = $_POST[ "use_pay_method" ]; // 결제 방법         
			$enc_info        = $_POST[ "enc_info"       ]; // 암호화 정보       
			$enc_data        = $_POST[ "enc_data"       ]; // 암호화 데이터     
			$cash_yn         = $_POST[ "cash_yn"        ];
			$cash_tr_code    = $_POST[ "cash_tr_code"   ];
			/* 기타 파라메터 추가 부분 - Start - */
			$param_opt_1    = $_POST[ "param_opt_1"     ]; // 기타 파라메터 추가 부분
			$param_opt_2    = $_POST[ "param_opt_2"     ]; // 기타 파라메터 추가 부분
			$param_opt_3    = $_POST[ "param_opt_3"     ]; // 기타 파라메터 추가 부분
			/* 기타 파라메터 추가 부분 - End -   */

		  $tablet_size     = "1.0"; // 화면 사이즈 고정
		  // $url = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

		/*
		  // 결제관련 코드값 체크
		  echo "g_conf_gw_url : ".$g_conf_gw_url."<br>";
		  echo "g_conf_js_url : ".$g_conf_js_url."<br>";
		  echo "g_wsdl : ".$g_wsdl."<br>";
		  echo "g_conf_site_cd : ".$g_conf_site_cd."<br>";
		  echo "g_conf_site_key : ".$g_conf_site_key."<br>";
		  echo "g_conf_site_name : ".$g_conf_site_name."<br>";
		*/

		?>

		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
		<head>
		<title>*** NHN KCP [Mobile Version] ***</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Cache-Control" content="No-Cache">
		<meta http-equiv="Pragma" content="No-Cache">

		<meta name="viewport" content="width=device-width, user-scalable=<?=$tablet_size?>, initial-scale=<?=$tablet_size?>, maximum-scale=<?=$tablet_size?>, minimum-scale=<?=$tablet_size?>">

		<link href="<?=NM_KCP_MO_URL?>/css/style.css" rel="stylesheet" type="text/css" id="cssLink"/>

		<link href="<?=NM_MO_URL?>/css/jquery.modal.css" rel="stylesheet" type="text/css" id="cssLink"/>
		<link href="<?=NM_MO_URL?>/css/jquery.modal.theme-xenon.css" rel="stylesheet" type="text/css" id="cssLink"/>
		<link href="<?=NM_MO_URL?>/css/jquery.modal.theme-atlant.css" rel="stylesheet" type="text/css" id="cssLink"/>
		<script type="text/javascript" src="<?=NM_MO_URL?>/js/jquery.min.js<?=vs_para();?>"></script>
		<script type="text/javascript" src="<?=NM_MO_URL?>/js/common.js<?=vs_para();?>"></script>
		<script type="text/javascript" src="<?=NM_MO_URL?>/js/jquery.modal.min.js<?=vs_para();?>"></script>

		<!-- 거래등록 하는 kcp 서버와 통신을 위한 스크립트-->
		<script type="text/javascript" src="<?=NM_KCP_MO_URL?>/js/approval_key.js<?=vs_para();?>"></script>
		<script type="text/javascript">
		  var controlCss = "<?=NM_KCP_MO_URL?>/css/style_mobile.css";
		  var isMobile = {
			Android: function() {
			  return navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function() {
			  return navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function() {
			  return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function() {
			  return navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function() {
			  return navigator.userAgent.match(/IEMobile/i);
			},
			any: function() {
			  return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
			}
		  };

		  if( isMobile.any() )
			document.getElementById("cssLink").setAttribute("href", controlCss);
		</script>
		<script type="text/javascript">

		   /* kcp web 결제창 호츨 (변경불가) */
		  function call_pay_form()
		  {
			var v_frm = document.order_info; 
			
			v_frm.action = PayUrl;

			if (v_frm.Ret_URL.value == "")
			{
			  /* Ret_URL값은 현 페이지의 URL 입니다. */
			  alert("연동시 Ret_URL을 반드시 설정하셔야 됩니다.");
			  return false;
			}
			else
			{
			  v_frm.submit();
			}
		  }

		   /* kcp 통신을 통해 받은 암호화 정보 체크 후 결제 요청 (변경불가) */
		  function chk_pay()
		  {
			self.name = "tar_opener";
			var pay_form = document.pay_form;

			if (pay_form.res_cd.value == "3001" )
			{
			  // alert("사용자가 취소하였습니다.");
			  alertBox("사용자가 취소하였습니다.", goto_url, {url:"<?=NM_URL?>/recharge.php"});
			  pay_form.res_cd.value = "";
			}
			
			if (pay_form.enc_info.value)
			  pay_form.submit();
		  }

		  function jsf__chk_type()
		  {
			if ( document.order_info.ActionResult.value == "card" )
			{
			  document.order_info.pay_method.value = "CARD";
			}
			else if ( document.order_info.ActionResult.value == "acnt" )
			{
			  document.order_info.pay_method.value = "BANK";
			}
			else if ( document.order_info.ActionResult.value == "vcnt" )
			{
			  document.order_info.pay_method.value = "VCNT";
			}
			else if ( document.order_info.ActionResult.value == "mobx" )
			{
			  document.order_info.pay_method.value = "MOBX";
			}
			else if ( document.order_info.ActionResult.value == "ocb" )
			{
			  document.order_info.pay_method.value = "TPNT";
			  document.order_info.van_code.value = "SCSK";
			}
			else if ( document.order_info.ActionResult.value == "tpnt" )
			{
			  document.order_info.pay_method.value = "TPNT";
			  document.order_info.van_code.value = "SCWB";
			}
			else if ( document.order_info.ActionResult.value == "scbl" )
			{
			  document.order_info.pay_method.value = "GIFT";
			  document.order_info.van_code.value = "SCBL";
			}
			else if ( document.order_info.ActionResult.value == "sccl" )
			{
			  document.order_info.pay_method.value = "GIFT";
			  document.order_info.van_code.value = "SCCL";
			}
			else if ( document.order_info.ActionResult.value == "schm" )
			{
			  document.order_info.pay_method.value = "GIFT";
			  document.order_info.van_code.value = "SCHM";
			}
		  }
		</script>
		</head>

		<body onload="jsf__chk_type();chk_pay();kcp_AJAX();">
		<div id="sample_wrap">
		<form name="order_info" method="post">
			<input type="hidden" name="ActionResult"          value="<?=$payway[1]?>"> 
			<input type="hidden" name="ordr_idxx"				value="<?=$order_no;?>">
			<input type="hidden" name="good_name"				value="<?=$crp_cash_name;?>">
			<input type="hidden" name="good_mny"				value="<?=$recharge['crp_won']?>">
			<input type="hidden" name="buyr_name"				value="<?=$nm_member['mb_id']?>">

		  
		  <!-- 공통정보 -->
		  <input type="hidden" name="req_tx"          value="pay">                           <!-- 요청 구분 -->
		  <input type="hidden" name="shop_name"       value="<?= $g_conf_site_name ?>">      <!-- 사이트 이름 --> 
		  <input type="hidden" name="site_cd"         value="<?= $g_conf_site_cd   ?>">      <!-- 사이트 코드 -->
		  <input type="hidden" name="currency"        value="410"/>                          <!-- 통화 코드 -->
		  <input type="hidden" name="eng_flag"        value="N"/>                            <!-- 한 / 영 -->
		  <!-- 결제등록 키 -->
		  <input type="hidden" name="approval_key"    id="approval">
		  <!-- 인증시 필요한 파라미터(변경불가)-->
		  <input type="hidden" name="escw_used"       value="N">
		  <input type="hidden" name="pay_method"      value="">
		  <input type="hidden" name="van_code"        value="">
		  <!-- 신용카드 설정 -->
		  <input type="hidden" name="quotaopt"        value="12"/>                           <!-- 최대 할부개월수 -->
		  <!-- 가상계좌 설정 -->
		  <input type="hidden" name="ipgm_date"       value=""/>
		  <!-- 가맹점에서 관리하는 고객 아이디 설정을 해야 합니다.(필수 설정) -->
		  <input type="hidden" name="shop_user_id"    value="<?=$nm_member['mb_id']?>"/>
		  <!-- 복지포인트 결제시 가맹점에 할당되어진 코드 값을 입력해야합니다.(필수 설정) -->
		  <input type="hidden" name="pt_memcorp_cd"   value=""/>
		  <!-- 현금영수증 설정 -->
		  <input type="hidden" name="disp_tax_yn"     value="Y"/>
		  <!-- 리턴 URL (kcp와 통신후 결제를 요청할 수 있는 암호화 데이터를 전송 받을 가맹점의 주문페이지 URL) -->
		  <input type="hidden" name="Ret_URL"         value="<?=$ret_url?>">
		  <!-- 화면 크기조정 -->
		  <input type="hidden" name="tablet_size"     value="<?=$tablet_size?>">

		  <!-- 추가 파라미터 ( 가맹점에서 별도의 값전달시 param_opt 를 사용하여 값 전달 ) -->
		  <input type="hidden" name="param_opt_1"     value="kcp">
		  <input type="hidden" name="param_opt_2"     value="<?=$payway_no;?>">
		  <input type="hidden" name="param_opt_3"     value="<?=$coupondc;?>">

		<?
			/* ============================================================================== */
			/* =   옵션 정보                                                                = */
			/* = -------------------------------------------------------------------------- = */
			/* =   ※ 옵션 - 결제에 필요한 추가 옵션 정보를 입력 및 설정합니다.             = */
			/* = -------------------------------------------------------------------------- = */
			/* 카드사 리스트 설정
			예) 비씨카드와 신한카드 사용 설정시
			<input type="hidden" name='used_card'    value="CCBC:CCLG">

			/*  무이자 옵션
					※ 설정할부    (가맹점 관리자 페이지에 설정 된 무이자 설정을 따른다)                             - "" 로 설정
					※ 일반할부    (KCP 이벤트 이외에 설정 된 모든 무이자 설정을 무시한다)                           - "N" 로 설정
					※ 무이자 할부 (가맹점 관리자 페이지에 설정 된 무이자 이벤트 중 원하는 무이자 설정을 세팅한다)   - "Y" 로 설정
			<input type="hidden" name="kcp_noint"       value=""/> */

			/*  무이자 설정
					※ 주의 1 : 할부는 결제금액이 50,000 원 이상일 경우에만 가능
					※ 주의 2 : 무이자 설정값은 무이자 옵션이 Y일 경우에만 결제 창에 적용
					예) 전 카드 2,3,6개월 무이자(국민,비씨,엘지,삼성,신한,현대,롯데,외환) : ALL-02:03:04
					BC 2,3,6개월, 국민 3,6개월, 삼성 6,9개월 무이자 : CCBC-02:03:06,CCKM-03:06,CCSS-03:06:04
			<input type="hidden" name="kcp_noint_quota" value="CCBC-02:03:06,CCKM-03:06,CCSS-03:06:09"/> */

			/* KCP는 과세상품과 비과세상품을 동시에 판매하는 업체들의 결제관리에 대한 편의성을 제공해드리고자, 
			   복합과세 전용 사이트코드를 지원해 드리며 총 금액에 대해 복합과세 처리가 가능하도록 제공하고 있습니다
			   복합과세 전용 사이트 코드로 계약하신 가맹점에만 해당이 됩니다
			   상품별이 아니라 금액으로 구분하여 요청하셔야 합니다
			   총결제 금액은 과세금액 + 부과세 + 비과세금액의 합과 같아야 합니다. 
			   (good_mny = comm_tax_mny + comm_vat_mny + comm_free_mny)
			
				<input type="hidden" name="tax_flag"       value="TG03">  <!-- 변경불가	   -->
				<input type="hidden" name="comm_tax_mny"   value=""    >  <!-- 과세금액	   --> 
				<input type="hidden" name="comm_vat_mny"   value=""    >  <!-- 부가세	   -->
				<input type="hidden" name="comm_free_mny"  value=""    >  <!-- 비과세 금액 --> */
			/* = -------------------------------------------------------------------------- = */
			/* =   옵션 정보 END                                                            = */
			/* ============================================================================== */
		?>

		</form>
		</div>

		<form name="pay_form" method="post" action="<?=$pay_url?>">
			<input type="hidden" name="req_tx"         value="<?=$req_tx?>">               <!-- 요청 구분          -->
			<input type="hidden" name="res_cd"         value="<?=$res_cd?>">               <!-- 결과 코드          -->
			<input type="hidden" name="tran_cd"        value="<?=$tran_cd?>">              <!-- 트랜잭션 코드      -->
			<input type="hidden" name="ordr_idxx"      value="<?=$ordr_idxx?>">            <!-- 주문번호           -->
			<input type="hidden" name="good_mny"       value="<?=$good_mny?>">             <!-- 휴대폰 결제금액    -->
			<input type="hidden" name="good_name"      value="<?=$good_name?>">            <!-- 상품명             -->
			<input type="hidden" name="buyr_name"      value="<?=$buyr_name?>">            <!-- 주문자명           -->
			<input type="hidden" name="buyr_tel1"      value="<?=$buyr_tel1?>">            <!-- 주문자 전화번호    -->
			<input type="hidden" name="buyr_tel2"      value="<?=$buyr_tel2?>">            <!-- 주문자 휴대폰번호  -->
			<input type="hidden" name="buyr_mail"      value="<?=$buyr_mail?>">            <!-- 주문자 E-mail      -->
			<input type="hidden" name="cash_yn"		   value="<?=$cash_yn?>">              <!-- 현금영수증 등록여부-->
			<input type="hidden" name="enc_info"       value="<?=$enc_info?>">
			<input type="hidden" name="enc_data"       value="<?=$enc_data?>">
			<input type="hidden" name="use_pay_method" value="<?=$use_pay_method?>">
			<input type="hidden" name="cash_tr_code"   value="<?=$cash_tr_code?>">

			<!-- 추가 파라미터 -->
			<input type="hidden" name="param_opt_1"	   value="<?=$param_opt_1?>">
			<input type="hidden" name="param_opt_2"	   value="<?=$param_opt_2?>">
			<input type="hidden" name="param_opt_3"	   value="<?=$param_opt_3?>">
		</form>
		</body>
		</html>
<? } ?>