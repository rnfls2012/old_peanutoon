<? if (!defined("_RECHARGE_")) exit; // 개별 페이지 접근 불가
	include_once './_common.php'; // 공통

	//--------------------------------------------------------------------------------
	// PAYCO 주문 완료시 호출되는 RETURN 페이지 샘플 ( PHP EASYPAY / PAY2 )
	// - PAYCO 결제창에서 비밀번호 입력후, 호출 하여 처리
	// payco_return.php
	// 2016-12-02	PAYCO기술지원 <dl_payco_ts@nhnent.com>
	//--------------------------------------------------------------------------------

	//-----------------------------------------------------------------------------
	// 이 문서는 text/html 형태의 데이터를 반환합니다.
	//-----------------------------------------------------------------------------
	header('Content-type: text/html; charset:UTF-8');
	include(NM_PAYCO_PATH."/cfg/payco_config.php");

	//-----------------------------------------------------------------------------
	// 오류가 발생했는지 기억할 변수와 결과를 담을 변수를 선언합니다.
	//-----------------------------------------------------------------------------
	$doApproval = true;																// 기본적으로 주문예약 및 결제인증 받은것으로 설정

	$reserveOrderNo					= $_REQUEST["reserveOrderNo"];					// 주문 예약 번호
	$sellerOrderReferenceKey		= $_REQUEST["sellerOrderReferenceKey"];			// 외부가맹점에서 관리하는 주문연동Key
	$paymentCertifyToken			= $_REQUEST["paymentCertifyToken"];				// 결제인증토큰(결제승인시 필요)
	$totalPaymentAmt				= $_REQUEST["totalPaymentAmt"];					// 총결제금액
	$discountAmt					= $_REQUEST["discountAmt"];						// 쿠폰할인금액(PAYCO포인트 미포함 )
	// $totalRemoteAreaDeliveryFeeAmt	= $_REQUEST["totalRemoteAreaDeliveryFeeAmt"];	// 총 도서산간비( 추가배송비 )
	$pointAmt						= $_REQUEST["pointAmt"];						// PAYCO 포인트 사용금액
	
	$cartNo							= $_REQUEST["cartNo"];							// returnUrlParam 에서 던진 값을 수신( 장바구니 번호 )
	$totalTaxableAmt				= $_REQUEST["tmpTotalTaxableAmt"];				// returnUrlParam 에서 던진 값을 수신( 과세 )
	$totalVatAmt					= $_REQUEST["tmpTotalVatAmt"];					// returnUrlParam 에서 던진 값을 수신( 부과세 )		
	$totalTaxfreeAmt 				= $_REQUEST["tmpTotalTaxfreeAmt"];				// returnUrlParam 에서 던진 값을 수신( 면세 )
	
	//-----------------------------------------------------------------------------
	
	$code							= $_REQUEST["code"];							// 결과코드
	$message						= $_REQUEST["message"];							// 결과코드

	
	$cart_no												= tag_filter($cartNo);
	$total_taxable_amt										= tag_filter($totalTaxableAmt);
	$total_vat_amt											= tag_filter($totalVatAmt);
	$total_taxfree_amt										= tag_filter($totalTaxfreeAmt);

	$payco_reserve_order_no									= tag_filter($reserveOrderNo);
	$payco_seller_order_reference_key						= tag_filter($sellerOrderReferenceKey);
	$payco_payment_certify_token							= tag_filter($paymentCertifyToken);
	$payco_total_payment_amt								= tag_filter($totalPaymentAmt);
	$payco_discount_amt										= tag_filter($discountAmt);
	$totalRemoteAreaDeliveryFeeAmt							= "";// 도서산간비 필요없음
	$payco_point_amt										= tag_filter($pointAmt);

	// db_pay_order
	$payco_order = $payco_seller_order_reference_key;		// 취소시 $cart_no으로....
	
	//-----------------------------------------------------------------------------
	// Read_code 값이 0 또는 2222, 또는 실패시 오류코드 값 중 하나가 옵니다.
	//	 0 - 결제 인증 성공
	// 2222 - 사용자에 의한 결제 취소
	// 내역을 표시하고 창을 닫습니다.
	//-----------------------------------------------------------------------------
	
	//-----------------------------------------------------------------------------
	// (로그) 호출 시점과 호출값을 파일에 기록합니다.
	//-----------------------------------------------------------------------------
	// Write_Log("payco_return.php is Called - reserveOrderNo : $reserveOrderNo , sellerOrderReferenceKey : $sellerOrderReferenceKey , paymentCertifyToken : $paymentCertifyToken , totalPaymentAmt : $totalPaymentAmt , discountAmt : $discountAmt ,  totalRemoteAreaDeliveryFeeAmt : $totalRemoteAreaDeliveryFeeAmt , pointAmt : $pointAmt , code : $code");	
	// cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "", "", "reserveOrderNo : ".$reserveOrderNo." , sellerOrderReferenceKey : ".$sellerOrderReferenceKey." , paymentCertifyToken : ".$paymentCertifyToken." , totalPaymentAmt : ".$totalPaymentAmt." , discountAmt : ".$discountAmt." ,  totalRemoteAreaDeliveryFeeAmt : ".$totalRemoteAreaDeliveryFeeAmt." , pointAmt : ".$pointAmt." , code : ".$code, 0, "payco_return.php is Called", $nm_member); // state-0	

	//-----------------------------------------------------------------------------
	// response 값이 없으면 에러(ERROR)를 돌려주고 로그를 기록한 뒤 
	// 오류페이지를 보여주거나 주문되지 않았음을 고객에게 통보하는 페이지로 이동합니다.
	//-----------------------------------------------------------------------------

	// 상태체크 및 메세지
	$db_result['state'] = 's000';
	if($code != ""){	$db_result['state'] = $code; }
	$db_result['msg'] = 'payco 네트워크 오류';
	if($message != ""){	$db_result['msg'] = $message; }
	
	$ErrBoolean = $bSucc = false; // DB 정상처리 완료 여부
	$boolean_mpi = false;
	$boolean_cash = false;
	$boolean_sr = false;
	$randombox_coupon_use = false;

	if($code != 0){ 
		// Write_Log("payco_return.php ERROR - reserveOrderNo : $reserveOrderNo , sellerOrderReferenceKey : $sellerOrderReferenceKey , paymentCertifyToken : $paymentCertifyToken , totalPaymentAmt : $totalPaymentAmt , code : $code");		
		cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "", "", "reserveOrderNo : ".$reserveOrderNo." , sellerOrderReferenceKey : ".$sellerOrderReferenceKey." , paymentCertifyToken : ".$paymentCertifyToken." , totalPaymentAmt : ".$totalPaymentAmt." , code : ".$code, 1, "payco_return.php ERROR", $nm_member); // state-1	

		$db_result['state'] = 1;
		$payco_status = 'error';
		if($code == 2222){
			$db_result['state'] = 2;
			$db_result['msg'] = "주문을 취소하셨습니다.";
			$payco_status = 'cancel';
			$payco_order = $cart_no; // 취소시 $cart_no으로....
		}		
	
		$sql_payco_update_return = " UPDATE pg_channel_payco SET  
		payco_code = '".$code."', payco_message = '".$message."', payco_status = '".$payco_status."', payco_approval_date = '".NM_TIME_YMDHIS."' 
		WHERE payco_member = '".$nm_member['mb_no']."' AND payco_member_id = '".$nm_member['mb_id']."' AND payco_member_idx = '".$nm_member['mb_idx']."' AND 
		payco_order = '".$payco_order."' ";
		if(sql_query($sql_payco_update_return)){

		}else{
			// Write_Log("result.php(payco_return.php) is nexcube db no save  - customerOrderNumber : $payco_reserve_order_no , orderNo : $payco_order \n sql: $sql_payco_update_return");
			cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "sql_payco_update_return", "", $sql_payco_update_return, 2, "result.php(payco_return.php) is nexcube db no save", $nm_member); // state-2	
		}
	}else{
			
		//-----------------------------------------------------------------------------
		// 이곳에 위의 변수들을 이용해 가맹점에서 필요한 데이터를 처리합니다.
		// 예) 재고 체크, 매출금액 확인, 주문서 생성 , 총결제 금액 비교($totalPaymentAmt) , 외부가맹점 번호일치($sellerOrderReferenceKey) 등등
		//-----------------------------------------------------------------------------
		
		$ItemStock	= 10;							// DB 재고수량 , 연동 실패를 테스트 하시려면 값을 0 으로 설정하시고 정상으로 테스트 하시려면 1보다 큰 값을 넣으세요.
		
		$sql_total_payco_field_return = "SELECT count(*) as total_payco FROM pg_channel_payco ";
		$sql_payco_field_return = "SELECT * FROM pg_channel_payco ";
		$sql_total_payco_where_return = " WHERE 
		payco_member = '".$nm_member['mb_no']."' AND payco_member_id = '".$nm_member['mb_id']."' AND payco_member_idx = '".$nm_member['mb_idx']."' AND 
		payco_order = '".$payco_order."' ";

		$sql_total_payco_return = $sql_total_payco_field_return.$sql_total_payco_where_return;
		$sql_payco_return = $sql_payco_field_return.$sql_total_payco_where_return;

		$row_total_payco_return = sql_count($sql_total_payco_return, 'total_payco');
		if($row_total_payco_return != '1'){
			// Write_Log("result.php(payco_return.php) is nexcube db no select  - customerOrderNumber : $payco_order , orderNo : $payco_order \n sql: $sql_total_payco_return");
			cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "payco_order", "", $payco_order, 3, "result.php(payco_return.php) is nexcube db no select", $nm_member); // state-3	
		}else{
			$row_payco_return = sql_fetch($sql_payco_return);	
			$ItemTotalOrderAmt = intval($row_payco_return['payco_amt']);                
			// DB에서 주문시 주문했던 총 금액(PAYCO 에 주문예약할때 던졌던 값.)을 가져옵니다.(주문값) 
			// 연동 실패를 테스트 하시려면 값을 주문값을 totalPaymentAmt 값과 틀리게 설정하세요.
		}
		
		//-----------------------------------------------------------------------------
		// 수신 데이터 사용 예제2 ( 결제금액 위변조 확인 )
		//-----------------------------------------------------------------------------
		
		/*
		
		★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
		★★★★★★★★★★															              ★★★★★★★★★★
		★★★★★★★★★★                						중요 사항 				                      ★★★★★★★★★★
		★★★★★★★★★★                        										                             ★★★★★★★★★★
		★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★ ★★★★★★★★★★
		
		 총 금액 결제된 금액을 주문금액과 비교.(반드시 필요한 검증 부분.)
		 주문금액을 변조하여 결제를 시도 했는지 확인함.(반드시 DB에서 읽어야 함.)
		 결제승인 요청 전에 확인 해야 합니다.
		 ( 금액이 변조되었으면 반드시 승인을 취소해야 함. )

		★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
		
		*/	
		
		if( $totalPaymentAmt != $ItemTotalOrderAmt ){    // 위에서 파라메터로 받은 totalPaymentAmt 값과 주문값이 같은지 비교합니다.
			
			// Write_Log("payco_return.php 결제금액 위변조 확인 > DB 총 주문금액 : ".$ItemTotalOrderAmt." 원 과 PAYCO 결제금액 : ".$totalPaymentAmt." 원 이 같지 않습니다.");
			cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "", "", "주문금액 : ".$ItemTotalOrderAmt." 원 과 PAYCO 결제금액 : ".$totalPaymentAmt." 원 이 같지 않습니다.", 4, "payco_return.php 결제금액 위변조 확인 > DB 총 주문금액", $nm_member); // state-4	
			
			$doApproval = false;   						// DB 총 주문금액과 PAYCO결제금액이 다르다면 오류로 설정
			
			//-----------------------------------------------------------------------------
			//오류일 경우 오류페이지를 표시하거나 결제되지 않았음을 고객에게 통보합니다.
			//-----------------------------------------------------------------------------
			
			$db_result['state'] = 3;
			$payco_status = 'error';
		}	
		//★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
			
		
		
		if ( $doApproval == true ){
			// 결제승인요청 상태저장
			$sql_payco_update_return = " UPDATE pg_channel_payco SET  
			payco_status = 'approval', payco_approval_date = '".NM_TIME_YMDHIS."' 
			WHERE payco_member = '".$nm_member['mb_no']."' AND payco_member_id = '".$nm_member['mb_id']."' AND payco_member_idx = '".$nm_member['mb_idx']."' AND 
			payco_order = '".$payco_order."' ";
			if(sql_query($sql_payco_update_return)){

			}else{
				$db_result['state'] = 4;
				$db_result['msg'] = '결제승인요청 DB에러';
				// Write_Log("result.php(payco_return.php) is nexcube db no save  - customerOrderNumber : $payco_order , orderNo : $payco_order \n sql: $sql_payco_update_return");				
				cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "payco_order", "", $payco_order, 5, "result.php(payco_return.php) is nexcube db no select", $nm_member); // state-5	
			}
		
			
			
			//-----------------------------------------------------------------------------
			// 확인결과(재고확인,결제금액 위변조 확인)가 정상이면 PAYCO 에 결제 승인을 요청
			//-----------------------------------------------------------------------------
			//-----------------------------------------------------------------------------
			// 결제 승인 요청에 담을 JSON OBJECT를 선언합니다.
			//-----------------------------------------------------------------------------		
			
			$approvalOrder["sellerKey"]					= $sellerKey;					// 가맹점 코드. payco_config.php 에 설정
			$approvalOrder["reserveOrderNo"]			= $reserveOrderNo;				// 예약주문번호.
			$approvalOrder["paymentCertifyToken"]		= $paymentCertifyToken;			// 결제인증토큰.
			$approvalOrder["sellerOrderReferenceKey"]	= $sellerOrderReferenceKey;		// 외부가맹점에서 관리하는 주문연동Key		
			$approvalOrder["totalPaymentAmt"]			= $totalPaymentAmt;				// 주문 총 금액.		
					
			$Result = payco_approval(stripslashes(json_encode($approvalOrder)));  		// 배열을 JSON 형식으로 변환 및 백슬래시 제거 후에, 결제 승인 요청함.
			// Write_Log("payco_return.php  payco_approval -  Result : $Result");  
			// cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "Result", "", "주문금액 : ".$Result, 0, "payco_return.php  payco_approval", $nm_member); // state-0	
			        
				
			$Read_Data = json_decode($Result, true);                           			// JSON 형식의 전달받은 값을, 배열로 변환. 
			
			/* 181017-취소필수값저장 */
			if ( $doApproval == true ){
				$orderCertifyKey_require					= $Read_Data["result"]["orderCertifyKey"];					// 주문인증키
				$totalPaymentAmt_require					= $totalPaymentAmt;											// 주문 총 금액.
				
				// 결제승인요청 상태저장
				$sql_payco_update_approval_require = " UPDATE pg_channel_payco SET  
				payco_payment_order_certify_key = '$orderCertifyKey_require' 
				WHERE payco_member = '".$nm_member['mb_no']."' AND payco_member_id = '".$nm_member['mb_id']."' AND payco_member_idx = '".$nm_member['mb_idx']."' AND 
				payco_order = '".$payco_order."' ";
				if($orderCertifyKey_require !=''){
					if(sql_query($sql_payco_update_approval_require)){
					}else{
						$db_result['state'] = 51;
						$db_result['msg'] = 'sql_payco_update_approval_require 에러';
						// Write_Log("result.php(payco_return.php) is nexcube db no save  - customerOrderNumber : $payco_order , orderNo : $payco_order \n sql: $sql_payco_update_return");				
						cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "sql_payco_update_approval_require", "", $sql_payco_update_approval_require, 51, "result.php(payco_return.php) is nexcube db no update", $nm_member); // state-5	
					
					}
				}else{
					$db_result['state'] = 52;
					$db_result['msg'] = 'payco_payment_order_certify_key 에러';
					// Write_Log("result.php(payco_return.php) is nexcube db no save  - customerOrderNumber : $payco_order , orderNo : $payco_order \n sql: $sql_payco_update_return");				
					cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "payco_update_approval_require", "", $db_result['msg'], 52, "result.php(payco_return.php) is nexcube db no select", $nm_member); // state-5	
				}
			}
			/* 181017-취소필수값저장 end */
			
			if($Read_Data["code"] == 0){

				//-----------------------------------------------------------------------------
				// 결제 승인 수신 데이터 사용
				//-----------------------------------------------------------------------------
				
				$code								= $Read_Data["code"];										// 결과코드
				$message							= $Read_Data["message"];									// 거래 메세지

				// $sellerOrderReferenceKey			= $Read_Data["result"]["sellerOrderReferenceKey"];			// 가맹점에서 발급했던 주문 연동 Key
				// $reserveOrderNo						= $Read_Data["result"]["reserveOrderNo"];					// PAYCO에서 발급한 주문예약번호
				$orderNo							= $Read_Data["result"]["orderNo"];							// PAYCO에서 발급한 주문번호
				$memberName							= $Read_Data["result"]["memberName"];						// 주문자명
				$memberEmail						= $Read_Data["result"]["memberEmail"];						// 주문자EMAIL
				$orderChannel						= $Read_Data["result"]["orderChannel"];						// 주문자EMAIL
				// $totalOrderAmt						= $Read_Data["result"]["totalOrderAmt"];					// 총 주문 금액
				// $totalDeliveryFeeAmt				= $Read_Data["result"]["totalDeliveryFeeAmt"];				// 총 배송비 금액
				// $totalRemoteAreaDeliveryFeeAmt		= $Read_Data["result"]["totalRemoteAreaDeliveryFeeAmt"];	// 총 추가배송비 금액
				// $totalPaymentAmt					= $Read_Data["result"]["totalPaymentAmt"];					// 총 결제 금액
				$paymentCompletionYn 				= $Read_Data["result"]["paymentCompletionYn"];				// 결제완료여부 
				// orderProducts																				// 주문상품 List
				// paymentDetails																				// 결제내역 List
				$orderCertifyKey 					= $Read_Data["result"]["orderCertifyKey"];					// 주문인증키
				
				// //////////////////////// database 변수 담기 ////////////////////////
				$payco_approval_order = $orderNo;
				$payco_member_name = $memberName; 
				$payco_member_email = $memberEmail; 
				$payco_order_channel = $memberEmail;
				$payco_payment_completion_yn = $paymentCompletionYn; 
				$payco_payment_order_certify_key = $orderCertifyKey; 
				
				// orderProducts 주문상품 List
				// $orderProductNo						= $Read_Data["result"]["orderProducts"][0]["orderProductNo"];					// 주문상품번호
				// $cpId								= $Read_Data["result"]["orderProducts"][0]["cpId"];								// 상점ID
				// $productId							= $Read_Data["result"]["orderProducts"][0]["productId"];						// 상품ID
				// $sellerOrderProductReferenceKey     = $Read_Data["result"]["orderProducts"][0]["sellerOrderProductReferenceKey"];	// 가맹점에서 관리하는 	주문상품 연동key 필요없음
				$orderProductStatusCode				= $Read_Data["result"]["orderProducts"][0]["orderProductStatusCode"];			// 주문상품 상태코드
				$orderProductStatusName				= $Read_Data["result"]["orderProducts"][0]["orderProductStatusName"];			// 주문상품 상태명
				// $productKindCode					= $Read_Data["result"]["orderProducts"][0]["productKindCode"];					// 상품 타입
				// $productPaymentAmt					= $Read_Data["result"]["orderProducts"][0]["productPaymentAmt"];				// 상품결제금액 : 배송비 상품의 경우
				// $originalProductPaymentAmt			= $Read_Data["result"]["orderProducts"][0]["originalProductPaymentAmt"];		// 원 상품결제금액
				
				// //////////////////////// database 변수 담기 ////////////////////////
				$payco_order_product_status_code = $orderProductStatusCode;
				$payco_order_product_status_name = $orderProductStatusName;
						
				// paymentDetails 결제내역 List
				$paymentTradeNo						= $Read_Data["result"]["paymentDetails"][0]["paymentTradeNo"];					// 결제번호
				$paymentMethodCode					= $Read_Data["result"]["paymentDetails"][0]["paymentMethodCode"];				// 결제수단코드
				$paymentMethodName					= $Read_Data["result"]["paymentDetails"][0]["paymentMethodName"];				// 결제수단명
				$paymentAmt							= $Read_Data["result"]["paymentDetails"][0]["paymentAmt"];						// 결제금액
				$tradeYmdt							= $Read_Data["result"]["paymentDetails"][0]["tradeYmdt"];						// 결제일시 (yyyyMMddHHmmss)
				$pgAdmissionNo						= $Read_Data["result"]["paymentDetails"][0]["pgAdmissionNo"];					// PG승인번호
				$pgAdmissionYmdt					= $Read_Data["result"]["paymentDetails"][0]["pgAdmissionYmdt"];					// PG승인일시 (yyyyMMddHHmmss)
				$easyPaymentYn						= $Read_Data["result"]["paymentDetails"][0]["easyPaymentYn"];					// 간편결제여부 (Y/N)
				
				// //////////////////////// database 변수 담기 ////////////////////////
				$payco_payment_trade_no = $paymentTradeNo;
				$payco_payment_method_code = $paymentMethodCode; 
				$payco_payment_method_name = $paymentMethodName;
				$payco_payment_amt = $paymentAmt; 
				$payco_trade_ymdt = $tradeYmdt; 
				$payco_pg_admission_no = $pgAdmissionNo; 
				$payco_pg_admission_ymdt = $pgAdmissionYmdt; 
				$payco_easy_payment_yn = $easyPaymentYn; 

				// cardSettleInfo																									// 신용카드 결제 정보
				// cellphoneSettleInfo																								// 핸드폰 결제 정보	
				// realtimeAccountTransferSettleInfo																				// 실시간계좌이체 결제정보
				// nonBankbookSettleInfo																							// 무통장입금 결제정보
				// couponSettleInfo																									// 쿠폰정보
			
				// cardSettleInfo 결제내역 신용카드 결제 정보 List
				$cardCompanyName				= $Read_Data["result"]["paymentDetails"][0]["cardSettleInfo"]["cardCompanyName"];				// 카드사명
				$cardCompanyCode				= $Read_Data["result"]["paymentDetails"][0]["cardSettleInfo"]["cardCompanyCode"];				// 카드사코드
				$cardNo							= $Read_Data["result"]["paymentDetails"][0]["cardSettleInfo"]["cardNo"];						// 카드번호
				$cardInstallmentMonthNumber		= $Read_Data["result"]["paymentDetails"][0]["cardSettleInfo"]["cardInstallmentMonthNumber"];	// 할부개월(MM)
				$cardAdmissionNo				= $Read_Data["result"]["paymentDetails"][0]["cardSettleInfo"]["cardAdmissionNo"];				// 카드사 승인번호
				$cardInterestFreeYn				= $Read_Data["result"]["paymentDetails"][0]["cardSettleInfo"]["cardInterestFreeYn"];			// 무이자여부(Y/N)
				$corporateCardYn				= $Read_Data["result"]["paymentDetails"][0]["cardSettleInfo"]["corporateCardYn"];				// 법인카드여부(개인 N ,법인 Y)
				$partCancelPossibleYn			= $Read_Data["result"]["paymentDetails"][0]["cardSettleInfo"]["partCancelPossibleYn"];			// 부분취소가능유무(Y/N)

				// //////////////////////// database 변수 담기 ////////////////////////
				$payco_card_company_name = $cardCompanyName;
				$payco_card_company_code = $cardCompanyCode; 
				$payco_card_no = $cardNo; 
				$payco_card_installment_month_number = $cardInstallmentMonthNumber; 
				$payco_card_admission_no = $cardAdmissionNo; 
				$payco_cardInterestFree_yn = $cardInterestFreeYn; 
				$payco_corporate_card_yn = $corporateCardYn; 
				$payco_part_cancel_possible_yn = $partCancelPossibleYn;
				
				// cellphoneSettleInfo 핸드폰 결제 정보 List
				$cellphoneNo					= $Read_Data["result"]["paymentDetails"][0]["cellphoneSettleInfo"]["cellphoneNo"];				// 휴대폰번호
				// //////////////////////// database 변수 담기 ////////////////////////
				$payco_cellphone_no = $cellphoneNo;

				// realtimeAccountTransferSettleInfo 실시간계좌이체 정보 List
				$bankName						= $Read_Data["result"]["paymentDetails"][0]["realtimeAccountTransferSettleInfo"]["bankName"];	// 은행명
				$bankCode						= $Read_Data["result"]["paymentDetails"][0]["realtimeAccountTransferSettleInfo"]["bankCode"];	// 은행코드

				// nonBankbookSettleInfo 무통장입금 결제정보 List
				if(count($Read_Data["result"]["paymentDetails"][0]["nonBankbookSettleInfo"]) > 1){
					$bankName						= $Read_Data["result"]["paymentDetails"][0]["nonBankbookSettleInfo"]["bankName"];				// 은행명
					$bankCode						= $Read_Data["result"]["paymentDetails"][0]["nonBankbookSettleInfo"]["bankCode"];				// 은행코드
				}
				$accountNo						= $Read_Data["result"]["paymentDetails"][0]["nonBankbookSettleInfo"]["accountNo"];				// 계좌번호
				$paymentExpirationYmd			= $Read_Data["result"]["paymentDetails"][0]["nonBankbookSettleInfo"]["paymentExpirationYmd"];	// 입금만료일

				// //////////////////////// database 변수 담기 ////////////////////////
				$payco_bank_name = $bankName;
				$payco_bank_code = $bankCode;
				$payco_account_no = $accountNo;
				$payco_payment_expiration_ymd = $paymentExpirationYmd;

				// couponSettleInfo 쿠폰정보 List
				// $discountAmt					= $Read_Data["result"]["paymentDetails"][0]["couponSettleInfo"]["discountAmt"];					// 쿠폰할인금액(위에서 가져옴)
				$discountConditionAmt			= $Read_Data["result"]["paymentDetails"][0]["couponSettleInfo"]["discountConditionAmt"];		// 쿠폰할인조건
				// //////////////////////// database 변수 담기 ////////////////////////
				$payco_discount_condition_amt = $discountConditionAmt;

				//-----------------------------------------------------------------------------
				// <<<< 시작 >>>>
				// DB 저장중 오류가 발생하였으면 , 전체 취소 API( payco_cancel.php )를 호출 합니다. 
				//
				//-----------------------------------------------------------------------------
					
					//$ErrBoolean = true;              //  기본 적으로  DB 저장 성공으로 설정
					$ErrBoolean = false;              //  기본 적으로  DB 저장 성공으로 설정
				

				// 결제승인요청 상태저장
				$sql_payco_update_complete = " UPDATE pg_channel_payco SET  
				payco_status = 'complete', payco_complete_date = '".NM_TIME_YMDHIS."', 
				payco_code = '$code', payco_message = '$message', 

				payco_approval_order = '$payco_approval_order', 
				payco_seller_order_reference_key = '$payco_seller_order_reference_key',  
				payco_payment_certify_token = '$payco_payment_certify_token',  
				payco_total_payment_amt = '$payco_total_payment_amt',  
				payco_point_amt = '$payco_point_amt',  
				payco_member_name = '$payco_member_name',  
				payco_member_email = '$payco_member_email',  
				payco_order_channel = '$payco_order_channel',  
				payco_payment_completion_yn = '$payco_payment_completion_yn', 
				payco_payment_order_certify_key = '$payco_payment_order_certify_key', 

				payco_order_product_status_code = '$payco_order_product_status_code', 
				payco_order_product_status_name = '$payco_order_product_status_name', 

				payco_payment_trade_no = '$payco_payment_trade_no',  
				payco_payment_method_code = '$payco_payment_method_code',  
				payco_payment_method_name = '$payco_payment_method_name', 
				payco_payment_amt = '$payco_payment_amt',  
				payco_trade_ymdt = '$payco_trade_ymdt',  
				payco_pg_admission_no = '$payco_pg_admission_no',  
				payco_pg_admission_ymdt = '$payco_pg_admission_ymdt',  
				payco_easy_payment_yn = '$payco_easy_payment_yn',  

				payco_card_company_name = '$payco_card_company_name',  
				payco_card_company_code = '$payco_card_company_code',  
				payco_card_no = '$payco_card_no',  
				payco_card_installment_month_number = '$payco_card_installment_month_number',  
				payco_card_admission_no = '$payco_card_admission_no',  
				payco_cardInterestFree_yn = '$payco_cardInterestFree_yn',  
				payco_corporate_card_yn = '$payco_corporate_card_yn',  
				payco_part_cancel_possible_yn = '$payco_part_cancel_possible_yn',  

				payco_cellphone_no = '$payco_cellphone_no',  

				payco_bank_name = '$payco_bank_name',  
				payco_bank_code = '$payco_bank_code',  
				payco_account_no = '$payco_account_no',  
				payco_payment_expiration_ymd = '$payco_payment_expiration_ymd',  

				payco_discount_amt = '$payco_discount_amt',  
				payco_discount_condition_amt = '$payco_discount_condition_amt'  

				WHERE payco_member = '".$nm_member['mb_no']."' AND payco_member_id = '".$nm_member['mb_id']."' AND payco_member_idx = '".$nm_member['mb_idx']."' AND 
				payco_order = '".$payco_order."' ";

				if(!($code != 0 || $payco_payment_amt == ""  || $payco_payment_amt == "0" || $payco_payment_amt == 0 || $payco_reserve_order_no == "" || $payco_order == "")){ // 상태값 , 결제금액, 거래번호, 주문번호
					/* 181001 */
					if(sql_query($sql_payco_update_complete)){
						// payco 저장 정보 가져오기
						$row_payco = cs_pg_channel_payco($nm_member, $payco_order);	  
						$mpi_won = intval($row_payco['payco_amt']);
						$mpi_cash_point = intval($row_payco['payco_cash_point'] + $row_payco['payco_event_cash_point']);
						$mpi_point = intval($row_payco['payco_point'] + $row_payco['payco_event_point']);
						$mpi_from = "payco(".$payco_payment_method_name.")[".$row_payco['payco_product']."]";
						
						// 무통장 입금시
						if($payco_payment_method_code == "02"){
							$bSucc = true;
							// Write_Log("payco_payment_method_code: ".$payco_payment_method_code." ".$_SERVER['PHP_SELF']);
							cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "payco_payment_method_code", "", $payco_payment_method_code, 6, "payco_payment_method_code", $nm_member); // state-6
							
						}else{
							// 새로고침시 또 부여되어서 검사
							$row_mpi = cs_member_point_income($nm_member, $payco_order);
							if(count($row_mpi) > 1){
							  $bSucc = $ErrBoolean = true;
							}else{
								// 회원 포인트 부여
								$bSucc = $ErrBoolean = $boolean_mpi = cs_set_member_point_income($nm_member, $mpi_cash_point, $mpi_point, $mpi_won, $mpi_from, $payco_order);
								if($boolean_mpi == false){ 
									//실패 로그 남기기						
									// Write_Log("function cs_set_member_point_income error".$_SERVER['PHP_SELF']);								
									cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "", "cs_set_member_point_income", "", 7, "회원 포인트 부여 실패", $nm_member); // state-7
								}

								// 회원 캐쉬
								$bSucc = $ErrBoolean = $boolean_cash = mb_set_cash($nm_member, $row_payco, 'payco');
								if($boolean_cash == false){ 
									//실패 로그 남기기 
									// Write_Log("function mb_set_cash error".$_SERVER['PHP_SELF']);
									cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "", "mb_set_cash", "", 8, "회원 캐쉬 실패", $nm_member); // state-8
								}

								// 회원 포인트 통계
								$bSucc = $ErrBoolean = $boolean_sr = cs_set_sales_recharge($nm_member, $row_payco, 'payco');
								if($boolean_sr == false){ 
									//실패 로그 남기기 
									// Write_Log("function cs_set_sales_recharge error".$_SERVER['PHP_SELF']);
									cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "", "cs_set_sales_recharge", "", 9, "회원 포인트 통계 실패", $nm_member); // state-9
								}
			  
							  // 회원 랜덤박스 할인권 사용시 사용처리
								if(intval($coupondc)>0 && $coupondc != '') {
									$randombox_coupon_use = false;

									// 가격 가져오기
									$sql_crp = "SELECT * FROM config_recharge_price WHERE crp_state = 'y' AND crp_no = '".$row_payco['payco_crp_no']."'";
									$row_crp = sql_fetch($sql_crp);

									// 1.이벤트결제상품(첫결제), 2.50땅콩할인제외
									if($row_crp['crp_cash_count_event'] != '') {
										/* 2. 이벤트 상품 제외 - 예시 첫결제 */
									} else if(intval($row_crp['crp_cash_point']) < 50 ) {
										/* 3. 50땅콩 미만인 결제 상품에는 보너스 지급 안되도록 2017-08-31 */	
									} else { 
										$randombox_coupon_use = true; 
									} // end else

									if($randombox_coupon_use == true) {
										randombox_coupon_use($nm_member, $coupondc, $payway_no, $row_payco['payco_crp_no']);		  	
									} // end if
								} // end if
							}
						}
					}else{
						$db_result['state'] = 5;
						$db_result['msg'] = '결제완료요청 DB에러';
						// Write_Log("result.php(payco_return.php) is nexcube db no save  - customerOrderNumber : $payco_order , orderNo : $payco_order \n sql: $sql_payco_update_complete");
						cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "sql_payco_update_complete", "", $sql_payco_update_complete, 10, "결제승인요청 상태저장 실패", $nm_member); // state-10
					}
				}else{
					/* 181001 */
					$db_result['state'] = 99;
					$db_result['msg'] = 'payco 네트워크 오류';
					// Write_Log("result.php(payco_return.php) is nexcube db no save  - customerOrderNumber : $payco_order , orderNo : $payco_order \n sql: $sql_payco_update_complete");
					cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "sql_payco_null", "", $sql_payco_update_complete, 99, "payco 정보 오류 실패", $nm_member); // state-10
				}

				//------------------------------------------------------------------------------
				// $ErrBoolean 상태값에 따른 결과값 생성
				//------------------------------------------------------------------------------
				if($ErrBoolean == false){
					$resultValue = "ERROR";		//오류가 있으면 ERROR를 설정
				} else {
					$resultValue = "OK";		//오류가 없으면 OK 설정
				}
				
			
				
				if ($resultValue == "ERROR"){ // 나중에;;
					/*
					// 결제상세 조회(검증용)API 호출 - 승인완료 후 DB 저장시 오류 났을 경우, 결제취소시에 사용합니다.
					$detailForVerify = array();
					$detailForVerify["sellerKey"] 				= $sellerKey;					// 가맹점 코드. payco_config.php 에 설정
					$detailForVerify["reserveOrderNo"] 			= $reserveOrderNo; 				// 예약주문번호.
					$detailForVerify["sellerOrderReferenceKey"] = $sellerOrderReferenceKey; 	// 외부가맹점에서 관리하는 주문연동Key
				
					$detailForVerify_Result = payco_detailForVerify(stripslashes(json_encode($detailForVerify)));  		// 배열을 JSON 형식으로 변환 및 백슬래시 제거 후에, 결제 승인 요청함.
					$detailForVerify_Read_Data = json_decode($detailForVerify_Result, true);                  			// JSON 형식의 전달받은 값을, 배열로 변환.
				
					//-----------------------------------------------------------------------------
					//결제 취소 API 호출 ( PAYCO 에서 받은 결제정보를 이용해 전체 취소를 합니다. )
					// 취소 내역을 담을 JSON OBJECT를 선언합니다.
					//------------------------------------------------------------------------------
					$cancelOrder = array();
					$cancelOrder["sellerKey"]					= $sellerKey;							// 가맹점 코드. payco_config.php 에 설정
					$cancelOrder["sellerOrderReferenceKey"]		= $detailForVerify_Read_Data["result"]["sellerOrderReferenceKey"];				// 취소주문연동키. ( 파라메터로 넘겨 받은 값 )
					$cancelOrder["orderCertifyKey"]				= $detailForVerify_Read_Data["result"]["orderCertifyKey"];						// 주문완료통보시 내려받은 인증값
					$cancelOrder["cancelTotalAmt"]				= $detailForVerify_Read_Data["result"]["totalPaymentAmt"];						// PAYCO 주문서의 총 금액을 입력합니다. (전체취소, 부분취소 전부다)
			
					Write_Log("payco_return.php is DB Error : try cancel : ".$cancelOrder);
					
					$cancel_Result = payco_cancel(stripslashes(json_encode($cancelOrder)));
					*/
				}
		
			//-----------------------------------------------------------------------------
			//
			// DB 저장중 오류가 발생하였으면 , 전체 취소 API( payco_cancel.php )를 호출 합니다.  
			// <<<< 끝 >>>>
			//-----------------------------------------------------------------------------
			
			
			
			}else if($Read_Data["code"] == 4005){ // 중복
				
				$db_result['state'] = 6;
				$db_result['msg'] = '중복결제';

			}else{ // 결제 승인 실패			
				$db_result['state'] = 7;
				$db_result['msg'] = '결제 승인 실패';	
			}	

					
			
			//-----------------------------------------------------------------------------
			// ...
			// 결제 승인 수신 데이터를 이용하여 결제 승인후 처리할 부분을 이곳에 작성합니다.
			// ...
			//-----------------------------------------------------------------------------
			
			


		}else{
			//-----------------------------------------------------------------------------
			//
			// 오류일 경우 오류페이지를 표시하거나 결제되지 않았음을 고객에게 통보합니다.
			// 팝업창 닫기 또는 구매 실패 페이지 작성 ( 팝업창 닫을때 Opener 페이지 이동 등 )
			//
			//-----------------------------------------------------------------------------
			//결제 인증 후 내부 오류가 있어 승인은 받지 않았습니다. 오류내역을 여기에 표시하세요. 예) 재고 수량이 부족합니다.
			
			if($code != 0){ // 주문 실패
				 // Write_Log("payco_return.php ERROR - reserveOrderNo : $reserveOrderNo , sellerOrderReferenceKey : $sellerOrderReferenceKey , paymentCertifyToken : $paymentCertifyToken , totalPaymentAmt : $totalPaymentAmt , code : $code");
				 cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "", "", "reserveOrderNo : ".$reserveOrderNo." , sellerOrderReferenceKey : ".$sellerOrderReferenceKey." , paymentCertifyToken : ".$paymentCertifyToken." , totalPaymentAmt : ".$totalPaymentAmt." , code : ".$code, 11, "payco_return.php ERROR", $nm_member); // state-11				
			}
		}
	}
	// 취소 처리 위에서 처리
	/*
	if($db_result['state'] == 2){ 
		if(is_mobile()){ 
			cs_alert($db_result['msg'], $ret_url); 
		}else{ 
			cs_alert_close($db_result['msg']); 
		}
		die;
		$db_result['msg'] = "주문을 취소하셨습니다.";
	}
	*/
	
	/* 181001 */
	// $bSucc = false;
	// $Read_Data["code"] = 1111;
	if($bSucc == false)
	{
		if($Read_Data["code"] == 4005 || $code == 2222){ // 새로고침 || 사용자취소
			if($code == 2222){ // 사용자취소
				$Read_Data["code"] = $code;
				$Read_Data["message"] = $db_result['msg'] = "주문을 취소하셨습니다.";
			}
			cs_del_pg_member_order($nm_member); // 주문번호 삭제
		}else{
			sleep(2); // 2초 지연
			$transaction['reserveOrderNo'] = $reserveOrderNo;
			$transaction['sellerOrderReferenceKey'] = $sellerOrderReferenceKey;
			$transaction['sellerKey'] = $sellerKey;
			
			/* 181017-취소필수값저장 */
			$transaction['orderCertifyKey'] = $orderCertifyKey_require;
			$transaction['totalPaymentAmt'] = $totalPaymentAmt_require;
			/* 181017-취소필수값저장 end */
			// $db_result = cs_recharge_save_cancel($nm_member, 'payco', $_SESSION['recharge_order_no'], $transaction, $coupondc, $boolean_mpi, $boolean_cash, $boolean_sr, $randombox_coupon_use);	
			// 181024 주문번호-캔슬용 삭제 : 세션처리했었는데 뒤로가기할때 문제가 발생되어 DB 저장하기로 함
			$cs_get_pg_member_order = cs_get_pg_member_order($nm_member); // 주문번호 가져오기
			cs_del_pg_member_order($nm_member); // 주문번호 삭제
			// $db_result = cs_recharge_save_cancel($nm_member, 'payco', $cs_get_pg_member_order, $transaction, $coupondc, $boolean_mpi, $boolean_cash, $boolean_sr, $randombox_coupon_use);	

			// $Read_Data["code"] = $db_result['state'];
			// $Read_Data["message"] = $db_result['msg'];		

			// 취소 x-테스트 - 181120
			$nm_cancel_member = mb_get($nm_member['mb_id']);  // 취소전에 데이터 갱신
			$row_mpi_check = cs_member_point_income($nm_cancel_member, $cs_get_pg_member_order);

			// 기록 땅콩과 현재 땅콩이 같으면 취소 패스			
			if(intval($row_mpi_check['mpi_no']) > 0){

				$x_point = "mpi_cpb:".$row_mpi_check['mpi_cash_point_balance']."||";
				$x_point.= "mb_cp:".$nm_cancel_member['mb_cash_point']."||";
				$x_point.= "mpi_pb:".$row_mpi_check['mpi_point_balance']."||";
				$x_point.= "mb_p:".$nm_cancel_member['mb_point'];

				if($row_mpi_check['mpi_cash_point_balance'] == $nm_cancel_member['mb_cash_point'] && $row_mpi_check['mpi_point_balance'] == $nm_cancel_member['mb_point'] ){
					$sql_x_pg_cancel = " 
					INSERT INTO x_pg_cancel (x_cancel_member, x_cancel_from, x_cancel_order, 
					x_mpi_no, x_point, x_mpi_state, 
					x_cancel_mode, x_cancel_useragent, x_cancel_server_ip, x_cancel_date 
					)VALUES( '".$nm_member['mb_no']."', '".'payco'."', '".$cs_get_pg_member_order."', 
					'".$row_mpi_check['mpi_no']."', '".$x_point."', '1', 
					'".$nm_config['nm_mode']."', '".HTTP_USER_AGENT."', '".$_SERVER['SERVER_ADDR']."', '".NM_TIME_YMDHIS."' )
					";
					@mysql_query($sql_x_pg_cancel);
				}else{
					$sql_x_pg_cancel = " 
					INSERT INTO x_pg_cancel (x_cancel_member, x_cancel_from, x_cancel_order, 
					x_mpi_no, x_point, x_mpi_state, 
					x_cancel_mode, x_cancel_useragent, x_cancel_server_ip, x_cancel_date 
					)VALUES( '".$nm_member['mb_no']."', '".'payco'."', '".$cs_get_pg_member_order."', 
					'".$row_mpi_check['mpi_no']."', '".$x_point."', '2', 
					'".$nm_config['nm_mode']."', '".HTTP_USER_AGENT."', '".$_SERVER['SERVER_ADDR']."', '".NM_TIME_YMDHIS."' )
					";
					@mysql_query($sql_x_pg_cancel);
				    // 위 테스트가 제대로 돌아간다면 주석 풀기
					// $db_result = cs_recharge_save_cancel($nm_cancel_member, 'payco', $cs_get_pg_member_order, $transaction, $coupondc, $boolean_mpi, $boolean_cash, $boolean_sr, $randombox_coupon_use);	
					// $Read_Data["code"] = $db_result['state'];
					// $Read_Data["message"] = $db_result['msg'];	
				}
			}
			// 취소 x-테스트 end - 181120

			$nm_member = mb_get($nm_member['mb_id']);
			
			
		}
	}else{
		cs_del_pg_member_order($nm_member); // 주문번호 삭제
	}


	/* 결제 보여주기에서 사용할 변수 정의 */
	$row_view 							= cs_pg_channel_payco($nm_member, $payco_order);
	$row_view_mb 						= mb_get($nm_member['mb_id']);
	
	$view_order							= $row_view['payco_order'];				// 주문번호
	$view_product						= $row_view['payco_product'];			// 상품명
	$view_amt								= $row_view['payco_amt'];				// 결제금액
	$view_date							= $row_view['payco_complete_date'];		// 결제시간
	$view_recharge_cash     = $row_view['payco_cash_point'];// 충전땅콩
	$view_recharge_point    = $row_view['payco_point'];     // 충전 미니땅콩
    
	$view_cash_point		= number_format($row_view_mb['mb_cash_point']);
	$view_point				= number_format($row_view_mb['mb_point']);
	
	$view_payway = $nm_config['cf_payway'][$payway_no][2]."(".$row_view['payco_payment_method_name'].")";			// 결제방법
	if($Read_Data["code"] == 4005){ // 새로고침
		$view_payway = $nm_config['cf_payway'][$payway_no][2]."(".$row_view['payco_payment_method_name'].")[F5]";			// 결제방법-새로고침
		$view_payway	.= "<br/>".$Read_Data["message"]."(code=".$Read_Data["code"].")";
	}
	
	$receipt_text_error = "";								// 결제 상단 메세지
	$receipt_state = 0;
	if($Read_Data["code"] != '0'){
		$receipt_text_error = "결제가 정상적으로 되지 않았습니다.";
		$receipt_text_error.= "<br/>".$Read_Data["message"]."<br/>"."(code=".$Read_Data["code"].")"; 
		$receipt_state = 1;
	}
	/* 결제 보여주기 */
	include_once NM_PROC_PATH.'/recharge_receipt.php';

?>