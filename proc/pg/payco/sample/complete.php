<?PHP

	//-------------------------------------------------------------------------------
	// 가맹점 Return 페이지에서 호출한 opener 페이지 샘플 ( PHP )
	// complete.php
	// 2015-08-26	PAYCO기술지원 <dl_payco_ts@nhnent.com>
	//-------------------------------------------------------------------------------
	header('Content-type: text/html; charset: UTF-8');

	include("payco_config.php");


	//---------------------------------------------------------------------------------
	// 결제창이 닫혔습니다.
	// 완료페이지를 이곳에 작성하시면 됩니다.
	// 고객이 주문한 주문 리스트를 나열하시면 됩니다. (상품링크 또는 주문상태포함 등등...)
	// 이 페이지는 PAYCO 결제창에서 가맹점의 Return URL이 정상적으로 호출이 되어야 표시되는 페이지 입니다.
	// 고객이 PAYCO 결제창을 강제로 닫으면 호출이 안될 수 있습니다.
	//---------------------------------------------------------------------------------

?>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="0">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">

<meta name="keyword" content="컨텐츠">

<title>PAYCO_DEMOWEB COMPLETE(PHP - EASYPAY PAY2)</title>

<link href="css/common.css" rel="stylesheet" type="text/css">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!--
<script src="/share/js/requirejs/require.js"></script>
<script src="/share/js/requirejs/require.config.js"></script>
-->
<script type="text/javascript" src="https://static-bill.nhnent.com/payco/checkout/js/payco.js" charset="UTF-8"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">

function order_state_modify(){
    // 선택박스 필수 옵션을 체크 함
	if( $('#orderNo').val() == "" ) {
        alert('주문번호를 입력해주세요.');
        return false;
	}

	if( $('#sellerOrderProductReferenceKey').val() == "" ) {
        alert('주문상품연동키를 입력해주세요.');
        return false;
	}

	if( $('#orderProductStatus option:selected').val() == "" ) {
        alert('상태값을 선택해주세요.');
        return false;
    }

    // 선택박스 필수 옵션을 체크 함
    var Params =  "&sellerOrderProductReferenceKey="
			   + $('#sellerOrderProductReferenceKey').val()
			   + "&orderProductStatus="
			   + $('#orderProductStatus option:selected').val()
			   + "&orderNo="
			   + $('#orderNo').val();

	//alert(Params);

	// localhost 로 테스트 시 크로스 도메인 문제로 발생하는 오류
    $.support.cors = true;

	/* + "&" + $('order_product_delivery_info').serialize() ); */
	$.ajax({
		type: "POST",
		url: "<?=$AppWebPath?>payco_upstatus.php",
		data: Params,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		dataType:"json",
		success:function(data){
			if(data.code == '0') {
				alert("변경되었습니다.");
			} else {
				alert("code:"+data.code+"\n"+"message:"+data.message);
			}
		},
        error: function(request,status,error) {
            //에러코드
            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			return false;
        }
	});
}

function cancel_order_all_test(){
    // 선택박스 필수 옵션을 체크 함
	if( $('#sellerOrderReferenceKey_all').val() == "" ) {			alert('가맹점에서 발급하는 주문 연동 Key를 입력해주세요.');     return false;	}
	if( $('#cancelTotalAmt_all').val() == "" ) {					alert('취소할 총 금액을 입력해주세요.');						return false;	}
	if( $('#orderCertifyKey_all').val() == "" ) {					alert('인증값을 입력해주세요.');								return false;	}

    // 선택박스 필수 옵션을 체크 함
    var Params = "cancelType=ALL" 
			   + "&orderNo="
			   + $('#orderNo_all').val()
			   + "&sellerOrderReferenceKey="
			   + $('#sellerOrderReferenceKey_all').val()
			   + "&orderCertifyKey="
			   + encodeURIComponent($('#orderCertifyKey_all').val())
			   + "&cancelTotalAmt="
			   + $('#cancelTotalAmt_all').val()
			   + "&totalCancelTaxfreeAmt="
			   + $('#totalCancelTaxfreeAmt_all').val()
			   + "&totalCancelTaxableAmt="
			   + $('#totalCancelTaxableAmt_all').val()
			   + "&totalCancelVatAmt="
			   + $('#totalCancelVatAmt_all').val()
			   + "&totalCancelPossibleAmt="
			   + $('#totalCancelPossibleAmt_all').val()
			   + "&requestMemo="
			   + $('#requestMemo_all').val();

	//alert(Params);

	// localhost 로 테스트 시 크로스 도메인 문제로 발생하는 오류
    $.support.cors = true;

	/* + "&" + $('order_product_delivery_info').serialize() ); */
	$.ajax({
		type: "POST",
		url: "<?=$AppWebPath?>payco_cancel.php",
		data: Params,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		dataType:"json",
		success:function(data){
			if(data.code == '0') {
				if(data.result.cancelPossibleYn == "N"){
					alert(data.result.cancelImpossibleReason+"\n이미 취소되었는지 확인하세요.");
				} else {
					alert("주문이 정상적으로 취소되었습니다.\n( 주문취소번호 : "+data.result.cancelTradeSeq+" )");
				}
			} else {
				alert("code:"+data.code+"\n"+"message:"+data.message);
			}
		},
        error: function(request,status,error) {
            //에러코드
            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			return false;
        }
	});
}

function cancel_order_part_test(){
    // 선택박스 필수 옵션을 체크 함
	if( $('#orderNo_part').val() == "" ) {
        alert('주문번호를 입력해주세요.');
        return false;
	}

	if( $('#cancelTotalAmt_part').val() == "" ) {
        alert('취소할 상품이 포함된 주문서의 총 주문금액을 입력해주세요.');
        return false;
	}

	if( $('#sellerOrderProductReferenceKey_part').val() == "" ) {
        alert('주문서에서 취소할 상품의 ID를 입력해주세요.');
        return false;
	}

	if( $('#cancelTotalAmt_part').val() == "" ) {
        alert('주문서에서 취소할 상품의 금액을 입력해주세요.');
        return false;
	}
// 

    // 선택박스 필수 옵션을 체크 함
   var Params = "cancelType=PART" 
			   + "&orderCertifyKey="
			   + encodeURIComponent($('#orderCertifyKey_part').val())				   
			   + "&cancelTotalAmt="
			   + $('#cancelTotalAmt_part').val()
			   + "&sellerOrderProductReferenceKey="
			   + $('#sellerOrderProductReferenceKey_part').val()
			   + "&cancelAmt="
			   + $('#cancelAmt_part').val()
			   + "&orderNo="
			   + $('#orderNo_part').val()
			   + "&totalCancelTaxfreeAmt="
			   + $('#totalCancelTaxfreeAmt_part').val()
			   + "&totalCancelTaxableAmt="
			   + $('#totalCancelTaxableAmt_part').val()
			   + "&totalCancelVatAmt="
			   + $('#totalCancelVatAmt_part').val()
			   + "&totalCancelPossibleAmt="
			   + $('#totalCancelPossibleAmt_part').val()
			   + "&requestMemo="
			   + $('#requestMemo_part').val()
			   + "&cancelDetailContent="
			   + $('#cancelDetailContent_part').val();	
			   

	//alert(Params);

	// localhost 로 테스트 시 크로스 도메인 문제로 발생하는 오류
    $.support.cors = true;

	/* + "&" + $('order_product_delivery_info').serialize() ); */
	$.ajax({
		type: "POST",
		url: "<?=$AppWebPath?>payco_cancel.php",
		data: Params,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		dataType:"json",
		success:function(data){
			if(data.code == '0') {
				//{"result":{"cancelPossibleYn":"N","partCancelPossibleYn":"N","pgCancelPossibleAmt":0.0,"cancelImpossibleReason":"취소 결제금액은 0보다 커야합니다.","orderNo":"201503172000160701"},"code":0,"message":"success"}
				if ( data.result.partCancelPossibleYn == "N" ){
					alert(data.result.cancelImpossibleReason);
				} else {
					alert("주문이 정상적으로 취소되었습니다.\n( 주문취소번호 : "+data.result.cancelTradeSeq+" / 취소상품금액 : "+data.result.totalCancelPaymentAmt+" )");
				}
			} else {
				alert("code:"+data.code+"\n"+"message:"+data.message);
			}
		},
        error: function(request,status,error) {
            //에러코드
            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			return false;
        }
	});
}

function mileage_cancel_test(){
    // 선택박스 필수 옵션을 체크 함
	if( $('#orderNo_mile').val() == "" ) {
        alert('주문번호를 입력해주세요.');
        return false;
	}

	if( $('#cancelPaymentAmount_mile').val() == "" ) {
        alert('취소할 주문서의 총 취소 금액을 입력해주세요.\n(마일리지 적립율을 곱한 금액이 취소됩니다.)');
        return false;
	}

    // 선택박스 필수 옵션을 체크 함
    var Params = "orderNo="
			   + $('#orderNo_mile').val()
			   + "&cancelPaymentAmount="
			   + $('#cancelPaymentAmount_mile').val();

	//alert(Params);

	// localhost 로 테스트 시 크로스 도메인 문제로 발생하는 오류
    $.support.cors = true;

	/* + "&" + $('order_product_delivery_info').serialize() ); */
	$.ajax({
		type: "POST",
		url: "<?=$AppWebPath?>payco_mileage_cancel.php",
		data: Params,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		dataType:"json",
		success:function(data){
			if(data.code == '0') {
				if(data.result.cancelPossibleYn == "N"){
					alert(data.result.cancelImpossibleReason);
				} else {
					alert("주문이 정상적으로 취소되었습니다.\n( 취소 마일리지 : "+data.result.canceledMileageAcmAmount+", 잔여 마일리지 : "+data.result.remainingMileageAcmAmount+" )");
				}
			} else {
				alert("code:"+data.code+"\n"+"message:"+data.message);
			}
		},
        error: function(request,status,error) {
            //에러코드
            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			return false;
        }
	});
}


function receipt_go(){
	if($(".payco input:radio[name=receipt]:checked").val() == null){
		alert("출력할 영수증을 선택하세요.");		
		return;
	}
	var orderurl = "https://alpha-bill.payco.com/outseller/receipt/"+$('#sellerOrderKey_Receipt').val()+"?receiptKind="+$(".payco input:radio[name=receipt]:checked").val();
	window.open(orderurl, 'payco_receipt');
}


//결제상세 조회(검증용)API 호출
function detailForVerify(){
    // 선택박스 필수 옵션을 체크 함
	 if( $('#sellerKey_detail').val() == "" ) {
        alert('가맹점키 를 입력해주세요.');
        return false;
	} 

	if( $('#reserveOrderNo_verify').val() == "" ) {
        alert('PAYCO 주문예약번호 를 입력해주세요.');
        return false;
	}

	/* if( $('#sellerOrderReferenceKey_detail').val() == "" ) {
        alert('외부가맹점에서 발급하는 주문 연동 Key 를 입력해주세요.');
        return false;
	} */
	

    // 선택박스 필수 옵션을 체크 함
   var Params = "sellerKey="
			   + $('#sellerKey_detail').val()
			   + "&reserveOrderNo="
			   + $('#reserveOrderNo_verify').val()
			   + "&sellerOrderReferenceKey="
			   + $('#sellerOrderReferenceKey_verify').val();
			   

	//alert(Params);

	// localhost 로 테스트 시 크로스 도메인 문제로 발생하는 오류
    $.support.cors = true;
	
	$.ajax({
		type: "POST",
		url: "<?=$AppWebPath?>payco_detailForVerify.php",
		data: Params,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		dataType:"json",
		success:function(data){
			   		 			     
			 if(data.code == '0') {				

				alert("결제상세(검증용) 내용이 정상적으로 조회 되었습니다.");

				detail_write_info('조회 결과', JSON.stringify(data, null, 4));
								
			} else {
				alert("code:"+data.code+"\n"+"message:"+data.message);
			} 			
		},
        error: function(request,status,error) {
            //에러코드
            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			return false;
        }
	});
}

//예약 정보 조회 결과 화면에 출력
function detail_write_info(title, content) {
	$("#list_detail_info").empty();                      // 화면 초기화
    $("#list_detail_info").append("<div class='header'>" + title + ":</div><div><pre>" + content + "</pre></div>");
}


</script>

</head>

<body>
<div id="header">
	<div class="gnb" id="gognb">
		<div class="wrap">
			<a href="http://www.payco.com" class="logo">PAYCO</a>
			<ul class="gognb">
				<li><a href="<?=$AppWebPath?>">DEMO WEB 결제 완료 페이지 ( EASYPAY - PAY2 )</a></li>
			</ul>
		</div>
	</div>
</div>

<div id="container" class="clearfix">
	<div class="main_fix_wrap easyPay_wrap">		

	<div style="height:30px;"></div>	
	
		<div class="detail_area">
				<div class="payco">
					<span>▣ <strong>주문 상태 변경 테스트</strong></span>
					<ul style="border-bottom:none;">
						<li style="margin:20px 0;">
							<em>가맹점에서 발급하는 주문 연동 Key ( sellerOrderReferenceKey ) </em>
							<input type="text" class="form-control input_text" name="sellerOrderReferenceKey" id="sellerOrderReferenceKey" value="">
							<em>PAYCO에서 발급받은 주문서 번호 ( orderNo ) <font color="red">[ 필수 ]</font> </em>
							<input type="text" class="form-control input_text" name="orderNo" id="orderNo" value="">
							<em>주문상품연동키 ( sellerOrderProductReferenceKey ) <font color="red">[ 필수 ]</font></em>
							<input type="text" class="form-control input_text" name="sellerOrderProductReferenceKey" id="sellerOrderProductReferenceKey" value="">
							<em>상태값 <font color="red">[ 필수 ]</font> </em>
							<div class="input-group">
								<select id="orderProductStatus" name="orderProductStatus" class="fs12 gray_03" style="width: 220px">
										<option value="">선택하세요</option>
										<option value="PAYMENT_WAITNG">입금대기</option>
										<option value="PAYED">결제완료 (빌링 결제완료)</option>
										<option value="DELIVERY_READY">배송 준비 중 [deprecated]</option>
										<option value="DELIVERING">배송 중 [deprecated]</option>
										<option value="DELIVERY_COMPLETE">배송 완료 [deprecated]</option>
										<option value="DELIVERY_START">배송 시작(출고지시)</option>
										<option value="PURCHASE_DECISION">구매확정</option>
										<option value="CANCELED">취소</option>
								</select>
								<span class="input-group-btn">
								 <button id="order_modify_btn" class="btn btn-default" type="button" onclick="order_state_modify();">GO</button>
								</span>
							</div>
						</li>
					</ul>
					<span>▣ <strong>주문 취소 테스트 (전체)</strong></span>
					<ul style="border-bottom:none;">
						<li style="margin:20px 0;">
							<em>PAYCO에서 발급받은 주문서 번호 ( orderNo )<font color="red">[ 필수 ]</font></em>
							<input type="text" class="form-control input_text" name="orderNo_all" id="orderNo_all" value="">
							<em>가맹점에서 발급하는 주문 연동 Key ( sellerOrderReferenceKey )<font color="red">[ 필수 ]</font></em>
							<input type="text" class="form-control input_text" name="sellerOrderReferenceKey_all" id="sellerOrderReferenceKey_all" value="">
							<em>주문완료통보시 내려받은 인증값 ( orderCertifyKey ) <font color="red">[ 필수 ]</font></em>
							<input type="text" class="form-control input_text" name="orderCertifyKey_all" id="orderCertifyKey_all" value="">
							<em>취소할 총 금액(면세금액+과세공급가액+과세부가세액) ( cancelTotalAmt )<font color="red">[ 필수 ]</font></em>
							<input type="text" class="form-control input_text" name="cancelTotalAmt_all" id="cancelTotalAmt_all" value="">
							<em>주문 총 면세금액 ( totalCancelTaxfreeAmt )</em>
							<input type="text" class="form-control input_text" name="totalCancelTaxfreeAmt_all" id="totalCancelTaxfreeAmt_all" value="">
							<em>주문 총 과세 공급가액 ( totalCancelTaxableAmt )</em>
							<input type="text" class="form-control input_text" name="totalCancelTaxableAmt_all" id="totalCancelTaxableAmt_all" value="">
							<em>주문 총 과세 부가세액 ( totalCancelVatAmt )</em>
							<input type="text" class="form-control input_text" name="totalCancelVatAmt_all" id="totalCancelVatAmt_all" value="">
							<em>총 취소가능금액(취소가능금액 검증) ( totalCancelPossibleAmt )</em>
							<input type="text" class="form-control input_text" name="totalCancelPossibleAmt_all" id="totalCancelPossibleAmt_all" value="">
							<em>취소처리 요청메모 ( requestMemo )</em>
							<div class="input-group">
								<input type="text" class="form-control input_text" name="requestMemo_all" id="requestMemo_all" value="">
								<span class="input-group-btn">
									<button id="order_cancel_all__btn" class="btn btn-default" type="button" onclick="cancel_order_all_test();">GO</button>
								</span>
							</div>
						</li>
					</ul>
					<span>▣ <strong>주문 취소 테스트 (부분 - 상품 1개만)</strong></span>
					<ul style="border-bottom:none;">
						<li style="margin:20px 0;">
							<em>주문인증키 ( orderCertifyKey ) <font color="red">[ 필수 ]</font></em>
							<input type="text" class="form-control input_text" name="orderCertifyKey_part" id="orderCertifyKey_part" value="">		
							<em>상품ID ( sellerOrderProductReferenceKey ) <font color="red">[ 필수 ]</font></em>
							<input type="text" class="form-control input_text" name="sellerOrderProductReferenceKey_part" id="sellerOrderProductReferenceKey_part" value="">
							<em>주문번호 ( orderNo ) <font color="red">[ 필수 ]</font></em>
							<input type="text" class="form-control input_text" name="orderNo_part" id="orderNo_part" value="">
							<em>취소할 총 금액 ( 면세금액+과세공급가액+과세부가세액) ( cancelTotalAmt ) <font color="red">[ 필수 ]</font></em>
							<input type="text" class="form-control input_text" name="cancelTotalAmt_part" id="cancelTotalAmt_part" value="">
							<em>총 취소할 면세금액(선택)</em>
							<input type="text" class="form-control input_text" name="totalCancelTaxfreeAmt_part" id="totalCancelTaxfreeAmt_part" value="">
							<em>총 취소할 과세금액(선택)</em>
							<input type="text" class="form-control input_text" name="totalCancelTaxableAmt_part" id="totalCancelTaxableAmt_part" value="">
							<em>총 취소할 부가세(선택)</em>
							<input type="text" class="form-control input_text" name="totalCancelVatAmt_part" id="totalCancelVatAmt_part" value="">
							<em>총 취소가능금액(선택)</em>
							<input type="text" class="form-control input_text" name="totalCancelPossibleAmt_part" id="totalCancelPossibleAmt_part" value="">						
							<em>취소사유(선택)</em>
							<input type="text" class="form-control input_text" name="cancelDetailContent_part" id="cancelDetailContent_part" value="">	
							<em>취소처리 요청메모(선택)</em>
							<input type="text" class="form-control input_text" name="requestMemo_part" id="requestMemo_part" value="">
							<em>취소 할 주문상품 금액 ( 부분취소금액 ) <font color="red">[ 필수 ]</font></em>
							<div class="input-group">
								<input type="text" class="form-control input_text" name="cancelAmt_part" id="cancelAmt_part" value="">
								<span class="input-group-btn">
									<button id="order_cancel_btn" class="btn btn-default" type="button" onclick="cancel_order_part_test();">GO</button>
								</span>
							</div>
						</li>
					</ul>
					<span>▣ <strong>마일리지 적립 취소 테스트</strong></span>
					<ul style="border-bottom:none;">
						<li style="margin:20px 0;">
							<em>주문번호 ( orderNo )</em>
							<input type="text" class="form-control input_text" name="orderNo_mile" id="orderNo_mile" value="">
							<em>취소 총 금액 ( cancelPaymentAmount )</em>
							<div class="input-group">
								<input type="text" class="form-control input_text" name="cancelPaymentAmount_mile" id="cancelPaymentAmount_mile" value="">
								<span class="input-group-btn">
									<button id="order_mile_cancel_btn" class="btn btn-default" type="button" onclick="mileage_cancel_test();">GO</button>
								</span>
							</div>
						</li>
					</ul>
					<span>▣ <strong>영수증 확인</strong></span>
					<ul style="border-bottom:none;">
						<li style="margin:20px 0;">
							<em>주문번호 ( orderNo )</em>
							<input type="text" class="form-control input_text" name="sellerOrderKey_Receipt" id="sellerOrderKey_Receipt" value="">
							<em>결제수단</em>
							<div class="input-group">
								<span style= "margin-right: 3px;"><input type="radio"  value="cash" name="receipt"> <label for="pay01">현금영수증</label></span>
								<span style= "margin-right: 3px;"><input type="radio"  value="online" name="receipt"><label for="pay02">온라인영수증</label></span>	
								<span style= "margin-right: 3px;"><input type="radio"  value="card" name="receipt" checked><label for="pay03">신용카드매출전표</label></span>	
								<span class="input-group-btn">
									<button id="order_mile_cancel_btn" class="btn btn-default" type="button" onclick="receipt_go();">GO</button>
								</span>
							</div>
						</li>
					</ul>
					<span>▣ <strong>결제 상세 조회</strong></span>
					<ul style="border-bottom:none;">
						<li style="margin:20px 0;">
							<em>가맹점키 ( sellerKey ) <font color="red">[ 필수 ]</font></em>
							<input type="text" class="form-control input_text" name="sellerKey_detail" id="sellerKey_detail" value="S0FSJE">
							<em>외부가맹점에서 발급하는 주문 연동 Key ( sellerOrderReferenceKey)</em>
							<input type="text" class="form-control input_text" name="sellerOrderReferenceKey_verify" id="sellerOrderReferenceKey_verify" value="">
							<em>PAYCO 주문예약번호 ( reserveOrderNo ) <font color="red">[ 필수 ]</font></em>
							<div class="input-group">
								<input type="text" class="form-control input_text" name="reserveOrderNo_verify" id="reserveOrderNo_verify" value="">
								<span class="input-group-btn">
									<button id="verifyPayment_btn" class="btn btn-default" type="button" onclick="detailForVerify();">GO</button>
								</span>
							</div>
							
							<div id="list_detail_info"></div>
							
						</li>
					</ul>
				</div>
			</div>
		
		
	</div>
	
	<button type="button" class="btn btn-default btn-lg" id="more_btn" style="margin-bottom :20px; display:none;">
	  <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span> 주문 예약 API 정보
	</button>
	
</div>

</body>
</html>