<?PHP
	include_once './_common.php'; // 공통
	//--------------------------------------------------------------------------------
	// PAYCO 주문 완료시 호출되는 RETURN 페이지 샘플 ( PHP EASYPAY / PAY2 )
	// - PAYCO 결제창에서 비밀번호 입력후, 호출 하여 처리
	// payco_return.php
	// 2016-12-02	PAYCO기술지원 <dl_payco_ts@nhnent.com>
	//--------------------------------------------------------------------------------

	//-----------------------------------------------------------------------------
	// 이 문서는 text/html 형태의 데이터를 반환합니다.
	//-----------------------------------------------------------------------------
	header('Content-type: text/html; charset:UTF-8');
	include(NM_PAYCO_PATH."/cfg/payco_config.php");

	//-----------------------------------------------------------------------------
	// 오류가 발생했는지 기억할 변수와 결과를 담을 변수를 선언합니다.
	//-----------------------------------------------------------------------------
	$doApproval = true;																// 기본적으로 주문예약 및 결제인증 받은것으로 설정

	$reserveOrderNo					= $_REQUEST["reserveOrderNo"];					// 주문 예약 번호
	$sellerOrderReferenceKey		= $_REQUEST["sellerOrderReferenceKey"];			// 외부가맹점에서 관리하는 주문연동Key
	$paymentCertifyToken			= $_REQUEST["paymentCertifyToken"];				// 결제인증토큰(결제승인시 필요)
	$totalPaymentAmt				= $_REQUEST["totalPaymentAmt"];					// 총결제금액
	$discountAmt					= $_REQUEST["discountAmt"];						// 쿠폰할인금액(PAYCO포인트 미포함 )
	$totalRemoteAreaDeliveryFeeAmt	= $_REQUEST["totalRemoteAreaDeliveryFeeAmt"];	// 총 도서산간비( 추가배송비 )
	$pointAmt						= $_REQUEST["pointAmt"];						// PAYCO 포인트 사용금액
	
	$cartNo							= $_REQUEST["cartNo"];							// returnUrlParam 에서 던진 값을 수신( 장바구니 번호 )
	$totalTaxableAmt				= $_REQUEST["tmpTotalTaxableAmt"];				// returnUrlParam 에서 던진 값을 수신( 과세 )
	$totalVatAmt					= $_REQUEST["tmpTotalVatAmt"];					// returnUrlParam 에서 던진 값을 수신( 부과세 )		
	$totalTaxfreeAmt 				= $_REQUEST["tmpTotalTaxfreeAmt"];				// returnUrlParam 에서 던진 값을 수신( 면세 )
	
	//-----------------------------------------------------------------------------
	
	$code							= $_REQUEST["code"];							// 결과코드
	$message						= $_REQUEST["message"];							// 결과코드
	
	//-----------------------------------------------------------------------------
	// Read_code 값이 0 또는 2222, 또는 실패시 오류코드 값 중 하나가 옵니다.
	//	 0 - 결제 인증 성공
	// 2222 - 사용자에 의한 결제 취소
	// 내역을 표시하고 창을 닫습니다.
	//-----------------------------------------------------------------------------
	
	
	
	
	//-----------------------------------------------------------------------------
	// (로그) 호출 시점과 호출값을 파일에 기록합니다.
	//-----------------------------------------------------------------------------
	Write_Log("payco_return.php is Called - reserveOrderNo : $reserveOrderNo , sellerOrderReferenceKey : $sellerOrderReferenceKey , paymentCertifyToken : $paymentCertifyToken , totalPaymentAmt : $totalPaymentAmt , discountAmt : $discountAmt ,  totalRemoteAreaDeliveryFeeAmt : $totalRemoteAreaDeliveryFeeAmt , pointAmt : $pointAmt , code : $code");
		
	
	//-----------------------------------------------------------------------------
	// response 값이 없으면 에러(ERROR)를 돌려주고 로그를 기록한 뒤 
	// 오류페이지를 보여주거나 주문되지 않았음을 고객에게 통보하는 페이지로 이동합니다.
	//-----------------------------------------------------------------------------

	if($code != 0){ 
		
		 Write_Log("payco_return.php ERROR - reserveOrderNo : $reserveOrderNo , sellerOrderReferenceKey : $sellerOrderReferenceKey , paymentCertifyToken : $paymentCertifyToken , totalPaymentAmt : $totalPaymentAmt , code : $code");
		
		 if($code == 2222){
		 	?>
		 			<html>
		 				<head>
		 					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		 					<title>사용자 취소</title>
		 				</head>
		 				<script type="text/javascript">
		 				
		 					alert("사용자에 의해 결제취소 되었습니다.  \n" + "ErrCode : <?=$code?> \n");
		 					
		 					var isMobile = <?=$isMobile?>;
		 					
		 					if(isMobile == 0){
		 						location.href = "index.php";		 						
		 					}else{
		 						opener.location.href = "index.php";
		 						window.close(); 		 						 
		 					}
		 					
		 				</script>
		 				<body>			
		 				</body>
		 			</html>
		 		<?php	
		 			
		 }	
				
		return;
	}
	
	//-----------------------------------------------------------------------------
	// 이곳에 위의 변수들을 이용해 가맹점에서 필요한 데이터를 처리합니다.
	// 예) 재고 체크, 매출금액 확인, 주문서 생성 , 총결제 금액 비교($totalPaymentAmt) , 외부가맹점 번호일치($sellerOrderReferenceKey) 등등
	//-----------------------------------------------------------------------------
			
	
	$ItemStock	= 10;							// DB 재고수량 , 연동 실패를 테스트 하시려면 값을 0 으로 설정하시고 정상으로 테스트 하시려면 1보다 큰 값을 넣으세요.
	
	$ItemTotalOrderAmt = 102500;                // DB에서 주문시 주문했던 총 금액(PAYCO 에 주문예약할때 던졌던 값.)을 가져옵니다.(주문값) 
	                                            // 연동 실패를 테스트 하시려면 값을 주문값을 totalPaymentAmt 값과 틀리게 설정하세요.
	
	
	//-----------------------------------------------------------------------------
	// 수신 데이터 사용 예제1 ( 재고 체크 )
	//-----------------------------------------------------------------------------
	
	if( $ItemStock < 1 ){
		
		Write_Log("payco_return.php 재고 확인 > ".$ItemName." 상품의 재고 부족으로 연동 오류 발생 하였습니다.");
		
		$doApproval = false;   					// 재고가 1보다 작다면 오류로 설정
	
		//-----------------------------------------------------------------------------
		//오류일 경우 오류페이지를 표시하거나 결제되지 않았음을 고객에게 통보합니다.
		//-----------------------------------------------------------------------------
	?>
						<html>
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
								<title>재고 부족</title>
							</head>
							<script type="text/javascript">			
								alert("재고 수량이 부족합니다.");
												
								var isMobile = <?=$isMobile?>;
												
								if(isMobile == 0){
									location.href = "index.php";
								}else{
									opener.location.href = "index.php";
									window.close(); 
								}
							</script>
							<body>				
							</body>
						</html>
	<?php
	
	}
	
	//-----------------------------------------------------------------------------
	// 수신 데이터 사용 예제2 ( 결제금액 위변조 확인 )
	//-----------------------------------------------------------------------------
	
	/*
	
	★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
	★★★★★★★★★★															              ★★★★★★★★★★
	★★★★★★★★★★                						중요 사항 				                      ★★★★★★★★★★
	★★★★★★★★★★                        										                             ★★★★★★★★★★
	★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★ ★★★★★★★★★★
	
	 총 금액 결제된 금액을 주문금액과 비교.(반드시 필요한 검증 부분.)
	 주문금액을 변조하여 결제를 시도 했는지 확인함.(반드시 DB에서 읽어야 함.)
	 결제승인 요청 전에 확인 해야 합니다.
     ( 금액이 변조되었으면 반드시 승인을 취소해야 함. )

	★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
	
	*/	
	
	if( $totalPaymentAmt != $ItemTotalOrderAmt ){    // 위에서 파라메터로 받은 totalPaymentAmt 값과 주문값이 같은지 비교합니다.
		
		Write_Log("payco_return.php 결제금액 위변조 확인 > DB 총 주문금액 : ".$ItemTotalOrderAmt." 원 과 PAYCO 결제금액 : ".$totalPaymentAmt." 원 이 같지 않습니다.");
		
		$doApproval = false;   						// DB 총 주문금액과 PAYCO결제금액이 다르다면 오류로 설정
		
		//-----------------------------------------------------------------------------
		//오류일 경우 오류페이지를 표시하거나 결제되지 않았음을 고객에게 통보합니다.
		//-----------------------------------------------------------------------------
	?>
						<html>
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
								<title>결제금액 위변조 확인</title>
							</head>
							<script type="text/javascript">			
								alert("주문금액과 결제금액이 틀립니다.");
												
								var isMobile = <?=$isMobile?>;
												
								if(isMobile == 0){
									location.href = "index.php";
								}else{
									opener.location.href = "index.php";
									window.close(); 
								}
							</script>
							<body>				
							</body>
						</html>
	<?php
	
	//★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
		
	}
	
	if ( $doApproval == true ){
	
		
		
		//-----------------------------------------------------------------------------
		// 확인결과(재고확인,결제금액 위변조 확인)가 정상이면 PAYCO 에 결제 승인을 요청
		//-----------------------------------------------------------------------------
		//-----------------------------------------------------------------------------
		// 결제 승인 요청에 담을 JSON OBJECT를 선언합니다.
		//-----------------------------------------------------------------------------		
		
		$approvalOrder["sellerKey"]					= $sellerKey;					// 가맹점 코드. payco_config.php 에 설정
		$approvalOrder["reserveOrderNo"]			= $reserveOrderNo;				// 예약주문번호.
		$approvalOrder["paymentCertifyToken"]		= $paymentCertifyToken;			// 결제인증토큰.
		$approvalOrder["sellerOrderReferenceKey"]	= $sellerOrderReferenceKey;		// 외부가맹점에서 관리하는 주문연동Key		
		$approvalOrder["totalPaymentAmt"]			= $totalPaymentAmt;				// 주문 총 금액.		
				
		$Result = payco_approval(stripslashes(json_encode($approvalOrder)));  		// 배열을 JSON 형식으로 변환 및 백슬래시 제거 후에, 결제 승인 요청함.
		Write_Log("payco_return.php  payco_approval -  Result : $Result");          
			
		$Read_Data = json_decode($Result, true);                           			// JSON 형식의 전달받은 값을, 배열로 변환. 
		
				
		//----------------------------------------------------------------------------
		// 수신 데이터 추출 예제 ( 종합 )
		//----------------------------------------------------------------------------
		
		/*
		 	
		$Result_Total = $Read_Data["result"];
		
		foreach ($Result_Total as $key => $value){
			switch ($key){
				case "deliveryPlace":
					$deliveryPlace = $Result_Total["deliveryPlace"];
					
					foreach ($deliveryPlace as $key => $value){
						Write_Log("deliveryPlace[$key] : ".$value);
		
					}	
				break;
		
				case "orderProducts":
					$orderProducts = $Result_Total["orderProducts"];
					
					foreach ($orderProducts as $key => $value){
						Write_Log("orderProducts[$key]");
						$orderProduct = $orderProducts[$key];
						
						foreach ($orderProduct as $key => $value){
							Write_Log("    $key : ".$value);
						}
					}
					
				break;
		
				case "paymentDetails":
					$paymentDetails = $Result_Total["paymentDetails"];
					
					foreach ($paymentDetails as $key => $value){
						Write_Log("paymentDetails[$key] : ");
						$paymentDetail = $paymentDetails[$key];
			
						foreach ($paymentDetail as $key => $value){
							switch ($paymentDetail["paymentMethodCode"]){
								case "31":
									if ($key=="cardSettleInfo"){
										Write_Log("    cardSettleInfo :");
										$cardSettleInfo = $paymentDetail["cardSettleInfo"];
		
										foreach ($cardSettleInfo as $key => $value){
											Write_Log("        $key : ".$value);
										}
									} else {
										Write_Log("    $key : ".$value);
									}
								break;
		
								case "75":
								case "76":
								case "77":
									if ($key=="couponSettleInfo"){
										Write_Log("    couponSettleInfo : ");
										$couponSettleInfo = $paymentDetail["couponSettleInfo"];
			
										foreach ($couponSettleInfo as $key => $value){
											Write_Log("        $key : ".$value);
										}
									} else {
										Write_Log("    $key : ".$value);
									}
								break;
		
								case "98":
									Write_Log("    $key : ".$value);
								break;
		
								default:
								break;
							}
						}
					}
		
					break;
		
					default:
						Write_Log("$key : ".$value);
					break;
				}
			}
		
		*/
		
		//-----------------------------------------------------------------------------
		// 결제 승인 수신 데이터 사용
		//-----------------------------------------------------------------------------
		
		$sellerOrderReferenceKey			= $Read_Data["result"]["sellerOrderReferenceKey"];			// 가맹점에서 발급했던 주문 연동 Key
		$reserveOrderNo						= $Read_Data["result"]["reserveOrderNo"];					// PAYCO에서 발급한 주문예약번호
		$orderNo							= $Read_Data["result"]["orderNo"];							// PAYCO에서 발급한 주문번호
		$memberName							= $Read_Data["result"]["memberName"];						// 주문자명
		$totalOrderAmt						= $Read_Data["result"]["totalOrderAmt"];					// 총 주문 금액
		$totalDeliveryFeeAmt				= $Read_Data["result"]["totalDeliveryFeeAmt"];				// 총 배송비 금액
		$totalRemoteAreaDeliveryFeeAmt		= $Read_Data["result"]["totalRemoteAreaDeliveryFeeAmt"];	// 총 추가배송비 금액
		$totalPaymentAmt					= $Read_Data["result"]["totalPaymentAmt"];					// 총 결제 금액
		
		$sellerOrderProductReferenceKey     = $Read_Data["result"]["orderProducts"][0]["sellerOrderProductReferenceKey"];	// 가맹점에서 관리하는 	주문상품 연동key
		$orderCertifyKey 					= $Read_Data["result"]["orderCertifyKey"];					// 주문인증키
				
				
		
		
		//-----------------------------------------------------------------------------
		// <<<< 시작 >>>>
		// DB 저장중 오류가 발생하였으면 , 전체 취소 API( payco_cancel.php )를 호출 합니다. 
		//
		//-----------------------------------------------------------------------------
			
			//$ErrBoolean = true;              //  기본 적으로  DB 저장 성공으로 설정
			$ErrBoolean = false;              //  기본 적으로  DB 저장 성공으로 설정
		
		//------------------------------------------------------------------------------
		// $ErrBoolean 상태값에 따른 결과값 생성
		//------------------------------------------------------------------------------
			if($ErrBoolean){
				$resultValue = "ERROR";		//오류가 있으면 ERROR를 설정
			} else {
				$resultValue = "OK";		//오류가 없으면 OK 설정
			}
			
		
			
			if ($resultValue == "ERROR"){
			
				// 결제상세 조회(검증용)API 호출 - 승인완료 후 DB 저장시 오류 났을 경우, 결제취소시에 사용합니다.
				$detailForVerify = array();
				$detailForVerify["sellerKey"] 				= $sellerKey;					// 가맹점 코드. payco_config.php 에 설정
				$detailForVerify["reserveOrderNo"] 			= $reserveOrderNo; 				// 예약주문번호.
				$detailForVerify["sellerOrderReferenceKey"] = $sellerOrderReferenceKey; 	// 외부가맹점에서 관리하는 주문연동Key
			
				$detailForVerify_Result = payco_detailForVerify(stripslashes(json_encode($detailForVerify)));  		// 배열을 JSON 형식으로 변환 및 백슬래시 제거 후에, 결제 승인 요청함.
				$detailForVerify_Read_Data = json_decode($detailForVerify_Result, true);                  			// JSON 형식의 전달받은 값을, 배열로 변환.
			
				//-----------------------------------------------------------------------------
				//결제 취소 API 호출 ( PAYCO 에서 받은 결제정보를 이용해 전체 취소를 합니다. )
			    // 취소 내역을 담을 JSON OBJECT를 선언합니다.
				//------------------------------------------------------------------------------
				$cancelOrder = array();
				$cancelOrder["sellerKey"]					= $sellerKey;							// 가맹점 코드. payco_config.php 에 설정
				$cancelOrder["sellerOrderReferenceKey"]		= $detailForVerify_Read_Data["result"]["sellerOrderReferenceKey"];				// 취소주문연동키. ( 파라메터로 넘겨 받은 값 )
				$cancelOrder["orderCertifyKey"]				= $detailForVerify_Read_Data["result"]["orderCertifyKey"];						// 주문완료통보시 내려받은 인증값
				$cancelOrder["cancelTotalAmt"]				= $detailForVerify_Read_Data["result"]["totalPaymentAmt"];						// PAYCO 주문서의 총 금액을 입력합니다. (전체취소, 부분취소 전부다)
		
				Write_Log("payco_return.php is DB Error : try cancel : ".$cancelOrder);
				
				$cancel_Result = payco_cancel(stripslashes(json_encode($cancelOrder)));
			
			?>
						 			<html>
						 				<head>
						 					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
						 					<title>DB 저장오류 결제취소</title>
						 				</head>
						 				<script type="text/javascript">
			
					 						alert("DB 저장오류 발생하여 결제취소 되었습니다. ");
			
						 					var isMobile = <?=$isMobile?>;
						 					
						 					if(isMobile == 0){
						 						location.href = <?$AppWebPath?>"index.php";	
						 						 						
						 					}else{
						 						opener.location.href = <?$AppWebPath?>"index.php";
						 						window.close(); 		 						 
						 					}
						 				</script>
						 				<body>			
						 				</body>
						 			</html>
						 		<?php
					return;
			}
		
		//-----------------------------------------------------------------------------
		//
		// DB 저장중 오류가 발생하였으면 , 전체 취소 API( payco_cancel.php )를 호출 합니다.  
		// <<<< 끝 >>>>
		//-----------------------------------------------------------------------------
		
		
		
		if($Read_Data["code"] == 0){	
		?>
			
					<html>			
						<head>
						<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
							<title>주문 완료</title>
						</head>						
						
						<script type="text/javascript">			
							
							alert("장바구니 <?=$cartNo?>번 의 주문이 정상적으로 완료되었습니다.");
							
							var isMobile = <?=$isMobile?>;
							
							if(isMobile == 0){     // MOBILE
								 
								location.href = "<?=$AppWebPath?>complete.php";								
								
								window.close();
								
								
							}else{				   // PC
			
								
								
								opener.location.href = "<?=$AppWebPath?>complete.php";					
								 
								var successPoller = setInterval(function() {               // setInterval 은 일정시간 마다 함수가 실행되도록 처리
														
										<? // 결과를 complete.php 페이지에 전송.  ?>
												
										try {
											if(typeof opener.document.getElementById("reserveOrderNo_verify") !== 'undefined' ) {
														
												opener.document.getElementById("sellerOrderReferenceKey").value = "<?=$sellerOrderReferenceKey?>";
												opener.document.getElementById("orderNo").value = "<?=$orderNo?>";
												opener.document.getElementById("sellerOrderProductReferenceKey").value = "<?=$sellerOrderProductReferenceKey?>";
			
												opener.document.getElementById("orderNo_all").value = "<?=$orderNo?>";
												opener.document.getElementById("sellerOrderReferenceKey_all").value = "<?=$sellerOrderReferenceKey?>";
												opener.document.getElementById("orderCertifyKey_all").value = "<?=$orderCertifyKey?>";
												opener.document.getElementById("cancelTotalAmt_all").value = "<?=$totalPaymentAmt?>";
												opener.document.getElementById("totalCancelTaxfreeAmt_all").value = "<?=$totalTaxfreeAmt?>";
												opener.document.getElementById("totalCancelTaxableAmt_all").value = "<?=$totalTaxableAmt?>";
												opener.document.getElementById("totalCancelVatAmt_all").value = "<?=$totalVatAmt?>";
												opener.document.getElementById("totalCancelPossibleAmt_all").value = "<?=$totalPaymentAmt?>";
												opener.document.getElementById("requestMemo_all").value = "전체취소 테스트입니다.";
			
												opener.document.getElementById("sellerOrderProductReferenceKey_part").value = "<?=$sellerOrderProductReferenceKey?>";
												opener.document.getElementById("orderCertifyKey_part").value = "<?=$orderCertifyKey?>";
												opener.document.getElementById("orderNo_part").value = "<?=$orderNo?>";
												opener.document.getElementById("cancelTotalAmt_part").value = "<?=$totalPaymentAmt?>";
												opener.document.getElementById("totalCancelTaxfreeAmt_part").value = "<?=$totalTaxfreeAmt?>";
												opener.document.getElementById("totalCancelTaxableAmt_part").value = "<?=$totalTaxableAmt?>";
												opener.document.getElementById("totalCancelVatAmt_part").value = "<?=$totalVatAmt?>";
												//opener.document.getElementById("totalCancelPossibleAmt_part").value = "<?=$totalPaymentAmt?>"; // 취소 가능금액 필수 아님.
												opener.document.getElementById("cancelDetailContent_part").value = "부분취소 테스트입니다.";
												opener.document.getElementById("requestMemo_part").value = "부분취소 테스트입니다.";
												opener.document.getElementById("cancelAmt_part").value = "<?=$totalPaymentAmt?>";									
												//opener.document.getElementById("sellerOrderReferenceKey_part").value = "<?=$sellerOrderReferenceKey?>";									
												
												opener.document.getElementById("orderNo_mile").value = "<?=$orderNo?>";
												opener.document.getElementById("cancelPaymentAmount_mile").value = "<?=$totalPaymentAmt?>";
			
												opener.document.getElementById("sellerOrderKey_Receipt").value = "<?=$orderNo?>";
			
												opener.document.getElementById("sellerOrderReferenceKey_verify").value = "<?=$sellerOrderReferenceKey?>";
												opener.document.getElementById("reserveOrderNo_verify").value = "<?=$reserveOrderNo?>";
												
												clearInterval(successPoller);   //clearInterval 은 setInterval 로 설정한 작업을 취소
												
												window.close();  // 주석처리 및 PAYCO 결제창이 닫히지않게 하고, refresh 로 재호출시의 오류를 처리하는지 확인이 필요합니다.
												
												}
											}
											catch (error) {
												alert(error);
											}
										}, 500);
															 
							}
			
						</script>
						<body>			
						</body>
					</html>
					
					
	<?php
			
		}else if($Read_Data["code"] == 4005){
			?>
			 			<html>
			 				<head>
			 					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			 					<title>중복결제 확인</title>
			 				</head>
			 				<script type="text/javascript">

		 						alert("이미 주문 완료되었습니다. \n" + "ErrCode : : <?=$Read_Data["code"]?> \n");

			 					var isMobile = <?=$isMobile?>;
			 					
			 					if(isMobile == 0){
			 						location.href = <?$AppWebPath?>"index.php";	
			 						window.close(); 	 						
			 					}else{
			 						opener.location.href = <?$AppWebPath?>"index.php";
			 						window.close(); 		 						 
			 					}
			 				</script>
			 				<body>			
			 				</body>
			 			</html>
			 		<?php	
			 			
		}else{
			
			$ErrBoolean = true;              //  결제 승인 코드값이 0 (성공) 이 아니면 오류로 설정
				
			$Err_code    = $Read_Data["code"];
			$Err_message = $Read_Data["message"];
			
			Write_Log("payco_return.php 결제 승인 실패 - code : $Err_code , message : $Err_message ");
			
			
			
			?>
				<html>
					<head>
						<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
						<title>결제 승인 실패</title>
					</head>
					<script type="text/javascript">
						alert("code : <?=$Read_Data["code"]?> \n" + "message : <?=$Read_Data["message"]?>");
						var isMobile = <?=$isMobile?>;
						if(isMobile == 0){
							location.href = "index.php";
						}else{
							opener.location.href = "index.php";
							window.close(); 
						}
					</script>
					<body>			
					</body>
				</html>
			<?				
			return;
			
		}	// 결제 승인 실패		

				
		
		//-----------------------------------------------------------------------------
		// ...
		// 결제 승인 수신 데이터를 이용하여 결제 승인후 처리할 부분을 이곳에 작성합니다.
		// ...
		//-----------------------------------------------------------------------------
		
		


	}else{

		//-----------------------------------------------------------------------------
		//
		// 오류일 경우 오류페이지를 표시하거나 결제되지 않았음을 고객에게 통보합니다.
		// 팝업창 닫기 또는 구매 실패 페이지 작성 ( 팝업창 닫을때 Opener 페이지 이동 등 )
		//
		//-----------------------------------------------------------------------------
		//결제 인증 후 내부 오류가 있어 승인은 받지 않았습니다. 오류내역을 여기에 표시하세요. 예) 재고 수량이 부족합니다.
		
		if($code != 0){ 
		
			
			 Write_Log("payco_return.php ERROR - reserveOrderNo : $reserveOrderNo , sellerOrderReferenceKey : $sellerOrderReferenceKey , paymentCertifyToken : $paymentCertifyToken , totalPaymentAmt : $totalPaymentAmt , code : $code");
		
		 
?>
		<html>				
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<title>주문 실패</title>
			</head>
			<script type="text/javascript">

				alert("결제가 승인되지 않았습니다. \n" + "code : <?=$code?> ");

				var isMobile = <?=$isMobile?>;

				if(isMobile == 0){
					location.href = "index.php";
				}else{
					// 주문페이지로 다시 돌아가기 위해 창만 닫거나 특정 페이지로 이동합니다.
					//opener.location.href = "index.php";
					window.close(); 
				}
			</script>
			<body>					
			</body>
		</html>
<?php	
		 			
		}
	
		return;	
	}

?>
		
