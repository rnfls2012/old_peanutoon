<?php
	//-----------------------------------------------------------------------------
	// PAYCO 주문 페이지 샘플 ( PHP EASYPAY / PAY2 )
	// 2016-08-26	PAYCO기술지원 <dl_payco_ts@nhnent.com>
	//-----------------------------------------------------------------------------
	include("payco_config.php");

	//-----------------------------------------------------------------------------
	// 테스트용 고객 주문 번호 생성
	//-----------------------------------------------------------------------------
	//$customerOrderNumber = "TEST2016".str_replace("-","",date("Y-m-d")).substr("000000".mt_rand(0,999999), -6);
	// 결제완료 후 에도 주문예약번호 자동생성 가능하도록 임시 로 function callPaycoUrl()에 구현되어 주석처리.
	
	//echo "isMobile >>> ".$isMobile;
?>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="0">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">

<meta name="keyword" content="컨텐츠">

<title>PAYCO_DEMOWEB (PHP EasyPay PAY2)</title>

<link href="css/common.css" rel="stylesheet" type="text/css">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!--
<script src="/share/js/requirejs/require.js"></script>
<script src="/share/js/requirejs/require.config.js"></script>
-->
<script type="text/javascript" src="https://static-bill.nhnent.com/payco/checkout/js/payco.js" charset="UTF-8"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">

function order_chk(){
	if($(".left input:radio[name=sort]:checked").val() == null){
		alert("결제방식을 선택하세요.");		
		return;
	}else{
		if($(".left input:radio[name=sort]:checked").val() == "payco"){
			callPaycoUrl();
			return;
		}else{
			alert($(".left input:radio[name=sort]:checked").val());
			return;
		}	
	}
}


function callPaycoUrl(){

	var randomStr = "";               
	var randomStrCart = "";
	
	for(var i=0;i<10;i++){
		randomStr += Math.ceil(Math.random() * 9 + 1);
	}
	
	var customerOrderNumber = "TEST2016" + randomStr;               // ( 결제완료 후 에도 주문예약번호 자동생성 가능하도록 임시 추가 ) 가맹점 고객 주문번호 입력
	//var Params = "customerOrderNumber=<?=$customerOrderNumber?>";	// 가맹점 고객 주문번호 입력

	for(var j=0;j<5;j++){
		randomStrCart += Math.ceil(Math.random() * 9 + 1);
	}
	var cartNo = "CartNo_" + randomStrCart;                         // 장바구니 번호
	
    // localhost 로 테스트 시 크로스 도메인 문제로 발생하는 오류 
    $.support.cors = true;

	/* + "&" + $('order_product_delivery_info').serialize() ); */
	$.ajax({
		type: "POST",
		url: "<?=$AppWebPath?>/payco_reserve.php",

		//data: Params,		// JSON 으로 보낼때는 JSON.stringify(customerOrderNumber)
		data:{"customerOrderNumber":customerOrderNumber, "cartNo":cartNo},
		
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		dataType:"json",
		success:function(data){
			if(data.code == '0') {
				//console.log(data.result.reserveOrderNo);	// 주석 해제시, 일부 웹브라우저 에서 PAYCO 결제창이 뜨지 않습니다.			
				$('#order_num').val(data.result.reserveOrderNo);
				$('#order_url').val(data.result.orderSheetUrl);										
			}else {
				alert("code:"+data.code+"\n"+"message:"+data.message);
			}
		},
        error: function(request,status,error) {
            //에러코드
            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			return false;
        }
	});
}

function payco_open(){

	var order_Url = $('#order_url').val();

	if (order_Url == "")
	{
		alert("주문예약실행 버튼을 먼저 누르세요.");
		return false;
	}	
	
	/*
	-----------------------------------------------------------------------------
	 USER-AGENT 구분 ( Mobile 이면 페이지 전환, Pc 이면 팝업 호출 )
	-----------------------------------------------------------------------------
	*/

	var isMobile = <?=$isMobile?>;
	
	if (isMobile == 0){  // MOBILE 
		document.location.href = order_Url;
		//location.href = order_Url;
		
	}else{               // PC              
		window.open(order_Url, 'popupPayco', 'top=100, left=300, width=727px, height=512px, resizble=no, scrollbars=yes'); 
	}	
}

function payco_direct_open(){
	var randomStr = "";               
	var randomStrCart = "";

	// 가맹점 고객 주문번호 생성
	for(var i=0;i<10;i++){
		randomStr += Math.ceil(Math.random() * 9 + 1);
	}	
	var customerOrderNumber = "TEST2016" + randomStr;               // ( 결제완료 후 에도 주문예약번호 자동생성 가능하도록 임시 추가 ) 가맹점 고객 주문번호 입력
	//var Params = "customerOrderNumber=<?=$customerOrderNumber?>";	// 가맹점 고객 주문번호 입력

	
	// 장바구니 번호 생성
	for(var j=0;j<5;j++){
		randomStrCart += Math.ceil(Math.random() * 9 + 1);
	}	
	var cartNo = "CartNo_" + randomStrCart;                         // 장바구니 번호	
	
	window.open("payco_popup.php?customerOrderNumber="+customerOrderNumber+"&cartNo="+cartNo,"popupPayco", 'top=100, left=300, width=727px, height=512px, resizble=no, scrollbars=yes');	
	//가맹점 고객 주문번호, 장바구니 번호 전달 및 PAYCO 팝업창 호출
}


</script>

</head>
<body>

<div id="header">
	<div class="gnb" id="gognb">
		<div class="wrap">
			<ul class="gognb" >
				<li><h3>PHP 간편결제(EASYPAY - PAY2)</h3></li>
			</ul>
		</div>
	</div>
</div>

<div id="container" class="clearfix">
	<div class="main_fix_wrap easyPay_wrap">

		<table cellspacing="0" cellpadding="0" class="tbl_std">
			<colgroup>
				<col width="9%">
				<col width="46%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="15%">
			</colgroup>
			<thead>
				<tr>
					<th colspan="2" class="fst left">상품정보</th>
					<th>수량</th>
					<th>상품금액</th>
					<th>적립금</th>
					<th>주문금액</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="fst"><img src="http://image.popshoes.co.kr/images/goods_img/20150127/115312/115312_a_500.jpg?20150213114102"	alt="나이키 우먼스 덩크 스카이 하이 에센셜 (NIKE WMNS DUNK SKY HI ESSENTIAL) 644877 010" width="80" height="80"></td>
					<td class="left">
						<p>아디다스 위네오 슈퍼 웨지 (ADIDAS  WENEO SUPER WEDGE) F38577</p>
						<p>옵션 : 245</p>
					</td>
					<td>1</td>
					<td>
						<p>100,000 원</p>
					</td>
					<td class="bg_sum">0원</td>
					<td class="bg_sum txt_sum text_bold">100,000 원</td>
				</tr>
				<tr>
					<td class="fst left" colspan="4"></td>
					<td colspan="2" class="bg_total left">
						<ul class="total_wrap">
							<li><p>총상품금액</p>
								<strong>100,000원</strong></li>
							<li><p>총적립금</p>
								<strong>0원</strong></li>
							<li><p>배송비</p>
								<strong>2,500원</strong></li>
							<li><p>결제금액</p>
								<strong class="point">102,500원</strong></li>
						</ul>
					</td>
				</tr>
			</tbody>
		</table>

	<div style="height:30px;"></div>

	<table cellspacing="0" cellpadding="0" class="save_point_wrap">
		<colgroup>
			<col width="78%">
			<col width="22%">
		</colgroup>
		<tbody>
			<tr>
				<td>
					<!-- s:안내 -->
					<table cellspacing="0" cellpadding="0" class="save_point">
						<colgroup>
							<col width="20%">
							<col width="80%">
						</colgroup>
						<tbody>
							<tr>
								<th class="underline">결제방식</th>
								<td class="left underline">
									<input id="paym_01" type="radio" name="sort" value="card" disabled> <label for="paym_01">신용카드</label>&nbsp;
									<input id="paym_05" type="radio" name="sort" value="virtual" disabled> <label for="paym_05">무통장(가상계좌)</label>&nbsp; 
									<input id="paym_03" type="radio" name="sort" value="transfer" disabled> <label for="paym_03">실시간계좌이체</label>&nbsp; 
									<input id="paym_04" type="radio" name="sort" value="mobile" disabled> <label for="paym_04">휴대폰결제</label>&nbsp; 
									<input id="paym_07" type="radio" name="sort" value="payco" checked="checked"> <label for="paym_07" id="payco_type1">
										<div class="payco">
											<div id="payco_btn_type_A1"></div>
										</div> 
								</td>
							</tr>

							<!-- PAYCO 안내 -->
							<tr id="div_toastpay" class="pay_detail"
								style="height: 148px">
								<th>PAYCO</th>
								<td class="left">
									<ul>
										<li><font color="red"><strong>PAYCO 간편결제 안내</strong></font></li>
										<li>PAYCO는 NHN엔터테인먼트가 만든 안전한 간편결제 서비스입니다.</li>
										<li>휴대폰과 카드 명의자가 동일해야 결제 가능하며, 결제금액 제한은 없습니다.</li>
										<li>- 지원카드: 모든 국내 신용/체크카드</li>
									</ul>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	
	<table align="center" style="margin-top:50px;" >
				<tr>
					<td style="padding:30px; border:solid 1px;" valign="Top">
						<div class="easyPay_div">주문예약, 결제하기 분리방식</div>
						<div class="easyPay_div"><button type="button" class="btn easyPay_btn"  onclick="order_chk();" >주문예약실행</button> </div>
						<div class="easyPay_div">
						<li style="margin:20px 0;"><em>예약주문번호 </em>
							<input type="text" class="form-control input_text" name="order_num" id="order_num" value=""  ></li>
							<li><em>주문창URL </em>
							<input type="text" class="form-control input_text" name="order_url" id="order_url" value=""  ></li>
						</li>
						<div class="easyPay_div"><button type="button" class="btn easyPay_btn"  onclick="payco_open();" >결제하기( PC-팝업, MOBILE-리다이렉트 )</button> </div>
					</td>
					<td width="50" style="padding:10px;">
					</td>
					<td style="padding:30px; border:solid 1px;" valign="Top">
						<div class="easyPay_div">팝업차단시 차단메시지 안뜨고 열리게 하는 방법 </div>
						<div class="easyPay_div"><button type="button" class="btn easyPay_btn"  onclick="payco_direct_open();" >결제하기(팝업차단 후 클릭)</button> </div>
					</td>
				</tr>
	</table>
	</div>
	<div style="height:100px"></div>
				
</div>

<script type="text/javascript">
	  Payco.Button.register({
		SELLER_KEY:'1111',
		ORDER_METHOD:"EASYPAY",
		BUTTON_TYPE:"A1",
		BUTTON_HANDLER:callPaycoUrl, // function callPaycoUrl() {} 를 실행함.
		DISPLAY_PROMOTION:"Y",
		DISPLAY_ELEMENT_ID:"payco_btn_type_A1",
		"":""
	  });
</script>

</body>
</html>