<? if (!defined("_RECHARGE_")) exit; // 개별 페이지 접근 불가
	include_once './_common.php'; // 공통

	header('Content-type: text/html; charset:UTF-8');
	include_once(NM_PAYCO_PATH."/cfg/payco_config.php");

	/* 181017-취소필수값저장 */
	if($sellerOrderReferenceKey != ''){

		// 결제상세 조회(검증용)API 호출 - 승인완료 후 DB 저장시 오류 났을 경우, 결제취소시에 사용합니다.
		$detailForVerify = array();
		$detailForVerify["sellerKey"] 				= $sellerKey;					// 가맹점 코드. payco_config.php 에 설정
		$detailForVerify["reserveOrderNo"] 			= $reserveOrderNo; 				// 예약주문번호.
		$detailForVerify["sellerOrderReferenceKey"] = $sellerOrderReferenceKey; 	// 외부가맹점에서 관리하는 주문연동Key

		$detailForVerify_Result = payco_detailForVerify(stripslashes(json_encode($detailForVerify)));  		// 배열을 JSON 형식으로 변환 및 백슬래시 제거 후에, 결제 승인 요청함.
		$detailForVerify_Read_Data = json_decode($detailForVerify_Result, true);                  			// JSON 형식의 전달받은 값을, 배열로 변환.

		//-----------------------------------------------------------------------------
		//결제 취소 API 호출 ( PAYCO 에서 받은 결제정보를 이용해 전체 취소를 합니다. )
		// 취소 내역을 담을 JSON OBJECT를 선언합니다.
		//------------------------------------------------------------------------------
		$cancelOrder = array();
		$cancelOrder["sellerKey"]					= $sellerKey;							// 가맹점 코드. payco_config.php 에 설정
		$cancelOrder["sellerOrderReferenceKey"]		= $detailForVerify_Read_Data["result"]["sellerOrderReferenceKey"];				// 취소주문연동키. ( 파라메터로 넘겨 받은 값 )
		$cancelOrder["orderCertifyKey"]				= $detailForVerify_Read_Data["result"]["orderCertifyKey"];						// 주문완료통보시 내려받은 인증값
		$cancelOrder["cancelTotalAmt"]				= $detailForVerify_Read_Data["result"]["totalPaymentAmt"];						// PAYCO 주문서의 총 금액을 입력합니다. (전체취소, 부분취소 전부다)
	}else{
		if($orderCertifyKey ==''){
			cs_x_error_log("x_pg_channel_payco", "payco", NM_PAYCO_PATH, "payco_cancel_require", "", $orderCertifyKey, 53, "cancel.php(payco_return.php) is orderCertifyKey null", $nm_member); // state-5	
		}else{
			$cancelOrder = array();
			$cancelOrder["sellerKey"]					= $sellerKey;							// 가맹점 코드. payco_config.php 에 설정
			$cancelOrder["orderNo"]					    = $orderNo;		                        // 주문번호
			$cancelOrder["cancelTotalAmt"]				= $totalPaymentAmt;						// PAYCO 주문서의 총 금액을 입력합니다. (전체취소, 부분취소 전부다)
			$cancelOrder["orderCertifyKey"]				= $orderCertifyKey;						// 주문완료통보시 내려받은 인증값
		}
	}
	/* 181017-취소필수값저장 end */

	Write_Log("payco_return.php is DB Error : try cancel : ".$cancelOrder);
	
	$cancel_Result = payco_cancel(stripslashes(json_encode($cancelOrder)));
	$cancel_Read_Data = json_decode($cancel_Result, true); 
	

	$db_result['state'] = 'cancel-'.$cancel_Read_Data['code'];
	$db_result['msg']   = '취소요청-'.$cancel_Read_Data['message'];
?>