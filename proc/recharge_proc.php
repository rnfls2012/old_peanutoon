<? include_once '_common.php'; // 공통
define('_RECHARGE_', true); // 결제페이지 개별로 접근 금지하기 위한 상수

cs_login_check(); // 로그인 체크

$payway = eng_check(tag_filter($_REQUEST['payway']));
$payway_no = num_check(tag_filter($_REQUEST['payway_no']));
$get_param_opt_1 = eng_check(tag_filter($_REQUEST['param_opt_1']));
$get_param_opt_2 = num_check(tag_filter($_REQUEST['param_opt_2']));
$get_param_opt_3 = num_check(tag_filter($_REQUEST['param_opt_3']));
$coupondc = num_check(tag_filter($_REQUEST['coupondc'])); // 할인권
if($get_param_opt_1 == "kcp"){
	$payway = $get_param_opt_1;
	if($get_param_opt_2 != ""){
		$payway_no = $get_param_opt_2;
	}
	if($get_param_opt_3 != ""){
		$coupondc = $get_param_opt_3;
	}
}

if($payway == false ){
	cs_alert('PG사가 잘못되였습니다.',NM_URL."/recharge.php");
}else{
	if(is_mobile()){ $device = "mobile"; }
	else{ $device = "pc"; }

	// pc사 파일 경로
	$pg_arr = cs_payway($payway,$device);
	$pg_url = $pg_arr['url'];
	$pg_path = $pg_arr['path'];

	$ret_url = NM_PROC_URL."/".cs_recharge_save_filename_ctrl().".php?payway=".$payway."&payway_no=".$payway_no."&coupondc=".$coupondc;

	// pc사 연결
	include_once $pg_path.'/pp_cli_hub.php';
}

?>