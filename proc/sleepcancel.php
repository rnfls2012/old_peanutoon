<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동

if(!$_mode) {
	cs_alert("파라미터가 없습니다!", HTTP_REFERER);
	die;
} // end if

if(!isset($_SESSION['ss_human_id'])) {
	cs_alert("세션 값이 없습니다!", HTTP_REFERER);
	die;
} // end if

if($_SESSION['ss_human_referer'] == "") {
	$_SESSION['ss_human_referer'] = NM_URL;
} // end if

$human_id = $_SESSION['ss_human_id'];
$link = $_SESSION['ss_human_referer'];

// 회원 정보
$human_mb = mb_get($human_id);

// 세션 삭제
del_session("ss_human_id");
del_session("ss_human_referer");

if($_mode == "cancel") {
	$cancel_sql = "UPDATE member SET mb_state_sub = 'y', mb_human_date = '' WHERE mb_id = '".$human_mb['mb_id']."' and mb_idx = '".$human_mb['mb_idx']."' and mb_no = '".$human_mb['mb_no']."'";
	if(sql_query($cancel_sql)) {
		// 로그인 세션 초기화
		$_SESSION['ss_login_id'] = '';
		$_SESSION['ss_login_pw'] = '';

		// 회원아이디 세션 생성
		set_session('ss_mb_id', $human_mb['mb_id']);
		set_session('ss_mb_no', $human_mb['mb_no']);
		// FLASH XSS 공격에 대응하기 위하여 회원의 고유키를 생성해 놓는다. 관리자에서 검사함 - 110106
		set_session('ss_mb_key', md5($human_mb['mb_join_date'] . REMOTE_ADDR . $_SERVER['HTTP_USER_AGENT']));

		// 회원정보 업데이트 & 회원로그
		mb_login_date_update($human_mb);
		mb_stats_log_member_update($human_mb);

		// 휴면 해제 로그
		$cancel_log_sql = "INSERT INTO z_human_cancel(z_hc_member, z_hc_id, z_hc_idx, z_hc_human_date, z_hc_cancel_date) VALUES('".$human_mb['mb_no']."', '".$human_mb['mb_id']."', '".$human_mb['mb_idx']."', '".$human_mb['mb_human_date']."', '".NM_TIME_YMDHIS."')";
		sql_query($cancel_log_sql);

		goto_url($link);
	} else { 
		cs_alert("휴면 해제에 실패하였습니다! 관리자에게 문의하세요.", $link);
		die;
	} // end else
} else if($_mode == "keep") {
	goto_url($link);
} else {
	cs_alert("잘못된 파라미터 입니다.", $link);
	die;
} // end else
?>