<? if (!defined("_RECHARGE_")) exit; // 개별 페이지 접근 불가
include_once './_common.php'; // 공통

// $_SESSION['recharge_order_no'] = ''; // 주문번호 초기화
// 181024 주문번호-캔슬용 삭제 : 세션처리했었는데 뒤로가기할때 문제가 발생되어 DB 저장하기로 함
cs_del_pg_member_order($nm_member); // 주문번호 삭제 -> 혹시 몰라서 한번 더 삭제

$template_number = 2333;    // 알림톡 결제관련 템플릿 번호
$row_kakao = select_template($template_number);

/*======================================
 * 최초 1회 동의 쿠키값 처리 - 180309 (지완)
 =======================================*/

$row_mb = mb_get($nm_member['mb_id']);

/* 각 회원 최초 1회 동의 및 쿠키값 설정 시 IF 블록 실행 - 180309 (지완)
 * 쿠키값 확인이 안됨. 확인필요 (mobile 인 경우 url변경으로 추정)
 */
 
if ( $row_mb['mb_pay_agree'] === 'n' ) {
    $expire_time = 60 * 60; // 60min * 60sec
    $sql = "
        UPDATE member SET 
        mb_pay_agree='y' 
        WHERE mb_no=".$nm_member['mb_no']
    ;

    sql_query($sql);

  // del_cookie('mb_agree', $expire_time); // 생성했던 시간 값 대로 똑같은 시간 값줘서 삭제.
}

/*=====================================*/

/*======================================
 * 알림톡 전송 - 180309 (지완)
 =======================================*/
if ( get_cookie('send_talk') && $row_kakao['kt_temp_use'] === 'y' && $receipt_state == 0) { // 정상 결제시

    if ( $view_recharge_point > 0 ) {
        $view_recharge_cash = $view_recharge_cash."(".$nm_config['cf_point_unit_ko']." ".$view_recharge_point.")";
    }

    $receiver_arr = set_kakao_conf($row_mb['mb_name'], $row_mb['mb_phone']);

    $msg_arr = set_message(
        $row_kakao['kt_temp_no'],
        array(
            number_format($view_amt),
            $row_mb['mb_id'],
            $view_date,
            $row_kakao['kt_temp_usage'],
            $view_recharge_cash,
            NM_DOMAIN_ONLY
        )
    );

    $send_arr = array_merge($receiver_arr, $msg_arr);

    $response = send_talk($send_arr);

    // 알림톡의 설정을 위한 쿠키값 삭제
    // (결제 완료페이지 새로고침으로 인한 오류) - 180403(fixed)
    del_cookie('send_talk');
}

/*======================================*/

if(is_mobile()){ 
	$receipt_head_path = NM_PATH.'/_head.php';
	$receipt_tail_path = NM_PATH.'/_tail.php';
	$device = "mobile";
}else{ 
	$receipt_head_path = $nm_config['nm_path'].'/_head.sub.php';
	$receipt_tail_path = $nm_config['nm_path'].'/_tail.sub.php';
	$device = "pc";
	$nm_config['footer'] = false;
}

// 아무타스 리타겟팅 태그 - 첫결제시 출력 => 쿠키생성 18-11-27
$gtm_amutus_charge_cookie = get_cookie('gtm_amutus_charge');
if(intval($nm_member["mb_no"]) > 0){
	if($gtm_amutus_charge_cookie != $nm_member["mb_no"]){
		set_cookie('gtm_amutus_charge', $nm_member["mb_no"]);
	}
}

include_once ($receipt_head_path); // 공통

$receipt_text = "결제가 완료되었습니다!";
if($receipt_text_error != ''){ $receipt_text = $receipt_text_error; }

/* 친구초대 reward 지급 */
if($nm_member['mb_invite'] == '1' && $nm_member['mb_won_count'] == '0') {
	pf_recharge_reward($nm_member);
} // end if

/* AceCounter 결제 -> 사용안함 */
/*
$acecounter_order = array();
$acecounter_order['state']			= $db_result['state'];
$acecounter_order['count']			= count($row_mpi);
$acecounter_order['order']			= $view_order;
$acecounter_order['product']		= $view_product;
$acecounter_order['amt']				= $view_amt;
$acecounter_order['payway']			= $view_payway;
$acecounter_order['cash_point']	= $view_cash_point;

mkt_rdt_acecounter_recharge($nm_member, $acecounter_order);
*/

// 181001 아무타스 리타겟팅 태그
gtm_amutus($nm_member); 
/* 181001 첫결제시 출력 */
if (defined("_RECHARGE_FIRST_")){ 
	if($receipt_state == 0 || $_SESSION['cf_pg_pass_ctrl'] == 'y'){ // 정상 결제시 || 결제대행패스-테스트 모드 (관리자)
		if(intval($nm_member["mb_no"]) > 0){
			// 18-12-12 가입후 72시간 안에 결제한 회원들 한에서 charge 노출 요청-엔야상&김성훈차장 181211
			$mb_join_date_72_hour = date('Y-m-d H:i:s', strtotime('+72 hours', strtotime($nm_member["mb_join_date"])));
			if(NM_TIME_YMDHIS < $mb_join_date_72_hour){
				if($gtm_amutus_charge_cookie != $nm_member["mb_no"]){ // 쿠키가 있다면 출력안함
					gtm_amutus_charge($nm_member); 
				}
			}
		}
	}
}
?>

<link rel="stylesheet" type="text/css" href="<?=NM_URL;?>/css/receipt.css<?=vs_para();?>" />
<script type="text/javascript" src="<?=NM_URL;?>/js/receipt.js<?=vs_para();?>"></script>

<? /* 티켓소켓 이벤트 */ 
$tksk_order = array();
$tksk_order['number'] = $view_order;
$tksk_order['email'] = $nm_member['mb_email'];
$tksk_order['revenue'] = $view_amt;
$tksk_order['page'] = 'recharge_receipt';
$tksk_order['name'] = $nm_member['mb_no'];
$tksk_order['productname'] = $view_product;

$tksk_publickey='ics9';
$row_tksk = tksk_ics_row($tksk_publickey);
if($row_tksk['tksk_state'] == 'y' && $receipt_state == 0){
	//180109 부사장님께서 정의장대표가 결제 실패시 티켓소켓 안나오게 하라고 요청함
	tksk_js::tksk_ics_peanutoon_js($tksk_order, $tksk_publickey); 
}

$receipt_url = $ret_url;


// 181022 리다이렉트
echo '
<script type="text/javascript">
<!--
	var redirect="'.get_js_cookie("redirect").'";
//-->
</script>
';

if(get_js_cookie("redirect")){
	$receipt_url = goto_redirect_cookie();
}
if($payco_payment_method_code == "02"){ // payco무통장일경우
	$receipt_url = $ret_url;
}
?>
		
				<div class="receipt">
					<h2><?=$receipt_text?></h2>
					<h3>결제내역</h3>
					<table cellspacing="0" cellspacing="0">
						<tr>
							<th>주문 번호</th>
							<td><?=$view_order?></td>
						</tr>
						<tr>
							<th>상품명</th>
							<td><?=$view_product?></td>
						</tr>
						<tr>
							<th>결제금액</th>
							<td><?=number_format($view_amt)?>원</td>
						</tr>
						<tr>
							<th>결제방법</th>
							<td><?=$view_payway?></td>
						</tr>
						<tr>
							<th>결제시간</th>
							<td><?=$view_date?></td>
						</tr>
					</table>
					<ul>
						<li>
							<dl>
								<dt>충전 <?=$nm_config['cf_cash_point_unit_ko'];?></dt>
								<dd><?=$view_cash_point;?> <img src="<?=NM_IMG.$nm_config['cf_cash'];?>"></dd>
							</dl>
						</li>
						<li>
							<dl>
								<dt>충전 <?=$nm_config['cf_point_unit_ko'];?></dt>
								<dd><?=$view_point;?> <img src="<?=NM_IMG.$nm_config['cf_point'];?>"></dd>
							</dl>
						</li>
					</ul>
					
					<? /* 티켓소켓 ics 공유버튼 */ 
					if($row_tksk['tksk_state'] == 'y' && $db_result['state'] == 0){ ?>
					<div class="receipt_tksk">
						<img src="<?=$nm_config['nm_url_img'];?>tksk/ics9/ticketsocket_receipt.png<?=vs_para();?>" alt="티켓소켓이벤트" />
						<div class="tksk_valign">
							<input type="text" id="tksk_ics_email" placeholder="이메일 주소를 입력하세요" autocomplete="off" value="<?=$tksk_order['email'];?>">
							<span class="group_btn"><button class="tksk_ok" onclick="tksk_ics_peanutoon_js();">페이스북 공유하기</button></span>
						</div>
					</div>
					<? } ?>

					<button onclick="receipt('<?=$device?>', '<?=$receipt_url?>');">확인</button>
				</div> <!-- /receipt_tb -->
				<div class="receipt_footer">
					문제가 있거나 궁금한 점이 있으시면 <a href="mailto:<?=$nm_config['cf_admin_email'];?>"><?=$nm_config['cf_admin_email'];?></a> 으로 문의주시기 바랍니다.
				</div>
	
<? include_once ($receipt_tail_path); // 공통?>