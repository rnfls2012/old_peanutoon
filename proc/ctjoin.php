<?
include_once '_common.php'; // 공통

$http_referer = base_filter($_POST['http_referer']);
$join_id = base_filter($_POST['join_id']);
$join_pw = trim($_POST['join_pw']);
$app_mb_token		= base_filter($_REQUEST['mb_token']);
$join_date = NM_TIME_YMDHIS;
$mb_post = 'n';
if($_REQUEST['regi_event']) {
	$mb_post = 'y';
} // end if

$redirect		= base_filter($_POST['redirect']); // 리다이렉트 추가-18-12-03

$mb_idx = mb_get_idx($join_id);

/*************************************** 
가입완료페이지 추가로 아래 $http_referer 사용 안함 - 20180406 
$link = $http_referer;
preg_match("/".etc_filter(NM_URL)."/i", etc_filter($http_referer),  $http_referer_ckeck);
if(count($http_referer_ckeck) < 1 || $http_referer == ''){ $link = NM_URL; }
***************************************/

/* 가입완료페이지 */
$link = NM_URL."/ctjoinup.php";

/* 가입완료보던페이지 */
$config_http_referer = HTTP_REFERER;
if($http_referer != ""){ $config_http_referer = $http_referer; }

if($config_http_referer == ''){
	$config_http_referer = NM_URL;
}else{
	if(strpos($config_http_referer, 'ctlogin.php') || strpos($config_http_referer, 'ctjoin.php') || strpos($config_http_referer, 'ctjoinup.php') || strpos($config_http_referer, 'ctjoinsns.php')){
		$cookie_http_referer = urldecode(get_session('ss_http_referer'));
	}else{
		set_session('ss_http_referer', urlencode($config_http_referer));
		$cookie_http_referer = $config_http_referer;
	}
}
if($cookie_http_referer == ''){ $cookie_http_referer = NM_URL; }

if($redirect !=''){ // 리다이렉트 추가-18-12-03
	$cookie_http_referer = goto_redirect($redirect);
}

/* 중복 아이디 체크 */
$join_idx = mb_get_idx($join_id);

/* 회원 있는지 중복체크 */
$sql_mb = " SELECT count(*) as mb_total FROM member 
			WHERE mb_id = '".$join_id."' AND mb_idx ='".$join_idx."' AND  mb_state='y' ";
$mb_total = sql_count($sql_mb, 'mb_total');

if($mb_total >= 1) {
	$link = NM_URL."/ctjoin.php";
	cs_alert("중복된 아이디 입니다!", $link);
	die;
/* 아이디 이메일 양식 체크 */
} else if(email_check($join_id) == false) {
	$link = NM_URL."/ctjoin.php";
	cs_alert("이메일 형식의 아이디를 입력해 주세요!", $link);
	die;
/* 비밀번호 길이 체크 */
} else if(strlen($join_pw) < 4) {
	$link = NM_URL."/ctjoin.php";
	cs_alert("비밀번호는 4자 이상 입력해 주세요!", $link);
	die;
} else {
	$mb_email = $mb_email_idx = $mb_email_domain = "";
	$mb_email = $join_id;
	$mb_email_idx = substr($mb_email, 0, 1);
	$mb_email_domain = substr($mb_email, strpos($mb_email, '@')+1, strlen($mb_email));
	$insert_query = "insert INTO member(mb_id, mb_idx, mb_email, mb_email_idx, mb_email_domain, mb_pass, mb_join_date, mb_post, mb_token) values('".$join_id."', '".$mb_idx."', '".$mb_email."', '".$mb_email_idx."', '".$mb_email_domain."', password('".$join_pw."'), '".$join_date."', '".$mb_post."', '".$app_mb_token."')";
	
	/* 회원 추가가 성공이라면 */
	if(sql_query($insert_query)) {
		// 회원 정보 가져오기
		$mb = mb_get($join_id);

		// 로그인 세션 초기화
		$_SESSION['ss_login_id'] = '';
		$_SESSION['ss_login_pw'] = '';

		// 회원아이디 세션 생성
		set_session('ss_mb_id', $mb['mb_id']);

		// FLASH XSS 공격에 대응하기 위하여 회원의 고유키를 생성해 놓는다. 관리자에서 검사함 - 110106
		set_session('ss_mb_key', md5($mb['mb_join_date'] . $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT']));
		
		// 회원정보 업데이트 & 회원로그
		mb_login_date_update($mb, $app_mb_token);
		mb_stats_log_member_update($mb);

		// 마케팅 가입이라면...
		mkt_marketer_join($mb);

		// 티켓소켓 가입이라면...		
		tksk_ics_join($mb);
		
		// 친구초대 가입이라면..
		// pf_invite_join($mb); 일반가입 막아버림. 임시메일로 인해 악용사례가 많음 180103

		// 초대코드 업데이트
		mb_set_invite_code($mb);

		// mkt - ICE COUNTER JOIN
		// rdt_acecounter_ss_join('y');

		goto_url($link);
		
	} else {
		cs_alert("회원가입에 실패하였습니다. 관리자에게 문의하세요!", NM_URL."/ctjoin.php", "");
		die;
	} // end else
} // end else

?>