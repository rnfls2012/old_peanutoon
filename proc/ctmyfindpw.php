<? include_once '_common.php'; // 공통

$mb_id = base_filter($_POST['mb_id']);
$mb_pw = trim($_POST['mb_pw']);
$mb_pw_check = trim($_POST['mb_pw_check']);

if($mb_pw == '' || $mb_pw_check == ''){
	cs_alert("패스워드 또는 패스워드 확인을 입력하셔야 합니다.", NM_URL."/ctmyfind.php");
	die;
}

if($mb_pw != $mb_pw_check){
	cs_alert("입력하신 패스워드가 틀립니다.", NM_URL."/ctmyfind.php");
	die;
}

$find_id = mb_get($mb_id);

$sql_pw_update = "update member set mb_pass = password('".$mb_pw."') where mb_id = '".$find_id['mb_id']."' AND mb_idx = '".$find_id['mb_idx']."'";
if(sql_query($sql_pw_update)){
	cs_alert("입력하신 패스워드로 수정되였습니다.", NM_URL."/ctlogin.php");
	die;
}
?>