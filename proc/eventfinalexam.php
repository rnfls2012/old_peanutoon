<?php

include_once '_common.php'; // 공통

$score_one = $answer_count = $score = 0;

$efp_no = num_check(tag_get_filter($_REQUEST['efp_no'])); 
$ef_count = num_check(tag_get_filter($_REQUEST['ef_count']));
$ef_time = tag_get_filter($_REQUEST['ef_time']);

$ef_no_str = implode("|",$_REQUEST['ef_no']); 
$ef_choice_str = implode("|",$_REQUEST['ef_choice']); 

$ef_no_arr = explode("|",$ef_no_str); 
$ef_choice_arr = explode("|",$ef_choice_str); 

$score_one = 100 / intval($ef_count); // 1문제당 점수계산

$efp_arr = array();
// 시험지 가져오기
$sql_ef = "SELECT * FROM event_finalexam WHERE ef_state='y' AND ef_efp_no='".$efp_no."' ORDER BY ef_no ";
$result_ef = sql_query($sql_ef);
for($e=1; $row_ef = sql_fetch_array($result_ef); $e++){	
	$efp_arr[$row_ef['ef_no']] = $row_ef['ef_answer'];
}

// print_r($efp_arr);
// echo "<br/><br/>";

foreach($ef_no_arr as $ef_no_key => $ef_no_val){
	/*
	echo $ef_no_val." : ";
	echo $efp_arr[$ef_no_val]."-";
	echo $ef_choice_arr[$ef_no_key]."<br/>";
	*/
	if($efp_arr[$ef_no_val] == $ef_choice_arr[$ef_no_key]){
		$answer_count++; // 정답 카운팅
	}
}

/*
echo "<br/><br/>";
echo $ef_no_str;
echo "<br/><br/>";
echo $ef_choice_str;
echo "<br/><br/>";
echo $answer_count;
*/

$score = $answer_count * $score_one; // 정답 점수 계산

if($is_member == false){
	alert('올바른 방법으로 이용하시기 바랍니다.', NM_URL.'/eventfinalexam_reception.php');
	die;
}else{
	// 데이터 가져오기
	$sql_efp = "SELECT * FROM event_finalexam_period WHERE efp_state = 'y' AND efp_no='".$efp_no."' ";
	$row_efp = sql_fetch($sql_efp);

	$sql_efm = " SELECT * FROM event_finalexam_member WHERE 
	            efm_member = '".$nm_member['mb_no']."' AND  efm_member_idx = '".$nm_member['mb_idx']."' 
			   ";
	$row_efm = sql_fetch($sql_efm);

	$z_efm_count = intval($row_efm['efm_count'.$row_efp['efp_mark']])+1;

	if($z_efm_count > 3){		
		alert($row_efp['efp_mark'].'교시의 응시하기가 3번 넘었습니다.', NM_URL.'/eventfinalexam_reception.php');
		die;
	}

	// 로그 남기기
	$sql_insert_z_efm = " INSERT INTO z_event_finalexam_member ( 
						z_efm_efp_no, z_efm_member, z_efm_member_idx, 
						z_efm_count,z_efm_test,z_efm_answer,z_efm_score,
						z_efm_reg_date
					    ) VALUES ( 
						'".$efp_no."', '".$nm_member['mb_no']."', '".$nm_member['mb_idx']."', 
						'".$z_efm_count."', '".$ef_no_str."', '".$ef_choice_str."', '".$score."', 						
						'".NM_TIME_YMDHIS."')
					  ";
	sql_query($sql_insert_z_efm);

	// efm 업데이트
	$row_efm['efm_count'.$row_efp['efp_mark']] = intval($row_efm['efm_count'.$row_efp['efp_mark']])+1;
	$row_efm['efm_date'.$row_efp['efp_mark']] = NM_TIME_YMDHIS;
	$row_efm['efm_efp_no'.$row_efp['efp_mark']] = $efp_no;
	if($row_efm['efm_highscore'.$row_efp['efp_mark']] < $score){
		$row_efm['efm_highscore'.$row_efp['efp_mark']] = $score;
	}

	$sql_update_efm = " UPDATE event_finalexam_member SET 
	efm_count".$row_efp['efp_mark']."='".$row_efm['efm_count'.$row_efp['efp_mark']]."', 
	efm_date".$row_efp['efp_mark']."='".$row_efm['efm_date'.$row_efp['efp_mark']]."', 
	efm_efp_no".$row_efp['efp_mark']."='".$row_efm['efm_efp_no'.$row_efp['efp_mark']]."', 
	efm_highscore".$row_efp['efp_mark']."='".$row_efm['efm_highscore'.$row_efp['efp_mark']]."' 
	WHERE efm_member = '".$nm_member['mb_no']."' AND  efm_member_idx = '".$nm_member['mb_idx']."' 
	";
	sql_query($sql_update_efm);
}


$link = NM_URL."/eventfinalexam_score.php?efp_no=".$efp_no."&score=".intval($score)."&time=".$ef_time;
goto_url($link);