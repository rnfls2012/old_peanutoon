<? include_once '_common.php'; // 공통

$mb_email_token = base_filter($_REQUEST['mb_email_token']);
$mb_email_idx = base_filter($_REQUEST['mb_email_idx']);
$mb_email_domain = base_filter($_REQUEST['mb_email_domain']);

$mb_pw = trim($_POST['mb_pw']);
$mb_pw_check = trim($_POST['mb_pw_check']);

if($mb_pw == '' || $mb_pw_check == ''){
	cs_alert("패스워드 또는 패스워드 확인을 입력하셔야 합니다.", HTTP_REFERER);
	die;
}

if($mb_pw != $mb_pw_check){
	cs_alert("입력하신 변경 비밀번호와 변경 비밀번호 확인이 다릅니다.", HTTP_REFERER);
	die;
}

if(strlen($mb_pw) < 4) {
	cs_alert("비밀번호는 4자 이상 입력해 주세요!", HTTP_REFERER);
	die;
}

/* DB */
$sql = "SELECT * FROM member WHERE mb_state='y' AND mb_email_idx='".$mb_email_idx."' AND mb_email_domain='".$mb_email_domain."' AND mb_email_token = '".$mb_email_token."' ";
$result = sql_query($sql);
$row_size = sql_num_rows($result);

if($row_size > 0){ 
	$sql_pw_update = "	UPDATE member SET mb_pass = password('".$mb_pw."') , customer_password='n', mb_email_token=''  
						WHERE mb_state='y' AND mb_email_idx='".$mb_email_idx."' AND mb_email_domain='".$mb_email_domain."' AND mb_email_token = '".$mb_email_token."' ";
	if(sql_query($sql_pw_update)){
		cs_alert("입력하신 패스워드로 수정되였습니다.", NM_URL."/ctlogin.php");
		die;
	}
}else{
	cs_alert("변경할 ID가 없습니다.<br/>\nID 로그인을 확인해보시거나 관리자에게 문의하시기바랍니다.[error:size-t0]", HTTP_REFERER);
	die;
}
?>