<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동

// 로그인 체크
cs_login_check(HTTP_REFERER);

// 날짜 변수
$set_date['year_month']	= NM_TIME_YM;
$set_date['year']		= NM_TIME_Y;
$set_date['month']		= NM_TIME_M;
$set_date['day']		= NM_TIME_D;
$set_date['hour']		= NM_TIME_H;
$set_date['week']		= NM_TIME_W;


/* 테스트 날짜 변수 
$set_date['year'] = '2017';
$set_date['month'] = '09';
$set_date['day'] = '01';
 end 테스트 날짜 변수 */

$set_date['ymd'] = $set_date['year'].'-'.$set_date['month'].'-'.$set_date['day'];

// 현재 달(MONTH)의 출석 이벤트 정보 가져오기
$ea_arr = array();
$ea_arr = sql_fetch("select * from event_attend where ea_month=".$set_date['month']);

// 현재 달(MONTH)의 연속 출석 정보 가져오기
$ea_cont_arr = array(
	$ea_arr['ea_cont_date_1']=>$ea_arr['ea_cont_point_1'], 
	$ea_arr['ea_cont_date_2']=>$ea_arr['ea_cont_point_2'], 
	$ea_arr['ea_cont_date_3']=>$ea_arr['ea_cont_point_3']
);
ksort($ea_cont_arr);

// 회원의 나이대를 가져오기
$nm_member['mb_age'] = mb_get_age_group($nm_member['mb_birth']);

// 회원의 이번 달 현재 출석 상황 가져오기
$member_attend_sql = "select * from z_member_attend where z_ma_member='".$nm_member['mb_no']."' and z_ma_attend_year='".$set_date['year']."' and z_ma_attend_month='".$set_date['month']."' order by z_ma_attend_date desc";
$member_attend_result = sql_query($member_attend_sql);
$member_attend_row = array();
while($row = sql_fetch_array($member_attend_result)) {
	array_push($member_attend_row, $row);
} // end while

// 이벤트 진행중인지 체크
if($ea_arr['ea_use'] != "y") {
	cs_alert("출석 이벤트가 진행중이지 않습니다!", HTTP_REFERER);
	die;
} // end if

// 출석 여부 확인
$member_attend_today_sql = "select count(*) as today_attend from z_member_attend where z_ma_member='".$nm_member['mb_no']."' and z_ma_attend_year='".$set_date['year']."' and z_ma_attend_month='".$set_date['month']."' and z_ma_attend_day='".$set_date['day']."'";
$member_attend_today_count = sql_count($member_attend_today_sql, "today_attend");

if($member_attend_today_count != 0) {
	cs_alert("오늘자 출석을 이미 완료하셨습니다!", HTTP_REFERER);
	die;
} // end if

// echo preg_replace("/[^0-9]*/s", "", "2017-09-30");
/* 값 지정 */
$z_ma_point = $ea_arr['ea_oneday_point']; // 지급 미니땅콩
$z_ma_cont_count = 1; // 연속 출석일
$z_ma_attend_regu = "n"; // 개근 체크
$z_ma_attend_cont = "n"; // 연속출석 체크
$from = "출석 보상 "; // 메세지

if(count($member_attend_row) != 0) { // 첫 출석일때
	$cmp_attend_date = preg_replace("/[^0-9]*/s", "", $member_attend_row[0]['z_ma_attend_date']); // 비교용 최신 출석날짜
	$cmp_today = preg_replace("/[^0-9]*/s", "", $set_date['ymd']); // 비교용 오늘 날짜
	$cmp_val = $cmp_today - $cmp_attend_date; // 오늘 날짜 - 최신 출석날짜

	

	if($cmp_val == 1) { // 전 날 출석을 했을때
		$z_ma_cont_count = $member_attend_row[0]['z_ma_cont_count'] + 1; // 이전 연속 출석일에 +1

		// 개근 및 연속 출석 체크
		if($ea_arr['ea_regu_use'] == "y" && $z_ma_cont_count == $ea_arr['ea_regu_date']) { // 개근일 때

			$z_ma_point = $ea_arr['ea_regu_point']; // 지급 미니땅콩
			$z_ma_attend_regu = "y"; // 개근 적용

		} else if($ea_arr['ea_cont_use'] == "y") { // 연속출석일 때

			foreach($ea_cont_arr as $key => $val) {
				if($key != "") {
					if($z_ma_cont_count % $key == 0) {
						$z_ma_point = $val; // 지급 미니땅콩
						$z_ma_attend_cont = "y"; // 연속출석 적용
					} // end if
				} // end if
			} // end foreach

		} // end else if
	} // end if
} // end if

$from .= $z_ma_point." ".$nm_config['cf_point_unit_ko']." 지급";

$insert_sql = "INSERT INTO z_member_attend(z_ma_member, z_ma_id, z_ma_idx, z_ma_sex, z_ma_age, z_ma_point, z_ma_attend_year, z_ma_attend_month, z_ma_attend_day, z_ma_cont_count, z_ma_attend_cont, z_ma_attend_regu, z_ma_attend_date, z_ma_attend_save_date, z_ma_user_agent) VALUES('".$nm_member['mb_no']."', '".$nm_member['mb_id']."', '".$nm_member['mb_idx']."', '".$nm_member['mb_sex']."', '".$nm_member['mb_age']."', '".$z_ma_point."', '".$set_date['year']."', '".$set_date['month']."', '".$set_date['day']."', '".$z_ma_cont_count."', '".$z_ma_attend_cont."', '".$z_ma_attend_regu."', '".$set_date['ymd']."', '".NM_TIME_YMDHIS."', '".HTTP_USER_AGENT."')";

if(cs_set_member_point_income_event($nm_member, "0", $z_ma_point, "attend", $from, "y")) {
	if(sql_query($insert_sql)) {
		// 출석 통계 처리
		st_stats_event_attend_cms($ea_arr, $set_date, $z_ma_point);
		cs_alert("출석체크가 완료되었습니다!", HTTP_REFERER);
		die;
	} else {
		cs_alert("출석 기록에 오류가 발생했습니다. 관리자에게 문의하세요!", HTTP_REFERER);
		die;
	} // end else
} else {
	cs_alert("출석에 실패하였습니다. 관리자에게 문의하세요!", HTTP_REFERER);
	die;
} // end else
?>