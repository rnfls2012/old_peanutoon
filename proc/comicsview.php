<?
include_once '_common.php'; // 공통

cs_login_check(); // 로그인 체크

/* 실제구매 카운트 */
$mb_all_pay_count = 0;

/* 사용내역변수 */
$mpu_from = "";
$ce_chapter_pre = 0;

/* DB */
$comics = get_comics($_comics);

// 성인체크
cs_comics_adult($comics['cm_no'], $nm_member);

// 이벤트
$event_arr = array();
$sql_event = "SELECT * FROM event_pay WHERE 1 AND ep_state = 'y' ";
$result_event = sql_query($sql_event);
if(sql_num_rows($result_event) != 0){
	while ($row_event = sql_fetch_array($result_event)) {
		array_push($event_arr, $row_event['ep_no']);
	}
}
// 이벤트 번호 큰수 하나만
$ce_epc_free = $ce_epc_sale = '';
if(count($event_arr) > 0){
	$sql_event_comics_in = implode(", ",$event_arr);
	$sql_event_comics = "SELECT * FROM event_pay_comics WHERE 1 AND epc_ep_no in(".$sql_event_comics_in.") AND epc_comics = '".$comics['cm_no']."' ORDER BY epc_ep_no DESC LIMIT 0, 1";
	$row_event_comics = sql_fetch($sql_event_comics);
	$ce_epc_free = $row_event_comics['epc_free'];
	$ce_epc_sale = $row_event_comics['epc_sale'];
}

if(!$_episode) {
	cs_alert("선택된 화가 없습니다.", HTTP_REFERER);
	die;
}

/* ////////////////// 소유한 작품 체크 ////////////////// */
$mb_buy_episode_list = array();
$mb_get_buy_episode = mb_get_buy_episode($nm_member, $comics['cm_no'], 'y');

// 에피소드
$goto_url = ""; // 전체결제후 바로 이동할 URL	
$sql = "SELECT * FROM comics_episode_".$comics['cm_big']." WHERE 1 AND ce_comics = '".$comics['cm_no']."' AND ce_no in (".$_episode.") ORDER BY ce_order";
$result = sql_query($sql);
for ($e=0; $row=sql_fetch_array($result); $e++) {
	$episode = $row;
	 // 전체결제후 바로 이동할 URL	
	if($e==0){ $goto_url = NM_URL."/comicsview.php?comics=".$row['ce_comics']."&episode=".$row['ce_no'].""; }
	
	/* ////////////////// 체크 변수 초기화////////////////// */
	$episode['ce_event_free'] = $episode['ce_event_sale'] = $event_sale = $event_free = $comics_buy = $comics_free = $state_except = 'n'; 
	// 이벤트세일 = 이벤트무료 = 코믹스무료 = 통계제외 = n
	$comics_buy = array();
	$comics_buy['sl_event'] =  $comics_buy['sl_sale'] =  $comics_buy['sl_free'] = 0;
	$comics_buy['mb_cash_point'] = $comics_buy['mb_point'] = $comics_buy['sl_pay'] = 0;
	$comics_buy['mbc_own_type'] = $comics['cm_own_type'];

	/* ////////////////// 회원 체크 ////////////////// */
	if($nm_member['mb_class'] == "a" || $_SESSION['ss_adm_id'] != "" || $_SESSION['ss_adm_id'] != NULL){ $state_except = $comics_free = "y"; }
	if($nm_member['mb_class'] == "p"){ 
		$state_except = "y";
		// 파트너 자신의 작품에 한해서 무료 170912
		$comics_free = cs_partner_return_y($nm_member, $comics);
	} // end if

	/* ////////////////// 무료인지 체크 ////////////////// */
	// 에피소드 가격
	if($episode['ce_pay'] == 0){ 
		$comics_free = "y"; 
		$comics_buy['sl_free'] = 1;
		$comics_buy['mbc_own_type'] = 0;
	}

	// 코믹스 무료 화수
	if( 0 < $episode['ce_chapter'] && $episode['ce_chapter'] <= $comics['cm_free'] && $episode['ce_pay'] != 0){
		$comics_free = "y";
		$comics_buy['sl_free'] = 1;
		$comics_buy['mbc_own_type'] = 0;
	}

	/* ////////////////// 이벤트 체크 ////////////////// */
	// 이벤트 무료
	if($ce_epc_free == 'y'){
		if( 0 < $episode['ce_chapter'] && $episode['ce_chapter'] <= $row_event_comics['epc_free_e'] && $episode['ce_chapter'] >= $row_event_comics['epc_free_s'] && $episode['ce_pay'] != 0){
			$episode['ce_event_free'] = $event_free = $comics_free = "y";
			$comics_buy['sl_free'] = $comics_buy['sl_event'] = 1;
			$comics_buy['mbc_own_type'] = 0;
		} 
	}

	// 이벤트 코인할인
	if($ce_epc_sale == 'y'){
		if( 0 < $episode['ce_chapter'] && $episode['ce_chapter'] <= $row_event_comics['epc_sale_e'] && $episode['ce_chapter'] >= $row_event_comics['epc_sale_s'] && $episode['ce_pay'] != 0){
			$episode['ce_pay'] = $row_event_comics['epc_sale_pay'];
			$episode['ce_event_sale'] = $event_sale = "y";
			$comics_buy['sl_sale'] = $comics_buy['sl_event'] = 1;
		}			
	}

	/* ////////////////// 소유한 작품 체크 ////////////////// */
	if(in_array($episode['ce_no'], $mb_get_buy_episode)){
		$comics_free = "y";
		$comics_buy['sl_event'] =  $comics_buy['sl_sale'] =  $comics_buy['sl_free'] = 0;
		$comics_buy['mb_cash_point'] = $comics_buy['mb_point'] = $comics_buy['sl_pay'] = 0;
	}

	/* ////////////////// 결제 ////////////////// */
	if($comics_free == 'n'){ 
		$comics_buy = cs_set_expen_episode($comics, $episode, $nm_member, $comics_buy, 7, 'n');  // 북마크포함
		/* 투표 관련 함수 */
		vote_z_event_vote_pay(NM_VOTE_YEAR, $comics['cm_no'], $episode['ce_no'], $comics_buy);
		/* 랜덤박스 관련 함수 */
		if(randombox_state_get() != "") {
			randombox_member_push($nm_member, $comics_buy, randombox_state_get()); // 이벤트 번호 제어 randombox_state_get() 사용 180213
		} // end if

		/* 18-11-23 크리스마스 이벤트 관련 함수 -> event.lib.php */
		if(event_christmas_state_get() != "") {
			event_christmas_point_used_push($nm_member, $comics_buy, event_christmas_state_get());
		} // end if

		$mb_all_pay_count++;
		
		// 화번호 비교
		// echo ($ce_chapter_pre+1)." == ".intval($episode['ce_chapter'])."<br/>";

		if($mb_all_pay_count > 1){
			if($ce_chapter_pre+1 == intval($episode['ce_chapter'])){
				$ce_chapter_pre = intval($episode['ce_chapter']);
			}else{
				if($ce_chapter_pre > 0){
					$mpu_from.= "~".$ce_chapter_pre;
				}
				$mpu_from.= $d_cm_up_kind[$comics['cm_up_kind']].",";
				
				
				if(intval($episode['ce_chapter']) < 0){
					$mpu_from.= $episode['ce_notice'];
				}else{
					$mpu_from.= $episode['ce_chapter'];
				}
				$ce_chapter_pre = intval($episode['ce_chapter']);
			}
		}else{
			// 사용내역에서 사용할 내용
			if($mb_all_pay_count == 1){ // 첫번째만
				if(intval($episode['ce_chapter']) < 0){
					$mpu_from = $comics['cm_series'].'<br/>'.$episode['ce_notice'];
				}else{
					$mpu_from = $comics['cm_series'].'<br/>'.$episode['ce_chapter'];
				}
				$ce_chapter_pre = intval($episode['ce_chapter']);
			}
		}
	}

	/* ////////////////// 통계 ////////////////// */
	if($state_except == 'n' && $comics_free == 'n'){ //sales, sales_age_sex, sales_episode_1, sales_episode_1_age_sex
		$comics_buy = cs_set_sales_data($comics, $episode, $nm_member, $comics_buy, 'n', 7);
		/* 투표 관련 함수 */
		vote_z_event_vote_comicsview(NM_VOTE_YEAR, $comics['cm_no'], $episode['ce_no']);
	}
}

if($ce_chapter_pre > 0){
	$mpu_from.= "~".$ce_chapter_pre;
}
$mpu_from_b = $mpu_from.$d_cm_up_kind[$comics['cm_up_kind']]." (총:".$mb_all_pay_count.$d_cm_up_kind[$comics['cm_up_kind']]." 전체구입)";
$mpu_from_e = $mpu_from.$d_cm_up_kind[$comics['cm_up_kind']]." (총:".$mb_all_pay_count.$d_cm_up_kind[$comics['cm_up_kind']].") 전체구입보너스";

// 사용내역 저장
$prev_mb_cash_point = intval($nm_member['mb_cash_point']);
$prev_mb_point = intval($nm_member['mb_point']);

// 회원정보 포인트 처리 후 재로드을 위해서...
$nm_member = mb_get($nm_member['mb_id']);
$next_mb_cash_point = intval($nm_member['mb_cash_point']);
$next_mb_point = intval($nm_member['mb_point']);
$mb_cash_point_use = $prev_mb_cash_point - $next_mb_cash_point;
$mb_point_use = $prev_mb_point - $next_mb_point;

cs_set_member_point_used_expen($comics, $nm_member, $mb_cash_point_use, $mb_point_use, $next_mb_cash_point, $next_mb_point, $mb_all_pay_count, $mpu_from_b, 7);

// 전체구매 보너스코인
$sin_allpay_arr = cs_sin_allpay();
$mb_set_allpay_point = 0;
// print_r($sin_allpay_arr);
for($i=0; $i<count($sin_allpay_arr); $i++){
	if($i>0){ $sin_allpay_pre = $sin_allpay_arr[$i-1]['csa_buy_count'];} // 이전값 가져오기
	
	if($sin_allpay_arr[$i]['csa_buy_than'] == 'n'){
		if($sin_allpay_pre <= $mb_all_pay_count && $mb_all_pay_count < $sin_allpay_arr[$i]['csa_buy_count']){
			$mb_set_allpay_point = $sin_allpay_arr[$i]['csa_point'];
		}
	}else{
		if($sin_allpay_arr[$i]['csa_buy_count'] <= $mb_all_pay_count){
			$mb_set_allpay_point = $sin_allpay_arr[$i]['csa_point'];
		}	
	}
}

if(intval($mb_set_allpay_point) > 0){
	cs_set_member_point_income_event($nm_member, 0, $mb_set_allpay_point, 'allpay', $mpu_from_e);
}
goto_url($goto_url);
?>