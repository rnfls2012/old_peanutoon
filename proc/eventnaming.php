<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동

cs_login_check();

$squirrel_name = $peanut_name = $mini_peanut_name = "";
if($_POST['squirrel_name'] && $_POST['peanut_name'] && $_POST['mini_peanut_name']) {
	$squirrel_name = $_POST['squirrel_name'];
	$peanut_name = $_POST['peanut_name'];
	$mini_peanut_name = $_POST['mini_peanut_name'];
	
	if(!(preg_match('/([^a-zA-Z가-힣]\s)/', $squirrel_name)) && !(preg_match('/([^a-zA-Z가-힣]\s)/', $peanut_name)) && !(preg_match('/([^a-zA-Z가-힣]\s)/', $mini_peanut_name))) {
		$insert_sql = "INSERT INTO event_naming(en_mb_no, en_mb_id, en_mb_idx, en_s_name, en_p_name, en_mp_name, en_reg_date) VALUES('".$nm_member['mb_no']."', '".$nm_member['mb_id']."', '".$nm_member['mb_idx']."', '".$squirrel_name."', '".$peanut_name."', '".$mini_peanut_name."', '".NM_TIME_YMDHIS."')";

		if(sql_query($insert_sql)) {
			cs_alert("응모되었습니다!", HTTP_REFERER);
			die;
		} else {
			cs_alert("응모가 정상적으로 이루어 지지 않았습니다. 관리자에게 문의하세요!", HTTP_REFERER);
			die;
		} // end else
	} else {
		cs_alert("한글과 영문으로만 이름을 지어주세요!", HTTP_REFERER);
		die;
	} // end else
} else {
	cs_alert("이름을 모두 입력해주세요!", HTTP_REFERER);
	die;
} // end else 
?>