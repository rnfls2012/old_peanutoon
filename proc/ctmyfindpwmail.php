<? include_once '_common.php'; // 공통
include_once(NM_LIB_PATH.'/PHPMailer/class.phpmailer.php'); //메일
include_once(NM_LIB_PATH.'/PHPMailer/class.smtp.php');

$send_email = base_filter($_REQUEST['idpw_mail']);

$ctmyfind_url = NM_URL.'/ctmyfindpw.php';

if($send_email == ''){
	cs_alert("이메일을 입력하셔야 합니다.", $ctmyfind_url);
	die;
}

if(email_check($send_email) == false) {
	cs_alert("이메일 형식으로 입력해 주세요!", $ctmyfind_url);
	die;
}

$charset = 'UTF-8'; 
$mb_pw = ""; 
$mb_email_token = substr(strtolower(random_string("alnum", 30)).time().get_eng($send_email), 0, 190);

// email_idx, email_domain
$send_email_idx = substr($send_email, 0, 1);
$send_email_domain = substr($send_email, strpos($send_email, '@')+1, strlen($send_email));

$sql_mb_email = "
 SELECT * FROM member WHERE mb_state='y' 
 AND mb_email_idx='".$send_email_idx."' AND mb_email_domain='".$send_email_domain."' 
 AND mb_email='".$send_email."' 
";
$result_mb_email = sql_query($sql_mb_email);
$row_size_mb_email = sql_num_rows($result_mb_email);

$email_url = NM_URL."/ctmyfindpw_direct.php?mb_email_token=".urlencode($mb_email_token)."&mb_email_idx=".$send_email_idx."&mb_email_domain=".$send_email_domain;
$email_arr = array();
$content = "안녕하세요.".$nm_config['cf_title']." 입니다.<br/><br/>";
$content.= "해당 URL 접속하셔서 패스워드 변경하시기 바랍니다.<br/><br/>";
$content.= "<a href='".$email_url."'>";
$content.= $email_url;
$content.= "</a><br/><br/>";
$content.= "변경이 안되면 문의하시기 바랍니다.<br/><br/>";
$content.= "<strong>문의 이메일 : ".$nm_config['cf_admin_email']."</strong><br/>";

$mb_email_chk_arr = array();
if($row_size_mb_email > 0){
	while ($row_mb_email = sql_fetch_array($result_mb_email)) {
		$tmp_email = $row_mb_email['mb_email'];
		if($row_mb_email['mb_email'] == '' || email_check($row_mb_email['mb_email'])){
			$tmp_email = $row_mb_email['mb_id'];
		}

		if(email_check($tmp_email)){
			$sql_up = " UPDATE member SET mb_email_token ='".$mb_email_token."' WHERE mb_no='".$row_mb_email['mb_no']."' AND mb_idx='".$row_mb_email['mb_idx']."' ";
			if(sql_query($sql_up)){
				$sql_in = " INSERT INTO x_ctmyfindpwmail ( 
				x_cmfm_send_email, x_cmfm_send_email_token, x_cmfm_sql, x_cmfm_sql_up, 
				x_send_email, x_send_email_content, x_cmfm_state, x_cmfm_date ) VALUES(
				'".$send_email."', '".$mb_email_token."', \"".$sql_mb_email."\", \"".$sql_up."\", 
				'', \"".$content."\", 'y', '".NM_TIME_YMDHIS."' )
				";
				sql_query($sql_in);
				$row_mb_email['x_cmfm_state'] = 'y';
				array_push($mb_email_chk_arr, $row_mb_email);
				
				/* 메일 작성 */
				if($row_mb_email['mb_nick'] != ''){			$wr_name = $row_mb_email['mb_nick']; }
				else if($row_mb_email['mb_name'] != ''){	$wr_name = $row_mb_email['mb_name']; }
				else{										$wr_name = $row_mb_email['mb_id']; }

				$email_to = $tmp_email;
				$email_from = $nm_config['cf_admin_email'];
				$email_from_name = $nm_config['cf_admin_email_name'];
				$email_subject = $nm_config['cf_admin_email_name']."에서 패스워드 변경할수 있는 URL이 도착하였습니다.";
				// mailer($wr_name, $email_from, 'idpwfind', $email_to, $email_subject, $content, 1);	->받는사람, 보내는 사람이 바낌 17-09-07
				mailer($email_from_name, $email_from, $wr_name, $email_to, $email_subject, $content, 1);	

			}else{
				$sql_in = " INSERT INTO x_ctmyfindpwmail ( 
				x_cmfm_send_email, x_cmfm_send_email_token, x_cmfm_sql, x_cmfm_sql_up, 
				x_send_email, x_send_email_content, x_cmfm_state, x_cmfm_date ) VALUES(
				'".$send_email."', '".$mb_email_token."', \"".$sql_mb_email."\", \"".$sql_up."\", 
				'', \"".$content."\", 'n', '".NM_TIME_YMDHIS."' )
				";
				sql_query($sql_in);
				$row_mb_email['x_cmfm_state'] = 'n';
				array_push($mb_email_chk_arr, $row_mb_email);
			}
		}else{
			$sql_in = " INSERT INTO x_ctmyfindpwmail ( 
			x_cmfm_send_email, x_cmfm_send_email_token, x_cmfm_sql, x_cmfm_sql_up, 
			x_send_email, x_send_email_content, x_cmfm_state, x_cmfm_date ) VALUES(
			'".$send_email."', '".$mb_email_token."', \"".$sql_mb_email."\", '', 
			'', \"".$content."\", 'n', '".NM_TIME_YMDHIS."' )
			";
			sql_query($sql_in);
			$row_mb_email['x_cmfm_state'] = 'n';
			array_push($mb_email_chk_arr, $row_mb_email);
		}
	}
	
	$cs_alert_msg = '';
	foreach($mb_email_chk_arr as $mb_email_chk_key  => $mb_email_chk_val){
		$cs_alert_msg_state = '발신되였습니다.<br/>\n확인해보시기 바랍니다.';
		if($mb_email_chk_val['x_cmfm_state'] == 'n'){ $cs_alert_msg_state = '발신되지 않았습니다.[관리자에게 문의 바랍니다.]'; }
		$cs_alert_msg.= $mb_email_chk_val['mb_id'].' 회원님 '.$mb_email_chk_val['mb_email'].'의 e-mail에 '.$cs_alert_msg_state.'<br/>\n';
	}
	cs_alert($cs_alert_msg, $ctmyfind_url);

}else{
	$sql_in = " INSERT INTO x_ctmyfindpwmail ( 
	x_cmfm_send_email, x_cmfm_send_email_token, x_cmfm_sql, x_cmfm_sql_up, 
	x_send_email, x_send_email_content, x_cmfm_state, x_cmfm_date ) VALUES(
	'".$send_email."', '".$mb_email_token."', \"".$sql_mb_email."\", '', 
	'', '조회값0입니다.', '0', '".NM_TIME_YMDHIS."' )
	";
	sql_query($sql_in);
	cs_alert("조회값0입니다.", $ctmyfind_url);
}

?>