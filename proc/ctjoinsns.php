<?
include_once '_common.php'; // 공통
include_once(NM_OAUTH_PATH.'/functions.php');

$mb_post = 'n';
if($_REQUEST['regi_event']) {
	$mb_post = 'y';
} // end if

$app_mb_token		= base_filter($_REQUEST['mb_token']);

// 상태체크, 가입체크
$state	= $mb_join_state	= 0;
$msg	= '';

/* 가입완료페이지 */
$link = NM_URL."/ctjoinup.php?sns=join";

$zmbsj_id_encrypt_aes256 = $_REQUEST['zmbsj_id'];

$zmbsj_id = Security::decrypt_aes256(rawurldecode($zmbsj_id_encrypt_aes256), NM_JOIN_SNS_ENC_KEY);
$zmbsj_id_arr = explode("_", $zmbsj_id);
$zmbsj_sns_type = $zmbsj_id_arr[1]; 
$service = $zmbsj_sns_service = get_oauth_full($zmbsj_sns_type); 



$sql_zmbsj = "
SELECT * FROM z_member_sns_join WHERE 
zmbs_id = '".$zmbsj_id."' AND 
zmbs_idx = '".mb_get_idx($zmbsj_id)."' AND 
zmbsj_sns_type = '".$zmbsj_sns_type."' ";


$row_zmbsj = sql_fetch($sql_zmbsj);

if($row_zmbsj['zmbsj_id'] == ""){ 
	$state	= 99;

	$x_mbs_join_msg = "소셜 로그인 필수값인 ID이 넘어오지 않았습니다.";
	$x_mbs_join_msg.= $nm_config['cf_admin_email']."에 문의해주시기 바랍니다.";
	$x_mbs_join_msg.= "( join sns_id : zmbsj_id_null Error [".NM_TIME_YMDHIS."] )";

	get_oauth_x_mbs('x_member_sns_join', $service, $sql_zmbsj."-".$zmbsj_id_encrypt_aes256, $x_mbs_join_msg, $state);

	alert($x_mbs_join_msg, NM_URL);
	die;

}else{
	/* 회원 있는지 체크 */
	$sql_mb = " SELECT count(*) as mb_total FROM member 
				WHERE mb_id = '".$zmbsj_id."' AND mb_idx ='".mb_get_idx($zmbsj_id)."' AND  mb_state='y' "; // 1. 상태 처리
	$mb_total = sql_count($sql_mb, 'mb_total');

	if($mb_total > 0){
		$state	= 1;
		
		$x_mbs_join_msg = "소셜 로그인 필수값인 ID가 중복입니다.";
		$x_mbs_join_msg.= $nm_config['cf_admin_email']."에 문의해주시기 바랍니다.";
		$x_mbs_join_msg.= "( join sns_id : ".$row_zmbsj['mb_id']." - ".$mb_total."  Error [".NM_TIME_YMDHIS."] )";

		get_oauth_x_mbs('x_member_sns_join', $service, $row_zmbsj['mb_id'], $x_mbs_join_msg, $state);

		alert($x_mbs_join_msg, NM_URL.'/ctlogin.php');
		die;
	}else{

		// member_email
		$zmbsj_email = "'".$row_zmbsj['zmbsj_email']."'";
		$zmbsj_email_idx = $zmbsj_email_domain = "";

		if($row_zmbsj['zmbsj_email'] == ''){ 
			$zmbsj_email = "NULL"; 
			$zmbsj_email_idx = $zmbsj_email_domain = "";
		}else{
			$zmbsj_email_idx = substr($row_zmbsj['zmbsj_email'], 0, 1);
			$zmbsj_email_domain = substr($row_zmbsj['zmbsj_email'], strpos($row_zmbsj['zmbsj_email'], '@')+1, strlen($row_zmbsj['zmbsj_email']));
		}

		$sql_mb_insert = "	INSERT INTO member( mb_id, mb_idx, mb_pass, 
												mb_email, mb_email_idx, mb_email_domain , 
												mb_".$zmbsj_sns_service."_id, mb_sns_type,  
												mb_nick, mb_name, mb_level,  
												mb_join_date, mb_login_date, mb_post, mb_token ) VALUES (
												'".$row_zmbsj['zmbsj_id']."', '".$row_zmbsj['zmbsj_idx']."', '".$row_zmbsj['zmbsj_pass']."', 
												".$zmbsj_email.", '".$zmbsj_email_idx."', '".$zmbsj_email_domain."', 
												'".$row_zmbsj['zmbsj_'.$zmbsj_sns_service.'_id']."', '".$row_zmbsj['zmbsj_sns_type']."', 
												'".get_int_eng_kor_etc(tag_get_filter($row_zmbsj['zmbsj_nick']))."', 
												'".get_int_eng_kor_etc(tag_get_filter($row_zmbsj['zmbsj_name']))."', 
												'".$row_zmbsj['zmbsj_level']."', 
												'".$row_zmbsj['zmbsj_date']."', '".NM_TIME_YMDHIS."', 
												'".$mb_post."', '".$app_mb_token."'
												)";
		if(sql_query($sql_mb_insert)){

		}else{
			$state	= 88;

			$x_mbs_join_msg = "소셜 로그인 가입 에러 입니다.";
			$x_mbs_join_msg.= $nm_config['cf_admin_email']."에 문의해주시기 바랍니다.";
			$x_mbs_join_msg.= "( join sns_id : ".$row_zmbsj['mb_id']." - ".$mb_total."  Error [".NM_TIME_YMDHIS."] )";

			get_oauth_x_mbs('x_member_sns_join', $service, $sql_mb_insert, $x_mbs_join_msg, $state);

			alert($x_mbs_join_msg, NM_URL.'/ctlogin.php');
			die;
		}

		$mb_sns =  mb_get($row_zmbsj['zmbsj_id']);

		// 회원정보 업데이트 & 회원로그
		mb_login_date_update($mb_sns, $app_mb_token);
		mb_stats_log_member_update($mb_sns);
		
		// 마케팅 가입이라면...
		mkt_marketer_join($mb_sns);
		// 티켓소켓 가입이라면...		
		tksk_ics_join($mb_sns);
		// 친구초대 가입이라면...
		pf_invite_join($mb_sns);
		// 초대코드 업데이트
		mb_set_invite_code($mb_sns);
		// mkt - ICE COUNTER JOIN
		rdt_acecounter_ss_join('y');
		
		// 자동로그인 ---------------------------
		// 쿠키 3달간 저장
		$key = cs_mb_auto_key($mb_sns);

		set_cookie('ck_mb_id', $mb_sns['mb_id'], 86400 * 90);
		set_cookie('ck_mb_auto', $key, 86400 * 90);

		$_SESSION['ss_login_state'] = $state;

		$_SESSION['ss_login_id'] = '';
		$_SESSION['ss_login_pw'] = '';
	
		unset($sql_mb, $mb_total, $mb_overlap, $mb_sns);
		goto_url($link);
	}
}

?>