<?
include_once '_common.php'; // 공통

$http_referer		= base_filter($_POST['http_referer']);
$login_id			= base_filter($_POST['login_id']);
$login_pw			= trim($_POST['login_pw']);
$login_id_save		= base_filter($_POST['login_id_save']); // 아이디 저장
$login_auto_save	= base_filter($_POST['login_auto_save']); // 로그인 상태 유지
$adult_yn			= base_filter($_POST['adult_yn']); // 19클릭
$app_mb_token	= base_filter($_REQUEST['mb_token']);
// $_SESSION['token'] = $_POST['token'];

$redirect		= base_filter($_POST['redirect']); // 리다이렉트 추가-18-12-03

$login_pw_sha1 = '';
if(MEMCACHED == true){
	$password_sha1 = trim(strip_tags(stripslashes($_POST['login_pw'])));
	$login_pw_sha1 = bin2hex(mhash(MHASH_SHA1, $password_sha1));
}

$state = 0;

// $link = $http_referer;
if($redirect !=''){
	$link = goto_redirect($redirect);
}else{
	$link = $http_referer;
}

preg_match("/".etc_filter(NM_URL)."/i", etc_filter($http_referer),  $http_referer_ckeck);
if(count($http_referer_ckeck) < 1 || $http_referer == ''){ $link = NM_URL; }
// 프로세스로 넘어가는 referer일 경우도 홈페이지...
if(strpos($http_referer, '/proc/') > 0){ $link = NM_URL; }

if($is_member == true){
	$link = NM_URL;
	// 로그인 세션 초기화
	$_SESSION['ss_login_id'] = '';
	$_SESSION['ss_login_pw'] = '';
	$_SESSION['ss_login_state'] = '';
}else{
	// 로그인 세션 설정
	$_SESSION['ss_login_id'] = $login_id;
	$_SESSION['ss_login_pw'] = $login_pw;

	if ($login_id == "" || $login_pw == ""){
		/* 로그인 아이디 또는 패스워드가 빈 값이라면 튕겨내기 */
		$link = NM_URL."/ctlogin.php";
		$state = 1;
	}else{

		$login_idx = mb_get_idx($login_id);

		/* 회원 있는지 중복체크 */
		$sql_mb = " SELECT count(*) as mb_total FROM member 
					WHERE mb_id = '".$login_id."' AND mb_idx ='".$login_idx."' AND  mb_state='y'"; // 1. 상태 처리
		$mb_total = sql_count($sql_mb, 'mb_total');

		if($mb_total > 1){
			// 181024 - 중복로그인 처리 / 초대코드로 검색
		    $sql_mb_mb_invite_code = "  SELECT count(*) as mb_total FROM member 
										WHERE mb_id = '".$login_id."' AND mb_idx ='".$login_idx."' 
										AND mb_state='y' AND mb_invite_code is not null; "; // 검색
			$mb_mb_invite_code_total = sql_count($sql_mb_mb_invite_code, 'mb_total');

			if($mb_mb_invite_code_total != 1){
				// 초대코드로 검색
				$sql_overlap_only_find = "SELECT *, min(mb_no) FROM member 
					WHERE mb_id = '".$login_id."' AND mb_idx ='".$login_idx."' AND  mb_state='y';"; // 1. 상태 처리
			}else{
				$sql_overlap_only_find = "SELECT *, min(mb_no) FROM member 
					WHERE mb_id = '".$login_id."' AND mb_idx ='".$login_idx."' AND  mb_state='y'
					AND mb_invite_code is not null;"; // 1. 상태 처리
			}
			$row_overlap_only_find = sql_fetch($sql_overlap_only_find);
			// 위 빼고 전부 다 탈퇴상태로 전환
			if(intval($row_overlap_only_find['mb_no']) > 0){
				$sql_update_overlap_only_find="
						UPDATE member set mb_state='n', mb_state_sub='a', mb_out_date='".NM_TIME_YMDHIS."' 
						WHERE mb_id = '".$login_id."' AND mb_idx ='".$login_idx."' AND  mb_state='y'
						AND not mb_no ='".$row_overlap_only_find['mb_no']."';
				";
				sql_query($sql_update_overlap_only_find);
				/* 회원 있는지 중복체크 - 재확인 */
				$sql_mb2 = " SELECT count(*) as mb_total FROM member 
							WHERE mb_id = '".$login_id."' AND mb_idx ='".$login_idx."' AND  mb_state='y'"; // 1. 상태 처리
				$mb_total2 = sql_count($sql_mb2, 'mb_total');
				if($mb_total2 > 1){
					$link = NM_URL."/ctlogin.php";
					$state = 2;
				}else{
					$mb_total = $mb_total2;
				}
			}else{
				$link = NM_URL."/ctlogin.php";
				$state = 2;
			}
		}

		// }else if($mb_total == 1){
		if($mb_total == 1){
			$mb = mb_get($login_id);
			// 가입된 회원이 아니다. 비밀번호가 틀리다. 라는 메세지를 따로 보여주지 않는 이유는
			// 회원아이디를 입력해 보고 맞으면 또 비밀번호를 입력해보는 경우를 방지하기 위해서입니다.
			// 불법사용자의 경우 회원아이디가 틀린지, 비밀번호가 틀린지를 알기까지는 많은 시간이 소요되기 때문입니다.			
			/* 3. 패스워드 업데이트 */
			if($mb['customer_password'] == 'y'){
				if($mb['mb_pass'] == $login_pw_sha1){
					// 같다면 업데이트
					$sql_password = sql_password($login_pw);
					$sql_update = " UPDATE member SET mb_pass='".$sql_password."', customer_password='n' WHERE mb_no='".$mb['mb_no']."' ";
					sql_query($sql_update);
				}else{
					$link = NM_URL."/ctlogin.php";
					$state = 3;
				}
			}else{
				if (!$mb['mb_id'] || !check_password($login_pw, $mb['mb_pass'])) {
					$link = NM_URL."/ctlogin.php";
					$state = 3;
				}
			}

			// 탈퇴한 아이디인가?
			if ($mb['mb_state'] == 'n') {
				$link = NM_URL."/ctlogin.php";
				$state = 5;
			}

			// 차단된 아이디인가?
			if ($mb['mb_state'] == 'y' && $mb['mb_state_sub'] == 's') {
				$link = NM_URL."/ctlogin.php";
				cs_alert("이용 중지된 아이디 입니다. 관리자에게 문의하세요!", $link);
				$state = 4;
				die;
			}

			// 휴면한 아이디인가?
			if ($mb['mb_state'] == 'y' && $mb['mb_state_sub'] == 'h') {
				$_SESSION['ss_human_id'] = $login_id;
				$_SESSION['ss_human_referer'] = $http_referer;
				$link = NM_URL."/sleepcancel.php";
				$state = 6;
			}

			// 휴면 대기인 아이디인가?
			if($mb['mb_state'] == 'y' && $mb['mb_state_sub'] == 't') {
				$state_sub_sql = "UPDATE member set mb_state_sub='y', mb_human_email_date='' WHERE mb_no='".$mb['mb_no']."' ";
				sql_query($state_sub_sql);
			} // end if

		}else{		
			$link = NM_URL."/ctlogin.php";
			$state = 7;
		}
	}

	if($state == 0){
		// 로그인 세션 초기화
		$_SESSION['ss_login_id'] = '';
		$_SESSION['ss_login_pw'] = '';
		// 회원아이디 세션 생성
		set_session('ss_mb_id', $mb['mb_id']);
		set_session('ss_mb_no', $mb['mb_no']);
		// FLASH XSS 공격에 대응하기 위하여 회원의 고유키를 생성해 놓는다. 관리자에서 검사함 - 110106
		set_session('ss_mb_key', md5($mb['mb_join_date'] . REMOTE_ADDR . $_SERVER['HTTP_USER_AGENT']));

		// 회원정보 업데이트 & 회원로그
		mb_login_date_update($mb, $app_mb_token);
		mb_stats_log_member_update($mb);

		// mkt - ICE COUNTER LOGIN
		rdt_acecounter_ss_login('y');

		//남성 성인 회원 로그인 시 성인 장르로 유도 URL(지시사항) - 171206
		if(substr($http_referer, 0, -1) == NM_DOMAIN || $http_referer == NM_DOMAIN || $http_referer == NM_DOMAIN."/index.php" || $http_referer == ""){
			if($mb['mb_adult'] == 'y' && $mb['mb_sex'] == 'm'){ 	
				$link = NM_DOMAIN."/cmgenre.php?menu=6";
				if($redirect !=''){
					$link = goto_redirect($redirect);
				}
			}
		}

		if($login_auto_save == 'y'){
			// 자동로그인 ---------------------------
			// 쿠키 3달간 저장
			$key = cs_mb_auto_key($mb);

			set_cookie('ck_mb_id', $mb['mb_id'], 86400 * 90);
			set_cookie('ck_mb_auto', $key, 86400 * 90);
		} else {
			set_cookie('ck_mb_id', '', 0);
			set_cookie('ck_mb_auto', '', 0);
		}

		if($login_id_save == 'y'){
			// 아이디 저장
			// 쿠키 3달간 저장
			set_cookie('ck_mb_id', $mb['mb_id'], 86400 * 90);
		}
	}
	$_SESSION['ss_login_state'] = $state;
}

// echo $state;
if($mb['mb_adult'] == 'y') { $adult_yn = 'y'; } // 성인이 로그인 했을때는 성인탭으로 강제 이동 171110
if($adult_yn != ''){ 	$link = cs_para_attach($link, 'adult='.$adult_yn); }
goto_url($link);
?>
