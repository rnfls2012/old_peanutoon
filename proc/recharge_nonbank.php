<? include_once '_common.php'; // 공통

// PAYCO 무통장입금 처리 통보 API 페이지 샘플 ( PHP )
// payco_without_bankbook.php
// 2015-03-25	PAYCO기술지원 <dl_payco_ts@nhnent.com>
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// 이 문서는 text/html 형태의 데이터를 반환합니다. ( OK 또는 ERROR 만 반환 )
//-------------------------------------------------------------------------------
header('Content-type: text/html; charset: UTF-8');
include(NM_PAYCO_PATH."/cfg/payco_config.php");

try {

	//-----------------------------------------------------------------------------
	// 오류가 발생했는지 기억할 변수와 결과를 담을 변수를 선언합니다.
	//-----------------------------------------------------------------------------
	$ErrBoolean = True;		// 미리 오류라고 가정

	$readValue	= stripslashes($_REQUEST["response"]);		   // PHP 5.2 / PHP 함수 stripslashes() 사용하여, Payco 에서 송신하는 값(response)을 JSON 형태로 변경하기전 백슬래시 기호 제거
	//$readValue	= $_REQUEST["response"];				   // PHP 5.3 이상에서 적용 / Payco 에서 전달하는 값(response)을 저장


	//-----------------------------------------------------------------------------
	// (로그) 호출 시점과 호출값을 파일에 기록합니다.
	//-----------------------------------------------------------------------------
	Write_Log("payco_without_bankbook.php is Called - response : $readValue");
	cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "readValue", "", $readValue, 1, "payco_without_bankbook.php is Called - response"); // state-1

	//-----------------------------------------------------------------------------
	// POST 값 중 response 값이 없으면 에러를 표시하고 API를 종료합니다.
	//-----------------------------------------------------------------------------
	if( $readValue == "" ){
		$resultValue = "Parameter is nothing.";
		Write_Log("payco_without_bankbook.php send Result : ERROR ($resultValue)");
		cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "readValue", "", $readValue, 2, "payco_without_bankbook.php send Result : ERROR (".$resultValue.")"); // state-2
		echo "ERROR";
		return;
	}

	//-----------------------------------------------------------------------------
	// Payco 에서 송신하는 값(response)을 JSON 형태로 변경
	// 데이터 확인에 필요한 값을 변수에 담아 처리합니다.
	//-----------------------------------------------------------------------------
	$Read_Data = json_decode($readValue, true);

	//-----------------------------------------------------------------------------
	// 수신 데이터 사용 예제( 주문서 찾기 )
	//-----------------------------------------------------------------------------
	$sellerOrderReferenceKey	= $Read_Data["sellerOrderReferenceKey"];			// 가맹점에서 발급하는 주문 연동 Key
	$reserveOrderNo				= $Read_Data["reserveOrderNo"];						// 주문예약번호
	$orderNo					= $Read_Data["orderNo"];							// 주문번호
	$memberName					= $Read_Data["memberName"];							// 주문자명

	//-----------------------------------------------------------------------------
	// ...
	// 기타 주문서 생성에 필요한 정보를 가지고 주문서를 조회합니다.
	// 예) 무통장 입금 확인 필드 업데이트
	//-----------------------------------------------------------------------------
	$paymentCompletionYn = $Read_Data["paymentCompletionYn"];			// 지급완료 값 ( Y/N )
	if( $paymentCompletionYn == "Y" ){
		//-------------------------------------------------------------------------
		//지급이 완료 되었다고 받았으면 지급 완료 처리
		//--------------------------------------------------------------------------
		$ErrBoolean = False;											// 정상처리를 위해 오류 표시를 해제
	}

	//-----------------------------------------------------------------------------
	// 이곳에 가맹점에서 필요한 데이터 처리를 합니다.
	//-----------------------------------------------------------------------------
	$payco_seller_order_reference_key = $sellerOrderReferenceKey;
	$payco_reserve_order_no = $reserveOrderNo;
	$payco_order_no = $orderNo;
	$payco_member_name = $memberName;
	$payco_payment_completion_yn = $paymentCompletionYn;

	$payco_order_no 							= $Read_Data["orderNo"];
	$payco_member_email 						= $Read_Data["memberEmail"];
	$payco_order_channel 						= $Read_Data["orderChannel"];
	$payco_total_order_amt 						= $Read_Data["totalOrderAmt"];
	$payco_total_payment_amt					= $Read_Data["totalPaymentAmt"];

	$payco_order_product_no						= $Read_Data["orderProducts"][0]["orderProductNo"];
	$payco_seller_order_product_reference_key	= $Read_Data["orderProducts"][0]["sellerOrderProductReferenceKey"];
	$payco_order_product_status_code			= $Read_Data["orderProducts"][0]["orderProductStatusCode"];
	$payco_order_product_status_name			= $Read_Data["orderProducts"][0]["orderProductStatusName"];
	$payco_cp_id 								= $Read_Data["orderProducts"][0]["cpId"];
	$payco_product_id 							= $Read_Data["orderProducts"][0]["productId"];

	$payco_product_kind_code					= $Read_Data["orderProducts"][0]["productKindCode"];
	$payco_product_payment_amt					= $Read_Data["orderProducts"][0]["productPaymentAmt"];
	$payco_original_product_payment_amt			= $Read_Data["orderProducts"][0]["originalProductPaymentAmt"];

	$payco_bank_name 							= $Read_Data["paymentDetails"][0]["nonBankbookSettleInfo"]["bankName"];
	$payco_bank_code 							= $Read_Data["paymentDetails"][0]["nonBankbookSettleInfo"]["bankCode"];
	$payco_account_no 							= $Read_Data["paymentDetails"][0]["nonBankbookSettleInfo"]["accountNo"];

	$payco_payment_trade_no						= $Read_Data["paymentDetails"][0]["paymentTradeNo"];
	$payco_payment_method_code					= $Read_Data["paymentDetails"][0]["paymentMethodCode"];
	$payco_payment_method_name					= $Read_Data["paymentDetails"][0]["paymentMethodName"];
	$payco_payment_amt							= $Read_Data["paymentDetails"][0]["paymentAmt"];
	$payco_taxfree_amt 							= $Read_Data["paymentDetails"][0]["taxfreeAmt"];
	$payco_taxable_amt 							= $Read_Data["paymentDetails"][0]["taxableAmt"];
	$payco_vat_amt 								= $Read_Data["paymentDetails"][0]["vatAmt"];
	$payco_service_amt 							= $Read_Data["paymentDetails"][0]["serviceAmt"];

	$payco_trade_ymdt 							= $Read_Data["paymentDetails"][0]["tradeYmdt"];
	$payco_pg_admission_no 						= $Read_Data["paymentDetails"][0]["pgAdmissionNo"];
	$payco_pg_admission_ymdt 					= $Read_Data["paymentDetails"][0]["pgAdmissionYmdt"];
	$payco_easy_payment_yn 						= $Read_Data["paymentDetails"][0]["easyPaymentYn"];

	$payco_order_certify_key 					= $Read_Data["orderCertifyKey"];
	$payco_payment_complete_ymdt 				= $Read_Data["paymentCompleteYmdt"];

	// payco 정보 저장
	$sql_payco_insert = " INSERT INTO pg_channel_payco_nonbank (
		payco_seller_order_reference_key, payco_reserve_order_no, payco_order_no, 
		payco_member_name, payco_member_email, payco_order_channel, 
		payco_total_order_amt, payco_total_payment_amt, 

		payco_order_product_no, payco_seller_order_product_reference_key, 
		payco_order_product_status_code, payco_order_product_status_name, 

		payco_cp_id, payco_product_id, payco_product_kind_code, 
		payco_product_payment_amt, payco_original_product_payment_amt, 

		payco_bank_name, payco_bank_code, payco_account_no, 

		payco_payment_trade_no, payco_payment_method_code, payco_payment_method_name, 
		payco_payment_amt, payco_taxfree_amt, payco_taxable_amt, 
		payco_vat_amt, payco_service_amt, payco_trade_ymdt, 

		payco_pg_admission_no, payco_pg_admission_ymdt, payco_easy_payment_yn, 
		payco_order_certify_key, payco_payment_completion_yn, payco_payment_complete_ymdt, 
		payco_remote_addr, payco_save 
		)VALUES( 
		'".$payco_seller_order_reference_key."', '".$payco_reserve_order_no."', '".$payco_order_no."',
		'".$payco_member_name."', '".$payco_member_email."', '".$payco_order_channel."',
		'".$payco_total_order_amt."', '".$payco_total_payment_amt."', 

		'".$payco_order_product_no."', '".$payco_seller_order_product_reference_key."', 
		'".$payco_order_product_status_code."', '".$payco_order_product_status_name."', 

		'".$payco_cp_id."', '".$payco_product_id."', '".$payco_product_kind_code."',
		'".$payco_product_payment_amt."', '".$payco_original_product_payment_amt."', 

		'".$payco_bank_name."', '".$payco_bank_code."', '".$payco_account_no."',

		'".$payco_payment_trade_no."', '".$payco_payment_method_code."', '".$payco_payment_method_name."',
		'".$payco_payment_amt."', '".$payco_taxfree_amt."', '".$payco_taxable_amt."',
		'".$payco_vat_amt."', '".$payco_service_amt."', '".$payco_trade_ymdt."',

		'".$payco_pg_admission_no."', '".$payco_pg_admission_ymdt."', '".$payco_easy_payment_yn."',
		'".$payco_order_certify_key."', '".$payco_payment_completion_yn."', '".$payco_payment_complete_ymdt."', 
		'".$_SERVER['REMOTE_ADDR']."' , '".NM_TIME_YMDHIS."'  
		)";
	if(sql_query($sql_payco_insert)){
		$ErrBoolean = False;											// 정상처리를 위해 오류 표시를 해제	
	}else{
		// 실패 로그 남기기
		Write_Log("recharge_nonbank.php - 1 \n sql: $sql_payco_insert");
		cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "sql_payco_insert", "", $sql_payco_insert, 3, "payco 정보 저장 실패"); // state-3
	}

	// pg_channel_payco 검색 - 무통장코드 AND 입금대기코드 AND 위 4개 정보로 검색 
	$sql_pg_channel_payco = "SELECT * FROM pg_channel_payco WHERE 1 
							 AND payco_payment_method_code = '02'
							 AND payco_order_product_status_code = 'OPSPWAI' 
							 AND payco_payment_completion_yn = 'N' 
							 AND payco_seller_order_reference_key = '".$sellerOrderReferenceKey."'
							 AND payco_reserve_order_no = '".$reserveOrderNo."'
							 AND payco_approval_order = '".$orderNo."'
							 AND payco_member_name = '".$memberName."'
							 AND payco_bank_code = '".$payco_bank_code."'
							 AND payco_account_no = '".$payco_account_no."'
							";
	$row_pg_channel_payco = sql_fetch($sql_pg_channel_payco);

	// 위 검색이 나왔는지 체크
	if($row_pg_channel_payco['payco_member_id'] == ''){
		// Write_Log("recharge_nonbank.php - 2 \n sql: $sql_pg_channel_payco");
		cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "sql_pg_channel_payco", "", $sql_pg_channel_payco, 4, "pg_channel_payco 검색 - 무통장코드 AND 입금대기코드 AND 위 4개 정보로 검색 실패"); // state-4
	}else{
		$ErrBoolean = False;											// 정상처리를 위해 오류 표시를 해제
		// 가격이 동일한지 체크
		if($row_pg_channel_payco['payco_amt'] == $payco_total_payment_amt){
			$ErrBoolean = False;											// 정상처리를 위해 오류 표시를 해제
			// 우선 상태 업데이트
			$sql_update_pg_channel_payco = " UPDATE pg_channel_payco SET 
											 payco_payment_trade_no = '".$payco_payment_trade_no."', 
											 payco_trade_ymdt = '".$payco_trade_ymdt."', 
											 payco_pg_admission_no = '".$payco_pg_admission_no."', 
											 payco_pg_admission_ymdt = '".$payco_pg_admission_ymdt."', 
											 payco_easy_payment_yn = '".$payco_easy_payment_yn."', 
											 payco_order_product_status_code = '".$payco_order_product_status_code."', 
											 payco_order_product_status_name = '".$payco_order_product_status_name."', 
											 payco_payment_completion_yn = '".$payco_payment_completion_yn."', 
											 payco_complete_date = '".$payco_payment_complete_ymdt."'
											 WHERE 1 
											 AND payco_payment_method_code = '02'
											 AND payco_order_product_status_code = 'OPSPWAI' 
											 AND payco_payment_completion_yn = 'N' 
											 AND payco_seller_order_reference_key = '".$sellerOrderReferenceKey."'
											 AND payco_reserve_order_no = '".$reserveOrderNo."'
											 AND payco_approval_order = '".$orderNo."'
											 AND payco_member_name = '".$memberName."'
											 AND payco_bank_code = '".$payco_bank_code."'
											 AND payco_account_no = '".$payco_account_no."'
											";
			if(sql_query($sql_update_pg_channel_payco)){
				$ErrBoolean = False;											// 정상처리를 위해 오류 표시를 해제
				$nm_member = mb_get($row_pg_channel_payco['payco_member_id']);
				$payco_order = $row_pg_channel_payco['payco_order'];
				
				// payco 저장 정보 가져오기
				$row_payco = cs_pg_channel_payco($nm_member, $payco_order);	 
				$mpi_won = intval($row_payco['payco_amt']);
				$mpi_cash_point = intval($row_payco['payco_cash_point'] + $row_payco['payco_event_cash_point']);
				$mpi_point = intval($row_payco['payco_point'] + $row_payco['payco_event_point']);
				$mpi_from = "payco(".$row_payco['payco_payment_method_name']." )[".$row_payco['payco_product']."]";

				// 회원 포인트 부여
				$boolean_mpi = cs_set_member_point_income($nm_member, $mpi_cash_point, $mpi_point, $mpi_won, $mpi_from, $payco_order);
				$ErrBoolean = !$boolean_mpi;
				if($boolean_mpi == false){ 
					//실패 로그 남기기						
					// Write_Log("recharge_nonbank.php - 3 \n function cs_set_member_point_income error".$_SERVER['PHP_SELF']);
					cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "", "cs_set_member_point_income", "", 5, "회원 포인트 부여 실패"); // state-5
				}

				// 회원 캐쉬
				$boolean_cash = mb_set_cash($nm_member, $row_payco, 'payco');
				$ErrBoolean = !$boolean_cash;
				if($boolean_cash == false){ 
					//실패 로그 남기기 
					// Write_Log("recharge_nonbank.php - 4 \n function mb_set_cash error".$_SERVER['PHP_SELF']);
					cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "", "mb_set_cash", "", 6, "회원 캐쉬 실패"); // state-6
				}

				// 회원 포인트 통계
				$boolean_sr = cs_set_sales_recharge($nm_member, $row_payco, 'payco');
				$ErrBoolean = !$boolean_sr;
				if($boolean_sr == false){ 
					//실패 로그 남기기 
					// Write_Log("recharge_nonbank.php - 5 \n function cs_set_sales_recharge error".$_SERVER['PHP_SELF']);
					cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "", "cs_set_sales_recharge", "", 7, "회원 포인트 통계 실패"); // state-7
				}
			}else{
				// 실패 로그 남기기
				// Write_Log("recharge_nonbank.php - 6 \n sql: $sql_update_pg_channel_payco");
				cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "sql_update_pg_channel_payco", "", $sql_update_pg_channel_payco, 8, "우선 상태 업데이트 실패"); // state-8
			}
		}else{
			// Write_Log("recharge_nonbank.php - 7 \n amt error: ".$row_pg_channel_payco['payco_amt']." == ".$payco_total_payment_amt);
			cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "", "", $row_pg_channel_payco['payco_amt']." == ".$payco_total_payment_amt, 9, "가격이 동일한지 체크 실패"); // state-9
		}
	}
	//-----------------------------------------------------------------------------
	// 결과값을 생성
	//-----------------------------------------------------------------------------
	if( $ErrBoolean == true ){
		$resultValue = "ERROR";											// 오류가 있으면 ERROR를 설정
	} else {
		$resultValue = "OK";											// 오류가 없으면 OK 설정

		/* 181001 */
		// 상태 변환 -> 구매확정
		try {
			//--------------------------------------------------------------------------
			// 주문 상태를 변경하기 위한 값을 설정합니다.
			//--------------------------------------------------------------------------		
			$sellerOrderProductReferenceKey = $payco_seller_order_product_reference_key; 				// 가맹점 상품 연동 키
			$orderProductStatus				= 'PURCHASE_DECISION'; 							            // 변경할 상태 값 -> 구매확정
			$orderNo						= $payco_order_no; 									        // 주문번호

			//---------------------------------------------------------------------------
			// (로그) 호출 시점과 호출값을 파일에 기록합니다.
			//---------------------------------------------------------------------------
			Write_Log("payco_upstatus.php is Called - sellerOrderProductReferenceKey : $sellerOrderProductReferenceKey, orderProductStatus : $orderProductStatus, orderNo:$orderNo");
			
			cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "payco_upstatus.php is Called", "", $sellerOrderProductReferenceKey."orderProductStatus : ".$orderProductStatus.", orderNo:".$orderNo, 70, "payco_without_bankbook.php is Called - response"); // state-1

			//----------------------------------------------------------------------------
			// 설정한 주문정보 변수들로 Json String 을 작성합니다.
			//----------------------------------------------------------------------------
			$modifyValue = array();
			$modifyValue["sellerKey"]						= $sellerKey;
			$modifyValue["sellerOrderProductReferenceKey"]	= $sellerOrderProductReferenceKey;
			$modifyValue["orderProductStatus"]				= $orderProductStatus;
			$modifyValue["orderNo"]							= $orderNo;

			//----------------------------------------------------------------------------
			// 주문 상태변경 함수 호출 ( JSON 데이터를 String 형태로 전달 
			//----------------------------------------------------------------------------
			$Modify_Result = payco_upstatus(stripslashes(json_encode($modifyValue)));

			$Modify_Result_Data = json_decode($Modify_Result, true);
			$payco_order_product_status_code_modify			= $Modify_Result_Data["orderProducts"][0]["orderProductStatusCode"];
			$payco_order_product_status_name_modify			= $Modify_Result_Data["orderProducts"][0]["orderProductStatusName"];
			
			cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "payco_upstatus.php is Called", "", "payco_upstatus : ".$payco_order_product_status_code_modify.", ".$payco_order_product_status_name_modify, 71, "payco_without_bankbook.php is Called - ".implode(", ",$modifyValue)); // state-1

			// 상태 업데이트
			$sql_update_pg_channel_payco_modify = " UPDATE pg_channel_payco SET 
											        payco_message = concat(payco_message, '+', '".$payco_order_product_status_code_modify."', '//+//', '".$payco_order_product_status_name_modify."')
													WHERE 1 
													AND payco_payment_method_code = '02'
													AND payco_order_product_status_code = 'OPSPWAI' 
													AND payco_payment_completion_yn = 'N' 
													AND payco_seller_order_reference_key = '".$sellerOrderReferenceKey."'
													AND payco_reserve_order_no = '".$reserveOrderNo."'
													AND payco_approval_order = '".$orderNo."'
													AND payco_member_name = '".$memberName."'
													AND payco_bank_code = '".$payco_bank_code."'
													AND payco_account_no = '".$payco_account_no."'
											      ";
			sql_query($sql_update_pg_channel_payco_modify);

		} catch ( Exception $e ) {
			//-----------------------------------------------------------------------------
			// 작업 결과를 담을 JSON OBJECT를 선언합니다.
			//-----------------------------------------------------------------------------
			$resultValue = array();
			$resultValue["code"]		= $e->getCode();
			$resultValue["message"]		= $e->getMessage();
			Write_Log("payco_upstatus.php Logical Error : Code - ".$e->getCode().", Description - ".$e->getMessage());			
			
			cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "payco_upstatus.php Logical Error", "", "Logical Error : Code - ".$e->getCode().", Description - ".$e->getMessage(), 71, "payco_without_bankbook.php is Called - response"); // state-1

			$Result = json_encode($resultValue);
		}

		// 상태 변환 end
	}

	//-----------------------------------------------------------------------------
	// 오류일 경우 상세내역을 기록
	//-----------------------------------------------------------------------------
	if( $resultValue == "ERROR" ){
		Write_Log("payco_without_bankbook.php - 8 has item error : Couldn't find order.");		// 오류 상세 내역을 이곳에 표시합니다. ( DB 및 주문서 찾기등 오류)
		cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "", "", "", 10, "결과값 오류 있음"); // state-10
	}

} catch ( Exception $e ){
	$resultValue = "ERROR";
	Write_Log("payco_without_bankbook.php - 9  has logical error : Code - ".$e->getCode().", Description - ".$e->getMessage());
	cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "", "", "Code - ".$e->getCode().", Description - ".$e->getMessage(), 11, "has logical error"); // state-11
}

//---------------------------------------------------------------------------------
// 결과를 PAYCO 쪽에 리턴
//---------------------------------------------------------------------------------
Write_Log("payco_without_bankbook.php - 10  send Result : $resultValue");		// 리턴 내역을 기록
cs_x_error_log("x_pg_channel_payco_nonbank", "payco_nonbank", NM_PROC_PATH, "readValue", "", $readValue, 0, "payco_without_bankbook.php send Result"); // state-0
	
echo $resultValue;

?>