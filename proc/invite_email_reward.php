<?
include_once '_common.php'; // 공통

if($_member) {
	$mb = mb_get_no($_member); // 회원 정보 가져오기
	
	if($mb['mb_invite'] == '1') {
		// 지급 여부 조회
		$reward_sql = "SELECT * FROM invite_join WHERE ij_be_member = '".$mb['mb_no']."' AND ij_be_member_idx = '".$mb['mb_idx']."'";
		$reward_row = sql_fetch($reward_sql);

		if($reward_row['ij_invite_reward'] == '1') {
			cs_alert("보상이 이미 지급되었습니다.", NM_URL);
			die;
		} else {
			if(pf_invite_reward($mb)) {
				cs_alert("친구초대 보상이 지급되었습니다!", NM_URL."/myinfo.php");
				die;
			} else {
				cs_alert("보상 지급에 오류가 발생했습니다. 관리자에게 문의하세요!", NM_URL);
				die;
			} // end else
		}  // end else
	} else {
		cs_alert("친구초대 이벤트에 해당되는 회원이 아닙니다.", NM_URL);
		die;
	} // end else
} else {
	cs_alert("회원 정보가 없습니다!", NM_URL);
	die;
} // end else

?>