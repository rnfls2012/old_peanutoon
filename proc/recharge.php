<? include_once '_common.php'; // 공통
define('_RECHARGE_', true); // 결제페이지 개별로 접근 금지하기 위한 상수

// cs_login_check(); // 로그인 체크
cs_login_redirect("/recharge.php");

/*======================================
 * 최초 1회 동의 쿠키값 처리 - 180309 (지완)
 * 쿠키값 최종 결제완료페이지 까지 확인불가(mobile일 경우)
 =======================================*/

del_cookie('mb_agree');

$row_mb = mb_get($nm_member['mb_id']);

if ( $row_mb['mb_pay_agree'] === 'n' && !get_cookie('mb_agree') ) {
    $mb_agree = eng_check(tag_filter($_REQUEST['mb_agree']));
    $expire_time = 10 * 60; // 10min * 60sec
    set_cookie('mb_agree', $mb_agree, $expire_time); // 쿠키 10분 간 유지
}

/*======================================*/

$payway = array();

$actionresult = eng_check(tag_filter($_REQUEST['ActionResult']));
$tran_cd_para = num_check(tag_filter($_REQUEST['tran_cd']));

// PC사 처리
$get_payway = eng_check(tag_filter($_REQUEST['payway']));
$get_param_opt_1 = eng_check(tag_filter($_REQUEST['param_opt_1']));
if($get_param_opt_1 == "kcp"){
	$get_payway = $get_param_opt_1;
}

if($actionresult != "" || $tran_cd_para == "00100000"){ // kcp 모바일 전용
	// $payway[0] = $get_payway;
	$payway[0] = 'kcp';
}else{
	$crp_no = num_check(tag_filter($_REQUEST['crp_no'])); // 캐쉬 충전
	$payway_no = num_check(tag_filter($_REQUEST['payway_no'])); // 결제방법
	$coupondc = num_check(tag_filter($_REQUEST['coupondc'])); // 할인권
	$get_param_opt_2 = num_check(tag_filter($_REQUEST['param_opt_2']));
	if($get_param_opt_1 == "kcp" && $get_param_opt_2 != ""){
		$payway_no = $get_param_opt_2;
	}

	if($crp_no == "" || $payway_no == ""){
		$goto_url = cs_goto_referer();
		// cs_alert('캐쉬충전 또는 결제방법을 클릭 또는 확인해주시기 바랍니다.', $goto_url);
		// die;
	}

	if($crp_no != "" && $payway_no != ""){
		$crp_arr = $crp_arr_base = $crp_event_arr = array();
		$crp_base_arr = cs_recharge_list('n', $crp_no, '', $coupondc);
		$crp_event_arr = cs_recharge_list('y', $crp_no, '', $coupondc);
		$crp_arr =  array_merge($crp_base_arr, $crp_event_arr);

		$crp_total = count($crp_arr);
		if($crp_total != 1){
			$goto_url = cs_goto_referer();
			// cs_alert('캐쉬충전이 '.$crp_total.'개 입니다. 잘못된 인수입니다.', $goto_url);
			// die;
		}

		$recharge = $crp_arr[0];
		$payway = $nm_config['cf_payway'][$payway_no];
	}
	//}else{
	/* 181001 */
	if($get_param_opt_1 == "kcp"){
		// 한솔씨의 국민카드쪽 에러로 인해 추가 2017/06/23
		$payway[0] = $get_payway;
	}

	// crp_cash_count_event 가 빈칸이 아니면 무조건 이벤트

	/*
	print_r($recharge);
	echo "<br/>";
	print_r($payway);
	die;
	*/

	// 주문번호생성
	$order_no = get_uniqid();

	/* 181001 */
	// 주문번호세션처리
	// $_SESSION['recharge_order_no'] = $order_no;	
	// 181024 주문번호-캔슬용 삭제 : 세션처리했었는데 뒤로가기할때 문제가 발생되어 DB 저장하기로 함
	cs_del_pg_member_order($nm_member);
	cs_set_pg_member_order($nm_member, $order_no);

	// 상품명 => 상품캐쉬명_이벤트캐쉬명
	$crp_cash_name = cs_cash_name($recharge);
}

if(is_mobile()){
	$device = "mobile";
}else{
	$device = "pc";
}

// pc사 파일 경로
$pg_arr = cs_payway($payway[0],$device);
$pg_url = $pg_arr['url'];
$pg_path = $pg_arr['path'];

	$ret_url = NM_PROC_URL."/recharge.php";
	$pay_url = NM_PROC_URL."/recharge_proc.php";	
/*
if($payway[0] == "kcp"){
	$ret_url = NM_PROC_URL."/recharge.php";
	$pay_url = NM_PROC_URL."/recharge_proc.php";	
}else{
	$ret_url = NM_PROC_URL."/recharge.php?payway=".$payway[0]."&payway_no=".$payway_no;
	$pay_url = NM_PROC_URL."/recharge_proc.php?payway=".$payway[0]."&payway_no=".$payway_no;
}
*/

/* 181001 결제 테스트모드 */
$cf_pg_test_ctrl = eng_check(tag_filter($_REQUEST['cf_pg_test_ctrl']));
if($cf_pg_test_ctrl == 'y' && mb_class_permission('a') != true){
	$_SESSION['cf_pg_test_ctrl'] = '';
	alert('관리자 전용 기능 입니다. 권한을 확인해보세요!!!',NM_URL."/recharge.php");
	die;
}else{
	if($cf_pg_test_ctrl == 'y' && mb_class_permission('a') == true){
		$_SESSION['cf_pg_test_ctrl'] = 'y';
	}else{
		if($actionresult != "" || $tran_cd_para == "00100000"){ // kcp 모바일 전용
			// cf_pg_test_ctrl 유지해야함
		}else{
			$_SESSION['cf_pg_test_ctrl'] = '';
		}
	}
}

/* 181001 결제대행패스-테스트모드 */
$cf_pg_pass_ctrl = eng_check(tag_filter($_REQUEST['cf_pg_pass_ctrl']));
if($cf_pg_pass_ctrl == 'y' && mb_class_permission('a') != true){
	$_SESSION['cf_pg_pass_ctrl'] = '';
	alert('관리자 전용 기능 입니다. 권한을 확인해보세요!!!',NM_URL."/recharge.php");
	die;
}else{
	if($cf_pg_pass_ctrl == 'y' && mb_class_permission('a') == true){
		$_SESSION['cf_pg_pass_ctrl'] = 'y';
	}else{
		if($actionresult != "" || $tran_cd_para == "00100000"){ // kcp 모바일 전용
			// cf_pg_pass_ctrl 유지해야함
		}else{
			$_SESSION['cf_pg_pass_ctrl'] = '';
		}
	}
}

/* prossess */
$pg_path_error_msg = $pg_path_error_url = "";
if($pg_path == ""){
	$pg_path_msg = "결제정보가 제대로 넘어오지 않았습니다. 다시 한번 결제 부탁드리며 계속되는 에러시 1:1문의 부탁드립니다.";
	$pg_path_error_url = NM_URL."/recharge.php?v=".substr(NM_VERSION,1,1)."&mb_crp_no=".$crp_no."&mb_payway_no=".$payway_no."&mb_coupondc=".$coupondc;
	if($device == "pc"){
		pop_close($pg_path_msg, $pg_path_error_url);
	}else{
		alert($pg_path_msg, $pg_path_error_url);
	}
}else{ 
	/* 181001 결제대행패스-테스트모드 */
	include_once $pg_path.'/order.php'; // pc사 연결
}

?>