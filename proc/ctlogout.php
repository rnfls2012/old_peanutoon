<?
include_once '_common.php'; // 공통

if($_SESSION['app'] != ""){
	$ss_app = $_SESSION['app'];
	$ss_device = $_SESSION['device'];
	$ss_appver = $_SESSION['appver'];
	$ss_appupdate_later = $_SESSION['appupdate_later'];
}

session_unset(); // 모든 세션변수를 언레지스터 시켜줌
session_destroy(); // 세션해제함


// 자동로그인 해제 --------------------------------
set_cookie('ck_mb_id', '', 0);
set_cookie('ck_mb_auto', '', 0);
// 자동로그인 해제 end --------------------------------

@session_start();

if($ss_app != ""){
	$_SESSION['app'] = $ss_app;
	$_SESSION['device'] = $ss_device;
	$_SESSION['appver'] = $ss_appver;
	$_SESSION['appupdate_later'] = $ss_appupdate_later;
}

goto_url(NM_URL);

?>