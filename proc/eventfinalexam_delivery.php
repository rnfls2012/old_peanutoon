<?php
include_once '_common.php'; // 공통


$efm_name = tag_get_filter($_REQUEST['name']);
$efm_tel = tag_get_filter($_REQUEST['phone']);
$efm_address = tag_get_filter($_REQUEST['address']);
$efm_postal = tag_get_filter($_REQUEST['postal']);

$efm_win = false;
$is_member_false = array('올바른 방법으로 이용하시기 바랍니다.', NM_URL.'/eventfinalexam_delivery.php');
$efm_win_false = array('당첨자이시면 1:1문의하시기 바랍니다.[error code:efm_win-false]', NM_URL.'/eventfinalexam_delivery.php');
if($is_member == false){
	alert($is_member_false[0], $is_member_false[1]);
	die;
}else{
	// 당첨자 정보 가져오기
	$sql_efm = " select efm_member from event_finalexam_member where 1 AND efm_win=1 
	 AND efm_member='".$nm_member['mb_no']."' AND efm_member_idx='".$nm_member['mb_idx']."'
	";
	$row_efm = sql_fetch($sql_efm);
	if($row_efm['efm_member'] !=''){ $efm_win = true; }
}

// 당첨자 아니면...
if($efm_win == false){
	alert($efm_win_false[0], $efm_win_false[1]);
	die;
}else{
	$sql_update_efm = "update event_finalexam_member 
	 set efm_tel='".$efm_tel."', efm_address='".$efm_address."', efm_postal='".$efm_postal."', efm_name='".$efm_name."'
	 where 1 AND efm_win=1 
	 AND efm_member='".$nm_member['mb_no']."' AND efm_member_idx='".$nm_member['mb_idx']."' 
	 ";
	 if(sql_query($sql_update_efm)){ }else{
		alert('당첨자 정보가 저장되지 않았습니다.<br/>관리자에게 1:1문의바랍니다.(errorcode:delivery_update_error)', NM_URL.'/eventfinalexam_delivery.php');
		die;
	 }
}

if(is_app()==true && is_mobile()){
	alert('당첨자 정보가 저장되였습니다.', NM_URL.'/eventfinalexam_result.php');
}else{
	pop_close('당첨자 정보가 저장되였습니다.', NM_URL.'/eventfinalexam_result.php');
}
die;