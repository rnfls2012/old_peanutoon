<?
include_once '_common.php'; // 공통

$mb_no = $nm_member['mb_no'];
$mb_sns_type = $nm_member['mb_sns_type'];
$pw1 = trim($_POST['pw1']);
$pw2 = trim($_POST['pw2']);

/* SNS type */
if($mb_sns_type == "") {
	/* 패스워드 */
	if($pw1 != "" && $pw2 != "") {
		/* 패스워드 비교 */
		$cmp_query = "select if(mb_pass = password('$pw1'), true, false) cmp_pw from member where mb_no = '$mb_no'";
		$cmp_result = sql_query($cmp_query);
		$row = sql_fetch_array($cmp_result);

		if($row['cmp_pw']) {
			$leave_query = "update member set mb_state = 'n', mb_out_date = '".NM_TIME_YMDHIS."' where mb_no = '$mb_no'";
			$leave_result = sql_query($leave_query);
			if($leave_result) {
				session_unset(); // 모든 세션변수를 언레지스터 시켜줌
				session_destroy(); // 세션해제함

				// 자동로그인 해제 --------------------------------
				set_cookie('ck_mb_id', '', 0);
				set_cookie('ck_mb_auto', '', 0);
				// 자동로그인 해제 end --------------------------------

				// mkt - ICE COUNTER LEAVE leave
				rdt_acecounter_ss_leave('y');

				cs_alert("탈퇴 되었습니다!", NM_URL);
			} else {
				cs_alert("탈퇴가 되지 않았습니다. 관리자에게 문의하세요.", NM_URL."/myleave.php");
			} // end else
		} else {
			cs_alert("비밀번호를 확인해 주세요!", NM_URL."/myleave.php");
			die;
		} // end else
	} // end if
} else {
	$leave_query = "update member set mb_state='n', mb_out_date = '".NM_TIME_YMDHIS."' where mb_no = '$mb_no'";
	$leave_result = sql_query($leave_query);
	if($leave_result) {
		session_unset(); // 모든 세션변수를 언레지스터 시켜줌
		session_destroy(); // 세션해제함

		// 자동로그인 해제 --------------------------------
		set_cookie('ck_mb_id', '', 0);
		set_cookie('ck_mb_auto', '', 0);
		// 자동로그인 해제 end --------------------------------

		// mkt - ICE COUNTER LEAVE leave
		rdt_acecounter_ss_leave('y');

		cs_alert("탈퇴 되었습니다!", NM_URL);
	} else {
		cs_alert("탈퇴가 되지 않았습니다. 관리자에게 문의하세요.", NM_URL."/myleave.php");
	} // end else
}
?>