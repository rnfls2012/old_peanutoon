<?
include_once '_common.php'; // 공통
include_once(NM_PATH.'/config/kt_connect.php'); //kt 연동
include_once NM_ADM_PATH.'/_array_update.php'; // PARAMITER
include_once(NM_LIB_PATH.'/PHPMailer/class.phpmailer.php'); //메일
include_once(NM_LIB_PATH.'/PHPMailer/class.smtp.php');

/* 업로드한 파일명 */
$bp_file_tmp_name = $_FILES['bp_file']['tmp_name'];
$bp_file_name = $_FILES['bp_file']['name'];

/* 확장자 얻기 / .zip 파일이 아니면 튕겨내기 */
$bp_file_extension = substr($bp_file_name, strrpos($bp_file_name, "."), strlen($bp_file_name));

if($bp_file_extension != '.zip') {
    cs_alert("zip형식으로 압축하여 첨부해주시기 바랍니다.", $_SERVER['HTTP_REFERER']);
    die;
} // end if

//email check
if (email_check($_POST['bp_return']) === false) {
    cs_alert("이메일 형식을 확인해주시기 바랍니다.", $_SERVER['HTTP_REFERER']);
    die();
}

/* PARAMETER CHECK */
array_push($para_list, 'bp_member', 'bp_member_idx', 'bp_title','bp_text', 'bp_return' ,'bp_file', 'bp_date');

/* PARAMITER 숫자검사하면서 $_PARAMITER로 값 대입  */
para_checked();

$dbtable = "board_publish";

/* 파일 경로 */
/* 대분류 경로설정 - 폴더 체크 및 생성 */
$file_path = 'publish/'.get_int(NM_TIME_YM).'/';

/* 고정된 파일 명 */
$bp_file_name_define = $nm_member['mb_no'].'_'.time().$bp_file_extension;

/* 저장 경로 및 DB 파일명 및 파일경로 저장 */
$bp_file_src = $file_path.$bp_file_name_define;
$_bp_file = $bp_file_src;

/* 파일 업로드 */ 
$bp_file_result = false;
if ($bp_file_name != '') { 
	$bp_file_result = kt_storage_upload($bp_file_tmp_name, $file_path, $bp_file_name_define); 
} // end if

/* 임시파일이 존재하는 경우 삭제 */
if (file_exists($bp_file_tmp_name) && is_file($bp_file_tmp_name)) {
	unlink($bp_file_tmp_name);
} // end if

/* 업로드 성공시 파라미터 sql-insert문 생성 */
if ($bp_file_name != '' && $bp_file_result == true) {
	$sql_reg = para_sql_insert($dbtable);

	if(sql_query($sql_reg)){
		$db_result['msg'] = '연재작품 문의가 등록되었습니다.';
		
		/* 등록 완료시 email 보내기 */       
		$email_to = "heeya3693@nexcube.co.kr"; // 조은희 사원 메일
		$email_from = $nm_config['cf_admin_email'];
		$email_subject = $nm_member['mb_id']."님으로 부터 연재문의가 들어왔습니다.";
		$content = $nm_member['mb_id']."님으로 부터 연재문의가 들어왔습니다."."<br>"."관리자 페이지에서 확인하세요!";
		mailer($wr_name, $email_from, '조은희', $email_to, $email_subject, $content, 0);
	}else{
		$db_result['state'] = 1;
		$db_result['msg'] = '연재작품 등록에 실패하였습니다.\n 관리자에게 문의하세요!';
		$db_result['error'] = $sql_reg;
	} // end else
} else { 
	$db_result['msg'] = '연재작품 전송이 실패하였습니다.\n 관리자에게 문의하세요!';
} // end else

alert($db_result['msg'], $_SERVER['HTTP_REFERER']);

/*
if(is_nexcube()){
	echo $sql_reg;
}else{
	alert($db_result['msg'], $_SERVER['HTTP_REFERER']);
}
die;
*/
?>