<?
include_once '_common.php'; // 공통

$mb_no = base_filter($_POST['mb_no']); // 회원 번호
$pw1 = trim($_POST['pw1']);
$pw2 = trim($_POST['pw2']);
$pw3 = trim($_POST['pw3']);
$mb_post = base_filter($_POST['mem_mail']);
$mb_human_prevent = base_filter($_POST['m_radio']);
$mb_nick = $_POST['mb_nick'];
$mb_email = email_check($_POST['mb_email']);
$chn_arr = array(); // 변경될 변수 저장용
$chn_set = array(); // 쿼리용
// $var_out = 0; // 회원 탈퇴용 변수

/* 메일 동의 */
if($mb_post) {
	array_push($chn_arr, array('mb_post', $mb_post));
} // end if

/* 휴면방지 기간 */
if($mb_human_prevent && $mb_human_prevent != $nm_member['mb_human_prevent']) {
	array_push($chn_arr, array('mb_human_prevent', $mb_human_prevent));
} // end if

/* 패스워드 */
if($pw1 != "" && $pw2 != "" && $pw3 != "") {
	/* 패스워드 비교 */
	$cmp_query = "select if(mb_pass = password('$pw1'), true, false) cmp_pw from member where mb_no = '$mb_no'";
	$cmp_result = sql_query($cmp_query);
	$row = sql_fetch_array($cmp_result);

	if($row['cmp_pw']) {
		array_push($chn_arr, array('mb_pass', $pw2));
		array_push($chn_arr, array('customer_password', 'n'));
	} else {
		cs_alert("비밀번호를 확인해 주세요!", NM_URL."/myinfo.php");
		die;
	} // end else
} // end if

/* 닉네임 */
if($mb_nick && $mb_nick != $nm_member['mb_nick']) {
	array_push($chn_arr, array('mb_nick', $mb_nick));
} // end if

/* 이메일 */
if($mb_email && $mb_email != $nm_member['mb_email']) {
	/* 이메일 비교 
	$email_query = "select count(*) as email_cnt from member where mb_email = '".$mb_email."'";
	$email_count = sql_count($email_query, "email_cnt");

	if($email_count != 0) {
		cs_alert("이미 등록된 이메일 입니다!", NM_URL."/myinfo.php");
		die;
	} else {
	*/
	array_push($chn_arr, array('mb_email', $mb_email));
} else if(!$mb_email) {
	cs_alert("이메일 양식이 아닙니다!", NM_URL."/myinfo.php");
	die;
} 

/* 업데이트 */
if(count($chn_arr) > 0) {
	foreach($chn_arr as $key => $val) {
		if($val[0] == 'mb_pass') {
			array_push($chn_set, $val[0]." = password('".$val[1]."')");
		} else {
			array_push($chn_set, $val[0]." = '".$val[1]."'");
		} // end else
	} // end foreach

	$update_query = "update member set ".implode($chn_set, ", ")." where mb_no = '$mb_no'";
	$update_result = sql_query($update_query);

	if($update_result) {
		$msg = "변경되었습니다!";
	} else {
		$msg = "변경에 실패하였습니다. 관리자에게 문의해주세요!";
	} // end else
} else {
	$msg = "변경된 값이 없습니다.";
} // end else

/*
if($var_out != 0) {
	session_unset(); // 모든 세션변수를 언레지스터 시켜줌
	session_destroy(); // 세션해제함

	// 자동로그인 해제 --------------------------------
	set_cookie('ck_mb_id', '', 0);
	set_cookie('ck_mb_auto', '', 0);
	// 자동로그인 해제 end --------------------------------

	$out_query = "update member set mb_out_date = '".NM_TIME_YMDHIS."' where mb_no = '$mb_no'"; // 회원 번호 받아오는 건 나중에 처리
	$out_result = sql_query($out_query);

	if($out_query) {
		// 업데이트 성공
	} else {
		// 업데이트 실패
	} // end else
} // end if
*/
cs_alert($msg, NM_URL."/myinfo.php");
?>