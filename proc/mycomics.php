<?php

// 삭제(숨김) 기능 안되게 수정 - 문제: 사용자 전체가 삭제됨

include_once '_common.php'; // 공통

$comics_view = $_REQUEST['comics_check'];

//체크한 코믹 번호 배열 확인(url임의 값 가능성).
if(is_array($comics_view)) {
	$comics_view_list = implode($comics_view, ",");
	
	if(!num_check($comics_view_list)) {
		$_mode='';
		alert("옳바르지 않은 번호 입니다.");
		die;
	}
} else {
	$_mode='';
	alert("옳바르지 않은 접근입니다.");
	die;
}

$result = false;
$err_msg = "";

switch($_mode){
	case "comicslist":
		$comics_view_sql = "UPDATE member_buy_comics SET mbc_view = 'y' 
							WHERE mbc_member='".$nm_member['mb_no']."' AND mbc_comics in(".$comics_view_list.")";
		$result = sql_query($comics_view_sql); 
		
		$err_msg="구매목록 삭제가 되지 않았습니다!";
		break;
	
	case "recent":
		$comics_view_sql = "UPDATE member_buy_comics SET mbc_recent = 'y' 
							WHERE mbc_member='".$nm_member['mb_no']."' AND mbc_comics in(".$comics_view_list.")";
		$result = sql_query($comics_view_sql); 

		$err_msg="최근 본 작품 숨기기가 되지 않았습니다!";
		break;
	
	case "bookmark":
		$comics_view_sql = "DELETE FROM member_comics_mark 
							WHERE mcm_member='".$nm_member['mb_no']."' AND mcm_comics in(".$comics_view_list.")";
		$result = sql_query($comics_view_sql);
		
		$err_msg="즐겨찾기 삭제가 되지 않았습니다!";
		break;
}


if($result) {
	goto_url($_SERVER['HTTP_REFERER']);
} else {
	alert($err_msg." 관리자에게 문의해 주세요!", $_SERVER['HTTP_REFERER']);
}

/*
if($_mode == "comicslist") {
	$comics_view = $_REQUEST['comics_check'];
	$comics_view_list = implode($comics_view, ", ");

	$comics_view_sql = "UPDATE member_buy_comics SET mbc_view = 'y' WHERE mbc_comics in(".$comics_view_list.")";   
	$result = sql_query($comics_view_sql); 

	if($result) {
		goto_url($_SERVER['HTTP_REFERER']);
	} else {
		alert("삭제가 되지 않았습니다! 관리자에게 문의해 주세요!", $_SERVER['HTTP_REFERER']);
	} // end else
} else if($_mode == "recent") {
	$comics_view = $_REQUEST['comics_check'];
	$comics_view_list = implode($comics_view, ", ");

	$comics_view_sql = "UPDATE member_buy_comics SET mbc_recent = 'y' WHERE mbc_comics in(".$comics_view_list.")";
	$result = sql_query($comics_view_sql);

	if($result) {
		goto_url($_SERVER['HTTP_REFERER']);
	} else {
		alert("숨기기가 되지 않았습니다! 관리자에게 문의해 주세요!", $_SERVER['HTTP_REFERER']);
	} // end else
} else if($_mode == "bookmark") {
	$comics_view = $_REQUEST['comics_check'];
	$comics_view_list = implode($comics_view, ", ");

	$comics_view_sql = "DELETE FROM member_comics_mark WHERE mcm_comics in(".$comics_view_list.")";
	$result = sql_query($comics_view_sql);

	if($result) {
		goto_url($_SERVER['HTTP_REFERER']);
	} else {
		alert("삭제가 되지 않았습니다! 관리자에게 문의해 주세요!", $_SERVER['HTTP_REFERER']);
	} // end else
} // end else if
*/

?>