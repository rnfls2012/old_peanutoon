<?
include_once '_common.php'; // 공통

$er_no = randombox_state_get("f");

/* DB */
if($er_no == "" && $nm_member['mb_class'] != "a") {
	cs_alert("진행중인 이벤트가 없습니다!", NM_URL);
	die;
} else {
	/* 총 쿠폰 수량 */
	$event_total_sql = "SELECT * FROM event_randombox WHERE er_no='".$er_no."'";
	$event_total_row = sql_fetch($event_total_sql);
	$total_coupon = $event_total_row['er_first_count'];
	$total_sql = "SELECT COUNT(*) AS total_cnt FROM event_randombox_coupon WHERE erc_er_no='".$er_no."'";
	$pop_count = sql_count($total_sql, "total_cnt");
	$total_coupon -= $pop_count;
	
	if($total_coupon <= 0) {
		$total_coupon = 0;		
	} // end if

	/* 회원이 뽑았는지 확인 */
	$member_pop = 0;
	if($nm_member['mb_id'] != "") {
		$pop_sql = "SELECT COUNT(*) AS pop_cnt FROM event_randombox_coupon WHERE erc_er_no='".$er_no."' AND erc_member='".$nm_member['mb_no']."'";
		$member_pop = sql_count($pop_sql, "pop_cnt");
	} // end if

	/* 버튼 비활성화 */
	$nochk = "";
	if($member_pop > 0 || $total_coupon == 0 || $nm_member['mb_id'] == "") {
		$nochk = "nochk";
	} // end if

	$fullmoon_img = NM_IMG.$nm_config['nm_mode'].'/event/fullmoon'; // 이미지 경로
} // end else

/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */
/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/eventfullmoon.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>