<?php
include_once './_common.php'; // 공통

if(get_os(HTTP_USER_AGENT) == 'iPhone OS' || get_os(HTTP_USER_AGENT) == 'iPad MAC') {
	alert("iOS용 앱 추후 서비스 예정입니다.", HTTP_REFERER);
	die;
} // end if

if (preg_match('/'.NM_APP_MARKET.'/i', HTTP_USER_AGENT)) {	
	alert("구글 앱 버전에서는 완전판 APP이 다운로드 불가합니다. 모바일 웹을 이용해주세요!", HTTP_REFERER);
	die;
}

if (preg_match('/'.NM_APP_SETAPK.'/i', HTTP_USER_AGENT) && $_SESSION['appver'] == $nm_config['cf_appsetapk']) {	
	alert("현재 완전판 APP을 사용하고 계십니다.", NM_URL.'/index.php');
	die;
}

if(app_android_chk() == true){
	if (floatval(app_android()) < floatval(NM_APP_ANDROID_IS_VER)) {	
		alert("안드로이드 ".floatval(app_android())." 버전을 사용하고 계십니다. 피너툰 APP 최소버전은 안드로이드 ".sprintf("%.1f",NM_APP_ANDROID_IS_VER)." 입니다.", NM_URL.'/index.php');
		echo "1111111111";
		die;
	}
}

// update일 경우
$app_update = "";
if($_mode == "update"){ $app_update = $_mode; }

  // http://osunhitech.pofler.com/data/item/download.php?it_id=8410431393&it_sf=it_sf_m&filename=3667180685_C3ePdSns_2014_ED98B8ECB0BDEBB98CEB94A9.pdf&file_view_name=
  // $filedir = "./data/item/";
  // $filedir.= $_GET['it_id']."/";
  // $filedir.= $_GET['it_sf']."/";
  // $file = $filedir.$_GET['filename'];
  $file = NM_APP_PATH."/".NM_APP_SETAPK.".apk";
  //이름 바꿔서 저장
  $file_view_name = 'peanutoon.apk';
  
  if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/vnd.android.package-archive');
		// header('Content-Disposition: attachment; filename='.basename($file));
    if(is_app()) {
    	header('Content-Disposition:'.$file_view_name);
    } else {
    	header('Content-Disposition: attachment; filename='.$file_view_name);
    }
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
	st_stats_log_apk_update($nm_member, $app_update);

    exit;
  }
?>
<script type="text/javascript">
<!--
	sr_pop.self.close();
-->
</script>