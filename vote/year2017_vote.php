<?
include_once '_common.php'; // 공통

// 회원인지 확인
$is_vote_member = false;
if($nm_member['mb_id'] !='' && mb_is($nm_member['mb_id'])){
	// 회원일 경우 로그정보 남기기
	$is_vote_member = true;

	$_ev_no = num_check(tag_get_filter($_REQUEST['ev_no']));
	$_evc_no = num_check(tag_get_filter($_REQUEST['evc_no']));

	if(intval($_ev_no) > 0){
		vote_z_event_vote_member(NM_VOTE_YEAR, $comics, $ev_no);	// 로그
		vote_member_update(NM_VOTE_YEAR, $comics, $ev_no);			// 회원 투표 관리 업데이트
	}
}
goto_url(NM_VOTE_URL.'/year2017.php?menu='.$_evc_no."#vote_2017_tabs");
?>