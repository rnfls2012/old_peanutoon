<?
include_once '_common.php'; // 공통

/* DB */

// 회원인지 확인
$is_vote_member = false;
$row_vote_member = "";
if($nm_member['mb_id'] !='' && mb_is($nm_member['mb_id'])){
	// 회원일 경우 정보 insert
	$is_vote_member = true;
	vote_member_insert(NM_VOTE_YEAR);
	$row_vote_member = vote_member(NM_VOTE_YEAR);
}


// 데이터 리스트
$ev_evc_no_arrs = $ev_arrs = array();
$result_ev = sql_query("SELECT * FROM event_vote WHERE ev_year='2017' ORDER BY ev_evc_no, ev_no");
while ($row_ev = sql_fetch_array($result_ev)) {
	array_push($ev_arrs, $row_ev);
	array_push($ev_evc_no_arrs, $row_ev['ev_evc_no']);
}

// 중복제거
$evc_no_arrs = array_unique($ev_evc_no_arrs);

// 탭 리스트
$evc_arrs = array();
$result_evc = sql_query("SELECT * FROM event_vote_category WHERE evc_no IN (".join(", ",$evc_no_arrs).") order by evc_no");
while ($row_evc = sql_fetch_array($result_evc)) {
	$evc_arrs[$row_evc['evc_no']] = $row_evc['evc_name'];
}

/* 이미지 경로 */
$vote_2017_img = NM_IMG.$nm_config['nm_mode'].'/vote/2017';


/* 위에서 가져온 DB로 해도 되지만 소스가 지져분해져서 재정렬 
	view 탭정보 */
$tab_contents_arrs = $tabs_arrs = array();
$tab_no = 0;
foreach($evc_arrs as $evc_key => $evc_val){ 
	$tab_no++;	
	$tab_active = "";
	$tab_content_active = "hide";
	if(intval($_menu)> 0){
		if($tab_no == $_menu){ 
			$tab_active = "active"; 
			$tab_content_active = "active";
		}
	}else{
		if($tab_no == 1){ 
			$tab_active = "active";
			$tab_content_active = "active"; 
		}
	}

	$tmp_arrs = array();
	$tmp_arrs['evc_tab_no']				= $tab_no;
	$tmp_arrs['evc_active']				= $tab_active;
	$tmp_arrs['evc_content_active']		= $tab_content_active;
	$tmp_arrs['evc_name']				= $evc_val;
	$tmp_arrs['evc_no']					= $evc_key;
	
	// 투표 확인
	if(vote_member_ckeck($year, $evc_key) || $is_vote_member == false){
		$tmp_arrs['evc_tab_vote_img']		= $vote_2017_img.'/con04_tab_btn02.png'.vs_para();
		$tmp_arrs['evc_tab_vote_img_alt']	= "투표하기";
	}else{
	// 만약 투표했다면...
		$tmp_arrs['evc_tab_vote_img']		= $vote_2017_img.'/con04_tab_btn03.png'.vs_para();
		$tmp_arrs['evc_tab_vote_img_alt']	= "투표완료";
	}
	// $tmp_arrs['evc_tab_vote_img']		= $evc_key;

	array_push($tabs_arrs, $tmp_arrs);

	// 탭 컨텐츠
	$tab_contents_no = 0;
	foreach($ev_arrs as $ev_key => $ev_val){ 
		if($evc_key != $ev_val['ev_evc_no']){ continue; }
		$tab_contents_no++;	
		/* 작품정보 가져오기 */
		$db_ev_comics = get_comics($ev_val['ev_comics']);
		$db_cm_series = trim(str_replace("[웹툰판]", "", $db_ev_comics['cm_series']));
		$tmp_contents_arrs = array();
		$tmp_contents_arrs['ev_no']						= $ev_val['ev_no'];
		$tmp_contents_arrs['ev_evc_no']					= $ev_val['ev_evc_no'];
		$tmp_contents_arrs['ev_comics']					= $ev_val['ev_comics'];
		$tmp_contents_arrs['ev_cm_series']				= $db_cm_series;
		$tmp_contents_arrs['ev_cm_professional_info']	= $db_ev_comics['cm_professional_info'];
		$tmp_contents_arrs['ev_tab_contents_no']		= $tab_contents_no;
		$tmp_contents_arrs['ev_cover_pc']				= $ev_val['ev_cover_pc'];

		// 비회원일 경우
		if($is_vote_member == false){
			$tmp_contents_arrs['tab_view_link']				= 'link(\''.get_comics_url($ev_val['ev_comics']).'\');';
			$tmp_contents_arrs['tab_vote_link']				= 'alertBox(\'회원만 투표 하실수 있습니다.\', goto_url, {url:\''.NM_URL.'/ctlogin.php'.'\'})';
		}else{
		// 회원일 경우
			$tmp_contents_arrs['tab_view_link']				= 'link(\''.NM_VOTE_URL.'/year2017_view.php?comics='.$ev_val['ev_comics'].'&ev_no='.$ev_val['ev_no'].'\');';
			if($tmp_arrs['evc_tab_vote_img_alt'] == "투표완료"){
				$tmp_contents_arrs['tab_vote_link']				= 'alertBox(\''.$tmp_arrs['evc_name'].'투표는 완료되었습니다.<br/>내일 다시 참여해주시기 바랍니다.\')';
			}else{
				$tmp_contents_arrs['tab_vote_link']				= 'link(\''.NM_VOTE_URL.'/year2017_vote.php?comics='.$ev_val['ev_comics'].'&ev_no='.$ev_val['ev_no'].'&evc_no='.$ev_val['ev_evc_no'].'\');';
			}
		}
		array_push($tab_contents_arrs, $tmp_contents_arrs);
	}
}

/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once(NM_VOTE_PATH.'/'.$nm_config['nm_mode']."/year2017.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>