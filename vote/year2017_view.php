<?
include_once '_common.php'; // 공통

// 회원인지 확인
$is_vote_member = false;
if($nm_member['mb_id'] !='' && mb_is($nm_member['mb_id'])){
	// 회원일 경우 로그정보 남기기
	$is_vote_member = true;

	$_ev_no = num_check(tag_get_filter($_REQUEST['ev_no']));

	if(intval($_ev_no) > 0){
		vote_z_event_vote_view(NM_VOTE_YEAR, $comics, $ev_no);	// 로그
		vote_comics_view_update(NM_VOTE_YEAR, $comics, $ev_no);// 회원 투표 보러가기 관리 업데이트
	}
}

goto_url(get_comics_url($_comics));
?>