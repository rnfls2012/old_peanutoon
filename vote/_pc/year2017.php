<? include_once '_common.php'; // 공통
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_VOTE_PC_URL;?>/css/year2017.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_VOTE_PC_URL;?>/js/year2017.js<?=vs_para();?>"></script>
		<style type="text/css">
			.year2017 .evtcontainer { background:url(<?=$vote_2017_img?>/header_bg02.png<?=vs_para()?>) repeat-x left top; }
			.year2017 .evt_header {  background:url(<?=$vote_2017_img?>/header_bg.png<?=vs_para()?>) no-repeat center top; background-size: contain; background-color: #fff; }
			.year2017 .evt_con04 { background-color: #fff; background: url(<?=$vote_2017_img?>/con04_top.png<?=vs_para()?>) no-repeat center top;}

			.year2017 .evt_con04 ul li a.tab01 { background: url(<?=$vote_2017_img?>/con04_tabmenu01.png<?=vs_para()?>) no-repeat left bottom; }
			.year2017 .evt_con04 ul li a.tab01:hover { background: url(<?=$vote_2017_img?>/con04_tabmenu01.png<?=vs_para()?>) no-repeat left top; }
			.year2017 .evt_con04 ul li a.tab02 { background: url(<?=$vote_2017_img?>/con04_tabmenu02.png<?=vs_para()?>) no-repeat left bottom; }
			.year2017 .evt_con04 ul li a.tab02:hover { background: url(<?=$vote_2017_img?>/con04_tabmenu02.png<?=vs_para()?>) no-repeat left top; }
			.year2017 .evt_con04 ul li a.tab03 { background: url(<?=$vote_2017_img?>/con04_tabmenu03.png<?=vs_para()?>) no-repeat left bottom; }
			.year2017 .evt_con04 ul li a.tab03:hover { background: url(<?=$vote_2017_img?>/con04_tabmenu03.png<?=vs_para()?>) no-repeat left top; }
			.year2017 .evt_con04 ul li a.tab04 { background: url(<?=$vote_2017_img?>/con04_tabmenu04.png<?=vs_para()?>) no-repeat left bottom; }
			.year2017 .evt_con04 ul li a.tab04:hover { background: url(<?=$vote_2017_img?>/con04_tabmenu04.png<?=vs_para()?>) no-repeat left top; }
			.year2017 .evt_con04 ul li a.active { background-position: left top; }
		</style>


		<div class="year2017">
			<div class="evtcontainer">
				<div class="evt_header">
					<canvas id="canvas"></canvas>
					<div class="evt_header_dummy" id="headerdummy"><img src="<?=$vote_2017_img?>/header_bg.png<?=vs_para()?>" alt="최애 Pick 대작전" /></div>
					<div class="evt_header_tit"><img src="<?=$vote_2017_img?>/header_tit.png<?=vs_para()?>" alt="최애 Pick 대작전" /></div>
				</div>

				<div class="evt_con01"><img src="<?=$vote_2017_img?>/con01.png<?=vs_para()?>" alt="최애 Pick 대작전" /></div>

				<div class="evt_con02"><img src="<?=$vote_2017_img?>/con02.png<?=vs_para()?>" alt="최애 Pick 대작전" /></div>

				<div class="evt_con03">
					<img src="<?=$vote_2017_img?>/con03_top.png<?=vs_para()?>" alt="최애 Pick 대작전" />
					<div class="evt_con03_img"><img src="<?=$vote_2017_img?>/con03.png<?=vs_para()?>" alt="최애 Pick 대작전" /></div>
				</div>

				<div class="evt_con04" id="vote_2017">
					<div class="evt_con04_tabs hidden" id="vote_2017_tabs">&nbsp;</div>
					<!-- tabs -->
					<ul class="tabs">
					<? foreach($tabs_arrs as $tabs_key => $tabs_val){ ?>
						<li><a class="tab0<?=$tabs_val['evc_tab_no'];?> <?=$tabs_val['evc_active'];?>"><?=$tabs_val['evc_name'];?></a></li>
					<? } /* foreach($evc_arrs as $evc_key => $evc_val) end */?>
					</ul>
					<!-- /tabs -->

					<!-- tab_contents -->
					<div class="tab_contents">
						<? foreach($tabs_arrs as $tabs_key => $tabs_val){ ?>
						<div class="tab0<?=$tabs_val['evc_tab_no'];?>_contents <?=$tabs_val['evc_content_active'];?>">
							<? foreach($tab_contents_arrs as $tab_contents_key => $tab_contents_val){ 
								if($tab_contents_val['ev_evc_no'] != $tabs_val['evc_no']){ continue; }							
							?>
							<div class="tab_figure tab_q0<?=$tab_contents_val['ev_tab_contents_no'];?>">
								<img src="<?=NM_IMG.$tab_contents_val['ev_cover_pc'].vs_para();?>" alt="<?=$tab_contents_val['ev_cm_series']?>" />
								<span class="title"><?=$tab_contents_val['ev_cm_series']?></span>
								<span class="author">- <?=$tab_contents_val['ev_cm_professional_info']?> -</span>
								<a onclick="<?=$tab_contents_val['tab_view_link'];?>" class="tab_see"><img src="<?=$vote_2017_img?>/con04_tab_btn01.png<?=vs_para()?>" alt="보러가기"></a>
								<a onclick="<?=$tab_contents_val['tab_vote_link'];?>" class="tab_vote"><img src="<?=$tabs_val['evc_tab_vote_img'];?>" alt="<?=$tabs_val['evc_tab_vote_img_alt'];?>"></a>
							</div>
							<? } /* foreach($tab_contents_arrs as $tab_contents_key => $tab_contents_val) end */ ?>
							</div> <!-- /tab0<?=$tabs_val['evc_tab_no'];?>_contents -->
						<? } /* foreach($tabs_arrs as $tabs_key => $tabs_val) end */ ?>
					</div> <!-- /tab_contents -->
				</div> <!-- /evt_con04 -->

				<div class="evt_con05">
					<img src="<?=$vote_2017_img?>/con05_top.png<?=vs_para()?>" alt="라미's PICK" />
					<div class="link_wrap">
						<a href="<?=get_comics_url(1365)?>"><img src="<?=$vote_2017_img?>/con05_link01.png<?=vs_para()?>" alt="머리 괜찮냐?!" /></a>
						<a href="<?=get_comics_url(2376)?>"><img src="<?=$vote_2017_img?>/con05_link02.png<?=vs_para()?>" alt="너의 시선 속에서" /></a>
						<a href="<?=get_comics_url(2290)?>"><img src="<?=$vote_2017_img?>/con05_link03.png<?=vs_para()?>" alt="변태 교수의 관능 플레이" /></a>
					</div>
				</div>

				<div class="evt_con06">
					<div class="evt_con06_warn"><img src="<?=$vote_2017_img?>/con06.png<?=vs_para()?>" alt="라미's PICK" /></div>
				</div>
			</div>
		</div><!-- /year2017 -->