window.onload = function(){	
	/* tabs tab_contents start */
	$('#vote_2017 .tabs li a').click(function(event){
		// tabs
		$('#vote_2017 .tabs li a').removeClass('active');
		$(this).addClass('active');
		
		// tab_contents
		var index = Number($('#vote_2017 .tabs li a').index(this))+1;		
		$('#vote_2017 .tab_contents > div').removeClass('active').addClass('hide');
		$('#vote_2017 .tab_contents .tab0'+index+'_contents').removeClass('hide').addClass('active');
	});
	/* tabs tab_contents end */
}
