<?
include_once '_common.php'; // 공통

$_menu = num_eng_check(tag_filter($_REQUEST['menu']));
$_limit = num_check(tag_filter($_REQUEST['limit']));
$_total = num_check(tag_filter($_REQUEST['total']));
$_bool = num_eng_check(tag_filter($_REQUEST['bool']));

if($_menu == '' || $_limit == '' || $_bool == ''){
	echo "필수 파라미터와 유효값을 확인하시기 바랍니다.";
	die;
}

if($_bool != "false"){
	echo "코믹스 다 출력되였습니다.";
	die;
}

$result['state'] = 0;
$result['msg'] = "";
$result['boolean'] = $_bool;

// 전체 월1 화2 수3 목4 금5 토6 일7
$sql_cm_public_cycle_in = "";

if(!($_menu == 'all' || $_menu == '')){
	$cmserial_cycle_in_arr = get_comics_cycle_fix($_menu);
	$sql_cm_public_cycle_in = "AND c.cm_public_cycle IN(".$cmserial_cycle_in_arr.") ";
}

// 오늘 요일 표기 하기 위한...
$cm_public_cycle_in = get_comics_cycle_fix(week_type(7));

$cmserial_arr = array();
$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');

// cm_reg_date_new 아직 사용 안하지만 나중에 사용할수 있음
$sql_cmserial = " SELECT c.*, c_prof.cp_name as prof_name, 
					if(c.cm_public_cycle IN(".$cm_public_cycle_in.")=1,
						if(left(c.cm_episode_date,10)>='".NM_TIME_M1."',1,0),0
					)as cm_public_cycle_ck, 
					if(left(c.cm_reg_date,10)>='".NM_TIME_M1."',c.cm_reg_date,0)as cm_reg_date_new, 
					if(left(c.cm_episode_date,10)>='".NM_TIME_M1."',1,0)as cm_episode_date_up 
				FROM comics c 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 $sql_cm_adult $sql_cm_public_cycle_in 
				and c.cm_service = 'y' and c.cm_end='n' 
				ORDER BY cm_episode_date_up DESC, cm_reg_date DESC, cm_episode_date DESC 
				LIMIT ".$_limit.", ".NM_SUB_LIST_LIMIT."; ";

// 리스트 가져오기
$cmserial_arr = cs_comics_content_list($sql_cmserial, substr($nm_config['nm_path'], -1));

header( "content-type: application/json; charset=utf-8" );
echo json_encode($cmserial_arr);
die;
?>