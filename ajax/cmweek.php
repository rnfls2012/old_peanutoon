<?
include_once '_common.php'; // 공통

$_menu = num_eng_check(tag_filter($_REQUEST['menu']));
$_limit = num_check(tag_filter($_REQUEST['limit']));
$_total = num_check(tag_filter($_REQUEST['total']));
$_bool = num_eng_check(tag_filter($_REQUEST['bool']));

if($_menu == '' || $_limit == '' || $_bool == ''){
	echo "필수 파라미터와 유효값을 확인하시기 바랍니다.";
	die;
}

if($_bool != "false"){
	echo "코믹스 다 출력되였습니다.";
	die;
}

$result['state'] = 0;
$result['msg'] = "";
$result['boolean'] = $_bool;

// 전체 월1 화2 수3 목4 금5 토6 일7
$sql_cm_public_cycle_in = "";

if(!($_menu == 'all' || $_menu == '')){
	$cmweek_cycle_in_arr = get_comics_cycle_fix($_menu);
	$sql_cm_public_cycle_in = "AND c.cm_public_cycle IN(".$cmweek_cycle_in_arr.") ";
}

// 오늘 요일 표기 하기 위한...
$cm_public_cycle_in = get_comics_cycle_fix(week_type(7));

$cmweek_arr = array();
$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');

// cm_reg_date_new 아직 사용 안하지만 나중에 사용할수 있음

// 특정 만화 순서 앞으로...18-04-25
for($i=1;$i<=7;$i++){ $week_comicno_arr[$i] = array(); } // 배열선언
$sql_cw_adult = sql_adult($mb_adult_permission , 'cw_adult');
$sql_cw_class = "";
if($sql_cw_adult !=''){ $sql_cw_class = " AND cw_class LIKE '%_t' "; }

$sql_cw = " select * from comics_week where 1 $sql_cw_adult $sql_cw_class order by cw_class, cw_ranking ";
$result_cmweek = sql_query($sql_cw);
while ($row_cmweek = sql_fetch_array($result_cmweek)) {
	$cw_class_key = intval(str_replace("_t", "", $row_cmweek['cw_class']));
	if($cw_class_key == 0){
		array_push($week_comicno_arr[7],$row_cmweek['cw_comics']);
	}else{
		array_push($week_comicno_arr[$cw_class_key],$row_cmweek['cw_comics']);
	}
}

if(!($_menu == 'all' || $_menu == '')){
	$week_comicno_order = $week_comicno_arr[$_menu];
}else{
	$week_comicno_order = $week_comicno_arr[week_type(7)];
}

$sql_week_comicno_field = "";
$sql_week_comicno_order = "";

$sql_week_comicno_where = ""; // 18-05-14 마오랑대표&부사장님지시사항

if(count($week_comicno_order) > 0){
	$week_comicno_order_arr = $week_comicno_order;

	$sql_week_comicno_field = " CASE ";
	foreach($week_comicno_order_arr as $week_comicno_order_key => $week_comicno_order_val){
		$sql_then = 99 - intval($week_comicno_order_key);
		$sql_week_comicno_field.= " WHEN c.cm_no = '".$week_comicno_order_val."' THEN ".$sql_then." ";
	}
	$sql_week_comicno_field.= "ELSE 0 ";
	$sql_week_comicno_field.= "END AS cm_no_special, ";

	$sql_week_comicno_order = "cm_no_special DESC, ";

	$sql_week_comicno_where = " OR c.cm_no IN (".implode(", ",$week_comicno_order).") "; // 18-05-14 마오랑대표&부사장님지시사항
}

$sql_cmweek = " SELECT c.*, c_prof.cp_name as prof_name, 
					".$sql_week_comicno_field."
					if(c.cm_public_cycle IN(".$cm_public_cycle_in.")=1,
						if(left(c.cm_episode_date,10)>='".NM_TIME_M1."',1,0),0
					)as cm_public_cycle_ck, 
					if(left(c.cm_reg_date,10)>='".NM_TIME_M1."',c.cm_reg_date,0)as cm_reg_date_new, 
					if(left(c.cm_episode_date,10)>='".NM_TIME_M1."',1,0)as cm_episode_date_up 
				FROM comics c 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 $sql_cm_adult $sql_cm_public_cycle_in and c.cm_service = 'y' 
				$sql_week_comicno_where 
				ORDER BY ".$sql_week_comicno_order." 
				cm_end ASC, cm_public_cycle_ck DESC, cm_episode_date_up DESC, cm_reg_date DESC, cm_episode_date DESC 
				LIMIT ".$_limit.", ".NM_SUB_LIST_LIMIT."; ";

// 리스트 가져오기
$cmweek_arr = cs_comics_content_list($sql_cmweek, substr($nm_config['nm_path'], -1));

header( "content-type: application/json; charset=utf-8" );
echo json_encode($cmweek_arr);
die;
?>