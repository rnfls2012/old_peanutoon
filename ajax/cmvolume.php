<?
include_once '_common.php'; // 공통

$_small = num_eng_check(tag_filter($_REQUEST['small']));
$_limit = num_check(tag_filter($_REQUEST['limit']));
$_total = num_check(tag_filter($_REQUEST['total']));
$_bool = num_eng_check(tag_filter($_REQUEST['bool']));

if($_limit == '' || $_bool == ''){
	echo "필수 파라미터와 유효값을 확인하시기 바랍니다.";
	die;
}

if($_bool != "false"){
	echo "코믹스 다 출력되였습니다.";
	die;
}

$result['state'] = 0;
$result['msg'] = "";
$result['boolean'] = $_bool;

/* 성인6, BL/GL5, TL2, 순정4, 드라마1, 코믹3, 소설7 */
$small_val = get_int($lnb_sub_arr[0]['cn_link']); // 위 _head.php에서 가져옴

if($_small != ''){
	$small_val = $_small;
} else {
	$_small = $small_val;
} // end else

$sql_small == '';
if($_small != ''){
	$sql_small = "AND c.cm_small = ".$small_val." ";
	// $sql_small = "AND (c.cm_small = ".$small_val.")";
}

$cmvolume_arr = array();
$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');
$sql_cmvolume = " SELECT c.*, c_prof.cp_name as prof_name  FROM comics c 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE cm_service = 'y' AND (c.cm_big = 1 OR c.cm_big = 3) $sql_cm_adult $sql_small ORDER BY cm_episode_date DESC, cm_reg_date DESC LIMIT ".$_limit.", ".NM_SUB_LIST_LIMIT."; ";

// 리스트 가져오기
$cmvolume_arr = cs_comics_content_list($sql_cmvolume, substr($nm_config['nm_path'], -1));

header( "content-type: application/json; charset=utf-8" );
echo json_encode($cmvolume_arr);
die;
?>