<?
include_once '_common.php'; // 공통

$_crs_class = num_eng_ext_check(tag_filter($_REQUEST['crs_class']));

if($_crs_class == ''){
	echo "필수 파라미터와 유효값을 확인하시기 바랍니다.";
	die;
}

$result['state'] = 0;
$result['msg'] = "";
$result['boolean'] = $_bool;



/* /mgenre.php 소스 복사 */

// 특정 만화 순서 앞으로...18-04-25
for($i=0;$i<=8;$i++){ $week_comicno_arr[$i] = array(); } // 배열선언
$sql_cg_adult = sql_adult($mb_adult_permission , 'cg_adult');

$cg_class = $_crs_class;

$_small = $cg_class;

// if($sql_cg_adult !=''){ $cg_class = $cg_class.'_t'; }
$sql_cg_class = " AND cg_class = '". $cg_class."' ";


$sql_cg = " select * from comics_genre where 1 $sql_cg_adult $sql_cg_class order by cg_class, cg_ranking ";
$result_cg = sql_query($sql_cg);
while ($row_cg = sql_fetch_array($result_cg)) {
	$cg_class_key = intval(str_replace("_t", "", $row_cg['cg_class']));
	array_push($week_comicno_arr[$cg_class_key], $row_cg['cg_comics']);
}
$week_comicno_order = $week_comicno_arr[$_small];

$sql_week_comicno_field = "";
$sql_week_comicno_order = "";

$sql_week_comicno_where = ""; // 18-05-14 마오랑대표&부사장님지시사항

if(count($week_comicno_order) > 0){
	$week_comicno_order_arr = $week_comicno_order;

	$sql_week_comicno_field = " CASE ";
	foreach($week_comicno_order_arr as $week_comicno_order_key => $week_comicno_order_val){
		$sql_then = 99 - intval($week_comicno_order_key);
		$sql_week_comicno_field.= " WHEN c.cm_no = '".$week_comicno_order_val."' THEN ".$sql_then." ";
	}
	$sql_week_comicno_field.= "ELSE 0 ";
	$sql_week_comicno_field.= "END AS cm_no_special, ";

	$sql_week_comicno_order = "cm_no_special DESC, ";

	$sql_week_comicno_where = " OR c.cm_no IN (".implode(", ",$week_comicno_order).") "; // 18-05-14 마오랑대표&부사장님지시사항
}

$sql_crs_class = " AND crs.crs_class = '".$cg_class."' ";

/* 
	cs_comics_content_list() 사용시
	SQL문에 이미지 사이즈 넣었을때( cm_cover, cm_cover_sub 사이즈 둘다 있어야 함 ) 
	cm_cover_thumb_wxh, cm_cover_sub_thumb_wxh
*/

$sql_genre = " 

SELECT CG.*, 
@rownum:=@rownum+1 AS rownum,  
if(@rownum < 4,'tn388x198','tn226x115') as cm_cover_thumb_wxh, 
if(@rownum < 4,'tn133x133','tn133x133') as cm_cover_sub_thumb_wxh
FROM(
	SELECT c.*, crs.crs_ranking, 
		".$sql_week_comicno_field."
		c_prof.cp_name as prof_name 
	FROM comics_ranking_sales_cmgenre_auto crs 
	left JOIN comics c ON crs.crs_comics = c.cm_no 				 
	left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 

	,(SELECT @rownum :=0) AS R

	WHERE 1 and c.cm_service = 'y' $sql_crs_class
	$sql_week_comicno_where 	
	GROUP BY c.cm_no 
	ORDER BY ".$sql_week_comicno_order." 
	crs.crs_ranking ASC  
) AS CG LIMIT 0, 16
";

// 리스트 가져오기
$cmgenre_arr = cs_comics_content_list($sql_genre, substr($nm_config['nm_path'], -1));

header( "content-type: application/json; charset=utf-8" );
echo json_encode($cmgenre_arr);
die;
?>