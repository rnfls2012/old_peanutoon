<?
include_once '_common.php'; // 공통

$_crs_class = num_eng_ext_check(tag_filter($_REQUEST['crs_class']));

if($_crs_class == ''){
	echo "필수 파라미터와 유효값을 확인하시기 바랍니다.";
	die;
}

$result['state'] = 0;
$result['msg'] = "";
$result['boolean'] = $_bool;

/* DB */
$cmweek_cycle_in_arr = get_comics_cycle_fix($_crs_class);
$sql_cm_public_cycle_in = "AND c.cm_public_cycle IN(".$cmweek_cycle_in_arr.") ";

// 18-05-19 마오랑대표&부사장님지시사항
$sql_cmweek_field_ck = " if(c.cm_public_cycle IN(".$cmweek_cycle_in_arr.")=1,1,0)as cmweek_field_ck, ";

// 오늘 요일 표기 하기 위한...
$cm_public_cycle_in = get_comics_cycle_fix($_crs_class);

$cmweek_arr = array();
$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');

// 특정 만화 순서 앞으로...18-04-25
for($i=1;$i<=7;$i++){ $week_comicno_arr[$i] = array(); } // 배열선언
$sql_cw_adult = sql_adult($mb_adult_permission , 'cw_adult');
$sql_cw_class = "";
if($sql_cw_adult !=''){ $sql_cw_class = " AND cw_class LIKE '%_t' "; }

$sql_cmweek = " select * from comics_week where 1 $sql_cw_adult $sql_cw_class order by cw_class, cw_ranking ";

$result_cmweek = sql_query($sql_cmweek);

while ($row_cmweek = sql_fetch_array($result_cmweek)) {
	$cw_class_key = intval(str_replace("_t", "", $row_cmweek['cw_class']));
	if($cw_class_key == 0){
		array_push($week_comicno_arr[7],$row_cmweek['cw_comics']);
	}else{
		array_push($week_comicno_arr[$cw_class_key],$row_cmweek['cw_comics']);
	}
}

$week_comicno_order = $week_comicno_arr[$_crs_class];

$sql_week_comicno_where = ""; // 18-05-14 마오랑대표&부사장님지시사항

if(count($week_comicno_order) > 0){
	$week_comicno_order_arr = $week_comicno_order;

	$sql_week_comicno_field = " CASE ";
	foreach($week_comicno_order_arr as $week_comicno_order_key => $week_comicno_order_val){
		$sql_then = 99 - intval($week_comicno_order_key);
		$sql_week_comicno_field.= " WHEN c.cm_no = '".$week_comicno_order_val."' THEN ".$sql_then." ";
	}
	$sql_week_comicno_field.= "ELSE 0 ";
	$sql_week_comicno_field.= "END AS cm_no_special, ";

	$sql_week_comicno_order = "cm_no_special DESC, ";

	$sql_week_comicno_where = " OR c.cm_no IN (".implode(", ",$week_comicno_order).") "; // 18-05-14 마오랑대표&부사장님지시사항
} // end if

// 18-05-14 부사장님&마오랑 추가사항
$sql_limit = " LIMIT 0, 16 ";

$sql_cmweek = " SELECT c.*, c_prof.cp_name as prof_name, 
					".$sql_week_comicno_field."
					".$sql_cmweek_field_ck."
					if(c.cm_public_cycle IN(".$cm_public_cycle_in.")=1,
						if(left(c.cm_episode_date,10)>='".NM_TIME_M1."',1,0),0
					)as cm_public_cycle_ck, 
					if(left(c.cm_reg_date,10)>='".NM_TIME_M1."',c.cm_reg_date,0)as cm_reg_date_new, 
					if(left(c.cm_episode_date,10)>='".NM_TIME_M1."',1,0)as cm_episode_date_up 
				FROM comics c 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 $sql_cm_adult $sql_cm_public_cycle_in and c.cm_service = 'y' and c.cm_big = 2 
				$sql_week_comicno_where 
				ORDER BY ".$sql_week_comicno_order." 
				cm_end ASC, cm_public_cycle_ck DESC, cm_episode_date_up DESC, cm_reg_date DESC, cm_episode_date DESC 
				".$sql_limit." ";			

// 리스트 가져오기
$cmweek_arr = cs_comics_content_list($sql_cmweek, substr($nm_config['nm_path'], -1));

header( "content-type: application/json; charset=utf-8" );
echo json_encode($cmweek_arr);
die;
?>