<?
include_once '_common.php'; // 공통

$_small = num_eng_check(tag_filter($_REQUEST['small']));
$_limit = num_check(tag_filter($_REQUEST['limit']));
$_total = num_check(tag_filter($_REQUEST['total']));
$_bool = num_eng_check(tag_filter($_REQUEST['bool']));

if($_limit == '' || $_bool == ''){
	echo "필수 파라미터와 유효값을 확인하시기 바랍니다.";
	die;
}

if($_bool != "false"){
	echo "코믹스 다 출력되였습니다.";
	die;
}

$result['state'] = 0;
$result['msg'] = "";
$result['boolean'] = $_bool;

$sql_small = '';
$crs_class = '0';
if($_small != ''){
	$sql_small = "AND c.cm_small = ".$_small." ";
	$crs_class = strval($_small);
}

$cmbest_arr = array();
$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');

if($mb_adult_permission != 'y'){ $crs_class.= '_t';  }

$sql_cmbest = " SELECT c.*, c_prof.cp_name as prof_name  FROM comics c 
				LEFT JOIN comics_ranking_small_auto crsa ON crsa.crs_comics = c.cm_no 
				LEFT JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 $sql_cm_adult $sql_small and c.cm_service = 'y' 
						AND crsa.crs_class='".$crs_class."' 				
				ORDER BY crsa.crs_ranking ASC 
				LIMIT ".$_limit.", ".NM_SUB_LIST_LIMIT."; ";

// 리스트 가져오기
$cmbest_arr = cs_comics_content_list($sql_cmbest, substr($nm_config['nm_path'], -1));

header( "content-type: application/json; charset=utf-8" );
echo json_encode($cmbest_arr);
die;
?>