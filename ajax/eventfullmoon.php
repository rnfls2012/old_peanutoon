<?
include_once '_common.php'; // 공통

/* 로그인 체크 */
cs_login_check();

/* 반환할 값 배열 */
$result_arr = array("result"=>false, "text"=>"");

/* 랜덤박스 진행정보 가져오기 */
$er_no_y = randombox_state_get("f");

/* 회원이 뽑은 수 가져오기 */
$pop_sql = "SELECT COUNT(*) AS pop_cnt FROM event_randombox_coupon WHERE erc_er_no='".$er_no_y."' AND erc_member='".$nm_member['mb_no']."'";
$member_pop = sql_count($pop_sql, "pop_cnt");

/* 이벤트 정보 가져오기 */
$fullmoon_event_sql = "SELECT * FROM event_randombox WHERE er_no='".$er_no_y."'"; // 이벤트 번호 제어 randombox_state_get() 사용 180213
$fullmoon_row = sql_fetch($fullmoon_event_sql);

/* 랜덤박스 이벤트가 진행중인지 확인 */
if($fullmoon_row['er_state'] == "n") {
	$result_arr['text'] = "종료된 이벤트입니다! 관리자에게 문의하세요.";
	echo json_encode($result_arr);
	die;
} // end if

/* 랜덤박스 쿠폰 설정이 돼 있는지 확인 */
if($fullmoon_row['er_discount_rate_list'] == "") {
	$result_arr['text'] = "등록된 할인권이 없습니다! 관리자에게 문의하세요.";
	echo json_encode($result_arr);
	die;
} // end if

/* 당일 뽑기 찬스가 남아 있는지 확인 */
if($member_pop > 0) {
	$result_arr['text'] = "오늘자 할인권을 이미 뽑으셨습니다!";
	echo json_encode($result_arr);
	die;
} // end if

/* 관리자일때는 데이터 저장X, 무조건 TRUE 반환, 나중에 지우기
if($nm_member['mb_class'] == 'a' || is_nexcube()) {
	$fullmoon_coupon = discount_rate($fullmoon_row);
	$result_arr = array_merge($result_arr, $fullmoon_coupon);
	$result_arr['er_available_date_end'] = str_replace("-", ". ", $result_arr['er_available_date_end']);
	$result_arr['result'] = true;

	echo json_encode($result_arr);
	die;
} // end if
*/

/* 한 번도 뽑지 않았을 때 시작 */
if($member_pop == 0) {
	$fullmoon_coupon = randombox_discount_rate($fullmoon_row);

	$result_arr = array_merge($result_arr, $fullmoon_coupon);
	$result_arr['er_available_date_end'] = str_replace("-", ". ", $result_arr['er_available_date_end']);
	$result_arr['result'] = true;

	$randombox_mb = array("erm_member" => $nm_member['mb_no'], "erm_member_ndx" => $nm_member['mb_ndx'], "erm_member_idx" => $nm_member['mb_idx']);

	// 쿠폰 저장
	if($coupon_no = randombox_member_coupon_get_first($randombox_mb, $result_arr)) {
		$result_arr['erc_no'] = $coupon_no;
		// 성공했을 시 로그 남기기
		if(randombox_pop_log($randombox_mb, $result_arr)) {
			// 성공했을 시 통계 남기기
			st_stats_randombox_pop($result_arr);
		} // end if
	} // end if

	echo json_encode($result_arr);
	die;
} // end if
?>
