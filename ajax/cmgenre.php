<?
include_once '_common.php'; // 공통

$_small = num_eng_check(tag_filter($_REQUEST['small']));
$_limit = num_check(tag_filter($_REQUEST['limit']));
$_total = num_check(tag_filter($_REQUEST['total']));
$_bool = num_eng_check(tag_filter($_REQUEST['bool']));

if($_small == '' || $_limit == '' || $_bool == ''){
	echo "필수 파라미터와 유효값을 확인하시기 바랍니다.";
	die;
}

if($_bool != "false"){
	echo "코믹스 다 출력되였습니다.";
	die;
}

$result['state'] = 0;
$result['msg'] = "";
$result['boolean'] = $_bool;

// 특정 만화 순서 앞으로...18-04-25
for($i=0;$i<=8;$i++){ $week_comicno_arr[$i] = array(); } // 배열선언
$sql_cg_adult = sql_adult($mb_adult_permission , 'cg_adult');

$cg_class = 0;

if(!($_small == 'all' || $_small == '')){
	$cg_class = intval($_small);
}

if($sql_cg_adult !=''){ $cg_class = $cg_class.'_t'; }
$sql_cg_class = " AND cg_class = '". $cg_class."' ";


$sql_cg = " select * from comics_genre where 1 $sql_cg_adult $sql_cg_class order by cg_class, cg_ranking ";
$result_cg = sql_query($sql_cg);
while ($row_cg = sql_fetch_array($result_cg)) {
	$cg_class_key = intval(str_replace("_t", "", $row_cg['cg_class']));
	array_push($week_comicno_arr[$cg_class_key], $row_cg['cg_comics']);
}
$week_comicno_order = $week_comicno_arr[$_small];

$sql_week_comicno_field = "";
$sql_week_comicno_order = "";

$sql_week_comicno_where = ""; // 18-05-14 마오랑대표&부사장님지시사항

if(count($week_comicno_order) > 0){
	$week_comicno_order_arr = $week_comicno_order;

	$sql_week_comicno_field = " CASE ";
	foreach($week_comicno_order_arr as $week_comicno_order_key => $week_comicno_order_val){
		$sql_then = 99 - intval($week_comicno_order_key);
		$sql_week_comicno_field.= " WHEN c.cm_no = '".$week_comicno_order_val."' THEN ".$sql_then." ";
	}
	$sql_week_comicno_field.= "ELSE 0 ";
	$sql_week_comicno_field.= "END AS cm_no_special, ";

	$sql_week_comicno_order = "cm_no_special DESC, ";

	$sql_week_comicno_where = " OR c.cm_no IN (".implode(", ",$week_comicno_order).") "; // 18-05-14 마오랑대표&부사장님지시사항
}

$sql_crs_class = " AND crs.crs_class = '".$cg_class."' ";

$sql_genre = " SELECT c.*, crs.crs_ranking, 
					".$sql_week_comicno_field."
					c_prof.cp_name as prof_name 
				FROM comics c 
				left JOIN comics_ranking_sales_cmgenre_auto crs ON crs.crs_comics = c.cm_no 				 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 and c.cm_service = 'y' $sql_crs_class
				$sql_week_comicno_where 	
				GROUP BY c.cm_no 
				ORDER BY ".$sql_week_comicno_order." 
				crs.crs_ranking ASC  
				LIMIT ".$_limit.", ".NM_SUB_LIST_LIMIT."; ";

// 리스트 가져오기
$cmgenre_arr = cs_comics_content_list($sql_genre, substr($nm_config['nm_path'], -1));

header( "content-type: application/json; charset=utf-8" );
echo json_encode($cmgenre_arr);
die;
?>