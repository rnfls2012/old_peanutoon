<?
include_once '_common.php'; // 공통

// $_menu = num_eng_check(tag_filter($_REQUEST['menu']));
$_limit = num_check(tag_filter($_REQUEST['limit']));
$_total = num_check(tag_filter($_REQUEST['total']));
$_bool = num_eng_check(tag_filter($_REQUEST['bool']));

if($_menu == '' || $_limit == '' || $_bool == ''){
	echo "필수 파라미터와 유효값을 확인하시기 바랍니다.";
	die;
}

if($_bool != "false"){
	echo "코믹스 다 출력되였습니다.";
	die;
}

$result['state'] = 0;
$result['msg'] = "";
$result['boolean'] = $_bool;

// 전체 월1 화2 수3 목4 금5 토6 일7
$sql_cm_public_cycle_in = "";

if(!($_menu == 'all' || $_menu == '')){
	$cmtop_cycle_in_arr = get_comics_cycle_fix($_menu);
	$sql_cm_public_cycle_in = "AND c.cm_public_cycle IN(".$cmtop_cycle_in_arr.") ";
}

// 오늘 요일 표기 하기 위한...
$cm_public_cycle_in = get_comics_cycle_fix(week_type(7));

$cmtop_arr = array();
$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');

// cm_reg_date_new 아직 사용 안하지만 나중에 사용할수 있음

// 특정 만화 순서 앞으로...18-04-25
$week_comicno_arr[0] = array(); // 배열선언
$sql_cf_adult = sql_adult($mb_adult_permission , 'cf_adult');
$sql_cf_class = "";
if($sql_cf_adult !=''){ $sql_cf_class = " AND cf_class LIKE '%_t' "; }

$sql_cn = " select * from comics_free where 1 $sql_cf_adult $sql_cf_class order by cf_class, cf_ranking ";
$result_cn = sql_query($sql_cn);
while ($row_cn = sql_fetch_array($result_cn)) {
	$cf_class_key = intval(str_replace("_t", "", $row_cn['cf_class']));
	array_push($week_comicno_arr[$cf_class_key], $row_cn['cf_comics']);
}

$week_comicno_order = $week_comicno_arr[0];

$sql_week_comicno_field = "";
$sql_week_comicno_order = "";

$sql_week_comicno_where = ""; // 18-05-14 마오랑대표&부사장님지시사항

if(count($week_comicno_order) > 0){
	$week_comicno_order_arr = $week_comicno_order;

	$sql_week_comicno_field = " CASE ";
	foreach($week_comicno_order_arr as $week_comicno_order_key => $week_comicno_order_val){
		$sql_then = 99 - intval($week_comicno_order_key);
		$sql_week_comicno_field.= " WHEN c.cm_no = '".$week_comicno_order_val."' THEN ".$sql_then." ";
	}
	$sql_week_comicno_field.= "ELSE 0 ";
	$sql_week_comicno_field.= "END AS cm_no_special, ";

	$sql_week_comicno_order = "cm_no_special DESC, ";

	$sql_week_comicno_where = " OR c.cm_no IN (".implode(", ",$week_comicno_order).") "; // 18-05-14 마오랑대표&부사장님지시사항
}

/* 클래스 */
$sql_cf_class_val = '0';
if($sql_cf_adult !=''){ $sql_cf_class_val = '0_t'; }
$sql_cf_class = " AND crs.crs_class = '".$sql_cf_class_val."' ";

$sql_cmfree = " SELECT c.*, crs.crs_ranking, 
					".$sql_week_comicno_field."
					c_prof.cp_name as prof_name 
				FROM comics c 
				left JOIN comics_ranking_sales_cmfree_auto crs ON crs.crs_comics = c.cm_no 				 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 $sql_cf_class 
				ORDER BY ".$sql_week_comicno_order." 
				crs.crs_ranking ASC  
				LIMIT ".$_limit.", ".NM_SUB_LIST_LIMIT."; ";
// 리스트 가져오기
$cmtop_arr = cs_comics_content_list($sql_cmfree, substr($nm_config['nm_path'], -1));

header( "content-type: application/json; charset=utf-8" );
echo json_encode($cmtop_arr);
die;
?>