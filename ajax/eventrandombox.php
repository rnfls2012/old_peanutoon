<?
include_once '_common.php'; // 공통

/* 로그인 체크 */
cs_login_check();

/* 반환할 값 배열 */
$result_arr = array("result"=>false, "text"=>"");

/* 회원 랜덤박스 진행정보 가져오기 */
$er_no_y = randombox_state_get();
$randombox_mb = randombox_member_get($nm_member, $er_no_y); // 이벤트 번호 제어 randombox_state_get() 사용 180213
$randombox_mb['erm_cash_point_total'] = ($randombox_mb['erm_cash_point_push'] + ($randombox_mb['erm_point_push'] / 10)) - $randombox_mb['erm_cash_point_pop']; // 결제 땅콩, 뽑기에 쓴 땅콩 계산한 값
$randombox_mb['erm_pop_chance'] = floor($randombox_mb['erm_cash_point_total'] / 10); // 뽑을 수 있는 횟수

/* 이벤트 정보 가져오기 */
$randombox_event_sql = "SELECT * FROM event_randombox WHERE er_no='".$er_no_y."'"; // 이벤트 번호 제어 randombox_state_get() 사용 180213
$randombox_row = sql_fetch($randombox_event_sql);

/* 랜덤박스 이벤트가 진행중인지 확인 */
if($randombox_row['er_state'] == "n") {
	$result_arr['text'] = "종료된 이벤트입니다! 관리자에게 문의하세요.";
	echo json_encode($result_arr);
	die;
} // end if

/* 랜덤박스 쿠폰 설정이 돼 있는지 확인 */
if($randombox_row['er_discount_rate_list'] == "") {
	$result_arr['text'] = "등록된 할인권이 없습니다! 관리자에게 문의하세요.";
	echo json_encode($result_arr);
	die;
} // end if

/* 관리자일때는 데이터 저장X, 무조건 TRUE 반환, 나중에 지우기
if($nm_member['mb_class'] == 'a' || is_nexcube()) {
	$randombox_coupon = discount_rate($randombox_row);
	$result_arr = array_merge($result_arr, $randombox_coupon);
	$result_arr['er_available_date_end'] = str_replace("-", ". ", $result_arr['er_available_date_end']);
	$result_arr['result'] = true;

	echo json_encode($result_arr);
	die;
} // end if
*/
/* 뽑을 수 있는 참여 땅콩이 됐는지 확인 */
if($randombox_mb['erm_pop_chance'] <= 0) {
	$result_arr['text'] = "참여 땅콩이 부족합니다!";
	echo json_encode($result_arr);
	die;
} // end if

/* 당일 뽑기 찬스가 남아 있는지 확인 */
if($randombox_mb['erm_pop_count'] <= 0) {
	$result_arr['text'] = "오늘 뽑기 횟수를 모두 사용하셨습니다!";
	echo json_encode($result_arr);
	die;
} // end if

/* 뽑을 수 있는 참여 땅콩이 됐는지 & 당일 뽑기 찬스가 남아 있을때 뽑기 시작 */
if($randombox_mb['erm_pop_chance'] > 0 && $randombox_mb['erm_pop_count'] > 0) {
	$randombox_coupon = randombox_discount_rate($randombox_row);

	$result_arr = array_merge($result_arr, $randombox_coupon);
	$result_arr['er_available_date_end'] = str_replace("-", ". ", $result_arr['er_available_date_end']);
	$result_arr['result'] = true;

	// 쿠폰 저장
	if($coupon_no = randombox_member_coupon_get($randombox_mb, $result_arr)) {
		$result_arr['erc_no'] = $coupon_no;
		// 뽑기 찬스 및 참여 땅콩 소모
		if(randombox_member_pop($randombox_mb)) {
			// 성공했을 시 로그 남기기
			if(randombox_pop_log($randombox_mb, $result_arr)) {
				// 성공했을 시 통계 남기기
				st_stats_randombox_pop($result_arr);
			} // end if
		} // end if
	} // end if

	echo json_encode($result_arr);
	die;
} // end if
?>
