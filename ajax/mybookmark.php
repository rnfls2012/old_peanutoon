<?
include_once '_common.php'; // 공통

$result['state'] = 0;
$result['msg'] = "";
$result['mode'] = "";

$comics = get_comics($_REQUEST['cm_no']); // 단행본 정보 가져오기

if($nm_member['mb_id'] == ""){
	$result['state'] = 1;
	$result['msg'] = "로그인 해주세요.";
}

if($comics['cm_no'] == ""){
	$result['state'] = 2;
	$result['msg'] = "코믹스를 확인 해주세요.";
}

if($result['state'] == 0){
	$bookmark_query = "select * from member_comics_mark where mcm_member = '".$nm_member['mb_no']."' and mcm_comics = '".$cm_no."'";
	$count_bookmark = sql_num_rows(sql_query($bookmark_query));

	/* INSERT / DELETE */
	if($count_bookmark == 0) { // 결과 없을 때 INSERT
		$sql_bookmark = "insert into member_comics_mark(mcm_member, mcm_member_idx, mcm_comics, mcm_big, mcm_small, mcm_date) values('".
			$nm_member['mb_no']."', '".$nm_member['mb_idx']."', '".$comics['cm_no']."', '".$comics['cm_big']."', '".$comics['cm_small']."', '".NM_TIME_YMDHIS."')";
		$result['mode'] = "in";
		$result['msg'] = "즐겨찾기에 저장되였습니다.";
	} else { // 결과 있을 때 DELETE
		$sql_bookmark = "delete from member_comics_mark where mcm_member = '".$nm_member['mb_no']."' and mcm_comics = '".$cm_no."'";
		$result['mode'] = "del";
		$result['msg'] = "즐겨찾기에 삭제되였습니다.";
	} // end else

	if(!sql_query($sql_bookmark)) {
		$result['state'] = 3;
		$result['msg'] = "즐겨찾기 연동 실패, 관리자에게 문의하세요.";
		// $result['msg'] = $sql_bookmark;
	} // end if
}

header( "content-type: application/json; charset=utf-8" );
echo json_encode($result);
die;
?>