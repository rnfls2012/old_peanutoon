<?
include_once '_common.php'; // 공통

/* 로그인 체크 */
cs_login_check();

/* 반환할 값 배열 */
$result_arr = array("result"=>false, "text"=>"", "action"=>"", "code"=>"0", "goods"=>"n", "loss"=>"y", "eci_no"=>"0", "text1"=>"", "text2"=>"");


/* 회원 크리스마스 진행정보 가져오기 */
$event_christmas_bool = true;

if(event_christmas_state_get() == "") {
	$event_christmas_bool = false;
}

if($event_christmas_bool == false){
	$result_arr['result'] = true;
	$result_arr['text'] = "진행중인 이벤트가 없습니다!";
	$result_arr['code'] = "9";
}else{
	$ec_no = intval(event_christmas_state_get()); // 이벤트 번호

	$christmas_mb = event_christmas_point_used_get($nm_member);	

	if($christmas_mb['ecpu_member'] == "") {
		event_christmas_point_used_set($nm_member);
		$christmas_mb = event_christmas_point_used_get($nm_member);
	} // end if


	$christmas_decision = event_christmas_point_used_decision($christmas_mb);
	$christmas_action = $christmas_decision['action']; // 회원상태
	$christmas_calc= intval($christmas_decision['calc']); // 소진 땅콩수
	$christmas_chance = intval($christmas_decision['chance']); // 뽑기 수
	$christmas_fill= intval($christmas_decision['fill']); // 땅콩 채울 수

	if($christmas_action == 'enable'){
		// item, member 처리
		$eci_no = event_christmas_catagory_rand($nm_member);
		if($eci_no >= 0){
			// ecpu_chance_used 처리
			$result_chance = event_christmas_gift_chance_used($nm_member);
			if($result_chance == 1){
				$result_arr['result'] = true;

				if($eci_no == 0){ // 꽝 => 미니땅콩
					$eci_no_data = event_christmas_rows_get();
						cs_set_member_point_income_event($nm_member, "0", $eci_no_data['ec_loss'], "christmas", "크리스마스 이벤트 ".$eci_no_data['ec_loss']." 미니땅콩 지급", "y");
					$result_arr['text'] = "미니땅콩 ".$eci_no_data['ec_loss']."개";
				}else{
					$eci_no_data = event_christmas_item_get_no($eci_no);
					$result_arr['goods'] = $eci_no_data['ecc_is_goods'];
					$result_arr['loss'] = 'n';
					if($eci_no_data['ecc_is_goods'] == 'n'){  // 선물 => 미니땅콩
						cs_set_member_point_income_event($nm_member, "0", $eci_no_data['ecc_point'], "christmas", "크리스마스 이벤트 ".$eci_no_data['ecc_point']." 미니땅콩 지급", "y");
						$result_arr['text'] = "미니땅콩 ".$eci_no_data['ecc_point']."개";
					}else{ // 선물 => 상품
						$result_arr['text'] = "<strong>".$eci_no_data['ecc_name']."</strong> 당첨 됐어요!";
						$result_arr['eci_no'] = $eci_no;
					}
				}

			}else{
				$result_arr['result'] = true;
				$result_arr['text'] = "뽑기 저장중에 에러났습니다. 캡처 후 관리자에게 문의 바랍니다.";
				$result_arr['code'] = "-1";
			}
		}else{
			$result_arr['result'] = true;
			$result_arr['text'] = "저장중에 에러났습니다. 캡처 후 관리자에게 문의 바랍니다.";
			$result_arr['code'] = $result_rand;
		}

	}else if($christmas_action == 'unable'){
		$result_arr['result'] = true;
		$result_arr['text'] = "선물 상자까지 ".$christmas_fill." 땅콩 남았습니다.";
		$result_arr['code'] = "8";
	}else{
		$result_arr['result'] = true;
		$result_arr['text'] = "해당 이벤트는 로그인시 참여하실 수 있습니다.";
		$result_arr['code'] = "7";
	}
	
	$christmas_mb = event_christmas_point_used_get($nm_member);
	$christmas_decision = event_christmas_point_used_decision($christmas_mb);
	$christmas_action = $christmas_decision['action']; // 회원상태
	$christmas_calc= intval($christmas_decision['calc']); // 소진 땅콩수
	$christmas_chance = intval($christmas_decision['chance']); // 뽑기 수
	$christmas_fill= intval($christmas_decision['fill']); // 땅콩 채울 수
	$christmas_percent = intval($christmas_decision['percent']); // 땅콩 %
	
	if($christmas_action == 'enable'){
		$result_arr['text1']   = $christmas_chance."번";
		$result_arr['text2']   = "OPEN!";
	}else if($christmas_action == 'unable'){
		$result_arr['text1'] = $christmas_calc."개";
		$result_arr['text2'] = "CLOSE!";
	}else{
		$result_arr['text1'] = "로그인시";
		$result_arr['text2'] = "참여 가능";
	}
	$result_arr['percent']   = $christmas_percent;	
	$result_arr['calc']   = $christmas_calc;
	$result_arr['action'] = $christmas_action;
}


// print_r($result_arr);

header( "content-type: application/json; charset=utf-8" );
echo json_encode($result_arr);
die;
?>
