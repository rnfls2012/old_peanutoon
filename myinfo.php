<?
include_once '_common.php'; // 공통

error_page();

/* DB */
/* 회원정보 가져오기 */
cs_login_check();

/* 충전 & 이벤트 충전내역 배열 */
$income_arr = array();
$income_sql = "SELECT *, CASE(mpu_type) 
				WHEN 'cms-admin' THEN '관리자 지급'
				WHEN 'allpay' THEN '전체구매 보너스' 
				ELSE 
				IF(mpu_refund=1 && mpu_recharge_won>0, '환불중',
					IF(mpu_refund=1 && mpu_recharge_won<0, '환불완료', 
						IF(mpu_refund=2, '환불완료',
						'충전')))
				end AS mpu_in_type 
				FROM member_point_used WHERE (mpu_class='r' OR mpu_class='e') and mpu_member='".$nm_member['mb_no']."' and mpu_member_idx = '".$nm_member['mb_idx']."'
				ORDER BY mpu_date DESC, mpu_cancel DESC";
$income_result = sql_query($income_sql);

while($row = sql_fetch_array($income_result)) {
	array_push($income_arr, $row);
} // end while

/* 사용내역 배열 */
$expen_arr = array();
$expen_sql = "SELECT * FROM member_point_used WHERE mpu_class='b' and mpu_member='".$nm_member['mb_no']."' and mpu_member_idx = '".$nm_member['mb_idx']."'
				ORDER BY mpu_date DESC";
$expen_result = sql_query($expen_sql);

while($row = sql_fetch_array($expen_result)) {
	array_push($expen_arr, $row);
} // end while

/* 1:1문의내역 배열 */
$ask_arr = array();
$ask_arr = mb_get_ask($nm_member, "ba_date", "desc", 50);
$ask_mode_arr = array("답변 대기중", "답변 완료");

/* 이메일 수신 여부 */
if($nm_member['mb_post'] == "y") {
	$mb_post_ok = "on";
} else {
	$mb_post_no = "on";
} // end else

$mem_rest3 = $mem_rest5 = $mem_restever = "";
switch($nm_member['mb_human_prevent']) {
	case 3 :
		$mem_rest3 = "checked";
		break;
	
	case 5 :
		$mem_rest5 = "checked";
		break;

	case "out" :
		$mem_restever = "checked";
		break;
} // end switch

/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/myinfo.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>