<?
include_once '_common.php'; // 공통
/*
if(!is_nexcube()) {
	alert("서비스 준비중입니다.", "index.php");
	die;
}
*/

/* 관리자만보기 */ 
if(mb_class_permission('c,p') && $_e_date != '') { 
	unset($_e_date);
	/*
	if(intval(NM_TIME_M) != intval($_e_date) {
		cs_alert('현재 관리자만 보입니다. 등급을 관리자에게 문의 바랍니다.',NM_URL);
		$_e_date = "";
		die;
	} // end if
	*/
} // end if

// 날짜 변수
$set_date['year'] = NM_TIME_Y;
$set_date['month'] = NM_TIME_M;
$set_date['day'] = NM_TIME_D;

/* 테스트 날짜 변수 */
if($_e_date) { 
	// $set_date['year'] = '2017';
	$set_date['month'] = $_e_date;
	$set_date['day'] = '01';
} // end if

$set_date['ymd'] = $set_date['year'].'-'.$set_date['month'].'-'.$set_date['day'];

/* DB */
// 현재 달(MONTH)의 출석 이벤트 정보 가져오기
$ea_arr = array();
$ea_arr = sql_fetch("select * from event_attend where ea_month=".$set_date['month']);

// 현재 달(MONTH)의 연속 출석 정보 가져오기
$ea_cont_arr = array(
	$ea_arr['ea_cont_date_1']=>$ea_arr['ea_cont_point_1'], 
	$ea_arr['ea_cont_date_2']=>$ea_arr['ea_cont_point_2'], 
	$ea_arr['ea_cont_date_3']=>$ea_arr['ea_cont_point_3']
);
ksort($ea_cont_arr);


// 회원의 나이대를 가져오기
$nm_member['mb_age'] = mb_get_age_group($nm_member['mb_birth']);

// 회원의 현재 출석 상황 가져오기
// $member_attend_sql = "select * from z_member_attend where z_ma_member='".$nm_member['mb_no']."' and z_ma_attend_year='".$set_date['year']."' and z_ma_attend_month=".$set_date['month']." order by z_ma_attend_date asc";
$member_attend_sql = "select * from z_member_attend where z_ma_member='".$nm_member['mb_no']."' and z_ma_attend_year='".$set_date['year']."' and z_ma_attend_month=".$set_date['month']." group by z_ma_attend_day order by z_ma_attend_date asc";
$member_attend_result = sql_query($member_attend_sql);
$member_attend_row = array();
while($row = sql_fetch_array($member_attend_result)) {
	array_push($member_attend_row, $row);
} // end while

// 회원의 출석 보상 합계
$member_attend_point_sum = 0;
if(count($member_attend_row) != 0) {
	foreach($member_attend_row as $key => $val) {
		$member_attend_point_sum += $val['z_ma_point'];
	} // end foreach
} // end if

// 달력 출력용

$month_eng = date("F", mktime(0, 0, 0, $set_date['month'], 1, $set_date['year'])); // 1. 현재 달의 월 영어 풀네임
$date = date("t", mktime(0, 0, 0, $set_date['month'], 1, $set_date['year'])); // 2. 현재 달의 일수
$start_day = date("w", mktime(0, 0, 0, $set_date['month'], 1, $set_date['year'])); // 3. 현재 달의 시작 요일
$start_date = 1; // 4. 시작 일
$week_rows = ceil(($date + $start_day)/7); // 5. 출력해야할 행 계산

/* 달력 출력 배열 */
$cal_arr = $day_arr = array(); // 달력 출력 배열
/* // ^^-ksk 주석처리
$attend_img = NM_IMG.'_pc/event/2017/attend'; // 이미지 경로
if(is_mobile()) { $attend_img = NM_IMG.'_mobile/event/2017/attend'; }
*/
$attend_img_common = NM_IMG.$nm_config['nm_mode'].'/event/attend/common'; // 공통 이미지 경로
$attend_img_header = NM_IMG.$ea_arr['ea_header_img'.$nm_config['nm_mode']].vs_para(); // 헤더 이미지 경로
$attend_img_reward = NM_IMG.$ea_arr['ea_reward_img'.$nm_config['nm_mode']].vs_para(); // 리워드 이미지 경로
$attend_img_warning = NM_IMG.$ea_arr['ea_warning_img'.$nm_config['nm_mode']].vs_para(); // 경고 이미지 경로

for($week=0; $week<$week_rows; $week++) { 
	
	for($cols=0; $cols<7; $cols++) {
		$cell_index = (7 * $week) + $cols;
		$weekend_class = ""; // 토, 일 클래스
		$today_class = ""; // 오늘 클래스
		$empty_class = ""; // 작월, 익월 클래스
		$stamp_alt = ""; // 출석 이름
		$stamp_img = ""; // 출석 이미지
		$date_count = ""; // 출력 날짜
		$li_count = ""; // li 출력용

		if($start_day <= $cell_index && $start_date <= $date) { // 이번 달일때

			/* 주말 표기 클래스 */
			$weekend_index = date("w", mktime( 0, 0, 0, $set_date['month'], $start_date, $set_date['year']));
		
			if($weekend_index == 6) {
				$weekend_class = "sat";
			} else if($weekend_index == 0) {
				$weekend_class = "sun";
			} // end else if
			
			/* 오늘 표시 클래스 */
			if($start_date == $set_date['day']) { $today_class = "today"; } // end if

			/* 이미지 처리 */
			// 이벤트 기간 내 처리
			if($start_date >= $ea_arr['ea_start_date'] && $start_date <= $ea_arr['ea_end_date'] && $ea_arr['ea_use'] == "y") {
				$stamp_name = "미출석";
				$stamp_img = '<img src="'.$attend_img_common.'/evt05_stamp_off.png'.vs_para().'" alt="'.$stamp_name.'">';

				// 회원 출석 여부 체크
				if(count($member_attend_row) > 0) {
					foreach($member_attend_row as $key => $val) {
						// 출석한 날짜중 맞는 날짜 검색
						if($start_date == $val['z_ma_attend_day']) {
							if($val['z_ma_attend_regu'] == "y") { // 개근일 때
								$stamp_name = $ea_arr['ea_regu_date']."일 개근";
								$stamp_img = '<img src="'.$attend_img_common.'/evt05_stamp30_on.png'.vs_para().'" alt="'.$stamp_name.'">';
							} else if($val['z_ma_attend_cont'] == "y") { // 연속 출석일 때
								// 이미지 처리해야 함
								foreach($ea_cont_arr as $ea_cont_key => $ea_cont_val) {
									if($ea_cont_key != "") {
										if($val['z_ma_cont_count'] % $ea_cont_key == 0) {
											$stamp_name = $ea_cont_val."일 연속 출석";
											$stamp_img = '<img src="'.$attend_img_common.'/evt05_stamp5_on.png'.vs_para().'" alt="'.$stamp_name.'">'; // 이미지 이름 처리
										} // end if
									} // end if
								} // end foreach
							} else { // 일반 출석일 때
								$stamp_name = "출석";
								$stamp_img = '<img src="'.$attend_img_common.'/evt05_stamp_on.png'.vs_para().'" alt="'.$stamp_name.'">';
							} // end else
						// 출석한 날짜가 맞지 않을 때
						} // end if
					} // end foreach
				} // end if
			} // end if
			
			/* 날짜 표기 */
			$date_count = $start_date++;
			
		} else if($cell_index < $start_day || $cell_index >= $date) { // 이전 달 || 다음 달일때
			$empty_class = "empty";
		} // end else if
		
		/* 배열에 데이터 집어넣기 */
		$day_arr['weekend_class'] = $weekend_class;
		$day_arr['today_class'] = $today_class;
		$day_arr['empty_class'] = $empty_class;
		$day_arr['stamp_alt'] = $stamp_alt;
		$day_arr['stamp_img'] = $stamp_img;
		$day_arr['date_count'] = $date_count;
		$day_arr['li_count'] = $cell_index%7;
		
		array_push($cal_arr, $day_arr);
	} // end for
} // end for

/* MAIN-DB - 메인은 제외 PC와 mobile 메인화면이 틀림 */
/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/eventattend.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>