<? include_once '_common.php'; // 공통

error_page();

include_once (NM_PATH.'/_head.php'); // 공통

/* COOKIE */

$sub_list_url = get_js_cookie('ck_sub_list_url');
$sub_list_limit = intval(get_int(get_js_cookie('ck_load_list_limit')));
if($sub_list_limit < NM_SUB_LIST_LIMIT || $sub_list_limit == ''){ $sub_list_limit = NM_SUB_LIST_LIMIT; }

if($sub_list_url != $_SERVER['REQUEST_URI']){ $sub_list_limit = NM_SUB_LIST_LIMIT; }

/* DB */

$cmtop_arr = array();
$sql_cm_adult = sql_adult($mb_adult_permission , 'cm.cm_adult');

// 총갯수
$sql_cmtop_total = "
 SELECT count(*) as total FROM comics_ranking_sales_cmtop_auto c 
 WHERE 1 
 ; ";
$row_total = sql_count($sql_cmtop_total, 'total');


// 익스플로러일때 전체 출력-임시
if(substr(get_brow(HTTP_USER_AGENT), 0, 4) == "MSIE"){ $sub_list_limit = $row_total; }
$_menu = "0"; // 단일 메뉴일 경우(서브메뉴x)

// cm_reg_date_new 아직 사용 안하지만 나중에 사용할수 있음

// 특정 만화 순서 앞으로...18-04-25
$week_comicno_arr[0] = array(); // 배열선언
$sql_ct_adult = sql_adult($mb_adult_permission , 'ct_adult');
$sql_ct_class = "";
if($sql_ct_adult !=''){ $sql_ct_class = " AND ct_class LIKE '%_t' "; }

$sql_ct = " select * from comics_top where 1 $sql_ct_adult $sql_ct_class order by ct_class, ct_ranking ";
$result_ct = sql_query($sql_ct);
while ($row_ct = sql_fetch_array($result_ct)) {
	$ct_class_key = intval(str_replace("_t", "", $row_ct['ct_class']));
	array_push($week_comicno_arr[$ct_class_key], $row_ct['ct_comics']);
}

$week_comicno_order = $week_comicno_arr[0];

$sql_week_comicno_field = "";
$sql_week_comicno_order = "";

$sql_week_comicno_where = ""; // 18-05-14 마오랑대표&부사장님지시사항

if(count($week_comicno_order) > 0){
	$week_comicno_order_arr = $week_comicno_order;

	$sql_week_comicno_field = " CASE ";
	foreach($week_comicno_order_arr as $week_comicno_order_key => $week_comicno_order_val){
		$sql_then = 99 - intval($week_comicno_order_key);
		$sql_week_comicno_field.= " WHEN c.cm_no = '".$week_comicno_order_val."' THEN ".$sql_then." ";
	}
	$sql_week_comicno_field.= "ELSE 0 ";
	$sql_week_comicno_field.= "END AS cm_no_special, ";

	$sql_week_comicno_order = "cm_no_special DESC, ";

	$sql_week_comicno_where = " OR c.cm_no IN (".implode(", ",$week_comicno_order).") "; // 18-05-14 마오랑대표&부사장님지시사항
}

/* 클래스 */
$sql_ct_class_val = '0';
if($sql_ct_adult !=''){ $sql_ct_class_val = '0_t'; }
$sql_ct_class = " AND crs.crs_class = '".$sql_ct_class_val."' ";

$sql_cmtop = " SELECT c.*, crs.crs_ranking, 
					".$sql_week_comicno_field."
					c_prof.cp_name as prof_name 
				FROM comics c 
				left JOIN comics_ranking_sales_cmtop_auto crs ON crs.crs_comics = c.cm_no 				 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 $sql_ct_class 
				$sql_week_comicno_where 	
				GROUP BY c.cm_no 	
				ORDER BY ".$sql_week_comicno_order." 
				crs.crs_ranking ASC  
				LIMIT 0, ".$sub_list_limit."; ";
// 리스트 가져오기
$cmtop_arr = cs_comics_content_list($sql_cmtop, substr($nm_config['nm_path'], -1));

$sub_list_bool = "false";
if(count($cmtop_arr) < NM_SUB_LIST_LIMIT){ $sub_list_bool = "true"; }

/* view */

include_once($nm_config['nm_path']."/cmtop.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>