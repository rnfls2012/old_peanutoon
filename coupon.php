<?php

include_once("_common.php");

// WEB 쿠폰항목
$coupon_list_web = array();
array_push($coupon_list_web, array("날짜", "temp_1")); // 회원이 쿠폰 획득 한 날짜
array_push($coupon_list_web, array("지급형태", "temp_2")); // 쿠폰의 형태 (이벤트, 보상...)
array_push($coupon_list_web, array("쿠폰명", "temp_3")); // 쿠폰의 이름 (30%결제할인...)
array_push($coupon_list_web, array("쿠폰내용", "temp_4")); // 쿠폰 발행된 이벤트 (설날이벤트, 점검보상...)
array_push($coupon_list_web, array("유효기간", "temp_5")); // 쿠폰의 유효기간 or 쿠폰 발행된 이벤트 종료날짜
array_push($coupon_list_web, array("사용하기", "temp_5")); // 쿠폰의 유효기간 or 쿠폰 발행된 이벤트 종료날짜

// WEB 쿠폰사용항목
$coupon_used_list_web = array();
array_push($coupon_used_list_web, array("지급형태", "temp_2")); // 쿠폰의 형태 (이벤트, 보상...)
array_push($coupon_used_list_web, array("쿠폰명", "temp_3")); // 쿠폰의 이름 (30%결제할인...)
array_push($coupon_used_list_web, array("쿠폰내용", "temp_4")); // 쿠폰 발행된 이벤트 (설날이벤트, 점검보상...)
array_push($coupon_used_list_web, array("사용일시", "temp_5")); // 유저가 쿠폰을 사용한 날짜

// MOBILE 쿠폰항목
$coupon_list_mob = array();
array_push($coupon_list_mob, array("날짜", "temp_1")); // 회원이 쿠폰 획득 한 날짜
array_push($coupon_list_mob, array("쿠폰명", "temp_2")); // 쿠폰의 이름 (30%결제할인...)
array_push($coupon_list_mob, array("유효기간", "temp_3")); // 쿠폰의 유효기간 or 쿠폰 발행된 이벤트 종료날짜

// MOBILE 쿠폰사용항목
$coupon_used_list_mob = array();
array_push($coupon_used_list_mob, array("날짜", "temp_1")); // 회원이 쿠폰 획득 한 날짜
array_push($coupon_used_list_mob, array("쿠폰명", "temp_2")); // 쿠폰의 이름 (30%결제할인...)
array_push($coupon_used_list_mob, array("사용일시", "temp_3")); // 유저가 쿠폰을 사용한 날짜

$coupon_sql = "
SELECT er.er_name AS coupon_cont, CASE(erc_er_type) WHEN 'd' THEN '할인권' END AS coupon_type, erc_discount_rate AS coupon_name, er.er_available_date_end AS coupon_end_date, erc_date, erc_state, erc_use, erc_no
FROM event_randombox_coupon erc
LEFT JOIN event_randombox er ON erc.erc_er_no = er.er_no
WHERE erc.erc_member = '".$nm_member['mb_no']."' AND erc.erc_member_idx = '".$nm_member['mb_idx']."' AND erc.erc_use = 'n' ORDER BY erc_date DESC, coupon_name DESC, DATEDIFF(coupon_end_date, erc_date)
";

$used_sql = "
SELECT er.er_name AS coupon_cont, CASE(erc_er_type) WHEN 'd' THEN '할인권' END AS coupon_type, erc_discount_rate AS coupon_name, erc.erc_use_date AS coupon_used_date
FROM event_randombox_coupon erc
LEFT JOIN event_randombox er ON erc.erc_er_no = er.er_no
WHERE erc.erc_member = '".$nm_member['mb_no']."' AND erc.erc_member_idx = '".$nm_member['mb_idx']."' AND erc.erc_use = 'y' AND erc.erc_use_date != ''
";

$used_give_sql = "
SELECT ecm.ecm_ticket AS coupon_cont, CASE WHEN ec_code IS NOT NULL THEN '지급권' END AS coupon_type, ecm.ecm_point AS coupon_name,  SUBSTRING(ec.ec_use_date, 1, 10) AS coupon_used_date
FROM event_coupon ec
LEFT JOIN event_couponment ecm ON ec.ec_ecm_no = ecm.ecm_no
WHERE ec.ec_member = '".$nm_member['mb_no']."' AND ec.ec_member_idx = '".$nm_member['mb_idx']."' AND ec.ec_use_date != ''
";

$coupon_result = sql_query($coupon_sql);
$used_result = sql_query($used_sql);
$used_give_result = sql_query($used_give_sql);

$coupon_arr = $used_arr = $used_give_arr = $temp = array();

while($row_coupon = sql_fetch_array($coupon_result)) {
    $row_coupon['coupon_name'] .= " "."% 결제 할인";
    array_push($coupon_arr, $row_coupon);
} // end while

while($row_used = sql_fetch_array($used_result)) {
    $row_used['coupon_name'] .= " "."% 결제 할인";
    array_push($used_arr, $row_used);
} // end while

while($row_give = sql_fetch_array($used_give_result)) {
    $row_give['coupon_name'] .= " ".$nm_config['cf_point_unit_ko'];
    array_push($used_give_arr, $row_give);
} // end while

$total_used_arr = array_merge($used_arr, $used_give_arr);

$total_used_arr = bubble_down_sort($total_used_arr,'coupon_used_date');

// 할인권 쿠폰 URL
$recharge_url = NM_URL."/recharge.php?v=".substr(NM_VERSION,1,1);
if(intval($_mb_crp_no) > 0){ $recharge_url.= "&mb_crp_no=".$_mb_crp_no; }
if(intval($_mb_payway_no) >= 0 && $_mb_payway_no !=''){ $recharge_url.= "&mb_payway_no=".$_mb_payway_no; }

include_once(NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/coupon.php");

include_once(NM_PATH.'/_tail.php'); // 공통
/* view */