<?
include_once '_common.php'; // 공통

error_page();

/* DB */
$comics = get_comics($_comics);

// 에피소드
$sql = "SELECT * FROM comics_episode_".$comics['cm_big']." WHERE 1 AND ce_comics = '".$comics['cm_no']."' AND ce_no = '".$_episode."' ";
$episode = sql_fetch($sql);
if($episode['ce_no'] == ''){ cs_alert('작품이 없습니다.', NM_URL); die; }

// 마케팅으로 인해 성인체크는 로그인전 보기 하면 바로 보이게 수정하기170901
// 로그인 후 보기
if($episode['ce_login'] == 'y'){
	cs_login_check(); // 로그인 체크
	// 성인체크
	cs_comics_adult($comics['cm_no'], $nm_member);
}else{
}

// css 파일명 제어
$comicsview_css = "comicsviewscroll.css";
if($comics['cm_page_way'] == "r" || $comics['cm_page_way'] == "l") {
	$comicsview_css = "comicsviewpage.css";
} // end if

$randombox_state_get_ck = false;
/* 뷰어 내 이벤트 안내 20180213 */
if(randombox_state_get() != "") {
	// css 파일명 제어
	$comicsview_css = "comicsviewscroll_randombox.css";
	if($comics['cm_page_way'] == "r" || $comics['cm_page_way'] == "l") {
		$comicsview_css = "comicsviewpage_randombox.css";
	} // end if

	$randombox_mb = randombox_member_get($nm_member, randombox_state_get()); // 이벤트 번호 제어 randombox_state_get() 사용 180213
	$randombox_mb['erm_cash_point_total'] = ($randombox_mb['erm_cash_point_push'] + ($randombox_mb['erm_point_push'] / 10)) - $randombox_mb['erm_cash_point_pop']; // 결제 땅콩, 뽑기에 쓴 땅콩 계산한 값

	$randombox_state_get_ck = true;
} // end if

// 토큰
$user_token = get_token($nm_member['mb_no']."_".$comics['cm_no']."_".$episode['ce_no']);

// 에피소드 리스트
// 이전화, 다음화, 화 리스트
$ce_no_prev = $ce_no_next = $episode_inviewer_list_arr = array();

$sql_episode_list = "SELECT * FROM comics_episode_".$comics['cm_big']." WHERE 1 AND ce_comics = '".$comics['cm_no']."' ORDER BY ce_order ASC ";
$result_episode_list = sql_query($sql_episode_list);
$episode_inviewer_list_arr = cs_get_view_episode_list($comics['cm_no'], $nm_member, $episode['ce_no']);
$episode_total = count($episode_inviewer_list_arr);

$ce_no_prev = $episode_inviewer_list_arr[$episode_total-1]; // 이전화 정보
$episode['ce_no_prev'] = $ce_no_prev['ce_no'];				// 이전화 번호
if(cs_partner_return_y($nm_member, $comics) == "y") {		// 작가, 출판사, 제공사 무료 170912
	$episode['ce_no_prev_pay'] = 0;
} else {
	$episode['ce_no_prev_pay'] = intval($ce_no_prev['ce_pay']); // 이전화 가격
} // end else
$episode['ce_no_prev_txt'] = $ce_no_prev['ce_txt'];			// 이전화 텍스트
array_pop($episode_inviewer_list_arr);						// 화 리스트에서 이전화 제거

$ce_no_next = $episode_inviewer_list_arr[$episode_total-2]; // 다음화 정보 
$episode['ce_no_next'] = $ce_no_next['ce_no'];				// 다음화 번호
if(cs_partner_return_y($nm_member, $comics) == "y") {       // 작가, 출판사, 제공사 무료 170912
	$episode['ce_no_next_pay'] = 0;
} else {
	$episode['ce_no_next_pay'] = intval($ce_no_next['ce_pay']); // 다음화 가격
} // end else

$episode['ce_no_next_txt'] = $ce_no_next['ce_txt'];			// 다음화 텍스트
array_pop($episode_inviewer_list_arr);						// 화 리스트에서 다음화 제거

krsort($episode_inviewer_list_arr); // 역순정렬

/*
print_r($episode_inviewer_list_arr);
echo "<br/>";
print_r($ce_no_prev);
echo "<br/>";
print_r($ce_no_next);
*/

/* 서비스 상태 판별 */
$service_state_bool = false;

/* 서비스 중 */
if($episode['ce_service_state'] == 'y'){
	$service_state_bool = true;
}

/* 서비스 완전 종료 */
if($episode['ce_service_state'] == 'x'){
	$service_state_bool = true;
	if($nm_member['mb_class'] != "a"){
		cs_alert('서비스가 종료된 화 입니다.x',NM_URL."/comics.php?comics=".$comics['cm_no']);
		die;
	}
}

// 이벤트
$event_arr = array();
$sql_event = "SELECT * FROM event_pay WHERE 1 AND ep_state = 'y' ";
$result_event = sql_query($sql_event);
if(sql_num_rows($result_event) != 0){
	while ($row_event = sql_fetch_array($result_event)) {
		array_push($event_arr, $row_event['ep_no']);
	}
}


// 이벤트 번호 큰수 하나만
$ce_epc_free = $ce_epc_sale = '';
if(count($event_arr) > 0){
	$sql_event_comics_in = implode(", ",$event_arr);
	$sql_event_comics = "SELECT * FROM event_pay_comics WHERE 1 AND epc_ep_no in(".$sql_event_comics_in.") AND epc_comics = '".$comics['cm_no']."' ORDER BY epc_ep_no DESC LIMIT 0, 1";
	$row_event_comics = sql_fetch($sql_event_comics);
	$ce_epc_free = $row_event_comics['epc_free'];
	$ce_epc_sale = $row_event_comics['epc_sale'];
}

/* ////////////////// 체크 변수 초기화////////////////// */
$episode['ce_event_free'] = $episode['ce_event_sale'] = $event_sale = $event_free = $episode_buy = $comics_buy = $comics_free = $state_except = 'n'; 
// 이벤트세일 = 이벤트무료 = 코믹스무료 = 통계제외 = n
$comics_buy = array();
$comics_buy['sl_event'] =  $comics_buy['sl_sale'] =  $comics_buy['sl_free'] = 0;
$comics_buy['mb_cash_point'] = $comics_buy['mb_point'] = $comics_buy['sl_pay'] = 0;
$comics_buy['mbc_own_type'] = $comics['cm_own_type'];

/* ////////////////// 회원 체크 ////////////////// */
if($nm_member['mb_class'] == "a" || $_SESSION['ss_adm_id'] != "" || $_SESSION['ss_adm_id'] != NULL){ $state_except = $comics_free = "y"; }
if($nm_member['mb_class'] == "p"){ 
	$state_except = "y";
	// 파트너 자신의 작품에 한해서 무료 170912
	$comics_free = cs_partner_return_y($nm_member, $comics);
} // end if

/* ////////////////// 무료인지 체크 ////////////////// */
// 에피소드 가격
if($episode['ce_pay'] == 0){ 
	$comics_free = "y"; 
	$comics_buy['sl_free'] = 1;
	$comics_buy['mbc_own_type'] = 0;
}

// 코믹스 무료 화수
if( 0 < $episode['ce_chapter'] && $episode['ce_chapter'] <= $comics['cm_free'] && $episode['ce_pay'] != 0){
	$comics_free = "y";
	$comics_buy['sl_free'] = 1;
	$comics_buy['mbc_own_type'] = 0;
}

/* ////////////////// 이벤트 체크 ////////////////// */
// 이벤트 무료
if($ce_epc_free == 'y'){
	if( 0 < $episode['ce_chapter'] && $episode['ce_chapter'] <= $row_event_comics['epc_free_e'] && $episode['ce_chapter'] >= $row_event_comics['epc_free_s'] && $episode['ce_pay'] != 0){
		$episode['ce_event_free'] = $event_free = $comics_free = "y";
		$comics_buy['sl_free'] = $comics_buy['sl_event'] = 1;
		$comics_buy['mbc_own_type'] = 0;
	} 
}

// 이벤트 코인할인
if($ce_epc_sale == 'y'){
	if( 0 < $episode['ce_chapter'] && $episode['ce_chapter'] <= $row_event_comics['epc_sale_e'] && $episode['ce_chapter'] >= $row_event_comics['epc_sale_s'] && $episode['ce_pay'] != 0){
		$episode['ce_pay'] = $row_event_comics['epc_sale_pay'];
		$episode['ce_event_sale'] = $event_sale = "y";
		$comics_buy['sl_sale'] = $comics_buy['sl_event'] = 1;
	}			
}

/* ////////////////// 소유한 작품 체크 ////////////////// */
$mb_buy_episode_list = array();
$mb_get_buy_episode = mb_get_buy_episode($nm_member, $comics['cm_no'], 'y');
if(in_array($episode['ce_no'], $mb_get_buy_episode)){
	$episode_buy = $comics_free = "y";
	$comics_buy['sl_event'] =  $comics_buy['sl_sale'] =  $comics_buy['sl_free'] = 0;
	$comics_buy['mb_cash_point'] = $comics_buy['mb_point'] = $comics_buy['sl_pay'] = 0;
}

if($episode_buy == 'y'){
	$service_state_bool = true;
}else{
	/* 서비스 예약 */
	if($episode['ce_service_state'] == 'r'){
		$service_state_bool = true;
		if($nm_member['mb_class'] != "a"){
			cs_alert('서비스가 곧 제공될 화 입니다.',NM_URL."/comics.php?comics=".$comics['cm_no']);
			die;
		}
	}

	/* 서비스 소장 이외 종료 */
	if($episode['ce_service_state'] == 'n'){
		$service_state_bool = true;
		if($episode_buy != 'y'){
			if($nm_member['mb_class'] != "a"){
				cs_alert('서비스가 종료된 화 입니다.n',NM_URL."/comics.php?comics=".$comics['cm_no']);
				die;
			}
		}
	}

}

/* 서비스 상태 확인 */
if($service_state_bool != true){
	cs_alert('해당 화 서비스 상태를 관리자에게 문의 바랍니다.[state_bool_false]',NM_URL."/comics.php?comics=".$comics['cm_no']);
	die;
}

// 코믹스 리스트 링크
$comics_url = get_comics_url($comics['cm_no']);

// 다이렉트로 올 경우 다시한번 결제 할지 팝업창 띄우기
// 트위터에서 앱을 실행시켜서 결제진행해버리는 바람에 if(!is_app()){ 주석처리함 - 17-12-26
// if(!is_app()){
	// if( !strpos(HTTP_REFERER, HTTP_HOST) || HTTP_REFERER == ''){
	$mb_buy_click = false;
	if(strpos(HTTP_REFERER, NM_DOMAIN_ONLY.'/mycomics.php')){ $mb_buy_click = true; } // 코믹스리스트
	if(strpos(HTTP_REFERER, NM_DOMAIN_ONLY.'/comics.php')){ $mb_buy_click = true; } // 내서재
	if(strpos(HTTP_REFERER, NM_DOMAIN_ONLY.'/comicsview.php')){ $mb_buy_click = true; } // VIEW화면
	if($mb_buy_click == false){
		// if( $_ce_dt != $user_token ){
			if($episode['ce_pay'] > 0){ // 유료일 경우
				if($comics_free != "y"){ // 유료일 경우
					$direct_ce_up_kind = '화';
					if($comics['cm_up_kind'] == '1'){ $direct_ce_up_kind = '권'; }
					if($episode['ce_chapter'] > 0){
						$direct_ce_txt = $episode['ce_chapter'].$direct_ce_up_kind;
						if($episode['ce_title'] != ''){ $direct_ce_txt = $episode['ce_chapter'].$direct_ce_up_kind." - ".$episode['ce_title']; }
					}else{
						$direct_ce_txt = $episode['ce_notice'];
					}
					$direct_txt = $direct_ce_txt."를 보시려면 결제가 필요합니다.<br/>";
					$direct_txt.= $episode['ce_pay'].$nm_config['cf_cash_point_unit_ko']."을 사용하여 결제하시겠습니까?<br/>";
					$direct_txt.= "구매한 작품은 내 서재에 보관됩니다.";
					cs_confirm_url($direct_txt, NM_DOMAIN.$_SERVER['REQUEST_URI']."&ce_dt=".$user_token, $comics_url);
					die;
				}
			}
		// }
	}
// }

/* ////////////////// 즐겨찾기 체크 ////////////////////// */
$bookmark_query = "select * from member_comics_mark where mcm_member = '".$nm_member['mb_no']."' and mcm_comics = '".$comics['cm_no']."'";
$count_bookmark = sql_num_rows(sql_query($bookmark_query));


/* ////////////////// 결제 ////////////////// */
if($episode_buy == 'n'){
	/* 화 가격, 무료 화 가격, 시크릿 코드 일치했을때 소장정보로 넘겨준다 0802 */
	if($_epay != '' && $_fpay != '' && $_mysecret != '') {
		if($_mysecret == $user_token) {
			$comics_free = 'n'; // 내 서재 - 화 리스트에서 온 거라면 결제로 넘어가게 설정 0802
			if($nm_member['mb_class'] == "a" || $_SESSION['ss_adm_id'] != "" || $_SESSION['ss_adm_id'] != NULL){ $comics_free = "y"; }
			if($nm_member['mb_class'] == "p"){ 
				// 파트너 자신의 작품에 한해서 무료 170912
				$comics_free = cs_partner_return_y($nm_member, $comics);
			} // end if
		} // end if
	} // end else
}

if($comics_free == 'n'){ 
	/* 소장 화 리스트/일반 화 리스트에 따라 함수 사용 0802 */
	if($_epay != '' && $_fpay != '') {
		$comics_buy = cs_set_expen_myepisode($comics, $episode, $nm_member, $comics_buy, 1, 'y', $_epay, $_fpay);  // 함수 추가 소장 화 리스트에서 왔을 시 0820		

		/* 18-11-23 크리스마스 이벤트 관련 함수 -> event.lib.php */
		if(event_christmas_state_get() != "") {
			event_christmas_point_used_push($nm_member, $comics_buy, event_christmas_state_get());
		} // end if
		
	} else {
		$comics_buy = cs_set_expen_episode($comics, $episode, $nm_member, $comics_buy);  // 북마크포함
		/* 투표 관련 함수 */
		vote_z_event_vote_pay(NM_VOTE_YEAR, $comics['cm_no'], $episode['ce_no'], $comics_buy);
		/* 랜덤박스 관련 함수 */
		if(randombox_state_get() != "") {
			randombox_member_push($nm_member, $comics_buy, randombox_state_get()); // 이벤트 번호 제어 randombox_state_get() 사용 180213

			$randombox_mb = randombox_member_get($nm_member, randombox_state_get()); // 이벤트 번호 제어 randombox_state_get() 사용 180213
			$randombox_mb['erm_cash_point_total'] = ($randombox_mb['erm_cash_point_push'] + ($randombox_mb['erm_point_push'] / 10)) - $randombox_mb['erm_cash_point_pop']; // 결제 땅콩, 뽑기에 쓴 땅콩 계산한 값
		} // end if

		/* 18-11-23 크리스마스 이벤트 관련 함수 -> event.lib.php */
		if(event_christmas_state_get() != "") {
			event_christmas_point_used_push($nm_member, $comics_buy, event_christmas_state_get());
		} // end if
		
	} // end else
}else{
	if($_SESSION['ss_adm_id'] == "" || $_SESSION['ss_adm_id'] == NULL) {
		if(count($mb_get_buy_episode) == 0){ // 소유한 에피소드가 없다면...
			cs_mb_comics_bookmark($comics, $episode, $nm_member, 'y'); // 북마크
		}else{
			cs_mb_comics_bookmark($comics, $episode, $nm_member); // 북마크
		}
	}
}

/* ////////////////// 통계 ////////////////// */
if($state_except == 'n'){ //sales, sales_age_sex, sales_episode_1, sales_episode_1_age_sex
	$comics_buy = cs_set_sales_data($comics, $episode, $nm_member, $comics_buy, $comics_free);
	/* 투표 관련 함수 */
	vote_z_event_vote_comicsview(NM_VOTE_YEAR, $comics['cm_no'], $episode['ce_no']);
	/* 추석 이벤트 조회수 함수 */
	if(get_cookie('thanksgivingtime') == $comics['cm_no']){
		st_stats_event_thanksgiving_data($comics, $episode, 'thanksgivingtime');
	}
}

// 코믹스 에피소드 리스트
$episode_list = cs_episode_list($episode);
$episode_list_count = count($episode_list) -1; //0부터 시작이므로...
$episode_chapter = $episode['ce_chapter'].$d_cm_up_kind[$comics['cm_up_kind']];
if($episode['ce_chapter'] < 0){ 
	$episode_chapter = $episode['ce_notice'];
	if($episode['ce_outer'] == 'y'){
		$episode_chapter = '외전'.$episode['ce_outer_chapter'].$d_cm_up_kind[$comics['cm_up_kind']];
	}
}

// 북마크 링크
$cs_comic_bookmark_url = cs_comic_bookmark_url($comics['cm_no']);

// 내서재 링크
$mycomics_url = NM_URL."/mycomics.php?mbc=2";

// 기본 왼쪽 페이지
$page_slider = 0;
	$left_page = "prev_page";
	$right_page = "next_page";
if($comics['cm_page_way'] == 'r'){ // 오른쪽 페이지일 경우	
	$page_slider = $episode_list_count; 
$left_page = "next_page";
$right_page = "prev_page";
}

// 타이블제목
if($episode['ce_title'] == $comics['cm_series'] && $episode['ce_title'] != ''){
	$head_title = $episode['ce_title']."::".$nm_config['cf_title'];
}else{
	$head_title = $episode['ce_title']."::".$comics['cm_series']."::".$nm_config['cf_title'];
	if($episode['ce_title'] == ''){ $head_title = $comics['cm_series'].$episode_chapter."::".$nm_config['cf_title']; }
}

/* view */
$comicsview = "comicsviewscroll.php";
if($comics['cm_page_way'] == "r" || $comics['cm_page_way'] == "l"){ $comicsview = "comicsviewpage.php"; }
$nm_config['footer'] = false; /* 하단 사용안함 */

$ondragstart = true; // 드래그 방지
$onselectstart = true; // 선택 방지

// SNS 썸네일
$head_thumbnail = NM_IMG.$episode['ce_cover'];
if($episode['ce_cover'] == ''){ $head_thumbnail = NM_IMG.$comics['cm_cover']; }

// 고정 Footer 용(Mobile) - 활성화가 Default 171011
$prev_disabled = "";
$prev_btn_class = "active";
$next_disabled = "";
$next_btn_class = "active";

include_once($nm_config['nm_path']."/".$comicsview);

?>