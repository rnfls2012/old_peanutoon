<?php

namespace Classes;

class Push
{
    const MAKE_CHECKSUM_ERROR_CODE = 2001;
    const SEND_PUSH_MSG_ERROR_CODE = 2002;
    private $url;

    public function __construct($targetUrl) {
        $this->url = $targetUrl;
    }

    public function sendPushMsg($pushData) {

        $checksum = null;
        $timestamp = time();

        try {
            $checksum = $this->makeChecksum($pushData);
        } catch (\Exception $exception) {
            echo 'Occur Error. Error Message / Error Code : '.$exception->getMessage().'/'.$exception->getCode();
            die;
        }

        if ( is_array($pushData) ) {
            $pushData['timestamp'] = $timestamp;
            $pushData['checksum'] = $checksum;
            $jsonData = json_encode($pushData);
        } elseif ( $this->isJSON($pushData) ) {
            $push_array = json_decode($pushData, TRUE);
            $push_array['timestamp'] = $timestamp;
            $push_array['checksum'] = $checksum;
            $pushData = json_encode($push_array);
            $jsonData = $pushData;
        } else {
            throw new \Exception('Data type is wrong', self::SEND_PUSH_MSG_ERROR_CODE);
        }

        $header = array(
            'Content-Type: application/json'
        );

        $curl = curl_init();
        curl_setopt( $curl, CURLOPT_URL, $this->url);
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $curl, CURLOPT_HTTPHEADER, $header );

        curl_setopt( $curl, CURLOPT_POST, count($jsonData) );
        curl_setopt( $curl, CURLOPT_POSTFIELDS, $jsonData );

        $result['info'] = curl_exec($curl);
        $result['detail'] = curl_getinfo($curl);

        return $result;
    }

    private function isJSON($string, $return_data = false) {
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    }

    private function makeChecksum($data) {

        $mixed_value = '';

        if ( is_array($data) ) {
            foreach ($data as $key => $value) {
                $mixed_value .= $value;
            }
        } elseif ( $this->isJSON($data) ) {
            $array_value = json_decode($data);

            foreach ($array_value as $key => $value) {
                $mixed_value .= $value;
            }
        } else {
            throw new \Exception('Check data type.', self::MAKE_CHECKSUM_ERROR_CODE);
        }

        $checksum = sha1($mixed_value);

        return $checksum;
    }
}