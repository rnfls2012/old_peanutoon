<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/eventfamily.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/eventfamily.js<?=vs_para();?>"></script>
		<style type="text/css">
			.evt-content-wrap {
				background-image: url(<?=$family_img?>/bg03.png<?=vs_para();?>), url(<?=$family_img?>/bg02.png<?=vs_para();?>), url(<?=$family_img?>/bg01.png<?=vs_para();?>);
				background-size: 20%, 20%, 100%;
				background-position: right bottom, left bottom, top;
				background-repeat: no-repeat;
			}

			.evt-link {
				background-color: #fff9ec;
				background-image:url(<?=$family_img?>/bg04.png<?=vs_para();?>);
				background-size: 100%;
				background-position: center top;
				background-repeat: no-repeat;
			}

			.ModalPopup {
				background-image: url(<?=$family_img?>/bg12.png<?=vs_para();?>), url(<?=$family_img?>/bg11.png<?=vs_para();?>);
				background-repeat: no-repeat,repeat-x;
				background-size: auto;
			}

			.ModalPopup .p_content {
				background-image: url(<?=$family_img?>/bg13.png<?=vs_para();?>);
				background-size: 100%;
				background-repeat: no-repeat;
				background-position: center;
			}


		</style>
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<input type="hidden" id="member_no" class="member_no" value="<?=$nm_member['mb_no'];?>" />
		<input type="hidden" id="total_coupon" class="total_coupon" value="<?=$total_coupon;?>" />

		<div class="evt-container">
			<!-- Modal Popup -->
			<div class="Modalalert">
				<div class="ModalBackground"></div>
				<div class="ModalPopup">
					<div class="ModalClose"><i class="material-icons">clear</i></div>
					<div class="p_content">
						<div class="p_coupon">
							<img src="<?=$family_img?>/p_title.png<?=vs_para();?>" alt="15% 결제 할인 쿠폰">
						</div>
						<div class="p_get">
							<img src="<?=$family_img?>/p_warn.png<?=vs_para();?>" alt="쿠폰 사용 유의사항">
						</div>
						<ul>
							<li><button id="recharge_link" class="p_buy" onclick="location.href='<?=NM_URL?>/recharge.php'">결제하기</button></li>
							<li><button class="p_couponpage" onclick="location.href='<?=NM_URL?>/coupon.php'">쿠폰 보기</button></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /Modal Popup -->
			<div class="evt-header">
				<img src="<?=$family_img?>/title.png<?=vs_para();?>" alt="피너툰 패밀리 이벤트" />
			</div>
			<div class="evt-content-wrap">
				<div class="evt-content">
					<div class="coupon">
						<div class="couponimg">
							<img src="<?=$family_img?>/coupon01.png<?=vs_para();?>" alt="땅콩 결제 15% 쿠폰" />
						</div>
						<div class="couponbadge">
							<img src="<?=$family_img?>/coupon_badge.png<?=vs_para();?>" alt="선착순 200명">
						</div>
						<div class="couponpara">
							남은 수량 : <span><?=$total_coupon;?></span>장
						</div>
						<div class="couponget">
							<button id="open_btn" class="<?=$nochk;?>"><?=$coupon_off_text;?></button>
						</div>
					</div>
					<img src="<?=$family_img?>/c1_info.png<?=vs_para();?>" alt="1인 1인 1회에 한하여 쿠폰발급이 가능합니다. ">
				</div>
			</div>
			<div class="evt-link">
				<div class="evt-info"><img src="<?=$family_img?>/c2_info.png<?=vs_para();?>" alt="가정의 달에 어울리는 따스한 추천 작품"></div>
				<a href="<?=get_comics_url(2024);?>">
					<img src="<?=$family_img?>/c2_link01.png<?=vs_para();?>" alt="호식이 이야기">
				</a>
				<a href="<?=get_comics_url(996);?>">
					<img src="<?=$family_img?>/c2_link02.png<?=vs_para();?>" alt="우리집 신령님">
				</a>
				<a href="<?=get_comics_url(2080);?>">
					<img src="<?=$family_img?>/c2_link03.png<?=vs_para();?>" alt="가족이 되자">
				</a>
				<a href="<?=get_comics_url(2491);?>">
					<img src="<?=$family_img?>/c2_link04.png<?=vs_para();?>" alt="다녀왔어, 어서 와">
				</a>
				<a href="<?=get_comics_url(1098);?>">
					<img src="<?=$family_img?>/c2_link05.png<?=vs_para();?>" alt="19살에 아빠랑 엄마!?">
				</a>
			</div>
			<div class="evt-warn"><img src="<?=$family_img?>/c3_warn.png<?=vs_para();?>" alt="이벤트 주의사항"></div>