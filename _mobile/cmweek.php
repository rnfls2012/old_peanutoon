<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<? include NM_MO_PART_PATH.'/event.php';		// [mobile] 서브슬라이드?>

			<? include NM_MO_PART_PATH.'/banner.php';		// [mobile] 배너 스크롤?>

			<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/cmweek.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_MO_URL;?>/js/cmweek.js<?=vs_para();?>"></script>
			<!-- 작품리스트 -->
			<div id="cmweek" class="container_bg">
				<div class="sub_list clear" data-limit_define="<?=NM_SUB_LIST_LIMIT;?>" data-limit="<?=$sub_list_limit;?>" data-total="<?=$row_total;?>" data-url="<?=$_SERVER['REQUEST_URI'];?>" data-menu="<?=$_menu;?>" data-bool="<?=$sub_list_bool;?>"> 
					<? foreach($cmweek_arr as $cmweek_key => $cmweek_val){ 
					// 마오랑 요청 - 원조교제 아닌데요에 금요베스트 넣기
					$cmweek_best_txt = "";
						if($sql_cmweek_field_ck != "" & $cmweek_val['cmweek_field_ck'] == 0 && $cmweek_val['cm_public_cycle'] > 0 && $cmweek_val['cm_end'] == 'n'){
							$cmweek_best_txt = $cmweek_val['cm_week_txt']."요베스트";
						}
					// 마오랑 요청 - 원조교제 아닌데요에 금요베스트 넣기 end
					?>
					<a href="<?=$cmweek_val['cm_serial_url']?>" target="_self" data-week="<?=$cmweek_val['cm_week_txt']?>">
						<div class="sub_list_thumb">
							<div class="icon_view">
								<?=$cmweek_val['cm_free_mark'];?>
								<?=$cmweek_val['cm_sale_mark'];?>
							</div>
							<div class="img_view">
								<img class="<?=$date_up_class;?>" src="<?=$cmweek_val['cm_cover_sub']?>" alt="<?=$cmweek_val['cm_series']?>" />
								<div class="icon_view_bottom">
									<?=$cmweek_val['cm_adult_mark'];?>
									<?=$cmweek_val['cm_up_icon'];?>
									<?=$cmweek_val['cm_new_icon'];?>
								</div>
							</div>
							<dl>
								<dt class="ellipsis">
									<span class="cm_title"><?=str_replace("[웹툰판]", "", $cmweek_val['cm_series']);?></span>
								</dt>
								<dd class="ellipsis"><?=$cmweek_val['cm_small_span']?><?=$cmweek_val['cm_end_span']?><?=$cmweek_best_txt;?></dd>
								<dd class="ellipsis"><?=$cmweek_val['cm_professional_info']?></dd>
							</dl>
						</div>
					</a>
					<? } /* foreach($cmweek_arr as $cmweek_key => $cmweek_val){  */ ?>
				</div>
			</div>

			<? // [mobile] webtoon 신작	
			if($_menu != 'all'){ include NM_MO_PART_PATH.'/cmweek_new.php'; } ?>