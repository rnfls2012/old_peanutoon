<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<? include NM_MO_PART_PATH.'/event.php';		// [mobile] 서브슬라이드?>

			<? include NM_MO_PART_PATH.'/banner.php';		// [mobile] 배너 스크롤?>

			<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/cmnew.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_MO_URL;?>/js/cmnew.js<?=vs_para();?>"></script>
			<!-- 작품리스트 -->
			<div id="cmnew" class="container_bg">
				<div class="sub_list clear" data-limit_define="<?=NM_SUB_LIST_LIMIT;?>" data-limit="<?=$sub_list_limit;?>" data-total="<?=$row_total;?>" data-url="<?=$_SERVER['REQUEST_URI'];?>" data-menu="<?=$_menu;?>" data-bool="<?=$sub_list_bool;?>"> 
					<? foreach($cmnew_arr as $cmnew_key => $cmnew_val){ ?>
					<a href="<?=$cmnew_val['cm_serial_url']?>" target="_self" data-week="<?=$cmnew_val['cm_week_txt']?>">
						<div class="sub_list_thumb">
							<div class="icon_view">
								<?=$cmnew_val['cm_free_mark'];?>
								<?=$cmnew_val['cm_sale_mark'];?>
							</div>
							<div class="img_view">
								<img class="<?=$date_up_class;?>" src="<?=$cmnew_val['cm_cover_sub']?>" alt="<?=$cmnew_val['cm_series']?>" />
								<div class="icon_view_bottom">
									<?=$cmnew_val['cm_adult_mark'];?>
									<?=$cmnew_val['cm_up_icon'];?>
									<?=$cmnew_val['cm_new_icon'];?>
								</div>
							</div>
							<dl>
								<dt class="ellipsis">
									<span class="cm_title"><?=str_replace("[웹툰판]", "", $cmnew_val['cm_series']);?></span>
								</dt>
								<dd class="ellipsis"><?=$cmnew_val['cm_small_span']?><?=$cmnew_val['cm_end_span']?></dd>
								<dd class="ellipsis"><?=$cmnew_val['cm_professional_info']?></dd>
							</dl>
						</div>
					</a>
					<? } /* foreach($cmnew_arr as $cmnew_key => $cmnew_val){  */ ?>
				</div>
			</div>