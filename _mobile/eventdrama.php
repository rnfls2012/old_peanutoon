<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<style type="text/css">
			.evt-container {
				position: relative;
				width: 100%;
				background-color: #ea284d;
			}
			.evt-container img {
				width: 100%;
				height: auto;
			}
			.evt-header {
				position: relative;
				width: 100%;
			}
			.evt-content {
				position: relative;
				width: 100%;
			}
		</style>

		<div class="evt-container">
			<div class="evt-header">
				<img src="<?=$drama_img?>/title.png<?=vs_para();?>" alt="피너툰 드라마 데이" />
			</div>
			<div class="evt-content">
				<div class="evt-link">
					<a href="<?=get_comics_url(3027);?>">
						<img src="<?=$drama_img?>/link01.png<?=vs_para();?>" alt="7의잔재">
					</a>
					<a href="<?=get_comics_url(2991);?>">
						<img src="<?=$drama_img?>/link02.png<?=vs_para();?>" alt="도시괴담">
					</a>
					<a href="<?=get_comics_url(1827);?>">
						<img src="<?=$drama_img?>/link03.png<?=vs_para();?>" alt="뿔통-무간천하">
					</a>
					<a href="<?=get_comics_url(1655);?>">
						<img src="<?=$drama_img?>/link04.png<?=vs_para();?>" alt="좀비플래너">
					</a>
					<a href="<?=get_comics_url(2599);?>">
						<img src="<?=$drama_img?>/link05.png<?=vs_para();?>" alt="체어맨">
					</a>
				</div>
			</div>

			<div class="evt-warn"><img src="<?=$drama_img?>/warn.png<?=vs_para();?>" alt="이벤트 주의사항"></div>
		</div>