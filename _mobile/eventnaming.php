<?
include_once '_common.php'; // 공통
$naming_img = NM_IMG.'_mobile/event/2017/naming';
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/event.css<?=vs_para();?>" />
		<style type="text/css">
			#evtcontainer {
				background:url(<?=$naming_img;?>/evt02_bg.jpg) no-repeat;
				background-size: cover;
				width: 100%;
				position: relative;
				outline: none;
			}
			#evtcontainer .evtcontents { width: 100%; background:url(<?=$naming_img;?>/evt02_bg.jpg) no-repeat; background-size: 100%; }
			#evtcontainer .crt_guide { margin-top: 10px; padding: 20px 30px; width: 100%; background:url(<?=$naming_img;?>/evt02_con03_bar.png) center top no-repeat; background-size: 100%; text-align: left; }
		</style>
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/event.js<?=vs_para();?>"></script>
			<div id="evtcontainer">
				<div class="evtcontents">
					<div class="evt_title">
						<img src="<?=$naming_img;?>/evt02_header.png" alt="피너툰 캐릭터 이름짓기 대잔치" />
					</div>

					<div class="evt_con01">
						<img src="<?=$naming_img;?>/evt02_con01.png" />
					</div>

					<div class="evt_con02">
						<ul>
							<li><img src="<?=$naming_img;?>/evt02_pre01.png" alt="금상 3명 - 문화상품권 6만원권"></li>
							<li><img src="<?=$naming_img;?>/evt02_pre02.png" alt="은상 6명 - 문화상품권 3만원권"></li>
							<li><img src="<?=$naming_img;?>/evt02_pre03.png" alt="동상 15명 - 문화상품권 1만원권"></li>
						</ul>
					</div>

					<form name="naming_event" id="naming_event" method="post" action="<?=NM_PROC_URL;?>/eventnaming.php" onsubmit="return eventnaming_submit();">
					<div class="evt_con03">
						<ul>
							<li>
								<div class="crt_img"><img src="<?=$naming_img;?>/evt02_con02_squall.png" alt="다람쥐" /></div>
								<div class="crt_name"><img src="<?=$naming_img;?>/evt02_con02_squall02.png" alt="다람쥐" /></div>
								<div class="crt_exp">
									땅콩을 좋아하는 무늬다람쥐로 취미는 땅콩 낚시.
								</div>
								<div class="crt_text">
									<input type="text" name="squirrel_name" placeholder="다람쥐의 이름을 지어주세요!" maxlength="10"/>
								</div>
							</li>

							<li>
								<div class="crt_img"><img src="<?=$naming_img;?>/evt02_con02_bpeanut.png" alt="큰땅콩" /></div>
								<div class="crt_name"><img src="<?=$naming_img;?>/evt02_con02_bpeanut02.png" alt="큰땅콩" /></div>
								<div class="crt_exp">
									땅콩껍질을 모자 삼아 쓰고 다니는 멋쟁이 어른 땅콩.
								</div>
								<div class="crt_text">
									<input type="text" name="peanut_name" placeholder="큰 땅콩의 이름을 지어주세요!" maxlength="10"/>
								</div>
							</li>

							<li>
								<div class="crt_img"><img src="<?=$naming_img;?>/evt02_con02_speanut.png" alt="작은 땅콩" /></div>
								<div class="crt_name"><img src="<?=$naming_img;?>/evt02_con02_speanut02.png" alt="작은 땅콩" /></div>
								<div class="crt_exp">
									큰 땅콩의 동생으로 얼른 어른이 되고 싶어하는 꼬마 땅콩
								</div>
								<div class="crt_text">
									<input type="text" name="mini_peanut_name" placeholder="작은 땅콩의 이름을 지어주세요!" maxlength="10"/>
								</div>
							</li>
						</ul>

					</div>

					<div class="evt_con04">
						<button>전송하기</button>
					</div>
					</form>

					<div class="evt_con05">
						<img src="<?=$naming_img;?>/evt02_con03_txt.png" alt="안내사항" />
						<div class="crt_guide">
							<ol>
								<li>응모기간 : 7월 18일 ~ 30일</li>
								<li>당선작 발표일 : 7월 31일</li>
								<li>당선작 - 금상 :  각 캐릭터 당 1명씩 총 3명 <br /> <span>은상 : 각 캐릭터 당 2명씩 총 6명</span> <br /> <span> 동상 : 각 캐릭터 당 5명씩 총 15명</span></li>
								<li>동일한 이름 응모 시 우선 등록된 순서로 선정</li>
							</ol>
						</div>
					</div>
				</div>
			</div>