<? include_once '_common.php'; // 공통 ?>
		<?
		/* input */
		$temp_login_id = $temp_login_pw =  "";
		/* 쿠키 */
		$temp_login_id = get_cookie('ck_mb_id');

		/* 로그인세션 */
		if($_SESSION['ss_login_id'] != ''){ $temp_login_id = $_SESSION['ss_login_id']; }
		$temp_login_pw = $_SESSION['ss_login_pw'];
		$state = $_SESSION['ss_login_state'];
		switch($state) {
			case 1:
				$msg = "아이디 또는 비밀번호를 입력하세요.";
				break;
			case 2:
				$msg = "중복된 아이디 입니다. 관리자에게 메일문의하세요.";
				break;
			case 3:
				$msg = "아이디 또는 비밀번호가 일치하지 않습니다!";
				break;
			case 4:
				$msg = "이용이 제한된 아이디입니다. 관리자에게 문의하세요.";
				break;
			case 5:
				$msg = "탈퇴 계정입니다. 관리자에게 문의하세요.";
				break;
			case 6:
				$msg = "휴면 계정입니다. 관리자에게 문의하세요.";
				break;
			case 7:
				$msg = "입력하신 아이디가 없거나 일치하지 않습니다.";
				break;
			default:
				$msg = "아이디 또는 비밀번호가 일치하지 않습니다!";
				break;
		} // end switch

		/* checkbox */
		$temp_login_id_save = $temp_login_auto_save = "";
		if(get_cookie('ck_mb_id')){ $temp_login_id_save = "checked"; }
		if(get_cookie('ck_mb_auto')){ $temp_login_auto_save = "checked"; }

		/* label class  */
		$login_id_completed = $login_pw_completed = "";
		$form_field_class = "hidden";
		if($temp_login_id != ''){ $login_id_completed = "completed"; $form_field_class = ""; }
		if($temp_login_pw != ''){ $login_pw_completed = "completed"; $form_field_class = ""; }


		// $social_oauth_url = NM_OAUTH_URL.'/login.php?service=';
		// $twiter_oauth_url = NM_OAUTH_URL.'/twitterlogin.php';

		/* SNS URL- 18-12-03 수정 */		
		$social_oauth_url = NM_OAUTH_URL.'/login.php?'.get_add_para_redirect().'&service=';
		$twiter_oauth_url = NM_OAUTH_URL.'/twitterlogin.php?'.get_add_para_redirect();
		?>

		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/ct.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/ct.js<?=vs_para();?>"></script>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<div class="login_box">
				<h5><?=$nm_config['cf_title'];?> 로그인</h5>
				<fieldset>
					<form name="ctlogin_form" id="ctlogin_form" method="post" action="<?=NM_PROC_URL;?>/ctlogin.php" onsubmit="return ctlogin_submit();">
						<input type="hidden" name="http_referer" value="<?=$cookie_http_referer;?>"/>
						<input type="hidden" name="redirect" value="<?=$_redirect;?>"/>
						<input type="hidden" name="adult_yn" value="<?=$_adult?>"/>
						<input type="hidden" name="mb_token" value="<?=get_js_cookie('mb_token');?>"/>

						<div class="form_field">
							<input type="text" class="form_field_text" id="login_id" name="login_id" value="<?=$temp_login_id?>">
							<label for="login_id" class="<?=$login_id_completed?>">아이디</label>
							<span class="error">이메일 양식인 아이디를 입력해 주세요.</span>
							</div>
						<div class="form_field">
							<input type="password" class="form_field_text" id="login_pw" name="login_pw" value="<?=$temp_login_pw?>">
							<label for="login_pw" class="<?=$login_pw_completed?>">패스워드</label>
							<span class="fail <?=$form_field_class;?>"><?=$msg;?></span>
						</div>
						<div class="form_save clear">
							<input type="checkbox" id="login_id_save" name="login_id_save" value="y" <?=$temp_login_id_save?>>
							<label for="login_id_save">아이디 저장</label>
							<input type="checkbox" id="login_auto" name="login_auto_save" value="y" <?=$temp_login_auto_save?>>
							<label for="login_auto">로그인 상태 유지</label>
						</div>
						<div class="form_submit">
							<input type="submit" id="login_submit" name="login_submit" value="로그인">
						</div>
					</form>
				</fieldset>
				<div class="login_legi">
					<a href="<?=NM_URL;?>/ctmyfind.php?<?=get_add_para_redirect();?>">아이디·비밀번호찾기</a>  /  <a href="<?=NM_URL;?>/ctjoin.php?<?=get_add_para_redirect();?>"><span>회원가입</span></a>
				</div>

				<div class="login_or"> <!-- 다른곳 아이디로 로그인 -->
					<ul>
						<li><a href="<?=$social_oauth_url?>facebook"><i class="fa fa-facebook" aria-hidden="true"></i> 페이스북 아이디로 로그인</a></li>
						<li><a href="<?=$twiter_oauth_url?>"><i class="fa fa-twitter" aria-hidden="true"></i> 트위터 아이디로 로그인</a></li>
						<li><a href="<?=$social_oauth_url?>kakao"><img src="<?=NM_MO_IMG?>ct/login_kakao.png" alt="카카오 로고" /> 카카오 아이디로 로그인</a></li>
						<li><a href="<?=$social_oauth_url?>naver"><img src="<?=NM_MO_IMG?>ct/login_naver.png" alt="네이버 로고" /> 네이버 아이디로 로그인</a></li>
						<!--<li><a href="<?=$social_oauth_url?>google"><img src="<?=NM_MO_IMG?>ct/login_naver.png" alt="네이버 로고" /> 구글 아이디로 로그인</a></li>-->
					</ul>
				</div>

				<div class="login_help">
					문제가 있거나 궁금한 점이 있으시면<br />
					<a href="mailto:<?=$nm_config['cf_admin_email'];?>"><?=$nm_config['cf_admin_email'];?></a> 으로 문의주시기 바랍니다.
				</div>
			</div>
