<?
include_once '_common.php'; // 공통
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/my.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_MO_URL;?>/js/my.js<?=vs_para();?>"></script>

			<? 
			//삭제기능
			if(count($member_buy_arr) > 0) {
			?>
				<!-- 삭제하기 메뉴 -->
				<div class="lib_del"> 
					<div class="lib_del_chk">
						<div class="lib_chk">
							<input type="checkbox" id="lib_delete" name="all_radio" />
							<label for="lib_delete"></label>
						</div>
						<label for="lib_delete"><span>전체선택</span></label>
					</div>

					<!-- 취소버튼 // 삭제모드시 보임 -->
					<button type="button" class="lib_del_btn"><i class="fa fa-trash" aria-hidden="true"></i> 삭제</button>
					<button type="button" class="lib_cancel_btn">취소</button>
				</div>
			
				<form name="mycomics" id="mycomics" method="post" action="<?=NM_PROC_URL;?>/mycomics.php" onsubmit="return mycomics_submit();">
					<input type="hidden" id="mode" name="mode" value="recent">
					<div class="lib_con">
						<? 
						foreach($member_buy_arr as $key => $val) { 
							$img_url = img_url_para($val['cm_cover_sub'], $val['cm_reg_date'], $val['cm_mod_date'], 200, 200); 
						?>
							 <!-- 썸네일 리스트 -->
							<div class="lib_list">
								<a href="<?=get_comics_url($val['cm_no']);?>"> <!-- <-이거 클릭했을때 체크박스도 클릭되어야해요!! -->
									<div class="lib_chk">

										<!-- 삭제 체크박스 -->
										<input type="checkbox" id="lib_delete_<?=$key;?>" name="comics_check[]" value="<?=$val['cm_no'];?>"/>
										<label for="lib_delete_<?=$key;?>"></label>
									</div>
									<dl>
										<dt>
											<img src="<?=$img_url;?>" alt="<?=$val['cm_series'];?>" />
											<div class="icon_up"><img src="<?=NM_MO_IMG?>common/icon_up.png" alt="update" /></div> <!-- update -->
										</dt>
										<dt><?=$val['cm_series'];?></dt>
										<dd><?=$val['cm_professional_info'];?></dd>
										<dd><?=substr($val['mbc_date'], 0, 10);?></dd>
									</dl>
								</a>
							</div> <!-- / 썸네일 리스트 -->
						<? } // end foreach ?>
					</div>
				</form>
			<? } else { ?>
				<div class="lib_no_data" style="text-align: center;">최근 본 목록이 없습니다.</div>
			<? } // end else ?>