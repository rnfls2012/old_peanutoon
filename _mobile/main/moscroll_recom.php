<? include_once '_common.php'; // 공통

$iscroll_url = NM_URL."/cmgenre.php?menu=5";

/* DB */
$iscroll_arr = array();
$sql_iscroll_adult = sql_adult($mb_adult_permission , 'cm_adult');
$sql_iscroll = "SELECT *, if(cm_episode_date > cm_reg_date,cm_episode_date,cm_reg_date) as cm_date
						FROM comics c left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional
						WHERE 1 AND cm_service = 'y' AND cm_type = '1' $sql_iscroll_adult
						group by cm_no 
						ORDER BY cm_date DESC LIMIT 0 , 10";

$iscroll_arr = cs_comics_content_list($sql_iscroll, substr($nm_config['nm_path'], -1), 'cm_cover_sub', 'tn222x222');

/* 랜덤으로 섞기 */
shuffle($iscroll_arr);

?>
<? if(count($iscroll_arr) > 0){ ?>
<!-- body space -->
	<!-- wrapper space -->
			<!-- container_bg -->
				<link rel="stylesheet" type="text/css" href="<?=NM_MO_MAIN_URL;?>/css/moscroll_recom.css<?=vs_para();?>" />
				<script type="text/javascript" src="<?=NM_MO_MAIN_URL;?>/js/moscroll_recom.js<?=vs_para();?>"></script>
				<div id="moscroll_recom" class="container_bg">
					<div class="moscroll_list clear">
						<h2>추천 작품</h2>
						<span class="moreread"><a href="<?=$iscroll_url?>">더보기</a></span>
						<div id="moscroll<?=$iscroll_no?>"  class="moscroll_bg clear">
							<div>
								<ul>
								<?php
									foreach($iscroll_arr as $iscroll_key => $iscroll_val){ ?>
										<li>
											<div>
												<a href="<?=$iscroll_val['cm_serial_url'];?>" target="_self">
													<dl> <!-- 업데이트 시 class="update" -->
														<dt><!-- 180511 수정 -->
															<div class="icon_view">
																<?=$iscroll_val['cm_free_mark'];?>
																<?=$iscroll_val['cm_sale_mark'];?>
															</div>
															<div class="img_view">
																<!-- <?=$date_up_img_url;?> -->
																<img class="<?=$date_up_class;?>" src="<?=$iscroll_val['cm_cover_sub']?>" alt="<?=$iscroll_val['cm_series']?>" />
																<div class="icon_view_bottom">
																	<?=$iscroll_val['cm_adult_mark'];?>
																	<?=$iscroll_val['cm_up_icon'];?>
																	<?=$iscroll_val['cm_new_icon'];?>
																</div>
															</div>
														</dt>
														<dt class="series ellipsis"><?=str_replace("[웹툰판]", "", $iscroll_val['cm_series']);?></dt>
														<!-- 장르, 완결 표시. 아이콘은 today.css랑 똑같이 넣어주세요. --> <dd class="ellipsis"><?=$iscroll_val['cm_small_span']?></dd>
														<dd class="professional ellipsis"><?=$iscroll_val['cm_professional_info']?></dd>
													</dl>
												</a>
											</div>
										</li>
								<? } ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
<? } ?>