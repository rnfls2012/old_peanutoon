<? include_once '_common.php'; // 공통

$iscroll_url = NM_URL."/cmweek.php";

/* DB */
$iscroll_arr = array();
$sql_iscroll_adult = sql_adult($mb_adult_permission , 'cm_adult');

/*
$sql_iscroll = "SELECT *, if(cm_episode_date > cm_reg_date,cm_episode_date,cm_reg_date) as cm_date
				FROM comics c left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional
				WHERE 1 AND cm_service = 'y' $sql_iscroll_adult
				group by cm_no
				having cm_date >= '".NM_TIME_M14." ".NM_TIME_HI_start."'
				AND cm_episode_date <= '".NM_TIME_YMD." ".NM_TIME_HI_end."'
				ORDER BY cm_episode_date DESC LIMIT 0 , 10";

$iscroll_arr = cs_comics_content_list($sql_iscroll, substr($nm_config['nm_path'], -1), 'cm_cover_sub', 'tn222x222');

// 랜덤으로 섞기
shuffle($iscroll_arr);
*/

/* 가져오기 */

/* DB */

$iscroll_arr = array();
$sql_cm_adult = sql_adult($mb_adult_permission , 'cm.cm_adult');

// 총갯수
$sql_iscroll_total = "
 SELECT count(*) as total FROM comics_ranking_sales_cmnew_auto c 
 WHERE 1 
 ; ";
$row_total = sql_count($sql_iscroll_total, 'total');


// 익스플로러일때 전체 출력-임시
if(substr(get_brow(HTTP_USER_AGENT), 0, 4) == "MSIE"){ $sub_list_limit = $row_total; }
$_menu = "0"; // 단일 메뉴일 경우(서브메뉴x)

// cm_reg_date_new 아직 사용 안하지만 나중에 사용할수 있음

// 특정 만화 순서 앞으로...18-04-25
$week_comicno_arr[0] = array(); // 배열선언
$sql_cms_adult = sql_adult($mb_adult_permission , 'cms_adult');;
$sql_cms_class = " AND cms_class = '".NM_TIME_W."' "; 
if($sql_cms_adult !=''){ $sql_cms_class = " AND cms_class = '".NM_TIME_W."_t' ";  }

$sql_cms = " select * from comics_main_slide_moscroll where 1 $sql_cms_adult $sql_cms_class order by cms_class, cms_ranking ";
$result_cms = sql_query($sql_cms);
while ($row_cms = sql_fetch_array($result_cms)) {
	$cms_class_key = intval(str_replace("_t", "", $row_cms['cms_class']));
	array_push($week_comicno_arr[0], $row_cms['cms_comics']);
}
$week_comicno_order = $week_comicno_arr[0];
$sql_week_comicno_field = "";
$sql_week_comicno_order = "";

$sql_week_comicno_where = ""; // 18-05-14 마오랑대표&부사장님지시사항

if(count($week_comicno_order) > 0){
	$week_comicno_order_arr = $week_comicno_order;

	$sql_week_comicno_field = " CASE ";
	foreach($week_comicno_order_arr as $week_comicno_order_key => $week_comicno_order_val){
		$sql_then = 99 - intval($week_comicno_order_key);
		$sql_week_comicno_field.= " WHEN c.cm_no = '".$week_comicno_order_val."' THEN ".$sql_then." ";
	}
	$sql_week_comicno_field.= "ELSE 0 ";
	$sql_week_comicno_field.= "END AS cm_no_special, ";

	$sql_week_comicno_order = "cm_no_special DESC, ";

	$sql_week_comicno_where = " OR c.cm_no IN (".implode(", ",$week_comicno_order).") "; // 18-05-14 마오랑대표&부사장님지시사항
}

/* 클래스 */
$sql_cms_class_val = NM_TIME_W;
if($sql_cms_adult !=''){ $sql_cms_class_val = NM_TIME_W.'_t'; }
$sql_cms_class = " AND crs.crs_class = '".$sql_cms_class_val."' ";

$sql_iscroll = " SELECT c.*, crs.crs_ranking, 
					".$sql_week_comicno_field."
					c_prof.cp_name as prof_name 
				FROM comics_ranking_sales_main_slide_moscroll_auto crs 
				left JOIN comics c ON crs.crs_comics = c.cm_no 				 
				left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
				WHERE 1 $sql_cms_class 
				$sql_week_comicno_where 	
				GROUP BY c.cm_no 			   
				ORDER BY ".$sql_week_comicno_order." 
				crs.crs_ranking ASC  
				LIMIT 0, 10 ";
// 리스트 가져오기
$iscroll_arr = cs_comics_content_list($sql_iscroll, substr($nm_config['nm_path'], -1), 'cm_cover_sub', 'tn222x222');

/* 가져오기 끝 */



?>
<? if(count($iscroll_arr) > 0){ ?>
<!-- body space -->
	<!-- wrapper space -->
			<!-- container_bg -->
				<link rel="stylesheet" type="text/css" href="<?=NM_MO_MAIN_URL;?>/css/moscroll.css<?=vs_para();?>" />
				<script type="text/javascript" src="<?=NM_MO_MAIN_URL;?>/js/moscroll.js<?=vs_para();?>"></script>
				<div id="mo_moscroll" class="container_bg">
					<div class="moscroll_list clear">
						<h2>업데이트 작품</h2>
						<span class="moreread"><a href="<?=$iscroll_url?>">더보기</a></span>
						<div id="moscroll<?=$iscroll_no?>"  class="moscroll_bg clear">
							<div>
								<ul>
								<?php
									foreach($iscroll_arr as $iscroll_key => $iscroll_val){ ?>
										<li>
											<div>
												<a href="<?=$iscroll_val['cm_serial_url'];?>" target="_self">
													<dl> <!-- 업데이트 시 class="update" -->
														<dt><!-- 180511 수정 -->
															<div class="icon_view">
																<?=$iscroll_val['cm_free_mark'];?>
																<?=$iscroll_val['cm_sale_mark'];?>
															</div>
															<div class="img_view">
																<!-- <?=$date_up_img_url;?> -->
																<img class="<?=$date_up_class;?>" src="<?=$iscroll_val['cm_cover_sub']?>" alt="<?=$iscroll_val['cm_series']?>" />
																<div class="icon_view_bottom">
																	<?=$iscroll_val['cm_adult_mark'];?>
																	<?=$iscroll_val['cm_up_icon'];?>
																	<?=$iscroll_val['cm_new_icon'];?>
																</div>
															</div>
														</dt>
														<dt class="series ellipsis"><?=str_replace("[웹툰판]", "", $iscroll_val['cm_series']);?></dt>
														<!-- 장르, 완결 표시. 아이콘은 today.css랑 똑같이 넣어주세요. --> <dd class="ellipsis"><?=$iscroll_val['cm_small_span']?></dd>
														<dd class="professional ellipsis"><?=$iscroll_val['cm_professional_info']?></dd>
													</dl>
												</a>
											</div>
										</li>
								<? } ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
<? } ?>