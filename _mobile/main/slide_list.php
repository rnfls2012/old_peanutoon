<? include_once '_common.php'; // 공통 

/* DB */
/*
$ranking_arr = array();
$d_cr_class_arr = array();
if($mb_adult_permission == 'y'){ $d_cr_class_arr = $d_cr_class; }
else{$d_cr_class_arr = $d_cr_t_class;}

$ranking_arr_count = 0;
foreach($d_cr_class_arr as $cr_class_key => $cr_class_val){	
	$ranking_limit = 0;
	${'sql_ranking_big_'.$cr_class_key} = "";
	${'sql_ranking_comics_'.$cr_class_key} = "";
	${'ranking_comics_'.$cr_class_key} = array();
	${'ranking_arr_'.$cr_class_key} = array();

	${'sql_ranking_'.$cr_class_key} = " SELECT * FROM `comics_ranking` WHERE cr_class='$cr_class_key' ORDER BY cr_ranking ASC LIMIT 5;";
	${'result_ranking_'.$cr_class_key} = sql_query(${'sql_ranking_'.$cr_class_key});
	${'row_size_'.$cr_class_key} = sql_num_rows(${'result_ranking_'.$cr_class_key});
	if(${'row_size_'.$cr_class_key} == 0){ continue; }
	while (${'row_ranking_'.$cr_class_key} = sql_fetch_array(${'result_ranking_'.$cr_class_key})) {
		array_push(${'ranking_arr_'.$cr_class_key},  ${'row_ranking_'.$cr_class_key});
		array_push(${'ranking_comics_'.$cr_class_key},  ${'row_ranking_'.$cr_class_key}['cr_comics']);
		if($ranking_arr_count != 0){
			${'sql_ranking_big_'.$cr_class_key} = " AND cm_big = '".${'row_ranking_'.$cr_class_key}['cr_big']."' ";
		}
	}
	// 5개 미만이라면...
	$ranking_limit = 5 - intval(${'row_size_'.$cr_class_key});
	if($ranking_limit > 0){
		$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');
		${'sql_ranking_comics_'.$cr_class_key} = " SELECT * FROM comics  WHERE 1 AND cm_no not in (".implode(", ",${'ranking_comics_'.$cr_class_key}).") ".${'sql_ranking_big_'.$cr_class_key}." $sql_cm_adult ORDER BY cm_episode_date ";
		${'result_ranking_comics'.$cr_class_key} = sql_query(${'sql_ranking_comics_'.$cr_class_key});
		while (${'row_ranking_comics'.$cr_class_key} = sql_fetch_array(${'result_ranking_comics'.$cr_class_key})) {
			${'row_ranking_comics'.$cr_class_key}['cr_comics'] = ${'row_ranking_comics'.$cr_class_key}['cm_no'];
			array_push(${'ranking_arr_'.$cr_class_key},  ${'row_ranking_comics'.$cr_class_key});
		}
	}
	$ranking_arr_count++;
}

*/



$d_cr_class_arr = array();

$sql_iscroll_adult = sql_adult($mb_adult_permission , 'c.cm_adult');
$sql_crs_crs_class_cm_adult = "";
$sql_c_cm_adult = "";
$cm_small_list_arr = array(2,3,4,6);
$cm_small_list_txt_arr = array('GL','로맨스','드라마','성인');
if($sql_iscroll_adult != ''){
	$sql_crs_crs_class_cm_adult = "_t";
	$sql_c_cm_adult = $sql_iscroll_adult;
	$cm_small_list_arr = array(2,3,4,5);
	$cm_small_list_txt_arr = array('GL','로맨스','드라마','코믹');
}
// 2,3,4,6 ||  2,3,4,5

for($s=0; $s<4;$s++){
	$row_iscroll_arr[$s] = array(); // 배열선언
	$sql_iscroll = "
		SELECT c.*, crs.crs_ranking, 
		c_prof.cp_name as prof_name 
		FROM comics c 
		left JOIN comics_ranking_sales_cmgenre_auto crs ON crs.crs_comics = c.cm_no
		left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
		WHERE 1 and c.cm_service = 'y' and c.cm_small = '".$cm_small_list_arr[$s]."' ".$sql_c_cm_adult." AND crs.crs_class = '0".$sql_crs_crs_class_cm_adult."' 
		GROUP BY c.cm_no 
		ORDER BY crs.crs_ranking ASC 
		LIMIT 0, 5
		";
		
	$result_iscroll = sql_query($sql_iscroll);
	while ($row_iscroll = sql_fetch_array($result_iscroll)) {
		array_push($row_iscroll_arr[$s], $row_iscroll);
	}
	// echo $sql_iscroll.";<br/>";
}



	$slide_centerMode_val = 'true';
	$slide_rightPadding_val = "'60px'";
?>
<!-- body space -->
	<!-- wrapper space -->
			<!-- container_bg -->
				<link rel="stylesheet" type="text/css" href="<?=NM_MO_MAIN_URL;?>/css/slide_list.css<?=vs_para();?>" />
				<script type="text/javascript" src="<?=NM_MO_MAIN_URL;?>/js/slide_list.js<?=vs_para();?>"></script>
				<script type="text/javascript">
					<!--
					$(document).on('ready', function() {
						$("#mo_ranking_class_list .regular").slick({
							dots: true,
							infinite: true,
							centerMode: <?=$slide_centerMode_val?>,
							rightPadding: <?=$slide_rightPadding_val?>,
							speed: 500,
							rankingsToShow: 1,
							rankingsToScroll: 1,
							autoplay: true,
							autoplaySpeed: 5000,
							arrows: false
						});
					});
					<? /* fade: true, cssEase: 'linear', 
						  vertical: true, verticalSwiping: true,
					*/ ?>
					//-->
				</script>
				<div class="container_bg">
					<div id="mo_ranking_class_list">
						<div class="mo_ranking_list_bg clear">
							<h2>인기작품</h2>
							<div class="mo_ranking_list clear">
								<section class="regular rankingr">
								<?  foreach($cm_small_list_arr as $cr_class_key => $cr_class_val){ ?>
									<div>
										<h3><?=$cm_small_list_txt_arr[$cr_class_key];?></h3>
										<ol>
										<?  foreach($row_iscroll_arr[$cr_class_key] as $ranking_key => $ranking_val){
											/* 컨텐츠 정보 */
											$get_comics = get_comics($ranking_val['cm_no']);

											$cr_width = $cr_height = 75;
											$cr_ratio = $cr_x = $cr_y = '';
											if($ranking_key == 0){ 
												$cr_cover = 'cm_cover_episode';
												$db_cr_img_url = img_url_para($get_comics[$cr_cover], $get_comics['cm_reg_date'], $get_comics['cm_mod_date'], 'cm_cover_episode', 'tn668x334');
												if($get_comics[$cr_cover] ==''){
													$db_cr_img_url = img_url_para($get_comics['cm_cover'], $get_comics['cm_reg_date'], $get_comics['cm_mod_date'], 'cm_cover', 'tn655x335');
												}
											}else{
												$cr_cover = 'cm_cover_sub';
												$db_cr_img_url = img_url_para($get_comics[$cr_cover], $get_comics['cm_reg_date'], $get_comics['cm_mod_date'], 'cm_cover_sub', 'tn75x75');
											}

											/* 코믹스 URL */
											$db_cr_comics_url = NM_URL."/comics.php?comics=".$get_comics['cm_no'];

											/* 이미지 표지 */
											// $db_cr_img_url = img_url_para($get_comics[$cr_cover], $get_comics['cm_reg_date'], $get_comics['cm_mod_date']);
							
											/* 컨텐츠 제목 */ 
											$db_cr_series = text_change($get_comics['cm_series'], "]");

											// $db_cr_series = $get_comics['cm_series'];
							
											/* 컨텐츠 작가명 */
											$db_cr_professional = $get_comics['cm_professional_info'];
							
											/* 순위 */
											$db_cr_ranking = $ranking_val['cr_ranking'];

											/* 6위부터 출력안함 */
											if($ranking_key > 4){ continue; }
										?>
											<? if($ranking_key == 0){ ?>
												<style type="text/css">
													.mo_ranking_list_bg .mo_ranking_list .main_famous_list_first_<?=$cr_class_key?>{ background:url(<?=$db_cr_img_url;?>) left top no-repeat; background-size: cover; }
												</style>
												<li>
													<a href="<?=$db_cr_comics_url?>" target="_self">
														<div class="main_famous_list_first main_famous_list_first_<?=$cr_class_key?>">
															<div class="main_famous_list_first_gradient">
																<p class="list_title cm_title"><?=$db_cr_series;?></p>
																<p class="list_author"><?=$db_cr_professional;?></p>
															</div>
														</div>
													</a>
												</li>
											<? }else{ ?>
												<li>
													<a href="<?=$db_cr_comics_url?>" target="_self">
														<div class="main_famous_list_child">
															<ul class="famous_num clear">
																<li>
																	<img src="<?=$db_cr_img_url;?>" alt="<?=$db_cr_series;?>"/>
																</li>
																<li>
																	<p class="famous_number"><?=$db_cr_ranking;?></p>
																</li>
																<li class="famous_txt ellipsis">
																	<span class="famous_title cm_title"><?=$db_cr_series;?></span><br />
																	<span class="famous_author"><?=$db_cr_professional;?></span>
																</li>
															</ul>
														</div>
													</a>
												</li>
											<? } ?>
										
										<? } ?>
										</ol>
									</div>	
								<? } ?>
								</section>
							</div>
						</div>
					</div>
				</div>