<? include_once '_common.php'; // 공통

/* DB */
$iscroll_arr = array();
$sql_iscroll_adult = sql_adult($mb_adult_permission , 'cm_adult');
$sql_iscroll = "SELECT *, if(cm_episode_date > cm_reg_date,cm_episode_date,cm_reg_date) as cm_date
				FROM comics c left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional
				WHERE 1 AND cm_service = 'y' AND cm_ck_id = '60' $sql_iscroll_adult
				group by cm_no 
				ORDER BY cm_date DESC LIMIT 0 , 10";
$iscroll_url = NM_URL."/cmvolume.php?small=2&mode=key";

$result_iscroll = sql_query($sql_iscroll);
while ($row_iscroll = sql_fetch_array($result_iscroll)) {
	array_push($iscroll_arr,  $row_iscroll);
}
/* 랜덤으로 섞기 */
shuffle($iscroll_arr);
?>
<? if(count($iscroll_arr) > 0){ ?>
<!-- body space -->
	<!-- wrapper space -->
			<!-- container_bg -->
				<link rel="stylesheet" type="text/css" href="<?=NM_MO_MAIN_URL;?>/css/moscroll_big_tl.css<?=vs_para();?>" />
				<script type="text/javascript" src="<?=NM_MO_MAIN_URL;?>/js/moscroll_big_tl.js<?=vs_para();?>"></script>

				<div id="moscroll_big_tl" class="container_bg">
					<div class="moscroll_bic_list clear">
						<h2>TL 작품</h2>
						<span class="moreread"><a href="<?=$iscroll_url?>">더보기</a></span>

						<div id="iscroll<?=$iscroll_no?>"  class="moscroll_bic_bg clear">
							<div>
								<ul>
								<?php
									foreach($iscroll_arr as $iscroll_key => $iscroll_val){
										/* 코믹스 URL */
										$iscroll_url_link = NM_URL."/comics.php?comics=".$iscroll_val['cm_no'];

										/* 이미지 표지 */
										//$iscroll_ratio = 'RK';
										$iscroll_ratio = '';
										$iscroll_img_url = img_url_para($iscroll_val['cm_cover'], $iscroll_val['cm_reg_date'], $iscroll_val['cm_mod_date'], 500, 760, $iscroll_ratio);

										/* 컨텐츠 제목 */ 
										$iscroll_series = text_change($iscroll_val['cm_series'], "]");

										/* 줄거리 */
										$mb_str_limit = 28;
										$iscroll_somaery = stripslashes($iscroll_val['cm_somaery']);
										?>
										<li>
											<div>
												<a href="<?=$iscroll_url_link?>" target="_self">
													<dl>
														<dt><img class="iscroll_img" src="<?=$iscroll_img_url?>" alt="<?=$iscroll_val['cm_series']?>"/></dt>
														<dt class="series ellipsis"><?=$iscroll_series?></dt>
														<dd class="small ellipsis"><?=$nm_config['cf_small'][$iscroll_val['cm_small']]?></dd>
														<dd class="somaery"><?=$iscroll_somaery?></dd>
													</dl>
												</a>
											</div>
										</li>
								<? } ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
<? } ?>