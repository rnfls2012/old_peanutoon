<? include_once '_common.php'; // 공통

$iscroll_url = NM_URL."/cmgenre.php?menu=1";

/* DB */
$iscroll_arr = array();
$sql_iscroll_adult = sql_adult($mb_adult_permission , 'cm_adult');
/*
$crs_class = '1';
if($mb_adult_permission != 'y'){ $crs_class.= '_t';  }
$sql_iscroll = "SELECT *, if(cm_episode_date > cm_reg_date,cm_episode_date,cm_reg_date) as cm_date
				FROM comics c 
				LEFT JOIN comics_ranking_small_auto crsa ON crsa.crs_comics = c.cm_no 
				LEFT JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional
				WHERE 1 AND cm_service = 'y' AND cm_big = '2' AND cm_end='y' $sql_iscroll_adult
						AND crsa.crs_class='".$crs_class."' 
				group by cm_no 
				ORDER BY crsa.crs_ranking ASC 
				LIMIT 0 , 10";

$iscroll_arr = cs_comics_content_list($sql_iscroll, substr($nm_config['nm_path'], -1), 'cm_cover', 'tn518x265');

// 랜덤으로 섞기 
shuffle($iscroll_arr);
*/

// 피너툰 컨텐츠 화면 배치 가이드 적용 - 180518
$sql_iscroll = "
	SELECT c.*, crs.crs_ranking,
	CASE
	WHEN c.cm_no = '2147' THEN 99
	WHEN c.cm_no = '2754' THEN 98
	WHEN c.cm_no = '2333' THEN 97
	WHEN c.cm_no = '2423' THEN 96
	WHEN c.cm_no = '2530' THEN 95
	WHEN c.cm_no = '2762' THEN 94
	ELSE 0 END AS cm_no_special,
	c_prof.cp_name as prof_name
	FROM comics c 
	left JOIN comics_ranking_sales_cmtop_auto crs ON crs.crs_comics = c.cm_no
	left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional
	WHERE 1 and c.cm_service = 'y' and c.cm_end = 'y' AND crs.crs_class = '0'

	OR c.cm_no IN (2147, 2754, 2333, 2423, 2530, 2762)

	GROUP BY c.cm_no
	ORDER BY cm_no_special DESC, crs.crs_ranking ASC
	LIMIT 0, 6
";

$iscroll_arr = cs_comics_content_list($sql_iscroll, substr($nm_config['nm_path'], -1), 'cm_cover', 'tn518x265');

?>
<? if(count($iscroll_arr) > 0){ ?>
<!-- body space -->
	<!-- wrapper space -->
			<!-- container_bg -->
				<link rel="stylesheet" type="text/css" href="<?=NM_MO_MAIN_URL;?>/css/moscroll_big_one.css<?=vs_para();?>" />
				<script type="text/javascript" src="<?=NM_MO_MAIN_URL;?>/js/moscroll_big_one.js<?=vs_para();?>"></script>
				<div id="moscroll_big_one" class="container_bg">
					<div class="moscroll_bic_list clear">
						<h2>1<?=$nm_config['cf_cash_point_unit_ko'];?> 특별관  (6/8~6/13)</h2>
						<!--<span class="moreread"><a href="<?=$iscroll_url?>">더보기</a></span>-->

						<div id="iscroll<?=$iscroll_no?>"  class="moscroll_bic_bg clear">
							<div>
								<ul>
								<?php
									foreach($iscroll_arr as $iscroll_key => $iscroll_val){ ?>
										<li>
											<div><!--180511 변경 -->
												<a href="<?=$iscroll_val['cm_serial_url'];?>" target="_self">
													<dl>
														<dt>
															<div class="icon_view">
																<?=$iscroll_val['cm_free_mark'];?>
																<?=$iscroll_val['cm_sale_mark'];?>
															</div>
															<div class="img_view">																
																<img class="iscroll_img" src="<?=$iscroll_val['cm_cover']?>" alt="<?=$iscroll_val['cm_series']?>" />

																<div class="icon_view_bottom">
																	<?=$iscroll_val['cm_adult_mark'];?>
																	<?=$iscroll_val['cm_up_icon'];?>
																	<?=$iscroll_val['cm_new_icon'];?>
																</div>
															</div>
														</dt>
														
														<dt class="series ellipsis"><?=str_replace("[웹툰판]", "", $iscroll_val['cm_series']);?></dt>
														<!-- 180511 // <dd class="small ellipsis"></dd> // -->
														<dd class="somaery"><?=$iscroll_val['cm_small_span']?><?=$iscroll_val['cm_end_span']?><?=stripslashes($iscroll_val['cm_somaery']);?></dd>
													</dl>
												</a>
											</div>
										</li>
								<? } ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
<? } ?>