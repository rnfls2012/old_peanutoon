<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
			<!-- container_bg -->
				<link rel="stylesheet" type="text/css" href="<?=NM_MO_MAIN_URL;?>/css/siteinfo.css<?=vs_para();?>" />
				<script type="text/javascript" src="<?=NM_MO_MAIN_URL;?>/js/siteinfo.js<?=vs_para();?>"></script>
				<div class="<?=$siteinfo_class?>">
					<div id="site">
						<ul>
							<li><a href="<?=NM_URL;?>/siteterms.php">서비스 이용약관</a></li>
							<li><a href="<?=NM_URL;?>/siteprivacy.php">개인정보 처리방침</a></li>
							<li><a href="<?=NM_URL;?>/cscenter.php">고객센터</a></li>
						</ul>
						<? // if(substr($_SERVER['PHP_SELF'], 1) == "cscenter.php") { ?>
						<div class="copyright">
							<?=$nm_config['cf_company'];?> / 대표이사 : <?=$nm_config['cf_ceo'];?><br />
							사업자등록번호 : <?=$nm_config['cf_business_no'];?><br />
							통신판매업신고번호 : <?=$nm_config['cf_mail_order_sales'];?><br />
							대표번호 : <?=$nm_config['cf_tel'];?><br />
							E-Mail : <?=$nm_config['cf_admin_email'];?><br />
							<i class="fa fa-map-marker" aria-hidden="true"></i> <?=$nm_config['cf_address'];?><br />
							<span><?=$nm_config['cf_copy'];?></span>
						</div>
						<div class="okmark">
							<a href="http://www.copyrightok.kr" target="_blank">
								<img src="<?=NM_IMG?>common/okmark.png<?=vs_para();?>" alt="저작권 OK" />
							</a>
						</div>
						<? // } // end if ?>
					</div>
				</div>