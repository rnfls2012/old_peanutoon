<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/comics.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/comics.js<?=vs_para();?>"></script>
		<script type="text/javascript"> 
		<!-- 
			<?/* 고객 포인트 정보 + 전체구매시 추가지급 정보 */?>
			var total_point = "<?=$mb_sum_point?>";
			var sin_allpay_arr = <?=$sin_allpay_arr_js;?>;
			var episode_btn_check = true;
		//-->
		</script>
		<div id="comics"> <!-- 상단 작품배너와 작품소개 -->
			<div class="detail_header">
				<div class="d_h_top"><img src="<?=$comics['cm_cover_url'];?>" alt="<?=$comics['cm_series'];?>" /></div>
				<div class="d_h_top_visibility visibility"><img src="<?=$comics['cm_cover_url'];?>" alt="<?=$comics['cm_series'];?>" /></div>
				<div class="d_h_con">
					<div class="d_h_title">
						<h4><?=$comics['cm_series'];?></h4>
						<p><?=$comics['cm_professional_info'];?></p>
					</div>
					<span class="d_h_gen">
						<a href="<?=NM_DOMAIN?>/cmbest.php?small=<?=$comics['cm_small'];?>"><?=$small_txt;?></a>
					</span>

					<div class="d_h_ex"><?=stripslashes($comics['cm_somaery']);?></div>

					<div class="d_h_buttonwrap clear">
						<button id="mybookmark" class="d_faborite" onclick="set_bookmark('<?=$comics['cm_no'];?>');" data-mybookmark="<?=$count_bookmark;?>">
							<i class="fa fa-star-o" aria-hidden="true";></i>
							즐겨찾기
						</button>
						<a class="d_first" href="<?=$cs_comic_first_url;?>" target="_self">첫화보기</a>
						<? if($cs_comic_cont_url != "") { ?>
						<a class="d_cont" href="<?=$cs_comic_cont_url;?>" target="_self">
							이어보기
							<span class="d_cont_episode"><?=$cs_comic_cont_episode;?></span>
						</a>
						<? } // end if ?>
					</div>
				</div> <!-- /d_h_con -->
			</div> <!-- /detail_header -->

			<div class="detail_list"> <!-- 회차리스트 영역 -->
				<ul class="d_l_sort clear"> <!-- 회차리스트 상단 -->
					<li><?=$cm_public_cycle_arr_txt;?></li>
					<li>
						<span class="d_l_list"><i class="fa fa-list" aria-hidden="true"></i></span>
						<a href="<? echo get_comics_url($comics['cm_no'])."&order=ASC";?>" target="_self" class="<?=$comics_order_asc;?>">정주행</a>
						<span class="centerbar"></span>
						<a href="<? echo get_comics_url($comics['cm_no'])."&order=DESC";?>" target="_self" class="<?=$comics_order_desc;?>">최신순</a>
					</li>
				</ul>
				<? if($purchased_cnt > 0) { ?>
				<div class="all_buy"><button onclick="check_enalble();">유료화 전체구매하기</button></div>
				<? } // end if ?>
				<div class ="detail_allby clear">
					<input type="hidden" name="cm_no" id="cm_no" value="<?=$comics['cm_no']?>">
					<div class="d_a_chk">
						<div class="detail_chk">
							<input type="checkbox" id="sin_allpay" name="sin_allpay" onchange="sin_allpay()" />
							<label for="sin_allpay"></label>
						</div>
						<label class="sin_allpay_btn" for="sin_allpay">전체선택</label>
					</div>
					<div class="d_a_ok">
						<button onclick="check_buy();">구매하기</button>
					</div>
					<div class="d_a_cancel">
						<button onclick="check_cancel();">취소</button>
					</div>
					<div class="d_a_bonus">
						<span class="d_a_b_0"> 
							총<span id="chapter_count">0</span>화 
						</span>
						<span class="d_a_b_1">
							<span id="cach_point_sum">0</span><?=$nm_config['cf_cash_point_unit_ko'];?>
						</span><br />
						<span class="d_a_b_2">
							<span id="point_service">0</span> <?=$nm_config['cf_point_unit_ko'];?> 보너스!
						</span>
					</div>
				</div>

				<div class ="episode_list clear">
					<ol>
						<? foreach($episode_arr as $episode_key => $episode_val){ ?>
						<li class="<?=$episode_val['ce_check']?>">
							<?=$episode_val['ce_url'];?>
								<div class="detail_thumb  <?=$episode_val['ce_icon_up']?>  <?=$episode_val['ce_bookmark']?>" style="background-image:url('<?=$episode_val['ce_img_url']?>');">
									<div class="detail_chk <?=$episode_val['ce_purchased'];?> <?=$episode_val['ce_event_free'];?> <?=$episode_val['ce_event_sale'];?>">
										<input type="checkbox" id="episode<?=$episode_val['ce_no'];?>" class=" <?=$episode_val['ce_disabled'];?>" <?=$episode_val['ce_disabled'];?> name="episode[]" value="<?=$episode_val['ce_no'];?>" data-cash_point="<?=$episode_val['ce_pay']?>" />
										<label class="<?=$episode_val['ce_disabled'];?>" for="episode<?=$episode_val['ce_no'];?>"></label>
									</div>
									<div class="icon_up"><img src="<?=NM_MO_IMG?>common/icon_up.png" alt="update" /></div>
									<div class="icon_bookmark"><img src="<?=NM_IMG?><?=$nm_config['cf_mobile_bookmark']?>" alt="bookmark" /></div>
									<dl class="clear">
										<dt><?=$episode_val['ce_txt']?></dt>
										<dd><?=$episode_val['ce_service_date_10']?></dd>
									</dl>
									<div class="btn"><?=$episode_val['ce_btn']?></div>
								</div>
							</a>
						</li>
						<? } ?>
					</ol>
				</div>
			</div> <!-- /detail_list -->
		</div>
