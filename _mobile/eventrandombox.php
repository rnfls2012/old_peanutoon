<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/eventrandombox.css<?=vs_para();?>" />
		<style type="text/css">
			.evt_gauge { background:url(<?=$randombox_img;?>/gauge_bg.png<?=vs_para();?>) no-repeat center top; background-size: 100%; }
			.evt_btn { background:url(<?=$randombox_img;?>/blankimg02.png<?=vs_para();?>); background-size: 100%; }
			.evt_btn a { background:url(<?=$randombox_img;?>/btn_bg.png<?=vs_para();?>) no-repeat left top; background-size: 100%; }
			.ModalPopup { background:url(<?=$randombox_img;?>/bg01.png<?=vs_para();?>); background-size: auto; }
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket1 + label::before { background: url(<?=$randombox_img;?>/p_pocket01.png<?=vs_para();?>); background-repeat: no-repeat; background-size: 200%; }
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket2 + label::before { background: url(<?=$randombox_img;?>/p_pocket02.png<?=vs_para();?>);	background-repeat: no-repeat; background-size: 200%; }
			.ModalPopup .p_content01 ul.p_choice li input[type='radio']#pocket3 + label::before { background: url(<?=$randombox_img;?>/p_pocket03.png<?=vs_para();?>);	background-repeat: no-repeat;	background-size: 200%; }
			.ModalPopup .p_content02 .p_coupon { background: url(<?=$randombox_img;?>/p_coupon.png<?=vs_para();?>) no-repeat center top; background-size: 100%; }
			/*.evt_gauge { background:url(<?=$randombox_img;?>/gauge_bg.png<?=vs_para();?>); background-size: 100%; background-color: f5eee8; }*/
		</style>
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/eventrandombox.js<?=vs_para();?>"></script>
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/jquery.autocomplete.css">
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/jquery.autocomplete-ui.min.js"></script>
		<link rel="stylesheet" href="<?=NM_MO_URL;?>/css/circle.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<input type="hidden" id="erm_member" class="erm_member" value="<?=$randombox_mb['erm_member'];?>" />
		<input type="hidden" id="erm_pop_count" class="erm_pop_count" value="<?=$randombox_mb['erm_pop_count'];?>" />
		<input type="hidden" id="erm_cash_point_total" class="erm_cash_point_total" value="<?=$randombox_mb['erm_cash_point_total'];?>" />

			<!-- Modal Popup -->
			<div class="Modalalert">
				<div class="ModalBackground"></div>
				<div class="ModalPopup">
					<div class="ModalClose"><i class="material-icons">clear</i></div>
					<!-- 복주머니 부분 -->
					<div class="p_content01">
						<div class="p_title">
							<img src="<?=$randombox_img;?>/p_title.png<?=vs_para();?>" alt="땅콩 품은 복주머니" />
						</div>
						<div class="p_exp">
							<img src="<?=$randombox_img;?>/p_exp.png<?=vs_para();?>" alt="마음에 드는 복주머니를 선택하세요">
						</div>
						<ul class="p_choice">
							<li>
								<input type="radio" name="pocket" value="pocket1" id="pocket1">
								<label for="pocket1">복주머니1</label>
							</li>
							<li>
								<input type="radio" name="pocket" value="pocket2" id="pocket2">
								<label for="pocket2">복주머니2</label>
							</li>
							<li>
								<input type="radio" name="pocket" value="pocket3" id="pocket3">
								<label for="pocket3">복주머니3</label>
							</li>
						</ul>
						<div class="p_btnbig">
							<button id="open_btn" class="nochk">바로 열기</button>
						</div>
					</div>

					<!-- 쿠폰 부분 -->
					<div class="p_content02" style="display: none;">
						<div class="p_coupon">
							<p><span class="coupon_value">5</span>%</p>
							<span class="coupon_time"><span class="coupon_value02">2018. 2. 28</span>까지 사용가능</span>
						</div>
						<div class="p_get">
							<span class="coupon_value">5</span><span>%</span>결제 할인권 획득!
						</div>
						<div class="p_exp">
							<img src="<?=$randombox_img;?>/con02_warn.png<?=vs_para();?>" alt="당첨된 쿠폰은 쿠폰함에서 확인 가능합니다.">
						</div>
						<ul>
							<li><button id="recharge_link" class="p_buy" onclick="location.href='<?=NM_URL?>/recharge.php'">결제하기</button></li>
							<li><button class="p_couponpage" onclick="location.href='<?=NM_URL?>/coupon.php'">쿠폰 보기</button></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /Modal Popup -->
			<div class="goToTop"><img src="<?=$randombox_img;?>/gototop.png<?=vs_para();?>" alt="위로가기" /></div>
			<div class="evt_header">
				<img src="<?=$randombox_img;?>/header.png<?=vs_para();?>" alt="땅콩 품은 복주머니">
			</div>
			<div class="evt_gauge">
				<img src="<?=$randombox_img;?>/gauge_bg.png<?=vs_para();?>" alt="게이지 배경" />
				<div class="gauge_wrap">
					<div class="c100 p<?=$randombox_mb['erm_pop_gauge'];?> bukuro <?=$mobile_logout_class;?>">
						<span class="gauge_text">복주머니 열기</span>
						<span class="gauge_charge"><?=$randombox_mb['erm_pop_chance'];?></span>
						<!-- <span class="gauge_logout">로그인을<br />해주세요</span> -->
						<div class="slice">
							<div class="bar"></div>
							<div class="fill"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="evt_blankimg"><img src="<?=$randombox_img;?>/blankimg01.png<?=vs_para();?>" /></div>
			<div class="evt_btn">
				<a href="#">오늘 남은 횟수 <span><?=$randombox_mb['erm_pop_count'];?></span>회</a>
			</div>
			<div class="evt_next"><img src="<?=$randombox_img;?>/header_bottom.png<?=vs_para();?>" alt="이벤트 기간 : 2018.02.15 - 2018.02.19" /></div>
			<div class="con02">
				<img src="<?=$randombox_img;?>/con02_01.png<?=vs_para();?>" alt="이벤트 참여방법" />
				<img src="<?=$randombox_img;?>/con02_02.png<?=vs_para();?>" alt="이벤트 참여방법" />
				<img src="<?=$randombox_img;?>/con02_03.png<?=vs_para();?>" alt="이벤트 참여방법" />
				<img src="<?=$randombox_img;?>/con02_04.png<?=vs_para();?>" alt="이벤트 참여방법" />
				<img src="<?=$randombox_img;?>/con02_05.png<?=vs_para();?>" alt="이벤트 참여방법" />
				<img src="<?=$randombox_img;?>/con02_06.png<?=vs_para();?>" alt="이벤트 참여방법" />
				<div class="gotocoupon">
					<button onclick="location.href='<?=NM_URL?>/coupon.php'">쿠폰함 바로가기</button>
				</div>
			</div>
			<div class="con03">
				<img src="<?=$randombox_img;?>/con03.png" alt="이벤트 유의사항" />
			</div>
