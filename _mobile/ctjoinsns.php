<?
include_once '_common.php'; // 공통
$social_oauth_url = NM_OAUTH_URL.'/login.php?service=';


if(is_nexcube()){
	// echo urldecode(get_session('ss_http_referer'))."<br/>";
	// echo $cookie_http_referer;
}
?>


<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/ct.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/ct.js<?=vs_para();?>"></script>

		<div class="login_box">
			<h5><?=$nm_config['cf_title'];?> 회원가입</h5>
			<fieldset>
				<form name="ctjoin_form" id="ctjoin_form" method="post" action="<?=NM_PROC_URL;?>/ctjoinsns.php" onsubmit="return ctjoinsns_page_submit();">
					<input type="hidden" name="zmbsj_id" value="<?=$zmbsj_id;?>"/>
					<input type="hidden" name="mb_token" value="<?=get_js_cookie('mb_token');?>"/>

					<div class="regi_ck">
						<label class="regi_agree" for="regi_agree">
							<input type="checkbox" id="regi_agree" />
								<span>전체동의</span>
						</label>
					</div>
					<div class="regi_ck">
						<label class="regi_terms" for="regi_terms">
							<input type="checkbox" id="regi_terms" name="regi_terms" />
								<span>이용약관에 동의합니다.</span>
						</label>
							<a href="<?=NM_URL."/siteterms.php";?>" target="_blank"><span class="regi_detail">상세보기</span></a>
					</div>
					<div class="regi_ck">
						<label class="regi_pinfo" for="regi_pinfo">
							<input type="checkbox" id="regi_pinfo" name="regi_pinfo" />
								<span>개인정보취급방침에 동의합니다.</span>
						</label>
							<a href="<?=NM_URL."/siteprivacy.php";?>" target="_blank"><span class="regi_detail">상세보기</span></a>
					</div>
					<div class="regi_ck">
						<label class="regi_event" for="regi_event">
							<input type="checkbox" id="regi_event" name="regi_event" />
								<span>정보/이벤트 메일 수신 동의합니다.(선택)</span>
						</label>
					</div>
							
					<?if($zmbsj_id_msg == ""){?>
					<button type="submit" id="login_ok" name="login_ok">회원가입</button>
					<?}else{?>
					<button type="button" id="login_error" name="login_error" class="login_no"><?=$zmbsj_id_msg;?></button>
					<?}?>
					<button type="button" id="login_no" name="login_no" class="login_no">취소</button>
				</form>
			</fieldset>

			<div class="login_help">
				문제가 있거나 궁금한 점이 있으시면<br />
				<a href="mailto:<?=$nm_config['cf_admin_email'];?>"><?=$nm_config['cf_admin_email'];?></a> 으로 문의주시기 바랍니다.
			</div>
		</div>