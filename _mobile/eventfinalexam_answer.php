<?php

?>

<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL?>/css/eventfinalexam_answer.css<?=vs_para()?>" />
<script type="text/javascript" src="<?=NM_MO_URL?>/js/eventfinalexam_answer.js<?=vs_para()?>"></script>

	<style type="text/css">		
		.evt-container {
			background: url(<?=$finalexam_img_url;?>/bg01.jpg<?=vs_para();?>);
			background-color: #ecece1;
		}
		
		.evt-header {
			background-image:url(<?=$finalexam_img_url;?>/bg04.png<?=vs_para();?>), url(<?=$finalexam_img_url;?>/bg03.png<?=vs_para();?>), url(<?=$finalexam_img_url;?>/bg02.png<?=vs_para();?>);
			background-position:center, right top, left bottom;
			background-size: auto;
			background-repeat:no-repeat;
		}
		.evt-present:before {
			background: url(<?=$finalexam_img_url;?>/bg05.png<?=vs_para();?>);
		}
		.evt-input:before {
			background: url(<?=$finalexam_img_url;?>/bg05.png<?=vs_para();?>);
		}
		.evt-answer:before {
			background: url(<?=$finalexam_img_url;?>/bg05.png<?=vs_para();?>);
		}
		.exp-header-wrap {
			background: url(<?=$finalexam_img_url;?>/bg01.jpg<?=vs_para();?>);
			background-color: #ecece1;
		}
	</style>

	<div class="evt-container">
		<div class="question">
			<div class="exp-header-wrap">
				<div class="exp-header">
					<div class="q_part">
						<span><?=$efp_arr['efp_mark'];?>교시</span>
					</div>
					<div class="q_title"><img src="<?=$finalexam_answer_img_url;?>/title.png<?=vs_para();?>" alt="전국덕후학력평가 <?=$efp_arr['efp_title'];?> 답안해설" /></div>
					<div class="q_paging">
						<ul>
							<li class="off"><span><i class="fa fa-angle-left" aria-hidden="true"></i></span></li>
							<li>1/5</li>
							<li><span><i class="fa fa-angle-right" aria-hidden="true"></i></span></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="exp-con">
				<? for($i=1; $i<=5; $i++){ $exp_img_css = "display:none;"; if($i==1){ $exp_img_css = ""; } ?>
				<div class="exp_img" style="<?=$exp_img_css;?>">
					<? for($j=1; $j<=6; $j++){ ?>
						<img src="<?=$finalexam_answer_img_url;?>/<?=$i;?>-<?=$j;?>.png<?=vs_para();?>" alt="<?=$efp_arr['efp_title'];?> 답안해설 <?=$i;?>-<?=$j;?>번">
					<? } ?>
				</div>
				<? } ?>
			</div>
		</div>
	</div>