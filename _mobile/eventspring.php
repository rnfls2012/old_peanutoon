<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/eventspring.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/eventspring.js<?=vs_para();?>"></script>
		<style type="text/css">
			.evt-content-wrap {
				background-image: url(<?=$spring_img;?>/bg02.png<?=vs_para();?>), url(<?=$spring_img;?>/bg01.png<?=vs_para();?>);
				background-size: 100%;
				background-position: bottom, top;
				background-repeat: no-repeat;
			}
			.evt-content .warning {
				background-image: url(<?=$spring_img;?>/con-line.png<?=vs_para();?>);
				background-repeat: no-repeat;
				background-position: top;
				background-size: 100%;
			}
			.ModalPopup {
				background-image: url(<?=$spring_img;?>/p_bg.png<?=vs_para();?>);
				background-repeat: no-repeat;
				background-size: 100%;
			}
			.ModalPopup .p_content .p_coupon {
				background-image: url(<?=$spring_img;?>/p_coupon.png<?=vs_para();?>);
				background-size: 100%;
				background-repeat: no-repeat;
			}
		</style>
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<input type="hidden" id="member_no" class="member_no" value="<?=$nm_member['mb_no'];?>" />
		<input type="hidden" id="total_coupon" class="total_coupon" value="<?=$total_coupon;?>" />

		<div class="evt-container">
			<!-- Modal Popup -->
			<div class="Modalalert">
				<div class="ModalBackground"></div>
				<div class="ModalPopup">
					<div class="ModalClose"><i class="material-icons">clear</i></div>
					<div class="p_content">
						<div class="p_coupon">
							<p><span class="coupon_value">15</span> %</p>
							<span class="coupon_time"><span class="coupon_value02">2018.3.10</span>일까지 사용가능</span>
						</div>
						<div class="p_get">
							<span class="coupon_value">15</span><span>%</span> 결제 할인권 발급!
						</div>
						<div class="p_exp">
							<ol>
								<li>해당 쿠폰은 중복 사용이 불가합니다.</li>
								<li>해당 쿠폰은 9,900원 이상 상품부터 적용 가능합니다.</li>
								<li>해당 쿠폰은 중복 할인이 불가합니다.</li>
								<li>해당 쿠폰의 사용기간은 2018년 3월 10일까지 입니다.</li>
							</ol>
							<img src="<?=$spring_img;?>/p_info.png<?=vs_para();?>" alt="당첨된 쿠폰은 [쿠폰함] 에서 확인 가능합니다.">
						</div>
						<ul>
							<li><button class="p_buy" onclick="location.href='<?=NM_URL?>/recharge.php'">결제하기</button></li>
							<li><button class="p_couponpage" onclick="location.href='<?=NM_URL?>/coupon.php'">쿠폰 보기</button></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /Modal Popup -->
			<div class="evt-header">
				<img src="<?=$spring_img;?>/header.png<?=vs_para();?>" alt="웹툰 봄" />
			</div>
			<div class="evt-content-wrap">
				<div class="evt-content">
					<img src="<?=$spring_img;?>/evt-info.png<?=vs_para();?>" alt="이벤트 설명" />
					<div class="coupon">
						<div class="couponimg">
							<img src="<?=$spring_img;?>/evt-coupon<?=$coupon_off;?>.png<?=vs_para();?>" alt="땅콩 결제 15% 쿠폰" />
						</div>
						<div class="couponpara">
							<ul>
								<li>남은 수량<span><?=$total_coupon;?></span></li>
								<li>
									<button id="open_btn" class="<?=$nochk;?>"><?=$coupon_off_text;?></button>
								</li>
							</ul>
						</div>
					</div>
					<img src="<?=$spring_img;?>/evt-info02.png<?=vs_para();?>" alt="1인 1인 1회에 한하여 쿠폰발급이 가능합니다. ">
					<div class="warning">
						<img src="<?=$spring_img;?>/evt-warn.png<?=vs_para();?>" alt="이벤트 유의사항">
					</div>
				</div>
			</div>
		</div>