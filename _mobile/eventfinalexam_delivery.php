<?php

?>

<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL?>/css/eventfinalexam_delivery.css<?=vs_para()?>" />
<script type="text/javascript" src="<?=NM_MO_URL?>/js/eventfinalexam_delivery.js<?=vs_para()?>"></script>
	<style type="text/css">		
		.evt-container {
			background: url(<?=$finalexam_img_url;?>/bg01.jpg<?=vs_para();?>);
			background-color: #ecece1;
		}
		.question-result .q_score span:after {
			background:url(<?=$finalexam_img_url;?>/pencel.png<?=vs_para();?>);
			background-size: 100%;
			background-repeat: no-repeat;
		}
	</style>
	
	<div class="evt-container">
		<div class="question">
			<form name="prize_form_page" id="prize_form_page" method="post" action="<?=NM_PROC_URL;?>/eventfinalexam_delivery.php" onsubmit="return prize_page_submit();">
				<div class="question-header">
					<h3>당첨자 정보 입력</h3>
				</div>
				<div class="prizeinfo">
					<div class="prize_name">
						<label for="name">당첨자 성명</label>
						<input type="text" id="name" name="name" placeholder="당첨자 성명을 입력해주세요" required autocomplete="off" value="<?=$row_efm['efm_name'];?>" />
					</div>
					<div class="prize_phone">
						<label for="phone">휴대폰 번호</label>
						<input type="text" id="phone" name="phone" placeholder="'-'를 포함하지 않고 입력해주세요" required autocomplete="off" value="<?=$row_efm['efm_tel'];?>" />
					</div>
					<div class="prize_address">
						<label for="address">배송지 정보</label>
						<input type="text" id="address" name="address" placeholder="'도로명 주소'를 입력해주세요" required autocomplete="off" value="<?=$row_efm['efm_address'];?>" />
					</div>
					<div class="prize_postal">
						<label for="postal">우편번호</label>
						<input type="text" id="postal" name="postal" placeholder="'우편번호 5자리'를 입력해주세요" required autocomplete="off" value="<?=$row_efm['efm_postal'];?>" />
					</div>
					<div class="prize_check">
						<h4>개인정보 수집 및 이용에 대한 안내</h4>
						<div class="prize_term">
							개인정보 수집 및 이용에 대한 안내 ㈜피너툰은  피너툰 3주년 이벤트 경품 당첨자를 대상으로 아래와 같이 개인정보를 수집하고 있습니다.<br/><br/>
							1. 수집 개인정보 항목 : [필수] 성명/연락처/주소/우편번호<br/>
							2. 개인정보의 수집 및 이용목적 : 경품 배송을 위한 본인확인 및 배송 주소, 우편번호, 연락 경로 확인<br/>
							3. 개인정보의 이용기간 : 이용목적 달성 후, 즉시 파기. 단, 관계법령의 규정에 의하여 보존할 필요가 있는 경우 일정기간 동안 개인정보를 보관 할 수 있습니다.<br/><br/>

							그 밖의 사항은 ㈜피너툰 개인정보취급방침을 준수합니다.
						</div>
						<input type="checkbox"" name="term_ok" id="term_ok" value="y"><label for="term_ok"><span></span> 개인정보 수집 및 이용안내에 동의합니다.</label>
					</div>
				</div>
				<div class="question-footer">
					<!-- <button>등록하기</button> -->
					<input type="submit" id="prize_ok" name="prize_ok" value="등록하기">
				</div>
			</div>
		</form>
	</div>
