<?php 

?>

<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL?>/css/eventfinalexam_reception.css<?=vs_para()?>" />
<script type="text/javascript" src="<?=NM_MO_URL?>/js/eventfinalexam_reception.js<?=vs_para()?>"></script>
	<style type="text/css">
		.evt-container {
			background: url(<?=$finalexam_img_url;?>/bg01.jpg<?=vs_para();?>);
			background-color: #ecece1;
		}
		.question-result .q_score span:after {
			background:url(<?=$finalexam_img_url;?>/pencel.png<?=vs_para();?>);
			background-size: 100%;
			background-repeat: no-repeat;
		}
	</style>
	<script type="text/javascript">
	<!--
		<?=$scrolltop_js;?>
	//-->
	</script>

	<div class="evt-container">
		<div class="evt-header">
			<img src="<?=$finalexam_img_url;?>/title.png<?=vs_para();?>" alt="전국덕후학력평가" />
		</div>
		<div class="evt-test">
			<div class="test_tab">
				<ul>
				<? foreach($efp_arr as $efp_key => $efp_val){ ?>
					<li class="<?=$efp_val['tab_class'];?>" data-tab-use="<?=$efp_val['data_tab_use'];?>" data-tab-date="<?=$efp_val['data_tab_date'];?>">
						<a><span><?=$efp_val['efp_mark'];?>교시</span></a>
					</li>
				<? } ?>
				</ul>
			</div>
			<? foreach($efp_arr as $efp_key => $efp_val){ ?>
				<div class="test_con <?=$efp_val['con_class'];?>">
					<h3><?=$efp_val['efp_title']?></h3>
					<div class="test_insp">
						<p>
							응시기간 : <?=$efp_val['efp_date_start_txt']?>(<?=$efp_val['efp_date_start_week_txt']?>) 
							~ <?=$efp_val['efp_date_end_txt']?>(<?=$efp_val['efp_date_end_week_txt']?>) 
						</p>
						<ul>
							<li>시험 문항 수는 20개 입니다.</li>
							<li>제한시간 15분이 지나가면 자동으로 종료되며, 남은 문제는 0점 처리됩니다.</li>
							<li>시험 응시는 3번 가능합니다.</li>
							<li>해당 영역은 피너툰 웹툰 <?=$efp_val['efp_title']?> 입니다.</li>
							<li>도중에 종료할 경우, 남은 문제는 0점 처리 됩니다.</li>
							<li>
								<?=$efp_val['efp_title']?> 
								<span><?=$efp_val['efp_memo']?></span>
							</li>
						</ul>
					</div>
					<div class="test_score">
						내 점수 : <span><?=$efp_val['efm_highscore'.($efp_key+1)];?></span>점
					</div>
					<div class="test_go">
						<button id="finalexam" data-link-msg="<?=$efp_val['efp_link_msg'];?>" data-efp-link="<?=$efp_val['efp_link'];?>">
							<?=$efp_val['efp_link_txt'];?>(<?=$efp_val['efm_count'.($efp_key+1)];?>/3)
						</button>
						※응시자 전원에게 15% 결제 할인 쿠폰을 드립니다.
					</div>
				</div>
			<? } ?>
		</div>

		<div class="evt-present"><img src="<?=$finalexam_img_url;?>/present.png" alt="이벤트 상품구성" /></div>
		<div class="evt-warn"><img src="<?=$finalexam_img_url;?>/warn.png" alt="이벤트 유의사항">
	</div>