<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<? include NM_MO_PART_PATH.'/event.php';		// [mobile] 서브슬라이드?>

			<? include NM_MO_PART_PATH.'/banner.php';		// [mobile] 배너 스크롤?>

			<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/cmtop.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_MO_URL;?>/js/cmtop.js<?=vs_para();?>"></script>
			<!-- 작품리스트 -->
			<div id="cmtop" class="container_bg">
				<div class="sub_list clear" data-limit_define="<?=NM_SUB_LIST_LIMIT;?>" data-limit="<?=$sub_list_limit;?>" data-total="<?=$row_total;?>" data-url="<?=$_SERVER['REQUEST_URI'];?>" data-menu="<?=$_menu;?>" data-bool="<?=$sub_list_bool;?>"> 
					<? foreach($cmtop_arr as $cmtop_key => $cmtop_val){ ?>
					<a href="<?=$cmtop_val['cm_serial_url']?>" target="_self" data-week="<?=$cmtop_val['cm_week_txt']?>">
						<div class="sub_list_thumb">
							<div class="icon_view">								
								<?=$cmtop_val['cm_free_mark'];?>
								<?=$cmtop_val['cm_sale_mark'];?>
							</div>
							<div class="img_view">
								<img class="<?=$date_up_class;?>" src="<?=$cmtop_val['cm_cover_sub']?>" alt="<?=$cmtop_val['cm_series']?>" />
								<div class="icon_view_bottom">
									<?=$cmtop_val['cm_adult_mark'];?>
									<?=$cmtop_val['cm_up_icon'];?>
									<?=$cmtop_val['cm_new_icon'];?>
								</div>
							</div>
							<dl>
								<dt class="ellipsis">
									<span class="cm_title"><?=str_replace("[웹툰판]", "", $cmtop_val['cm_series']);?></span>
								</dt>
								<dd class="ellipsis"><?=$cmtop_val['cm_small_span']?><?=$cmtop_val['cm_end_span']?></dd>
								<dd class="ellipsis"><?=$cmtop_val['cm_professional_info']?></dd>
							</dl>
						</div>
					</a>
					<? } /* foreach($cmtop_arr as $cmtop_key => $cmtop_val){  */ ?>
				</div>
			</div>