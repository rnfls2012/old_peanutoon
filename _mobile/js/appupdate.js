
function app_update(user_appver, new_appver)
{
	var msg_arr = Array();

	if(user_appver == 0){
		/* 초기버전 이라면 */
		msg_arr.push('회원님의 피너툰 앱은 초기 버전으로 확인되였습니다.');
		msg_arr.push('초기 버전은 기존 피너툰 앱을 삭제하시고');
		msg_arr.push('신규 버전으로 다시 다운로드 받으셔야 합니다.');
		msg_arr.push('불편을 드려 대단히 죄송합니다.');
		msg_arr.push('');
		msg_arr.push('<strong>다운로드 위치</strong>');
		msg_arr.push('피너툰 홈페이지('+nm_url+')');
		msg_arr.push('맨하단에 피너툰 완전판 앱 다운로드 클릭하셔서');
		msg_arr.push('설치안내에 따라 설치해주시기 바랍니다.');

		$('#contents .vernotice_footer').html('<a onclick="appupdate_later()">다음에 하기</a>');
	}else{
		msg_arr.push(new_appver+' 버전 다운로드 합니다.');
		msg_arr.push('설치 부탁드립니다.');
		msg_arr.push('불편을 드려 대단히 죄송합니다.');
		msg_arr.push('');
		msg_arr.push('<strong>핸드폰 기기에 따라 다운로드가 안될 시</strong>');
		msg_arr.push('피너툰 홈페이지('+nm_url+')');
		msg_arr.push('맨하단에 피너툰 완전판 앱 다운로드 클릭하셔서');
		msg_arr.push('설치안내에 따라 설치해주시기 바랍니다.');
	}
	
	alertBox(msg_arr.join("<br>\n"), vernotice_guide, msg_arr);
}

function vernotice_guide(msg_arr)
{
	var msg_length = Number(msg_arr.length);
	
	if(msg_length == 8){ // 위 app_update - msg_arr 갯수에 영향이 있습니다.
		window.open(nm_url+'/app/app_setapk.php?mode=appupdate', '_blank');
	}
	$('#contents .vernotice_con .vernotice_guide').html('<h3>알림 내용</h3>'+msg_arr.join("<br/>"));
}

function appupdate_later()
{
	link(nm_url+'/proc/appupdate.php');
}