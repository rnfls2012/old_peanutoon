$(function(){
	var ready = false;
	if(ready == false){ ready = empty_image_ready(ready); }
	
	/* 스크롤 맨 하단으로 내려가면 메뉴 보이기 */
	$(window).scroll(function () { 
		if ($(window).scrollTop() == $(document).height() - $(window).height()) { 
			menu_show();
		} else { 
			menu_hide();
		} // end else
	});

	$("#comicsviewpage .comic .bg").click(function(event){ // 흑백배경
		empty_image_ready_end();
	});
	$("#comicsviewpage .webtoon-viewer-list").click(function(event){ // 메뉴영역
		event.stopPropagation();
		menuToggle();
	});

	$("#bmk_list").click(function(){
		var viewerlist_stats = $(".inviewer_list").css("display");
		/* none, block */
		if(viewerlist_stats == "none"){
			$(".inviewer_list").slideDown('fast');
		}else{
			$(".inviewer_list").slideUp('fast');
		}
		inviewer_load();
	});

	/* 즐겨찾기 들어갈 때 유무 처리 */
	if($("#mybookmark").data("mybookmark") != 0) {
		var bookmark_star = $("#mybookmark").find("i");

		bookmark_star.removeClass("fa-star-o");
		bookmark_star.addClass("fa-star");
	} // end if
	
	/* 랜덤박스 하단 뷰어 */
	$("#viewer_footer .viewer_event_wrap .viewer_event_close").click(function(){
		$("#viewer_footer .viewer_event_wrap").hide();
		$("#viewer_footer").css("height","50px");
		$("#viewer_footer").css("border-top","1px solid #EEEEEE");
	});
});

function inviewer_load(){
	var list_on_height = 0;
	$(".inviewer_list ol li").each(function( idx, val ){
		if($(this).hasClass('list_on')== true){
		list_on_height = idx * 37;
		}
	});
		console.log(list_on_height);
	$(".inviewer_list").animate({scrollTop:list_on_height}, 0);
}

function direction_view(){
	$('#comicsviewpage .direction img').css({ "position" : "absolute", "left" : ($(window).width() - $('.direction img').width()) / 2, "top" : ($(window).height() - $('.direction img').height()) / 2 }).load(function(){
		$(this).css({ "left" : ($(window).width() - $(this).width()) / 2, "top" : ($(window).height() - $(this).height()) / 2 });
	});
	$('#comicsviewpage .direction').show();
}

function direction_hide(){
	$('#comicsviewpage .direction').fadeOut();
}

function menu_hide(){
	if ($("#comicsviewpage #viewer_header").is(":visible")){
		$("#comicsviewpage #viewer_header").slideUp();
		$("#comicsviewpage #viewer_footer").slideUp();
		$("#comicsviewpage #comic_page_controller").slideUp();
		$(".inviewer_list").hide('');
	}
}

function menu_show(){
	if ($("#comicsviewpage #viewer_header").is(":hidden")){
		$("#comicsviewpage #viewer_header").slideDown();
		$("#comicsviewpage #viewer_footer").slideDown();
		$("#comicsviewpage #comic_page_controller").slideDown();
	}
}

function menuToggle(){
	if ($("#comicsviewpage #viewer_header").is(":hidden")){
		menu_show();
	} else{
		menu_hide();
	}
}

function empty_image_ready(){
	direction_view();
	empty_image_ready_end();
	return true;
}

function empty_image_ready_end(){
	$('#comicsviewpage .direction').delay(1000).fadeOut(function(){
		menu_hide();
	});
}

function prev_image(){
	var $episode_list = $('.empty_image ul li');
	var active = '';
	$episode_list.each(function(idx, val) {
		if($(this).hasClass('active')){
			active = idx;
		}
	});
	active--;

	if(active < 0){
		if(episode_prev == ''){
			toast('처음 화 입니다.');
		}else{
			toast('이전 화를 불러오는 중입니다.');
			episode_goto_url(episode_prev, episode_prev_pay, episode_prev_txt);
		}
	}else{
		movepage(active);
	}

}

function next_image(){
	var $episode_list = $('.empty_image ul li');
	var active = '';
	$episode_list.each(function(idx, val) {
		if($(this).hasClass('active')){
			active = idx;
		}
	});
	active++;
	if(active > page_slider_count){
		if(episode_next == ''){
			toast('마지막 화 입니다.');
		}else{
			toast('다음 화를 불러오는 중입니다.');
			episode_goto_url(episode_next, episode_next_pay, episode_next_txt);
		}
	}else{
		movepage(active);
	}
}


function episode_goto_url(episode_no, episode_pay, episode_txt){
	episode_url = nm_comicsview+"?comics="+comics+"&episode="+episode_no;

	if(Number(episode_pay) > 0){
		txt = episode_txt+"를 보시려면 결제가 필요합니다.<br/>";
		txt+= episode_pay+nm_cash_point_unit_ko+"을 사용하여 결제하시겠습니까?<br/>";
		txt+= "구매한 작품은 내 서재에 보관 됩니다.";
		confirmBox(txt, goto_replace_url, {url:episode_url});
	}else{
		document.location.replace(episode_url);
	}
}

function cm_page_way_page(page){
	var cm_page = 0;
	if(cm_page_way == 'l'){
		cm_page = Number(page);
	}else{
		cm_page = Number(page_slider_count) - Number(page);
	}
	return cm_page;
}

function movepage(page){
	if(page == ''){page = 0;}
	var page_next_count = 0;
	var $episode_list = $('.empty_image ul li');
	var data_img_src_chk = $episode_list.eq(page).find('img').attr('data_img_src_chk');
	var data_img_src = $episode_list.eq(page).find('img').attr('data_img_src');
	var data_img_alt = $episode_list.eq(page).find('img').attr('alt');
	var $comic_viewer_img = $('.comic_viewer .cut img');
	// 다음꺼
	var data_img_src_chk_next =  data_img_src_next = "";
	var page_next = page;

	// 해당 페이지 active 수정
	$episode_list.removeClass('active');
	$episode_list.eq(page).addClass('active');

	// 해당 페이지 src 수정
	if(data_img_src_chk != 'success'){
		$episode_list.eq(page).find('img').attr('src',data_img_src);
		$episode_list.eq(page).find('img').attr('data_img_src_chk','success');
	}
	// 해당 페이지 표기
	var page_num_start = Number(page)+1;
	var page_num_end = Number(page_slider_count)+1;
	$('#page_num').text(page_num_start + ' / ' +page_num_end);
	// 바이동
	var cm_page = cm_page_way_page(page);
	$('#sliding_bar').val(cm_page);

	//view 적용
	$comic_viewer_img.attr('src',data_img_src);
	$comic_viewer_img.attr('alt',data_img_alt);

	for(page_next_count ; page_next_count < 4 && page_next < page_slider_count; page_next_count++ ){
		page_next++; // 다음꺼
		data_img_src_chk_next = $episode_list.eq(page_next).find('img').attr('data_img_src_chk');
		if(data_img_src_chk_next != 'success'){
			data_img_src_next = $episode_list.eq(page_next).find('img').attr('data_img_src');
			$episode_list.eq(page_next).find('img').attr('src',data_img_src_next);
			$episode_list.eq(page_next).find('img').attr('data_img_src_chk','success');
		}
	}
}

/* set_bookmark */
function set_bookmark(comics){
	var bookmark_star = $("#mybookmark").find("i");

	$.ajax({
		url: nm_url+"/ajax/mybookmark.php",
		dataType: "json",
		type: "POST",
		data: {"cm_no": comics},
		success : function(data) {
			if(data.state == 0){
				if(data.mode == 'in') {
					bookmark_star.removeClass("fa-star-o");
					bookmark_star.addClass("fa-star");
				} else {
					bookmark_star.removeClass("fa-star");
					bookmark_star.addClass("fa-star-o");
				} // end else
				toast(data.msg);
			}else{
				alertBox(data.msg);
			}
		}
	});
} // set_bookmark