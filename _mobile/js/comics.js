$(document).ready(function() {
	
	/* 코믹스 메인 이미지 투명처리 */
	var dheaderTop = $("#comics .detail_header > .d_h_con").offset().top;
	var dheaderAlpha = function(){

		var scrollTop = $(window).scrollTop() + dheaderTop;
		dheaderTop = dheaderTop + 5;
		// console.log(scrollTop);
		// console.log(dheaderTop);
		if (scrollTop > dheaderTop) {
			$("#comics .detail_header > .d_h_top").css({"opacity":"0.6"});
		} else {
			$("#comics .detail_header > .d_h_top").css({"opacity":"1"});
		}
	};

	dheaderAlpha();

	$(window).scroll(function() {
		dheaderAlpha();
	});

	/* 각각의 에피소드체크박스 처리 */
	$(".episode_list a").click(function(e){
		var $episode_list = $('#comics .episode_list');
		var $sin_allpay = $('#comics #sin_allpay input[type="checkbox"]');

		var $this_check_type = $(this).parent();
		
		if($episode_list.hasClass('detail_chk_on') == true){
			if($this_check_type.hasClass('check_on') == true){				
				if($(this).find('input[type="checkbox"]').is(':checked') == true){
					$(this).find('input[type="checkbox"]').prop('checked', false);
				}else{
					$(this).find('input[type="checkbox"]').prop('checked', true);
				}
			}
			sin_pay_calc(sin_allpay_arr, total_point);	
			a_click_false();
		}
	});

	/* 즐겨찾기 들어갈 때 유무 처리 */
	if($("#mybookmark").data("mybookmark") != 0) {
		var bookmark_star = $("#mybookmark").find("i");

		bookmark_star.removeClass("fa-star-o");
		bookmark_star.addClass("fa-star");
	} // end if
});

/* 에피소드 링크+체크박스처리 */
/*
function get_episode_url(comics, episode){
	nm_episode_url = nm_comicsview + '?comics=' + comics + '&episode=' + episode;
	location.href = nm_episode_url;
	a_click_false();
}
*/
/* 즐겨찾기 처리 */
function set_bookmark(comics){
	var bookmark_star = $("#mybookmark").find("i");

	$.ajax({
		url: nm_url+"/ajax/mybookmark.php",
		dataType: "json",
		type: "POST",
		data: {"cm_no": comics},
		success : function(data) {
			if(data.state == 0){
				if(data.mode == 'in') {
					bookmark_star.removeClass("fa-star-o");
					bookmark_star.addClass("fa-star");
				} else {
					bookmark_star.removeClass("fa-star");
					bookmark_star.addClass("fa-star-o");
				} // end else
			}else{
				alertBox(data.msg);
			}
		}
	});
} // set_bookmark

/* ///////// 유료화 전체구매하기 ///////// */
/* 유료화 전체구매하기-체크박스 활성화 */
function check_enalble(){
	detail_check(true);	
	episode_btn_check = false;
}

/* 유료화 전체구매하기-체크박스 비활성화 */
function check_cancel(){
	detail_check(false);
	episode_btn_check = true;
}

function detail_check(bool){	
	// 반대로 적용 될 변수
	if(bool == true){ bool_reverse = false;	}
	else{ bool_reverse = true; }

	// 유료화 전체구매하기 view
	var $all_buy = $('#comics .all_buy');

	// 전체선택 view
	var $detail_allby = $('#comics .detail_allby');
	$detail_allby.find('input[type="checkbox"]').prop('checked', bool);

	// 에피소드선택 view
	var $detail_chk = $('#comics .check_on .detail_chk');
	var $episode_list = $('#comics .episode_list');

	// 에피소드선택 checkbox
	var $detail_checkbox = $('#comics .check_on .detail_chk input[type="checkbox"]');
	$detail_checkbox.prop('checked', bool);

	if(bool == true){
		$all_buy.hide();
		$detail_allby.show();
		$detail_allby.find('.detail_chk').show();

		$detail_chk.show();
	
		if($episode_list.hasClass('detail_chk_on') == bool_reverse){
			$episode_list.addClass('detail_chk_on');
		}
	}else{
		$all_buy.show();
		$detail_allby.hide();
		$detail_allby.find('.detail_chk').hide();

		$detail_chk.hide();
	
		if($episode_list.hasClass('detail_chk_on') == bool_reverse){
			$episode_list.removeClass('detail_chk_on');
		}
	}
	sin_pay_calc(sin_allpay_arr, total_point);	
}

function sin_allpay(){
	var $sin_allpay = $('#comics #sin_allpay input[type="checkbox"]');
	var $detail_checkbox = $('#comics .detail_chk input[type="checkbox"]');
	$detail_checkbox.prop('checked', $detail_checkbox.prop('checked'));
	sin_pay_calc(sin_allpay_arr, total_point);	
}

/* 유료화 전체구매하기-체크박스 구매 */
function check_buy(){
	var comics = $('#cm_no').val();
	var $episode_list = $('#comics .check_on');
	var episode_arr = Array();
	$episode_list.each(function( idx, val ){
		if($(this).find('.detail_chk input[type="checkbox"]').is(':checked') == true){
			episode_arr.push($(this).find('.detail_chk input[type="checkbox"]').val());
		}
	});

	var total_episode_cnt = episode_arr.length; // 총 구매 화수
	var total_episode_sum = $("#cach_point_sum").text(); // 총 구매 가격

	if(total_episode_cnt > 0) {
		nm_comicsview_url = nm_url  + '/proc/comicsview.php?comics=' + comics + '&episode=' + episode_arr.join(","); 
		txt = total_episode_sum+"땅콩을 사용하여 총 "+total_episode_cnt+"화를 구매하시겠습니까?<br/>";
		txt += "구매한 작품은 내 서재에 보관됩니다.";
		confirmBox(txt, goto_replace_url, {url:nm_comicsview_url});
	} else {
		alertBox("선택된 화가 없습니다.");
		return false;
	} // end else
}

/* 유료화 전체구매하기-충전하기 */
function check_recharge(){
	nm_recharge_url = nm_url + '/recharge.php';
	location.href = nm_recharge_url;
}

/* 체크박스시 전체구매시 추가지급 계산 */
function sin_pay_calc(sin_allpay_arr, total_point){ 
	var $episode_list = $('#comics .check_on');
	var episode_list_count = $episode_list.length;
	var chapter_count = 0;
	var cach_point_sum = 0;
	var point_service = 0;	
	var $detail_allby = $('#comics .detail_allby');
	
	// 화 리스트
	$episode_list.each(function( idx, val ){
		if($(this).find('.detail_chk input[type="checkbox"]').is(':checked') == true){
			chapter_count++; 
			cach_point_sum+= Number($(this).find('.detail_chk input[type="checkbox"]').attr('data-cash_point'));
		}
	});
	
	// 계산
	var sin_allpay_pre = 0;
	for(i=0; i<sin_allpay_arr.length; i++){
		if(i>0){ sin_allpay_pre = sin_allpay_arr[i-1][0];} // 이전값 가져오기

		if(sin_allpay_arr[i][1] == 'n'){
			if(sin_allpay_pre <= chapter_count && chapter_count < sin_allpay_arr[i][0] ){
				point_service = sin_allpay_arr[i][2];
			}
		}else{
			if(sin_allpay_arr[i][0] <= chapter_count){
				point_service = sin_allpay_arr[i][2];
			}			
		}
	}

	// textview
	$('#chapter_count').text(chapter_count);
	$('#cach_point_sum').text(cach_point_sum);
	$('#point_service').text(point_service);

	// 전체선택
	if(episode_list_count == chapter_count){
		$detail_allby.find('input[type="checkbox"]').prop('checked', true);
	}else{
		$detail_allby.find('input[type="checkbox"]').prop('checked', false);
	}

	// 구매하기 버튼 땅콩부족시 충전하기로 변경
	if(total_point < cach_point_sum){
		$('#comics .detail_allby .d_a_ok button').attr('onclick','check_recharge();');
		$('#comics .detail_allby .d_a_ok button').text('충전하기');
	}else{
		$('#comics .detail_allby .d_a_ok button').attr('onclick','check_buy();');
		$('#comics .detail_allby .d_a_ok button').text('구매하기');
	}
}

/* 결제전 묻기 */
function comics_episode_goto_url(comics_no, episode_no, episode_pay, episode_txt){

	if(episode_btn_check == false){
		return false;
	}else{
		episode_url = nm_comicsview+"?comics="+comics_no+"&episode="+episode_no;

		if(Number(episode_pay) > 0){
			txt = episode_txt+"를 보시려면 결제가 필요합니다.<br/>";
			txt+= episode_pay+nm_cash_point_unit_ko+"을 사용하여 결제하시겠습니까?<br/>";
			txt+= "구매한 작품은 내 서재에 보관됩니다.";
			confirmBox(txt, goto_replace_url, {url:episode_url});
		}else{
			document.location.replace(episode_url);
		}
	}
}