$(function() {
	var mb_email = $("#mem_email").text();
	var mb_nick = $("#mem_nick").text();
	
	/* MyInfo */
	$("#mem_pw").click(function(){
		var repass = $(".re_pw").css('display');
		/* none, block */
		if(repass == "none"){
			$(".re_pw").show();
			$("#mem_pw").addClass("on");
		}else{
			$(".re_pw").hide();
			$("#mem_pw").removeClass("on");
		}
	});

	$("#mem_mail_ok").click(function() {
		$(this).addClass('on');
		$("#mem_mail").val('y');
		$("#mem_mail_no").removeClass('on');
	});
	$("#mem_mail_no").click(function() {
		$(this).addClass('on');
		$("#mem_mail").val('n');
		$("#mem_mail_ok").removeClass('on');
	});
	
	/* 이메일 */
	$("#mb_mod_email").click(function() {
		var remail = $("#mb_email").css("display");

		/* none, block */
		if(remail == "none"){
			$(this).addClass('on');
			$("#mb_email").show();
			$("#mb_email").focus();
			$("#mem_email").hide();
			$(this).text("취소");
		}else{
			$(this).removeClass('on');
			$("#mb_email").hide();
			$("#mb_email").val(mb_email);
			$("#mem_email").show();
			$(this).text("변경");
		}	
	});
	
	/* 닉네임 */
	$(".mb_write_nick").click(function() {
		var renick = $("#mb_nick").css("display");
		/* none, block */
		if(renick == "none"){
			$(this).addClass('on');
			$("#mb_nick").show();
			$("#mb_nick").focus();
			$("#mem_nick").hide();
			$(this).text("취소");
		}else{
			$(this).removeClass('on');
			$("#mb_nick").hide();
			$("#mb_nick").val(mb_nick);
			$("#mem_nick").show();
			$(this).text("변경");
		}	
	});
	
	/* MyInfo Paging */
	var paging_arr = new Array("income", "income_event", "expen", "ask");

	for(var i=0; i<paging_arr.length; i++) {
		paging(paging_arr[i]);
	} // end for

	/* MyComics */
	mycomics();
	
	/* 구매목록 단행본 메인 이미지 투명처리 */	
	if($("#comics .detail_header > .d_h_con").length > 0) {
		var dheaderTop = $("#comics .detail_header > .d_h_con").offset().top;
		var dheaderAlpha = function(){

			var scrollTop = $(window).scrollTop() + dheaderTop;
			dheaderTop = dheaderTop + 5;
			// console.log(scrollTop);
			// console.log(dheaderTop);
			if (scrollTop > dheaderTop) {
				$("#comics .detail_header > .d_h_top").css({"opacity":"0.6"});
			} else {
				$("#comics .detail_header > .d_h_top").css({"opacity":"1"});
			}
		};

		dheaderAlpha();

		$(window).scroll(function() {
			dheaderAlpha();
		});
	} // end if

	/* MyLeave */
	$('.form_field_text').blur(function(){
		if($(this).val() != ''){
			$(this).next().addClass('completed');
		}else{
			$(this).next().removeClass('completed');
		}
	});
});

// alertBox 이용함수
function myinfo_focus(obj){
	$(obj.input_val1).val("");
	$(obj.input_val2).val("");
	$(obj.input_name).focus();
}

function myinfo_submit() {
	var pw1 = $('[name="pw1"]').val();
	var pw2 = $('[name="pw2"]').val();
	var pw3 = $('[name="pw3"]').val();

	if(pw1 != "") {
		if(pw2 == "") {
			alertBox("변경할 비밀번호를 입력해주세요!", myinfo_focus, {input_name:'[name="pw2"]'});
			return false;
		} else if(pw2 != "" && pw3 == "") {
			alertBox("비밀번호 확인을 입력해주세요!", myinfo_focus, {input_name:'[name="pw3"]'});
			return false;
		} else if((pw2 != "" && pw3 != "") && (pw2 != pw3)) {
			alertBox("변경될 비밀번호가 일치하지 않습니다.", myinfo_focus, {input_name:'[name="pw2"]',input_val1:'[name="pw2"]',input_val2:'[name="pw3"]'});
			return false;
		} // end else if
	} else {
		if(pw2 != "" || pw3 != "") {
			alertBox("현재 비밀번호를 입력해주세요!", myinfo_focus, {input_name:'[name="pw1"]'});
			return false;
		} else if(pw2 == "" && pw1 != "") {
			alertBox("변경할 비밀번호를 입력해주세요!", myinfo_focus, {input_name:'[name="pw2"]'});
			return false;
		} else if(pw2 != "" && pw3 == "") {
			alertBox("비밀번호 확인을 입력해주세요!", myinfo_focus, {input_name:'[name="pw3"]'});
			return false;
		} // end else if
	} // end else
} // myinfo_submit

/* paging */
function paging(arr_name) {
	
	var start_view = 0;
	var end_view = 4;
	var arr_cnt = $("#"+arr_name+"_cnt").val();
	var total_page = Math.ceil(arr_cnt/5);
	var now_page = 1;

	if(arr_cnt > 0) {
		$("#"+arr_name+"_prev").css("background-color", "#e2e2e2");
		$("#"+arr_name+"_prev").css("cursor", "auto");

		if(total_page == 1) {
			$("#"+arr_name+"_next").css("background-color", "#e2e2e2");
			$("#"+arr_name+"_next").css("cursor", "auto");
		} // end if

		for(var j=0; j<=arr_cnt; j++) {
			$("#"+arr_name+"_"+j).hide();
		} // end for

		for(var i=start_view; i<=end_view; i++) {
			$("#"+arr_name+"_"+i).show();
		} // end for

		$("#"+arr_name+"_next").click(function() {
			if(now_page == total_page) {
				return false;
			} else {
				start_view = start_view + 5;
				now_page++;

				if(now_page > 1) {
					$("#"+arr_name+"_prev").css("background-color", "#FFFFFF");
					$("#"+arr_name+"_prev").css("cursor", "pointer");
				} // end if

				for(var j=0; j<=arr_cnt; j++) {
					$("#"+arr_name+"_"+j).hide();
				} // end for

				for(var i=start_view; i<=start_view+4; i++) {
					$("#"+arr_name+"_"+i).show();
				} // end for

				if(now_page == total_page) {
					$("#"+arr_name+"_next").css("background-color", "#e2e2e2");
					$("#"+arr_name+"_next").css("cursor", "auto");
				} // end if
			} // end else
		});

		$("#"+arr_name+"_prev").click(function() {
			if(now_page <= 1) {
				return false;
			} else {
				start_view = start_view - 5;
				now_page--;

				if(now_page < total_page) {
					$("#"+arr_name+"_next").css("background-color", "#FFFFFF");
					$("#"+arr_name+"_next").css("cursor", "pointer");
				} // end if
			
				for(var j=0; j<=arr_cnt; j++) {
					$("#"+arr_name+"_"+j).hide();
				} // end for

				for(var i=start_view; i<=start_view+4; i++) {
					$("#"+arr_name+"_"+i).show();
				} // end for

				if(now_page <= 1) {
					$("#"+arr_name+"_prev").css("background-color", "#e2e2e2");
					$("#"+arr_name+"_prev").css("cursor", "auto");
				} // end if
			} // end else
		});
	} else {
		$("#"+arr_name+"_prev").css("background-color", "#e2e2e2");
		$("#"+arr_name+"_prev").css("cursor", "auto");
		$("#"+arr_name+"_next").css("background-color", "#e2e2e2");
		$("#"+arr_name+"_next").css("cursor", "auto");
	} // end else
} // end paging

function mycomics() {
	
	var view_list = $("[name='comics_check[]']").length;
	var check_count = 0;

	// 비 활성화 중 삭제버튼 눌렀을 시
	$(".lib_del_btn").click(function() {
		if($(".lib_del_chk").is(":hidden") && $(".lib_cancel_btn").is(":hidden") && $(".lib_chk").is(":hidden")) { // 삭제 비 활성화일 때
			$(".lib_del_chk").show();
			$(".lib_cancel_btn").show();
			$(".lib_chk").show();
			$("#lib_delete").prop("checked", false);
			$("[name='comics_check[]']").prop("checked", false);
		} else { // 삭제 활성화일 때
			if(check_count == 0) {
				alert("삭제할 작품을 선택하세요!");
				return false;
			} else {
				txt="정말 삭제하시겠습니까?";
				confirmBoxYN(txt, document.mycomics);
			} // end else
		} // end else
	});

	// 취소 버튼
	$(".lib_cancel_btn").click(function() {
		if($(".lib_del_chk").is(":visible") && $(".lib_cancel_btn").is(":visible") && $(".lib_chk").is(":visible")) { // 삭제 활성화일 때
			$(".lib_del_chk").hide();
			$(".lib_cancel_btn").hide();
			$(".lib_chk").hide();
			$(".lib_list").removeClass("up");
		} // end if
	});
	
	// 전체 선택, 해제
	$("#lib_delete").click(function() {
		if($("#lib_delete").prop("checked")) {
			$("[name='comics_check[]']").prop("checked", true);
			$(".lib_list").addClass("up");
			check_count = view_list;
		} else {
			$("[name='comics_check[]']").prop("checked", false);
			$(".lib_list").removeClass("up");
			check_count = 0;
		} // end else
	});

	// 전체 선택, 해제 자동 확인
	if(view_list == check_count) {
		$("#lib_delete").prop("checked", true);
	} else {
		$("#lib_delete").prop("checked", false);
	} // end else

	// 개인별 체크, 해제	
	$(".lib_list").click(function() { 
		if($(".lib_cancel_btn").is(":visible")) {
			if($(this).find('input[type="checkbox"]').is(':checked') == true) {
				$(this).find('input[type="checkbox"]').prop('checked', false);
				$(this).removeClass("up");
				check_count--;
				if(view_list != check_count) { $("#lib_delete").prop("checked", false); } // end if
			} else {
				$(this).find('input[type="checkbox"]').prop('checked', true);
				$(this).addClass("up");
				check_count++;
				if(view_list == check_count) { $("#lib_delete").prop("checked", true); } // end if
			} // end else

			a_click_false();
		} // end if
	});
} // mycomics

/****************** 회원 탈퇴 ******************/
function mycomics_submit() {

} // mycomics_submit

function myleave(check) {
	if(check == "go") {
		$("#myleave_form").submit();
	} else {
		history.go(-1);
	}
} // myleave

function myleave_submit() {
	var sns_type = $('#mb_sns_check').val();
	
	if(sns_type == "n") {
		var pw1 = $('[name="pw1"]').val();
		var pw2 = $('[name="pw2"]').val();

		if((pw1 == "" && pw2 == "") || (pw1 == "" && pw2 != "")) {
			alertBoxFocus("비밀번호를 입력해주세요!", $('[name="pw1"]'));
			return false;
		} else if(pw1 != "" && pw2 == "") {
			alertBoxFocus("비밀번호 확인을 입력해주세요!", $('[name="pw2"]'));
			return false;
		} else if((pw1 != "" && pw2 != "") && (pw1 != pw2)) {
			alertBox("비밀번호 확인과 일치하지 않습니다!");
			$('[name="pw1"]').val("");
			$('[name="pw2"]').val("");
			$('.myp_dropout_pw').find('label').removeClass();
			return false;
		} // end else if
	} // end if
} // myleave_submit


/* 결제전 묻기 (일반 화 리스트) */
function comics_episode_goto_url(comics_no, episode_no, episode_pay, episode_txt){

	episode_url = nm_comicsview+"?comics="+comics_no+"&episode="+episode_no;

	if(Number(episode_pay) > 0){
		txt = episode_txt+"를 보시려면 결제가 필요합니다.<br/>";
		txt+= episode_pay+nm_cash_point_unit_ko+"을 사용하여 결제하시겠습니까?<br/>";
		txt+= "구매한 작품은 내 서재에 보관됩니다.";
		confirmBox(txt, goto_replace_url, {url:episode_url});
	}else{
		document.location.replace(episode_url);
	}
}

/* 결제전 묻기 (내 서재 소장 화 리스트) */
function comics_myepisode_goto_url(comics_no, episode_no, episode_pay, free_pay, episode_txt, mysecret){

	episode_url = nm_comicsview+"?comics="+comics_no+"&episode="+episode_no+"&epay="+episode_pay+"&fpay="+free_pay+"&mysecret="+mysecret;
	episode_view = nm_comicsview+"?comics="+comics_no+"&episode="+episode_no;
	txt = "";

	var confirmview_mode = 'free';

	if(Number(episode_pay) > 0) { // 유료 화 일때
		txt += episode_pay+nm_cash_point_unit_ko+"을 사용하여 영구소장 하시겠습니까?";
		confirmview_mode = 'pay';
	} else if(Number(free_pay) > 0) { // 무료 화 일때
		txt += free_pay+nm_cash_point_unit_ko+"을 사용하여 영구소장 하시겠습니까?<br/>";
		txt += "선택하신 화는 무료로도 볼 수 있는 화 입니다.";
	} else { // 공지 및 휴재 등
		txt += "무료로 소장하시겠습니까?";
	} // end else
	
	if(confirmview_mode == 'pay'){
		confirmBox(txt, goto_replace_url, {url:episode_url});
	}else{
		confirmView(txt, goto_replace_url, {url:episode_url}, {url:episode_view});
	}
}