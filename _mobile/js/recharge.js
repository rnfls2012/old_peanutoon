$(document).ready(function() {
	var $crp_no = $("input:radio[name='crp_no']");	
	var payway_limit = '';
	var payway_limit_arr = new Array();
	var data_crp_no = '';
	var data_crp_no_attr = '';
	var data_crp_no_attr_arr = new Array();

	$crp_no.each(function( idx, val ){
		payway_limit = '';
		data_crp_no = '';
		if($(this).attr('data-payway_limit') != ''){
			payway_limit = $(this).attr('data-payway_limit');
			payway_limit_arr = payway_limit.split(',');
			for(var i=0; i<payway_limit_arr.length; i++){
				data_crp_no = $('#recharge .'+payway_limit_arr[i]).attr('data-crp_no');
				if(data_crp_no != '' & typeof data_crp_no != 'undefined'){ data_crp_no = data_crp_no + ","+ idx; }
				else{ data_crp_no = idx; }
				$('#recharge .'+payway_limit_arr[i]).attr('data-crp_no',data_crp_no);
			}
		}
	});

	var $payway_no = $("input:radio[name='payway_no']");
	$payway_no.click(function(){
		if($(this).hasClass('mobx') && $("#clt_mobx").val() != ""){
			$("#mar_mobx_guide").show();
		}else{
			$("#mar_mobx_guide").hide();
		}

		if($(this).hasClass('card') && $("#clt_card").val() != ""){
			$("#mar_card_guide").show();
		}else{
			$("#mar_card_guide").hide();
		}

		if($(this).hasClass('payco') && $("#clt_payco").val() != ""){
			$("#mar_payco_guide").show();
		}else{
			$("#mar_payco_guide").hide();
		}

		if($(this).hasClass('toss') && $("#clt_toss").val() != ""){
			$("#mar_toss_guide").show();
		}else{
			$("#mar_toss_guide").hide();
		}

		if($(this).hasClass('sccl') && $("#clt_sccl").val() != ""){
			$("#mar_sccl_guide").show();
		}else{
			$("#mar_sccl_guide").hide();
		}

		if($(this).hasClass('schm') && $("#clt_schm").val() != ""){
			$("#mar_schm_guide").show();
		}else{
			$("#mar_schm_guide").hide();
		}

		if($(this).hasClass('scbl') && $("#clt_scbl").val() != ""){
			$("#mar_scbl_guide").show();
		}else{
			$("#mar_scbl_guide").hide();
		}

		if($(this).hasClass('acnt') && $("#clt_acnt").val() != ""){
			$("#mar_acnt_guide").show();
		}else{
			$("#mar_acnt_guide").hide();
		}
		
		$("#recharge .mar_section02 ol li").show();
		if($(this).attr('data-crp_no') != '' & typeof $(this).attr('data-crp_no') != 'undefined'){
			data_crp_no_attr = $(this).attr('data-crp_no');
			data_crp_no_attr_arr = data_crp_no_attr.split(',');
			for(var j=0; j<data_crp_no_attr_arr.length; j++){
				// 결제금액 확인 초기화
				$("input:radio[name='crp_no']").prop("checked", false);
				$('.config_recharge_price_'+data_crp_no_attr_arr[j]).hide();
			}
		}
	});
	
	/* 쿠폰 */
	$(".code").keyup(function(e) {
		var text_id = $(this).attr("id");
		var text_length = $(this).val().length;
		var text_max_length = $(this).attr("maxLength");

		if(e.keyCode == 8) { // 백 스페이스 였을때
			if(text_id != "code1" && text_length == 0) {
				$(this).prev().focus();
			} // end if
		} else { // 일반 입력시
			if((text_length == text_max_length) && text_id != "code4") {
				$(this).next().focus();
			} // end if
			$(this).val($(this).val().toUpperCase());
		} // end else
	});
	
	// 쿠폰함링크
	$("#recharge .coupondc_btn").click(function(){
		var coupondc_href = $(this).data('href');
		var mb_crp_no = $('input:radio[name="crp_no"]:checked').val();
		var mb_payway_no = $('input:radio[name="payway_no"]:checked').val();
		var mb_coupondc = $('#coupondc').val();

		var coupondc_url = coupondc_href+"?v="+nm_ver;
		if(typeof mb_crp_no != 'undefined'){ coupondc_url+= "&mb_crp_no="+mb_crp_no; }
		if(typeof mb_payway_no != 'undefined'){ coupondc_url+= "&mb_payway_no="+mb_payway_no; }
		if(typeof mb_coupondc != 'undefined' && coupondc_href.search("recharge.php") == 0){ coupondc_url+= "&mb_coupondc="+mb_coupondc; }
		link(coupondc_url);
	});
});


function recharge_payway_submit(device){
    var para = "";

	var form = document.recharge_form;

	/* 필수항목 체크 */
	var unit_ko = $('.mar_section02 h3').text();
	var payway_ko = $('.mar_section03 h3').text();
	
	var crp_check = false;
	var payway_check = false;
  var mb_agree = $("[name='mb_agree']").val(); // 1회 최초 동의 - 180308 (지완)

	var $crp_no = $("input:radio[name='crp_no']");
	var $payway_no = $("input:radio[name='payway_no']");
  var is_checked  	= $("#pay_agree").prop("checked"); // 1회 최초 동의 - 180308 (지완)

	$crp_no.each(function( idx, val ){
		if($(this).is(":checked") == true){ crp_check = true; }
	});

	$payway_no.each(function( idx, val ){
		if($(this).is(":checked") == true){ payway_check = true; }
	})

	if(crp_check == false){ alertBox(unit_ko+' 체크해주세요'); return false; }
	if(payway_check == false){ alertBox(payway_ko+' 체크해주세요'); return false; }
    if ( is_checked === false ) { alertBox('이용약관에 동의 하지 않았습니다.'); return false; } // 1회 최초 동의 - 180308 (지완)
	
	/*
    if(typeof mb_agree !== 'undefined'){
        para+="&mb_agree="+mb_agree;

        var newInput =document.createElement('input');
        newInput.setAttribute('type', 'hidden');
        newInput.setAttribute('name', 'mb_agree');
        newInput.setAttribute('value', mb_agree);

        form.appendChild(newInput);
    }
	*/

	if(device == 'pc'){
		// var serialize =  $('#recharge_form').serialize(); // NHN KCP 보안패치 17-09-08
		// var action =  $('#recharge_form').attr('action'); // NHN KCP 보안패치 17-09-08

		var crp_no = $('input:radio[name="crp_no"]:checked').val();
		var payway_no = $('input:radio[name="payway_no"]:checked').val();
		var coupondc_no = $('#coupondc').val();

		para+="crp_no="+crp_no;
		para+="&payway_no="+payway_no;
		if(typeof mb_agree !== 'undefined'){ para+="&mb_agree="+mb_agree; }
		if(typeof coupondc_no != 'undefined'){ para+="&coupondc="+coupondc_no; }		

		/* 181001 */
		var cf_pg_test_ctrl = $('input:checkbox[name="cf_pg_test_ctrl"]:checked').val();
		if(typeof cf_pg_test_ctrl != 'undefined'){ para+="&cf_pg_test_ctrl="+cf_pg_test_ctrl; }

		var cf_pg_pass_ctrl = $('input:checkbox[name="cf_pg_pass_ctrl"]:checked').val();
		if(typeof cf_pg_pass_ctrl != 'undefined'){ para+="&cf_pg_pass_ctrl="+cf_pg_pass_ctrl; }

		var action =  $('#recharge_form').attr('action');
		var order_Url = action+'?'+para;

		window.open(order_Url, 'recharge', 'top=100, left=300, width=757px, height=557px, resizble=no, scrollbars=yes');
	}else{
		form.submit();
	}
}

function coupon_submit() {
	var code = $("#code1").val()+$("#code2").val()+$("#code3").val()+$("#code4").val();
	var strReg = /^[A-Za-z0-9]+$/;

	if(!strReg.test(code) || code.length < 16) {
		alertBox("잘못된 코드 입니다!");
		return false;
	} // end if
		
} // coupon_submit