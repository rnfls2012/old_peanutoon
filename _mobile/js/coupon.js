$(function() {
    var paging_arr = new Array("coupon", "used");

    for(var i=0; i < paging_arr.length; i++) {
        if (paging_arr[i] == 'coupon') {
            paging(paging_arr[i], 5);
        } else if (paging_arr[i] == 'used') {
            paging(paging_arr[i], 3);
        } else {
            paging(paging_arr[i]);
        }
    } // end for
});


function paging(param_name, view_per_page) {

    var start_view = 0;
    var end_view = view_per_page-1;
    var arr_cnt = parseInt($("#"+param_name+"_cnt").val());
    var total_page = Math.ceil(arr_cnt / view_per_page);
    var now_page = 1;
    console.log(total_page);
    console.log(param_name);
    if(arr_cnt > 0) {
        $("#"+param_name+"_prev").css("background-color", "#e2e2e2");
        $("#"+param_name+"_prev").css("cursor", "auto");
        if(total_page == 1) {
            $("#"+param_name+"_next").css("background-color", "#e2e2e2");
            $("#"+param_name+"_next").css("cursor", "auto");
        } // end if

        for(var j=0; j<=arr_cnt; j++) {
            $("#"+param_name+"_"+j).hide();
        } // end for

        for(var i=start_view; i<=end_view; i++) {
            $("#"+param_name+"_"+i).show();
        } // end for

        $("#"+param_name+"_next").click(function() {
            if(now_page == total_page) {
                return false;
            } else {
                start_view += view_per_page;
                now_page++;

                if(now_page > 1) {
                    $("#"+param_name+"_prev").css("background-color", "#FFFFFF");
                    $("#"+param_name+"_prev").css("cursor", "pointer");
                } // end if

                for(var j=0; j<=arr_cnt; j++) {
                    $("#"+param_name+"_"+j).hide();
                } // end for

                for(var i=start_view; i<start_view + view_per_page; i++) {
                    $("#"+param_name+"_"+i).show();
                } // end for

                if(now_page == total_page) {
                    $("#"+param_name+"_next").css("background-color", "#e2e2e2");
                    $("#"+param_name+"_next").css("cursor", "auto");
                } // end if
            } // end else
        });

        $("#"+param_name+"_prev").click(function() {
            if(now_page <= 1) {
                return false;
            } else {
                start_view -= view_per_page;
                now_page--;

                if(now_page < total_page) {
                    $("#"+param_name+"_next").css("background-color", "#FFFFFF");
                    $("#"+param_name+"_next").css("cursor", "pointer");
                } // end if

                for(var j=0; j<=arr_cnt; j++) {
                    $("#"+param_name+"_"+j).hide();
                } // end for

                for(var i=start_view; i<start_view + view_per_page; i++) {
                    $("#"+param_name+"_"+i).show();
                } // end for

                if(now_page <= 1) {
                    $("#"+param_name+"_prev").css("background-color", "#e2e2e2");
                    $("#"+param_name+"_prev").css("cursor", "auto");
                } // end if
            } // end else
        });
    } else {
        $("#"+param_name+"_prev").css("background-color", "#e2e2e2");
        $("#"+param_name+"_prev").css("cursor", "auto");
        $("#"+param_name+"_next").css("background-color", "#e2e2e2");
        $("#"+param_name+"_next").css("cursor", "auto");
    } // end else
} // end paging