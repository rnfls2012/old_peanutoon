function prize_page_submit(){
	var efm_form = document.forms["prize_form_page"];

	// 성명
	if(efm_form.name.value == ''){ efm_form.name.focus(); return false; }

	// 휴대폰번호
	if(efm_form.phone.value == ''){ efm_form.phone.focus(); return false; }

	// 배송지정보
	if(efm_form.address.value == ''){ efm_form.address.focus(); return false; }

	// 우편번호
	if(efm_form.postal.value == ''){ efm_form.postal.focus(); return false; }

	if($('#term_ok').prop("checked") == false){ 
		alertBox("동의하셔야 합니다.");
		efm_form.postal.focus(); return false; 
	}

	efm_form.submit();
}