$(function() {
	var pointpark_frm = document.pointpark_from; 
	pointpark_frm.submit();
});

	
function iframe_auto_resize(){
	var width_max = 780;
	var iframe_height_max = 900;
	var iframe_height = 0;
	
	var this_w = '';
	if(navigator.userAgent.indexOf('Chrome') != -1 || navigator.userAgent.indexOf('Safari') != -1){
		this_w = document.body.scrollWidth;
	}
	else{
		this_w = document.documentElement.scrollWidth;
	}
	this_w = parseInt(this_w);
	if(this_w > width_max){
		iframe_height = iframe_height_max + parseInt((this_w - width_max) / 4);		
	}else{
		iframe_height = iframe_height_max;
	}
	$('#pointpark #pointpark_iframe').height(iframe_height);
}