$(function(){
	/* ////////////////////// login ////////////////////// */
	$('.form_field_text').blur(function(){
		if($(this).val() != ''){
			$(this).next().addClass('completed');
		}else{
			$(this).next().removeClass('completed');
		}
	});

	/* ////////////////////// join ////////////////////// */	
	$('#regi_agree').click(function(e){ /* Registration Check Form */
		var agree_checked = '';
		agree_checked = $(this).is(":checked");
			$('#regi_terms').prop("checked", agree_checked);
			$('#regi_pinfo').prop("checked", agree_checked);
			$('#regi_event').prop("checked", agree_checked);
	});

	/* join */
	$('#regi_terms, #regi_pinfo, #regi_event').click(function(e){
		var terms_checked = '';
		terms_checked = $(this).is(":checked");
		if(terms_checked == false){
			$('#regi_agree').prop("checked", false);
		}

		if($('#regi_terms').is(":checked") && $('#regi_pinfo').is(":checked") && $('#regi_event').is(":checked")) {
			$('#regi_agree').prop("checked", true);
		} else {
			$('#regi_agree').prop("checked", false);
		} // end else
	});
	
	/* ID/패스워드 찾기 - 이메일btn */
	$("#find_email_idpw").click(function(){
		$("#find_email_idpw_txt").toggle();
	});
	
	/* SNS 가입취소 */
	$(".login_no").click(function(){
		link(nm_url);
	});
	
	/* SNS 로그인 */
	$(".login_or").click(function(){
		loading();
	});
});

function ctlogin_submit(){
	
} // ctlogin_submit



function ctjoin_submit() {
	var regi_terms = $('#regi_terms').is(":checked");
	var regi_pinfo = $('#regi_pinfo').is(":checked");
	var regi_event = $('#regi_event').is(":checked");
	var join_id = $('#join_id').val();
	var join_pw = $('#join_pw').val();
	var email_validity = /^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/;

	/* 아이디 & 비밀번호 */
	if(join_id == "" || join_pw == "") {
		var empty_var = "join_pw";
		var msg = "비밀번호를 ";
		if(join_id == "") {	
			empty_var = "join_id"; 
			msg = "아이디를 ";
		} // end if
		
		alertBoxFocus(msg+"입력해 주세요!", $('#'+empty_var));
		return false;
	} // end if

	/* 비밀번호 4자 이상 */
	if(join_pw.length < 4) {
		alertBoxFocus("비밀번호는 4자 이상 입력해 주세요!", $('#join_pw'));
		$('#join_pw').val("");
		return false;
	} // end if

	/* 아이디 Email 형식(유효성 검사) */
	if(email_validity.test(join_id) == false) {
		alertBoxFocus("이메일 형식의 아이디를 입력해 주세요!", $('#join_id'));
		$('#join_id').val("");
		return false;
	} // end if
	
	/* 약관 */
	if(regi_terms == false || regi_pinfo == false) {
		alertBox("약관에 동의하여 주세요!.");
		return false;
	} // end if
} // ctjoin_submit

/* ctmyfindpw_direct */
function ctmyfindpw_direct_submit(){
	
} // ctmyfindpw_direct_submit



function ctjoinsns_page_submit() {
	var regi_terms = $('#regi_terms').is(":checked");
	var regi_pinfo = $('#regi_pinfo').is(":checked");
	var regi_event = $('#regi_event').is(":checked");

	/* 약관 */
	if(regi_terms == false || regi_pinfo == false) {
		alertBox("약관에 동의하여 주세요!.");
		return false;
	} // end if

} // ctjoin_submit