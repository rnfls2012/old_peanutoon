$(function(){	
	if($("#search_txt").length > 0){
		$( "#search_txt" ).autocomplete({
			source : function( request, response ) {
				 $.ajax({
						type: 'POST',
						url: nm_url+"/ajax/search.php",
						dataType: "json",
						data: { search_txt: request.term },
						success: function(data) {
							if(data == null){
								data = {}
							}
							response( 
								$.map(data, function(item) {
									return {
										value: item.value,
										editor: item.editor,
										link: item.link,
										img: item.img,
										label: item.label
									}
								})
							);
						}
				   });
				},
			minLength: 1,
			focus: function( event, ui ) {
				return false;
			},
			select: function( event, ui ) {
			},
			open: function(){
			},
			close: function(){
				disable=false; $(this).focus();
			}
		}).data("uiAutocomplete")._renderItem = function(ul, item){
			var search_html_arr = '<a href="'+item.link+'" >';
			search_html_arr+= '<img src="'+item.img+'" alt="'+item.label+'" />';
			search_html_arr+= '<span><strong>';
			search_html_arr+= item.label+'</strong><br/>'
			search_html_arr+= item.editor
			search_html_arr+= '</span>';
			search_html_arr+= '</a>';
			return $("<li>").append(search_html_arr).appendTo(ul);
		}

		$.fn.selectRange = function(start, end) {
			return this.each(function() {
				if(this.setSelectionRange) {
					 this.focus();
					 this.setSelectionRange(start, end);
				} else if(this.createTextRange) {
					 var range = this.createTextRange();
					 range.collapse(true);
					 range.moveEnd('character', end);
					 range.moveStart('character', start);
					 range.select();
				}
			});
		};
	}

	// app_push
	$('#push_set').click(function(){
		$push_set = $('#push_set');
		push=$(this).children('input').val();
		$.ajax({
			url:nm_url+'/ajax/app_push.php'
		}).success(function(data){
			if(data=='ON'){
				$('#push_set').removeClass('off').addClass('on');
				$('#push_set').find('i').removeClass('fa-bell-slash').addClass('fa-bell');
			}else{
				$('#push_set').removeClass('on').addClass('off');
				$('#push_set').find('i').removeClass('fa-bell').addClass('fa-bell-slash');
			}
		});
	});
});

/* common */
function app_device_token(){
	var token = '';
	if( /Android/i.test(navigator.userAgent)) {
		window.megacomics.Token('token');
	} else {
		window.webkit.messageHandlers.Token.postMessage('token');
	}
}

function token(text){	
	if(text == ''){ 
		alertBox('APP을 종료 후 재실행 해주세요.'); 
	}else{
		$.ajax({
			url:nm_url+'/ajax/app_token.php',
			type:'post',
			data:{
				token:text
			}
		});
		
		var app_get_token = $("#app_get_token").length;
		if(app_get_token > 0){
			alertBox('APP 정보를 받을 수 있습니다.');
			$('#app_get_token').hide();
		}
	}
}

/*
// 새로고침했을때 맨 위로 이동
window.addEventListener('load',function(){
	setTimeout(scrollTo, 0, 1, 1);
}, false);

if (navigator.userAgent.indexOf('iPhone') != -1) {
	addEventListener("load", function() {
		setTimeout(hideURLbar, 0);
	}, false);
}
// 위에껀 아이폰
// 요 아래껀 다른폰
*/

function hideURLbar() {
	window.addEventListener('load', function() {
		setTimeout(scrollTo, 0, 0, 1);
	}, false);
}

function link(url) {
	window.document.url = url;
	location.href = window.document.url;
}

/* a태그 #책받임 기능 막기 */
function a_click_false(){
	event.preventDefault(); //FF
	event.returnValue = false; //IE
}

// 이메일형식 검사
function email_chk(email)
{
	var email_check = true;
    if (!email){ 
		email_check = false;
	}else{
		var pattern = /([0-9a-zA-Z_-]+)@([0-9a-zA-Z_-]+)\.([0-9a-zA-Z_-]+)/;
		if (!pattern.test(email)) {
			email_check = false;
		}
	}
	return email_check;
}

// 정수만 추출
function getNumberOnly(str)
{
    var res;
    res = str.replace(/[^0-9\-]/g,"");
    return res;
}

// alertBox 이용함수
function goto_url(obj){
	location.href = obj.url;
}
// alertBox 이용함수
function goto_replace_url(obj){
	document.location.replace(obj.url);
}

function mode_chage(mode){
	/*
	if(mode == 'pc'){
		$.cookie("ck_default_mode", 'pc', { expires: 7, path: '/' }); // 7일 쿠키보관
	}else{
		$.cookie("ck_default_mode", 'mobile', { expires: 7, path: '/' }); // 7일 쿠키보관
	}
	*/
	if(mode == 'pc'){
		$.cookie("ck_default_mode", 'pc');
	}else{
		$.cookie("ck_default_mode", 'mobile');
	}
	location.reload();
}
/* //////////////////////// 모달팝업 //////////////////////// */

function alertBox(txt, callbackMethod, jsonData){
	var ua = window.navigator.userAgent;
	ua_lower = ua.toLowerCase();
	if(ua_lower.search("safari")!= -1 && ua_lower.search("chrome")== -1){
		alert(txt);
		if(callbackMethod){ callbackMethod(jsonData); }
	}else{
		modal({
			type: 'alert',
			title: '알림',
			text: txt,
			callback: function(result){
				if(callbackMethod){ callbackMethod(jsonData); }
			}
		});
	}
}
 
function alertBoxFocus(txt, obj){
    modal({
        type: 'alert',
        title: '알림',
        text: txt,
        callback: function(result){
            obj.focus();
        }
    });
}
 
    
function confirmBox(txt, callbackMethod, jsonData){
    modal({
        type: 'confirm',
        title: '알림',
        text: txt,
        callback: function(result) {
            if(result){
                callbackMethod(jsonData);
            }
        }
    });
}

function confirmBoxYN(txt, form){
    modal({
        type: 'confirm',
        title: '알림',
        text: txt,
        callback: function(result) {
			if(result == true){
				form.submit();
			}
        }
    });
}

function confirmBoxUrl(txt, callbackMethod, jsonData_true, jsonData_false){
    modal({
        type: 'confirm',
        title: '알림',
        text: txt,
        callback: function(result) {
            if(result){
                callbackMethod(jsonData_true);
            }else{
                callbackMethod(jsonData_false);
			}
        }
    });
}
 
function promptBox(txt, callbackMethod, jsonData){
    modal({
        type: 'prompt',
        title: 'Prompt',
        text: txt,
        callback: function(result) {
            if(result){
                callbackMethod(jsonData);
            }
        }
    });
}
 
/* confirmview: "View" add 17-09-26 */ 
function confirmView(txt, callbackMethod, jsonData, jsonView){
    modal({
        type: 'confirmview',
        title: '알림',
        text: txt,
        callback: function(result) {
            if(result=='view'){
				callbackMethod(jsonView);
            }else if(result){
                callbackMethod(jsonData);
			}
        }
    });
}

/* confirmjoin: "Join" add 18-04-18 */ 
function confirmJoin_imgload(title, txt){
	
	var title_imgload = txt_imgload = confirmJoin_imgload_bool = false;
	var width_img = '';
	var title_img = '<img id="confirmjoin_title_img" src="'+title+'" alt="회원가입/로그인팝업-title" '+width_img+'>';
	var txt_img = '<img id="confirmjoin_txt_img" src="'+txt+'" alt="회원가입/로그인팝업-txt" '+width_img+'>';
	
	$('body').append('<div class="hide" style="display:none;">'+title_img+txt_img+'</div>');
}
 
/* confirmjoin: "Join" add 18-04-18 */ 
function confirmJoin(title, txt){

	callbackMethod = goto_replace_url;

	var join_url = nm_url+'/ctjoin.php';
	var login_url = nm_url+'/ctlogin.php';
	jsonJoin = {url:join_url};
	jsonData = {url:login_url};

	var width_img = '';
	var title_img = '';
	var txt_img = '';

	// 무조건 이미지로...
	width_img = ' width="100%" ';
	title_img = '<img id="confirmjoin_title_img" src="'+title+'" alt="회원가입/로그인팝업-title" '+width_img+'>';
	txt_img = '<img id="confirmjoin_txt_img" src="'+txt+'" alt="회원가입/로그인팝업-txt" '+width_img+'>';
	
	modal({
		type: 'confirmjoin',
		title: title_img,
		text: txt_img,
		callback: function(result) {
			if(result=='join'){
				callbackMethod(jsonJoin);
			}else if(result){
				callbackMethod(jsonData);
			}
		}
	});
}

function successBox(txt){
    modal({
        type: 'success',
        title: 'Success',
        text: txt
    });
}
 
function warningBox(txt){
    modal({
        type: 'warning',
        title: 'Warning',
        text: txt,
        center: false
    });
}
 
function infoBox(txt){
    modal({
        type: 'info',
        title: 'Info',
        text: txt,
        autoclose: true
    });
}
 
function errorBox(txt){
    modal({
        type: 'error',
        title: 'Error',
        text: txt
    });
}
 
function invertedBox(txt){
    modal({
        type: 'inverted',
        title: 'Inverted',
        text: txt
    });
}
 
function primaryBox(txt){
    modal({
        type: 'primary',
        title: 'Primary',
        text: txt
    });
}


/* 팝업 */
function popup(url,frame,w,h) {
	var bwidth = 700;
	var bheight =700;
	if(w == 0 || w == "" || typeof w == "undefined"){ w = bwidth; }
	if(h == 0 || h== ""  || typeof h == "undefined"){ h = bheight; }
	if(frame== "" || typeof frame == "undefined"){ frame = "newpopup"; }
	var popup_win = window.open(url, frame, "left=0, top=0, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width="+w+", height="+h);
	popup_win.resizeTo(w, h);
	/* popup_win.moveTo(0, 0); */
	a_click_false();
}

/* //////////////////////// 모달팝업 end //////////////////////// */

/* header */

	//alert
	function alert_win(){
		user_menu_hide();
		comice_search_hide();
		a_click_false();
	}

	// search
	function comice_search(){
		var search_state = $('.search img').hasClass('off');
		if(search_state == true){
			comice_search_hide();
		}else{
			comice_search_show();
		}
		a_click_false();
	}

	function comice_search_show(){
		$('#search_list').show();
		$('.search img').addClass('off');
		var data_img = $('.search img').attr('data_img_off');
		$('.search img').attr('src',data_img);
		$('#alert_win').addClass('mgt50');
		$('#alert_win').fadeIn(300);

		$('#search_txt').focus();
		if($('.ui-autocomplete li').length > 0){ $('.ui-autocomplete').show(); }
	}
	function comice_search_hide(){
		$('#search_list').hide();
		$('.search img').removeClass('off');
		var data_img = $('.search img').attr('data_img_on');
		$('.search img').attr('src',data_img);
		$('#alert_win').hide();
		$('#alert_win').removeClass('mgt50');
		$('.ui-autocomplete').hide();
	}

	// user_menu
	function user_menu(){
		var hide_check = Number(getNumberOnly($('#user_menu').css('right')));
		if(hide_check < 0){
			user_menu_show();
		}else{
			user_menu_hide();
		}
		a_click_false();
	}

	function user_menu_show(){
		comice_search_hide();
		$('#user_menu').animate({right: '0%'}, 300, 'easeInQuad');
		$('#alert_win').fadeIn(300);
	}

	function user_menu_hide(){
		$('#user_menu').animate({right: '-100%'}, 300, 'easeOutQuad');
		$('#alert_win').fadeOut(300);
	}

	var onToast;
	function toast(msg)
	{
		if (onToast)
		{
			onToast.remove();
			onToast = null;
		}
		onToast = $("<div class='pop_toast'>" + msg + "</div>").appendTo("body");
		onToast.css({ "left" : ($(window).width() - onToast.width()) / 2, "top" : $(window).height() / 2 });
		onToast.delay(600).fadeOut(200, function()
		{
			$(this).remove();
			onToast = null;
		});
	}

	function goto_coupon(url){
		link(url);
		alert_win();
	}

	/* 로딩 */
	var onLoading;
	function loading()
	{
		if (onLoading){ return false; }
		onLoading = $("<div class='pop_load'><i class='loading_icon fa fa-spinner fa-pulse fa-3x fa-fw'></i></div>").appendTo("body");
		onLoading.children('.loading_icon').css({ "top" : $(window).height() / 2,'position':'relative' });
	}