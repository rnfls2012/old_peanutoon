$(function(){
	var sub_list_url = $(".sub_list").attr('data-url');										// URL체크
	$.cookie("ck_sub_list_url", sub_list_url);												// URL저장
	setTimeout(isLoading_check, 1200);														// 1.2초 후 실행

	function isLoading_check(){
		var isLoading = false;																// isLoading 상태
		var $sub_list_img = "";

		var sub_list_top = Math.round(Number($(".sub_list").offset().top));					// 시작위치
		var sub_list_small = $(".sub_list").attr('data-small');								// 메뉴

		var sub_list_limit = Number($(".sub_list").attr('data-limit'));						// 갯수
		var sub_list_limit_define = Number($(".sub_list").attr('data-limit_define'));		// 정해진 갯수
		var sub_list_total = Number($(".sub_list").attr('data-total'));						// 총갯수
		var sub_list_url = $(".sub_list").attr('data-url');									// URL체크
		$.cookie("ck_sub_list_url", sub_list_url);											// URL저장
		var sub_list_bool = $(".sub_list").attr('data-bool');								// 코믹스 더 있는지 체크

		var sub_list_piece = Math.round(Number($(".sub_list .sub_list_thumb").height()));	// 코믹스 1개의 높이
		var load_height = calc_load_height(sub_list_top, sub_list_piece, sub_list_limit);	// 2줄 전 높이
		

		if(isLoading == false){																// false 로딩 끝
			if(sub_list_bool == "false"){													// false 코믹스 더 있음( 더 있음 )
				$.cookie("load_list_limit", sub_list_limit);								// 갯수저장
				$(window).scroll(function(){
					
					this_top = Number($(document).scrollTop()) + Number($(window).height());	// 현재 위치 + 창크기
					this_top = Math.round(this_top);
					if (this_top > load_height){
						if(isLoading == false){
							isLoading = true;
							sub_list_add(sub_list_small, sub_list_limit, sub_list_bool);
							sub_list_limit+=sub_list_limit_define;
							$.cookie("ck_load_list_limit", sub_list_limit);

							if(sub_list_limit < sub_list_total){
								isLoading = false;
								load_height = calc_load_height(sub_list_top, sub_list_piece, sub_list_limit);
							}
						}
					}
				});
			}
		}
	}
});

function calc_load_height(sub_list_top, sub_list_piece, sub_list_limit){					// 2줄 전 높이 계산
	var sub_list_sum = Math.round(sub_list_piece * sub_list_limit / 3) + sub_list_top;		// 코믹스 1 * 총갯수 (나누기) 한줄갯수
	var load_height = sub_list_sum - (sub_list_piece * 2);									// 2줄 전 높이	
	return load_height;
}

function sub_list_add(sub_list_small, sub_list_limit, sub_list_bool){
	var html_arr = new Array();
	var result = new Array();
	var bool = false;
	
	var sub_list_access = $.ajax({
		url: nm_url +"/ajax/cmgenre.php",
		dataType: "json",
		type: "POST",
		data: { small: sub_list_small, limit: sub_list_limit, bool: sub_list_bool},
		beforeSend: function( data ) {
			//console.log('로딩');
		}
	})
	sub_list_access.done(function( data ) {
		var html_tag = '';
		$(data).each(function(idx){
			html_arr.push('<a href="'+data[idx]['cm_serial_url']+'" target="_self" '+'data-week="'+data[idx]['cm_week_txt']+'">');
				html_arr.push('<div class="sub_list_thumb">');
					html_arr.push('<div class="icon_view">');
						html_arr.push(data[idx]['cm_free_mark']);
						html_arr.push(data[idx]['cm_sale_mark']);
					html_arr.push('</div>');
					html_arr.push('<div class="img_view">');
						html_arr.push('<img src="'+data[idx]['cm_cover_sub']+'" alt="'+data[idx]['cm_series']+'" />');
						html_arr.push('<div class="icon_view_bottom">');
							html_arr.push(data[idx]['cm_adult_mark']);
							html_arr.push(data[idx]['cm_up_icon']);
							html_arr.push(data[idx]['cm_new_icon']);
						html_arr.push('</div>');
					html_arr.push('</div>');
					html_arr.push('<dl>');
						html_arr.push('<dt class="ellipsis">'+'<span class="cm_title">'+data[idx]['cm_series'].replace("[웹툰판]", "")+'</span>'+'</dt>');
						html_arr.push('<dd class="ellipsis">'+data[idx]['cm_small_span']+data[idx]['cm_end_span']+'</dd>');
						html_arr.push('<dd class="ellipsis">'+data[idx]['cm_professional_info']+'</dd>');
					html_arr.push('</dl>');
				html_arr.push('</div>');
			html_arr.push('</a>');
		});
		$(".sub_list").append(html_arr.join("\n"));
	});
	sub_list_access.fail(function( data ) {
		//console.log('실패');
	});
	sub_list_access.always(function( data ) {
		//console.log('로딩삭제');
	});
}
