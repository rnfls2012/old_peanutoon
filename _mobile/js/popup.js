$(function() {
    $(".popup_layer_reject").click(function() {
        var id = $(this).attr('class').split(' ');
        var ck_name = id[1];
        var exp_time = parseInt(id[2]);
        $("#"+id[1]).css("display", "none");
        set_cookie(ck_name, 1, exp_time, '/');
		popup_layer_fold(); // 접기 기능
    });
    $('.popup_layer_close').click(function() {
        var idb = $(this).attr('class').split(' ');
		console.log(idb[1]);
        $('#'+idb[1]).css('display','none');
    });
	
	/* 드래그 플로그인 */
	$(".popup_layer").draggable();

	/* 팝업 링크 제어 */
	$(".popup_layer a").on("click touchstart", function(e){

		/* 통계처리 */

		/* 링크이동 */
		link($(this).attr('data-href'));
	});
	
	/* 팝업 접기 제어 */
    $(".popup_layer_fold").click(function() {
        var id = $(".popup_layer_reject").attr('class').split(' ');
        var ck_name = id[1];
        var exp_time = parseInt(id[2]);
        $("#"+id[1]).css("display", "block");
        delete_cookie(ck_name);
		popup_layer_fold(); // 접기 기능
    });
});

// 접기 기능
function popup_layer_fold(){
	var popup_layer_display = $('.popup_layer').css('display');
	$('.popup_layer_fold').hide();
	if(popup_layer_display == 'none'){
		$('.popup_layer_fold').show();
	}
}


// 쿠키 입력
function set_cookie(name, value, expirehours, domain)
{
    var today = new Date();
    today.setTime(today.getTime() + (60*60*1000*expirehours));
    document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + today.toGMTString() + ";";
    if (domain) {
        document.cookie += "domain=" + domain + ";";
    }
}

// 쿠키 얻음
function get_cookie(name)
{
    var find_sw = false;
    var start, end;
    var i = 0;

    for (i=0; i<= document.cookie.length; i++)
    {
        start = i;
        end = start + name.length;

        if(document.cookie.substring(start, end) == name)
        {
            find_sw = true
            break
        }
    }

    if (find_sw == true)
    {
        start = end + 1;
        end = document.cookie.indexOf(";", start);

        if(end < start)
            end = document.cookie.length;

        return unescape(document.cookie.substring(start, end));
    }
    return "";
}

// 쿠키 지움
function delete_cookie(name)
{
    var today = new Date();

    today.setTime(today.getTime() - 1);
    var value = get_cookie(name);
    if(value != "")
        document.cookie = name + "=" + value + "; path=/; expires=" + today.toGMTString();
}