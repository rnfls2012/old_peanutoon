$(function(){

    $('.Modalalert > .ModalBackground').click(function(){
        $('.Modalalert > .ModalBackground').hide();
        $('.Modalalert > .ModalPopup').hide();
        $('html, body').css({'overflow': 'auto'});
        $('.Modalalert > .ModalBackground').off('scroll touchmove mousewheel');
        $(this).removeAttr('style');
        $('.Modalalert > .ModalPopup').removeAttr('style');
    });

    $('.Modalalert > .ModalPopup > .ModalClose').click(function(){
        $('.Modalalert > .ModalBackground').hide();
        $('.Modalalert > .ModalPopup').hide();
        $('html, body').css({'overflow': 'auto'});
        $('.Modalalert > .ModalBackground').off('scroll touchmove mousewheel');
        $('.Modalalert > .ModalBackground').removeAttr('style');
        $(this).removeAttr('style');

    });

    function position_cm(obj) {
        var windowHeight = $(window).height();
        var topOfWindow = $(window).scrollTop()
        var headerH = $('#header').height();
        var $obj = $(obj);
        var objHeight = $obj.height();
        $obj.css ({
            'top':(windowHeight/2) - (objHeight/2) + topOfWindow
        });
        return this;
    };

    $("#open_btn").click(function(){
		var btn_activation = $(this).attr("class");

		// 버튼 활성유무에 따라 프로세스 진행
		if(btn_activation == "nochk") {
			if($("#member_no").val() == "") {
				alertBox("로그인 후 이용이 가능합니다!");
			} else if($("#total_coupon").val() <= 0){
				alertBox("남은 할인권이 없습니다. 내일 다시 도전하세요!");
			} else {
				alertBox("오늘자 할인권을 이미 뽑으셨습니다!");	
			}// end else

			return false;
		} else {
			// AJAX 처리
			$.ajax({
				url: nm_url+'/ajax/eventfullmoon.php',
				cache: false, 
				dataType : "json", 
				success: function(data) {
					if(data['result']) {
						var maskHeight = $(window).height();
						var topOfWindow = $(window).scrollTop();

						$(".coupon_value").text(data['ercm_discount_rate']);
						$(".coupon_value02").text(data['er_available_date_end']);
						$("#recharge_link").attr('onclick','link(nm_url+\'/recharge.php?mb_coupondc='+data['erc_no']+'\')');
						$('.Modalalert > .ModalBackground').show();
						$('.Modalalert > .ModalBackground').css({'height':maskHeight, 'top':topOfWindow});
						$('html, body').css({'overflow': 'hidden'});
						$('.Modalalert > .ModalBackground').on('scroll touchmove mousewheel', function(event) { // 터치무브와 마우스휠 스크롤 방지
							event.preventDefault();
							event.stopPropagation();
							return false;
						});
						position_cm($('.Modalalert > .ModalPopup'));
						$('.Modalalert > .ModalPopup').show();

					} else {
						alertBox(data['text']);
					} // end else
				} // end success
			}) // end ajax
		} // end else
    });
});