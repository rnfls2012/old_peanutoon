$(function(){
	/* CS_FAQ(Accordian menu) */
	$('.faq_list > dl > dt').click(function(){
		$('.faq_list > dl > dt').removeClass('currunt');
		$('.faq_list > dl > dd').slideUp('fast');
		if(!$(this).next().is(":visible")){
			$(this).next().slideDown('fast');
			$(this).addClass('currunt')
		} // end if
	});

	/* CS_QNA 1:1 문의 */
	if($('#myinfo_ba_no').length && $('#myinfo_ba_no').val() != "") {
		//alert($("#dt_"+$('#myinfo_ba_no').val()).data("ba_no"));
		$("#dt_"+$('#myinfo_ba_no').val()).addClass('currunt');
		$("#dt_"+$('#myinfo_ba_no').val()).find('i').removeClass('fa-chevron-circle-down');
		$("#dt_"+$('#myinfo_ba_no').val()).find('i').addClass('fa-chevron-circle-up');
		$("#dd_"+$('#myinfo_ba_no').val()).css('display', 'block');
	}

	$('.qna_list > dl > dt').click(function(){
		var this_down = $(this).find('i');
		var dt_down = $(".qna_list > dl > dt").find('i');
		this_down.removeClass('fa-chevron-circle-up');
		this_down.addClass('fa-chevron-circle-down');
		dt_down.removeClass('fa-chevron-circle-up');
		dt_down.addClass('fa-chevron-circle-down');

		$('.qna_list > dl > dt').removeClass('currunt');
		$('.qna_list > dl > dd').slideUp('fast');

		if(!$(this).next().is(":visible")){
			this_down.removeClass('fa-chevron-circle-down');
			this_down.addClass('fa-chevron-circle-up');
			$(this).next().slideDown('fast');
			$(this).addClass('currunt');
		} // end if
	});

	$('.qnaw_select_1').click(function() {
		$(this).removeClass('currunt');
		$('.qnaw_select_list').slideUp('fast');
	    ///$('.qnaw_select_list').show();
		// console.log(1);


		if(!$('.qnaw_select_list').is(":visible")){
			$(this).addClass('currunt');
			$('.qnaw_select_list').slideDown('fast')
		} // end if
    });

    $('.qnaw_select_list div').click(function() {
		$('.qnaw_select_1').removeClass('currunt');
		$('.qnaw_select_list').slideUp('fast');
		$('.qnaw_select_1').text($(this).text());
		$("#ba_category").val($(this).data("value"));
    });
});

function csaskwrite_submit() {
	if($("#ba_category").val() == "") {
		alertBox("분류를 선택해주세요!");
		return false;
	} // end if

	if($("#qnaw_title").val() == "") {
		alertBox("제목을 입력해주세요!");
		$("#qnaw_title").focus();
		return false;
	} // end if

	if($("#qnaw_request").val() == "") {
		alertBox("내용을 입력해주세요!");
		$("#qnaw_request").focus();
		return false;
	} // end if
}