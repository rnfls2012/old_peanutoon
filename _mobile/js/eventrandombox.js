$(function(){
    $( '.goToTop' ).click( function() {
        $( 'html, body' ).animate( { scrollTop : 0 }, 400 );
    });

    $('.Modalalert > .ModalBackground').click(function(){
        $('.Modalalert > .ModalBackground').hide();
        $('.Modalalert > .ModalPopup').hide();
        $('html, body').css({'overflow': 'auto'});
        $('.Modalalert > .ModalBackground').off('scroll touchmove mousewheel');
        $(this).removeAttr('style');
		$("input[name=pocket]").prop("checked", false);
		$("#open_btn").addClass("nochk");
        $('.Modalalert > .ModalPopup').removeAttr('style');

		if($(".p_content01").css("display") == "none") {
			$(".p_content01").css({'display': ''});
			$(".p_content02").css({'display': 'none'});
			location.reload();
		}
    });

    $('.Modalalert > .ModalPopup > .ModalClose').click(function(){
        $('.Modalalert > .ModalBackground').hide();
        $('.Modalalert > .ModalPopup').hide();
        $('html, body').css({'overflow': 'auto'});
        $('.Modalalert > .ModalBackground').off('scroll touchmove mousewheel');
        $('.Modalalert > .ModalBackground').removeAttr('style');
		$("input[name=pocket]").prop("checked", false);
		$("#open_btn").addClass("nochk");
        $(this).removeAttr('style');

		if($(".p_content01").css("display") == "none") {
			$(".p_content01").css({'display': ''});
			$(".p_content02").css({'display': 'none'});
			location.reload();
		}

    });

    function position_cm(obj) {
        var windowHeight = $(window).height();
        var topOfWindow = $(window).scrollTop()
        var headerH = $('#header').height();
        var $obj = $(obj);
        var objHeight = $obj.height();
        $obj.css ({
            'top':(windowHeight/2) - (objHeight/2) + topOfWindow
        });
        return this;
    };

    $('.evt_gauge > .gauge_wrap').click(function(){
		if($("#erm_member").val() == "") { // 로그인 안 됐을 시
			alertBox("로그인 후 이용이 가능합니다!");
			return false;
		} else if($("#erm_cash_point_total").val() < 10) { // 참여 땅콩이 부족할 시
			alertBox("참여 땅콩이 부족합니다!");
			return false;
		} else if($("#erm_pop_count").val() == 0) { // 뽑기 횟수를 다 소모했을 시
			alertBox("오늘 뽑기 횟수를 모두 사용하셨습니다!");
			return false;
		} else {
			var maskHeight = $(document).height();
			var topOfWindow = $(window).scrollTop()
			$('.Modalalert > .ModalBackground').show();
			$('.Modalalert > .ModalBackground').css({'height':maskHeight, 'top':topOfWindow});
			$('html, body').css({'overflow': 'hidden'});
			$('.Modalalert > .ModalBackground').on('scroll touchmove mousewheel', function(event) { // 터치무브와 마우스휠 스크롤 방지
				event.preventDefault();
				event.stopPropagation();
				return false;
			});
			position_cm($('.Modalalert > .ModalPopup'));
			$('.Modalalert > .ModalPopup').show();
		} // end else
    });

	/* 라디오 클릭시 바로열기 활성화 */
	$("input:radio[name=pocket]").click(function() {
		$("#open_btn").removeClass("nochk");
	});

	/* 바로열기 버튼 활성 유무에 따라 Alert */
	$("#open_btn").click(function() {
		if($('input:radio[name="pocket"]:checked').val()) {
			/* AJAX 처리 */
			$.ajax({
				url: nm_url+'/ajax/eventrandombox.php',
				cache: false, 
				dataType : "json", 
				success: function(data) {
					if(data['result']) {
						$(".p_content01").css({'display': 'none'});
						$(".p_content02").css({'display': ''});	
						$(".coupon_value").text(data['ercm_discount_rate']);
						$(".coupon_value02").text(data['er_available_date_end']);
						$("#recharge_link").attr('onclick','link(nm_url+\'/recharge.php?mb_coupondc='+data['erc_no']+'\')');
					} else {
						alertBox(data['text']);
					} // end else
				} // end success
			}) // end ajax
		} else {
			alertBox("복주머니를 선택하세요!");
			return false;
		} // end else
	});
});