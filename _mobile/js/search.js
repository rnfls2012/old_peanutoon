$.fn.selectRange = function(start, end) {
    return this.each(function() {
         if(this.setSelectionRange) {
             this.focus();
             this.setSelectionRange(start, end);
         } else if(this.createTextRange) {
             var range = this.createTextRange();
             range.collapse(true);
             range.moveEnd('character', end);
             range.moveStart('character', start);
             range.select();
         }
     });
 }; 

$(function(){
	var search_tx_length = $('#search_txt').val().length;
	if(search_tx_length > 0){
		$('#search_txt').selectRange(search_tx_length,search_tx_length);
	}
});