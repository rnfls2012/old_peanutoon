$(document).ready(function() {

	$(".evt-test .test_tab ul li").click(function(event){ // 흑백배경
        var tab_idx = $(this).index();
        var tab_use = $(this).attr('data-tab-use');
        var tab_date = $(this).attr('data-tab-date');

		if(tab_use == 'y'){
			$(".evt-test .test_tab ul li").removeClass('test_on');
			$(this).addClass('test_on');

			$(".evt-test .test_con").removeClass('hide');
			$(".evt-test .test_con").addClass('hide');
			$(".evt-test .test_con").eq(tab_idx).removeClass('hide');
		}else{
			alertBox(tab_date);
		}
    });

	$(".evt-test .test_go #finalexam").click(function(event){ // 흑백배경
        var exam_msg = $(this).attr('data-link-msg');
        var exam_link = $(this).attr('data-efp-link');
		if(exam_link == ""){
			alertBox(exam_msg);
		}else{
			alertBox(exam_msg, goto_url, {url:exam_link});
		}
    });
});

/*
function countChar(val) {
    var len = val.value.length;
    if (len > 50) {
        val.value = val.value.substring(0, 50);
    } else {
        $('.count_box_character').text(len);
    }
}

function eventrami_submit() {

    var comments    = $("#comments").val();
    var loginPage   =  nm_url  + '/ctlogin.php';

    if($("#mb_no").val() == "" ) {
        confirmBox("로그인이 필요합니다!", goto_url, {url: loginPage});
        return false;
    } else if(comments === "") {
        alertBoxFocus("응원글을 남겨주세요!", $("#comments"));
        return false;
    }
}
*/