<? include_once '_common.php'; // 공통 
$hanamembers_img = NM_MO_IMG.'event/2017/hanamembers';
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/eventhanamembers.css<?=vs_para();?>" />
		<style type="text/css">
			/* background */
			#eventhanamembers .evtcontainer { background:url('<?=$hanamembers_img;?>/evt03_m_bg.png') no-repeat; background-color: #f5c67c; background-size: 100%;}			
			#eventhanamembers .evt_contentswrap { background:url('<?=$hanamembers_img;?>/evt03_bg02.png'); background-color: #fff2df; }
		</style>
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/eventhanamembers.js<?=vs_para();?>"></script>
		
		<div id="eventhanamembers">
			<div class="evtcontainer">
				<div class="evt_title">
					<img src="<?=$hanamembers_img;?>/evt03_tit.png" alt="하나멤버스 앱 설치 및 가입하고 100 미니땅콩 받아가세요" />
				</div>
				<div class="evt_contentswrap">
					<div class="evt_dn_btn"><img src="<?=$hanamembers_img;?>/evt03_dn_btn.png" alt="하나멤버스 설치" /></div>
					<div class="evt_con">
						<img src="<?=$hanamembers_img;?>/evt03_con.png" alt="이벤트 참여방법">
					</div>
				</div>
				<div class="evt_footer01">
					<img src="<?=$hanamembers_img;?>/evt03_footer01.png" alt="이벤트 유의사항" />
				</div>
				<div class="evt_footer02">
					<img src="<?=$hanamembers_img;?>/evt03_footer02.png" alt="준법감시인 심사번호">
				</div>
			</div>
		</div> <!-- /eventhanamembers -->