<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<? include NM_MO_PART_PATH.'/event.php';		// [mobile] 서브슬라이드?>

			<? include NM_MO_PART_PATH.'/banner.php';		// [mobile] 배너 스크롤?>

			<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/cmserial.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_MO_URL;?>/js/cmserial.js<?=vs_para();?>"></script>
			<!-- 작품리스트 -->
			<div id="cmserial" class="container_bg">
				<div class="sub_list clear" data-limit_define="<?=NM_SUB_LIST_LIMIT;?>" data-limit="<?=$sub_list_limit;?>" data-total="<?=$row_total;?>" data-url="<?=$_SERVER['REQUEST_URI'];?>" data-menu="<?=$_menu;?>" data-bool="<?=$sub_list_bool;?>"> 
					<? foreach($cmserial_arr as $cmserial_key => $cmserial_val){ 
						/* Update Icon */
						$cm_episode_date_up = date("Y-m-d", strtotime($cmserial_val['cm_episode_date']."+1day"));
						$date_up_class = "";
						$date_up_img_url = "";
						if($cm_episode_date_up >= NM_TIME_YMD) { 
							$date_up_class = "iscroll_img";
							$date_up_img_url ='<img class="icon_up_img" src="'.$nm_config['nm_url_img'].'icon/icon_up.png" alt="UPDATE" />';
						} // end if
						?>
					<a href="<?=$cmserial_val['cm_serial_url']?>" target="_self">
						<div class="sub_list_thumb">
							<?=$date_up_img_url;?>
							<img class="<?=$date_up_class;?>" src="<?=$cmserial_val['cm_cover_sub']?>" alt="<?=$cmserial_val['cm_series']?>" />
							<dl>
								<dt class="ellipsis"><?=$cmserial_val['cm_series']?></dt>
								<dd class="ellipsis"><?=$cmserial_val['small_txt']?></dd>
								<dd class="ellipsis"><?=$cmserial_val['cm_professional_info']?></dd>
							</dl>
						</div>
					</a>
					<? } /* foreach($cmserial_arr as $cmserial_key => $cmserial_val){  */ ?>
				</div>
			</div>