<? include_once '_common.php'; // 공통

?>

<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/popupgray.css<?=vs_para();?>" />

<script type="text/javascript" src="<?=NM_MO_URL;?>/js/popupgray.js<?=vs_para();?>"></script>

<div id="blackpopup" class="blackpopup hide">
	<div id="blackpopup_bg" class="blackpopup_bg"></div>
	<div id="blackpopup_view" class="blackpopup_view"></div>
</div>