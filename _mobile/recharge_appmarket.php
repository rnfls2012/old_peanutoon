<? include_once '_common.php'; // 공통 

if(is_mobile()){ $device = "mobile"; }
else{ $device = "pc"; }
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/recharge.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_MO_URL;?>/js/recharge.js<?=vs_para();?>"></script>
			<!-- 작품리스트 -->
			<div id="recharge">
				<div class="mar_free">
						<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
						피너툰 APP에서는 결제를 할 수 없습니다.<br />
						결제를 이용하고 싶으신 고객님들께서는 피너툰 완전판 APP 또는, 웹 모바일 버젼을 이용해 주세요!
				</div><!-- mar_free -->
			</div>