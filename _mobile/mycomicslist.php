<?
include_once '_common.php'; // 공통
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/my.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/my.js<?=vs_para();?>"></script>

		<? 
		// 삭제(숨김)기능 안되게 false
		if(count($member_buy_arr) > 0 && false) { 
		?>
			<div class="lib_del"> <!-- 삭제하기 메뉴 -->
				<div class="lib_del_chk">
					<div class="lib_chk">
						<input type="checkbox" id="lib_delete" name="all_radio" />
						<label for="lib_delete"></label>
					</div>
					<span>전체선택</span>
				</div>
				<button type="button" class="lib_del_btn"><i class="fa fa-trash" aria-hidden="true"></i> 삭제</button>
				<!-- 취소버튼 // 삭제모드시 보임 --><button type="button" class="lib_cancel_btn">취소</button>
			</div>
			<? } ?>
				
			<? if(count($member_buy_arr) > 0) {?>
			<form name="mycomics" id="mycomics" method="post" action="<?=NM_PROC_URL;?>/mycomics.php" onsubmit="return mycomics_submit();">
				<input type="hidden" id="mode" name="mode" value="comicslist">
				<div class="mylib_con">
					<? foreach($member_buy_arr as $key => $val) { 
							$img_url = img_url_para($val['cm_cover_sub'], $val['cm_reg_date'], $val['cm_mod_date'], 200, 200); ?>
						<div class="lib_list"> <!-- Thumbnail list -->
							<a href="<?=NM_URL."/mycomics.php?mbc=2&comics=".$val['cm_no'];?>">
								<div class="lib_chk">
									<!-- 삭제 체크박스 -->
									<input type="checkbox" id="lib_delete_<?=$key;?>" name="comics_check[]" value="<?=$val['cm_no'];?>"/>
									<label for="lib_delete_<?=$key;?>"></label>
									<!-- /삭제 체크박스 -->
								</div>
								<dl>
									<dt>
										<img src="<?=$img_url;?>" alt="<?=$val['cm_series'];?>" />
										<div class="icon_up"><img src="<?=NM_MO_IMG?>common/icon_up.png" alt="update" /></div> <!-- update -->
									</dt>
									<dt><?=$val['cm_series'];?></dt>
									<dd><?=$val['cm_professional_info'];?></dd>
									<dd><?=substr($val['mbc_date'], 0, 10);?></dd>
								</dl>
							</a>
						</div> <!-- /Thumbnail list -->
					<? } // end foreach ?>
				</div>
			</form>
		<? } else { ?>
			<div class="lib_no_data" style="text-align: center;">구매목록이 없습니다.</div>
		<? } // end else ?>