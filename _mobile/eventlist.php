<?
include_once '_common.php'; // 공통
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/event.css<?=vs_para();?>" />
		<style type="text/css">
			.evt_wrap{ background-color: <?=$eme_bgcolor;?>; }
			.evt_pigure{ border:1px solid <?=$eme_bgcolor;?>; }
			.evt_pigure .cm_cp{ border-left:5px solid <?=$eme_bgcolor;?>; }
		</style>
		<div class="evt_wrap">
			<div class="evt_top">
				<img src="<?=$ep_collection_top_url;?>" alt="콜렉션 상단 이미지" />
			</div>
			<div class="evt_con" id="event_list">
			<? foreach($list_array as $key => $val) { ?>
				<div class="evt_pigure">
					<dl>
						<dt><a href="<?=get_comics_url($val['cm_no']);?>"><img src="<?=$val['comics_img_url']?>" alt="<?=$val['cm_series'];?>" /></a></dt>
						<dt class="cm_cp"><a href="<?=get_comics_url($val['cm_no']);?>"><?=stripslashes($val['cm_series']);?></a><span><?=$val['cm_professional_info'];?></span></dt>
						<dd class="ellipsis"><?=stripslashes($val['cm_somaery']);?></dd>
					</dl>
					<a href="<?=$val['comics_url']?>" class="first_ep">첫화 보기</a>
				</div>
			<? } // end foreach ?>
			</div>
		</div>