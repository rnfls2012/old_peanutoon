<?
include_once '_common.php'; // 공통
$social_oauth_url = NM_OAUTH_URL.'/login.php?service=';


if(is_nexcube()){
	// echo urldecode(get_session('ss_http_referer'))."<br/>";
	// echo $cookie_http_referer;
}

?>


<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/ct.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/ct.js<?=vs_para();?>"></script>

		<div class="joinup_box">
			<h5><?=$nm_config['cf_title'];?> 회원가입완료</h5>
			
			<div class="joinup_box">
				<div class="joinup_info clear">
					<div class="form_field">
						<p class="form_field_text"><?=$nm_member['mb_id'];?></p>
						<label for="page_join_id">가입하신 아이디</label>
					</div>
					<!--
					<div class="form_field">
						<p class="form_field_text"><?=$d_mb_sns_type[$nm_member['mb_sns_type']];?></p>
						<label for="page_join_id">가입타입</label>
					</div>
					-->
				<div class="joinup_link"> <!-- 링크 -->
					<ul>
						<li><a href="<?=NM_URL?>">메인화면</a></li>
						<li><a href="<?=$ss_http_referer?>">보던 페이지</a></li>
					</ul>
				</div>
				<div class="login_help">
					문제가 있거나 궁금한 점이 있으시면<br />
					<a href="mailto:<?=$nm_config['cf_admin_email'];?>"><?=$nm_config['cf_admin_email'];?></a> 으로 문의주시기 바랍니다.<br/><br/>
					※ <strong>sns 가입경우</strong><br/>
					sns정책 또는 sns개인정보설정에 <br/>
					따라 피너툰에 <br/>
					새로 가입 될 수 도 있습니다.<br/>
					기존 로그인 있는데 <br/>
					새로 가입될 시<br/>
					<a href="<?=NM_URL?>/cscenter.php?menu=3&mode=w">1:1문의</a> 또는 위 메일로 <br/>
					문의주시기 바랍니다.
				</div>
			</div>
		</div>