<? include_once '_common.php'; // 공통 
?>

<!-- body space -->
	<!-- wrapper space -->
		
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/sleepcancel.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/sleepcancel.js<?=vs_para();?>"></script>		
		
		<div id="container_sleep">
			<div class="rest_con">
				<h2>휴면 회원</h2>
				<div class="restmem">
					<span>1년 이상 미 로그인으로 인해 휴면 처리된 계정입니다. 계정을 활성화 하시겠습니까?</span>
				</div>
				<div class="vernotice_footer">
					<a href="<?=NM_PROC_URL;?>/sleepcancel.php?mode=cancel">휴면 해제</a><a href="<?=NM_PROC_URL;?>/sleepcancel.php?mode=keep">다음에 하기</a>
				</div>
			</div>
		</div> <!-- /container -->