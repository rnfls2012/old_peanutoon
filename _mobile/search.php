<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/search.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_MO_URL;?>/js/search.js<?=vs_para();?>"></script>
			<!-- 작품리스트 -->
			<div id="search" class="container_bg">
				<div class="sub_list">
					<? if (count($search_arr) > 0) {
	            foreach ($search_arr as $search_key => $search_val) {
	                $search_url = get_comics_url($search_val['cm_no']);

	                $small_txt = $nm_config['cf_small'][$search_val['cm_small']];

	                $cm_series_txt = str_replace($search_txt, '<span class="highlight">' . $search_txt . '</span>', $search_val['cm_series']);

	                $cm_professional_info_txt = str_replace($search_txt, '<span class="highlight">' . $search_txt . '</span>', $search_val['cm_professional_info']);
	                ?>
	                <a href="<?= $search_url ?>" target="_self" data-week="<?=$search_val['cm_week_txt']?>">
	                    <div class="sub_list_thumb">
	                        <img src="<?= $search_val['cm_cover_sub'] ?>"
	                             alt="<?= $search_val['cm_series'] ?>"/>
	                        <dl>
	                            <dt class="ellipsis"><span class="cm_title"><?= $cm_series_txt ?></span></dt>
	                            <dd class="ellipsis"><?= $small_txt ?></dd>
	                            <dd class="ellipsis"><?= $cm_professional_info_txt; ?></dd>
	                        </dl>
	                    </div>
	                </a>
	            <? } /* foreach($search_arr as $search_key => $search_val){  */
	        } else { ?>
	            <div class="search_no_data">검색어를 확인해주세요.</div>
	        <? } ?>
				</div>
			</div>