<?
include_once '_common.php'; // 공통
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/cs.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/cs.js<?=vs_para();?>"></script>
		<input type="hidden" name="myinfo_ba_no" id="myinfo_ba_no" value="<?=$_ba_no;?>">
			<div class="qna_write">
				<a href='<?=NM_URL."/cscenter.php?menu=3&mode=w";?>'>문의하기</a>
			</div>
			<div class="qna_list">
				<dl>
				<? foreach($ask_list as $key => $val) { 
					$answer_class = "hidden";
					$question_text = "질문 내용";
					if($val['ba_mode'] == 1) {
						$answer_class = "";
					} else if($val['ba_mode'] == 4) {
						$question_text = "";
					} // end else if ?>
					<dt id="dt_<?=$val['ba_no'];?>">
						<ul>
							<li>
								<i class="fa fa-chevron-circle-down" aria-hidden="true"></i> <?=$val['ba_title'];?>
							</li>
							<li>
								<?=$ask_state[$val['ba_mode']];?>
							</li>
						</ul>
					</dt>
					<dd id="dd_<?=$val['ba_no'];?>">
						<ol>
							<li>
								<span><?=$question_text;?></span>
								<?=nl2br($val['ba_request']);?>
							</li>
							<li class="<?=$answer_class;?>">
								<span>질문 답변</span>
								<?=nl2br($val['ba_admin_answer']);?>
							</li>
						</ol>
					</dd>
				<? } // end foreach ?>
				<? if(count($ask_list) == 0){?>
					<dt>
						<ul>
							<li>
								문의한 내용이 없습니다.
							</li>
						</ul>
					</dt>
				<?}?>
				</dl>
			</div>