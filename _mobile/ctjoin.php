<?
include_once '_common.php'; // 공통
$social_oauth_url = NM_OAUTH_URL.'/login.php?service=';
$twiter_oauth_url = NM_OAUTH_URL.'/twitterlogin.php';


if(is_nexcube()){
	// echo urldecode(get_session('ss_http_referer'))."<br/>";
	// echo $cookie_http_referer;
}

?>


<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/ct.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/ct.js<?=vs_para();?>"></script>

		<div class="login_box">
			<h5><?=$nm_config['cf_title'];?> 회원가입</h5>
			<fieldset>
				<form name="ctjoin_form" id="ctjoin_form" method="post" action="<?=NM_PROC_URL;?>/ctjoin.php" onsubmit="return ctjoin_submit();">
					<input type="hidden" name="http_referer" value="<?=$cookie_http_referer;?>"/>
					<input type="hidden" name="redirect" value="<?=$_redirect;?>"/>
					<input type="hidden" name="mb_token" value="<?=get_js_cookie('mb_token');?>"/>

					<div class="form_field">
						<input type="text" class="form_field_text" id="join_id" name="join_id">
						<label for="join_id">아이디 (E-Mail)</label>
						<span class="error">이메일 양식인 아이디를 입력해 주세요.</span>
						</div>
					<div class="form_field">
						<input type="password" class="form_field_text" id="join_pw" name="join_pw">
						<label for="join_pw">패스워드 (4자 이상)</label>
						<span class="error">비밀번호는 4자 이상이어야 합니다.</span>
					</div>
					<div class="regi_ck">
						<label class="regi_agree" for="regi_agree">
							<input type="checkbox" id="regi_agree" />
								<span>전체동의</span>
						</label>
					</div>
					<div class="regi_ck">
						<label class="regi_terms" for="regi_terms">
							<input type="checkbox" id="regi_terms" name="regi_terms" />
								<span>이용약관에 동의합니다.</span>
						</label>
							<a href="<?=NM_URL."/siteterms.php";?>" target="_blank"><span class="regi_detail">상세보기</span></a>
					</div>
					<div class="regi_ck">
						<label class="regi_pinfo" for="regi_pinfo">
							<input type="checkbox" id="regi_pinfo" name="regi_pinfo" />
								<span>개인정보취급방침에 동의합니다.</span>
						</label>
							<a href="<?=NM_URL."/siteprivacy.php";?>" target="_blank"><span class="regi_detail">상세보기</span></a>
					</div>
					<div class="regi_ck">
						<label class="regi_event" for="regi_event">
							<input type="checkbox" id="regi_event" name="regi_event" />
								<span>정보/이벤트 메일 수신 동의합니다.(선택)</span>
						</label>
					</div>
					<input type="submit" id="login_ok" name="login_ok" value="회원가입">
				</form>
			</fieldset>
			

			<div class="login_or"> <!-- 다른곳 아이디로 로그인 -->
				<ul>
					<li><a href="<?=$social_oauth_url?>facebook"><i class="fa fa-facebook" aria-hidden="true"></i> 페이스북 아이디로 로그인</a></li>
					<li><a href="<?=$twiter_oauth_url?>"><i class="fa fa-twitter" aria-hidden="true"></i> 트위터 아이디로 로그인</a></li>
					<li><a href="<?=$social_oauth_url?>kakao"><img src="<?=NM_MO_IMG?>ct/login_kakao.png" alt="카카오 로고" /> 카카오 아이디로 로그인</a></li>
					<li><a href="<?=$social_oauth_url?>naver"><img src="<?=NM_MO_IMG?>ct/login_naver.png" alt="네이버 로고" /> 네이버 아이디로 로그인</a></li>
					<!-- 도메인이 없어서 보류
					<li><a href="<?=$social_oauth_url?>google"><img src="<?=NM_MO_IMG?>ct/login_naver.png" alt="네이버 로고" /> 구글 아이디로 로그인</a></li> -->
				</ul>
			</div>

			<div class="login_help">
				문제가 있거나 궁금한 점이 있으시면<br />
				<a href="mailto:<?=$nm_config['cf_admin_email'];?>"><?=$nm_config['cf_admin_email'];?></a> 으로 문의주시기 바랍니다.
			</div>
		</div>