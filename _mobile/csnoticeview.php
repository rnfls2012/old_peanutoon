<?
include_once '_common.php'; // 공통
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/cs.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/cs.js<?=vs_para();?>"></script>
		
		<div class="notice_view">
			<table>
				<tr>
					<th><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><?=" ".$notice_view['bn_title'];?></th>
				</tr>
				<tr>
					<td>
						<div class="editor">
							<?=$notice_view['bn_text'];?>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="notice_back">
			<a href='<?=NM_URL."/cscenter.php?menu=1";?>'>목록으로</a>
		</div>