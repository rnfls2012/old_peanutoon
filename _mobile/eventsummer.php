<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/eventsummer.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/eventsummer.js<?=vs_para();?>"></script>
		<style type="text/css">
			.ModalPopup {
				background-image: url(<?=$summer_img;?>/bg12.png<?=vs_para();?>);
				background-color: #5ecbff;
			}
			.ModalPopup .p_content {
				background-image: url(<?=$summer_img;?>/bg13.png<?=vs_para();?>);
				background-size: 100%;
				background-repeat: no-repeat;
				background-position: center top;
			}

		</style>
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<input type="hidden" id="member_no" class="member_no" value="<?=$nm_member['mb_no'];?>" />
		<input type="hidden" id="total_coupon" class="total_coupon" value="<?=$total_coupon;?>" />

		<div class="evt-container">
			<!-- Modal Popup -->
			<div class="Modalalert">
				<div class="ModalBackground"></div>
				<div class="ModalPopup">
					<div class="ModalClose"><i class="material-icons">clear</i></div>
					<div class="p_content">
						<div class="p_coupon">
							<img src="<?=$summer_img;?>/p_title.png<?=vs_para();?>" alt="15% 결제 할인 쿠폰">
						</div>
						<div class="p_get">
							<img src="<?=$summer_img;?>/p_warn.png<?=vs_para();?>" alt="쿠폰 사용 유의사항">
						</div>
						<ul>
							<li><button class="p_buy" onclick="location.href='<?=NM_URL?>/recharge.php'">결제하기</button></li>
							<li><button class="p_couponpage" onclick="location.href='<?=NM_URL?>/coupon.php'">쿠폰 보기</button></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /Modal Popup -->
			<div class="evt-header">
				<img src="<?=$summer_img;?>/title.png<?=vs_para();?>" alt="피너툰 패밀리 이벤트" />
			</div>
			<div class="evt-content-wrap">
				<div class="evt-content">
					<div class="coupon">
						<div class="couponimg">
							<img src="<?=$summer_img;?>/coupon01.png<?=vs_para();?>" alt="땅콩 결제 15% 쿠폰" />
						</div>
						<div class="couponbadge">
							<img src="<?=$summer_img;?>/coupon_badge.png<?=vs_para();?>" alt="선착순 200명">
						</div>
						<div class="couponpara">
							남은 수량 : <span><?=$total_coupon;?></span>장
						</div>
						<div class="couponget">
							<button id="open_btn" class="<?=$nochk;?>"><?=$coupon_off_text;?></button>
						</div>
					</div>
					<img src="<?=$summer_img;?>/c1_info.png<?=vs_para();?>" alt="1인 1인 1회에 한하여 쿠폰발급이 가능합니다. ">
				</div>
			</div>
			<div class="evt-warn"><img src="<?=$summer_img;?>/c3_warn.png<?=vs_para();?>" alt="이벤트 주의사항"></div>
		</div>