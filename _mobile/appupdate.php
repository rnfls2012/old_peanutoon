<? include_once '_common.php'; // 공통 
?>

<!-- body space -->
	<!-- wrapper space -->

		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/appupdate.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/appupdate.js<?=vs_para();?>"></script>
		
		
		<div id="header">
			<a href="<?=NM_URL?>">
				<img src="<?=NM_MO_IMG?>appupdate/pean_minilogo.png" alt="<?=$head_title?> PLUS" />
				<span><?=$head_title?> PLUS</span>
			</a>
		</div>

		<div id="contents">
			<h2>
				업데이트 내용 (현재버전: <?=$user_appver_txt;?>)
				<? if(mb_class_permission('a') || is_nexcube()){
					echo "[".floatval($_SESSION['appver'])."]";
				} ?>
			</h2>
			<div class="vernotice_con">
				<p>
					APP <?=floatval($nm_config['cf_appsetapk']);?> ver 업데이트 <br />
					(2017.12.21)
					<!--(2017.12.11)-->
				</p>
				<ol>
					<li>
						앱 결제 시 10% 할인 적용 추가<br />
						피너툰 Plus 앱 고객께만 드리는 혜택!<br />
						피너툰 Plus 앱에서 결제 시 언제나<br />
						10% 할인 적용을 받으실 수 있습니다.<br />
						<strong>※ APP 1.3 ver 이하에서는 <br />
						&nbsp;&nbsp;&nbsp;
						10% 할인 적용이 안됩니다.</strong>
					</li>
					<li><strong style="color: red;">뒤로가기 시 앱 종료 버그 수정</strong></li>
					<!--
					<li>푸시 기능 개선</li>
					<li>안정성 개선</li>
					-->
				</ol>
				<div class="vernotice_guide">
				</div>
			</div>
			<div class="vernotice_footer">
				<button onclick="app_update('<?=$user_appver;?>','<?=floatval($nm_config['cf_appsetapk']);?>');">업데이트</button>
				<button onclick="appupdate_later()">다음에 하기</button>
			</div>
		</div>