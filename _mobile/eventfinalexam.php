<?php

?>

<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL?>/css/eventfinalexam.css<?=vs_para()?>" />
<script type="text/javascript" src="<?=NM_MO_URL?>/js/eventfinalexam.js<?=vs_para()?>"></script>
	<style type="text/css">		
		.evt-container {
			background: url(<?=$finalexam_img_url;?>/bg01.jpg<?=vs_para();?>);
			background-color: #ecece1;
		}
		.question-result .q_score span:after {
			background:url(<?=$finalexam_img_url;?>/pencel.png<?=vs_para();?>);
			background-size: 100%;
			background-repeat: no-repeat;
		}
	</style>

	<div class="evt-container">
		<div class="question">
			<div class="question-header">
				<div class="q_part"><span><?=$efp_arr['efp_mark'];?>교시</span></div>
				<div class="q_section"><h3><?=$efp_arr['efp_title'];?></h3></div>
				<div class="q_time"><span><?=$ef_time;?></span></div>
			</div>
			<div class="question-contents">
				<? /* 시험 리스트 */ ?>
				<form name="ef_question_form" id="ef_question_form" enctype="multipart/form-data" method="post" action="<?=NM_PROC_URL?>/eventfinalexam.php" onsubmit="return ef_question_submit();">
				<input type="hidden" name="efp_no" value="<?=$efp_arr['efp_no'];?>">
				<input type="hidden" name="ef_count" value="<?=$ef_count?>">
				<input type="hidden" name="ef_time" value="<?=$ef_time;?>" id="ef_time">

				<? foreach ($ef_arr as $ef_key => $ef_val) { ?>
					<div class="ef_list <?=$ef_val['ef_class'];?>">
						<input type="hidden" name="ef_no[<?=$ef_key;?>]" value="<?=$ef_val['ef_no']?>">
						<div class="q_title"><span><?=$ef_val['ef_no_txt'];?></span>. <?=$ef_val['ef_question']?></div>

						<? if($ef_val['ef_example_img_url'] != ''){ ?>
							<? /* 문제 이미지(보기)*/ ?>
							<div class="q_title_img"><img src="<?=$ef_val['ef_example_img_url'];?>" alt="<?=$ef_val['ef_no_txt'];?>"></div>
						<? } ?>
						
						<? if($ef_val['ef_type'] == 1 || $ef_val['ef_type'] == 3){ ?>
						<!-- 텍스트 지문01 -->
						<ul class="q_text01" data-ef-key="<?=$ef_key?>">
							<li>
								<input type="radio" name="ef_choice[<?=$ef_key?>]" id="<?=$ef_val['ef_no_txt'];?>_1" value="1"/>
								<label for="<?=$ef_val['ef_no_txt'];?>_1">① <?=$ef_val['ef_choice1'];?></label>
							</li>
							<li>
								<input type="radio" name="ef_choice[<?=$ef_key?>]" id="<?=$ef_val['ef_no_txt'];?>_2" value="2"/>
								<label for="<?=$ef_val['ef_no_txt'];?>_2">② <?=$ef_val['ef_choice2'];?></label>
							</li>
							<li>
								<input type="radio" name="ef_choice[<?=$ef_key?>]" id="<?=$ef_val['ef_no_txt'];?>_3" value="3"/>
								<label for="<?=$ef_val['ef_no_txt'];?>_3">③ <?=$ef_val['ef_choice3'];?></label>
							</li>
							<li>
								<input type="radio" name="ef_choice[<?=$ef_key?>]" id="<?=$ef_val['ef_no_txt'];?>_4" value="4"/>
								<label for="<?=$ef_val['ef_no_txt'];?>_4">④ <?=$ef_val['ef_choice4'];?></label>
							</li>
							<li>
								<input type="radio" name="ef_choice[<?=$ef_key?>]" id="<?=$ef_val['ef_no_txt'];?>_5" value="5"/>
								<label for="<?=$ef_val['ef_no_txt'];?>_5">⑤ <?=$ef_val['ef_choice5'];?></label>
							</li>
						</ul>
						<? } ?>

						<? if($ef_val['ef_type'] == 2 || $ef_val['ef_type'] == 4){ ?>
						<ul class="q_img" data-ef-key="<?=$ef_key?>">
							<li>
								<input type="radio" name="ef_choice[<?=$ef_key?>]" id="<?=$ef_val['ef_no_txt'];?>_1" value="1"/>
								<label for="<?=$ef_val['ef_no_txt'];?>_1"><span>①</span> <img src="<?=$ef_val['ef_choice1_img_url'];?>" alt="<?=$ef_val['ef_no_txt'];?>객관식1" /></label>
							</li>
							<li>
								<input type="radio" name="ef_choice[<?=$ef_key?>]" id="<?=$ef_val['ef_no_txt'];?>_2" value="2"/>
								<label for="<?=$ef_val['ef_no_txt'];?>_2"><span>②</span> <img src="<?=$ef_val['ef_choice2_img_url'];?>" alt="<?=$ef_val['ef_no_txt'];?>객관식2" /></label>
							</li>
							<li>
								<input type="radio" name="ef_choice[<?=$ef_key?>]" id="<?=$ef_val['ef_no_txt'];?>_3" value="3"/>
								<label for="<?=$ef_val['ef_no_txt'];?>_3"><span>③</span> <img src="<?=$ef_val['ef_choice3_img_url'];?>" alt="<?=$ef_val['ef_no_txt'];?>객관식3" /></label>
							</li>
							<li>
								<input type="radio" name="ef_choice[<?=$ef_key?>]" id="<?=$ef_val['ef_no_txt'];?>_4" value="4"/>
								<label for="<?=$ef_val['ef_no_txt'];?>_4"><span>④</span> <img src="<?=$ef_val['ef_choice4_img_url'];?>" alt="<?=$ef_val['ef_no_txt'];?>객관식4" /></label>
							</li>
							<li>
								<input type="radio" name="ef_choice[<?=$ef_key?>]" id="<?=$ef_val['ef_no_txt'];?>_5" value="5"/>
								<label for="<?=$ef_val['ef_no_txt'];?>_5"><span>⑤</span> <img src="<?=$ef_val['ef_choice5_img_url'];?>" alt="<?=$ef_val['ef_no_txt'];?>객관식5" /></label>
							</li>
						</ul>
						<? } ?>
						<div class="question-footer">
							<ul data-ef-key="<?=$ef_key?>">
								<? if($ef_key > 0) { ?>
								<li><a><span><i class="fa fa-angle-left" aria-hidden="true"></i></span></a></li>
								<? } ?>
								<li><?=$ef_val['ef_no_txt']?> / <?=$ef_count;?></li>
							</ul>
						</div>
					</div>
				<? } ?>
			</form>
		</div>
	</div>