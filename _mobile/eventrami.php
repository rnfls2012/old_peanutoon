<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-07-10
 * Time: 오전 11:39
 */

$rami_img_path = NM_IMG.$nm_config['nm_mode'].'/event/rami'; // 이미지 경로

?>

<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL?>/css/eventrami.css<?=vs_para()?>" />
<script type="text/javascript" src="<?=NM_MO_URL?>/js/eventrami.js<?=vs_para()?>"></script>

        <div class="evt-container">
            <div class="evt-title">
                <img src="<?=$rami_img_path?>/title.png<?=vs_para()?>" alt="라미, 넌 누구니?" />
            </div>
            <div class="evt-header">
                <img src="<?=$rami_img_path?>/header.png<?=vs_para()?>" alt="피너툰 새 얼굴을 공개합니다" />
            </div>
            <div class="evt-insp">
                <img src="<?=$rami_img_path?>/insp.png<?=vs_para()?>" alt="이벤트 안내" />
            </div>
            <div class="evt-reply">
                <?php
                if ( !$did_comment ) {
                ?>
                <div class="reply_text">
                    <div class="reply_byte">
                        <span class="count_box_character">0</span>
                        /
                        <span class="limit_box_character">50</span>
                    </div>
                    <div class="reply_input">
                        <form  method="post" action="<?=NM_PROC_URL;?>/eventrami_mobile.php" onsubmit="return eventrami_submit();">
                            <input type="hidden" name="mb_no" id="mb_no" value="<?=$nm_member['mb_no']?>" />
                            <textarea placeholder="라미를 위한 응원의 한마디! (50자 이내)" id="comments" name="comments" cols="1" onKeyPress="if (event.keyCode===13) return false;" onKeyUp="countChar(this)"></textarea>
                            <input type="image" src="<?=$rami_img_path?>/submit.png<?=vs_para()?>" name="submit" value="submit">
                        </form>
                    </div>
                </div>
                <?php } else { ?>
                <!-- 댓글 완료시 이미지 -->
                <div class="reply_img">
                    <img src="<?=$rami_img_path?>/reply_ok.png<?=vs_para()?>" alt="참여해주셔서 감사합니다!" />
                </div>
                <?php } ?>
                <div class="reply_list" id="reply_list" data-value="1">
                    <!-- Paging -->

                </div>
            </div>
            <div class="evt-warn">
                <img src="<?=$rami_img_path?>/warn.png" alt="이벤트 유의사항" />
            </div>
        </div>