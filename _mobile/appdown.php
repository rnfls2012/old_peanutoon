<?php
    include_once '_common.php'; // 공통
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->

			<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/appdown.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_MO_URL;?>/js/appdown.js<?=vs_para();?>"></script>
			
			<div id="appdown">
				<div class="evtcontainer">
					<div class="evt_con01">
						<img src="<?=NM_MO_IMG?>appdown/v2/img01.png<?=vs_para();?>" alt="피너툰 완전판 앱 다운로드" />
					</div>
					<div class="evt_con02">
						<a href="<?=NM_URL;?>/app/app_setapk.php">
							<img src="<?=NM_MO_IMG?>appdown/v2/btn.png<?=vs_para();?>" alt="피너툰 완전판 앱 다운로드" />
						</a>
					</div>
					<div class="evt_con03">
						<img src="<?=NM_MO_IMG?>appdown/v2/img02.png<?=vs_para();?>" alt="피너툰 완전판 앱 다운로드" />
					</div>
					<div class="evt_con04">
						<img src="<?=NM_MO_IMG?>appdown/v2/img03.png<?=vs_para();?>" alt="피너툰 완전판 앱 다운로드" />
					</div>
					<div class="evt_con05">
						<a href="<?=NM_URL;?>/app/app_setapk.php">
							<img src="<?=NM_MO_IMG?>appdown/v2/btn02.png<?=vs_para();?>" alt="피너툰 완전판 앱 다운로드" />
						</a>
					</div>
					<div class="evt_con06">
						<img src="<?=NM_MO_IMG?>appdown/v2/img04.png<?=vs_para();?>" alt="피너툰 완전판 앱 다운로드" />
					</div>
				</div>
			</div>