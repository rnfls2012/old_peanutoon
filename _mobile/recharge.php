<? include_once '_common.php'; // 공통 

if(is_mobile()){ $device = "mobile"; }
else{ $device = "pc"; }

// 앱 버전 표기
$recharge_user_appver_txt = "";
if (is_app(1) && is_mobile()){
	$recharge_user_appver_txt = "("."APP ver".floatval($_SESSION['appver']).")";
	// APP sale 버전인 1.4보다 낮을 경우 0으로 처리
	if(floatval($_SESSION['appver']) < floatval(NM_APP_SALE_VER)){ 
		$recharge_user_appver_txt = "(초기버전)"; 
	}
} // end if

?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/recharge.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_MO_URL;?>/js/recharge.js<?=vs_para();?>"></script>
			<!-- 작품리스트 -->
			<div id="recharge">
				<form name="recharge_form" id="recharge_form" method="post" action="<?=NM_PROC_URL;?>/recharge.php" onsubmit="return recharge_payway_submit();">

					<h2><?=$d_logmenu['recharge'];?></h2>
					<div class="mar_section01">
						<div class="mar_myinfo_userinfo">
							<i class="fa fa-user-circle-o" aria-hidden="true"></i> <?=$nm_member['mb_id'];?>님의 <?=$nm_config['cf_cash_point_unit_ko'];?> <!-- 링크 마이페이지로 넘어가게 -->
						</div>
						<div class="mar_mycoin_bg">
							<div class="mar_mycoin">
								<dl>
									<dt>보유 <?=$nm_config['cf_cash_point_unit_ko'];?></dt>
									<dd><span><?=number_format($nm_member['mb_cash_point']);?></span> <img src="<?=NM_IMG.$nm_config['cf_cash'];?>"></dd>
								</dl>
							</div>
							<div class="mar_mycoin">
								<dl>
									<dt>보유 <?=$nm_config['cf_point_unit_ko'];?></dt>
									<dd><span><?=number_format($nm_member['mb_point']);?></span> <img src="<?=NM_IMG.$nm_config['cf_point'];?>"></dd>
								</dl>
							</div>
						</div>

						<div class="mar_myinfo_notice">
							<ol>
								<? for($i=1; $i<=6; $i++) { 
									  if($long_text_row['clt_recharge'.$i] != "") { ?>
										 <li><?=$long_text_row['clt_recharge'.$i];?></li>
								   <? } // end if ?>
								<? } // end for ?>
							</ol>
						</div>
					</div><!-- /mar_section01 -->

					<? if($row_banner['embc_cover'] != ''){ ?>
					<div class="coin_event">
						<img src="<?=$banner_img_url;?>" alt="<?=$banner_val['emb_name'];?>"/>
					</div>
					<? } ?>
					
					<div class="pointpoak_event">
						<a href="<?=NM_URL?>/pointpark.php"><img src="<?=NM_MO_IMG."pointpark/pointpark_mo.png";?>" alt="포인트파크링크"></a>
					</div>

					<div class="mar_section02">
						<?if($crp_event_text != '')?>
						<div class="crp_event_text">
							<?=$crp_event_text;?>
						</div>
						<? if(count($crp_member_arr) > 0){ ?>
							<ol class="crp_event_list">
							<? foreach($crp_member_arr as $crp_key => $crp_val){
								$er_cash_point = $crp_val['er_cash_point'];
								$er_point = $crp_val['er_point'];
								if($crp_val['crp_point'] ==  0){ $data_er_point = 0; }

								// APP sale 이며 버전이 1.4일 경우
								$mar_pay_arr = cs_crp_won_txt($crp_val);
								$mar_pay_con_app_sale = $mar_pay_arr[0];
								$mar_pay_price = $mar_pay_arr[1];
												
							?>
								<li class="clear config_recharge_price_<?=$crp_count_arr?>" id="crp_no_id<?=$crp_val['crp_no']?>" data-crp_cash_point="<?=$crp_val['crp_cash_point'];?>" data-er_cash_point="<?=$crp_val['er_cash_point'];?>" data-crp_point="<?=$crp_val['crp_point'];?>" data-er_point="<?=$data_er_point;?>" data-crp_won="<?=$crp_val['crp_won'];?>" data-crp_coupondc_won="<?=$crp_val['crp_coupondc_won_txt'];?>" data-crp_coupondc="<?=$crp_val['crp_coupondc_txt'];?>">
									<label class="mar_radio" for="crp<?=$crp_count_arr?>">
										<div class="mar_pay_check"> <!-- 라디오버튼 -->
											<input type="radio" id="crp<?=$crp_count_arr?>" name="crp_no" value="<?=$crp_val['crp_no']?>" data-payway_limit="<?=$crp_val['crp_payway_limit_js']?>" <?=$crp_val['crp_no_select']?> />
											<span></span><!-- /라디오버튼 -->
										</div>
										<div class="mar_pay_con <?=$mar_pay_con_app_sale;?>">
											<span><?=cash_point_view($crp_val['crp_cash_point'], 'n', 'y', 'y');?></span>
											<?if($crp_val['crp_point'] > 0){?>
											<span class="minibonus">+ <?=point_view($crp_val['crp_point'], 'n', 'y', 'y');?></span>
											<?}?>
											<?if($crp_val['er_cash_point'] > 0 || $crp_val['er_point'] > 0){?>
											<br />
												<?if($crp_val['er_cash_point'] > 0){?>
												<span class="bonus"><?=cash_point_view($crp_val['er_cash_point'], 'n', 'y', 'y');?></span>
												<?}?>
												<?if($crp_val['er_point'] > 0){?>
												<span class="bonus">+<?=point_view($crp_val['er_point'], 'n', 'y', 'y');?></span>
												<?}?>
											<span class="bonus">더 지급</span>
											<?}?>
										</div>
										<div class="mar_pay_price">
											<?=$mar_pay_price;?>
										</div>
									</label>
								</li>
							<? $crp_count_arr++; }?>
							</ol>
						<? } ?>

						<h3><?=$nm_config['cf_cash_point_unit_ko'];?> 충전<?=$recharge_user_appver_txt;?></h3>
						<ol>
						<? foreach($crp_base_arr as $crp_key => $crp_val){
							$er_cash_point = $crp_val['er_cash_point'];
							$er_point = $crp_val['er_point'];
							if($crp_val['crp_point'] ==  0){ $data_er_point = 0; }

								// APP sale 이며 버전이 1.4일 경우
								$mar_pay_arr = cs_crp_won_txt($crp_val);
								$mar_pay_con_app_sale = $mar_pay_arr[0];
								$mar_pay_price = $mar_pay_arr[1];
						?>
							<li class="clear config_recharge_price_<?=$crp_count_arr?>" id="crp_no_id<?=$crp_val['crp_no']?>" data-crp_cash_point="<?=$crp_val['crp_cash_point'];?>" data-er_cash_point="<?=$crp_val['er_cash_point'];?>" data-crp_point="<?=$crp_val['crp_point'];?>" data-er_point="<?=$data_er_point;?>" data-crp_won="<?=$crp_val['crp_won'];?>" data-crp_coupondc_won="<?=$crp_val['crp_coupondc_won_txt'];?>" data-crp_coupondc="<?=$crp_val['crp_coupondc_txt'];?>">
								<label class="mar_radio" for="crp<?=$crp_count_arr?>">
									<div class="mar_pay_check"> <!-- 라디오버튼 -->
										<input type="radio" id="crp<?=$crp_count_arr?>" name="crp_no" value="<?=$crp_val['crp_no']?>" data-payway_limit="<?=$crp_val['crp_payway_limit_js']?>" <?=$crp_val['crp_no_select']?> />
										<span></span><!-- /라디오버튼 -->
									</div>
									<div class="mar_pay_con <?=$mar_pay_con_app_sale;?>">
										<span><?=cash_point_view($crp_val['crp_cash_point'], 'n', 'y', 'y');?></span>
										<?if($crp_val['crp_point'] > 0){?>
										<span class="minibonus">+ <?=point_view($crp_val['crp_point'], 'n', 'y', 'y');?></span>
										<?}?>
										<?if($crp_val['er_cash_point'] > 0 || $crp_val['er_point'] > 0){?>
										<br />
											<?if($crp_val['er_cash_point'] > 0){?>
											<span class="bonus"><?=cash_point_view($crp_val['er_cash_point'], 'n', 'y', 'y');?></span>
											<?}?>
											<?if($crp_val['er_point'] > 0){?>
											<span class="bonus">+<?=point_view($crp_val['er_point'], 'n', 'y', 'y');?></span>
											<?}?>
										<span class="bonus">더 지급</span>
										<?}?>
									</div>
									<div class="mar_pay_price">
										<?=$mar_pay_price;?>
									</div>
								</label>
							</li>
						<? $crp_count_arr++; }?>
						</ol>
					</div>

					<div class="mar_section03">
						<div class="clear">
							<h3 class="fleft">결제방법</h3>
							<a data-href="<?=NM_URL;?>/coupon.php" class="fright coupondc_btn">쿠폰함</a>
						</div>

						<ol>
							<li>
							<?  $i=0;
								foreach($nm_config['cf_payway'] as $payway_key => $payway_val){
								$i++;

								$payway_view = $payway_val[2];
								$payway_img_name = "";
								
								if($payway_val[3] != ''){ 
									$payway_img_name = trim(str_ireplace($payway_val[0], "", $payway_val[2]));
									$payway_view = '<img src="'.$payway_val[3].'" alt="'.$payway_val[2].'" /><strong>'.$payway_img_name.'</strong>'; 
								}

								$payway_select = "";
								if($payway_key == $_mb_payway_no && $_mb_payway_no != ''){ $payway_select = "checked"; }
								?>
								<label class="mar_pay_radio" for="payway<?=$payway_key?>">
									<div class="mar_pay_method"> <!-- 라디오버튼 -->
										<input type="radio" class="<?=$payway_val[1];?>" id="payway<?=$payway_key?>" name="payway_no" value="<?=$payway_key;?>" <?=$payway_select;?> />
										<span><?=$payway_view?></span><!-- /라디오버튼 -->
									</div>
								</label>
								<? if(($i)%2==0 && count($nm_config['cf_payway'])-1 != $payway_key){?>
								</li>
								<li>
								<? }?>
							<? }?>
							</li>
						</ol>
						<? foreach($nm_config['cf_payway'] as $payway_key => $payway_val) { ?>
						<div id="mar_<?=$payway_val[1];?>_guide" class="mar_guide">
							<input type="hidden" id="clt_<?=$payway_val[1];?>" name="clt_<?=$payway_val[1];?>" value="<?=$clt_row['clt_'.$payway_val[1]];?>">
							<span><?=$payway_val[2];?> 결제 안내</span>
							<?=nl2br($clt_row['clt_'.$payway_val[1]]);?>
						</div>
						<? } ?>
					</div> <!-- / section03 -->
					
					<? if($coupondc_list_ck == true){ ?>
					<!-- 쿠폰 -->
					<div class="coupondc_bg">
						<input type="hidden" name="coupondc" id="coupondc" value="<?=$_mb_coupondc?>">
						<ul>
							<li>
								<span>[<?=$coupondc_list['txt_coupondc_type'];?>] </span> 
								<?=$coupondc_list['txt_coupondc_rate'];?> <strong>적용</strong>
							</li>
							<li><?=$coupondc_list['txt_coupondc_name'];?></li>
							<li>
								발급일 : <?=$coupondc_list['txt_coupondc_date'];?>
							</li>
							<li>
								유효기간 : 
								<?=$coupondc_list['txt_coupondc_available_date_start'];?> ~				<?=$coupondc_list['txt_coupondc_available_date_end'];?>
							</li>
						</ul>
						<div class="coupon_cancel">
							<a data-href="<?=NM_URL;?>/recharge.php" class="coupondc_btn"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<!-- 쿠폰 end -->
					<? } ?>
					
					<?php
					$row_mb = mb_get($nm_member['mb_id']);
					if ( $row_mb['mb_pay_agree'] === 'n' ) {
						?>
						<div class="pay_agree">
							<input type="checkbox" value="y" id="pay_agree" name="mb_agree"/>
							<label for="pay_agree">개인정보 처리방침 동의 (최초 1회)</label>
							<label><a href="<?=NM_DOMAIN?>/siteprivacy.php" id="see_policy" target="_blank">내용보기</a></label>
						</div>
						<?
					}
						
					if(mb_class_permission('a') == true){ /* 181001 결제 테스트모드 */
						?>
						  <div class="pay_agree">
							  <input type="checkbox" value="y" id="cf_pg_test_ctrl" name="cf_pg_test_ctrl" />
							  <label for="cf_pg_test_ctrl">결제테스트 모드 (관리자)</label>
						  </div>
						<?
					}
						
					if(mb_class_permission('a') == true){ /* 181001 결제 패스모드 */
						?>
						  <div class="pay_agree">
							  <input type="checkbox" value="y" id="cf_pg_pass_ctrl" name="cf_pg_pass_ctrl" />
							  <label for="cf_pg_pass_ctrl">결제대행패스-테스트 모드 (관리자)</label>
						  </div>
						<?
					}
					?>

				</form>
				<div class="mar_section04">
					<button  onclick="recharge_payway_submit('<?=$device?>');">결제하기</button>
				</div>
			</div> <!-- /recharge -->