<?php 

?>

<script type="text/javascript" src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL?>/css/eventfinalexam_score.css<?=vs_para()?>" />
<script type="text/javascript" src="<?=NM_MO_URL?>/js/eventfinalexam_score.js<?=vs_para()?>"></script>
	<style type="text/css">
		.evt-container {
			background: url(<?=$finalexam_img_url;?>/bg01.jpg<?=vs_para();?>);
			background-color: #ecece1;
		}
		.question-result .q_score span:after {
			background:url(<?=$finalexam_img_url;?>/pencel.png<?=vs_para();?>);
			background-size: 100%;
			background-repeat: no-repeat;
		}
	</style>

	<div class="evt-container">
		<div class="question">
			<div class="question-header">
				<div class="q_part"><span><?=$efp_arr['efp_mark'];?>교시</span></div>
				<div class="q_section"><h3><?=$efp_arr['efp_title'];?></h3></div>
				<div class="q_time"><span><?=$time;?></span></div>
			</div>
			<div class="question-result">
				<p>나의 점수 :</p>
				<div class="q_score">
					<span><?=$score;?></span>점
				</div>
				<? if(is_app() == false){ ?>
				<div class="q_sns">
					내 덕력 SNS에 공유하기
					<ul>
						<li>
							<button onclick="share_sns('twitter', 'eventfinalexam', '[전국덕후학력평가] 나의 점수는? - <?=$score;?>점');">
								<img src="<?=$finalexam_img_url;?>/sns_twitter.png<?=vs_para();?>" alt="트위터로 공유하기">
							</button>
						</li>
						<!--
						<li>
							<button onclick="share_sns_app('kakaotalk', 'eventfinalexam', '<?=$score;?>점');">
								<img src="<?=$finalexam_img_url;?>/sns_kakao.png<?=vs_para();?>" alt="카카오톡으로 공유하기">
							</button>
						</li>
						-->
					</ul>
				</div>
				<? } ?>
			</div>
			<div class="question-footer">
				<button id="ef_s_end" data-efp-link="<?=NM_URL."/eventfinalexam_reception.php?efp_no=".$efp_no;?>">닫기</button>
			</div>
		</div>
	</div>