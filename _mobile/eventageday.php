<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/eventageday.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/eventageday.js<?=vs_para();?>"></script>

		<div class="evt-container">
			<div class="evt-header">
				<img src="<?=$ageday_img;?>/title.png" alt="20살 축하해!" />
			</div>
			<div class="evt-content">
				<img src="<?=$ageday_img;?>/exp01.png" alt="Event1. 3작품 2땅콩 할인">
				<div class="evt-link">
					<a href="<?=get_comics_url(2773);?>">
						<img src="<?=$ageday_img;?>/link01.png" alt="블라인드 데이트">
					</a>
					<a href="<?=get_comics_url(2718);?>">
						<img src="<?=$ageday_img;?>/link02.png" alt="내 사랑은 고단해">
					</a>
					<a href="<?=get_comics_url(2660);?>">
						<img src="<?=$ageday_img;?>/link03.png" alt="플레이 하우스">
					</a>
				</div>
				<img src="<?=$ageday_img;?>/exp02.png" alt="Event2. 5작품 총 20화 무료" />
				<div class="evt-link">
					<a href="<?=get_comics_url(2531);?>">
						<img src="<?=$ageday_img;?>/link04.png" alt="키스까지 앞으로 1초">
					</a>
					<a href="<?=get_comics_url(2580);?>">
						<img src="<?=$ageday_img;?>/link05.png" alt="몰락 왕자의 달콤한 키스">
					</a>
					<a href="<?=get_comics_url(2553);?>">
						<img src="<?=$ageday_img;?>/link06.png" alt="매일 너랑 첫 키스">
					</a>
					<a href="<?=get_comics_url(2931);?>">
						<img src="<?=$ageday_img;?>/link07.png" alt="키스하는 짐승">
					</a>
					<a href="<?=get_comics_url(1838);?>">
						<img src="<?=$ageday_img;?>/link08.png" alt="너는 키스를 거부할 수 없어">
					</a>
				</div>
				<img src="<?=$ageday_img;?>/blank.png" alt="블랭크 이미지" />
			</div>

			<div class="evt-warn"><img src="<?=$ageday_img;?>/warn.png" alt="이벤트 주의사항"></div>
		</div>