<?
include_once '_common.php'; // 공통
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/my.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/my.js<?=vs_para();?>"></script>

		<div id="comics">
			<div class="detail_header">
				<div class="d_h_top"><img src="<?=$comics['cm_cover_url'];?>" alt="<?=$comics['cm_series'];?>" /></div>
				<div class="d_h_top_visibility visibility"><img src="<?=$comics['cm_cover_url'];?>" alt="<?=$comics['cm_series'];?>" /></div>
				
				<div class="d_h_con">
					<div class="d_h_title">
						<h4><?=$comics['cm_series'];?></h4>
						<p><?=$comics['cm_professional_info'];?></p>
					</div>
					<span class="d_h_gen"><?=$small_txt;?></span>

					<div class="d_h_ex"><?=stripslashes($comics['cm_somaery']);?></div>
				</div> <!-- /d_h_con -->
			</div> <!-- /detail_header -->

			<div class="libd_con">
			<!-- 화 리스트 전체 배열 시작 -->
			<? foreach($episode_arr as $total_key => $total_val) {	
					// 이미지 url
					$img_url = img_url_para($total_val['ce_cover'], $total_val['ce_reg_date'], $total_val['ce_mod_date']);
					if($img_url == "") {
						$img_url = NM_MO_IMG."common/no_img.png";
					} // end if

					// 활성화, 비활성화 클래스 / 버튼 활성화, 비활성화
					$activate = "libd_thumb";
					$activate_num = "libd_thumb_num";
					$button_class = "button_off";
					if($total_val['ce_pay_unit'] == "0땅콩") { $total_val['ce_pay_unit'] = "무료"; } // 소장구분 버튼으로 변경 0802
					$button_txt = $total_val['ce_pay_unit'];
					if($total_val['ce_purchased'] == '') {
						$activate .= " none";
						$activate_num .= " gray";
						$button_class = "";
						$button_txt .= " 소장";
					} // end if

					/* 업데이트 표시할 시 사용
					// 화 업데이트 유무
					$update = "icon_down";
					if($total_val['ce_icon_up'] != "") {
						$update = "icon_up";
					} // end if
					*/

					// 이벤트 구분
					$ce_txt = $total_val['ce_txt'];
					if($total_val['ce_event_free'] == "event_free") { // 이벤트 무료 일때
						$ce_txt .= " [무료 이벤트!]";
					} else if($total_val['ce_event_sale'] == "event_sale") { // 이벤트 할인 일때
						$ce_txt .= " [할인 이벤트!]";
					} // end else if

					/* 소장구분 버튼으로 변경 0802
					// 버튼 텍스트 구분
					if(($total_val['ce_purchased'] == '' && $total_val['ce_notice'] != '') || 
						($total_val['ce_purchased'] == '' && $total_val['ce_pay_unit'] == '무료')) { // 공지사항일때, 무료일때
						$button_txt = $total_val['ce_pay_unit'];
					} // end if
					*/
			?>
					<!-- 화 리스트 -->
					<?=$total_val['ce_url'];?>
						<div class="<?=$activate;?>">
							<!-- 업데이트 넣을시 사용
							<div class="<?=$update?>">
								<img src="<?=NM_MO_IMG?>common/icon_up.png" alt="update" />
							</div>
							-->
							<div class="libd_thumb_img">
								<img src="<?=$img_url;?>" alt="<?=$total_val['ce_chapter'];?>" />
								<button class="<?=$button_class;?>">
									<?=$button_txt;?>
								</button>
							</div>					
							<div class="<?=$activate_num;?>">
								<?=$ce_txt;?>
							</div>
						</div>
					</a>
		 <? } // end foreach ?>
			</div>
		</div>
