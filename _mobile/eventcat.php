<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<style type="text/css">
			.evt-container {
				position: relative;
				width: 100%;
				background-color: #ea284d;
			}
			.evt-container img {
				width: 100%;
				height: auto;
			}
			.evt-header {
				position: relative;
				width: 100%;
			}
			.evt-content {
				position: relative;
				width: 100%;
			}
		</style>

		<div class="evt-container">
			<div class="evt-header">
				<img src="<?=$cat_img;?>/title01.png<?=vs_para();?>" alt="고양이 아가씨와 경호원들 100화기념" />
			</div>
			<div class="evt-content">
				<div class="evt-link">
					<img src="<?=$cat_img;?>/title02.png<?=vs_para();?>" alt="냥냥웹툰 2땅콩 할인전" />
					<a href="<?=get_comics_url(978);?>">
						<img src="<?=$cat_img;?>/link01.png<?=vs_para();?>" alt="고양이 아가씨와 경호원들" />
					</a>
					<a href="<?=get_comics_url(2754);?>">
						<img src="<?=$cat_img;?>/link02.png<?=vs_para();?>" alt="고양이 패닉" />
					</a>
					<img src="<?=$cat_img;?>/title03.png<?=vs_para();?>" alt="고양잇과(?) 추천 컬렉션" />
					<a href="<?=get_comics_url(3043);?>">
						<img src="<?=$cat_img;?>/link03.png<?=vs_para();?>" alt="야옹 하고 울 테니 사랑해줘" />
					</a>
					<a href="<?=get_comics_url(2803);?>">
						<img src="<?=$cat_img;?>/link04.png<?=vs_para();?>" alt="잠든 고양이는 입맞춤으로 눈뜨지 않는다" />
					</a>
					<a href="<?=get_comics_url(2552);?>">
						<img src="<?=$cat_img;?>/link05.png<?=vs_para();?>" alt="천진난만 강아지와 내숭쟁이 고양이" />
					</a>
					<a href="<?=get_comics_url(2088);?>">
						<img src="<?=$cat_img;?>/link06.png<?=vs_para();?>" alt="버려진 고양이를 찾습니다" />
					</a>
					<a href="<?=get_comics_url(2813);?>">
						<img src="<?=$cat_img;?>/link07.png<?=vs_para();?>" alt="내가 너를 길러주지" />
					</a>
					<a href="<?=get_comics_url(3014);?>">
						<img src="<?=$cat_img;?>/link08.png<?=vs_para();?>" alt="첫 경험 상대는 야수였습니다" />
					</a>
				</div>
			</div>

			<div class="evt-warn"><img src="<?=$cat_img;?>/warn.png<?=vs_para();?>" alt="이벤트 유의사항" /></div>
		</div>