<?
include_once '_common.php'; // 공통
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/event.css<?=vs_para();?>" />
		<style type="text/css">
			#evtattcontainer .evt_con { background-color: #<?=$ea_arr['ea_bgcolor'];?>; } /* 전체 배경 */
			#evtattcontainer .evt_reward { border:5px solid #<?=$ea_arr['ea_cal_bandcolor'];?>; } /* 모바일에만 있는 css 달력 테두리 색상이랑 공유 */
			#evtattcontainer .evt_cal_wrap { background-color:#<?=$ea_arr['ea_cal_bgcolor'];?>; border:5px solid #<?=$ea_arr['ea_cal_bandcolor'];?>; border-top:none; } /* 달력 배경색상, 달력 테두리색상 cms 제어 */
			#evtattcontainer .evt_cal_month { border-bottom: 5px solid #<?=$ea_arr['ea_etc_bgcolor'];?>; } /* 버튼, 밑줄, 미니땅콩 개수 배경 */
			#evtattcontainer .evt_cal_myreward span strong { background-color: #<?=$ea_arr['ea_etc_bgcolor'];?>; color: #<?=$ea_arr['ea_etc_fontcolor'];?>; }
			#evtattcontainer .evt_cal_chkbtn button { background-color: #<?=$ea_arr['ea_etc_bgcolor'];?>; color: #<?=$ea_arr['ea_etc_fontcolor'];?>; }
		</style>
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/event.js<?=vs_para();?>"></script>

		<div id="evtattcontainer">

			<div class="evt_wrapper">
				<div class="evt_header">
					<img src="<?=$attend_img_header;?>" alt="출석체크 이벤트" />
				</div>
				<div class="evt_con">
					<div class="evt_reward">
						<img src="<?=$attend_img_reward;?>" alt="리워드" />
					</div>

					<!-- Calender area -->
					<div class="evt_cal_wrap">
						<div class="evt_cal_header">
							<div class="evt_cal_month"><span><?=$ea_arr['ea_month'];?></span><?=$month_eng;?></div>
							<div class="evt_cal_header_right">
							<form name="attend_event" id="attend_event" method="post" action="<?=NM_PROC_URL;?>/eventattend.php" onsubmit="return eventattend_submit();">
								<div class="evt_cal_myreward">내가 받은 <?=$nm_config['cf_point_unit_ko'];?> <span><strong><?=$member_attend_point_sum;?></strong>개</span></div>
								<div class="evt_cal_chkbtn">
									<button type="submit">출석</button>
								</div>
							</form>
							</div>
						</div><!-- /evt_cal_header -->
						<div class="evt_cal">
							<ul>
								<li class="cal_day">
									<div class="sun">SUN</div>
									<div class="mon">MON</div>
									<div class="tue">TUE</div>
									<div class="wed">WED</div>
									<div class="thu">THU</div>
									<div class="fri">FRI</div>
									<div class="sat">SAT</div>
								</li>
								<?
								foreach($cal_arr as $cal_key => $cal_val) {
									// 줄바꿈 시작
									if($cal_val['li_count'] == 0) { echo "<li>"; }

									// class 선언
									$class_group = $cal_val['empty_class']." ".$cal_val['weekend_class']." ".$cal_val['today_class'];

									// 날짜 출력
									$day_no = "<time>".$cal_val['date_count']."</time>";
									if($cal_val['empty_class'] != "") {
										$day_no = "";
									} // end if
								?>
									<div class="<?=$class_group?>">
										<?=$day_no;?>
										<?=$cal_val['stamp_img'];?>
									</div>
								<?
									// 줄바꿈 끝
									if($cal_val['li_count'] == 6) { echo "</li>"; }
								} // end foreach
								?>
							</ul>
						</div><!-- /evt_cal_day -->
					</div><!-- /evt_cal_wrap -->
					<div class="evt_warn">
						<img src="<?=$attend_img_warning;?>" alt="이벤트 주의사항" />
					</div>
				</div>
			</div>