<?
include_once '_common.php'; // 공통
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/cs.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/cs.js<?=vs_para();?>"></script>
			<div class="qnaw_form">
				<form name="askwrite" id="askwrite" method="post" action="<?=NM_PROC_URL;?>/csaskwrite.php" onsubmit="return csaskwrite_submit();">
				<input type="hidden" id="ba_category" name="ba_category" value="" />
					<div class="qnaw_select">
						<ul>
							<li class="qnaw_select_1">문의 분류 선택</li>
							<li class="qnaw_select_list">
								<div class="qnaw_select_2" data-value="0">결제 관련</div>
								<div class="qnaw_select_3" data-value="1">성인인증 관련</div>
								<div class="qnaw_select_4" data-value="2">가입 / 탈퇴</div>
								<div class="qnaw_select_5" data-value="3">제휴</div>
								<div class="qnaw_select_6" data-value="4">기타</div>
							</li>
						</ul>
					</div>
					<input type="text" id="qnaw_title" name="qnaw_title" placeholder="제목" />
					<textarea id="qnaw_request" name="qnaw_request" placeholder="내용을 입력하세요"></textarea>
					<div class="csask clear">
						<input type="submit" id="qnaw_ok" name="qnaw_ok" value="확인" />
						<a href='<?=NM_URL."/cscenter.php?menu=3";?>'>취소</a>
					</div>
				</form>
			</div>