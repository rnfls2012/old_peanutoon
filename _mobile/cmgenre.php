<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<? include NM_MO_PART_PATH.'/event.php';		// [mobile] 서브슬라이드?>

			<? include NM_MO_PART_PATH.'/banner.php';		// [mobile] 배너 스크롤?>
			
			<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/cmgenre.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_MO_URL;?>/js/cmgenre.js<?=vs_para();?>"></script>
			<!-- 작품리스트 -->
			<div id="cmgenre" class="container_bg">
				<div class="sub_list clear" data-limit_define="<?=NM_SUB_LIST_LIMIT;?>" data-limit="<?=$sub_list_limit;?>" data-total="<?=$row_total;?>" data-url="<?=$_SERVER['REQUEST_URI'];?>" data-small="<?=$_menu;?>" data-bool="<?=$sub_list_bool;?>"> 
					<? foreach($cmgenre_arr as $cmgenre_key => $cmgenre_val){ ?>
					<a href="<?=$cmgenre_val['cm_serial_url']?>" target="_self" data-week="<?=$cmgenre_val['cm_week_txt']?>">
						<div class="sub_list_thumb">
							<div class="icon_view">
								<?=$cmgenre_val['cm_free_mark'];?>
								<?=$cmgenre_val['cm_sale_mark'];?>
							</div>
							<div class="img_view">
								<img class="<?=$date_up_class;?>" src="<?=$cmgenre_val['cm_cover_sub']?>" alt="<?=$cmgenre_val['cm_series']?>" />
								<div class="icon_view_bottom">
									<?=$cmgenre_val['cm_adult_mark'];?>
									<?=$cmgenre_val['cm_up_icon'];?>
									<?=$cmgenre_val['cm_new_icon'];?>
								</div>
							</div>
							<dl>
								<dt class="ellipsis">
									<span class="cm_title"><?=str_replace("[웹툰판]", "", $cmgenre_val['cm_series']);?></span>
								</dt>
								<dd class="ellipsis"><?=$cmgenre_val['cm_small_span']?><?=$cmgenre_val['cm_end_span']?></dd>
								<dd class="ellipsis"><?=$cmgenre_val['cm_professional_info']?></dd>
							</dl>
						</div>
					</a>
					<? } /* foreach($cmserial_arr as $cmserial_key => $cmserial_val){  */ ?>
				</div>
			</div>