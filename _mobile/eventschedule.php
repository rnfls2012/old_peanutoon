<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<style type="text/css">
			.evt-container {
				position: relative;
				width: 100%;
				background-color: #16c0ff;
			}
			.evt-container img {
				width: 100%;
				height: auto;
			}

			.evt-content {
				position: relative;
				width: 100%;
			}
		</style>

		<div class="evt-container">
			<div class="evt-content">
				<img src="<?=$schedule_img;?>/cal01.png<?=vs_para();?>" alt="피너툰 스케줄러 6월의 신작" />
			</div>
			<div class="evt-content">
				<a href="<?=NM_URL."/cmnew.php";?>">
					<img src="<?=$schedule_img;?>/cal_btn.png<?=vs_para();?>" alt="신작 보러 가기" />
				</a>
			</div>
			<div class="evt-content">
				<img src="<?=$schedule_img;?>/cal02.png<?=vs_para();?>" alt="알려드립니다" />
			</div>
		</div>