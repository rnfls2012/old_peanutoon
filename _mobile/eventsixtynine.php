<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/eventsixtynine.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/eventsixtynine.js<?=vs_para();?>"></script>

		<div class="evt-container">
			<div class="evt-header">
				<img src="<?=$sixtynine_img;?>/title.png<?=vs_para();?>" alt="6월 9일 단 하루! 6/9DAY를 위한 특별한 2땅콩 할인 이벤트" />
			</div>
			<div class="evt-content">
				<div class="evt-link">
					<a href="<?=get_comics_url(2851);?>">
						<img src="<?=$sixtynine_img;?>/link01.png<?=vs_para();?>" alt="낭만주의자와 쾌락주의자가 만났을 때">
					</a>
					<a href="<?=get_comics_url(1308);?>">
						<img src="<?=$sixtynine_img;?>/link02.png<?=vs_para();?>" alt="너에게 바치는 7일">
					</a>
					<a href=<?=get_comics_url(2151);?>#">
						<img src="<?=$sixtynine_img;?>/link03.png<?=vs_para();?>" alt="난봉꾼과 왕자님">
					</a>
					<a href="<?=get_comics_url(2073);?>">
						<img src="<?=$sixtynine_img;?>/link04.png<?=vs_para();?>" alt="죽은 까마귀의 시선">
					</a>
				</div>
			</div>

			<div class="evt-warn"><img src="<?=$sixtynine_img;?>/warn.png<?=vs_para();?>" alt="이벤트 주의사항"></div>
		</div>