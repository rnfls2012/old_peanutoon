function share_sns(name, type){
	var sns_url = "";
	var share_url	= encodeURIComponent(document.URL);
	var share_text	= encodeURIComponent(document.title);

	var app = false;

	switch(name){
		case 'facebook' : 
			sns_url = "http://www.facebook.com/share.php?u=" + share_url + "&t=" + share_text;
		break;
		
		case 'twitter' : 
			sns_url = "https://twitter.com/intent/tweet?url=" + share_url + "&text=" + share_text;
		break;
		
		case 'kakaostory' : 
			sns_url = "https://story.kakao.com/share?url=" + share_url + "&text=" + share_text; 
		break;
		
		case 'googleplus' : 
			sns_url = "https://plus.google.com/share?url=" + share_url + "&t=" + share_text; 
		break;
		
		case 'naver_blog' : 
			sns_url = "http://blog.naver.com/openapi/share?url=" + share_url + "&title=" + share_text; 
		break;
		
		case 'naver_band' : 
			sns_url = "http://band.us/plugin/share?body=" + share_text + "  " + share_url + "&route=" +share_url; 
		break;
		
		case 'line' : 
			sns_url = "http://line.me/R/msg/text/?" + share_text + "  " + share_url; 
		break;

		default :
			alertBox('SNS가 아닙니다(관리자에게 문의 바랍니다).');
			return;
		break;
	}

	share_sns_stats(name, type);

	popup(sns_url,name);
}


function share_sns_stats(name, type){
	share_sns_stats_access = $.ajax({
		url: nm_url +"/ajax/share_sns_stats.php",
		dataType: "json",
		type: "POST",
		data: { name: name, type: type },
		beforeSend: function( data ) {
			//console.log('로딩');
		}
	})
	share_sns_stats_access.done(function( data ) {
		//console.log('성공');
	});
	share_sns_stats_access.fail(function( data ) {
		//console.log('실패');
	});
	share_sns_stats_access.always(function( data ) {
		//console.log('로딩삭제');
	});
}

function share_sns_app(name, type){
	var sns_url = "";
	var share_url	= document.URL;
	var share_text	= document.title;

	share_sns_stats(name, type);

	sns_image = $( 'meta[property="og:image"]' ).attr( 'content' );
	switch(name){
		case 'kakaotalk' : 
			try {
				Kakao.init('d363cc160a2914420143dca47c8324f3'); 
				Kakao.Link.sendTalkLink({ 
					label: document.title, 
					image: { 
						src: sns_image, 
						width: '300', 
						height: '200' 
					},
					webButton: { 
						text: document.title, 
						url: nm_url+location.pathname + location.search
					} 
				});	
			} catch(e) {
				Kakao.Link.sendTalkLink({ 
					label: document.title, 
					image: { 
						src: sns_image, 
						width: '300', 
						height: '200' 
					},
					webButton: { 
						text: document.title, 
						url: nm_url+location.pathname + location.search
					} 
				});					
			};
		break;
		
		case 'facebook' : 
		/*
			FB.api('/me', 'post', {"message":"하이"},
			function(response) {
      	console.log('Successful login for: ' + response);
      	if(response && !response.error){
      		alert('성공');
      	}else{      		
      		alert('실패');      	
      	}
    });
    
    var share = {
        method: 'stream.share',
        u: 'http://thinkdiff.net/'
    }; 
    
    FB.ui(share, function(response) { 
			window.close();
			self.close();
			window.open(name, "_self").close();
    });
    $.ajaxSetup({ cache: true });
        $.getScript('//connect.facebook.net/ko_KR/sdk.js', function(){
        FB.init({
          appId: '830243330364119',
          version: 'v2.10'
        });
        FB.ui({
            method: 'share',
            title: '공유 테스트 나팔나팔나팔',
            description: '배고프고 졸리고 집에가고 싶은데 멀고 언제가나~ 나팔나팔',
            href: 'https://www.youtube.com/watch?v=tF27TNC_4pc',
          },
          function(response) {
            if (response && !response.error_code) {
              alert('공유 되쪄염뿌');
            } else {
              alert('공유 안되었유');
            }
        });
  	});
    */			
		break;

		default :
			alertBox('SNS APP이 아닙니다(관리자에게 문의 바랍니다).');
			return;
		break;
	}
}

