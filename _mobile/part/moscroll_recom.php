<? include_once '_common.php'; // 공통 

/* 경로: /_mobile/comicsviewscroll.php */

// 보여주기 갯수
$recommend_count = 8;

/* DB */
$recommend_arr = array();
$recommend_comics = get_comics($_comics);

$recommend_merge = array();
$recommend_arr1 = $recommend_arr2 = $recommend_arr3 = $recommend_arr4 = $recommend_arr5 = array();

$sql_recommend = "select * from comics 
						where cm_no != '".$recommend_comics['cm_no']."' 
						and cm_no != '1763' 
						and cm_small = '".$recommend_comics['cm_small']."' 
						and cm_service = 'y' ";
$sql_recommend_ck = $sql_recommend;

// 키워드로 작품추천
$cm_ck_id_arr = $cm_ck_id_sub_arr = array();
if($recommend_comics['cm_ck_id_sub'] != ''){
	$cm_ck_id_sub_arr		= explode("|", $recommend_comics['cm_ck_id_sub']);
	$sql_recommend_ck.= "and ( ";
	foreach($cm_ck_id_sub_arr as $cm_ck_id_sub_val){
		$sql_recommend_ck.= "cm_ck_id_sub like '".$cm_ck_id_sub_val."' OR ";
	}
	$sql_recommend_ck = substr($sql_recommend_ck,0,strrpos($sql_recommend_ck, "OR"));
	$sql_recommend_ck.= ") ";
}

// 정렬
$sql_recommend_ck.= "order by cm_episode_date";
$sql_recommend.= "order by cm_episode_date";

$result_recommend_ck = sql_query($sql_recommend_ck);
while ($row_recommend_ck = sql_fetch_array($result_recommend_ck)) {
	$row_recommend_ck['cm_cover_sub_url'] = img_url_para($row_recommend_ck['cm_cover_sub'], $row_recommend_ck['cm_reg_date'], $row_recommend_ck['cm_mod_date'], 'cm_cover_sub', 'tn102x102'); // 이미지
	array_push($recommend_arr1,  $row_recommend_ck);
}

if(count($recommend_arr1) < $recommend_count){ // 키워드값이 $recommend_count개 미만이라면...
	$result_recommend = sql_query($sql_recommend);
	while ($row_recommend = sql_fetch_array($result_recommend)) {
		$row_recommend['cm_cover_sub_url'] = img_url_para($row_recommend['cm_cover_sub'], $row_recommend['cm_reg_date'], $row_recommend['cm_mod_date'], 'cm_cover_sub', 'tn102x102'); // 이미지

		if($row_recommend['cm_big'] == $recommend_comics['cm_big'] && $row_recommend['cm_adult'] == $recommend_comics['cm_adult'] ){
			array_push($recommend_arr2,  $row_recommend);
		}else if($row_recommend['cm_big'] == $recommend_comics['cm_big']){
			array_push($recommend_arr3,  $row_recommend);
		}else if($row_recommend['cm_adult'] == $recommend_comics['cm_adult']){
			array_push($recommend_arr4,  $row_recommend);
		}else{
			array_push($recommend_arr5,  $row_recommend);
		}
	}
	$recommend_merge2345 = array_merge($recommend_arr2, $recommend_arr3, $recommend_arr4, $recommend_arr5); /* 위 결과 병합 */
	shuffle($recommend_merge2345);/* 랜덤으로 섞기 */
	$recommend_merge = array_merge($recommend_arr1, $recommend_merge2345); // 앞에 키워드 무조건 보이기
}else{ // 키워드값이 3개 이상이라면...
	$recommend_merge = $recommend_arr1;	
	shuffle($recommend_merge);/* 랜덤으로 섞기 */
}

// 운영파트-장인영주임요청 181120
/*
1.
3437 코믹스 뷰어 페이지 하단 추천 작품에 
2151 1291 고정해주세요
2.
2151 1291 코믹스 뷰어 페이지 추천 작품에도 
3437 코믹스 고정해주세요
*/
if($_comics == '2151' || $_comics == '1291' || $_comics == '3437'){
	if($_comics == '3437'){ // 1.
		$sql_request_comics = '2151, 1291';	
	}else{ // 2.
		$sql_request_comics = '3437';	
	}
	$sql_request= "select * from comics 
							where cm_no in (".$sql_request_comics.")
							and cm_service = 'y' ";	
	$result_request = sql_query($sql_request);
	while ($row_request= sql_fetch_array($result_request)) {
		$row_request['cm_cover_sub_url'] = img_url_para($row_request['cm_cover_sub'], $row_request['cm_reg_date'], $row_request['cm_mod_date'], 'cm_cover_sub', 'tn102x102'); // 이미지
		array_unshift($recommend_merge, $row_request);
	}
}
// 운영파트-장인영주임요청 181120 end

for($r=0; $r<$recommend_count; $r++){ /* $recommend_count개만 추출 */
	array_push($recommend_arr, $recommend_merge[$r]);
}


?>
<? if(count($recommend_arr) > 0){ ?>
<!-- body space -->
	<!-- wrapper space -->
			<!-- container_bg -->
				<link rel="stylesheet" type="text/css" href="<?=NM_MO_PART_URL;?>/css/moscroll_recom.css<?=vs_para();?>" />
				<script type="text/javascript" src="<?=NM_MO_PART_URL;?>/js/moscroll_recom.js<?=vs_para();?>"></script>
				<div id="moscroll_recom" class="container_bg">
					<div class="moscroll_list clear">
						<h2>추천 작품</h2>
						<div class="moscroll_bg clear">
							<div>
								<ul>
								<?php
									foreach($recommend_arr as $recommend_key => $recommend_val){
										/* 코믹스 URL */
										$recommend_url_link = NM_URL."/comics.php?comics=".$recommend_val['cm_no'];

										/* 이미지 표지 */
										$recommend_img_url = img_url_para($recommend_val['cm_cover_sub'], $recommend_val['cm_reg_date'], $recommend_val['cm_mod_date'], 'cm_cover_sub', 'tn222x222');

										/* 컨텐츠 제목 */ 
										$recommend_series = text_change($recommend_val['cm_series'], "]");
										?>
										<li>
											<div>
												<a href="<?=$recommend_url_link?>" target="_self">
													<dl>
														<dt><img class="recommend_img" src="<?=$recommend_img_url?>" alt="<?=$recommend_val['cm_series']?>"/></dt>
														<dt class="series ellipsis"><?=$recommend_series?></dt>
														<dd class="professional ellipsis"><?=$recommend_val['cm_professional_info']?></dd>
													</dl>
												</a>
											</div>
										</li>
								<? } ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
<? } ?>