<? include_once '_common.php'; // 공통 
?>
<!-- body space -->
	<!-- wrapper space -->
			<!-- container_bg -->
				<script type="text/javascript" src="//developers.kakao.com/sdk/js/kakao.min.js"></script>			
				<link rel="stylesheet" type="text/css" href="<?=NM_MO_PART_URL;?>/css/share_sns.css<?=vs_para();?>" />
				<script type="text/javascript" src="<?=NM_MO_PART_URL;?>/js/share_sns.js<?=vs_para();?>"></script>
				<div id="share_sns" class="container_bg">
					<div class="share_sns_list clear">
						<h2 class="hide">SNS 공유</h2>
						<div class="share_sns_bg clear">
							<? if(is_app() == false){ ?>
							<button onclick="share_sns('facebook', 'comics');"><img src="<?=NM_IMG?>common/share_facebook.png" alt="Facebook 공유" /></button>
							<button onclick="share_sns('twitter', 'comics');"><img src="<?=NM_IMG?>common/share_twitter.png" alt="twitter 공유" /></button>
							<? } ?>
							<? if(is_mobile()){ ?>
							<button onclick="share_sns_app('kakaotalk', 'comics');"><img src="<?=NM_IMG?>common/share_kakao.png" alt="Kakao Talk 공유" /></button>
							<? } ?>
						</div>
					</div>
				</div>