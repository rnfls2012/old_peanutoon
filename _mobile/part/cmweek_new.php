<? include_once '_common.php'; // 공통

/* DB */

$cmweek_new_arr = array();

// 오늘 요일 표기 하기 위한...
$cm_public_cycle_in = get_comics_cycle_fix(week_type(7));

$sql_cm_adult = sql_adult($mb_adult_permission , 'cm_adult');

$sql_cmweek_new = " SELECT c.*, c_prof.cp_name as prof_name
					FROM comics c 
					left JOIN comics_professional c_prof ON c_prof.cp_no = c.cm_professional 
					WHERE 1 $sql_cm_adult 
					AND c.cm_service = 'y' and c.cm_big = 2 and c.cm_end = 'n' 
					AND c.cm_reg_date >= '".NM_TIME_MON_M3_S." ".NM_TIME_HI_start."'
					AND c.cm_reg_date <= '".NM_TIME_YMD." ".NM_TIME_HI_end."'
					ORDER BY cm_reg_date DESC, cm_episode_date DESC";				

// 리스트 가져오기
$cmweek_new_arr =cs_comics_content_list($sql_cmweek_new, substr($nm_config['nm_path'], -1));

?>
<? if(count($cmweek_new_arr) > 0){ ?>
<!-- body space -->
	<!-- wrapper space -->
			<!-- container_bg -->
				<link rel="stylesheet" type="text/css" href="<?=NM_MO_PART_URL;?>/css/cmweek_new.css<?=vs_para();?>" />
				<script type="text/javascript" src="<?=NM_MO_PART_URL;?>/js/cmweek_new.js<?=vs_para();?>"></script>
				<div id="cmweek_new" class="container_bg pdt0 sub_cmweek_new">
					<div class="moscroll_list clear">
						<h2>웹툰 신작 소개</h2>
						<!-- <span class="moreread"><a href="<?=$iscroll_url?>">더보기</a></span> -->
						<div id="moscroll<?=$iscroll_no?>"  class="moscroll_bg clear">
							<div>
								<ul>
								<?php
									foreach($cmweek_new_arr as $cmweek_new_key => $cmweek_new_val){ ?>
										<li>
											<div>
												<a href="<?=$cmweek_new_val ['cm_serial_url'];?>" target="_self">
													<div class="sub_list_thumb">
														<div class="icon_view">
															<?=$cmweek_new_val['cm_free_mark'];?>
															<?=$cmweek_new_val['cm_sale_mark'];?>
														</div>
														<div class="img_view">
															<img class="<?=$date_up_class;?>" src="<?=$cmweek_new_val['cm_cover_sub']?>" alt="<?=$cmweek_new_val['cm_series']?>" />
															<div class="icon_view_bottom">
																<?=$cmweek_new_val['cm_adult_mark'];?>
																<?=$cmweek_new_val['cm_up_icon'];?>
																<?=$cmweek_new_val['cm_new_icon'];?>
															</div>
														</div>
														<dl>
															<dt class="ellipsis">
																<span class="title"><?=str_replace("[웹툰판]", "", $cmweek_new_val['cm_series']);?></span>
															</dt>
															<dd class="ellipsis"><?=$cmweek_new_val['cm_small_span']?><?=$cmweek_new_val['cm_end_span']?></dd>
															<dd class="ellipsis"><?=$cmweek_new_val['cm_professional_info']?></dd>
														</dl>
													</div>
												</a>
											</div>
										</li>
								<? } ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
<? } ?>