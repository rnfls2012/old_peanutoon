<? include_once '_common.php'; // 공통 
// 초기화 - slide_no
if($slide_no == '' || $slide_no < 2){ $slide_no == 2; }

/* DB */
$slide_arr = array();
$sql_slide_adult = sql_adult($mb_adult_permission , 'eme_adult');
$sql_slide = " SELECT * FROM epromotion_event where eme_state = 'y' $sql_slide_adult order by eme_date desc ";
$result_slide = sql_query($sql_slide);
while ($row_slide = sql_fetch_array($result_slide)) {
	array_push($slide_arr,  $row_slide);
}
?>
<? if(count($slide_arr) > 0){ ?>
<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<link rel="stylesheet" type="text/css" href="<?=NM_MO_PART_URL;?>/css/event.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_MO_PART_URL;?>/js/event.js<?=vs_para();?>"></script>


			<script type="text/javascript">
			<!--
				$(document).on('ready', function() {
					$("#mo_slide_event .regular").slick({
						dots: true,
						infinite: true,
						speed: 500,
						slidesToShow: 1,
						slidesToScroll: 1,
						autoplay: true,
						autoplaySpeed: 5000,
						arrows: false
					});
				});
				<? /* fade: true, cssEase: 'linear', 
					  vertical: true, verticalSwiping: true,
				*/ ?>
			//-->
			</script>
			<!-- body space -->
				<!-- wrapper space -->
					<div>
						<div id="mo_slide_event">
							<div class="mo_slide_bg">
								<section class="regular slider">
									<? foreach($slide_arr as $slide_key => $slide_val){ 
										switch($slide_val['eme_event_type']) {
											case "t": $slide_val['eme_url'] = NM_URL."/eventview.php?event=".$slide_val['eme_no'];
											break;
											case "c": $slide_val['eme_url'] = NM_URL."/comics.php?comics=".$slide_val['eme_event_no'];
											break;
											case "e": $slide_val['eme_url'] = NM_URL."/eventlist.php?event=".$slide_val['eme_event_no'];
											break;
											case "u": $slide_val['eme_url'] = $slide_val['eme_direct_url'];
											break;
										} // end switch
										$eme_url = $slide_val['eme_url'];
										/* 이미지 표지 */
										$slide_img_url = img_url_para($slide_val['eme_cover'], $slide_val['eme_date'], '', 760, 500, 'RK');
									?>
									<div>
										<a href="<?=$slide_val['eme_url']?>" target="_self"><img src="<?=$slide_img_url;?>" alt="<?=$slide_val['eme_name'];?>"></a>
									</div>
									<? } ?>
								</section>
							</div>
						</div>
					</div>
<? } ?>