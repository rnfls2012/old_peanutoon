<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/event.css<?=vs_para();?>" />

		<div class="evt_view">
			<table>
				<tr>
					<th><span><i class="fa fa-caret-right" aria-hidden="true"></i></span><?=" ".$row_eventview['eme_name'];?></th>
				</tr>
				<tr>
					<td>
						<div class="editor">
							<?=$row_eventview['eme_text'];?>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="evt_back">
			<a href='<?=NM_URL."/event.php";?>'>목록으로</a>
		</div>