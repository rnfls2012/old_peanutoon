<? include_once '_common.php'; // 공통 ?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<? include NM_MO_PART_PATH.'/event.php';		// [mobile] 서브슬라이드?>

			<? include NM_MO_PART_PATH.'/banner.php';		// [mobile] 배너 스크롤?>
			
			<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/cmvolume.css<?=vs_para();?>" />
			<script type="text/javascript" src="<?=NM_MO_URL;?>/js/cmvolume.js<?=vs_para();?>"></script>
			<!-- 작품리스트 -->
			<div id="cmvolume" class="container_bg">
				<div class="sub_list clear" data-limit_define="<?=NM_SUB_LIST_LIMIT;?>" data-limit="<?=$sub_list_limit;?>" data-total="<?=$row_total;?>" data-url="<?=$_SERVER['REQUEST_URI'];?>" data-small="<?=$small_val;?>" data-bool="<?=$sub_list_bool;?>"> 
					<? foreach($cmvolume_arr as $cmvolume_key => $cmvolume_val){
						$cmvolume_url = get_comics_url($cmvolume_val['cm_no']);

						$small_txt = $nm_config['cf_small'][$cmvolume_val['cm_small']];
					?>
					<a href="<?=$cmvolume_val['cm_serial_url']?>" target="_self">
						<div class="sub_list_thumb">
							<img src="<?=$cmvolume_val['cm_cover_sub']?>" alt="<?=$cmvolume_val['cm_series']?>" />
							<dl>
								<dt class="ellipsis"><?=$cmvolume_val['cm_series']?></dt>
								<dd class="ellipsis"><?=$cmvolume_val['small_txt']?></dd>
								<dd class="ellipsis"><?=$cmvolume_val['cm_professional_info']?></dd>
							</dl>
						</div>
					</a>
					<? } /* foreach($cmserial_arr as $cmserial_key => $cmserial_val){  */ ?>
				</div>
			</div>