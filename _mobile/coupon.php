
<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/coupon.css<?=vs_para();?>" />
<script type="text/javascript" src="<?=NM_MO_URL;?>/js/coupon.js<?=vs_para();?>"></script>

<div class="con">
    <h3>쿠폰함</h3>
    <div class="couponwrap">
        <div class="couponinput">
            <span class="couponinput_title">쿠폰 등록하기</span>
            <span class="couponinput_exp">피너툰에서 발급받은 쿠폰번호를 입력하세요.</span>
            <div class="coupon_text">
                <form name="reg_coupon" id="reg_coupon" action="<?=NM_PROC_URL?>/coupon.php" method="POST" onsubmit="return this;">
                    <input type="text" name="coupon_num" class="coupontext" maxlength="20" placeholder="쿠폰번호를 입력해주세요" />
                    <input type="submit" value="쿠폰등록">
                </form>
                <span class="focus-border"></span>
            </div>
        </div>
    </div>

    <div class="list_area get">
        <span class="list_title">보유 중인 쿠폰</span>
        <input type="hidden" id="coupon_cnt" name="coupon_cnt" value="<?=count($coupon_arr);?>">
        <table cellpadding="0" cellspacing="0">
            <tbody>
            <?
            if (count($coupon_arr) > 0) {
                foreach ($coupon_arr as $coupon_key => $coupon_val) {
                     if (NM_TIME_YMD <= $coupon_val['coupon_end_date'] && $coupon_val['erc_state'] == 'y') {
                        $rate = "";
                        $btn_active = "active";
                        $disabled = "";
                    } else {
                        $rate = "rate";
                        $btn_active = "non_active";
                        $disabled = "disabled";
                    }
                    if ($coupon_val['erc_use'] === 'n') {
                        ?>
                        <tr id="<?= "coupon_".$coupon_key ?>" class="<?=$rate?>">
                            <td>
                                <ul>
                                    <li><span>[<?=$coupon_val['coupon_type']?>]</span> <?=$coupon_val['coupon_name']?></li>
                                    <li><?=$coupon_val['coupon_cont']?></li>
                                    <li><?=get_ymd($coupon_val['erc_date'])?>~<?=$coupon_val['coupon_end_date']?></li>
                                </ul>
                            </td>
                            <td>
                                <a href="<?=$recharge_url?>&mb_coupondc=<?=$coupon_val['erc_no']?>"><button type="button" class="<?=$btn_active?>" <?=$disabled?>>사용</button></a>
                            </td>
                        </tr>
                        <?
                    }
                }
            } else {
                ?>
                <tr>
                    <td align="center">보유중인 쿠폰이 없습니다.</td>
                </tr>
                <?
            }
            ?>
            </tbody>
        </table>

        <div class="list_paging"> <!-- 페이징처리 -->
            <ul>
                <li id="coupon_prev"><i class="fa fa-angle-left" aria-hidden="true"></i></li>
                <li id="coupon_next"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
            </ul>
        </div>
    </div>

    <div class="list_area used">
        <span class="list_title">쿠폰 사용 내역</span>
        <input type="hidden" id="used_cnt" name="used_cnt" value="<?=count($total_used_arr);?>">
        <table cellpadding="0" cellspacing="0">
            <tbody>
            <?
            if (count($total_used_arr) > 0) {
                foreach ($total_used_arr as $total_used_key => $total_used_val) {
                    $coupon_used_date = substr($total_used_val['coupon_used_date'],0,10);
                    ?>
                    <tr id="<?="used_".$total_used_key;?>">
                        <td>
                            <ul>
                                <li><span>[<?=$total_used_val['coupon_type']?>]</span> <?=$total_used_val['coupon_name']?></li>
                                <li><?=$total_used_val['coupon_cont']?></li>
                                <li>사용일시 : <?=$coupon_used_date?></li>
                            </ul>
                        </td>
                    </tr>
                    <?
                }
            } else { ?>
                <tr>
                    <td align="center">사용하신 쿠폰이 없습니다.</td>
                </tr>
            <? } ?>
            </tbody>
        </table>

        <div class="list_paging"> <!-- 페이징처리 -->
            <ul>
                <li id="used_prev"><i class="fa fa-angle-left" aria-hidden="true"></i></li>
                <li id="used_next"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
            </ul>
        </div>
    </div>
</div><!--- /con -->


