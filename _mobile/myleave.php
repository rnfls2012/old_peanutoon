<?
include_once '_common.php'; // 공통
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/my.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/my.js<?=vs_para();?>"></script>
			<div class="con">
				<h3>회원 탈퇴</h3>
				<div class="myp_dropout_warn">
					<?=nl2br($long_text['clt_mb_leave']);?>
				</div>
				<form name="myleave_form" id="myleave_form" method="post" action="<?=NM_PROC_URL;?>/myleave.php" onsubmit="return myleave_submit();">
				<input type="hidden" name="mb_sns_check" id="mb_sns_check" value="<? echo $nm_member['mb_sns_type']==''?"n":"y";?>"/>
				<? if($nm_member['mb_sns_type'] == "") { ?>
					<div class="myp_dropout_pw">
						<input type="password" class="form_field_text" id="myp_drop_pw1" name="pw1">
						<label for="myp_drop_pw1">비밀번호</label>
						</div>
					<div class="myp_dropout_pw">
						<input type="password" class="form_field_text" id="myp_drop_pw2" name="pw2">
						<label for="myp_drop_pw2">비밀번호 확인</label>
					</div>
				<? } // end if ?>
					<div class="myp_dropout_pw">
						<input type="button" class="myp_dropout_ok" id="myp_dropout_ok" name="myp_dropout_ok" value="탈퇴" onclick="myleave('go');">
						<input type="button" class="myp_dropout_no" id="myp_dropout_no" name="myp_dropout_no" value="취소" onclick="myleave('back');">
					</div>
				</form>
			</div><!-- /con -->