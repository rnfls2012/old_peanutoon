<?
include_once '_common.php'; // 공통
?>


<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
		<link rel="stylesheet" type="text/css" href="<?=NM_MO_URL;?>/css/my.css<?=vs_para();?>" />
		<script type="text/javascript" src="<?=NM_MO_URL;?>/js/my.js<?=vs_para();?>"></script>
				<div class="con">
					<div class="myp_info"> <!-- 상단 내정보 -->
						<ul>
							<li>
								<div class="mem_info"><span><?=$nm_member['mb_id'];?></span> 님</div>
								<div class="mem_shop"><a href="<?=NM_URL."/recharge.php"?>"><button class="shop">충전하기</button></a>
							</li>
							<li>
								<div class="mem_point">
									<dl>
										<dt>보유 <?=$nm_config['cf_cash_point_unit_ko'];?></dt>
										<dd><span><?=$nm_member['mb_cash_point'];?></span> <img src="<?=NM_IMG.$nm_config['cf_cash'];?>"></dd>
									</dl>
								</div>
								<div class="mem_mpoint">
									<dl>
										<dt>보유 <?=$nm_config['cf_point_unit_ko'];?></dt>
										<dd><span><?=$nm_member['mb_point'];?></span> <img src="<?=NM_IMG.$nm_config['cf_point'];?>"></dd>
									</dl>
								</div>
							</li>
						</ul>
					</div><!-- /myp_info -->

					<div class="myp_admin"> <!-- 내 정보 관리 -->
						<form name="myinfo_form" id="myinfo_form" method="post" action="<?=NM_PROC_URL;?>/myinfo.php" onsubmit="return myinfo_submit();">
						<input type="hidden" id="mb_no" name="mb_no" value="<?=$nm_member['mb_no'];?>">
							<table>
								<tr>
									<th>본인인증</th>
									<td>
									<? if($nm_member['mb_adult'] == "y" || $nm_member['mb_sex'] != "n") { ?>
										<span class="mem_adult_ok">인증된 회원입니다.</span>
									<? } else { ?>
										<a href="<?=NM_URL."/ctcertify.php";?>"><button type="button" id="mem_adult">본인 인증</button></a>
									<? } // end if ?>
									</td>
								</tr>
								<tr>
									<th>닉네임</th>
									<td>
										<div class="re_nick">
											<input type="text" name="mb_nick" id="mb_nick" class="mb_nick" value="<?=$nm_member['mb_nick']?>">
											<span id="mem_nick"><?=$nm_member['mb_nick'];?></span>
											<button type="button" id="mb_mod_nick" class="mb_write_nick">변경</button>
										</div>
									</td>
								</tr>
								<tr>
									<th>이메일</th>
									<td>
										<div class="re_email">
											<input type="text" name="mb_email" id="mb_email" class="mb_email" value="<?=$nm_member['mb_email']?>">
											<span id="mem_email"><?=$nm_member['mb_email'];?></span>
											<button type="button" id="mb_mod_email" class="mb_mod_email">변경</button>
										</div>
									</td>
								</tr>
								<tr>
									<th>비밀번호 변경</th>
									<td>
										<button type="button" id="mem_pw">변경</button>
										<div class="re_pw">
											<label for="pw1">현재 비밀번호</label> <input type="password" name="pw1" />
											<label for="pw2">변경 비밀번호</label> <input type="password" name="pw2" />
											<label for="pw3">변경 비밀번호 확인</label> <input type="password" name="pw3" />
										</div>
									</td>
								</tr>
								<tr>
									<th>이벤트 / 정보 메일<br>수신 동의</th>
									<td>
										<button type="button" class="<?=($nm_member['mb_post'] == "y")?'on':'';?>" id="mem_mail_ok">동의</button>
										<button type="button" class="<?=($nm_member['mb_post'] == "n")?'on':'';?>" id="mem_mail_no">동의 안 함</button>
										<input type="hidden" id="mem_mail" name="mem_mail" value="">
									</td>
								</tr>
								<tr class="t_last">
									<th>휴면방지기간</th>
									<td>
										<div class="myp_rest">
											<label class="myp_radio" for="mem_rest3"><input type="radio" id="mem_rest3" name="m_radio" value="3" <?=$mem_rest3;?> /><span>3년</span></label>
											<label class="myp_radio" for="mem_rest5"><input type="radio" id="mem_rest5" name="m_radio" value="5" <?=$mem_rest5;?> /><span>5년</span></label>
											<label class="myp_radio" for="mem_restever"><input type="radio" id="mem_restever" name="m_radio" value="out" <?=$mem_restever;?> /><span>탈퇴전까지</span></label>
										</div>
									</td>
								</tr>
							</table>
							<div class="myp_admin_ok"><input type="submit" value="변경사항 저장" /></div>
						</form>
					</div> <!-- /myp_admin -->

					<div class="myp_charge"><!-- 충전내역 -->
						<input type="hidden" id="income_cnt" name="income_cnt" value="<?=count($income_arr);?>">
						<h2>충전내역</h2>
						<table>
							<tr>
								<th class="date">날짜</th>
								<th class="type">지급형태</th>
								<th class="charge"><?=$nm_config['cf_cash_point_unit_ko'];?>/<?=$nm_config['cf_point_unit_ko'];?></th>
								<th class="from">충전내용</th>
							</tr>
							<? if(count($income_arr) > 0) {
									foreach($income_arr as $key => $val) { ?>
										<tr id="<?="income_".$key;?>">
											<td class="t_date"><?=substr($val['mpu_date'], 0, 10);?></td>
											<td><?=$val['mpu_in_type'];?></td>
											<td><?=$val['mpu_recharge_cash_point']." ".$nm_config['cf_cash_point_unit_ko']." / ".$val['mpu_recharge_point']." ".$nm_config['cf_point_unit_ko'];?></td>
											<td><?=$val['mpu_from'];?></td>
										</tr>
							<?		 } // end foreach
								} else { ?>
									<tr>
										<td colspan="4">충전내역이 없습니다.</td>
									</tr>
							<?  } // end else ?>
						</table>

						<div class="myp_paging">
							<ul>
								<li id="income_prev"><i class="fa fa-angle-left" aria-hidden="true"></i></li>
								<li id="income_next"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
							</ul>
						</div><!-- /myp_charge -->
					</div> 


					<div class="myp_use"> <!-- 땅콩 사용내역 -->
						<input type="hidden" id="expen_cnt" name="expen_cnt" value="<?=count($expen_arr);?>">
						<h2><?=$nm_config['cf_cash_point_unit_ko'];?>/<?=$nm_config['cf_point_unit_ko'];?> 사용내역</h2>
						<table>
							<tr>
								<th class="t_date">날짜</th>
								<th class="from">제목 / 회차</th>
								<th class="expen">소모 <?=$nm_config['cf_cash_point_unit_ko'];?>/<?=$nm_config['cf_point_unit_ko'];?></th>
							</tr>
							<? if(count($expen_arr) > 0) {
									foreach($expen_arr as $key => $val) { ?>
										<tr id="<?="expen_".$key;?>">
											<td class="t_date"><?=substr($val['mpu_date'], 0, 10);?></td>
											<td><?=$val['mpu_from'];?></td>
											<td><?=$val['mpu_cash_point']." ".$nm_config['cf_cash_point_unit_ko']." / ".$val['mpu_point']." ".$nm_config['cf_point_unit_ko'];?></td>
										</tr>
							<?		} // end foreach
								} else { ?>
									<tr>
										<td colspan="3"><?=$nm_config['cf_cash_point_unit_ko']."/".$nm_config['cf_point_unit_ko'];?> 사용내역이 없습니다.</td>
									</tr>
							<?  } // end else ?>
						</table>

						<div class="myp_paging">
							<ul>
								<li id="expen_prev"><i class="fa fa-angle-left" aria-hidden="true"></i></li>
								<li id="expen_next"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
							</ul>
						</div>

					</div> <!-- /myp_use -->

					<div class="myp_qna"> <!-- 1:1문의내역 -->
						<input type="hidden" id="ask_cnt" name="ask_cnt" value="<?=count($ask_arr);?>">
						<h2>1:1 문의내역</h2>
						<table>
							<tr>
								<th class="t_date">날짜</th>
								<th class="title">제목</th>
								<th class="mode">답변상태</th>
							</tr>
							<? if(count($ask_arr) > 0) {
									foreach($ask_arr as $key => $val) { ?>
										<tr id="<?="ask_".$key;?>">
											<td class="t_date"><?=substr($val['ba_date'],0 ,10);?></td>
											<td><a href="<?=NM_URL;?>/cscenter.php?menu=3&ba_no=<?=$val['ba_no'];?>"><?=$val['ba_title'];?></a></td>
											<? if($val['ba_mode'] == 0) { ?>
												<td class="t_con"><?=$ask_mode_arr[$val['ba_mode']];?></td>
											<? } else { ?>
												<td class="t_con"><span><?=$ask_mode_arr[$val['ba_mode']];?></span></td>
											<? } ?>
										</tr>
							<?		} // end foreach
								} else { ?>
									<tr>
										<td colspan="3">1:1문의내역이 없습니다.</td>
									</tr>
							<?  } // end else ?>
						</table>

						<div class="myp_paging">
							<ul>
								<li id="ask_prev"><i class="fa fa-angle-left" aria-hidden="true"></i></li>
								<li id="ask_next"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
							</ul>
						</div>

					</div> <!-- /myp_qna -->

					<div class="myp_drop"> <!-- 회원 탈퇴 -->
						<a href="<?=NM_URL;?>/myleave.php"><button>회원 탈퇴</button></a>
					</div> <!-- /myp_drop -->
					
					<!-- 보상 -->
					<? /* // 보상 종료 - 180828
					<? if(mb_get_reward_state($nm_member['mb_no'], 2) == "y" || mb_get_reward_state($nm_member['mb_no'], 3) == "y") { 
						$rm_no = 2; 
						if(mb_get_reward_state($nm_member['mb_no'], 3) == "y") { $rm_no = 3; } ?>
					<div class="my_reward">
						<a href="<?=NM_PROC_URL;?>/myreward.php?member=<?=$nm_member['mb_no'];?>&rewardment=<?=$rm_no;?>"><img src="<?=NM_IMG?>_mobile/event/2017/reward/mo_mypage_btn2.png"></a>
					</div>
					<? } // end if ?>
					*/ ?>
				</div>
		</html>