<?
include_once '_common.php'; // 공통

error_page();


/* DB */

/* comics + episode */
$comics = get_comics($_comics);

// 성인체크
// cs_comics_adult($comics['cm_no'], $nm_member);

$comics['cm_cover_url'] = img_url_para($comics['cm_cover'], $comics['cm_reg_date'], $comics['cm_mod_date'], 'cm_cover', 'tn');
// $comics['cm_cover_sub_url'] = img_url_para($comics['cm_cover_sub'], $comics['cm_reg_date'], $comics['cm_mod_date'], 200, 200, "RK");
$small_txt = $nm_config['cf_small'][$comics['cm_small']];

// 연재주기
$cm_public_cycle_arr = get_comics_cycle_val($comics['cm_public_cycle'], 'y');
if(count($cm_public_cycle_arr) > 0 && $cm_public_cycle_arr[0] != $nm_config['cf_public_cycle'][0][1]){
	krsort($cm_public_cycle_arr);
	foreach($cm_public_cycle_arr as &$cm_public_cyclee_val){
		$cm_public_cyclee_val = "<span class='day'>".$cm_public_cyclee_val."</span>";
	}
	$cm_public_cycle_arr_txt = $nm_config['cf_public_cycle_main'][$comics['cm_public_period']]." ".implode(",",$cm_public_cycle_arr)." 요일 연재";
}else{
	$cm_public_cycle_arr_txt = "연재 ".$cm_public_cycle_arr[0];
}

if($comics['cm_public_cycle'] > $nm_config['cf_public_cycle'][8][0]){ // 보름
	$cm_public_cycle_arr_txt = $nm_config['cf_public_cycle'][8][1]." 연재";
}
if($comics['cm_public_cycle'] > $nm_config['cf_public_cycle'][9][0]){ // 한달
	$cm_public_cycle_arr_txt = $nm_config['cf_public_cycle'][9][1]." 연재";
}

// 에피소드
$episode_arr = cs_get_episode_list($comics['cm_no'], $nm_member, 'y', $_order);
// $episode_arr = cs_get_episode_list($comics['cm_no'], $nm_member, 'y', $_order, substr($nm_config['nm_path'], -1)); 리사이징 적용한 함수, 썸네일 누락으로 인해 보류

$comics_order_asc = $comics_order_desc = "";
if($_order != 'ASC'){ $comics_order_desc = "on"; }
else{ $comics_order_asc = "on"; }

// 고객 캐쉬포인트+포인트 합친 sum_point포인트
$mb_sum_point = $nm_member['mb_cash_point'] + intval( $nm_member['mb_point'] / NM_POINT );

// 화리스트 전체 결제시 추가지급
$sin_allpay_arr_js = cs_sin_allpay('y');

// 첫화 링크
$cs_comic_first_url = cs_comic_first_url($comics['cm_no']);

// 이어보기 링크
$cs_comic_cont_url = "";
$cs_comic_cont_episode = "";
foreach($episode_arr as $episode_key => $episode_val) {
	if($episode_val['ce_bookmark'] != "") {
		$cs_comic_cont_url = get_episode_url($_comics, $episode_val['ce_no']);
		$cs_comic_cont_episode = $episode_val['ce_txt'];
	} // end if
} // end foreach

// 즐겨찾기 확인
$bookmark_query = "select * from member_comics_mark where mcm_member = '".$nm_member['mb_no']."' and mcm_comics = '".$comics['cm_no']."'";
$count_bookmark = sql_num_rows(sql_query($bookmark_query));

// 타이블제목
$head_title = $comics['cm_series']."::".$nm_config['cf_title'];

// SNS 썸네일
$head_thumbnail = NM_IMG.$comics['cm_cover'];

// 소장 가능 화 수 확인
$purchased_cnt = 0;
foreach($episode_arr as $episode_key => $episode_val) {
	if($episode_val['ce_pay'] > 0 && $episode_val['ce_purchased'] == "") {
		$purchased_cnt++;
	} // end if
} // end foreach

// 서비스종료 & 소장이 없다면
if($comics['cm_service'] != 'y' && $nm_member['mb_class'] != 'a' && count($episode_arr) == 0){
	cs_alert('서비스가 종료된 코믹스 입니다.',NM_URL);
	die;
}

// 추석이벤트 쿠키
if($_event_mode == 'thanksgivingtime'){
	set_cookie('thanksgivingtime', $_comics);
}

/* view */
include_once (NM_PATH.'/_head.php'); // 공통
// 181001 아무타스 리타겟팅 태그
gtm_amutus($nm_member); 

include_once($nm_config['nm_path']."/comics.php");

include_once (NM_PATH.'/_tail.php'); // 공통
?>