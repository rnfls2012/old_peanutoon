<?php
include_once '_common.php'; // 공통
include_once(NM_OAUTH_PATH.'/naver/oauth.lib.php');

if(!defined('NM_NAVER_OAUTH_CLIENT_ID') || !NM_NAVER_OAUTH_CLIENT_ID || !defined('NM_NAVER_OAUTH_SECRET_KEY') || !NM_NAVER_OAUTH_SECRET_KEY)
    alert_opener_url('네이버로그인 API 정보를 설정해 주십시오.');

$oauth = new NAVER_OAUTH(NM_NAVER_OAUTH_CLIENT_ID, NM_NAVER_OAUTH_SECRET_KEY);

$oauth->set_state_token();

$query = $oauth->get_auth_query();

//header('Location: '.$query);
?>