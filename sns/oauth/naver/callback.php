<?php
include_once '_common.php'; // 공통
include_once(NM_OAUTH_PATH.'/naver/oauth.lib.php');

if(!defined('NM_NAVER_OAUTH_CLIENT_ID') || !NM_NAVER_OAUTH_CLIENT_ID || !defined('NM_NAVER_OAUTH_SECRET_KEY') || !NM_NAVER_OAUTH_SECRET_KEY)
    alert_opener_url('네이버 로그인 API 정보를 설정해 주십시오.');

$oauth = new NAVER_OAUTH(NM_NAVER_OAUTH_CLIENT_ID, NM_NAVER_OAUTH_SECRET_KEY);

if($oauth->check_valid_state_token($_GET['state'])) {
	$x_mbs_check_valid_state_token = 'y';					// sns 방법체크(y:정상, n:에러)

    if($oauth->get_access_token($_GET['code'])) {
		$x_mbs_get_access_token = 'y';						// sns 장애체크(y:정상, n:에러)
		$x_mbs_get_check_valid_access_token = '';			// 토큰 정보 없음

        $oauth->get_profile();

        //var_dump($oauth->profile); exit;

        if($oauth->profile->message == 'success') {
			$x_mbs_get_profile_id = 'y';				// sns ID체크(y:정상, n:에러)

            $email = $oauth->profile->email;
            $info  = get_oauth_member_info($oauth->profile->id, $oauth->profile->nickname, 'naver');

			// 회원 이메일 안 넘어올시 -> sns-id_sns-type...180610
			$oauth_mb_id = $email;
			if($oauth_mb_id == ''){ $oauth_mb_id = $oauth->profile->id.'_'.get_oauth_idx('naver'); }

            if($info['id']) {
                unset($nm_member, $nm_sns_member);
                $nm_sns_member = array(
                            'mb_id'			=> $oauth_mb_id,
                            'mb_idx'		=> mb_get_idx($oauth_mb_id),
                            'mb_pass'		=> $info['pass'],
                            'mb_email'		=> $email,
                            'mb_naver_id'	=> $oauth->profile->id,
                            'mb_sns_type'	=> get_oauth_idx('naver'),
                            'mb_nick'		=> get_int_eng_kor_etc(tag_get_filter($info['nick'])),
                            'mb_name'		=> get_int_eng_kor_etc(tag_get_filter($oauth->profile->name)),
                            'mb_level'		=> 2,
                            'mb_join_date'	=> NM_TIME_YMDHIS,
                            'mb_login_date'	=> NM_TIME_YMDHIS,
                            'mb_point'		=> 0
                );
				set_session('ss_mbs_id', $oauth->profile->id);
				set_session('ss_mbs_sns_type', get_oauth_idx('naver'));
            }
        } else {
				$x_mbs_get_profile_id = 'n';
				$x_mbs_msg = '서비스 장애 또는 정보가 올바르지 않습니다(ID오류).';
                alert_close($x_mbs_msg);
        }
    } else {
		$x_mbs_get_access_token = 'n';
		$x_mbs_get_check_valid_access_token = ''; // 토큰 정보 없음
		$x_mbs_msg = '토큰 정보가 올바르지 않습니다.';
        alert_close($x_mbs_msg);
    }
} else {
	$x_mbs_check_valid_state_token = 'n';
	$x_mbs_msg = '올바른 방법으로 이용해 주십시오.';
    alert_close($x_mbs_msg);
}
?>