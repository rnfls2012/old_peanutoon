<?php
include_once '_common.php'; // 공통

//ver1.0 150517 @_untitle_d

if(!defined('NM_TWITTER_CLIENT_ID') || !NM_TWITTER_CLIENT_ID || !defined('NM_TWITTER_SECRET_KEY') || !NM_TWITTER_SECRET_KEY){
    alert_opener_url('트위터로그인 API 정보를 설정해 주십시오.');
}

/* Start session and load library. */
include_once(NM_OAUTH_PATH.'/twitter/oauth.lib.php');


/* Build TwitterOAuth object with client credentials. */
$connection = new TwitterOAuth(NM_TWITTER_CLIENT_ID, NM_TWITTER_SECRET_KEY);
 
/* Get temporary credentials. */
$request_token = $connection->getRequestToken(NM_OAUTH_URL."/twittercallback.php");

/* Save temporary credentials to session. */
$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

/* If last connection failed don't display authorization link. */

switch ($connection->http_code) {
  case 200:
    /* Build authorize URL and redirect user to Twitter. */
    $url = $connection->getAuthorizeURL($token);
	$query = $url;
    // header('Location: '.$url);
	// goto_url($url);
    break;
  default:
    /* Show notification if something went wrong. */
    //echo 'Could not connect to Twitter. Refresh the page or try again later.';
	// header('Location: '.NM_URL); 
	// goto_url(NM_URL);
    // exit;	
	$query = NM_URL;
    break;
}

?>