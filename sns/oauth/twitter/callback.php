<?php
include_once '_common.php'; // 공통
include_once(NM_OAUTH_PATH.'/twitter/oauth.lib.php');

if(!defined('NM_TWITTER_CLIENT_ID') || !NM_TWITTER_CLIENT_ID || !defined('NM_TWITTER_SECRET_KEY') || !NM_TWITTER_SECRET_KEY)
    alert_opener_url('트위터 로그인 API 정보를 설정해 주십시오.');

/* If the oauth_token is old redirect to the connect page. */
if (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
    $_SESSION['oauth_status'] = 'oldtoken';
	$url = NM_URL.'/ctlogout.php';
    // header('Location: '.$url);
	alert('oauth_token 일치하지 않습니다.', $url);
}

/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
$connection = new TwitterOAuth(NM_TWITTER_CLIENT_ID, NM_TWITTER_SECRET_KEY, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

/* Request access tokens from twitter */
$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

/* Save the access tokens. Normally these would be saved in a database for future use. */
$_SESSION['access_token'] = $access_token;

/* Remove no longer needed request tokens */
unset($_SESSION['oauth_token']);
unset($_SESSION['oauth_token_secret']);
if (200 == $connection->http_code) {
	$x_mbs_check_valid_state_token = 'y';					// sns 방법체크(y:정상, n:에러)

    $content  = $connection->get('account/verify_credentials');
	// print_r($content);
	// exit;
	
	if($content->id) {
		$x_mbs_get_access_token = 'y';						// sns 장애체크(y:정상, n:에러)
		$x_mbs_get_access_token = '';						// 토큰 정보 없음
		$x_mbs_get_check_valid_access_token = '';			// 토큰 정보 없음

		$email = '';
		$info  = get_oauth_member_info($content->id, $content->screen_name, 'twitter');
		unset($nm_member, $nm_sns_member);
		$nm_sns_member = array(
					'mb_id'			=> 'https://twitter.com/'.$content->screen_name,
					'mb_idx'		=> mb_get_idx('https://twitter.com/'.$content->screen_name),
					'mb_pass'		=> $info['pass'],
					'mb_email'		=> $email,
					'mb_twitter_id'	=> $content->id,
					'mb_sns_type'	=> get_oauth_idx('twitter'),
					'mb_nick'		=> get_int_eng_kor_etc(tag_get_filter(str_replace(' ', '', $content->screen_name))),
					'mb_name'		=> get_int_eng_kor_etc(tag_get_filter(str_replace(' ', '', $content->name))),
					'mb_level'		=> 2,
					'mb_join_date'	=> NM_TIME_YMDHIS,
					'mb_login_date'	=> NM_TIME_YMDHIS,
					'mb_point'		=> 0
		);
		set_session('ss_mbs_id', $content->id);
		set_session('ss_mbs_sns_type', get_oauth_idx('twitter'));
	} else {
		$x_mbs_get_profile_id = 'n';
		$x_mbs_get_access_token = '';					// 토큰 정보 없음
		$x_mbs_get_check_valid_access_token = '';		// 토큰 정보 없음
		$x_mbs_msg = '서비스 장애 또는 정보가 올바르지 않습니다(ID오류).';
		alert_close($x_mbs_msg);
	}
}else{
	$x_mbs_check_valid_state_token = 'n';
	$x_mbs_msg = '올바른 방법으로 이용해 주십시오.';
    alert_close($x_mbs_msg);
	/*
	$url = NM_URL.'/ctlogout.php';
	alert('올바른 방법으로 이용해 주십시오.', $url);
	*/
}
?>