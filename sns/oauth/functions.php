<?php

function get_oauth_member_info($no, $nick, $service)
{
    if(!$no || !$service){
        return '';
	}

    $id   = '';
    $str  = '';
    $pass  = '';
    $info = array();

    if(strlen($no) > 16){
        $no = str_baseconvert($no);
	}

	$str = get_oauth_idx($service);

    if($str){
        // $id = $str.NM_OAUTH_ID_DELIMITER.$no;
        $id = $no.NM_OAUTH_ID_DELIMITER.$str;
	}

	while($pass == ''){
		$pass = get_encrypt_string(pack('V*', rand(), rand(), rand(), rand()));
        usleep(10000); // 100분의 1초를 쉰다
	}
    $nick = NM_OAUTH_NICK_PREFIX.preg_replace('#[^0-9A-Za-zㄱ-ㅎ가-힣]#', '', $nick);

    $info = array('id' => $id, 'pass' => $pass, 'nick' => $nick);

    return $info;
}


function get_oauth_idx($service)
{
    if(!$service)
        return '';

    $str  = '';

    switch($service) {
        case 'naver':
            $str = 'nid';
            break;
        case 'kakao':
            $str = 'kko';
            break;
        case 'facebook':
            $str = 'fcb';
            break;
        case 'google':
            $str = 'ggl';
            break;
        case 'twitter':
            $str = 'twt';
            break;
        default:
            alert('올바르게 이용해 주십시오.');
            break;
    }

    return $str;
}

function get_oauth_full($service_idx)
{
    if(!$service_idx)
        return '';

    $str  = '';

    switch($service_idx) {
        case 'nid':
            $str = 'naver';
            break;
        case 'kko':
            $str = 'kakao';
            break;
        case 'fcb':
            $str = 'facebook';
            break;
        case 'ggl':
            $str = 'google';
            break;
        case 'twt':
            $str = 'twitter';
            break;
        default:
            alert('올바르게 이용해 주십시오.');
            break;
    }

    return $str;
}

function alert_opener_url($msg='', $url=NM_URL)
{
    $url = str_replace('&amp;', '&', $url);
    $url = preg_replace("/[\<\>\'\"\\\'\\\"\(\)]/", "", $url);

    // url 체크
    check_url_host($url);

    echo '<script>'.PHP_EOL;
    if(trim($msg))
        echo 'alert("'.$msg.'");'.PHP_EOL;
    echo 'window.opener.location.href = "'.$url.'";'.PHP_EOL;
    echo 'window.close();'.PHP_EOL;
    echo '</script>';
    exit;
}

// http://php.net/manual/kr/function.base-convert.php#109660
function str_baseconvert($str, $frombase=10, $tobase=36)
{
    $str = trim($str);
    if (intval($frombase) != 10) {
        $len = strlen($str);
        $q = 0;
        for ($i=0; $i<$len; $i++) {
            $r = base_convert($str[$i], $frombase, 10);
            $q = bcadd(bcmul($q, $frombase), $r);
        }
    }
    else $q = $str;

    if (intval($tobase) != 10) {
        $s = '';
        while (bccomp($q, '0', 0) > 0) {
            $r = intval(bcmod($q, $tobase));
            $s = base_convert($r, 10, $tobase) . $s;
            $q = bcdiv($q, $tobase, 0);
        }
    }
    else $s = $q;

    return $s;
}

function is_social_connected($mb_id, $service)
{
	/*
    global $g5;

    $sql = " select sm_id from {$g5['social_member_table']} where mb_id = '$mb_id' and sm_service = '$service' ";
    $row = sql_fetch($sql);

    return $row['sm_id'] ? true : false;
	*/
}

function reset_social_info()
{
	/*
    unset($GLOBALS['member']);

    set_session('ss_mb_id', '');
    set_session('ss_oauth_member_'.get_session('ss_oauth_member_no').'_info', '');
    set_session('ss_oauth_member_no', '');
	*/
}


function get_oauth_x_mbs($tb_name, $x_mbs_service, $x_mbs_query, $x_mbs_msg, $x_mbs_state)
{
	
	$sql_x_mbs = "
	 INSERT INTO ".$tb_name." ( x_mbs_service, x_mbs_query, x_mbs_msg, x_mbs_state, x_mbs_date, x_mbs_ss_id ) 
	 VALUES( '".$x_mbs_service."', '".$x_mbs_query."', '".$x_mbs_msg."',  '".$x_mbs_state."',  '".NM_TIME_YMDHIS."',  '".session_id()."')	
	";
	sql_query($sql_x_mbs);

}





// 관련 DB - table
/*
CREATE TABLE x_member_sns_join (
	x_mbs_no INT(11) NOT NULL AUTO_INCREMENT,
	x_mbs_service VARCHAR(30) NOT NULL DEFAULT '' COMMENT 'sns 종류',
	x_mbs_query TEXT NOT NULL COMMENT 'sns query',
	x_mbs_msg VARCHAR(200) NOT NULL DEFAULT '' COMMENT 'sns error 메세지',
	x_mbs_state TINYINT(2) NOT NULL DEFAULT '1' COMMENT 'sns 상태(0:정상, 1:로그)',
	x_mbs_date VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',
	PRIMARY KEY (x_mbs_no),
	INDEX x_mbs_service (x_mbs_service),
	INDEX x_mbs_query_state (x_mbs_state)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

CREATE TABLE x_member_sns_login (
	x_mbs_no INT(11) NOT NULL AUTO_INCREMENT,
	x_mbs_service VARCHAR(30) NOT NULL DEFAULT '' COMMENT 'sns 종류',
	x_mbs_query TEXT NOT NULL COMMENT 'sns query',
	x_mbs_msg VARCHAR(200) NOT NULL DEFAULT '' COMMENT 'sns error 메세지',
	x_mbs_state TINYINT(2) NOT NULL DEFAULT '1' COMMENT 'sns 상태(0:정상, 1:로그)',
	x_mbs_date VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',
	PRIMARY KEY (x_mbs_no),
	INDEX x_mbs_service (x_mbs_service),
	INDEX x_mbs_query_state (x_mbs_state)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

CREATE TABLE x_member_sns_callback (
	x_mbs_no INT(11) NOT NULL AUTO_INCREMENT,
	x_mbs_service VARCHAR(30) NOT NULL DEFAULT '' COMMENT 'sns 종류',
	x_mbs_query TEXT NOT NULL COMMENT 'sns query',
	x_mbs_msg VARCHAR(200) NOT NULL DEFAULT '' COMMENT 'sns error 메세지',
	x_mbs_state TINYINT(2) NOT NULL DEFAULT '1' COMMENT 'sns 상태(0:정상, 1:로그)',
	x_mbs_date VARCHAR(20) NOT NULL DEFAULT '' COMMENT '저장일',
	PRIMARY KEY (x_mbs_no),
	INDEX x_mbs_service (x_mbs_service),
	INDEX x_mbs_query_state (x_mbs_state)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

*/
?>