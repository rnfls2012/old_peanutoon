<?php
include_once '_common.php'; // 공통
include_once(NM_OAUTH_PATH.'/kakao/oauth.lib.php');

if(!defined('NM_KAKAO_OAUTH_REST_API_KEY') || !NM_KAKAO_OAUTH_REST_API_KEY)
    alert_opener_url('카카오로그인 API 정보를 설정해 주십시오.');

$oauth = new KAKAO_OAUTH(NM_KAKAO_OAUTH_REST_API_KEY);

$oauth->set_state_token();

$query = $oauth->get_auth_query();

//header('Location: '.$query);
?>