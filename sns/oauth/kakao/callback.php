<?php
include_once '_common.php'; // 공통
include_once(NM_OAUTH_PATH.'/kakao/oauth.lib.php');

if(!defined('NM_KAKAO_OAUTH_REST_API_KEY') || !NM_KAKAO_OAUTH_REST_API_KEY)
    alert_opener_url('카카오 로그인 API 정보를 설정해 주십시오.');

$oauth = new KAKAO_OAUTH(NM_KAKAO_OAUTH_REST_API_KEY);

if($oauth->check_valid_state_token($_GET['state'])) {
	$x_mbs_check_valid_state_token = 'y';					// sns 방법체크(y:정상, n:에러)

    if($oauth->get_access_token($_GET['code'])) {
		$x_mbs_get_access_token = 'y';						// sns 장애체크(y:정상, n:에러)
		$x_mbs_get_check_valid_access_token = '';			// 토큰 정보 없음

        $oauth->get_profile();

        // var_dump($oauth->profile); exit;

        if($oauth->profile->id) {
			$x_mbs_get_profile_id = 'y';					// sns ID체크(y:정상, n:에러)

            $email = $oauth->profile->kaccount_email; //https://developers.kakao.com/docs/restapi#사용자-관리-사용자-정보-요청 170430
            $info  = get_oauth_member_info($oauth->profile->id, $oauth->profile->properties->nickname, 'kakao');

            if($info['id']) {
                unset($nm_member, $nm_sns_member);
                $nm_sns_member = array(
                            'mb_id'			=> $oauth->profile->id,
                            'mb_idx'		=> mb_get_idx($oauth->profile->id),
                            'mb_pass'		=> $info['pass'],
                            'mb_email'		=> $email,
                            'mb_kakao_id'	=> $oauth->profile->id,
                            'mb_sns_type'	=> get_oauth_idx('kakao'),
                            'mb_nick'		=> get_int_eng_kor_etc(tag_get_filter($info['nick'])),
                            'mb_name'		=> get_int_eng_kor_etc(tag_get_filter($oauth->profile->properties->nickname)),
                            'mb_level'		=> 2,
                            'mb_join_date'	=> NM_TIME_YMDHIS,
                            'mb_login_date'	=> NM_TIME_YMDHIS,
                            'mb_point'		=> 0
                );
				set_session('ss_mbs_id', $oauth->profile->id);
				set_session('ss_mbs_sns_type', get_oauth_idx('kakao'));
            }
        } else {
				$x_mbs_get_profile_id = 'n';
				$x_mbs_msg = '서비스 장애 또는 정보가 올바르지 않습니다(ID오류).';
                alert_close($x_mbs_msg);
        }
    } else {
		$x_mbs_get_access_token = 'n';
		$x_mbs_get_check_valid_access_token = ''; // 토큰 정보 없음
		$x_mbs_msg = '토큰 정보가 올바르지 않습니다.';
        alert_close($x_mbs_msg);
    }
} else {
	$x_mbs_check_valid_state_token = 'n';
	$x_mbs_msg = '올바른 방법으로 이용해 주십시오.';
    alert_close($x_mbs_msg);
}
?>