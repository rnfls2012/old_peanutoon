<?
include_once '_common.php'; // 공통
include_once(NM_OAUTH_PATH.'/functions.php');

// 로그인 이젠 페이지 쿠키
$link	= $cookie_http_referer = urldecode(get_session('ss_http_referer'));
if($cookie_http_referer == ''){ $link = NM_URL; }
$link_msg = "";


if(is_dev()){
	// echo get_js_cookie('http_referer');
	// die;
}

$req_service	= get_session('ss_oauth_request_service');
$req_query		= get_session('ss_oauth_request_query');

// 상태체크, 가입체크
$state	= $mb_join_state	= 0;
$msg	= '';

set_session('ss_oauth_request_service'	, '');
set_session('ss_oauth_request_query'	, '');

$service = preg_replace('#[^a-z]#', '', $_GET['service']);

$service = "twitter";

$service_chk = '';
switch($service) {
    case 'naver' :
    case 'kakao' :
    case 'facebook' :
    case 'google' :
    case 'twitter' :
        break;
    default : $service_chk = 'error';
        break;
}


if($service_chk == 'error'){

	$state = 999;
	$x_mbs_callback_msg = "소셜 로그인 서비스가 올바르지 않습니다.(".$service." service_chk callback API Error [".NM_TIME_YMDHIS."] )";
	get_oauth_x_mbs('x_member_sns_callback', $service, $req_query, $x_mbs_callback_msg, $state);

	alert($x_mbs_callback_msg1, NM_URL);
	die;
}

$x_mbs_msg='';

require NM_OAUTH_PATH.'/'.$service.'/callback.php';
$ss_mbs_id			= get_session('ss_mbs_id');
$ss_mbs_sns_type	= get_session('ss_mbs_sns_type');
$member_sns_id_join = $ss_mbs_id.'_'.$ss_mbs_sns_type;



// 콜백로그 남기기	
$sql_x_mbs_callback_query = "";
$sql_x_mbs_callback_query.= " x_mbs_check_valid_state_token=".$x_mbs_check_valid_state_token." | ";
$sql_x_mbs_callback_query.= " x_mbs_get_access_token=".$x_mbs_get_access_token." | ";
$sql_x_mbs_callback_query.= " x_mbs_get_check_valid_access_token=".$x_mbs_get_check_valid_access_token." | ";
$sql_x_mbs_callback_query.= " x_mbs_get_profile_id=".$x_mbs_get_profile_id." | ";
$sql_x_mbs_callback_query.= " x_mbs_msg=".$x_mbs_msg." ";

get_oauth_x_mbs('x_member_sns_callback', $service, $sql_x_mbs_callback_query, '', '0');

if( $nm_sns_member['mb_id'] == "" ) { // 필수값 sns_ID 에러
	$state = 99;

	$x_mbs_callback_msg = "소셜 로그인 필수값인 ID이 넘어오지 않았습니다.(".$service." service_chk callback API-sns_id_null Error [".NM_TIME_YMDHIS."] )";
	get_oauth_x_mbs('x_member_sns_callback', $service, implode(" || ",$nm_sns_member), $x_mbs_callback_msg, $state);

	alert($x_mbs_callback_msg, NM_URL);
	die;

}else{
	/* 회원 있는지 체크 */
	$sql_mb = " 
	 SELECT count(*) as mb_total FROM member 
	 WHERE mb_state='y' AND mb_idx ='".$nm_sns_member['mb_idx']."' AND mb_id = '".$nm_sns_member['mb_id']."' 
	"; // 1. 상태 처리
	$mb_total = sql_count($sql_mb, 'mb_total');

	if($mb_total > 1){

		$login_id = $nm_sns_member['mb_id'];
		$login_idx = $nm_sns_member['mb_idx'];

		// 181024 - 중복로그인 처리 / 초대코드로 검색
		$sql_mb_mb_invite_code = "  SELECT count(*) as mb_total FROM member 
									WHERE mb_id = '".$login_id."' AND mb_idx ='".$login_idx."' 
									AND mb_state='y' AND mb_invite_code is not null; "; // 검색
		$mb_mb_invite_code_total = sql_count($sql_mb_mb_invite_code, 'mb_total');

		if($mb_mb_invite_code_total != 1){
			// 초대코드로 검색
			$sql_overlap_only_find = "SELECT *, min(mb_no) FROM member 
				WHERE mb_id = '".$login_id."' AND mb_idx ='".$login_idx."' AND  mb_state='y';"; // 1. 상태 처리
		}else{
			$sql_overlap_only_find = "SELECT *, min(mb_no) FROM member 
				WHERE mb_id = '".$login_id."' AND mb_idx ='".$login_idx."' AND  mb_state='y'
				AND mb_invite_code is not null;"; // 1. 상태 처리
		}
		$row_overlap_only_find = sql_fetch($sql_overlap_only_find);
		// 위 빼고 전부 다 탈퇴상태로 전환
		if(intval($row_overlap_only_find['mb_no']) > 0){
			$sql_update_overlap_only_find="
					UPDATE member set mb_state='n', mb_state_sub='a', mb_out_date='".NM_TIME_YMDHIS."' 
					WHERE mb_id = '".$login_id."' AND mb_idx ='".$login_idx."' AND  mb_state='y'
					AND not mb_no ='".$row_overlap_only_find['mb_no']."';
			";
			sql_query($sql_update_overlap_only_find);
			
			/* 회원 있는지 중복체크 - 재확인 */
			$sql_mb2 = " 
			 SELECT count(*) as mb_total FROM member 
			 WHERE mb_state='y' AND mb_idx ='".$nm_sns_member['mb_idx']."' AND mb_id = '".$nm_sns_member['mb_id']."' 
			"; // 1. 상태 처리
			$mb_total2 = sql_count($sql_mb2, 'mb_total');
			if($mb_total2 > 1){
				$state	= 1;
				$x_mbs_callback_msg = "소셜 로그인 필수값인 ID가 중복입니다. 관리자에게 메일문의하세요.(".$service." service_chk callback API-sns_id : ".$nm_sns_member['mb_id']." - ".$mb_total." Error [".NM_TIME_YMDHIS."] )";
				get_oauth_x_mbs('x_member_sns_callback', $service, $nm_sns_member['mb_id'], $x_mbs_callback_msg, $state);

				alert($x_mbs_callback_msg, NM_URL);
				die;
			}else{
				$mb_total = $mb_total2;
			}
		}else{
			$state	= 1;
			$x_mbs_callback_msg = "소셜 로그인 필수값인 ID가 중복입니다. 관리자에게 메일문의하세요.(".$service." service_chk callback API-sns_id : ".$nm_sns_member['mb_id']." - ".$mb_total." Error [".NM_TIME_YMDHIS."] )";
			get_oauth_x_mbs('x_member_sns_callback', $service, $nm_sns_member['mb_id'], $x_mbs_callback_msg, $state);

			alert($x_mbs_callback_msg, NM_URL);
			die;
		}
	}
	// }else if($mb_total == 1){
	if($mb_total == 1){
		
		// 중복인지 체크
		$mb_sns =  mb_get($nm_sns_member['mb_id']);

		// 탈퇴한 아이디인가?
		if ($mb_sns['mb_state'] == 'n') {		

			$link = NM_URL."/ctlogin.php"; 
			$state = 4;
			
			$x_mbs_callback_msg = "소셜 로그인 필수값인 ID가 탈퇴상태입니다. 관리자에게 메일문의하세요.(".$service." service_chk callback API-sns_id : ".$nm_sns_member['mb_id']." - ".$mb_sns['mb_state']."  Error [".NM_TIME_YMDHIS."] )";
			
			get_oauth_x_mbs('x_member_sns_callback', $service, $nm_sns_member['mb_id']."-".$mb_sns['mb_state'], $x_mbs_callback_msg, $state);

		} // end if
		
		// 중지된 아이디인가?
		if ($mb_sns['mb_state'] == 'y' && $mb_sns['mb_state_sub'] == 's') {

			$link = NM_URL."/ctlogin.php";
			$state = 3;			
						
			$x_mbs_callback_msg = "소셜 로그인 필수값인 ID가 이용 중지 상태입니다. 관리자에게 메일문의하세요.(".$service." service_chk callback API-sns_id : ".$nm_sns_member['mb_id']." - ".$nm_sns_member['mb_id']."-".$mb_sns['mb_state']."-".$mb_sns['mb_state_sub']."  Error [".NM_TIME_YMDHIS."] )";
			
			get_oauth_x_mbs('x_member_sns_callback', $service, $nm_sns_member['mb_id']."-".$mb_sns['mb_state']."-".$mb_sns['mb_state_sub'], $x_mbs_callback_msg, $state);
			
			cs_alert($x_mbs_callback_msg, $link);
			die;
		} // end if
		
		// 휴면한 아이디인가?
		if ($mb_sns['mb_state'] == 'y' && $mb_sns['mb_state_sub'] == 'h') {
			$_SESSION['ss_human_id'] = $mb_sns['mb_id'];
			$_SESSION['ss_human_referer'] = $cookie_http_referer;
			$link = NM_URL."/sleepcancel.php";
			$state = 5;
			
			$x_mbs_callback_msg = "소셜 로그인 필수값인 ID가 휴면 상태입니다. 관리자에게 메일문의하세요.(".$service." service_chk callback API-sns_id : ".$nm_sns_member['mb_id']." - ".$nm_sns_member['mb_id']."-".$mb_sns['mb_state']."-".$mb_sns['mb_state_sub']."  Error [".NM_TIME_YMDHIS."] )";
			
			get_oauth_x_mbs('x_member_sns_callback', $service, $nm_sns_member['mb_id']."-".$mb_sns['mb_state']."-".$mb_sns['mb_state_sub'], $x_mbs_callback_msg, $state);

		} // end if

		// 휴면 대기인 아이디인가?
		if($mb_sns['mb_state'] == 'y' && $mb_sns['mb_state_sub'] == 't') {
			
			$state = 0;
			
			$x_mbs_callback_msg = "소셜 로그인 필수값인 ID가 휴면 대기 상태입니다. 관리자에게 메일문의하세요.(".$service." service_chk callback API-sns_id : ".$nm_sns_member['mb_id']." - ".$nm_sns_member['mb_id']."-".$mb_sns['mb_state']."-".$mb_sns['mb_state_sub']."  Error [".NM_TIME_YMDHIS."] )";
			
			get_oauth_x_mbs('x_member_sns_callback', $service, $nm_sns_member['mb_id']."-".$mb_sns['mb_state']."-".$mb_sns['mb_state_sub'], $x_mbs_callback_msg, $state);

			$state_sub_sql = "UPDATE member set mb_state_sub='y', mb_human_email_date='' WHERE mb_no='".$mb_sns['mb_no']."' ";
			sql_query($state_sub_sql);
		} // end if	

	}else if($mb_total == 0){
		$state = -1;
		$x_mbs_callback_msg = "소셜 로그인 미가입 상태입니다. 관리자에게 메일문의하세요.(".$service." service_chk callback API-sns_id : ".$nm_sns_member['mb_id']." - ".$nm_sns_member['mb_id']."-".$mb_sns['mb_state']."-".$mb_sns['mb_state_sub']."   total:".$mb_total."  Error [".NM_TIME_YMDHIS."] )";

		get_oauth_x_mbs('x_member_sns_callback', $service, implode(" || ",$nm_sns_member), $x_mbs_callback_msg, $state);

	}else{
			
		$state = 9;
		
		$x_mbs_callback_msg = "소셜 로그인 에러 상태입니다. 관리자에게 메일문의하세요.(".$service." service_chk callback API-sns_id : ".$nm_sns_member['mb_id']." - ".$nm_sns_member['mb_id']."-".$mb_sns['mb_state']."-".$mb_sns['mb_state_sub']."   total:".$mb_total."  Error [".NM_TIME_YMDHIS."] )";

		get_oauth_x_mbs('x_member_sns_callback', $service, implode(" || ",$nm_sns_member), $x_mbs_callback_msg, $state);

		alert($x_mbs_callback_msg, NM_URL.'/ctlogin.php');
		die;
	}
}


// 로그인
if($state == 0){
	set_session('ss_mb_id', $mb_sns['mb_id']);
	set_session('ss_mb_no', $mb_sns['mb_no']);
	
	// SNS 로그인 한 ID에 SNS고유번호 없거나 sns 타입이 다르다면 업데이트
	if($mb_sns['mb_'.$service.'_id'] == '' || $mb_sns['mb_sns_type'] != $nm_sns_member['mb_sns_type']){
		$sql_mb_sns_update = "UPDATE member SET mb_".$service."_id='".$nm_sns_member['mb_'.$service.'_id']."', 
								mb_sns_type='".$nm_sns_member['mb_sns_type']."' 
								WHERE mb_no='".$mb_sns['mb_no']."'";
		if(sql_query($sql_mb_sns_update)){				
		}else{
			$x_mbs_callback_msg = "소셜 로그인 sns id update 에러 상태입니다. 관리자에게 메일문의하세요.(".$service." service_chk callback API-sns_id : ".$mb_sns['mb_no']."-".$nm_sns_member['mb_'.$service.'_id']."   Error [".NM_TIME_YMDHIS."] )";
			
			get_oauth_x_mbs('x_member_sns_callback', $service, $sql_mb_sns_update, $x_mbs_callback_msg, '-9');
		}
	}
	
	// 회원정보 업데이트 & 회원로그
	mb_login_date_update($mb_sns, get_js_cookie('mb_token'));
	mb_stats_log_member_update($mb_sns);

	//남성 성인 회원 로그인 시 성인 장르로 유도 URL(지시사항) - 171206
	if(substr($cookie_http_referer, 0, -1) == NM_DOMAIN || $cookie_http_referer == NM_DOMAIN || $cookie_http_referer == NM_DOMAIN."/index.php" || $cookie_http_referer == ""){
		if($mb_sns['mb_adult'] == 'y' && $mb_sns['mb_sex'] == 'm'){ 	
			$link = NM_DOMAIN."/cmgenre.php?menu=6"; 
			if(get_js_cookie("redirect") !=''){
				$link = goto_redirect(get_js_cookie("redirect"));
			}
		}
	}
	$adult_yn = 'n';
	if($mb_sns['mb_adult'] == 'y') { $adult_yn = 'y'; } // 성인이 로그인 했을때는 성인탭으로 강제 이동 171110
	if($adult_yn != ''){ 	$link = cs_para_attach($link, 'adult='.$adult_yn); }

	// mkt - ICE COUNTER LOGIN
	rdt_acecounter_ss_login('y');
	
	// 자동로그인 ---------------------------
	// 쿠키 3달간 저장
	$key = cs_mb_auto_key($mb_sns);

	set_cookie('ck_mb_id', $mb_sns['mb_id'], 86400 * 90);
	set_cookie('ck_mb_auto', $key, 86400 * 90);

	$_SESSION['ss_login_state'] = $state;

	$_SESSION['ss_login_id'] = '';
	$_SESSION['ss_login_pw'] = '';

}else if($state == -1){
	$sql_zmbsj_del = 
	"DELETE FROM z_member_sns_join WHERE  
	 zmbs_id = '".$member_sns_id_join."' AND 
	 zmbs_idx = '".mb_get_idx($member_sns_id_join)."' AND 
	 zmbsj_sns_type = '".$nm_sns_member['mb_sns_type']."' 
	";
	if(sql_query($sql_zmbsj_del)){			
		// sns_member_email
		$nm_sns_member_email = "'".$nm_sns_member['mb_email']."'";
		if($nm_sns_member['mb_email'] == ''){ $nm_sns_member_email = "NULL"; }

		$sql_zmbsj_insert = 
		"INSERT INTO z_member_sns_join( zmbs_id, zmbs_idx, 
		 zmbsj_id, zmbsj_idx, zmbsj_pass, 
		 zmbsj_email, 
		 zmbsj_".$service."_id, zmbsj_sns_type,  
		 zmbsj_nick, 
		 zmbsj_name, 
		 zmbsj_level,  
		 zmbsj_date ) VALUES (
		 '".$member_sns_id_join."', '".mb_get_idx($member_sns_id_join)."', 
		 '".$nm_sns_member['mb_id']."', '".$nm_sns_member['mb_idx']."', '".$nm_sns_member['mb_pass']."', 
		 ".$nm_sns_member_email.", 
		 '".$nm_sns_member['mb_'.$service.'_id']."', '".$nm_sns_member['mb_sns_type']."', 
		 '".get_int_eng_kor_etc(tag_get_filter($nm_sns_member['mb_nick']))."', 
		 '".get_int_eng_kor_etc(tag_get_filter($nm_sns_member['mb_name']))."', 
		 '".$nm_sns_member['mb_level']."', 
		 '".$nm_sns_member['mb_join_date']."' 
		 )
		";
		sql_query($sql_zmbsj_insert);	

		$zmbsj_id_encrypt_aes256 = Security::encrypt_aes256($member_sns_id_join, NM_JOIN_SNS_ENC_KEY);

		$link = NM_URL."/ctjoinsns.php?zmbsj_id=".$zmbsj_id_encrypt_aes256;
	}else{
		alert('sns가입에러 가입자에게 문의하시기 바랍니다.',NM_URL);
		die;
	}
}

unset($sql_mb, $mb_total, $mb_overlap, $mb_sns, $nm_sns_member);

goto_url($link);

?>