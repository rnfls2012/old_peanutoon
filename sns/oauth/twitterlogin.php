<?php
include_once('./_common.php');
include_once(NM_OAUTH_PATH.'/functions.php');


// 로그인 이젠 페이지 세션저장
$config_http_referer = HTTP_REFERER;
if($config_http_referer == ''){
	$config_http_referer = NM_URL;
}else{
	if(strpos($config_http_referer, 'ctlogin.php') || strpos($config_http_referer, 'ctjoin.php') || strpos($config_http_referer, 'ctjoinup.php') || strpos($config_http_referer, 'ctjoinsns.php')){
		$cookie_http_referer = urldecode(get_session('ss_http_referer'));
	}else{
		set_session('ss_http_referer', urlencode($config_http_referer));
		$cookie_http_referer = $config_http_referer;
	}
}
if($cookie_http_referer == ''){ $cookie_http_referer = NM_URL; }

if($_redirect !=''){ // 리다이렉트 추가-18-12-03
	$cookie_http_referer = goto_redirect($_redirect);
	set_session('ss_http_referer', urlencode($config_http_referer));
	$cookie_http_referer = $config_http_referer;
}

// cs_login_check() 으로 왔다면...171206
$ss_cs_login_check_http_referer = urldecode(get_session('ss_cs_login_check_http_referer'));
if($ss_cs_login_check_http_referer != ""){
	$cookie_http_referer = $ss_cs_login_check_http_referer;
	del_session('ss_cs_login_check_http_referer');
	$ss_cs_login_check_http_referer = "";
}

// 이전 페이지가 피너툰이 아니라면...
if(intval(strpos($cookie_http_referer, NM_DOMAIN_ONLY)) == 0){
	$cookie_http_referer = NM_URL;		
	set_session('ss_http_referer', urlencode($cookie_http_referer));
}

/*
// 이전 소스
if($nm_config['nm_mode'] == NM_PC){
	$config_http_referer = HTTP_REFERER;
	if($config_http_referer == ''){
		$config_http_referer = NM_URL;
	}else{
		if(strpos($config_http_referer, 'ctlogin.php') || strpos($config_http_referer, 'ctjoin.php')){
			$config_http_referer = urldecode(get_session('ss_http_referer'));
		}
	}
}
*/
$service = 'twitter';

unset($nm_member);
set_session('ss_mb_id', '');
set_session('ss_oauth_member_'.get_session('ss_oauth_member_no').'_info', '');
set_session('ss_oauth_member_no', '');

require NM_OAUTH_PATH.'/twitter/login.php';

// 연동처리를 위한 세션
set_session('ss_oauth_request_service', $service);
set_session('ss_oauth_request_query',	$query);

// 로그 남기기	  
// cs_error_log(NM_OAUTH_PATH.'/'.$service."/log/".NM_TIME_YMD.".log", $query, $_SERVER['PHP_SELF']); // 파일로 남기는 거 삭제(2중 서버)

get_oauth_x_mbs('x_member_sns_login', $service, $query, '', '0');

if($query == NM_URL){
	$x_mbs_login_msg = "SNS 연동이 불가능합니다.(".$service." Network API Error [".NM_TIME_YMDHIS."] )";
	
	get_oauth_x_mbs('x_member_sns_login', $service, $query, $x_mbs_login_msg, '1');

	cs_alert($x_mbs_login_msg, $query);
	die;
}else{
	goto_url($query);
}

?>