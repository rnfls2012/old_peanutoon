<?php
include_once '_common.php'; // 공통
include_once(NM_OAUTH_PATH.'/google/oauth.lib.php');

if(!defined('NM_GOOGLE_CLIENT_ID') || !NM_GOOGLE_CLIENT_ID || !defined('NM_GOOGLE_SECRET_KEY') || !NM_GOOGLE_SECRET_KEY)
    alert_opener_url('구글+ 로그인 API 정보를 설정해 주십시오.');

$oauth = new GOOGLE_OAUTH(NM_GOOGLE_CLIENT_ID, NM_GOOGLE_SECRET_KEY);

$oauth->set_state_token();

$query = $oauth->get_auth_query();

//header('Location: '.$query);
?>