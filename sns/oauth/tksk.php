<?php
include_once('./_common.php');
include_once(NM_OAUTH_PATH.'/functions.php');

$referer_ics 		= num_eng_check(tag_filter($_REQUEST['ics']));
$referer_icspage 	= num_eng_check(tag_filter($_REQUEST['icspage']));

$config_http_referer = NM_TKSK_URL.'/'.$referer_ics;
if($referer_icspage == 'complete'){
	$config_http_referer = NM_TKSK_URL.'/'.$referer_ics.'/'.$referer_icspage;	
}
set_js_cookie('http_referer', urlencode($config_http_referer));
/*
if($nm_config['nm_mode'] == NM_PC){
	$config_http_referer = HTTP_REFERER;
	if($config_http_referer == ''){
		$config_http_referer = NM_URL;
	}else{
		if(strpos($config_http_referer, 'ctlogin.php') || strpos($config_http_referer, 'ctjoin.php')){
			$cookie_http_referer = urldecode(get_js_cookie('http_referer'));
		}else{
			$cookie_http_referer = $config_http_referer;
		}
	}
	set_js_cookie('http_referer', urlencode($config_http_referer));
}
*/


$service = 'facebook';

require NM_OAUTH_PATH.'/'.$service.'/login.php';

// sns 에러 고유ID생성
$x_uniqid = get_uniqid();

// 연동처리를 위한 세션
set_session('ss_oauth_request_service', $service);
set_session('ss_oauth_request_query',	$query);
set_session('ss_oauth_request_uniqid',	$x_uniqid);
set_session('ss_oauth_request_ticketsocket',	'ticketsocket'); /* facebook - ticketsocket 전용 */
set_session('ss_oauth_request_mb_no',	$nm_member['mb_no']); /* facebook - ticketsocket 전용 */

// 로그 남기기	  
// cs_error_log(NM_OAUTH_PATH.'/'.$service."/log/".NM_TIME_YMD.".log", $query, $_SERVER['PHP_SELF']); // 파일로 남기는 거 삭제(2중 서버)
$sql_x_mbs = "	INSERT INTO x_member_sns_ticketsocket ( x_mbs_uniqid, x_mbs_service, x_mbs_login_query, x_mbs_login_query_check, x_mbs_login_date )
				VALUES( '".$x_uniqid."', '".$service."', '".$query."', 'l',  '".NM_TIME_YMDHIS."' )	";
sql_query($sql_x_mbs);

if($query == NM_URL){
	cs_alert("SNS 연동이 불가능합니다.(".$service." Network API Error)", $query);
	die;
}else{
	goto_url($query);
}

?>