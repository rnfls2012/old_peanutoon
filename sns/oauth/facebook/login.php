<?php
include_once '_common.php'; // 공통
include_once(NM_OAUTH_PATH.'/facebook/oauth.lib.php');

if(!defined('NM_FACEBOOK_CLIENT_ID') || !NM_FACEBOOK_CLIENT_ID || !defined('NM_FACEBOOK_SECRET_KEY') || !NM_FACEBOOK_SECRET_KEY)
    alert_opener_url('페이스북로그인 API 정보를 설정해 주십시오.');

$oauth = new FACEBOOK_OAUTH(NM_FACEBOOK_CLIENT_ID, NM_FACEBOOK_SECRET_KEY);

$oauth->set_state_token();

$query = $oauth->get_auth_query();

//header('Location: '.$query);
?>