<? include_once '_common.php'; // 공통 
	// error_page();
	
	$emf_no = num_check($_REQUEST['promo']); // 프로모션 번호

	if($emf_no == "") {
		cs_alert("필수 값이 없습니다!", NM_DOMAIN);
		die;
	} // end if

	$emf_row = pf_row($emf_no);

	/* 페이지 이동 */
	if($emf_row['emf_state'] == "y") {
		set_session('ss_emf_no', $emf_row['emf_no']);
		switch($emf_row['emf_no']) {
			case 1 :
				// 페이지 이동 시작
				$invite_code = base_get_filter($_REQUEST['code']); // 초대 코드
				set_session('ss_invite_code', $invite_code); // SESSION 값 저장

				if($invite_code != "") {
					// 클릭 수 업데이트
					pf_invite_click_update($invite_code);
					
					// 썸네일용
					$head_thumbnail = NM_IMG."_mobile/promoful/1/sns_thumbnail.png".vs_para(); 
					$head_title = "웰컴 투 피너툰!";
					include_once($nm_config['nm_path']."/_head.sub.php");					
					goto_url(NM_URL);
				} else {
					goto_url(NM_PROMOFUL_URL."/invite.php");
				} // end else
				break;
		} // end switch
	} else {
		cs_alert("중지된 프로모션 입니다!", NM_DOMAIN);
		die;
	} // end else
?>