<? 
	include_once '_common.php'; // 공통 
	$invite_img = NM_IMG.'_mobile/promoful/1';
	$li_width = "width: calc(33.3% - 8px);";
	$copy_img = "evt_con02_btn01.png";
	$kakao_img = "evt_con02_btn04.png";
	if(is_app()) {
		$li_width = "width: calc(50% - 8px);";
		$copy_img = "evt_con02_appbtn01.png";
		$kakao_img = "evt_con02_appbtn02.png";
	} // end if
	
	// 18년 1월 이후 이미지 변경 171229
	$evt_con01 = "/evt_con01.png";
	if(NM_TIME_YMD >= '2018-01-01') {
		$evt_con01 = "/evt_con01_50.png";
	} // end if
?>

<!-- body space -->
	<!-- wrapper space -->
		<!-- container space -->
			<script type="text/javascript" src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
			<link rel="stylesheet" type="text/css" href="<?=NM_PROMOFUL_MO_URL;?>/css/invite.css<?=vs_para();?>" />
			<style type="text/css">
				.evt_con02_form ul li { <?=$li_width;?> }
			</style>
			<script type="text/javascript" src="<?=NM_PROMOFUL_MO_URL;?>/js/invite.js<?=vs_para();?>"></script>

			<div class="evtcontainer">
				<div class="evt_header">
					<img src="<?=$invite_img;?>/evt_header.png<?=vs_para();?>" alt="웰컴 투 피너툰" />
				</div>
				<div class="evt_con01">
					<img src="<?=$invite_img.$evt_con01;?><?=vs_para();?>" alt="이벤트 참여방법" />
				</div>
				<div class="evt_con02">
					<div class="evt_con02_tit">
						<img src="<?=$invite_img;?>/evt_con02_tit.png<?=vs_para();?>" alt="친구 초대하기" />
					</div>
					<div class="evt_con02_form">
						<input type="hidden" id="invite_title" class="invite_title" name="invite_title" value="<?=$emf_row['emf_name'];?>"/>
						<input type="text" id="invite_url" class="invite_url" value="<?=$invite_url;?>" placeholder="로그인을 해주세요!" readonly="readonly"/>
						<ul>
							<li>
								<a id="cp_btn" class="cp_btn"><img src="<?=$invite_img.'/'.$copy_img;?>" alt="링크 복사"></a>
							</li>
							<? if(is_app() == false) { ?>
							<li>
								<a onclick="share_sns('twitter','<?=$invite_url?>', 'invite');"><img src="<?=$invite_img;?>/evt_con02_btn02.png<?=vs_para();?>" alt="트위터"></a>
							</li>
							<!--
							<li>
								<a onclick="alertBox('Facebook 정책상 공유 기능을 사용할 수 없습니다.');"><img src="<?=$invite_img;?>/evt_con02_btn03.png<?=vs_para();?>" alt="페이스북"></a>
							</li>
							-->
							<? } // end if ?>
							<li>
								<a onclick="share_sns_app('kakaotalk','<?=$invite_url?>', 'invite');"><img src="<?=$invite_img.'/'.$kakao_img;?>" alt="카카오톡"></a>
							</li>
						</ul>
					</div>
					<div class="evt_con02_score">
						<ol>
							<li>
								<img src="<?=$invite_img;?>/evt_con02_score11.png<?=vs_para();?>" alt="가입한 친구" />
								<span class="score"><?=$invite_member_count;?></span>
								<img src="<?=$invite_img;?>/evt_con02_score12.png<?=vs_para();?>" alt="명" />
							</li>
							<li>
								<img src="<?=$invite_img;?>/evt_con02_score21.png<?=vs_para();?>" alt="결제한 친구" />
								<span class="score"><?=$invite_recharge_count;?></span>
								<img src="<?=$invite_img;?>/evt_con02_score22.png<?=vs_para();?>" alt="명" />
							</li>
							<li>
								<img src="<?=$invite_img;?>/evt_con02_score31.png<?=vs_para();?>" alt="적립된 미니땅콩" />
								<span class="score"><?=$invite_reward_sum;?></span>
								<img src="<?=$invite_img;?>/evt_con02_score32.png<?=vs_para();?>" alt="개" />
							</li>
						</ol>
					</div>
					<div class="evt_con03">
						<img src="<?=$invite_img;?>/evt_con03.png<?=vs_para();?>" alt="이벤트 주의사항" />
					</div>
				</div>
			</div>