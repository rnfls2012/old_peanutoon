<? 
include_once '_common.php'; // 공통

// 변수 초기화
$invite_url = ""; // 초대 URL
$invite_member_count = 0; // 초대된 회원
$invite_reward_count_100 = 0; // 리워드 받은 회원 2018년 이전 100리워드 지급
$invite_reward_count_50 = 0; // 리워드 받은 회원 2018년 이후 50리워드 지급
$invite_recharge_count = 0; // 초대된 회원 중 결제한 회원
$invite_reward_sum = 0; // 보상받은 미니땅콩
$folder_mode = "./".$nm_config['nm_mode'];

// 로그인 체크
if($nm_member['mb_no'] != '' || $nm_member['mb_no'] != NULL || intval($nm_member['mb_no']) > 0) {
	if($_SESSION['ss_emf_no'] == "" || $_SESSION['ss_emf_no'] == NULL) {
		set_session('ss_emf_no', '1');
	} // end if
	
	// 초대 URL 생성
	$emf_select = "SELECT * FROM epromotion_full WHERE emf_no = '".$_SESSION['ss_emf_no']."'";
	$emf_row = sql_fetch($emf_select);
	$invite_url = $emf_row['emf_link']."&code=".urlencode($nm_member['mb_invite_code']);

	// 초대된 회원 구하기
	$invite_count_sql = "SELECT * FROM invite_join WHERE ij_to_member = '".$nm_member['mb_no']."' AND ij_to_member_idx = '".$nm_member['mb_idx']."'";
	$invite_count_result = sql_query($invite_count_sql);
	while($invite_count_row = sql_fetch_array($invite_count_result)) {
		// 결제한 회원 카운트 업
		if($invite_count_row['ij_recharge'] == '1') {
			$invite_recharge_count++;
		} // end if

		// 가입한 회원 카운트 업
		$invite_member_count++;

		// 보상받은 회원 카운트 업 20180103 수정
		if($invite_count_row['ij_invite_reward'] == '1') {
			if($invite_count_row['ij_date'] < '2018-01-01') {
				$invite_reward_count_100++;
			} else { 
				$invite_reward_count_50++;
			} // end else
		} // end if
	} // end while
} // end if

$invite_reward_sum = number_format((100 * ($invite_recharge_count + $invite_reward_count_100)) + ($invite_reward_count_50 * 50));
$head_thumbnail = NM_IMG."promoful/1/sns_thumbnail_kakao.png".vs_para();

/* view */
include_once (NM_PATH.'/_head.php'); // 공통

include_once($folder_mode.'/invite.php');

include_once (NM_PATH.'/_tail.php'); // 공통
?>