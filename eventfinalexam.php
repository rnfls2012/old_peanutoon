<?php
include_once '_common.php'; // 공통

$finalexam_img_url = NM_IMG.'/finalexam/reception'; // 이미지 경로

$efp_no = num_check(tag_get_filter($_REQUEST['efp_no'])); // 교시값 파라미터
$efp_arr = $ef_arr = array();

if($is_member == false){
	alert('올바른 방법으로 이용하시기 바랍니다.', NM_URL.'/eventfinalexam_reception.php');
	die;
}else{
	// 시험정보 가져오기
	$sql_efp = "SELECT * FROM event_finalexam_period WHERE efp_state = 'y' AND efp_no='".$efp_no."' ";
	$row_efp = sql_fetch($sql_efp);
	$efp_arr = $row_efp;

	$sql_efm = " SELECT * FROM event_finalexam_member WHERE 
	            efm_member = '".$nm_member['mb_no']."' AND  efm_member_idx = '".$nm_member['mb_idx']."' 
			   ";
	$row_efm = sql_fetch($sql_efm);
	$z_efm_count = intval($row_efm['efm_count'.$row_efp['efp_mark']])+1;
	if($z_efm_count > 3){		
		alert($row_efp['efp_mark'].'교시의 응시하기가 3번 넘었습니다.', NM_URL.'/eventfinalexam_reception.php');
		die;
	}

	// 시험지 가져오기
	$sql_ef = "SELECT * FROM event_finalexam WHERE ef_state='y' AND ef_efp_no='".$efp_no."' ORDER BY RAND() LIMIT 0, 20 ";
	// $sql_ef = "SELECT * FROM event_finalexam WHERE ef_state='y' AND ef_efp_no='".$efp_no."' ORDER BY ef_no LIMIT 0, 30 "; // 문제 확인용
	$result_ef = sql_query($sql_ef);
	for($e=1; $row_ef = sql_fetch_array($result_ef); $e++){
		// 이미지 처리
		$row_ef['ef_example_img_url'] = "";
		if($row_ef['ef_example_img'] != ''){
			$row_ef['ef_example_img_url'] = img_url_para($row_ef['ef_example_img'], $row_ef['ef_reg_date'], $row_ef['ef_mod_date']);
		}
		for($c=1; $c<=5; $c++){
			$row_ef['ef_choice'.$c.'_img_url'] = "";
			if($row_ef['ef_choice'.$c.'_img'] != ''){
				$row_ef['ef_choice'.$c.'_img_url'] = img_url_para($row_ef['ef_choice'.$c.'_img'], $row_ef['ef_reg_date'], $row_ef['ef_mod_date']);
			}
		}

		//view
		$row_ef['ef_no_txt'] = $e;
		$row_ef['ef_class'] = "hide";
		if($e==1){ $row_ef['ef_class'] = ""; }
		array_push($ef_arr, $row_ef);
	}
}

$ef_count = count($ef_arr);
$ef_time = '15:00';

include_once (NM_PATH.'/_head.php'); // 공통

include_once($nm_config['nm_path']."/eventfinalexam.php");

include_once (NM_PATH.'/_tail.php'); // 공통